import React from "react";
import { createRoot } from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import configureStore, { history } from "./store/configureStore";
import { Router } from "./components/Root";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import io from "socket.io-client";
import Swal from "sweetalert2";
import Cookies from "universal-cookie";
import { string } from "yup";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";

const cookies = new Cookies();
// import '../src/styles/bootstrap/bootstrap.scss'
// //import '../src/styles/antd.less'
// import "../src/styles/layout.scss"
// import "../src/styles/theme.scss"
// //import "../src/styles/ui.scss"
// //import "../src/styles/vendors.scss"
// import "../src/styles/custom.scss";

const socketUrl = "https://apibeta.theoceann.com/";
// const socketUrl = "http://3.1.49.38:9000";
const socket = io(socketUrl, {
  transports: ["websocket", "polling"],
});
// import "../src/styles/context-menu.scss";

window.socket = socket;
// window.emailTitltCorrectFunction = (initialPart, variableArray) => {
//   let strTest = initialPart

//   variableArray.map((item) => {
//     if (item) {
//       strTest = strTest + ` || ${item}`;
//     }
//   });

//   return strTest
// }

window.corrector = (inputString) => {
  const partArray = inputString.split("||");
  let initialStr = "";

  if (Array.isArray(partArray[0]) && partArray[0].length > 0) {
    partArray[0][0] =
      partArray[0][0].charAt(0).toUpperCase() + partArray[0][0].slice(1);
    initialStr = partArray[0].join("") + "Invoice";
  } else if (typeof partArray[0] === "string" && partArray[0].trim() !== "") {
    partArray[0] = partArray[0].replace(/_/g, " ");
    partArray[0] = partArray[0].replace(/\b\w/g, (firstLetter) =>
      firstLetter.toUpperCase()
    );

    if (/\w\w+/.test(partArray[0])) {
      initialStr = partArray[0].replace(/([a-z])([A-Z])/g, "$1 $2") + " ";
    } else {
      initialStr = partArray[0] + " ";
    }
  }

  partArray.slice(1).forEach((item) => {
    if (item !== "undefined") {
      item = item.replace(/_/g, " ");
      item = item.replace(/\b\w/g, (firstLetter) => firstLetter.toUpperCase());
      initialStr = initialStr + ` || ${item}`;
    }
  });

  return initialStr;
};

window.clickedTab = "Analytical Dashboard";
if (localStorage.getItem("lastClickedItem") === null) {
  localStorage.setItem("lastClickedItem", "Analytical Dashboard");
}

const store = configureStore();

let name=''
if(localStorage.getItem('oceanToken')){
  name = JSON.parse(atob(localStorage.getItem('oceanToken').split('.')[1])).first_name 
}

if (name) {
  window.userName = name.charAt(0).toUpperCase() + name.slice(1);
}
window.emitNotification = (userInput) => {
  socket.emit("postNotification", userInput);
};

window.notificationMessageCorrector = (inputString) => {
  const stringArray = inputString.split(",");
  let finalString = "";
  stringArray.map((item) => {
    if (!item.includes("undefined")) {
      finalString = finalString + item;
    }
  });
  return finalString;
};

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);
root.render(
  // <StrictMode>

  <GoogleOAuthProvider clientId="380315452391-tbvplprrntar187htkon5i03kl21i77j.apps.googleusercontent.com">
  <Provider store={store}>
    <RouterProvider router={Router} history={history} />
  </Provider>
  </GoogleOAuthProvider>
);
serviceWorker.unregister();

// Override the XMLHttpRequest object
const originalXHR = window.XMLHttpRequest;

function newXHR() {
  const xhr = new originalXHR();

  // Override the open method to log information about the XHR request
  const originalOpen = xhr.open;
  xhr.open = function (method, url, async, user, password) {
    return originalOpen.call(this, method, url, async, user, password);
  };

    // Override the onreadystatechange event to log when the request completes
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if(xhr.status===401){
              // window.location.href='/#/user/login'
              window.myNavigator("/#/user/login")
            }
            if(xhr.status === 403){
              Swal.fire({
                icon: 'error',
                title: 'Access Denied',
                text: 'You do not have permission to access this resource.',
              });
              // alert("ex")
              // window.myNavigator("/access-denied",{replace:true})
              // return <h1>Hellow</h1>
            }

            // console.log('XHR Response Text:', xhr.responseText);
        }
    };
    return xhr;
}

window.XMLHttpRequest = newXHR;
//    NETWORK INTERCEPTOR FOR THIS TAB WITH ALL MONITERLOGS

// Override the XMLHttpRequest object
