import React, { Component } from 'react';
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CContainer,
  CDataTable,
  CForm,
  CFormGroup,
  CHeader,
  CInput,
  CInputCheckbox,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupText,
  CLabel,
  CRow,
  CTextarea,
} from '@coreui/react';
import UI_CONFIG from './UIConfig';
import CIcon from '@coreui/icons-react';

class Delays extends Component {
  constructor(props) {
    super(props);
    let firstColumns = {};
    UI_CONFIG.delaysfields.map(item => (firstColumns[item.key] = item.field));

    this.state = {
      collapse: false,
      accordion: null,
    };
  }

  setCollapse = value => {
    this.setState({
      ...this.state,
      collapse: value,
    });
  };

  setAccordion = value => {
    this.setState({
      ...this.state,
      accordion: value,
    });
  };

  render() {
    const { accordion } = this.state;

    return (
      <CContainer fluid>
        <div className="table-filter-wrapper mb-20">
          <div className="section">
            <span className="wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li>
                  <CIcon name="cilSave" height="22" />
                  <span className="text-bt">Save</span>
                </li>
                <li>
                  <span className="text-bt">Bunker Prices</span>
                </li>
                <li>
                  <CIcon name="cilChartPie" height="22" />
                  <span className="text-bt">Report</span>
                </li>
              </ul>
            </span>
          </div>
        </div>

        <hr />

        <CRow>
          <CCol md="12" sm="12" xs="12">
            <CForm action="" method="post">
              <CRow>
                <CCol md="4" sm="12" xs="12">
                  <CFormGroup row>
                    <CCol md="4">
                      <CLabel htmlFor="vessel">Vessel</CLabel>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInputGroup>
                        <CInput
                          type="text"
                          name="input-small"
                          className="input-sm"
                          id="vessel"
                          placeholder="GOLDEN ROSE"
                        />
                        <CInputGroupAppend>
                          <CInputGroupText>D00289</CInputGroupText>
                        </CInputGroupAppend>
                      </CInputGroup>
                    </CCol>
                  </CFormGroup>
                </CCol>

                <CCol md="4" sm="12" xs="12">
                  <CFormGroup row>
                    <CCol md="4">
                      <CLabel htmlFor="voy_no">Voy No.</CLabel>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput
                        type="text"
                        name="input-small"
                        className="input-sm"
                        id="voy_no"
                        placeholder="1"
                      />
                    </CCol>
                  </CFormGroup>
                </CCol>

                <CCol md="4" sm="12" xs="12">
                  <CFormGroup row>
                    <CCol md="4">
                      <CLabel htmlFor="commencing">Commencing</CLabel>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput
                        type="text"
                        id="commencing"
                        name="commencing"
                        disabled
                        autoComplete=""
                        placeholder="05/04/19 21:06"
                      />
                    </CCol>
                  </CFormGroup>
                </CCol>

                <CCol md="4" sm="12" xs="12">
                  <CFormGroup row>
                    <CCol md="4">
                      <CLabel htmlFor="completing">Completing</CLabel>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput
                        type="text"
                        id="completing"
                        name="completing"
                        autoComplete="completing"
                        disabled
                        placeholder="08/07/19 10:18"
                      />
                    </CCol>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CForm>
          </CCol>
        </CRow>

        <hr />

        <CRow>
          <CCol xs="12" sm="12" md="6" lg="12">
            <CCard>
              <CCardBody className="wrap-table delays-wrapper">
                <CDataTable
                  items={UI_CONFIG.delaysData}
                  columnHeaderSlot={this.firstColumns}
                  fields={UI_CONFIG.delaysfields}
                  itemsPerPage={5}
                  pagination
                  noItemsViewSlot={<div className="pt-5"></div>}
                />
                <CCollapse show={accordion === 0}>
                  <div className="wrap-table-form">
                    <CForm action="" method="post">
                      <CRow>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="activity">Activity</CLabel>
                            <CInput
                              type="text"
                              id="activity"
                              name="activity"
                              defaultValue=""
                              placeholder="Activity"
                              autoComplete="activity"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="reason">Reason</CLabel>
                            <CInput
                              type="text"
                              id="reason"
                              name="reason"
                              defaultValue=""
                              placeholder="Reason"
                              autoComplete="reason"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="from">From</CLabel>
                            <CInput
                              type="text"
                              id="from"
                              name="from"
                              defaultValue=""
                              placeholder="From"
                              autoComplete="from"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="zone">Zone</CLabel>
                            <CInput
                              type="text"
                              id="zone"
                              name="zone"
                              placeholder="Zone"
                              defaultValue=""
                              autoComplete="zone"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="to">To</CLabel>
                            <CInput
                              type="text"
                              id="to"
                              name="to"
                              placeholder="To"
                              defaultValue=""
                              autoComplete="to"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="zone">Zone</CLabel>
                            <CInput
                              type="text"
                              id="zone"
                              name="zone"
                              placeholder="Zone"
                              defaultValue=""
                              autoComplete="zone"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="hours">Hours</CLabel>
                            <CInput
                              type="text"
                              id="hours"
                              name="hours"
                              placeholder="Hours"
                              defaultValue=""
                              autoComplete="hours"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="ded_adj_tci">Ded Adj (TCI)</CLabel>
                            <CInput
                              type="text"
                              id="ded_adj_tci"
                              name="ded_adj_tci"
                              placeholder="Ded Adj (TCI)"
                              defaultValue=""
                              autoComplete="ded_adj_tci"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="miles">Miles</CLabel>
                            <CInput
                              type="text"
                              id="miles"
                              name="miles"
                              placeholder="Miles"
                              defaultValue=""
                              autoComplete="miles"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="hfo_qty">HFO Qty</CLabel>
                            <CInput
                              type="text"
                              id="hfo_qty"
                              name="hfo_qty"
                              placeholder="HFO Qty"
                              defaultValue=""
                              autoComplete="hfo_qty"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="hfo_rob">HFO ROB</CLabel>
                            <CInput
                              type="text"
                              id="hfo_rob"
                              name="hfo_rob"
                              placeholder="HFO ROB"
                              defaultValue=""
                              autoComplete="hfo_rob"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="mdo_qty">MDO Qty</CLabel>
                            <CInput
                              type="text"
                              id="mdo_qty"
                              name="mdo_qty"
                              placeholder="MDO Qty"
                              defaultValue=""
                              autoComplete="mdo_qty"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="mdo_rob">MDO ROB</CLabel>
                            <CInput
                              type="text"
                              id="mdo_rob"
                              name="mdo_rob"
                              placeholder="MDO ROB"
                              defaultValue=""
                              autoComplete="mdo_rob"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="mdo_qty">MDO Qty</CLabel>
                            <CInput
                              type="text"
                              id="mdo_qty"
                              name="mdo_qty"
                              placeholder="MDO Qty"
                              defaultValue=""
                              autoComplete="mdo_qty"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="mgo_rob">MGO ROB</CLabel>
                            <CInput
                              type="text"
                              id="mgo_rob"
                              name="mgo_rob"
                              placeholder="MGO ROB"
                              defaultValue=""
                              autoComplete="mgo_rob"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="tci">TCI%</CLabel>
                            <CInput
                              type="text"
                              id="tci"
                              name="tci"
                              placeholder="TCI"
                              defaultValue=""
                              autoComplete="tci"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="tci_daily_cost">TCI Daily Cost</CLabel>
                            <CInput
                              type="text"
                              id="tci_daily_cost"
                              name="tci_daily_cost"
                              placeholder="TCI Daily Cost"
                              defaultValue=""
                              autoComplete="tci_daily_cost"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="tci_l">TCI L</CLabel>
                            <CInput
                              type="text"
                              id="tci_l"
                              name="tci_l"
                              placeholder="TCI L"
                              defaultValue=""
                              autoComplete="tci_l"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="tci_lumpsum">TCI Lumpsum</CLabel>
                            <CInput
                              type="text"
                              id="tci_lumpsum"
                              name="tci_lumpsum"
                              placeholder="TCI Lumpsum"
                              defaultValue=""
                              autoComplete="tci_lumpsum"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="tci_stmt">TCI Stmt</CLabel>
                            <CInput
                              type="text"
                              id="tci_stmt"
                              name="tci_stmt"
                              placeholder="TCI Stmt"
                              defaultValue=""
                              autoComplete="tci_stmt"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="remarks">Remarks</CLabel>
                            <CInput
                              type="text"
                              id="remarks"
                              name="remarks"
                              placeholder="Remarks"
                              defaultValue=""
                              autoComplete="remarks"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="last_update">Last Update</CLabel>
                            <CInput
                              type="text"
                              id="last_update"
                              name="last_update"
                              placeholder="Last Update"
                              defaultValue=""
                              autoComplete="last_update"
                            />
                          </CFormGroup>
                        </CCol>

                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="last_updated_by">Last Updated By</CLabel>
                            <CInput
                              type="text"
                              id="last_updated_by"
                              name="last_updated_by"
                              placeholder="Last Updated By"
                              defaultValue=""
                              autoComplete="last_updated_by"
                            />
                          </CFormGroup>
                        </CCol>
                      </CRow>
                    </CForm>
                  </div>
                </CCollapse>
              </CCardBody>
              <CCardFooter className="table-card-footer">
                {accordion === 0 ? (
                  <div className="float-right">
                    <CButton size={'sm'} color={'success'} className="m-0 mr-1">
                      Save
                    </CButton>
                    <CButton
                      size={'sm'}
                      color={'danger'}
                      onClick={() => this.setAccordion(null)}
                      className="m-0 mr-1"
                    >
                      Cancel
                    </CButton>
                  </div>
                ) : (
                  <CButton
                    size={'sm'}
                    onClick={() => this.setAccordion(0)}
                    color={'primary'}
                    className="m-0 mr-1"
                  >
                    Add New
                  </CButton>
                )}
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    );
  }
}
export default Delays;
