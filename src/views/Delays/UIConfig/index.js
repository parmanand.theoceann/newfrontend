const UI_CONFIG = {
    "delaysfields": [
        { key: 'activity', field: 'Activity', _style: { width: '2%' } },
        { key: 'reason', field: 'Reason', _style: { width: '2%' } },
        { key: 'from', field: 'From', _style: { width: '2%' } },
        { key: 'zone', field: 'Zone', _style: { width: '5%' } },
        { key: 'to', field: 'To', _style: { width: '2%' } },
        { key: 'zone', field: 'Zone', _style: { width: '2%' } },
        { key: 'hours', field: 'Hours', _style: { width: '2%' } },
        { key: 'ded_adj_tci', field: 'Ded Adj (TCI)', _style: { width: '5%' } },
        { key: 'miles', field: 'Miles', _style: { width: '2%' } },
        { key: 'hfo_qty', field: 'HFO Qty', _style: { width: '5%' } },
        { key: 'hfo_rob', field: 'HFO ROB', _style: { width: '5%' } },
        { key: 'mdo_qty', field: 'MDO Qty', _style: { width: '5%' } },
        { key: 'mdo_rob', field: 'MDO ROB', _style: { width: '5%' } },
        { key: 'mgo_qty', field: 'MGO Qty', _style: { width: '5%' } },
        { key: 'mgo_rob', field: 'MGO ROB', _style: { width: '5%' } },
        { key: 'tci', field: 'TCI%', _style: { width: '2%' } },
        { key: 'tci_daily_cost', field: 'TCI Daily Cost', _style: { width: '8%' } },
        { key: 'tci_l', field: 'TCI L', _style: { width: '3%' } },
        { key: 'tci_lumpsum', field: 'TCI Lumpsum', _style: { width: '8%' } },
        { key: 'tci_stmt', field: 'TCI Stmt', _style: { width: '5%' } },
        { key: 'remarks', field: 'Remarks', _style: { width: '5%' } },
        { key: 'last_update', field: 'Last Update', _style: { width: '5%' } },
        { key: 'last_updated_by', field: 'Last Updated By', _style: { width: '10%' } },
    ],

    "delaysData": [ ],
}

export default UI_CONFIG