import React, { Component } from "react";
import {
  CContainer,
  CForm,
  CFormGroup,
  CLabel,
  CCol,
  CRow,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CButton,
} from "@coreui/react";
import URL_WITH_VERSION, {
  URL_WITHOUT_VERSION,
  getAPICall,
  postAPICall,
  CALL_MASTER_API,
} from "../shared";
import VasselConsumption from "./vassel-consumption/VasselConsumption";
import ModalAlertBox from "../shared/ModalAlertBox";
import { VESSEL_TYPE } from "../shared/constants";

class VasselScheduleTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseData: { frm: [], tabs: [], active_tab: {} },
      formData: {},
      postData: {},
      modalHeader: null,
      modalBody: null,
      modalColor: null,
      modalStatus: false,
      ddlData: [],
    };
  }

  componentDidMount = async () => {
    const response = await getAPICall(
      `${URL_WITHOUT_VERSION}get/${this.props.frmName}`
    );
    const data = await response["data"];
    let postData = {};

    if (data && data.frm && data.frm.length > 0) {
      data.frm.map((i) => {
        postData[i.f_name] = i.f_default;
      });
    }

    this.callForDdlBinding(VESSEL_TYPE);

    this.setState({ ...this.state, responseData: data, postData });
  };

  callForDdlBinding = async (type) => {
    await CALL_MASTER_API("get", type, null, null, (data) => {
      let ddlData = [];
      if (data && data.total_rows > 0) {
        let typeCode = [];
        data.data.map((i) => {
          typeCode.push({ label: i.vessel_type, value: i.id });
        });
        ddlData["type_code"] = typeCode;
      }
      let currentYear = new Date().getFullYear();
      let years = [];
      for (let i = currentYear; i > currentYear - 30; i--) {
        years.push({ label: i, value: i });
      }
      ddlData["year_built"] = years;

      this.setState({ ...this.state, ddlData, modalHeader: "fsdfsdfds" });
    });
  };

  onUpdateField = (evt, field) => {
    let { postData } = this.state;
    postData[field] = evt.target.value;
    this.setState({ ...this.state, postData });
  };

  onSaveVesselSchedule = (e) => {
    e.preventDefault();
    const { postData } = this.state;
    alert(this.props.frmName);
    postAPICall(
      `${URL_WITH_VERSION}/vessel/save?frm=${this.props.frmName}`,
      postData,
      (data) => {
        if (data.data) {
          this.setState({
            ...this.state,
            modalHeader: "Success",
            modalBody: data.message,
            modalColor: "success",
            modalStatus: true,
          });
        } else {
          let dataMessage = data.message;
          let msg = "<div class='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg += "<div class='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else msg += "<div class='row col-sm-12'" > +dataMessage + "</div>";

          msg += "</div>";

          this.setState({
            ...this.state,
            modalHeader: "Error",
            modalBody: msg,
            modalColor: "danger",
            modalStatus: true,
          });
        }
      }
    );
  };

  closeOpenModal = (status) => {
    this.setState({
      ...this.state,
      modalHeader: null,
      modalBody: null,
      modalFooter: null,
      modalStatus: status,
      modalColor: null,
    });
  };

  render() {
    const {
      responseData,
      postData,
      modalHeader,
      modalBody,
      modalColor,
      modalStatus,
      ddlData,
    } = this.state;

    const ModalBodyContent = () => {
      return modalStatus ? (
        <div dangerouslySetInnerHTML={{ __html: modalBody }} />
      ) : null;
    };

    return (
      <CContainer fluid>
        <CForm action="" method="post">
          <CRow>
            <CCol sm="12">
              <CFormGroup row className="float-right">
                <CButton
                  size={"sm"}
                  color={"success"}
                  className="m-0 mr-1"
                  onClick={(evt) => this.onSaveVesselSchedule(evt)}
                >
                  Save
                </CButton>
                <CButton size={"sm"} color={"danger"} className="m-0 mr-1">
                  Reset
                </CButton>
              </CFormGroup>
            </CCol>
          </CRow>
          <CRow>
            {responseData.frm.map((e) => {
              return (
                <CCol sm="4" key={e.f_name}>
                  <CFormGroup row>
                    <CCol md="4">
                      <CLabel htmlFor="vs-name">{e.name}</CLabel>
                    </CCol>
                    <CCol xs="12" md="8">
                      {e.f_type === "dropdown" ? (
                        <select
                          className="form-control"
                          value={
                            postData && postData[e.f_type]
                              ? postData[e.f_type]
                              : null
                          }
                          onChange={(evt) => this.onUpdateField(evt, e.f_name)}
                        >
                          <option value="null">Select</option>
                          {ddlData[e.f_name]
                            ? ddlData[e.f_name].map((v, i) => (
                                <option key={i} value={v.value}>
                                  {v.label}
                                </option>
                              ))
                            : null}
                        </select>
                      ) : (
                        <input
                          type={e.f_type}
                          className="form-control"
                          id="vs-name"
                          name="vs-name"
                          value={
                            postData && postData[e.f_type]
                              ? postData[e.f_type]
                              : null
                          }
                          placeholder={"Enter " + e.name}
                          // autoComplete="true"
                          onChange={(evt) => this.onUpdateField(evt, e.f_name)}
                        />
                      )}
                    </CCol>
                  </CFormGroup>
                </CCol>
              );
            })}
          </CRow>
          <hr />
          <div>
            <CTabs activeTab={responseData.active_tab.frm_code}>
              <CNav variant="tabs">
                {responseData.tabs.map((e) => {
                  return (
                    <CNavItem key={e.frm_code}>
                      <CNavLink data-tab={e.frm_code}>{e.tab_name}</CNavLink>
                    </CNavItem>
                  );
                })}
              </CNav>
              <CTabContent className="mb-20">
                {responseData.tabs.map((e) => {
                  return (
                    <CTabPane data-tab={e.frm_code} key={e.frm_code + "1"}>
                      <VasselConsumption />
                    </CTabPane>
                  );
                })}
              </CTabContent>
            </CTabs>

            {/* <CTabs activeTab="consumption">
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink data-tab="consumption">
                    Consumption
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="routes">
                    Routes
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="dwt-draft">
                    DWT/Draft
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="details">
                    Details
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="stowage">
                    Stowage
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="contacts">
                    Contacts
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="ld-perf">
                    L/D Perf
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="bunker-tanks">
                    Bunker Tanks
                </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="tce-target">
                    TCE Target
                  </CNavLink>
                </CNavItem>
              </CNav>
              <CTabContent className="mb-20">
                <CTabPane data-tab="consumption">
                  <VasselConsumption />
                </CTabPane>
                <CTabPane data-tab="routes">
                  456
              </CTabPane>
                <CTabPane data-tab="dwt-draft">
                  789
              </CTabPane>
                <CTabPane data-tab="details">
                  <VasselDetails />
                </CTabPane>
                <CTabPane data-tab="stowage">
                  789
              </CTabPane>
              </CTabContent>
            </CTabs> */}
          </div>
        </CForm>
        {modalStatus ? (
          <ModalAlertBox
            modalHeader={modalHeader}
            modalStatus={modalStatus}
            modalBody={ModalBodyContent}
            modalColor={modalColor}
            closeOpenModal={(evt) => this.closeOpenModal(evt)}
          />
        ) : null}
      </CContainer>
    );
  }
}

export default VasselScheduleTemplate;
