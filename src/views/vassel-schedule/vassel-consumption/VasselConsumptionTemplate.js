import React, { useState } from 'react';
import { CCollapse, CCard, CCardHeader, CCardBody, CDataTable, CCardFooter, CButton, CRow, CForm, CCol, CFormGroup, CLabel, CInput, CSelect, CLink } from '@coreui/react';
import consumptionData from './ConsumptionData'

export default function VasselConsumptionTemplate() {

  const [collapse, setCollapse] = useState(false);
  const [collapse2, setCollapse2] = useState(false);
  const fieldsPort = ['type', 'unit', 'g', 'capacity', 'margin', 'loading', 'disch', 'idle_on', 'heat', 'heat_plus', 'heat_plus_two', 'igs', 'clean', 'menu_uv', 'au']
  const fieldsSpeed = ['speed', 'bl', 'engine_load', 'pass_type']
  const toggle = (e) => {
    setCollapse(!collapse);
    setCollapse2(false);
    e.preventDefault();
  }

  const toggle2 = (e) => {
    setCollapse2(!collapse2);
    setCollapse(false);
    e.preventDefault();
  }

  return (
    <div>
      <CCard>
        <CCardHeader>
          In Port Consumption Table (Per Day)
        </CCardHeader>
        <CCardBody className="wrap-table">
          <CDataTable
            items={consumptionData.fieldsPort}
            fields={fieldsPort}
            itemsPerPage={5}
            pagination
            noItemsViewSlot={
              <div className="pt-5"></div>
            }
          />
          <CCollapse show={collapse}>
            <div className="wrap-table-form">
              <CRow>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="type">Type</CLabel>
                    <CSelect custom name="type" id="type">
                      <option value="Fuel type">Fuel type</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="unit">Unit</CLabel>
                    <CSelect custom name="unit" id="unit">
                      <option value="MT">MT</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="g">G</CLabel>
                    <CSelect custom name="g" id="g">
                      <option value="IFO CST 380">IFO CST 380</option>
                      <option value="IFO CST 180">IFO CST 180</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="capacity">Capacity</CLabel>
                    <CInput
                      type="number"
                      id="capacity"
                      name="capacity"
                      placeholder="Enter Capacity"
                      autoComplete="capacity"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="margin">Margin</CLabel>
                    <CInput
                      type="number"
                      id="margin"
                      name="margin"
                      placeholder="Enter Margin"
                      autoComplete="margin"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="loading">Loading</CLabel>
                    <CInput
                      type="number"
                      id="loading"
                      name="loading"
                      placeholder="Enter Loading"
                      autoComplete="loading"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="disch">Disch</CLabel>
                    <CInput
                      type="number"
                      id="disch"
                      name="disch"
                      placeholder="Enter Disch"
                      autoComplete="disch"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="idleOn">Idle On</CLabel>
                    <CInput
                      type="number"
                      id="idleOn"
                      name="idleOn"
                      placeholder="Enter Idle On"
                      autoComplete="idleOn"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="heat">Heat</CLabel>
                    <CInput
                      type="number"
                      id="heat"
                      name="heat"
                      placeholder="Enter Heat"
                      autoComplete="heat"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="heat-plus">Heat +</CLabel>
                    <CInput
                      type="number"
                      id="heat-plus"
                      name="heat-plus"
                      placeholder="Enter Heat +"
                      autoComplete="heat-plus"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="heat-plus-two">Heat 2+</CLabel>
                    <CInput
                      type="number"
                      id="heat-plus-two"
                      name="heat-plus-two"
                      placeholder="Enter Heat 2+"
                      autoComplete="heat-plus-two"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="igs">IGS</CLabel>
                    <CInput
                      type="number"
                      id="igs"
                      name="igs"
                      placeholder="Enter IGS"
                      autoComplete="igs"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="clean">Clean</CLabel>
                    <CInput
                      type="number"
                      id="clean"
                      name="clean"
                      placeholder="Enter Clean"
                      autoComplete="clean"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="menu-uv">Menu UV</CLabel>
                    <CInput
                      type="number"
                      id="menu-uv"
                      name="menu-uv"
                      placeholder="Enter Menu UV"
                      autoComplete="menu-uv"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="2">
                  <CFormGroup>
                    <CLabel htmlFor="au">Au</CLabel>
                    <CInput
                      type="number"
                      id="au"
                      name="au"
                      placeholder="Enter Au"
                      autoComplete="au"
                    />
                  </CFormGroup>
                </CCol>
              </CRow>
            </div>
          </CCollapse>
        </CCardBody>
        {/* <CCardFooter className="table-card-footer">
          <CLink onClick={toggle}>
            {!collapse ? 'Add New' : 'Save'}
          </CLink>
        </CCardFooter> */}
        <CCardFooter className="table-card-footer float-right">
          {
            collapse ? (
              <div className="float-right">
                <CButton
                  size={"sm"}
                  color={"success"}
                  className="m-0 mr-1"
                >
                  Save
                </CButton>
                <CButton
                  size={"sm"}
                  color={"danger"}
                  onClick={toggle}
                  className="m-0 mr-1"
                >
                  Cancel
                </CButton>
              </div>
            ) : (
              <CButton
                size={"sm"}
                onClick={toggle}
                color={"primary"}
                className="m-0 mr-1"
              >
                Add New
              </CButton>
            )
          }
        </CCardFooter>
      </CCard>
      <CCard>
        <CCardHeader>
          Speed Consumption Table (Per Day)
        </CCardHeader>
        <CCardBody className="wrap-table">
          <CDataTable
            items={consumptionData}
            fields={fieldsSpeed}
            itemsPerPage={5}
            pagination
            noItemsViewSlot={
              <div className="pt-5"></div>
            }
          />
          <CCollapse show={collapse}>
            <div className="wrap-table-form">
              <CRow>
                <CCol sm="4">
                  <CFormGroup>
                    <CLabel htmlFor="speed">Speed</CLabel>
                    <CInput
                      type="number"
                      id="speed"
                      name="speed"
                      placeholder="Enter Speed"
                      autoComplete="speed"
                    />
                  </CFormGroup>
                </CCol>
                <CCol sm="4">
                  <CFormGroup>
                    <CLabel htmlFor="speed">B/L</CLabel>
                    <CSelect custom name="bl" id="bl">
                      <option value="ballast">Ballast</option>
                      <option value="Laden">Laden</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
                <CCol sm="4">
                  <CFormGroup>
                    <CLabel htmlFor="speed_type">Speed Type</CLabel>
                    <CSelect custom name="speed_type" id="speed_type">
                      <option value="ECO">ECO</option>
                      <option value="C/P">C/P</option>
                      <option value="FULL">FULL</option>
                      <option value="General">General</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
              </CRow>
            </div>
          </CCollapse>
        </CCardBody>
        {/* <CCardFooter className="table-card-footer">
          <CLink onClick={toggle}>
            {!collapse ? 'Add New' : 'Save'}
          </CLink>
        </CCardFooter> */}

        <CCardFooter className="table-card-footer float-right">
          {
            collapse2 ? (
              <div className="float-right">
                <CButton
                  size={"sm"}
                  color={"success"}
                  className="m-0 mr-1"
                >
                  Save
                </CButton>
                <CButton
                  size={"sm"}
                  color={"danger"}
                  onClick={toggle2}
                  className="m-0 mr-1"
                >
                  Cancel
                </CButton>
              </div>
            ) : (
              <CButton
                size={"sm"}
                onClick={toggle2}
                color={"primary"}
                className="m-0 mr-1"
              >
                Add New
              </CButton>
            )
          }
        </CCardFooter>

      </CCard>
    </div>
  );
}
