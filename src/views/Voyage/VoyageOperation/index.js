import {
  CCol,
  CLabel,
  CContainer,
  CInputGroupAppend,
  CInputGroupText,
  CInputCheckbox,
  CForm,
  CRow,
  CFormGroup,
  CInput,
  CSelect,
  CTabs,
  CNav,
  CInputGroup,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
  CDataTable,
  CCollapse,
  CCardFooter,
  CButton,
  CTextarea,
  CCardHeader,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { Component } from 'react';
import Consumption from './Consumption';

import ModalAlertBox from '../../shared/ModalAlertBox';
import Delays from '../../Delays';

// Second Table Fields
const secondField = [
  { key: 'no', field: 'N', _style: { width: '1%' } },
  { key: 'cargo', field: 'Cargo', _style: { width: '7%' } },
  { key: 'qty', field: 'C/P Qty', _style: { width: '7%' } },
  { key: 'unit', field: 'Unit', _style: { width: '7%' } },
  { key: 'opt', field: 'Opt %', _style: { width: '7%' } },
  { key: 't', field: 'T', _style: { width: '7%' } },
  { key: 'frt', field: 'Frt Rate', _style: { width: '7%' } },
  { key: 'lumpsum', field: 'Lumpsum', _style: { width: '7%' } },
  { key: 'charterer', field: 'Charterer', _style: { width: '7%' } },
  { key: 'curr', field: 'Curr', _style: { width: '7%' } },
  { key: 'exch', field: 'Exch Rate', _style: { width: '7%' } },
  { key: 'obl', field: 'OBL', _style: { width: '7%' } },
  { key: 'cc', field: 'CC', _style: { width: '7%' } },
];
let secondColums = {}
secondField.map(item => secondColums[item.key] = item.field);

const secondData = [
  { 'no': '1', 'cargo': '', 'qty': '2,000.000', 'unit': 'L', 'opt': '1', 't': 'N', 'frt': '', 'lumpsum': '', 'charterer': '', 'curr': '', 'exch': '', 'obl': '', 'cc': '' },
  { 'no': '2', 'cargo': '', 'qty': '0.000', 'unit': '%', 'opt': '1', 't': 'Y', 'frt': '', 'lumpsum': '', 'charterer': '', 'curr': '', 'exch': '', 'obl': '', 'cc': '' },
]
// End Second Table Fields

// Third Table Fields
const thirdField = [
  { key: 'status', field: 'Status', _style: { width: '10%' } },
  { key: 'subject', field: 'Subject', _style: { width: '10%' } },
  { key: 'start_date', field: 'Start Date', _style: { width: '10%' } },
  { key: 'due_date', field: 'Due Date', _style: { width: '10%' } },
  { key: 'port', field: 'Port', _style: { width: '10%' } },
  { key: 'seq', field: 'Seq', _style: { width: '10%' } },
];
let thirdColums = {}
thirdField.map(item => thirdColums[item.key] = item.field);

const thirdData = [
  { 'status': 'code', 'subject': 'Extra Freight Term', 'port': 'Rate/Lump', 'start_date': '12-1-2020', 'due_date': '12-1-2020', 'seq': '12222' },
  { 'status': 'code', 'subject': 'Extra Freight Term', 'port': 'Rate/Lump', 'start_date': '12-1-2020', 'due_date': '12-1-2020', 'seq': '12222' },
]
// End Third Table Fields


class VoyageOperation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      accordion: null,
      modalStatus: false
    }
  }

  setCollapse = (value) => {
    this.setState({
      ...this.state,
      collapse: value
    })
  }

  setAccordion = (value) => {
    this.setState({
      ...this.state,
      accordion: value
    })
  }

  modalStatusChange = () => {
    this.setState({
      ...this.state,
      modalStatus: !this.state.modalStatus
    })
  }

  closeOpenModal = (status) => {
    this.setState({
      ...this.state,
      modalHeader: null,
      modalBody: null,
      modalFooter: null,
      modalStatus: status,
      modalColor: null
    })
  }

  render() {
    const { accordion, modalStatus } = this.state;

    const modalBodyWrapper = () => {
      return (
        <Delays />
      )
    }

    const modalFooterDeleteBtn = () => {
      let { modalStatus } = this.state;
      return (
        <>
          <CButton
            color="primary"
          // onClick={(evt) => this.onRowDeletedClick(evt)}
          >Ok</CButton>
          <CButton
            color="secondary"
            onClick={() => this.modalStatusChange(!modalStatus)}
          >Cancel</CButton>
        </>
      )
    }

    return (
      <CContainer fluid>
        <div className="table-filter-wrapper mb-20">
          <div className="section">
            <span className="wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li><CIcon name="cilPlus" height="22" /></li>
                <li><CIcon name="cilSearch" height="22" /></li>
                <li><CIcon name="cilSave" height="22" /><span className="text-bt">Save</span></li>
              </ul>
            </span>
            <span className="mlr-10 wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li><CIcon name="cilFile" height="22" /> <div className="show-arrow"></div></li>
                <li><CIcon name="cilTrash" height="22" /></li>
              </ul>
            </span>
            <span className="mlr-10 wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li><CIcon name="cilArrowThickLeft" height="22" /></li>
                <li><CIcon name="cilArrowThickRight" height="22" /></li>
              </ul>
            </span>
          </div>
          <div className="section">
            <span className="wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li><span className="text-bt">Estimate</span></li>
                <li><span className="text-bt">Freight <div className="show-arrow"></div></span></li>
                <li><span className="text-bt">Commission <div className="show-arrow"></div></span></li>
                <li><span className="text-bt">Other Rev/Exp <div className="show-arrow"></div></span></li>
                <li><span className="text-bt">Laytime <div className="show-arrow"></div></span></li>
                <li><span className="text-bt" onClick={() => this.modalStatusChange()}>Delays <div className="show-arrow"></div></span></li>
                <li><span className="text-bt">Bunkers <div className="show-arrow"></div></span></li>
                <li><span className="text-bt">Deviation <div className="show-arrow"></div></span></li>
              </ul>
            </span>
            <span className="wrap-bar-menu more-icon">
              <CIcon name="cilOptions" height="22" />
            </span>
          </div>
        </div>

        <hr />

        <CForm action="" method="post">
          <CRow>
            <CCol sm="4">
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-name">Vesssel</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <CInput
                    type="name"
                    id="vs-name"
                    name="vs-name"
                    placeholder="Enter Vesssel"
                    autoComplete="name"
                  />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-code">TC Code/Hire</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <CInputGroup>
                    <CInputGroupAppend>
                      <CInputGroupText>
                        D00289-I0001 </CInputGroupText>
                    </CInputGroupAppend>
                    <CInput type="text" id="cp_qty_unit" name="cp_qty_unit" autoComplete="cp_qty_unit" placeholder="10,000" />
                  </CInputGroup>
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-ownerShip">Fixture No.</CLabel></CCol>
                <CCol xs="12" md="7"> <CInput type="text" id="fixture" name="fixture" disabled autoComplete="fixture" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="">&nbsp;</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <div className="form-check checkbox">
                    <CInputCheckbox id="Consecutive" name="Consecutive" value="Consecutive" />
                    <CLabel variant="checkbox" className="form-check-label" htmlFor="Consecutive" >Consecutive Voyage</CLabel>
                  </div>
                  <div className="form-check checkbox">
                    <CInputCheckbox id="LS" name="LS" value="LS" />
                    <CLabel variant="checkbox" className="form-check-label" htmlFor="LS" >LS Only</CLabel>
                  </div>
                  <div className="form-check checkbox">
                    <CInputCheckbox id="DryDock" name="DryDock" value="DryDock" />
                    <CLabel variant="checkbox" className="form-check-label" htmlFor="DryDock" >DryDock</CLabel>
                  </div>
                  <div className="form-check checkbox">
                    <CInputCheckbox id="TCI" name="TCI" value="TCI" />
                    <CLabel variant="checkbox" className="form-check-label" htmlFor="TCI" >Last TCI Voy</CLabel>
                  </div>
                </CCol>
              </CFormGroup>
            </CCol>


            <CCol sm="4">
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="laytime_allowed">VsI Code/Voy No.</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <CInputGroup>
                    <CInput type="text" id="laytime_allowed" name="laytime_allowed" placeholder="D00289" />
                    <CInput type="text" id="laytime_allowed" name="laytime_allowed" placeholder="1" />
                  </CInputGroup>
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">Opr Type</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="Opr_type" name="Opr_type" placeholder="TCOV" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">Trade Area</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="trade_area" name="trade_area" placeholder="EAST COST SOUTH" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">Chtr Specialist</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="Chtr" name="Chtr" placeholder="gunaca" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">Opts Coordinator</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="coordinator" name="coordinator" placeholder="Pushp" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">User Group</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="user_group" name="user_group" placeholder="User Group" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">Controller</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="user_group" name="user_group" placeholder="Controller" />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-year-built">FD Manager</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CInput type="text" id="user_group" name="user_group" placeholder="FD Manager" />
                </CCol>
              </CFormGroup>
            </CCol>

            <CCol sm="4">
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-vessel-dwt">Voyage Status</CLabel></CCol>
                <CCol xs="12" md="7">
                  <CSelect custom name="voyage_status" id="voyage_status">
                    <option value="completed">Completed</option>
                  </CSelect>
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-Commencing">Voyage Commencing</CLabel></CCol>
                <CCol xs="12" md="7"> <CInput
                  type="date"
                  id="vs-Commencing"
                  name="vs-Commencing"
                  placeholder="05/04/19 21:06"
                  autoComplete="Commencing"
                />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-Completing">Voyage Completing</CLabel></CCol>
                <CCol xs="12" md="7"> <CInput
                  type="date"
                  id="vs-Completing"
                  name="vs-Completing"
                  placeholder="05/04/19 21:06"
                  autoComplete="Completing"
                  disabled
                />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="vs-GMT">Last Update GMT</CLabel></CCol>
                <CCol xs="12" md="7"> <CInput
                  type="date"
                  id="vs-GMT"
                  name="vs-GMT"
                  placeholder="05/04/19 21:06"
                  autoComplete="GMT"
                  disabled
                />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="Piracy">Piracy Routing</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <CInputGroup>
                    <CInput type="text" id="Piracy" autoComplete="Piracy" disabled name="Piracy" placeholder="Default" />
                    <CInput type="text" id="Piracy" name="Piracy" placeholder="" />
                  </CInputGroup>
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="Piracy">ECA Routing</CLabel>
                </CCol>
                <CCol xs="12" md="7">
                  <CInputGroup>
                    <CInput type="text" id="Piracy" placeholder="Default" />
                    <CInput type="text" id="Piracy" name="Piracy" placeholder="" />
                  </CInputGroup>
                </CCol>
              </CFormGroup>

              <CFormGroup row>
                <CCol xs="12" md="5">
                  <CLabel htmlFor="dwe">DWF %</CLabel></CCol>
                <CCol xs="12" md="7"><CInput
                  type="number"
                  id="dwe"
                  name="dwe"
                  placeholder="10.00"
                  autoComplete="dwf"
                />
                </CCol>
              </CFormGroup>
            </CCol>
          </CRow>
        </CForm>

        <hr />

        <h3>Itinerary</h3>
        <CTabs activeTab="consumption">
          <CNav variant="tabs">
            <CNavItem>
              <CNavLink data-tab="BL Info">
                BL Info
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="Corgo">
                Corgo
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="Transform">
                Transform
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="Draft/Restrictions">
                Draft/Restrictions
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="Bunkers">
                Bunkers
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="Port/Date">
                Port/Date
              </CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="consumption">
                Consumption
              </CNavLink>
            </CNavItem>
          </CNav>
          <CTabContent className="mb-20">
            <CTabPane data-tab="consumption">
              <Consumption />
            </CTabPane>
          </CTabContent>
        </CTabs>

        <hr />

        <h3>Cargoes</h3>

        <CRow>
          <CCol xs="12" sm="12" md="12" lg="12">
            <CCard>
              <CCardBody className="wrap-table">
                <CDataTable
                  items={secondData}
                  columnHeaderSlot={secondColums}
                  fields={secondField}
                  itemsPerPage={5}
                  pagination
                  noItemsViewSlot={
                    <div className="pt-5"></div>
                  }
                />
                <CCollapse show={accordion === 1}>
                  <div className="wrap-table-form">
                    <CForm action="" method="post">
                      <CRow>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="broker">Broker</CLabel>
                            <CInput
                              type="text"
                              id="broker"
                              name="broker"
                              defaultValue=""
                              placeholder="Broker"
                              autoComplete="broker"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="cargo">Cargo</CLabel>
                            <CInput
                              type="text"
                              id="cargo"
                              name="cargo"
                              defaultValue=""
                              placeholder="Cargo"
                              autoComplete="cargo"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="qty">C/P Qty</CLabel>
                            <CInput
                              type="text"
                              id="qty"
                              name="qty"
                              placeholder="C/P Qty"
                              autoComplete="qty"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="unit">Unit</CLabel>
                            <CInput
                              type="text"
                              id="unit"
                              name="unit"
                              placeholder="unit"
                              defaultValue=""
                              autoComplete="unit"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="opt">Opt %</CLabel>
                            <CInput
                              type="text"
                              id="opt"
                              name="opt"
                              placeholder="Opt %"
                              defaultValue=""
                              autoComplete="opt"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="t">T</CLabel>
                            <CInput
                              type="text"
                              id="t"
                              name="t"
                              placeholder="T"
                              defaultValue=""
                              autoComplete="t"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="frt">Frt Rate</CLabel>
                            <CInput
                              type="text"
                              id="frt"
                              name="frt"
                              placeholder="Frt Rate"
                              defaultValue=""
                              autoComplete="frt"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="lumpsum">Lumpsum</CLabel>
                            <CInput
                              type="text"
                              id="lumpsum"
                              name="lumpsum"
                              placeholder="Lumpsum"
                              defaultValue=""
                              autoComplete="lumpsum"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="charterer">Charterer</CLabel>
                            <CInput
                              type="text"
                              id="charterer"
                              name="charterer"
                              placeholder="Charterer"
                              defaultValue=""
                              autoComplete="charterer"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="curr">Curr</CLabel>
                            <CInput
                              type="text"
                              id="curr"
                              name="curr"
                              placeholder="Curr"
                              defaultValue=""
                              autoComplete="curr"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="exch">Exch Rate</CLabel>
                            <CInput
                              type="text"
                              id="exch"
                              name="exch"
                              placeholder="Exch Rate"
                              defaultValue=""
                              autoComplete="exch"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="obl">OBL</CLabel>
                            <CInput
                              type="text"
                              id="obl"
                              name="obl"
                              placeholder="OBL"
                              defaultValue=""
                              autoComplete="obl"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CLabel htmlFor="cc">CC</CLabel>
                            <CInput
                              type="text"
                              id="cc"
                              name="cc"
                              placeholder="CC"
                              defaultValue=""
                              autoComplete="cc"
                            />
                          </CFormGroup>
                        </CCol>
                      </CRow>
                    </CForm>
                  </div>
                </CCollapse>
              </CCardBody>
              <CCardFooter className="table-card-footer">
                {
                  accordion === 1 ? (
                    <div className="float-right">
                      <CButton
                        size={"sm"}
                        color={"success"}
                        className="m-0 mr-1"
                      >
                        Save
                      </CButton>
                      <CButton
                        size={"sm"}
                        color={"danger"}
                        onClick={() => this.setAccordion(null)}
                        className="m-0 mr-1"
                      >
                        Cancel
                      </CButton>
                    </div>
                  ) : (
                    <CButton
                      size={"sm"}
                      onClick={() => this.setAccordion(1)}
                      color={"primary"}
                      className="m-0 mr-1"
                    >
                      Add New
                    </CButton>
                  )
                }
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>

        <CRow>
          <CCol xs="12" sm="12" md="6" lg="6">
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol lg="4" md="4" sm="12" xs="12">
                    Tasks
                  </CCol>
                  <CCol lg="8" md="8" sm="12" xs="12">
                    <CRow>
                      <CCol lg="6" md="6" sm="6" xs="6">
                        <div className="form-check checkbox">
                          <CInputCheckbox id="s_date" name="s_date" value="s_date" />
                          <CLabel variant="checkbox" className="form-check-label" htmlFor="s_date" >Hide Unit Start Date</CLabel>
                        </div>
                      </CCol>
                      <CCol lg="6" md="6" sm="6" xs="6">
                        <div className="form-check checkbox">
                          <CInputCheckbox id="completed" name="completed" value="completed" />
                          <CLabel variant="checkbox" className="form-check-label" htmlFor="completed" >Show Completed</CLabel>
                        </div>
                      </CCol>
                    </CRow>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody className="wrap-table">
                <CDataTable
                  items={thirdData}
                  columnHeaderSlot={thirdColums}
                  fields={thirdField}
                  itemsPerPage={5}
                  pagination
                  noItemsViewSlot={
                    <div className="pt-5"></div>
                  }
                />
                <CCollapse show={accordion === 2}>
                  <div className="wrap-table-form">
                    <CForm action="" method="post">
                      <CRow>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="status"
                              name="status"
                              defaultValue=""
                              placeholder="Status"
                              autoComplete="status"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="subject"
                              name="subject"
                              defaultValue=""
                              placeholder="Subject"
                              autoComplete="subject"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="start_date"
                              name="start_date"
                              defaultValue=""
                              placeholder="Start Date"
                              autoComplete="start_date"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="due_date"
                              name="due_date"
                              defaultValue=""
                              placeholder="Due Date"
                              autoComplete="due_date"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="port"
                              name="port"
                              defaultValue=""
                              placeholder="Port"
                              autoComplete="port"
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol lg="2" md="4" sm="6" xs="12">
                          <CFormGroup>
                            <CInput
                              type="text"
                              id="seq"
                              name="seq"
                              defaultValue=""
                              placeholder="Seq"
                              autoComplete="seq"
                            />
                          </CFormGroup>
                        </CCol>
                      </CRow>
                    </CForm>
                  </div>
                </CCollapse>
              </CCardBody>
              <CCardFooter className="table-card-footer">
                {
                  accordion === 2 ? (
                    <div className="float-right">
                      <CButton
                        size={"sm"}
                        color={"success"}
                        className="m-0 mr-1"
                      >
                        Save
                      </CButton>
                      <CButton
                        size={"sm"}
                        color={"danger"}
                        onClick={() => this.setAccordion(null)}
                        className="m-0 mr-1"
                      >
                        Cancel
                      </CButton>
                    </div>
                  ) : (
                    <CButton
                      size={"sm"}
                      onClick={() => this.setAccordion(2)}
                      color={"primary"}
                      className="m-0 mr-1"
                    >
                      Add New
                    </CButton>
                  )
                }
              </CCardFooter>
            </CCard>
          </CCol>
          <CCol xs="12" sm="12" md="6" lg="6">
            <CRow>
              <CCol lg="12" md="12" sm="12" xs="12">
                <CFormGroup>
                  <CLabel htmlFor="remark">Voyage Remark</CLabel>
                  <CTextarea
                    name="remark"
                    id="remark"
                    rows="4"
                    placeholder="Voyage Remark"
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol lg="12" md="12" sm="12" xs="12">
                <CFormGroup>
                  <CLabel htmlFor="notes">Notes To Operation</CLabel>
                  <CTextarea
                    name="notes"
                    id="notes"
                    rows="4"
                    placeholder="Notes To Operation" className="textareaHeight"
                  />
                </CFormGroup>
              </CCol>
            </CRow>
          </CCol>
        </CRow>

        <ModalAlertBox
          modalStatus={modalStatus}
          modalHeader="Delay"
          modalBody={modalBodyWrapper}
          closeOpenModal={this.closeOpenModal}
          modalSize="xl"
          modalColor="primary"
          modalFooter={modalFooterDeleteBtn}
          className="delays-modal-wrapper"
        ></ModalAlertBox>

      </CContainer>
    )
  }
}
export default VoyageOperation;
