import React from 'react';
import { Row, Col, Select, Input, Tooltip, Pagination, Button } from 'antd';

import TOOLBARCONFIGURATION from '../../constants/rightbar-configuration';
const Option = Select.Option;

class CommonToolbarUI extends React.Component {
  constructor() {
    super();
    this.state = {
      routeUrl: null
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props && props.routeUrl && !state.routeUrl) {
      return {
        routeUrl: props.routeUrl
      }
    }
    return null;
  }

  onClickButton = (val) => {
    if (this.props.callback)
      this.props.callback(val);
  }

  render() {
    const { routeUrl } = this.state;
    const toolbarConfigurationArray = TOOLBARCONFIGURATION && TOOLBARCONFIGURATION[routeUrl] ? TOOLBARCONFIGURATION[routeUrl] : null

    return (
      <>
        <Row gutter={16}>
          <Col xs={24} sm={8} md={8} lg={8} xl={8}>
            {
              toolbarConfigurationArray ? (
                toolbarConfigurationArray.isLeftSection.map((el, i) => {
                  if (toolbarConfigurationArray.isLeftSection.length > 0) {
                    return (
                      <Row gutter={16} key={i}>
                        {
                          (el.isDropdown) === 1 ?
                            <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                              <Select defaultValue={el.defaultDropdownValue} onChange={(ev) => this.onClickButton(ev)}>
                                {
                                  el.isDropdownOption.map((d, i) => {
                                    return <Option key={i} value={d.value}>{d.options}</Option>
                                  })
                                }
                              </Select>
                            </Col> : undefined
                        }
                        {
                          (el.isInput) === 1 ?
                            <Col xs={10} sm={10} md={10} lg={12} xl={12}>
                              <Tooltip title="Please enter a value and hit enter key to search.">
                                <Input placeholder="Please Enter" onPressEnter={(ev) => this.onClickButton(ev.target.value)} />
                              </Tooltip>
                            </Col> : undefined
                        }
                      </Row>
                    )
                  } else {
                    return (null);
                  }
                })
              ) : null
            }
          </Col>

          <Col xs={24} sm={8} md={8} lg={8} xl={8}>
            {
              toolbarConfigurationArray ? (
                toolbarConfigurationArray.isCenterSection.map((ce, i) => {
                  if (ce.isPagination === 1) {
                    return (
                      <Row key={i} type="flex" align="middle" justify="center">
                        <span><b>Total :</b> {ce.total}</span>
                        <Pagination simple defaultCurrent={ce.current}
                          total={ce.total} defaultPageSize={ce.pageLimit} onChange={(ev) => this.onClickButton(ev)} />
                        <Select defaultValue="10" style={{ width: 120 }} onChange={(ev) => this.onClickButton(ev)}>
                          <Option value="10">10 Per Page</Option>
                          <Option value="20">20 Per Page</Option>
                          <Option value="30">30 Per Page</Option>
                          <Option value="40">40 Per Page</Option>
                        </Select>
                      </Row>
                    )
                  } else {
                    return (null);
                  }
                })
              ) : null
            }
          </Col>

          <Col xs={24} sm={8} md={8} lg={8} xl={8}>
            <Row type="flex" justify="end">
              <div className="wrap-button">
                {
                  toolbarConfigurationArray ? (
                    toolbarConfigurationArray.isRightSection.map((el, i) => {
                      if (toolbarConfigurationArray.isRightSection.length > 0) {
                        return (
                          <span key={(i + 1).toString()}>
                            {el.buttonSet.map((set, j) => {
                              return <Tooltip key={j} title={set.tooltip}><Button size={set.size ? set.size : "default"} onClick={(ev) => this.onClickButton(set.key)}>{set.icon}
                                {set.text ? set.text : undefined}</Button></Tooltip>
                            })}
                          </span>
                        )
                      } else {
                        return (null);
                      }
                    })
                  ) : null
                }

              </div>
            </Row>
          </Col>
        </Row>
      </>
    )
  }
}

export default CommonToolbarUI;
