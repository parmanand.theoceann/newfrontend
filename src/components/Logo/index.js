import React from "react";
import { Link } from "react-router-dom";

const Logo = () => (
    
    <Link to={"/chartering-dashboard"}>
    <img
        src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
        width={150}
        alt="logo"
      
    ></img>
   </Link>
);

export default Logo;
