import React, { Component } from 'react';
import HoldsInformation from './holdsinformation';
import CraneInformation from './craneinformation';

class Stowage extends Component {
    render() {
        return (
            <>
                <HoldsInformation />

                <hr />

                <CraneInformation />
            </>
        )
    }
}

export default Stowage;