import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        no: "Crane Type",
        grainft3: "Capacity",
        baleft3: "No. (Outreach)",
        length: "Radius",
        widthfwd: "Width FWD",
        widthaft: "Width AFT",
        wttanktop: " WT Tanktop",
        wthold: "WT Hold",
        b: "B",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class HoldsInformation extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'No.',
            dataIndex: 'no',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'no'),
        },
        {
            title: 'Grain (Ft3)',
            dataIndex: 'grainft3',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'grainft3'),
        },
        {
            title: 'Bale (Ft3)',
            dataIndex: 'baleft3',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'baleft3'),
        },
        {
            title: 'Length',
            dataIndex: 'length',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'length'),
        },
        {
            title: 'Width FWD',
            dataIndex: 'widthfwd',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'widthfwd'),
        },
        {
            title: 'Width AFT',
            dataIndex: 'widthaft',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'widthaft'),
        },
        {
            title: 'WT Tanktop',
            dataIndex: 'wttanktop',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'wttanktop'),
        },
        {
            title: 'WT Hold',
            dataIndex: 'wthold',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'wthold'),
        },
        {
            title: 'B',
            dataIndex: 'b',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'b'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <Table
                bordered
                dataSource={this.state.data}
                columns={this.columns}
                scroll={{ y: 300 }}
                size="small"
                pagination={false}
                title={() => 'Crane Information'}
                footer={() => <div className="text-center">
                    <Button type="link">Add New</Button>
                </div>
                }
            />
        )
    }
}

export default HoldsInformation;