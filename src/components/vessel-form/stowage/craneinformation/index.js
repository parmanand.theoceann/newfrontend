import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        cranetype: "Crane Type",
        capacity: "Capacity",
        nooutreach: "No. (Outreach)",
        radius: "Radius",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class CraneInformation extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Crane Type',
            dataIndex: 'cranetype',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'cranetype'),
        },
        {
            title: 'Capacity',
            dataIndex: 'capacity',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'capacity'),
        },
        {
            title: 'No. (Outreach)',
            dataIndex: 'nooutreach',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'nooutreach'),
        },
        {
            title: 'Radius',
            dataIndex: 'radius',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'radius'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <Table
                bordered
                dataSource={this.state.data}
                columns={this.columns}
                scroll={{ y: 300 }}
                size="small"
                pagination={false}
                title={() => 'Crane Information'}
                footer={() => <div className="text-center">
                    <Button type="link">Add New</Button>
                </div>
                }
            />
        )
    }
}

export default CraneInformation;