import React, { Component } from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;
const { TextArea } = Input;

class Contacts extends Component {
    render() {
        return (
            <div className="detailWrapper">

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Manager">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Sat A">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Sat B">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Sat C">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Mini-M">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Telex">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Fax">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Cellular">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Master's No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="CCR No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Bridge No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Email">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Dem Analyst">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Engine Make">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Vessel Remarks">
                            <TextArea placeholder="" autoSize />
                        </FormItem>
                    </div>
                </div>

            </div>
        )
    }
}

export default Contacts;