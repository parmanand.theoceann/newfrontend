import React, { Component } from 'react';
import { Form, Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        region: "Region",
        tollballast: "Toll (Ballast)",
        tolllade: "Toll (Lade)",
        pd: "PD",
        xp: "XP",
        func: "Func",
        block: "Block",
        hide: "Hide",
        notolls: "No Tolls",
        use: "Use",
    });
}

const FormItem = Form.Item;

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class Routes extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Region',
            dataIndex: 'region',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'region'),
        },
        {
            title: 'Toll (Ballast)',
            dataIndex: 'tollballast',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tollballast'),
        },
        {
            title: 'Toll (Lade)',
            dataIndex: 'tolllade',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tolllade'),
        },
        {
            title: 'PD',
            dataIndex: 'pd',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'pd'),
        },
        {
            title: 'XP',
            dataIndex: 'xp',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'xp'),
        },
        {
            title: 'Func',
            dataIndex: 'func',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'func'),
        },
        {
            title: 'Block',
            dataIndex: 'block',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'block'),
        },
        {
            title: 'Hide',
            dataIndex: 'hide',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'hide'),
        },
        {
            title: 'No Tolls',
            dataIndex: 'notolls',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'notolls'),
        },
        {
            title: 'Use',
            dataIndex: 'use',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'use'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    // title={() => 'Port / Date Group'}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />

                <hr />

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Inherit Route Preference From">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>
                </div>
            </>
        )
    }
}

export default Routes;