import React, { Component } from 'react';
import { Form, Input, DatePicker, Checkbox } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;

function onndetailCheckbox(checkedValues) {
    // console.log('checked = ', checkedValues);
}

class Details extends Component {
    render() {
        return (
            <div className="detailWrapper" style={{width: '100%', padding: '0 25px'}}>

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Vessel Identification</span></h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Call Letters">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Operator">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Former Name">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Official No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Suez Vsl Type">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="PNS No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Yard">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Hatch Type">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Builder">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="H&M Value">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Build Details">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="P&I Club">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Vessel Flag">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="GAP Value">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Registry">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Pool Point">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Disponent Owner">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="DWT Date">
                            <DatePicker />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Hull No./Type">
                            <InputGroup compact>
                                <Input style={{ width: '50%' }} defaultValue="" />
                                <Input style={{ width: '50%' }} defaultValue="" />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Last Dry Dock">
                            <DatePicker />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Cross Ref. No.">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Next Dry Dock">
                            <DatePicker />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Ventilation">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Next Survey">
                            <DatePicker />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Ice Class">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Next Inspection">
                            <DatePicker />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Engine Make">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Last Prop Polished">
                            <DatePicker />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Propeller Pitch">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" disabled />
                                <Input style={{ width: '20%' }} defaultValue="M" disabled />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Last Hull Cleaning">
                            <DatePicker />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Cargo/Gear">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="TVE Expires">
                            <DatePicker />
                        </FormItem>
                    </div>
                </div>

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Capacity and Draft</span></h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="OPA ^90">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Lightship">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" />
                                <Input style={{ width: '20%' }} defaultValue="MT" />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Winter Draft">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" />
                                <Input style={{ width: '20%' }} defaultValue="M" />
                            </InputGroup>
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Max. Draft">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" />
                                <Input style={{ width: '20%' }} defaultValue="M" />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="GRT Int'l">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="NRT Int'l">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Panama Gross">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Net">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Suez Gross">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Net">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Grabs Qty">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Grabs Capacity">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" disabled />
                                <Input style={{ width: '20%' }} defaultValue="M3" disabled />
                            </InputGroup>
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Unit Factor">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>
                </div>

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Dimensions</span></h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="LOA">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Beam">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Depth">
                            <Input size="default" placeholder="" disabled />
                        </FormItem>
                    </div>
                </div>

                <hr />

                <div className="row">
                    <div className="col-md-12">

                        <Checkbox.Group onChange={onndetailCheckbox}>
                            <div className="row">
                                <div className="col-md-3"><Checkbox value="CO2 Fitted">CO2 Fitted</Checkbox></div>
                                <div className="col-md-3"><Checkbox value="Bow Thruster">Bow Thruster</Checkbox></div>
                                <div className="col-md-3"><Checkbox value="Toledo Suitable">Toledo Suitable</Checkbox></div>
                                <div className="col-md-3"><Checkbox value="Strengthened for Heavy Cargoes">Strengthened for Heavy Cargoes</Checkbox></div>
                                <div className="col-md-3"><Checkbox value="Australian Ladders">Australian Ladders</Checkbox></div>
                                <div className="col-md-3"><Checkbox value="TVE Valid">TVE Valid</Checkbox></div>
                            </div>
                        </Checkbox.Group>

                    </div>
                </div>

            </div>
        )
    }
}

export default Details;