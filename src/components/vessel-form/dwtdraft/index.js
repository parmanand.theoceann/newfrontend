import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        draftm: "Draft (M)",
        dwtmt: "DWT (MT)",
        displmt: "Displ (MT)",
        tpc: "TPC",
        remarks: "Remarks",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

const columns1 = [{
    title: '',
    dataIndex: 'someData',
    key: 'someData',
}, {
    title: 'Draft (M)',
    dataIndex: 'draftm',
    key: 'draftm',
}, {
    title: 'DWT (MT)',
    dataIndex: 'dwtmt',
    key: 'dwtmt',
}, {
    title: 'Displ (MT)',
    dataIndex: 'displmt',
    key: 'displmt',
}, {
    title: 'Free Board (M)',
    dataIndex: 'freeboardm',
    key: 'freeboardm',
}, {
    title: 'TPC',
    dataIndex: 'tpc',
    key: 'tpc',
}];

const data1 = [{
    key: '1',
    someData: 'At Design',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
}, {
    key: '2',
    someData: 'At Summer',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '3',
    someData: 'Tropical FW',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '4',
    someData: 'Tropical SW',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '5',
    someData: 'Fresh Water',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '6',
    someData: 'Winter',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '7',
    someData: 'Normal Ballast',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
{
    key: '8',
    someData: 'Lightship',
    draftm: '',
    dwtmt: '',
    displmt: '',
    freeboardm: '',
    tpc: '',
},
];

class DWTDraft extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Draft (M)',
            dataIndex: 'draftm',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'draftm'),
        },
        {
            title: 'DWT (MT)',
            dataIndex: 'dwtmt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'dwtmt'),
        },
        {
            title: 'Displ (MT)',
            dataIndex: 'displmt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'displmt'),
        },
        {
            title: 'TPC',
            dataIndex: 'tpc',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tpc'),
        },
        {
            title: 'Remarks',
            dataIndex: 'remarks',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'remarks'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    // title={() => 'Port / Date Group'}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />

                <hr />

                <div className="table-info-wrapper">
                    <Table size="small" bordered pagination={false} columns={columns1} dataSource={data1} />
                </div>
            </>
        )
    }
}

export default DWTDraft;