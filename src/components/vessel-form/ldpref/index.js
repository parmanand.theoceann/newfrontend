import React, { Component } from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

class LDPref extends Component {
    render() {
        return (
            <div className="detailWrapper">

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Load Information</span></h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Load Time (Hr)">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Min. Liquid Pressure">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Min. Gas Return (M3/Hr)">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Max. Gas Pressure">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Discharge Information</span></h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Discharge Time (Hr)">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Max. Liquid Pressure">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Max. Gas Return (M3/Hr)">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="Min. Gas Pressure">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>
                </div>

            </div>
        )
    }
}

export default LDPref;