import React, { Component } from 'react';
import { Form, Input, Button, Table } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;

const columns1 = [
    {
        title: 'Daily Cost',
        dataIndex: 'dailycost',
        key: 'dailycost',
    },
    {
        title: 'From GMT',
        dataIndex: 'fromgmt',
        key: 'fromgmt',
    },
    {
        title: 'To GMT',
        dataIndex: 'togmt',
        key: 'togmt',
    },
    {
        title: 'Duration',
        dataIndex: 'duration',
        key: 'duration',
    },
    {
        title: 'Comments',
        dataIndex: 'comments',
        key: 'comments',
    },
];

const data1 = [
    {
        key: "1",
        dailycost: "Daily Cost",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        comments: "Comments",
    },
    {
        key: "2",
        dailycost: "Daily Cost",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        comments: "Comments",
    },
    {
        key: "3",
        dailycost: "Daily Cost",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        comments: "Comments",
    },
    {
        key: "4",
        dailycost: "Daily Cost",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        comments: "Comments",
    },
    {
        key: "5",
        dailycost: "Daily Cost",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        comments: "Comments",
    },
];

const columns2 = [
    {
        title: 'Speed',
        dataIndex: 'speed',
        key: 'speed',
    },
    {
        title: 'B/L',
        dataIndex: 'bl',
        key: 'bl',
    },
    {
        title: 'Engine Load',
        dataIndex: 'engineload',
        key: 'engineload',
    },
];

const data2 = [
    {
        key: "1",
        speed: "Speed",
        bl: "B/L",
        engineload: "Engine Load",
    },
    {
        key: "2",
        speed: "Speed",
        bl: "B/L",
        engineload: "Engine Load",
    },
    {
        key: "3",
        speed: "Speed",
        bl: "B/L",
        engineload: "Engine Load",
    },
    {
        key: "4",
        speed: "Speed",
        bl: "B/L",
        engineload: "Engine Load",
    },
    {
        key: "5",
        speed: "Speed",
        bl: "B/L",
        engineload: "Engine Load",
    },
];

class MultipleRates extends Component {

    render() {
        return (
            <div className="multipleRates" style={{width: '100%', padding: '0 25px'}}>

                <div className="row">
                    <div className="col-md-4">
                        <FormItem label="ID">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <FormItem label="Admin Fee/Day">
                            <InputGroup compact>
                                <Input style={{ width: '80%' }} defaultValue="" />
                                <Input style={{ width: '20%' }} defaultValue="" />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-4">
                        <div className="action-btn">
                            <Button type="primary" htmlType="submit">Create New Group</Button>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="table-info-wrapper">
                            <Table size="small" bordered pagination={false} columns={columns1} dataSource={data1} />
                        </div>
                    </div>
                </div>

                <hr />

                <div className="row">
                    <div className="col-md-12">
                        <div className="table-info-wrapper">
                            <Table size="small" bordered pagination={false} columns={columns2} dataSource={data2} />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default MultipleRates;