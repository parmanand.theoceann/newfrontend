import React from "react";
import "./fixediconstrap.css";
import URL_WITH_VERSION, { IMAGE_PATH } from "../../../shared";
const FixedIconStrap = () => {
  return (
    <>
      <div style={{ textAlign: "center" }} className="vertical">
        <div className="icons">
          <a href="#/vessel-form">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/addvessel.svg"}
                  alt="addvessel"
                />
              </div>
              <h4 className="h4">Add</h4>
              <h4 className="h4">vessel</h4>
            </div>
          </a>
          <a href="#/add-voyage-estimate">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/voyage.svg"}
                  alt="Voyage"
                />
              </div>
              <h4 className="h4">Voyage</h4>
              <h4 className="h4">Estimate</h4>
            </div>
          </a>
          <a href="#/map-intelligence">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/map.svg"}
                  alt="map"
                />
              </div>
              <h4 className="h4">Distance</h4>
              <h4 className="h4">Map</h4>
            </div>
          </a>
          
          <a href="#/track-my-fleet">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/track.svg"}
                  alt="Track"
                />
              </div>
              <h4 className="h4">Track my</h4>
              <h4 className="h4">fleet</h4>
            </div>
          </a>
          <div className="vessel">
            <div>
              <a href="#">
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/chat.svg"}
                  alt="chat"
                />
              </a>
            </div>
            <h4 className="h4">Chat</h4>
          </div>
          <a href="#/spot-price">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/bunker.svg"}
                  alt="Bunker"
                />
              </div>
              <h4 className="h4">Live</h4>
              <h4 className="h4">Bunker</h4>
            </div>
          </a>

          <a href="#/add-cargo-contract">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/cargo-vc.svg"}
                  alt="CargoVC"
                />
              </div>
              <h4 className="h4">Cargo VC</h4>
            </div>
          </a>
          <div className="vessel">
            <div>
              <a href="#">
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/inbox.svg"}
                  alt="Inboxsvg"
                />
              </a>
            </div>
            <h4 className="h4">Inbox</h4>
          </div>
          {/* <div className="vessel">
            <div>
              <a href="#">
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/market.svg"}
                  alt="marketsvg"
                />
              </a>
            </div>
            <h4 className="h4">Market</h4>
            <h4 className="h4">Order</h4>
          </div> */}

          <a href="#/GenerateCargoEnquiry">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/market.svg"}
                  alt="marketsvg"
                />
              </div>
              <h4 className="h4">Market</h4>
              <h4 className="h4">Order</h4>
            </div>
          </a>

          <a href="#/vessel-open-schedule">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/position.svg"}
                  alt="positionsvg"
                />
              </div>
              <h4 className="h4">Position</h4>
              <h4 className="h4">List</h4>
            </div>
          </a>
          <a href="#/voyage-manager-list">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/vm.svg"}
                  alt="vmsvg"
                />
              </div>
              <h4 className="h4">Voyage</h4>
              <h4 className="h4">Manager</h4>
              <h4 className="h4">List</h4>
            </div>
          </a>
          <a href="#/PDA-list">
            <div className="vessel">
              <div>
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/portcalls.svg"}
                  alt="port callssvg"
                />
              </div>
              <h4 className="h4">Port</h4>
              <h4 className="h4">Calls</h4>
            </div>
          </a>
          <div className="vessel">
            <div>
              <a href="#/calendar-view">
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/calendar.svg"}
                  alt="Calendrasvg"
                />
              </a>
            </div>
            <h4 className="h4">Calendar</h4>
            <h4 className="h4">View</h4>
          </div>

          <div className="vessel">
            <div>
              <a href="#/cii-dashboard">
                <img
                  className="svg"
                  src={IMAGE_PATH + "svgimg/cii.svg"}
                  alt="Cllsvg"
                />
              </a>
            </div>
            <h4 className="h4">Cll</h4>
          </div>
        </div>
      </div>
    </>
  );
};

export default FixedIconStrap;
