import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import AppCustomizer from "../../Customizer";
import APPCONFIG from "../../../constants/appConfig";
import App from "./App";
// import AppV2 from "./AppV2";
//import ContentOnly from "./ContentOnly";
import HeaderContentFooter from "./HeaderContentFooter";
import { useLocation, useNavigate } from "react-router-dom";
const AppLayout = (props) => {
  const [showSHF, setShowSHF] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();
  window.navigationFunction = () => {
    return navigate;
  };
  const { layout, boxedLayout, fixedSidenav, fixedHeader } = props;
  const updateLayout = (
    layout,
    boxedLayout,
    fixedSidenav,
    fixedHeader,
    showSHF
  ) => {
    switch (layout) {
      case "1":
        return (
          <App
            boxedLayout={boxedLayout}
            fixedSidenav={fixedSidenav}
            fixedHeader={fixedHeader}
            showSHF={showSHF}
          />
        );
      // case "2":
      //   return <AppV2 boxedLayout={boxedLayout} />;
      case "3":
        return (
          <HeaderContentFooter
            boxedLayout={boxedLayout}
            fixedHeader={fixedHeader}
          />
        );
      // case "4":
      //   return <ContentOnly boxedLayout={boxedLayout} />;
      default:
        return <App />;
    }
  };

  const isShowCustomizer = () => {
    if (APPCONFIG.customizer) {
      return <AppCustomizer />;
    }
    return null;
  };

  useEffect(() => {
    if (
      location.pathname.indexOf("/user/") === -1 &&
      location.pathname.indexOf("/docs/") === -1
    ) {
      // showSHF = true;
      setShowSHF(true);
    }
  }, [location]);

  return (
    <div id="app-layout-container">
      {updateLayout(layout, boxedLayout, fixedSidenav, fixedHeader, showSHF)}
      {isShowCustomizer()}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  layout: state.settings.layout,
  boxedLayout: state.settings.boxedLayout,
  fixedSidenav: state.settings.fixedSidenav,
  fixedHeader: state.settings.fixedHeader,
});

export default connect(mapStateToProps)(AppLayout);
