
import React from "react";
import classnames from "classnames";
import Cookies from "universal-cookie";
import { Outlet, Navigate } from "react-router-dom";
import { Layout } from "antd";
import AppHeader from "../Header";
import AppFooter from "../Footer";
import AppContent from "../Content";
import AppSidenav from "../Sidenav";
import AppBreadcrumb from "../Breadcrumb";
import FixedIconStrap from "./FixedIconStrap";
const cookies = new Cookies();
class AppLayout extends React.Component {
  render() {
    const { showSHF, boxedLayout, fixedSidenav, fixedHeader } = this.props;

    return showSHF === true ? (
      <Layout
        id="app-layout"
        className={classnames("app-layout", {
          "boxed-layout": boxedLayout,
          "fixed-sidenav": fixedSidenav,
          "fixed-header": fixedHeader,
        })}
      >
        <AppSidenav />
        {fixedHeader ? (
          // this component is responsible for layout.
          <Layout>
            <AppHeader />
            <div style={{ display: "flex" }}>
              <FixedIconStrap />

              <Layout>
                <div className="container-fluid no-breadcrumb chapter page-wrapper page-container">
                  <div className="header-wrapper">
                    <AppBreadcrumb />
                  </div>
                  <Outlet/>
                </div>
              </Layout>

            </div>
            <AppFooter />

          </Layout>
        ) : (
          <Layout>
            <AppHeader />

            <Outlet />
            <AppFooter />
          </Layout>
        )}
      </Layout>
    ) : (
      <Layout>
        <Outlet />
      </Layout>
    );
  }
}

export default AppLayout;
