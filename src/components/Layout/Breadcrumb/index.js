import React, { useEffect } from "react";
import { useLocation, useParams, useNavigate, Link } from "react-router-dom";
import { Breadcrumb } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { breadcrumb } from "../../../constants/breadcrumbConstant";

const Section = (props) => {
  const location = useLocation();
  const pathSnippets = location.pathname.split("/").filter((i) => i);
  let pathSnippetsPath = pathSnippets[pathSnippets.length - 1];
  const pathSnippetsFormatted =
    breadcrumb[pathSnippetsPath] ||
    pathSnippets.map((snippet, i) => {
      const words =
        breadcrumb[snippet] || snippet.replace(/-/g, " ").split(" ");
      return words
        .map((word) => {
          return word[0].toUpperCase() + word.substring(1);
        })
        .join(" ");
    });

  const extraBreadcrumbItems = pathSnippetsFormatted.map((snippet, index) => {
    return {
      key: snippet,
      title: snippet,
    };
  });

  const breadcrumbItems = [
    {
      href: "/",
      title: <Link to="/chartering-dashboard"><HomeOutlined /></Link>,
     
    },
  ].concat(extraBreadcrumbItems);

  return (
    // <div className="app-breadcrumb add-button-wrap">
    <div className="app-breadcrumb">
      <Breadcrumb items={breadcrumbItems} />
      {/* <div className="toolbar-ui-wrapper">
          <div className="rightsection">
            <span className="wrap-bar-menu">
              <ul className="wrap-bar-ul">
                <li><span className="text-bt">Save All</span></li>
                <li><span className="text-bt">Rename</span></li>
                <li><span className="text-bt">Copy</span></li>
                <li><span className="text-bt">Delete</span></li>
                <li><span className="text-bt"><Icon type="global" /></span></li>
                <li><span className="text-bt"><Icon type="table" /></span></li>
              </ul>
            </span>
          </div>
        </div> */}
    </div>
  );
};

const withLocation = (Component) => (props) => {
  let location = useLocation();
  let navigate = useNavigate();
  let params = useParams();
  return <Component {...props} router={{ location, navigate, params }} />;
};

export default withLocation(Section);
