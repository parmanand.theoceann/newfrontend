import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import { Navigate, useNavigate } from "react-router-dom";
import DEMO from "../../../constants/demoData";
import {
  Layout,
  Menu,
  Dropdown,
  Avatar,
  Popover,
  Divider,
  Tooltip,
} from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  BellOutlined,
  SettingOutlined,
  UserOutlined,
  LogoutOutlined,
  InfoCircleOutlined,
  AppstoreOutlined,
} from "@ant-design/icons";

import Logo from "../../../components/Logo";
import Cookies from "universal-cookie";
import URL_WITH_VERSION, {
  IMAGE_PATH,
  getAPICall,
} from "../../../../src/shared";

import {
  toggleCollapsedNav,
  toggleOffCanvasMobileNav,
} from "../../../actions/settingsActions";
import Notifications from "../../../routes/layout/routes/header/components/Notifications";
import { useLocation } from "react-router-dom";

const { Header } = Layout;

const cookies = new Cookies();
let d = new Date();
d.setTime(d.getTime() + 3600 * 10 * 1000);
const AppHeader = (props) => {
  const navigate = useNavigate();
  const [data, setData] = useState(null);
  const [clickCount, setClickCount] = useState(0);
  const location = useLocation();
  const { collapsedNav, offCanvasMobileNav, colorOption, showLogo } = props;
  useEffect(() => {
    const { handleToggleCollapsedNav, collapsedNav } = props;
    if (location.pathname) {
      handleToggleCollapsedNav({ collapsedNav: false });
    }
  }, [location.pathname]);

  let currentUser = {};
  useEffect(() => {
    const token = localStorage.getItem("oceanToken").split(".");
    if (token) {
      const userDetail = JSON.parse(atob(token[1]));
      setData(userDetail);
    } else {
      navigate("/user/login");
    }

    // if (username != null) {
    //   getuserinfo(username);
    // } else {
    // <Navigate to={"/user/login"} replace />;
    // navigate("/user/login")
    // }
  }, []);

  // const getuserinfo = async (username) => {
  //   const response = await getAPICall(
  //     `${URL_WITH_VERSION}/user/edit?un=${username}`
  //   );
  //   const respData = await response["data"];
  //   console.log(respData,"responcedata");
  //   cookies.set("user_id", `${respData.user_id}`, { path: "/", expires: d });
  //   setData(respData);
  // };
  const onToggleCollapsedNav = () => {
    const { handleToggleCollapsedNav, collapsedNav } = props;
    handleToggleCollapsedNav(!collapsedNav);
  };

  const onToggleOffCanvasMobileNav = () => {
    const { handleToggleOffCanvasMobileNav, offCanvasMobileNav } = props;
    handleToggleOffCanvasMobileNav(!offCanvasMobileNav);
  };

  const logout = () => {
    localStorage.clear();
    return <Navigate to={"/user/login"} replace={true} />;
  };

  const avatarDropdown = (
    <Menu className="app-header-dropdown">
      <Menu.Item key="4" className="d-block d-md-none">
        {" "}
        Signed in as <strong>{DEMO.user}</strong>{" "}
      </Menu.Item>
      <Menu.Divider className="d-block d-md-none" />
      <Menu.Item key="1" disabled>
        <SettingOutlined />
        Settings{" "}
      </Menu.Item>
      <Menu.Item key="0">
        {" "}
        <a href="#/my-profile">
          <UserOutlined />
          Profile
        </a>{" "}
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
        {" "}
        <a onClick={logout} href={DEMO.headerLink.signOut}>
          <LogoutOutlined />
          Log out
        </a>{" "}
      </Menu.Item>
    </Menu>
  );

  return (
    <Header className="app-header">
      <div
        className={classnames("app-header-inner", {
          "bg-white":
            ["11", "12", "13", "14", "15", "16", "21"].indexOf(colorOption) >=
            0,
          "bg-dark": colorOption === "31",
          "bg-primary": ["22", "32"].indexOf(colorOption) >= 0,
          "bg-success": ["23", "33"].indexOf(colorOption) >= 0,
          "bg-info": ["24", "34"].indexOf(colorOption) >= 0,
          "bg-warning": ["25", "35"].indexOf(colorOption) >= 0,
          "bg-danger": ["26", "36"].indexOf(colorOption) >= 0,
        })}
      >
        <div className="header-left">
          <div className="list-unstyled list-inline">
            {showLogo && [
              <Logo key="logo" />,
              <Divider type="vertical" key="line" />,
            ]}
            <a
              className="list-inline-item d-none d-md-inline-block"
              onClick={onToggleCollapsedNav}
            >
              {collapsedNav ? (
                <MenuUnfoldOutlined className="list-icon" />
              ) : (
                <MenuFoldOutlined className="list-icon" />
              )}
            </a>
            <a
              className="list-inline-item d-md-none"
              onClick={onToggleOffCanvasMobileNav}
            >
              {offCanvasMobileNav ? (
                <MenuUnfoldOutlined className="list-icon" />
              ) : (
                <MenuFoldOutlined className="list-icon" />
              )}
            </a>
            {/* <Tooltip placement="bottom" title="UI Overview">
                        <a href="#/ui-overview" className="list-inline-item d-none d-md-inline-block">
                          <Icon type="shop" className="list-icon" />
                        </a>
                      </Tooltip> */}
          </div>
        </div>

        <div className="header-right">
          <div className="list-unstyled list-inline">
            {/* <li className="list-inline-item search-box seach-box-right d-none d-md-inline-block">
              <Tooltip placement="bottom" title="watch videos for user manual">
                <a
                  href={DEMO.helpurl}
                  target="_blank"
                  className="list-inline-item"
                  style={{ color: "#fff", fontSize: "26px" }}
                >
                  <InfoCircleOutlined className="list-notification-icon" />
                </a>
              </Tooltip>
            </li> */}
            <a
              href="#/service-desk"
              className="list-inline-item"
              style={{
                color: "#fff",
                fontSize: "26px",
                background: "#12406A",
              }}
            >
              <AppstoreOutlined />
              <span style={{ fontSize: "14px" }}> Service Desk</span>
            </a>
            <Popover
              placement="bottomRight"
              content={<Notifications />}
              trigger="click"
              overlayClassName="app-header-popover"
            >
              <a href={DEMO.link} className="list-inline-item">
                {/* <Badge count={4}>
                  <NotificationOutlined className="list-notification-icon" />
                </Badge> */}
                <BellOutlined className="list-notification-icon" />
              </a>
            </Popover>
            <Dropdown
              className="list-inline-item"
              overlay={avatarDropdown}
              trigger={["click"]}
              placement="bottomRight"
            >
              <a className="ant-dropdown-link no-link-style" href={DEMO.link}>
                <Avatar src={IMAGE_PATH + "gravatar/user-g.png"} size="small" />
                <span className="avatar-text d-none d-md-inline">
                  {localStorage.setItem(
                    "currentUser",
                    (data?.first_name ? data.first_name : "-") +
                      " " +
                      (data?.last_name ? data.last_name : "-")
                  )}
                  {data?.first_name ? data.first_name : "-"}{" "}
                  {data?.last_name ? data.last_name : "-"}
                </span>
                {/* <p className="avatar-text d-none d-md-inline">({this.state.data && this.state.data.user_name ? this.state.data.user_name : '--'})</p> */}
              </a>
            </Dropdown>
          </div>
        </div>
      </div>
    </Header>
  );
};

const mapStateToProps = (state) => ({
  offCanvasMobileNav: state.settings.offCanvasMobileNav,
  collapsedNav: state.settings.collapsedNav,
  colorOption: state.settings.colorOption,
});

const mapDispatchToProps = (dispatch) => ({
  handleToggleCollapsedNav: (isCollapsedNav) => {
    dispatch(toggleCollapsedNav(isCollapsedNav));
  },
  handleToggleOffCanvasMobileNav: (isOffCanvasMobileNav) => {
    dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader);
