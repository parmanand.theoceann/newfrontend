import React, { Component } from "react";
import { Link } from "react-router-dom";
import styles from "../../../styles/menu.module.css";
import { connect } from "react-redux";
import { Menu, Tooltip, Collapse, Tabs } from "antd";
import permissionFunction from "./permissions";
import { toggleOffCanvasMobileNav } from "../../../actions/settingsActions";
import Cookies from "universal-cookie";
import { decrypt } from "../../../routes/form/routes/forms/components/crpyto";
import URL_WITH_VERSION, { postAPICall, getAPICall } from "../../../shared";
import { DownOutlined, UpOutlined, LockFilled } from "@ant-design/icons";
import ListItem from "./listItem";
import ErrorList from "antd/es/form/ErrorList";

const cookies = new Cookies();
const SubMenu = Menu.SubMenu;

class AppMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ISBROKERS: null,
      userAccessdata: [],
      showCharteringChildren: true,
      showoperationChildren: true,
      showfinanceChildren: true,
      showsettingChildren: true,
      showoceannzeroChildren: true,
      showoceannMailChildren: true,
      isHovered: false,
      hoverIndex: "",
      accessList: [],
      permissionList: [],
      isAdmin: true,
    };
    this.menuref = React.createRef();
  }

  componentDidMount = () => {
    this.getMenuList();
    const permissions = JSON.parse(localStorage.getItem("permissionList"));
    // console.log("listmounted",permissions);
    this.setState((prevState) => ({
      permissionList: permissions,
    }));
  };

  menuHasAccess = (menu) => {
    const { userAccessdata } = this.state;

    let smenu = menu.replace(/[^A-Z0-9]/gi, "").toLocaleLowerCase()
    
    return userAccessdata.some((item) => {
      let sitem =
        item && item["menu_name"]
          ? item.menu_name.replace(/[^A-Z0-9]/gi, "").toLocaleLowerCase()
          : "";
      if (
        sitem == smenu ||
        (item["child"] &&
          item["child"].length > 0 &&
          item["child"].some(
            (el) =>
              el.menu_name.replace(/[^A-Z0-9]/gi, "").toLocaleLowerCase() ==
              smenu
          ))
      ) {
        return true;
      } else {
        return false;
      }
    });
  };

  getMenuList = async () => {
    // let userId = cookies.get("user_id") ? cookies.get("user_id") : null;
    try {
      // const response = await getAPICall(
      //   `${URL_WITH_VERSION}/userauth/permissions?un=${"USR-1110"}`
      // );
      // const respdata = await response;
      // const data = respdata && respdata.data ? respdata.data : [];
      const data = permissionFunction();
      data.map((el) => {
        el["key"] = el.id;
        if (el["child"] && el["child"].length > 0) {
          el["child"].map((item) => (item["key"] = item.short_code));
        }
      });

      this.setState({ ...this.state, userAccessdata: [...data] });
    } catch (err) {
      console.log("err", err);
    }
  };

  render() {
    const { location } = this.props;
    const { ISBROKERS } = this.state;
    let activePathName = location && location.pathname ? location.pathname : "";
    let activeClass = "bg-theme-menu";

    let usertype = cookies.get("user_type") ? cookies.get("user_type") : null;

    //----------------------------------------------------

    let charteringchildren = [
      {
        title: "Voyage Estimate - List",
        label: "Voyage Estimate - List",
        link: ["#/add-voyage-estimate", "#/Voyage-Estimate-list"],
        children: true,
      },
      {
        title: "Quick Estimate - List",
        label: "Quick Estimate - List",
        link: ["#/quick-estimate", "#/quick-estimate-list"],
        children: true,
      },

      {
        title: "TC Estimate - List",
        label: "TC Estimate - List",
        link: ["#/tc-est-fullestimate", "#/TC-Estimate-list"],
        children: true,
      },
      {
        title: "TC IN - List",
        label: "TC IN - List",
        link: ["#/add-TC-IN", "#/TC-IN-list"],
        children: true,
      },
      {
        title: "TC OUT - List",
        label: "TC OUT - List",
        link: ["#/Add-TC-Out", "#/TC-OUT-list"],
        children: true,
      },
      {
        title: "Voyage Charter - List",
        label: "Cargo Contract - List",
        link: ["#/add-cargo-contract", "#/cargo-contract-list"],
        children: true,
      },

      {
        title: "Bunker Live Prices",
        label: "Bunker Live Prices",
        link: ["#/spot-price"],
        children: false,
      },

      {
        title: "My Fleet For Tracking",
        label: "My Fleet For Tracking",
        link: ["#/track-my-fleet"],
        children: false,
      },

      {
        title: "Vessel Open schedule",
        label: "Vessel Open Position",
        link: ["#/vessel-open-schedule"],
        children: false,
      },

      {
        title: "VC(Purchase) - List",
        label: "VC(Purchase) - List",
        link: ["#/add-voyage-cargo", "#/voyage-cargo-list"],
        children: true,
      },

      {
        title: "COA (VC) - List",
        label: "COA (VC) - List",
        link: ["#/add-coa-vci", "#/list-coa-vci"],
        children: true,
      },

      {
        title: "Voy Relet - List",
        label: "Voy Relet - List",
        link: ["#/voy-relet-full-estimate", "#/voy-relet-list"],
        children: true,
      },
    ];

    let operationchildren = [
      {
        title: "Analytical Dashboard",
        label: "Analytical Dashboard",
        link: ["#/chartering-dashboard"],
        children: false,
      },

      {
        title: "Voyage Manager -List",
        label: "Voyage Manager List",
        link: ["#/voyage-manager-list"],
        children: false,
      },

      {
        title: "Bunker requirement -List",
        label: "Bunker requirement -List",
        link: ["#/bunker-requirement", "#/bunker-requirement-list"],
        children: true,
      },

      {
        title: "Laytime - List",
        label: "Laytime - List",
        link: ["#/laytime-invoice", "#/laytime-listing"],
        children: true,
      },

      {
        title: "Port Cost List",
        label: "Port Cost List",
        link: ["#/port-cost"],
        children: false,
      },
    ];

    let financechildren = [
      {
        title: "Finance Dashboard",
        label: "Finance Dashboard",
        link: ["#/finance-dashboard"],
        children: false,
      },

      {
        title: "Hire Payable Trxn List",
        label: "Hire Payable Trxn List",
        link: ["#/hire-payable-list"],
        children: false,
      },

      {
        title: "Hire Issue Rcv Trxn List",
        label: "Hire Issue Rcv Trxn List",
        link: ["#/hire-receivable-list"],
        children: false,
      },

      {
        title: "Bunker Invoice - List",
        label: "Bunker Invoice - List",
        link: ["#/bunker-invoice", "#/bunker-invoice-list"],
        children: true,
      },

      {
        title: "Trxn. Summary List",
        label: "Trxn. Summary List",
        link: ["#/transaction-summary-list"],
        children: false,
      },

      {
        title: "Vendor transactional List",
        label: "Vendor transaction List",
        link: ["#/vendor-transaction-list"],
        children: false,
      },

     
    ];

    let settingChildren = [
      {
        title: "Add New Vessel - List",
        label: "Add New Vessel - List",
        link: ["#/vessel-form", "#/vessel-list"],
        children: true,
      },

      {
        title: "Add New Company - List",
        label: "Add New Company - List",
        link: ["#/add-address-form", "#/address-list"],
        children: true,
      },

      {
        title: "Task & Alert",
        label: "Task & Alert",
        link: ["#/voyage-task-and-alert"],
        children: false,
      },
    ];

    let oceanzeroChildren = [
      {
        title: "Live Fleet Map Intel",
        label: "Live Fleet Map Intel",
        link: ["#"],
        children: false,
      },

      {
        title: "Vessel Perf. Dynamics",
        label: "Vessel Perf. Dynamics",
        link: ["#"],
        children: false,
      },

      {
        title: "CII Dynamics",
        label: "CII Dynamics",
        link: ["#"],
        children: false,
      },

      {
        title: "EU ETS Dynamics",
        label: "EU ETS Dynamics",
        link: ["#"],
        children: false,
      },

      {
        title: "Vessel E Log",
        label: "Vessel E Log",
        link: ["#"],
        children: false,
      },
    ];

    let oceanMailChildren = [
      {
        title: "Inbox",
        label: "Inbox",
        link: ["#"],
        children: false,
      },

      {
        title: "Trade data",
        label: "Trade data",
        link: ["#"],
        children: false,
      },

      {
        title: "Market data",
        label: "Market data",
        link: ["#"],
        children: false,
      },

      {
        title: "Whatsapp",
        label: "Whatsapp",
        link: ["#"],
        children: false,
      },
    ];

    let charteringsubchildren1 = [
      {
        title: "My Fleet For Tracking",
        label: "My Fleet For Tracking",
        link: ["#/track-my-fleet"],
        children: false,
      },

      {
        title: "Track All vessel LIVE",
        label: "Track All vessel LIVE",
        link: ["#/live-vessel"],
        children: false,
      },

      {
        title: "Port/Berth Database",
        label: "Port Data",
        link: ["#/port-data"],
        children: false,
      },

      {
        title: "Map Intelligence",
        label: "Map Intelligence",
        link: ["#/map-intelligence"],
        children: false,
      },

      {
        title: "Vessel Database",
        label: "Vessel Data",
        link: ["#/vessel-data"],
        children: false,
      },
    ];

    let charteringsubchildren2 = [
      {
        title: "COA(Cargo)-list",
        label: "COA(Cargo)-list",
        link: ["#/add-coa-contract", "#/coa-list"],
        children: false,
      },

      {
        title: "Doc sharelink",
        label: "Doc sharelink",
        link: ["#/docshare"],
        children: false,
      },
    ];

    // let charteringsubchildren3 = [
    //   {
    //     title: "Time Charter In Contract",
    //     label: "TC In Contract",
    //     link: ["#/add-TC-IN"],
    //     children: false,
    //   },

    //   {
    //     title: "Time Charter out Contract",
    //     label: "TC out Contract",
    //     link: ["#/Add-TC-Out"],
    //     children: false,
    //   },
    // ];

    let charteringsubchildren4 = [
      // {
      //   title: "Bunkering Performance Report",
      //   label: "Bunker performance matrix",
      //   link: ["#/bunkerperformance-report"],
      //   children: false,
      // },

      {
        title: "Finance Dashboard",
        label: "Finance Dashboard",
        link: ["#/finance-dashboard"],
        children: false,
      },

      {
        title: "Port Cost",
        label: "Port performance matrix",
        link: ["#/port-cost"],
        children: false,
      },

      {
        title: "Chartering Dashboard",
        label: "Chartering Dashboard",
        link: ["#/chartering-dashboard"],
        children: false,
      },
      {
        title: "Voyage-Efficiency-Dashboard",
        label: "Voyage Efficiency Dashboard",
        link: ["#/voyage-efficiency-dashboard"],
        children: false,
      },
      {
        title: "Cargo Analytical Dashboard",
        label: "Cargo Analytical Dashboard",
        link: ["#/cargo-analytical-dashboard"],
        children: false,
      },
      {
        title: "Bunker Dashboard",
        label: "Bunker Dashboard",
        link: ["#/bunker-dashboard"],
        children: false,
      },
    ];

    let charteringchildrenarr = [
      ...charteringsubchildren1,
      ...charteringsubchildren2,
      // ...charteringsubchildren3,
      ...charteringsubchildren4,
    ];

    let operationsubchildren1 = [
      {
        title: "Bunker Purchase - List",
        label: "Bunker Purchase - List",
        link: ["#/bunker-purchased-order", "#/bunker-purchase-list"],
        children: true,
      },

      {
        title: "Bunker Invoice - List",
        label: "Bunker Invoice - List",
        link: ["#/bunker-invoice", "#/bunker-invoice-list"],
        children: true,
      },
    ];

    let operationsubchildren2 = [
      {
        title: "PDA List",
        label: "PDA List",
        link: ["#/PDA-list"],
        children: false,
      },

      {
        title: "FDA List",
        label: "FDA List",
        link: ["#/FDA-list"],
        children: false,
      },

      {
        title: "Port Informaiton List",
        label: "Port Informaiton List",
        link: ["#/port-information-list"],
        children: false,
      },

      {
        title: "Port Payment Invoice List",
        label: "Port Payment Invoice List",
        link: ["#"],
        children: false,
      },
    ];

    let operationsubchildren3 = [
      {
        title: "Laytime Calculator",
        label: "Laytime Calculator",
        link: ["#"],
        children: false,
      },

      {
        title: "Claim Invoice List",
        label: "Claim Invoice List",
        link: ["#/claim-listing"],
        children: false,
      },

      {
        title: "Disputed Claim List",
        label: "Disputed Claim List",
        link: ["#/dispute-claim-listing"],
        children: false,
      },
    ];

    let operationsubchildren4 = [
      {
        title: "Hire Payment Invoice List",
        label: "Hire Payment Invoice List",
        link: ["#/hire-payable-list"],
        children: false,
      },

      {
        title: "Hire Issue Bill List",
        label: "Hire Issue Bill List",
        link: ["#/hire-receivable-list"],
        children: false,
      },
    ];

    let operationsubchildren5 = [
      {
        title: "Other Expense - List",
        label: "Other Expense - List",
        link: ["#/other-revenue-expense", "#/other-expense-list"],
        children: true,
      },

      {
        title: "Other Revenue - List",
        label: "Other Revenue - List",
        link: ["#/other-revenue", "#/other-revenue-list"],
        children: true,
      },

      {
        title: "OffHire/Deviation - List",
        label: "OffHire/Deviation - List",
        link: ["#/OffHire-Deviation", "#/OffHire-Deviation-list"],
        children: true,
      },
    ];

    let operationsubchildren6 = [
      {
        title: "TC IN - List",
        label: "Time Charter IN - List",
        link: ["#/add-TC-IN", "#/TC-IN-list"],
        children: true,
      },

      {
        title: "TCO - List",
        label: "Time Charter Out - List",
        link: ["#/add-TC-Out", "#/TC-OUT-list"],
        children: true,
      },

      {
        title: "FHS List",
        label: "FHS List",
        link: ["#"],
        children: false,
      },
    ];

    let operationsubchildren7 = [
      {
        title: "Freight Invoice - List",
        label: "Freight Invoice - List",
        link: ["#/freight-invoice", "#/initial-freight-invoice-summary"],
        children: true,
      },

      {
        title: "Freight Commission - List",
        label: "Freight Commission - List",
        link: ["#/freight-commission", "#/freight-commission-list"],
        children: true,
      },

      {
        title: "Demmurrage Commission - List",
        label: "Demmurrage Commission - List",
        link: ["#", "#"],
        children: true,
      },
    ];

    // let operationsubchildren8 = [
    //   {
    //     title: "Bunkering Time Performance",
    //     label: "Bunkering Time Performance",
    //     link: ["#/bunkertime-performance"],
    //     children: false,
    //   },

    //   {
    //     title: "Voyage Operation Performance",
    //     label: "Voyage Operation Performance",
    //     link: ["#/voyageoperation-performance"],
    //     children: false,
    //   },

    //   {
    //     title: "Cargo Performance",
    //     label: "Cargo Performance",
    //     link: ["#/cargo-performance"],
    //     children: false,
    //   },
    //   {
    //     title: "Pooling Distribution-List",
    //     label: "Pooling Distribution-List",
    //     link: ["#/pooling-distribution"],
    //     children: false,
    //   },
    // ];

    let operationsubchildrenarr = [
      ...operationsubchildren1,
      ...operationsubchildren2,
      ...operationsubchildren3,
      ...operationsubchildren4,
      ...operationsubchildren5,
      ...operationsubchildren6,
      ...operationsubchildren7,
      // ...operationsubchildren8,
    ];

    let financesubchildrenarr1 = [
      {
        title: "Bunker Vendor Transaction List",
        label: "Bunker Vendor Transaction List",
        link: ["#/bunker-vendor-transaction-list"],
        children: false,
      },
      {
        title: "Other Vendors Transaction List",
        label: "Other Vendors Transaction List",
        link: ["#/other-vendor-transaction-list"],
        children: false,
      },

      {
        title: "Agent Vendors Transaction List",
        label: "Port Expense Transaction List",
        link: ["#//agent-transaction-list"],
        children: false,
      },
    ];

    let financesubchildrenarr2 = [
      {
        title: "Prepared Invoice List",
        label: "Prepared Invoice List",
        link: ["#/prepared-invoice-list"],
        children: false,
      },
      {
        title: "Verified Invoice List",
        label: "Verified Invoice List",
        link: ["#/verified-invoice-list"],
        children: false,
      },

      {
        title: "Approved Invoice List",
        label: "Approved Invoice List",
        link: ["#/approved-invoice-list"],
        children: false,
      },
      {
        title: "Posted Invoice List",
        label: "Posted Invoice List",
        link: ["#/posted-invoice-list"],
        children: false,
      },

      {
        title: "Deleted Invoice List",
        label: "Deleted Invoice List",
        link: ["#/delete-invoice-list"],
        children: false,
      },
    ];

    let financesubchildrenarr3 = [
      {
        title: "TC Commission Invoice List",
        label: "TC Commission Invoice List",
        link: ["#/tc-commission-invoice-list"],
        children: false,
      },
      {
        title: "Address Commission List",
        label: "Address Commission List",
        link: ["#/address-commission-invoice-list"],
        children: false,
      },

      {
        title: "Demmurrage Commission - List",
        label: "Demmurrage Commission - List",
        link: ["#"],
        children: false,
      },
    ];



    let financesubchildrenarr4 = [
      {
        title: "Advance Payment - List",
        label: "Advance Payment - List",
        link: ["#/advance-payment", "#/advance-payment-list"],
        children: true,
      },
      {
        title: "Advance Payment Receipt Allocations",
        label: "Advance Payment Receipt Allocations",
        link: ["#/advance-payment-receipt"],
        children: false,
      },

      
    ];

    let financesubchildrenarr = [
      ...financesubchildrenarr1,
      ...financesubchildrenarr2,
      ...financesubchildrenarr3,
      ...financesubchildrenarr4
    ];

    let settingsubchildrenarr1 = [
      // Charter Party Term
      {
        title: "Charter Party Forms",
        label: "Charter Party Forms",
        link: ["#"],
        children: false,
      },

      {
        title: "Freight Codes",
        label: "Freight Codes",
        link: ["#/data-center/charter-party-terms/freight-codes"],
        children: false,
      },

      {
        title: "Laytime Types",
        label: "Laytime Types",
        link: ["#/data-center/charter-party-terms/laytime-types"],
        children: false,
      },

      {
        title: "Loading Costs",
        label: "Loading Costs",
        link: ["#/data-center/charter-party-terms/loading-costs"],
        children: false,
      },

      {
        title: "Laytime To Commence",
        label: "Laytime To Commence",
        link: ["#/data-center/charter-party-terms/laytime-commence"],
        children: false,
      },

      {
        title: "NOR To Tender",
        label: "NOR To Tender",
        link: ["#/data-center/charter-party-terms/nor-tender"],
        children: false,
      },

      {
        title: "Normal Off/Hrs",
        label: "Normal Off/Hrs",
        link: ["#/data-center/charter-party-terms/normal-off"],
        children: false,
      },

      {
        title: "Other Loading Terms",
        label: "Other Loading Terms",
        link: ["#/data-center/charter-party-terms/other-loading-terms"],
        children: false,
      },

      {
        title: "Shifting Terms",
        label: "Shifting Terms",
        link: ["#/data-center/charter-party-terms/shifting-terms"],
        children: false,
      },

      {
        title: "Loading/Discharging Terms",
        label: "Loading/Discharging ",
        link: ["#/data-center/charter-party-terms/shinc-terms"],
        children: false,
      },

      {
        title: "Time To Tender",
        label: "Time To Tender",
        link: ["#/data-center/charter-party-terms/time-tender"],
        children: false,
      },

      {
        title: "Time Used",
        label: "Time Used",
        link: ["#/data-center/charter-party-terms/time-used"],
        children: false,
      },

      {
        title: "Working Days",
        label: "Working Days",
        link: ["#/data-center/charter-party-terms/working-days"],
        children: false,
      },
    ];

    let settingsubchildrenarr2 = [
      //Vessel Term
      {
        title: "Fuel Consumption Categories",
        label: "Fuel Consumption Categories",
        link: ["#/data-center/vessels/fuel-consumption-categories"],
        children: false,
      },

      {
        title: "Fuel Grade",
        label: "Fuel Grade",
        link: ["#/data-center/vessels/fuel-grades"],
        children: false,
      },

      {
        title: "Fuel Types",
        label: "Fuel Types",
        link: ["#/data-center/vessels/fuel-types"],
        children: false,
      },
    ];

    let settingsubchildrenarr3 = [
      //Weather & Delay Terms
      {
        title: "Beaufort Scale",
        label: "Beaufort Scale",
        link: ["#/data-center/delay-weather/beaufort-scale"],
        children: false,
      },

      {
        title: "Sea States",
        label: "Sea States",
        link: ["#/data-center/delay-weather/sea-states"],
        children: false,
      },

      {
        title: "Swell States",
        label: "Swell States",
        link: ["#/data-center/delay-weather/swell-states"],
        children: false,
      },

      {
        title: "Delay Reasons",
        label: "Delay Reasons",
        link: ["#/data-center/delay-weather/delay-reasons"],
        children: false,
      },

      {
        title: "Delay Type",
        label: "Delay Type",
        link: ["#/data-center/delay-weather/delay-types"],
        children: false,
      },
    ];

    let settingsubchildrenarr4 = [
      // Port Expenses Category/List
      {
        title: "Port Instruction Set",
        label: "Port Instruction Set",
        link: ["#/instruction-set"],
        children: false,
      },

      {
        title: "Port Information",
        label: "Port Information",
        link: ["#/port-information"],
        children: false,
      },
    ];

    let settingsubchildrenarr5 = [
      // Profile Setup
      {
        title: "Registration",
        label: "Registration",
        link: ["#/user-list"],
        children: false,
      },

      {
        title: "Registration",
        label: "Role",
        link: ["#/access-right"],
        children: false,
      },

      {
        title: "Register Company",
        label: "Register Company",
        link: ["#/organization"],
        children: false,
      },

      {
        title: "Register Legal Identity",
        label: "Register Legal Identity",
        link: ["#/organization"],
        children: false,
      },
    ];

    let settingsubchildrenarr6 = [
      // Other Terms
      {
        title: "Departments/Terms",
        label: "Departments/Terms",
        link: ["#/data-center/other/departments-teams"],
        children: false,
      },
      {
        title: "Trade Areas",
        label: "Trade Areas",
        link: ["#/data-center/other/trade-areas"],
        children: false,
      },
      {
        title: "Operations Ledger",
        label: "Operations Ledger",
        link: ["#/data-center/other/operations-ledger"],
        children: false,
      },
      {
        title: "Units Of Measures",
        label: "Units Of Measures",
        link: ["#/data-center/other/units-measure"],
        children: false,
      },
      {
        title: "Port Activities Terms",
        label: "Port Activities Terms",
        link: ["#/data-center/other/port-activities-term"],
        children: false,
      },
      {
        title: "Port Functions",
        label: "Port Functions",
        link: ["#/data-center/other/port-functions"],
        children: false,
      },
      {
        title: "OPA Rates",
        label: "OPA Rates",
        link: ["#/data-center/other/opa-rates"],
        children: false,
      },
      {
        title: "Cargo Groups",
        label: "Cargo Groups",
        link: ["#/data-center/other/cargo-groups"],
        children: false,
      },
      {
        title: "Fuel Zone",
        label: "Fuel Zone",
        link: ["#/data-center/other/fuel-zone"],
        children: false,
      },
    ];

    let settingsubchildrenarr7 = [
      // Currency Terms
      {
        title: "Currency Types",
        label: "Currency Types",
        link: ["#/data-center/currencies/currency-types"],
        children: false,
      },
      {
        title: "Exchange Rates",
        label: "Exchange Rates",
        link: ["#/data-center/currencies/exchange-rates"],
        children: false,
      },
    ];

    let settingsubchildrenarr8 = [
      // Accounting Terms
      {
        title: "Business Rule",
        label: "Business rule",
        link: ["#/business-rule-accounting"],
        children: false,
      },
      {
        title: "Accounting Rule",
        label: "Accounting rule",
        link: [""],
        children: false,
      },

      {
        title: "Bank Names",
        label: "Bank Names",
        link: ["#/data-center/other/bank-names"],
        children: false,
      },
    ];

    let settingsubchildrenarr9 = [
      // Payment & Receipt
      {
        title: "Subscription Plan",
        label: "Subscription Plan",
        link: ["#/data-center/payments/subs"],
        children: false,
      },
    ];

    let settingsubchildrenarr10 = [
      // Port Details
      {
        title: "Port List",
        label: "Port List",
        link: ["#/ports-list"],
        children: false,
      },
    ];

    let settingsubchildrenarr11 = [
      // Country Details
      {
        title: "Country List",
        label: "Country List",
        link: ["#/country-list"],
        children: false,
      },
    ];

    let settingsubchildrenarr12 = [
      // cargo details
      {
        title: "New Cargo - List",
        label: "New Cargo - List",
        link: ["#/add-cargo", "#/cargos"],
        children: true,
      },

      {
        title: "Cargo-Vessel Match",
        label: "Cargo-Vessel Match",
        link: ["#/cargo-tonnage"],
        children: false,
      },

      {
        title: "CP Desk",
        label: "CP Desk",
        link: ["#/cpdesk"],
        children: false,
      },

      {
        title: "Smart Mail",
        label: "Smart Mail",
        link: ["#/smart-mail"],
        children: false,
      },
    ];

    let settingsubchildrenarr = [
      ...settingsubchildrenarr1,
      ...settingsubchildrenarr2,
      ...settingsubchildrenarr3,
      ...settingsubchildrenarr4,
      ...settingsubchildrenarr5,
      ...settingsubchildrenarr6,
      ...settingsubchildrenarr7,
      ...settingsubchildrenarr8,
      ...settingsubchildrenarr9,
      ...settingsubchildrenarr10,
      ...settingsubchildrenarr11,
      ...settingsubchildrenarr12,
    ];

    let oceanZerosubChildrenarr1 = [
      {
        title: "Archives List",
        label: "Archives List",
        link: ["#"],
        children: false,
      },

      {
        title: "Add Vessel list",
        label: "Add Vessel list",
        link: ["#"],
        children: false,
      },

      {
        title: "new voyage estimate-list",
        label: "new voyage estimate-list",
        link: ["#"],
        children: false,
      },

      {
        title: "Noon Verification List",
        label: "Noon Verification List",
        link: ["#/noon-verification"],
        children: false,
      },
    ];

    let oceanZerosubChildrenarr2 = [
      {
        title: "Activated/Deactivate Vessel",
        label: "Activated/Deactivate Vessel",
        link: ["#"],
        children: false,
      },

      {
        title: "Vessel optimisation matrix",
        label: "Vessel optimisation matrix",
        link: ["#"],
        children: false,
      },

      {
        title: "Track fleet By Noon Rpt",
        label: "Track fleet By Noon Rpt",
        link: ["#/dynamic-vspm"],
        children: false,
      },

      {
        title: "Voyage optimization",
        label: "Voyage optimization",
        link: ["#/voyage-optimization"],
        children: false,
      },
    ];

    let oceanZerosubChildrenarr3 = [
      {
        title: "MRV Report",
        label: "MRV Report",
        link: ["#"],
        children: false,
      },

      {
        title: "Annual Flag State Report",
        label: "Annual Flag State Report",
        link: ["#"],
        children: false,
      },

      {
        title: "EIA Report",
        label: "EIA Report",
        link: ["#"],
        children: false,
      },

      {
        title: "GHG Report",
        label: "GHG Report",
        link: ["#"],
        children: false,
      },

      {
        title: "IMO DCS Report",
        label: "IMO DCS Report",
        link: ["#"],
        children: false,
      },

      {
        title: "EPI Report",
        label: "EPI Report",
        link: ["#"],
        children: false,
      },

      {
        title: "EEXI Report",
        label: "EEXI Report",
        link: ["#"],
        children: false,
      },
    ];

    let oceanZerosubChildrenarr = [
      ...oceanZerosubChildrenarr1,
      ...oceanZerosubChildrenarr2,
      ...oceanZerosubChildrenarr3,
    ];

    let oceanMailsubchildrenarr1 = [
      {
        title: "Order list",
        label: "Order list",
        link: ["#"],
        children: false,
      },

      {
        title: "Tonnage List",
        label: "Tonnage List",
        link: ["#"],
        children: false,
      },

      {
        title: "Matching",
        label: "Matching",
        link: ["#"],
        children: false,
      },

      {
        title: "Map",
        label: "Map",
        link: ["#"],
        children: false,
      },
    ];

    let oceanMailsubchildrenarr2 = [
      {
        title: "Skype",
        label: "Skype",
        link: ["#"],
        children: false,
      },
    ];

    let oceanMailsubChildren = [
      ...oceanMailsubchildrenarr1,
      ...oceanMailsubchildrenarr2,
    ];

    const linkClicked = (userInput) => {
      window.clickedTab = userInput;
      localStorage.setItem("lastClickedItem", userInput);
      // console.log(userInput,"fdsfdf");
    };

    return (
      <>
        <div className="side-nav">
          {/* <ul className="list-unstyled components">
            {usertype == 6 ? (
              <li>
                <a href="#/access-control">Access Control</a>
              </li>
            ) : undefined}
          </ul> */}

          {/*  chartering children   */}
          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showCharteringChildren: !this.state.showCharteringChildren,
              })
            }
          >
            {this.menuHasAccess("Chartering") ? (
              <li className="bg-theme-menu">
                <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                  Chartering
                  <span className="float-right">
                    {this.state.showCharteringChildren ? (
                      <DownOutlined />
                    ) : (
                      <UpOutlined />
                    )}
                  </span>
                </b>
                {charteringchildrenarr.some((item) =>
                  this.menuHasAccess(item.title)
                ) ? (
                  <div
                    className="custom-drop"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <div className="cardbackground">
                      <div className="card-group">
                        {charteringsubchildren1.some((item, index) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">
                              Trade Intelligence
                            </div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {charteringsubchildren1.map((el) => {
                                  // if (this.menuHasAccess(el.title)) {
                                  //   let lablearr = el.label.split("-");
                                  //   return (
                                  //     <li>
                                  //       {lablearr.map((label, index) => (
                                  //         <a href={el.link[index]}>{label}</a>
                                  //       ))}
                                  //     </li>
                                  //   );
                                  // }

                                  let lablearr = el.label.split("-");
                                  return (
                                    <ListItem
                                      listItem={lablearr}
                                      el={el}
                                      label={el.label}
                                      permissions={this.state.permissionList}
                                    />
                                  );
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {charteringsubchildren2.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Cargo Trade</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {charteringsubchildren2.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}
                        {/*{charteringsubchildren3.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Time Charter</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {charteringsubchildren3.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined} */}

                        {charteringsubchildren4.some((item) =>{
                           return this.menuHasAccess(item.title)
                        }
                        ) ? (
                          <div className="card">
                            <div className="card-header">Analytics</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {charteringsubchildren4.map((el) => {
                                 
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}
                      </div>
                    </div>
                  </div>
                ) : undefined}
              </li>
            ) : undefined}
          </ul>

          {this.state.showCharteringChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {charteringchildren.map(
                    ({ title, label, link, children }) => {
                      if (this.menuHasAccess(title)) {
                        let lablearr = label.split("-");
                        return (
                          <ListItem
                            listItem={lablearr}
                            label={label}
                            isSubChild={true}
                            link={link}
                            children={children}
                            permissions={this.state.permissionList}
                          />
                          // <li
                          //   onClick={() => {
                          //     linkClicked(label);
                          //   }}
                          // >
                          //   <a href={link[0]}>{lablearr[0]}</a>
                          //   {children && <a href={link[1]}>-{lablearr[1]}</a>}
                          // </li>
                        );
                      }
                    }
                  )}
                </ul>
              </div>
            </div>
          )}

          {/*  operation children   */}

          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showoperationChildren: !this.state.showoperationChildren,
              })
            }
          >
            {this.menuHasAccess("Operation") ? (
              <li className="bg-theme-menu operation-side-nav">
                <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                  Operation
                  <span className="float-right">
                    {this.state.showoperationChildren ? (
                      <DownOutlined />
                    ) : (
                      <UpOutlined />
                    )}
                  </span>
                </b>
                {operationsubchildrenarr.some((item) =>
                  this.menuHasAccess(item.title)
                ) ? (
                  <div
                    className="custom-drop"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <div className="row">
                      <div className="col-12">
                        <div className="card-group">
                          {operationsubchildren1.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">Bunker</div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren1.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {operationsubchildren2.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">Port Call</div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren2.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {operationsubchildren3.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">Claim</div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren3.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {operationsubchildren4.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">Hire Payment</div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren4.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-12">
                        <div className="card-group">
                          {operationsubchildren5.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">
                                Other Vendor Payment
                              </div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren5.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {operationsubchildren6.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">
                                Time Charter manager
                              </div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren6.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {operationsubchildren7.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">
                                Freight & Commission
                              </div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren7.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined}

                          {/* {operationsubchildren8.some((item) =>
                            this.menuHasAccess(item.title)
                          ) ? (
                            <div className="card">
                              <div className="card-header">
                                Report & Analytics
                              </div>
                              <div className="card-body">
                                <ul className="list-unstyled ">
                                  {operationsubchildren8.map((el) => {
                                    if (this.menuHasAccess(el.title)) {
                                      let lablearr = el.label.split("-");
                                      return (
                                        <ListItem
                                          listItem={lablearr}
                                          el={el}
                                          label={el.label}
                                          permissions={
                                            this.state.permissionList
                                          }
                                        />
                                      );
                                    }
                                  })}
                                </ul>
                              </div>
                            </div>
                          ) : undefined} */}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : undefined}
              </li>
            ) : undefined}
          </ul>

          {this.state.showoperationChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {operationchildren.map(({ title, label, link, children }) => {
                    // if (this.menuHasAccess(title)) {
                    //   let lablearr = label.split("-");
                    //   return (
                    //     <li>
                    //       <a href={link[0]}>{lablearr[0]}</a>
                    //       {children && <a href={link[1]}>-{lablearr[1]}</a>}
                    //     </li>
                    //   );
                    // }
                    // for access control
                    let lablearr = label.split("-");
                    return (
                      <ListItem
                        listItem={lablearr}
                        label={label}
                        isSubChild={true}
                        link={link}
                        children={children}
                        permissions={this.state.permissionList}
                      />
                      //   <li
                      //     onClick={() => {
                      //       linkClicked(label);
                      //     }}
                      //   >
                      //     <a href={link[0]}>{lablearr[0]}</a>
                      //     {children && <a href={link[1]}>-{lablearr[1]}</a>}
                      //   </li>
                      // );
                    );
                  })}
                </ul>
              </div>
            </div>
          )}

          {/* finance children */}

          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showfinanceChildren: !this.state.showfinanceChildren,
              })
            }
          >
            {this.menuHasAccess("Finance") ? (
              <li className="bg-theme-menu">
                <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                  Finance
                  <span className="float-right">
                    {this.state.showfinanceChildren ? (
                      <DownOutlined />
                    ) : (
                      <UpOutlined />
                    )}
                  </span>
                </b>
                {financesubchildrenarr.some((item) =>
                  this.menuHasAccess(item.title)
                ) ? (
                  <div
                    className="custom-drop"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <div className="cardbackground">
                      <div className="card-group">
                        {financesubchildrenarr1.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">
                              Transaction Data Entry
                            </div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {financesubchildrenarr1.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {financesubchildrenarr2.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">List</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {financesubchildrenarr2.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {financesubchildrenarr3.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Commission List</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {financesubchildrenarr3.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}



{financesubchildrenarr4.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Advance Payment</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {financesubchildrenarr4.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

















                      </div>
                    </div>
                  </div>
                ) : undefined}
              </li>
            ) : undefined}
          </ul>
          {this.state.showfinanceChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {financechildren.map(({ title, label, link, children }) => {
                    if (this.menuHasAccess(title)) {
                      let lablearr = label.split("-");
                      return (
                        <ListItem
                          listItem={lablearr}
                          label={label}
                          isSubChild={true}
                          link={link}
                          children={children}
                          permissions={this.state.permissionList}
                        />
                        // <li
                        //   onClick={() => {
                        //     linkClicked(label);
                        //   }}
                        // >
                        //   <a href={link[0]}>{lablearr[0]}</a>
                        //   {children && <a href={link[1]}>-{lablearr[1]}</a>}
                        // </li>
                      );
                    }
                  })}
                </ul>
              </div>
            </div>
          )}

          {/* { setting children} */}

          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showsettingChildren: !this.state.showsettingChildren,
              })
            }
          >
            {this.menuHasAccess("Setting TERM") ? (
              <li className="bg-theme-menu setting-side-nav">
                <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                  Setting
                  <span className="float-right">
                    {this.state.showsettingChildren ? (
                      <DownOutlined />
                    ) : (
                      <UpOutlined />
                    )}
                  </span>
                </b>

                {settingsubchildrenarr.some((item) =>
                  this.menuHasAccess(item.title)
                ) ? (
                  <div
                    className="custom-drop"
                    onClick={(e) => e.stopPropagation()}
                  >
                    {/*  extra setting terms  */}
                    <div className="card-group">
                      <div className="card">
                        {/* first group  only one */}
                        {settingsubchildrenarr1.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">
                              Charter party term
                            </div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr1.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}
                      </div>

                      <div className="card">
                        {settingsubchildrenarr2.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Vessel Term</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr2.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {settingsubchildrenarr6.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Other Terms</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr6.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}
                      </div>

                      <div className="card">
                        {/* third group 3 elements */}
                        {settingsubchildrenarr3.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <>
                            <div className="card-header">
                              Weather & Delay Terms
                            </div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr3.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </>
                        ) : undefined}

                        {settingsubchildrenarr7.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <>
                            <div className="card-header">Currency Terms</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr7.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </>
                        ) : undefined}

                        {settingsubchildrenarr8.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <>
                            <div className="card-header">Accounting Terms</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr8.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </>
                        ) : undefined}
                      </div>

                      <div className="card">
                        {/*  four group  */}
                        {settingsubchildrenarr4.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <>
                            <div className="card-header">
                              Port Expenses Category/List
                            </div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr4.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </>
                        ) : undefined}

                        {settingsubchildrenarr9.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Payment & Receipt</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr9.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {settingsubchildrenarr10.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Port Details</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr10.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {settingsubchildrenarr11.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Country Details</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr11.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}

                        {settingsubchildrenarr12.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <div className="card">
                            <div className="card-header">Cargo Details</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr12.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </div>
                        ) : undefined}
                      </div>

                      <div className="card">
                        {settingsubchildrenarr5.some((item) =>
                          this.menuHasAccess(item.title)
                        ) ? (
                          <>
                            <div className="card-header">Profile Setup</div>
                            <div className="card-body">
                              <ul className="list-unstyled ">
                                {settingsubchildrenarr5.map((el) => {
                                  if (this.menuHasAccess(el.title)) {
                                    let lablearr = el.label.split("-");
                                    return (
                                      <ListItem
                                        listItem={lablearr}
                                        el={el}
                                        label={el.label}
                                        permissions={this.state.permissionList}
                                      />
                                    );
                                  }
                                })}
                              </ul>
                            </div>
                          </>
                        ) : undefined}
                      </div>
                    </div>
                  </div>
                ) : undefined}
              </li>
            ) : undefined}
          </ul>
          {this.state.showsettingChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {settingChildren.map(({ title, label, link, children }) => {
                    /*
                    if (this.menuHasAccess(title)) {
                      let lablearr = label.split("-");
                      return (
                        <li>
                          <a href={link[0]}>{lablearr[0]}</a>
                          {children && <a href={link[1]}>-{lablearr[1]}</a>}
                        </li>
                      );
                    }
                   after adding these menu in access control table, then this if block will open. till then
*/

                    let lablearr = label.split("-");
                    return (
                      <ListItem
                        listItem={lablearr}
                        label={label}
                        isSubChild={true}
                        link={link}
                        children={children}
                        permissions={this.state.permissionList}
                      />
                      // <li
                      //   onClick={() => {
                      //     linkClicked(label);
                      //   }}
                      // >
                      //   <a href={link[0]}>{lablearr[0]}</a>
                      //   {children && <a href={link[1]}>-{lablearr[1]}</a>}
                      // </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          )}

          {/*       Oceann Zero    children    */}

          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showoceannzeroChildren: !this.state.showoceannzeroChildren,
              })
            }
          >
            <li className="bg-theme-menu">
              <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                Oceann Zero
                <span className="float-right">
                  {this.state.showoceannzeroChildren ? (
                    <DownOutlined />
                  ) : (
                    <UpOutlined />
                  )}
                </span>
              </b>

              <div
                className="custom-drop ocean-zero-side-nav"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="cardbackground">
                  <div className="card-group">
                    <div className="card">
                      <div className="card-header">List</div>
                      <div className="card-body">
                        <ul className="list-unstyled ">
                          {oceanZerosubChildrenarr1.map((el) => {
                            let lablearr = el.label.split("-");
                            return (
                              <ListItem
                                listItem={lablearr}
                                el={el}
                                label={el.label}
                                permissions={this.state.permissionList}
                              />
                            );
                          })}
                        </ul>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-header">Performance</div>
                      <div className="card-body">
                        <ul className="list-unstyled ">
                          {oceanZerosubChildrenarr2.map((el) => {
                            let lablearr = el.label.split("-");
                            return (
                              <ListItem
                                listItem={lablearr}
                                el={el}
                                label={el.label}
                                permissions={this.state.permissionList}
                              />
                            );
                          })}
                        </ul>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-header">Time Charter</div>
                      <div className="card-body">
                        <ul className="list-unstyled ">
                          {oceanZerosubChildrenarr3.map((el) => {
                            let lablearr = el.label.split("-");
                            return (
                              <ListItem
                                listItem={lablearr}
                                el={el}
                                label={el.label}
                                permissions={this.state.permissionList}
                              />
                            );
                          })}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>

          {this.state.showoceannzeroChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {oceanzeroChildren.map(({ title, label, link, children }) => {
                    /*
                    if (this.menuHasAccess(title)) {
                      let lablearr = label.split("-");
                      return (
                        <li>
                          <a href={link[0]}>{lablearr[0]}</a>
                          {children && <a href={link[1]}>-{lablearr[1]}</a>}
                        </li>
                      );
                    }
                     after adding these menu in access control table, then this if block will open. till then
                    */
                    let lablearr = label.split("-");
                    return (
                      <li
                        onClick={() => {
                          linkClicked(label);
                        }}
                      >
                        <a href={link[0]}>{lablearr[0]}</a>
                        {children && <a href={link[1]}>-{lablearr[1]}</a>}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          )}

          {/* oceann mail children  */}

          <ul
            className="list-unstyled components"
            onClick={() =>
              this.setState({
                ...this.state,
                showoceannMailChildren: !this.state.showoceannMailChildren,
              })
            }
          >
            <li className="bg-theme-menu">
              <b style={{ fontWeight: "normal", padding: "5px 20px" }}>
                Oceann Mail
                <span className="float-right">
                  {this.state.showoceannMailChildren ? (
                    <DownOutlined />
                  ) : (
                    <UpOutlined />
                  )}
                </span>
              </b>

              <div
                className="custom-drop oceanMail"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="cardbackground">
                  <div className="card-group">
                    <div className="card">
                      <div className="card-header">Market Trend</div>
                      <div className="card-body">
                        <ul className="list-unstyled ">
                          {oceanMailsubchildrenarr1.map((el) => {
                            let lablearr = el.label.split("-");
                            return (
                              <ListItem
                                listItem={lablearr}
                                el={el}
                                label={el.label}
                                permissions={this.state.permissionList}
                              />
                            );
                          })}
                        </ul>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-header">Chat</div>
                      <div className="card-body">
                        <ul className="list-unstyled ">
                          {oceanMailsubchildrenarr2.map((el) => {
                            let lablearr = el.label.split("-");
                            return (
                              <ListItem
                                listItem={lablearr}
                                el={el}
                                label={el.label}
                                permissions={this.state.permissionList}
                              />
                            );
                          })}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>

          {this.state.showoceannMailChildren && (
            <div className="roushancard">
              <div className="card-body">
                <ul className="list-unstyled ">
                  {oceanMailChildren.map(({ title, label, link, children }) => {
                    /*
                    if (this.menuHasAccess(title)) {
                      let lablearr = label.split("-");
                      return (
                        <li>
                          <a href={link[0]}>{lablearr[0]}</a>
                          {children && <a href={link[1]}>-{lablearr[1]}</a>}
                        </li>
                      );
                    }
                    after adding these menu in access control table, then this if block will open. till then

*/

                    let lablearr = label.split("-");
                    return (
                      <li
                        onClick={() => {
                          linkClicked(label);
                        }}
                      >
                        <a href={link[0]}>{lablearr[0]}</a>
                        {children && <a href={link[1]}>-{lablearr[1]}</a>}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    collapsedNav: state.settings.collapsedNav,
    colorOption: state.settings.colorOption,
    location: state.routing.location,
  };
};

const mapDispatchToProps = (dispatch) => ({
  handleToggleOffCanvasMobileNav: (isOffCanvasMobileNav) => {
    dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppMenu);
