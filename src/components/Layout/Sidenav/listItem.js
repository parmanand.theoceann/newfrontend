import { useEffect, useState } from "react";
import styles from "../../../styles/menu.module.css";
import { LockFilled } from "@ant-design/icons";
import { postAPICall } from "../../../shared";

const ListItem = (props) => {
 
  const { listItem, label, el, isSubChild, link, children,permissions, toggleChartering} = props;  
  const [label1, setLable] = useState("");
  const [listItem1, setListItem] = useState([]);
  const [el1, setEl] = useState();
  const [accessList, setAccessList] = useState([]);
  const [isHovered, setIsHovered] = useState(false);
  const [isSubChild1, setIsSubchild] = useState();
  const [children1, setChildren] = useState();
  const [link1, setLink] = useState();
  const [isAdmin,setIsAdmin]=useState(false);

   useEffect(() => {
    setListItem(listItem);
    setLable(label);
    setEl(el);
    setPermissions();
    setIsSubchild(isSubChild);
    setChildren(children);
    setLink(link);
     // Replace with your new access lis);
  }, []);

  const setPermissions=()=>{
    const accessListData = JSON.parse(
      localStorage.getItem("permissionList")
    );

      // console.log(accessListData,"*****");
      if(!accessListData){
        setIsAdmin(true)
      }
      else{
        setIsAdmin(false)
        setAccessList(accessListData);
      }
  }

  

  const linkClicked = (e,userInput) => {
    //e.stopPropagation();   
    window.clickedTab = userInput;
    localStorage.setItem("lastClickedItem", userInput);
  };  

  const handleMouseEnter = (howerIndex) => {
    setIsHovered(true);
  };

  const handleMouseLeave = (hoverIndex) => {
    setIsHovered(false);
  };

  return (
    <>
      {!isSubChild1 ? (
        <li
          onClick={(e) => {
            linkClicked(e,label1);
          }}
          onMouseEnter={() => {
            handleMouseEnter();
          }}
          onMouseLeave={() => {
            handleMouseLeave();
          }}
          
        >
          {listItem1.map((label, index) => (
            <div key={index}>
              <a href={el1.link[index]}>{label}</a>
              {/* {!accessList?.includes(el1.label) &&
                isHovered &&
                index === listItem1.length - 1 && !isAdmin &&(
                  <div
                    style={{
                      width: label.length * 10 >= 100 ? label.length * 10 : 100,
                    }}
                    className={styles.accessPopup}
                  >
                    No access
                  </div>
                )} */}
              {!accessList?.includes(el1.label) &&
                index === listItem1.length - 1 && !isAdmin && (
                  <div className={styles.lock}>
                    <LockFilled />
                  </div>
                )}
            </div>
          ))}
        </li>
      ) : (
        <div>
          <li
            onClick={(e) => {
              linkClicked(e,label);
            }}
            onMouseEnter={() => {
              handleMouseEnter();
            }}
            onMouseLeave={() => {
              handleMouseLeave();
            }}
          >
            <a href={link[0]}>{listItem1[0]}</a>
            {listItem1[1]&&<a href={link[1]}>-{listItem1[1]}</a>}
            {/* <p style={{color:'red'}}>{listItem1[1]}</p> */}
            {/* {!accessList?.includes(`${listItem1[0]}-${listItem1[1]}`) &&
              isHovered && !isAdmin&&(
                <div
                  style={{
                    width: "90%",
                  }}
                  className={styles.accessPopup}
                >
                  No access
                </div>
              )} */}
            {!accessList?.includes(`${listItem1[0]}-${listItem1[1]}`) && !isAdmin && (
              <div className={props.isSubChild1?styles.lockSubchild: styles.lock}>
                <LockFilled />
              </div>
            )}
          </li>
        </div>
      )}
    </>
  );
};

export default ListItem;
