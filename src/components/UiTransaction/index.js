import React from "react";
import {
  Table,
  Input,
  Row,
  Col,
  Button,
  Tabs,
  Form,
  DatePicker,
  Menu,
  Dropdown,
  Popconfirm,
  Checkbox,
} from "antd";
import {
  SaveOutlined,
  CheckOutlined,
  EditOutlined,
  FileExcelOutlined,
  FolderOpenOutlined,
  BookOutlined,
  PictureOutlined,
  RetweetOutlined,
  IdcardOutlined,
  DownOutlined,
  CopyrightOutlined,
} from "@ant-design/icons";
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const InputGroup = Input.Group;

const menu = (
  <Menu>
    <Menu.Item>
      <span>1st menu item</span>
    </Menu.Item>
  </Menu>
);
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const dataSource = [];

const columns = [
  {
    title: "Bank Code",
    dataIndex: "bank_code",
    width: "10%",
  },
  {
    title: "Approval",
    dataIndex: "approval",
    width: "10%",
  },
  {
    title: "Date Paid",
    dataIndex: "date_paid",
    width: "10%",
  },
  {
    title: "By",
    dataIndex: "by",
    width: "10%",
  },
  {
    title: "Check/WT No.",
    dataIndex: "wt_nu",
    width: "10%",
  },
  {
    title: "Amount Paid",
    dataIndex: "amount_paid",
    width: "10%",
  },
  {
    title: "Base Amount",
    dataIndex: "base_amount",
    width: "10%",
  },
  {
    title: "Pay Trans No.",
    dataIndex: "trans_num",
    width: "10%",
  },
  {
    title: "Memo",
    dataIndex: "memo",
    width: "10%",
  },
];

class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    editable: false,
  };
  handleChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  };
  check = () => {
    this.setState({ editable: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  };
  edit = () => {
    this.setState({ editable: true });
  };
  render() {
    const { value, editable } = this.state;
    return (
      <div className="editable-cell">
        {editable ? (
          <Input
            value={value}
            onChange={this.handleChange}
            onPressEnter={this.check}
            suffix={
              <CheckOutlined
                className="editable-cell-icon-check"
                onClick={this.check}
              />
            }
          />
        ) : (
          <div style={{ paddingRight: 24 }}>
            {value || " "}
            <EditOutlined className="editable-cell-icon" onClick={this.edit} />
          </div>
        )}
      </div>
    );
  }
}

class UiTransaction extends React.Component {
  constructor() {
    super();
    this.columns1 = [
      {
        title: "Comp",
        dataIndex: "comp",
        width: "5%",
        render: (text, record) => (
          <EditableCell
            value={text}
            onChange={this.onCellChange(record.key, "comp")}
          />
        ),
      },
      {
        title: "LOB",
        dataIndex: "lob",
        width: "5%",
      },
      {
        title: "Vessel",
        dataIndex: "vessel",
        width: "5%",
      },
      {
        title: "Vessel Name",
        dataIndex: "vessel_name",
        width: "8%",
      },
      {
        title: "Acount",
        dataIndex: "account",
        width: "5%",
      },
      {
        title: "AP/AR Acct",
        dataIndex: "ap_ar",
        width: "8%",
      },
      {
        title: "Voy",
        dataIndex: "voy",
        width: "8%",
      },
      {
        title: "Dept",
        dataIndex: "dept",
        width: "8%",
      },
      {
        title: "Port",
        dataIndex: "port",
        width: "8%",
      },
      {
        title: "IC",
        dataIndex: "ic",
        width: "8%",
      },
      {
        title: "Description",
        dataIndex: "description",
        width: "15%",
      },
      {
        title: "Amount",
        dataIndex: "amount",
        width: "8%",
      },
      {
        title: "Base Equiv",
        dataIndex: "base_equiv",
        width: "8%",
      },
      {
        title: "Actions",
        dataIndex: "action",
        render: (text, record) => {
          return this.state.dataSource1.length > 0 ? (
            <div>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.onDelete(record.key)}
              >
                <Button type="link">Delete</Button>
              </Popconfirm>
            </div>
          ) : null;
        },
      },
    ];
    this.state = {
      dataSource1: [],
      count: 1,
    };
  }

  edit = () => {
    this.setState({ editable: true });
  };

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource1 = [...this.state.dataSource1];
      const target = dataSource1.find((item) => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource1 });
      }
    };
  };

  onDelete = (key) => {
    const dataSource1 = [...this.state.dataSource1];
    this.setState({
      dataSource1: dataSource1.filter((item) => item.key !== key),
    });
  };

  handleAdd = () => {
    const { count, dataSource1 } = this.state;
    const newData = {
      key: count,
    };
    this.setState({
      dataSource1: [...dataSource1, newData],
      count: count + 1,
    });
  };

  render() {
    const { dataSource1 } = this.state;
    const columns1 = this.columns1;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Row>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <FileExcelOutlined />
                      </li>
                    </ul>
                  </span>
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <FolderOpenOutlined />
                      </li>
                    </ul>
                  </span>
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <SaveOutlined />

                        <span className="text-bt">Save and Post</span>
                      </li>
                      <li>
                        <BookOutlined />
                        <span className="text-bt">Pay current Invoice</span>
                      </li>
                    </ul>
                  </span>
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <PictureOutlined />
                        <span className="text-bt">Attachments</span>
                      </li>
                      <li>
                        <RetweetOutlined />
                        <span className="text-bt">Reverse</span>
                      </li>
                      <li>
                        <IdcardOutlined />
                        <Dropdown overlay={menu}>
                          <span className="text-bt">
                            Reports <DownOutlined />
                          </span>
                        </Dropdown>
                      </li>
                    </ul>
                  </span>
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <CopyrightOutlined />
                        <span className="text-bt">Base Currency</span>
                      </li>
                    </ul>
                  </span>
                </Col>
              </Row>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <Row gutter={16}>
                  <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <FormItem label="Invoice AP/AR" {...formItemLayout}>
                      <Row gutter={16}>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                          <Input
                            placeholder="Invoice AP/AR"
                            defaultValue="Payable"
                          />
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                          <label>Status</label>
                        </Col>
                        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                          <Input
                            placeholder="Status"
                            defaultValue="Payable"
                            disabled
                          />
                        </Col>
                      </Row>
                    </FormItem>
                    <FormItem label="Accounting Date" {...formItemLayout}>
                      <DatePicker />
                    </FormItem>
                    <FormItem label="Vendor" {...formItemLayout}>
                      <Input
                        placeholder="Vendor"
                        defaultValue="FAB(first abu dhabhi)"
                      />
                    </FormItem>
                    <FormItem label="Bill Via" {...formItemLayout}>
                      <Input placeholder="Bill Via" defaultValue="" />
                    </FormItem>
                    <FormItem label="Invoice Date" {...formItemLayout}>
                      <Row gutter={16}>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                          <DatePicker />
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
                          <label>Due Date</label>
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
                          <DatePicker />
                        </Col>
                      </Row>
                    </FormItem>
                    <FormItem label="Late Reason" {...formItemLayout}>
                      <Input placeholder="Late Reason" defaultValue="" />
                    </FormItem>
                    <FormItem label="Invoice No." {...formItemLayout}>
                      <Row gutter={16}>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                          <Input placeholder="Invoice No." defaultValue="" />
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                          <label>Terms</label>
                        </Col>
                        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                          <Input placeholder="Terms" defaultValue="" />
                        </Col>
                      </Row>
                    </FormItem>
                    <FormItem label="Terms Description" {...formItemLayout}>
                      <Input placeholder="Terms Description" defaultValue="" />
                    </FormItem>
                    <FormItem label="Note" {...formItemLayout}>
                      <Input placeholder="Note" defaultValue="" />
                    </FormItem>
                    <FormItem label="Rebill Type" {...formItemLayout}>
                      <Row gutter={16}>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                          <Input
                            placeholder="Rebill Type"
                            defaultValue="non-reabill"
                          />
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                          <label>Contact</label>
                        </Col>
                        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                          <Input placeholder="Contact" defaultValue="" />
                        </Col>
                      </Row>
                    </FormItem>
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <FormItem label="Trans No." {...formItemLayout}>
                      <Input placeholder="Trans No." defaultValue="" disabled />
                    </FormItem>
                    <FormItem label="Invoice Type" {...formItemLayout}>
                      <InputGroup compact>
                        <Input
                          style={{ width: "70%" }}
                          placeholder=""
                          defaultValue=""
                        />
                        <Input
                          style={{ width: "30%" }}
                          placeholder=""
                          defaultValue=""
                          disabled
                        />
                      </InputGroup>
                    </FormItem>
                    <FormItem label="Invoice Amount" {...formItemLayout}>
                      <InputGroup compact>
                        <Input
                          style={{ width: "70%" }}
                          placeholder=""
                          defaultValue="0.0"
                        />
                        <Input
                          style={{ width: "30%" }}
                          placeholder=""
                          defaultValue="EUR"
                        />
                      </InputGroup>
                    </FormItem>
                    <FormItem label="Exch Rate/Date" {...formItemLayout}>
                      <InputGroup compact>
                        <Input
                          style={{ width: "50%" }}
                          placeholder=""
                          defaultValue="10000"
                        />
                        <DatePicker style={{ width: "50%" }} />
                      </InputGroup>
                    </FormItem>
                    <FormItem label="Base Amount" {...formItemLayout}>
                      <InputGroup compact>
                        <Input
                          style={{ width: "70%" }}
                          placeholder=""
                          defaultValue="0.00"
                        />
                        <Input
                          style={{ width: "30%" }}
                          placeholder=""
                          defaultValue="USD"
                          disabled
                        />
                      </InputGroup>
                    </FormItem>
                    <FormItem label="VAT Currency" {...formItemLayout}>
                      <Input placeholder="VAT Currency" defaultValue="" />
                    </FormItem>
                    <FormItem label="VAT Exch Rate" {...formItemLayout}>
                      <Input placeholder="VAT Exch Rate" defaultValue="" />
                    </FormItem>
                    <FormItem label="Reference No." {...formItemLayout}>
                      <Input placeholder="Reference No." defaultValue="" />
                    </FormItem>
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <FormItem label="Operation Trans No." {...formItemLayout}>
                      <Input
                        placeholder="Operation Trans No."
                        defaultValue=""
                        disabled
                      />
                    </FormItem>
                    <FormItem label="PO No." {...formItemLayout}>
                      <Input placeholder="PO No." defaultValue="" />
                    </FormItem>
                    <FormItem label="Received Date" {...formItemLayout}>
                      <DatePicker />
                    </FormItem>
                    <FormItem label="AP/AR Account No." {...formItemLayout}>
                      <Input placeholder="AP/AR Account No." defaultValue="" />
                    </FormItem>
                    <FormItem label="Approval" {...formItemLayout}>
                      <Input placeholder="Approval" defaultValue="" />
                    </FormItem>
                    <FormItem label="IC Trans No." {...formItemLayout}>
                      <Input placeholder="" defaultValue="" disabled />
                    </FormItem>
                  </Col>
                </Row>
              </Form>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Tabs type="card" defaultActiveKey="Accounting View">
                <TabPane tab="Accounting View" key="Accounting View">
                  <Table
                    size="small"
                    pagination={false}
                    dataSource={dataSource1}
                    columns={columns1}
                    title={() => <b>Invoice Details</b>}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link" onClick={this.handleAdd}>
                          Add New
                        </Button>
                      </div>
                    )}
                  />
                  <Form>
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                        <Row gutter={16}>
                          <Col span={8}>
                            <FormItem
                              label="Opr Last User"
                              labelCol={{ span: 11 }}
                              wrapperCol={{ span: 13 }}
                            >
                              <Input placeholder="" defaultValue="" disabled />
                            </FormItem>
                          </Col>
                          <Col span={8}>
                            <FormItem
                              label="Acct Last User"
                              labelCol={{ span: 11 }}
                              wrapperCol={{ span: 13 }}
                            >
                              <Input placeholder="" defaultValue="" disabled />
                            </FormItem>
                          </Col>
                          <Col span={8}>
                            <FormItem
                              label="Acct Last Update"
                              labelCol={{ span: 11 }}
                              wrapperCol={{ span: 13 }}
                            >
                              <Input placeholder="" defaultValue="" disabled />
                            </FormItem>
                          </Col>
                        </Row>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                        <Row type="flex" justify="end">
                          <Col span={24}>
                            <FormItem
                              label="Total"
                              labelCol={{ span: 8 }}
                              wrapperCol={{ span: 16 }}
                            >
                              <InputGroup compact>
                                <Input
                                  style={{ width: "50%" }}
                                  placeholder=""
                                  defaultValue="0.00"
                                  disabled
                                />
                                <Input
                                  style={{ width: "50%" }}
                                  placeholder=""
                                  defaultValue="0.00"
                                  disabled
                                />
                              </InputGroup>
                            </FormItem>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Form>
                </TabPane>
                <TabPane tab="Operation View" key="Operation View">
                  Operation View
                </TabPane>
                <TabPane tab="Remarks" key="Remarks">
                  Remarks
                </TabPane>
              </Tabs>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Table
                size="small"
                pagination={false}
                dataSource={dataSource}
                columns={columns}
                title={() => <b>Payment Information</b>}
              />
              <form>
                <Row gutter={16} className="m-t-18">
                  <Col xs={24} sm={24} md={5} lg={5} xl={5}>
                    <Checkbox>Release to Interface</Checkbox>
                  </Col>
                  <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                    <FormItem
                      label="Total"
                      labelCol={{ span: 8 }}
                      wrapperCol={{ span: 16 }}
                    >
                      <InputGroup compact>
                        <Input
                          style={{ width: "50%" }}
                          placeholder=""
                          defaultValue="0.00"
                        />
                        <Input
                          style={{ width: "50%" }}
                          placeholder=""
                          defaultValue="0.00"
                        />
                      </InputGroup>
                    </FormItem>
                  </Col>
                  <Col xs={24} sm={24} md={11} lg={11} xl={11}>
                    <Row type="flex" justify="end">
                      <Col span={24}>
                        <FormItem
                          label="Remmitance Bank"
                          labelCol={{ span: 8 }}
                          wrapperCol={{ span: 16 }}
                        >
                          <Input
                            placeholder="Remmitance Bank"
                            defaultValue=""
                          />
                        </FormItem>
                      </Col>
                      <Col span={24}>
                        <FormItem
                          label="Payment Bank/Code"
                          labelCol={{ span: 8 }}
                          wrapperCol={{ span: 16 }}
                        >
                          <InputGroup compact>
                            <Input
                              style={{ width: "50%" }}
                              placeholder="Payment Bank/Code"
                              defaultValue="0.00"
                            />
                            <Input
                              style={{ width: "50%" }}
                              placeholder=""
                              defaultValue="0.00"
                              disabled
                            />
                          </InputGroup>
                        </FormItem>
                      </Col>
                      <Col span={24}>
                        <FormItem
                          label="Payment Mode"
                          labelCol={{ span: 8 }}
                          wrapperCol={{ span: 16 }}
                        >
                          <Input placeholder="Payment Mode" defaultValue="" />
                        </FormItem>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default UiTransaction;
