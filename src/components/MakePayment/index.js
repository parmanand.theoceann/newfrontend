import React, { useRef, useEffect } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
//import Tde from "../../routes/tde/Tde";
import Tde from "../../routes/tde/";
import { Modal, Spin, Alert, notification } from "antd";
import DownloadInvoice from "../../routes/chartering/routes/make-payment/DownloadInvoice";
import moment from "moment";
import URL_WITH_VERSION, {
  postAPICall,
  getAPICall,
  apiDeleteCall,
  openNotificationWithIcon,
  useStateCallback,
  URL_WITHOUT_VERSION,
} from "../../shared";
import { DeleteOutlined, SaveOutlined, EditOutlined } from "@ant-design/icons";
import Attachment from "../../shared/components/Attachment";
import Remarks from "../../shared/components/Remarks";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../shared/attachments";

const openNotification = (keyName) => {
  let msg = "Please generate the Invoice No. First";
  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const MakePayment = (props) => {
  const [state, setState] = useStateCallback({
    showTDEForm: false,
    loadFrm: false,
    frmName: "tc_make_payment",
    responseData: { frm: [], tabs: [], active_tab: {} },
    formData: props.formData || {},
    //"period_to":'',
    checkbox: {},
    invList: {},
    isRemarkModel: false,
    tdeData: null,
    // "showDownloadInvoice": false,
    passData: {
      bunkerdeliveryredeliveryterm: [
        { dr_name: "Delivery Cost" },
        { dr_name: "Adj on delivery" },
        { dr_name: "Redelivery Cost" },
        { dr_name: "Adj on redelivery" },
      ],
    },
    makepaymentid: props.formData.id,
    loadInvoiceNo: undefined,
    loadData: {},
    showInvoicePopup: false,
    editMode: true,
  });
  const formdataref = useRef();
  
  useEffect(() => {
    const getFormData = async () => {
      let _frmData = Object.assign(state.passData, state.formData);
      let tci_id;
      let editmode = state.editMode;

      if (state.formData.hasOwnProperty("actualID")) {
        tci_id = _frmData["actualID"];
        const request = await getAPICall(
          `${URL_WITH_VERSION}/make_payment/payment?tci_id=` + tci_id
        );
        const response = await request["data"];

        if (response && response.length > 0) {
          _frmData["period_form"] = moment(response[0]["period_to"]).format(
            "YYYY-MM-DD HH:mm"
          );
        }
      }
      if (_frmData["-"]?.length > 0) {
        _frmData["-"].map((key) => {
          key["disablefield"] = ["description", "amount"];
        });

        if (_frmData["disablefield"]) {
          _frmData["---------------"]["disablefield"] = [
            "misc_adjustment",
            "hire_adjustment",
            "gratuity",
            "other_exp",
            "victualling",
            "other_revenue",
            "ballast_bonus",
            "ilohc",
            "ihc",
            "lashing_stow",
            "exp_allowance",
          ];
        }
        _frmData["disablefield"] = [
          "po_number",
          "days",
          // "purchase_type",
          // "period_to",
          "period_form",
          "invoice_type",
          "final_amt_loc",
          "curr_exe",
          "exch_rate",
        ];
      } else {
        _frmData["-"] = [
          {
            acc_head: "",
            amount: "0",
            amount_usd: "0",
            code: "",
            days: "",
            description: "",
            id: -9e6,
            period_form: "",
            period_to: "",
            visiblefield: [],
          },
        ];
      }

      if (
        _frmData.hasOwnProperty("id") &&
        _frmData["id"] > 0 &&
        _frmData["invoice_no"]
      ) {
        editmode = false;
      }

      setState(
        (prevState) => ({ ...prevState, formData: _frmData }),
        () => {
          setState((prevState) => ({
            ...prevState,
            loadFrm: true,
            editMode: editmode,
          }));
        }
      );
    };
    getFormData();
  }, []);

  const editformdata = async (id) => {
    setState((prevState) => ({ ...prevState, loadFrm: false }));
    const respData = await getAPICall(
      `${URL_WITH_VERSION}/make_payment/edit?e=${id}&frm=time_charter_in_form`
    );
    const editData = await respData["data"];
    const updatedFormdata = Object.assign(state.passData, editData);
    setState(
      (prevState) => ({
        ...prevState,
        loadFrm: false,
        makepaymentid: id,
        //formData: editData,
        formData: updatedFormdata,
      }),
      () => {
        setState((prevState) => ({ ...prevState, loadFrm: true }));
      }
    );
  };
  const saveFormData = async (vData) => {
    setState((prevState) => ({ ...prevState, loadFrm: false }));
    let type = "save";
    let suMethod = "POST";
    if (vData.hasOwnProperty("id")) {
      type = "update";
      suMethod = "PUT";
    }else{
      vData["-"].forEach((e) => delete e["id"]);
    }

    //vData["-"].forEach((e) => delete e["id"]);
    const { frmName } = state;
    let suURL = `${URL_WITH_VERSION}/make_payment/${type}?frm=${frmName}`;
    if (vData && vData.hasOwnProperty("lob")) {
      delete vData["lob"];
    }
    formdataref.current = vData;
    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);

        const _vData = { ...vData, invoice_no: data.row.invoice_no };

        setState((prevState) => ({
          ...prevState,
          makepaymentid: data.row.payment_id,
          loadFrm: true,
        }));
        
        if (props.hasOwnProperty("onUpdateMakePayment") && type === "update") {
          // setState(prevState => ({ ...prevState, makepaymentid: data.row.payment_id, loadFrm: true }));
          props.onUpdateMakePayment(data.row.payment_id, "TCI");
        } else {
          editformdata(data.row.payment_id);

          //  showDownloadInvoice(true, _vData);
        }
        // if (props.hasOwnProperty('modalCloseEvent')) {
        //   props.modalCloseEvent();
        // }
      } else {
        openNotificationWithIcon("error", data.message);
        setState((prevState) => ({
          ...prevState,
          loadFrm: true,
          formData: formdataref.current,
        }));
      }
    });
  };

  const _deleteInvoice = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => deleteInvoice(postData),
    });
  };

  const deleteInvoice = (data) => {
    const { frmName } = state;
    let URL = `${URL_WITH_VERSION}/make_payment/delete?frm=${frmName}`;
    apiDeleteCall(
      URL,
      { id: data["id"], payment_status: data["payment_status"] },
      (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          if (props.hasOwnProperty("modalCloseEvent")) {
            props.modalCloseEvent();
          }
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      }
    );
  };

  const showDownloadInvoice = (boolVal, _loadInvoice = undefined) => {
   // console.log('props.formData :',props.formData);
    //console.log('_loadInvoice :',_loadInvoice);
    let loadInvoice = {...props.formData,..._loadInvoice}
    //console.log('loadInvoice2 :',loadInvoice);
      if (loadInvoice && !loadInvoice.hasOwnProperty("invoice_no")) {
      openNotificationWithIcon("info", "Plese Create Invoice First");
      return;
    }
    // let period_to_date = document.getElementsByName("period_to");
    // if (period_to_date[0].value && loadInvoice) {
    //   loadInvoice["period_to"] = period_to_date[0].value;
    // }
    if (
      loadInvoice &&
      (loadInvoice["period_to"] === "" ||
        loadInvoice["period_to"] === undefined)
    ) {
      loadInvoice["period_to"] = new Date();
    }
    if (boolVal) {
      if (loadInvoice && loadInvoice.hasOwnProperty("actualID")) {
        loadInvoice["tci_id"] = loadInvoice["actualID"];
      }
      setState(
        (prevState) => ({
          ...prevState,
          loadInvoiceNo: loadInvoice,
          loadData: { TCIH: true, TCIBB: true },
        }),
        () => {
          setState((prevState) => ({
            ...prevState,
            downloadInvoice: boolVal,
            showInvoicePopup: true,
          }));
        }
      );
    } else {
      setState((prevState) => ({ ...prevState, downloadInvoice: boolVal }));
      if (props.hasOwnProperty("modalCloseEvent")) {
        // props.modalCloseEvent();
      }
    }
  };

  const closeDonloadInvoice = () => {
    setState((prevState) => ({ ...prevState, downloadInvoice: false }));
    if (props.hasOwnProperty("modalCloseEvent")) {
      // props.modalCloseEvent();
    }
  };

  const showTDE = async (key, bolVal) => {
    const { formData } = state;

    if (bolVal) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      let respData = response["data"];

      let resp = null,
        target = undefined,
        tdeData = {};
      const responseData = await getAPICall(
        `${URL_WITH_VERSION}/address/edit?ae=${formData["charterer_from"]}`
      );
      const responseAddressData = responseData["data"];
      let account_no =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["account_no"]
          : "";

      let accountCode =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["swift_code"]
          : "";
      if (formData && formData.invoice_no && formData.invoice_no !== "") {
        if (respData && respData.length > 0) {
          target = respData.find(
            (item) => item.invoice_no === formData.invoice_no
          );
          if (target && target.hasOwnProperty("id") && target["id"] > 0) {
            resp = await getAPICall(
              `${URL_WITH_VERSION}/tde/edit?e=${target["id"]}`
            );
          }
        }

        let accounting = [];
        if (
          target &&
          resp &&
          resp["data"] &&
          resp["data"].hasOwnProperty("id")
        ) {
          tdeData = resp["data"];
          tdeData["----"] = tdeData["----"][0];

          formData &&
            formData["-"] &&
            formData["-"].length > 0 &&
            formData["-"].map((val, ind) => {
              accounting.push({
                company: target["bill_via"],
                amount: val.amount,
                lob: formData["company_lob"],
                vessel_code: formData["vessel_code"],
                description: val.description,
                editable: true,
                vessel_name: formData["vessel_id"],
                account: accountCode,
                ap_ar_acct: target["ar_pr_account_no"],
                key: "table_row_" + ind,
                voyage: target["voyage_manager_id"],
                port: "",
                ic: "",
                id: -9e6 + ind,
              });
              return true;
            });
          //  tdeData['vessel']=
          tdeData["accounting"] = accounting;
          tdeData["--"] = { total: target["invoice_amount"] };
          //  tdeData['----'] = { 'total_due': target['invoice_amount'], 'total': target['invoice_amount'],"remittance_bank":resp['data']['----']["remittance_bank"]}
        } else {
          formData &&
            formData["-"] &&
            formData["-"].length > 0 &&
            formData["-"].map((val, ind) => {
              accounting.push({
                company: formData["my_company"],
                account: accountCode,
                // "lob": "",
                // "vessel_code": "",
                lob: formData["company_lob"],
                vessel_code: formData["vessel_code"],
                description: val.description,
                vessel_name: formData["vessel_id"],
                amount: val.amount,
                ap_ar_acct: account_no,
                voyage: formData["voyage_manager_id"],
                port: "",
                ic: "",
                id: -9e6 + ind,
              });
              return true;
            });
          tdeData = {
            invoice: formData["acc_type"],
            vessel: formData["vessel_id"],
            // accounting_date: "",
            po_number: formData["po_number"],
            invoice_date: formData["due_date"],
            received_date: formData["received_date"],
            invoice_no: formData.invoice_no,
            ar_pr_account_no: account_no,
            inv_status: formData.payment_status,
            invoice_type: "",
            vendor: formData.charterer_from,
            invoice_amount: formData.amount,
            bill_via: formData.my_company,
            account_base: formData.amount,
            payment_term: formData.terms,
            // '--': { 'total': formData['amount'] },
            // '----': { 'total_due': formData['amount'], 'total': formData['amount'] },
            accounting: accounting,
          };
        }
        setState((prevState) => ({
          ...prevState,
          showTDEForm: bolVal,
          tdeData: tdeData,
        }));
      } else {
        openNotificationWithIcon(
          "info",
          "Please generate the Invoice No. First"
        );
      }
    } else {
      setState((prevState) => ({ ...prevState, showTDEForm: bolVal }));
    }
  };

  const getCheckBoxVal = (code, checkboxesValue) => {
    let val = "";
    let checkboxesValueUpdated = { ...checkboxesValue };
    switch (code) {
      case "TCIH":
        //val = "add_tc_hire";
        checkboxesValueUpdated = { ...checkboxesValue, add_tc_hire: false };
        break;
      case "TCIGRA":
        //val = "gratuity";
        checkboxesValueUpdated = { ...checkboxesValue, gratuity: false };
        break;
      case "TCIOTHEXP":
        //val = "other_exp";
        checkboxesValueUpdated = { ...checkboxesValue, other_exp: false };
        break;
      case "TCIBCOM":
        // val = "broker_comm";
        checkboxesValueUpdated = { ...checkboxesValue, broker_comm: false };
        break;
      case "TCIVIC":
        //val = "victualling";
        checkboxesValueUpdated = { ...checkboxesValue, victualling: false };
        break;
      case "TCIOTHREV":
        //val = "other_revenue";
        checkboxesValueUpdated = { ...checkboxesValue, other_revenue: false };
        break;
      case "TCIBB":
        //val = "ballast_bonus";
        checkboxesValueUpdated = { ...checkboxesValue, ballast_bonus: false };
        break;
      case "TCIILOHC":
        //val = "ilohc";
        checkboxesValueUpdated = { ...checkboxesValue, ilohc: false };
        break;
      case "TCIIHC":
        // val = "ihc";
        checkboxesValueUpdated = { ...checkboxesValue, ihc: false };
        break;
      case "TCIREA":
        //val = "exp_allowance";
        checkboxesValueUpdated = { ...checkboxesValue, exp_allowance: false };
        break;
      case "TCILAST":
        //val = "lashing_stow";
        checkboxesValueUpdated = { ...checkboxesValue, lashing_stow: false };
        break;
      case "TCIBADVLSFO":
        //val = "bad_vlsfo";
        checkboxesValueUpdated = { ...checkboxesValue, bad_vlsfo: false };
        break;
      case "TCIBADIFO":
        //val = "bad_ifo";
        checkboxesValueUpdated = { ...checkboxesValue, bad_ifo: false };
        break;
      case "TCIBADMGO":
        // val = "bad_mgo";
        checkboxesValueUpdated = { ...checkboxesValue, bad_mgo: false };
        break;
      case "TCIBADLSMGO":
        //val = "bad_lsmgo";
        checkboxesValueUpdated = { ...checkboxesValue, bad_lsmgo: false };
        break;
      case "TCIBADULSFO":
        // val = "bad_ulsfo";
        checkboxesValueUpdated = { ...checkboxesValue, bad_ulsfo: false };
        break;
      case "TCIBARVLSFO":
        // val = "bor_vlsfo";
        checkboxesValueUpdated = { ...checkboxesValue, bor_vlsfo: false };
        break;
      case "TCIBARMGO":
        // val = "bor_mgo";
        checkboxesValueUpdated = { ...checkboxesValue, bor_mgo: false };
        break;
      case "TCIBARIFO":
        // val = "bor_ifo";
        checkboxesValueUpdated = { ...checkboxesValue, bor_ifo: false };
        break;
      case "TCIBARLSMGO":
        // val = "bor_lsmgo";
        checkboxesValueUpdated = { ...checkboxesValue, bor_lsmgo: false };
        break;
      case "TCIBARULSFO":
        // val = "bor_ulsfo";
        checkboxesValueUpdated = { ...checkboxesValue, bor_ulsfo: false };
        break;
    }
    // return val;
    return checkboxesValueUpdated;
  };

  const onClickExtraIcon = async (action, data, fulldata) => {
    const checkboxesValue = fulldata["---------------"];
    const checkboxesInfo = fulldata["-"];
    const amount = (
      parseFloat(fulldata["amount"]) - parseFloat(data.amount)
    ).toFixed(2);

    const checkboxesInfo2 = checkboxesInfo.filter(
      (obj) => obj.code !== data.code
    );
    const updatedcheckboxesValue = getCheckBoxVal(data.code, checkboxesValue);
    const updatedfulldata = {
      ...fulldata,
      "---------------": updatedcheckboxesValue,
      "-": checkboxesInfo2,
      amount: amount,
    };
    
    setState(
      (prevState) => ({
        ...prevState,
        loadFrm: false,
        formData: updatedfulldata,
      }),
      () => {
        setState((prevState) => ({ ...prevState, loadFrm: true }));
      }
    );
  };

  const {
    frmName,
    editMode,
    formData,
    loadFrm,
    showTDEForm,
    tdeData,
    downloadInvoice,
    loadInvoiceNo,
    loadData,
    makepaymentid,
    isRemarkModel,
  } = state;

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };

  const handleRemark = () => {
    setState((prevState) => ({
      ...prevState,
      isRemarkModel: true,
    }));
  };

 // console.log('state :',state?.formData)
  return (
    <>
      <div className="body-wrapper">
        {frmName && loadFrm === true ? (
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  formData={formData}
                  showForm={true}
                  frmCode={frmName}
                  addForm={true}
                  editMode={editMode}
                  showButtons={[
                    // { "id": "cancel", "title": "Reset", "type": "danger" },
                    {
                      id: "save",
                      title: "Save",
                      type: "primary",
                      event: (data) => {
                        saveFormData(data);
                      },
                    },
                  ]}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        {
                          key: "s1",
                          isSets: [
                            {
                              id: "1",
                              key: "save",
                              type: <SaveOutlined />,
                              withText: "Save",
                              showToolTip: true,
                              event: (key, data) => saveFormData(data),
                            },
                            makepaymentid && {
                              id: "2",
                              key: "delete",
                              type: <DeleteOutlined />,
                              withText: "Delete",
                              showToolTip: true,
                              event: (key, data) => _deleteInvoice(data),
                            },
                            {
                              id: "3",
                              key: "edit",
                              type: <EditOutlined />,
                              withText: "Remark ",
                              showToolTip: true,
                              event: (key, data) => handleRemark(),
                            },
                          ],
                        },
                      ],

                      isRightBtn: [
                        {
                          key: "s2",
                          isSets: [
                            {
                              key: "download_invoice",
                              isDropdown: 0,
                              withText: "Create Invoice",
                              type: "",
                              menus: null,
                              event: (key,data) => {
                                // console.log('data is :',data)
                                // console.log('props.formData is :',props.formData)
                                // console.log('state.formData :',state.formData)
                                showDownloadInvoice(
                                  true,
                                  data && data.hasOwnProperty("id") ? data :undefined
                                 //props.formData && props.formData.hasOwnProperty("id") ? props.formData : undefined,
                                );
                              },
                            },
                            {
                              key: "tde",
                              isDropdown: 0,
                              withText: "TDE",
                              type: "",
                              menus: null,
                              event: (key) => {
                                //showTDE(key, true);
                                if (formData?.invoice_no) {
                                  setState((prevState) => ({
                                    ...prevState,
                                    showTDEForm: true,
                                  }));
                                } else {
                                  openNotification("tde");
                                }
                              },
                            },
                            {
                              key: "attachment",
                              isDropdown: 0,
                              withText: "Attachment",
                              type: "",
                              menus: null,
                              event: (key, data) => {
                                data &&
                                data.hasOwnProperty("id") &&
                                data["id"] > 0
                                  ? ShowAttachment(true)
                                  : openNotificationWithIcon(
                                      "info",
                                      "Please save Invoice First.",
                                      3
                                    );
                              },
                            },
                          ],
                        },
                      ],
                      //isResetOption: false
                    },
                  ]}
                  inlineLayout={true}
                  isShowFixedColumn={["-", "Bunker Delivery / Redelivery Term"]}
                  tableRowDeleteAction={(action, data, fulldata) =>
                    onClickExtraIcon(action, data, fulldata)
                  }
                  summary={[{ gKey: "-", showTotalFor: ["amount_usd"] }]}
                />
              </div>
            </div>
          </article>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </div>

      {showTDEForm ? (
        <Modal
          title="TDE"
          open={showTDEForm}
          width="80%"
          onCancel={() =>
            setState((prevState) => ({ ...prevState, showTDEForm: false }))
          }
          style={{
            maxHeight: 790,
            overflowY: "auto",
            padding: "0.5rem",
            top: "10px",
          }}
          footer={null}
        >
          <Tde
            invoiceType="hire_payable"
            //isEdit={ tdeData !== null && tdeData.id && tdeData.id > 0 ? true : false}
            //deleteTde={() => showTDE(undefined, false)}
            //modalCloseEvent={() => showTDE(undefined, false)}
            //formData={tdeData}
            //saveUpdateClose={() => setState(prevState => ({ ...prevState, isVisible: false }))}

            formData={formData}
            //deleteTde={() => setState(prevState => ({ ...prevState, showTDEForm: false }))}
            //modalCloseEvent={() => setState(prevState => ({ ...prevState, showTDEForm: false }))}
            invoiceNo={formData.invoice_no}
          />
        </Modal>
      ) : undefined}
      {state.isShowAttachment ? (
        <Modal
          style={{ top: "2%" }}
          title="Upload Attachment"
          open={state.isShowAttachment}
          onCancel={() => ShowAttachment(false)}
          width="50%"
          footer={null}
        >
          {state.loadComponent}
        </Modal>
      ) : undefined}

      {downloadInvoice ? (
        <Modal
          title="Invoice Print"
          open={downloadInvoice}
          width="80%"
          onCancel={() => showDownloadInvoice(false,state.formData)}
          style={{
            maxHeight: 790,
            overflowY: "auto",
            padding: "0.5rem",
            top: "10px",
          }}
          footer={null}
        >
          {/* <ConfirmStatement vesselID={tciID.vessel_id} chartrerID={tciID.chartrer_id} tciID={tciID.tci_id} from={tciID.delivery_date} to={tciID.redelivery_date} /> */}

          <DownloadInvoice
            loadInvoice={loadInvoiceNo}
            loadData={loadData}
            invoiceType={"TCI"}
            makepaymentid={makepaymentid}
            showInvoicePopup
            closeDonloadInvoice={closeDonloadInvoice}
          />
        </Modal>
      ) : undefined}
      {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isRemarkModel: false }))
          }
          footer={false}
        >
          <Remarks
            remarksID={formData.tc_id}
            remarkType="make_payment"
            // idType="Bunker_no"
          />
        </Modal>
      )}
    </>
  );
};

export default MakePayment;
