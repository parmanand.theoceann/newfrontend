import React, { useEffect, useRef } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
//import Tde from "../../routes/tde/Tde";
import Tde from "../../routes/tde/";
import { Modal, Spin, Alert,notification } from "antd";
import DownloadInvoice from "../../routes/chartering/routes/make-payment/DownloadInvoice";
import moment from "moment";
import URL_WITH_VERSION, { postAPICall,getAPICall, apiDeleteCall, openNotificationWithIcon, useStateCallback,} from "../../shared";
import { DeleteOutlined, SaveOutlined,EditOutlined} from "@ant-design/icons";
import Attachment from '../../shared/components/Attachment'
import Remarks from "../../shared/components/Remarks";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../shared/attachments";

const openNotification = (keyName) => {
  let msg = "Please generate the Invoice No. First";
  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const MakePayment = (props) => {
  const [state, setState] = useStateCallback({
    showTDEForm: false,
    loadFrm: false,
    isRemarkModel: false,
    frmName: "tco_make_payment",
    responseData: { frm: [], tabs: [], active_tab: {} },
    frmOptions: [],
    formData: props.formData || {},
    //"period_to":'',
    checkbox: {},
    invList: {},
    tdeData: null,
    // "showDownloadInvoice": false,
    passData: {
      bunkerdeliveryredeliveryterm: [
        { dr_name: "Delivery Cost" },
        { dr_name: "Adj on delivery" },
        { dr_name: "Redelivery Cost" },
        { dr_name: "Adj on redelivery" },
      ],
    },
    makepaymentid: props.formData.id,
    loadInvoiceNo: undefined,
    loadData: {},
    showInvoicePopup: false,
    editMode: true,
  });

  //formdataref=React.createRef();
  const formdataref = useRef();

  useEffect(() => {
    const fetchData = async () => {
      try {
        let _frmData = Object.assign(state.passData, state.formData);

        let tco_id = _frmData["tco_id"] || _frmData["actualID"];
        const response1 = await getAPICall(`${URL_WITH_VERSION}/tco/voyage/${tco_id}`);
        const respSData = await response1['data'];

        if (respSData) {
          const frmOptions = [
            { key: 'voyage_manager_id', data: respSData },
          ]
          setState(prevState => ({ ...prevState, frmOptions: frmOptions }));
        }
        const { formData } = state;
        let editmode = state.editMode;
        if (state.formData.hasOwnProperty("actualID")) {
          tco_id = _frmData["actualID"];
          const request = await getAPICall(
            `${URL_WITH_VERSION}/make_payment/payment?tco_id=` + tco_id
          );
          const response = await request["data"];

          if (response && response.length > 0) {
            _frmData["period_form"] = moment(response[0]["period_to"]).format(
              "YYYY-MM-DD HH:mm"
            );
          }
        }

        // setState({ ...state, 'formData': _frmData }, () => setState({ ...state, "loadFrm": true }));

        if (_frmData["-"].length > 0) {
          _frmData["-"].map((key) => {
            key["disablefield"] = ["description", "amount"];
          });
          _frmData["---------------"]["disablefield"] = [
            "misc_adjustment",
            "hire_adjustment",
            "gratuity",
            "other_exp",
            "victualling",
            "other_revenue",
            "ballast_bonus",
            "ilohc",
            "ihc",
            "lashing_stow",
            "exp_allowance",
          ];
          _frmData["disablefield"] = [
            "po_number",
            "days",
            "purchase_type",
            "period_to",
            "period_form",
            "invoice_type",
            "final_amt_loc",
            "curr_exe",
            "exch_rate",
            "voyage_manager_id"
          ];
        }

        if (
          _frmData.hasOwnProperty("id") &&
          _frmData["id"] > 0 &&
          _frmData["invoice_no"]
        ) {
          editmode = false;
        }

        setState(prevState => ({ 
          ...prevState,
           formData: _frmData,
           loadFrm: true, 
           editMode: editmode
           }));

      } catch (err) {
        console.log(err);
      }
    }

    fetchData();
  }, [])

  const saveFormData = (vData) => {
    setState(prevState => ({ ...prevState, loadFrm: false }));
    let type = "save";
    let suMethod = "POST";
    if (vData.hasOwnProperty("id")) {
      type = "update";
      suMethod = "PUT";
    }

    const { frmName } = state;
    vData["-"].forEach((e) => delete e["id"]);
    let suURL = `${URL_WITH_VERSION}/make_payment/${type}?frm=${frmName}`;
    if (vData && vData.hasOwnProperty("lob")) {
      delete vData["lob"];
    }

    formdataref.current = vData;
    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        const _vData = { ...vData, invoice_no: data.row.invoice_no };
        setState(prevState => ({ ...prevState, makepaymentid: data.row.payment_id, loadFrm: true }));
        if (props.hasOwnProperty('onUpdateMakePayment') && type === 'update') {
          props.onUpdateMakePayment(data.row.payment_id, "TCO")
        } else {
          showDownloadInvoice(true, _vData);
        }
        // showDownloadInvoice(true, _vData);
        // if (props.hasOwnProperty('modalCloseEvent')) {
        //   props.modalCloseEvent();
        // }
      } else {
        openNotificationWithIcon("error", data.message);
        setState(prevState => ({...prevState, loadFrm: true, formData: formdataref.current}));
      }
    });
  };

  const _deleteInvoice = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => deleteInvoice(postData),
    });
  };

  const deleteInvoice = (data) => {
    const { frmName } = state;
    let URL = `${URL_WITH_VERSION}/make_payment/delete?frm=${frmName}`;
    apiDeleteCall(
      URL,
      { id: data["id"], payment_status: data["payment_status"] },
      (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          if (props.hasOwnProperty("modalCloseEvent")) {
            props.modalCloseEvent();
          }
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      }
    );
  };

  const showDownloadInvoice = (boolVal, loadInvoice = undefined) => {
    if (loadInvoice && !loadInvoice.hasOwnProperty("invoice_no")) {
      openNotificationWithIcon("info", "Plese Create Invoice First");
      return;
    }
    // let period_to_date = document.getElementsByName("period_to");
    // if (period_to_date[0].value && loadInvoice) {
    //   loadInvoice["period_to"] = period_to_date[0].value;
    // }
    if (
      (loadInvoice && (loadInvoice["period_to"] === "" || loadInvoice["period_to"] === undefined))

    ) {
      loadInvoice["period_to"] = new Date();
    }
    if (boolVal) {
      if (loadInvoice.hasOwnProperty("actualID")) {
        loadInvoice["tco_id"] = loadInvoice["actualID"];
      }

      setState(prevState => ({
        ...prevState, 
        loadInvoiceNo: loadInvoice,
        loadData: { TCOH: true, TCOBB: true },
        downloadInvoice: boolVal,
        showInvoicePopup: true,
      }
      ));

    } else {
      setState(prevState => ({...prevState, downloadInvoice: boolVal}));
      if (props.hasOwnProperty("modalCloseEvent")) {
        // props.modalCloseEvent();
      }
    }
  };

  const closeDonloadInvoice = () => {

    setState(prevState => ({...prevState, downloadInvoice: false}));

    if (props.hasOwnProperty("modalCloseEvent")) {
      // props.modalCloseEvent();
    }
  };

  const showTDE = async (key, bolVal) => {
    const { formData } = state;

    const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
    let respData = response["data"];

    let resp = null,
      target = undefined,
      tdeData = {};

    if (formData && formData.invoice_no && formData.invoice_no !== "") {
      if (respData && respData.length > 0) {
        target = respData.find(
          (item) => item.invoice_no === formData.invoice_no
        );
        if (target && target.hasOwnProperty("id") && target["id"] > 0) {
          resp = await getAPICall(
            `${URL_WITH_VERSION}/tde/edit?e=${target["id"]}`
          );
        }
      }

      let accounting = [];
      if (target && resp && resp["data"] && resp["data"].hasOwnProperty("id")) {
        tdeData = resp["data"];
        tdeData["----"] = tdeData["----"][0];

        formData &&
          formData["-"] &&
          formData["-"].length > 0 &&
          formData["-"].map((val, ind) => {
            accounting.push({
              company: target["bill_via"],
              amount: val.amount,
              lob: formData["company_lob"],
              vessel_code: formData["vessel_code"],
              description: val.description,
              editable: true,
              vessel_name: formData["vessel_id"],
              account: "",
              ap_ar_acct: target["ar_pr_account_no"],
              key: "table_row_" + ind,
              voyage: target["voyage_manager_id"],
              port: "",
              ic: "",
              id: -9e6 + ind,
            });
            return true;
          });
        //  tdeData['vessel']=
        tdeData["accounting"] = accounting;
        tdeData["--"] = { total: target["invoice_amount"] };
        //  tdeData['----'] = { 'total_due': target['invoice_amount'], 'total': target['invoice_amount'],"remittance_bank":resp['data']['----']["remittance_bank"]}
        //tdeData['----']=resp['data']['----'][0];
      } else {
        const responseData = await getAPICall(
          `${URL_WITH_VERSION}/address/edit?ae=${formData["charterer_from"]}`
        );
        const responseAddressData = responseData["data"];
        let account_no =
          responseAddressData &&
            responseAddressData["bank&accountdetails"] &&
            responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["account_no"]
            : "";

        let accountCode =
          responseAddressData &&
            responseAddressData["bank&accountdetails"] &&
            responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["swift_code"]
            : "";
        formData &&
          formData["-"] &&
          formData["-"].length > 0 &&
          formData["-"].map((val, ind) => {
            accounting.push({
              company: formData["my_company"],
              account: accountCode,
              // "lob": "",
              // "vessel_code": "",
              lob: formData["company_lob"],
              vessel_code: formData["vessel_code"],
              description: val.description,
              vessel_name: formData["vessel_id"],
              amount: val.amount,
              ap_ar_acct: account_no,
              voyage: formData["voyage_manager_id"],
              port: "",
              ic: "",
              id: -9e6 + ind,
            });
            return true;
          });
        tdeData = {
          invoice: formData["acc_type"],
          po_number: formData["po_number"],
          invoice_date: formData["due_date"],
          received_date: formData["received_date"],
          invoice_no: formData.invoice_no,
          ar_pr_account_no: account_no,
          inv_status: formData.payment_status,
          invoice_type: "",
          vendor: formData.charterer_from,
          invoice_amount: formData.amount,
          bill_via: formData.my_company,
          account_base: formData.amount,
          payment_term: formData.terms,
          vessel: formData["vessel_id"],
          // '--': { 'total': formData['amount'] },
          // '----': { 'total_due': formData['amount'], 'total': formData['amount'] },
          accounting: accounting,
        };
      }
      
      setState(prevState => ({...prevState, showTDEForm: bolVal, tdeData: tdeData}));
    } else {
      openNotificationWithIcon("info", "Please generate the Invoice No. First");
    }
  };

  const {
    frmName,
    formData,
    loadFrm,
    showTDEForm,
    tdeData,
    downloadInvoice,
    loadInvoiceNo,
    loadData,
    makepaymentid,
    editMode,
    frmOptions,
    isRemarkModel
  } = state;

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };

  
  const handleRemark = () => {
    setState(prevState => ({
      ...prevState,
      isRemarkModel: true,
    }));
  }

  return (
    <>
      <div className="body-wrapper">
        {frmName && loadFrm === true ? (
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  formData={formData}
                  showForm={true}
                  frmCode={frmName}
                  addForm={true}
                  editMode={editMode}
                  showButtons={[
                    // { "id": "cancel", "title": "Reset", "type": "danger" },
                    {
                      id: "save",
                      title: "Save",
                      type: "primary",
                      event: (data) => {
                        saveFormData(data);
                      },
                    },
                  ]}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        {
                          key: "s1",
                          isSets: [
                            {
                              id: "1",
                              key: "save",
                              type: <SaveOutlined />,
                              withText: "Save",
                              showToolTip: true,
                              event: (key, data) => saveFormData(data),
                            },
                            makepaymentid && {
                              id: "2",
                              key: "delete",
                              type: <DeleteOutlined />,
                              withText: "Delete",
                              showToolTip: true,
                              event: (key, data) => _deleteInvoice(data),
                            },

                            {
                              id: "3",
                              key: "edit",
                              type: <EditOutlined/>,
                              withText: "Remark",
                              showToolTip: true,
                              event: (key, data) => handleRemark(),
                            }
                          ],
                        },
                      ],

                      isRightBtn: [
                        {
                          key: "s2",
                          isSets: [
                            {
                              key: "download_invoice",
                              isDropdown: 0,
                              withText: "Create Invoice",
                              type: "",
                              menus: null,
                              event: (key) => {
                                showDownloadInvoice(
                                  true,
                                  props.formData &&
                                    props.formData.hasOwnProperty("id")
                                    ? props.formData
                                    : undefined
                                );
                              },
                            },
                            {
                              key: "tde",
                              isDropdown: 0,
                              withText: "TDE",
                              type: "",
                              menus: null,
                              event: (key) => {
                                showTDE(key, true);
                                if(formData?.invoice_no) {
                                  setState((prevState) => ({ ...prevState, showTDEForm: true }));
                                }else {
                                  openNotification("tde")
                                }
                              },
                            },
                            {
                              key: "attachment",
                              isDropdown: 0,
                              withText: "Attachment",
                              type: "",
                              menus: null,
                              event: (key, data) => {
                                data &&
                                data.hasOwnProperty("id") &&
                                data["id"] > 0
                                  ? ShowAttachment(true)
                                  : openNotificationWithIcon(
                                      "info",
                                      "Please save Invoice First.",
                                      3
                                    );
                              },
                            },
                          ],
                        },
                      ],
                      isResetOption: false,
                    },
                  ]}
                  inlineLayout={true}
                  isShowFixedColumn={[
                    "-",
                    "Bunker Delivery / Redelivery Term",
                  ]}
                  frmOptions={frmOptions}
                  tableRowDeleteAction={(action, data) => { }}
                  summary={[{ gKey: "-", showTotalFor: ["amount_usd"] }]}
                />
              </div>
            </div>
          </article>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert
                message=" "
                description="Please wait..."
                type="info"
              />
            </Spin>
          </div>
        )}
      </div>

      {showTDEForm ? (
        <Modal
          title="TDE"
          open={state.showTDEForm}
          width="80%"
          //onCancel={() => showTDE(undefined, false)} 
          onCancel={() => setState(prevState => ({ ...prevState, showTDEForm: false }))}
          style={{ top: "10px" }}
          bodyStyle={{ maxHeight: 790, overflowY: "auto", padding: "0.5rem" }}
          footer={null}
          maskClosable={false}
        >
          <Tde
            invoiceType="hire_payable"
            //isEdit={ tdeData != null && tdeData.id && tdeData.id > 0 ? true : false}
            //deleteTde={() => showTDE(undefined, false)}
            //modalCloseEvent={() => showTDE(undefined, false)}
            //formData={tdeData}
            formData={formData}
            saveUpdateClose={() => setState(prevState => ({...prevState, isVisible: false}))}

            invoiceNo={formData.invoice_no}
          />
        </Modal>
      ) : (
        undefined
      )}

      {state.isShowAttachment ? (
          <Modal
            style={{ top: "2%" }}
            title="Upload Attachment"
            open={state.isShowAttachment}
            onCancel={() => ShowAttachment(false)}
            width="50%"
            footer={null}
          >
            {state.loadComponent}
          </Modal>
        ) : undefined}

      {downloadInvoice ? (
        <Modal
          title="Invoice Print"
          open={downloadInvoice}
          width="80%"
          onCancel={() => showDownloadInvoice(false)}
          style={{ top: "10px" }}
          bodyStyle={{ maxHeight: 790, overflowY: "auto", padding: "0.5rem" }}
          footer={null}
          maskClosable={false}
        >
          {/* <ConfirmStatement vesselID={tciID.vessel_id} chartrerID={tciID.chartrer_id} tciID={tciID.tco_id} from={tciID.delivery_date} to={tciID.redelivery_date} /> */}
          <DownloadInvoice
            loadInvoice={loadInvoiceNo}
            loadData={loadData}
            invoiceType={"TCO"}
            makepaymentid={makepaymentid}
            showInvoicePopup
            closeDonloadInvoice={closeDonloadInvoice}
          />
        </Modal>
      ) : (
        undefined
      )}
{isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() => setState(prevState => ({ ...prevState, isRemarkModel: false }))}
          footer={false}
        >
          <Remarks
            remarksID={props.remarksID}
            remarkType="make_payment"
            // idType="Bunker_no"
          />


        </Modal>
      )}
    </>
  );
}

export default MakePayment;
