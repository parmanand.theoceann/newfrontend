import React, { useState, useEffect } from "react";
import { Menu, Layout } from "antd";
import RightBARCONFIGURATION from "../../constants/rightbar-configuration";
import { useNavigate } from "react-router-dom";

const { Sider } = Layout;

const RightBarUI = ({ pageTitle, callback }) => {
  const navigate=useNavigate()
  const [selectedKeys, setSelectedKeys] = useState(["1"]);
  const [rigthbarConfigurationArray, setRightbarConfigurationArray] =
    useState(null);

    

  useEffect(() => {
    if (pageTitle) {
      setSelectedKeys(["1"]);
      setRightbarConfigurationArray(
        RightBARCONFIGURATION && RightBARCONFIGURATION[pageTitle]
      );
    }
  }, [pageTitle]);

  const onClickButton = (key, options) => {
   if(key==="home"){
      navigate("/chartering-dashboard")
      return
    }
    if (callback) callback(key, options);
  };

  return (
    <>
      <Sider trigger={null} collapsible collapsed={true} onCollapse={true}>
        <Menu
          theme="dark"
          defaultSelectedKeys={selectedKeys}
          mode="inline"
          className="rigtsidebar-menu "
        >
          {rigthbarConfigurationArray
            ? rigthbarConfigurationArray.menuoption.map((menu, i) => (
                <Menu.Item
                  key={i}
                  onClick={() => onClickButton(menu.key, menu)}
                >

                  {menu.icon}
                  <span>{menu.title}</span>
                </Menu.Item>
              ))
            : null}
        </Menu>
      </Sider>
    </>
  );
};

export default RightBarUI;
