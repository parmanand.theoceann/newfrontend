import React from 'react';
import { Modal } from 'antd';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  getAPICall,
  URL_WITHOUT_VERSION,
  postAPICall,
  openNotificationWithIcon,
} from '../../shared';
import PortReport from '../../routes/data-center-reports/PortReport';
class PortForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      frmName: 'port_information',
      responseData: { frm: [], tabs: [], active_tab: {} },
      formData: this.props.formDataValues || {},
      isShowPortReport: false,
    };
  }

  PortReport = (showPortReport) =>
    this.setState({ ...this.state, isShowPortReport: showPortReport });

  componentDidMount = async () => {
    const response = await getAPICall(`${URL_WITHOUT_VERSION}get/${this.state.frmName}`);
    const data = await response['data'];
    this.setState({ ...this.state, responseData: data });
  };

  saveFormData = (data) => {
    const { frmName } = this.state;
    let suURL = `${URL_WITH_VERSION}/port/save?frm=${frmName}`;
    let suMethod = 'POST';

    if (data && data.hasOwnProperty('id')) {
      suURL = `${URL_WITH_VERSION}/port/update?frm=${frmName}`;
      suMethod = 'PATCH';
    }

    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon('success', data.message);
        if (this.props.hasOwnProperty('modalCloseEvent')) {
          this.props.modalCloseEvent();
        }
      } else {
        openNotificationWithIcon('error', data.message);
      }
    });
  };

  render() {
    const { frmName, formData, isShowPortReport } = this.state;
    return (
      <div className="body-wrapper">
        {frmName ? (
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <NormalFormIndex
                  key={'key_' + frmName + '_0'}
                  formClass="label-min-height"
                  formData={formData}
                  showForm={true}
                  frmCode={frmName}
                  addForm={true}
                  showButtons={[
                    { id: 'cancel', title: 'Reset', type: 'danger' },
                    {
                      id: 'save',
                      title: 'Save',
                      type: 'primary',
                      event: (data) => {
                        this.saveFormData(data);
                      },
                    },
                  ]}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        // {
                        //   key: "s1",
                        //   isSets: [
                        //     { id: "1", key: "port_serviced", type: "tool", withText: "Ports Serviced", "event": (key) => { // console.log(key) } },
                        //   ]
                        // },
                        // {
                        //   key: "s2",
                        //   isSets: [
                        //     { id: "1", key: "agent_expenses", type: "team", withText: "Agent Expenses", "event": (key) => { // console.log(key) } },
                        //     { id: "2", key: "reports", type: "solution", withText: "Print", "event": (key) => { // console.log(key) } }
                        //   ]
                        // }
                      ],
                      isRightBtn: [
                        {
                          isSets: [
                            {
                              key: 'report',
                              isDropdown: 0,
                              withText: 'Report',
                              type: '',
                              menus: null,
                              event: (key) => this.PortReport(true),
                            },
                          ],
                        },
                      ],
                      isResetOption: false,
                    },
                  ]}
                  inlineLayout={true}
                  // extraTableButton={{
                  //   "Remittance Information": [{"icon": "upload", "onClickAction": (action) => { // console.log(action) }}],
                  //   "Contacts": [{"icon": "upload", "onClickAction": (action) => { // console.log(action) }}]
                  // }}
                />
              </div>
            </div>
          </article>
        ) : undefined}

        {isShowPortReport ? (
          <Modal
            style={{ top: '2%' }}
            title="Report"
            open={isShowPortReport}
            onOk={this.handleOk}
            onCancel={() => this.PortReport(false)}
            width="95%"
            footer={null}
          >
            <PortReport />
          </Modal>
        ) : undefined}
      </div>
    );
  }
}

export default PortForm;
