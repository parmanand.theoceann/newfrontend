import React from 'react'
import { Form, Input, Checkbox, Select, Upload, Button,  } from 'antd'
import { UploadOutlined } from '@ant-design/icons';
const { Option } = Select;

class BarForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const props = {
      name: 'file',
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      headers: {
        authorization: 'authorization-text',
      },
    }
    
    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item>
          <Checkbox defaultChecked={false}>Internal</Checkbox>
        </Form.Item>
        <Form.Item>
          <Checkbox defaultChecked>Inactive</Checkbox>
        </Form.Item>
        <Form.Item label="Cross-Ref No">
          {getFieldDecorator('ref-no', {
            rules: [
              {
                type: 'text',
                message: 'Provide reference no',
              },
              {
                required: true,
                message: 'Please input reference no!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Parent Company">
          <Select defaultValue="1" style={{ width: 300 }}>
            <Option value="1">Company 1</Option>
            <Option value="2">Company 2</Option>
            <Option value="3">Company 3</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Payment Company">
          <Select defaultValue="1" style={{ width: 300 }}>
            <Option value="1">Company 1</Option>
            <Option value="2">Company 2</Option>
            <Option value="3">Company 3</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Logo File" >
          <Upload {...props}>
            <Button>
            <UploadOutlined /> Click to Upload
            </Button>
          </Upload>,
        </Form.Item>

      </Form>
    )
  }
}

// const WrappedBarForm = Form.create({ name: 'barform' })(BarForm);
const WrappedBarForm = BarForm;
export default WrappedBarForm;