import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="https://erp.theoceann.com/" target="_blank" rel="noopener noreferrer">Oseanwave</a>
        <span className="ml-1">&copy; 2021 Oseanwave.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        TheOceann
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
