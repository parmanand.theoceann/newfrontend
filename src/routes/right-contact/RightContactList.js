
import React, { Component } from 'react';
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon } from '../../shared';
import { Collapse, Spin, Alert } from 'antd';
const { Panel } = Collapse;

 
class RightContactList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      addvisible: false,
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
     data:null,
     expandIconPosition: 'right',
     frmVisible:false

    }
  }



  componentDidMount = async () => {
    this.fetchData();
  }

  fetchData = async () => {
    if(this.props.voyageManagerId){

    const response = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/contactlist?ae=${this.props.voyageManagerId}`);
      const respData = await response;
      this.setState({
        data:respData.data,
        frmVisible:true
      })
    }

  };

  render() {
const{expandIconPosition,data}=this.state

    return (
      <div className="body-wrapper modalWrapper m-0">
        <Collapse defaultActiveKey={['1']} accordion expandIconPosition={expandIconPosition}>
          <Panel header="My Company" key="1">
            {data ?
              <div className="box-body" >
                <h4 className="m-0 text-primary"><strong>Company Name:</strong> {data.companyName ? data.companyName :'--'}</h4>
                <p className="m-0"><strong>Phone No:</strong> {data.phone_no ? data.phone_no :'--'}</p>
                <p className="m-0"><strong>Company Type:</strong> {data.company_type ? data.company_type : '--'}</p>
              </div>
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Owner" key="2">
          {data && data.owner ?
              <div className="box-body" >
                <h4 className="m-0 text-primary"><strong>Owner Name:</strong> {data.owner.owner_name ? data.owner.owner_name : '--'}</h4>
                <p className="m-0"><strong>Phone No:</strong> {data.owner.phone_number ? data.owner.phone_number :'--'}</p>
                <p className="m-0"><strong>Company Type:</strong> {data.owner.company_type ? data.owner.company_type :'--'}</p>
                <p className="m-0"><strong>Email:</strong> {data.owner.email ? data.owner.email :'--'}</p>
              </div>
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Charterer" key="3">
            {data && data.charterer && data.charterer.length>0 ?
              data.charterer.map((item, i) => <div className="box box-default mb-2" key={i}>
               <div className="box-body" >
                <h4 className="m-0 text-primary"><strong>Charterer Name:</strong> {item.charterer_name ? item.charterer_name :'--'}</h4>
                <p className="m-0"><strong>Phone No:</strong> {item.phone_number ? item.phone_number : '--'}</p>
                <p className="m-0"><strong>Company Type:</strong> {item.company_type ? item.company_type :'--'}</p>
                <p className="m-0"><strong>Email:</strong> {item.email? item.email : '--'}</p>
                </div>
              </div>)
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Broker Commsion" key="4">
            {data && data.broker_commsion && data.broker_commsion.length > 0 ?
              data.broker_commsion.map((item, i) => <div className="box box-default mb-2" key={i}>
                <div className="box-body" >
                  <h4 className="m-0 text-primary"><strong>Commission Name:</strong> {item.commission_name ? item.commission_name : '--'}</h4>
                  <p className="m-0"><strong>Phone No:</strong> {item.phone_number ? item.phone_number : '--'}</p>
                  <p className="m-0"><strong>Company Type:</strong> {item.company_type ? item.company_type : '--'}</p>
                  <p className="m-0"><strong>Email:</strong> {item.email ? item.email : '--'}</p>
                </div>
              </div>)
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Agent" key="5">
            {data && data.agentName&&data.agentName.length>0 ?
              data.agentName.map((item,i)=>(
                <div className="box box-default mb-2" key={i}>
                <div className="box-body" >
                {/* <h4 className="m-0 text-primary"><strong>Agent Name:</strong> {data.agentName.agent_full_namename ? data.agentName.agent_full_namename : '--'}</h4> */}
                <h4 className="m-0 text-primary"><strong>Agent Name:</strong> {item["agentName"] ? item["agentName"] : '--'}</h4>

                </div>
              </div>
              ))
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Vessel" key="6">
            {data ?
              <div className="box-body" >
                <h4 className="m-0 text-primary"><strong>Vessel Name:</strong> {data.vessel_name ? data.vessel_name : '--'}</h4>
              </div>
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
          <Panel header="Other Vendor" key="7">
            {data && data.other_vendor&&data.other_vendor.length>0 ?
              data.other_vendor.map((item,i)=>(
                <div className="box box-default mb-2" key={i}>
                <div className="box-body" >
                {/* <h4 className="m-0 text-primary"><strong>Agent Name:</strong> {data.agentName.agent_full_namename ? data.agentName.agent_full_namename : '--'}</h4> */}
                <h4 className="m-0 text-primary"><strong>Other Vendor:</strong> {item["vendor_name"] ? item["vendor_name"] : '--'}</h4>

                </div>
              </div>
              ))
              :
              <div className="col col-lg-12">
                <h4>No Data</h4>
              </div>
            }
          </Panel>
        </Collapse>
          {/* <div className="box box-default mb-2">
            <div className="box-body">
                <h4 className="m-0">Seatide Maritime</h4>
                <p className="m-0">Charterers</p>
                <p className="m-0">Vessel Owner</p>
                <p className="text-primary m-0">contact@gmail.com <span className="float-right" >2325232556</span></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <h4 className="m-0">Seatide Maritime</h4>
                <p className="m-0">Charterers</p>
                <p className="m-0">Vessel Owner</p>
                <p className="text-primary m-0">contact@gmail.com <span className="float-right" >2325232556</span></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <h4 className="m-0">Seatide Maritime</h4>
                <p className="m-0">Charterers</p>
                <p className="m-0">Vessel Owner</p>
                <p className="text-primary m-0">contact@gmail.com <span className="float-right" >2325232556</span></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <h4 className="m-0">Seatide Maritime</h4>
                <p className="m-0">Charterers</p>
                <p className="m-0">Vessel Owner</p>
                <p className="text-primary m-0">contact@gmail.com <span className="float-right" >2325232556</span></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <h4 className="m-0">Seatide Maritime</h4>
                <p className="m-0">Charterers</p>
                <p className="m-0">Vessel Owner</p>
                <p className="text-primary m-0">contact@gmail.com <span className="float-right" >2325232556</span></p>
            </div>
          </div> */}

      </div>
    );
  }
}

export default RightContactList;


