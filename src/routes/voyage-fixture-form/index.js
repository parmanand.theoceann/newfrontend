import React, { Component } from 'react';
import { Form, Select, Input, Layout, Tabs, Icon, Drawer, Modal, Row, Col } from 'antd';
import RightBarUI from '../../components/RightBarUI';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
  apiDeleteCall,
  getAPICall
} from '../../shared';
import RightSummaryFixture from '../right-summary-fixture/RightSummaryFixture';
import CargoListing from './CargoListing';
import Itinerary from './components/Itinerary';
import PortDateDetails from './components/PortDateDetails';
import BunkerDetails from './components/BunkerDetails';
import PLMainSummary from '../../shared/components/Estimates/PLMainSummary';
import RightLog from '../../routes/right-log/RightLog';
import RightInvoice from '../right-invoice/RightInvoice';
import RightContactList from '../right-contact/RightContactList';
import RightProperties from '../right-properties/RightProperties';
import RightTaskAlert from '../right-task-alert/RightTaskAlert';
import VoyageFixtureReport from '../operation-reports/VoyageFixtureReport';
import FullEstimate from '../chartering/routes/voy-relet/FullEstimate';
import { DeleteOutlined, MenuFoldOutlined, SaveOutlined } from '@ant-design/icons';

const { Content } = Layout;
const FormItem = Form.Item;
const Option = Select.Option;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class VoyageFixtureForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      frmName: 'voyage_fixture_form',
      formData: this.props.formData,
      frmVisible: this.props.frmVisible || true,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      isShowVoyageFixtureReport: false,
      extraFormFields:null,
      isShowEstimate:false,
      estimateID:0,
      selectedID: null,
    };
  }

 async componentDidMount () {
    // this._calculateExtraFormFields()
  }

  _calculateExtraFormFields = () => {
    const { formData } = this.state;
    let extraFormFieldsData = { totalDistance: 0, ttlPortDays: 0, tsd: 0, ttlQty: 0, gsd: 0 };
    if (formData && formData.hasOwnProperty("id") && formData.id > 0) {
      const { portitinerary } = formData;
      this.setState({ ...this.state, frmVisible: false, });
      portitinerary &&portitinerary.map(item => {
        extraFormFieldsData.totalDistance += item.miles;
        extraFormFieldsData.ttlPortDays += item.t_port_days;
        extraFormFieldsData.tsd += parseFloat(item.tsd);
        extraFormFieldsData.ttlQty += item.l_d_qty;
        extraFormFieldsData.gsd += parseFloat(item.gsd);
      })

      this.setState({
        ...this.state,
        frmVisible: true,
        extraFormFieldsData,
        extraFormFields: { isShowInMainForm: true, "content": this.getExternalFormFields(extraFormFieldsData) }
      });
    } else {
      this.setState({
        ...this.state,
        extraFormFields: { isShowInMainForm: true, "content": this.getExternalFormFields(extraFormFieldsData) }
      });
    }
  }

  VoyageFixtureReport = (showVoyageFixtureReport) =>
    this.setState({ ...this.state, isShowVoyageFixtureReport: showVoyageFixtureReport });
  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });
  _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon('error', 'Cargo Id is empty. Kindly check it again!');
    }
    Modal.confirm({
      title: 'Confirm',
      content: 'Are you sure, you want to delete it?',
      onOk: () => this._onDelete(postData),
    });
  };

  _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/voyage-fixture/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.setState({ ...this.state, frmVisible: false }, () => {
          this.setState({
            ...this.state,
            formData: this.formDataValue,
            showSideListBar: true,
            frmVisible: true,
          });
        });
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };
  onClickRightMenu = (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case 'summary':
        loadComponent = <RightSummaryFixture />;
        break;
      case 'pl-summary':
        loadComponent = <PLMainSummary />;
        break;
      case 'log':
        loadComponent = <RightLog />;
        break;
      case 'invoice':
        loadComponent = <RightInvoice />;
        break;
      case 'contact':
        loadComponent = <RightContactList />;
        break;
      case 'proprties':
        loadComponent = <RightProperties />;
        break;
      case 'task-alert':
        loadComponent = <RightTaskAlert />;
        break;
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };
  saveFormData = (postData) => {
    const { frmName } = this.state;
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty('id') && postData["id"] > 0) {
      _url = "update";
      _method = "put";
    }
    if(postData && postData.hasOwnProperty('..'))
    delete postData['..']
    if(postData && postData.hasOwnProperty('g'))
    delete postData.g

    postAPICall(`${URL_WITH_VERSION}/voyagefixture/${_url}?frm=${frmName}`, postData, _method, (data) => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        this.setState({
          ...this.state,
          formData: Object.assign({
            "id": -1
          }, (this.props.formData || {}))
        })
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== "string") {
          Object.keys(dataMessage).map(i => msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>");
        } else {
          msg += dataMessage
        }

        msg += "</div>"
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />)
      }
    });
  }

  _onEditDataLoad = async (coaVciId) => {
    const { history } = this.props;
    this.setState({
      ...this.state, "frmVisible": false, extraFormFields: {
        isShowInMainForm: false,
        "content": undefined
      }
    }, () => history.push(`/voyage-fixture/${coaVciId.id}`));
    const response = await getAPICall(`${URL_WITH_VERSION}/voyagefixture/edit?ae=${coaVciId.id}`);
    const data = await response['data'];
    this.setState({ ...this.state, formData: data,frmVisible:true, selectedID: coaVciId.id}, () => {
      // this._calculateExtraFormFields();
    });
  }

  getExternalFormFields = (extraFormFieldsData) => {
    return <article className="article">
      <div className="box-body">
        <div className="row p10">
          <div className="col-md-4">
            <div className="row">
              <div className="col-md-8">
                <p>
                  <b>Total Distance :</b>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  {extraFormFieldsData && extraFormFieldsData.totalDistance ? extraFormFieldsData.totalDistance.toFixed(2) : 0}
                  <span className="float-right">
                    <b>Miles</b>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="row">
              <div className="col-md-8">
                <p>
                  <b>TTL Port Days :</b>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  {extraFormFieldsData && extraFormFieldsData.ttlPortDays ? extraFormFieldsData.ttlPortDays.toFixed(2) : 0}
                  <span className="float-right">
                    <b>Days</b>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="row">
              <div className="col-md-8">
                <p>
                  <b>TSD :</b>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  {extraFormFieldsData && extraFormFieldsData.tsd ? extraFormFieldsData.tsd.toFixed(2) : 0}
                  <span className="float-right">
                    <b>Days</b>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="row">
              <div className="col-md-8">
                <p>
                  <b>TTL Qty :</b>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  {extraFormFieldsData && extraFormFieldsData.ttlQty?extraFormFieldsData.ttlQty.toFixed(2):" "}
                  <span className="float-right">
                    <b>Mt</b>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="row">
              <div className="col-md-8">
                <p>
                  <b>GSD :</b>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  {extraFormFieldsData && extraFormFieldsData.gsd ? extraFormFieldsData.gsd.toFixed(2) : 0}
                  <span className="float-right">
                    <b>Days</b>
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  }

  showEstimate = (bolVal, estimateID) =>  this.setState({ ...this.state, estimateID: estimateID }, () => this.setState({ ...this.state, isShowEstimate: bolVal }));

  redirectToAdd = async (vessel) => {
    const { history } = this.props;
    const response = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/list?l=0`);
    const data = await response['data'];
    let voyageData = data.filter(el => el.voyage_number == vessel);
    if(voyageData && voyageData.length > 0){
      history.push(`/voyage-manager/${voyageData[0].id}`); 
    }
  }


  render() {
    const {
      frmName,
      loadComponent,
      title,
      visibleDrawer,
      isShowVoyageFixtureReport,
      formData,
      frmVisible,
      extraFormFields,
      isShowEstimate,
      estimateID,
      selectedID
    } = this.state;
    return (
      <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <Row gutter={16} style={{ marginRight: 0 }}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="body-wrapper">
                    <article className="article toolbaruiWrapper">
                      <div className="box box-default">
                        <div className="box-body">
                          {frmVisible ?
                          <NormalFormIndex
                            key={'key_' + frmName + '_0'}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            showToolbar={[
                              {
                                isLeftBtn: [
                                  {
                                    isSets: [
                                      { id: "7", key: "menu-fold", type: <MenuFoldOutlined/>, withText: "", "event": "menu-fold" },
                                      {
                                        id: '3',
                                        key: 'save',
                                        type: <SaveOutlined />,
                                        withText: '',
                                        event: (key, data) => this.saveFormData(data),
                                      },

                                      // {
                                      //   id: '4',
                                      //   key: 'delete',
                                      //   type: <DeleteOutlined />,
                                      //   withText: '',
                                      //   event: (key, data) => this._onDeleteFormData(data),
                                      // },
                                    ],
                                  },
                                ],
                                isRightBtn: [
                                  {
                                    isSets: [
                                      {
                                        key: 'estimate',
                                        isDropdown: 0,
                                        withText: 'Estimate',
                                        type: '',
                                        menus: null,
                                        event: (key, data) => {data && data['estimate_id'] ?  this.showEstimate(true, data['estimate_id']) :  openNotificationWithIcon('info', 'Please Select any Voyage Fixture from list!')}
                                      },

                                      {
                                        key: 'open_voyage',
                                        isDropdown: 0,
                                        withText: ' Open Voyage',
                                        type: '',
                                        menus: null,
                                        event: (key, data) => {data && data['voyage_no'] ?  this.redirectToAdd(data['voyage_no']) :  openNotificationWithIcon('info', 'Please Select any Voyage Fixture from list!')}
                                      },

                                      {
                                        key: 'attachment',
                                        isDropdown: 0,
                                        withText: 'Attachment',
                                        type: '',
                                        menus: null,
                                      },

                                      {
                                        key: 'report',
                                        isDropdown: 0,
                                        withText: ' Report',
                                        type: '',
                                        menus: null,
                                        event: (key) => this.VoyageFixtureReport(true),
                                      },
                                    ],
                                  },
                                ],
                              },
                            ]}
                            // voyage-manager/list?l=0
                            inlineLayout={true}
                            isShowFixedColumn={['Cargos', '..']}
                            
                            sideList={{
                              selectedID:selectedID,
                              showList: true,
                              title: 'Fixture List',
                              uri: '/voyagefixture/list?l=0',
                              columns: [
                                'fixture_no', 'contract_type',
                                'vessel_name',
                                'voyage_status',
                              ],
                              icon: true,
                              // searchString: '"TCI Code","TCI Status","Vessel Name","Owner Name"',
                              rowClickEvent: (evt) => this._onEditDataLoad(evt),
                            }}
                            extraFormFields={extraFormFields}

                            // isShowFixedColumn={'PORT_INTINERY', 'CARGO', 'BUNKER_DETAILS', 'PORT_DATE_DETAILS'}
                            // showSideListBar={null}
                          />
                          :
                          undefined
  }
                        </div>
                      </div>
                    </article>
                  </div>
                </Col>
              </Row>
            </Content>
          </Layout>

          <RightBarUI
            pageTitle="voyage-fixture-righttoolbar"
            callback={(data, options) => this.onClickRightMenu(data, options)}
          />

          {loadComponent !== undefined && title !== undefined && visibleDrawer === true ? (
            <Drawer
              title={this.state.title}
              placement="right"
              closable={true}
              onClose={this.onCloseDrawer}
             open={this.state.visibleDrawer}
              getContainer={false}
              style={{ position: 'absolute' }}
              width={this.state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  <div className="content-wrapper noHeight">{this.state.loadComponent}</div>
                </div>
              </div>
            </Drawer>
          ) : undefined}
        </Layout>

        {isShowVoyageFixtureReport ? (
          <Modal
            style={{ top: '2%' }}
            title="Reports"
           open={isShowVoyageFixtureReport}
            onOk={this.handleOk}
            onCancel={() => this.VoyageFixtureReport(false)}
            width="95%"
            footer={null}
          >
            <VoyageFixtureReport />
          </Modal>
        ) : undefined}
           {isShowEstimate && estimateID &&
            <Modal
              style={{ top: '2%' }}
             open={isShowEstimate}
              title="Estimate Detail"
              onCancel={() => this.showEstimate(false, 0)}
              footer={null}
              width={'90%'}
              maskClosable={false}
            >
              <FullEstimate estimateID={estimateID} />
            </Modal>
           }
      </div>
    );
  }
}

export default VoyageFixtureForm;
