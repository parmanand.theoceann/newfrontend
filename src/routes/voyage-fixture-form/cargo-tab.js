import React, { Component } from 'react';
import { Form, Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        port: "Port",
        miles: "Miles",
        spd: "SPD",
        sdays: "SDays",
        xsd: "XSD",
        f: "F",
        cargo: "Cargo",
        ldqty: "L/D Qty",
        ldrate: "L/D Rate",
        c: "C",
        terms: "Terms",
        tt: "TT",
        pdays: "PDays",
        estpd: "EstPD",
        curr: "Curr",
        portex: "PortEx",
        baseex: "BaseEx",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);

class CargoTab extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Port',
            dataIndex: 'port',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'port'),
        },
        {
            title: 'Miles',
            dataIndex: 'miles',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'miles'),
        },
        {
            title: 'Spd',
            dataIndex: 'spd',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'spd'),
        },
        {
            title: 'SDays',
            dataIndex: 'sdays',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'sdays'),
        },
        {
            title: 'XSD',
            dataIndex: 'xsd',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'xsd'),
        },
        {
            title: 'F',
            dataIndex: 'f',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'f'),
        },
        {
            title: 'Cargo',
            dataIndex: 'cargo',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'cargo'),
        },
        {
            title: 'L/D Qty',
            dataIndex: 'ldqty',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'ldqty'),
        },
        {
            title: 'L/D Rate',
            dataIndex: 'ldrate',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'ldrate'),
        },
        {
            title: 'C',
            dataIndex: 'c',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'c'),
        },
        {
            title: 'Terms',
            dataIndex: 'terms',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'terms'),
        },
        {
            title: 'TT',
            dataIndex: 'tt',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'tt'),
        },
        {
            title: 'PDays',
            dataIndex: 'pdays',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'pdays'),
        },
        {
            title: 'EstPD',
            dataIndex: 'estpd',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'estpd'),
        },
        {
            title: 'Curr',
            dataIndex: 'curr',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'curr'),
        },
        {
            title: 'PortEx',
            dataIndex: 'portex',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'portex'),
        },
        {
            title: 'BaseEx',
            dataIndex: 'baseex',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'baseex'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <a className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                    <a className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </a>
                                </span>
                                : <a className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></a>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 800, y: 300 }}
                    size="small"
                    pagination={false}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default CargoTab;