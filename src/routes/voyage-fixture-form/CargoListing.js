import React, { Component } from 'react';
import {SaveOutlined,DeleteOutlined,EditOutlined} from '@ant-design/icons';
import { Table, Popconfirm, Input,  Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        n: "1",
        id: "Cargo ID",
        charterer: "Charterer",
        cp_qty: "C/P Qty",
        unit: "Unit",
        opt_percentage: "Opt %",
        opt_type: "Opt Type",
        f_type: "Fraight Type",
        frt_rate: "Frt Rate",
        lumpsum: "Lumpsum",
        comm_percetage: "25%",
        extra_rev: "Extra Rev",
        curr: "Curr",
        dem_rate: "Dem Rate (Day)",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

/*const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};*/

//function onChange(checkedValues) {
// console.log('checked = ', checkedValues);
//}

//const plainOptions = ['Pool', 'Ice', 'Clean', 'Coated'];

class CargoesList extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'S/N',
            dataIndex: 'n',
            width: 50,
            render: (text, record) => this.renderColumns(text, record, 'n'),
        },
        {
            title: 'Cargo ID',
            dataIndex: 'id',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'id'),
        },
        {
            title: 'Charterer',
            dataIndex: 'charterer',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'charterer'),
        },
        {
            title: 'CP Qty',
            dataIndex: 'cp_qty',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'cp_qty'),
        },
        {
            title: 'Unit',
            dataIndex: 'unit',
            width: 60,
            render: (text, record) => this.renderColumns(text, record, 'unit'),
        },
        {
            title: 'Opt %',
            dataIndex: 'opt_percentage',
            width: 60,
            render: (text, record) => this.renderColumns(text, record, 'opt_percentage'),
        },
        {
            title: 'Opt Type',
            dataIndex: 'opt_type',
            width: 80,
            render: (text, record) => this.renderColumns(text, record, 'opt_type'),
        },
        {
            title: 'Freight Type',
            dataIndex: 'f_type',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'f_type'),
        },
        {
            title: 'Frt Rate',
            dataIndex: 'frt_rate',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'frt_rate'),
        },
        {
            title: 'Lumpsum',
            dataIndex: 'lumpsum',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'lumpsum'),
        },
        {
            title: 'Comm%',
            dataIndex: 'comm_percetage',
            width: 80,
            render: (text, record) => this.renderColumns(text, record, 'comm_percetage'),
        },
        {
            title: 'Extra Rev',
            dataIndex: 'extra_rev',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'extra_rev'),
        },
        {
            title: 'Curr',
            dataIndex: 'curr',
            width: 50,
            render: (text, record) => this.renderColumns(text, record, 'curr'),
        },
        {
            title: 'Dem.Rate (Day)',
            dataIndex: 'dem_rate',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'dem_rate'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 60,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <a href={{}} className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                    <a href={{}} className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> 
                                        </Popconfirm>
                                    </a>
                                </span>
                                : <a className="iconWrapper edit" href={{}} onClick={() => this.edit(record.key)}><EditOutlined /></a>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>

                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 800, y: 100 }}
                    size="small"
                    pagination={false}

                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />



            </>
        )
    }
}

export default CargoesList;