import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Port ID',
    dataIndex: 'portid',
    width: 80,
  },

  {
    title: 'Port',
    dataIndex: 'port',
    width: 150,
  },

  {
    title: 'Funct.',
    dataIndex: 'funct',
    width: 100,
  },

  {
    title: 'Miles',
    dataIndex: 'miles',
    width: 80,
  },

  {
    title: 'Passage',
    dataIndex: 'passage',
    width: 100,
  },

  {
    title: 'Stype',
    dataIndex: 'stype',
    width: 80,
  },

  {
    title: 'WF%',
    dataIndex: 'wf',
    width: 80,
  },

  {
    title: 'SPD',
    dataIndex: 'spd',
    width: 100,
  },

  {
    title: 'Eff-SPD',
    dataIndex: 'effspd',
    width: 100,
  },

  {
    title: 'GSD',
    dataIndex: 'gsd',
    width: 80,
  },

  {
    title: 'TSD',
    dataIndex: 'tsd',
    width: 80,
  },

  {
    title: 'XSD',
    dataIndex: 'xsd',
    width: 80,
  },

  {
    title: 'L/D QTY',
    dataIndex: 'ldqty',
    width: 80,
  },

  {
    title: 'L/D Rate(D)',
    dataIndex: 'ldrated',
    width: 100,
  },

  {
    title: 'L/D Rate(H)',
    dataIndex: 'ldrateh',
    width: 100,
  },

  {
    title: 'L/D Term',
    dataIndex: 'ldterm',
    width: 100,
  },

  {
    title: 'TurnTime',
    dataIndex: 'turntime',
    width: 100,
  },

  {
    title: 'P Days',
    dataIndex: 'pdays',
    width: 80,
  },

  {
    title: 'Xpd',
    dataIndex: 'xpd',
    width: 80,
  },

  {
    title: 'P.EXP',
    dataIndex: 'pexp',
    width: 80,
  },

  {
    title: 'TP Days',
    dataIndex: 'totalportdays',
    width: 80,
  },

  {
    title: 'Currency',
    dataIndex: 'currency',
    width: 80,
  },
];
const data = [
  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: '10,000',
    passage: 'Passage',
    stype: 'Stype',
    wf: 'WF%',
    spd: '00.00kt',
    effspd: 'Eff-SPD',
    gsd: 'GSD',
    tsd: 'TSD',
    xsd: 'XSD',
    ldqty: 'L/D QTY',
    ldrated: 'L/D Rate(D)',
    ldrateh: 'L/D Rate(H)',
    ldterm: 'L/D Term',
    turntime: 'TurnTime',
    pdays: 'P Days',
    xpd: 'Xpd',
    pexp: 'P.EXP',
    totalportdays: 'TP Days',
    currency: 'Currency',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: '10,000',
    passage: 'Passage',
    stype: 'Stype',
    wf: 'WF%',
    spd: '00.00kt',
    effspd: 'Eff-SPD',
    gsd: 'GSD',
    tsd: 'TSD',
    xsd: 'XSD',
    ldqty: 'L/D QTY',
    ldrated: 'L/D Rate(D)',
    ldrateh: 'L/D Rate(H)',
    ldterm: 'L/D Term',
    turntime: 'TurnTime',
    pdays: 'P Days',
    xpd: 'Xpd',
    pexp: 'P.EXP',
    totalportdays: 'TP Days',
    currency: 'Currency',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: '10,000',
    passage: 'Passage',
    stype: 'Stype',
    wf: 'WF%',
    spd: '00.00kt',
    effspd: 'Eff-SPD',
    gsd: 'GSD',
    tsd: 'TSD',
    xsd: 'XSD',
    ldqty: 'L/D QTY',
    ldrated: 'L/D Rate(D)',
    ldrateh: 'L/D Rate(H)',
    ldterm: 'L/D Term',
    turntime: 'TurnTime',
    pdays: 'P Days',
    xpd: 'Xpd',
    pexp: 'P.EXP',
    totalportdays: 'TP Days',
    currency: 'Currency',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: '10,000',
    passage: 'Passage',
    stype: 'Stype',
    wf: 'WF%',
    spd: '00.00kt',
    effspd: 'Eff-SPD',
    gsd: 'GSD',
    tsd: 'TSD',
    xsd: 'XSD',
    ldqty: 'L/D QTY',
    ldrated: 'L/D Rate(D)',
    ldrateh: 'L/D Rate(H)',
    ldterm: 'L/D Term',
    turntime: 'TurnTime',
    pdays: 'P Days',
    xpd: 'Xpd',
    pexp: 'P.EXP',
    totalportdays: 'TP Days',
    currency: 'Currency',
  },
];

class Itinerary extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <Table
          bordered
          columns={columns}
          dataSource={data}
          pagination={false}
          scroll={{ x: 1900 }}
          footer={() => (
            <div className="text-center">
              <Button type="link">Add New</Button>
            </div>
          )}
        />
      </div>
    );
  }
}

export default Itinerary;
