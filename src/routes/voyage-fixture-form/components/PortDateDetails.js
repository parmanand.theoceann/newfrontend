import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Port ID',
    dataIndex: 'portid',
    width: 80,
  },

  {
    title: 'Port',
    dataIndex: 'port',
    width: 150,
  },

  {
    title: 'Funct.',
    dataIndex: 'funct',
    width: 80,
  },

  {
    title: 'Miles',
    dataIndex: 'miles',
    width: 80,
  },

  {
    title: 'Passage',
    dataIndex: 'passage',
    width: 100,
  },

  {
    title: 'Stype',
    dataIndex: 'stype',
    width: 80,
  },

  {
    title: 'SPD',
    dataIndex: 'spd',
    width: 80,
  },

  {
    title: 'WF%',
    dataIndex: 'wf',
    width: 80,
  },


  {
    title: 'TSD',
    dataIndex: 'tsd',
    width: 80,
  },

  {
    title: 'XSD',
    dataIndex: 'xsd',
    width: 80,
  },

  {
    title: 'Arrival Date',
    dataIndex: 'arrivaldatetime',
    width: 100,
  },

  {
    title: 'Day',
    dataIndex: 'day',
    width: 100,
  },

  {
    title: 'T.PDays',
    dataIndex: 'tpdays',
    width: 100,
  },

  {
    title: 'Departure Date',
    dataIndex: 'departure',
  },

];
const data = [
  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    stype: 'Stype',
    spd: '00.00kt',
    wf: 'WF%',
    tsd: 'TSD',
    xsd: 'XSD',
    arrivaldatetime: 'Arrival Date',
    day: 'Day',
    tpdays: 'T.PDays',
    departure: 'Departure Date',

  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    stype: 'Stype',
    spd: '00.00kt',
    wf: 'WF%',
    tsd: 'TSD',
    xsd: 'XSD',
    arrivaldatetime: 'Arrival Date',
    day: 'Day',
    tpdays: 'T.PDays',
    departure: 'Departure Date',

  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    funct: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    stype: 'Stype',
    spd: '00.00kt',
    wf: 'WF%',
    tsd: 'TSD',
    xsd: 'XSD',
    arrivaldatetime: 'Arrival Date',
    day: 'Day',
    tpdays: 'T.PDays',
    departure: 'Departure Date',

  },


];

class PortDateDetails extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <Table
          bordered
          columns={columns}
          dataSource={data}
          pagination={false}
          scroll={{ x: 1315 }}

          footer={() => (
            <div className="text-center">
              <Button type="link">Add New</Button>
            </div>
          )}
        />
      </div>
    );
  }
}

export default PortDateDetails;

