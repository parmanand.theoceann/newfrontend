import React, { Component } from 'react';
import { Table } from 'antd';

const { Column, ColumnGroup } = Table;

const data = [
  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    f: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    spdtype: 'SPD Type',
    spd: 'SPD',
    arrivaldatetime: '01/01/2021 02:25',
    departure: 'Departure',
    ifo: 'IFO',
    vlsfo: 'VLSFO',
    ulsfo: 'ULSFO',
    lsmgo: 'LSMGO',
    mgo: 'MGO',
    aifo: 'IFO',
    avlsfo: 'VLSFO',
    alsmgo: 'LSMGO',
    amgo: 'MGO',
    aulsfo: 'ULSFO',
    pifo: 'IFO',
    pvlsfo: 'VLSFO',
    plsmgo: 'LSMGO',
    pmgo: 'MGO',
    pulsfo: 'ULSFO',
    rifo: 'IFO',
    rvlsfo: 'VLSFO',
    rlsmgo: 'LSMGO',
    rmgo: 'MGO',
    rulsfo: 'ULSFO',
    difo: 'IFO',
    dlsfo: 'LSFO',
    dlsmgo: 'LSMGO',
    dmgo: 'MGO',
    dulsfo: 'ULSFO',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    f: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    spdtype: 'SPD Type',
    spd: 'SPD',
    arrivaldatetime: '12/04/2021 02:25',
    departure: 'Departure',
    ifo: 'IFO',
    vlsfo: 'VLSFO',
    ulsfo: 'ULSFO',
    lsmgo: 'LSMGO',
    mgo: 'MGO',
    aifo: 'IFO',
    avlsfo: 'VLSFO',
    alsmgo: 'LSMGO',
    amgo: 'MGO',
    aulsfo: 'ULSFO',
    pifo: 'IFO',
    pvlsfo: 'VLSFO',
    plsmgo: 'LSMGO',
    pmgo: 'MGO',
    pulsfo: 'ULSFO',
    rifo: 'IFO',
    rvlsfo: 'VLSFO',
    rlsmgo: 'LSMGO',
    rmgo: 'MGO',
    rulsfo: 'ULSFO',
    difo: 'IFO',
    dlsfo: 'LSFO',
    dlsmgo: 'LSMGO',
    dmgo: 'MGO',
    dulsfo: 'ULSFO',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    f: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    spdtype: 'SPD Type',
    spd: 'SPD',
    arrivaldatetime: '12/04/2021 02:25',
    departure: 'Departure',
    ifo: 'IFO',
    vlsfo: 'VLSFO',
    ulsfo: 'ULSFO',
    lsmgo: 'LSMGO',
    mgo: 'MGO',
    aifo: 'IFO',
    avlsfo: 'VLSFO',
    alsmgo: 'LSMGO',
    amgo: 'MGO',
    aulsfo: 'ULSFO',
    pifo: 'IFO',
    pvlsfo: 'VLSFO',
    plsmgo: 'LSMGO',
    pmgo: 'MGO',
    pulsfo: 'ULSFO',
    rifo: 'IFO',
    rvlsfo: 'VLSFO',
    rlsmgo: 'LSMGO',
    rmgo: 'MGO',
    rulsfo: 'ULSFO',
    difo: 'IFO',
    dlsfo: 'LSFO',
    dlsmgo: 'LSMGO',
    dmgo: 'MGO',
    dulsfo: 'ULSFO',
  },

  {
    key: '1',
    portid: 'Port ID',
    port: 'Port',
    f: 'Funct.',
    miles: 'Miles',
    passage: 'Passage',
    spdtype: 'SPD Type',
    spd: 'SPD',
    arrivaldatetime: '12/04/2021 02:25',
    departure: 'Departure',
    ifo: 'IFO',
    vlsfo: 'VLSFO',
    ulsfo: 'ULSFO',
    lsmgo: 'LSMGO',
    mgo: 'MGO',
    aifo: 'IFO',
    avlsfo: 'VLSFO',
    alsmgo: 'LSMGO',
    amgo: 'MGO',
    aulsfo: 'ULSFO',
    pifo: 'IFO',
    pvlsfo: 'VLSFO',
    plsmgo: 'LSMGO',
    pmgo: 'MGO',
    pulsfo: 'ULSFO',
    rifo: 'IFO',
    rvlsfo: 'VLSFO',
    rlsmgo: 'LSMGO',
    rmgo: 'MGO',
    rulsfo: 'ULSFO',
    difo: 'IFO',
    dlsfo: 'LSFO',
    dlsmgo: 'LSMGO',
    dmgo: 'MGO',
    dulsfo: 'ULSFO',
  },
];

class BunkerDetails extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <Table bordered dataSource={data} pagination={false} footer={false} scroll={{ x: 2700 }}>
          <Column title="Port ID" dataIndex="portid" width="80px" />
          <Column title="Port" dataIndex="port" width="150px" />
          <Column title="Funct." dataIndex="f" width="100px" />
          <Column title="Miles" dataIndex="miles" width="100px" />
          <Column title="Passage" dataIndex="passage" width="80px" />
          <Column title="SPD Type" dataIndex="spdtype" width="100px" />
          <Column title="SPD" dataIndex="spd" width="80px" />
          <Column title="Arrival" dataIndex="arrivaldatetime" width="130px" />
          <Column title="Departure" dataIndex="departure" width="120px" />

          <ColumnGroup title="Fuel Grade (Sea Cons. In MT)">
            <Column title="IFO" dataIndex="ifo" />
            <Column title="VLSFO" dataIndex="vlsfo" />
            <Column title="LSMGO" dataIndex="lsmgo" />
            <Column title="MGO" dataIndex="mgo" />
            <Column title="ULSFO" dataIndex="ulsfo" />
          </ColumnGroup>

          <ColumnGroup title="Arrival ROB">
            <Column title="IFO" dataIndex="aifo" />
            <Column title="VLSFO" dataIndex="avlsfo" />
            <Column title="LSMGO" dataIndex="alsmgo" />
            <Column title="MGO" dataIndex="amgo" />
            <Column title="VLSFO" dataIndex="aulsfo" />
          </ColumnGroup>

          <ColumnGroup title="Port Cons. Fuel">
            <Column title="IFO" dataIndex="pifo" />
            <Column title="VLSFO" dataIndex="pvlsfo" />
            <Column title="LSMGO" dataIndex="plsmgo" />
            <Column title="MGO" dataIndex="pmgo" />
            <Column title="ULSFO" dataIndex="pulsfo" />
          </ColumnGroup>

          <ColumnGroup title="Received">
            <Column title="IFO" dataIndex="rifo" />
            <Column title="VLSFO" dataIndex="rvlsfo" />
            <Column title="LSMGO" dataIndex="rlsmgo" />
            <Column title="MGO" dataIndex="rmgo" />
            <Column title="ULSFO" dataIndex="rulsfo" />

          </ColumnGroup>

          <ColumnGroup title="DEP.ROB">
            <Column title="IFO" dataIndex="difo" />
            <Column title="VLSFO" dataIndex="dvlsfo" />
            <Column title="LSMGO" dataIndex="dlsmgo" />
            <Column title="MGO" dataIndex="dmgo" />
            <Column title="ULSFO" dataIndex="dulsfo" />
          </ColumnGroup>

        </Table>
      </div>
    );
  }
}

export default BunkerDetails;
