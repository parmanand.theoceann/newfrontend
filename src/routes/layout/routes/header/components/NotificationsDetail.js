import React, { useEffect, useState } from "react";
import styles from "../../../../../styles/notifications.module.css";
import { Select, Pagination, notification } from "antd";
// import { ClockCircleOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, { getAPICall } from "../../../../../shared";
// import { func } from "prop-types";
import DataTable from "react-data-table-component";
// import { compose } from "redux";
const NotificationDetailSection = () => {
  const { Option } = Select;
  const [data, setData] = useState([]);
  const [filterData, setFilterData] = useState([]);
  const [allNotificationTypes, setAllNotificationTypes] = useState([]);
  const getBackgroundColor = (index) => {
    const colors = ["blue", "green", "red"]; // Add more colors if needed
    const colorIndex = index % colors.length;
    return colors[colorIndex];
  };

  const columns = [
    {
      name: "",
      selector: (row, index) => row.msg,
      cell: (row, index) => {
        return (
          <>

            <div className={styles.notificationTab}>
              <div className={styles.photoDiv}>
                <div className={styles.photoContainer}>
                  {/* <img className={styles.image} src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Eo_circle_indigo_letter-g.svg/512px-Eo_circle_indigo_letter-g.svg.png?20200417142220" alt="" /> */}
                <p className={styles.nameHeader}>{row.msg && row.msg.split('by ')[1]?.slice(0,1)||''}</p>
                </div>
              </div>
              <div className={styles.headerDiv}>
                <div className={styles.notificationTime}>
                  <div
                    className={styles.notificationType}
                  >
                    {row.n_type}
                  </div>
                  <div className={styles.time}>
                    {/* <ClockCircleOutlined /> */}
                    <span className={styles.timeText}>{row.date}</span>
                  </div>
                </div>

                <div className={styles.notificationText}>
                  <p>{row.msg}</p>
                </div>
              </div>

            </div>
          </>
        );
      },
    },
  ];
  useEffect(() => {
    getAPICall(`${URL_WITH_VERSION}/notifications/list`).then((response) => {
      setData(response.data);
      setFilterData(response.data);
      const uniqueNTypes = [
        ...new Set(
          response.data.filter((obj) => obj.n_type).map((obj) => obj.n_type)
        ),
      ];
      setAllNotificationTypes(uniqueNTypes);
    });
  }, []);

  return (
    <>
      <div className={styles.filter}>
        
        <Select
          className={styles.selectTag}
          onChange={(e) => {
            let FilData = data.filter((item) => {
              return JSON.stringify(item)
                .toLowerCase()
                .includes(e.toLowerCase());
            });
            setFilterData(FilData);
          }}
          defaultValue="All"
        >
          <Option value="">All</Option>
          {allNotificationTypes.map((n_type, index) => (
            <Option key={index} title={n_type} value={n_type}>
              {n_type}
            </Option>
          ))}
        </Select>
        
      </div>
      <div className={styles.dataTable}>
        <DataTable
          columns={columns}
          noTableHead={true}
          data={filterData}
          pagination
          paginationPerPage={5}
          paginationRowsPerPageOptions={[5, 8]}
        />
      </div>
    </>
  );
};

export default NotificationDetailSection;
