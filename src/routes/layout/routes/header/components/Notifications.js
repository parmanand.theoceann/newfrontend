import styles from "../../../../../styles/notificationTab.module.css"
import React, { useEffect, useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import {
  ArrowRightOutlined,
  BellOutlined,
  ProfileOutlined,
} from "@ant-design/icons";
import { List, Tabs, Tag, Button, Popover } from "antd";
import DEMO from "../../../../../constants/demoData";
import URL_WITH_VERSION, { getAPICall } from "../../../../../shared";
import { openNotificationWithIcon } from "../../../../../shared";
const list = DEMO.list;
const TabPane = Tabs.TabPane;

const NotificationTab = ({ handleNavigate, data }) => {
  return (
    <List
    
      footer={
        <div
          style={{
            display: "flex",
            marginTop:"8px",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          <Button
            onClick={handleNavigate}
            className="no-link-style"
            style={{ background: "transparent",
          display:"flex",
        justifyContent:"center",
      alignItems:"center"}}
          >
            Read All <ArrowRightOutlined />
          </Button>
        </div>
      }
      itemLayout="horizontal"
      dataSource={data.slice(0, 5)}
      renderItem={(item) => (
        <List.Item style={{width:'270px',padding:'0',borderBottom: "1px solid #e0e0e0"}}>
          <div className={styles.notificationMain}>
              <div className={styles.iconSection}>
                <div className={styles.icon}
                >{item?.msg?.split('by ')[1]?.slice(0,1)||''}</div>
              </div>
              <div className={styles.textSection}>
                <div className="list-item__title" style={
                  {fontWeight:"bold", display:"flex",
                color:"#12406a"}
                  }>{item.n_type}</div>
                <div className="list-item__datetime" style={
                  {display:"flex", fontSize:"12px",
                lineHeight:"14px",
              marginBottom:"4px",
            fontWeight:"600"}
                  }>{item.msg}</div>
              </div>
          </div>
        </List.Item>
      )}
    />
  );
};

// const MessageTab = () => (
//   <List
//     footer={<a href={DEMO.link} className="no-link-style">Read All <ArrowRightOutlined /></a>}
//     itemLayout="horizontal"
//     dataSource={list.messages}
//     renderItem={item => (
//       <List.Item>
//         <div className="list-style-v1">
//           <div className="list-item">
//             <Avatar src={item.avatar} className="mr-3"/>
//             <div className="list-item__body">
//               <div className="list-item__title">{item.title}</div>
//               <div className="list-item__desc">{item.desc}</div>
//               <div className="list-item__datetime">{item.datetime}</div>
//             </div>
//           </div>
//         </div>
//       </List.Item>
//     )}
//   />
// );

const TaskTab = ({ handleNavigate }) => (
  <List
    footer={
      <div
        style={{
          display: "flex",
          marginTop:"8px",
            justifyContent: "flex-end",
            alignItems: "center",
        }}
      >
        <Button
          onClick={handleNavigate}
          className="no-link-style"
          style={{ background: "transparent" }}
        >
          Read All <ArrowRightOutlined />
        </Button>
      </div>
    }
    itemLayout="horizontal"
    // dataSource={list.tasks}
    dataSource={list.notifications}
    renderItem={(item) => (
      <List.Item>
        <div className="list-style-v1">
          <div className="list-item">
            <div className="list-item__body">
              <div className="list-item__title">
                {item.title} <Tag color={item.tagColor}>{item.tag}</Tag>
              </div>
              <div className="list-item__datetime">{item.desc}</div>
            </div>
          </div>
        </div>
      </List.Item>
    )}
  />
);


const PopoverTabs = ({ handleNavigate, data }) => (


  <Tabs animated={false}>
   
      <TabPane

        tab={
          <span>
             <BellOutlined />
            Notifications (5)
          </span>
        }
        key="1"
      >
        <NotificationTab handleNavigate={handleNavigate} data={data} />

      </TabPane>
      {/* <TabPane tab={<span><Icon type="message" />Messages (3)</span>} key="2">
      <MessageTab />
    </TabPane> */}
      <TabPane
        tab={
          <span>
            <ProfileOutlined />
            Tasks (5)
          </span>
        }
        key="3"
      >
        <TaskTab handleNavigate={handleNavigate} />
      </TabPane>
    
  </Tabs >


);

const Notifications = ({ history }) => {
  const navigate = useNavigate();
  const socketRef = useRef();
  const [view, setViewTab] = useState(true);
  const [notificationData, setNotificationData] = useState([]);
  const handleNavigate = () => {
    navigate("/notifications-detail");
  };

  useEffect(() => {
    // const socket = io("http://3.1.49.38:8000", {
    //   transports: ["websocket", "polling"],
    // });
    // socketRef.current = socket;
    // window.socket = socket;
    // setSocket(socket);
    window.socket.on("OnNotificationReceive", (response) => {
      // console.log("notification received");
      // alert("notification received");
      openNotificationWithIcon("success", response.msg, 3);
      setNotificationData((prevNotifications) => [
        response,
        ...prevNotifications,
      ]);
    });

    fetchNotifications().then((response) => {
      const notifications = response.data.slice(0, 5);
      setNotificationData((prevData) => [...notifications, ...prevData]);
    });
  }, []);

  // window.socket.on("postNotification", (response) => {
  //   console.log("notification received");
  //   alert("notification received");
  //   openNotificationWithIcon("success", response.msg, 3);
  //   setNotificationData((prevNotifications) => [
  //     response,
  //     ...prevNotifications,
  //   ]);
  // });

  const fetchNotifications = async () => {
    const url = `${URL_WITH_VERSION}/notifications/list`;
    const response = await getAPICall(url);
    return response;
  };

  if (view) {
    return (
      <div className="app-header-notifications">
        <PopoverTabs handleNavigate={handleNavigate} data={notificationData} />
      </div>
    );
  }
};

export default Notifications;
