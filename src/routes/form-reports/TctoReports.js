import React, { Component } from "react";
import { PrinterOutlined } from "@ant-design/icons";
import ReactToPrint from "react-to-print";

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formReportdata: this.props.data || {},
    };
  }

  render() {
    const { formReportdata } = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <img className="reportlogo" src={""} alt="no Img" />
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">TCTO ID :</td>
                      <td>
                        {formReportdata["tcto_id"]
                          ? formReportdata["tcto_id"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Fixed by( user) :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Status :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Vessel Name :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">DWF % :</td>
                      <td>
                        {formReportdata["dwf_per"]
                          ? formReportdata["dwf_per"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">My company :</td>
                      <td>
                        {formReportdata["my_company_name"]
                          ? formReportdata["my_company_name"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">TCI Code :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">TCO code :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Trade area :</td>
                      <td>
                        {formReportdata["trade_area_name"]
                          ? formReportdata["trade_area_name"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">TCI Daily Hire :</td>
                      <td>
                        {formReportdata["tci_d_hire"]
                          ? formReportdata["tci_d_hire"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">TCO Daily Hire :</td>
                      <td>
                        {formReportdata["tco_d_hire"]
                          ? formReportdata["tco_d_hire"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Charterer :</td>
                      <td>
                        {formReportdata["charterer_name"]
                          ? formReportdata["charterer_name"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">TCI add com :</td>
                      <td>
                        {formReportdata["tci_add_com"]
                          ? formReportdata["tci_add_com"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">TCO add com :</td>
                      <td>
                        {formReportdata["tco_add_com"]
                          ? formReportdata["tco_add_com"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Commence date :</td>
                      <td>
                        {formReportdata["commence_date"]
                          ? formReportdata["commence_date"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Delivery Port :</td>
                      <td>
                        {formReportdata["delivery_port"]
                          ? formReportdata["delivery_port"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Ballast port :</td>
                      <td>
                        {formReportdata["ballast_port_name"]
                          ? formReportdata["ballast_port_name"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Completing date :</td>
                      <td>
                        {formReportdata["completing_date"]
                          ? formReportdata["completing_date"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Redelivery port :</td>
                      <td>
                        {formReportdata["redelivery_port"]
                          ? formReportdata["redelivery_port"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Reposition Port :</td>
                      <td>
                        {formReportdata["reposition_port_name"]
                          ? formReportdata["reposition_port_name"]
                          : " "}
                      </td>

                      <td className="font-weight-bold">Routing type :</td>
                      <td>
                        {formReportdata["routing_type"]
                          ? formReportdata["routing_type"]
                          : " "}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Tot Voyage Days :</td>
                      <td>
                        {formReportdata["total_voyage_days"]
                          ? formReportdata["total_voyage_days"]
                          : " "}
                      </td>
                      <td colSpan="2"></td>
                    </tr>
                  </tbody>
                </table>
                <table className="table table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="border-0">
                        <table className="table border-table table-invoice-report-colum">
                          <thead>
                            <tr>
                              <th rowSpan="2">SPD Type</th>
                              <th colSpan="2" className="text-center">
                                Ballast Bonus
                              </th>
                              <th colSpan="2" className="text-center">
                                Laden
                              </th>
                            </tr>
                            <tr>
                              <th>Kt</th>
                              <th>Cons</th>
                              <th>Kt</th>
                              <th>Cons</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td className="border-0">
                        <table className="table border-table table-invoice-report-colum">
                          <thead>
                            <tr>
                              <th>P$</th>
                              <th>CP$</th>
                              <th>Fuel</th>
                              <th>Laden</th>
                              <th>Ballast</th>
                              <th>Load</th>
                              <th>Disch</th>
                              <th>Heat</th>
                              <th>Idle</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                            </tr>

                            <tr>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                              <td>Value</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <h4 className="font-weight-bold">C/P Term</h4>
                <hr className="invoice-line" />

                <h4>TCO Hire Terms</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Hire Rate</th>
                      <th>Rate Type</th>
                      <th>From Date (GMT)</th>
                      <th>To Date (GMT)</th>
                      <th>Duration</th>
                      <th>Period</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Commission</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Broker Name</th>
                      <th>Rate(Amt)</th>
                      <th>Rate Type</th>
                      <th>From Date (GMT)</th>
                      <th>To Date (GMT)</th>
                      <th>Duration</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>TCO Other Terms</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Delivery Port</th>
                      <th>GMT Date/Time</th>
                      <th>Redelivery Port</th>
                      <th>GMT Date/Time</th>
                      <th>Duration(Days)</th>
                      <th>Remark</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Other Pricing</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Code Name</th>
                      <th>Description</th>
                      <th>Rate Type</th>
                      <th>Amount</th>
                      <th>Commission</th>
                      <th>From GMT</th>
                      <th>To GMT</th>
                      <th>Total Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>TCI Hire Terms</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Hire Rate</th>
                      <th>Rate Type</th>
                      <th>From Date (GMT)</th>
                      <th>To Date (GMT)</th>
                      <th>Duration</th>
                      <th>Period</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Commission</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Broker Name</th>
                      <th>Rate(Amt)</th>
                      <th>Rate Type</th>
                      <th>From Date (GMT)</th>
                      <th>To Date (GMT)</th>
                      <th>Duration</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>TCI Other Terms</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Delivery Port</th>
                      <th>GMT Date/Time</th>
                      <th>Redelivery Port</th>
                      <th>GMT Date/Time</th>
                      <th>Duration (Days)</th>
                      <th>Remark</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Other Pricing</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Code Name</th>
                      <th>Description</th>
                      <th>Rate Type</th>
                      <th>Amount</th>
                      <th>Commission</th>
                      <th>From GMT</th>
                      <th>To GMT</th>
                      <th>Total Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Port Itinerary</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Port ID</th>
                      <th>Port</th>
                      <th>Funct.</th>
                      <th>Miles</th>
                      <th>Passage</th>
                      <th>STYPE</th>
                      <th>WF %</th>
                      <th>SPD</th>
                      <th>Eff-SPD</th>
                      <th>GSD</th>
                      <th>TSD</th>
                      <th>XSD</th>
                      <th>L/D QTY</th>
                      <th>L/D Rate(D)</th>
                      <th>L/D Rate(H)</th>
                      <th>L/D Term</th>
                      <th>TurnTime</th>
                      <th>P Days</th>
                      <th>Xpd</th>
                      <th>P.EXP</th>
                      <th>Total Port Days</th>
                      <th>Currency</th>
                    </tr>
                  </thead>

                  <tbody>
                    {formReportdata &&
                      formReportdata.portitinerary &&
                      formReportdata.portitinerary.length > 0 &&
                      formReportdata.portitinerary.map((e, id) => {
                        return (
                          <tr key={e.id}>
                            <td>{e.port_id ? e.port_id : ""}</td>
                            <td>{e.port ? e.port : ""}</td>
                            <td>{e.funct_name ? e.funct_name : ""}</td>
                            <td>{e.miles ? e.miles : ""}</td>
                            <td>{e.passage_name ? e.passage_name : ""}</td>
                            <td>{e.s_type_name ? e.s_type_name : ""}</td>
                            <td>{e.wf_per ? e.wf_per : ""}</td>
                            <td>{e.speed ? e.speed : ""}</td>
                            <td>{e.eff_speed ? e.eff_speed : ""}</td>
                            <td>{e.gsd ? e.gsd : ""}</td>
                            <td>{e.tsd ? e.tsd : ""}</td>
                            <td>{e.xsd ? e.xsd : ""}</td>
                            <td>{e.l_d_qty ? e.l_d_qty : ""}</td>
                            <td>{e.l_d_rate ? e.l_d_rate : ""}</td>
                            <td>{e.l_d_rate1 ? e.l_d_rate1 : ""}</td>
                            <td>{e.l_d_term_name ? e.l_d_term_name : ""}</td>
                            <td>{e.turn_time ? e.turn_time : " "}</td>
                            <td>{e.days ? e.days : " "}</td>
                            <td>{e.xpd ? e.xpd : " "}</td>
                            <td>{e.p_exp ? e.p_exp : " "}</td>
                            <td>{e.t_port_days ? e.t_port_days : " "}</td>
                            <td>{e.currency_name ? e.currency_name : " "}</td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Bunker Details</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th rowSpan="2">Port ID</th>
                      <th rowSpan="2">Port</th>
                      <th rowSpan="2">F</th>
                      <th rowSpan="2">Miles</th>
                      <th rowSpan="2">Passage</th>
                      <th rowSpan="2">SPD Type</th>
                      <th rowSpan="2">SPD</th>
                      <th rowSpan="2">Arrival Date-Time</th>
                      <th rowSpan="2">Departure</th>
                      <th colSpan="5">Fuel Grade (Sea Cons. In MT)</th>
                      <th colSpan="5">Arrival ROB</th>
                    </tr>
                    <tr>
                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                      <th>ULSFO</th>

                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                      <th>ULSFO</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata &&
                      formReportdata.bunkerdetails &&
                      formReportdata.bunkerdetails.length > 0 &&
                      formReportdata.bunkerdetails.map((e, id) => {
                        return (
                          <tr key={e.id}>
                            <td>{e.port_id ? e.port_id : " "}</td>
                            <td>{e.port ? e.port : " "}</td>
                            <td>{e.funct ? e.funct : " "}</td>
                            <td>{e.miles ? e.miles : " "}</td>
                            <td>{e.passage_name ? e.passage_name : " "}</td>
                            <td>{e.spd_type_name ? e.spd_type_name : " "}</td>
                            <td>{e.speed ? e.speed : " "}</td>
                            <td>
                              {e.arrival_date_time ? e.arrival_date_time : " "}
                            </td>
                            <td>{e.departure ? e.departure : " "}</td>
                            <td>{e.departure ? e.departure : " "}</td>
                            <td></td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th colSpan="5">Port Cons. Fuel</th>
                      <th colSpan="5">Received</th>
                      <th colSpan="5">DEP.ROB</th>
                    </tr>
                    <tr>
                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                      <th>ULSFO</th>

                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                      <th>ULSFO</th>

                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                      <th>ULSFO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Port Date Details</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Port ID</th>
                      <th>Port</th>
                      <th>Funct.</th>
                      <th>MILES</th>
                      <th>Passage</th>
                      <th>STYPE</th>
                      <th>SPD</th>
                      <th>WF%</th>
                      <th>TSD</th>
                      <th>XSD</th>
                      <th>Arrival Date Time</th>
                      <th>Day</th>
                      <th>T.PDays</th>
                      <th>Departure</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Total Distance</th>
                      <th>TTL Port Days</th>
                      <th>TSD</th>
                      <th>TTL Qty</th>
                      <th>GSD</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        XXXXX <span className="float-right">Miles</span>
                      </td>
                      <td>
                        XXX <span className="float-right">Days</span>
                      </td>
                      <td>
                        XXXXX <span className="float-right">Days</span>
                      </td>
                      <td>
                        XX <span className="float-right">Mt</span>
                      </td>
                      <td>
                        XXX <span className="float-right">Days</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class TctoReports extends Component {
  constructor() {
    super();
    this.state = {
      name: "Printer",
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                ref={(el) => (this.componentRef = el)}
                data={this.props.data}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default TctoReports;
