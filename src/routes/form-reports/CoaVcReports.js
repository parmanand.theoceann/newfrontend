import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import URL_WITH_VERSION from '../../shared';
import jsPDF from 'jspdf';
import html2canvas from "html2canvas";
import Email from '../../components/Email';
import { Modal, Spin } from 'antd';
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {

    }


    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }
  }
  render() {
    const { formReportdata } = this.state
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id='divToPrint'>
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
            
                  <img  className="title reportlogo" src={formReportdata.logo} alt="no img"/>
                  <p className="sub-title m-0">{formReportdata.full_name}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata.address}</p>
                </div>
              </div>
            </div>


            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">COA(VC) ID :</td>
                      <td className="">{formReportdata.contract_id}</td>

                      <td className="font-weight-bold">Cargo Group :</td>
                      <td className="">{formReportdata.cargo_name}</td>

                      <td className="font-weight-bold">Status :</td>
                      <td className="">{formReportdata.vci_status_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Master Contract ID :</td>
                      <td className="">{formReportdata.mst_contract}</td>

                      <td className="font-weight-bold">COA Qty/Unit :</td>
                      <td className="">{formReportdata.coa_qty} / {formReportdata.coa_unit_name}</td>

                      <td className="font-weight-bold">Currency :</td>
                      <td className="">{formReportdata.currency_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Owner Name :</td>
                      <td className="">{formReportdata.owner_id_name}</td>

                      <td className="font-weight-bold">Option %/ Type :</td>
                      <td className="">{formReportdata.opt_percentage} / {formReportdata.opt_type_name}</td>

                      <td className="font-weight-bold">Invoice By :</td>
                      <td className="">{formReportdata.invoice_by_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Start Date :</td>
                      <td className="">{formReportdata.start_date}</td>

                      <td className="font-weight-bold">Min/Max Qty :</td>
                      <td className="">{formReportdata.min_qty} / {formReportdata.max_qty}</td>

                      <td className="font-weight-bold">Min Inv Qty :</td>
                      <td className="">{formReportdata.min_inv_qty}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">End date :</td>
                      <td className="">{formReportdata.end_date}</td>

                      <td className="font-weight-bold">Confirm CP Lift :</td>
                      <td className="">{formReportdata.firm_cp_lifting}</td>

                      <td className="font-weight-bold">Freight Type :</td>
                      <td className="">{formReportdata.freight_type1}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">My Company/LOB :</td>
                      <td className="">{formReportdata.company_id_name} / {formReportdata.company_lob_name}</td>

                      <td className="font-weight-bold">Optional Liftings :</td>
                      <td className="">{formReportdata.opt_cp_lifting}</td>

                      <td className="font-weight-bold">Freight Rate :</td>
                      <td className="">{formReportdata.freight_rate}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Cargo Name :</td>
                      <td className="">{formReportdata.cargo_name1}</td>

                      <td className="font-weight-bold">CP QTY Per lift :</td>
                      <td className="">{formReportdata.cp_qty_per_lift}</td>

                      <td className="font-weight-bold">VAT No. / Tax % :</td>
                      <td className="">{formReportdata.vat_number} / {formReportdata.vat_percentage}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Freight Bill Via :</td>
                      <td className="">{formReportdata.bill_via_name}</td>

                      <td className="font-weight-bold">Min/Max Qty/Lift :</td>
                      <td className="">{formReportdata.min_max_qty} / {formReportdata.min_max_lift}</td>

                      <td className="font-weight-bold">Fixed By User :</td>
                      <td className="">{formReportdata.fixed_by_user}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Trade Area :</td>
                      <td className="">{formReportdata.trade_area_name}</td>

                      <td className="font-weight-bold">Total COA Qty :</td>
                      <td className="">{formReportdata.total_coa_qty}</td>

                      <td className="font-weight-bold">Ref Company :</td>
                      <td className="">{formReportdata.ref_company_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">CP Date/Place :</td>
                      <td className="">{formReportdata.cp_date} / {formReportdata.cp_place}</td>

                      <td className="font-weight-bold">Ttl Fixed Qty :</td>
                      <td className="">{formReportdata.ttl_fixed_qty}</td>

                      <td className="font-weight-bold">Reference No./PO :</td>
                      <td className="">{formReportdata.reference_no}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Commence Date :</td>
                      <td className="">{formReportdata.commence_date}</td>

                      <td className="font-weight-bold">Ttl Remaining Qty :</td>
                      <td className="">{formReportdata.ttl_remaining_qty}</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-invoice-report-colum">
                  <tbody>
                    <td className="border-0">
                      <h4>Load Options</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Port/Area</th>
                            <th>L/D Rates</th>
                            <th>L/H Rates</th>
                            <th>Terms</th>
                            <th>TT</th>
                            <th>PExp</th>
                            <th>Draft</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.loadoptions && formReportdata.loadoptions &&
                            formReportdata.loadoptions.length > 0 ? formReportdata.loadoptions.map((e, idx) => {
                              return (
                                <>
                                  <tr key={idx}>
                                    <td>{e.port_area_name}</td>
                                    <td>{e.l_d_rates}</td>
                                    <td>{e.ld_ru}</td>
                                    <td>{e.ld_terms_name}</td>
                                    <td>{e.ld_tt}</td>
                                    <td>{e.pexp}</td>
                                    <td>{e.draft}</td>
                                  </tr>
                                </>
                              )
                            }) : undefined

                          }
                        </tbody>
                      </table>
                    </td>

                    <td className="border-0">
                      <h4>Discharge Options</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Port/Area</th>
                            <th>L/D Rates</th>
                            <th>L/H Rates</th>
                            <th>Terms</th>
                            <th>TT</th>
                            <th>PExp</th>
                            <th>Draft</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.dischargeoptions && formReportdata.dischargeoptions.length > 0 ? formReportdata.dischargeoptions.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.port_area_name}</td>
                                  <td>{e.l_d_rates}</td>
                                  <td>{e.ld_ru}</td>
                                  <td>{e.ld_terms_name}</td>
                                  <td>{e.ld_tt}</td>
                                  <td>{e.pexp}</td>
                                  <td>{e.draft}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                          }
                        </tbody>
                      </table>
                    </td>
                  </tbody>
                </table>

                <h4>Cargo Itinerary</h4>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>F</th>
                      <th>Port</th>
                      <th>Quantity</th>
                      <th>Berth</th>
                      <th>L/D Rate</th>
                      <th>Rate Unit</th>
                      <th>Terms</th>
                      <th>TT</th>
                      <th>Port Exp</th>
                      <th>H</th>
                      <th>Draft</th>
                      <th>Unit</th>
                      <th>Salanity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.itineraryoptions && formReportdata.itineraryoptions && formReportdata.itineraryoptions.length > 0 ? formReportdata.itineraryoptions.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.io_f_name}</td>
                            <td>{e.port_id_name}</td>
                            <td>{e.io_qty}</td>
                            <td>{e.berth}</td>
                            <td>{e.l_d_rate}</td>
                            <td>{e.rate_unit}</td>
                            <td>{e.io_terms_name}</td>
                            <td>{e.io_tt}</td>
                            <td>{e.port_exp}</td>
                            <td>{e.io_h}</td>
                            <td>{e.draft}</td>
                            <td>{e.io_unit_name}</td>
                            <td>{e.salanity}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }
                  </tbody>
                </table>

                <h4>Cargo Pricing & Billing</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Load Port(S)</th>
                      <th>Discharge Port(S)</th>
                      <th>Cargo</th>
                      <th>Frt Type</th>
                      <th>Amount</th>
                      <th>Table</th>
                       
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata['--'] && formReportdata['--'].length > 0 ? formReportdata['--'].map((e, idx) => {
                      return (
                        <>

                          <tr key={idx}>
                            <td>{e.load_ports_name}</td>
                            <td>{e.discharge_ports_name}</td>
                            <td>{e.cargo_name}</td>
                            <td>{e.frt_type_name}</td>
                            <td>{e.amount}</td>
                            <td>{e.table1}</td>
                             
                          </tr>
                        </>
                      )
                    }) : undefined

                    }
                  </tbody>
                </table>

                <table className="table table-invoice-report-colum">
                  <tbody>
                    <td className="border-0">
                      <h4>Broker</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Broker</th>
                            <th>Amount</th>
                            <th>Rate Type</th>
                            <th>Pay Method</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.broker && formReportdata.broker && formReportdata.broker.length > 0 ? formReportdata.broker.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.brocker_name}</td>
                                  <td>{e.amount}</td>
                                  <td>{e.t_name}</td>
                                  <td>{e.f_name}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                          }

                        </tbody>
                      </table>
                    </td>

                    <td className="border-0">
                      <h4>Extra Freight Term</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Code</th>
                            <th>Extra Freight Term</th>
                            <th>Rate/Lump</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.extrafreightterm && formReportdata.extrafreightterm && formReportdata.extrafreightterm.length > 0 ? formReportdata.extrafreightterm.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.code}</td>
                                  <td>{e.extra_freight_term}</td>
                                  <td>{e.rate_lump}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                          }
                        </tbody>
                      </table>
                    </td>
                  </tbody>
                </table>

                <h4>Billing and Banking details</h4>
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>


                    <tr>
                      <td className="font-weight-bold">Remit Bank :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.remittance_bank_name}</td>
                      ) : undefined
                      }
                      <td className="font-weight-bold">Account No :</td>

                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.account_no}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">1st Invoice(%) :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (

                        <td className="text-left">{formReportdata.billingandbankingdetails.inv_per}</td>
                      ) : undefined
                      }
                    </tr>




                    <tr>
                      <td className="font-weight-bold">Balance(%) :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (

                        <td className="text-left">{formReportdata.billingandbankingdetails.bal_per}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Payment Term :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.payment_term_name}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Due Date :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.due_date}</td>
                      ) : undefined
                      }
                    </tr>


                    <tr>
                      <td className="font-weight-bold">Billing Days :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.billing_days}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Freight Surcharge :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.freight_surcharge}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Billing Basis :</td>
                      {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                        <td className="text-left">{formReportdata.billingandbankingdetails.billing_basis_name}</td>
                      ) : undefined
                      }
                    </tr>

                  </tbody>

                </table>

                <h4>Supplier/Receiver Info</h4>
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>

                    <tr>
                      <td className="font-weight-bold">Final Destination :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].final_destination}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Supplier :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].supplier_name}</td>
                      ) : undefined
                      }
                      <td className="font-weight-bold">Receiver :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].reciever_name}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Ref Company :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].ref_company_name}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">VAT No / % :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].vat_no}/{formReportdata['---------'].vat_per}</td>
                      ) : undefined
                      }


                      <td className="font-weight-bold">OBL No. :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].obl_no}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Ref Contract :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].ref_contract}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Commingle Code :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (
                        <td className="">{formReportdata['---------'].commingle_code}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Trader :</td>
                      {formReportdata['---------'] && formReportdata['---------'] ? (

                        <td className="">{formReportdata['---------'].trader_name}</td>
                      ) : undefined
                      }
                    </tr>


                  </tbody>
                </table>

                <h4>Estimate Rev/Exp Info</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Description Type</th>
                      <th>Rev/Exp</th>
                      <th>Type Of Rev/Exp</th>
                      <th>Price</th>
                      <th>Currency</th>
                      <th>Total Amount($)</th>
                      <th>Final Amount</th>
                      <th>Account Code</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.revexp && formReportdata.revexp && formReportdata.revexp.length > 0 ? formReportdata.revexp.map((e, idx) => {
                      return (
                        <>

                          <tr key={idx}>

                            <td>{e.description}</td>
                            <td>{e.rev_exp}</td>
                            <td>{e.rev_exp_name}</td>
                            <td>{e.re_currency_name}</td>
                            <td>{e.re_amt_curr_name}</td>
                            <td>{e.amount_base}</td>
                            <td>{e.rev_exp}</td>
                            <td>{e.code}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }

                  </tbody>
                </table>

                <h4>Rebill</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>F</th>
                      <th>Port</th>
                      <th>Ledger</th>
                      <th>Ledger Code Description</th>
                      <th>Cost Details Description</th>
                      <th>Rebillable</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.rebillsettings && formReportdata.rebillsettings && formReportdata.rebillsettings.length > 0 ? formReportdata.rebillsettings.map((e, idx) => {
                      return (
                        <>

                          <tr key={idx}>

                            <td>{e.cv_f_name}</td>
                            <td>{e.cv_port_name}</td>
                            <td>{e.ledger}</td>
                            <td>{e.ledger_code_description}</td>
                            <td>{e.cost_detail_description}</td>
                            <td>{e.rebillable}</td>
                          </tr>

                        </>
                      )
                    }) : undefined

                    }
                  </tbody>
                </table>

                <h4>Dem/Des Term</h4>
                <table className="table table-bordered table-invoice-report-colum">




                  <tbody>

                    <tr>
                      <td className="font-weight-bold">Load Dem/Des :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].load_dem}/{formReportdata['-----'].load_des}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Total Time Bar :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].total_time_bar}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Per Day/Per hour :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].per_day}/{formReportdata['-----'].per_hour}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">TT Hours :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].tt_hours}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Disc Dem/Des :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].disch_dem}/{formReportdata['-----'].disch_des}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Freight Surcharge :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].freight_surcharge}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Per Day/Per hour :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].d_per_day}/{formReportdata['-----'].d_per_hour}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Bunker Surcharge :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].bunker_surcharge}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Dem/Des Curr :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].dem_des_curr_name}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Admin Charges :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].admin_charges}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Demurrage :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].demurrage}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Dem/Des Commitable :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].dem_des_commitable}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Laytime :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].laytime}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Reversible All Port :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].reversible_all_port}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Laytime Allowed :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].laytime_allowed}</td>
                      ) : undefined
                      }

                      <td className="font-weight-bold">Non Rev. All Port :</td>
                      {formReportdata['-------'] && formReportdata['-------'] ? (
                        <td className="text-right">{formReportdata['-------'].non_rev_all_port}</td>
                      ) : undefined
                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Days / Hours :</td>
                      {formReportdata['-----'] && formReportdata['-----'] ? (
                        <td className="text-right">{formReportdata['-----'].days}/{formReportdata['-----'].hours}</td>
                      ) : undefined
                      }
                    </tr>
                  </tbody>

                </table>

                <h4>Nomination Task</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Nomination Tasks</th>
                      <th>Due Date Offset</th>
                      <th>Remainder Offset</th>
                      <th>Priority</th>
                      <th>Category</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.nominationtasks && formReportdata.nominationtasks && formReportdata.nominationtasks.length > 0 ? formReportdata.nominationtasks.map((e, idx) => {
                      return (
                        <>

                          <tr key={idx}>

                            <td>{e.nomination_task}</td>
                            <td>{e.due_date}</td>
                            <td>{e.reminder_offset}</td>
                            <td>{e.priority_name}</td>
                            <td>{e.category}</td>

                          </tr>
                        </>
                      )
                    }) : undefined

                    }

                  </tbody>
                </table>

                <table className="table table-invoice-report-colum">
                  <tbody>
                    <td className="border-0">
                      <h4>Cargo</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Cargo Name</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.cargo && formReportdata.cargo.length > 0 ? formReportdata.cargo.map((e, idx) => {
                            return (
                              <>

                                <tr key={idx}>

                                  <td>{e.cargo_id_name}</td>
                                </tr>

                              </>
                            )
                          }) : undefined

                          }
                        </tbody>
                      </table>
                    </td>

                    <td className="border-0">
                      <h4>Vessel Type</h4>
                      <table className="table border-table table-invoice-report-colum">
                        <thead>
                          <tr>
                            <th>Vessel</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata.vesseltype && formReportdata.vesseltype && formReportdata.vesseltype.length > 0 ? formReportdata.vesseltype.map((e, idx) => {
                            return (
                              <>

                                <tr key={idx}>

                                  <td>{e.vessel_name}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                          }

                        </tbody>
                      </table>
                    </td>
                  </tbody>
                </table>

                <h4>Link Trade Cargos</h4>
                <table className="table border-table table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Number of Cargo Liftings :</td>
                      {formReportdata['-'] && formReportdata['-'] ? (

                        <td className="text-right">{formReportdata['-'].opt_cp_lifting}</td>

                      ) : undefined

                      }



                      <td className="font-weight-bold">COA Qty :</td>
                      {formReportdata['-'] && formReportdata['-'] ? (

                        <td className="text-right">{formReportdata['-'].coa_qty}</td>

                      ) : undefined

                      }

                      <td className="font-weight-bold">Option %/Type :</td>
                      {formReportdata['-'] && formReportdata['-'] ? (
                        <td className="text-right">{formReportdata['-'].opt_percentage}/{formReportdata['-'].option_type}</td>

                      ) : undefined

                      }

                      <td className="font-weight-bold">Status :</td>
                      {formReportdata['-'] && formReportdata['-'] ? (
                        <td className="text-right">{formReportdata['-'].c_status_name}</td>

                      ) : undefined

                      }
                    </tr>
                  </tbody>
                </table>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Cargo ID</th>
                      <th>Laycan From</th>
                      <th>Laycan To</th>
                      <th>Qty</th>
                      <th>Unit</th>
                      <th>Voyage No</th>
                      <th>Linked Voyage</th>
                      <th>Status</th>
                      <th>Freight AMT</th>
                      <th>Currency</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata['-'] && formReportdata['-'] && formReportdata['-'].length > 0 ? formReportdata['-'].map((e, idx) => {
                      return (
                        <>

                          <tr key={idx}>
                            <td>{e.cargo_id_name}</td>
                            <td>{e.laycan_from}</td>
                            <td>{e.laycan_to}</td>
                            <td>{e.cp_qty}</td>
                            <td>{e.cp_unit_name}</td>
                            <td>{e.voyage_no}</td>
                            <td>{e.voyage_cp_in_id}</td>
                            <td>{e.c_status_name}</td>
                            <td>{e.cargo_id}</td>
                            <td>{e.currency_name}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class CoaVcReports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Printer',
      pdfData: "",
      userInput: "",
      emailModal: false,
      loading: false,
      mailTitlePayload: {},
    };
    console.log('props',this.props.data)
  }

  printReceipt() {
    window.print();
  }
  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns } = this.state;

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
  //  if (cols && cols.length > 0) {
  //     params = params + '&c=' + cols.join(',')
  //   } 

    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}`, '_blank');
  }


  printDocument(){

    var quotes = document.getElementById('divToPrint');

    html2canvas(quotes,{
       logging: true,
       letterRendering: 1,
       useCORS: true,
       allowTaint: true    }).then(function (canvas) {
        const link = document.createElement("a");
        link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF('p', 'mm');
      var position = 20;
      var pageData = canvas.toDataURL('image/jpeg', 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, 'PNG', 5, position, imgWidth-8, imgHeight-7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(imgData, 'PNG', 5, position+20, imgWidth-8, imgHeight-7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save('CoaVc Report.pdf');

    });
  };

  sendEmailReportModal = async () => {
    try {
      
      this.setState({ loading: true });
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      
  
      this.setState({
        loading: false,
        pdfData: blob,
        emailModal: true,
      });
  
    } catch (error) {
      console.error('Error:', error);
      this.setState({ loading: false });
      // Handle errors here
    }
  };


  render() {
    const { full_name, company_id_name, vci_status_name } =this.props.data;
    
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt">
                          <li onClick={this.sendEmailReportModal} style={{cursor:this.state.loading ? 'not-allowed' : 'pointer'}}>Send Email</li>
                        <li onClick={this.printDocument}>
                        Download
                      </li>
                        </span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data} />
            </div>
          </div>
        </article>
        {this.state.emailModal && (
          <Modal
            title="New Message"
            visible={this.state.emailModal}
            onOk={() => {
              // Instead of calling this.state.emailModal, set it to false
              this.setState({ emailModal: false });
            }}
            onCancel={() => {
              // Instead of calling this.state.emailModal, set it to false
              this.setState({ emailModal: false });
            }}
            footer={null}
          >
            {this.state.pdfData && (
              <Email
                handleClose={() => {
                  this.setState({ emailModal: false });
                }}
                attachmentFile={this.state.pdfData}
                title={window.corrector(`COA_VC_Report||${full_name}||${company_id_name}||${vci_status_name}`)}

              // title={`Bunker_Purchase_Report|| ${vessel_name} || ${my_company_name} || ${po_number} || ${vendor_remark}`}
              />
            )}
          </Modal>
        )}

{
          this.state.loading && (
            <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
              <Spin size="large" />
            </div>
          )
        }

      </div>
    );
  }
}

export default CoaVcReports;
