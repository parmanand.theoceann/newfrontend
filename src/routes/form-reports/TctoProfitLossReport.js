import React, { forwardRef, useEffect, useRef } from "react";
import { PrinterOutlined } from "@ant-design/icons";
import ReactToPrint from "react-to-print";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";
import { useState } from "react";
import { useStateCallback } from "../../shared";

const ComponentToPrint = forwardRef((props, ref) => {
  const [state, setState] = useStateCallback({
    istcov: true,
    estimateReportdata: props.data || {},
    expensesEstimateVivo:
      props.data["voyage_manager"]["ops_type_name"] === "VIVO" &&
      props.data["estimate"] &&
      props.data["estimate"]["expenses"]
        ? props.data["estimate"]["expenses"]
        : {},
    expensesActualVivo:
      props.data["voyage_manager"]["ops_type_name"] === "VIVO" &&
      props.data["actual"] &&
      props.data["actual"]["expenses"]
        ? props.data["actual"]["expenses"]
        : {},
    voyageResultEstimateTcov: {},
    voyageResultActualTcov: {},
    voyageResultEstimateVT: {},
    voyageResultActualVT: {},
  });

  useEffect(() => {
    getFormData();
  }, []);

  const getFormData = () => {
    const { estimateReportdata } = state;

    if (
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate"
    ) {
      setState((prevState) => ({
        ...prevState,
        voyageResultEstimateTcov:
          estimateReportdata["estimate"]["voyage_result"],
        voyageResultActualTcov: estimateReportdata["actual"]["voyage_result"],
      }));
    }
    if (
      estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
    ) {
      setState((prevState) => ({
        ...prevState,
        voyageResultEstimateVT: estimateReportdata["estimate"]["voyage_result"],
        voyageResultActualVT: estimateReportdata["actual"]["voyage_result"],
      }));
    }
  };

  const parseValue = (val) => {
    if (typeof val === "number" && !isNaN(val)) {
      return val.toFixed(2);
    }
    if (val) {
      return Number(val.replace(/[^\d.-]/g, "")).toFixed(2);
    }
    return "0.00";
  };

  const calculateDiff = (actual, estimate) => {
    let parseActual = 0;
    let parseEstimate = 0;
    if (typeof actual === "number" && !isNaN(actual)) {
      parseActual = actual;
    } else if (typeof actual === "string") {
      parseActual = Number(actual.replace(/[^\d.-]/g, ""));
    }
    if (typeof estimate === "number" && !isNaN(estimate)) {
      parseEstimate = estimate;
    } else if (typeof estimate === "string") {
      parseEstimate = Number(estimate.replace(/[^\d.-]/g, ""));
    }

    return (parseActual - parseEstimate).toFixed(2) || "0.00";
  };

  const calculatePercentDiff = (actual, estimate) => {
    let parseActual = 0;
    let parseEstimate = 0;
    if (typeof actual === "number" && !isNaN(actual)) {
      parseActual = actual;
    } else if (typeof actual === "string") {
      parseActual = Number(actual.replace(/[^\d.-]/g, ""));
    }
    if (typeof estimate === "number" && !isNaN(estimate)) {
      parseEstimate = estimate;
    } else if (typeof estimate === "string") {
      parseEstimate = Number(estimate.replace(/[^\d.-]/g, ""));
    }

    if (parseEstimate !== 0) {
      return (
        (((parseActual - parseEstimate) / parseEstimate) * 100).toFixed(2) ||
        "0.00"
      );
    }
    return "0.00";
  };

  const calculatePercentDiffPostCash = (actual, estimate) => {
    let parseActual = 0;
    let parseEstimate = 0;
    if (typeof actual === "number" && !isNaN(actual)) {
      parseActual = actual;
    } else if (typeof actual === "string") {
      parseActual = Number(actual.replace(/[^\d.-]/g, ""));
    }
    if (typeof estimate === "number" && !isNaN(estimate)) {
      parseEstimate = estimate;
    } else if (typeof estimate === "string") {
      parseEstimate = Number(estimate.replace(/[^\d.-]/g, ""));
    }
    if (parseActual !== 0) {
      return (
        (((parseActual - parseEstimate) / parseActual) * 100).toFixed(2) ||
        "0.00"
      );
    }
    return "0.00";
  };

  const {
    estimateReportdata,
    voyageResultEstimateTcov,
    voyageResultActualTcov,
    voyageResultEstimateVT,
    voyageResultActualVT,
  } = state;
  const { total_eca_sea_cons, total_eca_port_cons,total_noneca_port_cons } =
    estimateReportdata?.actual?.expenses?.bunker_expenses;
  const { CO2_adj_profit,co2_adjusted_tce,co2expense } = estimateReportdata?.actual?.voyage_result;

 const {total_eca_port_cons:total_eca_port_cons_est,total_eca_sea_cons:total_eca_sea_cons_est,total_noneca_port_cons:total_noneca_port_cons_est} =estimateReportdata?.estimate?.expenses?.bunker_expenses
  
  // console.log("total_voyage_result", CO2_adj_profit);
  // console.log("total_etotal_eca_sea_consca_port_cons",total_eca_sea_cons);
  // console.log("=====state", state);
  const expensesEstimateVivo = state["expensesEstimateVivo"];
  const expensesActualVivo = state["expensesActualVivo"];
  const { post = {}, cash = {} } = estimateReportdata;

  let estimate_Sea_consumption = 0,
    actual_Sea_consumption = 0;
  let estimate_port_consumption = 0,
    actual_port_consumption = 0;
  let estimate_bunker_expense = 0;
  let actual_bunker_expense = 0;
  let estimate_vessel_expense = 0,
    actual_vessel_expenses = 0;
  let estimate_hire_expenses = 0,
    estimate_ballast_bonus = 0,
    actual_hire_expenses = 0,
    actual_ballast_bonus = 0;
  let estimate_tci_add_comm = 0,
    actual_tci_add_comm = 0;
  let estimate_freight = 0,
    actual_freight = 0;
  let estimate_freight_commission = 0,
    actual_freight_commission = 0;
  let estimate_demmurage = 0,
    actual_demmurage = 0;
  let estimate_demmurageCommission = 0,
    actual_demmurageCommission = 0;
  let estimate_desmurage = 0,
    actual_desmurage = 0;
  let estimate_desmurageCommission = 0,
    actual_desmurageCommission = 0;
  let estimate_tco_hire = 0,
    actual_tco_hire = 0;
  let estimate_tco_b_comm = 0,
    actual_tco_b_comm = 0;
  let estimate_tco_add_comm = 0,
    actual_tco_add_comm = 0;
  let estimate_tco_bb = 0,
    actual_tco_bb = 0;
  let estimate_grossfreight = 0,
    actual_grossfreight = 0;
  let estimate_ttl_comm = 0,
    actual_ttl_comm = 0;
  let estimate_demmurage_sales = 0,
    actual_demmurage_sales = 0;
  let estimate_demmurageCommission_sales = 0,
    actual_demmurageCommission_sales = 0;
  let estimate_desmurage_sales = 0,
    actual_desmurage_sales = 0;
  let estimate_desmurageCommission_sales = 0,
    actual_desmurageCommission_sales = 0;
  let estimate_tci_add_comm_exp = 0,
    actual_tci_add_comm_exp = 0;
  let estimate_tci_broker_comm = 0,
    actual_tci_broker_comm = 0;
  let estimate_port_expenses = 0,
    actual_port_expenses = 0;
  let estimate_sea_consumption_ifo = 0,
    actual_sea_consumption_ifo = 0;
  let estimate_sea_consumption_mgo = 0,
    actual_sea_consumption_mgo = 0;
  let estimate_sea_consumption_vlsfo = 0,
    actual_sea_consumption_vlsfo = 0;
  let estimate_sea_consumption_lsmgo = 0,
    actual_sea_consumption_lsmgo = 0;
  let estimate_sea_consumption_ulsfo = 0,
    actual_sea_consumption_ulsfo = 0;
  let estimate_port_consumption_ifo = 0,
    actual_port_consumption_ifo = 0;
  let estimate_port_consumption_mgo = 0,
    actual_port_consumption_mgo = 0;
  let estimate_port_consumption_vlsfo = 0,
    actual_port_consumption_vlsfo = 0;
  let estimate_port_consumption_lsmgo = 0,
    actual_port_consumption_lsmgo = 0;
  let estimate_port_consumption_ulsfo = 0,
    actual_port_consumption_ulsfo = 0;
  let estimate_mis_expenses = 0,
    actual_mis_expenses = 0;
  let estimate_grossexpanse = 0,
    actual_grossexpanse = 0;
  let estimate_net_expenses = 0,
    actual_net_expenses = 0;

  if (
    estimateReportdata["voyage_manager"] &&
    (estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate")
  ) {
    actual_bunker_expense =
      (estimateReportdata["actual"] &&
        estimateReportdata["actual"]["expenses"] &&
        estimateReportdata["actual"]["expenses"]["totalportexp"] &&
        parseFloat(
          estimateReportdata["actual"]["expenses"]["totalportexp"].replaceAll(
            ",",
            ""
          )
        ) * 1) +
      (estimateReportdata["actual"] &&
        estimateReportdata["actual"]["expenses"] &&
        estimateReportdata["actual"]["expenses"]["totalseaexp"] &&
        parseFloat(
          estimateReportdata["actual"]["expenses"]["totalseaexp"].replaceAll(
            ",",
            ""
          )
        ) * 1);
    estimate_vessel_expense =
      parseFloat(
        estimateReportdata["estimate"]["expenses"] &&
          estimateReportdata["estimate"]["expenses"]["vessel_expenses"] &&
          estimateReportdata["estimate"]["expenses"]["vessel_expenses"][
            "hire_expenses"
          ].replaceAll(",", "")
      ) * 1;
    actual_vessel_expenses =
      parseFloat(
        estimateReportdata["actual"]["expenses"] &&
          estimateReportdata["actual"]["expenses"]["vessel_expenses"] &&
          estimateReportdata["actual"]["expenses"]["vessel_expenses"][
            "hire_expenses"
          ].replaceAll(",", "")
      ) * 1;
    estimate_hire_expenses =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["vessel_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["vessel_expenses"][
        "hire_expenses"
      ] &&
      parseFloat(
        estimateReportdata["estimate"]["expenses"]["vessel_expenses"][
          "hire_expenses"
        ].replaceAll(",", "") * 1
      ).toFixed(2);
    actual_hire_expenses =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["vessel_expenses"] &&
      estimateReportdata["actual"]["expenses"]["vessel_expenses"][
        "hire_expenses"
      ] &&
      parseFloat(
        estimateReportdata["actual"]["expenses"]["vessel_expenses"][
          "hire_expenses"
        ].replaceAll(",", "") * 1
      ).toFixed(2);
    estimate_ballast_bonus =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["vessel_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["vessel_expenses"][
        "ballast_bonus"
      ] &&
      parseFloat(
        estimateReportdata["estimate"]["expenses"]["vessel_expenses"][
          "ballast_bonus"
        ].replaceAll(",", "")
      ).toFixed(2);
    actual_ballast_bonus =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["vessel_expenses"] &&
      estimateReportdata["actual"]["expenses"]["vessel_expenses"][
        "ballast_bonus"
      ] &&
      parseFloat(
        estimateReportdata["actual"]["expenses"]["vessel_expenses"][
          "ballast_bonus"
        ].replaceAll(",", "")
      ).toFixed(2);
    actual_tci_add_comm =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["tci_add_comm"] &&
      parseFloat(
        estimateReportdata["actual"]["expenses"]["tci_add_comm"].replaceAll(
          ",",
          ""
        ) * 1
      ).toFixed(2);
    estimate_tci_add_comm =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["tci_add_comm"] &&
      parseFloat(
        estimateReportdata["estimate"]["expenses"]["tci_add_comm"].replaceAll(
          ",",
          ""
        ) * 1
      ).toFixed(2);

    estimate_demmurageCommission_sales =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["demmurageCommission_sales"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "demmurageCommission_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_demmurageCommission_sales =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["demmurageCommission_sales"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "demmurageCommission_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_desmurage_sales =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["desmurage_sales"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "desmurage_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_desmurage_sales =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["desmurage_sales"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "desmurage_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_tci_add_comm_exp =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["tci_add_comm"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"][
              "tci_add_comm"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_tci_add_comm_exp =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["tci_add_comm"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["tci_add_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_tci_broker_comm =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["tci_broker_comm"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"][
              "tci_broker_comm"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_tci_broker_comm =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["tci_broker_comm"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"][
              "tci_broker_comm"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_port_expenses =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["port_expenses"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"][
              "port_expenses"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_expenses =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["port_expenses"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"][
              "port_expenses"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_Sea_consumption =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["totalseaexp"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["totalseaexp"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;

    estimate_sea_consumption_ifo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["ifo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["ifo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_sea_consumption_ifo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["ifo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["ifo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_sea_consumption_mgo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["mgo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["mgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_sea_consumption_mgo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["mgo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["mgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_sea_consumption_vlsfo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["vlsfo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["vlsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_sea_consumption_vlsfo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["vlsfo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["vlsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_sea_consumption_lsmgo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["lsmgo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["lsmgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_sea_consumption_lsmgo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["lsmgo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["lsmgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_sea_consumption_ulsfo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["ulsfo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["ulsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_sea_consumption_ulsfo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "sea_consumption"
      ]["ulsfo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "sea_consumption"
            ]["ulsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["totalportexp"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["totalportexp"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_port_consumption_ifo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["ifo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["ifo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption_ifo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["ifo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["ifo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_port_consumption_mgo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["mgo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["mgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption_mgo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["mgo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["mgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_port_consumption_vlsfo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["vlsfo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["vlsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption_vlsfo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["vlsfo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["vlsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_port_consumption_lsmgo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["lsmgo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["lsmgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption_lsmgo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["lsmgo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["lsmgo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_port_consumption_ulsfo =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["ulsfo"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["ulsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_port_consumption_ulsfo =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ] &&
      estimateReportdata["actual"]["expenses"]["bunker_expenses"][
        "port_consumption"
      ]["ulsfo"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["bunker_expenses"][
              "port_consumption"
            ]["ulsfo"].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_mis_expenses =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["expenses"] &&
      estimateReportdata["estimate"]["expenses"]["mis_expenses"]
        ? parseFloat(
            estimateReportdata["estimate"]["expenses"][
              "mis_expenses"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_mis_expenses =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["expenses"] &&
      estimateReportdata["actual"]["expenses"]["mis_expenses"]
        ? parseFloat(
            estimateReportdata["actual"]["expenses"]["mis_expenses"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;

    // estimate_grossexpanse = estimateReportdata["voyage_manager"][
    //   "ops_type_name"
    // ] === "TCOV"  || "Voyage Estimate"
    //   ? estimateReportdata["estimate"]&&estimateReportdata["estimate"]["expenses"]&&estimateReportdata["estimate"]["expenses"][
    //       "grossexpensee"
    //     ]
    //   : estimateReportdata["voyage_manager"][
    //       "ops_type_name"
    //     ] === "TCTO" || "TC Estimate"
    //     ? estimateReportdata["estimate"]&&estimateReportdata["estimate"]["expenses"]&&estimateReportdata["estimate"]["expenses"][
    //         "gross_expenses"
    //       ]
    //     : undefined
    estimate_grossexpanse =
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate"
        ? estimateReportdata["estimate"] &&
          estimateReportdata["estimate"]["expenses"] &&
          estimateReportdata["estimate"]["expenses"]["grossexpensee"]
        : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
          estimateReportdata["voyage_manager"]["ops_type_name"] ===
            "TC Estimate"
        ? estimateReportdata["estimate"] &&
          estimateReportdata["estimate"]["expenses"] &&
          estimateReportdata["estimate"]["expenses"]["gross_expenses"]
        : undefined;

    actual_grossexpanse =
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate"
        ? estimateReportdata["actual"] &&
          estimateReportdata["actual"]["expenses"] &&
          estimateReportdata["actual"]["expenses"]["grossexpensee"]
        : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
          estimateReportdata["voyage_manager"]["ops_type_name"] ===
            "TC Estimate"
        ? estimateReportdata["actual"] &&
          estimateReportdata["actual"]["expenses"] &&
          estimateReportdata["actual"]["expenses"]["gross_expenses"]
        : undefined;

    estimate_net_expenses =
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate"
        ? estimateReportdata["estimate"] &&
          estimateReportdata["estimate"]["expenses"] &&
          estimateReportdata["estimate"]["expenses"]["total_expenses"]
        : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
          estimateReportdata["voyage_manager"]["ops_type_name"] ===
            "TC Estimate"
        ? estimateReportdata["estimate"] &&
          estimateReportdata["estimate"]["expenses"] &&
          estimateReportdata["estimate"]["expenses"]["netExpenses"]
        : undefined;

    actual_net_expenses =
      estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
      estimateReportdata["voyage_manager"]["ops_type_name"] ===
        "Voyage Estimate"
        ? estimateReportdata["actual"] &&
          estimateReportdata["actual"]["expenses"] &&
          estimateReportdata["actual"]["expenses"]["total_expenses"]
        : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
          estimateReportdata["voyage_manager"]["ops_type_name"] ===
            "TC Estimate"
        ? estimateReportdata["actual"] &&
          estimateReportdata["actual"]["expenses"] &&
          estimateReportdata["actual"]["expenses"]["netExpenses"]
        : undefined;
  }

  if (
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
  ) {
    estimate_freight =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["freight"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["freight"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_freight =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["freight"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["freight"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_freight_commission =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["freight_commission"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "freight_commission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_freight_commission =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["freight_commission"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "freight_commission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_demmurage =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["demmurage"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["demmurage"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_demmurage =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["demmurage"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["demmurage"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_demmurageCommission =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["demmurageCommission"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "demmurageCommission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_demmurageCommission =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["demmurageCommission"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "demmurageCommission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_desmurage = estimate_desmurage =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["desmurage"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["desmurage"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_desmurage =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["desmurage"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["desmurage"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_desmurageCommission =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["desmurageCommission"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "desmurageCommission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_desmurageCommission =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["desmurageCommission"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "desmurageCommission"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
  }

  if (
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
  ) {
    estimate_tco_hire =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["tco_hire"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["tco_hire"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_tco_hire =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["tco_hire"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["tco_hire"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_tco_b_comm =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["tco_b_comm"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["tco_b_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_tco_b_comm =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["tco_b_comm"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["tco_b_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_tco_add_comm =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["tco_add_comm"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "tco_add_comm"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_tco_add_comm =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["tco_add_comm"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["tco_add_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_tco_bb =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["tco_bb"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["tco_bb"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_tco_bb =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["tco_bb"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["tco_bb"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
  }

  if (estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO") {
    estimate_grossfreight =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["grossfreight"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "grossfreight"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_grossfreight =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["grossfreight"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["grossfreight"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_ttl_comm =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["ttl_comm"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"]["ttl_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    actual_ttl_comm =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["ttl_comm"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"]["ttl_comm"].replaceAll(
              ",",
              ""
            ) * 1
          ).toFixed(2)
        : 0;
    estimate_demmurage_sales =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["demmurage_sales"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "demmurage_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_demmurage_sales =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["demmurage_sales"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "demmurage_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    estimate_desmurageCommission_sales =
      estimateReportdata["estimate"] &&
      estimateReportdata["estimate"]["revenue"] &&
      estimateReportdata["estimate"]["revenue"]["desmurageCommission_sales"]
        ? parseFloat(
            estimateReportdata["estimate"]["revenue"][
              "desmurageCommission_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
    actual_desmurageCommission_sales =
      estimateReportdata["actual"] &&
      estimateReportdata["actual"]["revenue"] &&
      estimateReportdata["actual"]["revenue"]["desmurageCommission_sales"]
        ? parseFloat(
            estimateReportdata["actual"]["revenue"][
              "desmurageCommission_sales"
            ].replaceAll(",", "") * 1
          ).toFixed(2)
        : 0;
  }
  const estimate_miscRevenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["mis_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["misc_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["extra_rev"]
      : undefined;
  const actual_miscRevenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["mis_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["misc_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["extra_rev"]
      : undefined;
  const estimate_gross_Revenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["gross_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["gross_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["gross_revenue"]
      : undefined;
  const actual_Gross_Revenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["gross_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["gross_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["gross_revenue"]
      : undefined;
  const estimate_Net_Revenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["net_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["net_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["estimate"] &&
        estimateReportdata["estimate"]["revenue"] &&
        estimateReportdata["estimate"]["revenue"]["net_revenue"]
      : undefined;
  const actual_Net_Revenue =
    estimateReportdata["voyage_manager"]["ops_type_name"] === "TCOV" ||
    estimateReportdata["voyage_manager"]["ops_type_name"] === "Voyage Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["net_revenue"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "TCTO" ||
        estimateReportdata["voyage_manager"]["ops_type_name"] === "TC Estimate"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["net_rev"]
      : estimateReportdata["voyage_manager"]["ops_type_name"] === "VIVO"
      ? estimateReportdata["actual"] &&
        estimateReportdata["actual"]["revenue"] &&
        estimateReportdata["actual"]["revenue"]["net_revenue"]
      : undefined;

  const Sea_consumption_obj =
    estimateReportdata["estimate"]["expenses"] &&
    estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
    estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
      "sea_consumption"
    ]
      ? estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
          "sea_consumption"
        ]
      : {};
  const port_consumption_obj =
    estimateReportdata["estimate"]["expenses"] &&
    estimateReportdata["estimate"]["expenses"]["bunker_expenses"] &&
    estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
      "port_consumption"
    ]
      ? estimateReportdata["estimate"]["expenses"]["bunker_expenses"][
          "port_consumption"
        ]
      : {};
  for (let key in Sea_consumption_obj) {
    estimate_Sea_consumption +=
      parseFloat(Sea_consumption_obj[key].replaceAll(",", "")) * 1;
  }

  for (let key in port_consumption_obj) {
    estimate_port_consumption +=
      parseFloat(port_consumption_obj[key].replaceAll(",", "")) * 1;
  }

  estimate_bunker_expense =
    estimate_Sea_consumption + estimate_port_consumption;

  return (
    <article className="article toolbaruiWrapper" ref={ref}>
      <div className="box box-default" id="divToPrint">
        <div className="box-body">
          <div className="invoice-inner-download mt-3">
            <div className="row">
              <div className="col-12 text-center">
                <img
                  className="reportlogo"
                  src={estimateReportdata["voyage_manager"]["logo"]}
                  alt="No img"
                />
                <p className="sub-title m-0">
                  {estimateReportdata["voyage_manager"]["my_company_lob_name"]
                    ? estimateReportdata["voyage_manager"][
                        "my_company_lob_name"
                      ]
                    : " "}
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 mx-auto">
              <div className="text-center invoice-top-address">
                <p>
                  {estimateReportdata["voyage_manager"]["address"]
                    ? estimateReportdata["voyage_manager"]["address"]
                    : " "}
                </p>
              </div>
            </div>
          </div>

          <table className="table table-bordered table-invoice-report-colum">
            <tbody>
              <tr>
                <td className="font-weight-bold">Vessel Name :</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["vessel_name"]
                    ? estimateReportdata["voyage_manager"]["vessel_name"]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">B.Port:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["b_port_name"]
                    ? estimateReportdata["voyage_manager"]["b_port_name"]
                    : 0.0}
                </td> */}

                <td className="font-weight-bold">My Company/LOB:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["my_company_lob_name"]
                    ? estimateReportdata["voyage_manager"][
                        "my_company_lob_name"
                      ]
                    : 0.0}
                  /{" "}
                  {estimateReportdata["voyage_manager"]["company_lob_name"]
                    ? estimateReportdata["voyage_manager"]["company_lob_name"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Status:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["cvm_status_name"]
                    ? estimateReportdata["voyage_manager"]["cvm_status_name"]
                    : 0.0}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">Vessel Code / Voy No.:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["vessel_code"]
                    ? estimateReportdata["voyage_manager"]["vessel_code"]
                    : 0.0}
                  /
                  {estimateReportdata["voyage_manager"]["estimate_id"]
                    ? estimateReportdata["voyage_manager"]["estimate_id"]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">Repos. Port:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["reposition_port_name"]
                    ? estimateReportdata["voyage_manager"][
                        "reposition_port_name"
                      ]
                    : "0.00"}
                </td> */}

                <td className="font-weight-bold">CP Date:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["cp_date"]
                    ? estimateReportdata["voyage_manager"]["cp_date"]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">Total Days:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["total_days"]
                    ? estimateReportdata["voyage_manager"]["total_days"]
                    : "0.00"}
                </td> */}

                <td className="font-weight-bold">Voyage Ops Type:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["ops_type_name"]
                    ? estimateReportdata["voyage_manager"]["ops_type_name"]
                    : 0.0}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">HF/TCI Code:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["tc_code"]
                    ? estimateReportdata["voyage_manager"]["tc_code"]
                    : "N/A"}
                </td>

                {/* <td className="font-weight-bold">Repos. Days:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["repso_days"]
                    ? estimateReportdata["voyage_manager"]["repso_days"]
                    : "0.00"}
                </td> */}

                <td className="font-weight-bold">WF (%):</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["dwf"]
                    ? estimateReportdata["voyage_manager"]["dwf"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Trader / Ops User:</td>
                <td>
                  {" "}
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["fixed_by_user"]
                    ? estimateReportdata["voyage_manager"]["fixed_by_user"]
                    : "N/A"}{" "}
                  /{" "}
                  {estimateReportdata["voyage_manager"]["ops_user_name"]
                    ? estimateReportdata["voyage_manager"]["ops_user_name"]
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">HF/TCI Hire Rate:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["tci_d_hire"]
                    ? estimateReportdata["voyage_manager"]["tci_d_hire"]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">Trade Area:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["trade_area_name"]
                    ? estimateReportdata["voyage_manager"]["trade_area_name"]
                    : 0.0}
                </td> */}

                <td className="font-weight-bold">Other Cost:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["other_cost"]
                    ? estimateReportdata["voyage_manager"]["other_cost"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Commence Date:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["commence_date"]
                    ? estimateReportdata["voyage_manager"]["commence_date"]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">Other Rev:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["mis_cost"]
                    ? estimateReportdata["voyage_manager"]["mis_cost"]
                    : 0.0}
                </td> */}
              </tr>

              <tr>
                <td className="font-weight-bold">TCI Add/Bro(%):</td>
                <td>
                  {" "}
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["add_percentage"]
                    ? estimateReportdata["voyage_manager"]["add_percentage"]
                    : 0.0}{" "}
                  /{" "}
                  {estimateReportdata["voyage_manager"]["bro_percentage"]
                    ? estimateReportdata["voyage_manager"]["bro_percentage"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Ballast Bonus:</td>

                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["bb"]
                    ? estimateReportdata["voyage_manager"]["bb"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Completing Date:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["completing_date"]
                    ? estimateReportdata["voyage_manager"]["completing_date"]
                    : 0.0}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">TCO Code:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["tco_code"]
                    ? estimateReportdata["voyage_manager"]["tco_code"]
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Other Revenue</td>

                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["mis_cost"]
                    ? estimateReportdata["voyage_manager"]["mis_cost"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Total Voyage Days:</td>

                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["total_days"]
                    ? estimateReportdata["voyage_manager"]["total_days"]
                    : 0.0}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">Eca Fuel Grade:</td>

                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["eca_fuel_grade_name"]
                    ? estimateReportdata["voyage_manager"][
                        "eca_fuel_grade_name"
                      ]
                    : 0.0}
                </td>

                {/* <td className="font-weight-bold">TCO Add/Bro(%):</td>
                <td>
                  {" "}
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["tco_add_percentage"]
                    ? estimateReportdata["voyage_manager"]["tco_add_percentage"]
                    : 0.0}{" "}
                  /{" "}
                  {estimateReportdata["voyage_manager"]["tco_bro_percentage"]
                    ? estimateReportdata["voyage_manager"]["tco_bro_percentage"]
                    : 0.0}
                </td> */}
                <td className="font-weight-bold">Routing Type:</td>
                <td>
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["routing_type_name"]
                    ? estimateReportdata["voyage_manager"]["routing_type_name"]
                    : 0.0}
                </td>

                <td className="font-weight-bold">Trade Area:</td>

                <td>
                  {" "}
                  {estimateReportdata &&
                  estimateReportdata["voyage_manager"] &&
                  estimateReportdata["voyage_manager"]["trade_area_name"]
                    ? estimateReportdata["voyage_manager"]["trade_area_name"]
                    : 0.0}{" "}
                </td>
              </tr>
            </tbody>
          </table>

          <h4 className="font-weight-bold">Profit & Loss Summary</h4>
          <table className="table border table-striped table-invoice-report">
            <thead>
              {/* <tr>
                  <th rowSpan="2" />
                  <th rowSpan="2">Estimate</th>
                  <th colSpan="2" className="text-center">
                    Actual
                  </th>
                  <th colSpan="4" className="text-center">
                    Variance
                  </th>
                </tr> */}
              <tr>
                <th rowSpan="2" />
                <th>Estimate</th>
                <th>Actual</th>
                <th>Posted</th>
                <th>Cash In</th>
                <th>Diff(Act.-Est.)</th>
                <th>%Diff</th>
                <th>Post Vs Cash</th>
                <th>% Post vs cash</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td
                  colSpan="9"
                  className="font-weight-bold pl-2"
                  style={{ color: "#12406A", fontSize: "18px" }}
                >
                  Revenue
                </td>
              </tr>

              {estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "TCOV" ||
              estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "Voyage Estimate" ? (
                <>
                  <tr>
                    <td className="pl-4">Freight</td>
                    <td>{estimate_freight}</td>
                    <td>{actual_freight}</td>
                    <td>{parseValue(post["frightInvoice_posted"])}</td>
                    <td>{parseValue(cash["frightInvoice_cash"])}</td>
                    <td>{calculateDiff(actual_freight, estimate_freight)}</td>
                    <td>
                      {calculatePercentDiff(actual_freight, estimate_freight)}
                    </td>
                    <td>
                      {calculateDiff(
                        post["frightInvoice_posted"],
                        cash["frightInvoice_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["frightInvoice_posted"],
                        cash["frightInvoice_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Freight Commission</td>
                    <td>{estimate_freight_commission}</td>
                    <td>{actual_freight_commission}</td>
                    <td>{parseValue(post["frightComm_posted"])}</td>
                    <td>{parseValue(cash["frightComm_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_freight_commission,
                        estimate_freight_commission
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_freight_commission,
                        estimate_freight_commission
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["frightComm_posted"],
                        cash["frightComm_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["frightComm_posted"],
                        cash["frightComm_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Misc. Revenue</td>
                    <td>{estimate_miscRevenue}</td>
                    <td>{actual_miscRevenue}</td>
                    <td>{parseValue(post["miscRev_posted"])}</td>
                    <td>{parseValue(cash["miscRev_cash"])}</td>
                    <td>
                      {calculateDiff(actual_miscRevenue, estimate_miscRevenue)}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_miscRevenue,
                        estimate_miscRevenue
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["miscRev_posted"],
                        cash["miscRev_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["miscRev_posted"],
                        cash["miscRev_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Amt</td>
                    <td>{estimate_demmurage}</td>
                    <td>{actual_demmurage}</td>
                    <td>{parseValue(post["demAmt_posted"])}</td>
                    <td>{parseValue(cash["demAmt_cash"])}</td>
                    <td>
                      {calculateDiff(actual_demmurage, estimate_demmurage)}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_demmurage,
                        estimate_demmurage
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["demAmt_posted"],
                        cash["demAmt_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["demAmt_posted"],
                        cash["demAmt_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Comm Amt</td>
                    <td>{estimate_demmurageCommission}</td>
                    <td>{actual_demmurageCommission}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_demmurageCommission,
                        estimate_demmurageCommission
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_demmurageCommission,
                        estimate_demmurageCommission
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Des Amt</td>
                    <td>{estimate_desmurage}</td>
                    <td>{actual_desmurage}</td>
                    <td>{parseValue(post["desAmt_posted"])}</td>
                    <td>{parseValue(cash["desAmt_cash"])}</td>
                    <td>
                      {calculateDiff(actual_desmurage, estimate_desmurage)}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_desmurage,
                        estimate_desmurage
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["desAmt_posted"],
                        cash["desAmt_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["desAmt_posted"],
                        cash["desAmt_cash"]
                      )}
                    </td>
                  </tr>
                </>
              ) : estimateReportdata["voyage_manager"]["ops_type_name"] ===
                  "TCTO" ||
                estimateReportdata["voyage_manager"]["ops_type_name"] ===
                  "TC Estimate" ? (
                <>
                  <tr>
                    <td className="pl-4">TCO Hire $</td>
                    <td>{estimate_tco_hire}</td>
                    <td>{actual_tco_hire}</td>
                    <td>{parseValue(post["tcohireExpenses_posted"])}</td>
                    <td>{parseValue(cash["tcohireExpenses_cash"])}</td>
                    <td>{calculateDiff(actual_tco_hire, estimate_tco_hire)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_hire, estimate_tco_hire)}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tcohireExpenses_posted"],
                        cash["tcohireExpenses_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tcohireExpenses_posted"],
                        cash["tcohireExpenses_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCO Hire B. comm</td>
                    <td>{estimate_tco_b_comm}</td>
                    <td>{actual_tco_b_comm}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(actual_tco_b_comm, estimate_tco_b_comm)}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tco_b_comm,
                        estimate_tco_b_comm
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCO Hire add com</td>
                    <td>{estimate_tco_add_comm}</td>
                    <td>{actual_tco_add_comm}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_tco_add_comm,
                        estimate_tco_add_comm
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tco_add_comm,
                        estimate_tco_add_comm
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCO BB</td>
                    <td>{estimate_tco_bb}</td>
                    <td>{actual_tco_bb}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCO BB Comm</td>
                    <td>{estimate_tco_b_comm}</td>
                    <td>{actual_tco_b_comm}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>     

                  <tr>
                    <td className="pl-4">Net Ballast Bonus</td>
                    <td>{estimate_tco_bb}</td>
                    <td>{actual_tco_bb}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>  

                  <tr>
                  <td className="pl-4">Misc. Revenue</td>
                    <td>{estimate_tco_bb}</td>
                    <td>{actual_tco_bb}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                  <td className="pl-4">Total TCO Net Hire</td>
                    <td>{estimate_tco_bb}</td>
                    <td>{actual_tco_bb}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                  <td className="pl-4">Total Net TCO BB</td>
                    <td>{estimate_tco_bb}</td>
                    <td>{actual_tco_bb}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_tco_bb, estimate_tco_bb)}</td>
                    <td>
                      {calculatePercentDiff(actual_tco_bb, estimate_tco_bb)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  

                </>
              ) : estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "VIVO" ? (
                <>
                  <tr>
                    <td className="pl-4">Gross Frieght</td>
                    <td>{estimate_grossfreight}</td>
                    <td>{actual_grossfreight}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_grossfreight,
                        estimate_grossfreight
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_grossfreight,
                        estimate_grossfreight
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Total Comm.</td>
                    <td>{estimate_ttl_comm}</td>
                    <td>{actual_ttl_comm}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>{calculateDiff(actual_ttl_comm, estimate_ttl_comm)}</td>
                    <td>
                      {calculatePercentDiff(actual_ttl_comm, estimate_ttl_comm)}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Amt</td>
                    <td>{estimate_demmurage_sales}</td>
                    <td>{actual_demmurage_sales}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_demmurage_sales,
                        estimate_demmurage_sales
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_demmurage_sales,
                        estimate_demmurage_sales
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Comm Amt</td>
                    <td>{estimate_demmurageCommission_sales}</td>
                    <td>{actual_demmurageCommission_sales}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_demmurageCommission_sales,
                        estimate_demmurageCommission_sales
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_demmurageCommission_sales,
                        estimate_demmurageCommission_sales
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Des Amt</td>
                    <td>{estimate_desmurage_sales}</td>
                    <td>{actual_desmurage_sales}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_desmurage_sales,
                        estimate_desmurage_sales
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_desmurage_sales,
                        estimate_desmurage_sales
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                </>
              ) : undefined}

              <tr>
                <td className="pl-4">Gross Revenue</td>
                <td>{estimate_gross_Revenue}</td>

                <td>{actual_Gross_Revenue}</td>

                <td>0.00</td>
                <td>0.00</td>
                <td>
                  {calculateDiff(actual_Gross_Revenue, estimate_gross_Revenue)}
                </td>
                <td>
                  {calculatePercentDiff(
                    actual_Gross_Revenue,
                    estimate_gross_Revenue
                  )}
                </td>
                <td>0.00</td>
                <td>0.00</td>
              </tr>

              <tr style={{ backgroundColor: "#B4CDFF" }}>
                <td className="pl-4">Net Revenue</td>
                <td>{estimate_Net_Revenue}</td>
                <td>{actual_Net_Revenue}</td>
                <td>0.00</td>
                <td>0.00</td>
                <td>
                  {calculateDiff(actual_Net_Revenue, estimate_Net_Revenue)}
                </td>
                <td>
                  {calculatePercentDiff(
                    actual_Net_Revenue,
                    estimate_Net_Revenue
                  )}
                </td>
                <td>0.00</td>
                <td>0.00</td>
              </tr>
              {/*   EXpenese------------------------------------------------------------------------------------------------- */}

              <tr>
                <td
                  colSpan="9"
                  className="font-weight-bold pl-2"
                  style={{ color: "#12406A", fontSize: "18px" }}
                >
                  Expenses
                </td>
              </tr>

              {estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "TCOV" ||
              estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "Voyage Estimate" ||
              estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "TCTO" ||
              estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "TC Estimate" ? (
                <>
                  <tr>
                    <td
                      className="pl-4"
                      style={{ fontWeight: "600", fontSize: "15px" }}
                    >
                      Vessel expenses
                    </td>
                    <td>{estimate_vessel_expense.toFixed(2)}</td>
                    <td>{actual_vessel_expenses.toFixed(2)}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_vessel_expenses,
                        estimate_vessel_expense
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_vessel_expenses,
                        estimate_vessel_expense
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td
                      className="pl-4"
                      style={{ fontWeight: "600", fontSize: "15px" }}
                    >
                    Hire Expense
                    </td>
                    <td>{estimate_vessel_expense.toFixed(2)}</td>
                    <td>{actual_vessel_expenses.toFixed(2)}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_vessel_expenses,
                        estimate_vessel_expense
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_vessel_expenses,
                        estimate_vessel_expense
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  {/* <tr>
                      <td className="pl-6">Other Cost</td>
                      <td>
                        {
                         estimate_ballast_bonus
                        }
                      </td>
                      <td>
                        {
                         actual_ballast_bonus
                        }
                      </td>
                      <td>0.00</td>
                      <td>0.00</td>
                      <td>{calculateDiff(actual_ballast_bonus,estimate_ballast_bonus)}</td>
                      <td>{calculatePercentDiff(actual_ballast_bonus,estimate_ballast_bonus)}</td>
                      <td>0.00</td>
                      <td>0.00</td>
                    </tr> */}

                  <tr>
                    <td className="pl-4">TCI Add. Comm.</td>
                    <td>{estimate_tci_add_comm_exp}</td>
                    <td>{actual_tci_add_comm_exp}</td>
                    <td>{parseValue(post["tciaddcom_posted"])}</td>
                    <td>{parseValue(cash["tciaddcom_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_tci_add_comm_exp,
                        estimate_tci_add_comm_exp
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tci_add_comm_exp,
                        estimate_tci_add_comm_exp
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tciaddcom_posted"],
                        cash["tciaddcom_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tciaddcom_posted"],
                        cash["tciaddcom_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCI Broker Comm.</td>
                    <td>{estimate_tci_broker_comm}</td>
                    <td>{actual_tci_broker_comm}</td>
                    <td>{parseValue(post["tcibcom_posted"])}</td>
                    <td>{parseValue(cash["tcibcom_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCI Ballast Bonus</td>
                    <td>{estimate_tci_broker_comm}</td>
                    <td>{actual_tci_broker_comm}</td>
                    <td>{parseValue(post["tcibcom_posted"])}</td>
                    <td>{parseValue(cash["tcibcom_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">TCI BB Comm</td>
                    <td>{estimate_tci_broker_comm}</td>
                    <td>{actual_tci_broker_comm}</td>
                    <td>{parseValue(post["tcibcom_posted"])}</td>
                    <td>{parseValue(cash["tcibcom_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Reposition Cost</td>
                    <td>{estimate_tci_broker_comm}</td>
                    <td>{actual_tci_broker_comm}</td>
                    <td>{parseValue(post["tcibcom_posted"])}</td>
                    <td>{parseValue(cash["tcibcom_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_tci_broker_comm,
                        estimate_tci_broker_comm
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["tcibcom_posted"],
                        cash["tcibcom_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Port Expenses</td>
                    <td>{estimate_port_expenses}</td>
                    <td>{actual_port_expenses}</td>
                    <td>{parseValue(post["portExpenses_posted"])}</td>
                    <td>{parseValue(cash["portExpenses_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Emission Expenses</td>
                    <td>{estimate_port_expenses}</td>
                    <td>{actual_port_expenses}</td>
                    <td>{parseValue(post["portExpenses_posted"])}</td>
                    <td>{parseValue(cash["portExpenses_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                  </tr>

                  {/* <tr>
                    <td className="pl-4">Misc.Expenses</td>
                    <td>{estimate_port_expenses}</td>
                    <td>{actual_port_expenses}</td>
                    <td>{parseValue(post["portExpenses_posted"])}</td>
                    <td>{parseValue(cash["portExpenses_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_expenses,
                        estimate_port_expenses
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["portExpenses_posted"],
                        cash["portExpenses_cash"]
                      )}
                    </td>
                  </tr> */}

                  <tr>
                    <td
                      className="pl-4"
                      style={{ fontWeight: "600", fontSize: "15px" }}
                    >
                      Bunker Expenses
                    </td>
                    <td>{estimate_bunker_expense.toFixed(2)}</td>
                    <td>{actual_bunker_expense.toFixed(2)}</td>
                    <td>{parseValue(post["bunkerExpenses_posted"])}</td>
                    <td>{parseValue(cash["bunkerExpenses_cash"])}</td>
                    <td>
                      {calculateDiff(
                        actual_bunker_expense,
                        estimate_bunker_expense
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_bunker_expense,
                        estimate_bunker_expense
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["bunkerExpenses_posted"],
                        cash["bunkerExpenses_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["bunkerExpenses_posted"],
                        cash["bunkerExpenses_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-6">Sea Consumption Non ECA</td>
                    <td>{estimate_Sea_consumption.toFixed(2)}</td>
                    <td>{actual_Sea_consumption}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_Sea_consumption,
                        estimate_Sea_consumption
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_Sea_consumption,
                        estimate_Sea_consumption
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">IFO</td>
                    <td>{estimate_sea_consumption_ifo}</td>
                    <td>{actual_sea_consumption_ifo}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_ifo,
                        estimate_sea_consumption_ifo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_ifo,
                        estimate_sea_consumption_ifo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">MGO</td>
                    <td>{estimate_sea_consumption_mgo}</td>
                    <td>{actual_sea_consumption_mgo}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_mgo,
                        estimate_sea_consumption_mgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_mgo,
                        estimate_sea_consumption_mgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">VLSFO</td>
                    <td>{estimate_sea_consumption_vlsfo}</td>
                    <td>{actual_sea_consumption_vlsfo}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_vlsfo,
                        estimate_sea_consumption_vlsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_vlsfo,
                        estimate_sea_consumption_vlsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">LSMGO</td>
                    <td>{estimate_sea_consumption_lsmgo}</td>
                    <td>{actual_sea_consumption_lsmgo}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_lsmgo,
                        estimate_sea_consumption_lsmgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_lsmgo,
                        estimate_sea_consumption_lsmgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">ULSFO</td>
                    <td>{estimate_sea_consumption_ulsfo}</td>
                    <td>{actual_sea_consumption_ulsfo}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_ulsfo,
                        estimate_sea_consumption_ulsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_ulsfo,
                        estimate_sea_consumption_ulsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  {/*------------------- sea Eca------------------- */}
                  <tr>
                    <td className="pl-6">Sea Consumption ECA</td>
                    <td>{estimate_Sea_consumption.toFixed(2)}</td>
                    <td>{actual_Sea_consumption}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_Sea_consumption,
                        estimate_Sea_consumption
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_Sea_consumption,
                        estimate_Sea_consumption
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">IFO</td>
                    {/* <td>{estimate_sea_consumption_ifo}</td> */}
                    <td>{total_eca_sea_cons_est.ifo}</td>
                    <td>{total_eca_sea_cons.ifo}</td>
                    {/* <td>{actual_sea_consumption_ifo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_ifo,
                        estimate_sea_consumption_ifo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_ifo,
                        estimate_sea_consumption_ifo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">MGO</td>
                    {/* <td>{estimate_sea_consumption_mgo}</td> */}
                        <td>{total_eca_sea_cons_est.mgo}</td>
                    <td>{total_eca_sea_cons.mgo}</td>
                    {/* <td>{actual_sea_consumption_mgo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_mgo,
                        estimate_sea_consumption_mgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_mgo,
                        estimate_sea_consumption_mgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">VLSFO</td>
                    {/* <td>{estimate_sea_consumption_mgo}</td> */}
                    <td>{total_eca_sea_cons_est.vlsfo}</td>
                    <td>{total_eca_sea_cons.vlsfo}</td>
                    {/* <td>{actual_sea_consumption_vlsfo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_vlsfo,
                        estimate_sea_consumption_vlsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_vlsfo,
                        estimate_sea_consumption_vlsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">LSMGO</td>
                    {/* <td>{estimate_sea_consumption_lsmgo}</td> */}
                    <td>{total_eca_sea_cons_est.lsmgo}</td>
                    <td>{total_eca_sea_cons.lsmgo}</td>
                    {/* <td>{actual_sea_consumption_lsmgo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_lsmgo,
                        estimate_sea_consumption_lsmgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_lsmgo,
                        estimate_sea_consumption_lsmgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">ULSFO</td>
                    {/* <td>{estimate_sea_consumption_mgo}</td> */}
                    <td>{total_eca_sea_cons_est.ulsfo}</td>
                    <td>{total_eca_sea_cons.ulsfo}</td>
                    {/* <td>{actual_sea_consumption_ulsfo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_sea_consumption_ulsfo,
                        estimate_sea_consumption_ulsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_sea_consumption_ulsfo,
                        estimate_sea_consumption_ulsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Port Consumption</td>
                    <td>{estimate_port_consumption.toFixed(2)}</td>
                    <td>{actual_port_consumption}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption,
                        estimate_port_consumption
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption,
                        estimate_port_consumption
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">IFO</td>
                    <td>{total_noneca_port_cons_est.ifo.toFixed(2)}</td>
                    <td>{total_noneca_port_cons.ifo.toFixed(2)}</td>
                    {/* <td>{actual_port_consumption_ifo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_ifo,
                        estimate_port_consumption_ifo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_ifo,
                        estimate_port_consumption_ifo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">MGO</td>
                    <td>{total_noneca_port_cons_est.mgo}</td>
                    <td>{total_noneca_port_cons.mgo}</td>
                    {/* <td>{actual_port_consumption_mgo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_mgo,
                        estimate_port_consumption_mgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_mgo,
                        estimate_port_consumption_mgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">VLSFO</td>
                    <td>{total_noneca_port_cons_est.vlsfo.toFixed(2)}</td>
                    <td>{total_noneca_port_cons.vlsfo.toFixed(2)}</td>
                    {/* <td>{actual_port_consumption_vlsfo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_vlsfo,
                        estimate_port_consumption_vlsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_vlsfo,
                        estimate_port_consumption_vlsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">LSMGO</td>
                    <td>{total_noneca_port_cons_est.lsmgo}</td>
                    <td>{total_noneca_port_cons.lsmgo}</td>
                    {/* <td>{actual_port_consumption_lsmgo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_lsmgo,
                        estimate_port_consumption_lsmgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_lsmgo,
                        estimate_port_consumption_lsmgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-8">ULSFO</td>
                    <td>{total_noneca_port_cons_est.ulsfo}</td>
                    <td>{total_noneca_port_cons.ulsfo}</td>
                    {/* <td>{actual_port_consumption_ulsfo}</td> */}
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_ulsfo,
                        estimate_port_consumption_ulsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_ulsfo,
                        estimate_port_consumption_ulsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  {/*---------------------- port ECa ----------------*/}
                  {/* <tr>
                    <td className="pl-6">Port Consumption ECA</td>
                    <td>{estimate_port_consumption.toFixed(2)}</td>
                    <td>{actual_port_consumption}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption,
                        estimate_port_consumption
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption,
                        estimate_port_consumption
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  {/* <tr>
                    <td className="pl-8">IFO</td>
                    <td>{total_eca_port_cons_est.ifo}</td>
                    <td>{total_eca_port_cons.ifo}</td> */}
                    {/* <td>{actual_port_consumption_ifo}</td> */}
                    {/* <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_ifo,
                        estimate_port_consumption_ifo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_ifo,
                        estimate_port_consumption_ifo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td> */}
                  {/* </tr> */}
{/* 
                  <tr>
                    <td className="pl-8">MGO</td>
                    <td>{total_eca_port_cons_est.mgo}</td>
                    <td>{total_eca_port_cons.mgo}</td> */}
                    {/* <td>{actual_port_consumption_mgo}</td> */}
                    {/* <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_mgo,
                        estimate_port_consumption_mgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_mgo,
                        estimate_port_consumption_mgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  {/* <tr>
                    <td className="pl-8">VLSFO</td>
                    <td>{total_eca_port_cons_est.vlsfo}</td>
                    <td>{total_eca_port_cons.vlsfo}</td> */}
                    {/* <td>{actual_port_consumption_vlsfo}</td> */}
                    {/* <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_vlsfo,
                        estimate_port_consumption_vlsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_vlsfo,
                        estimate_port_consumption_vlsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  {/* <tr>
                    <td className="pl-8">LSMGO</td>
                    <td>{total_eca_port_cons_est.lsmgo}</td>
                    <td>{total_eca_port_cons.lsmgo}</td> */}
                    {/* <td>{actual_port_consumption_lsmgo}</td> */}
                    {/* <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_lsmgo,
                        estimate_port_consumption_lsmgo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_lsmgo,
                        estimate_port_consumption_lsmgo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  {/* <tr>
                    <td className="pl-8">ULSFO</td>
                    <td>{total_eca_port_cons_est.ulsfo}</td>
                    <td>{total_eca_port_cons.ulsfo}</td> */}
                    {/* <td>{actual_port_consumption_ulsfo}</td> */}
                    {/* <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_port_consumption_ulsfo,
                        estimate_port_consumption_ulsfo
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_port_consumption_ulsfo,
                        estimate_port_consumption_ulsfo
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  <tr>
                    <td className="pl-4">Other Exp</td>
                    <td>{parseValue(expensesEstimateVivo["other_exp"])}</td>
                    <td>{parseValue(expensesActualVivo["other_exp"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["other_exp"],
                        expensesEstimateVivo["other_exp"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["other_exp"],
                        expensesEstimateVivo["other_exp"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Gross Expenses</td>
                    <td>{estimate_grossexpanse}</td>
                    <td>{actual_grossexpanse}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_grossexpanse,
                        estimate_grossexpanse
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_grossexpanse,
                        estimate_grossexpanse
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr style={{ backgroundColor: "#fca38b" }}>
                    <td
                      className="pl-4"
                      style={{ fontWeight: "600", fontSize: "15px" }}
                    >
                      Net Expenses
                    </td>
                    <td>{estimate_net_expenses}</td>

                    <td>{actual_net_expenses}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        actual_net_expenses,
                        estimate_net_expenses
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        actual_net_expenses,
                        estimate_net_expenses
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                </>
              ) : estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "VIVO" ? (
                <>
                  <tr>
                    <td className="pl-4">Gross Frieght</td>
                    <td>{parseValue(expensesEstimateVivo["grossfright"])}</td>
                    <td>{parseValue(expensesActualVivo["grossfright"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["grossfright"],
                        expensesEstimateVivo["grossfright"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["grossfright"],
                        expensesEstimateVivo["grossfright"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Ttl Comm</td>
                    <td>{parseValue(expensesEstimateVivo["ttl_comm"])}</td>
                    <td>{parseValue(expensesActualVivo["ttl_comm"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["ttl_comm"],
                        expensesEstimateVivo["ttl_comm"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["ttl_comm"],
                        expensesEstimateVivo["ttl_comm"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Other Revenue</td>
                    <td>{parseValue(expensesEstimateVivo["extra_rev"])}</td>
                    <td>{parseValue(expensesActualVivo["extra_rev"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["extra_rev"],
                        expensesEstimateVivo["extra_rev"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["extra_rev"],
                        expensesEstimateVivo["extra_rev"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Amt</td>
                    <td>
                      {parseValue(expensesEstimateVivo["demmurage_purch"])}
                    </td>
                    <td>{parseValue(expensesActualVivo["demmurage_purch"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["demmurage_purch"],
                        expensesEstimateVivo["demmurage_purch"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["demmurage_purch"],
                        expensesEstimateVivo["demmurage_purch"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Dem Comm Amt</td>
                    <td>
                      {parseValue(
                        expensesEstimateVivo["demmurageCommission_purch"]
                      )}
                    </td>
                    <td>
                      {parseValue(
                        expensesActualVivo["demmurageCommission_purch"]
                      )}
                    </td>
                    <td>{parseValue(post["demComAmt_posted"])}</td>
                    <td>{parseValue(post["demComAmt_cash"])}</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["demmurageCommission_purch"],
                        expensesEstimateVivo["demmurageCommission_purch"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["demmurageCommission_purch"],
                        expensesEstimateVivo["demmurageCommission_purch"]
                      )}
                    </td>
                    <td>
                      {calculateDiff(
                        post["demComAmt_posted"],
                        post["demComAmt_cash"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiffPostCash(
                        post["demComAmt_posted"],
                        post["demComAmt_cash"]
                      )}
                    </td>
                  </tr>

                  <tr>
                    <td className="pl-4">Des Amt</td>
                    <td>
                      {parseValue(expensesEstimateVivo["desmurage_purch"])}
                    </td>
                    <td>{parseValue(expensesActualVivo["desmurage_purch"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["desmurage_purch"],
                        expensesEstimateVivo["desmurage_purch"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["desmurage_purch"],
                        expensesEstimateVivo["desmurage_purch"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Des Comm Amt</td>
                    <td>
                      {parseValue(
                        expensesEstimateVivo["desmurageCommission_purch"]
                      )}
                    </td>
                    <td>
                      {parseValue(
                        expensesActualVivo["desmurageCommission_purch"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["desmurageCommission_purch"],
                        expensesEstimateVivo["desmurageCommission_purch"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["desmurageCommission_purch"],
                        expensesEstimateVivo["desmurageCommission_purch"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-4">Other Exp</td>
                    <td>{parseValue(expensesEstimateVivo["other_exp"])}</td>
                    <td>{parseValue(expensesActualVivo["other_exp"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["other_exp"],
                        expensesEstimateVivo["other_exp"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["other_exp"],
                        expensesEstimateVivo["other_exp"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  {/* <tr>
                    <td className="pl-4">Other Expenses</td>
                    <td>{parseValue(expensesEstimateVivo["grossexpanse"])}</td>
                    <td>{parseValue(expensesActualVivo["grossexpanse"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["grossexpanse"],
                        expensesEstimateVivo["grossexpanse"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["grossexpanse"],
                        expensesEstimateVivo["grossexpanse"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> */}

                  <tr>
                    <td className="pl-4">Gross Expenses </td>
                    <td>{parseValue(expensesEstimateVivo["grossexpanse"])}</td>
                    <td>{parseValue(expensesActualVivo["grossexpanse"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["grossexpanse"],
                        expensesEstimateVivo["grossexpanse"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["grossexpanse"],
                        expensesEstimateVivo["grossexpanse"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr style={{ backgroundColor: "#a52b2b" }}>
                    <td
                      className="pl-4"
                      style={{ fontWeight: "600", fontSize: "15px" }}
                    >
                      Net Expenses
                    </td>
                    <td>{parseValue(expensesEstimateVivo["netexpanse"])}</td>
                    <td>{parseValue(expensesActualVivo["netexpanse"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        expensesActualVivo["netexpanse"],
                        expensesEstimateVivo["netexpanse"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        expensesActualVivo["netexpanse"],
                        expensesEstimateVivo["netexpanse"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                </>
              ) : undefined}

              <tr>
                <td
                  className="font-weight-bold pl-2"
                  style={{
                    fontWeight: "600",
                    fontSize: "18px",
                    color: "#12406A",
                    
                  }}
                  colSpan="12"
                >
                  Voyage Result 
                </td>
              </tr>

              {estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "TCOV" ||
              estimateReportdata["voyage_manager"]["ops_type_name"] ===
                "Voyage Estimate" ? (
                <>
                  <tr>
                    <td className="pl-6">Profit (Loss)</td>
                    <td>
                      {parseValue(voyageResultEstimateTcov["profit_loss"])}
                    </td>

                    <td>{parseValue(voyageResultActualTcov["profit_loss"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["profit_loss"],
                        voyageResultEstimateTcov["profit_loss"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["profit_loss"],
                        voyageResultEstimateTcov["profit_loss"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Daily Profit (Loss)</td>
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["daily_profit_loss"]
                      )}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["daily_profit_loss"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["daily_profit_loss"],
                        voyageResultEstimateTcov["daily_profit_loss"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["daily_profit_loss"],
                        voyageResultEstimateTcov["daily_profit_loss"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                 
                  



                  <tr>
                    <td className="pl-6">TCE Hire ( Net Daily )</td>
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["tce_hire_net_daily"]
                      )}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["tce_hire_net_daily"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["tce_hire_net_daily"],
                        voyageResultEstimateTcov["tce_hire_net_daily"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["tce_hire_net_daily"],
                        voyageResultEstimateTcov["tce_hire_net_daily"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Gross TCE</td>

                    <td>{parseValue(voyageResultEstimateTcov["gross_tce"])}</td>

                    <td>{parseValue(voyageResultActualTcov["gross_tce"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["gross_tce"],
                        voyageResultEstimateTcov["gross_tce"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["gross_tce"],
                        voyageResultEstimateTcov["gross_tce"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Average Freight rate ($/T)</td>
                    <td>
                      {parseValue(voyageResultEstimateTcov["avargefright"])}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["avargefright"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["avargefright"],
                        voyageResultEstimateTcov["avargefright"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["avargefright"],
                        voyageResultEstimateTcov["avargefright"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                   <tr>
                    <td className="pl-6">Breakeven & Freight rate ($/T)</td>
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                    <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr> 

                  <tr>
                    <td className="pl-6">Co2 Cost </td>
                  
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                     <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> 
                       <td>{co2expense}</td>
                    <td>0.00</td>
                    <td>0.00</td> 
                     <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td> 
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    {/* <td>0.00</td> */}
                  </tr>

                  <tr>
                    <td className="pl-6">CO2 Adjusted Profit (Loss)</td>
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                    {/* <td>{CO2_adj_profit}</td> */}

                    {/* <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> */}

                    <td>{CO2_adj_profit}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">CO2 Adjusted (Net) TCE </td>
                    
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                    {/* <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> */}
                    <td>{co2_adjusted_tce}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Total Sea Days</td>
                    <td>
                      {parseValue(voyageResultEstimateTcov["total_sea_days"])}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["total_sea_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["total_sea_days"],
                        voyageResultEstimateTcov["total_sea_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["total_sea_days"],
                        voyageResultEstimateTcov["total_sea_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Total Port Days</td>
                    <td>
                      {parseValue(voyageResultEstimateTcov["total_port_days"])}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["total_port_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["total_port_days"],
                        voyageResultEstimateTcov["total_port_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["total_port_days"],
                        voyageResultEstimateTcov["total_port_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Net Voyage Days</td>
                    <td>
                      {parseValue(voyageResultEstimateTcov["net_voyage_days"])}
                    </td>

                    <td>
                      {parseValue(voyageResultActualTcov["net_voyage_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["net_voyage_days"],
                        voyageResultEstimateTcov["net_voyage_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["net_voyage_days"],
                        voyageResultEstimateTcov["net_voyage_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                </>
              ) : estimateReportdata["voyage_manager"]["ops_type_name"] ===
                  "VIVO" ||
                estimateReportdata["voyage_manager"]["ops_type_name"] ===
                  "TCTO" ||
                estimateReportdata["voyage_manager"]["ops_type_name"] ===
                  "TC Estimate" ? (
                <>
                  <tr>
                    <td className="pl-6">Profit (Loss)</td>
                    <td>{parseValue(voyageResultEstimateVT["profit_loss"])}</td>
                    <td>{parseValue(voyageResultActualVT["profit_loss"])}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualVT["profit_loss"],
                        voyageResultEstimateVT["profit_loss"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualVT["profit_loss"],
                        voyageResultEstimateVT["profit_loss"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Daily Profit (Loss)</td>
                    <td>
                      {parseValue(voyageResultEstimateVT["daily_profit_loss"])}
                    </td>
                    <td>
                      {parseValue(voyageResultActualVT["daily_profit_loss"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualVT["daily_profit_loss"],
                        voyageResultEstimateVT["daily_profit_loss"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualVT["daily_profit_loss"],
                        voyageResultEstimateVT["daily_profit_loss"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Co2 Cost </td>
                  
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                     <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> 
                       <td>{co2expense}</td>
                    <td>0.00</td>
                    <td>0.00</td> 
                     <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td> 
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    {/* <td>0.00</td> */}
                  </tr>


                  <tr>
                    <td className="pl-6">CO2 Adjusted Profit (Loss)</td>
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                    {/* <td>{CO2_adj_profit}</td> */}

                    {/* <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> */}

                    <td>{CO2_adj_profit}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>


                  <tr>
                    <td className="pl-6">CO2 Adjusted (Net) TCE </td>
                    
                    <td>
                      {parseValue(
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>

                    {/* <td>
                      {parseValue(
                        voyageResultActualTcov["breakeven_and_freight_rate"]
                      )}
                    </td> */}
                    <td>{co2_adjusted_tce}</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualTcov["breakeven_and_freight_rate"],
                        voyageResultEstimateTcov["breakeven_and_freight_rate"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Total Sea Days</td>
                    <td>
                      {parseValue(voyageResultEstimateVT["total_sea_days"])}
                    </td>
                    <td>
                      {parseValue(voyageResultActualVT["total_sea_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualVT["total_sea_days"],
                        voyageResultEstimateVT["total_sea_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualVT["total_sea_days"],
                        voyageResultEstimateVT["total_sea_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Total Port Days</td>
                    <td>
                      {parseValue(voyageResultEstimateVT["total_port_days"])}
                    </td>
                    <td>
                      {parseValue(voyageResultActualVT["total_port_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualVT["total_port_days"],
                        voyageResultEstimateVT["total_port_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualVT["total_port_days"],
                        voyageResultEstimateVT["total_port_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>

                  <tr>
                    <td className="pl-6">Net Voyage Days</td>
                    <td>
                      {parseValue(voyageResultEstimateVT["net_voyage_days"])}
                    </td>
                    <td>
                      {parseValue(voyageResultActualVT["net_voyage_days"])}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>
                      {calculateDiff(
                        voyageResultActualVT["net_voyage_days"],
                        voyageResultEstimateVT["net_voyage_days"]
                      )}
                    </td>
                    <td>
                      {calculatePercentDiff(
                        voyageResultActualVT["net_voyage_days"],
                        voyageResultEstimateVT["net_voyage_days"]
                      )}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                </>
              ) : undefined}
            </tbody>
          </table>

          {/*
                  
                  this part is said to be removed by pallav sir.

                  
                  <h4 className="font-weight-bold">Profit & Loss Details</h4>
                  <table className="table border table-striped table-invoice-report">
                      <thead>
                          <tr>
                              <th rowSpan="2"></th>
                              <th rowSpan="2">Estimate</th>
                              <th rowSpan="2">Actual</th>
                              <th rowSpan="2">Posted</th>
                              <th colSpan="2" className="text-center">Variance</th>
                          </tr>
                          <tr>
                              <th>Act-Est</th>
                              <th>%</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Revenue</td>
                          </tr>
                          <tr><td colSpan="7">Freight</td></tr>
                          <tr>
                              <td className="pl-4">Barites 15,000.000 MT * 12.15 USD</td>
                              <td></td>
                              <td>182,250</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">Barites 15,000.000 MT * 12.15 USD</td>
                              <td>182,250</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="text-left font-weight-bold">Freight Total</td>
                              <td>182,250</td>
                              <td>182,250</td>
                              <td></td>
                              <td>(1)</td>
                              <td>0</td>
                          </tr>

                          <tr><td colSpan="7">Freight Comm.</td></tr>
                          <tr>
                              <td className="pl-4">Cargo Barites, Commition: Freight 182,250.00 @ 0.63 % for IFCHOR</td>
                              <td></td>
                              <td>(1,139)</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">Cargo Barites, Commition: Freight 182,250.00 @ 0.63 % for IFCHOR</td>
                              <td>(1,139)</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td colSpan="7" className="font-weight-bold">Expenses</td>
                          </tr>
                          <tr><td colSpan="7">Hire</td></tr>

                          <tr>
                              <td className="pl-4">Hire 15.6125000 days @ 6,000.00 USD/day (01/29/19) 22:00 - 02/14/19 12:42)</td>
                              <td></td>
                              <td>93,675.00</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">TC Hire 12.418877 days * 6,000.00</td>
                              <td>74,513.26</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                      </tbody>

                  </table>

                  <h4 className="font-weight-bold">Bunker Details</h4>
                  <h5 className="font-weight-bold">Bunker Calculation Method: LIFO</h5>

                  <table className="table border table-striped table-invoice-report">
                      <thead>
                          <tr>
                              <th rowSpan="2"></th>
                              <th colSpan="3" className="text-center">380</th>
                              <th colSpan="3" className="text-center">MDO</th>
                          </tr>
                          <tr>
                              <th>Qty (MT)</th>
                              <th>Price (USD)</th>
                              <th>Value (USD))</th>

                              <th>Qty (MT)</th>
                              <th>Price (USD)</th>
                              <th>Value (USD))</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Open Inventory</td>
                          </tr>
                       
                          <tr>
                              <td></td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>

                          <tr>
                              <td>Total Open Inv</td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>

                          <tr>
                              <td>Total Inventory</td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Consumed</td>
                          </tr>

                          <tr>
                              <td></td>
                              <td>76.080</td>
                              <td>410.00</td>
                              <td>31,192.80</td>
                              <td>32.300</td>
                              <td>625.00</td>
                              <td>13,937.50</td>
                          </tr>

                          <tr>
                              <td>Total Consumed</td>
                              <td>76.080</td>
                              <td>410.00</td>
                              <td>31,192.80</td>
                              <td>32.300</td>
                              <td>625.00</td>
                              <td>13,937.50</td>
                          </tr>

                          <tr>
                              <td colSpan="7" className="font-weight-bold">Case Inventory</td>
                          </tr>

                          <tr>
                              <td></td>
                              <td>491.310</td>
                              <td>410.00</td>
                              <td>201,437.10</td>
                              <td>29.550</td>
                              <td>625.00</td>
                              <td>18,468.75</td>
                          </tr>

                          <tr>
                              <td>Total Close Inv</td>
                              <td>491.310</td>
                              <td>410.00</td>
                              <td>201,437.10</td>
                              <td>29.550</td>
                              <td>625.00</td>
                              <td>18,468.75</td>
                          </tr>

                      </tbody>

                  </table> */}
        </div>
      </div>
    </article>
  );
});

const TctoProfitLossReport = (props) => {
  const [state, setState] = useStateCallback({
    name: "Printer",
  });

  const [pdfData, setPdfData] = useState();
  const [userInput, setUserInput] = useState();
  const [emailModal, setEmailModal] = useState(false);
  const [loading, setLoading] = useState(false);

  const componentRef = useRef();

  const printReceipt = () => {
    window.print();
  };

  const sendEmailReportModal = async () => {
    try {
      setLoading(true);

      const quotes = document.getElementById("divToPrint");

      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });

      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      const doc = new jsPDF("p", "mm");
      let position = 25;
      const pageData = canvas.toDataURL("image/jpeg", 1.0);
      doc.addImage(pageData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          pageData,
          "PNG",
          5,
          position + 25,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }

      // Create Blob
      const blob = doc.output("blob");

      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      setLoading(false);
      setPdfData(blob);
      setEmailModal(true);
    } catch (error) {
      console.error("Error:", error);
      setLoading(false);
      // this.setState({ loading: false });
      // Handle errors here
    }
  };

  const printDocument = () => {
    var quotes = document.getElementById("divToPrint");

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true,
    }).then(function (canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF("p", "mm");
      var position = 30;
      var pageData = canvas.toDataURL("image/jpeg", 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          imgData,
          "PNG",
          5,
          position + 30,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save("Profit & Loss Report.pdf");
    });
  };

  return (
    <div className="body-wrapper modalWrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection" />
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li onClick={sendEmailReportModal}>Send Email</li>
                    <li>
                      <span className="text-bt" onClick={printDocument}>
                        {" "}
                        Download
                      </span>
                    </li>
                    <li>
                      <ReactToPrint
                        trigger={() => (
                          <span className="text-bt">
                            <PrinterOutlined /> Print
                          </span>
                        )}
                        content={() => componentRef.current}
                      />
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <ComponentToPrint ref={componentRef} data={props.data} />
          </div>
        </div>
      </article>
    </div>
  );
};

export default TctoProfitLossReport;
