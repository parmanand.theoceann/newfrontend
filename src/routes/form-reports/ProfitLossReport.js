import React, { Component } from 'react';
import {PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>
            <h5 className="font-weight-bold">Vessel: Daisy Glory - Voyage No. 1 (Commenced)</h5>
                  <table className="table border table-invoice-report">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Estimate Period :</td>
                        <td className="text-right">2/20/2020 6:07 AM (GST -3)</td>

                        <td className="font-weight-bold">To :</td>
                        <td className="text-right">4/22/2020 10:58 PM (GST +8)</td>
                        <td className="text-right" colSpan="2">62.20 Days</td>
                      </tr>

                      <tr>
                        <td>Load Port :</td>
                        <td>Value</td>

                        <td>Discharge port :</td>
                        <td>Value</td>

                        <td>Fixture No :</td>
                        <td>Value</td>
                      </tr>

                      <tr>
                        <td>Estimate ID :</td>
                        <td>Value</td>

                        <td>Cargo :</td>
                        <td>Value</td>

                        <td>CP Date :</td>
                        <td>Value</td>
                      </tr>

                      <tr>
                        <td>Charterer :</td>
                        <td>Value</td>

                        <td>Operation :</td>
                        <td>Value</td>

                        <td>Bunker Cal. Method :</td>
                        <td>Value</td>
                      </tr>

                      <tr>
                        <td>Last Update :</td>
                        <td>Value</td>

                        <td>Currency :</td>
                        <td>Value</td>

                        <td colSpan="2"></td>
                      </tr>      
                    </tbody>
                  </table>
                  <h4 className="font-weight-bold">Profit & Loss Summary</h4>
                  <table className="table border table-striped table-invoice-report">
                      <thead>
                          <tr>
                              <th rowSpan="2"></th>
                              <th rowSpan="2">Estimate</th>
                              <th colSpan="2" className="text-center">Actual</th>
                              <th colSpan="4" className="text-center">Variance</th>
                          </tr>
                          <tr>
                              <th>Actual</th>
                              <th>Posted</th>
                              <th>Act-Est</th>
                              <th>%</th>
                              <th>Act-Post</th>
                              <th>%</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td colSpan="8" className="font-weight-bold">Revenue</td>
                          </tr>
                          <tr>
                              <td className="pl-4">Freight</td>
                              <td>2,443,691</td>
                              <td>2,443,691</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>2,443,691</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4 font-weight-bold">Total Revenues</td>
                              <td>2,443,691</td>
                              <td>2,443,691</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>2,443,691</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td colSpan="8" className="font-weight-bold">Expenses</td>
                          </tr>

                          <tr>
                              <td className="pl-4">VC In Freight</td>
                              <td>2,284,103</td>
                              <td>2,310,701</td>
                              <td></td>
                              <td>26,598</td>
                              <td>1</td>
                              <td>2,310,701</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">VC In Freight Add. Comm.</td>
                              <td>(85,654)</td>
                              <td>(86,651)</td>
                              <td></td>
                              <td>(997)</td>
                              <td>(1)</td>
                              <td>(86,651)</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4 font-weight-bold">Total Expenses</td>
                              <td>2,198,449</td>
                              <td>2,224,050</td>
                              <td></td>
                              <td>25,601</td>
                              <td>1</td>
                              <td>2,224,050</td>
                              <td></td>
                          </tr>
                          <tr>
                              <td>Profit / Loss</td>
                              <td>245,442</td>
                              <td>219,641</td>
                              <td></td>
                              <td>(25,601)</td>
                              <td>(10)</td>
                              <td>219,641</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Net Daily (TCE)</td>
                              <td>3,972</td>
                              <td>2,413</td>
                              <td></td>
                              <td>(1,558)</td>
                              <td>(29)</td>
                              <td>2,413</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Net Voyage Days</td>
                              <td>61.74</td>
                              <td>91.01</td>
                              <td></td>
                              <td>29.27</td>
                              <td>47</td>
                              <td>91.01</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Daily Profit / Loss</td>
                              <td>3,972</td>
                              <td>2,413</td>
                              <td></td>
                              <td>(1,558)</td>
                              <td>(39)</td>
                              <td>2,413</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Total/Off hire days</td>
                              <td>91.74 |</td>
                              <td>91.01 |</td>
                              <td></td>
                              <td>29.27 | 0.00</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Port/Sea days</td>
                              <td>24.89 | 36.86</td>
                              <td>47.89 | 43.12</td>
                              <td></td>
                              <td>23.00 | 6.26</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>
                      </tbody>

                  </table>
                  <h4 className="font-weight-bold">Profit & Loss Details</h4>
                  <table className="table border table-striped table-invoice-report">
                      <thead>
                          <tr>
                              <th rowSpan="2"></th>
                              <th rowSpan="2">Estimate</th>
                              <th rowSpan="2">Actual</th>
                              <th rowSpan="2">Posted</th>
                              <th colSpan="2" className="text-center">Variance</th>
                          </tr>
                          <tr>
                              <th>Act-Est</th>
                              <th>%</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Revenue</td>
                          </tr>
                          <tr><td colSpan="7">Freight</td></tr>
                          <tr>
                              <td className="pl-4">Barites 15,000.000 MT * 12.15 USD</td>
                              <td></td>
                              <td>182,250</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">Barites 15,000.000 MT * 12.15 USD</td>
                              <td>182,250</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="text-right font-weight-bold">Freight Total</td>
                              <td>182,250</td>
                              <td>182,250</td>
                              <td></td>
                              <td>(0)</td>
                              <td>0</td>
                          </tr>

                          <tr><td colSpan="7">Freight Comm.</td></tr>
                          <tr>
                              <td className="pl-4">Cargo Barites, Commition: Freight 182,250.00 @ 0.63 % for IFCHOR</td>
                              <td></td>
                              <td>(1,139)</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">Cargo Barites, Commition: Freight 182,250.00 @ 0.63 % for IFCHOR</td>
                              <td>(1,139)</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td colSpan="7" className="font-weight-bold">Expenses</td>
                          </tr>
                          <tr><td colSpan="7">Hire</td></tr>

                          <tr>
                              <td className="pl-4">Hire 15.6125000 days @ 6,000.00 USD/day (01/29/19) 22:00 - 02/14/19 12:42)</td>
                              <td></td>
                              <td>93,675.00</td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                          <tr>
                              <td className="pl-4">TC Hire 12.418877 days * 6,000.00</td>
                              <td>74,513.26</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>

                      </tbody>

                  </table>

                  <h4 className="font-weight-bold">Bunker Details</h4>
                  <h5 className="font-weight-bold">Bunker Calculation Method: LIFO</h5>

                  <table className="table border table-striped table-invoice-report">
                      <thead>
                          <tr>
                              <th rowSpan="2"></th>
                              <th colSpan="3" className="text-center">380</th>
                              <th colSpan="3" className="text-center">MDO</th>
                          </tr>
                          <tr>
                              <th>Qty (MT)</th>
                              <th>Price (USD)</th>
                              <th>Value (USD))</th>

                              <th>Qty (MT)</th>
                              <th>Price (USD)</th>
                              <th>Value (USD))</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Open Inventory</td>
                          </tr>
                       
                          <tr>
                              <td></td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>

                          <tr>
                              <td>Total Open Inv</td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>

                          <tr>
                              <td>Total Inventory</td>
                              <td>567.390</td>
                              <td>410.00</td>
                              <td>232,629.90</td>
                              <td>51.850</td>
                              <td>625.00</td>
                              <td>32,406.25</td>
                          </tr>
                          <tr>
                              <td colSpan="7" className="font-weight-bold">Consumed</td>
                          </tr>

                          <tr>
                              <td></td>
                              <td>76.080</td>
                              <td>410.00</td>
                              <td>31,192.80</td>
                              <td>32.300</td>
                              <td>625.00</td>
                              <td>13,937.50</td>
                          </tr>

                          <tr>
                              <td>Total Consumed</td>
                              <td>76.080</td>
                              <td>410.00</td>
                              <td>31,192.80</td>
                              <td>32.300</td>
                              <td>625.00</td>
                              <td>13,937.50</td>
                          </tr>

                          <tr>
                              <td colSpan="7" className="font-weight-bold">Case Inventory</td>
                          </tr>

                          <tr>
                              <td></td>
                              <td>491.310</td>
                              <td>410.00</td>
                              <td>201,437.10</td>
                              <td>29.550</td>
                              <td>625.00</td>
                              <td>18,468.75</td>
                          </tr>

                          <tr>
                              <td>Total Close Inv</td>
                              <td>491.310</td>
                              <td>410.00</td>
                              <td>201,437.10</td>
                              <td>29.550</td>
                              <td>625.00</td>
                              <td>18,468.75</td>
                          </tr>

                      </tbody>

                  </table>
          </div>
        </div>
      </article>
    );
  }
}

class ProfitLossReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                             <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ProfitLossReport;
