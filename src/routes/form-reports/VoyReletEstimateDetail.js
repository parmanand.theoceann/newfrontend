import React, { Component } from 'react';
import {  Modal } from 'antd';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import moment from "moment";

import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import URL_WITH_VERSION, { awaitPostAPICall, openNotificationWithIcon } from '../../shared';
import Email from '../../components/Email/index';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {

    }

    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }
  }
  render() {
    
    const { formReportdata } = this.state
  

    return (

      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint" >
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">{formReportdata.logo}</span>
                  <p className="sub-title m-0">{formReportdata.full_name}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata.address}</p>
                </div>
              </div>
            </div>
            <table className="table table-invoice-report-colum">
              <tbody>
                <td className="border-0">
                  <h5 className="font-weight-bold">Vessel Details</h5>
                  <table className="table table-bordered table-invoice-report-colum voy-report">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Voy Relet ID :</td>
                        <td className="text-right">{formReportdata.estimate_id}</td>

                        <td className="font-weight-bold">Ballast Port :</td>
                        <td className="text-right">{formReportdata.ballast_port_name}</td>

                        <td className="font-weight-bold">Fixed By/Ops User :</td>
                        <td className="">{formReportdata.fixed_by ? formReportdata.fixed_by : "N/A"}   /  {formReportdata.ops_user ? formReportdata.ops_user : "N/A"}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Vessel Name/Code :</td>
                        <td className="text-right">{formReportdata && formReportdata.fix && formReportdata.fix.vessel_name}/{formReportdata && formReportdata.vessel_code}</td>

                        <td className="font-weight-bold">Reposition Port :</td>
                        <td className="text-right">{formReportdata.reposition_port_name}</td>

                        <td className="font-weight-bold">Voyage Ops Type :</td>
                        <td className="text-left">{formReportdata.ops_type_name}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">DWT :</td>

                        <td className="text-right">{formReportdata.dwt}</td>


                        <td className="font-weight-bold">Repos. Days :</td>

                        <td className="text-right">{formReportdata.repso_days}</td>


                        <td className="font-weight-bold">C/P Date :</td>

                        <td className="text-left">{formReportdata.cp_date}</td>

                      </tr>

                      <tr>
                        <td className="font-weight-bold">TCI Code :</td>
                        <td className="text-right">{formReportdata.tci_code}</td>

                        <td className="font-weight-bold">Commence Date :</td>
                        <td className="text-right">{formReportdata.commence_date}</td>

                        <td className="font-weight-bold">My Company/LOB :</td>
                        <td className="">{formReportdata.address_id} / {formReportdata.company_lob_name}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Routing Type :</td>
                        <td className="text-right">{formReportdata.routing_type_name}</td>

                        <td className="font-weight-bold">Daily Rate :</td>
                        <td className="text-right">{formReportdata.tci_d_hire}</td>

                        <td className="font-weight-bold">Completing Date :</td>
                        <td className="text-left">{formReportdata.completed_date}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Trade Area :</td>
                        <td className="text-right">{formReportdata.trade_area_name}</td>

                        <td className="font-weight-bold">Add Com./Bro % :</td>
                        <td className="text-right">{formReportdata.bro_percentage}</td>

                        <td className="font-weight-bold">Total voyage days :</td>
                        <td className="text-left">{formReportdata.total_days}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Mis Cost :</td>
                        <td className="text-right">{formReportdata.mis_cost}</td>

                        <td className="font-weight-bold"> WF (%) :</td>
                        <td className="text-right">{formReportdata.dwf}</td>

                        <td className="font-weight-bold">Ballast Bonus :</td>
                        <td className="text-left">{formReportdata.bb}</td>
                      </tr>
                    </tbody>
                  </table>
                  <table className="table voy-report">
                    <tbody>
                      <td>
                        <h5 className="font-weight-bold">Speed/Cons and price $</h5>
                        <table className="table border-table table-invoice-report-colum">
                          <thead>
                            <tr>
                              <th rowSpan="2">SPD Type</th>
                              <th colSpan="2" className="text-center">
                                Ballast Bonus
                              </th>
                              <th colSpan="2" className="text-center">
                                Laden
                              </th>
                            </tr>
                            <tr>
                              <th>Kt</th>
                              <th>Cons</th>
                              <th>Kt</th>
                              <th>Cons</th>
                            </tr>
                          </thead>
                          <tbody>
                            {formReportdata['-'] && formReportdata['-'] && formReportdata['-'].length > 0 ? formReportdata['-'].map((e, idx) => {
                              return (
                                <>

                                  <tr key={idx}>
                                    <td>{e.spd_type}</td>
                                    <td>{e.laden_spd}</td>
                                    <td>{e.laden_con}</td>
                                    <td>{e.laden_spd}</td>
                                    <td>{e.laden_con}</td>
                                  </tr>
                                </>
                              )
                            }) : undefined

                            }
                          </tbody>
                        </table>
                      </td>
                      <td>
                        <h5 className="font-weight-bold">Eco Speed</h5>
                        <table className="table border-table table-invoice-report-colum">
                          <thead>
                            <tr>
                              <th>P$</th>
                              <th>CP$</th>
                              <th>Fuel</th>
                              <th>Laden</th>
                              <th>Ballast</th>
                              <th>Load</th>
                              <th>Disch</th>
                              <th>Heat</th>
                              <th>Idle</th>
                            </tr>
                          </thead>
                          <tbody>
                            {formReportdata['.']['eco_data'] && formReportdata['.']['eco_data'] && formReportdata['.']['eco_data'].length > 0 ? formReportdata['.']['eco_data'].map((e, idx) => {
                              return (
                                <>
                                  <tr key={idx}>
                                    <td>{e.purchase_price}</td>
                                    <td>{e.cp_price}</td>
                                    <td>{e.fuel_code}</td>
                                    <td>{e.laden_value}</td>
                                    <td>{e.ballast_value}</td>
                                    <td>{e.con_loading}</td>
                                    <td>{e.con_disch}</td>
                                    <td>{e.con_heat}</td>
                                    <td>{e.con_ideal_on}</td>
                                  </tr>
                                </>
                              )
                            }) : undefined

                            }
                          </tbody>
                        </table>
                      </td>
                    </tbody>
                  </table>
                  <h5 className="font-weight-bold">Cargo Summary</h5>
                  <table className="table border-table table-invoice-report-colum voy-report">
                    <thead>
                      <tr>
                        <th>Cargo name</th>
                        <th>Charterer</th>
                        <th>CP qty</th>
                        <th>UNIT</th>
                        <th>Opt%</th>
                        <th>Opt Type</th>
                        <th>Freight Type</th>
                        <th>F rate</th>
                        <th>Lumsum</th>
                        <th>Commiss</th>
                        <th>Currency</th>
                      </tr>
                    </thead>
                    <tbody>
                      {formReportdata.cargos && formReportdata.cargos && formReportdata.cargos.length > 0 ? formReportdata.cargos.map((e, idx) => {
                        return (
                          <>

                            <tr key={idx}>
                              <td>{e.cargo_name1}</td>
                              <td>{e.charterer_name}</td>
                              <td>{e.quantity}</td>
                              <td>{e.unit_name}</td>
                              <td>{e.option_percentage}</td>
                              <td>{e.option_type_name}</td>

                              <td>{e.f_type_name}</td>
                              <td>{e.f_rate}</td>
                              <td>{e.lumpsum}</td>
                              <td>{e.commission}</td>
                              <td>{e.currency_name}</td>
                            </tr>
                          </>
                        )
                      }) : undefined

                      }

                    </tbody>
                  </table>
                   
                  {/* <h5 className="font-weight-bold">Itinerary summary</h5>
                  <table className="table border-table table-invoice-report-colum">
                    <thead>
                      <tr>
                        <th>Port</th>
                        <th>Funct.</th>
                        <th>Miles</th>
                        <th>Passage</th>
                        <th>Stype</th>
                        <th>WF%</th>
                        <th>SPD</th>
                        <th>GSD</th>
                        <th>TSD</th>
                        <th>L/D Qty</th>
                        <th>L/D Rate (D)</th>
                        <th>L/D Rate (H)</th>
                        <th>L/D Term</th>
                        <th>Turntime</th>
                        <th>P.Exp</th>
                        <th>Tatal P</th>
                      </tr>
                    </thead>
                    <tbody>
                    {formReportdata.portitinerary && formReportdata.portitinerary && formReportdata.portitinerary.length > 0 ? formReportdata.portitinerary.map((e, idx)=>{
                      return (
                        <>
                  
                    <tr key={idx}>
                    
                        <td>{e.port}</td>
                        <td>{e.funct_name}</td>
                        <td>{e.miles}</td>
                        <td>{e.passagename}</td>
                        <td>{e.s_type_name}</td>
                        <td>{e.wf_per}</td>
                        <td>{e.speed}</td>
                        <td>{e.gsd}</td>
                        <td>{e.tsd}</td>
                        <td>{e.l_d_qty}</td>
                        <td>{e.l_d_rate}</td>
                        <td>{e.l_d_rate1}</td>
                        <td>{e.l_d_termname}</td>
                        <td>{e.turn_time}</td>
                        <td>{e.p_exp}</td>
                        <td>{e.ttl}</td>
                      </tr>

                      </>
                    )
                    }): undefined

                  }
                    </tbody>
                  </table>
                   */}

                  <table className="table voy-table ">
                    <tbody>
                        <h5 className="font-weight-bold ">Itinerary summary</h5>
                        <table className="table border-table table-invoice-report-column">
                          <thead>
                            <tr>
                              <th>Port</th>
                              <th>Funct.</th>
                              <th>Distance</th>
                              <th>spdType</th>
                              <th>effSpd</th>
                              <th>TSD</th>
                              <th>LDterm</th>
                              <th>TPDays</th>

                            </tr>
                          </thead>
                          <tbody>
                            {formReportdata.portitinerary && formReportdata.portitinerary &&
                              formReportdata.portitinerary.length > 0 ? formReportdata.portitinerary.map((e, idx) => {
                                return (
                                  <>
                                    <tr key={idx}>
                                      <td>{e.port}</td>
                                      <td>{e.funct_name}</td>
                                      <td>{e.miles}</td>
                                      <td>{e.s_type_name}</td>
                                      <td>{e.eff_speed}</td>
                                      <td>{e.tsd}</td>
                                      <td>{e.l_d_termname}</td>
                                      <td>{e.days}</td>

                                    </tr>
                                  </>
                                )
                              }) : undefined

                             }
                          </tbody>
                        </table>
                         
                        <table className="table voy-report-cons">
                          <tbody>
                            <td>
                              <h5 className="font-weight-bold text-center">SeaCons</h5>
                              <table className="table border-table table-invoice-report-colum">
                                <thead>
                                  <tr>
                                    <th>IFO</th>
                                    <th>VLSFO</th>
                                    <th>LSMGO</th>
                                    <th>MGO</th>
                                    <th>ULSFO</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {formReportdata.bunkerdetails && formReportdata.bunkerdetails &&
                                    formReportdata.bunkerdetails.length > 0 ? formReportdata.bunkerdetails.map((e, idx) => {
                                      return (
                                        <>
                                          <tr key={idx}>
                                            <td>{e.ifo}</td>
                                            <td>{e.vlsfo}</td>
                                            <td>{e.lsmgo}</td>
                                            <td>{e.mgo}</td>
                                            <td>{e.ulsfo}</td>
                                          </tr>
                                        </>
                                      )
                                    }) : undefined

                                  }
                                </tbody>
                              </table>
                            </td>

                            <td>
                              <h5 className="font-weight-bold text-center voy-report-con">PortCons</h5>
                              <table className="table border-table table-invoice-report-colum">
                                <thead>
                                  <tr>
                                    <th>IFO</th>
                                    <th>VLSFO</th>
                                    <th>LSMGO</th>
                                    <th>MGO</th>
                                    <th>ULSFO</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {formReportdata.bunkerdetails && formReportdata.bunkerdetails &&
                                    formReportdata.bunkerdetails.length > 0 ? formReportdata.bunkerdetails.map((e, idx) => {
                                      return (
                                        <>
                                          <tr key={idx}>
                                            <td>{e.pc_ifo}</td>
                                            <td>{e.pc_vlsfo}</td>
                                            <td>{e.pc_lsmgo}</td>
                                            <td>{e.pc_mgo}</td>
                                            <td>{e.pc_ulsfo}</td>
                                          </tr>
                                        </>
                                      )
                                    }) : undefined

                                  }
                                </tbody>
                              </table>
                              </td>
                             

                             <td>
                             <h5 className="font-weight-bold text-center">Date-Time</h5> 
                        <table className="table border-table voy-report-cons">
                          <thead>
                            <tr>
                              <th>Arrival.</th>
                              <th>Departure.</th>
                            </tr>
                          </thead>
                          <tbody>
                            {formReportdata.portdatedetails && formReportdata.portdatedetails &&
                              formReportdata.portdatedetails.length > 0 ? formReportdata.portdatedetails.map((e, idx) => {
                                return (
                                  <>
                                    <tr key={idx}>
                                      <td>{moment(e.arrival_date_time).format('YYYY-MM-DD  HH:MM')}</td>
                                      <td>{moment(e.departure).format('YYYY-MM-DD  HH:MM')}</td>
                                    </tr>
                                  </>
                                )
                              }) : undefined

                            }

                          </tbody>
                          </table>
                             </td>


                              </tbody>
                             </table>
                    
                       
                        
                         
                    </tbody>
                  </table>
































                  <table className="table border-table table-invoice-report-colum voy-report">
                    <thead>
                      <tr>
                        <th>Total Distance</th>
                        <th>TTL Port Days</th>
                        <th>TSD</th>
                        <th>TTL Qty</th>
                        <th>GSD</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          {formReportdata['------'] && formReportdata['------'] ? formReportdata['------'].total_distance : ""} <span>Miles</span>
                        </td>
                        <td>
                          {formReportdata['------'] && formReportdata['------'] ? formReportdata['------'].totalt_port_days : ""} <span>Days</span>
                        </td>
                        <td>
                          {formReportdata['------'] && formReportdata['------'] ? formReportdata['------'].total_tsd : ""} <span >Days</span>
                        </td>
                        <td>
                          {formReportdata['------'] && formReportdata['------'] ? formReportdata['------'].total_load : ""} <span >Mt</span>
                        </td>
                        <td>
                          {formReportdata['------'] && formReportdata['------'] ? formReportdata['------'].total_gsd : ""} <span >Days</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                   
                  <h5 className="font-weight-bold">P & L Summary</h5>
                  <div className='estimate-first'>
                
                <table className="table border-table table-invoice-report-colum voy-report-L">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">Freight :</td>
                    {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (

                      <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue.freight}</td>
                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Demurrage :</td>
                    {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (

                      <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue.demurrage}</td>
                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Gross Income :</td>
                    {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (

                      <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue.grossrevenue}</td>
                    ) : undefined
                    }
                  </tr>
                  <tr>
                    <td className="font-weight-bold">Less Commissions :</td>

                    {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (
                      <td className="text-right">{formReportdata.p_l_summary.revenue.b_commission}</td>
                    ) : undefined

                    }
                  </tr>

                    <tr>
                      <td className="font-weight-bold">Less Tax :</td>
                      {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue ? (
                         // Here  condition is wrong. needs to discuss
                         <td className="text-right">{formReportdata.cargos ? formReportdata.cargos.frat_tax : ''}</td>
                       
                      ) : undefined

                      }
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Less Despatch :</td>
                      {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (
                        <td className="text-right">{formReportdata.p_l_summary.revenue.dem_disch}</td>
                      ) : undefined

                      }
                    </tr>

              <tr>
                <td className="font-weight-bold">Misc Income :</td>
                {/* {formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? ( */}
                <td className="text-right">{formReportdata.mis_cost}</td>
                {/* ):undefined

              } */}
              </tr>
              <tr>
                <td className="font-weight-bold">Net Income :</td>
                {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (

                  <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue.netrevenue}</td>
                ) : undefined
                }
              </tr> 
            
              </tbody>
                </table>

      
          <div className='estimate'>
        <table className="table border-table table-invoice-report-colum mt-2 pltable">
          <tbody>
            <tr>
              <td className="font-weight-bold">Freight Expenses :</td>
              {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense && formReportdata.p_l_summary.expense ? (

                <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.freightExpense}</td>
              ) : undefined
              }
            </tr>
           
            <tr>
              <td className="font-weight-bold">Less Address :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.lessexpense}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Canel Expenses :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.cancelexpense}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Port Expenses :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.portexpense}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Bunker Expenses :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.bunkerexpense}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Misc Expenses :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.misexpense}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Less Rebills :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.lessrebil}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Total Expenses :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.expense.netexpense}</td>
            </tr>
          </tbody>
        </table>
        <table className="table border-table table-invoice-report-colum mt-2 pltable">
          <tbody>
            <tr>
              <td className="font-weight-bold">Profit :</td>

              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage.profit_loss}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Daily Profit :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage.daily_profit_loss}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Breakeven :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage.breakeven_freight_rate}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Freight Rate(USD/Mt) :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage.freight_rate}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">TC Equiv(USD/Day) :</td>
              <td className="text-right">{formReportdata.tco_equi}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Gross TCE :</td>
              <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.revenue.gross_revenue}</td>
            </tr>
          </tbody>
        </table>

          <table className="table border-table table-invoice-report-colum mt-2 pltable">
          <tbody>
            <tr>
              <td className="font-weight-bold">Commenced On :</td>
              <td className="text-right">{formReportdata.commence_date}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Complete On :</td>
              <td className="text-right">{formReportdata.completed_date}</td>
            </tr>

            <tr>
              <td className="font-weight-bold">Voyage Days :</td>
              {formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage && formReportdata.p_l_summary.totalvoyage ? (
                <td className="text-right">{formReportdata && formReportdata.p_l_summary && formReportdata.p_l_summary.totalvoyage.total_days}</td>
              ) : undefined
              }
            </tr>
           </tbody>
          </table>
          </div>
          </div>
                </td>                     
             
              </tbody>
            </table>
          </div>
        </div>
      </article>

    );
  }
}

class VoyReletEstimateDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEmailModal: false,
      name: 'Printer',
    };
  }
  showEmailDialog = () => this.setState({ ...this.state, showEmailModal: true })

  printReceipt = () => window.print()

  printDocument() {
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save("download.pdf");
      });
  }

  emailDocument = (email, subject, message) => {
    const self = this;
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(async function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        const params = {
          subject,
          message,
          recepients: [email],
          attachments: [{
            "ContentType": "application/pdf",
            "Filename": "sample.pdf",
            "Base64Content": "JVBERi0xLjMNCiXi48/TDQoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL091dGxpbmVzIDIgMCBSDQovUGFnZXMgMyAwIFINCj4+DQplbmRvYmoNCg0KMiAwIG9iag0KPDwNCi9UeXBlIC9PdXRsaW5lcw0KL0NvdW50IDANCj4+DQplbmRvYmoNCg0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0NvdW50IDINCi9LaWRzIFsgNCAwIFIgNiAwIFIgXSANCj4+DQplbmRvYmoNCg0KNCAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDw8DQovRm9udCA8PA0KL0YxIDkgMCBSIA0KPj4NCi9Qcm9jU2V0IDggMCBSDQo+Pg0KL01lZGlhQm94IFswIDAgNjEyLjAwMDAgNzkyLjAwMDBdDQovQ29udGVudHMgNSAwIFINCj4+DQplbmRvYmoNCg0KNSAwIG9iag0KPDwgL0xlbmd0aCAxMDc0ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBBIFNpbXBsZSBQREYgRmlsZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIFRoaXMgaXMgYSBzbWFsbCBkZW1vbnN0cmF0aW9uIC5wZGYgZmlsZSAtICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjY0LjcwNDAgVGQNCigganVzdCBmb3IgdXNlIGluIHRoZSBWaXJ0dWFsIE1lY2hhbmljcyB0dXRvcmlhbHMuIE1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NTIuNzUyMCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDYyOC44NDgwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjE2Ljg5NjAgVGQNCiggdGV4dC4gQW5kIG1vcmUgdGV4dC4gQm9yaW5nLCB6enp6ei4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjA0Ljk0NDAgVGQNCiggbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDU5Mi45OTIwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNTY5LjA4ODAgVGQNCiggQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA1NTcuMTM2MCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBFdmVuIG1vcmUuIENvbnRpbnVlZCBvbiBwYWdlIDIgLi4uKSBUag0KRVQNCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCjYgMCBvYmoNCjw8DQovVHlwZSAvUGFnZQ0KL1BhcmVudCAzIDAgUg0KL1Jlc291cmNlcyA8PA0KL0ZvbnQgPDwNCi9GMSA5IDAgUiANCj4+DQovUHJvY1NldCA4IDAgUg0KPj4NCi9NZWRpYUJveCBbMCAwIDYxMi4wMDAwIDc5Mi4wMDAwXQ0KL0NvbnRlbnRzIDcgMCBSDQo+Pg0KZW5kb2JqDQoNCjcgMCBvYmoNCjw8IC9MZW5ndGggNjc2ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBTaW1wbGUgUERGIEZpbGUgMiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIC4uLmNvbnRpbnVlZCBmcm9tIHBhZ2UgMS4gWWV0IG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NzYuNjU2MCBUZA0KKCBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY2NC43MDQwIFRkDQooIHRleHQuIE9oLCBob3cgYm9yaW5nIHR5cGluZyB0aGlzIHN0dWZmLiBCdXQgbm90IGFzIGJvcmluZyBhcyB3YXRjaGluZyApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY1Mi43NTIwIFRkDQooIHBhaW50IGRyeS4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NDAuODAwMCBUZA0KKCBCb3JpbmcuICBNb3JlLCBhIGxpdHRsZSBtb3JlIHRleHQuIFRoZSBlbmQsIGFuZCBqdXN0IGFzIHdlbGwuICkgVGoNCkVUDQplbmRzdHJlYW0NCmVuZG9iag0KDQo4IDAgb2JqDQpbL1BERiAvVGV4dF0NCmVuZG9iag0KDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL05hbWUgL0YxDQovQmFzZUZvbnQgL0hlbHZldGljYQ0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCg0KMTAgMCBvYmoNCjw8DQovQ3JlYXRvciAoUmF2ZSBcKGh0dHA6Ly93d3cubmV2cm9uYS5jb20vcmF2ZVwpKQ0KL1Byb2R1Y2VyIChOZXZyb25hIERlc2lnbnMpDQovQ3JlYXRpb25EYXRlIChEOjIwMDYwMzAxMDcyODI2KQ0KPj4NCmVuZG9iag0KDQp4cmVmDQowIDExDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTkgMDAwMDAgbg0KMDAwMDAwMDA5MyAwMDAwMCBuDQowMDAwMDAwMTQ3IDAwMDAwIG4NCjAwMDAwMDAyMjIgMDAwMDAgbg0KMDAwMDAwMDM5MCAwMDAwMCBuDQowMDAwMDAxNTIyIDAwMDAwIG4NCjAwMDAwMDE2OTAgMDAwMDAgbg0KMDAwMDAwMjQyMyAwMDAwMCBuDQowMDAwMDAyNDU2IDAwMDAwIG4NCjAwMDAwMDI1NzQgMDAwMDAgbg0KDQp0cmFpbGVyDQo8PA0KL1NpemUgMTENCi9Sb290IDEgMCBSDQovSW5mbyAxMCAwIFINCj4+DQoNCnN0YXJ0eHJlZg0KMjcxNA0KJSVFT0YNCg=="
          }]
        }
        await awaitPostAPICall(`${URL_WITH_VERSION}/email/send-with-attachment`, params);
        self.setState({ ...self.state, showEmailModal: false })
        openNotificationWithIcon('success', 'Successfully sent report to provide email.');

        // openNotificationWithIcon('error', 'Failed to send email. Please try again.');
        // this.setState({ ...this.state, showEmailModal: false })
      });
  }

  render() {
    const { showEmailModal } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={this.printDocument}>
                        Download
                      </li>
                      <li onClick={this.showEmailDialog}>
                        Send Mail
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                   <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data} />
            </div>
          </div>
        </article>

        <Modal
          title="Email"
         open={showEmailModal}
          onOk={() => { this.setState({ ...this.state, showEmailModal: false }) }}
          onCancel={() => { this.setState({ ...this.state, showEmailModal: false }) }}
          footer={null}
        >
          <Email emailDocument={this.emailDocument} />
        </Modal>
      </div>
    );
  }
}


export default VoyReletEstimateDetail;
