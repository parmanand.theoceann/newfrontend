import React, {
  Component,
  forwardRef,
  useEffect,
  useRef,
  useState,
} from "react";
import { Modal, Spin } from "antd";
import { PrinterOutlined } from "@ant-design/icons";
import jsPDF from "jspdf";
import moment from "moment";
import * as htmlToImage from "html-to-image";
import html2canvas from "html2canvas";
import ReactToPrint from "react-to-print";
//import VoyReletPlSummary from '../pl-summary-list/VoyReletPlSummary';
import URL_WITH_VERSION, {
  awaitPostAPICall,
  openNotificationWithIcon,
} from "../../shared";
import Email from "../../components/Email/index";

const ComponentToPrint = forwardRef((props, ref) => {
  const [formReportdata, setFormReportdata] = useState(
    Object.assign({}, props.data || {})
  );

  // const { port_consumption, sea_consumption } =
  //   formReportdata &&
  //   formReportdata?.estimate &&
  //   formReportdata?.estimate?.expenses?.bunker_expenses;
  // let total_port_cons = 0;
  // let total_sea_cons = 0;
  // let total_bunker_expenses = 0;

  // for (let key in port_consumption) {
  //   total_port_cons += parseFloat(
  //     port_consumption[key].replaceAll(",", "") * 1
  //   );
  // }
  // for (let key in sea_consumption) {
  //   total_sea_cons += parseFloat(
  //     sea_consumption[key].replaceAll(",", "") * 1
  //   );
  // }
  // total_bunker_expenses = total_port_cons + total_sea_cons;

  // let { ballast_bonus, hire_expenses } =
  //   formReportdata &&
  //   formReportdata.estimate &&
  //   formReportdata.estimate.expenses &&
  //   formReportdata.estimate.expenses.vessel_expenses;

  // let vessel_expenses = hire_expenses.replaceAll(",", "") * 1;

  // let {
  //   profit_loss,
  //   daily_profit_loss,
  //   net_voyage_days,
  //   total_sea_days,
  //   total_port_days,
  // } =
  //   formReportdata &&
  //   formReportdata.estimate &&
  //   formReportdata.estimate.voyage_result &&
  //   formReportdata.estimate.voyage_result;

  // let {
  //   gross_expenses,
  //   mis_expenses,
  //   netExpenses,
  //   port_expenses,
  //   tci_add_comm,
  //   tci_broker_comm,
  //   total_expenses,
  // } =
  //   formReportdata &&
  //   formReportdata.estimate &&
  //   formReportdata.estimate.expenses;

  // let {
  //   gross_rev,
  //   misc_rev,
  //   net_rev,
  //   tco_add_comm,
  //   tco_b_comm,
  //   tco_bb,
  //   tco_hire,
  // } =
  //   formReportdata &&
  //   formReportdata.estimate &&
  //   formReportdata.estimate.revenue;

  const { expenses } = formReportdata?.estimate || {};
  const { vessel_expenses } = formReportdata?.estimate?.expenses || {};
  const { revenue } = formReportdata?.estimate || {};
  const {
    total_noneca_sea_cons,
    total_eca_sea_cons,
    total_noneca_port_cons,
    total_eca_port_cons,
  } = formReportdata?.estimate?.expenses?.bunker_expenses || {};
  return (
    <article className="article toolbaruiWrapper">
      <div className="box box-default" id="divToPrint">
        <div className="box-body">
          <div className="invoice-inner-download mt-3">
            <div className="row">
              <div className="col-12 text-center">
                <img
                  className="title reportlogo"
                  src={formReportdata.logo}
                  alt="no img"
                />
                <p className="sub-title m-0">{formReportdata.full_name}</p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 mx-auto">
              <div className="text-center invoice-top-address">
                <p>{formReportdata.address}</p>
              </div>
            </div>
          </div>

          <table className="table custom-table-bordered tc-table">
            <br />
            <h4 className="font-weight-bold tc-sub-header ml-4">
              P & L Summary
            </h4>
            <tbody>
              <td className="border-0">
                <table className="table voy-table ">
                  <tbody>
                    <table className="table voy-report-cons">
                      <tbody>
                        <td>
                          <h5 className="font-weight-bold text-center">
                            Revenue
                          </h5>
                          <table className="table border-table">
                            <tbody>
                              <tr>
                                <td className="font-weight-bold">
                                  Gross TCO Hire $:
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_hire ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross TCO Hire B. Comm:
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_hire_br_comm ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross TCO Hire add Com :
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_hire_add_comm ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross TCO BB:
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_bb ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross TCO BB Comm :
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_bb_comm ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Net Ballast Bonus :
                                </td>
                                <td className="text-right">
                                  {revenue?.net_b_bonus ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Misc. Revenue :
                                </td>
                                <td className="text-right">
                                  {revenue?.misc_rev ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Total TCO Net Hire :
                                </td>
                                <td className="text-right">
                                  {revenue?.tco_net_hire ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Total Net TCO BB :
                                </td>
                                <td className="text-right">
                                  {revenue?.net_tco_bb ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross Revenue :
                                </td>
                                <td className="text-right">
                                  {revenue?.gross_rev ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Net Revenue :
                                </td>
                                <td className="text-right">
                                  {revenue?.net_rev ?? "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>

                        <td>
                          <h5 className="font-weight-bold text-center voy-report-cons">
                            Expense
                          </h5>
                          <table className="table border-table ">
                            <tbody>
                              <tr>
                                <td className="font-weight-bold">
                                  Vessel Expenses:
                                </td>
                                <td className="text-right">
                                  {vessel_expenses?.tci_hire_expenses ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  TCI Add. Comm. :
                                </td>
                                <td className="text-right">
                                  {expenses?.tci_add_comm ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  TCI Bro. Comm. :
                                </td>
                                <td className="text-right">
                                  {expenses?.tci_broker_comm ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Port Expenses:
                                </td>
                                <td className="text-right">
                                  {expenses?.port_expenses ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Other Expenses:
                                </td>
                                <td className="text-right">
                                  {expenses?.mis_expenses ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Bunker Expenses:
                                </td>
                                <td className="text-right">
                                  {vessel_expenses?.sum_bunker_expenses ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Sea Consp Non Eca:
                                </td>
                                <td className="text-right">
                                  {vessel_expenses?.sum_sea_cons_non_eca ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Sea Consp Eca:
                                </td>
                                <td className="text-right">
                                  {vessel_expenses?.sum_sea_eca_cons ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Port Consp:
                                </td>
                                <td className="text-right">
                                  {vessel_expenses?.sum_port_cons ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Gross Expenses :
                                </td>
                                <td className="text-right">
                                  {expenses?.gross_expenses ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Net Expenses :
                                </td>
                                <td className="text-right">
                                  {expenses?.netExpenses ?? "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>

                        <td>
                          <h5 className="font-weight-bold text-center">
                            Voyage Result
                          </h5>
                          <table className="table border-table voy-report-cons">
                            <tbody>
                              <tr>
                                <td className="font-weight-bold">
                                  Profit (Loss):
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.profit_loss ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Daily Profit (Loss):
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.daily_profit_loss ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">CO2 Cost:</td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.co2_cost ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  CO2 Adjusted Profit (Loss):
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.CO2_adj_profit ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  CO2 Adjusted (Net) TCE:
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.co2_adjusted_tce ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Total Sea Days:
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.total_sea_days ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Total Port Days:
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.total_port_days ?? "N/A"}
                                </td>
                              </tr>

                              <tr>
                                <td className="font-weight-bold">
                                  Net Voyage Days:
                                </td>
                                <td className="text-right">
                                  {formReportdata?.estimate?.voyage_result
                                    ?.net_voyage_days ?? "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tbody>
                    </table>
                  </tbody>
                </table>

                <h5 className="font-weight-bold tc-sub-header">
                  Vessel Details
                </h5>
                <table className="table table-bordered ">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Vessel Name/Code:</td>
                      <td className="text-right">
                        {formReportdata?.vessel_name ?? "N/A"}
                        {/* /{" "}
                        {formReportdata?.vessel_code ?? "N/A"} */}
                      </td>
                      <td className="font-weight-bold">My Company LOB:</td>
                      <td className="text-right">
                        {formReportdata?.my_company_name ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">TC Est. ID:</td>
                      <td className="text-right">
                        {formReportdata?.estimate_id ?? "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">DWT:</td>
                      <td className="text-right">
                        {formReportdata?.dwt ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">CP Date:</td>
                      <td className="text-right">
                        {formReportdata?.cp_date ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Fixed By (Ops user):</td>
                      <td className="text-right">
                        {formReportdata?.fixed_by_user ?? "N/A"} /{" "}
                        {formReportdata?.ops_user ?? "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">
                        Head Fixture/TCI Code:
                      </td>
                      <td className="text-right">
                        {formReportdata?.tci_code ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">HF/TCI Hire Rate:</td>
                      <td className="text-right">
                        {formReportdata?.tci_d_hire ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Commence date:</td>
                      <td className="text-right">
                        {formReportdata?.commence_date ?? "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">TCI add com/bro.com:</td>
                      <td className="text-right">
                        {formReportdata?.add_percentage_tci ?? "N/A"} /{" "}
                        {formReportdata?.bro_percentage_tci ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Wf(%):</td>
                      <td className="text-right">
                        {formReportdata?.dwf ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Completing Date:</td>
                      <td className="text-right">
                        {formReportdata?.completing_date ?? "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Other Cost:</td>
                      <td className="text-right">
                        {formReportdata?.mis_cost ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Ballast Bonus:</td>
                      <td className="text-right">
                        {formReportdata?.bb ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Total Voyage Days:</td>
                      <td className="text-right">
                        {formReportdata?.tot_voy_days ?? "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">TCO Code:</td>
                      <td className="text-right">
                        {formReportdata?.tco_code ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">ECA Cons Fuel Grade:</td>
                      <td className="text-right">
                        {formReportdata?.eca_fuel_grade_name ?? "N/A"}
                      </td>
                      <td className="font-weight-bold">Trade Area:</td>
                      <td className="text-right">
                        {formReportdata?.trade_area_name ?? "N/A"}
                      </td>
                    </tr>
                  </tbody>
                </table>

                <table className="table">
                  <tbody>
                    <td>
                      <h5 className="font-weight-bold">Eco/Cp speed</h5>
                      <table className="table border-table custom-table-bordered">
                        <thead>
                          <tr>
                            <th rowSpan="2">SPD Type</th>
                            <th colSpan="1" className="text-center">
                              Ballast Bonus
                            </th>
                            <th colSpan="1" className="text-center">
                              Laden
                            </th>
                          </tr>
                          <tr>
                            <th>Kt</th>

                            <th>Kt</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata["-"]?.length > 0
                            ? formReportdata["-"].map((e, idx) => (
                                <tr key={idx}>
                                  <td>{e.spd_type ?? "N/A"}</td>
                                  <td>{e.ballast_spd ?? "N/A"}</td>
                                  <td>{e.laden_spd ?? "N/A"}</td>
                                </tr>
                              ))
                            : null}
                        </tbody>
                      </table>
                    </td>
                    <td>
                      <h5 className="font-weight-bold">
                        Speed/Cons and price $
                      </h5>
                      <table className="table border-table custom-table-bordered">
                        <thead>
                          <tr>
                            <th>CP$</th>
                            <th>Fuel</th>
                            <th>Laden</th>
                            <th>Ballast</th>
                            <th>Load</th>
                            <th>Disch</th>
                            <th>Heat</th>
                            <th>Idle</th>
                          </tr>
                        </thead>
                        <tbody>
                          {formReportdata["."]?.length > 0
                            ? formReportdata["."].map((e, idx) => (
                                <tr key={idx}>
                                  <td>{e.cp_price ?? "N/A"}</td>
                                  <td>{e.fuel_code ?? "N/A"}</td>
                                  <td>{e.laden_value ?? "N/A"}</td>
                                  <td>{e.ballast_value ?? "N/A"}</td>
                                  <td>{e.con_loading ?? "N/A"}</td>
                                  <td>{e.con_disch ?? "N/A"}</td>
                                  <td>{e.con_heat ?? "N/A"}</td>
                                  <td>{e.con_ideal_on ?? "N/A"}</td>
                                </tr>
                              ))
                            : null}
                        </tbody>
                      </table>
                    </td>
                  </tbody>
                </table>
                <h4 className="font-weight-bold tc-sub-header">Tco Term</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th rowSpan="2">Charterer</th>
                      <th rowSpan="2">Broker</th>
                      <th rowSpan="2">Duration</th>
                      <th rowSpan="2">Daily Hire</th>
                      <th rowSpan="2">Rate Type </th>
                      <th rowSpan="2">Ballast Bonus</th>
                      <th rowSpan="2">A.Com %</th>
                      <th rowSpan="2">B.Com%</th>
                      <th rowSpan="2">BB COMM</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.tcoterm?.length > 0
                      ? formReportdata.tcoterm.map((e, idx) => (
                          <tr key={idx}>
                            <td>{e.charterer ?? "N/A"}</td>
                            <td>{e.broker ?? "N/A"}</td>
                            <td>{e.duration ?? "N/A"}</td>
                            <td>{e.dailyhire ?? "N/A"}</td>
                            <td>{e.ratetype ?? "N/A"}</td>
                            <td>{e.bb ?? "N/A"}</td>
                            <td>{e.acom ?? "N/A"}</td>
                            <td>{e.bcom ?? "N/A"}</td>
                            <td>{e.bbcomm ?? "N/A"}</td>
                          </tr>
                        ))
                      : null}
                  </tbody>
                </table>

                <h4 className="font-weight-bold tc-sub-header"> Term</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th rowSpan="2">Delivery Port</th>
                      <th rowSpan="2">Delivery Time </th>
                      <th rowSpan="2">Redelivery Port </th>
                      <th rowSpan="2">Redelivery Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.term && formReportdata.term.length > 0 ? (
                      formReportdata.term.map((e, idx) => (
                        <tr key={idx}>
                          <td>{e.port_del ?? "N/A"}</td>
                          <td>{e.del_time ?? "N/A"}</td>
                          <td>{e.reddel_port ?? "N/A"}</td>
                          <td>{e.reddel_time ?? "N/A"}</td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colSpan="4">N/A</td>
                      </tr>
                    )}
                  </tbody>
                </table>
                <table className="table voy-table ">
                  <tbody>
                    <h5 className="font-weight-bold tc-sub-header">
                      Port Itinerary
                    </h5>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th>Port</th>
                          <th>Funct.</th>
                          <th>Passage</th>
                          <th>STYPE</th>
                          <th>Miles</th>
                          <th>Eca/Seca Dist</th>
                          <th>WF %</th>
                          <th>Eca/Seca Days</th>
                          <th>SPD</th>
                          <th>Eff-SPD</th>
                          <th>GSD</th>
                          <th>TSD</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.portitinerary.length > 0 &&
                          formReportdata.portitinerary.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.port ?? "N/A"}</td>
                              <td>{e.funct ?? "N/A"}</td>
                              <td>{e.passage_name ?? "N/A"}</td>
                              <td>{e.s_type ?? "N/A"}</td>
                              <td>{e.miles ?? "N/A"}</td>
                              <td>{e.seca_length ?? "N/A"}</td>
                              <td>{e.wf_per ?? "N/A"}</td>
                              <td>{e.eca_days ?? "N/A"}</td>
                              <td>{e.speed ?? "N/A"}</td>
                              <td>{e.eff_speed ?? "N/A"}</td>
                              <td>{e.gsd ?? "N/A"}</td>
                              <td>{e.tsd ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>

                      <thead>
                        <tr>
                          <th>XSD</th>
                          <th>L/D QTY</th>
                          <th>L/D Rate(D)</th>

                          <th>L/D Rate(H)</th>
                          <th>L/D Term</th>
                          <th> TurnTime(H)</th>
                          <th>P Days</th>
                          <th>Xpd</th>
                          <th>P.EXP</th>
                          <th>Total Port Days</th>
                          <th colSpan={2}>Euro. Port</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.portitinerary?.length > 0 &&
                          formReportdata.portitinerary.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.xsd ?? "N/A"}</td>
                              <td>{e.l_d_qty ?? "N/A"}</td>
                              <td>{e.l_d_rate ?? "N/A"}</td>
                              <td>{e.l_d_rate1 ?? "N/A"}</td>
                              <td>{e.l_d_term ?? "N/A"}</td>
                              <td>{e.turn_time ?? "N/A"}</td>
                              <td>{e.days ?? "N/A"}</td>
                              <td>{e.xpd ?? "N/A"}</td>
                              <td>{e.p_exp ?? "N/A"}</td>
                              <td>{e.t_port_days ?? "N/A"}</td>
                              <td colSpan={2}>{e.is_eur ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    {/* 
                      <table className="table voy-report-cons">
                        <tbody>
                          <td>
                            <h5 className="font-weight-bold text-center">
                              SeaCons
                            </h5>
                            <table className="table border-table custom-table-bordered">
                              <thead>
                                <tr>
                                  <th>IFO</th>
                                  <th>VLSFO</th>
                                  <th>LSMGO</th>
                                  <th>MGO</th>
                                  <th>ULSFO</th>
                                </tr>
                              </thead>
                              <tbody>
                                {formReportdata.bunkerdetails &&
                                formReportdata.bunkerdetails &&
                                formReportdata.bunkerdetails.length > 0
                                  ? formReportdata.bunkerdetails.map(
                                      (e, idx) => {
                                        return (
                                          <>
                                            <tr key={idx}>
                                              <td>{e.ifo}</td>
                                              <td>{e.vlsfo}</td>
                                              <td>{e.lsmgo}</td>
                                              <td>{e.mgo}</td>
                                              <td>{e.ulsfo}</td>
                                            </tr>
                                          </>
                                        );
                                      }
                                    )
                                  : undefined}
                              </tbody>
                            </table>
                          </td>

                          <td>
                            <h5 className="font-weight-bold text-center voy-report-cons">
                              PortCons
                            </h5>
                            <table className="table border-table custom-table-bordered">
                              <thead>
                                <tr>
                                  <th>IFO</th>
                                  <th>VLSFO</th>
                                  <th>LSMGO</th>
                                  <th>MGO</th>
                                  <th>ULSFO</th>
                                </tr>
                              </thead>
                              <tbody>
                                {formReportdata.bunkerdetails &&
                                formReportdata.bunkerdetails &&
                                formReportdata.bunkerdetails.length > 0
                                  ? formReportdata.bunkerdetails.map(
                                      (e, idx) => {
                                        return (
                                          <>
                                            <tr key={idx}>
                                              <td>{e.pc_ifo}</td>
                                              <td>{e.pc_vlsfo}</td>
                                              <td>{e.pc_lsmgo}</td>
                                              <td>{e.pc_mgo}</td>
                                              <td>{e.pc_ulsfo}</td>
                                            </tr>
                                          </>
                                        );
                                      }
                                    )
                                  : undefined}
                              </tbody>
                            </table>
                          </td>

                          <td>
                            <h5 className="font-weight-bold text-center">
                              Date-Time
                            </h5>
                            <table className="table border-table voy-report-cons">
                              <thead>
                                <tr>
                                  <th>Arrival.</th>
                                  <th>Departure.</th>
                                </tr>
                              </thead>
                              <tbody>
                                {formReportdata.portdatedetails &&
                                formReportdata.portdatedetails &&
                                formReportdata.portdatedetails.length > 0
                                  ? formReportdata.portdatedetails.map(
                                      (e, idx) => {
                                        return (
                                          <>
                                            <tr key={idx}>
                                              <td>
                                                {e.arrival_date_time &&
                                                e.arrival_date_time !=
                                                  "0000-00-00 00:00:00"
                                                  ? moment(
                                                      e.arrival_date_time
                                                    ).format(
                                                      "YYYY-MM-DD  HH:MM"
                                                    )
                                                  : "N/A"}
                                              </td>
                                              <td>
                                                {e.departure &&
                                                e.departure !=
                                                  "0000-00-00 00:00:00"
                                                  ? moment(e.departure).format(
                                                      "YYYY-MM-DD  HH:MM"
                                                    )
                                                  : "N/A"}
                                              </td>
                                            </tr>
                                          </>
                                        );
                                      }
                                    )
                                  : undefined}
                              </tbody>
                            </table>
                          </td>
                        </tbody>
                      </table> */}

                    <h4 className="font-weight-bold tc-sub-header">
                      Bunker Details
                    </h4>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th rowSpan="2">Port</th>
                          <th rowSpan="2">Function</th>
                          <th rowSpan="2">Miles</th>
                          <th rowSpan="2">Eca/Seca Dist</th>
                          <th rowSpan="2">Eca/Seca Days</th>
                          <th rowSpan="2">Passage</th>
                          <th rowSpan="2">SPD Type</th>
                          <th rowSpan="2">SPD</th>
                          <th rowSpan="2">Arrival Date-Time</th>
                          <th rowSpan="2">Departure</th>
                          <th rowSpan="2">Eca/Seca Cons (In MT)</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.bunkerdetails?.length > 0 &&
                          formReportdata.bunkerdetails.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.port ?? "N/A"}</td>
                              <td>{e.funct ?? "N/A"}</td>
                              <td>{e.miles ?? "N/A"}</td>
                              <td>{e.seca_length ?? "N/A"}</td>
                              <td>{e.eca_days ?? "N/A"}</td>
                              <td>{e.passage ?? "N/A"}</td>
                              <td>{e.spd_type ?? "N/A"}</td>
                              <td>{e.speed ?? "N/A"}</td>
                              <td>{e.arrival_date_time ?? "N/A"}</td>
                              <td>{e.departure ?? "N/A"}</td>
                              <td>{e.ec_ulsfo ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th colSpan="5">Fuel Grade (Sea Cons. In MT)</th>
                          <th colSpan="5">Arrival ROB</th>
                          <th colSpan="5">Port Cons.Fuel</th>
                        </tr>
                        <tr>
                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.bunkerdetails?.length > 0 &&
                          formReportdata.bunkerdetails.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.ifo ?? "N/A"}</td>
                              <td>{e.vlsfo ?? "N/A"}</td>
                              <td>{e.lsmgo ?? "N/A"}</td>
                              <td>{e.mgo ?? "N/A"}</td>
                              <td>{e.ulsfo ?? "N/A"}</td>

                              <td>{e.arob_ifo ?? "N/A"}</td>
                              <td>{e.arob_vlsfo ?? "N/A"}</td>
                              <td>{e.arob_lsmgo ?? "N/A"}</td>
                              <td>{e.arob_mgo ?? "N/A"}</td>
                              <td>{e.arob_ulsfo ?? "N/A"}</td>

                              <td>{e.pc_ifo ?? "N/A"}</td>
                              <td>{e.pc_vlsfo ?? "N/A"}</td>
                              <td>{e.pc_lsmgo ?? "N/A"}</td>
                              <td>{e.pc_mgo ?? "N/A"}</td>
                              <td>{e.pc_ulsfo ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th colSpan="5">Received</th>
                          <th colSpan="5">DEP.ROB</th>
                        </tr>
                        <tr>
                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.bunkerdetails?.length > 0 &&
                          formReportdata.bunkerdetails.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.r_ifo ?? "N/A"}</td>
                              <td>{e.r_vlsfo ?? "N/A"}</td>
                              <td>{e.r_lsmgo ?? "N/A"}</td>
                              <td>{e.r_mgo ?? "N/A"}</td>
                              <td>{e.r_ulsfo ?? "N/A"}</td>

                              <td>{e.dr_ifo ?? "N/A"}</td>
                              <td>{e.dr_vlsfo ?? "N/A"}</td>
                              <td>{e.dr_lsmgo ?? "N/A"}</td>
                              <td>{e.dr_mgo ?? "N/A"}</td>
                              <td>{e.dr_ulsfo ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">
                      Total Bunker Details
                    </h4>

                    <table className="table custom-table-bordered tc-table">
                      <thead>
                        <tr className="HeaderBoxText">
                          <th rowSpan="2">Miles:</th>
                          <th rowSpan="2">Eca/Seca Dist:</th>
                          <th rowSpan="2">Eca/Seca Days:</th>
                          <th rowSpan="2">SPD:</th>
                          <th rowSpan="2">ECA S.Cons:</th>
                          <th rowSpan="2">S.Cons IFO:</th>
                          <th rowSpan="2">S.Cons VLSFO:</th>
                          <th rowSpan="2">S.Cons LSMGO:</th>
                          <th rowSpan="2">S.Cons MGO:</th>
                          <th rowSpan="2">S.Cons ULSFO:</th>
                          <th rowSpan="2">P.Cons IFO:</th>
                          <th rowSpan="2">P.Cons VLSFO:</th>
                          <th rowSpan="2">P.Cons LSMGO:</th>
                          <th rowSpan="2">P.Cons MGO:</th>
                          <th rowSpan="2">P.Cons ULSFO:</th>
                        </tr>
                      </thead>

                      <tbody>
                        {formReportdata.totalbunkerdetails?.length > 0 &&
                          formReportdata.totalbunkerdetails.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.ttl_miles ?? "0.00"}</td>
                              <td>{e.ttl_seca_length ?? "0.00"}</td>
                              <td>{e.ttl_eca_days ?? "0.00"}</td>
                              <td>{e.ttl_speed ?? "0.00"}</td>
                              <td>00</td>
                              <td>{e.ttl_ifo ?? "0.00"}</td>
                              <td>{e.ttl_vlsfo ?? "0.00"}</td>
                              <td>{e.ttl_lsmgo ?? '0.00'}</td>
                              <td>{e.ttl_mgo ?? "0.00"}</td>
                              <td>{e.ttl_ulsfo ?? "0.00"}</td>
                              <td>{e.ttl_pc_ifo ?? "0.00"}</td>
                              <td>{e.ttl_pc_vlsfo ?? "0.00"}</td>
                              <td>{e.ttl_pc_lsmgo ?? "0.00"}</td>   
                              <td>{e.ttl_pc_mgo ?? "0.00"}</td> 
                              <td>{e.ttl_pc_ulsfo ?? "0.00"}</td> 
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">
                      Port Date Details
                    </h4>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th>Port</th>
                          <th>Funct.</th>
                          <th>MILES</th>
                          <th>Passage</th>
                          <th>STYPE</th>
                          <th>SPD</th>
                          <th>WF%</th>
                          <th>TSD</th>
                          <th>XSD</th>
                          <th>Arrival Date Time</th>
                          <th>Day</th>
                          <th>T.PDays</th>
                          <th>Departure</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.portdatedetails?.length > 0 &&
                          formReportdata.portdatedetails.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.port ?? "N/A"}</td>
                              <td>{e.funct ?? "N/A"}</td>
                              <td>{e.miles ?? "N/A"}</td>
                              <td>{e.passage_name ?? "N/A"}</td>
                              <td>{e.s_type ?? "N/A"}</td>
                              <td>{e.speed ?? "N/A"}</td>
                              <td>{e.wf_per ?? "N/A"}</td>
                              <td>{e.tsd ?? "N/A"}</td>
                              <td>{e.xsd ?? "N/A"}</td>
                              <td>{e.arrival_date_time || "N/A"}</td>
                              <td>{e.day ?? "N/A"}</td>
                              <td>{e.pdays ?? "N/A"}</td>
                              <td>{e.departure || "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">
                      CII Dynamics
                    </h4>

                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th rowSpan={2}>Year</th>
                          <th rowSpan={2}>Port</th>
                          <th rowSpan={2}>Function</th>
                          <th rowSpan={2}>Passage</th>
                          <th colSpan={3}>Operation Days</th>
                          <th rowSpan={2}>DISTANCE SAILED </th>
                          <th colSpan={5}>Sea Consumption </th>
                          <th colSpan={5}>Port Consumption</th>
                        </tr>
                        <tr>
                          <th>Sea </th>
                          <th>Port </th>
                          <th>Total</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.ciidynamics?.length > 0 &&
                          formReportdata.ciidynamics.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.year ?? "N/A"}</td>
                              <td>{e.port ?? "N/A"}</td>
                              <td>{e.funct ?? "N/A"}</td>
                              <td>{e.passage_name ?? "N/A"}</td>
                              <td>{e.sea ?? "N/A"}</td>
                              <td>{e.ops_port ?? "N/A"}</td>
                              <td>{e.total ?? "N/A"}</td>
                              <td>{e.dst_sailed ?? "N/A"}</td>

                              <td>{e.ifo ?? "N/A"}</td>
                              <td>{e.vlsfo ?? "N/A"}</td>
                              <td>{e.lsmgo ?? "N/A"}</td>
                              <td>{e.mgo ?? "N/A"}</td>
                              <td>{e.ulsfo ?? "N/A"}</td>

                              <td>{e.pc_ifo ?? "N/A"}</td>
                              <td>{e.pc_vlsfo ?? "N/A"}</td>
                              <td>{e.pc_lsmgo ?? "N/A"}</td>
                              <td>{e.pc_mgo ?? "N/A"}</td>
                              <td>{e.pc_ulsfo ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th rowSpan={2}>Co2 Emission VLSFO/ULSFO</th>
                          <th rowSpan={2}>Co2 Emission MGO/LSMGO </th>
                          <th rowSpan={2}>Co2 Emission IFO</th>
                          <th rowSpan={2}>Total Co2 Emission (MT)</th>
                          <th rowSpan={2}>CII Ref</th>
                          <th rowSpan={2}>CII Required</th>
                          <th rowSpan={2}>CII Attained</th>
                          <th rowSpan={2}>CII Rating</th>
                          <th rowSpan={2}>CII Band </th>
                          <th rowSpan={2}>Predicted Required CII 2025</th>
                          <th rowSpan={2}>Predicted CII Rating 2025</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.ciidynamics?.length > 0 &&
                          formReportdata.ciidynamics.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.co2_emission_vu ?? "N/A"}</td>
                              <td>{e.co2_emission_ml ?? "N/A"}</td>
                              <td>{e.co2_emission_ifo ?? "N/A"}</td>
                              <td>{e.co2_emission_total ?? "N/A"}</td>
                              <td>{e.cii_ref ?? "N/A"}</td>
                              <td>{e.cii_req ?? "N/A"}</td>
                              <td>{e.cii_att ?? "N/A"}</td>
                              <td>{e.cii_ret ?? "N/A"}</td>
                              <td>{e.cii_band ?? "N/A"}</td>
                              <td>{e.cii_pred ?? "N/A"}</td>
                              <td>{e.cii_pred_ret ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">
                      Total CII Dynamics Summary
                    </h4>

                    <table className="table custom-table-bordered tc-table">
                      <thead>
                        <tr className="HeaderBoxText">
                          <th>VLSFO Consumption:</th>
                          <th>ULSFO Consumption</th>
                          <th>IFO Consumption</th>
                          <th>LSMGO Consumption</th>
                          <th>MGO Consumption</th>
                          <th>CO2 Emission</th>
                        </tr>
                      </thead>

                      <tbody>
                        {formReportdata.totalciidynamicssummary?.length > 0 &&
                          formReportdata.totalciidynamicssummary.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.ttl_vlsfo_con ?? "0.00"}</td>
                              <td>{e.ttl_ulsfo_con ?? "0.00"}</td>
                              <td>{e.ttl_ifo_con ?? "0.00"}</td>
                              <td>{e.ttl_lsmgo_con ?? "0.00"}</td>
                              <td>{e.ttl_mgo_con ?? "0.00"}</td>
                              <td>{e.ttl_co2_emission_total ?? "0.00"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">EU ETS</h4>

                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th rowSpan={2}>Year</th>
                          <th rowSpan={2}>Port</th>
                          <th rowSpan={2}>Function</th>
                          <th colSpan={5}>Sea Consumption </th>
                          <th colSpan={5}>Port Consumption</th>
                          <th rowSpan={2}>Sea Emission (MT)</th>
                          <th rowSpan={2}>Port Emission (MT)</th>
                          <th rowSpan={2}>Total Emission(MT)</th>
                        </tr>
                        <tr>
                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>

                          <th>IFO</th>
                          <th>VLSFO</th>
                          <th>LSMGO</th>
                          <th>MGO</th>
                          <th>ULSFO</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.euets?.length > 0 &&
                          formReportdata.euets.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.year ?? "N/A"}</td>
                              <td>{e.port ?? "N/A"}</td>
                              <td>{e.funct_name ?? "N/A"}</td>
                              <td>{e.ifo ?? "N/A"}</td>
                              <td>{e.vlsfo ?? "N/A"}</td>
                              <td>{e.lsmgo ?? "N/A"}</td>
                              <td>{e.mgo ?? "N/A"}</td>
                              <td>{e.ulsfo ?? "N/A"}</td>
                              <td>{e.pc_ifo ?? "N/A"}</td>
                              <td>{e.pc_vlsfo ?? "N/A"}</td>
                              <td>{e.pc_lsmgo ?? "N/A"}</td>
                              <td>{e.pc_mgo ?? "N/A"}</td>
                              <td>{e.pc_ulsfo ?? "N/A"}</td>
                              <td>{e.sea_ems ?? "N/A"}</td>
                              <td>{e.port_ets_ems ?? "N/A"}</td>
                              <td>{e.ttl_ems ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                    <table className="table border-table table-invoice-report-colum">
                      <thead>
                        <tr>
                          <th rowSpan={2}>Phase In %</th>
                          <th rowSpan={2}>Sea ETS %</th>
                          <th rowSpan={2}>Sea ETS Emission (MT) </th>
                          <th rowSpan={2}>Port ETS %</th>
                          <th rowSpan={2}>Port ETS Emission (MT)</th>
                          <th rowSpan={2}>Total EU ETS (MT)</th>
                          <th rowSpan={2}>Total EU ETS Expense</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.euets?.length > 0 &&
                          formReportdata.euets.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.phase_pre ?? "N/A"}</td>
                              <td>{e.sea_pre_ets ?? "N/A"}</td>
                              <td>{e.sea_ets_ems ?? "N/A"}</td>
                              <td>{e.port_pre_ems ?? "N/A"}</td>
                              <td>{e.port_ems ?? "N/A"}</td>
                              <td>{e.ttl_eu_ets ?? "N/A"}</td>
                              <td>{e.ttl_eu_ets_exp ?? "N/A"}</td>
                            </tr>
                          ))}
                      </tbody>
                    </table>

                    <h4 className="font-weight-bold tc-sub-header">
                      Total EU ETS Summary
                    </h4>

                    <table className="table custom-table-bordered tc-table">
                      <thead>
                        <tr className="HeaderBoxText">
                          <th>Total Sea Emission:</th>
                          <th>Total Port Emission:</th>
                          <th>Total Emission:</th>
                          <th>Total Sea Ets Emission:</th>
                          <th>Total Port Ets Emission:</th>
                          <th>Total Ets Emission:</th>
                          <th>Total EU-Ets Emission:</th>
                        </tr>
                      </thead>

                      <tbody>

                      {formReportdata.totaleuetssummary?.length > 0 &&
                          formReportdata.totaleuetssummary.map((e, idx) => (
                            <tr key={idx}>
                              <td>{e.ttl_sea_emi ?? "0.00"}</td>
                              <td>{e.ttl_port_emi ?? "0.00"}</td>
                              <td>{e.ttl_emi ?? "0.00"}</td>
                              <td>{e.ttl_sea_ets_emi ?? "0.00"}</td>
                              <td>{e.ttl_port_ets_emi ?? "0.00"}</td>
                              <td>{e.ttl_ets_emi ?? "0.00"}</td>
                              <td>{e.ttl_eu_ets_emi ?? "0.00"}</td>
                            </tr>
                          ))}



                      </tbody>
                    </table>
                  </tbody>
                </table>

                {/* 

 changed as per discussion with pallav sir
                <td className="border-0">
                  <h5 className="font-weight-bold">P & L Summary</h5>
                  <table className="table border-table custom-table-bordered">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Freight :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td>{formReportdata.p_l_summary.revenue.freight} </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Demurrage :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.revenue.demurrage}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Gross Income :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.revenue.grossRevenue}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Less Commissions :</td>

                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.revenue.b_commission}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Less Tax :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue ? (
                          // Here condition is wrong. needs to discuss.
                          <td className="text-right">
                            {formReportdata.cargos
                              ? formReportdata.cargos.frat_tax
                              : ""}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Less Despatch :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.revenue.dem_disch}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Misc Income :</td>
                        {/* {formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? (
                        <td className="text-right">
                          {formReportdata.mis_cost}
                        </td>
                        {/* ):undefined

                      }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Net Income :</td>
                        {/* {formReportdata.p_l_summary.revenue && formReportdata.p_l_summary.revenue ? ( 
                        <td className="text-right">
                          {formReportdata.n_income}
                        </td>
                        {/* ):undefined

                      } 
                      </tr>
                    </tbody>
                  </table>
                  <table className="table border-table custom-table-bordered mt-2">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Vessel Expenses :</td>

                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.revenue &&
                        formReportdata.p_l_summary.revenue ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.expense.vessel_expenxe}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Less Address :</td>

                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.expense &&
                        formReportdata.p_l_summary.expense ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.expense.less_address}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Canel Expenses :</td>

                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.expense &&
                        formReportdata.p_l_summary.expense ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.expense.cancel_expense}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Port Expenses :</td>
                        {formReportdata.bunkerdetails &&
                        formReportdata.bunkerdetails ? (
                          <td className="text-right">
                            {formReportdata.bunkerdetails.pc_ifo}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Bunker Expenses :</td>
                        {/* {formReportdata.port_expense && formReportdata.p_l_summary.expense.bunker_expense.port_expense ? ( 
                        <td className="text-right">{formReportdata.r_ifo}</td>
                        {/* ):undefined
                      } 
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Misc Expenses :</td>
                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.expense &&
                        formReportdata.p_l_summary.expense ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.expense.mis_cost}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Less Rebills :</td>
                        <td className="text-right">{formReportdata.reb}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Total Expenses :</td>

                        {formReportdata &&
                        formReportdata.p_l_summary &&
                        formReportdata.p_l_summary.expense &&
                        formReportdata.p_l_summary.expense ? (
                          <td className="text-right">
                            {formReportdata.p_l_summary.expense.totalExpenses}
                          </td>
                        ) : (
                          undefined
                        )}
                      </tr>
                    </tbody>
                  </table>
                  <table className="table border-table custom-table-bordered mt-2">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Profit :</td>
                        <td className="text-right">
                          {formReportdata && formReportdata.p_l_summary
                            ? formReportdata.p_l_summary.totalvoyage
                                .total_profit
                            : ""}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Daily Profit :</td>
                        <td className="text-right">
                          {formReportdata.daily_profit}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Breakeven :</td>
                        <td className="text-right">
                          {formReportdata.breakeven}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">
                          Freight Rate(USD/Mt) :
                        </td>
                        <td className="text-right">
                          {formReportdata.cargos
                            ? formReportdata.cargos.frat_rate
                            : ""}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">
                          TC Equiv(USD/Day) :
                        </td>
                        <td className="text-right">
                          {formReportdata.tco_equi}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Gross TCE :</td>
                        <td className="text-right">
                          {formReportdata && formReportdata.p_l_summary
                            ? formReportdata.p_l_summary.totalvoyage
                                .gross_revenue
                            : ""}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table className="table border-table custom-table-bordered mt-2">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Commenced On :</td>
                        <td className="text-right">
                          {formReportdata.commence_date}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Complete On :</td>
                        <td className="text-right">
                          {formReportdata.completing_date}
                        </td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Voyage Days :</td>
                        <td className="text-right">
                          {formReportdata && formReportdata.p_l_summary
                            ? formReportdata.p_l_summary.totalvoyage.total_days
                            : ""}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td> */}
              </td>
            </tbody>
          </table>
        </div>
      </div>
    </article>
  );
});

const TctoEstimateDetailReport = (props) => {
  const [pdfData, setPdfData] = useState();
  const [userInput, setUserInput] = useState();
  const [emailModal, setEmailModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [mailTitlePayload, setMailTitlePayload] = useState({});

  const componentRef = useRef(null);

  useEffect(() => {
    setUserInput(props.data);
  }, []);

  const sendEmailReportModal = async () => {
    try {
      setLoading(true);

      const quotes = document.getElementById("divToPrint");

      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });

      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      const doc = new jsPDF("p", "mm");
      let position = 25;
      const pageData = canvas.toDataURL("image/jpeg", 1.0);
      doc.addImage(pageData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          pageData,
          "PNG",
          5,
          position + 25,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }

      // Create Blob
      const blob = doc.output("blob");

      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      setLoading(false);
      setPdfData(blob);
      setEmailModal(true);
    } catch (error) {
      console.error("Error:", error);
      setLoading(false);
      // this.setState({ loading: false });
      // Handle errors here
    }
  };

  const printDocument = () => {
    const quotes = document.getElementById("divToPrint");
    // alert("tco");
    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true,
    }).then(function (canvas) {
      // const link = document.createElement("a");
      // link.download = "html-to-img.png";
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;
      const doc = new jsPDF("p", "mm");
      let position = 8;
      const pageData = canvas.toDataURL("image/jpeg", 1.0);
      const imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          imgData,
          "PNG",
          5,
          position + 10,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save("TCTOEstimateDetail.pdf");
    });
  };

  return (
    <div className="body-wrapper modalWrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection" />
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li onClick={sendEmailReportModal}>Send Email</li>
                    <li onClick={printDocument}>Download</li>
                    <li>
                      <ReactToPrint
                        trigger={() => (
                          <span className="text-bt">
                            <PrinterOutlined />
                            Print
                          </span>
                        )}
                        content={() => componentRef.current}
                      />
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <ComponentToPrint ref={componentRef} data={props.data} />
          </div>
        </div>
      </article>

      {emailModal && (
        <Modal
          title="New Message"
          open={emailModal}
          onOk={() => {
            setEmailModal(false);
          }}
          onCancel={() => {
            setEmailModal(false);
          }}
          footer={null}
        >
          {pdfData && (
            <Email
              handleClose={() => {
                setEmailModal(false);
              }}
              attachmentFile={pdfData}
              title={window.corrector(
                `TCTO Estimate Detail Report  ${userInput.estimate_id} ||${userInput.vessel_name}||${userInput.charterer_name}||${userInput.company_lob_name}||${userInput.tco_status_name}`
              )}
            />
          )}
        </Modal>
      )}

      {loading && (
        <div
          style={{
            position: "absolute",
            top: "10%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        >
          <Spin size="large" />
        </div>
      )}
    </div>
  );
};

export default TctoEstimateDetailReport;
