import React, { Component, useEffect, useRef, useState } from 'react';
import { Menu, Dropdown, Modal , Spin } from 'antd';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import URL_WITH_VERSION from '../../shared';
import Email from '../../components/Email';

import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import html2canvas from 'html2canvas';
import moment from 'moment';
import { forwardRef } from 'react';
const ComponentToPrint = forwardRef((props, ref) => {

  const [formReportdata, setFormReportdata] = useState(Object.assign({}, props.data || {}))

  return (
    <article className="article toolbaruiWrapper" ref={ref}>
      <div className="box box-default" id="divToPrint">
        <div className="box-body">
          <div className="invoice-inner-download mt-3">
            <div className="row">
              <div className="col-12 text-center">

                <img className="title reportlogo" src={formReportdata.logo} alt="No img" crossOrigin='anonymous' />
                <p className="sub-title m-0">{formReportdata.my_company_name ? formReportdata.my_company_name : "N/A"}</p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 mx-auto">
              <div className="text-center invoice-top-address">
                <p>{formReportdata.address ? formReportdata.address : "N/A"}</p>
              </div>
            </div>
          </div>

          <div className="row p10">
            <div className="col-md-12">
              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">COA Cargo ID :</td>
                    <td className="">{formReportdata.contract_id || "N/A"}</td>

                    <td className="font-weight-bold">Cargo Group :</td>
                    <td className="">{formReportdata.cargo_name2 || "N/A"}</td>

                    <td className="font-weight-bold">Status :</td>
                    <td className="">{formReportdata.coa_status_name || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Master Contract ID :</td>
                    <td className="">N/A</td>

                    <td className="font-weight-bold">COA Qty/Unit :</td>
                    <td className="">{formReportdata.coa_qty || "N/A"} / {formReportdata.coa_qty_unit_name || "N/A"}</td>

                    <td className="font-weight-bold">Currency :</td>
                    <td className="">{formReportdata.currency_name || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Charterer Name :</td>
                    <td className="">{formReportdata.charterer_name || "N/A"}</td>

                    <td className="font-weight-bold">Option %/ Type :</td>
                    <td className="">{formReportdata.coa_option || "0.00"} / {formReportdata.coa_option_type_name || "N/A"}</td>

                    <td className="font-weight-bold">Invoice By :</td>
                    <td className="">{formReportdata.invoice_by_name || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Start Date :</td>
                    <td className="">{formReportdata.start || "N/A"}</td>

                    <td className="font-weight-bold">Min/Max Qty :</td>
                    <td className="">{formReportdata.min_qty || "N/A"} / {formReportdata.max_qty || "N/A"} </td>

                    <td className="font-weight-bold">Min Inv Qty :</td>
                    <td className="">{formReportdata.min_inv_qty || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">End date :</td>
                    <td className="">{formReportdata.end || "0000-00-00"}</td>

                    <td className="font-weight-bold">Confirm CP Lift :</td>
                    <td className="">{formReportdata.confirm_cp_liftings || "N/A"}</td>

                    <td className="font-weight-bold">Freight Type :</td>
                    <td className="">{formReportdata.freight_type_name || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">My Company/LOB :</td>
                    <td className="">{formReportdata.my_company_name || "N/A"} / {formReportdata.lob_name || "N/A"}</td>

                    <td className="font-weight-bold">Optional Liftings :</td>
                    <td className="">{formReportdata.optional_liftings || "N/A"}</td>

                    <td className="font-weight-bold">Freight Rate :</td>
                    <td className="">{formReportdata.freight_rate || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Cargo Name :</td>
                    <td className="">{formReportdata.c_name_name || "N/A"}</td>

                    <td className="font-weight-bold">CP QTY Per lift :</td>
                    <td className="">{formReportdata.cp_qty_per_lift || "N/A"}</td>

                    <td className="font-weight-bold">VAT No. / Tax % :</td>
                    <td className="">{formReportdata.vat_no || "N/A"} / {formReportdata.vat_per || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Freight Bill Via :</td>
                    <td className="">{formReportdata.freight_bill_via_name || "N/A"}</td>

                    <td className="font-weight-bold">Min/Max Qty/Lift :</td>
                    <td className="">{formReportdata.min_qty_lift || "N/A"} / {formReportdata.max_qty_lift || "N/A"}</td>

                    <td className="font-weight-bold">Fixed By User :</td>
                    <td className="">{formReportdata.fixed_by_user || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Trade Area :</td>
                    <td className="">{formReportdata.trade_area_name || "N/A"}</td>

                    <td className="font-weight-bold">Total COA Qty :</td>
                    <td className="">{formReportdata.ttl_coa || "N/A"}</td>

                    <td className="font-weight-bold">Ref Company :</td>
                    <td className="">{formReportdata.ref_company_name || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">CP Date/Place :</td>
                    <td className="">{formReportdata.cp_date || "0000-00-00"} / N/A</td>

                    <td className="font-weight-bold">Ttl Fixed Qty :</td>
                    <td className="">{formReportdata.ttl_fixed_qty || "N/A"}</td>

                    <td className="font-weight-bold">Reference No./PO :</td>
                    <td className="">{formReportdata.reference_number || "N/A"}</td>
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Commence Date :</td>
                    <td className="">{formReportdata.comence_date || "0000-00-00"}</td>

                    <td className="font-weight-bold">Ttl Remaining Qty :</td>
                    <td className="">{formReportdata.ttl_remaining_qty || "N/A"}</td>

                    <td className="font-weight-bold">{" "}</td>
                    <td className="">{" "}</td>
                  </tr>
                </tbody>
              </table>

              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <td className="border-0">
                    <h4 className="font-weight-bold tc-sub-header">Load Options</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Port/Area</th>
                          <th>L/D Rates</th>
                          <th>L/H Rates</th>
                          <th>Terms</th>
                          <th>TT</th>
                          <th>PExp</th>
                          <th>Draft</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.loadoptions && formReportdata.loadoptions &&
                          formReportdata.loadoptions.length > 0 ? formReportdata.loadoptions.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.port_area_name || "N/A"}</td>
                                  <td>{e.l_d_rates ? parseFloat(e.l_d_rates).toFixed(2) : "N/A"}</td>
                                  <td>{e.ld_ru || "N/A"}</td>
                                  <td>{e.ld_terms_name || "N/A"}</td>
                                  <td>{e.ld_tt || "N/A"}</td>
                                  <td>{e.pexp || "N/A"}</td>
                                  <td>{e.draft || "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                        }

                      </tbody>
                    </table>
                  </td>

                  <td className="border-0">
                    <h4 className="font-weight-bold tc-sub-header">Discharge Options</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Port/Area</th>
                          <th>L/D Rates</th>
                          <th>L/H Rates</th>
                          <th>Terms</th>
                          <th>TT</th>
                          <th>PExp</th>
                          <th>Draft</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.dischargeoptions && formReportdata.dischargeoptions &&
                          formReportdata.dischargeoptions.length > 0 ? formReportdata.dischargeoptions.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.port_area_name || "N/A"}</td>
                                  <td>{e.l_d_rates ? parseFloat(e.l_d_rates).toFixed(2) : "N/A"}</td>
                                  <td>{e.ld_ru || "N/A"}</td>
                                  <td>{e.ld_terms_name || "N/A"}</td>
                                  <td>{e.ld_tt || "N/A"}</td>
                                  <td>{e.pexp || "N/A"}</td>
                                  <td>{e.draft || "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                        }


                      </tbody>
                    </table>
                  </td>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Cargo Itinerary</h4>

              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>F</th>
                    <th>Port</th>
                    <th>Quantity</th>
                    <th>Berth</th>
                    <th>L/D Rate(Days)</th>
                    <th>L/D Rate(Hours)</th>
                    <th>Terms</th>
                    <th>Port Exp</th>
                    <th>H</th>
                    <th>Draft</th>
                    <th>Unit</th>
                    <th>Salanity</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata.itineraryoptions && formReportdata.itineraryoptions &&
                    formReportdata.itineraryoptions.length > 0 ? formReportdata.itineraryoptions.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.io_f_name || "N/A"}</td>
                            <td>{e.port_id_name || "N/A"}</td>
                            <td>{e.io_qty || "N/A"}</td>
                            <td>{e.berth || "N/A"}</td>
                            <td>{e.l_d_rate || "N/A"}</td>
                            <td>{e.rate_unit || "N/A"}</td>
                            <td>{e.io_terms_name || "N/A"}</td>
                            <td>{e.port_exp || "N/A"}</td>
                            <td>{e.io_h || "N/A"}</td>
                            <td>{e.draft || "N/A"}</td>
                            <td>{e.io_unit_name || "N/A"}</td>
                            <td>{e.salanity || "N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                  }
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Cargo Pricing & Billing</h4>
              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>Load Port(S)</th>
                    <th>Discharge Port(S)</th>
                    <th>Cargo</th>
                    <th>Frt Type</th>
                    <th>Amount</th>
                    <th>Table</th>

                  </tr>
                </thead>
                <tbody>
                  {formReportdata['--'] && formReportdata['--'] &&
                    formReportdata['--'].length > 0 ? formReportdata['--'].map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.load_ports_name || "N/A"}</td>
                            <td>{e.discharge_ports_name || "N/A"}</td>
                            <td>{e.cargo_name || "N/A"}</td>
                            <td>{e.frt_type_name || "N/A"}</td>
                            <td>{e.amount || "N/A"}</td>
                            <td>{e.p_table || "N/A"}</td>


                          </tr>
                        </>
                      )
                    }) : undefined

                  }
                </tbody>
              </table>

              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <td className="border-0">
                    <h4 className="font-weight-bold tc-sub-header">Broker</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Broker</th>
                          <th>Amount</th>
                          <th>Rate Type</th>
                          <th>Pay Method</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.broker && formReportdata.broker &&
                          formReportdata.broker.length > 0 ? formReportdata.broker.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.broker_name || "N/A"}</td>
                                  <td>{e.amount || "N/A"}</td>
                                  <td>{e.rate_type_name || "N/A"}</td>
                                  <td>{e.pay_method_name || "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                        }
                      </tbody>
                    </table>
                  </td>

                  <td className="border-0">
                    <h4 className="font-weight-bold tc-sub-header">Extra Freight Term</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Code</th>
                          <th>Extra Freight Term</th>
                          <th>Rate/Lump</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.extrafreightterm && formReportdata.extrafreightterm &&
                          formReportdata.extrafreightterm.length > 0 ? formReportdata.extrafreightterm.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.code || "N/A"}</td>
                                  <td>{e.extra_freight_term_name || "N/A"}</td>
                                  <td>{e.rate_lump || "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined

                        }
                      </tbody>
                    </table>
                  </td>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Billing and Banking details</h4>
              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">Remit Bank :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.remittance_bank_name}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">Account No :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.account_no}</td>
                    ) : undefined
                    }

                    <td className="font-weight-bold">1st Invoice(%) :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{parseFloat(formReportdata.billingandbankingdetails.inv_per).toFixed(2)}</td>
                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Balance(%) :</td>
                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{parseFloat(formReportdata.billingandbankingdetails.bal_per).toFixed(2)}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">Payment Term :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.payment_term_name}</td>
                    ) : undefined
                    }

                    <td className="font-weight-bold">Due Date :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.due_date}</td>
                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Billing Days :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.billing_days}</td>
                    ) : undefined
                    }

                    <td className="font-weight-bold">Freight Surcharge :</td>

                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{parseFloat(formReportdata.billingandbankingdetails.freight_surcharge).toFixed(2)}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">Billing Basis :</td>
                    {formReportdata.billingandbankingdetails && formReportdata.billingandbankingdetails ? (
                      <td className="">{formReportdata.billingandbankingdetails.billing_basis}</td>
                    ) : undefined
                    }
                  </tr>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Supplier/Receiver Info</h4>
              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">Final Destination :</td>
                    {formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.final_destination}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">Supplier :</td>

                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.supplier_name}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">Receiver :</td>

                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.reciever_name}</td>
                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Ref Company :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.ref_company_name}</td>
                    ) : undefined
                    }
                    <td className="font-weight-bold">VAT No / % :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.vat_no} / {formReportdata.supplierreciever.vat_per}</td>

                    ) : undefined
                    }

                    <td className="font-weight-bold">OBL No. :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.obl_no}</td>

                    ) : undefined
                    }
                  </tr>

                  <tr>
                    <td className="font-weight-bold">Ref Contract :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.ref_contract}</td>

                    ) : undefined
                    }
                    <td className="font-weight-bold">Commingle Code :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.commingle_code}</td>

                    ) : undefined
                    }
                    <td className="font-weight-bold">Trader :</td>
                    {formReportdata.supplierreciever && formReportdata.supplierreciever ? (
                      <td className="">{formReportdata.supplierreciever.trader}</td>
                    ) : undefined
                    }
                  </tr>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Estimate Rev/Exp Info</h4>
              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>Description Type</th>
                    <th>Rev/Exp</th>
                    <th>Type Of Rev/Exp</th>
                    <th>Exchange Rate</th>
                    <th>Currency</th>
                    <th>Amount Base</th>
                    <th>Account Code</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata.revexp && formReportdata.revexp &&
                    formReportdata.revexp.length > 0 ? formReportdata.revexp.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.description || "N/A"}</td>
                            <td>{e.rev_exp_name || "N/A"}</td>
                            <td>{e.re_type_name || "N/A"}</td>
                            <td>{e.exchange_rate || "N/A"}</td>
                            <td>{e.re_currency_name || "N/A"}</td>
                            <td>{e.amount_base || "N/A"}</td>
                            <td>{e.code || "N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined
                  }

                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Rebill</h4>
              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>F</th>
                    <th>Port</th>
                    <th>Ledger Code</th>
                    <th>Ledger Code Description</th>
                    <th>Cost Details Description</th>
                    <th>Rebillable</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata.rebillsettings && formReportdata.rebillsettings &&
                    formReportdata.rebillsettings.length > 0 ? formReportdata.rebillsettings.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.cv_f_name ? e.cv_f_name : "N/A"}</td>
                            <td>{e.cv_port_name ? e.cv_port_name : "N/A"}</td>
                            <td>{e.ledger || "N/A"}</td>
                            <td>{e.ledger_code_description || "N/A"}</td>
                            <td>{e.cost_detail_description || "N/A"}</td>
                            <td>{e.rebillable == 1 ? "Yes" : "No"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined
                  }
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Dem/Des Term</h4>
              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">Load Dem/Des :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].load_dem} / {formReportdata['-----'].load_des}</td>
                    ) : undefined}



                    <td className="font-weight-bold">Total Time Bar :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].total_time_bar}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Per Day/Per hour :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].d_per_day == 1 ? "Yes" : "No"} / {formReportdata['-----'].d_per_hour == 1 ? 'Yes' : 'No'}</td>
                    ) : undefined}


                    <td className="font-weight-bold">TT Hours :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].tt_hours}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Disc Dem/Des :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].disch_dem} / {formReportdata['-----'].disch_des}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Freight Surcharge :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].freight_surcharge}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Per Day/Per hour :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].per_day == 1 ? "Yes" : "No"} / {formReportdata['-----'].per_hour == 1 ? 'Yes' : 'No'}</td>
                    ) : undefined}

                    <td className="font-weight-bold">Bunker Surcharge :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].bunker_surcharge}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Dem/Des Curr :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].dem_des_curr_name}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Admin Charges :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].admin_charges}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Demurrage :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].demurrage}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Dem/Des Commitable :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].dem_des_commitable == 1 ? "Yes" : 'No'}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Laytime :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].laytime}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Reversible All Port :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].reversible_all_port == 1 ? "Yes" : "No"}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Laytime Allowed :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].laytime_allowed}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Non Rev. All Port :</td>

                    {formReportdata['------'] && formReportdata['------'] ? (
                      <td className="">{formReportdata['------'].non_rev_all_port == 1 ? 'Yes' : "No"}</td>
                    ) : undefined}

                  </tr>

                  <tr>
                    <td className="font-weight-bold">Days / Hours :</td>

                    {formReportdata['-----'] && formReportdata['-----'] ? (
                      <td className="">{formReportdata['-----'].day == 1 ? "Yes" : "No"} / {formReportdata['-----'].hour == 1 ? 'Yes' : "No"}</td>
                    ) : undefined}
                    <td className="font-weight-bold">{" "}</td>
                    <td>{" "}</td>
                  </tr>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Nomination Task</h4>
              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>Nomination Tasks</th>
                    <th>Due Date Offset</th>
                    <th>Remainder Offset</th>
                    <th>Priority</th>
                    <th>Category</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata.nominationtask && formReportdata.nominationtask &&
                    formReportdata.nominationtask.length > 0 ? formReportdata.nominationtask.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.nomination_task || "N/A"}</td>
                            <td>{e.due_date || "0000-00-00"}</td>
                            <td>{e.reminder_offset || "N/A"}</td>
                            <td>{e.priority_name || "N/A"}</td>
                            <td>{e.category || "N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined
                  }
                </tbody>
              </table>

              <table className="table  table-invoice-report-colum">
                <tbody>
                  <td>
                    <h4 className="font-weight-bold tc-sub-header">Cargo</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Cargo Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.cargo && formReportdata.cargo &&
                          formReportdata.cargo.length > 0 ? formReportdata.cargo.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.cargo_id_name ? e.cargo_id_name : "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined
                        }
                      </tbody>
                    </table>
                  </td>

                  <td>
                    <h4 className="font-weight-bold tc-sub-header">Vessel Type</h4>
                    <table className="table  custom-table-bordered tc-table">
                      <thead>
                        <tr>
                          <th>Vessel</th>
                        </tr>
                      </thead>
                      <tbody>
                        {formReportdata.vesseltype && formReportdata.vesseltype &&
                          formReportdata.vesseltype.length > 0 ? formReportdata.vesseltype.map((e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.vessel_name ? e.vessel_name : "N/A"}</td>
                                </tr>
                              </>
                            )
                          }) : undefined
                        }
                      </tbody>
                    </table>
                  </td>
                </tbody>
              </table>

              <h4 className="font-weight-bold tc-sub-header">Link Trade Cargos</h4>
              <table className="table  custom-table-bordered tc-table">
                <tbody>
                  <tr>
                    <td className="font-weight-bold">Number of Cargo Liftings :</td>

                    {formReportdata['....'] && formReportdata['....'] ? (
                      <td className="text-right">{formReportdata['....'].number_of_cargo_liftings}</td>
                    ) : undefined}


                    <td className="font-weight-bold">COA Qty :</td>

                    {formReportdata['....'] && formReportdata['....'] ? (
                      <td className="text-right">{formReportdata['....'].coa_qty}</td>
                    ) : undefined}


                    <td className="font-weight-bold">Option %/Type :</td>

                    {formReportdata['....'] && formReportdata['....'] ? (
                      <td className="text-right"> {formReportdata['....'].option_per || "0.00"} / {formReportdata['....'].option_type || "N/A"} </td>
                    ) : undefined}


                    <td className="font-weight-bold">Status :</td>
                    {formReportdata['....'] && formReportdata['....'] ? (
                      <td className="text-right">{formReportdata['....'].status}</td>
                    ) : undefined}
                  </tr>
                </tbody>
              </table>
              <table className="table  custom-table-bordered tc-table">
                <thead>
                  <tr>
                    <th>Cargo ID</th>
                    <th>Cargo Name</th>
                    <th>Laycan From</th>
                    <th>Laycan To</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Voyage No</th>
                    <th>Linked Voyage</th>
                    <th>Status</th>
                    <th>Freight rate</th>
                    <th>Currency</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata['...'] && formReportdata['...'] &&
                    formReportdata['...'].length > 0 ? formReportdata['...'].map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.cargo_contract_id || "N/A"}</td>
                            <td>{e.cargo_full_name || "N/A"}</td>
                            <td>{e.laycan_from ? moment(e.laycan_from).format("YYYY-MM-DD HH:mm") : "0000-00-00 00:00:00"}</td>
                            <td>{e.laycan_to ? moment(e.laycan_to).format("YYYY-MM-DD HH:mm") : "0000-00-00 00:00:00"}</td>
                            <td>{parseFloat(e.cp_qty.replaceAll(",", "")).toFixed(2)}</td>
                            <td>{e.cp_unit_name || "N/A"}</td>
                            <td>{e.voyage_no ? e.voyage_no : "N/A"}</td>
                            <td>{e.linked_voyage ? e.linked_voyage : "N/A"}</td>
                            <td>{e.cargo_status_name || "N/A"}</td>
                            <td>{e.freight_rate || "N/A"}</td>
                            <td>{e.currency_name || "N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </article>
  );
})

const CoaCargoReports = (props) => {
  const [pdfData, setPdfData] = useState();
  const [userInput, setUserInput] = useState()
  const [emailModal, setEmailModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [mailTitlePayload, setMailTitlePayload] = useState({})
  const [titleArray,setTitleArray] = useState([])
  const [name, setname] = useState('Printer')




  const componentRef = useRef()

  const printReceipt = () => {
    window.print();
  }

  useEffect(() => {
   
    // const {contract_id,charterer_name,my_company_name,lob_name}=props.data
    // const tempArray=[contract_id,charterer_name,my_company_name,lob_name]
    // setTitleArray(tempArray)
    setUserInput(props.data)

  }, [])

  const printDocument = () => {

    var quotes = document.getElementById('divToPrint');

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true
    }).then(function (canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF('p', 'mm');
      var position = 8;
      var pageData = canvas.toDataURL('image/jpeg', 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, 'PNG', 5, position, imgWidth - 8, imgHeight + 37);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(imgData, 'PNG', 5, position + 10, imgWidth - 8, imgHeight + 37);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save('CaoCargoReport.pdf');

    });
  };


 

  

  const sendEmailReportModal = async () => {
    try {
      
      setLoading(true)
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      setLoading(false)
      setPdfData(blob)
      setEmailModal(true)
  
    } catch (error) {
      console.error('Error:', error);
      setLoading(false)
      // this.setState({ loading: false });
      // Handle errors here
    }
  };



 

  
  return (
    <>
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={sendEmailReportModal}>Send Email</li>
                      {/* <button onClick={sendEmailReportModal}>Open Email Modal</button> */}

                      <li onClick={printDocument}>
                        Download
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => componentRef.current}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={componentRef} data={props.data} />
            </div>
          </div>
        </article>
        {emailModal && (
          <Modal
            title="New Message"
            visible={emailModal}
            onOk={() => {
              setEmailModal(false);
            }}
            onCancel={() => {
              setEmailModal(false);
            }}
            footer={null}
          >
             
            {pdfData && <Email
              handleClose={
                () => {
                  setEmailModal(false);
                }
                
              }
              attachmentFile={pdfData}        
             
            title={window.corrector(`COA_Cargo_Contract_Report||${userInput.contract_id}||${userInput.charterer_name}||${userInput.my_company_name}||${userInput.lob_name}` ) }
              
              // title={window.emailTitltCorrectFunction('COA Cargo Contract Report',titleArray) }
           
            />}
          </Modal>
        )}
          {
      loading && (
        <div style={{position:'absolute', top:'10%',left: '50%',transform: 'translate(-50%, -50%)' }}>
          <Spin  size="large" />
        </div>
      )
     }
      </div>
    </>
  );
}

export default CoaCargoReports;
