import React from 'react';
import { Table,Popconfirm } from 'antd';
import { DeleteOutlined, EditOutlined  } from '@ant-design/icons';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  ResizeableTitle,
  apiDeleteCall,
  openNotificationWithIcon
} from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';

class HireScheduleList extends React.Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => this.redirectToAdd(e, record)}>
            <EditOutlined />

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => this.onRowDeletedClick(record.id)}
              >
             <DeleteOutlined/>

              </Popconfirm>
            </span>
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['hire-schedule-list'] ? FIELDS['hire-schedule-list']['tableheads'] : []
    );
    tableHeaders.push(tableAction);
    this.state = {
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      sidebarVisible: false,
      loading: true,
      typesearch:{}
    };
  }
  componentDidMount = () => {
    this.getTableData();
  };



  redirectToAdd=(e, record = null)=>{
   
  }


  onRowDeletedClick=(id)=>{
    let _url = `${URL_WITH_VERSION}/make_payment/hire-schedlue-list/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })

  }





  getTableData = async (searchtype = {}) => {
    const { pageOptions } = this.state;
 
    let qParams = { p: pageOptions.pageIndex, l: +pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };
    let search=searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue')?searchtype:this.state.typesearch;
    
    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim()

      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
      this.state.typesearch={'searchOptions':search.searchOptions,'searchValue':search.searchValue}
      
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/make_payment/hire-schedlue-list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    if (dataArr && dataArr.length > 0) {
      dataArr.map(e => {
        e['form_to_date'] = e.commence_date + '/' + e.completing_date;
        return true;
      });
    }
    let state = { loading: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }

    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }


  render() {
    const { columns, responseData, search, pageOptions, loading ,sidebarVisible} = this.state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={'hire-schedule-list-toolbar'}
                    optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                    callback={e => this.callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv', 'hire-schedule') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf', 'hire-schedule') },
                      { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'hire-schedule') },
                    ]}
                  />
                ) : (
                  undefined
                )}
                 {sidebarVisible ? (
            <SidebarColumnFilter
              columns={columns}
              sidebarVisible={sidebarVisible}
              callback={(e) => this.callOptions(e)}
            />
          ) : null}
              </div>
            </div>
            <div className="box-body">
              <Table
                bordered
                size="small"
                pagination={false}
                dataSource={responseData}
                loading={loading}
                scroll={{ x: 'max-content' }}
                columns={tableColumns}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default HireScheduleList;
