import React, { Component } from "react";
import { Table, Popconfirm, Modal, Col, Row, Select } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../../shared";
import { FIELDS } from "../../../shared/tableFields";
import ToolbarUI from "../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../shared/SidebarColumnFilter";
import MakePayment from "../../../components/MakePayment";
import { useNavigate } from "react-router-dom";

import { useEffect } from "react";
import ClusterColumnChart from "../../dashboard/charts/ClusterColumnChart";
import LineChart from "../../dashboard/charts/LineChart";
import PieChart from "../../dashboard/charts/PieChart";
import StackHorizontalChart from "../../dashboard/charts/StackHorizontalChart";

const HirePaymentList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    paymentData: {},
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });

  const navigate = useNavigate();
  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };
  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["hire-payable-list"]
        ? FIELDS["hire-payable-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/make_payment/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const redirectToAdd = async (e, record = null) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/make_payment/edit?e=${record.id}&frm=time_charter_in_form`
    );
    let respData = await response;

    const response1 = await getAPICall(
      `${URL_WITH_VERSION}/tci/edit?e=${respData["data"]["tci_id"]}`
    );
    const data1 = await response1["data"];

    let bunker = [];
    bunker = [
      { dr_name: "Delivery Cost" },
      { dr_name: "Adj on delivery" },
      { dr_name: "Redelivery Cost" },
      { dr_name: "Adj on redelivery" },
    ];

    data1["bunkers"].map((e) => {
      if (e.description === 3 || e.description === 6 || e.description === 4) {
        let index = e.description === 3 ? 0 : 2;
        let cDesc = JSON.parse(e.c_fields_values);
        let item = { ifo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0, mgo: 0 };
        let iv = {
          IFO: "ifo",
          VLSFO: "vlsfo",
          ULSFO: "ulsfo",
          MGO: "mgo",
          LSMGO: "lsmgo",
        };
        cDesc.map(
          (_e) => (item[iv[_e.name]] = _e.consumption + " X " + _e.price)
        );
        bunker[index] = Object.assign(item, bunker[index]);
      }
    });

    respData["data"]["bunkerdeliveryredeliveryterm"] = bunker;

    if (respData && respData["data"] && respData["data"].hasOwnProperty("id")) {
      setState((prevState) => ({
        ...prevState,
        paymentData: respData["data"],
        isVisible: true,
      }));
    }
  };

  const onCancel = () => {
    setState({ ...state, isAdd: true, isVisible: true });
  };

  const modalClose = () => {
    setState({ ...state, isAdd: true, isVisible: false });
    //  setState({ ...state, isAdd: true, isVisible: false }, ()=>getTableData());
  };

  const onRowDeletedClick = (id) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/make_payment/delete?frm=time_charter_in_form`,
      { id: id },
      (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon("error", response.message);
        }
      }
    );
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const tableCol = state.columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  const ClusterDataxAxis = [
    "SOUTH LOYALTY",
    "Stephy",
    "Anr shipping",
    "VERITY",
    "DS SHIPPING",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amount",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const NestedPieChartData = [
    { value: 70, name: "Vessel 1" },
    { value: 30, name: "Vessel 2" },
  ];

  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];

  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000];
  const LineCharxAxisData = [
    "INV-TCI-00176",
    "INV-TCI-00174",
    "INV-TCI-00171",
    "INV-TCI-00170",
    "INV-TCI-00165",
  ];
  const PieChartData = [
    { value: 40, name: "Prepared 40k" },
    { value: 30, name: "Verified 30k" },
    { value: 13, name: "Approved 13k" },
    { value: 10, name: "Posted 10k" },
  ];
  const StackHorizontalChartyAxis = [
    "KING-TCI-0011",
    "KING-TCI-0013",
    "KING-TCI-0015",
    "KING-TCI-0016",
    "KING-TCI-0010",
  ];
  const StackHorizontalChartSeries = [
    {
      name: "Prepared",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599, 555],
    },
    {
      name: "Verified",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 777, 555],
    },
    {
      name: "Approved",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [220, 182, 191, 888, 555],
    },
    {
      name: "Posted",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [210, 282, 491, 288, 355],
    },
  ];
  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Hire Payable List</span>
                  </h4>
                </div>
                {/* <div className="action-btn">
                    <Link to="add-tcov">Add Bunker Invoice</Link>
                  </div> */}
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {state.loading === false ? (
                  <ToolbarUI
                    routeUrl={"hire-payable-toolbar"}
                    optionValue={{
                      pageOptions: state.pageOptions,
                      columns: state.columns,
                      search: state.search,
                    }}
                    showGraph={showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "hirepay"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "hirepay"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "hirepay"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {state.isVisible ? (
          <Modal
            title={"Edit Invoice"}
            open={state.isVisible}
            width="95%"
            onCancel={modalClose}
            // onCancel={onCancel}
            style={{ top: "10px" }}
            bodyStyle={{ height: 790, overflowY: "auto", padding: "0.5rem" }}
            footer={null}
          >
            {
              <MakePayment
                modalCloseEvent={modalClose}
                formData={state.paymentData}
                disableEdit={true}
              />
            }
          </Modal>
        ) : undefined}
        {state.sidebarVisible ? (
          <SidebarColumnFilter
            columns={state.columns}
            sidebarVisible={state.sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}

        <Modal
          title="Hire Payable Dashboard"
          open={state.isGraphModal}
          //  onOk={handleOk}
          width="90%"
          onCancel={handleCancel}
          footer={null}
        >
          <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Vessels</p>
                <p>300</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Amount</p>
                <p>300 $  </p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Invoice</p>
                <p>240</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Vessel Name</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Invoice No</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Account Type</p>
              <Select
                placeholder="Payable"
                optionFilterProp="children"
                options={[
                  {
                    value: "Payable",
                    label: "Payable",
                  },
                  {
                    value: "Receivable",
                    label: "Receivable",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Status</p>
              <Select
                placeholder="PREPARED"
                optionFilterProp="children"
                options={[
                  {
                    value: "PREPARED",
                    label: "PREPARED",
                  },
                  {
                    value: "APPROVED",
                    label: "APPROVED",
                  },
                  {
                    value: "POSTED",
                    label: "POSTED",
                  },
                  {
                    value: "VERIFIED",
                    label: "VERIFIED",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Date To</p>
              <Select
                placeholder="2024-02-18"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-26",
                    label: "2024-01-26",
                  },
                  {
                    value: "2023-11-16",
                    label: "2023-11-16",
                  },
                  {
                    value: "2023-11-17",
                    label: "2023-11-17",
                  },
                ]}
              ></Select>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount Per Vessel"}
                ClusterDataxAxis={ClusterDataxAxis}
                ClusterDataSeries={ClusterDataSeries}
                maxValueyAxis={"350"}
              />
            </Col>
            <Col span={12}>
              <LineChart
                LineCharSeriesData={LineCharSeriesData}
                LineCharxAxisData={LineCharxAxisData}
                Heading={"Total Amount Per Invoice"}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <PieChart
                PieChartData={PieChartData}
                Heading={"Total Inv Amount Per Payment Status"}
              />
            </Col>
            <Col span={12}>
              <StackHorizontalChart
                StackHorizontalChartSeries={StackHorizontalChartSeries}
                StackHorizontalChartyAxis={StackHorizontalChartyAxis}
                Heading={"Total Amount Per TCI ID"}
              />
            </Col>
          </Row>
        </Modal>
      </div>
    </>
  );
};

export default HirePaymentList;
