import React from 'react';
import { Form, Input, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class Filter extends React.Component {
  render() {
    return (
      <div className="body-wrapper">
        <Form>
          <div className="row">
            <div className="col-md-6">
              <FormItem {...formItemLayout} label="From Date">
                <Input type="date" size="default" />
              </FormItem>
            </div>

            <div className="col-md-6">
              <FormItem {...formItemLayout} label="To Date">
                <Input type="date" size="default" />
              </FormItem>
            </div>

            <div className="col-md-6">
              <FormItem {...formItemLayout} label="CP Date From">
                <Input type="date" size="default" />
              </FormItem>
            </div>

            <div className="col-md-6">
              <FormItem {...formItemLayout} label="CP Date To">
                <Input type="date" size="default" />
              </FormItem>
            </div>

            <div className="col-md-12">
              <FormItem {...formItemLayout} label="Company">
                <Select defaultValue="1">
                  <Option value="1">Option</Option>
                  <Option value="2">Option</Option>
                  <Option value="3">Option</Option>
                  <Option value="4">Option</Option>
                </Select>
              </FormItem>
            </div>

            <div className="col-md-12">
              <FormItem {...formItemLayout} label="LOB">
                <Select defaultValue="1">
                  <Option value="1">Option</Option>
                  <Option value="2">Option</Option>
                  <Option value="3">Option</Option>
                  <Option value="4">Option</Option>
                </Select>
              </FormItem>
            </div>

            <div className="col-md-12">
              <FormItem {...formItemLayout} label="Vessel Type">
                <Select defaultValue="1">
                  <Option value="1">Option</Option>
                  <Option value="2">Option</Option>
                  <Option value="3">Option</Option>
                  <Option value="4">Option</Option>
                </Select>
              </FormItem>
            </div>

            <div className="col-md-12">
              <FormItem {...formItemLayout} label="Ops Coordinator">
                <Select defaultValue="1">
                  <Option value="1">Option</Option>
                  <Option value="2">Option</Option>
                  <Option value="3">Option</Option>
                  <Option value="4">Option</Option>
                </Select>
              </FormItem>
            </div>

            <div className="col-md-12">
              <FormItem {...formItemLayout} label="Fleet">
                <Select defaultValue="1">
                  <Option value="1">Option</Option>
                  <Option value="2">Option</Option>
                  <Option value="3">Option</Option>
                  <Option value="4">Option</Option>
                </Select>
              </FormItem>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default Filter;
