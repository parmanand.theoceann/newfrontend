import React, { Component } from 'react';
import { Layout, Drawer, Modal, Row, Col, Badge, Tag, Spin, Alert } from 'antd';
import RightBarUI from '../../components/RightBarUI';
import Filter from './right-panel/Filter';
import HirePaymentList from './right-panel/HirePaymentList';
import HireIssueBillList from './right-panel/HireIssueBillList';
import HireScheduleReport from '../operation-reports/HireScheduleReport';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, ResizeableTitle } from '../../shared';
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
const localizer = momentLocalizer(moment);
const { Content } = Layout;


class HireSchedule extends Component {
  constructor(props) {
    super(props)
    let tableHeaders = [
      {
        title: 'Vessel Name',
        dataIndex: 'vessel_name',
      },
      {
        title: 'Company',
        dataIndex: 'company_name',
      },
      {
        title: 'Due Date',
        dataIndex: 'due_date',
      },
      {
        title: 'Invoice No',
        dataIndex: 'invoice_no',
      },
      {
        title: 'CP Date',
        dataIndex: 'cp_date',
      },
      {
        title: 'Owner Name',
        dataIndex: 'owner_name',
      },
    ]
    this.state = {
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      modals: {
        HireScheduleReport: false,
      },
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      fromToDate: [],
      responseData: [],
      columns: tableHeaders,
      loading: true
    }
  }
  componentDidMount() {
    this.fetchData()
  }

  fetchData = async (search = {}) => {
    var colors = ['red', 'green', 'blue', 'orange', 'yellow', 'purple ', 'pink', 'brown'];
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc = { OR: {} };
        wc['OR'][search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      data: [],
    }, () => {
      this.setState({
        loading: true,
      })
    });

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/make_payment/hire-schedlue-list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;

    let arr = []
    let from_ToDate = []
    if (data && data.data.length > 0) {
      data.data.map((val, index) => {
        let date1 = new Date(val.commence_date && val.commence_date.split(':')[0])
        let date2 = new Date(val.completing_date && val.completing_date.split(':')[0])
        let diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);
        //estimate_id
        let obj = {
          id: val.id,
          start_date: val.commence_date,
          end_date: val.completing_date,
          duration: diffDays,
          vessel_name: val.vessel_name,
          company_name: val.company_name,
          invoice_no: val.invoice_no,
          vessel_id: val.vessel_id,
          owner_name: val.owner_name,
          vessel_type: val.vessel_type_name,
          tc_code: val.tc_code,
          estimate_id: val.estimate_id,
          due_date: val.due_date

        }

        let start_to_end_date = {
          start: moment(val.due_date).toDate(val.due_date),
          end: moment(val.due_date)
            .add(1, "days")
            .toDate(),
          title: val.vessel_name,
          color: '#ff0000'
          // startDate: date1,
          // endDate: addDays(date2, diffDays),
          // color: colors[Math.floor(Math.random() * colors.length)],
          // key: 'selection'
        }
        from_ToDate.push(start_to_end_date)
        arr.push(obj)
      })
    }
    let state = {
      loading: false,
    };
    if (arr.length > 0 && totalRows > this.state.data.length) {
      const arrayUniqueByKey = [...new Map(arr.map(item =>
        [item.estimate_id, item])).values()];
      state['responseData'] = arrayUniqueByKey;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
      fromToDate: from_ToDate
    });
    // this.setState({
    //   data:arr
    // })
  }

  components = {
    header: {
      cell: ResizeableTitle,
    },
  }

  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  }

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.fetchData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    }
  }

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  onClickRightMenu = (key, options) => {
    const { formData } = this.state;
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case 'filter':
        loadComponent = <Filter />;
        break;

      case 'hire-payment-list':
        loadComponent = <HirePaymentList />;
        break;

      case 'hire-issue-bill-list':
        loadComponent = <HireIssueBillList />;
        break;
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };
  eventStyleGetter = (event, start, end, isSelected) => {
    var style = {
      backgroundColor: event.color,
      borderRadius: '0px',
      opacity: 0.8,
      color: 'white',
      border: '0px',
      display: 'block'
    };
    return {
      style: style
    };
  }

  render() {
    const { loadComponent, title, visibleDrawer, fromToDate, loading, responseData, pageOptions, search, columns } = this.state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
          color: '#ffffff'
        }),
      }));
    return (
      <div className="wrap-rightbar full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body">
                      <div className="toolbar-ui-wrapper">
                        <div className="leftsection">
                          <span key="first" className="wrap-bar-menu">
                            <ul className="wrap-bar-ul pt-4">
                              <h4>Hire Schedule</h4>
                            </ul>
                          </span>
                        </div>
                        <div className="rightsection">
                          <span className="wrap-bar-menu">
                            <ul className="wrap-bar-ul pt-4">
                              <li>
                                <span
                                  className="text-bt"
                                  onClick={() => this.showHideModal(true, 'HireScheduleReport')}
                                >
                                  Reports
                                </span>
                              </li>
                            </ul>
                          </span>
                        </div>
                      </div>
                      <hr />
                      <div
                        className="section"
                        style={{
                          width: '100%',
                          marginBottom: '10px',
                          paddingLeft: '15px',
                          paddingRight: '15px',
                        }}
                      >
                        {
                          loading === false ?
                            <ToolbarUI
                              routeUrl={'hire-schedule-list-toolbar'}
                              optionValue={{ pageOptions: pageOptions, columns: this.state.columns, search: search }}
                              callback={e => this.callOptions(e)}
                            />
                            :
                            <Spin>
                              <Alert
                                message="Please wait"
                                description="Loading....."
                                type="info"
                              />
                            </Spin>


                        }
                      </div>
                      <div className="row p10">
                        <div className="col-md-12">
                          <div className="border p-2 wx-default">
                            <Row>
                              <Col span={8} style={{ borderColor: '#adadad' }}>
                                <Row style={{ margin: 20, borderColor: '#adadad' }}>
                                  {columns && columns.length > 0 &&
                                    columns.map((e, i) => {
                                      return (
                                        e.title != 'Due Date' && e.title != 'Invoice No' && e.title != 'CP Date' && e.title != 'Owner Name' &&
                                        <Col key={i} style={{ fontSize: 20, color: '#999999' }} span={i == 0 ? 8 : 12}> {e.title}</Col>

                                      )
                                    })}
                                </Row>
                                {responseData && responseData.length > 0 &&
                                  responseData.map((e, i) => {
                                    return (
                                      <Row style={{ margin: 20 }} key={i}>
                                        <Col span={8}> {e.vessel_name}</Col>
                                        <Col span={12}>{e.company_name}</Col></Row>
                                    )
                                  })}
                              </Col>
                              <Col>
                                <Calendar
                                  localizer={localizer}
                                  defaultDate={new Date()}
                                  defaultView="month"
                                  events={fromToDate}
                                  style={{ height: "100vh" }}
                                  eventPropGetter={this.eventStyleGetter}
                                />
                              </Col>
                            </Row>
                            <Row>
                              <Col span={12}>
                                <Tag color="#28a745">Scheduled</Tag>
                                <Tag color="#01bcd4" >Delivered</Tag>
                                <Tag color="#0726ff" >Redelivered</Tag>
                                <Tag color="#ff0000" >Completed</Tag>
                                <Tag color="#faad14" >Commenced</Tag>
                                <Tag color="#81d742" >Fix</Tag>
                                <Tag color='#fadb14'>Draft</Tag>
                                <Tag color="#9e9e9e" >Default</Tag>
                              </Col>
                              <Col span={8}></Col>
                            </Row>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </article>
              </div>
            </Content>
          </Layout>

          <RightBarUI
            pageTitle="hire-schedule-righttoolbar"
            callback={(data, options) => this.onClickRightMenu(data, options)}
          />
          {loadComponent !== undefined && title !== undefined && visibleDrawer === true ? (
            <Drawer
              title={this.state.title}
              placement="right"
              closable={true}
              onClose={this.onCloseDrawer}
             open={this.state.visibleDrawer}
              getContainer={false}
              style={{ position: 'absolute' }}
              width={this.state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  <div className="content-wrapper noHeight">{this.state.loadComponent}</div>
                </div>
              </div>
            </Drawer>
          ) : (
            undefined
          )}
        </Layout>

        <Modal
          style={{ top: '2%' }}
          title="Report"
         open={this.state.modals['HireScheduleReport']}
          onCancel={() => this.showHideModal(false, 'HireScheduleReport')}
          width="75%"
          footer={null}
        >
          <HireScheduleReport />
        </Modal>
      </div>
    );
  }
}

export default HireSchedule;
