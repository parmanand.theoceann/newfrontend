// import { Layout, Table, Typography, Input, Row, Select, Button, Icon, Col, Divider, Modal, message } from 'antd';
// import React, { Component } from 'react'
// import URL_WITH_VERSION, {
//     objectToQueryStringFunc,
//     getAPICall
// } from '../../shared';
// import Chart from "react-apexcharts";

// const { Content } = Layout;
// const { Title } = Typography;
// const { Option } = Select;

// class FleetPerformance extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             data: [],
//             mapData: null,
//             loading: false,
//             filterVessels: [],
//             columns: [
//                 {
//                     title: 'Date',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'GHG Emit(tCo2)',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'Distance (m) ',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'Cargo (DWT)',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'Cargo Type',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'No of Passage',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 },
//                 {
//                     title: 'No of Vessel',
//                     dataIndex: 'data',
//                     key: 'sn',
//                 }
//             ],

//         }
//     }
//     handleChange = (value) => {
//         // console.log(`selected ${value}`);
//     }
//     render() {
//         return (
//             <div className="tcov-wrapper full-wraps">
//                 <Layout className="layout-wrapper">
//                     <Layout>
//                         <Content className="content-wrapper">
//                             <div className="fieldscroll-wrap">
//                                 <div className="body-wrapper">
//                                     <article className="article">
//                                         <div className="box box-default">
//                                             <div className="box-body common-fields-wrapper">
//                                                 {/* <Title level={4}>Fleet CII performance</Title> */}
//                                                 <table style={{ width: '100%' }}>
//                                                     <tr>
//                                                         <td>
//                                                             <Title level={2}>Fleet CII performance</Title>
//                                                         </td>
//                                                         <td valign='right' style={{ textAlign: 'right' }}>
//                                                             <Input placeholder="Search Vessel Name" style={{ marginTop: 10, marginBottom: 10 }} />
//                                                         </td>
//                                                     </tr>
//                                                 </table>
//                                                 <Divider></Divider>

//                                                 {/* <table style={{ width: '100%' }}>
//                                                     <tbody>
//                                                         <tr>
//                                                             <td>
//                                                                 <Title level={4}>GHG Emission Summary</Title>
//                                                             </td>
//                                                         </tr>
//                                                         <tr>
//                                                             <td>
//                                                                 <label>Date Range</label><br />
//                                                                 <Select defaultValue="q1" style={{ width: 220 }} onChange={this.handleChange}>
//                                                                     <Option value="q1">Q1</Option>
//                                                                     <Option value="q2">Q2</Option>
//                                                                     <Option value="q3">Q3</Option>
//                                                                     <Option value="q4">Q4</Option>
//                                                                 </Select>
//                                                             </td>
//                                                             <td style={{ float: 'right' }}>
//                                                                 <label>&nbsp;</label><br />
//                                                                 <Button type="primary">Print</Button>
//                                                             </td>
//                                                         </tr>
//                                                     </tbody>
//                                                 </table> */}

//                                                 <Table
//                                                     bordered
//                                                     columns={this.state.columns}
//                                                     dataSource={[]}
//                                                     scroll={{
//                                                         x: 1800,
//                                                         y: 400
//                                                     }}
//                                                     className="inlineTable resizeableTable"
//                                                     size="small"
//                                                     rowClassName={(r, i) =>
//                                                         i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
//                                                     }
//                                                     style={{ marginTop: 30 }}
//                                                 />

//                                                 <Chart
//                                                     options={{
//                                                         chart: {
//                                                             id: "basic-bar"
//                                                         },
//                                                         xaxis: {
//                                                             categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
//                                                         }
//                                                     }}
//                                                     series={[
//                                                         {
//                                                             name: "series-1",
//                                                             data: [30, 40, 45, 50, 49, 60, 70, 91]
//                                                         }
//                                                     ]}
//                                                     type="bar"
//                                                     width="100%"
//                                                 />
//                                             </div>
//                                         </div>
//                                     </article>
//                                 </div>
//                             </div>
//                         </Content>
//                     </Layout>
//                 </Layout>
//             </div>
//         )
//     }
// }
// export default FleetPerformance;
