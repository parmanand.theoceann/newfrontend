// import React, { Component } from 'react';
// import { Table, Modal } from 'antd';
// import CreateMainAccountCategoryModal from './CreateMainAccountCategoryModal';

// const columns = [
//   {
//     title: 'Short Code',
//     dataIndex: 'short_code',
//   },

//   {
//     title: 'Bank Name',
//     dataIndex: 'bank_name',
//   },
//   {
//     title: 'Bank Descrpition',
//     dataIndex: 'bank_description',
//   },
// ];
// const data = [
//   {
//     key: '1',
//     short_code: 'PORTX',
//     bank_name: 'Port Bank of India',
//     bank_description: 'Agency Communication',
//   },
//   {
//     key: '2',
//     short_code: 'PORTZ',
//     bank_name: 'Port Bank of China',
//     bank_description: 'Agency Communication',
//   },
// ];

import React, { Component } from 'react';
import { Modal } from 'antd';
import NormalFormIndex from '../../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, { postAPICall, getAPICall, openNotificationWithIcon } from '../../../shared';

class BankNames extends Component {
  constructor(props) {
    super(props)
    this.state = {
      frmName: "bank_names_form", // "vessel_attachment_form",
      formData: {},
      loadForm: false,
      oldFormData: this.props.frmData || {},
      isUpdate: false,
      indexNumber: 0,
    }
  }

  componentDidMount = () => {
    const { oldFormData } = this.state;
    // let vessel_form_id = oldFormData.id;
    this.prepaireFormData('vessel_form_id')
  }

  prepaireFormData = async (vessel_form_id) => {
    // if (vessel_form_id) {
    //   const response = await getAPICall(`${URL_WITH_VERSION}/vessel/vesselattachment/edit?e=${vessel_form_id}`);
    //   const respData = await response['data'];
    //   if (respData) {
    //     this.setState({ formData: respData, loadForm: true, isUpdate: true })
    //   } else {
    //     this.setState({ loadForm: true, isUpdate: false })
    //   }
    // }
  }

  saveFormData = (data) => {
    const { frmName, oldFormData, imageFile } = this.state;
    let vessel_form_id = oldFormData.id;
    if (imageFile && imageFile.length > 0) {
      imageFile.map(val => {
        data['..'].map((e, index) => {
          if (val.indexNumber === index) {
            e['attachment'] = val.fileName
          }
          return true;
        })
      })
    }
    if (data['..'] && data['..'].length > 0) {
      data['..'].map(e => {
        if (!e['vessel_id']) { e['vessel_id'] = vessel_form_id }
        delete e.editable
        delete e.ocd
        delete e.id
        delete e.index
        delete e.key
      })
    }
    let suURL = `${URL_WITH_VERSION}/vessel/vesselattachment/save?frm=${frmName}`;
    let suMethod = 'POST';
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon('success', data.message);
        if (vessel_form_id) {
          this.setState({ loadForm: false })
          this.prepaireFormData(vessel_form_id);
        }
      } else {
        openNotificationWithIcon('error', data.message)
      }
    });
  }

  updateFormData = (data) => {
    const { frmName, oldFormData, imageFile } = this.state;
    let vessel_form_id = oldFormData.id;
    if (imageFile && imageFile.length > 0) {
      imageFile.map(val => {
        data['..'].map((e, index) => {
          if (val.indexNumber === index) {
            e['attachment'] = val.fileName
            e['share_link'] = val.url
          }
          return true;
        })
      })
    }
    if (data['..'] && data['..'].length > 0) {
      data['..'].map(e => {
        if (!e['vessel_id']) { e['vessel_id'] = vessel_form_id }
        //if (e.id < 0) { delete e.id }
        delete e.editable
        delete e.ocd
        delete e.index
        delete e.key
      })
    }

    let suURL = `${URL_WITH_VERSION}/vessel/vesselattachment/update?frm=${frmName}`;
    let suMethod = 'PUT';
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon('success', data.message);
        if (vessel_form_id) {
          this.setState({ loadForm: false })
          this.prepaireFormData(vessel_form_id);
        }
      } else {
        openNotificationWithIcon('error', data.message)
      }
    });
  }

  deleteTableData = async (id) => {
    const { oldFormData } = this.state;
    let vessel_form_id = oldFormData.id;
    let delete_data = {
      'id': id
    }
    postAPICall(`${URL_WITH_VERSION}/vessel/vesselattachment/delete`, delete_data, 'delete', response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.prepaireFormData(vessel_form_id);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  }
  onClickExtraIcon = async (action, data) => {
    if (Math.sign(data.id) > 0) {
      this.deleteTableData(data.id)
    }
  }

  render() {
    const { frmName, formData, isUpdate, loadForm, visibleVesselAttachment, indexNumber } = this.state;
    return (
      <>
        {loadForm &&
          <NormalFormIndex
            key={'key_' + frmName + '_0'}
            formClass="label-min-height"
            formData={formData}
            frmCode={'vessel_attachment_form'}
            showForm={true}
            addForm={true}
            showToolbar={[{
              isLeftBtn: [
                {
                  key: 's1',
                  isSets: [
                    { id: '1', key: 'save', type: isUpdate === true ? 'save' : 'save', withText: '', "event": (event, data) => { isUpdate === true ? this.updateFormData(data) : this.saveFormData(data) } },
                    { id: '2', key: 'share', type: 'share-alt', withText: '', "event": (event, data) => { this.openShareView(data) } }
                  ]
                }
              ],
              isRightBtn: [{ key: 's1', isSets: [] }],
              isResetOption: false,
            },
            ]}
            extraTableButton={{
              "..": [{ "icon": "upload", "onClickAction": (action, data) => { this.onUploadVesselDoc(action, data) } }],
            }}
            tableRowDeleteAction={(action, data) => this.onClickExtraIcon(action, data)}
            inlineLayout={true}
          />
        }
      </>
    );
  }
}

export default BankNames;
