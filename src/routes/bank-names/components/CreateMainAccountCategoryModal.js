import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Short Code',
    dataIndex: 'short_code',
  },
  {
    title: 'Bank Name',
    dataIndex: 'bank_name',
  },
  {
    title: 'Bank Description',
    dataIndex: 'bank_description',
  },
];
const data = [
  {
    key: '1',
    bank_name: 'PORTX',
    bank_description: 'Port expenses',
  },
  {
    key: '2',
    bank_name: 'FINV',
    bank_description: 'Freight Invoice',
  },

];

class CreateMainAccountCategoryModal extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                    rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default CreateMainAccountCategoryModal;
