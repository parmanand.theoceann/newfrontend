import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Vessel Name',
    dataIndex: 'name',
  },

  {
    title: 'Vessel Type',
    dataIndex: 'v_type',
  },

  {
    title: 'DWT',
    dataIndex: 'dwt',
  },

  {
    title: 'Checkbox',
    dataIndex: 'check',
  },
];
const data = [
  {
    key: '1',
    name: 'Vessel Name',
    v_type: 'Vessel Type',
    dwt: 'DWT',
    check: 'Checkbox',
  },

  {
    key: '2',
    name: 'Vessel Name',
    v_type: 'Vessel Type',
    dwt: 'DWT',
    check: 'Checkbox',
  },

  {
    key: '3',
    name: 'Vessel Name',
    v_type: 'Vessel Type',
    dwt: 'DWT',
    check: 'Checkbox',
  },

  {
    key: '4',
    name: 'Vessel Name',
    v_type: 'Vessel Type',
    dwt: 'DWT',
    check: 'Checkbox',
  },

  {
    key: '5',
    name: 'Vessel Name',
    v_type: 'Vessel Type',
    dwt: 'DWT',
    check: 'Checkbox',
  },
];

class SelectVessel extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
              <div className="row p10">
                <div className="col-md-12">
                  <div className="action-btn text-right">
                    <Button className="ant-btn ant-btn-primary">Submit</Button>
                    <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default SelectVessel;
