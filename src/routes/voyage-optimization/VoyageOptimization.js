import React, { Component } from "react";
import { Modal, Table } from "antd";
import {
  CodeSandboxOutlined,
  LeftCircleOutlined,
  LineChartOutlined,
  SelectOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import VoyageDetailModal from "./VoyageDetailModal";
import { IMAGE_PATH } from "../../shared";
import SelectVessel from "./SelectVessel";
// import BarChart from './BarChart';
import PieChart from "./PieChart";
// import SideNavdVspm from "../dynamic-vspm/sidenavdvspm";
import {
  ProfileOutlined,
  FileProtectOutlined,
  RiseOutlined,
  HistoryOutlined,
  AppstoreOutlined,
  MonitorOutlined,
  FileExcelOutlined,
  ContainerOutlined,
} from "@ant-design/icons";
import DynamicMap from "../dynamic-vspm/DynamicMap";
import { DatePicker, Input, Select } from "antd";

import GaugeChart from "../dashboard/charts/GaugeChart";
import BarChart from "../dashboard/dcharts/Barchart";

const { Column, ColumnGroup } = Table;

const data = [
  {
    key: "1",
    vessel: "John",
    vesseltype: "Brown",
    passage: 32,
    rpm: "RPM",
    spd: "SPD",
    consmt: "Cons. (MT)",
    ttlfreight: "Ttl Freight",
    tced: "Tce $",
    tcedays: "Tce days",
    netprofit: "Net Profit",
    profitdays: "Profi days",
    asvsog: "Asv SOG",
    voyagedays: "Voyage days",
    avemecons: "Ave me cons.",
    ttlfuelcons: "Ttl fuel cons",
    ttlfuelcostusd: "Ttl fuel cost usd",
  },

  {
    key: "2",
    vessel: "John",
    vesseltype: "Brown",
    passage: 32,
    rpm: "RPM",
    spd: "SPD",
    consmt: "Cons. (MT)",
    ttlfreight: "Ttl Freight",
    tced: "Tce $",
    tcedays: "Tce days",
    netprofit: "Net Profit",
    profitdays: "Profi days",
    asvsog: "Asv SOG",
    voyagedays: "Voyage days",
    avemecons: "Ave me cons.",
    ttlfuelcons: "Ttl fuel cons",
    ttlfuelcostusd: "Ttl fuel cost usd",
  },

  {
    key: "3",
    vessel: "John",
    vesseltype: "Brown",
    passage: 32,
    rpm: "RPM",
    spd: "SPD",
    consmt: "Cons. (MT)",
    ttlfreight: "Ttl Freight",
    tced: "Tce $",
    tcedays: "Tce days",
    netprofit: "Net Profit",
    profitdays: "Profi days",
    asvsog: "Asv SOG",
    voyagedays: "Voyage days",
    avemecons: "Ave me cons.",
    ttlfuelcons: "Ttl fuel cons",
    ttlfuelcostusd: "Ttl fuel cost usd",
  },

  {
    key: "4",
    vessel: "John",
    vesseltype: "Brown",
    passage: 32,
    rpm: "RPM",
    spd: "SPD",
    consmt: "Cons. (MT)",
    ttlfreight: "Ttl Freight",
    tced: "Tce $",
    tcedays: "Tce days",
    netprofit: "Net Profit",
    profitdays: "Profi days",
    asvsog: "Asv SOG",
    voyagedays: "Voyage days",
    avemecons: "Ave me cons.",
    ttlfuelcons: "Ttl fuel cons",
    ttlfuelcostusd: "Ttl fuel cost usd",
  },

  {
    key: "5",
    vessel: "John",
    vesseltype: "Brown",
    passage: 32,
    rpm: "RPM",
    spd: "SPD",
    consmt: "Cons. (MT)",
    ttlfreight: "Ttl Freight",
    tced: "Tce $",
    tcedays: "Tce days",
    netprofit: "Net Profit",
    profitdays: "Profi days",
    asvsog: "Asv SOG",
    voyagedays: "Voyage days",
    avemecons: "Ave me cons.",
    ttlfuelcons: "Ttl fuel cons",
    ttlfuelcostusd: "Ttl fuel cost usd",
  },
];

class VoyageOptimization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modals: {
        VoyageDetail: false,
        SelectVessel: false,
        ShipDetails1: false,
      },
      filterData: {
        commence_date: "", // initial state
        completed_date: "", // initial state
      },
      title: `vessel Name:VSL1412211\nlat:18.5204\nlng:73.8567`,
      latlng: [],
      shipData: null,
      vessel_name: "Spring",
      loading: false,
      isShowVesselModal: false,
      isShowVesselCiiModal: false,
      isShowVesselDetailsModal: false,
    };
    this.onChange = this.onChange.bind(this);
  }

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };
  onChange = (value, dateString) => {
    this.setState((prevState) => ({
      ...prevState,
      filterData: {
        ...prevState.filterData,
        commence_date: dateString[0],
        completed_date: dateString[1],
      },
    }));
  };

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <div className="box box-default" style={{ padding: "16px" }}>
          <div className="row" style={{ alignItems: "start" }}>
            <div className="col-md-1" style={{ height: "fit-content" }}>
              {/* <SideNavdVspm /> */}
            </div>
            <div className="col-md-11">
              <article className="article toolbaruiWrapper">
                <div className="box box-default">
                  <div className="box-body">
                    <div className="row">
                      <div className="col-md-12 mb-3">
                        <div className="form-heading">
                          <h4 className="title">
                            <span className="float-right">
                              <a
                                href="#/dynamic-vspm"
                                className="btn ant-btn-primary pl-2 pr-2 pt-1 pb-1"
                              >
                                <LeftCircleOutlined />
                              </a>
                            </span>
                          </h4>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-7">
                        <div className="google-map">
                          <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41756662.22592472!2d80.00244695!3d-29.91602475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x18174db2727f6a1f%3A0xead4bf5063a99637!2sIndian%20Ocean!5e1!3m2!1sen!2sin!4v1628574367248!5m2!1sen!2sin"
                            width="100%"
                            height="500"
                            className="border"
                            loading="lazy"
                            title="myframe"
                          ></iframe>
                          <div className="googlemap-voyage-menu">
                            <ul>
                              <li>
                                <a
                                  href={{}}
                                  title="Voyage details"
                                  onClick={() =>
                                    this.showHideModal(true, "VoyageDetail")
                                  }
                                >
                                  <HomeOutlined />
                                </a>
                              </li>
                              <li>
                                <a
                                  href={{}}
                                  title="Select vessel"
                                  onClick={() =>
                                    this.showHideModal(true, "SelectVessel")
                                  }
                                >
                                  <SelectOutlined />
                                </a>
                              </li>
                              <li>
                                <a title="Analytics" href={{}}>
                                  <CodeSandboxOutlined />
                                </a>
                              </li>
                              <li>
                                <a title="Compare" href={{}}>
                                  <LineChartOutlined />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-5">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="border mb-3 p-2 text-center rounded">
                              <h3>
                                <b>49.1</b>{" "}
                                <small>
                                  <sub>MT</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Average cons. Per day</p>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="border mb-3 p-2 text-center rounded">
                              <h3>
                                <b>15.7</b>{" "}
                                <small>
                                  <sub>kt</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Aver Recomd spd per day</p>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="border mb-3 p-2 text-center rounded">
                              <h3>
                                <b>1807.7</b>{" "}
                                <small>
                                  <sub>MT</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Ave RPM per day</p>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="border mb-3 p-2 text-center rounded">
                              <h3>
                                <b>116.4</b>{" "}
                                <small>
                                  <sub>kt</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Total cons. for leg</p>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="border mb-3 p-2 text-center rounded">
                              <h3>
                                <b>88.6</b>{" "}
                                <small>
                                  <sub>%</sub>
                                </small>
                              </h3>
                              <p className="mb-0">ME load</p>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">
                            <BarChart />
                          </div>
                          <div className="col-md-6">
                            <PieChart />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row p10">
                      <div className="col-md-12">
                        <div className="table-voyage-optimization mt-3">
                          <Table
                            bordered
                            dataSource={data}
                            pagination={false}
                            footer={false}
                            scroll={{ x: "max-content" }}
                            rowClassName={(r, i) =>
                              i % 2 === 0
                                ? "table-striped-listing"
                                : "dull-color table-striped-listing"
                            }
                          >
                            <Column title="Vessel" dataIndex="vessel" />
                            <Column
                              title="Vessel Type"
                              dataIndex="vesseltype"
                            />
                            <Column title="Passage" dataIndex="passage" />
                            <ColumnGroup
                              title="Recommend"
                              className="bg-recommended"
                            >
                              <Column title="RPM" dataIndex="rpm" />
                              <Column title="SPD" dataIndex="spd" />
                              <Column title="Cons.. (MT)" dataIndex="consmt" />
                            </ColumnGroup>
                            <ColumnGroup
                              title="Overall Voyage"
                              className="overall-bg"
                            >
                              <Column
                                title="Ttl freight"
                                dataIndex="ttlfreight"
                              />
                              <Column title="TCE $" dataIndex="tced" />
                              <Column title="TCE/days" dataIndex="tcedays" />
                              <Column
                                title="Net profit"
                                dataIndex="netprofit"
                              />
                              <Column
                                title="Profit per/days"
                                dataIndex="profitdays"
                              />
                              <Column
                                title="Voyage days"
                                dataIndex="voyagedays"
                              />
                            </ColumnGroup>
                            <ColumnGroup title="Fuel Perf." className="fuel-bg">
                              <Column title="Asv SOG" dataIndex="asvsog" />
                              <Column
                                title="Ave ME cons/days"
                                dataIndex="avemecons"
                              />
                              <Column
                                title="TTL fuel cons."
                                dataIndex="ttlfuelcons"
                              />
                              <Column
                                title="TTL fuel cost $"
                                dataIndex="ttlfuelcostusd"
                              />
                            </ColumnGroup>
                          </Table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12">
                    <DynamicMap />
                  </div>
                </div>
                <div className="below-map">
                  <div className="container">
                    <div className="row">
                      {/* Cards section on the left */}
                      <div className="col-lg-6">
                        <div className="row mt-4 justify-content-center">
                          {" "}
                          {/* Added justify-content-center class */}
                          <div className="col-md-12">
                            <div
                              className="border mb-3 p-2 text-center rounded"
                              style={{ width: "100%" }}
                            >
                              <h3>
                                <b>51</b>{" "}
                                <small>
                                  <sub>Vessels</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Total number of vessels</p>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div
                              className="border mb-3 p-2 text-center rounded"
                              style={{ width: "100%" }}
                            >
                              <h3>
                                <b>15.7</b>{" "}
                                <small>
                                  <sub>MT</sub>
                                </small>
                              </h3>
                              <p className="mb-0">
                                Total estd. fuel consumption
                              </p>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div
                              className="border mb-3 p-2 text-center rounded"
                              style={{ width: "100%" }}
                            >
                              <h3>
                                <b>1.34</b>{" "}
                                <small>
                                  <sub>Mil dollars</sub>
                                </small>
                              </h3>
                              <p className="mb-0">Total estd. profit</p>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Filter section on the right */}
                      <div className="col-lg-6 d-flex justify-content-end">
                        <h4
                          className="Char-dashboard mb-10"
                          style={{ margin: "20px", color: "#0d6efd" }}
                        >
                          Filter
                        </h4>{" "}
                        {/* Added mb-3 class */}
                        <div
                          className="card-group p-0 border-0 rounded-0"
                          style={{ flexDirection: "column" }}
                        >
                          <div
                            className="card p-0 border-0 rounded-0"
                            style={{ width: "310px" }}
                          >
                            <div className="card-body border-0 rounded-0 pl-0">
                              {/* Filter inputs */}
                              <Input
                                size="default"
                                placeholder="Vessel Name"
                                className="mb-10"
                                style={{ margin: "50px" }}
                              />{" "}
                              {/* Added mb-3 class */}
                              <Input
                                size="default"
                                placeholder="Voyage No"
                                style={{ margin: "50px" }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div>
                    <div className="row">
                      {/* <div className="col-md-6">
                      <BarChart />
                    </div> */}
                      <div className="col-md-6">
                        <GaugeChart
                          value={18.03}
                          maxValue={35.5}
                          Heading="Total fuel savings"
                          style={{ padding: "20px" }}
                        />
                      </div>
                      <div className="col-md-6">
                        <GaugeChart
                          value={5.11}
                          maxValue={10}
                          Heading="Total cost savings"
                        />
                      </div>

                      <div className="row justify-content-center">
                        <div className="col-md-6">
                          <PieChart />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="table-voyage-optimization mt-3">
                      <Table
                        bordered
                        dataSource={data}
                        pagination={false}
                        footer={false}
                        scroll={{ x: "max-content" }}
                        rowClassName={(r, i) =>
                          i % 2 === 0
                            ? "table-striped-listing"
                            : "dull-color table-striped-listing"
                        }
                      >
                        <Column title="Vessel" dataIndex="vessel" />
                        <Column
                          title="voyage number"
                          dataIndex="voyage-number"
                        />
                        <Column title="Passage" dataIndex="passage" />
                        <ColumnGroup
                          title="overall voyage"
                          className="overall-voyage"
                        >
                          <Column title="Ttl revenue" dataIndex="ttl-revenue" />
                          <Column title="Ttl profit" dataIndex="ttl-profit" />
                          <Column title="TCE" dataIndex="tce" />
                          <Column title="Voyage days" dataIndex="voyage-days" />
                          <Column title="Avg. speed" dataIndex="avg-speed" />
                          <Column
                            title="Ttl consumption"
                            dataIndex="ttl-consumption"
                          />
                          <Column
                            title="Ttl fuel cost"
                            dataIndex="ttl-fuel-cost"
                          />
                        </ColumnGroup>

                        <ColumnGroup
                          title="Recommended"
                          className="recommended"
                        >
                          <Column title="Speed" dataIndex="speed" />
                        </ColumnGroup>

                        <ColumnGroup
                          title="Optimized result"
                          className="Optimized-result"
                        >
                          <Column title="Ttl profit" dataIndex="ttl-profit" />
                          <Column title="TCE" dataIndex="tce" />
                          <Column
                            title="TTL fuel cons."
                            dataIndex="ttlfuelcons"
                          />
                          <Column
                            title="TTL fuel cost $"
                            dataIndex="ttlfuelcostusd"
                          />
                        </ColumnGroup>
                      </Table>
                    </div>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VoyageOptimization;
