import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';

let bar3 = {};

bar3.option = {
  title: {
    text: 'Spd type /Fuel cons.',
  },
  tooltip: {
    trigger: 'axis',
  },

  calculable: true,
  xAxis: [
    {
      type: 'value',
      boundaryGap: [0, 0.01],
      axisLabel: {
        textStyle: {
          color: CHARTCONFIG.color.text,
        },
      },
      splitLine: {
        lineStyle: {
          color: CHARTCONFIG.color.splitLine,
        },
      },
    },
  ],
  yAxis: [
    {
      type: 'category',
      data: ['ECO', 'ECO2', 'ECO', 'C/P'],
      axisLabel: {
        textStyle: {
          color: CHARTCONFIG.color.text,
        },
      },
      splitLine: {
        lineStyle: {
          color: CHARTCONFIG.color.splitLine,
        },
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: CHARTCONFIG.color.splitArea,
        },
      },
    },
  ],
  series: [
    {
      type: 'bar',
      data: [93, 23, 34, 970],
    },
  ],
};

const Bar3 = () => (
  <div className="box box-default mb-4 chart-height">
    <div className="box-body">
      <ReactEcharts option={bar3.option} theme={'macarons'} />
    </div>
  </div>
);

export default Bar3;
