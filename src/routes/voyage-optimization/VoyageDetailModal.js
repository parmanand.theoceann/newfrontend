import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
class VoyageDetailModal extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="TtL freight">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Leg">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Ballast port">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Load port">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Disport">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Distance">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                </div>
                <div className="row p10">
                  <div className="col-md-12">
                    <div className="action-btn">
                      <Button className="ant-btn ant-btn-primary">Submit</Button>
                      <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VoyageDetailModal;
