import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

let pie2 = {};

pie2.option = {
  title: {
    text: 'Cons. breakdown',
    x: 'left',
  },
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)',
  },

  calculable: true,
  series: [
    {
      name: 'Cons. breakdown',
      type: 'pie',
      radius: ['50%', '70%'],
      itemStyle: {
        normal: {
          label: {
            show: true,
          },
          labelLine: {
            show: true,
          },
        },
      },
      data: [
        { value: 335, name: 'ME' },
        { value: 310, name: 'Boiler' },
        { value: 234, name: 'AE' },
      ],
    },
  ],
};

const Chart = () => (
  <div className="box box-default chart-height mb-4 ">
    <div className="box-body">
      <ReactEcharts option={pie2.option} theme={'macarons'} />
    </div>
  </div>
);

export default Chart;
