import React, {  useEffect } from 'react';
import { Form, Select, Layout, Drawer, Modal, Row, Col, Tag } from 'antd';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, useStateCallback } from '../../shared';
import RightBarUI from '../../components/RightBarUI';
import VesselScheduleReport from '../operation-reports/VesselScheduleReport';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
const localizer = momentLocalizer(moment);

const FormItem = Form.Item;
const Option = Select.Option;
const { Content } = Layout;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const scales = [
  { unit: 'month', step: 1, format: 'MMMM yyy' },
  { unit: 'day', step: 1, format: 'd' },
];

const columns = [
  { name: 'vessel_name', label: 'Vessel', width: '100%' },
  { name: 'my_company_lob_name', label: 'Company', width: '100%' },
];

const tasks = [
  {
    id: 1,
    start_date: '2020-11-27',
    duration: 8,
    text: 'MV CALYPSO',
    company: 'ABC',
  },
  {
    id: 2,
    start_date: '2020-11-06',
    duration: 4,
    text: 'NANOS',
    company: 'DEFG',
  },

  {
    id: 3,
    start_date: '2020-12-06',
    duration: 2,
    text: 'NANOS',
    company: 'XYZ',
  },

  {
    id: 4,
    start_date: '2020-11-04',
    duration: 3,
    text: 'NANOS',
    company: 'DEFG',
  },
];

const links = [{ source: 2, target: 1, type: 0 }];

const VesselOpenSchedule = () => {
  let tableHeaders = [
    {
      title: 'Vessel Name',
      dataIndex: 'vessel_name',
    },
    {
      title: 'Company',
      dataIndex: 'my_company_lob_name',
    },
    {
      title: 'Status',
      dataIndex: 'vm_status',
    },
    {
      title: 'Vessel Type',
      dataIndex: 'vessel_type',
    },
  ]

  const [state,setState] = useStateCallback({
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    modals: {
      VesselScheduleReport: false,
    },
    responseData: [],
    loadingComp: false,
    columns: [],
    fromToDate: []
  })

  useEffect(() => {
    fetchData();
  }, [])

  const fetchData = async (search = {}) => {
    try {
      const { pageOptions } = state;
      let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
      let headers = { order_by: { id: 'desc' } };
  
      if (
        search &&
        search.hasOwnProperty('searchValue') &&
        search.hasOwnProperty('searchOptions') &&
        search['searchOptions'] !== '' &&
        search['searchValue'] !== ''
      ) {
        let wc = {};
        if (search['searchOptions'].indexOf(';') > 0) {
          let so = search['searchOptions'].split(';');
          wc = { OR: {} };
          so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
        } else {
          wc = { OR: {} };
          wc['OR'][search['searchOptions']] = { l: search['searchValue'] };
        }
  
        headers['where'] = wc;
      }
  
      setState(prevState => ({ ...prevState, data: [], loadingComp: true }))
  
      let qParamString = objectToQueryStringFunc(qParams);
      let _url = `${URL_WITH_VERSION}/vessel/vessel_schedule?${qParamString}`;
      const response = await getAPICall(_url, headers);
      const data = await response;
      const totalRows = data && data.total_rows ? data.total_rows : 0;
  
      let arr = []
      let from_ToDate = []
      if (data && data.data.length > 0) {
        data.data.map((val, index) => {
          let date1 = new Date(val.commence_date && val.commence_date.split(':')[0])
          let date2 = new Date(val.completing_date && val.completing_date.split(':')[0])
          let diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);
          let obj = {
            id: val.id,
            start_date: val.commence_date && val.commence_date.split(':')[0],
            duration: diffDays,
            vessel_name: val.vessel_name,
            my_company_lob_name: val.my_company_lob_name,
            vessel_code: val.vessel_code,
            vessel_id: val.vessel_id,
            vessel_type: val.vessel_type,
            status_name: val.status_name
          }
  
          let start_to_end_date = {
            start: moment(val.commence_date).toDate(val.completing_date),
            end: moment(val.completing_date)
              .add(diffDays, "days")
              .toDate(),
            title: val.vessel_name,
            color: val.status_name == 'SCHEDULED' ? '#28a745' : val.status_name === 'DELIVERED' ? '#01bcd4'
              : val.status_name === 'REDELIVERED' ? '#0726ff' : val.status_name === 'COMPLETED' ? '#ff0000'
                : val.status_name === 'FIX' ? '#81d742' : val.status_name == 'COMMENCED' ? '#faad14' : val.status_name == 'DRAFT' ? '#fadb14' : '#9e9e9e'
          }
          from_ToDate.push(start_to_end_date)
          arr.push(obj)
        })
      }
      let _state = {
        loadingComp: false,
      };
      if (arr.length > 0 && totalRows > 0) {
        _state['responseData'] = arr;
      }

  
      setState(prevState => ({
        ...prevState,
        ..._state,
        pageOptions: {
          pageIndex: pageOptions.pageIndex,
          pageLimit: pageOptions.pageLimit,
          totalRows: totalRows,
        },
        loadingComp: false,
        fromToDate: from_ToDate
      }))
  
      // setState({
      //   data:arr
      // })
    }catch(err) {
      console.log(err);
    }
  }

  const callOptionsComp = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;

      setState(prevState => ({ ...prevState, search: search, pageOptions: pageOptions }),()=>{
        fetchData(evt);
      })

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = state.pageOptions;
      pageOptions['pageIndex'] = 1;
      
      setState(prevState => ({ ...prevState, search: {}, pageOptions: pageOptions }),()=>{
        fetchData();
      })

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let data = state.data;
      let columns = Object.assign([], state.columns);;

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      
      setState(prevState => ({ 
        ...prevState,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
        }))

    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      setState(prevState => ({ ...prevState,pageOptions: pageOptions }),()=>{
        fetchData();
      })
    }
  };


  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {};
    _modal[modal] = visible;
    
    setState(prevState => ({ 
      ...prevState, 
      modals: Object.assign(modals, _modal),
     }))

  };

  const onCloseDrawer = () =>
    {
      setState(prevState => ({ 
        ...prevState, 
              visibleDrawer: false,
        title: undefined,
        loadComponent: undefined,
       }))
    }
    
  const onClickRightMenu = (key, options) => {
    onCloseDrawer();
    let loadComponent = undefined;
    switch (
    key
    //   case 'search':
    //     loadComponent = <RightSearch />;
    //     break;

    ) {
    }

    setState(prevState => ({ 
      ...prevState, 
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
     }))
  };

  const eventStyleGetter = (event, start, end, isSelected) => {
    var backgroundColor = event.color;
    var style = {
      backgroundColor: backgroundColor,
      borderRadius: '0px',
      opacity: 0.8,
      color: 'black',
      border: '0px',
      display: 'block'
    };
    return {
      style: style
    };
  }

  const { loadComponent, title, visibleDrawer, loadingComp, pageOptions, search, fromToDate, responseData, columns } = state;
  return (
    <div className="tcov-wrapper full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">

                    <div className="toolbar-ui-wrapper">
                      <div className="leftsection">
                        <span key="first" className="wrap-bar-menu">
                          <ul className="wrap-bar-ul">
                            <h4>Vessel Open Schedule</h4>
                          </ul>
                        </span>
                      </div>
                      <div className="rightsection">
                        <span className="wrap-bar-menu">
                          <ul className="wrap-bar-ul">

                            <li>
                              <span
                                className="text-bt"
                                onClick={() => showHideModal(true, 'VesselScheduleReport')}
                              >
                                Reports
                              </span>
                            </li>
                          </ul>
                        </span>
                      </div>
                    </div>
                    <hr />

                    {/* <div className="row p10">
                        <div className="col-md-12">
                          <Form>
                            <div className="row">
                              <div className="col-md-3">
                                <FormItem {...formItemLayout} label="Date From/TO">
                                  <Input type="date" size="default" />
                                </FormItem>
                              </div>
                              <div className="col-md-3">
                                <FormItem {...formItemLayout} label="Vessel Name">
                                  <Input size="default" />
                                </FormItem>
                              </div>
                              <div className="col-md-3">
                                <FormItem {...formItemLayout} label="Vessel Type">
                                  <Input size="default" />
                                </FormItem>
                              </div>
                              <div className="col-md-3">
                                <FormItem {...formItemLayout} label="Status">
                                  <Select defaultValue="1">
                                    <Option value="1">FIXED</Option>
                                    <Option value="2">Schedule</Option>
                                    <Option value="3">Delivered</Option>
                                    <Option value="4">Commence</Option>
                                    <Option value="5">Completed</Option>
                                    <Option value="6">TCI</Option>
                                    <Option value="7">TCO</Option>
                                    <Option value="8">VCI-VCO</Option>
                                  </Select>
                                </FormItem>
                              </div>
                            </div>
                          </Form>
                        </div>
                      </div> */}
                    <div
                      className="section"
                      style={{
                        width: '100%',
                        marginBottom: '10px',
                        paddingLeft: '15px',
                        paddingRight: '15px',
                      }}
                    >
                      {
                        loadingComp === false ?
                          <ToolbarUI
                            routeUrl={'Vessel-schedule-list-toolbar'}
                            optionValue={{ pageOptions: pageOptions, columns: state.columns, search: search }}
                            callback={e => callOptionsComp(e)}
                          />
                          :
                          undefined
                      }
                    </div>
                    <div className="row p10">
                      <div className="col-md-12">
                        <div className="border p-2 wx-default">
                          {/* <Gantt scales={scales} columns={columns} tasks={state.data} links={links} /> */}
                          <Row>
                            <Col span={8} style={{ borderColor: '#adadad' }}>
                              <Row style={{ margin: 20, borderColor: '#adadad' }}>
                                {columns && columns.length > 0 &&
                                  columns.map((e, i) => {
                                    return (
                                      e.Status != 'Status' && e.vessel_type != 'Vessel Type' &&
                                      <Col key={i} style={{ fontSize: 20, color: '#999999' }} span={i == 0 ? 8 : 12}> {e.title}</Col>

                                    )
                                  })}
                              </Row>
                              {responseData && responseData.length > 0 &&
                                responseData.map((val, i) => {
                                  return (
                                    <Row style={{ margin: 20 }} key={i}>
                                      <Col span={8}><Tag color={val.status_name == 'SCHEDULED' ? '#28a745' : val.status_name === 'DELIVERED' ? '#01bcd4'
                                        : val.status_name === 'REDELIVERED' ? '#0726ff' : val.status_name === 'COMPLETED' ? '#ff0000'
                                          : val.status_name === 'FIX' ? '#81d742' : val.status_name == 'COMMENCED' ? '#faad14' : val.status_name == 'DRAFT' ? '#fadb14' : '#9e9e9e'}>{val.vessel_name}</Tag> </Col>
                                      <Col span={12}>{val.my_company_lob_name}</Col></Row>
                                  )
                                })}
                            </Col>
                            <Col>
                              <Calendar
                                localizer={localizer}
                                defaultDate={new Date()}
                                defaultView="month"
                                events={fromToDate}
                                style={{ height: "100vh" }}
                                eventPropGetter={eventStyleGetter}
                              />
                            </Col>
                          </Row>
                          <Row>
                            <Col span={12}>
                              <Tag color="#28a745">Scheduled</Tag>
                              <Tag color="#01bcd4" >Delivered</Tag>
                              <Tag color="#0726ff" >Redelivered</Tag>
                              <Tag color="#ff0000" >Completed</Tag>
                              <Tag color="#faad14" >Commenced</Tag>
                              <Tag color="#81d742" >Fix</Tag>
                              <Tag color='#fadb14'>Draft</Tag>
                              <Tag color="#9e9e9e" >Default</Tag>
                            </Col>
                            <Col span={8}></Col>
                          </Row>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </div>
          </Content>
        </Layout>

        <RightBarUI
          pageTitle="open-vessel-schedule-righttoolbar"
          callback={(data, options) => onClickRightMenu(data, options)}
        />
        {loadComponent !== undefined && title !== undefined && visibleDrawer === true &&
          <Drawer
            title={state.title}
            placement="right"
            closable={true}
            onClose={onCloseDrawer}
            open={state.visibleDrawer}
            getContainer={false}
            style={{ position: 'absolute' }}
            width={state.width}
            maskClosable={false}
            className="drawer-wrapper-container"
          >
            <div className="tcov-wrapper">
              <div className="layout-wrapper scrollHeight">
                <div className="content-wrapper noHeight">{state.loadComponent}</div>
              </div>
            </div>
          </Drawer>
        }
      </Layout>

      <Modal
        style={{ top: '2%' }}
        title="Reports"
        open={state.modals['VesselScheduleReport']}
        onCancel={() => showHideModal(false, 'VesselScheduleReport')}
        width="95%"
        footer={null}
      >
        <VesselScheduleReport />
      </Modal>
    </div>
  );
}

export default VesselOpenSchedule;
