import React, { useState, useEffect } from 'react';
import EditableTable from '../../shared/EditableTable/index.js';
import COMPONENTS from '../../constants/uiComponents';
import { useParams, useLocation } from 'react-router-dom';

function Masters(props) {
  const { slug } = useParams();
  const location = useLocation();
  let propObject = COMPONENTS.filter(e => e.path === location?.pathname)


  const [state, setState] = useState({
    noShowList: {
      'fuel-grades': ['menu-evt-edit'],
      'fuel-types': [],
    },
    pageTitle: (propObject && propObject.length > 0 && propObject[0].hasOwnProperty('pageTitle') && propObject[0].pageTitle !== '' ? propObject[0].pageTitle : undefined),
    slug: props.isMaster ? props.slug : slug || generateSlug(location?.pathname),
    children: props.isMaster ? props.children : null,
    callback: props.callback ? props.callback : null,
    showPage: false,
  });

  useEffect(() => {
    if (props.isMaster && (props.slug !== state.slug)) {
      setState({
        ...state,
        slug: props.slug,
        children: props.isMaster ? props.children : null,
        callback: props.isMaster ? props.callback : null,
      });
    } else if (slug && slug !== state.slug) {
      setState({ ...state, slug });
    }
  }, [props, slug, location]);

  function generateSlug(path) {
    return path.toLowerCase().replaceAll(' ', '').replaceAll('/', '');
  }

  const pathSnippetsFormatted = (snippet) => {
    let snipp = snippet?.replace(/-/g, ' ');
    return snipp?.charAt(0)?.toUpperCase() + snipp?.slice(1);
  };

  const { children, callback, pageTitle, noShowList } = state;

  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title">
                  <span>{pageTitle || pathSnippetsFormatted(slug)}</span>
                </h4>
              </div>
            </div>
            <div className="row">
              <EditableTable noShowList={noShowList} tableKey={slug} callback={callback}>
                {children}
              </EditableTable>
            </div>
          </div>
        </div>
      </article>
    </div>
  );
}

export default Masters;
