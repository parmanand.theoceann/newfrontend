import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Vessel/Voy :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Requirement ID :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Invoice No. :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Port Name :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Currency/Ex Rate :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Invoice Status :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Vendor :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Account No. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Delivery Date :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Broker :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">PO Number :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Invoice Date :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">My Company:</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Remittance Bank :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Payment Terms :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Person In Charge :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Due Date :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Received Date :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Remark :</td>
                      <td>Value</td>
                      <td colSpan="6"></td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th>Grade</th>
                      <th>Sulphar %</th>
                      <th>Invoice Qty</th>
                      <th>Reced Qty</th>
                      <th>Fuel Cost</th>
                      <th>Barge Rate</th>
                      <th>Barge Cost</th>
                      <th>Other Costs</th>
                      <th>Sales Tax</th>
                      <th>Net Cost</th>
                      <th>Port Charge</th>
                      <th>Invoice Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">GST % :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">PST % :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Grand Total :</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class BunkerInvoiceReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                          <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default BunkerInvoiceReport;
