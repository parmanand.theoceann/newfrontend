import React, { Component } from 'react';
import FuelConsumptionChart from './fuel-consumption-chart';
import FuelConsumptionCategoriesChart from './fuel-consumption-categories-chart';
import FuelEmissionsChart from './fuel-consumption-chart';
import FuelEmissionCategoriesChart from './fuel-consumption-categories-chart';
import SeaConsumptionChart from './sea-consumption-chart';
import PortConsumptionChart from './port-consumption-chart';
import SeaEmissionsChart from './sea-emissions-chart';
import PortEmissionsChart from './port-emissions-chart';

class VesselSpeedPredictiveMatrix extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <div className="row">
                    <div className="col-md-3">
                        <SeaConsumptionChart />
                    </div>
                    <div className="col-md-3">
                        <SeaEmissionsChart />
                    </div>
                    <div className="col-md-3">
                        <PortConsumptionChart />
                    </div>
                    <div className="col-md-3">
                        <PortEmissionsChart />
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6">
                        <FuelConsumptionChart />
                    </div>
                    <div className="col-md-6">
                        <FuelConsumptionCategoriesChart />
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6">
                        <FuelEmissionsChart />
                    </div>
                    <div className="col-md-6">
                        <FuelEmissionCategoriesChart />
                    </div>
                </div>
            </div>
        )
    }
}

export default VesselSpeedPredictiveMatrix;