import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import { Card } from 'antd';

let gauge2 = {};

gauge2.option = {
    "toolbox": {
        "show": false,
        "feature": {
            "mark": {
                "show": true
            },
            "restore": {
                "show": true
            },
            "saveAsImage": {
                "show": true
            }
        }
    },
    "series": [{
        "name": "KPI",
        "type": "gauge",
        "startAngle": 180,
        "endAngle": 0,
        "center": ["50%", "77%"],
        "radius": 150,
        "axisLine": {
            "lineStyle": {
                "width": 30,
                "color": [[0.298, "#fd5a62"], [1, "#dce3ec"]]
            }
        },
        "axisTick": {
            "show": false
        },
        "axisLabel": {
            "show": false
        },
        "splitLine": {
            "show": false
        },
        "pointer": {
            "width": 10,
            "length": "70%",
            "color": "#fd5a62"
        },
        "title": {
            "show": true,
            "offsetCenter": [25, "25%"],
            "textStyle": {
                "color": "#fd5a62",
                "fontSize": 20,
                "fontWeight": "bold"
            }
        },
        "detail": {
            "show": false
        },
        "data": [{
            "value": 15.60,
            "name": "15.60"
        }]
    }]
};

const SeaEmissionsChart = () => (
    <div className="box box-default mb-4">
        <div className="box-body">
            <Card size="small" title="Avg. At Sea Emissions (CO2)" extra={<a nohref="">More</a>} style={{ width: '100%' }}>
                <ReactEcharts option={gauge2.option} theme={"macarons"} style={{height: '200px'}} />
            </Card>
        </div>
    </div>
)

export default SeaEmissionsChart;