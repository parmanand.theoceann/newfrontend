import React, { Component } from 'react';
import SeaLink from './sea-link';
import { Button, Tabs } from 'antd';

const TabPane = Tabs.TabPane;

class VesselDataReportingSystem extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">

                            <div className="form-wrapper">
                                <div className="form-heading">
                                    <h4 className="title"><span>Vessel Data Reporting System</span></h4>
                                </div>
                                <div className="action-btn">
                                    <Button type="primary" htmlType="submit">Save</Button>
                                    <Button>Reset</Button>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <Tabs defaultActiveKey="sealink" size="small">
                                        <TabPane tab="Estimates" key="estimates">Estimates</TabPane>
                                        <TabPane tab="Operations" key="operations">Operations</TabPane>
                                        <TabPane tab="Invoices" key="invoices">Invoices</TabPane>
                                        <TabPane tab="P&L" key="pl">P&L</TabPane>
                                        <TabPane tab="Sea Link" key="sealink"><SeaLink /></TabPane>
                                    </Tabs>
                                </div>
                            </div>

                        </div>
                    </div>
                </article>
            </div>
        )
    }
}

export default VesselDataReportingSystem;