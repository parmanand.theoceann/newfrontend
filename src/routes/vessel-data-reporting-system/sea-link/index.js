import React, { Component } from 'react';
import { Form, Input, DatePicker, Select,  Button, Table, Popconfirm } from 'antd';
import {DeleteOutlined,SaveOutlined,EditOutlined} from '@ant-design/icons';
const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

function voyageStatus(value) {
    // console.log(`selected ${value}`);
}

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        reporttype: "Report Type",
        eta: "ETA",
        speedtype: "Speed Type",
        speed: "Speed",
        speedrpm: "Speed RPM",
        reportdatetime: "Report Date Time",
        status: "Status",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class SeaLink extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Report Type',
            dataIndex: 'reporttype',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'reporttype'),
        },
        {
            title: 'ETA',
            dataIndex: 'eta',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'eta'),
        },
        {
            title: 'Speed Type',
            dataIndex: 'speedtype',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'speedtype'),
        },
        {
            title: 'Speed',
            dataIndex: 'speed',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'speed'),
        },
        {
            title: 'Speed RPM',
            dataIndex: 'speedrpm',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'speedrpm'),
        },

        {
            title: 'Report DateTime',
            dataIndex: 'reportdatetime',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'reportdatetime'),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'status'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> 
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <div className="detailWrapper">

                <div className="form-wrapper">
                    <div className="form-heading">
                        <h4 className="title"><span>Sea Link</span></h4>
                    </div>
                    <div className="action-btn">
                        <Button type="primary" htmlType="submit" size="small" ghost>Send Voyage Position Report to Master</Button>
                        <Button type="primary" htmlType="submit" size="small" ghost>Create Master Report</Button>
                        <Button type="primary" htmlType="submit" size="small" ghost>Upload Master Report</Button>
                    </div>
                </div>

                <Form>

                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="Vessel">
                                <Input size="default" placeholder="PIONEER" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="TC Code/Hire">
                                <InputGroup compact>
                                    <Input style={{ width: '50%' }} defaultValue="TCI-1068" />
                                    <Input style={{ width: '50%' }} defaultValue="5000.0000" />
                                </InputGroup>
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Fixture No.">
                                <Input size="default" placeholder="Fixture-1302" />
                            </FormItem>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="Bunker Calc Method">
                                <Input size="default" placeholder="" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Voy. No./Opr. Type">
                                <InputGroup compact>
                                    <Input style={{ width: '40%' }} defaultValue="Voyage" />
                                    <Input style={{ width: '30%' }} defaultValue="1302" />
                                    <Input style={{ width: '30%' }} defaultValue="" />
                                </InputGroup>
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Trade Area">
                                <Input size="default" placeholder="WORLDWIDE" />
                            </FormItem>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="Chtr Specialist">
                                <Input size="default" placeholder="ADANI" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Ops Coordinator">
                                <Input size="default" placeholder="" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Voyage Status">
                                <Select defaultValue="Option One" onChange={voyageStatus}>
                                    <Option value="Option One">Option One</Option>
                                    <Option value="Option Two">Option Two</Option>
                                </Select>
                            </FormItem>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="Voyage Commencing">
                                <DatePicker />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Voyage Completing">
                                <DatePicker />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Last Update GMT">
                                <DatePicker />
                            </FormItem>
                        </div>
                    </div>

                    <hr />

                    <Table
                        bordered
                        dataSource={this.state.data}
                        columns={this.columns}
                        scroll={{ y: 300 }}
                        size="small"
                        pagination={false}
                        footer={() => <div className="text-center">
                            <Button type="link">Add New</Button>
                        </div>
                        }
                    />

                </Form>
            </div>
        )
    }
}

export default SeaLink;