import React, { Component } from "react";
import { Form, Input, Select, DatePicker } from "antd";
import { LeftCircleOutlined } from "@ant-design/icons";

import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
} from "../../shared";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import BarChart from "../dashboard/dcharts/Barchart";

import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";

const Option = Select.Option;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

function handleChange(value) {
  // console.log(`selected ${value}`);
}

class OverallPerformanceAnalysis extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chart_1: {
        ordered_speed: [],
        overall_perf_sped: [],
        sea_cons: [],
        average_rpm: [],
        rpm_laden: [],
        rpm_ballast: [],
        wind_for: [],
        date: [],
      },
      formData: {},
      frmName: "vessel_performance_analysis_form",
      loadingMap: true,
      passage_type: [],
      vessel_name: [],
      voyage_no: [],
      data: [],
      report: 0,
      ifo: 0,
      vlsfo: 0,
      ulsfo: 0,
      mgo: 0,
      lsmgo: 0,
      emmisiion: 0,
      co2:0,
      avgSpeed:0,
      xAxisData:[],
      chartdata:[]
     
   
      // series: [{
      //   name: 'Product1',
      //   data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'Product2',
      //   data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'Product3',
      //   data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'Product4',
      //   data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // }],
      // options: {
      //   chart: {
      //     height: 350,
      //     type: 'bubble',
      //   },
      //   dataLabels: {
      //     enabled: false
      //   },
      //   fill: {
      //     type: 'gradient',
      //   },
      //   // title: {
      //   //   text: '3D Bubble Chart'
      //   // },
      //   xaxis: {
      //     tickAmount: 12,
      //     type: 'datetime',
      //     labels: {
      //       rotate: 0,
      //     }
      //   },
      //   yaxis: {
      //     max: 70
      //   },
      //   theme: {
      //     palette: 'palette2'
      //   }
      // },
      // seriesScatter: [{
      //   name: 'TEAM 1',
      //   data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'TEAM 2',
      //   data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 20, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'TEAM 3',
      //   data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 30, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'TEAM 4',
      //   data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 10, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // {
      //   name: 'TEAM 5',
      //   data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 30, {
      //     min: 10,
      //     max: 60
      //   })
      // },
      // ],
      // optionsScatter: {
      //   chart: {
      //     height: 350,
      //     type: 'scatter',
      //     zoom: {
      //       type: 'xy'
      //     }
      //   },
      //   dataLabels: {
      //     enabled: false
      //   },
      //   grid: {
      //     xaxis: {
      //       lines: {
      //         show: true
      //       }
      //     },
      //     yaxis: {
      //       lines: {
      //         show: true
      //       }
      //     },
      //   },
      //   xaxis: {
      //     type: 'datetime',
      //   },
      //   yaxis: {
      //     max: 70
      //   }
      // },
      // seriescandlestick: [{
      //   data: [{
      //       x: new Date(1538778600000),
      //       y: [6629.81, 6650.5, 6623.04, 6633.33]
      //     },
      //     {
      //       x: new Date(1538780400000),
      //       y: [6632.01, 6643.59, 6620, 6630.11]
      //     },
      //     {
      //       x: new Date(1538782200000),
      //       y: [6630.71, 6648.95, 6623.34, 6635.65]
      //     },
      //     {
      //       x: new Date(1538784000000),
      //       y: [6635.65, 6651, 6629.67, 6638.24]
      //     },
      //     {
      //       x: new Date(1538785800000),
      //       y: [6638.24, 6640, 6620, 6624.47]
      //     },
      //     {
      //       x: new Date(1538787600000),
      //       y: [6624.53, 6636.03, 6621.68, 6624.31]
      //     },
      //     {
      //       x: new Date(1538789400000),
      //       y: [6624.61, 6632.2, 6617, 6626.02]
      //     },
      //     {
      //       x: new Date(1538791200000),
      //       y: [6627, 6627.62, 6584.22, 6603.02]
      //     },
      //     {
      //       x: new Date(1538793000000),
      //       y: [6605, 6608.03, 6598.95, 6604.01]
      //     },
      //     {
      //       x: new Date(1538794800000),
      //       y: [6604.5, 6614.4, 6602.26, 6608.02]
      //     },
      //     {
      //       x: new Date(1538796600000),
      //       y: [6608.02, 6610.68, 6601.99, 6608.91]
      //     },
      //     {
      //       x: new Date(1538798400000),
      //       y: [6608.91, 6618.99, 6608.01, 6612]
      //     },
      //     {
      //       x: new Date(1538800200000),
      //       y: [6612, 6615.13, 6605.09, 6612]
      //     },
      //     {
      //       x: new Date(1538802000000),
      //       y: [6612, 6624.12, 6608.43, 6622.95]
      //     },
      //     {
      //       x: new Date(1538803800000),
      //       y: [6623.91, 6623.91, 6615, 6615.67]
      //     },
      //     {
      //       x: new Date(1538805600000),
      //       y: [6618.69, 6618.74, 6610, 6610.4]
      //     },
      //     {
      //       x: new Date(1538807400000),
      //       y: [6611, 6622.78, 6610.4, 6614.9]
      //     },
      //     {
      //       x: new Date(1538809200000),
      //       y: [6614.9, 6626.2, 6613.33, 6623.45]
      //     },
      //     {
      //       x: new Date(1538811000000),
      //       y: [6623.48, 6627, 6618.38, 6620.35]
      //     },
      //     {
      //       x: new Date(1538812800000),
      //       y: [6619.43, 6620.35, 6610.05, 6615.53]
      //     },
      //     {
      //       x: new Date(1538814600000),
      //       y: [6615.53, 6617.93, 6610, 6615.19]
      //     },
      //     {
      //       x: new Date(1538816400000),
      //       y: [6615.19, 6621.6, 6608.2, 6620]
      //     },
      //     {
      //       x: new Date(1538818200000),
      //       y: [6619.54, 6625.17, 6614.15, 6620]
      //     },
      //     {
      //       x: new Date(1538820000000),
      //       y: [6620.33, 6634.15, 6617.24, 6624.61]
      //     },
      //     {
      //       x: new Date(1538821800000),
      //       y: [6625.95, 6626, 6611.66, 6617.58]
      //     },
      //     {
      //       x: new Date(1538823600000),
      //       y: [6619, 6625.97, 6595.27, 6598.86]
      //     },
      //     {
      //       x: new Date(1538825400000),
      //       y: [6598.86, 6598.88, 6570, 6587.16]
      //     },
      //     {
      //       x: new Date(1538827200000),
      //       y: [6588.86, 6600, 6580, 6593.4]
      //     },
      //     {
      //       x: new Date(1538829000000),
      //       y: [6593.99, 6598.89, 6585, 6587.81]
      //     },
      //     {
      //       x: new Date(1538830800000),
      //       y: [6587.81, 6592.73, 6567.14, 6578]
      //     },
      //     {
      //       x: new Date(1538832600000),
      //       y: [6578.35, 6581.72, 6567.39, 6579]
      //     },
      //     {
      //       x: new Date(1538834400000),
      //       y: [6579.38, 6580.92, 6566.77, 6575.96]
      //     },
      //     {
      //       x: new Date(1538836200000),
      //       y: [6575.96, 6589, 6571.77, 6588.92]
      //     },
      //     {
      //       x: new Date(1538838000000),
      //       y: [6588.92, 6594, 6577.55, 6589.22]
      //     },
      //     {
      //       x: new Date(1538839800000),
      //       y: [6589.3, 6598.89, 6589.1, 6596.08]
      //     },
      //     {
      //       x: new Date(1538841600000),
      //       y: [6597.5, 6600, 6588.39, 6596.25]
      //     },
      //     {
      //       x: new Date(1538843400000),
      //       y: [6598.03, 6600, 6588.73, 6595.97]
      //     },
      //     {
      //       x: new Date(1538845200000),
      //       y: [6595.97, 6602.01, 6588.17, 6602]
      //     },
      //     {
      //       x: new Date(1538847000000),
      //       y: [6602, 6607, 6596.51, 6599.95]
      //     },
      //     {
      //       x: new Date(1538848800000),
      //       y: [6600.63, 6601.21, 6590.39, 6591.02]
      //     },
      //     {
      //       x: new Date(1538850600000),
      //       y: [6591.02, 6603.08, 6591, 6591]
      //     },
      //     {
      //       x: new Date(1538852400000),
      //       y: [6591, 6601.32, 6585, 6592]
      //     },
      //     {
      //       x: new Date(1538854200000),
      //       y: [6593.13, 6596.01, 6590, 6593.34]
      //     },
      //     {
      //       x: new Date(1538856000000),
      //       y: [6593.34, 6604.76, 6582.63, 6593.86]
      //     },
      //     {
      //       x: new Date(1538857800000),
      //       y: [6593.86, 6604.28, 6586.57, 6600.01]
      //     },
      //     {
      //       x: new Date(1538859600000),
      //       y: [6601.81, 6603.21, 6592.78, 6596.25]
      //     },
      //     {
      //       x: new Date(1538861400000),
      //       y: [6596.25, 6604.2, 6590, 6602.99]
      //     },
      //     {
      //       x: new Date(1538863200000),
      //       y: [6602.99, 6606, 6584.99, 6587.81]
      //     },
      //     {
      //       x: new Date(1538865000000),
      //       y: [6587.81, 6595, 6583.27, 6591.96]
      //     },
      //     {
      //       x: new Date(1538866800000),
      //       y: [6591.97, 6596.07, 6585, 6588.39]
      //     },
      //     {
      //       x: new Date(1538868600000),
      //       y: [6587.6, 6598.21, 6587.6, 6594.27]
      //     },
      //     {
      //       x: new Date(1538870400000),
      //       y: [6596.44, 6601, 6590, 6596.55]
      //     },
      //     {
      //       x: new Date(1538872200000),
      //       y: [6598.91, 6605, 6596.61, 6600.02]
      //     },
      //     {
      //       x: new Date(1538874000000),
      //       y: [6600.55, 6605, 6589.14, 6593.01]
      //     },
      //     {
      //       x: new Date(1538875800000),
      //       y: [6593.15, 6605, 6592, 6603.06]
      //     },
      //     {
      //       x: new Date(1538877600000),
      //       y: [6603.07, 6604.5, 6599.09, 6603.89]
      //     },
      //     {
      //       x: new Date(1538879400000),
      //       y: [6604.44, 6604.44, 6600, 6603.5]
      //     },
      //     {
      //       x: new Date(1538881200000),
      //       y: [6603.5, 6603.99, 6597.5, 6603.86]
      //     },
      //     {
      //       x: new Date(1538883000000),
      //       y: [6603.85, 6605, 6600, 6604.07]
      //     },
      //     {
      //       x: new Date(1538884800000),
      //       y: [6604.98, 6606, 6604.07, 6606]
      //     },
      //   ]
      // }],
      // optionscandlestick: {
      //   chart: {
      //     type: 'candlestick',
      //     height: 350
      //   },
      //   xaxis: {
      //     type: 'datetime'
      //   },
      //   yaxis: {
      //     tooltip: {
      //       enabled: true
      //     }
      //   }
      // },
      // seriesLine: [{
      //   name: 'TEAM A',
      //   type: 'column',
      //   data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30]
      // },
      // {
      //   name: 'TEAM B',
      //   type: 'line',
      //   data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39]
      // }],
      // optionsLine: {
      //   chart: {
      //     height: 350,
      //     type: 'line',
      //     stacked: false,
      //   },
      //   stroke: {
      //     width: [0, 2, 5],
      //     curve: 'smooth'
      //   },
      //   plotOptions: {
      //     bar: {
      //       columnWidth: '50%'
      //     }
      //   },

      //   fill: {
      //     opacity: [0.85, 0.25, 1],
      //     gradient: {
      //       inverseColors: false,
      //       shade: 'light',
      //       type: "vertical",
      //       opacityFrom: 0.85,
      //       opacityTo: 0.55,
      //       stops: [0, 100, 100, 100]
      //     }
      //   },
      //   labels: ['01/01/2003', '02/01/2003', '03/01/2003', '04/01/2003', '05/01/2003', '06/01/2003', '07/01/2003',
      //     '08/01/2003', '09/01/2003', '10/01/2003', '11/01/2003'
      //   ],
      //   markers: {
      //     size: 0
      //   },
      //   xaxis: {
      //     type: 'datetime'
      //   },
      //   yaxis: {
      //     title: {
      //       text: 'Points',
      //     },
      //     min: 0
      //   },
      //   tooltip: {
      //     shared: true,
      //     intersect: false,
      //     y: {
      //       formatter: function (y) {
      //         if (typeof y !== "undefined") {
      //           return y.toFixed(0) + " points";
      //         }
      //         return y;

      //       }
      //     }
      //   }
      // },
      // seriesPie: [44, 55, 13, 43, 22],
      // optionsPie: {
      //   chart: {
      //     width: 380,
      //     type: 'pie',
      //   },
      //   labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
      //   responsive: [{
      //     breakpoint: 480,
      //     options: {
      //       chart: {
      //         width: 200
      //       },
      //       legend: {
      //         position: 'bottom'
      //       }
      //     }
      //   }]
      // },
      // seriesBar: [{
      //   name: 'Cash Flow',
      //   data: [1.45, 5.42, 5.9, -0.42, -12.6, -18.1, -18.2, -14.16, -11.1, -6.09, 0.34, 3.88, 13.07,
      //     5.8, 2, 7.37, 8.1, 13.57, 15.75, 17.1, 19.8, -27.03, -54.4, -47.2, -43.3, -18.6, -
      //     48.6, -41.1, -39.6, -37.6, -29.4, -21.4, -2.4
      //   ]
      // }],
      // optionsBar: {
      //   chart: {
      //     type: 'bar',
      //     height: 350
      //   },
      //   plotOptions: {
      //     bar: {
      //       colors: {
      //         ranges: [{
      //           from: -100,
      //           to: 0,
      //           color: '#FEB019'
      //         }]
      //       },
      //       columnWidth: '80%',
      //     }
      //   },
      //   dataLabels: {
      //     enabled: false,
      //   },
      //   yaxis: {
      //     title: {
      //       text: 'Growth',
      //     },
      //     labels: {
      //       formatter: function (y) {
      //         return y.toFixed(0) + "%";
      //       }
      //     }
      //   },
      //   xaxis: {
      //     type: 'datetime',
      //     categories: [
      //       '2011-01-01', '2011-02-01', '2011-03-01', '2011-04-01', '2011-05-01', '2011-06-01',
      //       '2011-07-01', '2011-08-01', '2011-09-01', '2011-10-01', '2011-11-01', '2011-12-01',
      //       '2012-01-01', '2012-02-01', '2012-03-01', '2012-04-01', '2012-05-01', '2012-06-01',
      //       '2012-07-01', '2012-08-01', '2012-09-01', '2012-10-01', '2012-11-01', '2012-12-01',
      //       '2013-01-01', '2013-02-01', '2013-03-01', '2013-04-01', '2013-05-01', '2013-06-01',
      //       '2013-07-01', '2013-08-01', '2013-09-01'
      //     ],
      //     labels: {
      //       rotate: -90
      //     }
      //   }
      // },
      // seriesSimpleBar: [{
      //   name: 'PRODUCT A',
      //   data: [44, 55, 41, 67, 22, 43]
      // }, {
      //   name: 'PRODUCT B',
      //   data: [13, 23, 20, 8, 13, 27]
      // }],
      // optionsSimpleBar: {
      //   chart: {
      //     type: 'bar',
      //     height: 350,
      //     stacked: true,
      //     toolbar: {
      //       show: true
      //     },
      //     zoom: {
      //       enabled: true
      //     }
      //   },
      //   responsive: [{
      //     breakpoint: 480,
      //     options: {
      //       legend: {
      //         position: 'bottom',
      //         offsetX: -10,
      //         offsetY: 0
      //       }
      //     }
      //   }],
      //   plotOptions: {
      //     bar: {
      //       horizontal: false,
      //       borderRadius: 10
      //     },
      //   },
      //   xaxis: {
      //     type: 'datetime',
      //     categories: ['01/01/2011 GMT', '01/02/2011 GMT', '01/03/2011 GMT', '01/04/2011 GMT',
      //       '01/05/2011 GMT', '01/06/2011 GMT'
      //     ],
      //   },
      //   legend: {
      //     position: 'right',
      //     offsetY: 40
      //   },
      //   fill: {
      //     opacity: 1
      //   }
      // },
    };
  }
  componentDidMount = async () => {
    this.fetchData();
  };

  fetchData = async () => {
    this.setState({
      ...this.state,
      loading: true,
      data: [],
    });

    try {
      let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/grapreport`;
      const response = await getAPICall(_url);
      const data = await response;
      let dataArr = data && data.data ? data.data : [];
      let state = { loading: false };
      if (dataArr.length > 0) {
        state["data"] = dataArr;
      }
      this.setState(
        {
          ...this.state,
          ...state,
          loading: false,
        },
        () => {
          this.showData(dataArr);
        }
      );
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
      this.setState({
        ...this.state,
        loading: false,
      });
    }
  };
 showData = (data) =>{
  this.setState({
    loadingMap: true,
  });
  let co2 = 0,
  avgSpeed=0,
  ifo=0,
  vlsfo=0,
  ulsfo=0,
  mgo=0,
  lsmgo=0,
  chartdata=[],
  xAxisData=[]

  let checkData = [{...data}]
if(checkData.length>0){
   co2 = data.totalCo2Emission? data.totalCo2Emission :0;
   avgSpeed = data.average_speed ?data.average_speed:0;
   ifo= data.totalFuel.total?data.totalFuel.total:0;
   vlsfo=data.totalFuel.vlsfo?data.totalFuel.vlsfo:0;
   ulsfo=data.totalFuel.ulsfo?data.totalFuel.ulsfo:0;
   mgo=data.totalFuel.mgo?data.totalFuel.mgo:0;
   lsmgo= data.totalFuel.lsmgo?data.totalFuel.lsmgo:0;
   chartdata= data.totalFuel?Object.values(data.totalFuel):[]

//    "totalFuel": {
//     "lsmgo": 44,
//     "mgo": 58,
//     "total": 338,
//     "ulsfo": 105,
//     "vlsfo": 131
// }
xAxisData = Object.keys(data.totalFuel).map(key =>"TOTAL "+ key.toUpperCase());

}

  this.setState({
    co2,
    avgSpeed,
    ifo,
    vlsfo,
    ulsfo,
    mgo,
    lsmgo,
    xAxisData,
    chartdata
  });
 }

  getSum = (data, val) => {
    this.setState({
      loadingMap: true,
    });
    let passage_type = [],
      ordered_speed = [],
      overall_perf_sped = [],
      date = [],
      sea_cons = [],
      average_rpm = [],
      rpm_laden = [],
      rpm_ballast = [],
      wind_for = [],
      ifo = 0,
      vlsfo = 0,
      ulsfo = 0,
      mgo = 0,
      lsmgo = 0,
      report = 0,
      emmisiion = 0,
      vessel_name = [],
      voyage_no = [];
    if (data.length > 0) {
      data.map((val, i) => {
        passage_type.push({
          value: val.passage_type,
          id: val.passage_type,
        });
        i == 0 &&
          vessel_name.push({
            value: val.vessel_name,
            id: val.vessel_name,
          });
        i == 0 &&
          voyage_no.push({
            value: val.voyage_id,
            id: val.voyage_id,
          });
      });
      let data1 =
        val == null ? data : data.filter((val1) => val1.passage_type == val);
      if (data1.length > 0) {
        data1.map((value, indd) => {
          ordered_speed.push(
            value.ordered_speed ? parseInt(value.ordered_speed) : 0
          );
          overall_perf_sped.push(
            value.overall_perf_sped ? parseInt(value.overall_perf_sped) : 0
          );
          sea_cons.push(value.sea_cons ? parseInt(value.sea_cons) : 0);
          wind_for.push(value.wind_for ? parseInt(value.wind_for) : 0);
          average_rpm.push(value.average_rpm ? parseInt(value.average_rpm) : 0);
          if (value.passage_type == "Ballast") {
            rpm_ballast.push(
              value.average_rpm ? parseInt(value.average_rpm) : 0
            );
          } else {
            rpm_laden.push(value.average_rpm ? parseInt(value.average_rpm) : 0);
          }
          date.push(value.arr_date ? value.arr_date : `date${indd}`);
          report += value.report;
          ifo +=
            (value && value.m_E_cons && value.m_E_cons.ifo
              ? parseFloat(value.m_E_cons.ifo)
              : 0) +
            (value && value.generator && value.generator.ifo
              ? parseFloat(value.generator.ifo)
              : 0) +
            (value && value.aux_engine && value.aux_engine.ifo
              ? parseFloat(value.aux_engine.ifo)
              : 0) +
            (value && value.l_d && value.l_d.ifo
              ? parseFloat(value.l_d.ifo)
              : 0);
          vlsfo +=
            (value && value.m_E_cons && value.m_E_cons.vlsfo
              ? parseFloat(value.m_E_cons.vlsfo)
              : 0) +
            (value && value.generator && value.generator.vlsfo
              ? parseFloat(value.generator.vlsfo)
              : 0) +
            (value && value.aux_engine && value.aux_engine.vlsfo
              ? parseFloat(value.aux_engine.vlsfo)
              : 0) +
            (value && value.l_d && value.l_d.vlsfo
              ? parseFloat(value.l_d.vlsfo)
              : 0);
          ulsfo +=
            (value && value.m_E_cons && value.m_E_cons.ulsfo
              ? parseFloat(value.m_E_cons.ulsfo)
              : 0) +
            (value && value.generator && value.generator.ulsfo
              ? parseFloat(value.generator.ulsfo)
              : 0) +
            (value && value.aux_engine && value.aux_engine.ulsfo
              ? parseFloat(value.aux_engine.ulsfo)
              : 0) +
            (value && value.l_d && value.l_d.ulsfo
              ? parseFloat(value.l_d.ulsfo)
              : 0);
          mgo +=
            (value && value.m_E_cons && value.m_E_cons.mgo
              ? parseFloat(value.m_E_cons.mgo)
              : 0) +
            (value && value.generator && value.generator.mgo
              ? parseFloat(value.generator.mgo)
              : 0) +
            (value && value.aux_engine && value.aux_engine.mgo
              ? parseFloat(value.aux_engine.mgo)
              : 0) +
            (value && value.l_d && value.l_d.mgo
              ? parseFloat(value.l_d.mgo)
              : 0);
          lsmgo +=
            (value && value.m_E_cons && value.m_E_cons.lsmgo
              ? parseFloat(value.m_E_cons.lsmgo)
              : 0) +
            (value && value.generator && value.generator.lsmgo
              ? parseFloat(value.generator.lsmgo)
              : 0) +
            (value && value.aux_engine && value.aux_engine.lsmgo
              ? parseFloat(value.aux_engine.lsmgo)
              : 0) +
            (value && value.l_d && value.l_d.lsmgo
              ? parseFloat(value.l_d.lsmgo)
              : 0);
          emmisiion +=
            (value && value.emmisiion && value.emmisiion.ifo
              ? parseFloat(value.emmisiion.ifo)
              : 0) +
            (value && value.emmisiion && value.emmisiion.vlsfo
              ? parseFloat(value.emmisiion.vlsfo)
              : 0) +
            (value && value.emmisiion && value.emmisiion.lsmgo
              ? parseFloat(value.emmisiion.lsmgo)
              : 0) +
            (value && value.emmisiion && value.emmisiion.mgo
              ? parseFloat(value.emmisiion.mgo)
              : 0) +
            (value && value.emmisiion && value.emmisiion.ulsfo
              ? parseFloat(value.emmisiion.ulsfo)
              : 0);
        });
      }
      this.setState({
        loadingMap: false,
        passage_type,
        voyage_no,
        vessel_name,
        ifo,
        vlsfo,
        ulsfo,
        mgo,
        lsmgo,
        report,
        emmisiion,
        passage_val: val != null ? val : "",
        chart_1: {
          ordered_speed,
          overall_perf_sped,
          sea_cons,
          average_rpm,
          rpm_laden,
          rpm_ballast,
          wind_for,
          date,
        },
      });
    }
  };
  onChange = (val) => {
    // this.getSum(this.state.data, val);
    this.showData(this.state.data)
  };

  render() {
    const {
      formData,
      frmName,
      passage_type,
      loadingMap,
      chart_1,
      passage_val,
      ifo,
      vlsfo,
      ulsfo,
      mgo,
      lsmgo,
      report,
      emmisiion,
      data,
      co2,
      avgSpeed,
      vessel_name,
      voyage_no,
      chartdata,
      xAxisData
    } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-heading border-bottom pb-2">
                <h4 className="title">
                  <span>Overall Performance Analysis</span>
                  <span className="float-right">
                    <a
                      href="#/dynamic-vspm"
                      className="btn btn-primary pl-2 pr-2 pt-1 pb-1"
                    >
                      <LeftCircleOutlined />
                    </a>
                  </span>
                </h4>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <div className="body-wrapper">
                    <article className="article">
                      <div className="box box-default">
                        <div className="box-body">
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            showButtons={[
                              {
                                id: "save",
                                title: "Update",
                                type: "primary",
                                event: (data) => {
                                  this.saveFormData(data);
                                },
                              },
                              {
                                id: "save",
                                title: "Reset",
                                type: "primary",
                                event: (data) => {
                                  this.saveFormData(data);
                                },
                              },
                            ]}
                            // showToolbar={[
                            //   {
                            //     isLeftBtn: [
                            //       {
                            //         key: 's2',
                            //         isSets: [
                            //           {
                            //             id: '3',F
                            //             key: 'save',
                            //             type: 'save',
                            //             withText: '',
                            //             event: (key, data, innerCB) => this.saveFormData(data, innerCB),
                            //           },
                            //           {
                            //             id: '4',
                            //             key: 'reset',
                            //             type: 'reload',
                            //             withText: '',
                            //             event: (key, data, innerCB) => this.redirectToAddRole(data, innerCB),
                            //           },
                            //         ],
                            //       },
                            //     ],
                            //     isRightBtn: [],
                            //     isResetOption: false,
                            //   },
                            // ]}
                            inlineLayout={true}
                          />
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="card-group">
                    <div className="card">
                      <div className="card-header pt-1 pb-1">Total- At sea</div>
                      <div className="card-body text-center">
                        <h3 className="text-primary">
                          {/* {emmisiion.toFixed(2)}MT */}
                          { co2.toFixed(2)} MT
                            
                        </h3>
                        <p className="m-0">CO2 Emission</p>
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-header pt-1 pb-1">ave speed</div>
                      <div className="card-body text-center">
                        <h3 className="text-primary">
                          {/* {(emmisiion / data.length).toFixed(2)}MT */}
                          {avgSpeed.toFixed(2)}MT
                        </h3>
                        <p className="m-0">CO2 Emission</p>
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-header pt-1 pb-1">Total</div>
                      <div className="card-body text-center">
                        <h3 className="text-primary">20</h3>
                        <p className="m-0">Total distance</p>
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-header pt-1 pb-1">Total</div>
                      <div className="card-body text-center">
                        <h3 className="text-primary">20+</h3>
                        <p className="m-0"> Total steaming hours</p>
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-header pt-1 pb-1">Total</div>
                      <div className="card-body text-center">
                        <h3 className="text-primary">{report}</h3>
                        <p className="m-0">No of Report</p>
                      </div>
                    </div>
                  </div>

                  <div className="card mt-3">
                    <div className="card-header pt-1 pb-1">
                      Total Fuel Burn{" "}
                      <span className="float-right">At Sea</span>
                    </div>
                    <div className="card-group">
                      <div className="card p-0 rounded-0 border-bottom-0 border-top-0 border-left-0">
                        <div className="card-body text-center">
                          <h3 className="text-primary">{ifo.toFixed(2)}MT</h3>
                          <p className="m-0">IFO</p>
                        </div>
                      </div>
                      <div className="card p-0 rounded-0 border-bottom-0 border-top-0">
                        <div className="card-body text-center">
                          <h3 className="text-dark">{vlsfo.toFixed(2)}MT</h3>
                          <p className="m-0">VLSFO</p>
                        </div>
                      </div>
                      <div className="card p-0 rounded-0 border-bottom-0 border-top-0">
                        <div className="card-body text-center">
                          <h3 className="text-warning">{ulsfo.toFixed(2)}MT</h3>
                          <p className="m-0">ULSFO</p>
                        </div>
                      </div>
                      <div className="card p-0 rounded-0 border-bottom-0 border-top-0">
                        <div className="card-body text-center">
                          <h3 className="text-secondary">{mgo.toFixed(2)}MT</h3>
                          <p className="m-0">MGO</p>
                        </div>
                      </div>
                      <div className="card p-0 rounded-0 border-bottom-0 border-top-0 border-right-0">
                        <div className="card-body text-center">
                          <h3 className="text-danger">{lsmgo.toFixed(2)}MT</h3>
                          <p className="m-0">LSMGO</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row p-10">
                    {/* <BarChart Heading={"OverAll Consumption graph"} /> */}
                    <BarChart
                    barData={chartdata}
                    Heading={"OverAll Consumption graph"}
                    XaxisData={xAxisData}
                  />
                  </div>

                  <div className="row p-10">
                    <StackHorizontalChart
                      Heading={"fuel consumption from ME , AE and boiler"}
                      YaxisData={[
                        "Total Boiler Cons. SLR",
                        "Total AE Cons. SLR",
                        "Total ME Cons. SLR",
                      ]}
                      XaxisData={[
                        {
                          name: "Machinary Consumption",
                          type: "bar",
                          stack: "total",
                          label: {
                            show: true,
                          },
                          emphasis: {
                            focus: "series",
                          },
                          data: [320, 302, 301, 334, 390, 330, 320],
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default OverallPerformanceAnalysis;
