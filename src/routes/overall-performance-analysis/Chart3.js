import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';
class Chart3 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      option: {
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['Reproted Speed', 'Wind'],
          textStyle: {
            color: CHARTCONFIG.color.text
          }
        },
        // toolbox: {
        //   show: true,
        //   feature: {
        //     saveAsImage: { show: true, title: 'save' }
        //   }
        // },
        calculable: true,
        xAxis: [
          {
            type: 'category',
            data:this.props.data.date,
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            }
          }
        ],
        yAxis: [
          {
            type: 'value',
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            },
            splitArea: {
              show: true,
              areaStyle: {
                color: CHARTCONFIG.color.splitArea
              }
            }
          }
        ],
        series: [
          {
            name: 'Reproted Speed',
            type: 'bar',
            data:this.props.data.overall_perf_sped,
            markPoint: {
              data: [
                { type: 'max', name: 'Max' },
                { type: 'min', name: 'Min' }
              ]
            },
            markLine: {
              data: [
                { type: 'average', name: 'Average' }
              ]
            }
          },
          {
            name: 'Wind',
            type: 'bar',
            data:this.props.data.wind_for,
            markPoint: {
              data: [
                { type: 'max', name: 'Max' },
                { type: 'min', name: 'Min' }
              ]
            },
            markLine: {
              data: [
                { type: 'average', name: 'Average' }
              ]
            }
          }
        ]
      }
    }
  }
  render() {
    return (
      <div className="card mt-3">
    <div className="card-header pt-1 pb-1">Reproted Speed / Wind</div>
    <div className="card-body p-1">
      <ReactEcharts option={this.state.option} theme={"macarons"} />
    </div>
  </div>
    )

  }
}

export default Chart3;