import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';

let pie2 = {};

pie2.option = {
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    orient: 'horizontal',
    x: 'center',
    data: ['Voyage 1', 'Voyage 2'],
    textStyle: {
      color: CHARTCONFIG.color.text
    }
  },
  calculable: true,
  series: [
    {
      name: 'Reported Speed',
      type: 'pie',
      radius: ['50%', '70%'],
      itemStyle: {
        normal: {
          label: {
            show: false
          },
          labelLine: {
            show: false
          }
        },
      },
      data: [
        {value: 335, name: 'Voyage 1'},
        {value: 310, name: 'Voyage 2'},
      ]
    }
  ]
};

const Chart6 = () => (
  <div className="card mt-3">
    <div className="card-header pt-1 pb-1">Passage  Ballast</div>
    <div className="card-body">
      <ReactEcharts option={pie2.option} theme={"macarons"} style={{height: '270px'}} />
    </div>
  </div>
)

export default Chart6;