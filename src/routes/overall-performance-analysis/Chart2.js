

import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';
class Chart2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      option: {
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['Reported Speed', 'Sea Cons. Per day'],
          textStyle: {
            color: CHARTCONFIG.color.text
          }
        },
      //   toolbox: {
      //     show: true,
      //     feature: {
      //       saveAsImage: {show: true, title: 'save'}
      //     }
      //   },
        calculable: true,
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data: this.props.data.date,
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            }
          }
        ],
        yAxis: [
          {
            type: 'value',
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            },
            splitArea: {
              show: true,
              areaStyle: {
                color: CHARTCONFIG.color.splitArea
              }
            }
          }
        ],
        series: [
          {
            name: 'Reported Speed',
            type: 'line',
            stack: 'Sum',
            data: this.props.data.overall_perf_sped
          },
          {
            name: 'Sea Cons. Per day',
            type: 'line',
            stack: 'Sum',
            data: this.props.data.sea_cons
          },
          
        ]
      }
    }
  }
  render() {
    return (
      <div className="card mt-3">
    <div className="card-header pt-1 pb-1">Reported Speed / Sea Cons. Per day</div>
    <div className="card-body p-1">
      <ReactEcharts option={this.state.option} theme={"macarons"} />
    </div>
  </div>
    )

  }
}

export default Chart2;