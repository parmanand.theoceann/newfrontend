import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';
class Chart5 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      option:{
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['RPM at Ballast', 'RPM at Laden'],
          textStyle: {
            color: CHARTCONFIG.color.text
          }
        },
        calculable: true,
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data:this.props.data.date,
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            }
          }
        ],
        yAxis: [
          {
            type: 'value',
            axisLabel: {
              textStyle: {
                color: CHARTCONFIG.color.text
              }
            },
            splitLine: {
              lineStyle: {
                color: CHARTCONFIG.color.splitLine
              }
            },
            splitArea: {
              show: true,
              areaStyle: {
                color: CHARTCONFIG.color.splitArea
              }
            }
          }
        ],
        series: [
          {
            name: 'RPM at Ballast',
            type: 'line',
            stack: 'Sum',
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:this.props.data.rpm_ballast
          },
          {
            name: 'RPM at Laden',
            type: 'line',
            stack: 'Sum',
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:this.props.data.rpm_laden
          },
       
        ]
      }
    }
  }
  render() {
    return (
      <div className="card mt-3">
    <div className="card-header pt-1 pb-1">Ave. RPM</div>
    <div className="card-body p-1">
      <ReactEcharts option={this.state.option} theme={"macarons"} />
    </div>
  </div>
    )

  }
}

export default Chart5;