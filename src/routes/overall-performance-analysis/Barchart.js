import React, { useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
import CHARTCONFIG from '../../constants/chartConfig';


const BarChart = ({ Heading, fuelData,maxValue }) => {

  const [state, setState] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category",
          data: ["IFO", "LSMGO", "VLSFO", "ULSFO", "MGO"],
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          }
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        yAxis: {
          type: "value",
          min: 0,
          max: maxValue??1000
        },
        series: [
          {
            data: fuelData,
            type: "bar",
          },
        ],
      },
    },
  });

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts option={state.bar1.option} theme={"macarons"} />
      </div>
    </div>
  );
};

export default BarChart;
