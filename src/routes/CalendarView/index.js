import React from "react";
import URL_WITH_VERSION, { getAPICall } from "../../shared";
import { Link } from "react-router-dom";
import "./index.css";
import { useState, useEffect } from "react";
import {
  Select,
  DatePicker,
  Space,
  Table,
  Calendar,
  Button,
  Popover,
} from "antd";
import dayjs from "dayjs";
import moment from "moment";

const CalendarView = () => {
  let [Invoicedate, setInvoicedate] = useState("");
  let [dueDate, setdueDate] = useState("");
  let [response, setResponse] = useState([]);
  let [vesselName, setvesselName] = useState([]);
  let [companyName, setcompanyName] = useState([]);
  let [invoiceType, setInvoiceType] = useState("1");
  let [isLoading, setIsLoading] = useState(false);
  let [calendarMode, setcalndermode] = useState("month");
  let [filteredVesselName, setFilteredVesselName] = useState("");
  let [filteredCompanyName, setFilteredCompanyName] = useState("");
  let [filteredNewInvoiceDate, setfilteredNewInvoiceDate] = useState(null);
  const [clicked, setClicked] = useState(false);
  const [hovered, setHovered] = useState(false);
  const hide = () => {
    setClicked(false);
    setHovered(false);
  };
  const handleHoverChange = (open) => {
    setHovered(open);
    setClicked(false);
  };
  const handleClickChange = (open) => {
    setHovered(false);
    setClicked(open);
  };

  const onTypeChange = (value) => {
    let invcType = `${value}`;
    console.log("", invcType);
    setInvoiceType(invcType);
  };

  const onCompanyname = (value) => {
    let selectCompanyName = `${value}`;
    setFilteredCompanyName(selectCompanyName);
    let filteredcompanyList = response?.filter(
      (e) => e.my_company_name == selectCompanyName
    );
    setResponse(filteredcompanyList);
  };

  const InvoiceDateChange = (value) => {
    let Invoicedate = dayjs(value).format("YYYY-MM-DD");
    let filteredInvoicedate = response.filter(
      (e) => dayjs(e.invoice_date).format("YYYY-MM-DD") == Invoicedate
    );
    setInvoicedate(Invoicedate);
    setfilteredNewInvoiceDate(filteredInvoicedate);
    setResponse(filteredInvoicedate);
  };
  const DueDateChange = (value) => {
    const newDueDate = value
      ? dayjs(value).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD");
    setdueDate(newDueDate);
  };
  const onvesselChange = (label) => {
    let selectedVesselName = `${label}`;
    const filteredDataVessel = response.filter(
      (e) => e.vessel_name == selectedVesselName
    );
    setFilteredVesselName(filteredDataVessel);
    setResponse(filteredDataVessel);
  };

  // Filter `option.label` match the user type `input`
  const filterOption = (input, option) =>
    (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

  useEffect(() => {
    getVesselname();
    getCompanyName();
  }, []);

  useEffect(() => {
    getCalendarData();
  }, [dueDate, invoiceType, filteredNewInvoiceDate]);

  // To get the calendar view Data
  const getCalendarData = async () => {
    setIsLoading(true);
    const resp = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/calender-view?i=${invoiceType}&d=${dueDate}`
    );
    setResponse(resp.data);
    setIsLoading(false);
  };

  // To get the Vessel Name list
  const getVesselname = async () => {
    const vesselNameList = await getAPICall(
      `${URL_WITH_VERSION}/vessel/list?l=0`
    );
    setvesselName(vesselNameList.data);
  };
  // To get the Company name list
  const getCompanyName = async () => {
    const companyList = await getAPICall(
      `${URL_WITH_VERSION}/address/list?l=0`
    );
    setcompanyName(companyList.data);
  };

  const columns = [
    {
      title: "Invoice No.",
      dataIndex: "invoice_no",
      key: "1",
    },
    {
      title: "Voyage No.",
      dataIndex: "voyage_number",
      key: "2",
      render: (voy_no) => (
        <Link target="_blank" to={`/voyage-manager/${voy_no}`}>
          {voy_no}
        </Link>
      ),
    },
    {
      title: "Vessel Name",
      dataIndex: "vessel_name",
      key: "3",
    },
    {
      title: "Company Name",
      dataIndex: "my_company_name",
      key: "4",
    },
    {
      title: "Invoice Type",
      dataIndex: "Invoice_type",
      key: "5",
    },
    {
      title: "Invoice Date",
      dataIndex: "invoice_date",
      key: "6",
      render: (invcDate) => dayjs(invcDate).format("YYYY-MM-DD"),
    },
    {
      title: "Invoice Due Date",
      dataIndex: "due_date",
      key: "7",
      render: (invcDate) => dayjs(invcDate).format("YYYY-MM-DD"),
    },
    {
      title: "Invoice Amount",
      dataIndex: "invoice_amount",
      key: "8",
    },
  ];
  const modalColumn = [
    {
      title: "Invoice No.",
      dataIndex: "invoice_no",
      key: "1",
    },
    {
      title: "Voyage No.",
      dataIndex: "voyage_number",
      key: "2",
    },
    {
      title: "Invoice Due Date",
      dataIndex: "due_date",
      key: "3",
      render: (invcDate) => dayjs(invcDate).format("YYYY-MM-DD"),
    },
    {
      title: "Invoice Amount",
      dataIndex: "invoice_amount",
      key: "4",
    },
  ];

  const handleViewChange = (mode) => {
    setcalndermode(mode);
  };

  console.log("response data", response);
  const today = moment().format("DD MMMM YYYY");
  const hoverContent = <div>Click to see details...</div>;
  const clickContent = (
    <div>
      <Table
        className="popoverTable"
        placement="bottom"
        dataSource={response}
        pagination={{
          pageSize: 10,
          pageSizeOptions: ["10", "20", "30"],
        }}
        columns={modalColumn}
      />
    </div>
  );

  const cellRender = (value) => {
    const isSelectedDate = dayjs(value).isSame(dueDate, "day");
    return (
      <div className="miniPopover">
        {isSelectedDate && (
          <div className="lengthCount">
            <Popover
              style={{
                width: 500,
              }}
              content={hoverContent}
              trigger="hover"
              open={hovered}
              onOpenChange={handleHoverChange}
            >
              <Popover
                content={
                  <div>
                    {clickContent}
                    <a className="close_btn" onClick={hide}>X</a>
                  </div>
                }
                title="Details Available for the Selected Date"
                trigger="click"
                open={clicked}
                onOpenChange={handleClickChange}
              >
                <Button>{response?.length > 0 ? response?.length : 0}</Button>
              </Popover>
            </Popover>
          </div>
        )}
      </div>
    );
  };

  return (
    <div className="main_content">
      <div className="top_part">
        <h4 className="calendar_view_heading">Calendar View</h4>
        <br></br>
        <div className="select_box">
          <Select
            disabled={
              dueDate === null || !moment(dueDate).isValid() ? true : false
            }
            className="cv-select"
            showSearch
            placeholder="Select Vessel Name"
            onChange={onvesselChange}
            // onSearch={onSearch}
            filterOption={filterOption}
            options={vesselName.map((vesName, index) => {
              return {
                ...vesName,
                key: index,
                value: vesName.vessel_name,
                label: vesName.vessel_name.toUpperCase(),
              };
            })}
          />
          <Select
            className="cv-select"
            showSearch
            disabled={
              dueDate === null || !moment(dueDate).isValid() ? true : false
            }
            placeholder="Select Company Name"
            onChange={onCompanyname}
            filterOption={filterOption}
            options={companyName.map((mycompany, id) => {
              return {
                ...mycompany,
                key: { id },
                value: mycompany.full_name,
                label: mycompany.full_name,
              };
            })}
          />
          <Select
            className="cv-select"
            style={{ width: "220px" }}
            showSearch
            placeholder="Select Invoice Type*"
            onChange={onTypeChange}
            filterOption={filterOption}
            options={[
              {
                value: 1,
                label: "Frieght Invoice",
              },
              {
                value: 2,
                label: "Frieght Commision",
              },
              {
                value: 3,
                label: "Other Expense",
              },
              {
                value: 4,
                label: "Bunker Invoice",
              },
              {
                value: 5,
                label: "PDA Invoice",
              },
              {
                value: 6,
                label: "FDA invoice",
              },
              {
                value: 7,
                label: "Claim Invoice",
              },
              {
                value: 8,
                label: "Offhire Deviation Invoice",
              },
              {
                value: 9,
                label: "TCI Payments",
              },
              {
                value: 10,
                label: "TCO Payments",
              },
            ]}
          />

          <div className="cv-datepicker-box">
            <Space direction="horizontal" size={12} style={{ display: "flex" }}>
              <DatePicker
                disabled={
                  dueDate === null || !moment(dueDate).isValid() ? true : false
                }
                className="cv-datepicker"
                onChange={InvoiceDateChange}
                placeholder="Select Invoice Date"
                style={{ width: "175px" }}
                format={"YYYY-MM-DD"}
              />
              <DatePicker
                className="cv-datepicker"
                onChange={DueDateChange}
                placeholder="Select Due Date*"
                style={{ width: "155px" }}
                format={"YYYY-MM-DD"}
              />
            </Space>
          </div>
          <button className="search_button" onClick={getCalendarData}>
            Search
          </button>
        </div>
      </div>
      <div className="below_part">
        <div className="left_view">
          <div className="mini_calendar_date">
            {dueDate
              ? moment(dueDate).format("MMMM YYYY")
              : moment(today).format("MMMM YYYY")}
          </div>
          <Calendar
            value={dueDate ? dayjs(dueDate) : null}
            fullscreen={false}
          />
        </div>
        <div className="right_view">
          {console.log("calendar before", dueDate)}
          <div></div>
          <div className="calendar-controls">
            <div className="SelectedDate">
              {dueDate ? moment(dueDate).format("DD MMMM YYYY") : today}
            </div>
            <div className="calendar-buttons">
              <button
                className={`modebtn ${
                  calendarMode === "day" ? "activeMode" : ""
                } `}
                onClick={() => handleViewChange("day")}
              >
                Day
              </button>
              <button
                className={`modebtn ${
                  calendarMode === "week" ? "activeMode" : ""
                } `}
                onClick={() => handleViewChange("week")}
              >
                Week
              </button>
              <button
                className={`modebtn ${
                  calendarMode === "month" ? "activeMode" : ""
                } `}
                onClick={() => handleViewChange("month")}
              >
                Month
              </button>
              <button
                className={`modebtn ${
                  calendarMode === "year" ? "activeMode" : ""
                } `}
                onClick={() => handleViewChange("year")}
              >
                Year
              </button>
            </div>
          </div>
          <Calendar
            value={dueDate ? dayjs(dueDate) : null}
            // onChange={handleDateChange}
            mode={calendarMode}
            cellRender={cellRender}
          />
        </div>
      </div>
      <div className="belowtable">
        <Table
          dataSource={
            filteredNewInvoiceDate?.length > 0
              ? filteredNewInvoiceDate
              : response
          }
          columns={columns}
          loading={isLoading}
          pagination={{
            pageSize: 15,
          }}
          footer={false}
        />
      </div>
    </div>
  );
};

export default CalendarView;
