import React, { Component } from "react";
import { Table, Modal, Popconfirm, Col, Row, Select } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  apiDeleteCall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../shared";
import BunkerPe from "../voyage-list/components/BunkerPe";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { FIELDS } from "../../shared/tableFields";

import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import { useNavigate } from "react-router-dom";
import { useRef } from "react";
import { useEffect } from "react";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import LineChart from "../dashboard/charts/LineChart";
import PieChart from "../dashboard/charts/PieChart";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";

const FdaList = (props) => {
  const [state, setState] = useStateCallback({
    sidebarVisible: false,
    isshowportexpensedetails: false,
    columns: [],
    responseData: null,
    loading: true,
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    editportexid: null,
    voyID: props.voyID || null,
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });

  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  const navigate = useNavigate();
  const tableref = useRef();

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: "70",

      render: (el, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper edit"
              onClick={(ev) => port_expenses_detail(true, record.id)}
            >
              <EditOutlined />
            </span>

            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["fda-list"] ? FIELDS["fda-list"]["tableheads"] : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData(state.voyID);
    });
  }, []);

  const port_expenses_detail = async (showportexpensedetails, voyid = null) => {
    if (voyid !== null) {
      setState((prevState) => ({
        ...prevState,
        editportexid: voyid,
        isshowportexpensedetails: showportexpensedetails,
        voyId: voyid,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isshowportexpensedetails: showportexpensedetails,
      }));
    }
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/fdalist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const onRowDeletedClick = (id) => {
    const { voyID } = state;

    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/delete?e=${id}`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(voyID);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const handleclosePEDetail = (showPeDetail) => {
    setState({ ...state, isshowportexpensedetails: showPeDetail }, () =>
      getTableData()
    );
  };
  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const {
    isshowportexpensedetails,
    responseData,
    loading,
    pageOptions,
    search,
    columns,
    editportexpenseData,
    sidebarVisible,
    editportexid,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "ALKIVIADIS",
    "Stephy",
    "Anr shipping",
    "GSN SHIPPING",
    "JD CAPET",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amounts",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "BUNK-PE-00243",
    "BUNK-PE-00242",
    "BUNK-PE-00241",
    "BUNK-PE-00240",
    "BUNK-PE-00239",
  ];
  const PieChartData = [
    { value: 20, name: "Prepared" },
    { value: 10, name: "Verified" },
    { value: 5, name: "Approved" },
    { value: 20, name: "Posted" },
  ];

  const StackHorizontalChartyAxis2 = [
    "TCE09-23-00909",
    "TCE09-23-00914",
    "TCE09-23-00928",
    "TCE09-23-00940",
  ];
  const StackHorizontalChartSeries2 = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
  ];
  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];
  return (
    <>
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title" />
              </div>
            </div>
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {loading === false ? (
                <ToolbarUI
                  routeUrl={"fda-list-toolbar"}
                  optionValue={{
                    pageOptions: pageOptions,
                    columns: columns,
                    search: search,
                  }}
                  showGraph={showGraphs}
                  callback={(e) => callOptions(e)}
                  // here pdf is downloading wrong  may be backend problem.
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "fda-list"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "fda-list"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "fda-list"),
                    },
                  ]}
                />
              ) : undefined}
            </div>
            <div>
              <Table
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                columns={tableColumns}
                scroll={{ x: "max-content" }}
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? "table-striped-listing"
                    : "dull-color table-striped-listing"
                }
              />
            </div>
          </div>
        </div>
      </article>

      {sidebarVisible ? (
        <SidebarColumnFilter
          columns={columns}
          sidebarVisible={sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
      {isshowportexpensedetails ? (
        <Modal
          className="page-container"
          style={{ top: "4%" }}
          title="Port Expense Summary"
          open={isshowportexpensedetails}
          // onOk={handleOk}
          onCancel={() => port_expenses_detail(false)}
          width="90%"
          footer={null}
        >
          <BunkerPe
            id={editportexid}
            modalCloseEvent={(showPeDetail) =>
              handleclosePEDetail(showPeDetail)
            }
          />
        </Modal>
      ) : undefined}
      <Modal
        title="FDA Dashboard"
        open={state.isGraphModal}
        //  onOk={handleOk}
        width="90%"
        onCancel={handleCancel}
        footer={null}
      >
        <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
          <Col
            xs={24}
            sm={8}
            md={8}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Vessels</p>
              <p>300</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Amount</p>
              <p>300 $</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Invoice</p>
              <p>240</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Vessel Name</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "PACIFIC EXPLORER",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "OCEANIC MAJESTY",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "CS HANA",
                  label: "CS HANA",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Invoice No</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Account Type</p>
            <Select
              placeholder="Payable"
              optionFilterProp="children"
              options={[
                {
                  value: "Payable",
                  label: "Payable",
                },
                {
                  value: "Receivable",
                  label: "Receivable",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Status</p>
            <Select
              placeholder="PREPARED"
              optionFilterProp="children"
              options={[
                {
                  value: "PREPARED",
                  label: "PREPARED",
                },
                {
                  value: "APPROVED",
                  label: "APPROVED",
                },
                {
                  value: "POSTED",
                  label: "POSTED",
                },
                {
                  value: "VERIFIED",
                  label: "VERIFIED",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Date To</p>
            <Select
              placeholder="2024-02-18"
              optionFilterProp="children"
              options={[
                {
                  value: "2024-01-26",
                  label: "2024-01-26",
                },
                {
                  value: "2023-11-16",
                  label: "2023-11-16",
                },
                {
                  value: "2023-11-17",
                  label: "2023-11-17",
                },
              ]}
            ></Select>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Dashboard BarChart"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"350"}
            />
          </Col>
          <Col span={12}>
            <LineChart
              LineCharSeriesData={LineCharSeriesData}
              LineCharxAxisData={LineCharxAxisData}
              Heading={"Total Amount Per Invoice No."}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <PieChart
              PieChartData={PieChartData}
              Heading={"Fda Amount on the basis of Status"}
            />
          </Col>
          <Col span={12}>
            <StackHorizontalChart
              Heading={"Total Fda Amount Per Voyage Number"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis2}
              StackHorizontalChartSeries={StackHorizontalChartSeries2}
            />
          </Col>
        </Row>
      </Modal>
    </>
  );
};

export default FdaList;
