import React, { Component } from 'react';
import { Table, Input } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 6; i++) {
  data.push({
    key: i.toString(),
    vessel: 'Vessel',
    invoice_no: 'Invoice No',
    invoice_type: 'Invoice Type',
    due_date: 'Due Date',
    ap_ar: 'AP/AR',
    invoice_status: 'Invoice Status',
    tde_status: 'TDE Status',
    counter_party: 'Counter Party',
    bill_to: 'Bill To',
    bill_by: 'Bill By',
    my_company: 'My Company',
    inv_amount: 'Inv Amount',
  });
}

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable ? (
      <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
    ) : (
      value
    )}
  </div>
);
// End table section

class DeleteInvoice extends Component {
  constructor(props) {
    super(props);
    // Start table section
    this.columns = [
      {
        title: 'Vessel',
        dataIndex: 'vessel',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'vessel'),
      },
      {
        title: 'Invoice No',
        dataIndex: 'invoice_no',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'invoice_no'),
      },
     
      {
        title: 'Invoice Type',
        dataIndex: 'invoice_type',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'invoice_type'),
      },
      {
        title: 'Due Date',
        dataIndex: 'due_date',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'due_date'),
      },
      {
        title: 'AP/AR',
        dataIndex: 'ap_ar',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'ap_ar'),
      },
      {
        title: 'Invoice Status',
        dataIndex: 'invoice_status',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'invoice_status'),
      },

      {
        title: 'TDE Status',
        dataIndex: 'tde_status',
        // width: 150,vessel
        render: (text, record) => this.renderColumns(text, record, 'tde_status'),
      },

      {
        title: 'Counter Party',
        dataIndex: 'counter_party',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'counter_party'),
      },

      {
        title: 'Bill To',
        dataIndex: 'bill_to',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'bill_to'),
      },

      {
        title: 'Bill By',
        dataIndex: 'bill_by',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'my_company'),
      },

      {
        title: 'My Company',
        dataIndex: 'my_company',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'my_company'),
      },

      {
        title: 'INV Amount',
        dataIndex: 'inv_amount',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'inv_amount'),
      },
    ];
    this.state = { data };
    this.cacheData = data.map(item => ({ ...item }));
    // End table section
  }

  // Start table section
  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  edit(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ data: newData });
    }
  }

  save(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  cancel(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ data: newData });
    }
  }
  // End table section

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Table
                bordered
                dataSource={this.state.data}
                columns={this.columns}
                scroll={{ x: 'max-content' }}
                size="small"
                pagination={false}
                owClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }Post
                footer={false}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default DeleteInvoice;
