import React, { useRef, useState, useEffect } from "react";
import ReactECharts from "echarts-for-react";
import "echarts/theme/macarons";

const PieChart = ({ Heading, PieChartData, selectedMode, pieChartOptions,name }) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <ReactECharts
        option={ pieChartOptions || {
          tooltip: {
            trigger: "item",
            formatter: "{a} <br/>{b}: {c}",
          },
          grid: {
            left: 30,
            right: 30,
            bottom: 3,
            top: 5,
            containLabel: true,
          },
          // legend: {
          //   orient: "vertical",
          //   left: "left",
          // },
          series: [
            {
              type: "pie",
              radius: "65%",
              selectedMode: selectedMode,
              name:name,
              data: PieChartData,
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: "rgba(0, 0, 0, 0.5)",
                },
              },
            },
          ],
        }}
        style={{ height: "330px" }}
        ref={eChartsRef}
        onEvents={{
          resize: () => {
            if (eChartsRef && eChartsRef.current) {
              eChartsRef.current.getEchartsInstance().resize();
            }
          },
        }}
      />
    </div>
  );
};

export default PieChart;
