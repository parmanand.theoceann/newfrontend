import React, { useEffect, useRef, useState } from "react";
import ReactECharts from "echarts-for-react";
import "echarts/theme/macarons";

const NestedPieChart = ({ Heading, NestedPieChartData, selectedMode }) => {
  const [state, setState] = useState({
    bar1: {
      option: {
        tooltip: {
          trigger: "item",
          formatter: "{a} <br/>{b}: {c}%",
        },
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 5,
          containLabel: true,
        },
        // legend: {
        //   // orient: "vertical",
        //   // left: "left",
        // },
        series: [
          {
            type: "pie",
            radius: "45%",
            selectedMode: selectedMode,
            name: "Total Voyage days",
            data: NestedPieChartData,
            // [
            //   { value: 40, name: 'Vessel 1' },
            //   { value: 30, name: 'Vessel 2' },
            //   { value: 13, name: 'Vessel 3' },
            //   { value: 10, name: 'Vessel 4' },
            //   { value: 7, name: 'Vessel 5' },
            // ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
            label: {
              // Add label configuration here
              show: true,
              formatter: "{c}", // {b} refers to the name of each slice
              position: "inside", // Change this according to your preference
            },
          },
          {
            name: "Access From",
            type: "pie",
            radius: ["50%", "90%"],
            labelLine: {
              length: 50,
            },

            data: [
              { value: 234, name: "Union Ads" },
              { value: 147, name: "Bing" },
              { value: 135, name: "Video Ads" },
              { value: 102, name: "Others" },
            ],
            label: {
              // Add label configuration here
              show: true,
              formatter: "{c}", // {b} refers to the name of each slice
              position: "inside", // Change this according to your preference
            },
          },
        ],
      },
      legend: {
        orient: "horizontal",
        left: "left",
      },
    },
  });
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      {/* <div className="box-body"> */}
      <ReactECharts
        ref={eChartsRef}
        onEvents={{
          resize: () => {
            if (eChartsRef && eChartsRef.current) {
              eChartsRef.current.getEchartsInstance().resize();
            }
          },
        }}
        option={state.bar1.option}
        style={{ height: "300px" }}
      />
      {/* </div> */}
    </div>
  );
};

export default NestedPieChart;
