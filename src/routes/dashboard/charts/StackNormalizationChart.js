import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";

const StackGoupColumnChart = ({
  Heading,
  StackNormalizationSeriesData,
  StackNormalizationxAxisData,
  stackChartOptions,
}) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);
  const [state, setState] = useState(
    stackChartOptions || {
      bar1: {
        option: {
          xAxis: {
            type: "category", // Switched back to category type for vertical bars
            data: StackNormalizationxAxisData ?? null,
            // ["TCOVFULL304", "TCOVFULL305", "TCOVFULL306", "TCOVFULL307", "TCOVFULL308"],
          },
          yAxis: {
            type: "value",
            min: 0,
            max: 100,
            axisLabel: {
              formatter: "{value}k", // Show percentage on axis
            },
          },
          tooltip: {
            trigger: "axis",
            axisPointer: {
              type: "shadow",
            },
          },
          grid: {
            left: 30,
            right: 30,
            bottom: 3,
            top: 10,
            containLabel: true,
          },
          series: StackNormalizationSeriesData,
        },
      },
    }
  );

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      {/* <div className="box-body"> */}
      <ReactEcharts
        ref={eChartsRef}
        onEvents={{
          resize: () => {
            if (eChartsRef && eChartsRef.current) {
              eChartsRef.current.getEchartsInstance().resize();
            }
          },
        }}
        option={state.bar1.option}
        theme={"macarons"}
        style={{ height: "330px" }}
      />
      {/* </div> */}
    </div>
  );
};

export default StackGoupColumnChart;
