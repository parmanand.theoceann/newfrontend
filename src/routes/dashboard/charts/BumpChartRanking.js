import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";

const BumpChartRanking = ({ Heading }) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);
  const [state, setState] = useState({
    bar1: {
      option: {
        // title: {
        //     text: 'Bump Chart (Ranking)'
        // },
        tooltip: {
          trigger: "item",
        },
        grid: {
          left: 30,
          right: 110,
          bottom: 3,
          top: 15,
          containLabel: true,
        },
        // toolbox: {
        //     feature: {
        //         saveAsImage: {}
        //     }
        // },
        xAxis: {
          type: "category",
          splitLine: {
            show: true,
          },
          axisLabel: {
            margin: 30,
            fontSize: 16,
          },
          boundaryGap: false,
          data: ["qty2", "qty4", "qty6", "qty8", "qty10"],
        },
        yAxis: {
          type: "value",
          axisLabel: {
            margin: 0,
            fontSize: 16,
          },

          interval: 10,
          min: 0,
        },
        series: [
          {
            name: "IFO",
            symbolSize: 10,
            type: "line",
            smooth: true,
            emphasis: {
              focus: "series",
            },
            endLabel: {
              show: true,
              formatter: "{a}",
              distance: 20,
            },
            lineStyle: {
              width: 4,
            },
            data: [10, 15, 30, 35, 40, 50],
          },
          {
            name: "VLSFO",
            symbolSize: 10,
            type: "line",
            smooth: true,
            emphasis: {
              focus: "series",
            },
            endLabel: {
              show: true,
              formatter: "{a}",
              distance: 20,
            },
            lineStyle: {
              width: 4,
            },
            data: [10, 15, 20, 25, 30, 35],
          },
          {
            name: "LSMGO",
            symbolSize: 10,
            type: "line",
            smooth: true,
            emphasis: {
              focus: "series",
            },
            endLabel: {
              show: true,
              formatter: "{a}",
              distance: 20,
            },
            lineStyle: {
              width: 4,
            },
            data: [5, 10, 15, 20, 25, 40, 50],
          },
          {
            name: "MGO",
            symbolSize: 10,
            type: "line",
            smooth: true,
            emphasis: {
              focus: "series",
            },
            endLabel: {
              show: true,
              formatter: "{a}",
              distance: 20,
            },
            lineStyle: {
              width: 4,
            },
            data: [5, 15, 25, 30, 35, 40, 45, 50],
          },
          {
            name: "ULSFO",
            symbolSize: 10,
            type: "line",
            smooth: true,
            emphasis: {
              focus: "series",
            },
            endLabel: {
              show: true,
              formatter: "{a}",
              distance: 20,
            },
            lineStyle: {
              width: 4,
            },
            data: [5, 10, 20, , 30, , 40, 50],
          },
        ],
      },
    },
  });

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          ref={eChartsRef}
          onEvents={{
            resize: () => {
              if (eChartsRef && eChartsRef.current) {
                eChartsRef.current.getEchartsInstance().resize();
              }
            },
          }}
          option={state.bar1.option}
          theme={"macarons"}
          style={{ height: "300px" }}
        />
      </div>
    </div>
  );
};

export default BumpChartRanking;
