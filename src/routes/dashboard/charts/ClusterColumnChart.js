import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";

const formatNumber = (number) => {
  const abbrev = ["K", "M", "B", "T"];

  if (number === 0 || number < 0) {
    return number.toString();
  }

  const magnitude = Math.floor(Math.log10(Math.abs(number)) / 3);
  const normalized = number / (1000 ** magnitude);
  return fixedFloat(normalized.toFixed(1), 1) + abbrev[magnitude - 1];
};

const fixedFloat = (number, decimals) => {
  const factor = 10 ** decimals;
  return Math.floor(number * factor) / factor;
};

const ClusterColumnChart = ({
  Heading,
  maxValueyAxis, 
  minValueyAxis,
  ClusterDataxAxis,
  ClusterDataSeries,
  clusterChartOption,
}) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);
  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);

  const formatYAxisLabels = (value) => {
    // Adjust threshold based on your expected data range
    const threshold = 1000; // Format numbers above this value
    return value >= threshold ? formatNumber(value) : value;
  };

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          ref={eChartsRef}
          onEvents={{
            resize: () => {
              if (eChartsRef && eChartsRef.current) {
                eChartsRef.current.getEchartsInstance().resize();
              }
            },
          }}
          option={{
            xAxis: {
              type: "category",
              data: ClusterDataxAxis,
              axisLabel: {
                rotate: 45,
                fontSize: 12,
              },
            },
            tooltip: {
              trigger: "axis",
              axisPointer: {
                type: "shadow",
              },
            },
            grid: {
              left: 30,
              right: 30,
              bottom: 3,
              top: 10,
              containLabel: true,
            },
            yAxis: {
              type: "value",
              axisLabel: {
                // Use formatYAxisLabels function to format labels
                formatter: formatYAxisLabels,
              },
            },
            series: ClusterDataSeries,
          }}
          theme={"macarons"}
        />
      </div>
    </div>
  );
};

export default ClusterColumnChart;
