import React, { useState, useRef, useEffect } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";

const FunnelChart = ({ Heading }) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);
  const [state, setState] = useState({
    bar1: {
      option: {
        title: {
          // text: "Funnel",
        },
        tooltip: {
          trigger: "item",
          formatter: "{a} <br/>{b} : {c}%",
        },
        series: [
          {
            name: "Net Amount",
            type: "funnel",
            left: "10%",
            top: 60,
            bottom: 60,
            width: "80%",
            min: 0,
            max: 100,
            minSize: "0%",
            maxSize: "100%",
            sort: "descending",
            gap: 2,
            label: {
              show: true,
              position: "inside",
            },
            labelLine: {
              length: 10,
              lineStyle: {
                width: 1,
                type: "solid",
              },
            },
            itemStyle: {
              borderColor: "#fff",
              borderWidth: 1,
            },
            emphasis: {
              label: {
                fontSize: 20,
              },
            },
            data: [
              { value: 100, name: "150k" },
              { value: 80, name: "90k" },
              { value: 60, name: "82k" },
              { value: 50, name: "71k" },
              { value: 40, name: "60k" },
            ],
          },
        ],
      },
    },
  });
  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
         ref={eChartsRef}
         onEvents={{
           resize: () => {
             if (eChartsRef && eChartsRef.current) {
               eChartsRef.current.getEchartsInstance().resize();
             }
           },
         }}
          option={state.bar1.option}
          theme={"macarons"}
        />
      </div>
    </div>
  );
};

export default FunnelChart;
