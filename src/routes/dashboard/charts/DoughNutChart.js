import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
import CHARTCONFIG from "./chartConfig";

const DoughNutChart = ({
  Heading,
  value,
  legendhorizontal,
  DoughNutChartSeriesData,
  DoughNutChartSeriesRadius,
  DoughNutChartSeriesName,
  options,
  style
}) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      {/* <div className="box-body"> */}
      <ReactEcharts
        ref={eChartsRef}
        onEvents={{
          resize: () => {
            if (eChartsRef && eChartsRef.current) {
              eChartsRef.current.getEchartsInstance().resize();
            }
          },
        }}
        option={options || {
          tooltip: {
            trigger: "item",
            formatter: "{a} <br/>{b}: {c}",
          },
          grid: {
            left: 30,
            right: 30,
            bottom: 10,
            top: 10,
            containLabel: true
          },
          series: [
            {
              name: "Fleet Ratings",
              type: "pie",
              radius: ["20%", "70%"],
              avoidLabelOverlap: true,
              label: {
                show: true,
                position: "outside",
                formatter: "{b}: {d}%",
              },
              labelLine: {
                show: true,
                length: 10,
              },
              emphasis: {
                label: {
                  show: true,
                  fontSize: '12',
                },
              },
              data: DoughNutChartSeriesData,
            },
          ],
        }
        }
        theme={"macarons"}
        style={style || { height: "330px" }}
      />
    </div>
    // </div>
  );
};

export default DoughNutChart;
