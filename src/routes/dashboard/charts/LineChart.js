import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";

const LineChart = ({ Heading, LineCharSeriesData, LineCharxAxisData, lineChartOption }) => {
 
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);

  // const [state, setState] = useState( lineChartOption || {
  //   bar1: {
  //     option: {
  //       tooltip: {
  //         trigger: "axis",
  //         axisPointer: {
  //           // Use axis to trigger tooltip
  //           type: "shadow", // 'shadow' as default; can also be 'line' or 'shadow'
  //         },
  //       },
  //       xAxis: {
  //         type: "category",
  //         data: LineCharxAxisData,
  //         //   ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  //         axisTick: {
  //           alignWithLabel: true,
  //         },
  //         axisLabel: {
  //           rotate: 25, // Rotate labels by 45 degrees
  //         },
  //       },
  //       grid: {
  //         left: 30,
  //         right: 30,

  //         bottom: 10,
  //         top: 10,
  //         containLabel: true,
  //       },
  //       yAxis: {
  //         type: "value",
  //         axisLabel: {
  //           formatter: "{value}k",
  //         },
  //       },
  //       series: [
  //         {
  //           data: LineCharSeriesData,
  //           // [150, 230, 224, 218, 135, 147, 260],
  //           type: "line",
  //         },
  //       ],
  //     },
  //   },
  // });
  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          ref={eChartsRef}
          onEvents={{
            resize: () => {
              if (eChartsRef && eChartsRef.current) {
                eChartsRef.current.getEchartsInstance().resize();
              }
            },
          }}
          option={lineChartOption ||  {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                // Use axis to trigger tooltip
                type: "shadow", // 'shadow' as default; can also be 'line' or 'shadow'
              },
            },
            xAxis: {
              type: "category",
              data: LineCharxAxisData,
              //   ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
              axisTick: {
                alignWithLabel: true,
              },
              axisLabel: {
                rotate: 25, // Rotate labels by 45 degrees
              },
            },
            grid: {
              left: 30,
              right: 30,
    
              bottom: 10,
              top: 10,
              containLabel: true,
            },
            yAxis: {
              type: "value",
              axisLabel: {
                formatter: "{value}",
              },
            },
            series: [
              {
                data: LineCharSeriesData,
                // [150, 230, 224, 218, 135, 147, 260],
                type: "line",
              },
            ],
          }}
          theme={"macarons"}
          style={{ height: "300px" }}
        />
      </div>
    </div>
  );
};

export default LineChart;
