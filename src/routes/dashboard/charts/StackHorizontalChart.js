import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
import CHARTCONFIG from "./chartConfig";

const StackHorizontalChart = ({
  Heading,
  StackHorizontalChartyAxis,
  StackHorizontalChartxAxis,
  StackHorizontalChartSeries,
  formatte,
}) => {
  const eChartsRef = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (eChartsRef.current) {
      setChartInstance(eChartsRef.current.getEchartsInstance());
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      if (chartInstance) {
        chartInstance.resize();
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [chartInstance]);
  const [state, setState] = useState(
     {
      bar1: {
        option: {
          tooltip: {
            trigger: "axis",
            axisPointer: {
              // Use axis to trigger tooltip
              type: "shadow", // 'shadow' as default; can also be 'line' or 'shadow'
            },
          },
          // legend: {},
          grid: {
            left: 30,
            right: 30,
            bottom: 3,
            top: 5,
            containLabel: true,
          },
          xAxis: {
            type: "value",
            data: StackHorizontalChartxAxis ?? null,
            axisLabel: {
              formatter:formatte ||  "{value}k",
            },
            
          },
          yAxis: {
            type: "category",
            // data: ["Western Europe", "Central America", "Oceania", "Southeastern Asia",],
            data: StackHorizontalChartyAxis ?? null,
          },
          series: StackHorizontalChartSeries,
          // series: [
          //   {
          //     name: "Total Sea Cons.",
          //     type: "bar",
          //     // stack: "total",
          //     label: {
          //       show: true,
          //     },
          //     emphasis: {
          //       focus: "series",
          //     },
          //     data: [320, 302, 301,599],
          //   },
          //   {
          //     name: "Total Port Cons.",
          //     type: "bar",
          //     // stack: "total",
          //     label: {
          //       show: true,
          //     },
          //     emphasis: {
          //       focus: "series",
          //     },
          //     data: [120, 132, 101,777],
          //   },
          //   {
          //     name: "Total Bunkar Cons.",
          //     type: "bar",
          //     // stack: "total",
          //     label: {
          //       show: true,
          //     },
          //     emphasis: {
          //       focus: "series",
          //     },
          //     data: [220, 182, 191,888],
          //   },

          // ],
        },
      },
    }
  );
  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <ReactEcharts
        ref={eChartsRef}
        onEvents={{
          resize: () => {
            if (eChartsRef && eChartsRef.current) {
              eChartsRef.current.getEchartsInstance().resize();
            }
          },
        }}
        option={state.bar1.option}
        theme={"macarons"}
        style={{ height: "330px" }}
      />
      {/* </div> */}
    </div>
  );
};

export default StackHorizontalChart;
