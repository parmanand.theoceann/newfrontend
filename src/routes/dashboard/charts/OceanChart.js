import React, { useEffect, useRef } from 'react';
import * as echarts from 'echarts';

const MyChartComponent = ({ newData }) => {
  const chartRef = useRef(null);

  useEffect(() => {
    // Initialize ECharts instance
    const myChart = echarts.init(chartRef.current);

    // Initial chart configuration
    const option = {
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: [{
        data: [120, 200, 150, 80, 70, 110, 130],
        type: 'bar'
      }]
    };

    // Set initial options
    myChart.setOption(option);

    // Return a cleanup function to destroy the chart when component unmounts
    return () => {
      myChart.dispose();
    };
  }, []); // Only run this effect once, on component mount

  useEffect(() => {
    // Update chart data when newData prop changes
    if (chartRef.current && newData) {
      chartRef.current.getEchartsInstance().setOption({
        series: [{
          data: newData
        }]
      });
    }
  }, [newData]);

  return <div ref={chartRef} style={{ width: '100%', height: '400px' }} />;
};

export default MyChartComponent;
