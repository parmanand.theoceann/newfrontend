import React, { useRef } from "react";
import ReactEcharts from "echarts-for-react";
const StackAreaChart = ({Heading}) => {
    return (
        <div className="box box-default mb-4">
          <div className="box-header text-center">{Heading}</div>
          <div className="box-body">
            <ReactEcharts
              option={{
                // title: {
                //   text: 'Stacked Area Chart'
                // },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    type: 'cross',
                    label: {
                      backgroundColor: '#6a7985'
                    }
                  }
                },
                // legend: {
                //   data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine']
                // },
                // toolbox: {
                //   feature: {
                //     saveAsImage: {}
                //   }
                // },
                grid: {
                  left: '5%',
                  right: '5%',
                  bottom: '3%',
                  containLabel: true
                },
                xAxis: [
                  {
                    type: 'category',
                    boundaryGap: false,
                    data: ['TCE11-23-01243', 'TCE09-23-00940', 'TCE01-24-01513', 'TCE10-23-01219', 'TCE10-23-01190',],
                    axisLabel: {
                        rotate: 45, // Rotate the labels by 45 degrees
                      },
                  }
                ],
                yAxis: [
                  {
                    type: 'value'
                  }
                ],
                series: [
                  {
                    name: 'Email',
                    type: 'line',
                    stack: 'Total',
                    areaStyle: {},
                    emphasis: {
                      focus: 'series'
                    },
                    data: [120, 132, 101, 134, 90, 230, 210]
                  },
                  {
                    name: 'Union Ads',
                    type: 'line',
                    stack: 'Total',
                    areaStyle: {},
                    emphasis: {
                      focus: 'series'
                    },
                    data: [220, 182, 191, 234, 290, 330, 310]
                  },
                  {
                    name: 'Video Ads',
                    type: 'line',
                    stack: 'Total',
                    areaStyle: {},
                    emphasis: {
                      focus: 'series'
                    },
                    data: [150, 232, 201, 154, 190, 330, 410]
                  },
                  {
                    name: 'Direct',
                    type: 'line',
                    stack: 'Total',
                    areaStyle: {},
                    emphasis: {
                      focus: 'series'
                    },
                    data: [320, 332, 301, 334, 390, 330, 320]
                  },
                  {
                    name: 'Search Engine',
                    type: 'line',
                    stack: 'Total',
                    label: {
                      show: true,
                      position: 'top'
                    },
                    areaStyle: {},
                    emphasis: {
                      focus: 'series'
                    },
                    data: [820, 932, 901, 934, 1290, 1330, 1320]
                  }
                ]
              }}
            />
          </div>
        </div>
      );
}

export default StackAreaChart