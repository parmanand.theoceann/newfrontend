import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";

const BarChart = () => {

  const [state, setState] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category",
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            data: [120, 200, 150, 80, 70, 110, 130],
            type: "bar",
          },
        ],
      },
    },
  });

  const eChartsRef = useRef(null);

  // useEffect(() => {
  //   if (eChartsRef && eChartsRef.current) {
  //     eChartsRef.current?.getEchartsInstance().setOption({
  //       xAxis: {
  //         type: "category",
  //         data: XaxisData,
  //       },
  //       yAxis: {
  //         type: "value",
  //         min: 0,
  //         max: maxValue ?? 1000,
  //       },
  //       series: [
  //         {
  //           data: YaxisData,
  //           type: "bar",
  //         },
  //       ],
  //     });
  //   }


  // }, [YaxisData, XaxisData]);

  return (
    <div className="box box-default mb-4">
      {/* <div className="box-header text-center">{Heading}</div> */}
      <div className="box-body">
        <ReactEcharts
          option={state.bar1.option}
          theme={"macarons"}
          ref={eChartsRef}
        />
      </div>
    </div>
  );
};

export default BarChart;
