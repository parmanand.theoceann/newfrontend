import React, { useRef } from "react";
import ReactEcharts from "echarts-for-react";

const ScatteredChart = ({Heading}) => {
  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          
          option={{
            xAxis: {
              type: "category",
              data : ['TCE02-24-01592', 'TCE11-23-01243', 'TCE11-23-01235', 'TCE11-23-01232', 'TCE10-23-01219'],
              axisLabel: {
                rotate: 45, // Rotate labels by 45 degrees
                fontSize: 10
              },
            },
            yAxis: {
              type: "value",
              min: 0 ?? 0,
              max: 10 ?? 1000,
              axisLabel: {
                formatter: "{value}k",
              },
            },
            series: [
              {
                symbolSize: 20,
                data: [
                  [12.2, 7.83],
                  [2.02, 4.47],
                  [1.05, 3.33],
                  [4.05, 4.96],
                  [6.03, 7.24],
                  [12.0, 6.26],
                  [12.0, 8.84],
                  [7.08, 5.82],
                  [5.02, 5.68],
                ],
                type: "scatter",
              },
            ],
          }}
          style={{height:"330px"}}
        />
      </div>
    </div>
  );
};

export default ScatteredChart;
