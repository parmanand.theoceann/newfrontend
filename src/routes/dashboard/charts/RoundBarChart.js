import React, { useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
// import CHARTCONFIG from "../../constants/chartConfig";

const RoundBarChart = ({
  Heading,
  fuelData,
  maxValueyAxis,
  RoundBarChartxAxis,
  RoundBarChartSeriesData,
}) => {
  const [state, setState] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category",
          data: RoundBarChartxAxis,
          // ["TCOVFull123", "TCOVFull124", "TCOVFull125", "TCOVFull126", "TCOVFull127"],
          axisLabel: {
            rotate: 45, // Rotate labels by 45 degrees
            fontSize: 12
          },
        },
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow",
          },
        },
        grid: {
          left: "3%",
          right: "4%",
          bottom: "3%",
          containLabel: true,
        },
        yAxis: {
          type: "value",
          min: 0,
          max: maxValueyAxis ?? 1000,
        },
        series: [
          {
            name: "Total Revenue",
            type: "bar",
            barGap: 0,
            itemStyle: {
              // Add this property to customize border radius
              borderRadius: [15, 15, 0, 0],
              color: function (params) {
                var colorList = [
                  "#c23531",
                  "#2f4554",
                  "#61a0a8",
                  "#d48265",
                  "#749f83",
                  "#ca8622",
                  "#bda29a",
                ];
                return colorList[params.dataIndex];
              },
            },
            data: RoundBarChartSeriesData,
            // [120, 200, 150, 80, 70, 110, 130],
          },
        ],
      },
    },
  });

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts option={state.bar1.option} theme={"macarons"} />
      </div>
    </div>
  );
};

export default RoundBarChart;
