import React, { useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
import CHARTCONFIG from "./chartConfig";


const StackedLineChart= ({ Heading, fuelData,maxValue }) => {

  const [state, setState] = useState({
    bar1: {
        option :{
            // title: {
            //   text: 'Stacked Line'
            // },
            tooltip: {
              trigger: 'axis'
            },
            legend: {
              data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine']
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            toolbox: {
              feature: {
                saveAsImage: {}
              }
            },
            xAxis: {
              type: 'category',
              boundaryGap: false,
              data: ['Cristobal', 'Ijmuiden', 'New Orleans', 'Vladivostok', 'Yokohama', 'Aberdeen', 'Balboa']
            },
            yAxis: {
              type: 'value'
            },
            series: [
              {
                name: 'IFO',
                type: 'line',
                stack: 'Total',
                data: [120, 132, 101, 134, 90, 230, 210]
              },
              {
                name: 'VLSFO',
                type: 'line',
                stack: 'Total',
                data: [220, 182, 191, 234, 290, 330, 310]
              },
              {
                name: 'LSMGO',
                type: 'line',
                stack: 'Total',
                data: [150, 232, 201, 154, 190, 330, 410]
              },
              {
                name: 'MGO',
                type: 'line',
                stack: 'Total',
                data: [320, 332, 301, 334, 390, 330, 320]
              },
              {
                name: 'ULSFO',
                type: 'line',
                stack: 'Total',
                data: [820, 932, 901, 934, 1290, 1330, 1320]
              }
            ]
          }
    },
  });

  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts option={state.bar1.option} theme={"macarons"} />
      </div>
    </div>
  );
};

export default StackedLineChart;
