import React, { useEffect, useRef, useState } from "react";
import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";
import { set } from "lodash";

const BarChart = ({ Heading, barData,XaxisData,YaxisData}) => {
const [state, setState] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category",
          data: [],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            data: [],
            type: "bar",
          },
        ],
      },
    },
  });
  
  useEffect(() => {
    console.log(barData,"bar Data ______________________________")
    if (barData && barData.length > 0) {
      setState(prevState => ({
        ...prevState,
        bar1: {
          ...prevState.bar1,
          option: {
            ...prevState.bar1.option,
            xAxis: {
              ...prevState.bar1.option.xAxis,
              data: XaxisData,
            },
            series: [
              {
                ...prevState.bar1.option.series[0],
                data: barData,
              },
            ],
          },
        },
      }));
    }
  }, [barData, XaxisData]); 
  

  const eChartsRef = useRef(null);

  // useEffect(() => {
  //   if (eChartsRef && eChartsRef.current) {
  //     eChartsRef.current?.getEchartsInstance().setOption({
  //       xAxis: {
  //         type: "category",
  //         data: XaxisData,
  //       },
  //       yAxis: {
  //         type: "value",
  //         min: 0,
  //         max: maxValue ?? 1000,
  //       },
  //       series: [
  //         {
  //           data: YaxisData,
  //           type: "bar",
  //         },
  //       ],
  //     });
  //   }


  // }, [YaxisData, XaxisData]);

  
  return (
    <div className="box box-default mb-4">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          option={state.bar1.option}
          theme={"macarons"}
          ref={eChartsRef}
          onEvents={{
            resize: () => {
              if (eChartsRef && eChartsRef.current) {
                eChartsRef.current.getEchartsInstance().resize();
              }
            },
          }}
        />
      </div>
    </div>
  );
};

export default BarChart;
