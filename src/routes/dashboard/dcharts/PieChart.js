import React, { Component, useState, useEffect, useRef } from "react";

import ReactEcharts from "echarts-for-react";
import "echarts/theme/macarons";

const PieChart = ({ Heading, pieData,CssHeight}) => {
  
  console.log('piedata', pieData)
  // const [state, setState] = useState({
  //   bar1: {
  //     option: {
  //       tooltip: {
  //         trigger: "item",
  //       },
  //       legend: {
  //         orient: "vertical",
  //         left: "left",
  //       },
  //       series: [
  //         {
  //           name: "Access From", // as
  //           type: "pie",
  //           radius: "50%",
  //           data: [],

  //           emphasis: {
  //             itemStyle: {
  //               shadowBlur: 10,
  //               shadowOffsetX: 0,
  //               shadowColor: "rgba(0, 0, 0, 0.5)",
  //             },
  //           },
  //         },
  //       ],
  //     },
  //   },
  // });
  const eChartsRef = useRef(null);

  // if (eChartsRef && eChartsRef.current) {
  //   eChartsRef.current?.getEchartsInstance().setOption(getOption());
  // }

  const getOption = () => {
    return {
      tooltip: {
        trigger: "item",
      },
      legend: {
        top: '5%',
        left: 'center'
      },
      series: [
        {
          name: "Access From", // as
          type: "pie",
          radius: "50%",
          data: pieData,

          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: "rgba(0, 0, 0, 0.5)",
            },
          },
        },
      ],
    };
  };

  useEffect(() => {
    if (eChartsRef && eChartsRef.current) {
      eChartsRef.current.getEchartsInstance().setOption(getOption());
    }
  }, [pieData]); 

  return (
    <div className="box box-default mb-4 p-3">
      <div className="box-header text-center">{Heading}</div>
      <div className="box-body">
        <ReactEcharts
          option={getOption()}
          style={{ height: CssHeight ?? 600 }}
          ref={eChartsRef}
          onEvents={{
            resize: () => {
              if (eChartsRef && eChartsRef.current) {
                eChartsRef.current.getEchartsInstance().resize();
              }
            },
          }}
        />
      </div>
    </div>
  );
};

export default PieChart;
