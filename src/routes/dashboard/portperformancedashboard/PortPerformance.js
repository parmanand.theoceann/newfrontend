import React, { useEffect, useState } from "react";
import PortPerformanceList from "./PortPerformanceList";
import GaugeChart from "../charts/GaugeChart";
import BarChart from "../charts/BarChart";
import Piechart from "../charts/PieChart";
import VoyageMap from "../map/VoyageMap";
import StackedLineChart from "../charts/StackedLineChart";
import StackHorizontalChart from "../charts/StackHorizontalChart";
import DoughNutChart from "../charts/DoughNutChart";
import PieChart from "../charts/PieChart";
import { Button, Card, Col, Row, Select } from "antd";
import ClusterColumnChart from "../charts/ClusterColumnChart";
const PortPerformance = () => {
  const [state, setState] = useState({
    chartdata: {
      dognutdata: [
        { value: 1048, name: "Mumbai" },
        { value: 735, name: "chennai" },
        { value: 580, name: "taman" },
        { value: 484, name: "sikka" },
        { value: 300, name: "singapore" },
      ],
      bardata: [120, 200, 150, 80, 70, 110, 130],
      piedata: [
        { value: 1048, name: "Search Engine" },
        { value: 735, name: "Direct" },
        { value: 580, name: "Email" },
        { value: 484, name: "Union Ads" },
        { value: 300, name: "Video Ads" },
      ],
    },
  });

  const [pieChartOptions, setPieChartOptions] = useState({
    bar1: {
      option: {
        // title: {
        //   text: "Net Amount Per Vessel",
        //   left: "center",
        // },
        tooltip: {
          trigger: "item",
        },
        series: [
          {
            name: "Access From",
            type: "pie",
            radius: "50%",
            data: [
              { value: 1048, name: "Flushing 30k" },
              { value: 735, name: "Galveston 12.4k" },
              { value: 580, name: "Nakhodka 32.1k" },
              { value: 484, name: "Balboa 54.33k" },
              { value: 300, name: "Cristobal 31.22k" },
            ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
          },
        ],
      },
      // labelLine: {
      //   show: true,
      //   length: 20,
      //   length2: 20,
      // },
    },
  });

  const DoughNutChartSeriesData = [
    { value: 50, name: "Hong Kong	56k" },
    { value: 13, name: "Houston 69k" },
    { value: 20, name: "Khorfakkan 44k" },
    { value: 10, name: "Galveston 32k" },
    { value: 7, name: "Balboa 23k" },
  ];

  const StackHorizontalChartyAxis2 = [
    "Galveston",
    "Nakhodka",
    "Yokohama",
    "Aberdeen",
    "Cristobal",
  ];

  const StackHorizontalChartSeries2 = [
    {
      name: "Port Day",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 334, 390, 330, 320],
    },
    {
      name: "Extra port Days",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 134, 90, 230, 210],
    },
  ];

  const ClusterDataxAxis = [
    "Rotterdam",
    "Fujairah",
    "Amsterdam",
    "Houston",
    "Khorfakkan",
  ];

  const ClusterDataSeries = [
    {
      name: "P Days",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
    {
      name: "XPD,",
      type: "bar",

      data: [220, 182, 191, 234, 290],
    },
    {
      name: "Ttl Port Days",
      type: "bar",
      data: [150, 232, 201, 154, 190],
    },
  ];

  // const [stackChartOptions, setStackChartOptions] = useState({
  //   tooltip: {
  //     trigger: "axis",
  //     axisPointer: {
  //       // Use axis to trigger tooltip
  //       type: "shadow", // 'shadow' as default; can also be 'line' or 'shadow'
  //     },
  //   },
  //   // legend: {},
  //   grid: {
  //     left: 30,
  //     right: 30,
  //     bottom: 3,
  //     top: 5,
  //     containLabel: true,
  //   },
  //   xAxis: {
  //     type: "value",
  //     data: StackHorizontalChartyAxis2 ?? null,
  //     axisLabel: {
  //       formatter: "{value}%",
  //     },

  //   },
  //   yAxis: {
  //     type: "category",
  //     // data: ["Western Europe", "Central America", "Oceania", "Southeastern Asia",],
  //     data: StackHorizontalChartSeries2 ?? null,
  //   },
  //   series: StackHorizontalChartSeries2,
  //   // series: [
  //   //   {
  //   //     name: "Total Sea Cons.",
  //   //     type: "bar",
  //   //     // stack: "total",
  //   //     label: {
  //   //       show: true,
  //   //     },
  //   //     emphasis: {
  //   //       focus: "series",
  //   //     },
  //   //     data: [320, 302, 301,599],
  //   //   },
  //   //   {
  //   //     name: "Total Port Cons.",
  //   //     type: "bar",
  //   //     // stack: "total",
  //   //     label: {
  //   //       show: true,
  //   //     },
  //   //     emphasis: {
  //   //       focus: "series",
  //   //     },
  //   //     data: [120, 132, 101,777],
  //   //   },
  //   //   {
  //   //     name: "Total Bunkar Cons.",
  //   //     type: "bar",
  //   //     // stack: "total",
  //   //     label: {
  //   //       show: true,
  //   //     },
  //   //     emphasis: {
  //   //       focus: "series",
  //   //     },
  //   //     data: [220, 182, 191,888],
  //   //   },

  //   // ],
  // })
  const StackHorizontalChartyAxis = [
    "Ijmuiden",
    "Bolivar Roads",
    "Yokohama",
    "Aberdeen",
    "Cristobal",
  ];
  const StackHorizontalChartSeries = [
    {
      name: "New Orleans",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 334, 390, 330, 320],
    },
    {
      name: "Ijmuiden",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "Terneuzen",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "Aberdeen",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "Amsterdam",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 134, 90, 230, 210],
    },
  ];

  return (
    <div className="body-wrapper">
      <div className="body-wrapper modalWrapper">
        {/*       
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p12">
                <div className="col-12">
                  <div className="card-group w-100 mt-4">
                    <div className="card">
                      <div className="card-body dashboard-col-details">
                        <h4 className="Char_dashboard_sub">Total FDA Amount</h4>
                        <h2 className="font-weight-bold mb-0">
                        </h2>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-body dashboard-col-details">
                        <h4 className="Char_dashboard_sub">Total PDA Amount</h4>
                        <h2 className="font-weight-bold mb-0">
                        </h2>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-body dashboard-col-details">
                        <h4 className="Char_dashboard_sub">
                          Total Port Expense
                        </h4>
                        <h2 className="font-weight-bold mb-0">
                        </h2>
                      </div>
                    </div>

                    <div className="card">
                      <div className="card-body dashboard-col-details">
                        <h4 className="Char_dashboard_sub">
                          some extra fields
                        </h4>
                        <h2 className="font-weight-bold mb-0">
                        </h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article> */}
        <Row gutter={16}>
          <Col span={3.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Total FDA Amount
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                300 $
              </p>
            </Card>
          </Col>
          <Col span={3.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Total PDA Amount
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                500 $
              </p>
            </Card>
          </Col>
          <Col span={3.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Total Port Expense
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                40 $ 
              </p>
            </Card>
          </Col>
          <Col span={3}>
            <p>Voyage No.</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            />
          </Col>
          <Col span={3}>
            <p>Vessel</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "ECHPIN2306",
                  label: "ECHPIN2306",
                },
                {
                  value: "LAYPAN2306",
                  label: "LAYPAN2306",
                },
                {
                  value: "BLUPIN2306",
                  label: "BLUPIN2306",
                },
              ]}
            />
          </Col>
          <Col span={3}>
            <p>Fuel grade</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            />
          </Col>
          <Col span={3}>
            <p>Port Name</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            />
          </Col>
          <Col span={0.5}>
            <Button>Reset</Button>
          </Col>
        </Row>

        {/*  sea consumption and port cons graph */}

        {/*  barchart fuel ordering  ["IFO", "LSMGO", "VLSFO", "ULSFO", "MGO"], */}

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-12">
                      <ClusterColumnChart
                        Heading={"P & L Per Voyage"}
                        ClusterDataxAxis={ClusterDataxAxis}
                        ClusterDataSeries={ClusterDataSeries}
                        maxValueyAxis={"500"}
                      />
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-12">
                      <PieChart
                        Heading={"No. Of Days Per Port"}
                        // pieData={state.chartdata.piedata}
                        pieChartOptions={pieChartOptions}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        {/*    port emision and sea emision gauge chart */}

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-12">
                      <DoughNutChart
                        DoughNutChartSeriesData={DoughNutChartSeriesData}
                        DoughNutChartSeriesRadius={["40%", "70%"]}
                        Heading={"PDA Amount Vs port"}
                      />
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-12">
                      <StackHorizontalChart
                        Heading={"FDA Amount Vs Port"}
                        StackHorizontalChartyAxis={StackHorizontalChartyAxis}
                        StackHorizontalChartSeries={StackHorizontalChartSeries}
                        formatte={"{value}%"}
                        // option={stackChartOptions}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="border">
                    <h4 className="mb-0  p-4 Char-dashboard">
                      Live vessel (Commenced/Delivered)
                    </h4>

                    <VoyageMap
                      maptype={"mapbox://styles/mapbox/satellite-streets-v12"}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="border p-3">
                    {/* <h4 className="mb-3 Char-dashboard">
                      Total Port Days Vs Extra Port Days
                    </h4> */}
                    <StackedLineChart
                      Heading={"Port Days Vs Extra  Port Days"}
                    />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="border p-3">
                    {/* <h4 className="mb-3 Char-dashboard">
                      Port Consumption By Fuel Type
                    </h4> */}
                    <StackHorizontalChart
                      Heading={"Bunker consp. At Port "}
                      StackHorizontalChartyAxis={StackHorizontalChartyAxis2}
                      StackHorizontalChartSeries={StackHorizontalChartSeries2}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        {/* <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="border p-3">
                    <h4 className="mb-3 Char-dashboard">
                      Fuel Type Consumption Trend
                    </h4>
                    <StackedLineChart />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article> */}
      </div>

      <PortPerformanceList />
    </div>
  );
};
export default PortPerformance;
