import React, { Component, useEffect, useRef, useState } from "react";
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import "../../../styles/style.scss";
import URL_WITH_VERSION, { IMAGE_PATH } from "../../../shared";

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass =
  require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

const VoyageMap = (props) => {
  const [state, setState] = useState({
    coordinates: [],
    featuresData: [],
    lng: props.data && props.data.length > 0 ? props.data[0].vessel_lat : 0.0,
    lat: props.data && props.data.length > 0 ? props.data[0].vessel_lon : 0.0,
    zoom: 0.5,
    visibleModal: false,
    data: props.mapData,
  });
  const mapObject = useRef(null);
  const mapContainer = useRef(null);

  useEffect(() => {
    maprender();
  }, []);

  const maprender = () => {
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "point",
            properties: {},
            coordinates: state.coordinates ? state.coordinates : [],
          },
        },
      ],
    };
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: props.maptype,
      center: { lat: 20.5937, lng: 78.9629 },
      zoom: 1.5
   
    });
    //   setState((prevState)=>({...prevState, map: map }));
    mapObject.current = map;

    // map.on("load", () => {
    //   // add mapbox terrain dem source for 3d terrain rendering
    //   // mapDiv.style.width = '100%';
    //   map.addSource("LineString", {
    //     type: "geojson",
    //     data: geojson,
    //   });

    //   map.on("click", (e) => {
    //     // console.log(e);
    //   });

    //   map.addLayer({
    //     id: "LineString",
    //     type: "line",
    //     source: "LineString",
    //     layout: {
    //       "line-join": "round",
    //       "line-cap": "round",
    //     },
    //     paint: {
    //       "line-color": "#2414E2",
    //       "line-width": 4,
    //       "line-dasharray": [0, 1.5],
    //     },
    //   });

    //   map.loadImage(IMAGE_PATH+"icons/mapArrowYellow.png", (error, image) => {
    //     if (error) {
    //       return;
    //     }
    //     map.addImage("yellowIcon", image);
    //   });

    //   map.loadImage(IMAGE_PATH+"icons/mapArrowUnknown.png", (error, image) => {
    //     if (error) {
    //       console.log(">>>>> ", error);
    //       return;
    //     }
    //     map.addImage("unknownIcon", image);
    //   });

    //   map.loadImage(IMAGE_PATH+"icons/mapArrowOrange.png", (error, image) => {
    //     if (error) {
    //       return;
    //     }
    //     map.addImage("orangeIcon", image);
    //   });

    //   map.loadImage(IMAGE_PATH+"icons/mapArrowGreen.png", (error, image) => {
    //     if (error) {
    //       return;
    //     }
    //     map.addImage("greenIcon", image);
    //   });

    //   map.loadImage(IMAGE_PATH+"icons/map-icon-pointer.png", (error, image) => {
    //     if (error) {
    //       console.log("Error loading", error);
    //       throw error;
    //     }
    //     map.addImage("custom-marker", image);

    //     console.log("xczczc", state.featuresData);

    //     map.addSource("places", {
    //       // This GeoJSON contains features that include an "icon"
    //       // property. The value of the "icon" property corresponds
    //       // to an image in the Mapbox Streets style's sprite.
    //       type: "geojson",
    //       data: {
    //         type: "FeatureCollection",
    //         features: state.featuresData,
    //       },
    //     });

    //     // Add a layer showing the places.
    //     map.addLayer({
    //       id: "places",
    //       type: "symbol",
    //       source: "places",
    //       layout: {
    //         "icon-image": "greenIcon",
    //         "icon-allow-overlap": true,
    //         "icon-rotate": ["get", "rotation"],
    //         "icon-ignore-placement": true,
    //       },
    //     });

    //     // Create a popup, but don't add it to the map yet.
    //     const hoverpopup = new mapboxgl.Popup({
    //       maxWidth: "10vw",
    //       closeButton: false,
    //       closeOnClick: false,
    //     });

    //     // When a click event occurs on a feature in the places layer, open a popup at the
    //     // location of the feature, with description HTML from its properties.
    //     map.on("click", "places", (e) => {
    //       // Copy coordinates array.
    //       try {
    //         const dataset = JSON.parse(e.features[0].properties.dataset) || {};
    //         if (Object.keys(dataset).length) {
    //           getVesselIdByImoNumber(dataset.imo_no, dataset, true);
    //         }

    //         const addedMarker = document.getElementById("dottedArrowMarker");

    //         if (addedMarker) {
    //           addedMarker.remove();
    //         }
    //         const markerImage = document.createElement("img");
    //         markerImage.src = IMAGE_PATH+"icons/dottedArrow.png";
    //         markerImage.width = 140;
    //         markerImage.height = 20;
    //         markerImage.id = "dottedArrowMarker";

    //         new mapboxgl.Marker(markerImage, {
    //           rotation: e.features[0].properties.rotation,
    //         })
    //           .setLngLat([
    //             parseFloat(dataset.last_pos_lon),
    //             parseFloat(dataset.last_pos_lat),
    //           ])
    //           .addTo(map);
    //       } catch (error) {
    //         console.log(">>> ERROR", error);
    //       }
    //     });

    //     map.addSource("liveVessel", {
    //       // This GeoJSON contains features that include an "icon"
    //       // property. The value of the "icon" property corresponds
    //       // to an image in the Mapbox Streets style's sprite.

    //       type: "geojson",
    //       data: {
    //         type: "FeatureCollection",
    //         features: [],
    //       },
    //     });
    //     // Add a layer showing the places.
    //     map.addLayer({
    //       id: "liveVessel",
    //       type: "symbol",
    //       source: "liveVessel",
    //       layout: {
    //         "icon-image": "yellowIcon",
    //         "icon-allow-overlap": true,
    //       },
    //     });
    //     map.on("click", "liveVessel", (e) => {
    //       // Copy coordinates array.
    //       try {
    //         const dataset = JSON.parse(e.features[0].properties.dataset) || {};
    //         if (Object.keys(dataset).length) {
    //           getVesselIdByImoNumber(dataset.imo_no, dataset, true);
    //         }
    //       } catch (error) {
    //         console.log(">>> ERROR", error);
    //       }
    //     });

    //     // Change the cursor to a pointer when the mouse is over the places layer.
    //     map.on("mouseenter", "places", (e) => {
    //       const mapCoordinates = e.features[0].geometry.coordinates.slice();
    //       const description = e.features[0].properties.hoverProperties;

    //       // Ensure that if the map is zoomed out such that multiple
    //       // copies of the feature are visible, the popup appears
    //       // over the copy being pointed to.
    //       while (Math.abs(e.lngLat.lng - mapCoordinates[0]) > 180) {
    //         mapCoordinates[0] += e.lngLat.lng > mapCoordinates[0] ? 360 : -360;
    //       }

    //       hoverpopup.setLngLat(mapCoordinates).setHTML(description).addTo(map);

    //       map.getCanvas().style.cursor = "pointer";
    //     });

    //     // Change it back to a pointer when it leaves.
    //     map.on("mouseleave", "places", () => {
    //       map.getCanvas().style.cursor = "";
    //       hoverpopup.remove();
    //     });
    //   });
    // });
  };

  return (
    <div
      ref={mapContainer}
      className="map-container"
      style={{ height: "600px", width: '100%' }}
    />
  );
};

export default VoyageMap;
