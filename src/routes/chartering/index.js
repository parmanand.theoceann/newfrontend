import React from 'react';
import { Route } from 'react-router-dom';

import TCOV from './routes/tcov';
import TCI from './routes/tci';
import TCO from './routes/tco';
import Bunker from './routes/bunker';
import Cargo from './routes/cargo';
import Estimate from './routes/estimate';
import VCI from './routes/vci';
import TCTO from './routes/tcto';
import PLEstimatePage from './routes/pl-estimate-page';
import VCIVCOEst from './routes/vci-vco-est';
import CargoContract from './routes/cargo-contract';
import PaperTrade from './routes/paper-trade';
import TradeData from './routes/trade-data';
import MarketBunkerData from './routes/market-bunker-data';
import VoyageManagerForms from './routes/voyager-manager-form';
// import VoyageFixtureForms from './routes/voyage-fixture-form';
import VCIN from './routes/vc-in';
import COAContract from './routes/coa-contract';
import CoaVci from './routes/coa-vci';


const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/tcov`} component={TCOV} />
    <Route path={`${match.url}/tci`} component={TCI} />
    <Route path={`${match.url}/tco`} component={TCO} />
    <Route path={`${match.url}/bunker`} component={Bunker} />
    <Route path={`${match.url}/cargo`} component={Cargo} />
    <Route path={`${match.url}/estimate`} component={Estimate} />
    <Route path={`${match.url}/vci`} component={VCI} />
    <Route path={`${match.url}/tcto`} component={TCTO} />
    <Route path={`${match.url}/pl-estimate-page`} component={PLEstimatePage} />
    <Route path={`${match.url}/vci-vco-est`} component={VCIVCOEst} />
    <Route path={`${match.url}/cargo-contract`} component={CargoContract} />
    <Route path={`${match.url}/paper-trade`} component={PaperTrade} />
    <Route path={`${match.url}/trade-data`} component={TradeData} />
    <Route path={`${match.url}/market-bunker-data`} component={MarketBunkerData} />
    <Route path={`${match.url}/voyager-manager-form`} component={VoyageManagerForms} />
    {/* <Route path={`${match.url}/voyage-fixture-form`} component={VoyageFixtureForms} /> */}
    <Route path={`${match.url}/vc-in`} component={VCIN} />
    <Route path={`${match.url}/coa-contract`} component={COAContract} />
    <Route path={`${match.url}/coa-vci`} component={CoaVci} />
  </div>
)

export default CardComponents;
