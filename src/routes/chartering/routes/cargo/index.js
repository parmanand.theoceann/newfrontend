import React, { Component } from 'react';
import { Form, Input, Button, Table, Layout, Modal, Drawer, Row, Col, Checkbox } from 'antd';

import RightBarUI from '../../../../components/RightBarUI';
import ToolbarUI from '../../../../components/ToolbarUI';
import NormalFormIndex from '../../../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon, } from '../../../../shared';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const { Content } = Layout;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const columns10 = [{
  title: 'Quantity',
  dataIndex: 'Quantity',
  key: 'Quantity',
}, {
  title: 'Laycan From',
  dataIndex: 'laycan_from',
  key: 'laycan_from',
}, {
  title: 'Laycan To',
  dataIndex: 'laycan_to',
  key: 'laycan_to',
}, {
  title: 'CP Date',
  dataIndex: 'cp_date',
  key: 'cp_date',
}];

const data10 = [];



class CargoDetails extends Component {

  constructor(props) {

    super(props);
    this.state = {
      "visibleFixture": false,
      "isVisibleCopy": false,
      frmName: "vcargo_in_contract_form",
      formData: {},
      cargoContracts: [],
      frmVisible: true,

    }
  }

  componentDidMount = async () => {
    const response = await getAPICall(`${URL_WITH_VERSION}/voyagecargo/list?l=0`);
    const data = await response['data'];
    this.setState({ ...this.state, "cargoContracts": data, "extraFormFields": { "isShowInMainForm": true, "content": this.getExternalFormFields() } })
    const { match } = this.props
    if (match.params.id) {
      this.onEditClick();
    }

  }

  onClickRightMenu = (key) => {
    if (key === 'task') {
      this.setState({
        visibleFixture: true,
      });
    }
  }

  onCloseFixture = () => {
    this.setState({
      visibleFixture: false,
    });
  }

  callback = (evt) => {
    if (evt === "copy-existing") {
      this.setState({ isVisibleCopy: true });
    }
  }

  onCloseModal = () => {
    this.setState({
      isVisibleCopy: false,
    });
  }

  getExternalFormFields = () => {
    return <>
      <Row gutter={16} style={{ width: '100%', padding: '0 15px' }}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8} style={{ "width": '100%' }}>
          <FormItem
            label="Remark"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <TextArea placeholder="Remark" autoSize={{ minRows: 4, maxRows: 4 }} />
          </FormItem>
        </Col>
      </Row>
      <Row gutter={16} style={{ width: '100%', padding: '15px' }}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <FormItem
            label="Remattance Bank"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input placeholder="Remattance Bank" defaultValue="" />
          </FormItem>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <FormItem
            label="Invoice %"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input placeholder="Invoice %" defaultValue="0.000" />
          </FormItem>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <FormItem
            label="Payment Terms"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <InputGroup compact>
              <Input style={{ width: '25%' }} defaultValue="" />
              <Input style={{ width: '75%' }} defaultValue="" />
            </InputGroup>
          </FormItem>
        </Col>
      </Row>
      <Row gutter={16} style={{ width: '100%', padding: '15px' }}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <FormItem
            label="Balance %"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input placeholder="Balance %" defaultValue="0.00" disabled />
          </FormItem>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <FormItem
            label="Payment Terms"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <InputGroup compact>
              <Input style={{ width: '25%' }} defaultValue="" />
              <Input style={{ width: '75%' }} defaultValue="" />
            </InputGroup>
          </FormItem>
        </Col>
      </Row>
    </>
  }

  saveFormData = async (postData, innerCB) => {
    const { frmName } = this.state;
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty('id')) {
      _url = "update";
      _method = "put";
    }

    Object.keys(postData).forEach((key) => (postData[key] === null) && delete postData[key]);

    postAPICall(`${URL_WITH_VERSION}/voyagecargo/${_url}?frm=${frmName}`, postData, _method, (data) => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        if (this.props.modalCloseEvent && typeof this.props.modalCloseEvent === 'function') {
          this.props.modalCloseEvent();
        } else if (innerCB && typeof innerCB === 'function') {
          innerCB();
        }
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== "string") {
          Object.keys(dataMessage).map(i => msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>");
        } else {
          msg += dataMessage
        }

        msg += "</div>"
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />)
      }
    });
  }

  onCargoItemClick = async (cargoContractID) => {
    // console.log(cargoContractID)
    this.setState({ ...this.state, "frmVisible": false });
    const response = await getAPICall(`${URL_WITH_VERSION}/voyagecargo/edit?ae=${cargoContractID}`);
    const data = await response['data'];
    this.setState({ ...this.state, "formData": data }, () => {
      this.setState({ ...this.state, "frmVisible": true });
    });
  }
  onEditClick = async () => {
    const { match } = this.props;
    this.setState({ ...this.state, "frmVisible": false });
    if (match.params.id && match.path === "/edit-voyage-cargo/:id") {
      const response = await getAPICall(`${URL_WITH_VERSION}/voyagecargo/edit?ae=${match.params.id}`);
      const data = await response['data'];
      this.setState({ ...this.state, "formData": data }, () => {
        this.setState({ ...this.state, "frmVisible": true });
      });
    }
  }


  render() {
    const { frmName, formData, cargoContracts, frmVisible } = this.state;
    return (
      <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <Row gutter={16} style={{ marginRight: 0 }}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="body-wrapper">
                    <article className="article toolbaruiWrapper">
                      <div className="box box-default">
                        <div className="box-body">
                          <ToolbarUI routeUrl={'add-voyage-cargo'} callback={(e) => this.callback(e)} />
                        </div>
                      </div>
                    </article>

                    <div className="row">
                      <div className="col-md-2 pr0 fieldscroll-wraps-scroll">
                        <div className="normal-heading">Voyage Corgo (In) - List</div>
                        <div className="fieldscroll-wraps-list">

                          {
                            cargoContracts.map(e => {
                              return <article className="article">
                                <div className="box box-default">
                                  <div className="bunkerInvoiceWrapper" onClick={() => { this.onCargoItemClick(e['cargo_contract_id']) }}>
                                    <span className="heading"><strong>{e.cargo_contract_id}</strong></span>
                                    <span className="sub-heading">N/A</span>
                                    <span className="value">Uni Challence</span>
                                  </div>
                                </div>
                              </article>
                            })
                          }

                        </div>
                      </div>

                      <div className="col-md-10">
                        <article className="article">
                          <div className="box box-default">
                            <div className="box-body fieldscroll-wraps">
                              <Row>
                                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                  <article className="article">
                                    <div className="box box-default">
                                      <div className="box-body">
                                        {
                                          frmName && frmVisible ?
                                            <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formData}
                                              showForm={true} frmCode={frmName} inlineLayout={true} showButtons={[
                                                { "id": "cancel", "title": "Reset", "type": "danger" },
                                                { "id": "save", "title": "Save", "type": "primary", "event": (data, innerCB) => { this.saveFormData(data, innerCB) } }
                                              ]} showToolbar={[{
                                                // leftWidth: 8,
                                                // rightWidth: 16,
                                                isLeftBtn: [],
                                                isRightBtn: [],
                                                isResetOption: true
                                              },
                                              ]}
                                            />
                                            : undefined
                                        }
                                      </div>
                                    </div>
                                  </article>
                                </Col>
                              </Row>

                            </div>
                          </div>
                        </article>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Content>
          </Layout>

          <RightBarUI pageTitle="voyage-cargo-righttoolbar" callback={(data) => this.onClickRightMenu(data)} />

          <Drawer
            title="Task"
            placement="right"
            closable={true}
            onClose={this.onCloseFixture}
            open={this.state.visibleFixture}
            getContainer={false}
            style={{ position: 'absolute' }}
            width={500}
            maskClosable={false}
          >
            <Form layout="vertical" style={{ paddingBottom: '2rem' }}>
              Task RightBar UI
            </Form>
            <div
              style={{
                position: 'absolute',
                right: 0,
                bottom: 0,
                width: '100%',
                borderTop: '1px solid #e9e9e9',
                padding: '10px 16px',
                background: '#fff',
                textAlign: 'right',
              }}
            >
              <Button onClick={this.onCloseFixture} style={{ marginRight: 8 }}>Cancel</Button>
              <Button onClick={this.onCloseFixture} type="primary">Submit</Button>
            </div>
          </Drawer>

          <Modal
            title="Copy Setup"
            open={this.state.isVisibleCopy}
            width={1200}
            onCancel={this.onCloseModal}
            footer={false}
          >
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <Form className="m-b-18">
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          {...formItemLayout}
                          label="No. Of Copies">
                          <Input size="default" placeholder="No. Of Copies" defaultValue="0" />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          {...formItemLayout}
                          label="Total Period">
                          <Input size="default" placeholder="Total Period" defaultValue="0" />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          {...formItemLayout}
                          label="Start">
                          <Input size="default" placeholder="" defaultValue="0" />
                        </FormItem>
                      </Col>
                    </Row>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <Checkbox>Keep Nominated Vessel</Checkbox>
                        <Checkbox>Keep Booking No.</Checkbox>
                        <Button type="primary">Create</Button>
                      </Col>
                    </Row>
                  </Form>
                  <Table columns={columns10} dataSource={data10} pagination={false} bordered footer={() =>
                    <div className="text-center">
                      <Button type="link">Add New</Button>
                    </div>
                  } />
                </div>
              </div>
            </article>

          </Modal>

        </Layout>
      </div>
    )
  }
}

export default CargoDetails;