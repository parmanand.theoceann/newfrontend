import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        f: "L",
        port: "MUMBAI",
        berth: "QUAY",
        qty: "10,000",
        ldrate: "20,000.00",
        ru: "PD",
        terms: "SHINC",
        tt: "0.0",
        pd: "0.50",
        dem: "0.00",
        portexp: "",
        oexp: "0.00",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class Itinerary extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'F',
            dataIndex: 'f',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'f'),
        },
        {
            title: 'Port',
            dataIndex: 'port',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'port'),
        },
        {
            title: 'Berth',
            dataIndex: 'berth',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'berth'),
        },
        {
            title: 'Qty',
            dataIndex: 'qty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'qty'),
        },
        {
            title: 'LD Rate',
            dataIndex: 'ldrate',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'ldrate'),
        },
        {
            title: 'RU',
            dataIndex: 'ru',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'ru'),
        },
        {
            title: 'Terms',
            dataIndex: 'terms',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'terms'),
        },
        {
            title: 'TT',
            dataIndex: 'tt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tt'),
        },
        {
            title: 'PD',
            dataIndex: 'pd',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'pd'),
        },
        {
            title: 'Est PD',
            dataIndex: 'estpd',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'estpd'),
        },
        {
            title: 'Dem',
            dataIndex: 'dem',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'dem'),
        },
        {
            title: 'Port Exp',
            dataIndex: 'portexp',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'portexp'),
        },
        {
            title: 'O Exp',
            dataIndex: 'oexp',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'oexp'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <a className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                    <a className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </a>
                                </span>
                                : <a className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></a>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 1500, y: 300 }}
                    size="small"
                    // pagination={{ pageSize: 50 }}
                    pagination={false}
                    // title={() => 'Port / Date Group'}
                />
            </>
        )
    }
}

export default Itinerary;