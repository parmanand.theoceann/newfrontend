import React from 'react';

const VCIVCOEst = () => {
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            VCI -VCO Est.
            </div>
        </div>
      </article>
    </div>
  );
}

export default VCIVCOEst;