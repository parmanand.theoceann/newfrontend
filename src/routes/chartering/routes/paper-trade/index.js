import React from 'react';

const PaperTrade = () => {
    return (
        <div className="body-wrapper">
            <article className="article">
                <div className="box box-default">
                    <div className="box-body">
                        Paper Trade
                    </div>
                </div>
            </article>
        </div>
    );
}

export default PaperTrade;