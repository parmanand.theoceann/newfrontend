import React from 'react';

const VoyageManagerForms = () => {
    return (
        <div className="body-wrapper">
            <article className="article">
                <div className="box box-default">
                    <div className="box-body">
                        Voyage Manager Form
                    </div>
                </div>
            </article>
        </div>
    );
}

export default VoyageManagerForms;