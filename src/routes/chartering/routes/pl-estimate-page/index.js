import React from 'react';

const PLEstimatePage = () => {
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            P&L Estimate Page
            </div>
        </div>
      </article>
    </div>
  );
}

export default PLEstimatePage;