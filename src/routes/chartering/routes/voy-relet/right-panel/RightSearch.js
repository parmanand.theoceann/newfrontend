import React, { Component } from 'react';
import { Form, Select, Input, Layout, Tabs, Icon, Drawer, Row, Col } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
class RightSearch extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper m-0">
        <article className="article toolbaruiWrapper">
          <Form>
            <FormItem {...formItemLayout} label="Estimate ID ">
              <InputGroup compact>
                <Input style={{ width: '50%' }} />   
                <Input type="date" style={{ width: '50%' }} />
              </InputGroup>
            </FormItem>

            <FormItem {...formItemLayout} label="Vessel Name">
                <Input size="default" />   
            </FormItem>

            <FormItem {...formItemLayout} label="Vessel Type">
                <Input size="default" />   
            </FormItem>

            <FormItem {...formItemLayout} label="Voyage status">
                <Input size="default" />   
            </FormItem>

            <FormItem {...formItemLayout} label="Cargo ID">
                <Input size="default" />   
            </FormItem>    

            <FormItem {...formItemLayout} label="Charterer Name">
                <Input size="default" />
            </FormItem>

            <FormItem {...formItemLayout} label="Owner Name">
                <Input size="default" />   
            </FormItem>
          </Form>
        </article>
      </div>
    );
  }
}

export default RightSearch;
