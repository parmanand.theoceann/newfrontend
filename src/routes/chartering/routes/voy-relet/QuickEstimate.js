import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Tabs,
  Drawer,
  Layout,
  Select,
  Modal,
  Button,
  Spin,
  Alert,
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
} from "../../../../shared";

import {
  uploadAttachment,
  deleteAttachment,
} from "../../../../shared/attachments";
import RightBarUI from "../../../../components/RightBarUI";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import VoyReletPortInformation from "../../../right-port-info/VoyReletPortInformation";
import PL from "../../../../shared/components/PL/voyrelet";
import Properties from "./right-panel/Properties";
import RightSearch from "./right-panel/RightSearch";
import EstimateSummary from "../cargo-contract/EstimateSummary";
import VoyReletReports from "../../../form-reports/VoyReletReports";
import VoyReletEstimateDetail from "../../../form-reports/VoyReletEstimateDetail";
import CargoContract from "../../../chartering/routes/cargo-contract";
import CargoDetails from "../../../chartering/routes/voyage-cargo-in";
import QuickEstimate from "./QuickEstimate";
import VcPurchage from "../voyage-cargo-in/index";
import AddCargo from "../cargo-contract/index";
import PortDistance from "../../../port-to-port/PortAnalytics";
import ShipDistance from "../../../port-to-port/ShipDetails";
import AttachmentFile from "../../../../shared/components/Attachment";
import { DeleteOutlined, MenuFoldOutlined, SaveOutlined } from "@ant-design/icons";

const FormItem = Form.Item;
const { Content } = Layout;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class FullEstimate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      frmName: "VOYAGE_RELET",
      portItin: [],
      isShowPortDistance: false,
      isShowPortRoute: false,
      index: 0,
      refreshForm: true,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      disableSave: false,
      width: 1200,
      isShowVoyReletReports: false,
      isShowEstimateReport: false,
      estimateID: undefined,
      spType: undefined,
      visisbleCargoChild: false,
      cargoData: {},
      fullFormData: {},
      portData: null,
      formData: this.props.formData || {},
      visibleviewmodal: false,
      viewData: {},
      reportFormData: {
        portitinerary: [
          {
            port_id: "4444",
            port: "2944",
            funct: 3,
            s_type: 1,
            wf_per: 10,
            miles: "0.00",
            speed: "0.00",
            eff_speed: "0.00",
            gsd: "0.00",
            tsd: "0.00",
            xsd: "0.00",
            l_d_qty: "0.00",
            l_d_rate: "0.00",
            l_d_rate1: "0.00",
            turn_time: "0.00",
            days: "0.00",
            xpd: "0.00",
            p_exp: "0.00",
            t_port_days: "0.00",
            l_d_term: 1,
            editable: true,
            index: 0,
            ID: -9e6,
          },
          {
            port_id: "6297",
            port: "5530",
            s_type: 1,
            wf_per: 10,
            miles: "0.00",
            speed: "0.00",
            eff_speed: "0.00",
            gsd: "0.00",
            tsd: "0.00",
            xsd: "0.00",
            l_d_qty: "0.00",
            l_d_rate: "0.00",
            l_d_rate1: "0.00",
            turn_time: "0.00",
            days: "0.00",
            xpd: "0.00",
            p_exp: "0.00",
            t_port_days: "0.00",
            l_d_term: 1,
            editable: true,
            index: 1,
            ID: -9e6 + 1,
          },
        ],
      },
      tableID: -9e6,
      indexData: 0,
      quickMenuItem: undefined,
      showQuickPopup: false,
      fixData: {},
      fixModal: false,
      showItemList: true,
      sendBackData: { show: false },
      showFixMenu: {
        key: "fixopt",
        isDropdown: 0,
        withText: "Fix & Schedule",
        type: "",
        menus: null,
        event: (key) => this.onClickFixVoyRelet(true),
      },
      showLeftBtn: [
        {
          isSets: [
            {
              id: "7",
              key: "menu-fold",
              type: <MenuFoldOutlined/>,
              withText: "",
              event: "menu-fold",
            },
            // { id: '1', key: 'add', type: <PlusOutlined />, withText: '' },
            // (formData && !formData['generated_voyage'] && !formData['tcov_status'] ?
            {
              id: "3",
              key: "save",
              type: <SaveOutlined />,
              withText: "",
              event: (key, data) => this.saveForm(key, data),
            },
            //  : undefined),
            {
              id: "4",
              key: "delete",
              type: <DeleteOutlined />,
              withText: "",
            },
          ],
        },
      ],
      selectedID: null,
      vmCheck: this.props.vmCheck != undefined ? this.props.vmCheck : false,
    };
  }

  componentDidMount = () => {
    const { match, estimateID } = this.props;
    if (
      estimateID ||
      (match &&
        match.params &&
        match.params.id &&
        match.path == "/voy-relet-full-estimate-edit/:id")
    ) {
      let est_id = match && match.params.id ? match.params.id : estimateID;
      this.getcomponentMount(est_id);
    }
  };

  getcomponentMount = async (estimateID = null) => {
    const { history } = this.props;
    let fullEstData =
      this.props && this.props.location && this.props.location.state;
    const { showLeftBtn, formData, quickMenuItem, showFixMenu } = this.state;
    let vessel = undefined,
      cp_echo = undefined,
      _quickMenuItem = quickMenuItem,
      _showQuickEstimate = undefined,
      _showFixMenu = undefined;
    let response = null,
      respDataC = {},
      respData = {
        cargos: [],
        portitinerary: [],
        bunkerdetails: [],
      },
      fixData = null,
      fieldOptions = undefined,
      _showLeftBtn = Object.assign([], showLeftBtn);

    if (estimateID) {
      this.setState({ ...this.state, refreshForm: true }, () =>
        history.push(`/voy-relet-full-estimate-edit/${estimateID}`)
      );
      let qParams = { ae: estimateID };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/edit?${qParamString}`
      );
      respDataC = await response;
      respData = respDataC["data"];
      if (respData.tcov_status == 1) {
        this.setState({
          disableSave: true,
        });
      }
      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
      );
      vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${
          respData["vessel_id"]
        }/voyrelet/${estimateID}`
      );
      cp_echo = await response["data"];

      //if (respDataC.hasOwnProperty('row') && respDataC['row'] === 'quickestimate') {
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];

      //}
      respData["-"] = respData.hasOwnProperty("-")
        ? respData["-"]
        : cp_echo["-"];
      respData["."] = respData.hasOwnProperty(".")
        ? respData["."]
        : cp_echo["."]["eco_data"];

      if (respData && respData.hasOwnProperty("fix")) {
        fixData = respData["fix"];
        delete respData["fix"];
      }

      if (
        respData &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] == "0"
      ) {
        _showFixMenu = showFixMenu;
      }

      if (
        respData &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] == "1"
      ) {
        delete _showLeftBtn[0]["isSets"][2];
        delete _showLeftBtn[0]["isSets"][1];
      }
      if (!this.state.vmCheck) {
        _quickMenuItem = {
          key: "quick_estimate",
          isDropdown: 0,
          withText: "View Quick Est.",
          type: "",
          event: () => this.props.history.push(`/edit-voy-relet/${estimateID}`),
        };
      }
    }
    if (JSON.stringify(respData) === JSON.stringify({})) {
      respData = Object.assign({}, formData);
    }

    if (fullEstData) {
      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${fullEstData.vesseldetails.vessel_id}`
      );
      vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${
          fullEstData.vesseldetails.vessel_id
        }`
      );

      cp_echo = await response["data"];

      respData["cargos"].push(
        {
          sp_type: "Purchase",
          cargo_name: fullEstData.purchasecargorevenue.cargo_name,
          charterer: fullEstData.purchasecargorevenue.owner,
          quantity: fullEstData.purchasecargorevenue.quantity,
          quantity: fullEstData.purchasecargorevenue.quantity,
          unit: fullEstData.purchasecargorevenue.unit,
          f_type: fullEstData.purchasecargorevenue.f_type,
          f_rate: fullEstData.purchasecargorevenue.f_rate,
          commission: fullEstData.purchasecargorevenue.commission,
          extra_rev: fullEstData.purchasecargorevenue.other_revenue,
        },
        {
          sp_type: "Sell",
          cargo_name: fullEstData.salecargoexpense.s_cargo_name,
          charterer: fullEstData.salecargoexpense.charterer,
          quantity: fullEstData.salecargoexpense.s_quantity,
          unit: fullEstData.salecargoexpense.s_unit,
          f_type: fullEstData.salecargoexpense.s_f_type,
          f_rate: fullEstData.salecargoexpense.s_f_rate,
          commission: fullEstData.salecargoexpense.s_commission,
          extra_rev: fullEstData.salecargoexpense.other_cost,
        }
      );

      respData["portitinerary"].push({
        funct: 3,
        port: fullEstData.vesseldetails.ballast_port_name,
        port_id: fullEstData.vesseldetails.ballast_port,
        passage: 1,
        s_type: 1,
        miles: "0.00",
        wf_per: "0.00",
        eff_speed: "0.00",
        gsd: "0.00",
        tsd: "0.00",
        speed: fullEstData.vesseldetails.ballast_spd,
      });
      respData["bunkerdetails"].push({
        funct: 3,
        port: fullEstData.vesseldetails.ballast_port_name,
        port_id: fullEstData.vesseldetails.ballast_port,
        passage: 1,
        s_type: 1,
        miles: "0.00",
        wf_per: "0.00",
        eff_speed: "0.00",
      });
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];

      respData["-"] = respData.hasOwnProperty("-")
        ? respData["-"]
        : cp_echo["-"];
      respData["."] = respData.hasOwnProperty(".")
        ? respData["."]
        : cp_echo["."]["cp_data"];

      if (fullEstData && fullEstData.estimate_id) {
        respData["estimate_id"] = fullEstData.estimate_id;
      }
      if (fullEstData && fullEstData.vesseldetails.other_cost) {
        respData["mis_cost"] = fullEstData.vesseldetails.other_cost;
      }
      if (fullEstData && fullEstData.id) {
        respData["tcov_ref_id"] = fullEstData.id;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.vessel_id
      ) {
        respData["vessel_id"] = fullEstData.vesseldetails.vessel_id;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.bb
      ) {
        respData["bb"] = fullEstData.vesseldetails.bb;
      }

      if (
        fullEstData &&
        fullEstData.voyageresults &&
        fullEstData.voyageresults.total_voyage_days
      ) {
        respData["total_days"] = fullEstData.voyageresults.total_voyage_days;
      }

      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.reposition_port_name
      ) {
        respData["reposition_port_name"] =
          fullEstData.vesseldetails.reposition_port_name;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.reposition_port
      ) {
        respData["reposition_port"] = fullEstData.vesseldetails.reposition_port;
      }

      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.ballast_port_name
      ) {
        respData["ballast_port_name"] =
          fullEstData.vesseldetails.ballast_port_name;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.ballast_port
      ) {
        respData["ballast_port"] = fullEstData.vesseldetails.ballast_port;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.commence_date
      ) {
        respData["commence_date"] = fullEstData.vesseldetails.commence_date;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.completed_date
      ) {
        respData["completed_date"] = fullEstData.vesseldetails.completed_date;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.dwt
      ) {
        respData["dwt"] = fullEstData.vesseldetails.dwt;
      }
    }

    this.setState(
      {
        ...this.state,
        showLeftBtn: _showLeftBtn,
        showItemList: respData["id"] ? false : true,
        formData: respData,
        fixData: fixData,
        quickMenuItem: _quickMenuItem,
        fieldOptions: fieldOptions,
        showQuickEstimate: _showQuickEstimate,
        showFixMenu: _showFixMenu,
        estimateID: estimateID,
      },
      () => this.setState({ ...this.state, refreshForm: false })
    );
  };
  onClickRightMenu = async (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "port_route_details":
        this.portDistance(true, this.state.formData, key);
        break;
      case "port_distance":
        this.portDistance(true, this.state.formData, key);
        break;
      case "portinformation":
        loadComponent = <VoyReletPortInformation />;
        break;
      case "pl":
        this.setState({
          ...this.state,
          sendBackData: { show: true, options: options },
        });
        break;
      case "summary":
        loadComponent = <EstimateSummary />;
        break;

      case "properties":
        loadComponent = <Properties />;
        break;
      case "search":
        loadComponent = <RightSearch />;
        break;
      case "attachment":
        const { estimateID } = this.state;
        if (estimateID) {
          const url = `${URL_WITH_VERSION}/importexport/fetch?eid=${estimateID}`;
          const response = await getAPICall(url);
          const attachments = response["data"] || [];
          const callback = (fileArr) => uploadAttachment(fileArr, estimateID);
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) => deleteAttachment(file.url, file.name)}
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon("info", "Attachments not allowed here.");
        }
        break;
      default:
        break;
    }

    if (loadComponent) {
      this.setState({
        ...this.state,
        visibleDrawer: true,
        title: options.title,
        loadComponent: loadComponent,
        width: options.width && options.width > 0 ? options.width : 1200,
      });
    }
  };

  // _onLeftSideListClick = async (evt) => {
  //   console.log('evt',evt)
  //   this.props.history.push(`/voy-relet-full-estimate-edit/${evt['estimate_id']}`);
  //   this.setState({ ...this.state, refreshForm: true, selectedID: evt.id });
  //   const { showLeftBtn, formData, quickMenuItem, showFixMenu } = this.state;
  //   let vessel = undefined,
  //     cp_echo = undefined,
  //     _quickMenuItem = quickMenuItem,
  //     _showQuickEstimate = undefined,
  //     _showFixMenu = undefined;
  //   let response = null,
  //     respDataC = {},
  //     respData = {},
  //     fixData = null,
  //     fieldOptions = undefined,
  //     _showLeftBtn = Object.assign([], showLeftBtn);
  // //  this.setState({ ...this.state, refreshForm: true, selectedID: evt.id });
  //   let qParams = { ae: evt.estimate_id ? evt.estimate_id : evt };
  //   let qParamString = objectToQueryStringFunc(qParams);
  //   response = await getAPICall(
  //     `${URL_WITH_VERSION}/voyage-relet/edit?${qParamString}`
  //   );
  //   respDataC = await response;
  //   respData = respDataC["data"];

  //   // Vessel Details
  //   response = await getAPICall(
  //     `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
  //   );
  //   vessel = await response["data"];

  //   // Get CP ECHO Data
  //   response = await getAPICall(
  //     `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}`
  //   );
  //   cp_echo = await response["data"];
  //   if (
  //     respDataC.hasOwnProperty("row") &&
  //     respDataC["row"] === "quickestimate"
  //   ) {
  //     respData["purchase_data"] = cp_echo["."];
  //     respData["vessel_code"] = vessel["vessel_code"];
  //   }

  //   if (respData && respData.hasOwnProperty("fix")) {
  //     fixData = respData["fix"];
  //     delete respData["fix"];
  //   }

  //   respData["-"] = respData.hasOwnProperty("-") ? respData["-"] : cp_echo["-"];
  //   respData["."] = respData.hasOwnProperty(".")
  //     ? respData["."]
  //     : cp_echo["."]["cp_data"];

  //   _quickMenuItem = {
  //     key: "quick_estimate",
  //     isDropdown: 0,
  //     withText: "Quick Estimate",
  //     type: "",
  //     event: () =>
  //       this.props.history.push(
  //         `/edit-voy-relet/${evt.estimate_id ? evt.estimate_id : evt}`
  //       ),
  //   };
  //   if (JSON.stringify(respData) === JSON.stringify({})) {
  //     respData = Object.assign({}, formData);
  //   }

  //   this.setState(
  //     {
  //       ...this.state,
  //       showLeftBtn: _showLeftBtn,
  //       showItemList: respData["id"] ? false : true,
  //       formData: respData,
  //       fixData: fixData,
  //       quickMenuItem: _quickMenuItem,
  //       fieldOptions: fieldOptions,
  //       showQuickEstimate: _showQuickEstimate,
  //       showFixMenu: _showFixMenu,
  //       estimateID: evt.estimate_id ? evt.estimate_id : evt,
  //     },
  //     () => this.setState({ ...this.state, refreshForm: false })
  //   );

  //   //window.location.reload();
  // };

  _onLeftSideListClick = async (evt) => {
    this.setState({ ...this.state, refreshForm: true, selectedID: evt.id });
    this.getcomponentMount(evt["estimate_id"]);
  };

  onClickFixVoyRelet = (boolVal) =>
    this.setState({ ...this.state, fixModal: boolVal });
  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  //VoyReletReports = showVoyReletReports => this.setState({ ...this.state, isShowVoyReletReports: showVoyReletReports });
  VoyReletReports = async (showVoyReletReports) => {
    let qParamString = objectToQueryStringFunc({ ae: this.state.estimateID });
    // for report Api
    if (showVoyReletReports == true) {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/report?${qParamString}`
      );
      const respDataReport = await responseReport["data"];

      if (responseReport) {
        this.setState({ ...this.state, reportFormData: respDataReport }, () =>
          this.setState({
            ...this.state,
            isShowVoyReletReports: showVoyReletReports,
          })
        );
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } else {
      this.setState({
        ...this.state,
        isShowVoyReletReports: showVoyReletReports,
      });
    }
  };

  // EstimateReport = (showEstimateReport) =>
  // this.setState({ ...this.state, isShowEstimateReport: showEstimateReport });

  EstimateReport = async (showEstimateReport) => {
    let qParamString = objectToQueryStringFunc({ ae: this.state.estimateID });
    if (showEstimateReport == true) {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/estimatereport?${qParamString}`
      );
      const respDataReport = await responseReport["data"];

      if (respDataReport) {
        this.setState({ ...this.state, reportFormData: respDataReport }, () =>
          this.setState({
            ...this.state,
            isShowEstimateReport: showEstimateReport,
          })
        );
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } else {
      this.setState({
        ...this.state,
        isShowEstimateReport: showEstimateReport,
      });
    }
  };

  onCancel = () => this.setState({ ...this.state, visisbleCargoChild: false });
  viewQuickEstimate = (bolVal, estimateID) =>
    this.setState({ ...this.state, estimateID: estimateID }, () =>
      this.setState({ ...this.state, showQuickPopup: bolVal })
    );

  saveForm = (key, data) => {
    const { frmName } = this.state;
    if (data) {
      let _url = `${URL_WITH_VERSION}/voyage-relet/save?frm=${frmName}`;
      let _method = "POST";

      if (data["id"] > 0) {
        _url = `${URL_WITH_VERSION}/voyage-relet/update?frm=${frmName}`;
        _method = "PUT";
        if (data && data.hasOwnProperty("purchase_data"))
          delete data.purchase_data;
      }
      // if (_method === "POST" && data && data.hasOwnProperty('portitinerary') && data.portitinerary.length > 0) {
      //   data.portitinerary.shift()
      // }
      // if (data && data.hasOwnProperty("portitinerary") && data.portitinerary.length > 0) {
      //   let data1 = data.portitinerary.map((val, key) => {
      //     let val1 = { ...val }
      //     val1['port_id'] = val1['port_id'];
      //     return val1
      //   })
      //   data['portitinerary']=data1
      // }
      if (!this.state.disableSave) {
        postAPICall(`${_url}`, data, _method, (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);

            this.props.history.replace("/empty");
            setTimeout(() => {
              this.props.history.replace(
                `/voy-relet-full-estimate-edit/${response.data}`
              );
            }, 5);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        });
      } else {
        openNotificationWithIcon("error", "This Estimate is already fixed");
      }
    }
  };

  // _getCargoData = (_cargoTermData, data, _fullFormData) => {
  //   // let fd = _fullFormData['cargos'].filter(e => e['cargo_contract_id'] === _cargoTermData['cargo_contract_id'])
  //   _cargoTermData = {
  //     ..._cargoTermData,
  //     serial_no: _fullFormData["cargos"].length,
  //     cargo_name: data["cargo_name"],
  //     charterer: data["charterer"],
  //     quantity: data["cp_qty"],
  //     unit: data["cp_unit"],
  //     option_percentage: "0.00",
  //     option_type: undefined,
  //     f_type: data["freight_type"],
  //     f_rate: data["freight_rate"],
  //     lumpsum: "0.00",
  //     commission: "0.00",
  //     extra_rev: "0.00",
  //     curr: data["currency"],
  //     dem_rate_pd: "0.00",
  //     editable: true,
  //     index:
  //       _fullFormData["cargos"].length > 2
  //         ? _fullFormData["cargos"].length - 1
  //         : _fullFormData["cargos"].length,
  //     ocd: true,
  //     id: this.state.tableID + _fullFormData["cargos"].length,
  //   };

  //   if (
  //     _fullFormData.hasOwnProperty("cargos") &&
  //     _fullFormData["cargos"].length > 0
  //   ) {
  //     if (
  //       this.state.indexData >= 0 &&
  //       _fullFormData["cargos"][this.state.indexData]
  //     ) {
  //       _fullFormData["cargos"][this.state.indexData] = _cargoTermData;
  //     } else if (
  //       this.state.indexData === -1 &&
  //       (!_fullFormData["cargos"][0].hasOwnProperty("cargo_contract_id") ||
  //         (_fullFormData["cargos"][0].hasOwnProperty("cargo_contract_id") &&
  //           _fullFormData["cargos"][0]["cargo_contract_id"] === ""))
  //     ) {
  //       _fullFormData["cargos"][0] = _cargoTermData;
  //     } else {
  //       _fullFormData["cargos"].push(_cargoTermData);
  //     }
  //   } else {
  //     _fullFormData["cargos"] = [{ ..._cargoTermData }];
  //   }
  //   return _fullFormData;
  // };

  getCargo = (data) => {
    let cargo_contract_id = "";
    if (data["sp_type"] == 187) {
      cargo_contract_id = data["vc_purchase_id"];
    } else {
      cargo_contract_id = data["cargo_contract_id"];
    }
    this.setState(
      { ...this.state, refreshForm: true, visisbleCargoChild: false },
      () => {
        let _fullFormData = Object.assign({}, this.state.fullFormData);
        let _cargoTermData = {};

        _cargoTermData = {
          sp_type: data["sp_type"],
          serial_no: _fullFormData["cargos"].length,
          cargo_contract_id: cargo_contract_id,
          cargo_name: data["cargo_name"],
          charterer: data["charterer"],
          quantity: data["cp_qty"],
          unit: data["cp_unit"],
          option_percentage: "0.00",
          option_type: undefined,
          f_type: data["freight_type"],
          f_rate: data["freight_rate"],
          lumpsum: "0.00",
          commission: "0.00",
          extra_rev: "0.00",
          curr: data["currency"],
          dem_rate_pd: "0.00",
          editable: true,
          index:
            _fullFormData["cargos"].length > 2
              ? _fullFormData["cargos"].length - 1
              : _fullFormData["cargos"].length,
          ocd: true,
          id: this.state.tableID + _fullFormData["cargos"].length,
        };
        if (
          _fullFormData.hasOwnProperty("cargos") &&
          _fullFormData["cargos"].length > 0
        ) {
          _fullFormData["cargos"].push(_cargoTermData);
        } else {
          _fullFormData["cargos"] = [{ ..._cargoTermData }];
        }
        this.setState(
          {
            ...this.state,
            formData: _fullFormData,
            fullFormData: {},
            indexData: 0,
          },
          () => this.setState({ ...this.state, refreshForm: false })
        );
      }
    );
  };

  onClickAddCargo = (cargoContractData = {}, data = {}, dType = "purchase") => {
    this.setState(
      {
        ...this.state,
        cargoData: cargoContractData,
        fullFormData: data,
        spType: dType,
      },
      () => this.setState({ ...this.state, visisbleCargoChild: true })
    );
  };

  onClickExtraIcon = (action, data) => {
    if (data.hasOwnProperty("cargos")) {
      let dataNew =
        data && data["cargos"].length > 0 ? data["cargos"][action.index] : {};
      this.setState(
        {
          viewData: dataNew,
          index: action.index,
          fullFormData: data,
        },
        () =>
          this.setState({
            visibleviewmodal: true,
          })
      );
    } else {
      let delete_id = data && data.id;
      let groupKey = action["gKey"];
      let frm_code = "";
      if (groupKey == "Cargos") {
        groupKey = "cargos";
        frm_code = "VOYAGE_RELET";
      }
      if (groupKey == "Port Itinerary") {
        groupKey = "portitinerary";
        frm_code = "TAB_VOYAGE_RELET_PORT_ITINERARY";
      }
      if (groupKey == "Bunker Details") {
        groupKey = "bunkerdetails";
        frm_code = "TAB_VOYAGE_RELET_BUNKER_DETAILS";
      }

      if (groupKey == "Port Date Details") {
        groupKey = "portdatedetails";
        frm_code = "TAB_VOYAGE_RELET_PORT_DATE_DETAILS";
      }
      if (groupKey == ".") {
        frm_code = "VOYAGE_RELET";
      }
      if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
        let data1 = {
          id: delete_id,
          frm_code: frm_code,
          group_key: groupKey,
        };

        postAPICall(
          `${URL_WITH_VERSION}/tr-delete`,
          data1,
          "delete",
          (response) => {
            if (response && response.data) {
              openNotificationWithIcon("success", response.message);
            } else {
              openNotificationWithIcon("error", response.message);
            }
          }
        );
      }
    }
  };

  createVoyageManger = () => {
    const { fixData } = this.state;
    postAPICall(
      `${URL_WITH_VERSION}/voyage-relet/fix`,
      fixData,
      "POST",
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          this.props.history.push(`/voyage-manager/${fixData["estimate_id"]}`);
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div
              className="notify-error"
              dangerouslySetInnerHTML={{ __html: msg }}
            />
          );
        }
      }
    );
  };

  triggerEvent = (data) => {
    const { sendBackData } = this.state;
    if (data && sendBackData["show"] === true) {
      this.onCloseDrawer();
      let loadComponent = <PL formData={data} viewTabs={["Estimate View"]} />;
      this.setState({
        ...this.state,
        visibleDrawer: true,
        title: sendBackData.options.title,
        loadComponent: loadComponent,
        width:
          sendBackData.options.width && sendBackData.options.width > 0
            ? sendBackData.options.width
            : 1200,
        sendBackData: { show: false },
      });
    }
  };

  // triggerEvent = (data) => {
  //   const { sendBackData } = this.state;
  //   if (data && sendBackData['show'] === true) {
  //     let expCount = 0, revCount = 0, revFrtRate = 0, expFrtRate = 0;
  //     let  plData = { 'showExactPL': true, 'dataPL': {
  //       'revenue': {
  //         "freight": 0,
  //         "freightCommission": 0,
  //         "misRevenue": 0,
  //         "grossRevenue": 0,
  //         "netRevenue": 0
  //       },
  //       'expenses': {
  //         "freightExpenses": 0,
  //         "brokerComm": 0,
  //         "misExpenses": 0,
  //         "totalExpenses": 0
  //       },
  //       'voyageResult': {
  //         "profitLoss": 0,
  //         "dailyProfitLoss": 0,
  //         "freightRate": 0,
  //         "breakevenFreightRate": 0,
  //         "netVoyageDays": 0,
  //         "offHireDays": 0
  //       }
  //     }};

  //     data['cargos'].map(e => {
  //       let amount = 0
  //       if (e['sp_type'] === 186) {
  //         revFrtRate = revFrtRate + (e['f_rate'].replaceAll(',', '') * 1);
  //         revCount = revCount + 1;
  //         amount = ((e['f_rate'].replaceAll(',', '') * 1) * (e['quantity'].replaceAll(',', '') * 1))
  //         plData['dataPL']['revenue']['freight'] = plData['dataPL']['revenue']['freight'] + amount;
  //         plData['dataPL']['revenue']['freightCommission'] = plData['dataPL']['revenue']['freightCommission'] + ((amount * (e['commission'].replaceAll(',', '') * 1)) / 100);
  //         plData['dataPL']['revenue']['misRevenue'] = plData['dataPL']['revenue']['misRevenue'] + (e['extra_rev'].replaceAll(',', '') * 1);
  //       } else if (e['sp_type'] === 187) {
  //         expFrtRate = expFrtRate + (e['f_rate'].replaceAll(',', '') * 1);
  //         expCount = expCount + 1;
  //         amount = ((e['f_rate'].replaceAll(',', '') * 1) * (e['quantity'].replaceAll(',', '') * 1));
  //         plData['dataPL']['expenses']['freightExpenses'] = plData['dataPL']['expenses']['freightExpenses'] + amount;
  //         plData['dataPL']['expenses']['brokerComm'] = plData['dataPL']['expenses']['brokerComm'] + ((amount * (e['commission'].replaceAll(',', '') * 1)) / 100);
  //         plData['dataPL']['expenses']['misExpenses'] = plData['dataPL']['expenses']['misExpenses'] + (e['extra_rev'].replaceAll(',', '') * 1);
  //       }
  //     });

  //     plData['dataPL']['revenue']['grossRevenue'] = plData['dataPL']['revenue']['freight'] + plData['dataPL']['revenue']['misRevenue'];
  //     plData['dataPL']['revenue']['netRevenue'] = plData['dataPL']['revenue']['grossRevenue'] - plData['dataPL']['revenue']['freightCommission'];
  //     plData['dataPL']['expenses']['totalExpenses'] = plData['dataPL']['expenses']['freightExpenses'] + plData['dataPL']['expenses']['misExpenses'] - plData['dataPL']['expenses']['brokerComm'];

  //     plData['dataPL']['voyageResult']['profitLoss'] = plData['dataPL']['revenue']['netRevenue'] - plData['dataPL']['expenses']['totalExpenses'];
  //     plData['dataPL']['voyageResult']['dailyProfitLoss'] = plData['dataPL']['voyageResult']['profitLoss'] / (data['total_days'].replaceAll(',', '') * 1);
  //     plData['dataPL']['voyageResult']['netVoyageDays'] = ((""+data['total_days']).replaceAll(',', '') * 1);
  //     plData['dataPL']['voyageResult']['freightRate'] = revFrtRate / revCount;
  //     plData['dataPL']['voyageResult']['breakevenFreightRate'] = expFrtRate / expCount;

  //     this.onCloseDrawer();
  //     let loadComponent = <PL formData={plData} viewTabs={["Estimate View"]} />;
  //     this.setState({
  //       ...this.state,
  //       visibleDrawer: true,
  //       title: sendBackData.options.title,
  //       loadComponent: loadComponent,
  //       width: sendBackData.options.width && sendBackData.options.width > 0 ? sendBackData.options.width : 1200,
  //       sendBackData: {"show": false}
  //     });
  //   }
  // }

  getCargoImport = (data) => {
    const { fullFormData, index } = this.state;
    let frmData = Object.assign({}, fullFormData);
    let dataNew =
      frmData && frmData["cargos"].length > 0 ? frmData["cargos"][index] : {};
    this.setState(
      {
        refreshForm: true,
        visibleviewmodal: false,
      },
      () => {
        dataNew["cargo_name"] = data && data.cargo_name;
        dataNew["charterer"] = data && data.charterer;
        dataNew["f_rate"] = data && data.f_rate;
        dataNew["f_type"] = data && data.f_type;
        dataNew["curr"] = data && data.curr;
        frmData["cargos"][index] = dataNew;
        this.setState({
          ...this.state,
          refreshForm: false,
          formData: frmData,
          fullFormData: frmData,
        });
      }
    );
  };
  portDistance = (val, data, key) => {
    if (key == "port_route_details") {
      if (
        data &&
        data.hasOwnProperty("portitinerary") &&
        data["portitinerary"].length > 0
      ) {
        this.setState(
          {
            ...this.state,
            portData: data.portitinerary[0],
            portItin: data.portitinerary,
          },
          () => this.setState({ isShowPortRoute: val })
        );
      } else {
        this.setState({ ...this.state, isShowPortRoute: val });
      }
    } else {
      if (key == "port_distance") {
        this.setState({ ...this.state, isShowPortDistance: val });
      }
    }
  };
  render() {
    const {
      frmName,
      portItin,
      formData,
      refreshForm,
      loadComponent,
      isShowEstimateReport,
      showQuickPopup,
      reportFormData,
      portData,
      isShowVoyReletReports,
      title,
      visibleDrawer,
      visisbleCargoChild,
      spType,
      cargoData,
      quickMenuItem,
      showFixMenu,
      fixModal,
      fixData,
      sendBackData,
      showItemList,
      selectedID,
      visibleviewmodal,
      viewData,
      isShowPortRoute,
      isShowPortDistance,
      showLeftBtn,
      fullFormData,
    } = this.state;
    let fieldsVal = {
      port_id: "port_id",
      port: "port",
      funct: "funct",
      passage: "passage",
      speed: "speed",
      miles: "miles",
    };
    let fieldsVal2 = Object.assign(fieldsVal, {
      // xsd: "xsd",
      // day: "days",
      wf_per: "wf_per",
      t_port_days: "t_port_days",
      currency: "currency",
      days: "days",
      eff_speed: "eff_speed",
      funct: "funct",
      // gsd: "gsd",
      l_d_qty: "l_d_qty",
      l_d_rate: "l_d_rate",
      l_d_rate1: "l_d_rate1",
      l_d_term: "l_d_term",
      miles: "miles",
      p_exp: "p_exp",
      passage: "passage",
      port: "port",
      port_id: "port_id",
      spd_type: "s_type",
      speed: "speed",
      tcov_id: "tcov_id",
      tsd: "tsd",
      turn_time: "turn_time",
      wf_per: "wf_per",
      xpd: "xpd",
      xsd: "xsd",
    });

    const { match, estimateID } = this.props;

    return (
      <>
        <div className="wrap-rightbar full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">
                    <article className="article">
                      <h4 className="cust-header-title">
                        <b>
                          Voy Relet Full Estimate : &nbsp;
                          {estimateID
                            ? estimateID
                            : match && match.params && match.params.id
                              ? match.params.id
                              : ""}
                        </b>
                      </h4>
                      <div className="box box-default">
                        <div className="box-body">
                          {refreshForm === false ? (
                            <NormalFormIndex
                              key={"key_" + frmName + "_0"}
                              addForm={true}
                              showForm={true}
                              frmCode={frmName}
                              formClass="label-min-height"
                              formData={formData}
                              extraTableButton={{
                                Cargos: [
                                  {
                                    icon: "eye",
                                    onClickAction: (action, data) => {
                                      let spType = "purchase";
                                      if (
                                        data["cargos"][action.index][
                                          "sp_type"
                                        ] === "186"
                                      )
                                        spType = "sale";
                                      this.onClickExtraIcon(
                                        action,
                                        data,
                                        spType
                                      );
                                    },
                                  },
                                ],
                              }}
                              showToolbar={[
                                {
                                  isLeftBtn: showLeftBtn,
                                  isRightBtn: [
                                    {
                                      isSets: [
                                        quickMenuItem,
                                        {
                                          key: "Menu",
                                          isDropdown: 1,
                                          withText: "Menu",
                                          type: "",
                                          menus: [
                                            {
                                              href: null,
                                              icon: null,
                                              label: "Estimate report",
                                              modalKey: "estimate_report",
                                            },
                                            {
                                              href: null,
                                              icon: null,
                                              label: "P & L report",
                                              modalKey: "pl-report",
                                            },
                                            {
                                              href: null,
                                              icon: null,
                                              label: "Add Cargo ( Purchase )",
                                              modalKey: "add_cargo",
                                              event: (key, data) =>
                                                this.onClickAddCargo(
                                                  {},
                                                  data,
                                                  "purchase"
                                                ),
                                            },
                                            {
                                              href: null,
                                              icon: null,
                                              label: "Add Cargo ( Sale )",
                                              modalKey: "add_cargo",
                                              event: (key, data) =>
                                                this.onClickAddCargo(
                                                  {},
                                                  data,
                                                  "sale"
                                                ),
                                            },
                                          ],
                                        },
                                        showFixMenu,
                                        {
                                          key: "addcargo",
                                          isDropdown: 1,
                                          withText: "Add Cargo",
                                          type: "",
                                          menus: [
                                            {
                                              href: null,
                                              icon: null,
                                              label: "Sale Cargo",
                                              modalKey: "sale_cargo",
                                              event: (key, data) =>
                                                this.onClickAddCargo(
                                                  {},
                                                  data,
                                                  "sale"
                                                ),
                                            },
                                            {
                                              href: null,
                                              icon: null,
                                              label: "VC Purchase",
                                              modalKey: "vc_purchage",
                                              event: (key, data) =>
                                                this.onClickAddCargo(
                                                  {},
                                                  data,
                                                  "purchase"
                                                ),
                                            },
                                          ],
                                        },

                                        {
                                          key: "reports",
                                          isDropdown: 1,
                                          withText: "Reports",
                                          type: "",
                                          // menus: null,
                                          menus: [
                                            {
                                              href: null,
                                              icon: null,
                                              label: "Estimate Details Report",
                                              modalKey: null,
                                              event: (key) =>
                                                this.EstimateReport(true),
                                            },

                                            {
                                              href: null,
                                              icon: null,
                                              label: "Download Report",
                                              modalKey: null,
                                              event: (key) =>
                                                this.VoyReletReports(true),
                                            },
                                          ],
                                        },
                                        // {
                                        //   key: 'port_distance',
                                        //   isDropdown: 0,
                                        //   withText: 'Port Distance',
                                        //   type: '',
                                        //   menus: null,
                                        //   event: (key,data) => this.portDistance(true,data,key),
                                        // },
                                        // {
                                        //   key: 'port_route_details',
                                        //   isDropdown: 0,
                                        //   withText: 'Port Route Details',
                                        //   type: '',
                                        //   menus: null,
                                        //   event: (key,data) => this.portDistance(true,data,key),
                                        // },
                                      ],
                                    },
                                  ],
                                },
                              ]}
                              isShowFixedColumn={[
                                "Port Date Details",
                                "Bunker Details",
                                "Port Itinerary",
                                "Cargos",
                                ".",
                              ]}
                              // tabEvents={[
                              //   {
                              //     tabName: "Bunker Details",
                              //     event: {
                              //       type: "copy",
                              //       from: "Port Itinerary",
                              //       fields: {
                              //         port_id: "port_id",
                              //         port: "port",
                              //       },
                              //       showSingleIndex: false,
                              //     },
                              //   },
                              //   {
                              //     tabName: "Port Date Details",
                              //     event: {
                              //       type: "copy",
                              //       from: "Port Itinerary",
                              //       fields: {
                              //         port_id: "port_id",
                              //         port: "port",
                              //         funct: "funct",
                              //         passage: "passage",
                              //         speed: "speed",
                              //         miles: "miles",
                              //         wf_per: "wf_per",
                              //         tsd: "tsd",
                              //         xsd: "xsd",
                              //         day: "days",
                              //         t_port_days: "pdays",
                              //         s_type: "s_type",
                              //       },
                              //       showSingleIndex: false,
                              //     },
                              //   },
                              // ]}
                              tabEvents={[
                                // {
                                //   tabName: "Port Itinerary",
                                //   event: {
                                //     type: "copy",
                                //     from: "Bunker Details",

                                //     fields: Object.assign(fieldsVal, {
                                //       spd_type: "s_type",
                                //     }),

                                //     showSingleIndex: false,
                                //     group: {
                                //       to: "----------",
                                //       from: "---------",
                                //     },
                                //   },
                                // },
                                {
                                  tabName: "Bunker Details",
                                  event: {
                                    type: "copy",
                                    from: "Port Itinerary",
                                    fields: Object.assign(fieldsVal, {
                                      s_type: "spd_type",
                                      seca_length: "seca_length",
                                      eca_days: "eca_days",
                                    }),

                                    showSingleIndex: false,
                                    group: {
                                      to: "----------",
                                      from: "---------",
                                    },
                                    otherCopy: [
                                      {
                                        gKey: "purchase_data",
                                        tgKey: "bunkerdetails",
                                        type: "consumptionCopy",
                                      },
                                      {
                                        gKey: "portdatedetails",
                                        tgKey: "bunkerdetails",
                                        type: "arrDep",
                                      },
                                    ],
                                  },
                                },
                                {
                                  tabName: "Port Date Details",
                                  event: {
                                    type: "copy",
                                    from: "Port Itinerary",
                                    fields: {
                                      port_id: "port_id",
                                      port: "port",
                                      funct: "funct",
                                      passage: "passage",
                                      speed: "speed",
                                      miles: "miles",
                                      wf_per: "wf_per",
                                      t_port_days: "pdays",
                                      // currency: "currency",
                                      days: "days",
                                      // eff_speed: "eff_speed",
                                      // funct: "funct",
                                      // miles: "miles",
                                      p_exp: "p_exp",
                                      s_type: "s_type",
                                      tsd: "tsd",
                                      wf_per: "wf_per",
                                      xpd: "xpd",
                                      xsd: "xsd",
                                    },
                                    showSingleIndex: false,
                                    group: {
                                      to: "-----------",
                                      from: "---------",
                                    },
                                  },
                                },
                                {
                                  tabName: "Port Itinerary",
                                  event: {
                                    type: "copy",
                                    from: "Port Date Details",
                                    fields: fieldsVal2,
                                    showSingleIndex: false,
                                    group: {
                                      to: "-----------",
                                      from: "---------",
                                    },
                                  },
                                },
                              ]}
                              sideList={{
                                selectedID,
                                showList: true,
                                title: "Voyage Relet List",
                                uri: "/voyage-relet/list?l=0",
                                columns: [
                                  "estimate_id",
                                  "vessel_name",
                                  "tcov_status",
                                ],
                                icon: true,
                                rowClickEvent: (evt) =>
                                  this._onLeftSideListClick(evt),
                              }}
                              showSideListBar={showItemList}
                              tableRowDeleteAction={(action, data) =>
                                this.onClickExtraIcon(action, data)
                              }
                              inlineLayout={true}
                              sendBackData={sendBackData}
                              triggerEvent={this.triggerEvent}
                            />
                          ) : (
                            <Spin tip="Loading...">
                              <Alert
                                message=" "
                                description="Please wait..."
                                type="info"
                              />
                            </Spin>
                          )}
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </Content>
            </Layout>
            <RightBarUI
              pageTitle="voy-relet-righttoolbar"
              callback={(data, options) => this.onClickRightMenu(data, options)}
            />
            {loadComponent !== undefined &&
            title !== undefined &&
            visibleDrawer === true ? (
              <Drawer
                title={this.state.title}
                placement="right"
                closable={true}
                onClose={this.onCloseDrawer}
                open={this.state.visibleDrawer}
                getContainer={false}
                style={{ position: "absolute" }}
                width={this.state.width}
                maskClosable={false}
                className="drawer-wrapper-container"
              >
                <div className="tcov-wrapper">
                  <div className="layout-wrapper scrollHeight">
                    <div className="content-wrapper noHeight">
                      {this.state.loadComponent}
                    </div>
                  </div>
                </div>
              </Drawer>
            ) : (
              undefined
            )}
          </Layout>
          {isShowPortDistance ? (
            <Modal
              style={{ top: "2%" }}
              title="Port Distance"
              open={isShowPortDistance}
              onOk={this.handleOk}
              onCancel={() => this.portDistance(false, {}, "port_distance")}
              width="95%"
              footer={null}
            >
              <PortDistance />
            </Modal>
          ) : (
            undefined
          )}
          {isShowPortRoute ? (
            <Modal
              style={{ top: "2%" }}
              title="Port Route Details"
              open={isShowPortRoute}
              onOk={this.handleOk}
              onCancel={() =>
                this.portDistance(false, {}, "port_route_details")
              }
              width="100%"
              footer={null}
            >
              <ShipDistance
                data={portData}
                is_port_to_port={true}
                portItin={portItin}
              />
            </Modal>
          ) : (
            undefined
          )}
          {showQuickPopup ? (
            <Modal
              style={{ top: "2%" }}
              open={showQuickPopup}
              title="Quick Estimate"
              onCancel={() => this.viewQuickEstimate(false, undefined)}
              footer={null}
              width={"100%"}
              maskClosable={false}
            >
              <QuickEstimate
                estimateID={estimateID}
                showAddButton={false}
                history={this.props.history}
                props={this.props}
              />
            </Modal>
          ) : (
            undefined
          )}
          {visibleviewmodal ? (
            <Modal
              style={{ top: "2%" }}
              title={
                viewData && viewData.sp_type == "187"
                  ? "VC (Purchase)"
                  : " Edit Cargo Contract"
              }
              open={visibleviewmodal}
              onCancel={() => this.setState({ visibleviewmodal: false })}
              width="95%"
              footer={null}
            >
          
              {viewData && viewData.sp_type == "187" ? (
                // <VcPurchage
                //   cargoImport={(data) => this.getCargoImport(data)}
                //   import={true}
                //   isVoyageRelete={true}
                //  // data={viewData}
                //   formData={viewData}
                // />
               
                <CargoDetails
                  formData={viewData}
                  cargoImport={(data) => this.getCargoImport(data)}
                />
              ) : (
                // <AddCargo
                //   cargoImport={(data) => this.getCargoImport(data)}
                //   import={true}
                //   isVoyageRelete={true}
                //   data={viewData}
                // />
                <CargoContract
                  formData={viewData}
                  cargoImport={(data) => this.getCargoImport(data)}
                />
              )}
            </Modal>
          ) : (
            undefined
          )}

          {fixModal ? (
            <Modal
              style={{ top: "2%" }}
              open={fixModal}
              title="Create Voyage"
              onCancel={() => this.onClickFixVoyRelet(false)}
              footer={null}
              width="40%"
              maskClosable={false}
            >
              <div>
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body">
                        <Form>
                          <FormItem {...formItemLayout} label="Vessel">
                            <input
                              type="text"
                              className="ant-input"
                              name="vessel"
                              value={fixData["vessel_name"]}
                              readOnly
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Operation Type">
                            <input
                              type="text"
                              className="ant-input"
                              name="opt_type"
                              value={fixData["ops_type"]}
                              readOnly
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Commence Date">
                            <input
                              type="datatime"
                              className="ant-input"
                              name="commence_date"
                              value={fixData["commence_date"]}
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Completed Date">
                            <input
                              type="datatime"
                              className="ant-input"
                              name="completed_date"
                              value={fixData["completed_date"]}
                            />
                          </FormItem>

                          <div className="action-btn text-right">
                            <Button
                              type="primary"
                              onClick={() => this.createVoyageManger()}
                            >
                              Ok
                            </Button>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Modal>
          ) : (
            undefined
          )}

          {visisbleCargoChild ? (
            <Modal
              style={{ top: "2%" }}
              open={visisbleCargoChild}
              title={spType === "purchase" ? "Purchase Cargo" : "Sales Cargo"}
              onCancel={this.onCancel}
              footer={null}
              width={"90%"}
              maskClosable={false}
            >
              {spType === "sale" ? (
                <CargoContract
                  formData={cargoData}
                  showPendingList={{ cargo_status: "ENQUIRY" }}
                  onSaveEvent={(data) => this.getCargo(data)}
                  isShowImport={true}
                  isShowAddButton={false}
                  fullEstimate={true}
                  modalCloseEvent={this.onCancel}
                  getCargo={this.getCargo}
                />
              ) : spType === "purchase" ? (
                <CargoDetails
                  formData={cargoData}
                  showPendingList={{ c_status: "ENQUIRY" }}
                  onSaveEvent={(data) => this.getCargo(data)}
                  isShowImport={true}
                  fullEstimate={true}
                  modalCloseEvent={this.onCancel}
                  getCargo={this.getCargo}
                />
              ) : (
                undefined
              )}
            </Modal>
          ) : (
            undefined
          )}

          {isShowVoyReletReports ? (
            <Modal
              style={{ top: "2%" }}
              title="Reports"
              open={isShowVoyReletReports}
              onOk={this.handleOk}
              onCancel={() => this.VoyReletReports(false)}
              width="95%"
              footer={null}
            >
              <VoyReletReports data={reportFormData} />
            </Modal>
          ) : (
            undefined
          )}

          {isShowEstimateReport ? (
            <Modal
              style={{ top: "2%" }}
              title="Estimate Detail Report"
              open={isShowEstimateReport}
              onOk={this.handleOk}
              onCancel={() => this.EstimateReport(false)}
              width="95%"
              footer={null}
            >
              <VoyReletEstimateDetail data={reportFormData} />
            </Modal>
          ) : (
            undefined
          )}
        </div>
      </>
    );
  }
}

export default FullEstimate;
