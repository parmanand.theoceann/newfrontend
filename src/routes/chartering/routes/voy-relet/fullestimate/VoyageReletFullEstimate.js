import React, { Component, useEffect } from "react";
import {
  Form,
  Input,
  Table,
  Tabs,
  Drawer,
  Layout,
  Select,
  Modal,
  Button,
  Spin,
  Alert,
} from "antd";
import {
  DeleteOutlined,
  MenuFoldOutlined,
  SaveOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../../shared";

import {
  uploadAttachment,
  deleteAttachment,
} from "../../../../../shared/attachments";
import RightBarUI from "../../../../../components/RightBarUI/";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import VoyReletPortInformation from "../../../../right-port-info/VoyReletPortInformation";
import PL from "../../../../../shared/components/PL/voyrelet";
import Properties from "../right-panel/Properties";
import RightSearch from "../right-panel/RightSearch";
import EstimateSummary from "../../cargo-contract/EstimateSummary";
import VoyReletReports from "../../../../form-reports/VoyReletReports";
import VoyReletEstimateDetail from "../../../../form-reports/VoyReletEstimateDetail";
import CargoContract from "../../../../chartering/routes/cargo-contract";
import CargoDetails from "../../../../chartering/routes/voyage-cargo-in";
import QuickEstimate from "../../../../estimatequick/VoyageReletQuick";
import VcPurchage from "../../voyage-cargo-in/index";
import AddCargo from "../../cargo-contract/index";
import PortDistance from "../../../../port-to-port/PortAnalytics";
import ShipDistance from "../../../../port-to-port/ShipDetails";
import AttachmentFile from "../../../../../shared/components/Attachment";
import { useRef } from "react";

const FormItem = Form.Item;
const { Content } = Layout;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

let fieldsVal = {
  port_id: "port_id",
  port: "port",
  funct: "funct",
  passage: "passage",
  speed: "speed",
  miles: "miles",
};
let fieldsVal2 = Object.assign(fieldsVal, {
  // xsd: "xsd",
  // day: "days",
  wf_per: "wf_per",
  t_port_days: "t_port_days",
  currency: "currency",
  days: "days",
  eff_speed: "eff_speed",
  funct: "funct",
  // gsd: "gsd",
  l_d_qty: "l_d_qty",
  l_d_rate: "l_d_rate",
  l_d_rate1: "l_d_rate1",
  l_d_term: "l_d_term",
  miles: "miles",
  p_exp: "p_exp",
  passage: "passage",
  port: "port",
  port_id: "port_id",
  spd_type: "s_type",
  speed: "speed",
  tcov_id: "tcov_id",
  tsd: "tsd",
  turn_time: "turn_time",
  wf_per: "wf_per",
  xpd: "xpd",
  xsd: "xsd",
});

const VoyageReletFullEstimate = (props) => {
  const [state, setState] = useStateCallback({
    frmName: "VOYAGE_RELET",
    portItin: [],
    isShowPortDistance: false,
    isShowPortRoute: false,
    index: 0,
    refreshForm: true,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    disableSave: false,
    width: 1200,
    isShowVoyReletReports: false,
    isShowEstimateReport: false,
    estimateID: undefined,
    spType: undefined,
    visisbleCargoChild: false,
    cargoData: {},
    fullFormData: {},
    portData: null,
    formData: props.formData || {},
    visibleviewmodal: false,
    viewData: {},
    reportFormData: {
      portitinerary: [
        {
          port_id: "4444",
          port: "2944",
          funct: 3,
          s_type: 1,
          wf_per: 10,
          miles: "0.00",
          speed: "0.00",
          eff_speed: "0.00",
          gsd: "0.00",
          tsd: "0.00",
          xsd: "0.00",
          l_d_qty: "0.00",
          l_d_rate: "0.00",
          l_d_rate1: "0.00",
          turn_time: "0.00",
          days: "0.00",
          xpd: "0.00",
          p_exp: "0.00",
          t_port_days: "0.00",
          l_d_term: 1,
          editable: true,
          index: 0,
          ID: -9e6,
        },
        {
          port_id: "6297",
          port: "5530",
          s_type: 1,
          wf_per: 10,
          miles: "0.00",
          speed: "0.00",
          eff_speed: "0.00",
          gsd: "0.00",
          tsd: "0.00",
          xsd: "0.00",
          l_d_qty: "0.00",
          l_d_rate: "0.00",
          l_d_rate1: "0.00",
          turn_time: "0.00",
          days: "0.00",
          xpd: "0.00",
          p_exp: "0.00",
          t_port_days: "0.00",
          l_d_term: 1,
          editable: true,
          index: 1,
          ID: -9e6 + 1,
        },
      ],
    },
    tableID: -9e6,
    indexData: 0,
    quickMenuItem: undefined,
    showQuickPopup: false,
    fixData: {},
    fixModal: false,
    showItemList: false,
    sendBackData: { show: false },
    showFixMenu: {
      key: "fixopt",
      isDropdown: 0,
      withText: "Fix & Schedule",
      type: "",
      menus: null,
      event: (key) => onClickFixVoyRelet(true),
    },
    showLeftBtn: [
      {
        isSets: [
          {
            id: "7",
            key: "menu-fold",
            type: <MenuFoldOutlined />,
            withText: "",
            event: "menu-fold",
          },
          // { id: '1', key: 'add', type: <PlusOutlined />, withText: '' },
          // (formData && !formData['generated_voyage'] && !formData['tcov_status'] ?
          {
            id: "3",
            key: "save",
            type: <SaveOutlined />,
            withText: "",
            event: (key, data) => saveForm(key, data),
          },
          //  : undefined),
          {
            id: "4",
            key: "delete",
            type: <DeleteOutlined />,
            withText: "",
          },
          {
            id: "20",
            key: "refresh",
            type: <SyncOutlined />,
            withText: "",
            event: (key, data) => reFreshForm(),
          },
        ],
      },
    ],
    selectedID: null,
    vmCheck: props.vmCheck != undefined ? props.vmCheck : false,
  });

  const params = useParams();
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const { estimateID } = state;
    if (
      estimateID ||
      location.pathname == `/voy-relet-full-estimate-edit/${params.id}`
    ) {
      let est_id = params?.id ?? estimateID;
      getcomponentMount(est_id);
    }
  }, []);

  let formref = useRef(null);

  const reFreshForm = () => {
    setState(
      (prevState) => ({
        ...prevState,
        formData: { ...formref.current },
        //formData: {},
        refreshForm: false,
      }),
      () => {
        setState({ ...state, refreshForm: true });
      }
    );

  };

  useEffect(() => {
    formref.current = Object.assign({}, state.formData);
  }, []);

  const getcomponentMount = async (estimateID = null) => {
    const { history } = props;
    let fullEstData = location?.state?.data;
    const { showLeftBtn, formData, quickMenuItem, showFixMenu } = state;
    let vessel = undefined,
      cp_echo = undefined,
      _quickMenuItem = quickMenuItem,
      _showQuickEstimate = undefined,
      _showFixMenu = undefined;
    let response = null,
      respDataC = {},
      respData = {
        cargos: [],
        portitinerary: [],
        bunkerdetails: [],
      },
      fixData = null,
      fieldOptions = undefined,
      _showLeftBtn = Object.assign([], showLeftBtn);

    if (estimateID) {
      setState({ ...state, refreshForm: true }, () =>
        navigate(`/voy-relet-full-estimate-edit/${estimateID}`)
      );
      let qParams = { ae: estimateID };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/edit?${qParamString}`
      );
      respDataC = await response;
      respData = respDataC["data"];
      if (respData.tcov_status == 1) {
        setState({
          disableSave: true,
        });
      }
      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
      );
      vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}/voyrelet/${estimateID}`
      );
      cp_echo = await response["data"];

      //if (respDataC.hasOwnProperty('row') && respDataC['row'] === 'quickestimate') {
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];

      //}
      respData["-"] = respData.hasOwnProperty("-")
        ? respData["-"]
        : cp_echo["-"];
      respData["."] = respData.hasOwnProperty(".")
        ? respData["."]
        : cp_echo["."]["eco_data"];

      if (respData && respData.hasOwnProperty("fix")) {
        fixData = respData["fix"];
        delete respData["fix"];
      }

      if (
        respData &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] == "0"
      ) {
        _showFixMenu = showFixMenu;
      }

      if (
        respData &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] == "1"
      ) {
        delete _showLeftBtn[0]["isSets"][2];
        delete _showLeftBtn[0]["isSets"][1];
      }
      if (!state.vmCheck) {
        _quickMenuItem = {
          key: "quick_estimate",
          isDropdown: 0,
          withText: "View Quick Est.",
          type: "",
          event: () => navigate(`/edit-voy-relet/${estimateID}`),
        };
      }
    }
    if (JSON.stringify(respData) === JSON.stringify({})) {
      respData = Object.assign({}, formData);
    }

    if (fullEstData) {
      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${fullEstData.vesseldetails.vessel_id}`
      );
      vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${fullEstData.vesseldetails.vessel_id}`
      );

      cp_echo = await response["data"];

      respData["cargos"].push(
        {
          sp_type: "Purchase",
          cargo_name: fullEstData.purchasecargorevenue.cargo_name,
          charterer: fullEstData.purchasecargorevenue.owner,
          quantity: fullEstData.purchasecargorevenue.quantity,
          quantity: fullEstData.purchasecargorevenue.quantity,
          unit: fullEstData.purchasecargorevenue.unit,
          f_type: fullEstData.purchasecargorevenue.f_type,
          f_rate: fullEstData.purchasecargorevenue.f_rate,
          commission: fullEstData.purchasecargorevenue.commission,
          extra_rev: fullEstData.purchasecargorevenue.other_revenue,
        },
        {
          sp_type: "Sell",
          cargo_name: fullEstData.salecargoexpense.s_cargo_name,
          charterer: fullEstData.salecargoexpense.charterer,
          quantity: fullEstData.salecargoexpense.s_quantity,
          unit: fullEstData.salecargoexpense.s_unit,
          f_type: fullEstData.salecargoexpense.s_f_type,
          f_rate: fullEstData.salecargoexpense.s_f_rate,
          commission: fullEstData.salecargoexpense.s_commission,
          extra_rev: fullEstData.salecargoexpense.other_cost,
        }
      );

      respData["portitinerary"].push({
        funct: 3,
        port: fullEstData.vesseldetails.ballast_port_name,
        port_id: fullEstData.vesseldetails.ballast_port,
        passage: 1,
        s_type: 1,
        miles: "0.00",
        wf_per: "0.00",
        eff_speed: "0.00",
        gsd: "0.00",
        tsd: "0.00",
        speed: fullEstData.vesseldetails.ballast_spd,
      });
      respData["bunkerdetails"].push({
        funct: 3,
        port: fullEstData.vesseldetails.ballast_port_name,
        port_id: fullEstData.vesseldetails.ballast_port,
        passage: 1,
        s_type: 1,
        miles: "0.00",
        wf_per: "0.00",
        eff_speed: "0.00",
      });
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];

      respData["-"] = respData.hasOwnProperty("-")
        ? respData["-"]
        : cp_echo["-"];
      respData["."] = respData.hasOwnProperty(".")
        ? respData["."]
        : cp_echo["."]["cp_data"];

      if (fullEstData && fullEstData.estimate_id) {
        respData["estimate_id"] = fullEstData.estimate_id;
      }
      if (fullEstData && fullEstData.vesseldetails.other_cost) {
        respData["mis_cost"] = fullEstData.vesseldetails.other_cost;
      }
      if (fullEstData && fullEstData.id) {
        respData["tcov_ref_id"] = fullEstData.id;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.vessel_id
      ) {
        respData["vessel_id"] = fullEstData.vesseldetails.vessel_id;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.bb
      ) {
        respData["bb"] = fullEstData.vesseldetails.bb;
      }

      if (
        fullEstData &&
        fullEstData.voyageresults &&
        fullEstData.voyageresults.total_voyage_days
      ) {
        respData["total_days"] = fullEstData.voyageresults.total_voyage_days;
      }

      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.reposition_port_name
      ) {
        respData["reposition_port_name"] =
          fullEstData.vesseldetails.reposition_port_name;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.reposition_port
      ) {
        respData["reposition_port"] = fullEstData.vesseldetails.reposition_port;
      }

      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.ballast_port_name
      ) {
        respData["ballast_port_name"] =
          fullEstData.vesseldetails.ballast_port_name;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.ballast_port
      ) {
        respData["ballast_port"] = fullEstData.vesseldetails.ballast_port;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.commence_date
      ) {
        respData["commence_date"] = fullEstData.vesseldetails.commence_date;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.completed_date
      ) {
        respData["completed_date"] = fullEstData.vesseldetails.completed_date;
      }
      if (
        fullEstData &&
        fullEstData.vesseldetails &&
        fullEstData.vesseldetails.dwt
      ) {
        respData["dwt"] = fullEstData.vesseldetails.dwt;
      }
    }

    setState(
      {
        ...state,
        showLeftBtn: _showLeftBtn,
        showItemList: respData["id"] ? false : true,
        formData: respData,
        fixData: fixData,
        quickMenuItem: _quickMenuItem,
        fieldOptions: fieldOptions,
        showQuickEstimate: _showQuickEstimate,
        showFixMenu: _showFixMenu,
        estimateID: estimateID,
      },
      //() => setState({ ...state, refreshForm: false })
      (prevState) => {
        const updatedState = {
          ...prevState,
          refreshForm: false,
        };
        setState(updatedState);
      }
    );
  };

  const onClickRightMenu = async (key, options) => {
    onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "port_route_details":
        portDistance(true, state.formData, key);
        break;
      case "port_distance":
        portDistance(true, state.formData, key);
        break;
      case "portinformation":
        loadComponent = <VoyReletPortInformation />;
        break;
      case "pl":
        setState({
          ...state,
          sendBackData: { show: true, options: options },
        });
        break;
      case "summary":
        loadComponent = <EstimateSummary />;
        break;

      case "properties":
        loadComponent = <Properties />;
        break;
      case "search":
        loadComponent = <RightSearch />;
        break;
      case "attachment":
        const { estimateID } = state;
        if (estimateID) {
          const url = `${URL_WITH_VERSION}/importexport/fetch?eid=${estimateID}`;
          const response = await getAPICall(url);
          const attachments = response["data"] || [];
          const callback = (fileArr) => uploadAttachment(fileArr, estimateID);
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) => deleteAttachment(file.url, file.name)}
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon("info", "Attachments not allowed here.");
        }
        break;
      default:
        break;
    }

    if (loadComponent) {
      setState({
        ...state,
        visibleDrawer: true,
        title: options.title,
        loadComponent: loadComponent,
        width: options.width && options.width > 0 ? options.width : 1200,
      });
    }
  };

  const _onLeftSideListClick = async (evt) => {
    setState({ ...state, refreshForm: true, selectedID: evt.id });
    getcomponentMount(evt["estimate_id"]);
  };

  const onClickFixVoyRelet = (boolVal) =>
    setState({ ...state, fixModal: boolVal });

  const onCloseDrawer = () =>
    setState({
      ...state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  const VoyReletReports = async (showVoyReletReports) => {
    let qParamString = objectToQueryStringFunc({ ae: state.estimateID });
    // for report Api
    if (showVoyReletReports == true) {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/report?${qParamString}`
      );
      const respDataReport = await responseReport["data"];

      if (responseReport) {
        setState({ ...state, reportFormData: respDataReport }, () =>
          setState({
            ...state,
            isShowVoyReletReports: showVoyReletReports,
          })
        );
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } else {
      setState({
        ...state,
        isShowVoyReletReports: showVoyReletReports,
      });
    }
  };

  const EstimateReport = async (showEstimateReport) => {
    let qParamString = objectToQueryStringFunc({ ae: state.estimateID });
    if (showEstimateReport == true) {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/estimatereport?${qParamString}`
      );
      const respDataReport = await responseReport["data"];

      if (respDataReport) {
        setState({ ...state, reportFormData: respDataReport }, () =>
          setState({
            ...state,
            isShowEstimateReport: showEstimateReport,
          })
        );
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } else {
      setState({
        ...state,
        isShowEstimateReport: showEstimateReport,
      });
    }
  };

  const onCancel = () => setState({ ...state, visisbleCargoChild: false });
  const viewQuickEstimate = (bolVal, estimateID) =>
    setState({ ...state, estimateID: estimateID }, () =>
      setState({ ...state, showQuickPopup: bolVal })
    );

  const saveForm = (key, data) => {
    const { frmName } = state;
    if (data) {
      let _url = `${URL_WITH_VERSION}/voyage-relet/save?frm=${frmName}`;
      let _method = "POST";

      if (data["id"] > 0) {
        _url = `${URL_WITH_VERSION}/voyage-relet/update?frm=${frmName}`;
        _method = "PUT";
        if (data && data.hasOwnProperty("purchase_data"))
          delete data.purchase_data;
      }
      // if (_method === "POST" && data && data.hasOwnProperty('portitinerary') && data.portitinerary.length > 0) {
      //   data.portitinerary.shift()
      // }
      // if (data && data.hasOwnProperty("portitinerary") && data.portitinerary.length > 0) {
      //   let data1 = data.portitinerary.map((val, key) => {
      //     let val1 = { ...val }
      //     val1['port_id'] = val1['port_id'];
      //     return val1
      //   })
      //   data['portitinerary']=data1
      // }
      if (!state.disableSave) {
        postAPICall(`${_url}`, data, _method, (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);

            props.history.replace("/empty");
            setTimeout(() => {
              props.history.replace(
                `/voy-relet-full-estimate-edit/${response.data}`
              );
            }, 5);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        });
      } else {
        openNotificationWithIcon("error", "This Estimate is already fixed");
      }
    }
  };

  const getCargo = (data) => {
    let cargo_contract_id = "";
    if (data["sp_type"] == 187) {
      cargo_contract_id = data["vc_purchase_id"];
    } else {
      cargo_contract_id = data["cargo_contract_id"];
    }
    setState({ ...state, refreshForm: true, visisbleCargoChild: false }, () => {
      let _fullFormData = Object.assign({}, state.fullFormData);
      let _cargoTermData = {};

      _cargoTermData = {
        sp_type: data["sp_type"],
        serial_no: _fullFormData["cargos"].length,
        cargo_contract_id: cargo_contract_id,
        cargo_name: data["cargo_name"],
        charterer: data["charterer"],
        quantity: data["cp_qty"],
        unit: data["cp_unit"],
        option_percentage: "0.00",
        option_type: undefined,
        f_type: data["freight_type"],
        f_rate: data["freight_rate"],
        lumpsum: "0.00",
        commission: "0.00",
        extra_rev: "0.00",
        curr: data["currency"],
        dem_rate_pd: "0.00",
        editable: true,
        index:
          _fullFormData["cargos"].length > 2
            ? _fullFormData["cargos"].length - 1
            : _fullFormData["cargos"].length,
        ocd: true,
        id: state.tableID + _fullFormData["cargos"].length,
      };
      if (
        _fullFormData.hasOwnProperty("cargos") &&
        _fullFormData["cargos"].length > 0
      ) {
        _fullFormData["cargos"].push(_cargoTermData);
      } else {
        _fullFormData["cargos"] = [{ ..._cargoTermData }];
      }
      setState(
        {
          ...state,
          formData: _fullFormData,
          fullFormData: {},
          indexData: 0,
        },
        () => setState({ ...state, refreshForm: false })
      );
    });
  };

  const onClickAddCargo = (
    cargoContractData = {},
    data = {},
    dType = "purchase"
  ) => {
    setState(
      {
        ...state,
        cargoData: cargoContractData,
        fullFormData: data,
        spType: dType,
      },
      () => setState({ ...state, visisbleCargoChild: true })
    );
  };

  const onClickExtraIcon = (action, data) => {
    if (data.hasOwnProperty("cargos")) {
      let dataNew =
        data && data["cargos"].length > 0 ? data["cargos"][action.index] : {};
      setState(
        {
          viewData: dataNew,
          index: action.index,
          fullFormData: data,
        },
        () =>
          setState({
            visibleviewmodal: true,
          })
      );
    } else {
      let delete_id = data && data.id;
      let groupKey = action["gKey"];
      let frm_code = "";
      if (groupKey == "Cargos") {
        groupKey = "cargos";
        frm_code = "VOYAGE_RELET";
      }
      if (groupKey == "Port Itinerary") {
        groupKey = "portitinerary";
        frm_code = "TAB_VOYAGE_RELET_PORT_ITINERARY";
      }
      if (groupKey == "Bunker Details") {
        groupKey = "bunkerdetails";
        frm_code = "TAB_VOYAGE_RELET_BUNKER_DETAILS";
      }

      if (groupKey == "Port Date Details") {
        groupKey = "portdatedetails";
        frm_code = "TAB_VOYAGE_RELET_PORT_DATE_DETAILS";
      }
      if (groupKey == ".") {
        frm_code = "VOYAGE_RELET";
      }
      if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
        let data1 = {
          id: delete_id,
          frm_code: frm_code,
          group_key: groupKey,
        };

        postAPICall(
          `${URL_WITH_VERSION}/tr-delete`,
          data1,
          "delete",
          (response) => {
            if (response && response.data) {
              openNotificationWithIcon("success", response.message);
            } else {
              openNotificationWithIcon("error", response.message);
            }
          }
        );
      }
    }
  };

  const createVoyageManger = () => {
    const { fixData } = state;
    postAPICall(
      `${URL_WITH_VERSION}/voyage-relet/fix`,
      fixData,
      "POST",
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          props.history.push(`/voyage-manager/${fixData["estimate_id"]}`);
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
              (msg +=
                "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div
              className="notify-error"
              dangerouslySetInnerHTML={{ __html: msg }}
            />
          );
        }
      }
    );
  };

  const triggerEvent = (data) => {
    const { sendBackData } = state;
    if (data && sendBackData["show"] === true) {
      onCloseDrawer();
      let loadComponent = <PL formData={data} viewTabs={["Estimate View"]} />;
      setState({
        ...state,
        visibleDrawer: true,
        title: sendBackData.options.title,
        loadComponent: loadComponent,
        width:
          sendBackData.options.width && sendBackData.options.width > 0
            ? sendBackData.options.width
            : 1200,
        sendBackData: { show: false },
      });
    }
  };

  const getCargoImport = (data) => {
    const { fullFormData, index } = state;
    let frmData = Object.assign({}, fullFormData);
    let dataNew =
      frmData && frmData["cargos"].length > 0 ? frmData["cargos"][index] : {};
    setState(
      {
        refreshForm: true,
        visibleviewmodal: false,
      },
      () => {
        dataNew["cargo_name"] = data && data.cargo_name;
        dataNew["charterer"] = data && data.charterer;
        dataNew["f_rate"] = data && data.f_rate;
        dataNew["f_type"] = data && data.f_type;
        dataNew["curr"] = data && data.curr;
        frmData["cargos"][index] = dataNew;
        setState({
          ...state,
          refreshForm: false,
          formData: frmData,
          fullFormData: frmData,
        });
      }
    );
  };

  const portDistance = (val, data, key) => {
    if (key == "port_route_details") {
      if (
        data &&
        data.hasOwnProperty("portitinerary") &&
        data["portitinerary"].length > 0
      ) {
        setState(
          {
            ...state,
            portData: data.portitinerary[0],
            portItin: data.portitinerary,
          },
          () => setState({ isShowPortRoute: val })
        );
      } else {
        setState({ ...state, isShowPortRoute: val });
      }
    } else {
      if (key == "port_distance") {
        setState({ ...state, isShowPortDistance: val });
      }
    }
  };

  return (
    <div className="wrap-rightbar full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <h4 className="cust-header-title">
                    <b>
                      Voy Relet Full Estimate : &nbsp;
                      {state?.estimateID ?? params.id}
                    </b>
                  </h4>
                  <div className="box box-default">
                    <div className="box-body">
                      {state.refreshForm === true ?
                        <NormalFormIndex
                          key={"key_" + state.frmName + "_0"}
                          addForm={true}
                          showForm={true}
                          frmCode={state.frmName}
                          formClass="label-min-height"
                          formData={state.formData}
                          extraTableButton={{
                            Cargos: [
                              {
                                icon: "eye",
                                onClickAction: (action, data) => {
                                  let spType = "purchase";
                                  if (
                                    data["cargos"][action.index]["sp_type"] ===
                                    "186"
                                  )
                                    spType = "sale";
                                  onClickExtraIcon(action, data, spType);
                                },
                              },
                            ],
                          }}
                          showToolbar={[
                            {
                              isLeftBtn: state.showLeftBtn,
                              isRightBtn: [
                                {
                                  isSets: [
                                    state.quickMenuItem,
                                    {
                                      key: "Menu",
                                      isDropdown: 1,
                                      withText: "Menu",
                                      type: "",
                                      menus: [
                                        {
                                          href: null,
                                          icon: null,
                                          label: "Estimate report",
                                          modalKey: "estimate_report",
                                        },
                                        {
                                          href: null,
                                          icon: null,
                                          label: "P & L report",
                                          modalKey: "pl-report",
                                        },
                                        {
                                          href: null,
                                          icon: null,
                                          label: "Add Cargo ( Purchase )",
                                          modalKey: "add_cargo",
                                          event: (key, data) =>
                                            onClickAddCargo(
                                              {},
                                              data,
                                              "purchase"
                                            ),
                                        },
                                        {
                                          href: null,
                                          icon: null,
                                          label: "Add Cargo ( Sale )",
                                          modalKey: "add_cargo",
                                          event: (key, data) =>
                                            onClickAddCargo({}, data, "sale"),
                                        },
                                      ],
                                    },
                                    state.showFixMenu,
                                    {
                                      key: "addcargo",
                                      isDropdown: 1,
                                      withText: "Add Cargo",
                                      type: "",
                                      menus: [
                                        {
                                          href: null,
                                          icon: null,
                                          label: "Sale Cargo",
                                          modalKey: "sale_cargo",
                                          event: (key, data) =>
                                            onClickAddCargo({}, data, "sale"),
                                        },
                                        {
                                          href: null,
                                          icon: null,
                                          label: "VC Purchase",
                                          modalKey: "vc_purchage",
                                          event: (key, data) =>
                                            onClickAddCargo(
                                              {},
                                              data,
                                              "purchase"
                                            ),
                                        },
                                      ],
                                    },

                                    {
                                      key: "reports",
                                      isDropdown: 1,
                                      withText: "Reports",
                                      type: "",
                                      // menus: null,
                                      menus: [
                                        {
                                          href: null,
                                          icon: null,
                                          label: "Estimate Details Report",
                                          modalKey: null,
                                          event: (key) => EstimateReport(true),
                                        },

                                        {
                                          href: null,
                                          icon: null,
                                          label: "Download Report",
                                          modalKey: null,
                                          event: (key) => VoyReletReports(true),
                                        },
                                      ],
                                    },
                                    // {
                                    //   key: 'port_distance',
                                    //   isDropdown: 0,
                                    //   withText: 'Port Distance',
                                    //   type: '',
                                    //   menus: null,
                                    //   event: (key,data) => portDistance(true,data,key),
                                    // },
                                    // {
                                    //   key: 'port_route_details',
                                    //   isDropdown: 0,
                                    //   withText: 'Port Route Details',
                                    //   type: '',
                                    //   menus: null,
                                    //   event: (key,data) => portDistance(true,data,key),
                                    // },
                                  ],
                                },
                              ],
                            },
                          ]}
                          isShowFixedColumn={[
                            "Port Date Details",
                            "Bunker Details",
                            "Port Itinerary",
                            "Cargos",
                            ".",
                          ]}
                          // tabEvents={[
                          //   {
                          //     tabName: "Bunker Details",
                          //     event: {
                          //       type: "copy",
                          //       from: "Port Itinerary",
                          //       fields: {
                          //         port_id: "port_id",
                          //         port: "port",
                          //       },
                          //       showSingleIndex: false,
                          //     },
                          //   },
                          //   {
                          //     tabName: "Port Date Details",
                          //     event: {
                          //       type: "copy",
                          //       from: "Port Itinerary",
                          //       fields: {
                          //         port_id: "port_id",
                          //         port: "port",
                          //         funct: "funct",
                          //         passage: "passage",
                          //         speed: "speed",
                          //         miles: "miles",
                          //         wf_per: "wf_per",
                          //         tsd: "tsd",
                          //         xsd: "xsd",
                          //         day: "days",
                          //         t_port_days: "pdays",
                          //         s_type: "s_type",
                          //       },
                          //       showSingleIndex: false,
                          //     },
                          //   },
                          // ]}
                          tabEvents={[
                            // {
                            //   tabName: "Port Itinerary",
                            //   event: {
                            //     type: "copy",
                            //     from: "Bunker Details",

                            //     fields: Object.assign(fieldsVal, {
                            //       spd_type: "s_type",
                            //     }),

                            //     showSingleIndex: false,
                            //     group: {
                            //       to: "----------",
                            //       from: "---------",
                            //     },
                            //   },
                            // },
                            {
                              tabName: "Bunker Details",
                              event: {
                                type: "copy",
                                from: "Port Itinerary",
                                fields: Object.assign(fieldsVal, {
                                  s_type: "spd_type",
                                  seca_length: "seca_length",
                                  eca_days: "eca_days",
                                }),

                                showSingleIndex: false,
                                group: {
                                  to: "----------",
                                  from: "---------",
                                },
                                otherCopy: [
                                  {
                                    gKey: "purchase_data",
                                    tgKey: "bunkerdetails",
                                    type: "consumptionCopy",
                                  },
                                  {
                                    gKey: "portdatedetails",
                                    tgKey: "bunkerdetails",
                                    type: "arrDep",
                                  },
                                ],
                              },
                            },
                            {
                              tabName: "Port Date Details",
                              event: {
                                type: "copy",
                                from: "Port Itinerary",
                                fields: {
                                  port_id: "port_id",
                                  port: "port",
                                  funct: "funct",
                                  passage: "passage",
                                  speed: "speed",
                                  miles: "miles",
                                  wf_per: "wf_per",
                                  t_port_days: "pdays",
                                  // currency: "currency",
                                  days: "days",
                                  // eff_speed: "eff_speed",
                                  // funct: "funct",
                                  // miles: "miles",
                                  p_exp: "p_exp",
                                  s_type: "s_type",
                                  tsd: "tsd",
                                  wf_per: "wf_per",
                                  xpd: "xpd",
                                  xsd: "xsd",
                                },
                                showSingleIndex: false,
                                group: {
                                  to: "-----------",
                                  from: "---------",
                                },
                              },
                            },
                            {
                              tabName: "Port Itinerary",
                              event: {
                                type: "copy",
                                from: "Port Date Details",
                                fields: fieldsVal2,
                                showSingleIndex: false,
                                group: {
                                  to: "-----------",
                                  from: "---------",
                                },
                              },
                            },
                          ]}
                          sideList={{
                            selectedID: state.selectedID,
                            showList: true,
                            title: "Voyage Relet List",
                            uri: "/voyage-relet/list?l=0",
                            columns: [
                              "estimate_id",
                              "vessel_name",
                              "tcov_status",
                            ],
                            icon: true,
                            rowClickEvent: (evt) => _onLeftSideListClick(evt),
                          }}
                          showSideListBar={state.showItemList}
                          tableRowDeleteAction={(action, data) =>
                            onClickExtraIcon(action, data)
                          }
                          inlineLayout={true}
                          sendBackData={state.sendBackData}
                          triggerEvent={triggerEvent}
                        />
                        : undefined}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
        <RightBarUI
          pageTitle="voy-relet-righttoolbar"
          callback={(data, options) => onClickRightMenu(data, options)}
        />
        {state.loadComponent !== undefined &&
          state.title !== undefined &&
          state.visibleDrawer === true ? (
          <Drawer
            title={state.title}
            placement="right"
            closable={true}
            onClose={onCloseDrawer}
            open={state.visibleDrawer}
            getContainer={false}
            style={{ position: "absolute" }}
            width={state.width}
            maskClosable={false}
            className="drawer-wrapper-container"
          >
            <div className="tcov-wrapper">
              <div className="layout-wrapper scrollHeight">
                <div className="content-wrapper noHeight">
                  {state.loadComponent}
                </div>
              </div>
            </div>
          </Drawer>
        ) : undefined}
      </Layout>
      {state.isShowPortDistance ? (
        <Modal
          style={{ top: "2%" }}
          title="Port Distance"
          open={state.isShowPortDistance}

          onCancel={() => portDistance(false, {}, "port_distance")}
          width="95%"
          footer={null}
        >
          <PortDistance />
        </Modal>
      ) : undefined}
      {state.isShowPortRoute ? (
        <Modal
          style={{ top: "2%" }}
          title="Port Route Details"
          open={state.isShowPortRoute}

          onCancel={() => portDistance(false, {}, "port_route_details")}
          width="100%"
          footer={null}
        >
          <ShipDistance
            data={state.portData}
            is_port_to_port={true}
            portItin={state.portItin}
          />
        </Modal>
      ) : undefined}
      {state.showQuickPopup ? (
        <Modal
          style={{ top: "2%" }}
          open={state.showQuickPopup}
          title="Quick Estimate"
          onCancel={() => viewQuickEstimate(false, undefined)}
          footer={null}
          width={"100%"}
          maskClosable={false}
        >
          <QuickEstimate
            estimateID={state.estimateID}
            showAddButton={false}
            history={props.history}
            props={props}
          />
        </Modal>
      ) : undefined}
      {state.visibleviewmodal ? (
        <Modal
          style={{ top: "2%" }}
          title={
            state.viewData && state.viewData.sp_type == "187"
              ? "VC (Purchase)"
              : " Edit Cargo Contract"
          }
          open={state.visibleviewmodal}
          onCancel={() => setState({ visibleviewmodal: false })}
          width="95%"
          footer={null}
        >
          {state.viewData && state.viewData.sp_type == "187" ? (
            // <VcPurchage
            //   cargoImport={(data) => getCargoImport(data)}
            //   import={true}
            //   isVoyageRelete={true}
            //  // data={viewData}
            //   formData={viewData}
            // />

            <CargoDetails
              formData={state.viewData}
              cargoImport={(data) => getCargoImport(data)}
            />
          ) : (
            // <AddCargo
            //   cargoImport={(data) => getCargoImport(data)}
            //   import={true}
            //   isVoyageRelete={true}
            //   data={viewData}
            // />
            <CargoContract
              formData={state.viewData}
              cargoImport={(data) => getCargoImport(data)}
            />
          )}
        </Modal>
      ) : undefined}

      {state.fixModal ? (
        <Modal
          style={{ top: "2%" }}
          open={state.fixModal}
          title="Create Voyage"
          onCancel={() => onClickFixVoyRelet(false)}
          footer={null}
          width="40%"
          maskClosable={false}
        >
          <div>
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    <Form>
                      <FormItem {...formItemLayout} label="Vessel">
                        <input
                          type="text"
                          className="ant-input"
                          name="vessel"
                          value={state.fixData["vessel_name"]}
                          readOnly
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Operation Type">
                        <input
                          type="text"
                          className="ant-input"
                          name="opt_type"
                          value={state.fixData["ops_type"]}
                          readOnly
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Commence Date">
                        <input
                          type="datatime"
                          className="ant-input"
                          name="commence_date"
                          value={state.fixData["commence_date"]}
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Completed Date">
                        <input
                          type="datatime"
                          className="ant-input"
                          name="completed_date"
                          value={state.fixData["completed_date"]}
                        />
                      </FormItem>

                      <div className="action-btn text-right">
                        <Button
                          type="primary"
                          onClick={() => createVoyageManger()}
                        >
                          Ok
                        </Button>
                      </div>
                    </Form>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </Modal>
      ) : undefined}

      {state.visisbleCargoChild ? (
        <Modal
          style={{ top: "2%" }}
          open={state.visisbleCargoChild}
          title={state.spType === "purchase" ? "Purchase Cargo" : "Sales Cargo"}
          onCancel={onCancel}
          footer={null}
          width={"90%"}
          maskClosable={false}
        >
          {state.spType === "sale" ? (
            <CargoContract
              formData={state.cargoData}
              showPendingList={{ cargo_status: "ENQUIRY" }}
              onSaveEvent={(data) => getCargo(data)}
              isShowImport={true}
              isShowAddButton={false}
              fullEstimate={true}
              modalCloseEvent={onCancel}
              getCargo={getCargo}
            />
          ) : state.spType === "purchase" ? (
            <CargoDetails
              formData={state.cargoData}
              showPendingList={{ c_status: "ENQUIRY" }}
              onSaveEvent={(data) => getCargo(data)}
              isShowImport={true}
              fullEstimate={true}
              modalCloseEvent={onCancel}
              getCargo={getCargo}
            />
          ) : undefined}
        </Modal>
      ) : undefined}

      {state.isShowVoyReletReports ? (
        <Modal
          style={{ top: "2%" }}
          title="Reports"
          open={state.isShowVoyReletReports}

          onCancel={() => VoyReletReports(false)}
          width="95%"
          footer={null}
        >
          <VoyReletReports data={state.reportFormData} />
        </Modal>
      ) : undefined}

      {state.isShowEstimateReport ? (
        <Modal
          style={{ top: "2%" }}
          title="Estimate Detail Report"
          open={state.isShowEstimateReport}

          onCancel={() => EstimateReport(false)}
          width="95%"
          footer={null}
        >
          <VoyReletEstimateDetail data={state.reportFormData} />
        </Modal>
      ) : undefined}
    </div>
  );
};

export default VoyageReletFullEstimate;
