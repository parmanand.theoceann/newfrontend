import React, { useEffect, useRef, useState } from "react";
import {
  Row,
  Col,
  Layout,
  Form,
  Input,
  Drawer,
  Modal,
  notification,
} from "antd";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
  getAPICall,
  awaitPostAPICall,
  apiDeleteCall,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../shared";
//import DemdesTerm from './Demdesterm/';
import RightBarUI from "../../../../components/RightBarUI";
import PlSummary from "./PlSummary";
import EstimateSummary from "./EstimateSummary";
import Properties from "../tcov/Properties";
import CargoDetails from "../voyage-cargo-in";
import CoaVcReports from "../../../form-reports/CoaVcReports";
import Attachment from "../../../../shared/components/Attachment";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../shared/attachments";

import moment from "moment";
import AttachmentFile from "../../../../shared/components/Attachment";
import {
  DeleteOutlined,
  MenuFoldOutlined,
  PlusOutlined,
  SaveOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { useLocation, useNavigate, useParams } from "react-router-dom";

const { Content } = Layout;

const openNotification = (keyName, isValid = false) => {
  let msg = `Please save COA(VC) Form first, and then click on ${keyName}`;
  if (isValid)
    msg = `You cannot ${keyName} as it is already being attached. Kindly update it in same form.`;

  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const CoaVci = (props) => {
  const [refreshCounter, setRefreshCounter] = useState(0);
  const [state, setState] = useStateCallback({
    routeUrl: "coa-vci-list",
    frmName: "coa_vci_form",
    formData: Object.assign(
      {
        id: 0,
        page_type: "COA(VCI)",
        head_contract_type: "Purchase",
        start_date: moment(),
        end_date: moment(),
        cp_date: moment(),
        commence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
        nominationtasks: [{ due_date: moment(), reminder_offset: moment() }],
      },
      props.formData || {}
    ),
    extraFormFields: null,

    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    frmVisible: true,
    // showSideListBar: props.showSideListBar === false ? props.showSideListBar : true,
    showSideListBar: false,
    // isSetsLeftBtn: [
    //   {
    //     id: "1",
    //     key: "menu-fold",
    //     type: <MenuFoldOutlined />,
    //     withText: "List",
    //     showToolTip: true,
    //     event: "menu-fold",
    //   },
    //   {
    //     id: "2",
    //     key: "add",
    //     type: <PlusOutlined />,
    //     withText: "Add New",
    //     showToolTip: true,
    //     event: (key, data) => _onCreateFormData(),
    //   },
    //   {
    //     id: "3",
    //     key: "save",
    //     type: <SaveOutlined />,
    //     withText: "Save",
    //     showToolTip: true,
    //     event: (key, data) => saveFormData(data),
    //   },
    //   {
    //     id: "4",
    //     key: "delete",
    //     type: <DeleteOutlined />,
    //     withText: "Delete",
    //     showToolTip: true,
    //     event: (key, data) => _onDeleteFormData(data),
    //   },
    //   {
    //     id: "20",
    //     key: "refresh",
    //     type: <SyncOutlined />,
    //     withText: "Refresh",
    //     showToolTip: true,
    //     event: () => {
    //       reFreshForm();
    //     },
    //   },
    // ],
    isVisible: false,
    postFormData: [],
    isShowCoaVcReports: false,
    visibleContactAttachment: false,
    coaVciId: props.formData,
    selectedID: null,
  });

  const formRef = useRef();
  const navigate = useNavigate();
  const location = useLocation();
  const params = useParams();

  useEffect(() => {
    const _isSetsLeftBtn = [
      {
        id: "1",
        key: "menu-fold",
        type: <MenuFoldOutlined />,
        withText: "List",
        showToolTip: true,
        event: "menu-fold",
      },
      {
        id: "2",
        key: "add",
        type: <PlusOutlined />,
        withText: "Add New",
        showToolTip: true,
        event: (key, data) => _onCreateFormData(),
      },

      {
        id: "3",
        key: "save",
        type: <SaveOutlined />,
        withText: "Save",
        showToolTip: true,
        event: (key, data) => saveFormData(data),
      },

      ...(state?.formData?.id !== 0 && state?.formData?.id !== undefined
        ? [
          {
            id: "4",
            key: "delete",
            type: <DeleteOutlined />,
            withText: "Delete",
            showToolTip: true,
            event: (key, data) => _onDeleteFormData(data),
          },
        ]
        : []),
      {
        id: "20",
        key: "refresh",
        type: <SyncOutlined />,
        withText: "Refresh",
        showToolTip: true,
        event: () => {
          reFreshForm();
        },
      },
    ];

    setState((prevState) => ({
      ...prevState,
      isSetsLeftBtn: _isSetsLeftBtn,
      // isSetsRightBtn: _isSetsRightBtn,
    }));
  }, [state?.formData]);

  useEffect(() => {
    const { showLeftBtn, formData, showQuickEstimate, showFixMenu } = state;
    let vessel = undefined,
      cp_echo = undefined,
      tci_fiel = undefined,
      diffDays = undefined,
      vessel_id = undefined,
      _showQuickEstimate = undefined,
      _showFixMenu = undefined;
    let response = null,
      respDataC = {},
      respData = {},
      fixData = null,
      fieldOptions = undefined,
      _showLeftBtn = Object.assign([], showLeftBtn);

    if (params.id) {
      _onLeftSideListClick(params.id);
    } else {
      _onCreateFormData();
    }

    // if (coaVciId || (match?.params?.id && match.path == '/edit-voyage-estimate/:id')) {
    //   let qParams = { ae: (coaVciId || match.params.id) };
    //   let qParamString = objectToQueryStringFunc(qParams);
    // };

    // if (JSON.stringify(respData) === JSON.stringify({})) {
    //   respData = Object.assign({}, formData);
    // }

    // setState({
    //   ...state, showItemList: (respData['id'] ? false : true),
    //   'formData': respData, 'fixData': fixData, 'fieldOptions': fieldOptions, coaVciId: (coaVciId || (match && match.params && match.params.id ? match.params.id : undefined))
    // }, () =>
    //   setState({ ...state, refreshForm: false })
    // );

    formRef.current = Object.assign({}, state.formData);
  }, [window.location.hash]);

  const _onCreateFormData = () => {
    navigate(`/add-coa-vci`);

    setState(
      (prev) => ({ ...prev, frmVisible: false }),
      () => {
        setState((prev) => ({
          ...prev,
          formData: {
            id: 0,
            page_type: "COA(VCI)",
            head_contract_type: "Purchase",
            start_date: moment(),
            end_date: moment(),
            cp_date: moment(),
            commence_date: moment(),
            billingandbankingdetails: {
              due_date: moment(),
            },
            nominationtasks: [
              { due_date: moment(), reminder_offset: moment() },
            ],
          },
          showSideListBar: false,
          frmVisible: true,
          selectedID: null,
          coaVciId:null,
        }));
      }
    );
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Cargo Id is empty. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/coavci/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {

        openNotificationWithIcon("success", response.message);
        let setData = Object.assign({
          id: 0,
          page_type: "COA(VCI)",
          head_contract_type: "Purchase",
        });

        setState(

          (prev) => ({ ...prev, formData: setData, frmVisible: false }),
          () => {

            setState({ ...state, frmVisible: true, showSideListBar: true });

          }

        );
        navigate(`/add-coa-vci`);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };




  // const _onDelete = (postData) => {
  //   const _url = `${URL_WITH_VERSION}/coavci/delete`;

  //   apiDeleteCall(_url, { id: postData.id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon("success", response.message);

  //       // Assuming _onLeftSideListClick is a function that needs to be called
  //       if (postData.row && postData.row.contract_id) {
  //         _onLeftSideListClick(postData.row.contract_id);
  //       }

  //       const newFormData = {
  //         id: 0,
  //         page_type: "COA(VCI)",
  //         head_contract_type: "Purchase",
  //       };

  //       setState((prev) => ({
  //         ...prev,
  //         formData: newFormData,
  //         frmVisible: false,
  //         showSideListBar: true,
  //       }));
  //     } else {
  //       openNotificationWithIcon("error", response.message || "Error deleting data");
  //     }
  //   });
  // };


  const onCloseDrawer = () =>
    setState((prev) => ({
      ...prev,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    }));

  // console.log(state);

  const onClickRightMenu = async (key, options) => {
    onCloseDrawer();
    let loadComponent = undefined,
      _state = {};
    switch (key) {
      case "pl-summary":
        loadComponent = <PlSummary />;
        break;
      case "summary":
        loadComponent = <EstimateSummary />;
        break;
      case "properties":
        loadComponent = <Properties />;
        break;
      case "attachment":
        const { coaVciId } = state;

        if (coaVciId) {
          const attachments = await getAttachments(coaVciId, "EST");
          const callback = (fileArr) =>
            uploadAttachment(fileArr, coaVciId, "EST", "coa-vci");
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) =>
                deleteAttachment(file.url, file.name, "EST", "coa-vci")
              }
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon("info", "Attachments are not allowed here.");
        }
        break;
      default:
        break;
    }

    setState((prev) => ({
      ...prev,
      ..._state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    }));
  };

  const reFreshForm = () => {
    setRefreshCounter((prevCounter) => prevCounter + 1);
    // setState(
    //   (prevState) => ({
    //     ...prevState,
    //     formData: { ...formRef.current },
    //     frmVisible: false,
    //   }),
    //   () => {
    //     setState(prev => ({ ...prev,frmVisible: true }));
    //   }
    // );
  };

  const saveFormData = (postData, innerCB) => {
    const { frmName } = state;
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty("id") && postData["id"] > 0) {
      _url = "update";
      _method = "put";
    }

    if (
      state.formData.vci_status == 73 &&
      (postData["vci_status"] == 112 || postData["vci_status"] == 74)
    ) {
      let msg = `You cannot change Enquiry/Confirmed after fix status.`;
      openNotificationWithIcon(
        "error",
        <div dangerouslySetInnerHTML={{ __html: msg }} />,
        5
      );
      return false;
    }
    if (postData.disablefield) {
      delete postData.disablefield;
    }

    [
      "freight_type1",
      "l_d_rates",
      "ld_ru",
      "ld_terms",
      "ld_tt",
      "pexp",
      "draft",
    ].forEach((e) => delete postData[e]);

    Object.keys(postData).forEach(
      (key) => postData[key] === null && delete postData[key]
    );
    postAPICall(
      `${URL_WITH_VERSION}/coavci/${_url}?frm=${frmName}`,
      postData,
      _method,
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          let setData = Object.assign(
            {
              id: 0,
              page_type: "COA(VCI)",
              head_contract_type: "Purchase",
            },
            props.formData || {}
          );

          setState(
            (prev) => ({
              ...prev,
              frmVisible: false,
              formData: setData,
              showSideListBar: true,
            }),
            () => {
              setState((prev) => ({ ...prev, frmVisible: true }));
            }
          );
          if (
            props.modalCloseEvent &&
            typeof props.modalCloseEvent === "function"
          ) {
            props.modalCloseEvent();
          } else if (innerCB && typeof innerCB === "function") {
            innerCB();
          }
          // const { match, history } = props;
          if (data.row.contract_id) {
            _onLeftSideListClick(data.row);
          }
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
              (msg +=
                "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />
          );
        }
      }
    );
  };

  const _onLeftSideListClick = async (evt) => {
    let coaVciId = evt.contract_id ? evt.contract_id : evt;

    navigate(`/edit-coa-vci/${coaVciId}`);
    setState((prev) => ({ ...prev, frmVisible: false, coaVciId: coaVciId }));
    // const { match, history } = props;
    // if ( ['/edit-coa-vci/:id', "/add-coa-vci"].includes(match.path)) {
    //   history.push(`/edit-coa-vci/${coaVciId}`);
    // }
    const response = await getAPICall(
      `${URL_WITH_VERSION}/coavci/edit?ae=${coaVciId}`
    );
    const data = await response["data"];

    if (data && !data.hasOwnProperty("--"))
      data["--"] = [
        { load_ports_name: "Select Port", discharge_ports_name: "Select Port" },
      ];

    // if (data.hasOwnProperty('loadoptions') && data.loadoptions.length > 0) {
    //   data.loadoptions.map(e => {
    //     e['port_area_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('dischargeoptions') && data.dischargeoptions.length > 0) {
    //   data.dischargeoptions.map(e => {
    //     e['port_area_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('itineraryoptions') && data.itineraryoptions.length > 0) {
    //   data.itineraryoptions.map(e => {
    //     e['port_id_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('rebillsettings') && data.rebillsettings.length > 0) {
    //   data.rebillsettings.map(e => {
    //     e['cv_port_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('--') && data['--'].length > 0) {
    //   // data['--'].map(e => {
    //   //   e['load_ports_name'] = e.port_name
    //   //   e['discharge_ports_name'] = e.dis_port_name
    //   // })
    // }
    let setData = Object.assign(
      {
        id: 0,
        page_type: "COA(VCI)",
        head_contract_type: "Purchase",
        start_date: moment(),
        end_date: moment(),
        cp_date: moment(),
        commence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
        nominationtasks: [{ due_date: moment(), reminder_offset: moment() }],
      },
      data || {}
    );

    if (data.hasOwnProperty("is_fixed") && data["is_fixed"] === 1) {
      setData["disablefield"] = [
        "owner_id",
        "coa_qty",
        "coa_unit",
        "freight_rate",
        "freight_type",
      ];
    }

    setState(
      (prev) => ({
        ...prev,
        frmVisible: false,
        formData: setData,
        showSideListBar: false,
        selectedID: evt.id,
      }),
      () => {
        setState((prev) => ({ ...prev, frmVisible: true }));
      }
    );
  };

  const coaVcReports = async (showCoaVcReports, coaVciId) => {
    // console.log(state.coa_vc_purchase_id)

    let qParamString = objectToQueryStringFunc({
      ae: state.formData.contract_id,
    });
    // console.log(qParamString);
    // for report Api

    const responseReport = await getAPICall(
      `${URL_WITH_VERSION}/coavci/report?${qParamString}`
    );
    const respDataReport = await responseReport["data"];

    setState(
      (prev) => ({ ...prev, reportFormData: respDataReport }),
      () =>
        setState((prev) => ({ ...prev, isShowCoaVcReports: showCoaVcReports }))
    );
  };

  const toggleCargoRightMenu = async (val, type = "save", data = null) => {
    const { formData } = state;
    const {
      id,
      contract_id,
      company_id,
      vci_lob,
      vci_status,
      c_name,
      invoice_by,
      trade_area,
      cargo_name,
      min_inv_qty,
      owner_id,
      cp_qty_per_lift,
      currency,
      min_qty,
      max_qty,
      freight_bill_via,
      cp_date,
      cp_place,
      freight_type,
      freight_rate,
      fixed_by_user,
    } = formData;

    let postFormData = Object.assign({
      id: 0,
      my_company: company_id,
      lob: vci_lob,
      c_status: "",
      cargo_name: c_name,
      invoice_by,
      trade_area,
      cargo_group: cargo_name,
      min_inv_qty,
      charterer: owner_id,
      cp_qty: cp_qty_per_lift,
      currency,
      min_qty,
      max_qty,
      freight_bill_via,
      cp_date,
      cp_place,
      freight_type,
      freight_rate,
      // user_fixed_by: fixed_by_user,
      coa_vc_purchase_id: contract_id,
      billingandbankingdetails: formData.billingandbankingdetails,
      broker: formData.broker,
      extrafreightterm: formData.extrafreightterm,
      itineraryoptions: formData.itineraryoptions,
      loadoptions: formData.loadoptions,
      //nominationtasks:formData.nominationtasks,
      rebillsettings: formData.rebillsettings,
      revexpinfo: formData.revexp,
      dischargeoptions: formData.dischargeoptions,
      //cargo:formData.cargo,
      //vesseltype:formData.vesseltype
    });

    if (
      postFormData.billingandbankingdetails &&
      postFormData.billingandbankingdetails.id
    ) {
      delete postFormData.billingandbankingdetails.id;
    }
    if (postFormData.broker && postFormData.broker.length > 0) {
      postFormData.broker.map((e) => {
        delete e.id;
      });
    }
    if (
      postFormData.extrafreightterm &&
      postFormData.extrafreightterm.length > 0
    ) {
      postFormData.extrafreightterm.map((e) => {
        delete e.id;
      });
    }
    if (
      postFormData.itineraryoptions &&
      postFormData.itineraryoptions.length > 0
    ) {
      postFormData.itineraryoptions.map((e) => {
        delete e.id;
      });
    }
    if (postFormData.loadoptions && postFormData.loadoptions.length > 0) {
      postFormData.loadoptions.map((e) => {
        delete e.id;
      });
    }
    if (postFormData.rebillsettings && postFormData.rebillsettings.length > 0) {
      postFormData.rebillsettings.map((e) => {
        delete e.id;
      });
    }
    if (postFormData.revexpinfo && postFormData.revexpinfo.length > 0) {
      postFormData.revexpinfo.map((e) => {
        delete e.id;
      });
    }
    if (
      postFormData.dischargeoptions &&
      postFormData.dischargeoptions.length > 0
    ) {
      postFormData.dischargeoptions.map((e) => {
        delete e.id;
      });
    }

    if (postFormData.id == -1) {
      delete postFormData.id;
    }

    if (val) {
      if (type == "edit") {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/voyagecargo/list`
        );

        const respData = await response["data"];

        let filterData = respData.filter((e) => e.id === data.ID);

        if (filterData && filterData.length > 0) {
          let id = filterData[0]["vc_purchase_id"];
          postFormData = { params: { id } };
          props.history.push(`/edit-voyage-cargo/${id}`);
          //navigate(`/edit-voyage-cargo/${id}`);

          setState({ ...state, isVisible: val, postFormData });
        }
        // if (data && data.hasOwnProperty("index") && data["index"] > -1) {
        //   let id = formData["-"][data["index"]]["id"];
        //   postFormData = { "params": { id } }
        //   setState({ ...state, isVisible: val, postFormData });
        // }
      } else {
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          setState((prev) => ({ ...prev, isVisible: val, postFormData }));
        else openNotification("Create VC (purchase)");
      }
    } else {
      setState((prev) => ({ ...prev, isVisible: val, postFormData: [] }));
    }
  };

  const callbackReturn = (evt) => {
    const { history } = props;
    const { formData } = state;

    switch (evt) {
      case "refresh":
        history.push(`/edit-coa-vci/${formData.id}`);
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, isVisible: false }));
  };

  const isContactAttachmentOk = () => {
    setTimeout(() => {
      setState((prev) => ({ ...prev, visibleContactAttachment: false }));
    }, 3000);
  };

  const isContactAttachmentCancel = () =>
    setState((prev) => ({ ...prev, visibleContactAttachment: false }));

  const fixedCOAVCContract = (data) => {
    let cargo_contract_data = {
      contract_id: data["contract_id"],
      id: data["id"],
    };
    postAPICall(
      `${URL_WITH_VERSION}/coavci/fix`,
      { cargo_contract_id: data["contract_id"] },
      "POST",
      (respData) => {
        if (respData.data) {
          openNotificationWithIcon("success", respData.message);
          _onLeftSideListClick(cargo_contract_data);
        } else {
          let dataMessage = respData.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
              (msg +=
                "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />,
            5
          );
        }
      }
    );
  };
  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Rev/Exp") {
      groupKey = "revexp";
      frm_code = "coa_vci_rev_exp";
    }
    if (groupKey == "Rebill Settings") {
      groupKey = "rebillsettings";
      frm_code = "coa_vci_rebil_settings";
    }
    if (groupKey == "Nomination Tasks") {
      groupKey = "nominationtasks";
      frm_code = "coa_vci_nomination_task";
    }
    if (groupKey == "Cargo") {
      groupKey = "cargo";
      frm_code = "coa_vci_nomination_task";
    }
    if (groupKey == "Vessel Type") {
      groupKey = "vesseltype";
      frm_code = "coa_vci_nomination_task";
    }
    if (groupKey == "ItineraryOptions") {
      groupKey = "itineraryoptions";
      frm_code = "coa_vci_itinery_options";
    }
    if (groupKey == "Load Options") {
      groupKey = "loadoptions";
      frm_code = "coa_vci_form";
    }
    if (groupKey == "Discharge Options") {
      groupKey = "dischargeoptions";
      frm_code = "coa_vci_form";
    }
    if (groupKey == "--") {
      frm_code = "coa_vci_pricing";
    }
    if (groupKey == "Extra Freight Term") {
      frm_code = "coa_vci_pricing";
    }
    if (groupKey == "Broker") {
      frm_code = "coa_vci_pricing";
    }
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const {
    loadComponent,
    isShowCoaVcReports,
    title,
    visibleDrawer,
    frmName,
    formData,
    extraFormFields,
    frmVisible,
    showSideListBar,
    isSetsLeftBtn,
    isVisible,
    reportFormData,
    visibleContactAttachment,
    postFormData,
    selectedID,
  } = state;
  // const isSetsLeftBtnArr = isSetsLeftBtn.filter(
  //   (item) =>
  //     !(
  //       formData &&
  //       formData.hasOwnProperty("id") &&
  //       formData.id <= 0 &&
  //       item.key == "delete"
  //     )
  // );

  const isSetsLeftBtnArr = isSetsLeftBtn ? isSetsLeftBtn.filter(
    (item) =>
      !(
        formData &&
        formData.hasOwnProperty("id") &&
        formData.id <= 0 &&
        item.key === "delete"
      )
  ) : [];

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo diso">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body" key={refreshCounter}>
                        {formData &&
                          frmVisible &&
                          formData.hasOwnProperty("page_type") &&
                          formData.hasOwnProperty("head_contract_type") ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            showButtons={
                              [
                                // { "id": "cancel", "title": "Reset", "type": "danger" },
                                // { "id": "save", "title": "Save", "type": "primary", "event": (data, innerCB) => { saveFormData(data, innerCB) } }
                              ]
                            }
                            showToolbar={[
                              {
                                leftWidth: 8,
                                rightWidth: 16,
                                isLeftBtn: [{ isSets: isSetsLeftBtnArr }],
                                isRightBtn: [
                                  {
                                    isSets: [
                                      {
                                        key: "cvoyagec_in",
                                        isDropdown: 0,
                                        withText: "Menu",
                                        type: "",
                                        menus: null,
                                      },
                                      {
                                        key: "VC-Purchase",
                                        isDropdown: 0,
                                        withText: "Create VC (purchase)",
                                        type: "",
                                        menus: null,
                                        event: (key, data) =>
                                          data["is_fixed"] == 1
                                            ? toggleCargoRightMenu(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              `Please fix first and then click on ${key}!`
                                            ),
                                      },

                                      formData &&
                                        formData.hasOwnProperty("is_fixed") &&
                                        formData["is_fixed"] === 0
                                        ? {
                                          key: "fix_coa",
                                          isDropdown: 0,
                                          withText: "Fix",
                                          type: "",
                                          menus: null,
                                          event: (key, data) =>
                                            data &&
                                              data.hasOwnProperty("id") &&
                                              data["id"] > 0
                                              ? Modal.confirm({
                                                title: "Confirm",
                                                content:
                                                  "Are you sure, you want to Fix it?",
                                                onOk: () =>
                                                  fixedCOAVCContract(data),
                                              })
                                              : openNotification("Fix"),
                                        }
                                        : undefined,
                                      {
                                        key: "master_contract",
                                        isDropdown: 0,
                                        withText: "Master Contract",
                                        type: "",
                                        menus: null,
                                      },
                                      {
                                        key: "report",
                                        isDropdown: 0,
                                        withText: "Report",
                                        type: "",
                                        menus: null,
                                        event: (key) =>
                                          formData && formData["id"] > 0
                                            ? coaVcReports(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Please select any item in the list!"
                                            ),
                                      },
                                    ],
                                  },
                                ],
                                isResetOption: false,
                              },
                            ]}
                            inlineLayout={true}
                            sideList={{
                              selectedID: selectedID,
                              showList: true,
                              title: "COA(VC) - List",
                              uri: "/coavci/list?l=0",
                              icon: true,
                              columns: [
                                "contract_id",
                                "owner_name",
                                "vci_status_name",
                              ],
                              rowClickEvent: (evt) => _onLeftSideListClick(evt),
                            }}
                            showSideListBar={showSideListBar}
                            isShowFixedColumn={[
                              "Broker",
                              "Load Options",
                              "Discharge Options",
                              "Rev/Exp Info",
                              "ItineraryOptions",
                              "-",
                            ]}
                            extraTableButton={{
                              "-":
                                formData &&
                                  formData.hasOwnProperty("-") &&
                                  formData["-"].length > 0
                                  ? [
                                    {
                                      icon: "edit",
                                      onClickAction: (action, data) =>
                                        toggleCargoRightMenu(
                                          true,
                                          "edit",
                                          action
                                        ),
                                    },
                                  ]
                                  : [],
                            }}
                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                          />
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
        <RightBarUI
          pageTitle="add-contractform-righttoolbar"
          callback={(data, options) => onClickRightMenu(data, options)}
        />
        {isVisible === true ? (
          <Modal
            title="Create VC (purchase)"
            open={isVisible}
            width="95%"
            onCancel={() => toggleCargoRightMenu(false)}
            style={{ top: "10px" }}
            bodyStyle={{ height: 790, overflowY: "auto" }}
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  {postFormData && postFormData.hasOwnProperty("params") ? (
                    <CargoDetails
                      history={props.history}
                      match={postFormData}
                      isDisabled={true}
                      modalCloseEvent={(evt) => callbackReturn(evt)}
                    />
                  ) : (
                    <CargoDetails
                      history={props.history}
                      showSideListBar={false}
                      modalCloseEvent={(evt) => callbackReturn(evt)}
                      formData={postFormData}
                      isDisabled={true}
                    />
                  )}
                </div>
              </article>
            </div>
          </Modal>
        ) : undefined}
        {loadComponent !== undefined &&
          title !== undefined &&
          visibleDrawer === true ? (
          <Drawer
            title={state.title}
            placement="right"
            closable={true}
            onClose={onCloseDrawer}
            open={state.visibleDrawer}
            getContainer={false}
            style={{ position: "absolute" }}
            width={state.width}
            maskClosable={false}
            className="drawer-wrapper-container"
          >
            <div className="tcov-wrapper">
              <div className="layout-wrapper scrollHeight">
                <div className="content-wrapper noHeight">
                  {state.loadComponent}
                </div>
              </div>
            </div>
          </Drawer>
        ) : undefined}
      </Layout>

      {visibleContactAttachment ? (
        <Modal
          open={visibleContactAttachment}
          title="Upload Attachment ( Upload COA(VC) Details )"
          onOk={isContactAttachmentOk}
          onCancel={isContactAttachmentCancel}
          footer={null}
          width={1000}
          maskClosable={false}
        >
          <Attachment
            uploadType="Address Book"
            directory={
              formData.hasOwnProperty("estimate_id")
                ? formData["estimate_id"]
                : null
            }
          // onCloseUploadFileArray={fileArr => uploadedFiles(fileArr)}
          />
        </Modal>
      ) : undefined}

      {isShowCoaVcReports ? (
        <Modal
          style={{ top: "2%" }}
          title="Reports"
          open={isShowCoaVcReports}
          // onOk={handleOk}
          onCancel={() =>
            setState((prev) => ({ ...prev, isShowCoaVcReports: false }))
          }
          width="95%"
          footer={null}
        >
          <CoaVcReports data={reportFormData} />
        </Modal>
      ) : undefined}
    </div>
  );
};

export default CoaVci;
