import React, { Component } from "react";
import { Table, Row, Col, Spin, Alert, Modal } from "antd";
import { ES_TABLE_DATA } from "./ESTableInfo";
import URL_WITH_VERSION, { getAPICall } from "../../../../../shared";
import MakePayment from "../../../../../components/MakePayment";
import TcoMakePayment from "../../../../../components/MakePayment/TcoMakepayment";

class EstimateSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tciID: this.props.tciID || 0,
      tableColumns: [
        {
          title: "Description",
          dataIndex: "description",
          key: "description",
        },
      ],
      tableData: ES_TABLE_DATA,
      hirePayment: [],
      showSummary: false,
      showMakePayment: false,
      makePayment: {},
      tciformData: this.props.datatci || {},
      type: this.props.type || {},
      isUpdatePayment:true
    };
  }

  async componentDidMount() {
    this.getTableData()
  }

  getTableData = async ()=> {
    const { tciID, tableData,type } = this.state;
    let request=null;
   if(type=='TCI'){
       request  = await getAPICall(`${URL_WITH_VERSION}/make_payment/list?l=0`,
    {"id":{"where":"`tci_id`='"+ tciID + "'","order_by": "id DESC"}}
      )
    }

    if (type == "TCO") {
      request = await getAPICall(
        `${URL_WITH_VERSION}/make_payment/tco-list?l=0`,
        { id: { where: "`tco_id`='" + tciID + "'", order_by: "id DESC" } }
      );
    }
    const response = await request?.["data"];
    if (response && typeof response === "object") {
      if (response.length > 0) {
        let repID = [];
        response.map((e, i) => {
          repID.push({
            key: "hp_" + i,
            description: (
              <p
                className="indent-label"
                onClick={() => this.onInvoiceClick(e["id"], type)}
              >
                <span>
                  {e["invoice_no"]},{" "}
                  <span className="amount">$ {e["amount_usd"]}</span>
                </span>
                <span className="dt">{e["created_on"]}</span>
              </p>
            ),
          });
        });
        tableData[0]["children"] = repID;
        this.setState({ ...this.state, tableData: tableData }, () =>
          this.setState({ ...this.state, showSummary: true })
        );
      } else {
        tableData[0]['children'] = [];
        this.setState({ ...this.state, "tableData": tableData }, () => this.setState({ ...this.state, showSummary: true }));
      }
    } else {
      this.setState({ ...this.state, showSummary: true });
    }
  }




  onInvoiceClick = async (eID, type) => {
    const { tciformData } = this.state;
    let bunker = [];
    let request = null;

    if (
      tciformData &&
      tciformData.hasOwnProperty("bunkers") &&
      tciformData["bunkers"].length > 0
    ) {
      bunker = [
        { dr_name: "Delivery Cost" },
        { dr_name: "Adj on delivery" },
        { dr_name: "Redelivery Cost" },
        { dr_name: "Adj on redelivery" },
      ];

      tciformData["bunkers"].map((e) => {
        if (e.description === 3 || e.description === 6) {
          let index = e.description === 3 ? 0 : 2;
          let cDesc = JSON.parse(e.c_fields_values);
          let item = { ifo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0, mgo: 0 };
          let iv = {
            IFO: "ifo",
            VLSFO: "vlsfo",
            ULSFO: "ulsfo",
            MGO: "mgo",
            LSMGO: "lsmgo",
          };
          cDesc.map(
            (_e) => (item[iv[_e.name]] = _e.consumption + " X " + _e.price)
          );
          bunker[index] = Object.assign(item, bunker[index]);
        }
      });
    }
    if (type == "TCI") {
      request = await getAPICall(
        `${URL_WITH_VERSION}/make_payment/edit?e=${eID}&frm=time_charter_in_form`
      );
    }
    if (type == "TCO") {
      request = await getAPICall(
        `${URL_WITH_VERSION}/make_payment/edit?e=${eID}&frm=tco_make_payment`
      );
    }

    const response = await request["data"];
    if (response) {
      response["bunkerdeliveryredeliveryterm"] = bunker;
      response["vessel_code"] = tciformData["vessel_code"];
      response["company_lob"] = tciformData["company_lob"];
      response["vessel_id"] = tciformData["vessel_id"];
    }
    this.setState({ ...this.state, "makePayment": response,'makepaymentid':response.id}, () => this.setState({ ...this.state, showMakePayment: true, isUpdatePayment:true }));
  }



  onUpdateMakePayment = (eID, type) => {
    this.setState({ ...this.state, "makePayment": {}, isUpdatePayment: false }, ()=>this.onInvoiceClick(eID, type))
  }

  onCancelMakePayment = () => {
    this.setState({ ...this.state, "makePayment": {},showMakePayment: false, isUpdatePayment: false }, ()=>this.getTableData())
  };

  render() {
    const {
      tableColumns,
      tableData,
      showSummary,
      showMakePayment,
      makePayment,
      type,
      isUpdatePayment
    } = this.state;

    return (
      <>
        <Row gutter={16}>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            {showSummary ? (
              <Table
                columns={tableColumns}
                dataSource={tableData}
                pagination={false}
                bordered
              />
            ) : (
              <div className="col col-lg-12">
                <Spin tip="Loading...">
                  <Alert message=" " description="Please wait..." type="info" />
                </Spin>
              </div>
            )}
          </Col>
        </Row>
        
        { showMakePayment && type=='TCI' ? 
            <Modal title="Make Payment"
             open={this.state.showMakePayment}
              width="80%"
              onCancel={this.onCancelMakePayment}
              style={{ top: '10px' }}
              bodyStyle={{ maxHeight: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
            > {isUpdatePayment&&<MakePayment formData={makePayment} modalCloseEvent={() => this.onCancelMakePayment()} disableEdit={true} onUpdateMakePayment={this.onUpdateMakePayment}/>} </Modal> 
          : showMakePayment && type=='TCO' ? <Modal title="Issue Bill"
         open={this.state.showMakePayment}
          width="80%"
          onCancel={this.onCancelMakePayment}
          style={{ top: '10px' }}
          bodyStyle={{ maxHeight: 790, overflowY: 'auto', padding: '0.5rem' }}
          footer={null}
        > {isUpdatePayment&&<TcoMakePayment formData={makePayment} modalCloseEvent={() => this.onCancelMakePayment()} disableEdit={true} onUpdateMakePayment={this.onUpdateMakePayment}/>} </Modal> :undefined }
      </>
    );
  }
}

export default EstimateSummary;
