import React, { Component } from "react";
import FullEstimate from "./FullEstimate";
//import { withRouter } from "react-router-dom";
import {
  Form,
  Input,
  Button,
  Select,
  //DatePicker,
  //Tag,
  Layout,
  //Menu,
  Icon,
  Drawer,
  Row,
  Col,
  Tooltip,
  Collapse,
  Modal,
} from "antd";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  apiDeleteCall,
} from "../../../../shared";
import RightBarUI from "../../../../components/RightBarUI";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../shared/attachments";
import RightSearch from "./right-panel/RightSearch";
import { CloseOutlined, DeleteOutlined, SaveOutlined } from "@ant-design/icons";
const { Content } = Layout;

class TCTO  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      showArray: [],
      formDataArrays: [],
      showCollapse: [true],
      frmName: "tcto_form",
      isEdit: false,
      formDataValues: this.props.formData || {},
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      fullToQuick: false,
      refreshForm: false,
      showAddButton: true,
      estimateID: null,
    };

    this.formDataref = React.createRef();
    this.formDataref.current = this.props.formData || {};
  }

  componentDidMount = async () => {
    const { match, estimateID } = this.props;
    let respData = undefined,
      response = undefined;
    if (
      (match?.params.id && match?.path == "/edit-tc-estimate/:id") ||
      estimateID
    ) {
      let qParams = { ae: match.params.id || estimateID };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(
        `${URL_WITH_VERSION}/tcto/quick-estimate/edit?${qParamString}`
      );
      respData = await response["data"];
      this.formDataref.current = respData;
      this.setState(
        {
          ...this.state,
          formDataValues: respData,
          fullToQuick: false,
          showAddButton: false,
          formDataArrays: [...this.state.formDataArrays, ...[respData]],
          estimateID: match.params.id,
        },
        () => this.onAddMore("0", false, true)
      );
    } else {
      this.onAddMore();
    }
  };

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  onClickRightMenu = async (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "search":
        loadComponent = <RightSearch history={this.props.history} />;
        break;
      // case "properties":
      //   loadComponent = <Properties />;
      //   break;
      // case "pl":
      //   loadComponent = <PL />;
      //   break;
      // case "summary":
      //   loadComponent = <EstimateSummary />;
      //   break;
      // case "portinformation":
      //   loadComponent = <TcovPortInformation />;
      //   break;
      // case "attachment":
      //   const { estimateID } = this.state;
      //   if (estimateID) {
      //     const attachments = await getAttachments(estimateID, "EST");
      //     const callback = (fileArr) =>
      //       uploadAttachment(fileArr, estimateID, "EST", "tcov");
      //     loadComponent = (
      //       <AttachmentFile
      //         uploadType="Estimates"
      //         attachments={attachments}
      //         onCloseUploadFileArray={callback}
      //         deleteAttachment={(file) =>
      //           deleteAttachment(file.url, file.name, "EST", "tcov")
      //         }
      //         tableId={0}
      //       />
      //     );
      //   } else {
      //     openNotificationWithIcon("info", "Attachments are not allowed here.");
      //   }
       // break;
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };

  viewFullEstimate = (index = "") => {
    const { formDataValues, formDataArrays } = this.state;
    if (
      formDataArrays &&
      formDataArrays[index] &&
      formDataArrays[index].hasOwnProperty("tcto_id")
    ) {
      this.props.history.push(
        `/edit-tc-est-fullestimate/${formDataValues["tcto_id"]}`
      );
    } else if (
      formDataArrays[index] ||
      (formDataValues &&
        formDataValues.hasOwnProperty("id") &&
        formDataValues["id"] > 0 &&
        !formDataValues.hasOwnProperty("tcto_id"))
    ) {
      this.props.history.push({
        pathname: `/tc-est-fullestimate/${
          formDataArrays[index]["estimate_id"]
        }`,
        state: formDataArrays[index],
        // state:formDataValues,
      });
    } else {
      openNotificationWithIcon("error", "Quick estimate to be generated first");
    }
  };

  

  onAddMore = async (index = "", editValue = false, editing = false) => {
    const {
      showArray,
      showCollapse,
      formDataValues,
      fullToQuick,
      rightBtnPanel,
      formDataArrays
    } = this.state;
 
    let _formData = Object.assign({}, {});
    if (editValue || editing == true) {
      _formData = Object.assign({}, formDataValues);
    }

   

    if (editValue === true && editing === false) {
      showArray[index] = [];
    }
    // _formData = this._loadVoyageResultDetails(_formData, formDataValues);
    this.setState(
      {
        ...this.state,
        formDataValues: _formData,
        isEdit:
          formDataValues &&
          formDataValues.hasOwnProperty("id") &&
          formDataValues["id"] > 0,
        rightBtnPanel:
          _formData && _formData.hasOwnProperty("id") && _formData["id"] > 0
            ? rightBtnPanel
            : [
                {
                  key: "s2",
                  isSets: [
                    {
                      id: "2",
                      key: "menu",
                      isDropdown: 1,
                      type: "small-dash",
                      withText: "Menu",
                      showButton: true,
                      showToolTip: true,
                      // menus: null,
                      menus: [
                        {
                          id: "21",
                          key: "menu-1",
                          href: null,
                          icon: null,
                          label: "Convert to Full Estimate",
                          modalKey: null,
                        },

                        // {
                        //   id: "22",
                        //   key: "menu-2",
                        //   href: null,
                        //   icon: null,
                        //   label: "Go to Quick TCTO",
                        //   modalKey: null,
                        // },

                        // {
                        //   id: "23",
                        //   key: "menu-3",
                        //   href: null,
                        //   icon: null,
                        //   label: "Go Quick Voy relet",
                        //   modalKey: null,
                        //   event: () => this.viewFullEstimate(index), //this.props.history.push('/voy-relet-full-estimate')
                        // },

                        {
                          id: "24",
                          key: "menu-4",
                          href: null,
                          icon: null,
                          label: "Bunker-Live",
                          modalKey: null,
                        },

                        {
                          id: "25",
                          key: "menu-5",
                          href: null,
                          icon: null,
                          label: "Route to Map",
                          modalKey: null,
                        },

                        {
                          id: "26",
                          key: "menu-6",
                          href: null,
                          icon: null,
                          label: "Voyage optimize",
                          modalKey: null,
                        },
                      ],
                    },
                  ],
                },
              ],
      },
      () => {
        if (editValue === true) {
          showArray[index] = this.getContent(index, true);
        } else {
          showArray.push(this.getContent(showArray.length, false));
        }

        showCollapse[showArray.length] = true;
        this.setState({
          ...this.state,
          showArray: showArray,
          showCollapse,
        });
      }
    );
  };


  onLessMore = (index) => {
    const { showArray } = this.state;
  
    if (index > 0 && index < showArray.length) {
      showArray[index] = undefined;

      this.setState({ showArray: [...showArray] });
    }
  };

  onToggle = (i) => {
    this.state.showCollapse[i] = !this.state.showCollapse[i];
    this.setState(
      { ...this.state, showCollapse: this.state.showCollapse },
      () => {
        this.state.showArray[i] = this.getContent(i);
        this.setState({ ...this.state, showArray: this.state.showArray });
      }
    );
  };



  onSaveFormData = async (data, index) => {
     let _url = `${URL_WITH_VERSION}/tcto/save?frm=tcto_form`;
      let _method = "POST";
      if (data["id"] > 0) {
        _url = `${URL_WITH_VERSION}/tcto/update?frm=tcto_form`;
        _method = "PUT";
      }
    if (data) {
      this.setState({
        ...this.state,
        refreshForm: true,
      });
      this.formDataref.current = { ...data };

      await postAPICall(`${_url}`, data, _method, async (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
        } else {
          openNotificationWithIcon("error", response.message);
        }

        if (data && response.data) {
          let estimateID =
            (response && response.row && response.row.estimateID) ||
            data["estimate_id"];
          let qParams = { ae: estimateID };
          let qParamString = objectToQueryStringFunc(qParams);
          let response1 = await getAPICall(
            `${URL_WITH_VERSION}/tcto/quick-estimate/edit?${qParamString}`
          );
          let respData = await response1["data"];
          this.formDataref.current = respData;
          this.setState(
            {
              ...this.state,
              isEdit: false,
              formDataValues: { ...respData },
              refreshForm: false,
              showAddButton: true,
              // showArray:[]
              formDataArrays: [...this.state.formDataArrays, ...[respData]],
            },
            () => this.onAddMore(index, true)
          );
        } else {
          this.setState({ ...this.state, isEdit: false, refreshForm: false });
        }
      });
    }
  };

  copyQuickEstimate = (data, index, editValue = false, editing = false) => {
    const {
      showArray,
      showCollapse,
      formDataValues,
      fullToQuick,
      rightBtnPanel,
    } = this.state;

    delete data["estimate_id"];
    delete data["id"];
    let _formData = Object.assign({}, data);
    if (editValue || editing == true) {
      _formData = Object.assign({}, formDataValues);
    }

    if (editValue === true && editing === false) {
      showArray[index] = [];
    }
    // _formData = this._loadVoyageResultDetails(_formData, formDataValues);
    this.formDataref.current = _formData;
    this.setState(
      {
        ...this.state,
        formDataValues: _formData,
        isEdit:
          formDataValues &&
          formDataValues.hasOwnProperty("id") &&
          formDataValues["id"] > 0,
        rightBtnPanel:
          _formData && _formData.hasOwnProperty("id") && _formData["id"] > 0
            ? rightBtnPanel
            : [
                {
                  key: "s2",
                  isSets: [
                    {
                      id: "2",
                      key: "menu",
                      isDropdown: 1,
                      type: "small-dash",
                      withText: "Menu",
                      showButton: true,
                      showToolTip: true,
                      // menus: null,
                      menus: [
                        {
                          id: "21",
                          key: "menu-1",
                          href: null,
                          icon: null,
                          label: "Convert to Full Estimate",
                          modalKey: null,
                        },
                        {
                          id: "24",
                          key: "menu-4",
                          href: null,
                          icon: null,
                          label: "Bunker-Live",
                          modalKey: null,
                        },

                        {
                          id: "25",
                          key: "menu-5",
                          href: null,
                          icon: null,
                          label: "Route to Map",
                          modalKey: null,
                        },

                        {
                          id: "26",
                          key: "menu-6",
                          href: null,
                          icon: null,
                          label: "Voyage optimized",
                          modalKey: null,
                        },
                      ],
                    },
                  ],
                },
              ],
      },
      () => {
        if (editValue === true) {
          showArray[index] = this.getContent(index, true);
        } else {
          showArray.push(this.getContent(showArray.length, false));
        }

        showCollapse[showArray.length] = true;
        this.setState({
          ...this.state,
          showArray: showArray,
          showCollapse,
        });
      }
    );
  };

  deleteData = (id, tcov_id, innerCB, index) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/tcov/delete`,
      { id: id, tcov_id: tcov_id },
      (resData) => {
        if (resData.data === true) {
          openNotificationWithIcon("success", resData.message);
          if (index > 0) {
            this.onLessMore(index);
          } else {
            this.props.history.push("/quick-estimate-list");
          }
        } else {
          openNotificationWithIcon("error", resData.message);
        }
      }
    );
  };

  getContent(index, edit = false) {
    const {
      showCollapse,
      frmName,
      gID,
      formDataValues,
      isEdit,
      rightBtnPanel,
      formDataArrays,
    } = this.state;
 
    return (
      <>
        {!showCollapse[index] ? (
          <div className="all-sections wrap-small-collapse">
            <Row gutter={16}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="toolbar-ui-wrapper">
                  <div className="action-btn">
                    <Tooltip title="Expand">
                      <Button
                        type="dashed"
                        shape="circle"
                        icon="right"
                        size={"default"}
                        onClick={() => this.onToggle(index)}
                      />
                    </Tooltip>
                  </div>
                </div>
              </Col>
            </Row>
            <hr />

            <div className="body-se">
              <h1 class="main-title main-text">
                {formDataValues && formDataValues.hasOwnProperty("estimate_id")
                  ? formDataValues["estimate_id"]
                  : "EST-TCOV-0000" + index}
              </h1>
            </div>
          </div>
        ) : (
          <div className="all-sections">
            {this.state.refreshForm === false ? (
              <NormalFormIndex
                key={"key_" + frmName + index}
                formClass="label-min-height"
                // formData={formDataValues}
                formData={Object.assign({}, this.formDataref.current)}
                showForm={true}
                frmCode={frmName}
                addForm={true}
                showButtons={[
                  { id: "cancel", title: "Reset", type: "danger" },
                  {
                    id: "save",
                    title: "Save",
                    type: "primary",
                    event: (data) => {
                      this.onSaveFormData(data, index);
                    },
                  },
                ]}
                formDevideInCols="1"
                inlineLayout={true}
                showToolbar={[
                  {
                    isLeftBtn: [
                      {
                        key: "s1",
                        isSets: [
                          {
                            id: "1",
                            key: "save",
                            type: <SaveOutlined />,
                            withText: "Save",
                            showButton: true,
                            showToolTip: true,
                            event: (key, data) => {
                              this.onSaveFormData(data, index);
                            },
                          },

                          {
                            id: "2",
                            key: "delete",
                            type: <DeleteOutlined />,
                            withText: "Delete",
                            showToolTip: true,
                            event: (key, saveData, innerCB) => {
                              if (saveData["id"] && saveData["id"] > 0) {
                                Modal.confirm({
                                  title: "Confirm",
                                  content:
                                    "Are you sure, you want to delete it?",
                                  onOk: () =>
                                    this.deleteData(
                                      saveData["id"],
                                      saveData["tcov_id"],
                                      innerCB,
                                      index
                                    ),
                                });
                              }
                            },
                          },

                          {
                            id: "3",
                            key: "close",
                            type:  <CloseOutlined />,
                            withText: "Close",
                            showButton: true,
                            showToolTip: true,
                            event: (key) => {
                              this.onLessMore(index);
                            },
                          },

                          // {
                          //   id: '7',
                          //   key: 'collapse',
                          //   type: 'left',
                          //   withText: 'Collapse',
                          //   showButton: true,
                          //   showToolTip: true,
                          //   event: key => {
                          //     this.onToggle(index);
                          //   },
                          // },
                        ],
                      },
                    ],
                    isRightBtn: [
                      {
                        key: "s2",
                        isSets: [
                          {
                            id: "3",
                            key: "fullest",
                            isDropdown: 0,
                            type: "small-dash",
                            withText: "Full Estimate",
                            showButton: true,
                            showToolTip: true,
                            event: () => this.viewFullEstimate(index),
                          },

                          {
                            id: "2",
                            key: "menu",
                            isDropdown: 1,
                            type: "small-dash",
                            withText: "Menu",
                            showButton: true,
                            showToolTip: true,
                            menus: [
                              {
                                id: "24",
                                key: "menu-4",
                                href: null,
                                icon: null,
                                label: "Bunker-Live",
                                modalKey: null,
                              },

                              {
                                id: "25",
                                key: "menu-5",
                                href: null,
                                icon: null,
                                label: "Route to Map",
                                modalKey: null,
                              },

                              {
                                id: "26",
                                key: "menu-6",
                                href: null,
                                icon: null,
                                label: "Voyage optimize",
                                modalKey: null,
                              },

                              {
                                id: "27",
                                key: "menu-7",
                                href: null,
                                icon: null,
                                label: "Copy Estimate",
                                modalKey: null,

                                event: (key, data) => {
                                  let data1 = Object.assign({}, data);

                                  data1 &&
                                  data1.hasOwnProperty("id") &&
                                  data1["id"] > 0
                                    ? this.copyQuickEstimate(data1, index)
                                    : openNotificationWithIcon(
                                        "error",
                                        "Please save Quick Estimate first.",
                                        5
                                      );
                                },
                              },
                            ],
                          },
                          // {
                          //   id: '1',
                          //   key: 'createtcov',
                          //   isDropdown: 0,
                          //   withText: 'Full TCOV',
                          //   type: '',
                          //   menus: null,
                          //   event: (key, data) => this.props.history.push(`/edit-voy-est/${data.estimate_id}`),
                          // },
                        ],
                      },
                    ],
                    isResetOption: false,
                  },
                ]}
              />
            ) : (
              undefined
            )}
          </div>
        )}
      </>
    );
  }
  // viewFullEstimate = (bolVal, estimateID) =>{
  //   const {formDataValues}=this.state
  //   if(formDataValues && formDataValues.hasOwnProperty('id') && formDataValues['id']>0){
  //     this.props.history.push({
  //       pathname: `/add-tcov`,
  //       state: formDataValues
  //      });
  //   }
  //   else{
  //     openNotificationWithIcon('error',"Quick estimate to be generated first");
  //   }

  // }

  render() {
    const {
      loadComponent,
      title,
      visibleDrawer,
      showArray,
      showAddButton,
    } = this.state;
    return (
      <div className="tcov-wrapper scroll-borad">
        <Layout className="layout-wrapper">
          <Layout style={{ border: "none" }}>
            <Content className="content-wrapper">
              <div className="wrap-scrumboard-design">
                {showArray.map((field) => {
                  return <> {field} </>;
                })}

                {/* {showAddButton === true ? ( */}

                {/* 
                
                commented because this feature is creatting issues.
                <div className="wrap-action-plus">
                  <div className="fieldscroll-wrap add-section-wrapper">
                    <Tooltip title="Add Estimates" onClick={this.onAddMore}>
                      <Icon type="plus" />
                    </Tooltip>
                  </div>
                </div> */}

                {/* ) : (
                  undefined
                )} */}
              </div>
            </Content>
          </Layout>
          <RightBarUI
            pageTitle="tcov-quick-righttoolbar"
            callback={(data, options) => this.onClickRightMenu(data, options)}
          />
          {loadComponent !== undefined &&
          title !== undefined &&
          visibleDrawer === true ? (
            <Drawer
              title={this.state.title}
              placement="right"
              closable={true}
              onClose={this.onCloseDrawer}
              open={this.state.visibleDrawer}
              getContainer={false}
              style={{ position: "absolute" }}
              width={this.state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  <div className="content-wrapper noHeight">
                    {this.state.loadComponent}
                  </div>
                </div>
              </div>
            </Drawer>
          ) : (
            undefined
          )}
        </Layout>
      </div>
    );
  }
}
export default TCTO;
//export default withRouter(TCTO);
