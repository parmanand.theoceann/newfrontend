import React, { Component } from 'react';
import { Table, Form, Input, Button, Tabs} from 'antd';

//const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
//const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const columns = [
  {
    title: 'TCO ID',
    dataIndex: 'tcoid',
  },

  {
    title: 'Vessel Name',
    dataIndex: 'vesselname',
  },

  {
    title: 'Voyage Status',
    dataIndex: 'voyagestatus',
  },

  {
    title: 'Estimate ID',
    dataIndex: 'estimateid',
  },

  {
    title: 'Vessel Type',
    dataIndex: 'vesseltype',
  },
];
const data = [
  {
    key: '1',
    tcoid: 'TCO ID',
    vesselname: 'Vessel Name',
    voyagestatus: 'Voyage Status',
    estimateid: 'Estimate ID',
    vesseltype: 'Vessel Type'
  },

  {
    key: '2',
    tcoid: 'TCO ID',
    vesselname: 'Vessel Name',
    voyagestatus: 'Voyage Status',
    estimateid: 'Estimate ID',
    vesseltype: 'Vessel Type'
  },

  {
    key: '3',
    tcoid: 'TCO ID',
    vesselname: 'Vessel Name',
    voyagestatus: 'Voyage Status',
    estimateid: 'Estimate ID',
    vesseltype: 'Vessel Type'
  },
  
];

class SearchTco extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-2">
                    <FormItem {...formItemLayout} label="TCO ID">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Vessel Name">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Voyage Status">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-2">
                    <FormItem {...formItemLayout} label="Estimate ID">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-2">
                    <FormItem {...formItemLayout} label="Vessel Type">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="text-right">
                      <Button className="ant-btn ant-btn-primary mt-3">Submit</Button>
                    </div>
                  </div>
                </div>
              </Form>

              <div className="col-md-12">
                <Table
                  bordered
                  columns={columns}
                  dataSource={data}
                  pagination={false}
                  footer={false}
                />
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default SearchTco;
