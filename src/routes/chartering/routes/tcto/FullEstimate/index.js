import React, { useState, useEffect, useRef } from "react";
import { Form, Button, Layout, Drawer, Modal, Spin, Alert } from "antd";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import RightBarUI from "../../../../../components/RightBarUI";
import PL from "../../../../../shared/components/PL/tcto";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import Properties from "../right-panel/Properties";
import EstimateSummary from "../right-panel/EstimateSummary";
import TctoReports from "../../../../form-reports/TctoReports";
import TctoEstimateDetail from "../../../../form-reports/TctoEstimateDetail";
import * as moment from "moment";
import PortDistance from "../../../../port-to-port/PortAnalytics";
import ShipDistance from "../../../../port-to-port/ShipDetails";
import _ from "lodash";
import VoygenBunkerPlan from "../../../../voyage-bunker-plan";
import {
  getAttachments,
  deleteAttachment,
  uploadAttachment,
} from "../../../../../shared/attachments";

import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  apiDeleteCall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../../shared";
import AttachmentFile from "../../../../../shared/components/Attachment";
import Remarks from "../../../../../shared/components/Remarks";
import {
  DeleteOutlined,
  MenuFoldOutlined,
  PlusOutlined,
  SaveOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { Suspense } from "react";
import { calculateTotalSummary, calculateTotalaverage } from "../../utils";
import RepositionAnalysis from "../modals/TCtoRepositionAnalysis";
import SpotPrice from "../../../../port-to-port/SpotPrice";

const MemoizedVoygenBunkerPlan = React.memo(VoygenBunkerPlan);

const FormItem = Form.Item;
const { Content } = Layout;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const dataSourcespd = [
  {
    key: "1",
    spdtype: "CP (KTs/cons.)",
    bkt: "12.00",
    bcons: "30.00",
    lkt: "11.00",
    lcons: "10.50",
  },

  {
    key: "2",
    spdtype: "ECO (kns//Cons.)",
    bkt: "12",
    bcons: "30",
    lkt: "11",
    lcons: "10.5",
  },

  {
    key: "3",
    spdtype: "ECO (kns//Cons.)",
    bkt: "12",
    bcons: "30",
    lkt: "11",
    lcons: "10.5",
  },
];

let fieldsVal = {
  port_id: "port_id",
  port: "port",
  funct: "funct",
  passage: "passage",
  speed: "speed",
  miles: "miles",
  arrival_date_time: "arrival_date_time",
  departure: "departure",
  vlsfo: "vlsfo",
  ulsfo: "ulsfo",
  ifo: "ifo",
  tsd: "tsd",
  mgo: "mgo",
  lsmgo: "lsmgo",
  arob_ifo: "arob_ifo",
  arob_vlsfo: "arob_vlsfo",
  arob_lsmgo: "arob_lsmgo",
  arob_mgo: "arob_mgo",
  arob_ulsfo: "arob_ulsfo",
  pc_ifo: "pc_ifo",
  pc_vlsfo: "pc_vlsfo",
  pc_lsmgo: "pc_lsmgo",
  pc_mgo: "pc_mgo",
  pc_ulsfo: "pc_ulsfo",
  r_ifo: "r_ifo",
  r_vlsfo: "r_vlsfo",
  r_lsmgo: "r_lsmgo",
  r_mgo: "r_mgo",
  r_ulsfo: "r_ulsfo",
  dr_ifo: "dr_ifo",
  dr_vlsfo: "dr_vlsfo",
  dr_lsmgo: "dr_lsmgo",
  dr_mgo: "dr_mgo",
  dr_ulsfo: "dr_ulsfo",
  s_type: "s_type",
};

let fieldsVal2 = Object.assign(fieldsVal, {
  wf_per: "wf_per",
  t_port_days: "pdays",
  currency: "currency",
  days: "days",
  eff_speed: "eff_speed",
  funct: "funct",
  gsd: "gsd",
  l_d_qty: "l_d_qty",
  l_d_rate: "l_d_rate",
  l_d_rate1: "l_d_rate1",
  l_d_term: "l_d_term",
  miles: "miles",
  p_exp: "p_exp",
  passage: "passage",
  port: "port",
  port_id: "port_id",
  s_type: "s_type",
  speed: "speed",
  tcov_id: "tcov_id",
  tsd: "tsd",
  turn_time: "turn_time",
  // wf_per: "wf_per",
  xpd: "xpd",
  xsd: "xsd",
});

const TCTOFullEstimate = (props) => {
  const [state, setState] = useStateCallback({
    isBunker: false,
    isMap: false,
    portData: null,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    disableSave: false,
    isShowSearchTci: false,
    showItemList: false,
    isShowSearchTco: false,
    isShowTctoReports: false,
    isShowEstimateReport: false,
    frmName: "tcto_full_estimate_form",
    showQuickEstimateBoolean: false,
    formData: {},
    refreshForm: true,
    reportFormData: {
      portitinerary: [
        {
          port_id: "4444",
          port: "2944",
          funct: 3,
          s_type: 1,
          wf_per: 10,
          miles: "0.00",
          speed: "0.00",
          eff_speed: "0.00",
          gsd: "0.00",
          tsd: "0.00",
          xsd: "0.00",
          l_d_qty: "0.00",
          l_d_rate: "0.00",
          l_d_rate1: "0.00",
          turn_time: "0.00",
          days: "0.00",
          xpd: "0.00",
          p_exp: "0.00",
          t_port_days: "0.00",
          l_d_term: 1,
          editable: true,
          index: 0,
          ID: -9e6,
        },
        {
          port_id: "6297",
          port: "5530",
          s_type: 1,
          wf_per: 10,
          miles: "0.00",
          speed: "0.00",
          eff_speed: "0.00",
          gsd: "0.00",
          tsd: "0.00",
          xsd: "0.00",
          l_d_qty: "0.00",
          l_d_rate: "0.00",
          l_d_rate1: "0.00",
          turn_time: "0.00",
          days: "0.00",
          xpd: "0.00",
          p_exp: "0.00",
          t_port_days: "0.00",
          l_d_term: 1,
          editable: true,
          index: 1,
          ID: -9e6 + 1,
        },
      ],
    },
    isShowPortDistance: false,
    isShowItineraryInfo: false,
    fixModal: false,
    isShowPortRoute: false,
    isShowItineraryInfo: false,
    visibleAttachment: false,
    sendBackData: { show: false },
    portItin: [],
    fileArr: [],
    estimateID: null,
    vmCheck: props.vmCheck != undefined ? props.vmCheck : false,
    isShowVoyageBunkerPlan: false,
    // showLeftBtn: [
    //   {
    //     key: "s1",
    //     isSets: [
    //       {
    //         id: "0",
    //         key: "menu-fold",
    //         type: <MenuFoldOutlined />,
    //         withText: "List",
    //         showToolTip: true,
    //         event: "menu-fold",
    //       },
    //       {
    //         id: "1",
    //         key: "plus",
    //         type: <PlusOutlined />,
    //         withText: "Add New",
    //         showToolTip: true,
    //         event: (key, saveData, innerCB) => _onCreateFormData(),
    //       },

    //       {
    //         id: "3",
    //         key: "save",
    //         type: <SaveOutlined />,
    //         withText: "Save",
    //         showToolTip: true,
    //         event: (key, saveData, innerCB) => {
    //           saveFormData(saveData, innerCB);
    //         },
    //       },
    //       {
    //         id: "4",
    //         key: "delete",
    //         type: <DeleteOutlined />,
    //         withText: "Delete",
    //         showToolTip: true,
    //         event: (key, saveData, innerCB) => {
    //           if (saveData["id"] && saveData["id"] > 0) {
    //             Modal.confirm({
    //               title: "Confirm",
    //               content: "Are you sure, you want to delete it?",
    //               onOk: () =>
    //                 deleteData(saveData["id"], saveData["tcto_id"], innerCB),
    //             });
    //           }
    //         },
    //       },
    //       {
    //         id: "20",
    //         key: "refresh",
    //         type: <SyncOutlined />,
    //         showToolTip: true,
    //         withText: "Refresh",
    //         // event: () => reFreshForm()
    //       },
    //     ],
    //   },
    // ],
    showFixMenu: {
      key: "fix_schedule_voyage",
      isDropdown: 0,
      withText: "Fix & Schedule",
      type: "",
      menus: null,
      event: (data, key) =>
        Modal.confirm({
          title: "Confirm",
          content: "Are you sure, you want to Fix/Schedule it?",
          onOk: () => onClickFixTCTO(true),
        }),
    },
  });

  const [quickdata, setQuickdata] = useState(null);
  const location = useLocation();
  const params = useParams();
  const navigate = useNavigate();
  const menuRef = useRef(null);

  menuRef.current = [
    {
      key: "s1",
      isSets: [
        {
          id: "0",
          key: "menu-fold",
          type: <MenuFoldOutlined />,
          withText: "List",
          showToolTip: true,
          event: "menu-fold",
        },
        {
          id: "1",
          key: "plus",
          type: <PlusOutlined />,
          withText: "Add New",
          showToolTip: true,
          event: (key, saveData, innerCB) => _onCreateFormData(),
        },

        {
          id: "3",
          key: "save",
          type: <SaveOutlined />,
          withText: "Save",
          showToolTip: true,
          event: (key, saveData, innerCB) => {
            saveFormData(saveData, innerCB);
          },
        },
        {
          id: "4",
          key: "delete",
          type: <DeleteOutlined />,
          withText: "Delete",
          showToolTip: true,
          event: (key, saveData, innerCB) => {
            if (saveData["id"] && saveData["id"] > 0) {
              Modal.confirm({
                title: "Confirm",
                content: "Are you sure, you want to delete it?",
                onOk: () =>
                  deleteData(saveData["id"], saveData["tcto_id"], innerCB),
              });
            }
          },
        },
        {
          id: "20",
          key: "refresh",
          type: <SyncOutlined />,
          showToolTip: true,
          withText: "Refresh",
          event: (key, data, cb) => reFreshForm(data["estimate_id"]),
        },
      ],
    },
  ];

  // useEffect(() => {
  //   const quickdata = location?.state?.data;
  //   if (quickdata) {
  //     firstLoadForm(quickdata);
  //   } else {
  //     if (location.pathname === "/tc-est-fullestimate") {
  //       //initialLoadData();
  //       editformdata();
  //     } else {
  //       editformdata();
  //     }
  //   }
  // }, []);

  useEffect(() => {
    const quickdata = location?.state?.data;
    if (quickdata) {
      firstLoadForm(quickdata);
    } else {
      //if (location.key === "default") {  
        //bellow condition is added by harshit for changing estimate id from browser address bar
        if (location.key === "default" && location.pathname==="/tc-est-fullestimate") {
        _onCreateFormData();
      } else {
        editformdata(params.id);
      }
    }
  }, [location]);

  // useEffect(() => {
  //   const showHideToolBarMenu = async () => {
  //     if (state.formData.tcto_status === 2) {
  //       const updatedState = state.showLeftBtn.map((group) => ({
  //         ...group,
  //         isSets: group.isSets.filter(
  //           (item) => !(item.key === "save" && item.id === "3")
  //         ),
  //       }));

  //       setState((prevState) => ({
  //         ...prevState,
  //         showLeftBtn: updatedState,
  //       }));
  //     } else {
  //       setState((prevState) => ({
  //         ...prevState,
  //         showLeftBtn: menuRef.current,
  //       }));
  //     }
  //   };
  //   showHideToolBarMenu();
  // }, [state.formData.tcto_status]);

  useEffect(() => {
    const _isShowLeftBtn = [
      {
        key: "s1",
        isSets: [
          {
            id: "0",
            key: "menu-fold",
            type: <MenuFoldOutlined />,
            withText: "List",
            showToolTip: true,
            event: "menu-fold",
          },
          {
            id: "1",
            key: "plus",
            type: <PlusOutlined />,
            withText: "Add New",
            showToolTip: true,
            event: (key, saveData, innerCB) => _onCreateFormData(),
          },
          ...(state?.formData?.tcto_status !== 2
            ? [
                {
                  id: "3",
                  key: "save",
                  type: <SaveOutlined />,
                  withText: "Save",
                  showToolTip: true,
                  event: (key, saveData, innerCB) => {
                    saveFormData(saveData, innerCB);
                  },
                },
              ]
            : []),
          state?.formData?.id && {
            id: "4",
            key: "delete",
            type: <DeleteOutlined />,
            withText: "Delete",
            showToolTip: true,
            event: (key, saveData, innerCB) => {
              if (saveData["id"] && saveData["id"] > 0) {
                Modal.confirm({
                  title: "Confirm",
                  content: "Are you sure, you want to delete it?",
                  onOk: () =>
                    deleteData(saveData["id"], saveData["tcto_id"], innerCB),
                });
              }
            },
          },
          {
            id: "20",
            key: "refresh",
            type: <SyncOutlined />,
            showToolTip: true,
            withText: "Refresh",
            event: () => {
              reFreshForm();
            },
          },
        ],
      },
    ];

    const _isShowRightBtn = [
      {
        isSets: [
          state.vmCheck
            ? null
            : {
                key: "reposition_analysis",
                isDropdown: 0,
                withText: "Reposition Analysis",
                type: "",
                menus: null,
                event: (key) => {
                  openRepositionAnalysis(true);
                },
              },
          {
            key: "quick_estimate",
            isDropdown: 0,
            withText: "View Quick Est.",
            type: "",
            menus: null,
            event: (key, data) => {
              // navigate(
              //   `/edit-tc-estimate/${state.estimateID}`,
              //   {
              //     state: {
              //       data: state.estimateID,
              //     },
              //   }
              // )

              // console.log('data :',data);

              // if (state?.formData?.quick_estimate_id !== undefined && state?.formData?.quick_estimate_id !== null) {
              //   navigate(`/edit-tc-estimate/${state.formData.quick_estimate_id}`, {
              //     state: {
              //       data: state.formData.quick_estimate_id,
              //     },
              //   });
              // } else {
              //   navigate('/quick-estimate', {
              //     state: {
              //       data: state.formData
              //     },
              //   });
              // }

              if (data["quick_estimate_id"] && data["quick_id"]) {
                navigate(`/edit-tc-estimate/` + data["quick_estimate_id"], {
                  state: {
                    data: data,
                    fullEstimate: true,
                  },
                });
              } else {
                navigate(`/add-TC-estimate`, {
                  state: {
                    data: data,
                    fullEstimate: true,
                  },
                });
              }
            },
          },

          {
            key: "bunker_plan",
            isDropdown: 0,
            withText: "Bunker Plan",
            type: "",
            menus: null,
            event: (data, key) => voyageBunkerPlan(true),
          },
          {
            key: "ai",
            isDropdown: 0,
            withText: "AI",
            type: "",
            menus: null,
            event: (data, key) => console.log(),
          },
          // {
          //   key: "bench_mark",
          //   isDropdown: 0,
          //   withText: "Benchmarking",
          //   type: "",
          //   menus: null,
          //   event: (data, key) => console.log(),
          // },
          {
            key: "Send_Firm_Offer",
            isDropdown: 0,
            withText: "Send Firm Offer",
            type: "",
            menus: null,
            event: (data, key) => console.log(),
          },
          // {
          //   key: "Draw_CP",
          //   isDropdown: 0,
          //   withText: "Draw CP",
          //   type: "",
          //   menus: null,
          //   event: (data, key) => console.log(),
          // },
          {
            key: "share",
            isDropdown: 0,
            withText: "Share",
            type: "",
            menus: null,
            event: (data, key) => console.log(),
          },

          ...(state?.formData?.tcto_status !== 2
            ? [
                {
                  key: state?.formData?.tcto_status === 1 ? "schedule" : "fix",
                  isDropdown: 0,
                  withText:
                    state?.formData?.tcto_status === 1 ? "Schedule" : "Fix",
                  type: "",
                  menus: null,
                  event: (data, key) => {
                    Modal.confirm({
                      title: "Confirm",
                      content: "Are you sure, you want to Fix/Schedule it?",
                      // onOk: () => onClickFixTCTO(true),

                      onOk: () => {
                        if (state?.formData?.tcto_status === 1) {
                          createVoyageManger(
                            state.formData.fixedtcto,
                            state.formData.tcto_id,
                            state.formData.vessel_code
                          );
                        } else {
                          onClickFixTCTO(true);
                        }
                      },
                    });
                  },
                },
              ]
            : []),

          {
            key: "reports",
            isDropdown: 1,
            withText: "Reports",
            type: "",
            menus: [
              {
                href: null,
                icon: null,
                label: "Estimate Details Report",
                modalKey: null,
                event: (key) => EstimateReport(true),
              },
            ],
          },
          // {
          //   key: "lock",
          //   isDropdown: 0,
          //   withText: "Lock",
          //   type: "",
          //   menus: null,
          //   event: (data, key) =>
          //     console.log(),
          // },
        ],
      },
    ];

    setState((prevState) => ({
      ...prevState,
      showLeftBtn: _isShowLeftBtn,
      showRightBtn: _isShowRightBtn,
    }));
  }, [state?.formData]);

  const voyageBunkerPlan = (showVoyageBunkerPlan) => {
    setState((prevState) => ({
      ...prevState,
      isShowVoyageBunkerPlan: showVoyageBunkerPlan,
    }));
  };

  const reFreshForm = async (estimateId) => {
    if (location.pathname === "/tc-est-fullestimate") {
      setState(
        (prevState) => ({
          ...prevState,
          formData: {},
          refreshForm: true,
        }),
        () => {
          setState((prev) => ({
            ...prev,
            refreshForm: false,
          }));
        }
      );
    } else {
      editformdata(estimateId);
      // editformdata(data["estimate_id"]);
    }
  };

  const firstLoadForm = async (data) => {
    const { tci_details, estimate_id, tco_details, id } = data;

    const res = await updateMainForm(tci_details);

    let _tco = [
      {
        duration: tco_details.tco_duration,
        dailyhire: tco_details.tco_d_hire,
        bb: tco_details.tco_bb,
        acom: tco_details.tco_add_per,
        bcom: tco_details.tco_bro_per,
      },
    ];

    let _formdata = {
      estimateID: estimate_id,
      quick_estimate_id: estimate_id,
      quick_id: id,
      tcov_id: estimate_id,
      vessel_id: tci_details["vessel_id"],
      vessel_name: tci_details.vessel_name,
      vessel_code: tci_details.vessel_code,
      dwt: tci_details.dwt,
      tci_d_hire: tci_details.tci_daily_cost,
      add_percentage_tci: tci_details.tci_add_comm,
      bro_percentage_tci: tci_details.tci_bro_comm,
      wf: tci_details.wf,
      //commence_date: tci_details.commence_date,
      commence_date: tco_details.commence_date,
      //completing_date: tci_details.completed_date,
      completing_date: tco_details.completed_date,
      //total_days: tci_details.total_voyage_days,
      tot_voy_days: tco_details.total_voyage_days,
      mis_cost: tci_details.other_cost,
      ballast_bonus: tci_details.blast_bonus,
      bb: tci_details.blast_bonus,

      //tcoterm:_tco,
      portitinerary: formPortItinerary(data),
      "-": res["-"],
      ".": res["."],
      tci_code: res["tci_code"],
    };

    setState(
      (prevState) => {
        return {
          ...prevState,
          refreshForm: false,
          formData: {
            ...prevState.formData,
            ..._formdata,
          },
        };
      },
      () => {
        setQuickdata(data);
      }
    );
  };

  const initialLoadData = async () => {
    const { estimateID } = state;

    let respData = undefined,
      response = undefined;
    let vessel = undefined,
      cp_echo = undefined;
    const { showLeftBtn, showFixMenu } = state;
    let _showLeftBtn = Object.assign([], showLeftBtn);
    let _showFixMenu = undefined;

    //if ( (params.id && location.pathname == `/edit-tc-est-fullestimate/:${estimateID}`) || estimateID)
    /*
    if (
      params.id &&
      location.pathname == `/edit-tc-est-fullestimate/${params.id}`
    ) {
      respData = await editformdata();
      if (respData.tcov_status == 1) {
        setState({
          ...state,
          disableSave: true,
        });
      }
      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
      );
      vessel = await response["data"];

      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}/tcto/${respData["estimate_id"]}`
      );
      cp_echo = await response["data"];
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];
      respData["vessel_name"] = vessel["vessel_name"];

      respData["-"] =
        respData.hasOwnProperty("-") && respData["-"].length > 0
          ? respData["-"]
          : cp_echo["-"];
      respData["."] =
        respData.hasOwnProperty(".") && respData["."].length > 0
          ? respData["."]
          : cp_echo["."]["eco_data"];

      // // Get CP ECHO Data [TODO TECH]

      // response = await getAPICall(`${URL_WITH_VERSION}/vessel/cp_echo/${respData['vessel_id']}/tcto/${respData['estimate_id']}`);

      // cp_echo = await response['data'];

      // respData['purchase_data'] = cp_echo['.'];

      // respData['-'] = cp_echo.hasOwnProperty('-') && cp_echo['-'].length > 0 ? cp_echo['-'] : '';

      // respData['.'] = cp_echo.hasOwnProperty('.') && cp_echo['.'].length > 0 ? cp_echo['.'] : '';

      respData["tot_voy_days"] = respData["total_voyage_days"];
      respData["other_rev"] = respData["other_rev"];
      respData["fixedtcto"] = respData["id"];

      if (
        respData &&
        respData.hasOwnProperty("tcto_status") &&
        respData["tcto_status"] > 0 &&
        respData.hasOwnProperty("tcto_status") &&
        respData["tcto_status"] == 1
      ) {
        delete _showLeftBtn[0]["isSets"][2];
        delete _showLeftBtn[0]["isSets"][1];
      }

      if (
        respData &&
        respData.hasOwnProperty("tcto_status") &&
        respData["tcto_status"] == 0
      ) {
        _showFixMenu = showFixMenu;
      }

      if (respData && !respData.hasOwnProperty("portitinerary")) {
        respData["portitinerary"] = [{ port: "Select Port", index: 0 }];
      }

      setState(
        (prevState) => ({
          ...prevState,
          formData: Object.assign({}, respData),
          showLeftBtn: _showLeftBtn,
          showFixMenu: _showFixMenu,
          estimateID: estimateID != undefined ? estimateID : params.id,
          refreshForm: false,
        }),
        () => {}
      );
    }
    setState({ ...state, refreshForm: false });
    */
  };

  const editformdata = async (estid = null) => {
    //let estimateID = params.id || estid;
    let estimateID = estid || params.id;
    let vessel = undefined,
      cp_echo = undefined;
    let qParams = { ae: estimateID };
    if (estimateID) {
      setState((prevState) => ({ ...prevState, refreshForm: true }));
      let qParamString = objectToQueryStringFunc(qParams);
      let response = await getAPICall(
        `${URL_WITH_VERSION}/tcto/edit?${qParamString}`
      );
      let respDataC = await response;
      let respData = respDataC["data"];
      respData["tcto_id"] = respData["estimate_id"];

      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
      );
      vessel = await response["data"];
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}/tcto/${respData["estimate_id"]}`
      );
      cp_echo = await response["data"];
      respData["purchase_data"] = cp_echo["."];
      respData["vessel_code"] = vessel["vessel_code"];
      respData["vessel_name"] = vessel["vessel_name"];

      respData["-"] =
        respData.hasOwnProperty("-") && respData["-"].length > 0
          ? respData["-"]
          : cp_echo["-"];

      respData["."] =
        respData.hasOwnProperty(".") && respData["."].length > 0
          ? respData["."]
          : cp_echo["."]["eco_data"];

      respData["tot_voy_days"] = respData["tot_voy_days"];

      let fromfields = [
        "seca_length",
        "eca_days",
        "speed",
        "eff_speed",
        "xsd",
        "turn_time",
        "p_exp",
        "miles",
        "days",
        "t_port_days",
        "xpd",
        "tsd",
        "l_d_qty",
        "gsd",
      ];

      let tofields = [
        "ttl_eca_miles",
        "ttl_eca_days",
        "ttl_speed",
        "ttl_eff_speed",
        "ttl_xsd",
        "ttl_turntime",
        "ttl_exp",
        "total_distance",
        "ttl_pdays",
        "totalt_port_days",
        "ttl_xpd",
        "total_tsd",
        "total_load",
        "total_gsd",
      ];

      // const totalitenary = calculateTotalSummary(
      //   fromfields,
      //   tofields,
      //   "portitinerary",
      //   respData
      // );
      let totalitenary = calculateTotalSummary(
        fromfields,
        tofields,
        "portitinerary",
        respData
      );

      const totalspeed = calculateTotalaverage(
        "speed",
        "portitinerary",
        respData
      );

      const totaleffspeed = calculateTotalaverage(
        "eff_speed",
        "portitinerary",
        respData
      );

      totalitenary = {
        ...totalitenary,
        ttl_speed: totalspeed,
        ttl_eff_speed: totaleffspeed,
      };

      let tobunkerarr = [
        "ttl_miles",
        "ttl_seca_length",
        "ttl_eca_days",
        "ttl_speed",
        "ttl_ec_lsmgo",
        "ttl_ifo",
        "ttl_vlsfo",
        "ttl_lsmgo",
        "ttl_mgo",
        "ttl_ulsfo",
        "ttl_pc_ifo",
        "ttl_pc_vlsfo",
        "ttl_pc_lsmgo",
        "ttl_pc_mgo",
        "ttl_pc_ulsfo",
      ];

      let frombunkerarr = [
        "miles",
        "seca_length",
        "eca_days",
        "speed",
        "eca_consp",
        "ifo",
        "vlsfo",
        "lsmgo",
        "mgo",
        "ulsfo",
        "pc_ifo",
        "pc_vlsfo",
        "pc_lsmgo",
        "pc_mgo",
        "pc_ulsfo",
      ];

      const totalbunker = calculateTotalSummary(
        frombunkerarr,
        tobunkerarr,
        "bunkerdetails",
        respData
      );

      const fromciiarr = [
        "ifo",
        "vlsfo",
        "lsmgo",
        "mgo",
        "ulsfo",
        "pc_ifo",
        "pc_vlsfo",
        "pc_lsmgo",
        "pc_mgo",
        "pc_ulsfo",
        "co2_emission_total",
      ];

      const tociiarr = [
        "ttl_ifo_con",
        "ttl_vlsfo_con",
        "ttl_lsmgo_con",
        "ttl_mgo_con",
        "ttl_ulsfo_con",
        "ttl_pc_ifo_con",
        "ttl_pc_vlsfo_con",
        "ttl_pc_lsmgo_con",
        "ttl_pc_mgo_con",
        "ttl_pc_ulsfo_con",
        "ttl_co_emi",
      ];

      const totalcii = calculateTotalSummary(
        fromciiarr,
        tociiarr,
        "ciidynamics",
        respData
      );

      const totalciiVal = {
        ttl_vlsfo_con: (
          parseFloat(totalcii["ttl_vlsfo_con"]) +
          parseFloat(totalcii["ttl_pc_vlsfo_con"])
        ).toFixed(2),
        ttl_lsmgo_con: (
          parseFloat(totalcii["ttl_lsmgo_con"]) +
          parseFloat(totalcii["ttl_pc_lsmgo_con"])
        ).toFixed(2),
        ttl_ulsfo_con: (
          parseFloat(totalcii["ttl_ulsfo_con"]) +
          parseFloat(totalcii["ttl_pc_ulsfo_con"])
        ).toFixed(2),
        ttl_mgo_con: (
          parseFloat(totalcii["ttl_mgo_con"]) +
          parseFloat(totalcii["ttl_pc_mgo_con"])
        ).toFixed(2),
        ttl_ifo_con: (
          parseFloat(totalcii["ttl_ifo_con"]) +
          parseFloat(totalcii["ttl_pc_ifo_con"])
        ).toFixed(2),
        ttl_co_emi: totalcii["ttl_co_emi"],
      };

      const fromeuetcs = [
        "sea_ems",
        "port_ems",
        "ttl_ems",
        "sea_ets_ems",
        "port_ets_ems",
        "ttl_eu_ets",
        "ttl_eu_ets_exp",
      ];

      const toeuetcs = [
        "ttl_sea_emi",
        "ttl_port_emi",
        "ttl_emi",
        "ttl_sea_ets_emi",
        "ttl_port_ets_emi",
        "ttl_ets_emi",
        "ttl_eu_ets_emi",
      ];

      const totaleuets = calculateTotalSummary(
        fromeuetcs,
        toeuetcs,
        "euets",
        respData
      );
      let totalVoyDays =
        (totalitenary?.["totalt_port_days"] ?? 0) * 1 +
        (totalitenary?.["total_tsd"] ?? 0) * 1;
      respData["totalitinerarysummary"] = { ...totalitenary };
      respData["totalbunkersummary"] = { ...totalbunker };
      respData["totalciidynamicssummary"] = { ...totalciiVal };
      respData["totaleuetssummary"] = { ...totaleuets };
      respData["tot_voy_days"] = isNaN(totalVoyDays)
        ? 0
        : parseFloat(totalVoyDays).toFixed(2);
      setState((prevState) => ({
        ...prevState,
        refreshForm: false,
        estimateID: respData["estimate_id"],
        formData: Object.assign({}, respData),
      }));

      return respData;
    } else {
      setState((prevState) => ({
        ...prevState,
        refreshForm: false,
      }));
    }
  };

  const saveFormData = (postData, innerCB) => {
    const { frmName, fileArr } = state;
    //setState({ ...state, refreshForm: true, isShowVoyageBunkerPlan: false });
    setState((prevState) => ({
      ...prevState,
      refreshForm: true,
      isShowVoyageBunkerPlan: false,
    }));
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty("id")) {
      _url = "update";
      _method = "put";
    }
    if (postData.hasOwnProperty("totalbunkerdetails")) {
      delete postData["totalbunkerdetails"];
    }
    Object.keys(postData).forEach(
      (key) => postData[key] == null && delete postData[key]
    );
    postData["attachments"] = fileArr;

    // if (
    //   postData["vesselfile_id"] &&
    //   postData["vesselfile_id"] != "" &&
    //   postData["tci_code"] &&
    //   postData["tci_code"] != ""
    // ) {
    //   openNotificationWithIcon(
    //     "info",
    //     "Please Select either Head Fix-TCI or Vessel File Code.",
    //     3
    //   );
    //   return;
    // }

    if (postData["commence_date"]) {
      postData["commence_date"] = moment(postData["commence_date"]).format(
        "YYYY-MM-DD HH:mm"
      );
    }

    if (postData["completing_date"]) {
      postData["completing_date"] = moment(postData["completing_date"]).format(
        "YYYY-MM-DD HH:mm"
      );
    }

    if (
      postData.hasOwnProperty("bunkerdetails") &&
      !postData["bunkerdetails"].length > 0
    ) {
      delete postData["bunkerdetails"];
    }

    if (postData.hasOwnProperty("fixedtcto")) {
      delete postData["fixedtcto"];
    }
    if (postData.hasOwnProperty("vessel_name")) {
      delete postData["vessel_name"];
    }
    if (postData.hasOwnProperty("fix")) {
      delete postData["fix"];
    }

    if (postData.hasOwnProperty("tcov_id")) {
      delete postData["tcov_id"];
    }

    if (postData.hasOwnProperty("tcto_id")) {
      delete postData["tcto_id"];
    }

    if (postData.hasOwnProperty("totalciidynamicssummary")) {
      delete postData["totalciidynamicssummary"];
    }

    if (postData.hasOwnProperty("totaleuetssummary")) {
      delete postData["totaleuetssummary"];
    }

    if (postData.hasOwnProperty("totalbunkersummary")) {
      delete postData["totalbunkersummary"];
    }

    if (postData.hasOwnProperty("totalitinerarysummary")) {
      delete postData["totalitinerarysummary"];
    }
    if (postData.hasOwnProperty("purchase_data")) {
      delete postData["purchase_data"];
    }

    // if fuel_code is present in any row
    if (postData.hasOwnProperty(".")) {
      for (const obj of postData["."]) {
        if (!obj.hasOwnProperty("fuel_code")) {
          openNotificationWithIcon(
            "error",
            "Fuel Type can not be unselected",
            3
          );
          setState((prevState) => ({
            ...prevState,
            refreshForm: false,
          }));
          return;
        }
      }
    }

    if (!state.disableSave) {
      postAPICall(
        `${URL_WITH_VERSION}/tcto/${_url}?frm=${frmName}`,
        postData,
        _method,
        (data) => {
          if (data.data) {
            openNotificationWithIcon("success", data.message);

            setState(
              (prevState) => ({
                ...prevState,
                refreshForm: false,
                formData: {
                  ...state.formData,
                  id: data?.row?.id,
                  estimate_id: data?.row?.estimate_id,
                },
              }),
              () => {
                //editformdata(data?.row?.estimate_id);

                // setTimeout(() => {
                //   navigate(
                //     `/edit-tc-est-fullestimate/${data.row.estimate_id}`,
                //     {
                //       replace: true,
                //     }
                //   );
                // }, 1000);
                navigate(`/edit-tc-est-fullestimate/${data.row.estimate_id}`, {
                  replace: true,
                });
              }
            );
          } else {
            setState(
              (prevState) => ({
                ...prevState,
                refreshForm: false,
                //formData: savedataref.current,
              }),
              () => {
                let dataMessage = data.message;
                let msg = "<div className='row'>";

                if (typeof dataMessage !== "string") {
                  Object.keys(dataMessage)?.map(
                    (i) =>
                      (msg +=
                        "<div className='col-sm-12'>" +
                        dataMessage[i] +
                        "</div>")
                  );
                } else {
                  msg += dataMessage;
                }

                msg += "</div>";
                openNotificationWithIcon(
                  "error",
                  <div
                    className="notify-error"
                    dangerouslySetInnerHTML={{ __html: msg }}
                  />
                );
              }
            );
          }
        }
      );
    } else {
      openNotificationWithIcon("error", "This Estimate is already fixed");
    }
  };
  const SearchTci = (showSearchTci) =>
    setState({ ...state, isShowSearchTci: showSearchTci });
  const SearchTco = (showSearchTco) =>
    setState({ ...state, isShowSearchTco: showSearchTco });

  const portDistance = (val, data, key) => {
    if (key == "port_route_details") {
      if (
        data &&
        data.hasOwnProperty("portitinerary") &&
        data["portitinerary"].length > 0
      ) {
        setState(
          {
            ...state,
            portData: data.portitinerary[0],
            portItin: data.portitinerary,
          },
          () => setState({ ...state, isShowPortRoute: val })
        );
      } else {
        setState({ ...state, isShowPortRoute: val });
      }
    } else {
      if (key == "port_distance") {
        setState({ ...state, isShowPortDistance: val });
      }
    }
  };

  const _onCreateFormData = () => {
    setState(
      (prevState) => ({
        ...prevState,
        refreshForm: true,
        formData: {},
        estimateID: null,
      }),
      () => {
        setState((state) => ({
          ...state,
          refreshForm: false,
        }));
      }
    );
    navigate("/tc-est-fullestimate");
    // setTimeout(()=> {
    //   navigate("/tc-est-fullestimate",{replace:true});
    // },2000)
  };

  const itineraryInfo = (val) =>
    setState({ ...state, isShowItineraryInfo: val });

  const EstimateReport = async (showEstimateReport) => {
    if (showEstimateReport == true) {
      let qParamString = objectToQueryStringFunc({
        ae: state.estimateID || params.id,
      });

      // for report Api
      try {
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/tcto/estimatereport?${qParamString}`
          // `http://192.168.31.125:5001/api/v1/tcto/estimatereport?${qParamString}`
        );
        const respDataReport = await responseReport["data"];
        if (
          responseReport &&
          responseReport.data.hasOwnProperty("id") &&
          responseReport.data["id"]
        ) {
          setState(
            (prevState) => ({
              ...prevState,
              reportFormData: respDataReport,
            }),
            () => {
              setState((prevState) => ({
                ...prevState,
                isShowEstimateReport: showEstimateReport,
              }));
            }
          );
        } else {
          openNotificationWithIcon("info", "Please Save Tc Estimate First", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something Went Wrong.", 3);
      }
    } else {
      setState({
        ...state,
        isShowEstimateReport: showEstimateReport,
      });
    }
  };

  const onCloseDrawer = () =>
    setState({
      ...state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  const onClickRightMenu = async (key, options) => {
    onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "port_route_details":
        portDistance(true, state.formData, key);
        break;
      case "port_distance":
        portDistance(true, state.formData, key);
        break;
      case "pl-summary":
        setState({
          ...state,
          sendBackData: { show: true, options: options },
        });
        break;
      case "estimates-summary":
        loadComponent = <EstimateSummary />;
        break;

      case "properties":
        loadComponent = <Properties />;
        break;
      case "attachment":
        //const { estimateID } = state;
        const estimateID = state?.formData?.estimate_id;
        if (estimateID) {
          const attachments = await getAttachments(estimateID, "EST");
          const callback = (fileArr) =>
            uploadAttachment(fileArr, estimateID, "EST", "voyage-manager");
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) =>
                deleteAttachment(file.url, file.name, "EST", "voyage-manager")
              }
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon("info", "Attachments are not allowed here.");
        }
        break;
      case "remark":
        const { formData } = state;
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = (
            <Remarks
              remarksID={formData.estimate_id}
              remarkType="tcto"
              idType="tcto_id"
            />
          );
        else openNotificationWithIcon("info", "Please save TC Estimate First");
        break;
      case "bunker": {
        setState((prev) => ({ ...prev, isBunker: true }));
        break;
      }

      case "map": {
        setState((prev) => ({ ...prev, isMap: true }));
        break;
      }
    }
    if (loadComponent) {
      setState({
        ...state,
        visibleDrawer: true,
        title: options.title,
        loadComponent: loadComponent,
        width: options.width && options.width > 0 ? options.width : 1200,
      });
    }
  };

  const deleteData = (id, tcto_id, innerCB) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/tcto/delete`,
      { id: id, tcto_id: tcto_id },
      (resData) => {
        if (resData.data === true) {
          openNotificationWithIcon("success", resData.message);
          navigate("/TC-Estimate-list", {
            replace: true,
          });
        } else {
          openNotificationWithIcon("error", resData.message);
        }
      }
    );
  };

  const onClickExtraIcon = (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Port Itinerary") {
      groupKey = "portitinerary";
      frm_code = "tab_tcto_port_itinerary";
    }
    if (groupKey == "Bunker Details") {
      groupKey = "bunkerdetails";
      frm_code = "tab_tcto_bunker_details";
    }
    if (groupKey == "Port Date Details") {
      groupKey = "portdatedetails";
      frm_code = "tab_tcto_port_date_details";
    }
    if (groupKey == ".") {
      frm_code = "tcto_full_estimate_form";
    }
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };

      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const onClickFixTCTO = (boolVal) => setState({ ...state, fixModal: boolVal });

  const _onLeftSideListEvent = async (evt) => {
    navigate(
      `/edit-tc-est-fullestimate/` +
        (evt && evt["estimate_id"] ? evt["estimate_id"] : evt)
    );
  };

  const createVoyageManger = async (data, estimat_id, vessel_code) => {
    // const { fixData } = state;
    const { formData } = state;
    // if (
    //   formData["vesselfile_id"] &&
    //   formData["vesselfile_id"] != "" &&
    //   formData["tci_code"] &&
    //   formData["tci_code"] != ""
    // ) {
    //   openNotificationWithIcon(
    //     "info",
    //     "Please Select either Head Fix-TCI or Vessel File Code.",
    //     3
    //   );
    //   return;
    // }

    postAPICall(
      `${URL_WITH_VERSION}/tcto/fix`,
      {
        // id: data,
        tcto_id: formData.id,
        estimat_id: estimat_id,
        vessel_code: vessel_code,
        vessel_name: formData["vessel_name"],
        commence_date: formData["commence_date"],
        completed_date: formData["completing_date"],
      },
      "POST",
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          //if Schedule then navigate
          if (data.message === "Successfully Scheduled !") {
            navigate(`/voyage-manager/${estimat_id}`, { replace: true });
          } else {
            onClickFixTCTO(false);
            editformdata(estimat_id);
          }
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";
          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div
              className="notify-error"
              dangerouslySetInnerHTML={{ __html: msg }}
            />
          );
        }
      }
    );
  };

  const triggerEvent = (data) => {
    const { sendBackData } = state;
    if (data && sendBackData["show"] === true) {
      onCloseDrawer();
      let loadComponent = <PL formData={data} viewTabs={["Estimate View"]} />;
      setState({
        ...state,
        visibleDrawer: true,
        title: sendBackData.options.title,
        loadComponent: loadComponent,
        width:
          sendBackData.options.width && sendBackData.options.width > 0
            ? sendBackData.options.width
            : 1200,
        sendBackData: { show: false },
      });
    }
  };

  const formPortItinerary = (data) => {
    let portItinerary = [];
    const {
      blastformdata,
      loadformdata,
      dischargeformdata,
      reposformdata,
      tci_details,
      blastTodeliveryDistance = 0,
      deliveryToredeliveryDistance = 0,
      redeliveryToreposDistance = 0,
      seca_crossed,
    } = data;

    //if (blastformdata && blastformdata.port_name) {
    if (tci_details?.ballast_port) {
      portItinerary.push({
        port: tci_details?.ballast_port,
        days: blastformdata?.port_days,
        funct: blastformdata?.port_func,
        miles: 0,
        l_d_qty: blastformdata?.ld_qty_unit,
        l_d_rate: blastformdata?.ld_rate_per_day,
        l_d_rate1: blastformdata?.ld_rate_per_hour,
        l_d_term: blastformdata?.term,
        turn_time: blastformdata?.turn_time,
        xpd: blastformdata?.xpd,
        p_exp: blastformdata?.port_expense,
        seca_length: 0,
      });
    }
    if (tci_details?.delivery_port) {
      portItinerary.push({
        port: tci_details?.delivery_port,
        days: loadformdata?.port_days,
        funct: loadformdata?.port_func,
        miles: isNaN(blastTodeliveryDistance) ? 0 : blastTodeliveryDistance,
        l_d_qty: loadformdata?.ld_qty_unit,
        l_d_rate: loadformdata?.ld_rate_per_day,
        l_d_rate1: loadformdata?.ld_rate_per_hour,
        l_d_term: loadformdata?.term,
        turn_time: loadformdata?.turn_time,
        xpd: loadformdata?.xpd,
        p_exp: loadformdata?.port_expense,
        seca_length: seca_crossed?.blseca,
      });
    }
    if (tci_details?.redelivery_port) {
      portItinerary.push({
        port: tci_details?.redelivery_port,
        days: dischargeformdata?.port_days,
        funct: dischargeformdata?.port_func,
        miles: isNaN(deliveryToredeliveryDistance)
          ? 0
          : deliveryToredeliveryDistance,
        l_d_qty: dischargeformdata?.ld_qty_unit,
        l_d_rate: dischargeformdata?.ld_rate_per_day,
        l_d_rate1: dischargeformdata?.ld_rate_per_hour,
        l_d_term: dischargeformdata?.term,
        turn_time: dischargeformdata?.turn_time,
        xpd: dischargeformdata?.xpd,
        p_exp: dischargeformdata?.port_expense,
        seca_length: seca_crossed?.ldseca,
      });
    }
    if (tci_details?.repos_port) {
      portItinerary.push({
        port: tci_details?.repos_port,
        days: reposformdata?.port_days,
        funct: reposformdata?.port_func,
        miles: isNaN(redeliveryToreposDistance) ? 0 : redeliveryToreposDistance,
        l_d_qty: reposformdata?.ld_qty_unit,
        l_d_rate: reposformdata?.ld_rate_per_day,
        l_d_rate1: reposformdata?.ld_rate_per_hour,
        l_d_term: reposformdata?.term,
        turn_time: reposformdata?.turn_time,
        xpd: reposformdata?.xpd,
        p_exp: reposformdata?.port_expense,
        seca_length: seca_crossed?.drseca,
      });
    }

    return portItinerary;
  };

  const updateMainForm = async (formData) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/vessel/list/${formData["vessel_id"]}`
    );
    let vessel = await response["data"];

    // Get CP ECHO Data
    response = await getAPICall(
      `${URL_WITH_VERSION}/vessel/cp_echo/${formData["vessel_id"]}`
    );
    let cp_echo = await response["data"];

    // Get TCI Get Fields
    response = await getAPICall(
      `${URL_WITH_VERSION}/tci/get-fields/${formData["vessel_id"]}`
    );
    let tci_field = await response["data"];

    let respData = Object.assign(formData, {
      "-": cp_echo["-"],
      ".": cp_echo["."]["eco_data"],
      // vessel_code: vessel["vessel_code"],
      // tci_d_hire: vessel["daily_cost"],
      // dwt: vessel["vessel_dwt"],
      // add_percentage: tci_field["add_percentage"],
      // bro_percentage: tci_field["bro_percentage"],
      // description: tci_field["description"],
      // hire_rate: tci_field["hire_rate"],
      tci_code: tci_field["tci_code"],
    });

    return respData;

    // setState({ ...state, formData: respData }, () =>
    //   setState({ ...state, refreshForm: false })
    // );
  };

  // const showQuickEstimatePopup = (sd, bolVal) => {
  //   console.log('sd :',sd);
  //   console.log('bolVal :',bolVal);
  //   setState(
  //     (prevState) => ({
  //       ...prevState,
  //       estimateID: sd["estimate_id"],
  //       quickFormData: sd,
  //     }),
  //     () => setState({ ...state, showQuickEstimateBoolean: bolVal })
  //   );
  // }

  const openRepositionAnalysis = (bool) => {
    if (bool) {
      setState((prevState) => ({ ...prevState, openReposModal: true }));
    }
  };

  const updataCPPrice = (plandata) => {
    let consFuelarr = state.formData?.["."];
    let arr = [];
    consFuelarr?.map((fuelarr, index) => {
      let obj = Object.assign({}, fuelarr);
      let value1 = CpfromBunkerPlan(plandata, obj["fuel_type"]);
      obj["cp_price"] = value1;
      arr.push(obj);
    });

    let _formdata = Object.assign({}, state.formData);
    _formdata["."] = [...arr];
    setState(
      (prevstate) => ({
        ...prevstate,
        isShowVoyageBunkerPlan: false,
      }),
      () => {
        saveFormData(_formdata);
      }
    );
  };

  const CpfromBunkerPlan = (planarr, consFueltype) => {
    let price = 0;
    planarr?.map((el, ind) => {
      if (el.type === consFueltype) {
        price = el?.end_prc;
      }
    });
    price = price ?? "0.00";
    return price;
  };

  return (
    <div className="wrap-rightbar full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <h4 className="cust-header-title">
                    <b>
                      TC Estimate Full Estimate : &nbsp;
                      {params.id ? params.id : ""}
                    </b>
                  </h4>
                  <div className="box box-default">
                    <div className="box-body">
                      {state.refreshForm === false ? (
                        <NormalFormIndex
                          key={"key_" + state.frmName + "_0"}
                          formClass="label-min-height"
                          formData={state.formData}
                          showForm={true}
                          frmCode={state.frmName}
                          addForm={true}
                          sideList={{
                            showList: true,
                            title: "TC-EST List",
                            uri: "/tcto/list?l=0",
                            columns: [
                              "estimate_id",
                              "vessel_name",
                              "tcto_status",
                            ],
                            icon: true,
                            rowClickEvent: (evt) => _onLeftSideListEvent(evt),
                          }}
                          showToolbar={[
                            {
                              // isLeftBtn: state.showLeftBtn,
                              // isRightBtn: [
                              //   {
                              //     isSets: [
                              //       state.vmCheck
                              //         ? null
                              //         : {
                              //             key: "reposition_analysis",
                              //             isDropdown: 0,
                              //             withText: "Reposition Analysis",
                              //             type: "",
                              //             menus: null,
                              //             event: (key) => {
                              //               openRepositionAnalysis(true);
                              //             },
                              //           },
                              //       {
                              //         key: "quick_estimate",
                              //         isDropdown: 0,
                              //         withText: "View Quick Est.",
                              //         type: "",
                              //         menus: null,
                              //         event: (key, data) => {
                              //           // navigate(
                              //           //   `/edit-tc-estimate/${state.estimateID}`,
                              //           //   {
                              //           //     state: {
                              //           //       data: state.estimateID,
                              //           //     },
                              //           //   }
                              //           // )

                              //           // console.log('data :',data);

                              //           // if (state?.formData?.quick_estimate_id !== undefined && state?.formData?.quick_estimate_id !== null) {
                              //           //   navigate(`/edit-tc-estimate/${state.formData.quick_estimate_id}`, {
                              //           //     state: {
                              //           //       data: state.formData.quick_estimate_id,
                              //           //     },
                              //           //   });
                              //           // } else {
                              //           //   navigate('/quick-estimate', {
                              //           //     state: {
                              //           //       data: state.formData
                              //           //     },
                              //           //   });
                              //           // }

                              //           if (
                              //             data["quick_estimate_id"] &&
                              //             data["quick_id"]
                              //           ) {
                              //             navigate(
                              //               `/edit-tc-estimate/` +
                              //                 data["quick_estimate_id"],
                              //               {
                              //                 state: {
                              //                   data: data,
                              //                   fullEstimate: true,
                              //                 },
                              //               }
                              //             );
                              //           } else {
                              //             navigate(`/add-TC-estimate`, {
                              //               state: {
                              //                 data: data,
                              //                 fullEstimate: true,
                              //               },
                              //             });
                              //           }
                              //         },
                              //       },

                              //       {
                              //         key: "bunker_plan",
                              //         isDropdown: 0,
                              //         withText: "Bunker Plan",
                              //         type: "",
                              //         menus: null,
                              //         event: (data, key) =>
                              //           voyageBunkerPlan(true),
                              //       },
                              //       {
                              //         key: "ai",
                              //         isDropdown: 0,
                              //         withText: "AI",
                              //         type: "",
                              //         menus: null,
                              //         event: (data, key) => console.log(),
                              //       },
                              //       // {
                              //       //   key: "bench_mark",
                              //       //   isDropdown: 0,
                              //       //   withText: "Benchmarking",
                              //       //   type: "",
                              //       //   menus: null,
                              //       //   event: (data, key) => console.log(),
                              //       // },
                              //       {
                              //         key: "Send_Firm_Offer",
                              //         isDropdown: 0,
                              //         withText: "Send Firm Offer",
                              //         type: "",
                              //         menus: null,
                              //         event: (data, key) => console.log(),
                              //       },
                              //       // {
                              //       //   key: "Draw_CP",
                              //       //   isDropdown: 0,
                              //       //   withText: "Draw CP",
                              //       //   type: "",
                              //       //   menus: null,
                              //       //   event: (data, key) => console.log(),
                              //       // },
                              //       {
                              //         key: "share",
                              //         isDropdown: 0,
                              //         withText: "Share",
                              //         type: "",
                              //         menus: null,
                              //         event: (data, key) => console.log(),
                              //       },

                              //       ...(state?.formData?.tcto_status !== 2
                              //         ? [
                              //             {
                              //               key:
                              //                 state?.formData?.tcto_status === 1
                              //                   ? "schedule"
                              //                   : "fix",
                              //               isDropdown: 0,
                              //               withText:
                              //                 state?.formData?.tcto_status === 1
                              //                   ? "Schedule"
                              //                   : "Fix",
                              //               type: "",
                              //               menus: null,
                              //               event: (data, key) => {
                              //                 Modal.confirm({
                              //                   title: "Confirm",
                              //                   content:
                              //                     "Are you sure, you want to Fix/Schedule it?",
                              //                   // onOk: () => onClickFixTCTO(true),

                              //                   onOk: () => {
                              //                     if (
                              //                       state?.formData
                              //                         ?.tcto_status === 1
                              //                     ) {
                              //                       createVoyageManger(
                              //                         state.formData.fixedtcto,
                              //                         state.formData.tcto_id,
                              //                         state.formData.vessel_code
                              //                       );
                              //                     } else {
                              //                       onClickFixTCTO(true);
                              //                     }
                              //                   },
                              //                 });
                              //               },
                              //             },
                              //           ]
                              //         : []),

                              //       {
                              //         key: "reports",
                              //         isDropdown: 1,
                              //         withText: "Reports",
                              //         type: "",
                              //         menus: [
                              //           {
                              //             href: null,
                              //             icon: null,
                              //             label: "Estimate Details Report",
                              //             modalKey: null,
                              //             event: (key) => EstimateReport(true),
                              //           },
                              //         ],
                              //       },
                              //       // {
                              //       //   key: "lock",
                              //       //   isDropdown: 0,
                              //       //   withText: "Lock",
                              //       //   type: "",
                              //       //   menus: null,
                              //       //   event: (data, key) =>
                              //       //     console.log(),
                              //       // },
                              //     ],
                              //   },
                              // ],
                              isLeftBtn: state.showLeftBtn,

                              isRightBtn: state.showRightBtn,
                            },
                          ]}
                          tabEvents={[
                            {
                              tabName: "Bunker Details",
                              event: {
                                type: "copy",
                                from: "Port Itinerary",
                                page: "tcto",
                                fields: {
                                  port_id: "port_id",
                                  port: "port",
                                  funct: "funct",
                                  passage: "passage",
                                  speed: "speed",
                                  miles: "miles",
                                  seca_length: "seca_length",
                                  eca_days: "eca_days",
                                  s_type: "spd_type",
                                  tsd: "tsd",
                                },

                                showSingleIndex: false,
                                group: {
                                  to: "----------",
                                  from: "---------",
                                },
                                otherCopy: [
                                  {
                                    gKey: "purchase_data",
                                    tgKey: "bunkerdetails",
                                    type: "consumptionCopy",
                                  },
                                  {
                                    gKey: "portdatedetails",
                                    tgKey: "bunkerdetails",
                                    type: "arrDep",
                                  },
                                ],
                              },
                            },

                            {
                              tabName: "Port Date Details",
                              event: {
                                type: "copy",
                                from: "Port Itinerary",
                                page: "tcto",
                                fields: {
                                  port_id: "port_id",
                                  port: "port",
                                  funct: "funct",
                                  passage: "passage",
                                  speed: "speed",
                                  miles: "miles",
                                  wf_per: "wf_per",
                                  tsd: "tsd",
                                  xsd: "xsd",
                                  t_port_days: "pdays",
                                  s_type: "s_type",
                                },
                                showSingleIndex: false,
                                group: {
                                  to: "-----------",
                                  from: "---------",
                                },
                              },
                            },
                            {
                              tabName: "CII Dynamics",
                              event: {
                                type: "copy",
                                from: "Bunker Details",
                                page: "tcto",
                                fields: {
                                  port: "port",
                                  funct: "funct",
                                  ifo: "ifo",
                                  vlsfo: "vlsfo",
                                  ulsfo: "ulsfo",
                                  lsmgo: "lsmgo",
                                  mgo: "mgo",
                                  pc_ifo: "pc_ifo",
                                  pc_vlsfo: "pc_vlsfo",
                                  pc_ulsfo: "pc_ulsfo",
                                  pc_lsmgo: "pc_lsmgo",
                                  pc_mgo: "pc_mgo",
                                },
                                showSingleIndex: false,
                              },
                            },

                            {
                              tabName: "EU ETS",
                              event: {
                                type: "copy",
                                from: "Bunker Details",
                                page: "tcto",
                                fields: {
                                  port: "port",
                                  funct: "funct",
                                  ifo: "ifo",
                                  vlsfo: "vlsfo",
                                  ulsfo: "ulsfo",
                                  lsmgo: "lsmgo",
                                  mgo: "mgo",
                                  pc_ifo: "pc_ifo",
                                  pc_vlsfo: "pc_vlsfo",
                                  pc_ulsfo: "pc_ulsfo",
                                  pc_lsmgo: "pc_lsmgo",
                                  pc_mgo: "pc_mgo",
                                },
                                showSingleIndex: false,
                              },
                            },
                          ]}
                          inlineLayout={true}
                          showSideListBar={state.showItemList}
                          isShowFixedColumn={[
                            "Port Date Details",
                            "Bunker Details",
                            "Port Itinerary",
                            "TCO Term",
                            "CII Dynamics",
                            "EU ETS",
                            "Total Itinerary Details",
                            ".",
                            "Tco Term",
                            "Total Bunker Details",
                          ]}
                          sendBackData={state.sendBackData}
                          triggerEvent={triggerEvent}
                          tableRowDeleteAction={(action, data) =>
                            onClickExtraIcon(action, data)
                          }
                        />
                      ) : (
                        <div className="col col-lg-12">
                          <Spin tip="Loading...">
                            <Alert
                              message=" "
                              description="Please wait..."
                              type="info"
                            />
                          </Spin>
                        </div>
                      )}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
        <RightBarUI
          pageTitle="tcto-righttoolbar"
          callback={(data, options) => onClickRightMenu(data, options)}
        />
        {state.loadComponent !== undefined &&
        state.title !== undefined &&
        state.visibleDrawer === true ? (
          <Drawer
            title={state.title}
            placement="right"
            closable={true}
            onClose={onCloseDrawer}
            open={state.visibleDrawer}
            getContainer={false}
            style={{ position: "absolute" }}
            width={state.width}
            maskClosable={false}
            className="drawer-wrapper-container"
          >
            <div className="tcov-wrapper">
              <div className="layout-wrapper scrollHeight">
                <div className="content-wrapper noHeight">
                  {state.loadComponent}
                </div>
              </div>
            </div>
          </Drawer>
        ) : undefined}
      </Layout>

      {state.openReposModal ? (
        <Modal
          style={{ top: "2%" }}
          title="Reposition Analysis"
          open={state.openReposModal}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, openReposModal: false }))
          }
          width="85%"
          footer={null}
        >
          <RepositionAnalysis />
        </Modal>
      ) : undefined}

      {state.isShowSearchTci ? (
        <Modal
          style={{ top: "2%" }}
          title="Search TCI"
          open={state.isShowSearchTci}
          onCancel={() => SearchTci(false)}
          width="95%"
          footer={null}
        >
          <SearchTci />
        </Modal>
      ) : undefined}

      {state.isShowSearchTco ? (
        <Modal
          style={{ top: "2%" }}
          title="Search TCO"
          open={state.isShowSearchTco}
          onCancel={() => SearchTco(false)}
          width="95%"
          footer={null}
        >
          <SearchTco />
        </Modal>
      ) : undefined}

      {/* {isShowTctoReports ? (
      <Modal
        style={{ top: "2%" }}
        title="Reports"
        open={isShowTctoReports}
        onOk={handleOk}
        onCancel={() => TctoReports(false)}
        width="95%"
        footer={null}
      >
        <TctoReports />
      </Modal>
    ) : (
      undefined
    )} */}

      {state.isShowPortDistance ? (
        <Modal
          style={{ top: "2%" }}
          title="Port Distance"
          open={state.isShowPortDistance}
          onCancel={() => portDistance(false, {}, "port_distance")}
          width="95%"
          footer={null}
        >
          <PortDistance />
        </Modal>
      ) : undefined}
      {state.isShowPortRoute ? (
        <Modal
          style={{ top: "2%" }}
          title="Port Route Details"
          open={state.isShowPortRoute}
          onCancel={() => portDistance(false, {}, "port_route_details")}
          width="100%"
          footer={null}
        >
          <ShipDistance
            data={state.portData}
            is_port_to_port={true}
            portItin={state.portItin}
          />
        </Modal>
      ) : undefined}
      {state.isShowItineraryInfo ? (
        <Modal
          style={{ top: "2%" }}
          title="Itinerary Info"
          open={state.isShowItineraryInfo}
          onCancel={() => itineraryInfo(false)}
          width="95%"
          footer={null}
        >
          <TctoReports />
        </Modal>
      ) : undefined}

      {state.isShowEstimateReport ? (
        <Modal
          style={{ top: "2%" }}
          title="TC Estimate Detail Report"
          open={state.isShowEstimateReport}
          onCancel={() => EstimateReport(false)}
          width="95%"
          footer={null}
        >
          <TctoEstimateDetail data={state.reportFormData} />
        </Modal>
      ) : undefined}

      {state.fixModal ? (
        <Modal
          style={{ top: "2%" }}
          open={state.fixModal}
          title="Create Voyage"
          onCancel={() => onClickFixTCTO(false)}
          footer={null}
          width="40%"
          maskClosable={false}
        >
          <div>
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    <Form>
                      <FormItem {...formItemLayout} label="Vessel">
                        <input
                          type="text"
                          className="ant-input"
                          name="vessel"
                          value={state.formData["vessel_name"]}
                          readOnly
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Current Status">
                        <input
                          type="text"
                          className="ant-input"
                          name="opt_type"
                          value="TCE"
                          readOnly
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Commence Date">
                        <input
                          type="text"
                          className="ant-input"
                          name="commence_date"
                          value={state.formData["commence_date"]}
                          readOnly
                        />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Completed Date">
                        <input
                          type="text"
                          className="ant-input"
                          name="completed_date"
                          value={state.formData["completing_date"]}
                          readOnly
                        />
                      </FormItem>

                      <div className="action-btn text-right">
                        <Button
                          type="primary"
                          onClick={() =>
                            createVoyageManger(
                              state.formData.fixedtcto,
                              state.formData.tcto_id,
                              state.formData.vessel_code
                            )
                          }
                        >
                          Ok
                        </Button>
                      </div>
                    </Form>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </Modal>
      ) : undefined}

      {state.isShowVoyageBunkerPlan ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Voyage Bunker Plan"
          open={state.isShowVoyageBunkerPlan}
          //onOk={handleOk}
          onCancel={() => voyageBunkerPlan(false)}
          width="90%"
          footer={null}
          maskClosable={false}
        >
          <Suspense fallback="Loading....">
            <MemoizedVoygenBunkerPlan
              data={state?.formData?.bunkerdetails}
              updatecp={(data) => updataCPPrice(data)}
            />
          </Suspense>
        </Modal>
      ) : undefined}

      {state.isBunker && (
        <Modal
          style={{ top: "2%" }}
          title=""
          open={state.isBunker}
          onCancel={() => setState((prev) => ({ ...prev, isBunker: false }))}
          width="80%"
          footer={null}
        >
          <SpotPrice />
        </Modal>
      )}
      {state.isMap && (
        <Modal
          style={{ top: "2%" }}
          title="Weather Report"
          open={state.isMap}
          onCancel={() => setState((prev) => ({ ...prev, isMap: false }))}
          width="90%"
          footer={null}
        >
          

          <iframe
            src="https://www.ventusky.com/?p=19.1;72.9;5&l=radar"
            title="Report Section"
            style={{ width: "100%", height: "100vh" }}
            frameborder="0"
            allowFullScreen={true}
          />
        </Modal>
      )}
    </div>
  );
};

export default TCTOFullEstimate;
