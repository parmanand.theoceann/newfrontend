import React from 'react';

const MarketBunkerData = () => {
    return (
        <div className="body-wrapper">
            <article className="article">
                <div className="box box-default">
                    <div className="box-body">
                        Market Bunker Data
                    </div>
                </div>
            </article>
        </div>
    );
}

export default MarketBunkerData;