import React from 'react';

const TradeData = () => {
    return (
        <div className="body-wrapper">
            <article className="article">
                <div className="box box-default">
                    <div className="box-body">
                        Trade Data
                    </div>
                </div>
            </article>
        </div>
    );
}

export default TradeData;