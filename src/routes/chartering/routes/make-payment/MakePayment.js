import React, { Component } from 'react';
import { Table, Form, Select, Input, Modal,/* Button, Icon,*/ Checkbox } from 'antd';
//import DownloadInvoice from './DownloadInvoice';
import ConfirmStatement from './ConfirmStatement';
import Tde from '../../../tde/Tde';
//import TcovReports from '../tcov/tcov-report/TcovReports';


const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const columns = [
  {
    title: 'Invoice No.',
    dataIndex: 'invoiceno',
  },

  {
    title: 'Invoice Date',
    dataIndex: 'invoicedate',
  },

  {
    title: 'Terms',
    dataIndex: 'terms',
  },

  {
    title: 'Paid Date',
    dataIndex: 'paiddate',
  },

  {
    title: 'Amt. Paid',
    dataIndex: 'amtpaid',
  },
];
const data = [
  {
    key: '1',
    invoiceno: 'Invoice No',
    invoicedate: 'Invoice Date',
    terms: 'Terms',
    paiddate: 'Paid Date',
    amtpaid: 'Amt. Paid',
  },

  {
    key: '2',
    invoiceno: 'Invoice No',
    invoicedate: 'Invoice Date',
    terms: 'Terms',
    paiddate: 'Paid Date',
    amtpaid: 'Amt. Paid',
  },

  {
    key: '3',
    invoiceno: 'Invoice No',
    invoicedate: 'Invoice Date',
    terms: 'Terms',
    paiddate: 'Paid Date',
    amtpaid: 'Amt. Paid',
  },


];

class MakePayment extends Component {
  state = {
    modals: {
      DownloadInvoice: false,
      Tde: false,
    },
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };
  
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    {/* <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt">
                          <SaveOutlined /> Save
                        </span>
                      </li>
                      <li>
                        <span className="text-bt">
                         <DeleteOutlined /> />
                        </span>
                      </li>
                    </ul> */}
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt">Make Payment</span>
                      </li>
                      <li>
                        <span
                          className="text-bt"
                          onClick={() => this.showHideModal(true, 'DownloadInvoice')}
                        >

                          Hire Statement
                        </span>
                      </li>
                      <li>
                        <span className="text-bt">Statement of Account</span>
                      </li>
                      <li>
                        <span className="text-bt">Other Reports</span>
                      </li>

                      <li>
                        <span className="text-bt" 
                          onClick={() => this.showHideModal(true, 'Tde')}>TDE</span>
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Vessel Name">
                      <Select defaultValue="1" size="default">
                        <Option value="1">Select</Option>
                      </Select>
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="TCI Code">
                      <Input size="default" defaultValue="12345" disabled />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label=" Charterer From ">
                      <Select defaultValue="1" size="default">
                        <Option value="1">Select</Option>
                      </Select>
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="CP Date">
                      <Input type="date" size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Delivery">
                      <Input type="date" size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Redelivery">
                      <Input type="date" size="default" />
                    </FormItem>
                  </div>
                </div>
              </Form>

              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>

              <Form>
                <div className="row">
                  <div className="col-md-4">
                    <div className="row">
                      <div className="col-md-6">
                        <Checkbox />
                        <span className="ml-2">Invoiced</span>
                      </div>

                      <div className="col-md-6">
                        <Checkbox />
                        <span className="ml-2">Paid</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 ml-auto">
                    <FormItem {...formItemLayout} label="Total">
                      <Input size="default" defaultValue="12345" />
                    </FormItem>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>

        <Modal
          style={{ top: '2%' }}
          title="Select Statement"
          open={this.state.modals['DownloadInvoice']}
          onCancel={() => this.showHideModal(false, 'DownloadInvoice')}
          width="50%"
          footer={null}
        >
          <ConfirmStatement />
        </Modal>

        <Modal
          style={{ top: '2%' }}
          title="TDE"
          open={this.state.modals['Tde']}
          onCancel={() => this.showHideModal(false, 'Tde')}
          width="90%"
          footer={null}
        >
          <Tde />
        </Modal>
      </div>
    );
  }
}

export default MakePayment;
