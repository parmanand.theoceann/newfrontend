import React, { Component } from 'react';
import { Row, Col, Layout, Drawer, Modal, Spin, Alert } from 'antd';
import NormalFormIndex from '../../../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
} from '../../../../shared';
import RightBarUI from '../../../../components/RightBarUI';
import DemdesTerm from './dem-desh-term/DemdesTerm';
import { DeleteOutlined, MenuFoldOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';

const { Content } = Layout;
// const FormItem = Form.Item;
// const TextArea = Input.TextArea;
// const InputGroup = Input.Group;
 
class CargoContract extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      frmName: 'cargo_contract_form',
      copyFrmName: 'ccc_form',
      formData: this.props.formData || {},
      copyFormData: {},
      cargoContracts: [],
      frmVisible: true,
      extraFormFields: null,
      visibleTask: false,
      visibleRevision: false,
      isVisibleCopy: false,
      isVisible: true,
      doCopyContract: true,
    };
  }

  componentDidMount = async () => {
    const response = await getAPICall(`${URL_WITH_VERSION}/cargo/list?l=0`);
    const data = await response['data'];
    this.setState({
      ...this.state,
      cargoContracts: data,
      extraFormFields: {
        isShowInMainForm: true,
        //"content": this.getExternalFormFields()
      },
    });
    if (this.props.fullEstimate && this.props.fullEstimate == true) {
      if (this.props.formData) {
        this.setState({ ...this.state, formData: this.props.formData });
      }
    } else {
      const { match } = this.props;
      if (match && match.params && match.params.id) {
        this.onEditClick();
      }
    }
  };

  saveFormData = (postData, innerCB) => {
    const { frmName } = this.state;
    let _url = 'save';
    let _method = 'post';
    if (this.props.fullEstimate) {
      postData.isTCOVForm = 1;
    }
    if (postData.hasOwnProperty('id')) {
      _url = 'update';
      _method = 'put';
      if (this.props.fullEstimate) {
        postData.isTCOVForm = 1;
      }
    }

    Object.keys(postData).forEach(key => postData[key] === null && delete postData[key]);

    postAPICall(`${URL_WITH_VERSION}/cargo/${_url}?frm=${frmName}`, postData, _method, data => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        if (this.props.modalCloseEvent && typeof this.props.modalCloseEvent === 'function') {
          this.props.modalCloseEvent(data.row);
          if (this.props.fullEstimate) {
            this.props.getCargo(data.row);
          }
        } else if (innerCB && typeof innerCB === 'function') {
          innerCB();
        }
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />, 5);
      }
    });
  };

  onCargoItemClick = async cargoContractID => {
    this.setState({ ...this.state, frmVisible: false });
    const response = await getAPICall(`${URL_WITH_VERSION}/cargo/edit?e=${cargoContractID}`);
    const data = await response['data'];
    this.setState({ ...this.state, formData: data }, () => {
      this.setState({ ...this.state, frmVisible: true });
    });
  };

  onEditClick = async () => {
    const { match } = this.props;
    this.setState({ ...this.state, frmVisible: false });
    if (match.params.id && match.path === '/edit-voyage-charter/:id') {
      const response = await getAPICall(`${URL_WITH_VERSION}/cargo/edit?e=${match.params.id}`);
      const data = await response['data'];
      this.setState({ ...this.state, formData: data, }, () => {
        this.setState({ ...this.state, frmVisible: true });
      });
    }
  };


  onClickRightMenu = key => {
    // console.log(key)
    if (key === 'tasks') {
      this.setState({
        visibleTask: true,
      });
    } else if (key == 'revisions') {
      this.setState({
        visibleRevision: true,
      });
    }
  };

  onCloseDrawer = () => {
    this.setState({
      visibleTask: false,
    });
  };

  onCloseRevision = () => {
    this.setState({
      visibleRevision: false,
    });
  };

  onCloseModal = visible => {
    if (
      visible === true &&
      this.state.formData &&
      this.state.formData['id'] &&
      this.state.formData['id'] * 1 > 0
    ) {
      this.setState({ ...this.state, isVisibleCopy: visible });
    } else if (
      visible === true &&
      this.state.formData &&
      !this.state.formData.hasOwnProperty('id')
    ) {
      openNotificationWithIcon(
        'error',
        <div
          dangerouslySetInnerHTML={{ __html: 'Please Save Contract First and Then Click on Copy.' }}
        />,
        5
      );
    } else if (visible === false) {
      this.setState({ ...this.state, isVisibleCopy: visible });
    }
  };

  copyContract = (data, innerCB) => {
    let cpData = Object.assign({ cargo_id: this.state.formData['cargo_contract_id'] }, data);
    this.setState({ ...this.state, doCopyContract: false });
    postAPICall(`${URL_WITH_VERSION}/cargo/copy`, cpData, 'post', resp => {
      if (resp.data) {
        openNotificationWithIcon('success', resp.message);
        let rows = Object.assign(data, { contractcopy: resp.row.contract, id: 0 });
        this.setState({ ...this.state, copyFormData: rows, doCopyContract: true });
      } else {
        let dataMessage = resp.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />, 5);
      }
    });
  };

  ///  UPDATE BY AMAR 

  toggleCargoRightMenu = async (val, type = "save", data = null) => {
    const { formData } = this.state;
    const { id, contract_id, company_id, vci_lob, vci_status, c_name, invoice_by, trade_area, cargo_name, min_inv_qty, owner_id, cp_qty_per_lift, currency, min_qty, max_qty, freight_bill_via, cp_date, cp_place, freight_type, freight_rate, fixed_by_user } = formData

    let postFormData = Object.assign({
      id: -1,
      my_company: company_id,
      lob: vci_lob,
      c_status: vci_status,
      cargo_name: c_name,
      invoice_by,
      trade_area,
      cargo_group: cargo_name,
      min_inv_qty,
      charterer: owner_id,
      cp_qty: cp_qty_per_lift,
      currency,
      min_qty,
      max_qty,
      freight_bill_via,
      cp_date,
      cp_place,
      freight_type,
      freight_rate,
      // user_fixed_by: fixed_by_user,
      coa_vc_purchase_id: contract_id,
      billingandbankingdetails: formData.billingandbankingdetails,
      broker: formData.broker,
      extrafreightterm: formData.extrafreightterm,
      itineraryoptions: formData.itineraryoptions,
      loadoptions: formData.loadoptions,
      //nominationtasks:formData.nominationtasks,
      rebillsettings: formData.rebillsettings,
      revexpinfo: formData.revexp,
      dischargeoptions: formData.dischargeoptions,
      //cargo:formData.cargo,
      //vesseltype:formData.vesseltype
    })

    if (postFormData.billingandbankingdetails && postFormData.billingandbankingdetails.id) {
      delete postFormData.billingandbankingdetails.id
    }
    if (postFormData.broker && postFormData.broker.length > 0) {
      postFormData.broker.map(e => {
        delete e.id
      })
    }
    if (postFormData.extrafreightterm && postFormData.extrafreightterm.length > 0) {
      postFormData.extrafreightterm.map(e => {
        delete e.id
      })
    }
    if (postFormData.itineraryoptions && postFormData.itineraryoptions.length > 0) {
      postFormData.itineraryoptions.map(e => {
        delete e.id
      })
    }
    if (postFormData.loadoptions && postFormData.loadoptions.length > 0) {
      postFormData.loadoptions.map(e => {
        delete e.id
      })
    }
    if (postFormData.rebillsettings && postFormData.rebillsettings.length > 0) {
      postFormData.rebillsettings.map(e => {
        delete e.id
      })
    }
    if (postFormData.revexpinfo && postFormData.revexpinfo.length > 0) {
      postFormData.revexpinfo.map(e => {
        delete e.id
      })
    }
    if (postFormData.dischargeoptions && postFormData.dischargeoptions.length > 0) {
      postFormData.dischargeoptions.map(e => {
        delete e.id
      })
    }

    if (postFormData.id == -1) {
      delete postFormData.id
    }

    if (val) {
      if (type == "edit") {
        const response = await getAPICall(`${URL_WITH_VERSION}/voyagecargo/list`);
        const respData = await response['data'];
        let filterData = respData.filter(e => e.id == data.ID)
        if (filterData && filterData.length > 0) {
          let id = filterData[0]['vc_purchase_id'];
          postFormData = { "params": { id } }
          this.props.history.push(`/edit-voyage-cargo/${id}`)
          //this.setState({ ...this.state, isVisible: val, postFormData });
        }
        // if (data && data.hasOwnProperty("index") && data["index"] > -1) {
        //   let id = formData["-"][data["index"]]["id"];
        //   postFormData = { "params": { id } }
        //   this.setState({ ...this.state, isVisible: val, postFormData });
        // }
      } else {
        if (formData && formData.hasOwnProperty('id') && formData['id'] > 0)
          this.setState({ ...this.state, isVisible: val, postFormData });
        else
          openNotification('Create VC (purchase) FGGGG')
      }
    } else {
      this.setState({ ...this.state, isVisible: val, postFormData: [] });
    }
  }

  ///////////////

  updateCopyContarct = (data, innerCB) => {
    this.setState({ ...this.state, doCopyContract: false });
    postAPICall(`${URL_WITH_VERSION}/cargo/copy/update`, data, 'put', resp => {
      if (resp.data) {
        openNotificationWithIcon('success', resp.message);
        this.setState({ ...this.state, doCopyContract: true });
        this.onCloseModal(false);
        if (innerCB && typeof innerCB === 'function') {
          innerCB();
        }
      } else {
        let dataMessage = resp.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />, 5);
      }
    });
  };

  render() {
    const {
      frmName,
      formData,
      copyFrmName,
      copyFormData,
      cargoContracts,
      frmVisible,
      extraFormFields,
      isVisibleCopy,
      doCopyContract,
    } = this.state;
    return (
      <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <Row gutter={16} style={{ marginRight: 0 }}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="body-wrapper">
                    <div className="row">
                      {/* <div className="col-md-2 pr0 fieldscroll-wraps-scroll">
                        <div className="normal-heading">Corgo Contracts - List</div>
                        <div className="fieldscroll-wraps-list">
                          {
                            cargoContracts.map(e => {
                              return <article className="article">
                                <div className="box box-default">
                                  <div className="bunkerInvoiceWrapper" onClick={() => { this.onCargoItemClick(e['cargo_contract_id']) }}>
                                    <span className="heading"><strong>{e.cargo_contract_id}</strong></span>
                                    <span className="sub-heading">N/A</span>
                                    <span className="value">Uni Challence</span>
                                  </div>
                                </div>
                              </article>
                            })
                          }
                        </div>
                      </div> */}

                      <div className="col-md-12">
                        <article className="article">
                          <div className="box box-default">
                            <div className="fieldscroll-wraps">
                              <Row>
                                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                  {frmName && frmVisible && extraFormFields ? (
                                    <article className="article">
                                      <div className="box box-default">
                                        <div className="box-body">
                                          <NormalFormIndex
                                            key={'key_' + frmName + '_0'}
                                            formClass="label-min-height"
                                            formData={formData}
                                            showForm={true}
                                            frmCode={frmName}
                                            inlineLayout={true}
                                            extraFormFields={extraFormFields}
                                            showButtons={[
                                              { id: 'cancel', title: 'Reset', type: 'danger' },
                                              {
                                                id: 'save',
                                                title: 'Save',
                                                type: 'primary',
                                                event: (data, innerCB) => {
                                                  this.saveFormData(data, innerCB);
                                                },
                                              },
                                            ]}
                                            showToolbar={[
                                              {
                                                leftWidth: 8,
                                                rightWidth: 16,
                                                isLeftBtn: [
                                                  {
                                                    key: 's1',
                                                    isSets: [
                                                      {
                                                        id: '7',
                                                        key: 'menu-fold',
                                                        type: <MenuFoldOutlined/>,
                                                        withText: '',
                                                        event: 'menu-fold',
                                                      },
                                                      {
                                                        id: '8',
                                                        key: 'plus',
                                                        type: <PlusOutlined />,
                                                        withText: '',
                                                      },
                                                      {
                                                        id: '2',
                                                        key: 'save',
                                                        type: <SaveOutlined />,
                                                        withText: '',
                                                        event: key => {
                                                          console.log(key);
                                                        },
                                                      },
                                                      // { id: "3", key: "edit", type: "edit", withText: "", "event": (key) => { console.log(key) } },
                                                      {
                                                        id: '4',
                                                        key: 'delete',
                                                        type: <DeleteOutlined />,
                                                        withText: '',
                                                      },
                                                    ],
                                                  },
                                                ],
                                                isRightBtn: [
                                                  {
                                                    key: 's2',
                                                    isSets: [
                                                      {
                                                        key: 'menu',
                                                        isDropdown: 1,
                                                        withText: 'Menu',
                                                        type: '',
                                                        menus: [
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Report',
                                                            modalKey: null,
                                                          },
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Link VC-Pur',
                                                            modalKey: null,
                                                          },
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Edit VC',
                                                            modalKey: null,
                                                          },
                                                        ],
                                                      },
                                                      {
                                                        key: 'relet',
                                                        isDropdown: 1,
                                                        withText: 'Voy Relet',
                                                        type: '',
                                                        menus: [
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Create & Link VC-Pur',
                                                            modalKey: null,
                                                            event: (key, data) => data['is_fixed'] == 1 ? this.toggleCargoRightMenu(true) : openNotificationWithIcon('info', `Please fix first and then click on ${key}!`)
                                                          },
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Edit VC-Pur',
                                                            modalKey: null,
                                                          },
                                                        ],
                                                      },
                                                      {
                                                        key: 'create_fixture',
                                                        isDropdown: 0,
                                                        withText: 'Fix',
                                                        type: '',
                                                        menus: null,
                                                        event: key => {
                                                          // console.log(key);
                                                        },
                                                      },
                                                      {
                                                        key: 'schedule_voyage',
                                                        isDropdown: 1,
                                                        withText: 'Schedule Voyage',
                                                        type: '',
                                                        menus: [
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'TCOV Estimate',
                                                            modalKey: null,
                                                          },
                                                          {
                                                            href: null,
                                                            icon: null,
                                                            label: 'Voy-voy Estimate',
                                                            modalKey: null,
                                                          },
                                                        ],
                                                      },
                                                      {
                                                        key: 'reports',
                                                        isDropdown: 0,
                                                        withText: 'Reports',
                                                        type: '',
                                                        menus: null,
                                                      },
                                                    ],
                                                  },
                                                ],
                                                isResetOption: false,
                                              },
                                            ]}
                                            isShowFixedColumn={[
                                              'Load Options',
                                              'Rev/Exp Info',
                                              'Broker',
                                              'Extra Freight Term',
                                              'Discharge Options',
                                              'Itinerary Options',
                                            ]}
                                            sideList={{
                                              showList: true,
                                              title: 'Corgo Cont. List',
                                              uri: '/cargo/list?l=0',
                                              columns: [
                                                'cargo_contract_id',
                                                'group_id',
                                                'cargo_status',
                                              ],
                                            }}
                                            staticTabs={{
                                              'Dem/Des Term': () => {
                                                return <DemdesTerm />;
                                              },
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </article>
                                  ) : (
                                    undefined
                                  )}
                                </Col>
                              </Row>
                            </div>
                          </div>
                        </article>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Content>
          </Layout>

          <RightBarUI
            pageTitle="add-contractform-righttoolbar"
            callback={data => this.onClickRightMenu(data)}
          />
          <Drawer
            title="Task"
            placement="right"
            closable={true}
            onClose={this.onCloseDrawer}
            open={this.state.visibleTask}
            getContainer={false}
            style={{ position: 'absolute' }}
            width={500}
            maskClosable={false}
          ></Drawer>
          <Drawer
            title="Revisions"
            placement="right"
            closable={true}
            onClose={this.onCloseRevision}
            open={this.state.visibleRevision}
            getContainer={false}
            style={{ position: 'absolute' }}
            width={500}
            maskClosable={false}
          ></Drawer>

          {isVisible === true ? (
            <Modal
              title="Create VC (purchase)"
              open={isVisible}
              width="95%"
              onCancel={() => this.toggleCargoRightMenu(false)}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto' }}
              footer={null}
            >
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    {
                      postFormData && postFormData.hasOwnProperty("params")
                        ? <CargoDetails
                          history={this.props.history}
                          match={postFormData}
                          isDisabled={true}
                          modalCloseEvent={(evt) => this.callbackReturn(evt)}
                        />
                        : <CargoDetails
                          history={this.props.history}
                          showSideListBar={false}
                          modalCloseEvent={(evt) => this.callbackReturn(evt)}
                          formData={postFormData}
                          isDisabled={true}
                        />
                    }
                  </div>
                </article>
              </div>
            </Modal>
          ) : (
            undefined
          )}

          <Modal
            title="Copy Setup"
            open={isVisibleCopy}
            width={1200}
            onCancel={() => this.onCloseModal(false)}
            footer={false}
          >
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  {doCopyContract ? (
                    <NormalFormIndex
                      key={'key_' + copyFrmName + '_0'}
                      formClass="label-min-height"
                      formData={copyFormData}
                      showForm={true}
                      frmCode={copyFrmName}
                      inlineLayout={true}
                      showButtons={[
                        {
                          id: 'create',
                          title: 'Copy Contract',
                          type: 'default',
                          event: (data, innerCB) => {
                            this.copyContract(data, innerCB);
                          },
                        },
                        { id: 'cancel', title: 'Reset', type: 'danger' },
                        {
                          id: 'save',
                          title: 'Save',
                          type: 'primary',
                          event: (data, innerCB) => {
                            this.updateCopyContarct(data, innerCB);
                          },
                        },
                      ]}
                      isShowFixedColumn={['-------', '--------']}
                    />
                  ) : (
                    <div className="col col-lg-12">
                      <Spin tip="Loading...">
                        <Alert message=" " description="Please wait..." type="info" />
                      </Spin>
                    </div>
                  )}
                </div>
              </div>
            </article>
          </Modal>
        </Layout>
      </div>
    );
  }
}

export default CargoContract;
