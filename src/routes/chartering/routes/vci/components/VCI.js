import React from 'react';
import { Form, Input, Select, Row, Tabs, Col, Button } from 'antd';
import Itinerary from './Itinerary';
import Pricing from './Pricing';
const Option = Select.Option;
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const InputGroup = Input.Group;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class VCI extends React.Component {
  render() {
    return (
      <>
        {/* Start: Body Section */}
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <Form>
                  <div className="form-wrapper">
                    <div className="form-heading">
                      <h4 className="title"><span>VCI</span></h4>
                    </div>
                    <div className="action-btn">
                      <Button type="primary" htmlType="submit">Save</Button>
                      <Button>Reset</Button>
                    </div>
                  </div>
                  <Row gutter={16}>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <FormItem
                        {...formItemLayout}
                        label="Cargo"
                      >
                        <Input placeholder="Cargo" id="Cargo" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="M3/MT, Ft3/MT"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} defaultValue="0.0000" />
                          <Input style={{ width: '50%' }} defaultValue="0.0000" />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Charterer"
                      >
                        <Input placeholder="Charterer" id="Charterer" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Company/LOB"
                      >
                        <Input placeholder="Company/LOB" id="Company/LOB" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Department"
                      >
                        <Input placeholder="Department" id="Department" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="CP Place"
                      >
                        <Input placeholder="CP Place" id="CP Place" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Laycan From"
                      >
                        <Input placeholder="Laycan From" id="Laycan From" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Laycan To"
                      >
                        <Input placeholder="Laycan To" id="Laycan To" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Trade Area"
                      >
                        <Input placeholder="Trade Area" id="Trade Area" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Vessel Type"
                      >
                        <Input placeholder="Vessel Type" id="Vessel Type" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Nominated Vessel"
                      >
                        <Input placeholder="Nominated Vessel" id="Nominated Vessel" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Contact Link"
                      >
                        <Input placeholder="Contact Link" id="Contact Link" />
                      </FormItem>
                    </Col>

                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <FormItem
                        {...formItemLayout}
                        label="CP Date/Form"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="" />
                          <Input style={{ width: '50%' }} placeholder="" />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="CP Qty/Unit"
                      >
                        <Input addonAfter="MT" placeholder="CP Qty/Unit" defaultValue="1" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Option %/Type"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="" />
                          <Input style={{ width: '50%' }} placeholder="" />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Min/Max Qty"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="" />
                          <Input style={{ width: '50%' }} placeholder="" />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Freight Type"
                      >
                        <Input placeholder="Freight Type" defaultValue="F" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Frt Rate"
                      >
                        <Input placeholder="Frt Rate" defaultValue="10,000" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Curr/Exch Rate"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="USD" />
                          <Input style={{ width: '50%' }} placeholder="1,000" />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Min Inv Qty"
                      >
                        <Input placeholder="0.00" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Freight Via"
                      >
                        <Input placeholder="" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Demurrage Via"
                      >
                        <Input placeholder="" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Conf Date (GMT)"
                      >
                        <Input placeholder="" disabled />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="CP Draft (m)"
                      >
                        <Input placeholder="" />
                      </FormItem>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <FormItem
                        {...formItemLayout}
                        label="Status"
                      >
                        <Select defaultValue="Inquiry">
                          <Option value="Inquiry">Inquiry</Option>
                        </Select>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Cargo COA"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="" />
                          <Input style={{ width: '50%' }} placeholder="" disabled />
                        </InputGroup>
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Coordinator"
                      >
                        <Input placeholder="" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Reference No."
                      >
                        <Input placeholder="" />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Benchmark Link ID"
                      >
                        <Input placeholder="" disabled />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="VC in ID"
                      >
                        <Input placeholder="" disabled />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Estimate"
                      >
                        <Input placeholder="" disabled />
                      </FormItem>
                      <FormItem
                        {...formItemLayout}
                        label="Voyage"
                      >
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} placeholder="" />
                          <Input style={{ width: '50%' }} placeholder="" disabled />
                        </InputGroup>
                      </FormItem>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </article>

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="row">
                  <div className="col-md-12">
                    <Tabs defaultActiveKey="1" size="small">
                      <TabPane tab="Itinerary/Opt.." key="Itinerary/Opt..">
                        <Itinerary />
                      </TabPane>
                      <TabPane tab="Pricing" key="Pricing">
                        <Pricing />
                      </TabPane>
                      <TabPane tab="Other Info" key="Other Info">
                        <Itinerary />
                      </TabPane>
                      <TabPane tab="Exposure" key="Exposure">
                        <Itinerary />
                      </TabPane>
                      <TabPane tab="Rev/Exp" key="Rev/Exp">
                        <Itinerary />
                      </TabPane>
                      <TabPane tab="Rebill Settings" key="Rebill Settings">
                        <Itinerary />
                      </TabPane>
                    </Tabs>
                  </div>
                </div>
              </div>
            </div>
          </article>

        </div>
        {/* End: Body Section */}
      </>

    )
  }
}

export default VCI;
