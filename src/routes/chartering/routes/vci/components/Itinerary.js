import React from 'react';
import { Table, Input,  Row, Popconfirm, Col, Button } from 'antd';
import { EditOutlined , CheckOutlined} from '@ant-design/icons';
class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    editable: false,
  }
  handleChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  }
  check = () => {
    this.setState({ editable: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  }
  edit = () => {
    this.setState({ editable: true });
  }
  render() {
    const { value, editable } = this.state;
    return (
      <div className="editable-cell">
        {
          editable ? (
            <Input
              value={value}
              onChange={this.handleChange}
              onPressEnter={this.check}
              suffix={
                <CheckOutlined 
                  className="editable-cell-icon-check"
                  onClick={this.check}
                />
              }
            />
          ) : (
            <div style={{ paddingRight: 24 }}>
              {value || ' '}
              <EditOutlined 

                className="editable-cell-icon"
                onClick={this.edit}
              />
            </div>
          )
        }
      </div>
    );
  }
}

class Itinerary extends React.Component {
  constructor(props) {
    super(props);
    this.columns2 = [{
      title: 'F',
      dataIndex: 'F',
      width: '30%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'F')}
        />
      ),
    }, {
      title: 'L D Rate',
      dataIndex: 'L D Rate',
      width: '10%',
    }, {
      title: 'Ru',
      dataIndex: 'Ru',
      width: '10%',
    }, {
      title: 'Terms',
      dataIndex: 'Terms',
      width: '10%',
    }, {
      title: 'Tt',
      dataIndex: 'Tt',
      width: '10%',
    }, {
      title: 'P Exp',
      dataIndex: 'P Exp',
      width: '10%',
    }, {
      title: 'D',
      dataIndex: 'D',
      width: '10%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource2.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.columns1 = [{
      title: 'Port/Area',
      dataIndex: 'Port/Area',
      width: '10%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'port_area')}
        />
      ),
    }, {
      title: 'Port',
      dataIndex: 'Port',
      width: '5%',
    }, {
      title: 'Berth',
      dataIndex: 'Berth',
      width: '5%',
    }, {
      title: 'Qty',
      dataIndex: 'Qty',
      width: '5%',
    }, {
      title: 'L/D Rate',
      dataIndex: 'L/D Rate',
      width: '5%',
    }, {
      title: 'RU',
      dataIndex: 'RU',
      width: '5%',
    }, {
      title: 'C',
      dataIndex: 'C',
      width: '5%',
    }, {
      title: 'Terms',
      dataIndex: 'Terms',
      width: '5%',
    }, {
      title: 'TT',
      dataIndex: 'TT',
      width: '5%',
    },
    {
      title: 'Curr',
      dataIndex: 'Curr',
      width: '5%',
    },
    {
      title: 'PortExp',
      dataIndex: 'PortExp',
      width: '8%',
    },
    {
      title: 'BaseExp',
      dataIndex: 'BaseExp',
      width: '5%',
    }, {
      title: 'Alt Qty',
      dataIndex: 'alt_qty',
      width: '8%',
    }, {
      title: 'Alt Unit',
      dataIndex: 'alt_unit',
      width: '8%',
    },
    {
      title: 'OExp$/t',
      dataIndex: 'oexp',
      width: '8%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource1.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.state = {
      editable: false,
      dataSource1: [],
      dataSource2: [],
      count: 1,
    };
  }

  edit = () => {
    this.setState({ editable: true });
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource1 = [...this.state.dataSource1];
      const target = dataSource1.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource1 });
      }
    };
  }

  onDelete = (key) => {
    const dataSource1 = [...this.state.dataSource1];
    this.setState({ dataSource1: dataSource1.filter(item => item.key !== key) });
  }

  handleAdd = () => {
    const { count, dataSource1 } = this.state;
    const newData = {
      key: count,
    };
    this.setState({
      dataSource1: [...dataSource1, newData],
      count: count + 1,
    });
  }

  render() {
    const { dataSource1 } = this.state;
    const { dataSource2 } = this.state;
    const columns1 = this.columns1;
    const columns2 = this.columns2;
    return (
      <>
        <div className="m-b-18">
          <Table bordered pagination={false} scroll={{ x: 1500, y: 300 }} size="small" dataSource={dataSource1} columns={columns1} footer={() =>
            <div className="text-center">
              <Button type="link" onClick={this.handleAdd}>Add New</Button>
            </div>
          } />
        </div>
        <Row gutter={16}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Table bordered pagination={false} size="small" dataSource={dataSource2} columns={columns2} footer={() =>
              <div className="text-center">
                <Button type="link" onClick={this.handleAdd}>Add New</Button>
              </div>
            }
              title={() => 'Load Options'} />
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Table bordered pagination={false} size="small" dataSource={dataSource2} columns={columns2} footer={() =>
              <div className="text-center">
                <Button type="link" onClick={this.handleAdd}>Add New</Button>
              </div>
            }
              title={() => 'Discharge Options'} />
          </Col>
        </Row>
      </>
    );
  }
}

export default Itinerary;