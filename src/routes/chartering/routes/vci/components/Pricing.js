import React from 'react';
import { Table, Input, Row, Popconfirm, Col, Button, Checkbox, Form } from 'antd';
import { CheckOutlined, EditOutlined } from '@ant-design/icons';
const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
    md: { span: 10 },
    lg: { span: 10 },
    xl: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
    md: { span: 14 },
    lg: { span: 14 },
    xl: { span: 14 },
  },
};

class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    editable: false,
  }
  handleChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  }
  check = () => {
    this.setState({ editable: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  }
  edit = () => {
    this.setState({ editable: true });
  }
  render() {
    const { value, editable } = this.state;
    return (
      <div className="editable-cell">
        {
          editable ? (
            <Input
              value={value}
              onChange={this.handleChange}
              onPressEnter={this.check}
              suffix={
                <CheckOutlined 
                  className="editable-cell-icon-check"
                  onClick={this.check}
                />
              }
            />
          ) : (
            <div style={{ paddingRight: 24 }}>
              {value || ' '}
              <EditOutlined 

                className="editable-cell-icon"
                onClick={this.edit}
              />
            </div>
          )
        }
      </div>
    );
  }
}

class Pricing extends React.Component {
  constructor(props) {
    super(props);
    this.columns1 = [{
      title: 'Load Port(s)',
      dataIndex: 'Load Port(s)',
      width: '20%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'Load Port(s)')}
        />
      ),
    }, {
      title: 'Discharge Port(s)',
      dataIndex: 'Discharge Port(s)',
      width: '10%',
    }, {
      title: 'Cargo',
      dataIndex: 'Cargo',
      width: '10%',
    }, {
      title: 'Frt Type',
      dataIndex: 'Frt Type',
      width: '10%',
    }, {
      title: 'Amount',
      dataIndex: 'Amount',
      width: '10%',
    }, {
      title: 'Table',
      dataIndex: 'Table',
      width: '10%',
    }, {
      title: 'Basis',
      dataIndex: 'Basis',
      width: '10%',
    }, {
      title: 'Xfrt',
      dataIndex: 'Xfrt',
      width: '10%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource1.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.columns2 = [{
      title: 'Broker',
      dataIndex: 'Broker',
      width: '30%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'Broker')}
        />
      ),
    }, {
      title: 'Amount',
      dataIndex: 'Amount',
      width: '10%',
    }, {
      title: 'T',
      dataIndex: 'T',
      width: '10%',
    }, {
      title: 'F',
      dataIndex: 'F',
      width: '10%',
    }, {
      title: 'D',
      dataIndex: 'D',
      width: '10%',
    }, {
      title: 'M',
      dataIndex: 'M',
      width: '10%',
    }, {
      title: 'P',
      dataIndex: 'P',
      width: '10%',
    }, {
      title: 'V',
      dataIndex: 'V',
      width: '10%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource2.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.columns3 = [{
      title: 'Code',
      dataIndex: 'Code',
      width: '30%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'Code')}
        />
      ),
    }, {
      title: 'Extra Freight Term',
      dataIndex: 'Extra Freight Term',
      width: '30%',
    }, {
      title: 'Rate/Lump',
      dataIndex: 'Rate/Lump',
      width: '30%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource3.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.state = {
      editable: false,
      dataSource1: [],
      dataSource2: [],
      dataSource3: [],
      count: 1,
    };
  }

  edit = () => {
    this.setState({ editable: true });
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource1 = [...this.state.dataSource1];
      const target = dataSource1.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource1 });
      }
    };
  }

  onDelete = (key) => {
    const dataSource1 = [...this.state.dataSource1];
    this.setState({ dataSource1: dataSource1.filter(item => item.key !== key) });
  }

  handleAdd = () => {
    const { count, dataSource1 } = this.state;
    const newData = {
      key: count,
    };
    this.setState({
      dataSource1: [...dataSource1, newData],
      count: count + 1,
    });
  }

  render() {
    const { dataSource1 } = this.state;
    const { dataSource2 } = this.state;
    const { dataSource3 } = this.state;
    const columns1 = this.columns1;
    const columns2 = this.columns2;
    const columns3 = this.columns3;
    return (
      <>
        <div className="m-b-18">
          <Table bordered pagination={false} scroll={{ x: 1500, y: 300 }} size="small" dataSource={dataSource1} columns={columns1} footer={() =>
            <div className="text-center">
              <Button type="link" onClick={this.handleAdd}>Add New</Button>
            </div>
          }
            title={() =>
              <div>
                <div className="area-space"><Checkbox>Use Pricing From COA</Checkbox></div>
                <div className="area-space"><Checkbox>Advanced</Checkbox></div>
                <div className="area-space">Top-off</div>
                <div className="area-space"><Input placeholder="Text" /></div>
                <div className="area-space"><Checkbox>Use Booking Qty on Scale Table</Checkbox></div>
              </div>} />
        </div>
        <div className="m-b-18">
          <Row gutter={16}>
            <Col xs={24} sm={24} md={24} lg={24} xl={12}>
              <Table bordered pagination={false} size="small" dataSource={dataSource2} columns={columns2} footer={() =>
                <div className="text-center">
                  <Button type="link" onClick={this.handleAdd}>Add New</Button>
                </div>
              } />
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={12}>
              <Col xs={17} sm={17} md={17} lg={17} xl={17}>
                <Table bordered pagination={false} size="small" dataSource={dataSource3} columns={columns3} footer={() =>
                  <div className="text-center">
                    <Button type="link" onClick={this.handleAdd}>Add New</Button>
                  </div>
                } />
              </Col>
              <Col xs={7} sm={7} md={7} lg={7} xl={7}>
                <div className="m-b-18"><Button type="primary">Update From Options</Button></div>
                <div><Button type="primary">Lift CP Qty & Draft</Button></div>
              </Col>
            </Col>
          </Row>
        </div>
        <div className="m-b-18">
          <Form>
            <Row gutter={16}>
              <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                <Col xs={14} sm={14} md={14} lg={14} xl={14}>
                  <FormItem
                    {...formItemLayout}
                    label="Freight Surcharge"
                  >
                    <Input placeholder="Freight Surcharge" defaultValue="none" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Bunker Surcharge"
                  >
                    <Input placeholder="Bunker Surcharge" defaultValue="none" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Table"
                  >
                    <Input placeholder="Table" defaultValue="" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="."
                    className="no-label"
                  >
                    <Checkbox value="Use BL Date">Use BL Date</Checkbox>
                  </FormItem>
                </Col>
                <Col xs={10} sm={10} md={10} lg={10} xl={10}>
                  <div className="arae-space"> <Button type="primary">Edit Table</Button></div>
                  <div className="arae-space"> <Button type="primary">Edit Table</Button></div>
                </Col>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                <FormItem
                  {...formItemLayout}
                  label="Dem Scale Table"
                >
                  <Input placeholder="Dem Scale Table" defaultValue="" />
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="Dem Curr/Exch Rate"
                >
                  <InputGroup compact>
                    <Input style={{ width: '50%' }} placeholder="USD" />
                    <Input style={{ width: '50%' }} placeholder="1.000000" />
                  </InputGroup>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="Load Dem/Des"
                >
                  <InputGroup compact>
                    <Input style={{ width: '50%' }} placeholder="0.00" />
                    <Input style={{ width: '50%' }} placeholder="0.00" />
                  </InputGroup>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="Disch Dem/Des"
                >
                  <InputGroup compact>
                    <Input style={{ width: '50%' }} placeholder="0.00" />
                    <Input style={{ width: '50%' }} placeholder="0.00" />
                  </InputGroup>
                </FormItem>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                <FormItem
                  {...formItemLayout}
                  label="Laytime Scale Table"
                >
                  <Input placeholder="Laytime Scale Table" defaultValue="" />
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="NOR Offset"
                >
                  <Input placeholder="NOR Offset" defaultValue="0.0000" disabled />
                  <Checkbox>Reversible</Checkbox>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="Laytime Allowed"
                >
                  <InputGroup compact>
                    <Input style={{ width: '33%' }} placeholder="0.00" disabled />
                    <Input style={{ width: '33%' }} placeholder="H" disabled />
                    <Input style={{ width: '33%' }} placeholder="" disabled />
                  </InputGroup>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="Dem TB Days"
                >
                  <InputGroup compact>
                    <Input style={{ width: '50%' }} placeholder="0.00" />
                    <Input style={{ width: '50%' }} placeholder="0.00" disabled />
                  </InputGroup>
                </FormItem>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <FormItem
                  label="Remarks"
                  labelCol={{ span: 24 }}
                  wrapperCol={{ span: 24 }}
                >
                  <TextArea
                    placeholder="Remarks"
                    autoSize={{ minRows: 3, maxRows: 3 }}
                  />
                </FormItem>
              </Col>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Row gutter={16}>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <FormItem
                      label="Remittance Bank"
                      labelCol={{ span: 10 }}
                      wrapperCol={{ span: 14 }}
                    >
                      <Input placeholder="" />
                    </FormItem>
                  </Col>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <FormItem
                      label="Invoice %"
                      labelCol={{ span: 10 }}
                      wrapperCol={{ span: 14 }}
                    >
                      <Input placeholder="" defaultValue="0.00" />
                    </FormItem>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <FormItem
                      label="Payment Terms"
                      labelCol={{ span: 5 }}
                      wrapperCol={{ span: 19 }}
                    >
                      <InputGroup compact>
                        <Input style={{ width: '50%' }} defaultValue="" />
                        <Input style={{ width: '50%' }} placeholder="" />
                      </InputGroup>
                    </FormItem>
                  </Col>
                </Row>
                <Row type="flex" justify="end">
                  <FormItem
                    label="Balance %"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                  >
                    <Input placeholder="" defaultValue="100.00" />
                  </FormItem>
                </Row>
              </Col>
            </Row>
          </Form>
        </div>
      </>
    );
  }
}

export default Pricing;