import React, { Component } from 'react';
import { Table} from 'antd';

const columns = [
  {
    title: 'TCI ID',
    dataIndex: 'tci_id',
  },

  {
    title: 'Vessel Name',
    dataIndex: 'vessel_name',
  },

  {
    title: 'Type',
    dataIndex: 'type',
  },

  {
    title: 'DWT',
    dataIndex: 'dwt',
  },

  {
    title: 'Status',
    dataIndex: 'status',
  },
];
const data = [
  {
    key: '1',
    tci_id: 'TCI ID',
    vessel_name: 'Vessel Name',
    type: 'Type',
    dwt: 'DWT',
    status: 'Status',
  },
  {
    key: '2',
    tci_id: 'TCI ID',
    vessel_name: 'Vessel Name',
    type: 'Type',
    dwt: 'DWT',
    status: 'Status',
  },
  {
    key: '3',
    tci_id: 'TCI ID',
    vessel_name: 'Vessel Name',
    type: 'Type',
    dwt: 'DWT',
    status: 'Status',
  },
];

class TciSearch extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default TciSearch;
