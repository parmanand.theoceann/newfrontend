import React, { Component } from 'react';
import { Table} from 'antd';

const columns = [
  {
    title: 'Cargo name',
    dataIndex: 'cargo_name',
  },

  {
    title: 'Description',
    dataIndex: 'description',
  },

];
const data = [
  {
    key: '1',
    cargo_name: 'Cargo Name',
    description: 'Description',
  },
  {
    key: '2',
    cargo_name: 'Cargo Name',
    description: 'Description',
  },
  {
    key: '3',
    cargo_name: 'Cargo Name',
    description: 'Description',
  },
];

class CargoSearch extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default CargoSearch;
