import React, { Component } from 'react';
import { Table} from 'antd';

const columns = [
  {
    title: 'Vessel Name',
    dataIndex: 'vessel_name',
  },

  {
    title: 'DWT',
    dataIndex: 'dwt',
  },

  {
    title: 'Type',
    dataIndex: 'type',
  },

  {
    title: 'TCOV ID',
    dataIndex: 'tcov_id',
  },

  {
    title: 'Voyage Status',
    dataIndex: 'voyage_status',
  },

  {
    title: 'Status',
    dataIndex: 'status',
  },
];
const data = [
  {
    key: '1',
    vessel_name: 'Vessel Name',
    dwt: 'DWT',
    type: 'Type',
    tcov_id: 'TCOV ID',
    voyage_status: 'Voy Status',
    status: 'Status',
  },
  {
    key: '2',
    vessel_name: 'Vessel Name',
    dwt: 'DWT',
    type: 'Type',
    tcov_id: 'TCOV ID',
    voyage_status: 'Voy Status',
    status: 'Status',
  },
  {
    key: '3',
    vessel_name: 'Vessel Name',
    dwt: 'DWT',
    type: 'Type',
    tcov_id: 'TCOV ID',
    voyage_status: 'Voy Status',
    status: 'Status',
  },
];

class TcovSearch extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default TcovSearch;
