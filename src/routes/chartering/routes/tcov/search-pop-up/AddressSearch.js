import React, { Component } from 'react';
import { Table} from 'antd';

const columns = [
  {
    title: 'Short name',
    dataIndex: 'short_name',
  },

  {
    title: 'Full name',
    dataIndex: 'full_name',
  },

  {
    title: 'Type  Bank account',
    dataIndex: 'type_bank',
  },

  {
    title: 'Country’s',
    dataIndex: 'country',
  },

];
const data = [
  {
    key: '1',
    short_name: 'Short Name',
    full_name: 'Full Name',
    type_bank: 'Bank Account',
    country: 'Country',
  },
  {
    key: '2',
    short_name: 'Short Name',
    full_name: 'Full Name',
    type_bank: 'Bank Account',
    country: 'Country',
  },
  {
    key: '3',
    short_name: 'Short Name',
    full_name: 'Full Name',
    type_bank: 'Bank Account',
    country: 'Country',
  },
];

class AddressSearch extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default AddressSearch;
