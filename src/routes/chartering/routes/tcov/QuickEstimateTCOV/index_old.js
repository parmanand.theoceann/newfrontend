import React, { Component } from "react";
//import { withRouter } from "react-router-dom";
import { Button, Layout,  Drawer, Row, Col, Tooltip, Modal } from "antd";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  withLocation,
  // awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  getAPICall,
  apiDeleteCall,
} from "../../../../../shared";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../../shared/attachments";
import RightBarUI from "../../../../../components/RightBarUI";
import RightSearch from "../../tcto/right-panel/RightSearch";
import Properties from "../Properties";
import PL from "../../../../../shared/components/PL";
import EstimateSummary from "../../tcto/right-panel/EstimateSummary";
import TcovPortInformation from "../../../../right-port-info/TcovPortInformation";
import AttachmentFile from "../../../../../shared/components/Attachment";

const { Content } = Layout;

class QuickEstimateTCOV extends Component {
  constructor(props) {
        super(props);
    this.state = {
      visible: false,
      showArray: [],
      formDataArrays: [],
      showCollapse: [true],
      frmName: "tcov_form",
      isEdit: false,
      formDataValues: this.props.formData || {},
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      fullToQuick: false,
      refreshForm: false,
      showAddButton: true,
      estimateID: null,
    };

    this.formDataref = React.createRef();
    this.formDataref.current = this.props.formData || {};
  }

  componentDidMount = async () => {
    const { params,location} =this.props?.router;
    let estimateID=params.id
    let respData = undefined,
      response = undefined;
    if (
      (params?.id && location?.pathname === "/edit-quick-estimate/:id") ||
      estimateID
    ) {
      let qParams = { ae: params.id || estimateID };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(
        `${URL_WITH_VERSION}/tcov/quick-estimate/edit?${qParamString}`
      );
      respData = await response["data"];
      if (
        respData &&
        respData["cargodetails"] &&
        respData["cargodetails"].hasOwnProperty("f_type") &&
        respData["cargodetails"]["f_type"] === "104"
      ) {
        respData["cargodetails"]["disablefield"] = ["quantity"];
      }
      this.formDataref.current = respData;
      this.setState(
        {
          ...this.state,
          formDataValues: respData,
          fullToQuick: false,
          showAddButton: false,
          formDataArrays: [...this.state.formDataArrays, ...[respData]],
          estimateID: params.id,
        },
        () => this.onAddMore("0", false, true)
      );
    } else {
      this.onAddMore();
    }
  };

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  onClickRightMenu = async (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "search":
        loadComponent = <RightSearch history={this?.props?.router?.navigate} />;
        break;
      case "properties":
        loadComponent = <Properties />;
        break;
      case "pl":
        loadComponent = <PL />;
        break;
      case "summary":
        loadComponent = <EstimateSummary />;
        break;
      case "portinformation":
        loadComponent = <TcovPortInformation />;
        break;
      case "attachment":
        openNotificationWithIcon("info", "Attachments are not allowed here.");
        // const { estimateID } = this.state;
        // if (estimateID) {
        //   const attachments = await getAttachments(estimateID, "EST");
        //   const callback = (fileArr) =>
        //     uploadAttachment(fileArr, estimateID, "EST", "tcov");
        //   loadComponent = (
        //     <AttachmentFile
        //       uploadType="Estimates"
        //       attachments={attachments}
        //       onCloseUploadFileArray={callback}
        //       deleteAttachment={(file) =>
        //         deleteAttachment(file.url, file.name, "EST", "tcov")
        //       }
        //       tableId={0}
        //     />
        //   );
        // } else {
        //   openNotificationWithIcon("info", "Attachments are not allowed here.");
        // }
        break;
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };

  viewFullEstimate = (index = "") => {
    const { formDataValues, formDataArrays } = this.state;
    const {navigate} = this.props.router
    if (
      formDataArrays &&
      formDataArrays[index] &&
      formDataArrays[index].hasOwnProperty("tcov_id")
    ) {
      navigate(
        `/edit-voyage-estimate/${formDataArrays[index]["tcov_id"]}`
      );
    } else if (
      formDataArrays[index] ||
      (formDataValues &&
        formDataValues.hasOwnProperty("id") &&
        formDataValues["id"] > 0 &&
        !formDataValues.hasOwnProperty("tcov_id"))
    ) {
      navigate(
         `/edit-voyage-estimate/${
          formDataArrays[index]["estimate_id"]
        }`,
        {state: formDataArrays[index]},
        // state:formDataValues,
      );
    } else {
      openNotificationWithIcon("error", "Quick estimate to be generated first");
    }
  };

  _loadVoyageResultDetails = (_formData, formDataValues) => {
    let dollarUSLocale = Intl.NumberFormat("en-US", {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
    let grossRevenue = 0,
      netRevenue = 0,
      demmurage = 0,
      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      cpQty = 0,
      fr = 0,
      mr = 0,
      comm = 0,
      tpd = 0,
      pi = 0;
    let tsd = formDataValues["---------"]["tsd"];
    let hire = formDataValues["tci_d_hire"]
      ? (formDataValues["tci_d_hire"] + "").replaceAll(",", "") * 1
      : 0;
    let portDateDetails = formDataValues["portdatedetails"];
    let portitinerary = formDataValues["portitinerary"];
    let cargos = formDataValues["cargos"];
    let addPercentage = formDataValues["add_percentage"]
      ? (formDataValues["add_percentage"] + "").replaceAll(",", "") * 1
      : 0;
    let broPercentage = formDataValues["bro_percentage"]
      ? (formDataValues["bro_percentage"] + "").replaceAll(",", "") * 1
      : 0;

    if (portitinerary) {
      portitinerary.map((e) => (pi += (e.p_exp + "").replaceAll(",", "") * 1));
    }

    if (portDateDetails) {
      portDateDetails.map(
        (e) => (tpd += (e.pdays + "").replaceAll(",", "") * 1)
      );
    }

    if (cargos) {
      cargos.map((e) => {
        mr += e.extra_rev ? (e.extra_rev + "").replaceAll(",", "") * 1 : 0;
        if (parseFloat(e.option_percentage) !== 0) {
          let frtRate =
            ((e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) +
              ((e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) *
                (e.option_percentage
                  ? (e.option_percentage + "").replaceAll(",", "")
                  : 0)) /
                100) *
            (e.freight_rate
              ? (e.freight_rate + "").replaceAll(",", "") * 1
              : 0);
          let commission =
            (frtRate *
              1 *
              (e.comm_per ? (e.comm_per + "").replaceAll(",", "") * 1 : 0)) /
            100;
          fr += frtRate;
          comm += commission;
          cpQty +=
            (e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) +
            (e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0);
        } else {
          let frtRate =
            (e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (e.freight_rate
              ? (e.freight_rate + "").replaceAll(",", "") * 1
              : 0);
          let commission =
            (frtRate *
              1 *
              (e.comm_per ? (e.comm_per + "").replaceAll(",", "") * 1 : 0)) /
            100;
          fr += frtRate;
          comm += commission;
          cpQty += e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0;
        }
      });
    }

    grossRevenue = fr + mr + demmurage;
    netRevenue =
      grossRevenue - (freightCommission + demmurageCommission + dispatch);
    let totalExpenses =
      (tpd + (tsd ? tsd * 1 : 0)) * hire +
      (formDataValues["bb"]
        ? ("" + formDataValues["bb"]).replaceAll(",", "") * 1
        : 0) +
      pi +
      (formDataValues["mis_cost"]
        ? ("" + formDataValues["mis_cost"]).replaceAll(",", "") * 1
        : 0) +
      ((addPercentage * hire) / 100) * -1 +
      ((broPercentage * hire) / 100) * -1 +
      0;
    let itemValue = netRevenue - totalExpenses;

    _formData["voyageresults"]["total_expense"] = dollarUSLocale.format(
      totalExpenses
    );
    _formData["voyageresults"]["g_income"] = dollarUSLocale.format(
      grossRevenue.toFixed(2)
    );
    _formData["voyageresults"]["n_income"] = dollarUSLocale.format(
      netRevenue.toFixed(2)
    );
    _formData["voyageresults"]["total_profit"] = dollarUSLocale.format(
      itemValue >= 0
        ? itemValue.toFixed(2)
        : "(" + (itemValue * -1).toFixed(2) + ")"
    );
    _formData["voyageresults"]["daily_profit"] = dollarUSLocale.format(
      (itemValue / (tpd * 1 + (tsd ? tsd * 1 : 0) - 0)).toFixed(2)
    );
    _formData["voyageresults"]["tco_equi"] = dollarUSLocale.format(
      itemValue -
        (totalExpenses -
          ((formDataValues["bb"]
            ? ("" + formDataValues["bb"]).replaceAll(",", "") * 1
            : 0) *
            1 +
            pi +
            0 +
            (formDataValues["mis_cost"]
              ? ("" + formDataValues["mis_cost"]).replaceAll(",", "") * 1
              : 0) +
            ((addPercentage * hire) / 100) * -1)) /
          (_formData["voyageresults"]["total_profit"] / (tpd * 1 + tsd * 1 - 0))
    );
    _formData["voyageresults"]["breakeven"] = dollarUSLocale.format(
      totalExpenses / cpQty >= 0
        ? (totalExpenses / cpQty).toFixed(2)
        : "(" + ((totalExpenses / cpQty) * -1).toFixed(2) + ")"
    ); // Profit is 0 since we are doing Breakeven
    _formData["voyageresults"]["voyage_commence"] =
      formDataValues["commence_date"];
    _formData["voyageresults"]["voyage_complete"] =
      formDataValues["completing_date"];
    _formData["voyageresults"]["total_voyage_days"] =
      formDataValues["total_days"];
    _formData["voyageresults"]["last_update_gmt"] =
      formDataValues["modified_on"] || formDataValues["created_on"];

    return _formData;
  };

  onAddMore = async (index = "", editValue = false, editing = false) => {
    const {
      showArray,
      showCollapse,
      formDataValues,
      fullToQuick,
      rightBtnPanel,
    } = this.state;

    let _formData = Object.assign({}, {});
    if (editValue || editing == true) {
      _formData = Object.assign({}, formDataValues);
    }

    //TODO START tech@theoceann.com
    // if (
    //   fullToQuick &&
    //   formDataValues &&
    //   formDataValues.hasOwnProperty("id") &&
    //   formDataValues["id"] > 0
    // ) {
    //   _formData = {
    //     estimate_id: formDataValues["estimate_id"],
    //     vesseldetails: {},
    //     cargodetails: {},
    //     voyageresults: {},
    //   };
    //   _formData["vesseldetails"]["vessel_id"] = formDataValues["vessel_id"];
    //   _formData["vesseldetails"]["dwt"] = formDataValues["dwt"];
    //   _formData["vesseldetails"]["tci_d_hire"] = formDataValues["tci_d_hire"];
    //   _formData["vesseldetails"]["add_percentage"] =
    //     formDataValues["add_percentage"];
    //   _formData["vesseldetails"]["bro_percentage"] =
    //     formDataValues["bro_percentage"];
    //   _formData["vesseldetails"]["bb"] = formDataValues["bb"];
    //   _formData["vesseldetails"]["other_cost"] = formDataValues["mis_cost"];
    //   _formData["vesseldetails"]["ballast_port"] = formDataValues["b_port"];
    //   _formData["vesseldetails"]["reposition_port"] =
    //     formDataValues["repos_port"];
    //   _formData["vesseldetails"]["repos_days"] = formDataValues["repso_days"];

    //   if (formDataValues.hasOwnProperty("-") && formDataValues["-"][1]) {
    //     _formData["vesseldetails"]["ballast_spd"] =
    //       formDataValues["-"][1]["ballast_spd"];
    //     _formData["vesseldetails"]["laden_spd"] =
    //       formDataValues["-"][1]["laden_spd"];
    //   }

    //   // cargo's details
    //   if (
    //     formDataValues.hasOwnProperty("cargos") &&
    //     formDataValues["cargos"].length > 0
    //   ) {
    //     _formData["cargodetails"]["cargo_name"] =
    //       formDataValues["cargos"][0]["cargo"];
    //     _formData["cargodetails"]["quantity"] =
    //       formDataValues["cargos"][0]["cp_qty"];
    //     _formData["cargodetails"]["unit"] = 2; // formDataValues['cargos'][0]['cargo_unit']
    //     if (
    //       formDataValues.hasOwnProperty("portitinerary") &&
    //       formDataValues["portitinerary"].length > 0
    //     ) {
    //       formDataValues["portitinerary"].map((e) => {
    //         if (
    //           parseInt(e["funct"]) === 1 &&
    //           !_formData["cargodetails"].hasOwnProperty("l_port")
    //         ) {
    //           _formData["cargodetails"]["l_port"] = e["port"];
    //         } else if (
    //           parseInt(e["funct"]) === 2 &&
    //           !_formData["cargodetails"].hasOwnProperty("d_port")
    //         ) {
    //           _formData["cargodetails"]["d_port"] = e["port"];
    //         }
    //       });
    //     }
    //     _formData["cargodetails"]["f_type"] =
    //       formDataValues["cargos"][0]["freight_type"];
    //     _formData["cargodetails"]["f_rate"] =
    //       formDataValues["cargos"][0]["freight_rate"];
    //     _formData["cargodetails"]["commission"] =
    //       formDataValues["cargos"][0]["comm_per"];
    //     _formData["cargodetails"]["other_revenue"] =
    //       formDataValues["cargos"][0]["extra_rev"];
    //   }

    //   _formData = this._loadVoyageResultDetails(_formData, formDataValues);
    // }
    //TODO END tech@theoceann.com

    if (editValue === true && editing === false) {
      showArray[index] = [];
    }
    // _formData = this._loadVoyageResultDetails(_formData, formDataValues);
    this.setState(
      {
        ...this.state,
        formDataValues: _formData,
        isEdit:
          formDataValues &&
          formDataValues.hasOwnProperty("id") &&
          formDataValues["id"] > 0,
        rightBtnPanel:
          _formData && _formData.hasOwnProperty("id") && _formData["id"] > 0
            ? rightBtnPanel
            : [
                {
                  key: "s2",
                  isSets: [
                    {
                      id: "2",
                      key: "menu",
                      isDropdown: 1,
                      type: "small-dash",
                      withText: "Menu",
                      showButton: true,
                      showToolTip: true,
                      // menus: null,
                      menus: [
                        {
                          id: "21",
                          key: "menu-1",
                          href: null,
                          icon: null,
                          label: "Convert to Full Estimate",
                          modalKey: null,
                        },

                        // {
                        //   id: "22",
                        //   key: "menu-2",
                        //   href: null,
                        //   icon: null,
                        //   label: "Go to Quick TCTO",
                        //   modalKey: null,
                        // },

                        // {
                        //   id: "23",
                        //   key: "menu-3",
                        //   href: null,
                        //   icon: null,
                        //   label: "Go Quick Voy relet",
                        //   modalKey: null,
                        //   event: () => this.viewFullEstimate(index), //this.props.router.navigate('/voy-relet-full-estimate')
                        // },

                        {
                          id: "24",
                          key: "menu-4",
                          href: null,
                          icon: null,
                          label: "Bunker-Live",
                          modalKey: null,
                        },

                        {
                          id: "25",
                          key: "menu-5",
                          href: null,
                          icon: null,
                          label: "Route to Map",
                          modalKey: null,
                        },

                        {
                          id: "26",
                          key: "menu-6",
                          href: null,
                          icon: null,
                          label: "Voyage optimize",
                          modalKey: null,
                        },
                      ],
                    },
                  ],
                },
              ],
      },
      () => {
        if (editValue === true) {
          showArray[index] = this.getContent(index, true);
        } else {
          showArray.push(this.getContent(showArray.length, false));
        }

        showCollapse[showArray.length] = true;
        this.setState({
          ...this.state,
          showArray: showArray,
          showCollapse,
        });
      }
    );
  };


  onLessMore = (index) => {
    const { showArray } = this.state;
  
    if (index > 0 && index < showArray.length) {
      showArray[index] = undefined;

      this.setState({ showArray: [...showArray] });
    }
  };
  

  onToggle = (i) => {
    this.state.showCollapse[i] = !this.state.showCollapse[i];
    this.setState(
      { ...this.state, showCollapse: this.state.showCollapse },
      () => {
        this.state.showArray[i] = this.getContent(i);
        this.setState({ ...this.state, showArray: this.state.showArray });
      }
    );
  };

  onSaveFormData = async (data, index) => {
    let _url = `${URL_WITH_VERSION}/tcov/save?frm=tcov_form`;
    let method = "POST";
    if (data && data.hasOwnProperty("id")) {
      _url = `${URL_WITH_VERSION}/tcov/update?frm=tcov_form`;
      method = "PUT";
    }
    if (data) {
      this.setState({
        ...this.state,
        refreshForm: true,
      });
      delete data["vesseldetails"]["vessel_code"];
      data["vesseldetails"]["tci_d_hire"] =
        data["vesseldetails"]["tci_d_hire"] &&
        data["vesseldetails"]["tci_d_hire"] != ""
          ? data["vesseldetails"]["tci_d_hire"]
          : "0.00";
      this.formDataref.current = { ...data };

      await postAPICall(`${_url}`, data, method, async (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
        } else {
          openNotificationWithIcon("error", response.message);
        }

        if (data && response.data) {
          let estimateID =
            (response && response.row && response.row.estimateID) ||
            data["estimate_id"];
          let qParams = { ae: estimateID };
          let qParamString = objectToQueryStringFunc(qParams);
          let response1 = await getAPICall(
            `${URL_WITH_VERSION}/tcov/quick-estimate/edit?${qParamString}`
          );
          let respData = await response1["data"];

          // if (
          //   respData &&
          //   respData["cargodetails"] &&
          //   respData["cargodetails"].hasOwnProperty("f_type") &&
          //   respData["cargodetails"]["f_type"] === "104"
          // ) {
          //   respData["cargodetails"]["disablefield"] = ["quantity"];
          // }

          this.formDataref.current = respData;
          this.setState(
            {
              ...this.state,
              isEdit: false,
              formDataValues: { ...respData },
              refreshForm: false,
              showAddButton: true,
              // showArray:[]
              formDataArrays: [...this.state.formDataArrays, ...[respData]],
            },
            () => this.onAddMore(index, true)
          );
        } else {
          this.setState({ ...this.state, isEdit: false, refreshForm: false });
        }
      });
    }
  };

  copyQuickEstimate = (data, index, editValue = false, editing = false) => {
    const {
      showArray,
      showCollapse,
      formDataValues,
      fullToQuick,
      rightBtnPanel,
    } = this.state;

    delete data["estimate_id"];
    delete data["id"];
    let _formData = Object.assign({}, data);
    if (editValue || editing == true) {
      _formData = Object.assign({}, formDataValues);
    }

    if (editValue === true && editing === false) {
      showArray[index] = [];
    }
    // _formData = this._loadVoyageResultDetails(_formData, formDataValues);
    this.formDataref.current = _formData;
    this.setState(
      {
        ...this.state,
        formDataValues: _formData,
        isEdit:
          formDataValues &&
          formDataValues.hasOwnProperty("id") &&
          formDataValues["id"] > 0,
        rightBtnPanel:
          _formData && _formData.hasOwnProperty("id") && _formData["id"] > 0
            ? rightBtnPanel
            : [
                {
                  key: "s2",
                  isSets: [
                    {
                      id: "2",
                      key: "menu",
                      isDropdown: 1,
                      type: "small-dash",
                      withText: "Menu",
                      showButton: true,
                      showToolTip: true,
                      // menus: null,
                      menus: [
                        {
                          id: "21",
                          key: "menu-1",
                          href: null,
                          icon: null,
                          label: "Convert to Full Estimate",
                          modalKey: null,
                        },
                        {
                          id: "24",
                          key: "menu-4",
                          href: null,
                          icon: null,
                          label: "Bunker-Live",
                          modalKey: null,
                        },

                        {
                          id: "25",
                          key: "menu-5",
                          href: null,
                          icon: null,
                          label: "Route to Map",
                          modalKey: null,
                        },

                        {
                          id: "26",
                          key: "menu-6",
                          href: null,
                          icon: null,
                          label: "Voyage optimized",
                          modalKey: null,
                        },
                      ],
                    },
                  ],
                },
              ],
      },
      () => {
        if (editValue === true) {
          showArray[index] = this.getContent(index, true);
        } else {
          showArray.push(this.getContent(showArray.length, false));
        }

        showCollapse[showArray.length] = true;
        this.setState({
          ...this.state,
          showArray: showArray,
          showCollapse,
        });
      }
    );
  };

  deleteData = (id, tcov_id, innerCB, index) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/tcov/delete`,
      { id: id, tcov_id: tcov_id },
      (resData) => {
        if (resData.data === true) {
          openNotificationWithIcon("success", resData.message);
          if (index > 0) {
            this.onLessMore(index);
          } else {
            this?.props?.router?.navigate("/quick-estimate-list");
          }
        } else {
          openNotificationWithIcon("error", resData.message);
        }
      }
    );
  };

  getContent(index, edit = false) {
    const {
      showCollapse,
      frmName,
      gID,
      formDataValues,
      isEdit,
      rightBtnPanel,
      formDataArrays,
    } = this.state;
    return (
      <>
        {!showCollapse[index] ? (
          <div className="all-sections wrap-small-collapse">
            <Row gutter={16}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="toolbar-ui-wrapper">
                  <div className="action-btn">
                    <Tooltip title="Expand">
                      <Button
                        type="dashed"
                        shape="circle"
                        icon="right"
                        size={"default"}
                        onClick={() => this.onToggle(index)}
                      />
                    </Tooltip>
                  </div>
                </div>
              </Col>
            </Row>
            <hr />

            <div className="body-se">
              <h1 class="main-title main-text">
                {formDataValues && formDataValues.hasOwnProperty("estimate_id")
                  ? formDataValues["estimate_id"]
                  : "EST-TCOV-0000" + index}
              </h1>
            </div>
          </div>
        ) : (
          <div className="all-sections">
            {this.state.refreshForm === false ? (
              <NormalFormIndex
                key={"key_" + frmName + index}
                formClass="label-min-height"
                // formData={formDataValues}
                formData={Object.assign({}, this.formDataref.current)}
                showForm={true}
                frmCode={frmName}
                addForm={true}
                showButtons={[
                  { id: "cancel", title: "Reset", type: "danger" },
                  {
                    id: "save",
                    title: "Save",
                    type: "primary",
                    event: (data) => {
                      this.onSaveFormData(data, index);
                    },
                  },
                ]}
                formDevideInCols="1"
                inlineLayout={true}
                showToolbar={[
                  {
                    isLeftBtn: [
                      {
                        key: "s1",
                        isSets: [
                          {
                            id: "1",
                            key: "save",
                            type: "save",
                            withText: "Save",
                            showButton: true,
                            showToolTip: true,
                            event: (key, data) => {
                              this.onSaveFormData(data, index);
                            },
                          },

                          {
                            id: "2",
                            key: "delete",
                            type: "delete",
                            withText: "Delete",
                            showToolTip: true,
                            event: (key, saveData, innerCB) => {
                              if (saveData["id"] && saveData["id"] > 0) {
                                Modal.confirm({
                                  title: "Confirm",
                                  content:
                                    "Are you sure, you want to delete it?",
                                  onOk: () =>
                                    this.deleteData(
                                      saveData["id"],
                                      saveData["tcov_id"],
                                      innerCB,
                                      index
                                    ),
                                });
                              }
                            },
                          },

                          {
                            id: "3",
                            key: "close",
                            type: "close",
                            withText: "Close",
                            showButton: true,
                            showToolTip: true,
                            event: (key) => {
                              this.onLessMore(index);
                            },
                          },

                          // {
                          //   id: '7',
                          //   key: 'collapse',
                          //   type: 'left',
                          //   withText: 'Collapse',
                          //   showButton: true,
                          //   showToolTip: true,
                          //   event: key => {
                          //     this.onToggle(index);
                          //   },
                          // },
                        ],
                      },
                    ],
                    isRightBtn: [
                      {
                        key: "s2",
                        isSets: [
                          {
                            id: "3",
                            key: "fullest",
                            isDropdown: 0,
                            type: "small-dash",
                            withText: "Full Estimate",
                            showButton: true,
                            showToolTip: true,
                            event: () => this.viewFullEstimate(index),
                          },

                          {
                            id: "2",
                            key: "menu",
                            isDropdown: 1,
                            type: "small-dash",
                            withText: "Menu",
                            showButton: true,
                            showToolTip: true,
                            menus: [
                              {
                                id: "24",
                                key: "menu-4",
                                href: null,
                                icon: null,
                                label: "Bunker-Live",
                                modalKey: null,
                              },

                              {
                                id: "25",
                                key: "menu-5",
                                href: null,
                                icon: null,
                                label: "Route to Map",
                                modalKey: null,
                              },

                              {
                                id: "26",
                                key: "menu-6",
                                href: null,
                                icon: null,
                                label: "Voyage optimize",
                                modalKey: null,
                              },

                              {
                                id: "27",
                                key: "menu-7",
                                href: null,
                                icon: null,
                                label: "Copy Estimate",
                                modalKey: null,

                                event: (key, data) => {
                                  let data1 = Object.assign({}, data);

                                  data1 &&
                                  data1.hasOwnProperty("id") &&
                                  data1["id"] > 0
                                    ? this.copyQuickEstimate(data1, index)
                                    : openNotificationWithIcon(
                                        "error",
                                        "Please save Quick Estimate first.",
                                        5
                                      );
                                },
                              },
                            ],
                          },
                          // {
                          //   id: '1',
                          //   key: 'createtcov',
                          //   isDropdown: 0,
                          //   withText: 'Full TCOV',
                          //   type: '',
                          //   menus: null,
                          //   event: (key, data) => this?.props?.router?.navigate(`/edit-voy-est/${data.estimate_id}`),
                          // },
                        ],
                      },
                    ],
                    isResetOption: false,
                  },
                ]}
              />
            ) : (
              undefined
            )}
          </div>
        )}
      </>
    );
  }





  
  // viewFullEstimate = (bolVal, estimateID) =>{
  //   const {formDataValues}=this.state
  //   if(formDataValues && formDataValues.hasOwnProperty('id') && formDataValues['id']>0){
  //     this?.props?.router?.navigate({
  //       pathname: `/add-tcov`,
  //       state: formDataValues
  //      });
  //   }
  //   else{
  //     openNotificationWithIcon('error',"Quick estimate to be generated first");
  //   }

  // }

  render() {
    const {
      loadComponent,
      title,
      visibleDrawer,
      showArray,
      showAddButton,
    } = this.state;
    return (
      <div className="tcov-wrapper scroll-borad">
        <Layout className="layout-wrapper">
          <Layout style={{ border: "none" }}>
            <Content className="content-wrapper">
              <div className="wrap-scrumboard-design">
                {showArray.map((field) => {
                  return <> {field} </>;
                })}

                {/* {showAddButton === true ? ( */}

                {/* 
                
                commented because this feature is creatting issues.
                <div className="wrap-action-plus">
                  <div className="fieldscroll-wrap add-section-wrapper">
                    <Tooltip title="Add Estimates" onClick={this.onAddMore}>
                      <Icon type="plus" />
                    </Tooltip>
                  </div>
                </div> */}

                {/* ) : (
                  undefined
                )} */}
              </div>
            </Content>
          </Layout>
          <RightBarUI
            pageTitle="tcov-quick-righttoolbar"
            callback={(data, options) => this.onClickRightMenu(data, options)}
          />
          {loadComponent !== undefined &&
          title !== undefined &&
          visibleDrawer === true ? (
            <Drawer
              title={this.state.title}
              placement="right"
              closable={true}
              onClose={this.onCloseDrawer}
              open={this.state.visibleDrawer}
              getContainer={false}
              style={{ position: "absolute" }}
              width={this.state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  <div className="content-wrapper noHeight">
                    {this.state.loadComponent}
                  </div>
                </div>
              </div>
            </Drawer>
          ) : (
            undefined
          )}
        </Layout>
      </div>
    );
  }
}



// export default withRouter(QuickEstimateTCOV);
export default withLocation(QuickEstimateTCOV);
