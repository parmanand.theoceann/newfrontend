import React, { Component } from 'react';
import { Table, Form, Input, Select, Switch, Button, Tabs } from 'antd';
import BunkerDetails from '../Itinerary/BunkerDetails';
import PortDateDetails from '../Itinerary/PortDate';
import PortItinerary from '../Itinerary/PortItinerary';
import EcoSpeed from './EcoSpeed';
import Cargoes from '../cargoes';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const { Column, ColumnGroup } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const columns = [
  {
    title: 'Fuel',
    dataIndex: 'fuel',
  },

  {
    title: 'Unit',
    dataIndex: 'unit',
  },

  {
    title: 'Price',
    dataIndex: 'price',
  },

  {
    title: 'Ballast Cons',
    dataIndex: 'ballast_cons',
  },

  {
    title: 'Laden Cons',
    dataIndex: 'laden_cons',
  },

  {
    title: 'Load Cons',
    dataIndex: 'load_cons',
  },

  {
    title: 'Disch Cons',
    dataIndex: 'disch_cons',
  },

  {
    title: 'Idle Cons',
    dataIndex: 'idle_cons',
  },
];
const data = [
  {
    key: '1',
    fuel: 'HFO',
    unit: 'MT',
    price: '100.00',
    ballast_cons: '33.60',
    laden_cons: '33.60',
    load_cons: '5.00',
    disch_cons: '5.00',
    idle_cons: '2.80',
  },
  {
    key: '2',
  },
  {
    key: '3',
  },
];

const dataSourcespd = [
  {
    key: '1',
    spdtype: 'CP (KTs/cons.)',
    bkt: '12.00',
    bcons: '30.00',
    lkt: '11.00',
    lcons: '10.50',
  },

  {
    key: '2',
    spdtype: 'ECO (kns//Cons.)',
    bkt: '12',
    bcons: '30',
    lkt: '11',
    lcons: '10.5',
  },

  {
    key: '3',
    spdtype: 'ECO (kns//Cons.)',
    bkt: '12',
    bcons: '30',
    lkt: '11',
    lcons: '10.5',
  },
];

class TcovReports extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu"></span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt">Estimate Details Report</span>
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Voyage Astimate</span>
                  </h4>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-9">
                  <Form>
                    <div className="row p10">
                      <div className="col-md-12">
                        <div className="row p10">
                          <div className="col-md-6">
                            <FormItem {...formItemLayout} label="TCOV ID">
                              <Input size="default" defaultValue="" />
                            </FormItem>
                            <FormItem {...formItemLayout} label="Vessel Name">
                              <InputGroup compact>
                                <Input style={{ width: '60%' }} Placeholder="" />
                                <Input style={{ width: '40%' }} defaultValue="DH12F" disabled />
                              </InputGroup>
                            </FormItem>
                            <FormItem {...formItemLayout} label="DWT">
                              <Select size="default" defaultValue="1">
                                <Option value="1">Option</Option>
                                <Option value="2">Option</Option>
                                <Option value="3">Option</Option>
                              </Select>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Daily Rate">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Add Com./Brocker %">
                              <InputGroup compact>
                                <Input style={{ width: '50%' }} Placeholder="" />
                                <Input style={{ width: '50%' }} />
                              </InputGroup>
                            </FormItem>

                            <FormItem {...formItemLayout} label="WF (%)">
                              <Input size="default" defaultValue="" />
                            </FormItem>
                            <FormItem {...formItemLayout} label="Ballast Port">
                              <Select size="default" defaultValue="1">
                                <Option value="1">Option</Option>
                                <Option value="2">Option</Option>
                                <Option value="3">Option</Option>
                              </Select>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Reposition Port">
                              <Select size="default" defaultValue="1">
                                <Option value="1">Option</Option>
                                <Option value="2">Option</Option>
                                <Option value="3">Option</Option>
                              </Select>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Repos. Days">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Commence Date">
                              <Input type="date" size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Completing Date">
                              <Input type="date" size="default" defaultValue="" />
                            </FormItem>
                          </div>
                          <div className="col-md-6">
                            <FormItem {...formItemLayout} label="Total Voyage Days">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Ballast Bonus">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Fixed By/Ops User">
                              <InputGroup compact>
                                <Input style={{ width: '50%' }} Placeholder="" />
                                <Input style={{ width: '50%' }} />
                              </InputGroup>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Voyage Ops Type">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="C/P Date">
                              <Input type="date" size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="My Company/LOB">
                              <InputGroup compact>
                                <Select size="default" defaultValue="1" style={{ width: '50%' }}>
                                  <Option value="1">Option</Option>
                                  <Option value="2">Option</Option>
                                  <Option value="3">Option</Option>
                                </Select>

                                <Select size="default" defaultValue="1" style={{ width: '50%' }}>
                                  <Option value="1">Option</Option>
                                  <Option value="2">Option</Option>
                                  <Option value="3">Option</Option>
                                </Select>
                              </InputGroup>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Routing Type">
                              <Input size="default" defaultValue="" />
                            </FormItem>

                            <FormItem {...formItemLayout} label="Trade Area">
                              <Select size="default" defaultValue="1">
                                <Option value="1">Option</Option>
                                <Option value="2">Option</Option>
                                <Option value="3">Option</Option>
                              </Select>
                            </FormItem>

                            <FormItem {...formItemLayout} label="Mis Cost">
                              <Input size="default" defaultValue="" />
                            </FormItem>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <Table
                          bordered
                          columns={columns}
                          dataSource={data}
                          pagination={false}
                          footer={() => (
                            <div className="text-center">
                              <Button type="link">Add New</Button>
                            </div>
                          )}
                        />
                      </div>
                    </div>
                  </Form>

                  <div className="row p10">
                    <div className="col-md-12 mt-3 text-right ml-auto">
                      <Switch
                        checkedChildren="ECO Speed"
                        unCheckedChildren="CP Speed"
                        defaultChecked
                        className="mr-2"
                      />

                      <Switch
                        checkedChildren="CP$"
                        unCheckedChildren="CP$"
                        style={{ background: '#1d565c' }}
                      />
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-6">
                      <div className="table-info-wrapper mb-0 mt-3">
                        <Table
                          bordered
                          dataSource={dataSourcespd}
                          pagination={false}
                          footer={false}
                        >
                          <Column title="SPD Type" dataIndex="spdtype" />
                          <ColumnGroup title="Ballast">
                            <Column title="KT" dataIndex="bkt" />
                            <Column title="Cons." dataIndex="bcons" />
                          </ColumnGroup>
                          <ColumnGroup title="Laden">
                            <Column title="KT" dataIndex="lkt" />
                            <Column title="Cons." dataIndex="lcons" />
                          </ColumnGroup>
                        </Table>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="mt-3">
                        <EcoSpeed />
                      </div>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-12">
                      <Cargoes />
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-12">
                      <article className="article">
                        <div className="box box-default">
                          <div className="box-body">
                            <Tabs defaultActiveKey="portitinerary" size="small">
                              <TabPane className="pt-3" tab="Port Itinerary" key="portitinerary">
                                <PortItinerary />
                              </TabPane>
                              <TabPane className="pt-3" tab="Bunker Details" key="bunkerdetail">
                                <BunkerDetails />
                              </TabPane>
                              <TabPane
                                className="pt-3"
                                tab="Port Date Details"
                                key="portdatedetail"
                              >
                                <PortDateDetails />
                              </TabPane>
                            </Tabs>
                          </div>
                        </div>
                      </article>

                      <articcle class="article">
                        <div className="box box-default">
                          <div className="box-body">
                            <div className="row p10">
                              <div className="col-md-4">
                                <div className="row">
                                  <div className="col-md-8">
                                    <p>
                                      <b>Total Distance :</b>
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p>
                                      XXXXX
                                      <span className="float-right">
                                        <b>Miles</b>
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="row">
                                  <div className="col-md-8">
                                    <p>
                                      <b>TTL Port Days :</b>
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p>
                                      XXX
                                      <span className="float-right">
                                        <b>Days</b>
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="row">
                                  <div className="col-md-8">
                                    <p>
                                      <b>TSD :</b>
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p>
                                      XXXXX
                                      <span className="float-right">
                                        <b>Days</b>
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="row">
                                  <div className="col-md-8">
                                    <p>
                                      <b>TTL Qty :</b>
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p>
                                      XX
                                      <span className="float-right">
                                        <b>Mt</b>
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="row">
                                  <div className="col-md-8">
                                    <p>
                                      <b>GSD :</b>
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p>
                                      XXX
                                      <span className="float-right">
                                        <b>Days</b>
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </articcle>
                    </div>
                  </div>
                </div>
                <div className="col-md-3"></div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default TcovReports;
