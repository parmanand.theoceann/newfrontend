import React, { useState } from "react";
import { Form, Button, Select, Spin, Layout, Drawer, Modal, Alert } from "antd";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import {
  DeleteOutlined,
  MenuFoldOutlined,
  SaveOutlined,
} from "@ant-design/icons";

const FormItem = Form.Item;
const Option = Select.Option;
const { Content } = Layout;
const GenerateCargoEnquiry = () => {
  const [state, setState] = useState({
    refreshForm: false,
    frmName: "tcov_full_cargo_enq_form",
    formData: {},
  });

  const saveFormData = (savedata, innercb) => {
    //console.log("aa", savedata);
  };

  const deleteData = (id, tcov_id, innercb) => {};

  return (
    <div className="tcov-wrapper full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body common-fields-wrapper">
                      {state.refreshForm === false ? (
                        <NormalFormIndex
                          key={"key_" + state.frmName + "_0"}
                          formClass="label-min-height"
                          formData={state.formData}
                          showForm={true}
                          frmCode={state.frmName}
                          formDevideInCols ={1}
                          showToolbar={[
                            {
                              isLeftBtn: [
                                {
                                  key: "s1",
                                  isSets: [
                                    {
                                      id: "7",
                                      key: "menu-fold",
                                      type: <MenuFoldOutlined />,
                                      withText: "List",
                                      showToolTip: true,
                                      event: "menu-fold",
                                    },
                                    // { id: '8', key: 'plus', type: 'plus', withText: '', event: (key, saveData, innerCB) => (innerCB && typeof innerCB === 'function' ? innerCB() : undefined) },
                                    {
                                      id: "2",
                                      key: "save",
                                      type: <SaveOutlined />,
                                      withText: "Save",
                                      showToolTip: true,
                                      event: (key, saveData, innerCB) => {
                                        saveFormData(saveData, innerCB);
                                      },
                                    },
                                    {
                                      id: "3",
                                      key: "delete",
                                      type: <DeleteOutlined />,
                                      withText: "Delete",
                                      showToolTip: true,
                                      event: (key, saveData, innerCB) => {
                                        if (
                                          saveData["id"] &&
                                          saveData["id"] > 0
                                        ) {
                                          Modal.confirm({
                                            title: "Confirm",
                                            content:
                                              "Are you sure, you want to delete it?",
                                            onOk: () =>
                                              deleteData(
                                                saveData["id"],
                                                saveData["tcov_id"],
                                                innerCB
                                              ),
                                          });
                                        }
                                      },
                                    },
                                  ],
                                },
                              ],
                              isRightBtn: [
                                {
                                  key: "rbs1",
                                  isSets: [
                                    {
                                      key: "reports",
                                      isDropdown: 0,
                                      withText: "Reports",
                                      type: "",
                                      menus: [],
                                    },
                                  ],
                                },
                              ],
                              isResetOption: false,
                            },
                          ]}
                          inlineLayout={true}
                        />
                      ) : undefined}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default GenerateCargoEnquiry;
