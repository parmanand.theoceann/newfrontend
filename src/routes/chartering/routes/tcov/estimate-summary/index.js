import React, { Component } from 'react';
import { Form, Input, Checkbox, Switch, Select } from 'antd';

const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;

function onChange(checkedValues) {
  // console.log('checked = ', checkedValues);
}

function miscrevenuesChange(value) {
  // console.log(`selected ${value}`);
}

function bunkerexpensesChange(value) {
  // console.log(`selected ${value}`);
}

function miscexpensesChange(value) {
  // console.log(`selected ${value}`);
}

class EstimateSummary extends Component {
  render() {
    return (
      <>
        <div className="row">
          <div className="col-md-12">
            <FormItem label="Estimate Id">
              <Input size="default" placeholder="Estimate Id" />
            </FormItem>
          </div>
          <div className="col-md-12">
            <Checkbox.Group onChange={onChange}>
              <div className="row">
                <div className="col-md-6"><Checkbox value="Template">Template</Checkbox></div>
                <div className="col-md-6"><Checkbox value="Benchamark">Benchmark</Checkbox></div>
              </div>
            </Checkbox.Group>
          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col-md-12">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title"><span>P & L Summary USD</span></h4>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="Freight">
              <Input size="default" placeholder="Freight" />
            </FormItem>

            <FormItem label="Demurrage">
              <Input size="default" placeholder="Demurrage" />
            </FormItem>

            <FormItem label="Gross Income">
              <Input size="default" placeholder="Gross Income" />
            </FormItem>

            <FormItem label="Less Commissions">
              <Input size="default" placeholder="Less Commissions" />
            </FormItem>

            <FormItem label="Less Tax">
              <Input size="default" placeholder="Less Tax" />
            </FormItem>

            <FormItem label="Less Despatch">
              <Input size="default" placeholder="Less Despatch" />
            </FormItem>

            <FormItem label="Misc Revenues">
              <Select defaultValue="Option One" onChange={miscrevenuesChange}>
                <Option value="Option One">Option One</Option>
                <Option value="Option Two">Option Two</Option>
              </Select>
            </FormItem>

            <FormItem label="Net Income">
              <Input size="default" placeholder="Net Income" />
            </FormItem>

            <FormItem label="Vessel Expenses">
              <Input size="default" placeholder="Vessel Expenses" />
            </FormItem>

            <FormItem label="Less Addr Comm">
              <Input size="default" placeholder="Less Addr Comm" />
            </FormItem>

            <FormItem label="Port/Canal Exp">
              <Input size="default" placeholder="Port/Canal Exp" />
            </FormItem>

            <FormItem label="Transship Exp">
              <Input size="default" placeholder="Transship Exp" />
            </FormItem>

            <FormItem label="Bunker Expenses">
              <Select defaultValue="Option One" onChange={bunkerexpensesChange}>
                <Option value="Option One">Option One</Option>
                <Option value="Option Two">Option Two</Option>
              </Select>
            </FormItem>

            <FormItem label="Misc Expenses">
              <Select defaultValue="Option One" onChange={miscexpensesChange}>
                <Option value="Option One">Option One</Option>
                <Option value="Option Two">Option Two</Option>
              </Select>
            </FormItem>

            <FormItem label="Less Rebills">
              <Input size="default" placeholder="Less Rebills" />
            </FormItem>

            <FormItem label="Total Expenses">
              <Input size="default" placeholder="Total Expenses" />
            </FormItem>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="Profit">
              <div className="input-wrapper">
                <Switch checkedChildren="Yes" unCheckedChildren="No" />
                <Input size="default" placeholder="Profit" />
              </div>
            </FormItem>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="Daily Profit">
              <Input size="default" placeholder="Daily Profit" />
            </FormItem>

            <FormItem label="Breakeven">
              <Input size="default" placeholder="Breakeven" />
            </FormItem>

            <FormItem label="Frt Rate (USD/t)">
              <Input size="default" placeholder="Frt Rate (USD/t)" />
            </FormItem>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="TCE (USD/t)">
              <div className="input-wrapper">
                <Switch checkedChildren="Yes" unCheckedChildren="No" />
                <Input size="default" placeholder="TCE (USD/t)" />
              </div>
            </FormItem>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="Gross TCE">
              <Input size="default" placeholder="Gross TCE" />
            </FormItem>

          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col-md-12">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title"><span>Voyage Days</span></h4>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">

            <FormItem label="Commencing">
              <Input size="default" placeholder="Commencing" />
            </FormItem>

            <FormItem label="Completing">
              <Input size="default" placeholder="Completing" />
            </FormItem>

            <FormItem label="Voyage Days">
              <Input size="default" placeholder="Voyage Days" />
            </FormItem>

            <FormItem label="Remarks">
              <TextArea placeholder="Remarks" autoSize={{ minRows: 1, maxRows: 3 }} />
            </FormItem>

          </div>
        </div>
      </>
    )
  }
}

export default EstimateSummary;