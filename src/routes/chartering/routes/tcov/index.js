import React, { useState, useEffect, useRef, Suspense } from "react";
import { Form, Button, Select, Spin, Layout, Drawer, Modal, Alert } from "antd";
import {
  useAsyncError,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import {
  DeleteOutlined,
  EyeOutlined,
  MenuFoldOutlined,
  PlusOutlined,
  SaveOutlined,
  SyncOutlined,
  UnderlineOutlined,
} from "@ant-design/icons";

import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  apiDeleteCall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  withLocation,
  useStateCallback,
  sanitize,
} from "../../../../shared";
//import Map from "../../../port-to-port/MapIntellegence";
import SpotPrice from "../../../port-to-port/SpotPrice";
import QuickEstimateTCOV from "./QuickEstimateTCOV";
import Attachment from "../../../../shared/components/Attachment";
import Remarks from "../../../../shared/components/Remarks";
import RightBarUI from "../../../../components/RightBarUI";
import PL from "../../../../shared/components/PL/tcov/TcovEstimatePL";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
//import CargoChildForm from "../cargo/cargo-child";
import Properties from "./Properties";
import TcovReports from "../../../form-reports/TcovReports";
import TcovEstimateDetail from "../../../form-reports/TcovEstimateDetail";
import moment from "moment";
//import AddCargo from "../cargo-contract/index";
import CargoContract from "../cargo-contract";
import PortDistance from "../../../port-to-port/PortAnalytics";
import ShipDistance from "../../../port-to-port/ShipDetails";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../shared/attachments";
import RepositionAnalysis from "./modals/RepositionAnalysis";
import GenerateCargoEnquiry from "./modals/GenerateCargoEnquiry";
import VoygenBunkerPlan from "../../../voyage-bunker-plan";
import GenerateTonnageEnquiry from "./modals/GenerateTonnageEnquiry";
import { calculateTotalSummary, calculateTotalaverage } from "../utils";
import dayjs from "dayjs";
import MapIntellegence from "../../../port-to-port/MapIntellegence";

const MemoizedVoygenBunkerPlan = React.memo(VoygenBunkerPlan);

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const FormItem = Form.Item;
const Option = Select.Option;
const { Content } = Layout;

const TCOV = (props) => {
  const [state, setState] = useStateCallback({
    isBunker: false,
    isweather: false,
    editCargoIndex: -1,
    portItin: [],
    showItemList: false,
    showDiv: false,
    visible: false,
    visibleEstimate: false,
    isVisible: false,
    isShowPortDistance: false,
    estimatePL: false,
    isShowPortRoute: false,
    isShowVoyageBunkerPlan: false,
    // formData: Object.assign({ id: -1 }, props.formData || {}),
    frmName: "tcov_full_estimate_form",
    formData: {
      //   "portitinerary": [
      //     {
      //         "days": "",
      //         "dem_disp_amt": "",
      //         "eca_days": "0.00",
      //         "eff_speed": "",
      //         "funct": "",
      //         "gsd": "",
      //         "id": -9e6,
      //         "is_eur": "false",
      //         "l_d_qty": "0.00",
      //         "l_d_rate": "0.00",
      //         "l_d_rate1": "0.00",
      //         "l_d_term": 1,
      //         "locode": "",
      //         "miles": 0,
      //         "p_exp": "200",
      //         "passage": "",
      //         "port": "Select Port",
      //         "port_id": 0,
      //         "s_type": "1",
      //         "speed": "",
      //         "t_port_days": "0.00",
      //         "tcov_id":'',
      //         "tsd": "0",
      //         "turn_time": "0.00",
      //         "wf_per": "5.00",
      //         "xpd": "0.00",
      //         "xsd": ""
      //     }
      // ],
    },
    portData: null,
    reportFormData: {},
    cargoformData: {},
    value: 2,
    cargoContracts: [],
    vesselData: [],
    visibleSchedule: false,
    visibleAttachment: false,
    loading: false,
    fileArr: [],
    fullFormData: {},
    refreshForm: true,
    fixModal: false,
    fixData: {},
    visibleDrawer: false,
    disableSave: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    isShowTcovReports: false,
    isShowEstimateReport: false,
    fieldOptions: undefined,
    visisbleCargoChild: false,
    cargoData: {},
    visibleviewmodal: false,
    attachment: {},
    editMode: true,
    viewData: {},
    // showLeftBtn: _isShowLeftBtn,
    // showRightBtn: _isShowRightBtn,
    showQuickEstimate: {
      key: "quickview",
      isDropdown: 0,
      withText: "View Quick Est.",
      type: "",
      event: (key, data) => {
        showQuickEstimatePopup(data, true);
      },
    },
    sendBackData: { show: false },
    estimateID: undefined,
    quickFormData: {},
    showQuickEstimateBoolean: false,
    cargosData: props.cargosData || null,
    vmCheck: props.vmCheck !== undefined ? props.vmCheck : false,
    openReposModal: false,
    opencargoenquiryModal: false,
    openTonnageEnquiryModal: false,
  });
  const [quickdata, setQuickdata] = useState(null);

  const location = useLocation();
  const params = useParams();
  const navigate = useNavigate();
  useEffect(() => {
    const _isShowLeftBtn = [
      {
        key: "s1",
        isSets: [
          {
            id: "7",
            key: "menu-fold",
            type: <MenuFoldOutlined />,
            withText: "List",
            showToolTip: true,
            event: "menu-fold",
          },
          {
            id: "1",
            key: "plus",
            type: <PlusOutlined />,
            withText: "Add New",
            showToolTip: true,
            event: () => {
              _onCreateFormData();
            },
          },

          ...(state?.formData?.tcov_status !== "2"
            ? [
              {
                id: "2",
                key: "save",
                type: <SaveOutlined />,
                withText: "Save",
                showToolTip: true,
                event: (key, saveData, innerCB) => {
                  saveFormData(saveData, innerCB);
                },
              },
            ]
            : []),
          state?.formData?.id && {
            id: "3",
            key: "delete",
            type: <DeleteOutlined />,
            withText: "Delete",
            showToolTip: true,
            event: (key, saveData, innerCB) => {
              if (saveData["id"] && saveData["id"] > 0) {
                Modal.confirm({
                  title: "Confirm",
                  content: "Are you sure, you want to delete it?",
                  onOk: () =>
                    deleteData(saveData["id"], saveData["tcov_id"], innerCB),
                });
              }
            },
          },
          {
            id: "20",
            key: "refresh",
            type: <SyncOutlined />,
            withText: "Refresh",
            showToolTip: true,
            event: () => {
              reFreshForm();
            },
          },
        ],
      },
    ];

    const _isShowRightBtn = [
      {
        key: "rbs1",
        isSets: [
          {
            key: "reposition_analysis",
            isDropdown: 1,
            withText: "Reposition Analysis",
            type: "",
            menus: [
              {
                href: null,
                icon: null,
                label: "Reposition Analysis",
                modalKey: "reposition-analysis",
                event: (key) => {
                  openRepositionAnalysis(true);
                },
              },
            ],
          },
          {
            key: "quick_est",
            isDropdown: 0,
            withText: " View Quick Est.",
            type: "",
            menus: null,
            modalKey: "",
            event: (key, data) => {
              if (data["quick_estimate_id"] && data["quick_id"]) {
                navigate(`/edit-quick-estimate/` + data["quick_estimate_id"], {
                  state: {
                    data: data,
                    fullEstimate: true,
                  },
                });
              } else {
                navigate(`/quick-estimate`, {
                  state: {
                    data: data,
                    fullEstimate: true,
                  },
                });
              }
            },
          },

          {
            key: "add_cargo",
            isDropdown: 0,
            withText: "Add Cargo ",
            type: "",
            menus: null,
            modalKey: "add_cargo",
            event: (key, data) => onClickAddCargo({}, data, "sale"),
          },
          {
            key: "bunker_plan",
            isDropdown: 0,
            withText: "Bunker Plan",
            type: "",
            menus: null,
            modalKey: "",
            event: (key, data) => {
              voyageBunkerPlan(true, data);
            },
          },

          // {
          //   key: "ai_tools",
          //   isDropdown: 0,
          //   withText: "AI",
          //   type: "",
          //   menus: null,
          //   modalKey: "",
          //   event: (key, data) => {},
          // },

          // {
          //   key: "benchmarking",
          //   isDropdown: 0,
          //   withText: "Benchmarking",
          //   type: "",
          //   menus: null,
          //   modalKey: "benchmarking",
          //   event: (key, data) => { },
          // },
          {
            key: "sendcargo",
            isDropdown: 1,
            withText: "Cargo/tonnage enquiry",
            type: "",
            menus: [
              {
                href: null,
                icon: null,
                label: "Templete Cargo Order",
                modalKey: "templete-cargo-order",
                event: (key) => {
                  //sendcargotonnage(true);
                },
              },
              {
                href: null,
                icon: null,
                label: "Templete Tonnage Order",
                modalKey: "templete-tonnage-order",
                event: (key) => {
                  //sendcargotonnage(true);
                },
              },
              {
                href: null,
                icon: null,
                label: "Generate Cargo Enquiry",
                modalKey: "generate-cargo-enquiry",
                event: (key) => {
                  opencargoenquiry(true);
                },
              },
              {
                href: null,
                icon: null,
                label: "Generate Tonnage Enquiry",
                modalKey: "generate-tonnage-enquiry",
                event: (key) => {
                  openTonnageenquiry(true);
                },
              },
            ],
          },

          {
            key: "send_firm_offer",
            isDropdown: 0,
            withText: "Send Firm Offer",
            type: "",
            menus: null,
            modalKey: "send_firm_offer",
            event: (key, data) => { },
          },

          // {
          //   key: "draw_cp",
          //   isDropdown: 0,
          //   withText: "Draw CP",
          //   type: "",
          //   menus: null,
          //   modalKey: "draw_cp",
          //   event: (key, data) => { },
          // },
          {
            key: "share",
            isDropdown: 0,
            withText: "Share",
            type: "",
            menus: null,
            modalKey: "Share",
            event: (key, data) => { },
          },

          state?.formData?.tcov_status !== "2" &&
          state?.formData["tcov_status"] === "0" && {
            key: "fix",
            isDropdown: 0,
            withText: "Fix",
            type: "",
            menus: null,
            event: () => {
              if (state.formData["tcov_status"] === "0") {
                Modal.confirm({
                  title: "Confirm",
                  content: "Are you sure, you want to Fix it?",
                  onOk: () => onClickFixTCOV(true),
                });
              }
            },
          },

          state?.formData?.tcov_status !== "2" &&
          state?.formData["tcov_status"] === "1" && {
            key: "schedule",
            isDropdown: 0,
            withText: "schedule",
            type: "",
            menus: null,
            event: () => {
              if (state.formData["tcov_status"] === "1") {
                Modal.confirm({
                  title: "Confirm",
                  content: "Are you sure, you want to Schedule it?",
                  onOk: () => createVoyageManger(),
                });
              }
            },
          },

          {
            key: "reports",
            isDropdown: 1,
            withText: "Reports",
            type: "",
            menus: [
              {
                href: null,
                icon: null,
                label: "Estimate Details Report",
                modalKey: null,
                event: (key) => estimateReport(true),
              },
              // {
              //   href: null,
              //   icon: null,
              //   label: "Download Report",
              //   modalKey: null,
              //   event: (key) => tcovReports(true),
              // },
            ],
          },
        ],
      },
    ];

    setState((prevState) => ({
      ...prevState,
      showLeftBtn: _isShowLeftBtn,
      showRightBtn: _isShowRightBtn,
    }));
  }, [state.formData]);

  // useEffect(() => {
  //   const quickdata = location?.state?.data;

  //   if (quickdata) {
  //     if (quickdata.full_estimate_id) {
  //       editformdata(quickdata.full_estimate_id);
  //     } else {
  //       firstLoadForm(quickdata);
  //     }
  //     //updateMainForm(quickdata.vessel_details);
  //   } else if (location.key == "default") {
  //     //  initialLoadData();
  //     _onCreateFormData();
  //     // reFreshForm()
  //   } else {
  //     editformdata();
  //   }
  // }, [location.key]);

  
  
  useEffect(() => {
    const quickdata = location?.state?.data;
    if (quickdata) {
      firstLoadForm(quickdata);
    } else {
      // if (location.key === "default") {
         //bellow condition is added by harshit for changing estimate id from browser address bar
        if (location.key === "default" && location.pathname==="/add-voyage-estimate") {
        _onCreateFormData();
      } else {
        editformdata(params.id);
      }
    }
  }, [location]);
  
  let formref = useRef(null);
  let savedataref = useRef(null);
  let fixdataref = useRef(null);
  let cargodataref = useRef(null);

  const reFreshForm = () => {
    if (location.pathname === "/add-voyage-estimate") {
      setState(
        (prevState) => ({
          ...prevState,
          formData: {},
          refreshForm: true,
        }),
        () => {
          setState((prev) => ({
            ...prev,
            refreshForm: false,
          }));
        }
      );
    } else {
      editformdata(params.id);
    }

    // setState(
    //   (prevState) => ({
    //     ...prevState,
    //     formData: { ...formref.current },
    //     refreshForm: true,
    //   }),
    //   () => {
    //     setState({ ...state, refreshForm: false });
    //   }
    // );
  };

  useEffect(() => {
    formref.current = Object.assign({}, state.formData);
  }, []);

  const firstLoadForm = async (data) => {
    const {
      vessel_details,
      estimate_id,
      cargo_details,
      blastformdata,
      loadformdata,
      dischargeformdata,
      reposformdata,
      id,
    } = data;
    // setQuickdata(data);

    let _portItarr = formPortItinerary(data);

    _portItarr =
      _portItarr.length > 0
        ? _portItarr
        : [
          {
            port_id: "",
            port: "",
            funct: "",
            s_type: 1,
            wf_per: 10,
            miles: "0.00",
            speed: "0.00",
            eff_speed: "0.00",
            gsd: "0.00",
            tsd: "0.00",
            xsd: "0.00",
            l_d_qty: "0.00",
            l_d_rate: "0.00",
            l_d_rate1: "0.00",
            turn_time: "0.00",
            days: "0.00",
            xpd: "0.00",
            p_exp: "0.00",
            t_port_days: "0.00",
            l_d_term: 1,
            editable: true,
            index: 0,
            ID: -9e6,
          },
        ];

    let _formdata = {
      quick_estimate_id: estimate_id,
      quick_id: id,
      vessel_id: vessel_details.vessel_id,
      vessel_code: vessel_details.vessel_code,
      dwt: vessel_details.dwt,
      tci_d_hire: vessel_details.tci_daily_cost,
      add_percentage: vessel_details.tci_add_comm,
      bro_percentage: vessel_details.tci_bro_comm,
      dwf: vessel_details.wf,
      commence_date: dayjs(vessel_details.commence_date),
      completing_date: dayjs(vessel_details.completed_date),
      total_days: vessel_details.total_voyage_days,
      mis_cost: vessel_details.other_cost,  
      ballast_bonus: vessel_details.blast_bonus,
      bb: vessel_details.other_cost,
      cargos: [
        {
          b_commission: cargo_details.commission,
          cp_qty: cargo_details.cp_qty,
          frat_rate: cargo_details.frt_rate,
          frt_type: cargo_details.frt_type,
        },
      ],
      portitinerary: [..._portItarr],
    };

    const res = await updateMainForm(_formdata);
    _formdata = {
      ..._formdata,
      "-": res["-"],
      ".": res["."],
    };

    setState(
      (prevState) => {
        return {
          ...prevState,
          refreshForm: false,
          formData: {
            ...prevState.formData,
            ..._formdata,
          },
        };
      },
      () => {
        setQuickdata(data);
      }
    );
  };

  const formPortItinerary = (data) => {
    let portItinerary = [];

    const {
      vessel_details,
      blasttoloadDistance = 0,
      loadTodischargeDistance = 0,
      dischargetoreposDistance = 0,
      

      seca_crossed

    } = data;
    const { ballast_port, load_port, discharge_port, repos_port } =
      vessel_details;
    if (data?.blastformdata || ballast_port) {
      checkPortName(data?.blastformdata, portItinerary, ballast_port, 0,'',0);
    }
    if (data?.loadformdata || load_port) {
      checkPortName(
        data?.loadformdata,
        portItinerary,
        load_port,
        blasttoloadDistance,
        seca_crossed.blseca, seca_crossed.blcrossed
      );
    }
    if (data?.dischargeformdata || discharge_port) {
      checkPortName(
        data?.dischargeformdata,
        portItinerary,
        discharge_port,
        loadTodischargeDistance,
        seca_crossed.ldseca, seca_crossed.ldcrossed
      );
    }
    if (data?.reposformdata || repos_port) {
      checkPortName(
        data?.reposformdata,
        portItinerary,
        repos_port,
        dischargetoreposDistance,
        seca_crossed.drcrossed, seca_crossed.drcrossed
      );
    }
    return portItinerary;
  };

  const checkPortName = (
    portData,
    portItinerary = [],
    portName = "",
    distance = 0,
    seca_dist,secacrossed
  ) => {
    if ((portData && portData.port_name) || portName) {
      let portdays = isNaN(portData?.port_days)
        ? 0
        : parseFloat(portData.port_days);
      let xpd = isNaN(portData?.xpd) ? 0 : parseFloat(portData?.xpd);
      let totalportdays = portdays + xpd;
      portItinerary.push({
        days: isNaN(portdays) ? 0 : portdays.toFixed(2),
        funct: portData?.port_func,
        l_d_qty: portData?.ld_qty_unit,
        l_d_rate: portData?.ld_rate_per_day,
        l_d_rate1: portData?.ld_rate_per_hour,
        l_d_term: portData?.term,
        port: portData?.port_name || portName,
        turn_time: portData?.turn_time,
        xpd: isNaN(xpd) ? 0 : xpd.toFixed(2),
        id: -9e6 + portItinerary.length,
        index: portItinerary.length,
        p_exp: portData.port_expense,
        miles: distance,
        t_port_days: isNaN(totalportdays) ? 0 : totalportdays.toFixed(2),
        crossed:secacrossed,
        seca_length:seca_dist
      });
    }

    // if (portData && portData.port_name) {
    //   portItinerary.push({
    //     days: portData?.port_days,

    //     funct: portData?.port_func,

    //     l_d_qty: portData?.ld_qty_unit,
    //     l_d_rate: portData?.ld_rate_per_day,
    //     l_d_rate1: portData?.ld_rate_per_hour,
    //     l_d_term: portData?.term,
    //     port: portData?.port_name || portName,
    //     turn_time: portData?.turn_time,
    //     xpd: portData?.xpd,
    //     id: -9e6 + portItinerary.length,
    //     index: portItinerary.length,
    //     // need for confirmation abou this
    //     // dem_final_amount:
    //     //   data?.portitinerary?.[0]?.dem_disp == "Demmurage"
    //     //     ? data?.portitinerary?.[0]["dem_disp_amt"]
    //     //     : 0,
    //     // des_final_amount:
    //     //   data?.portitinerary?.[0]?.dem_disp == "Despatch"
    //     //     ? data?.portitinerary?.[0]["dem_disp_amt"]
    //     //     : 0,
    //   });
    // }
    return portItinerary;
  };

  const initialLoadData = async () => {
    const { showLeftBtn, formData, showQuickEstimate, showFixMenu } = state;

    let vessel = undefined,
      estimateID = props.estimateID,
      cp_echo = undefined,
      tci_field = undefined,
      diffDays = undefined,
      vessel_id = undefined,
      _showQuickEstimate = undefined,
      _showFixMenu = undefined;
    let response = null,
      respDataC = {},
      respData = {},
      fixData = null,
      editMode = true,
      fieldOptions = undefined,
      _showLeftBtn = Object.assign([], showLeftBtn);
    if (location?.state) {
      estimateID = location?.state?.estimate_id;
    }

    let fullEstData = location?.state;
    if (
      estimateID ||
      (params?.id &&
        location?.pathname === `/edit-voyage-estimate/${params.id}`)
    ) {
      let qParams = { ae: estimateID || params.id };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(
        `${URL_WITH_VERSION}/tcov/edit?${qParamString}`
      );
      respDataC = await response;

      respData = respDataC["data"];

      if (respData.tcov_status === 1) {
        delete _showLeftBtn[0]["isSets"][1];

        setState({
          ...state,
          disableSave: true,
        });
      }

      vessel_id =
        respDataC.hasOwnProperty("row") && respDataC["row"] === "quick_estimate"
          ? respData["vesseldetails"]["vessel_id"]
          : respData["vessel_id"];
      // Vessel Details
      // response = await getAPICall(
      //   `${URL_WITH_VERSION}/vessel/list/${vessel_id}`
      // );
      // vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${vessel_id}/tcov/${estimateID || params.id
        }`
      );
      cp_echo = await response["data"];

      // Get TCI Get Fields
      // response = await getAPICall(
      //   `${URL_WITH_VERSION}/tci/get-fields/${vessel_id}`
      // );
      // tci_field = await response["data"];

      /*
      if (
        respDataC.hasOwnProperty("row") &&
        respDataC["row"] === "quick_estimate"
      ) {
        let _quickDetails = _quickToFullEstimate(respData);
        respData = Object.assign({}, formData, _quickDetails);

        respData["tcov_id"] = _quickDetails["estimate_id"];
        respData["purchase_data"] = cp_echo["."];
        // respData["vessel_code"] = vessel["vessel_code"];
        respData["dwt"] = respDataC.data.vesseldetails.dwt; //vessel["vessel_dwt"];
        // respData["description"] = tci_field["description"];
        respData["hire_rate"] = respDataC.data.vesseldetails.tci_d_hire;
        // respData["tci_code"] = respDataC.data.vesseldetails.tci_code; //tci_field["tci_code"];
        respData["vessel_code"] = respDataC.data.vesseldetails.vessel_code;
      }


 no need of this code
      */
      if (respData && respData.hasOwnProperty("fix")) {
        fixData = respData["fix"];
        delete respData["fix"];
        //respData['disablefield'] = ["tci_code", "my_company_lob"]
        //respData['disablefield'] = ["vessel_id","tci_code","my_company_lob"]
      }

      if (
        respData &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] === 0
      ) {
        _showFixMenu = showFixMenu;
      } else if (
        respData &&
        respData.hasOwnProperty("full_tcov_id") &&
        respData["full_tcov_id"] > 0 &&
        respData.hasOwnProperty("tcov_status") &&
        respData["tcov_status"] === 1
      ) {
        setState({ ...state, editMode: false });
        delete _showLeftBtn[0]["isSets"][1];
        delete _showLeftBtn[0]["isSets"][2];
      }
      respData["-"] =
        respData.hasOwnProperty("-") && respData["-"].length > 0
          ? respData["-"]
          : cp_echo["-"];
      respData["purchase_data"] =
        respData.hasOwnProperty(".") && respData["."].length > 0
          ? respData["."]
          : cp_echo["."];
      respData["."] =
        respData.hasOwnProperty(".") && respData["."].length > 0
          ? respData["."]
          : cp_echo["."]["eco_data"];

      // respData["vessel_code"] = vessel["vessel_code"];
      _showQuickEstimate = showQuickEstimate;
    } else if (
      params.tci_code &&
      location.pathname === "/tci-voy-est/:tci_code"
    ) {
      let qParamString = objectToQueryStringFunc({
        tci_code: params.tci_code,
      });
      response = await getAPICall(
        `${URL_WITH_VERSION}/tci/edit?${qParamString}`
      );
      respData = await response["data"];

      // Vessel Details
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
      );
      vessel = await response["data"];

      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}`
      );
      cp_echo = await response["data"];
      // console.log("response55",response)
      // Get TCI Get Fields
      response = await getAPICall(
        `${URL_WITH_VERSION}/tci/get-fields/${respData["vessel_id"]}`
      );
      tci_field = await response["data"];

      diffDays = moment(respData["deliveryterm"][1]["est_gmt"]).diff(
        moment(respData["deliveryterm"][0]["est_gmt"]),
        "days"
      );
      respData = {
        "-": cp_echo["-"],
        ".": cp_echo["."]["eco_data"],
        purchase_data: cp_echo["."],
        vessel_id: respData["vessel_id"],
        vessel_code: vessel["vessel_code"],
        tci_d_hire: vessel["daily_cost"],
        dwt: vessel["vessel_dwt"],
        add_percentage: tci_field["add_percentage"],
        bro_percentage: tci_field["bro_percentage"],
        description: tci_field["description"],
        hire_rate: tci_field["hire_rate"],
        tci_code: tci_field["tci_code"],
        b_port: respData["deliveryterm"][0]["port"],
        repos_port: respData["deliveryterm"][1]["port"],
        repso_days: diffDays,
        total_days: diffDays,
        bb: respData["otherterm"][0]["amount"],
        trade_area: respData["trade_area"],
        my_company_lob: respData["company_fixed_with"],
        company_lob: respData["company_lob"],
      };
      fieldOptions = { tci_code: tci_field["options"] };
    } else if (fullEstData) {
      // Vessel Details
      // response = await getAPICall(
      //   `${URL_WITH_VERSION}/vessel/list/${fullEstData.vesseldetails.vessel_id}`
      // );
      // vessel = await response["data"];

      // Get CP ECHO Data

      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${fullEstData.vesseldetails.vessel_id}`
      );
      cp_echo = await response["data"];
      respData["purchase_data"] = cp_echo["."];
      // console.log("response55",response)
      // respData["vessel_code"] = vessel["vessel_code"];

      respData["-"] = respData.hasOwnProperty("-")
        ? respData["-"]
        : cp_echo["-"];
      respData["."] = respData.hasOwnProperty(".")
        ? respData["."]
        : cp_echo["."]["eco_data"];

      if (fullEstData && fullEstData.estimate_id) {
        respData["tcov_id"] = fullEstData.estimate_id;
      }

      const {
        vessel_id,
        reposition_port_name,
        ballast_port_name,
        dwt,
        repos_days,
      } = fullEstData && fullEstData.vesseldetails;
      respData["vessel_id"] = vessel_id;
      respData["repos_port_name"] = reposition_port_name;
      respData["b_port_name"] = ballast_port_name;
      respData["dwt"] = dwt;
      respData["repso_days"] = repos_days;
      const { commence_date, completed_date } =
        fullEstData && fullEstData.voyageresults;
      respData["commence_date"] = commence_date;
      respData["completing_date"] = completed_date;
      let {
        cargo_name,
        commission,
        f_rate,
        f_type,
        other_revenue,
        quantity,
        unit,
      } = fullEstData && fullEstData.cargodetails;

      let cargoObj = {
        cargo: cargo_name,
        frat_rate: f_type == 38 ? f_rate : 0,
        lumsum: f_type == 104 ? f_rate : 0,
        frt_type: f_type,
        extra_rev: other_revenue,
        cp_qty: quantity,
        cp_unit: unit,
        b_commission: commission,
      };

      respData["cargos"] = [{ ...cargoObj }];
    }

    if (props.cargosData) {
      Object.assign(respData, props.cargosData);
    }

    if (!respData.hasOwnProperty("id")) {
      delete _showLeftBtn[0]["isSets"][2];
    }

    if (JSON.stringify(respData) === JSON.stringify({})) {
      respData = Object.assign({}, formData);
    }

    // if (respData && !respData.hasOwnProperty("portitinerary")) {
    //   respData["portitinerary"] = [{ port: "Select Port", index: 0 }];
    // }

    setState(
      (prevState) => ({
        ...prevState,
        showLeftBtn: _showLeftBtn,
        showItemList: respData["id"] ? false : true,
        formData: respData,
        estimatePL: true,
        fixData: fixData,
        fieldOptions: fieldOptions,
        showQuickEstimate: _showQuickEstimate,
        showFixMenu: _showFixMenu,
        estimateID: estimateID || params?.id,
      }),
      () => setState({ ...state, refreshForm: false })
    );
  };

  const voyageBunkerPlan = (showVoyageBunkerPlan, data) => {
    setState((prevState) => ({
      ...prevState,
      isShowVoyageBunkerPlan: showVoyageBunkerPlan,
      formData: Object.assign({}, data),
    }));
  };

  const editformdata = async (estid = null) => {
    let estimateID = estid ? estid : params.id;
    let qParams = { ae: estimateID };
    let fixdata = {};
    let respData;
    if (estimateID) {
      try {
        setState((prevState) => ({ ...prevState, refreshForm: true }));
        let qParamString = objectToQueryStringFunc(qParams);
        let response = await getAPICall(
          `${URL_WITH_VERSION}/tcov/edit?${qParamString}`
        );
        let respDataC = await response;
        respData = respDataC["data"];
        respData["tcov_id"] = respData["estimate_id"];

        //  Vessel Details
        let response1 = await getAPICall(
          `${URL_WITH_VERSION}/vessel/list/${respData["vessel_id"]}`
        );
        let vessel = await response1["data"];

        let response2 = await getAPICall(
          `${URL_WITH_VERSION}/vessel/cp_echo/${respData["vessel_id"]}/tcov/${respData["estimate_id"]}`
        );

        let cp_echo = await response2["data"];
        respData["purchase_data"] = cp_echo["."];
        respData["vessel_code"] = vessel["vessel_code"];
        respData["vessel_name"] = vessel["vessel_name"];

        respData["-"] =
          respData.hasOwnProperty("-") && respData["-"].length > 0
            ? respData["-"]
            : cp_echo["-"];

        respData["."] =
          respData.hasOwnProperty(".") && respData["."].length > 0
            ? respData["."]
            : cp_echo["."]["eco_data"];

        if (respData.hasOwnProperty("fix")) {
          fixdata = { ...respData.fix };
          delete respData["fix"];
        }

        if (respData["cargos"] && respData["cargos"].length > 0) {
          //updating serial_no
          respData["cargos"].forEach((cargo, index) => {
            cargo.serial_no = index + 1;
          });
        }

        let toitinaryfields = [
          "total_distance",
          "total_tsd",
          "total_gsd",
          "total_seca",
          "total_seca_days",
          "total_xsd",
          "total_lq",
          "total_turntime",
          "totalt_port_days",
          "total_xpd",
          "total_exp",
          "total_port_pdays",
        ];

        let fromitinaryfields = [
          "miles",
          "tsd",
          "gsd",
          "seca_length",
          "eca_days",
          "xsd",
          "l_d_qty",
          "turn_time",
          "days",
          "xpd",
          "p_exp",
          "t_port_days",
        ];

        let totalitenary = calculateTotalSummary(
          fromitinaryfields,
          toitinaryfields,
          "portitinerary",
          respData
        );

        const totalspeed = calculateTotalaverage(
          "speed",
          "portitinerary",
          respData
        );

        const totaleffspeed = calculateTotalaverage(
          "eff_speed",
          "portitinerary",
          respData
        );

        totalitenary = {
          ...totalitenary,
          total_speed: isNaN(totalspeed) ? 0 : totalspeed,
          total_eff_spd: isNaN(totaleffspeed) ? 0 : totaleffspeed,
        };

        let tobunkerarr = [
          "ttl_miles",
          "ttl_seca_length",
          "ttl_eca_days",
          "ttl_speed",
          "ttl_ec_ulsfo",
          "ttl_ec_lsmgo",
          "ttl_ifo",
          "ttl_vlsfo",
          "ttl_lsmgo",
          "ttl_mgo",
          "ttl_ulsfo",
          "ttl_pc_ifo",
          "ttl_pc_vlsfo",
          "ttl_pc_lsmgo",
          "ttl_pc_mgo",
          "ttl_pc_ulsfo",
        ];

        let frombunkerarr = [
          "miles",
          "seca_length",
          "eca_days",
          "speed",
          "eca_consp",
          // "eca_consp",
          "ifo",
          "vlsfo",
          "lsmgo",
          "mgo",
          "ulsfo",
          "pc_ifo",
          "pc_vlsfo",
          "pc_lsmgo",
          "pc_mgo",
          "pc_ulsfo",
        ];

        const totalbunker = calculateTotalSummary(
          frombunkerarr,
          tobunkerarr,
          "bunkerdetails",
          respData
        );

        const fromciiarr = [
          "ifo",
          "vlsfo",
          "lsmgo",
          "mgo",
          "ulsfo",
          "pc_ifo",
          "pc_vlsfo",
          "pc_lsmgo",
          "pc_mgo",
          "pc_ulsfo",
          "co2_emission_total",
        ];

        const tociiarr = [
          "ttl_ifo_con",
          "ttl_vlsfo_con",
          "ttl_lsmgo_con",
          "ttl_mgo_con",
          "ttl_ulsfo_con",
          "ttl_pc_ifo_con",
          "ttl_pc_vlsfo_con",
          "ttl_pc_lsmgo_con",
          "ttl_pc_mgo_con",
          "ttl_pc_ulsfo_con",
          "ttl_co_emi",
        ];

        const totalcii = calculateTotalSummary(
          fromciiarr,
          tociiarr,
          "ciidynamics",
          respData
        );

        const totalciiVal = {
          ttl_vlsfo_con: (
            parseFloat(totalcii["ttl_vlsfo_con"]) +
            parseFloat(totalcii["ttl_pc_vlsfo_con"])
          ).toFixed(2),
          ttl_lsmgo_con: (
            parseFloat(totalcii["ttl_lsmgo_con"]) +
            parseFloat(totalcii["ttl_pc_lsmgo_con"])
          ).toFixed(2),
          ttl_ulsfo_con: (
            parseFloat(totalcii["ttl_ulsfo_con"]) +
            parseFloat(totalcii["ttl_pc_ulsfo_con"])
          ).toFixed(2),
          ttl_mgo_con: (
            parseFloat(totalcii["ttl_mgo_con"]) +
            parseFloat(totalcii["ttl_pc_mgo_con"])
          ).toFixed(2),
          ttl_ifo_con: (
            parseFloat(totalcii["ttl_ifo_con"]) +
            parseFloat(totalcii["ttl_pc_ifo_con"])
          ).toFixed(2),
          ttl_co_emi: totalcii["ttl_co_emi"],
        };

        const fromeuetcs = [
          "sea_ems",
          "port_ems",
          "ttl_ems",
          "sea_ets_ems",
          "port_ets_ems",
          "ttl_eu_ets",
          "ttl_eu_ets_exp",
        ];

        const toeuetcs = [
          "ttl_sea_emi",
          "ttl_port_emi",
          "ttl_emi",
          "ttl_sea_ets_emi",
          "ttl_port_ets_emi",
          "ttl_ets_emi",
          "ttl_eu_ets_emi",
        ];
        const totaleuets = calculateTotalSummary(
          fromeuetcs,
          toeuetcs,
          "euets",
          respData
        );


        let totalVoyDays = (totalitenary?.['total_port_pdays'] ?? 0) * 1 + (totalitenary?.['total_tsd'] ?? 0) * 1;
        respData["totaleuetssummary"] = { ...totaleuets };
        respData["totalitinerarysummary"] = { ...totalitenary };
        respData["totalbunkersummary"] = { ...totalbunker };
        respData["totalciidynamicssummary"] = { ...totalciiVal };
        respData['total_days'] = isNaN(totalVoyDays) ? 0 : parseFloat(totalVoyDays).toFixed(2);
        formref.current = respData;
        fixdataref.current = { ...fixdata };
        setState((prevState) => ({
          ...prevState,
          refreshForm: false,
          estimateID: respData["estimate_id"],
          formData: Object.assign({}, respData),
          fixData: { ...fixdata },
        }));
      } catch (err) {
        openNotificationWithIcon("error", err.message, 3);
        setState((prevState) => ({
          ...prevState,
          refreshForm: false,
          formData: Object.assign({}, formref.current),
        }));
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        refreshForm: false,
      }));
    }
    return respData;
  };

  const tcovReports = async (showTcovReports) => {
    let estimateID = params.id || state.formData.estimate_id;

    if (showTcovReports) {
      if (estimateID) {
        try {
          let qParamString = objectToQueryStringFunc({
            ae: estimateID,
          });
          // for report Api
          const responseReport = await getAPICall(
            `${URL_WITH_VERSION}/tcov/estimatereport?${qParamString}`
          
          );
          const respDataReport = await responseReport["data"];

          if (respDataReport) {
            setState(
              (prevState) => ({ ...prevState, reportFormData: respDataReport }),
              () => setState({ ...state, isShowTcovReports: showTcovReports })
            );
          } else {
            openNotificationWithIcon("error", "Unable to show report", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "Something went wrong.", 5);
        }
      } else {
        openNotificationWithIcon(
          "error",
          "Please save Voyage Estimate First",
          2
        );
      }
    } else {
      setState({ ...state, isShowTcovReports: showTcovReports });
    }
  };

  const estimateReport = async (showEstimateReport) => {
    let estimateID = params.id || state.formData.estimate_id;
    if (showEstimateReport) {
      if (estimateID) {
        try {
          let qParamString = objectToQueryStringFunc({
            ae: estimateID,
          });
          // for report Api
          const responseReport = await getAPICall(
            `${URL_WITH_VERSION}/tcov/estimatereport?${qParamString}`
          
          );
          const respDataReport = await responseReport["data"];
          if (respDataReport) {
            setState((prevState) => ({
              ...prevState,
              isShowEstimateReport: showEstimateReport,
              reportFormData: respDataReport,
            }));
          } else {
            openNotificationWithIcon("error", "Unable To Show Report.", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "something went wrong", 2);
        }
      } else {
        openNotificationWithIcon(
          "error",
          "Please Save Voyage Estimate First.",
          2
        );
      }
    } else {
      setState({ ...state, isShowEstimateReport: showEstimateReport });
    }
  };



  const onCloseDrawer = () =>
    setState({
      ...state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  const onClickRightMenu = async (key, options) => {
    onCloseDrawer();

    let loadComponent = undefined;
    switch (key) {
      // case 'search':showAttachementModal
      //   loadComponent = <RightSearch />;
      //   break;

      case "attachment":
        const { estimateID } = state;
        if (estimateID) {
          const attachments = await getAttachments(estimateID, "EST");
          const callback = (fileArr) =>
            uploadAttachment(fileArr, estimateID, "EST", "voyage-manager");
          loadComponent = (
            <Attachment
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) => {
                deleteAttachment(file.url, file.name, "EST", "voyage-manager");
              }}
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon(
            "info",
            "Attachments are not allowed here. Please Save First."
          );
        }
        break;
      case "remark":
        const { formData } = state;
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0) {
          loadComponent = (
            <Remarks
              remarksID={formData.estimate_id}
              remarkType="tcov"
              idType="tcov_id"
            />
          );
        } else
          openNotificationWithIcon("info", "Please save Voyage Estimate First");
        break;
      case "port_route_details":
        portDistance(true, state.formData, key);
        break;
      case "port_distance":
        portDistance(true, state.formData, key);
        break;
      case "properties":
        loadComponent = <Properties />;
        break;
      case "home":
        navigate("/chartering-dashboard");
        break;
      case "pl":
        setState({
          ...state,
          sendBackData: { show: true, options: options },
        });
        break;
      // case 'summary':
      //   loadComponent = <EstimateSummary />;
      //   break;
      // case 'portinformation':
      //   loadComponent = <TcovPortInformation />;
      //   break;
      case "bunker": {
        setState((prev) => ({ ...prev, isBunker: true }));
        break;
      }

      case "map": {
        setState((prev) => ({ ...prev, isMap: true }));
        break;
      }
      case "weather": {
        setState((prev) => ({ ...prev, isweather: true }));
        break;
      }
    }

    if (loadComponent) {
      setState({
        ...state,
        visibleDrawer: true,
        title: options.title,
        loadComponent: loadComponent,
        width: options.width && options.width > 0 ? options.width : 1200,
      });
    }
  };

  const onClickquick = (data) => {
    showQuickEstimatePopup(data, true);
    navigate(`/edit-quick-estimate/${data.estimate_id}`);
  };

  const getCargo = (data) => {
    //let _fullFormData = Object.assign({}, state.fullFormData);
    let _fullFormData = state.fullFormData;

    if (Object.keys(_fullFormData).length === 0) {
      _fullFormData=state.formData;
    } else {
    }

    let _index = 0;
    const _editCargoIndex = state.editCargoIndex;

     
    if (
      _fullFormData.hasOwnProperty("cargos") &&
      _fullFormData["cargos"].length == 1
    ) {
      if (
        _fullFormData["cargos"][0]["cargo_contract_id"] == "" ||
        !_fullFormData["cargos"][0]["cargo_contract_id"]
      ) {
        _fullFormData["cargos"] = [];
      }
    }

    if (
      _fullFormData.hasOwnProperty("cargos") &&
      _fullFormData["cargos"].length > 1
    ) {
      for (const cargo of _fullFormData["cargos"]) {
        if (cargo["cargo_contract_id"] === data["cargo_contract_id"]) {
          openNotificationWithIcon(
            "info",
            "Cannot import same Voyage Charter Twice."
          );
          return;
        }
      }
      _index = _fullFormData["cargos"].length;
    }
    setState(
      (prevState) => ({
        ...prevState,
        refreshForm: true,
        visisbleCargoChild: false,
      }),
      () => {
        let _cargoTermData = {};
        _cargoTermData = {
          sp_type: data["sp_type"],
          serial_no:
            _fullFormData?.["cargos"]?.length > 0
              ? _fullFormData?.["cargos"]?.length + 1
              : 1,
          cargo_contract_id: data["cargo_contract_id"],
          cargo: data["cargo_name"],
          charterer: data["charterer"],
          cp_qty: data["cp_qty"],
          cp_unit: data["cp_unit"],
          opt_percentage: "0.00",
          option_type: undefined,
          frt_type: data["freight_type"],
          frat_rate: data["freight_type"] == 38 ? data["freight_rate"] : "0.00",
          lumsum: data["freight_type"] == 104 ? data["freight_rate"] : "0.00",
          b_commission: "0.00",
          extra_rev: "0.00",
          curr: data["currency"],
          dem_rate_pd: "0.00",
          editable: true,
          index: _index,
          ocd: true,
          id: -9e6 + _fullFormData?.["cargos"]?.length,
        };
        if(_editCargoIndex>=0) {
          const newCargo = {..._cargoTermData,index:_editCargoIndex,serial_no:_editCargoIndex+1}
        

          if (_fullFormData.hasOwnProperty("cargos")) {
            _fullFormData["cargos"][_editCargoIndex]={...newCargo}
          } else {
            _fullFormData["cargos"] = [{ ...newCargo }];
          }
          
        }
        else {
          if (
            _fullFormData.hasOwnProperty("cargos") &&
            _fullFormData["cargos"].length >= 1
          ) {
            _fullFormData["cargos"].push(_cargoTermData);
          } else {
            _fullFormData["cargos"] = [{ ..._cargoTermData }];
          }
        }

        const tcov_id = state?.formData?.id;
        _fullFormData?.cargos?.forEach((cargo) => {
          cargo.tcov_id = tcov_id;
        });

        _fullFormData.cargos = _fullFormData.cargos.map((cargo) => {
          if (isNaN(cargo.id)) {
            const { id, ...cargoWithoutId } = cargo;
            return cargoWithoutId;
          }
          return cargo;
        });

        setState((prevState) => ({
          ...prevState,
          formData: { ...prevState.formData, ..._fullFormData },
          //formData: _fullFormData,
          editCargoIndex:-1,
          fullFormData: {},
          indexData: 0,
          refreshForm: false,
        }));
      }
    );
  };

  const redirectToAdd = async (e, id = null) => {
    if (id) {
      navigate(`/edit-voyage-charter/${id}`);
    } else navigate(`/add-voyage-charter`);
  };

  const _quickToFullEstimate = (data) => {
    let _vesselDetails = data["vesseldetails"];
    let _cargoDetails = data["cargodetails"];
    let _voyageResults = data["voyageresults"];
    let _fullDetails = {
      estimate_id: data["estimate_id"],
      cargos: [],
      portitinerary: [],
    };

    _fullDetails["vessel_id"] = _vesselDetails["vessel_id"];
    _fullDetails["dwt"] = _vesselDetails["dwt"];
    _fullDetails["tci_d_hire"] = _vesselDetails["tci_d_hire"];
    _fullDetails["add_percentage"] = _vesselDetails["add_percentage"];
    _fullDetails["bro_percentage"] = _vesselDetails["bro_percentage"];
    _fullDetails["bb"] = _vesselDetails["bb"];
    _fullDetails["mis_cost"] = _vesselDetails["other_cost"];
    _fullDetails["b_port_name"] = _vesselDetails["ballast_port_name"];
    _fullDetails["repos_port_name"] = _vesselDetails["reposition_port_name"];
    _fullDetails["repso_days"] = _vesselDetails["repos_days"];
    _fullDetails["commence_date"] = _voyageResults["commence_date"];
    _fullDetails["completing_date"] = _voyageResults["completed_date"];
    _fullDetails["total_days"] = data["voyageresults"]["total_voyage_days"];

    _fullDetails["portitinerary"].push({
      funct: 3,
      port: _vesselDetails["ballast_port_name"],
      port_id: _vesselDetails["ballast_port"],
      passage: 1,
      s_type: 1,
      miles: "0.00",
      wf_per: "0.00",
      eff_speed: "0.00",
      gsd: "0.00",
      tsd: "0.00",
      p_exp: _vesselDetails["port_exp"],
      speed: _vesselDetails["ballast_spd"],
      l_d_qty: _cargoDetails[0]["quantity"],
    });

    //

    _fullDetails["cargos"].push({
      cargo: _cargoDetails[0]["cargo_name"],
      cp_qty: _cargoDetails[0]["quantity"] * 1,
      cp_unit: _cargoDetails[0]["unit"],
      frt_type: _cargoDetails[0]["f_type"],
      frat_rate:
        _cargoDetails[0]["f_type"] == 38 ? _cargoDetails[0]["f_rate"] : 0,
      lumsum:
        _cargoDetails[0]["f_type"] == 104 ? _cargoDetails[0]["f_rate"] : 0,
      b_commission: _cargoDetails[0]["commission"],
      extra_rev: _cargoDetails[0]["other_revenue"],
    });

    return _fullDetails;
  };

  const onClickAddCargo = (
    cargoContractData = {},
    data = {},
    dType = "purchase"
  ) => {
    cargodataref.current = {};
    setState((prevState) => ({
      ...prevState,
      cargoData: cargoContractData,
      fullFormData: data,
      spType: dType,
      visisbleCargoChild: true,
    }));
  };

  const onCancel = () =>
    setState({
      ...state,
      visisbleCargoChild: false,
      visibleviewmodal: false,
    });

  const getFormData = (data) => {
    setState({
      ...state,
      formData: data,
      cargoformData: {
        id: 0,
        estimate_id: data["estimate_id"],
      },
    });
  };

  const saveFormData = (postData, innerCB) => {
    const { frmName, fileArr } = state;
    setState((prevState) => ({ ...prevState, refreshForm: true }));
    let _url = "save";
    let _method = "post";
    
    if (postData.hasOwnProperty("id")) {
      _url = "update";
      _method = "put";
    }
    if (postData.hasOwnProperty("totalbunkersummary")) {
      delete postData["totalbunkersummary"];
    }
    Object.keys(postData)?.forEach((key) => {
      if (postData[key] == null) {
        delete postData[key];
      } else if (postData[key] === "") {
        postData[key] = 0;
      }
    });
    postData["attachments"] = fileArr;

    if (!postData["cp_date"]) {
      postData["cp_date"] = moment(new Date()).format("YYYY-MM-DD HH:mm");
    } else {
      postData["cp_date"] = moment(postData["cp_date"]).format(
        "YYYY-MM-DD HH:mm"
      );
    }

    if (
      postData.hasOwnProperty("bunkerdetails") &&
      !postData["bunkerdetails"].length > 0
    ) {
      delete postData["bunkerdetails"];
    }

    if (postData.hasOwnProperty("totalitinerarysummary")) {
      delete postData["totalitinerarysummary"];
    }
    if (postData.hasOwnProperty("totalciidynamicssummary")) {
      delete postData["totalciidynamicssummary"];
    }
    if (postData.hasOwnProperty("totaleuetssummary")) {
      delete postData["totaleuetssummary"];
    }
    if (postData["cargos"] && postData["cargos"].length > 0) {
      postData["cargos"] = postData["cargos"].filter(
        (el) => el?.cargo_contract_id
      );
      
      //updating serial_no
      postData["cargos"].forEach((cargo, index) => {
        cargo.serial_no = index + 1;
      });
    }

    if (postData.hasOwnProperty("totalciidynamicssummary")) {
      delete postData["totalciidynamicssummary"];
    }

    if (postData.hasOwnProperty("totaleuetssummary")) {
      delete postData["totaleuetssummary"];
    }

    if (postData.hasOwnProperty("purchase_data")) {
      delete postData["purchase_data"];
    }

    savedataref.current = postData;

    if (!state.disableSave) {
      try {
        postAPICall(
          `${URL_WITH_VERSION}/tcov/${_url}?frm=${frmName}`,
          postData,
          _method,
          (data) => {
            if (data.data) {
              openNotificationWithIcon("success", data.message);
              setState(
                (prevState) => ({
                  ...prevState,
                  // refreshForm: false,
                  estimate_id: data?.row?.estimate_id,
                  formData: {
                    ...state.formData,
                    id: data?.row?.id,
                    estimate_id: data?.row?.estimate_id,
                  },
                }),
                () => {
                  // editformdata(data?.row?.estimate_id);

                  setTimeout(() => {
                    navigate(`/edit-voyage-estimate/${data.row.estimate_id}`, {
                      replace: true,
                    });
                  }, 2000);
                }
              );
            } else {
              setState(
                (prevState) => ({
                  ...prevState,
                  refreshForm: false,
                  formData: savedataref.current,
                }),
                () => {
                  let dataMessage = data.message;
                  let msg = "<div className='row'>";

                  if (typeof dataMessage !== "string") {
                    Object.keys(dataMessage)?.map(
                      (i) =>
                      (msg +=
                        "<div className='col-sm-12'>" +
                        dataMessage[i] +
                        "</div>")
                    );
                  } else {
                    msg += dataMessage;
                  }

                  msg += "</div>";
                  openNotificationWithIcon(
                    "error",
                    <div
                      className="notify-error"
                      dangerouslySetInnerHTML={{ __html: msg }}
                    />
                  );
                }
              );
            }
          }
        );
      } catch (err) {
        setState((prevState) => ({ ...prevState, refreshForm: false }));
        openNotificationWithIcon("error", "something went wrong.", 2);
      }
    } else {
      openNotificationWithIcon("error", "This Estimate is already fixed");
    }
  };

  const deleteData = (id, tcov_id, innerCB) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/tcov/delete`,
      { id: id, tcov_id: tcov_id },
      (resData) => {
        if (resData.data === true) {
          openNotificationWithIcon("success", resData.message);
          navigate("/Voyage-Estimate-list");
        } else {
          openNotificationWithIcon("error", resData.message);
        }
      }
    );
  };

  const isAttachmentOk = () => {
    setState({ ...state, loading: true }, () => {
      setState({ loading: false, visibleAttachment: false });
    });
  };

  const onClickExtraIcon = (action, data) => {
    
    if (data.hasOwnProperty("cargos") && action.gKey == "Cargos") {
      let dataNew = data && data["cargos"].length > 0 ? data["cargos"][action.index] : {};
      
      const preCargos= data.cargos;
     cargodataref.current = dataNew;
      setState(
        (prevState) => ({
          ...prevState,
          viewData: dataNew,
          editCargoIndex: action.index,
          cargoformData: preCargos,
        }),
        () => {
          setState((prevState) => ({ ...prevState, visisbleCargoChild: true, }));
        }
      );
    } else {
      let delete_id = data && data.id;
      
      let groupKey = action["gKey"];
      let frm_code = "";
      if (groupKey == "Cargos") {
        groupKey = "cargos";
        frm_code = "tcov_full_estimate_form";
      }
      if (groupKey == "Port Itinerary") {
        groupKey = "portitinerary";
        frm_code = "tcov_port_itinerary";
      }
      if (groupKey == "Bunker Details") {
        groupKey = "bunkerdetails";
        frm_code = "tcov_bunker_details";
      }
      if (groupKey == "Port Date Details") {
        groupKey = "portdatedetails";
        frm_code = "tcov_port_date_details";
      }
      if (groupKey == ".") {
        frm_code = "tcov_full_estimate_form";
      }
      //if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
        if (groupKey && delete_id && frm_code) {
          
          // console.log("delete_id",delete_id)
          
        let data1 = {
          id: delete_id,
          frm_code: frm_code,
          group_key: groupKey,
        };

        postAPICall(
          `${URL_WITH_VERSION}/tr-delete`,
          data1,
          "delete",
          (response) => {
            
            if (response && response.data) {
              openNotificationWithIcon("success", response.message);
            } else {
              openNotificationWithIcon("error", response.message);
            }
          }
        );
      } else if (groupKey == "cargos") {
        let _fullFormData = state.formData;
        let _cargo = _fullFormData["cargos"];
        let _leftcargo = _cargo?.filter(
          (el) => el.cargo_contract_id != data.cargo_contract_id
        );
        _leftcargo =
          _leftcargo?.length > 0
            ? _leftcargo?.forEach((el, index) => (el["serial_no"] = index + 1))
            : [];
        _fullFormData["cargos"] = [..._leftcargo];
        setState((prevState) => ({
          ...prevState,
          formData: _fullFormData,
        }));
      }
    }
  };

  const onClickFixTCOV = (boolVal) => {
    // if (formref.current?.tcov_status === "0") {
    //   setState((prevState) => ({ ...prevState, fixModal: boolVal }));
    // } else {
    //   openNotificationWithIcon("info", "Please Save Voyage Estimate First.", 3);
    // }
    if (
      state.formData.tcov_status === "0" ||
      state.formData.tcov_status === "1"
    ) {
      setState((prevState) => ({ ...prevState, fixModal: boolVal }));
    } else {
      openNotificationWithIcon("info", "Please Save Voyage Estimate First.", 3);
    }
  };

  const fixVoyage = async () => {
    let _url = `${URL_WITH_VERSION}/tcov/fix`;
    let response = await awaitPostAPICall(_url, fixdataref.current);
    return response;
  };

  const performFix = async () => {
    const { fixData, formData } = state;
    let respData;
    if (formData && formData.hasOwnProperty("id") && formData["id"] > 0) {
      respData = await fixVoyage();
      if (respData["data"]) {
        //openNotificationWithIcon("success", respData.message);

        window.emitNotification({
          n_type: "Voyage Estimate Fixed",
          msg: window.notificationMessageCorrector(
            `Voyage Full Estimate(${formData.estimate_id}) is fixed, for vessel(${formData.vessel_code}), by ${window.userName}`
          ),
        });

        // if schedule success message
        if (respData["message"] && state.formData.tcov_status === "1") {
          navigate(`/voyage-manager/${formData.estimate_id}`, {
            replace: true,
          });
        }

        setState(
          () => ({
            ...state,
            //tcov_status: respData.row.tcov_status,
            fixModal: false,
          }),
          () => {
            editformdata();
          }
        );
      } else {
        let dataMessage = respData.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== "string") {
          Object.keys(dataMessage)?.map(
            (i) =>
              (msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
          );
        } else {
          msg += dataMessage;
        }
        msg += "</div>";
        openNotificationWithIcon(
          "error",
          <div dangerouslySetInnerHTML={{ __html: msg }} />
        );
      }
    } else {
      openNotificationWithIcon(
        "info",
        `Please Save the Voyage Estimate First,then Click On Fix.`
      );
    }
    return respData;
  };

  const createVoyageManger = async () => {
    const data = await fixVoyage();
    if (data.data) {
      navigate(`/voyage-manager/${fixdataref.current["estimate_id"]}`);
    } else {
      openNotificationWithIcon("error", data.message, 2);
    }
  };

  const _onLeftSideListEvent = async (evt) => {
    // const data = await editformdata(evt["estimate_id"]);
    // if (data.hasOwnProperty("id") && data["id"]) {
    //   navigate(
    //     `/edit-voyage-estimate/` +
    //       (evt && evt["estimate_id"] ? evt["estimate_id"] : evt)
    //   );
    // }

    navigate(
      `/edit-voyage-estimate/` +
      (evt && evt["estimate_id"] ? evt["estimate_id"] : evt)
    );
  };

  const updateMainForm = async (formData) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/vessel/list/${formData["vessel_id"]}`
    );
    let vessel = await response["data"];

    // Get CP ECHO Data
    response = await getAPICall(
      `${URL_WITH_VERSION}/vessel/cp_echo/${formData["vessel_id"]}`
    );
    
    let cp_echo = await response["data"];

    // Get TCI Get Fields
    response = await getAPICall(
      `${URL_WITH_VERSION}/tci/get-fields/${formData["vessel_id"]}`
    );

    let tci_field = await response["data"];

    let respData = Object.assign(formData, {
      "-": cp_echo["-"],
      ".": cp_echo["."]["cp_data"],
      // vessel_code: vessel["vessel_code"],
      // tci_d_hire: vessel["daily_cost"],
      // dwt: vessel["vessel_dwt"],
      // add_percentage: tci_field["add_percentage"],
      // bro_percentage: tci_field["bro_percentage"],

      // hire_rate: tci_field["hire_rate"],
      tci_code: tci_field["tci_code"],
    });

    return respData;

    // setState({ ...state, formData: respData }, () =>
    //   setState({ ...state, refreshForm: false })
    // );
  };

  const triggerEvent = (data) => {
    const { sendBackData, estimatePL } = state;
    if (data && sendBackData["show"] === true) {
      // data["estimatePL"] = true;
      onCloseDrawer();
      let loadComponent = (
        <PL
          formData={data}
          viewTabs={["Estimate View"]}
          estimatePL={estimatePL}
        />
      );
      setState({
        ...state,
        visibleDrawer: true,
        title: sendBackData?.options?.title,
        loadComponent: loadComponent,
        width:
          sendBackData?.options?.width > 0 ? sendBackData.options.width : 1200,
        sendBackData: { show: false },
      });
    }
  };

  const showQuickEstimatePopup = (sd, bolVal) =>
    setState(
      (prevState) => ({
        ...prevState,
        estimateID: sd["estimate_id"],
        quickFormData: sd,
      }),
      () => setState({ ...state, showQuickEstimateBoolean: bolVal })
    );

  const portDistance = (val, data, key) => {
    if (key == "port_route_details") {
      if (
        data &&
        data.hasOwnProperty("portitinerary") &&
        data["portitinerary"].length > 0
      ) {
        setState(
          (prevState) => ({
            ...prevState,
            portData: data.portitinerary[0],
            portItin: data.portitinerary,
          }),
          () => setState({ ...state, isShowPortRoute: val })
        );
      } else {
        setState({ ...state, isShowPortRoute: val });
      }
    } else {
      if (key == "port_distance") {
        setState({ ...state, isShowPortDistance: val });
      }
    }
  };

  const openRepositionAnalysis = (bool) => {
    if (bool) {
      setState((prevState) => ({ ...prevState, openReposModal: true }));
    }
  };

  const opencargoenquiry = (bool) => {
    if (bool) {
      setState((prevState) => ({ ...prevState, opencargoenquiryModal: true }));
    }
  };

  const openTonnageenquiry = (bool) => {
    if (bool) {
      setState((prevState) => ({
        ...prevState,
        openTonnageEnquiryModal: true,
      }));
    }
  };

  const _onCreateFormData = () => {
    setState(
      (prevState) => ({
        ...prevState,
        refreshForm: true,
        formData: Object.assign({}, {}),
      }),
      () => {
        setState((state) => ({
          ...state,
          refreshForm: false,
          estimateID:null,
        }));
      }
    );
    navigate("/add-voyage-estimate");
  };



  const updataCPPrice = (plandata) => {
    let consFuelarr = state.formData?.["."];
    let arr = [];
    
    consFuelarr?.map((fuelarr, index) => {
      let obj = Object.assign({}, fuelarr);
      let value1 = CpfromBunkerPlan(plandata, obj["fuel_type"]);
      obj["cp_price"] = value1;
      arr.push(obj);
    });

    let _formdata = Object.assign({}, state.formData);
    _formdata["."] = [...arr];
    setState(
      (prevstate) => ({
        ...prevstate,
        isShowVoyageBunkerPlan: false,
      }),
      () => {
        saveFormData(_formdata);
      }
    );
  };

  const CpfromBunkerPlan = (planarr, consFueltype) => {
    let price = 0;
    planarr?.map((el, ind) => {
      if (el.type === consFueltype) {
        price = el?.end_prc;
      }
    });
    price = price ?? "0.00";
    return price;
  };

  const { viewData } = state;

  return (
    <>
      {state.showDiv === false ? (
        <div className="tcov-wrapper full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">
                    <article className="article">
                      <h4 className="cust-header-title">
                        <b>
                          Voyage Estimate Full Estimation: &nbsp;
                          {state.formData.estimate_id
                            ? state.formData.estimate_id
                            : ""}
                        </b>
                      </h4>
                      <div className="box box-default">
                        <div className="box-body common-fields-wrapper">
                          {state.refreshForm === false ? (
                            <NormalFormIndex
                              key={"key_" + state.frmName + "_0"}
                              formClass="label-min-height"
                              formData={state.formData}
                              showForm={true}
                              frmCode={state.frmName}
                              addForm={true}
                              editMode={state.editMode}
                              showToolbar={[
                                {
                                  isLeftBtn: state.showLeftBtn,

                                  isRightBtn: state.showRightBtn,
                                  isResetOption: false,
                                },
                              ]}
                              inlineLayout={true}
                              sideList={{
                                showList: true,
                                title: "VOY-EST List",
                                uri: "/tcov/list?l=0",
                                columns: [
                                  "estimate_id",
                                  "vessel_name",
                                  "tcov_status",
                                ],
                                icon: true,
                                rowClickEvent: (evt) =>
                                  _onLeftSideListEvent(evt),
                              }}
                              isShowFixedColumn={[
                                "Port Date Details",
                                "Bunker Details",
                                "Port Itinerary",
                                "Cargos",
                                "Total Itinerary Details",
                                "CII Dynamics",
                                "EU ETS",
                                ".",
                                "Total Bunker Details",
                              ]}
                              tabEvents={[
                                {
                                  tabName: "Bunker Details",
                                  event: {
                                    type: "copy",
                                    from: "Port Itinerary",
                                    page: "tcov",
                                    fields: {
                                      port_id: "port_id",
                                      port: "port",
                                      funct: "funct",
                                      passage: "passage",
                                      speed: "speed",
                                      miles: "miles",
                                      seca_length: "seca_length",
                                      eca_days: "eca_days",
                                      s_type: "spd_type",
                                      tsd: "tsd",
                                    },
                                    showSingleIndex: false,
                                    group: {
                                      to: "----------",
                                      from: "---------",
                                    },
                                    otherCopy: [
                                      {
                                        gKey: "portdatedetails",
                                        tgKey: "bunkerdetails",
                                        type: "arrDep",
                                      },
                                    ],
                                  },
                                },
                                {
                                  tabName: "Port Date Details",
                                  event: {
                                    type: "copy",
                                    from: "Port Itinerary",
                                    page: "tcov",
                                    fields: {
                                      port_id: "port_id",
                                      port: "port",
                                      funct: "funct",
                                      passage: "passage",
                                      speed: "speed",
                                      miles: "miles",
                                      wf_per: "wf_per",
                                      tsd: "tsd",
                                      xsd: "xsd",
                                      t_port_days: "pdays",
                                      s_type: "s_type",
                                    },
                                    showSingleIndex: false,
                                    group: {
                                      to: "-----------",
                                      from: "---------",
                                    },
                                  },
                                },

                                {
                                  tabName: "CII Dynamics",
                                  event: {
                                    type: "copy",
                                    from: "Bunker Details",
                                    page: "tcov",
                                    fields: {
                                      port: "port",
                                      funct: "funct",
                                      ifo: "ifo",
                                      vlsfo: "vlsfo",
                                      ulsfo: "ulsfo",
                                      lsmgo: "lsmgo",
                                      mgo: "mgo",
                                      pc_ifo: "pc_ifo",
                                      pc_vlsfo: "pc_vlsfo",
                                      pc_ulsfo: "pc_ulsfo",
                                      pc_lsmgo: "pc_lsmgo",
                                      pc_mgo: "pc_mgo",
                                    },
                                    showSingleIndex: false,
                                  },
                                },

                                {
                                  tabName: "EU ETS",
                                  event: {
                                    type: "copy",
                                    from: "Bunker Details",
                                    page: "tcov",
                                    fields: {
                                      port: "port",
                                      funct: "funct",
                                      ifo: "ifo",
                                      vlsfo: "vlsfo",
                                      ulsfo: "ulsfo",
                                      lsmgo: "lsmgo",
                                      mgo: "mgo",
                                      pc_ifo: "pc_ifo",
                                      pc_vlsfo: "pc_vlsfo",
                                      pc_ulsfo: "pc_ulsfo",
                                      pc_lsmgo: "pc_lsmgo",
                                      pc_mgo: "pc_mgo",
                                    },
                                    showSingleIndex: false,
                                  },
                                },
                              ]}
                              showSideListBar={state.showItemList}
                              tableRowDeleteAction={(action, data) =>
                                onClickExtraIcon(action, data)
                                
                              }
                              extraTableButton={{
                                Cargos: [
                                  {
                                    icon: <EyeOutlined />,
                                    onClickAction: (action, data) => {
                                      onClickExtraIcon(action, data);
                                    },
                                  },
                                ],
                              }}
                              updateMainForm={updateMainForm}
                              sendBackData={state.sendBackData}
                              triggerEvent={triggerEvent}
                            />
                          ) : (
                            <div className="col col-lg-12">
                              <Spin tip="Loading...">
                                <Alert
                                  message=" "
                                  description="Please wait..."
                                  type="info"
                                />
                              </Spin>
                            </div>
                          )}
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </Content>
            </Layout>

            <RightBarUI
              pageTitle="tcov-righttoolbar"
              callback={(data, options) => onClickRightMenu(data, options)}
            />
            {state.loadComponent !== undefined &&
              state.title !== undefined &&
              state.visibleDrawer === true ? (
              <Drawer
                title={state.title}
                placement="right"
                closable={true}
                onClose={onCloseDrawer}
                open={state.visibleDrawer}
                getContainer={false}
                style={{ position: "absolute" }}
                width={state.width}
                maskClosable={false}
                className="drawer-wrapper-container"
              >
                <div className="tcov-wrapper">
                  <div className="layout-wrapper scrollHeight">
                    <div className="content-wrapper noHeight">
                      {state.loadComponent}
                    </div>
                  </div>
                </div>
              </Drawer>
            ) : undefined}
          </Layout>

          {/*   


   no need of thiss
          {state.visibleviewmodal ? (
            <Modal
              style={{ top: "2%" }}
              title={
                state.viewData && state.viewData.sp_type == 187
                  ? "VC (Purchase)"
                  : "Add Voy.Contract (Cargo)"
              }
              open={state.visibleviewmodal}
              onCancel={() => setState({ ...state, visibleviewmodal: false })}
              width="95%"
              footer={null}
            >
              <CargoContract
                formData={state.viewData}
                isShowAddButton={false}
                fullEstimate={true}
                modalCloseEvent={onCancel}
              />
            </Modal>
          ) : undefined}


           */}

          {state.isShowVoyageBunkerPlan ? (
            <Modal
              className="page-container"
              style={{ top: "2%" }}
              title="Voyage Bunker Plan"
              open={state.isShowVoyageBunkerPlan}
              //onOk={handleOk}
              onCancel={() => voyageBunkerPlan(false)}
              width="90%"
              footer={null}
              maskClosable={false}
            >
              <MemoizedVoygenBunkerPlan
                data={state?.formData?.bunkerdetails}
                updatecp={(data) => updataCPPrice(data)}
              />
            </Modal>
          ) : undefined}

          {state.visisbleCargoChild ? (
            <Modal
              style={{ top: "2%" }}
              open={state.visisbleCargoChild}
              title="Voy.Contract (Cargo)"
              onCancel={onCancel}
              footer={null}
              width={"90%"}
              maskClosable={false}
            >
              <CargoContract
                formData={cargodataref.current}
                showPendingList={{ cargo_status: "ENQUIRY" }}
                isShowImport={true}
                isShowAddButton={false}
                fullEstimate={true}
                getCargo={(data) => getCargo(data)}
              />{" "}
            </Modal>
          ) : undefined}

          {state.isShowPortDistance ? (
            <Modal
              style={{ top: "2%" }}
              title="Port Distance"
              open={state.isShowPortDistance}
              // onOk={handleOk}
              onCancel={() => portDistance(false, {}, "port_distance")}
              width="95%"
              footer={null}
            >
              <PortDistance />
            </Modal>
          ) : undefined}
          {state.stateisShowPortRoute ? (
            <Modal
              style={{ top: "2%" }}
              title="Port Route Details"
              open={state.isShowPortRoute}
              // onOk={handleOk}
              onCancel={() => portDistance(false, {}, "port_route_details")}
              width="100%"
              footer={null}
            >
              <ShipDistance
                data={state.portData}
                is_port_to_port={true}
                portItin={state.portItin}
                parentFormData={state.formData}
              />
            </Modal>
          ) : undefined}

          {state.fixModal ? (
            <Modal
              style={{ top: "2%" }}
              open={state.fixModal}
              title="Create Voyage"
              onCancel={() => onClickFixTCOV(false)}
              footer={null}
              width="40%"
              maskClosable={false}
            >
              <div>
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body">
                        <Form>
                          <FormItem {...formItemLayout} label="Vessel">
                            <input
                              type="text"
                              className="ant-input"
                              name="vessel"
                              value={state.fixData["vessel_name"]}
                              readOnly
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Operation Type">
                            <input
                              type="text"
                              className="ant-input"
                              name="opt_type"
                              value={state.fixData["ops_type_name"]}
                              readOnly
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Commence Date">
                            <input
                              type="text"
                              className="ant-input"
                              name="commence_date"
                              value={state.fixData["commence_date"]}
                              readOnly
                            />
                          </FormItem>

                          <FormItem {...formItemLayout} label="Completed Date">
                            <input
                              type="text"
                              className="ant-input"
                              name="completed_date"
                              value={state.fixData["completed_date"]}
                              readOnly
                            />
                          </FormItem>

                          <div className="action-btn text-right">
                            <Button
                              type="primary"
                              onClick={() => {
                                performFix();
                              }}
                            >
                              Ok
                            </Button>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Modal>
          ) : undefined}
        </div>
      ) : undefined}

      {state.isShowTcovReports ? (
        <Modal
          style={{ top: "2%" }}
          title="Reports"
          open={state.isShowTcovReports}
          onCancel={() => tcovReports(false)}
          width="95%"
          footer={null}
        >
          <TcovReports data={state.reportFormData} />
        </Modal>
      ) : undefined}

      {state.openReposModal ? (
        <Modal
          style={{ top: "2%" }}
          title="Reposition Analysis"
          open={state.openReposModal}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, openReposModal: false }))
          }
          width="85%"
          footer={null}
        >
          <RepositionAnalysis />
        </Modal>
      ) : undefined}

      {state.opencargoenquiryModal ? (
        <Modal
          style={{ top: "2%" }}
          title="Generate Cargo Enquiry"
          open={state.opencargoenquiryModal}
          onCancel={() =>
            setState((prevState) => ({
              ...prevState,
              opencargoenquiryModal: false,
            }))
          }
          width="45%"
          footer={null}
        >
          <GenerateCargoEnquiry />
        </Modal>
      ) : undefined}

      {state.openTonnageEnquiryModal ? (
        <Modal
          style={{ top: "2%" }}
          title="Generate Tonnage Enquiry"
          open={state.openTonnageEnquiryModal}
          onCancel={() =>
            setState((prevState) => ({
              ...prevState,
              openTonnageEnquiryModal: false,
            }))
          }
          width="45%"
          footer={null}
        >
          <GenerateTonnageEnquiry />
        </Modal>
      ) : undefined}

      {state.isShowEstimateReport ? (
        <Modal
          style={{ top: "2%" }}
          title="Voyage Estimate Detail Report"
          open={state.isShowEstimateReport}
          onCancel={() => estimateReport(false)}
          width="95%"
          footer={null}
        >
          <TcovEstimateDetail data={state.reportFormData} />
        </Modal>
      ) : undefined}

      {state.showQuickEstimateBoolean ? (
        <Modal
          style={{ top: "2%" }}
          title="Voyage Profit & Loss Report"
          open={state.showQuickEstimateBoolean}
          onCancel={() => showQuickEstimatePopup(null, false)}
          width="95%"
          footer={null}
        >
          <QuickEstimateTCOV
            key="qck-est"
            // changeState={(e, data) => changeState(false, data)}
            estimateID={state.estimateID}
            formData={state.quickFormData}
          />
        </Modal>
      ) : undefined}
      {state.isBunker && (
        <Modal
          style={{ top: "2%" }}
          title=""
          open={state.isBunker}
          onCancel={() => setState((prev) => ({ ...prev, isBunker: false }))}
          width="80%"
          footer={null}
        >
          <SpotPrice />
        </Modal>
      )}

      {state.isMap && (
        <Modal
          style={{ top: "2%" }}
          title="Map Report"
          open={state.isMap}
          onCancel={() => setState((prev) => ({ ...prev, isMap: false }))}
          width="90%"
          footer={null}
        >
            <MapIntellegence/>
        </Modal>
      )}

      {state.isweather && (
        <Modal
          style={{ top: "2%" }}
          title="Weather Report"
          open={state.isweather}
          onCancel={() => setState((prev) => ({ ...prev, isweather: false }))}
          width="90%"
          footer={null}
        >
          {/* <Map /> */}

          <iframe
            src="https://www.ventusky.com/?p=19.1;72.9;5&l=radar"
            title="Report Section"
            style={{ width: "100%", height: "100vh" }}
            frameborder="0"
            allowFullScreen={true}
          />
        </Modal>
      )}
    </>
  );
};

export default TCOV;
