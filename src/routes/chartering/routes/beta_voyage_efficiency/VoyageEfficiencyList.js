import React, { useEffect, useState } from "react";
import URL_WITH_VERSION, {
  ResizeableTitle,
  getAPICall,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../shared";
import { EditOutlined } from "@ant-design/icons";
import { FIELDS } from "../../../../shared/tableFields";
import { Button, Modal, Table } from "antd";
import OtherExpenseModal from "../../../operation/revenue-expenses/components/OtherExpenseModal";
import SidebarColumnFilter from "../../../../shared/SidebarColumnFilter";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import LineChart from "../../../dashboard/charts/LineChart";
import PieChart from "../../../performancedashboard/PieChart";
import ToolbarUI from "../../../../components/CommonToolbarUI/toolbar_index";
import StackHorizontalChart from "../../../dashboard/charts/StackHorizontalChart";
import StackGoupColumnChart from "../../../dashboard/charts/StackNormalizationChart";
import StackAreaChart from "../../../dashboard/charts/StackAreaChart";
import ScatteredChart from "../../../dashboard/charts/ScatteredChart";
import { Col, Row, Card, Space, Select } from "antd";

const VoyageEfficiencyList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });

  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            <Popconfirm
              title="Are you sure, you want to delete it?"
              onConfirm={() => onRowDeletedClick(record.id)}
            >
             <DeleteOutlined /> />
            </Popconfirm>
          </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["voyage-efficiency-list"]
        ? FIELDS["voyage-efficiency-list"]["tableheads"]
        : []
    );

   

    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const redirectToAdd = async (e, record = null) => {
    _onLeftSideListClick(record.id);
  };

  const onChangeStatus = (status = false) => {
    setState({ ...state, isVisible: status }, () => getTableData());
  };

  const _onLeftSideListClick = async (delayId) => {
    setState((prevState) => ({ ...prevState, isVisible: false }));
    // setState({ ...state, isVisible: false });
    let _url = "edit?e=";
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];
    setState((prevState) => ({
      ...prevState,
      formDataValues: data,
      isVisible: true,
    }));
    // setState({ ...state, isVisible: false, formDataValues }, () => {
    //   setState({ ...state, isVisible: true });
    // });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    isVisible,
    sidebarVisible,
    formDataValues,
  } = state;
  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  console.log(state.column);

  const ClusterDataSeries = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [600, 500, 700, 800, 290],
    },
  ];
  const totalDashboarddat = [
    {
      title: "Count of vessels",
      value: "540",
    },
    {
      title: "Average Voyage Days",
      value: "300",
    },
    {
      title: "Average sea cons.",
      value: "120",
    },
    {
      title: "Average Port Cons.",
      value: "150",
    },
    {
      title: "Average  Bunker Cons.",
      value: "210",
    },
  ];

  const ClusterDataxAxis = [
    "TCOVFULL304",
    "TCOVFULL305",
    "TCOVFULL306",
    "TCOVFULL307",
    "TCOVFULL308",
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "FRT-INV-000586",
    "FRT-INV-000687",
    "FRT-INV-000788",
    "FRT-INV-000889",
    "FRT-INV-000989",
  ];
  const PieChartData = [
    { value: 40, name: "Vessel 1" },
    { value: 30, name: "Vessel 2" },
    { value: 20, name: "Vessel 4" },
  ];

  const StackHorizontalChartyAxis2 = [
    "FullName1",
    "FullName2",
    "FullName3",
    "FullName4",
  ];
  const StackHorizontalChartSeries2 = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
  ];
  const StackNormalizationSeriesData = [
    {
      name: "Total Revenue",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [50, 33.2, 70.1, 45.4, 29], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Total Expense",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Net Result",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
    },
  ];
  const StackNormalizationSeriesDatatwo = [
    {
      name: "Total Revenue",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [60, 39, 90, 45.4, 80], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Total Expense",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Net Result",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
    },
  ];

  const [pieChartOptions, setPieChartOptions] = useState({
    bar1: {
      option: {
        tooltip: {
          trigger: "item",
        },
        series: [
          {
            name: "Access From",
            type: "pie",
            radius: "50%",
            data: [
              { value: 1048, name: "CS HANA 444.3" },
              { value: 735, name: "OCEANIC  MAJESTY 121.4" },
              { value: 580, name: "PACIFIC EXPLORER 332.1" },
              { value: 484, name: "GOODWYN ISLAND 454.33" },
              { value: 300, name: "CARIBBEN PRINCESS 321.22" },
            ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
          },
        ],
      },
      // labelLine: {
      //   show: true,
      //   length: 20,
      //   length2: 20,
      // },
    },
  });

  const [stackChartOptionsone, setStackChartOptionsOne] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category", // Switched back to category type for vertical bars
          // data: StackNormalizationSeriesData??null
          data: ["TCOV-FULL-00979", "TCOV-FULL-00969", "TCR09-23-02123", "TCE09-23-01091", "TCE09-23-01098"],
          axisLabel: {
            rotate: 45, // Rotate labels by 45 degrees
            fontSize: 12
          },
        },
        yAxis: {
          // type: "category",
          // data: ['a','b',"c","d","e",],
          axisLabel: {
            formatter: "{value}K", // Show percentage on axis
          },
        },
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow",
          },
        },
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 10,
          containLabel: true,
        },
        series: [
          {
            name: "Total sea Cons",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "lightgreen", // Specify the desired color
            },
            data: [32, 33.2, 30.1, 33.4, 39], // Normalize data by dividing by max and multiplying by 100
          },
          {
            name: "Total port Cons.",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "Orange", // Specify the desired color
            },
            data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
          },
          {
            name: "Total Bunker Consumption",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "grey", // Specify the desired color
            },
            data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
          },
        ],
      },
    },
  });

  const [stackChartOptionstwo, setStackChartOptionsTwo] = useState({
    bar1: {
      option: {
        xAxis: {
          type: "category", // Switched back to category type for vertical bars
          // data: StackNormalizationSeriesData??null
          data: [
            "TCOVFULL304",
            "TCOVFULL305",
            "TCOVFULL306",
            "TCOVFULL307",
            "TCOVFULL308",
          ],
          axisLabel: {
            rotate: 45, // Rotate the labels by 45 degrees
          },
        },
        yAxis: {
          // type: "category",
          // data: ['a','b',"c","d","e",],
          axisLabel: {
            formatter: "{value}K", // Show percentage on axis
          },
        },
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow",
          },
        },
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 10,
          containLabel: true,
        },
        series: [
          {
            name: "Total sea Cons",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "lightblue", // Specify the desired color
            },
            data: [32, 33.2, 30.1, 33.4, 39], // Normalize data by dividing by max and multiplying by 100
          },
          {
            name: "Total port Cons.",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "yellow", // Specify the desired color
            },
            data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
          },
          {
            name: "Total Bunker Consumption",
            type: "bar",
            stack: "stack1",
            label: {
              show: true,
              position: "insideTop", // Display labels inside bars
              formatter: "{c}%", // Show percentage values
            },
            itemStyle: {
              // Set the color for "Total Revenue" series
              color: "grey", // Specify the desired color
            },
            data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
          },
        ],
      },
    },
  });

  return (
    <>
      <div className="body-wrapper">
        <Row gutter={[16, 16]} style={{ justifyContent: "space-between" }}>
          <Col span={4}>
            <Card
              title={
                <div style={{ color: "white", textAlign: "center" }}>
                  Average Bunker Cons.
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
                color: "white",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <h4 style={{ textAlign: "center", margin: 0, color: "white" }}>
                210mt
              </h4>
            </Card>
          </Col>
          <Col span={4}>
            <Card
              title={
                <div style={{ color: "white", textAlign: "center" }}>
                  Average Port Cons.
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <h4 style={{ textAlign: "center", margin: 0, color: "white" }}>
                150mt
              </h4>
            </Card>
          </Col>
          <Col span={4}>
            <Card
              title={
                <div style={{ color: "white", textAlign: "center" }}>
                  Average sea cons.
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <h4 style={{ textAlign: "center", margin: 0, color: "white" }}>
                120mt
              </h4>
            </Card>
          </Col>
          <Col span={4}>
            <Card
              title={
                <div style={{ color: "white", textAlign: "center" }}>
                  Count of vessels.
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <h4 style={{ textAlign: "center", margin: 0, color: "white" }}>
                540
              </h4>
            </Card>
          </Col>
          <Col span={4}>
            <Card
              title={
                <div style={{ color: "white", textAlign: "center" }}>
                  Average Voyage Days.
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <h4 style={{ textAlign: "center", margin: 0, color: "white" }}>
                300
              </h4>
            </Card>
          </Col>
          <Col span={4}>
            <Button>Reset</Button>
          </Col>
        </Row>
        <Row
          gutter={[16, 16]}
          style={{ justifyContent: "space-between", marginTop: "1rem" }}
        >
          <Col span={4}>Vessel</Col>
          <Col span={4}>Voyage no.</Col>
          <Col span={4}>Voyage Type</Col>
          <Col span={4}>Commence date</Col>
          <Col span={4}>completion date</Col>
          <Col span={4}>Status</Col>
        </Row>
        <Row
          gutter={[16, 16]}
          style={{ justifyContent: "space-between", marginTop: "1rem" }}
        >
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "PACIFIC EXPLORER",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "OCEANIC MAJESTY",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "CS HANA",
                  label: "CS HANA",
                },
              ]}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "SUPRAMAX",
                  label: "SUPRAMAX",
                },
                {
                  value: "PANAMAX",
                  label: "PANAMAX",
                },
                {
                  value: "KAMSARMAX",
                  label: "KAMSARMAX",
                },
              ]}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "2024-01-25",
                  label: "2024-01-25",
                },
                {
                  value: "2024-01-24",
                  label: "2024-01-24",
                },
                {
                  value: "2024-01-15",
                  label: "2024-01-15",
                },
              ]}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "2024-08-13",
                  label: "2024-08-13",
                },
                {
                  value: "2024-01-24",
                  label: "2024-01-24",
                },
                {
                  value: "2024-01-15",
                  label: "2024-01-15",
                },
              ]}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "PENDING",
                  label: "PENDING",
                },
                {
                  value: "FIX",
                  label: "FIX",
                },
                {
                  value: "SCHEDUED",
                  label: "SCHEDUED",
                },
              ]}
            />
          </Col>
        </Row>
        <div className="container-fluid mt-5">
          <div>
            <div className="row">
              <div className="col-md-6">
                <ClusterColumnChart
                  Heading={"Bunker Consp. per Voyage"}
                  ClusterDataxAxis={ClusterDataxAxis}
                  ClusterDataSeries={ClusterDataSeries}
                  maxValueyAxis={"650"}
                />
              </div>
              <div className="col-md-6">
                <StackGoupColumnChart
                  Heading={"P&L Per Voyage No."}
                  stackChartOptions={stackChartOptionsone}
                  StackNormalizationSeriesData={StackNormalizationSeriesData}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <PieChart
                  pieChartOptions={pieChartOptions}
                  PieChartData={PieChartData}
                  Heading={"Net Result (Numeric) for each vessel in a pie chart."}
                />
              </div>
              <div className="col-md-6">
                <ScatteredChart Heading={"Daily Profit/Loss Per Voyage No."} />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6"></div>
              <div className="col-md-6"></div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <StackGoupColumnChart
                  Heading={"Bunker Consp. Per Vessel"}
                  stackChartOptions={stackChartOptionstwo}
                  StackNormalizationSeriesData={StackNormalizationSeriesDatatwo}
                />
              </div>
              <div className="col-md-6">
                <StackAreaChart Heading={"Total Revenue per Voyage no."} />
              </div>
            </div>
          </div>
        </div>
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Voyage Efficiency Dashboard</span>
                  </h4>
                </div>
                {/* <div className="action-btn">
                <Link to="add-tcov">Add Bunker Invoice</Link>
              </div> */}
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"other-expense-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph={showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "otherrevenue"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "otherrevenue"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "otherrevenue"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {isVisible ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Edit Invoice"
            open={isVisible}
            onCancel={() => onChangeStatus()}
            width="95%"
            footer={null}
          >
            <OtherExpenseModal
              formData={formDataValues}
              isEdit={true}
              modalCloseEvent={() => onChangeStatus()}
            />
          </Modal>
        ) : undefined}
        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
      </div>
    </>
  );
};

export default VoyageEfficiencyList;
