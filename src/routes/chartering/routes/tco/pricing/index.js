import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        hirerate: "Hire Rate",
        ratetype: "Rate Type",
        code: "Code",
        lock: "Lock",
        fromgmt: "From GMT",
        togmt: "To GMT",
        duration: "Duration",
        tt: "TT",
        period: "Period",
        tclins: "TCL Ins",
        comments: "Comments",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class Pricing extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Hire Rate',
            dataIndex: 'hirerate',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'hirerate'),
        },
        {
            title: 'Rate Type',
            dataIndex: 'ratetype',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'ratetype'),
        },
        {
            title: 'Code',
            dataIndex: 'code',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'code'),
        },
        {
            title: 'Lock',
            dataIndex: 'lock',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'lock'),
        },
        {
            title: 'From GMT',
            dataIndex: 'fromgmt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'fromgmt'),
        },
        {
            title: 'To GMT',
            dataIndex: 'togmt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'togmt'),
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'duration'),
        },
        {
            title: 'TT',
            dataIndex: 'tt',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tt'),
        },
        {
            title: 'Period',
            dataIndex: 'period',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'period'),
        },
        {
            title: 'TCL Ins',
            dataIndex: 'tclins',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'tclins'),
        },
        {
            title: 'Comments',
            dataIndex: 'comments',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'dem'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 1500, y: 300 }}
                    size="small"
                    // pagination={{ pageSize: 50 }}
                    pagination={false}
                // title={() => 'Port / Date Group'}
                />
            </>
        )
    }
}

export default Pricing;