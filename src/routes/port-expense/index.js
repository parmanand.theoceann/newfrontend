import React, { Component } from 'react';
import {
  Form,
  Tabs,
  Layout,
  Select,
  Modal,
} from 'antd';
import Tde from '../tde/Tde';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import CreateInvoice from '../create-invoice/CreateInvoice';
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  postAPICall,
  apiDeleteCall,
} from '../../shared';
import PortExpenseReport from '../operation-reports/PortExpenseReport'
import { DeleteOutlined, SaveOutlined } from '@ant-design/icons';


const { Content } = Layout;

function object_equals(x, y) {
  if (x === y) return true;
  // if both x and y are null or undefined and exactly the same

  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects

  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.

  for (var p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor

    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined

    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal

    if (typeof (x[p]) !== "object") return false;
    // Numbers, Strings, Functions, Booleans must be strictly equal

    if (!object_equals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }

  for (p in y)
    if (y.hasOwnProperty(p) && !x.hasOwnProperty(p))
      return false;
  // allows x[ p ] to be set to undefined

  return true;
}

class PortExpense extends Component {
  constructor(props) {
    super(props);
    

    this.state = {
      frmName: this.props.isAgentView === true ? "port_expenses_agent" : "port_expenses",
      formData: {},
      loading: false,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      isShowFdaInvoice: false,
      isShowPdaInvoice: false,
      isShowTdeShow: false,
      isAgentView: this.props.isAgentView || false,
      editportexpData: this.props.editportexpData,
      responseData: this.props.responseData || {},
      vesselID: this.props.responseData && this.props.responseData.vessel || this.props.voyID,
      vesselDetails: {},
      isShowPortExpenseReport: false,
      saveIID: null || 162,
      selectedPortId: this.props.selectedPortId,
      port_call_id: this.props.editportexpData && this.props.editportexpData['port_call'] && this.props.editportexpData['port_call']['id'],
      msg: 'Are you sure FDA Calculation is correct !!',
      isUserView: this.props.isUserView,
      isResubmitButton: false,
      isAgentView: this.props.isAgentView === true ? true : false,
      modelVisible: false,
      fda_submit_st: false,
      pda_submit_st: false,
      reportData: [],
      TdeList: null,
      tde_id: 0,
      resubmitModal: false,
      pdaApproveModal: false,
      approveType: '',
      tdeList: this.props.tdeList || null,
      deleteModal: false,
      delete_port_id: 0
    };
  }

  componentDidMount = async () => {
    const { editportexpData } = this.state
    this.closeTdeModal()
    this.updatePortExpenseData(editportexpData)
    this.showPortDetails()
  };

  showPortDetails = () => {
    const { isUserView, editportexpData } = this.state;
    let port_call_ref_id = editportexpData['port_call']['port_call_ref_id']
    if (isUserView.length > 0) {
      isUserView.map(val => {
        if (val.port_call_ref_id == port_call_ref_id) {
          this.setState({ isResubmitButton: val.port_status == 'SUBMITTED' ? true : false })
        }
      })
    }
  }

  formShowPDA = (st) => { this.state.pda_submit_st = st; this.setState({ pda_submit_st: st }) }
  formShowFDA = (st) => { this.state.fda_submit_st = st; this.setState({ fda_submit_st: st }) }

  updatePortExpenseData = async (editportexpData) => {
    const { isAgentView, isUserView, formData, tdeList, TdeList } = this.state;
    let frmData = {}
    let port_call_ref_id = editportexpData['port_call']['port_call_ref_id']
    let isResubmitButton = false
    if (isUserView.length > 0) {
      isUserView.map(val => {
        if (val.port_call_ref_id == port_call_ref_id) {
          isResubmitButton = val.port_status == 'SUBMITTED' ? true : false
        }
      })
    }
    if (editportexpData['port_expense'] != -1) {
      let pdaTotalArraty = editportexpData['port_expense'];
      let new_array = []
      let tde_array = []
      let TDE_LIST = []
      let tde_id = 0
      let Tde_List = null
      let total_paid = 0.00
      pdaTotalArraty['port'] = editportexpData['port_call'].portcalldetails && editportexpData['port_call'].portcalldetails['port']
      pdaTotalArraty['voy_no'] = pdaTotalArraty['voyage_manager_id']
      pdaTotalArraty['voyage_manager_id'] = pdaTotalArraty['voyage_manager_id']
      pdaTotalArraty['fda_currency'] = pdaTotalArraty['..']['advance_curr']
      if (isAgentView === false) {
        if (TdeList == null) {
          const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
          const responseTdeData = response['data']
          TDE_LIST = responseTdeData
        } else {
          TDE_LIST = tdeList
        }
        Tde_List = TDE_LIST&&TDE_LIST.length>0?TDE_LIST.filter(el => pdaTotalArraty['disburmnt_inv'] == el.invoice_no):[];
        if (Tde_List.length > 0) {
          tde_id = Tde_List[0]['id']
          const editData = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${tde_id}`);
          const tdeEditData = await editData['data']
          if (tdeEditData.paymententrytable && tdeEditData.paymententrytable.length > 0) {
            tdeEditData.paymententrytable.map(el => {
              let data = {
                'pay_status': el.payment_status == 208 ? 'PAID/RECIEVED' : 'PENDING',
                'tde_no': tdeEditData.trans_no,
                'paid_amount': el.amount_paid,
                'date_paid': el.date_paid,
                'payment_id': el.id
              }
              total_paid += parseFloat(el.amount_paid)
              tde_array.push(data)
            })
            if (pdaTotalArraty['-'] && pdaTotalArraty['-'].length > 0 && tde_array && tde_array.length > 0) {
              let payment_array = []
              pdaTotalArraty['-'].map(p1 => {
                tde_array.map(el => {
                  if (el.payment_id === p1.payment_id) {
                    el['id'] = p1.id
                    let data = {
                      'pay_status': el.pay_status == 208 ? 'PAID/RECIEVED' : 'PENDING',
                      'tde_no': el.tde_no,
                      'paid_amount': el.paid_amount,
                      'date_paid': el.date_paid,
                      'payment_id': el.payment_id,
                      'id': p1.id
                    }
                    payment_array.push(data)
                  } else {
                    payment_array.push(el)
                  }
                })
              })
              let resArr = [];
              payment_array.forEach(function (item) {
                let i = resArr.findIndex(x => x.payment_id == item.payment_id);
                if (i <= -1) {
                  resArr.push(item);
                }
              });
              pdaTotalArraty['-'] = resArr
            } else {
              pdaTotalArraty['-'] = tde_array
            }
          }
        }
      }
      if (pdaTotalArraty.pda_submit == 0 && isAgentView === true) {
        if (pdaTotalArraty['...'] && pdaTotalArraty['...']) {
          pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': 160, 'disablefield': ['agent_exchange', 'our_exchange', 'due_to_agent1'] });
        }
        if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
          pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
            e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark']
          })
        } else {
          pdaTotalArraty['--'] = [{ 'disablefield': ['cost_item', 'fda_amount', 'act_tax', 'attchment', 'remark'] }]
        }
        this.formShowPDA(true)
      }

      if (pdaTotalArraty.pda_submit == 0 && isAgentView === false) {
        if (pdaTotalArraty['-'] && pdaTotalArraty['-'].length > 0) {
          pdaTotalArraty['-'] && pdaTotalArraty['-'].map(e => {
            e['disablefield'] = ['pay_status', 'tde_no', 'paid_amount', 'date_paid']
          })
        } else { pdaTotalArraty['-'] = [] }
        if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
          pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
            e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark']
          })
        } else {
          pdaTotalArraty['--'] = []
        }
        if (pdaTotalArraty['.'] && pdaTotalArraty['.'].length > 0) {
          pdaTotalArraty['.'] && pdaTotalArraty['.'].map(e => {
            e['disablefield'] = ['cost_item', 'quoted', 'agreed_adv', 'axct_tax', 'attachment', 'remark']
          })
        } else { pdaTotalArraty['.'] = [] }
        if (pdaTotalArraty['..'] && pdaTotalArraty['..']) {
          pdaTotalArraty['..'] = Object.assign({}, pdaTotalArraty['..'], { 'disablefield': ['advance_curr', 'our_exchange'] });
        }
        if (pdaTotalArraty['...'] && pdaTotalArraty['...']) {
          pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': 160, 'disablefield': ['agent_exchange', 'our_exchange', 'due_to_agent1'] });
        }
      }

      if (pdaTotalArraty.pda_submit == 1) {
        if (isAgentView === true) {
          if (pdaTotalArraty['.'] && pdaTotalArraty['.'].length > 0) {
            pdaTotalArraty['.'] && pdaTotalArraty['.'].map(e => {
              e['disablefield'] = ['cost_item', 'quoted', 'agreed_adv', 'axct_tax', 'attachment', 'remark']
            })
          }
          if (pdaTotalArraty['..'] && pdaTotalArraty['..']) {
            pdaTotalArraty['..'] = Object.assign({}, pdaTotalArraty['..'], { 'disablefield': ['advance_curr', 'our_exchange', 'agent_exchange'] });
          }
          if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
            if (!pdaTotalArraty['...']) {
              pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'agent_exchange': pdaTotalArraty['..']['agent_exchange'], 'our_exchange': pdaTotalArraty['..']['our_exchange'], 'due_to_agent1': 160, 'disablefield': ['due_to_agent1'] });
            }
            if (pdaTotalArraty['...']) {
              pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': parseFloat(pdaTotalArraty['...']['due_to_agent']) > 0 ? 160 : 161, 'disablefield': ['due_to_agent1'] });
            }
          } else {
            if (pdaTotalArraty['.'] && pdaTotalArraty['.'].length > 0) {
              pdaTotalArraty['--'] = []
              let count = -1
              pdaTotalArraty['.'].map(e => {
                count = count + 1
                pdaTotalArraty['--'].push({
                  'cost_item': e.cost_item,
                  'total_agreed': e.agreed_adv,
                  'total_quoted': e.quoted,
                  'remark': e.remark,
                  'attchment': e.attachment,
                  'act_tax': e.axct_tax,
                  'editable': true,
                  'id': -9e6 + count,
                  'index': count,
                  'key': `table_row_${count}`
                })
              })
            }
            pdaTotalArraty['...'] = { 'agent_exchange': pdaTotalArraty['..']['agent_exchange'], 'our_exchange': pdaTotalArraty['..']['our_exchange'], 'due_to_agent1': 160, 'disablefield': ['due_to_agent1'] }
          }

          this.formShowPDA(false)
          this.formShowFDA(true)
        }
        if (isAgentView === false) {
          if (pdaTotalArraty['-'] && pdaTotalArraty['-'].length > 0) {
            pdaTotalArraty['-'] && pdaTotalArraty['-'].map(e => {
              e['disablefield'] = ['pay_status', 'tde_no', 'paid_amount', 'date_paid']
            })
          } else { pdaTotalArraty['-'] = [] }
          if (pdaTotalArraty.fda_submit == 0) {
            //pdaTotalArraty['-'] = []
            if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
              pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
                e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark']
              })
            } else {
              pdaTotalArraty['--'] = []
            }

            if (pdaTotalArraty['...']) {
              pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': parseFloat(pdaTotalArraty['...']['due_to_agent']) > 0 ? 160 : 161, 'disablefield': ['agent_exchange', 'advance_curr', 'our_exchange', 'due_to_agent1'] });
            }
            this.formShowPDA(true)
            this.formShowFDA(false)
          } else if (pdaTotalArraty.fda_submit == 1) {
            if (pdaTotalArraty['.'] && pdaTotalArraty['.'].length > 0) {
              pdaTotalArraty['.'] && pdaTotalArraty['.'].map(e => {
                e['disablefield'] = ['cost_item', 'agreed_adv', 'axct_tax', 'attachment', 'remark']
              })
            }
            if (pdaTotalArraty['..'] && pdaTotalArraty['..']) {
              pdaTotalArraty['..'] = Object.assign({}, pdaTotalArraty['..'], { 'disablefield': ['advance_curr', 'our_exchange'] });
            }
            if (pdaTotalArraty['...'] && pdaTotalArraty['...']) {
              pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': parseFloat(pdaTotalArraty['...']['due_to_agent']) > 0 ? 160 : 161, 'disablefield': ['due_to_agent1'] });
            }
            if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
              pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
                e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed']
              })
            }
            this.formShowPDA(false)
            this.formShowFDA(true)
          }
        }
      }
      if (pdaTotalArraty.fda_submit == 1 && pdaTotalArraty.pda_submit == 1) {
        if (isAgentView === true) {
          if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
            pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
              //e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark']
            })
          }
          if (pdaTotalArraty['...'] && pdaTotalArraty['...']) {
            pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': parseFloat(pdaTotalArraty['...']['due_to_agent']) > 0 ? 160 : 161, 'disablefield': ['agent_exchange', 'our_exchange', 'due_to_agent1'] });
          }
        }
        if (isResubmitButton === true) {
          if (pdaTotalArraty['.'] && pdaTotalArraty['.'].length > 0) {
            pdaTotalArraty['.'] && pdaTotalArraty['.'].map(e => {
              e['disablefield'] = ['cost_item', 'quoted', 'agreed_adv', 'axct_tax', 'attachment', 'remark']
            })
          }
          if (pdaTotalArraty['..'] && pdaTotalArraty['..']) {
            pdaTotalArraty['..'] = Object.assign({}, pdaTotalArraty['..'], { 'disablefield': ['advance_curr', 'our_exchange', 'agent_exchange'] });
          }
          if (pdaTotalArraty['--'] && pdaTotalArraty['--'].length > 0) {
            pdaTotalArraty['--'] && pdaTotalArraty['--'].map(e => {
              //e['disablefield'] = ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark']
            })
          }
          if (pdaTotalArraty['...'] && pdaTotalArraty['...']) {
            pdaTotalArraty['...'] = Object.assign({}, pdaTotalArraty['...'], { 'due_to_agent1': parseFloat(pdaTotalArraty['...']['due_to_agent']) > 0 ? 160 : 161, 'disablefield': ['agent_exchange', 'our_exchange', 'due_to_agent1'] });
          }
        }
      }
      this.setState({ ...this.state, loading: true, formData: pdaTotalArraty, tde_id: tde_id, TdeList: Tde_List != null && Tde_List.length > 0 ? Tde_List : null });

    } else {
      const port_call_data = editportexpData['port_call'];
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${port_call_data.vessel}`);
      const data = await response['data'];
      const responseData = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${port_call_data.agentdetails[0].agent_id}`);
      const data2 = await responseData['data'];
      const bank_id = data2['bank&accountdetails'] && data2['bank&accountdetails'][0] && data2['bank&accountdetails'][0]['benificiary_bank']
      if (isAgentView === true) {
        frmData['--'] = [{ 'disablefield': ['cost_item', 'total_quoted', 'total_agreed', 'fda_amount', 'diff', 'act_tax', 'attchment', 'remark'] }]
        frmData['..'] = { 'advance_curr': 205, 'pda_base_curr': 205, 'agent_exchange': 1.0, 'our_exchange': 1.0, 'due_to_agent': 0.00 }
        frmData['...'] = { 'agent_exchange': 1.0, 'our_exchange': 1.0, 'due_to_agent1': 160, 'disablefield': ['agent_exchange', 'due_to_agent1'] }
      }

      if (isAgentView === false) {
        frmData['.'] = []
        frmData['..'] = { 'advance_curr': 205, 'pda_base_curr': 205, 'agent_exchange': 1.0, 'our_exchange': 1.0, 'due_to_agent': 0.00, 'disablefield': ['agent_exchange', 'advance_curr', 'our_exchange'] }
        frmData['-'] = []
        frmData['--'] = []
        frmData['...'] = { 'agent_exchange': 1.0, 'our_exchange': 1.0, 'due_to_agent1': 160, 'disablefield': ['agent_exchange', 'our_exchange', 'due_to_agent', 'due_to_agent1'] }
      }

      if (port_call_data && data) {
        frmData['vessel'] = port_call_data.vessel
        frmData['vessel_code'] = data.vessel_code
        frmData['voyage_manager_id'] = port_call_data.voyage_manager_id
        frmData['port'] = port_call_data.portcalldetails && port_call_data.portcalldetails['port']
        frmData['agent_bank'] = port_call_data.agentdetails && port_call_data.agentdetails[0]['agent_id']
        frmData['agent_full_name'] = port_call_data.agentdetails && port_call_data.agentdetails[0]['agent_id']
        frmData['voyage_number'] = port_call_data.portcalldetails && port_call_data.portcalldetails.voyage_number
        frmData['departure'] = port_call_data.portcalldetails && port_call_data.portcalldetails.etd
        frmData['arrival'] = port_call_data.portcalldetails && port_call_data.portcalldetails.eta
        frmData['agent_short_name'] = port_call_data.agentdetails && port_call_data.agentdetails[0]['short_name']
        frmData['portcall_refer_no'] = port_call_data && port_call_data.port_call_ref_id
      }
      this.setState({ ...this.state, formData: frmData, loading: true });
    }
  }

  reportAPI = async (show) => {
    const { modelVisible, editportexpData, tdeList, tde_id } = this.state;
    const port_expense_id = editportexpData['port_expense'].id;
    const reponse = await getAPICall(`${URL_WITH_VERSION}/port-call/pereport?e=${port_expense_id}`)
    const reportData = await reponse['data'];
    if (reportData) {
      reportData['lob'] = reportData.company_lob
      if (show == 'showPdaInvoice') {
        this.setState({ ...this.state, reportData: reportData , isShowPdaInvoice: true })
      } else if (show == 'isShowPortExpenseReport') {
        this.setState({ ...this.state, reportData: reportData , isShowPortExpenseReport: true })
      } else if (show == 'isShowTdeShow') {
        if (editportexpData['port_expense']['disburmnt_inv']) {
          if (tde_id != 0) {
            const editData = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${tde_id}`);
            const tdeEditData = await editData['data']
            tdeEditData['lob'] = reportData.company_lob
            this.setState({ ...this.state, TdeList: tdeEditData })
          }
          this.setState({ ...this.state, isShowTdeShow: true, reportData: reportData  })
        } else {
          this.setState({ msg: `Without "Disburmnt INV" you cant't access the TDE Form`, modelVisible: !modelVisible })
        }

      } else {
        this.setState({ ...this.state, isShowFdaInvoice: true , reportData: reportData })
      }
    } else {
      this.setState({ msg: 'data not available', modelVisible: !modelVisible })
    }
  }

  //FdaInvoice = showFdaInvoice => this.setState({ ...this.state, isShowFdaInvoice: showFdaInvoice });
  //PdaInvoice = showPdaInvoice => this.setState({ ...this.state, isShowPdaInvoice: showPdaInvoice });
  TdeShow = showTdeShow => this.setState({ ...this.state, isShowTdeShow: showTdeShow });
  PortExpenseReport = showPortExpenseReport => this.setState({ ...this.state, isShowPortExpenseReport: showPortExpenseReport });

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  savePDAFormData = (data, innerCB) => {
    const { editportexpData, port_call_id } = this.state;
    data['port'] = editportexpData['port_call']['cargodetails']['port_id']
    //if (this.state.isAgentView === true) {
    if (data && data['.']) {
      if (data['.'].disablefield) {
        delete data['.'].disablefield;
      }
      if (data['.'].length > 0) {
        data['.'].map(e => {
          e['agreed_adv'] = e.agreed_adv;
          e['attachment'] = e.attachment;
          e['axct_tax'] = e.axct_tax;
          e['cost_item'] = e.cost_item;
          e['diff'] = e.diff;
          //e['exp_id'] = port_call_id;
          e['quoted'] = e.quoted;
          e['remark'] = e.remark;
          e['short_code'] = e.short_code;
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }

    if (data && data['..']) {
      delete data['..'].diff
      delete data['..'].pda_base_curr
      delete data['..'].total_adv
      delete data['..'].total_diff
      delete data['..'].total_quoted
      delete data['..'].total_tax
      delete data['..'].due_to_agent
      delete data['..'].agreed_adv
      delete data['..'].total_agreed_adv
      if (data['..'].disablefield) {
        delete data['..'].disablefield;
      }
      if (data['..'].length > 0) {
        data['..'].map(e => {
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }

    let savedata = {
      '.': data['.'],
      '..': data['..'],
      '...' :  data['...'], 
      '-' :  data['-'] ,
      '--' :  data['--'],
      "adv_per": data.adv_per,
      "agent_bank": data.agent_bank,
      "agent_full_name": data.agent_full_name,
      "agent_short_name": data.agent_short_name,
      "agreed_est_amt": data.agreed_est_amt,
      "arrival": data.arrival,
      "da_approved_fda_sent": data.da_approved_fda_sent,
      "departure": data.departure,
      "disburmnt_inv": data.disburmnt_inv,
      "exchange_rate": data.exchange_rate,
      "fda_base_curr": data.fda_base_curr,
      "fda_currency": data.fda_currency,
      "fda_sent_date": data.fda_inv_date,
      "fda_status": data.fda_status,
      "intial_est_expesnes": data.intial_est_expesnes,
      "other_advance": data.other_advance,
      "p_status": data.p_status,
      "pda_adv_amt": data.pda_adv_amt,
      "pda_adv_status": data.pda_adv_status,
      "pda_sent_approved": data.pda_sent_approved,
      "pda_inv_date": data.pda_inv_date,
      "pic_user": data.pic_user,
      "po_no": data.po_no,
      "port": data.port,
      "portcall_refer_no": data.portcall_refer_no,
      "total_amt": data.total_amt,
      "vessel": data.vessel,
      "vessel_code": data.vessel_code,
      "voy_no": data.voyage_number,
      "voyage_manager_id": data.voyage_manager_id,
      "port_call_id": port_call_id
    }
    this.onSave(savedata, innerCB)
    // } else {
    //   data['port_call_id'] = port_call_id
    //   this.onSave(data, innerCB)
    // }

  }

  onSave = (data, innercB) => {
    const { port_call_id } = this.state;
    postAPICall(`${URL_WITH_VERSION}/port-call/pesave?frm=port_expenses`, data, 'POST', response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.editPortExpense(port_call_id)
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  }

  editPortExpense = async (portexpID) => {
    const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portexpID}`)
    const editData = await Editreponse['data'];

     
    this.state.editportexpData = editData;
    this.state.formData = {};
    this.setState({ ...this.state, loading: false, editportexpData: editData, formData: {} })
    this.updatePortExpenseData(editData)
  }


  onUpdate = (data, update_status) => {
    const { editportexpData, port_call_id, isAgentView } = this.state;
    if (data.hasOwnProperty('other_advance')) {
      delete data['other_advance'];
    }
    if (data && data.disablefield) {
      delete data.disablefield;
    }
    //PDA Table
    if (data && data['.'] && data['.'].length > 0) {
      data['.'].map(e => {
        //  if(Math.sign(e['id']) == -1){
        //   delete e['editable']
        //   delete e['id']
        //   delete e['index']
        //   delete e['key']
        //   delete e['ocd']
        //  }
        if (e.disablefield) {
          delete e.disablefield;
        }
      })
    }
    //FDA First Table
    if (data && data['-']) {
      if (data['-'].disablefield) {
        delete data['-'].disablefield;
      }
      if (data['-'].length > 0) {
        data['-'].map(e => {
          e.pay_status = e.pay_status == 'PAID/RECIEVED' ? 208 : 207
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }
    //FDA second table
    if (data && data['--']) {
       
      if (data['--'].disablefield) {
        
        delete data['--'].disablefield;
      }
      if (data['--'].length > 0) {
        //'cost_item', 'quoted', 'axct_tax', 'attachment', 'remark'
        data['--'].map(e => {
          delete e['axct_tax']
          delete e['quoted']
          delete e['attachment']
          delete e['agreed_adv']
          delete e['key']
          delete e['index']
          delete e['editable']
          delete e['short_code']
          delete e['cost_item_name']
          // delete e['exp_id']
          // delete   e['id']
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }
    //PDA table below data
    if (data && data['..']) {
      delete data['..'].diff
      delete data['..'].pda_base_curr
      delete data['..'].total_adv
      delete data['..'].total_diff
      delete data['..'].total_quoted
      delete data['..'].total_tax
      delete data['..'].due_to_agent
      //delete data['..'].our_exchange
      delete data['..'].total_agreed_adv
      delete data['..'].total_axct_tax
      delete data['..'].agreed_est_amt
      if (data['..'].disablefield) {
        delete data['..'].disablefield;
      }
      if (data['..'].length > 0) {
        data['..'].map(e => {
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }
    //FDA below table
    if (data && data['...']) {
      //data['...']['our_exchange'] = parseFloat(data['...']['agent_exchange']) > 0 ? data['...']['agent_exchange'] : parseFloat(data['...']['our_exchange']) >  0 ? data['...']['our_exchange'] : 1.0
      //data['..']['agent_exchange'] = parseFloat(data['...']['agent_exchange']) > 0 ? data['...']['agent_exchange']  : 1.0
      delete data['...']['total_amt']
      //delete data['...']['agent_exchange']
      delete data['...']['due_to_agent']
      //delete data['...']['our_exchange']
      delete data['...']['total_paid']
      delete data['...']['total_fda']
      delete data['...']['diff']
      delete data['...']['due_to_agent1']
      // delete data['...']['exp_id']
      // delete data['...']['id']
      if (data['...'].disablefield) {
        delete data['...'].disablefield;
      }
      if (data['...'].length > 0) {
        data['...'].map(e => {
          if (e.disablefield) {
            delete e.disablefield;
          }
        })
      }
    }
    if (isAgentView === true) {
      if (editportexpData['port_expense']['pda_submit'] == 1 || editportexpData['port_expense']['fda_submit'] == 1) {
        if (data['.']) {
          delete data['.'];
        }
      }
      if (editportexpData['port_expense']['pda_submit'] == 0) {
        if (data['-']) {
          delete data['-'];
        }
        if (data['--']) {
          delete data['--'];
        }
        if (data['...']) {
          delete data['...'];
        }
      }
    }
    if (isAgentView === false) {
      if (editportexpData['port_expense']['fda_submit'] == 0) {
        if (data['-']) {
          delete data['-'];
        }
        if (data['--']) {
          delete data['--'];
        }
        if (data['...']) {
          delete data['...'];
        }
      }
      if (editportexpData['port_expense']['fda_submit'] == 1) {
        // if (data['-']) {
        //   delete data['-'];
        // }
        if (data['.']) {
          delete data['.'];
        }
        // if (data['--']) {
        //   data['--'].map(e=>{
        //     if(e.id == 77){
        //       e['total_quoted'] = '1000'
        //     }
        //     if(e.id == 78 || e.id == 79){
        //       e['total_quoted'] = '2000'
        //     }
        //   })
        // }
      }
    }
    data['port_call_id'] = port_call_id;
    data['voy_no'] = editportexpData['port_call']['voyage_manager_id']
    data['port'] = editportexpData['port_call']['cargodetails']['port_id']
    // data['p_status'] = 'SENT'
    // data['pda_adv_status'] = 145
    postAPICall(`${URL_WITH_VERSION}/port-call/peupdate?frm=port_expenses`, data, 'PUT', respo2 => {
      if (respo2 && respo2.data) {
        if (update_status === true) {
          this.updateFormData(editportexpData, port_call_id)
        } else {
          openNotificationWithIcon('success', respo2.message);
          this.editPortExpense(port_call_id)
        }
      } else {
        openNotificationWithIcon('error', respo2.message);
      }
    });
  }

  updateFormData = async (editportexpData, port_call_id) => {
    if (editportexpData['port_expense'].pda_submit == 0) {
      const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pda`);
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.editPortExpense(port_call_id)
      } else {
        openNotificationWithIcon('error', response.message);
      }
    }
    if (editportexpData['port_expense'].pda_submit == 1 && editportexpData['port_expense'].fda_submit == 0) {
      const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=fda`);
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.editPortExpense(port_call_id)
      } else {
        openNotificationWithIcon('error', response.message);
      }
    }
  }

  onDeleteFormData = (data, innerCB) => {
    const { saveIID } = this.state
    apiDeleteCall(`${URL_WITH_VERSION}/port-call/pedelete`, { "id": data.id }, respon2 => {
      if (respon2.data === true) {
        openNotificationWithIcon('success', respon2.message);
      } else {
        openNotificationWithIcon('error', respon2.message);
      }
    });
  };

  updateByUser = async (data, innerCB) => {
    const { port_call_id, resubmitModal } = this.state;
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/resubmit?e=${port_call_id}`);
    if (response && response.data) {
      openNotificationWithIcon('success', response.message);
      this.resubmitForm(!resubmitModal)
      this.editPortExpense(port_call_id)
    } else {
      openNotificationWithIcon('error', response.message);
    }
  }

  resubmitForm = (st) => {
    this.setState({ resubmitModal: st })
  }

  approveModal = (st, type) => {
    this.setState({ pdaApproveModal: st, approveType: type })
  }
  pdaApproved = async (data, innerCB) => {
    const { port_call_id, editportexpData } = this.state;
    let pe_id = editportexpData['port_expense']['id']
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pdasub`);
    if (response && response.data) {
      openNotificationWithIcon('success', response.message);
      this.editPortExpense(port_call_id)
    } else {
      openNotificationWithIcon('error', response.message);
    }
  }
  fdaApproved = async (data, innerCB) => {
    const { port_call_id, editportexpData } = this.state;
    let pe_id = editportexpData['port_expense']['id']
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=fdasub`);
    if (response && response.data) {
      openNotificationWithIcon('success', response.message);
      this.editPortExpense(port_call_id)
    } else {
      openNotificationWithIcon('error', response.message);
    }
  }

  submitPDAData = async (port_call_id, status, data) => {
    const { editportexpData, isAgentView, modelVisible } = this.state;
    let update_status = this.checkUpdateFields(data)
    if (update_status === true && data) {
      this.onUpdate(data, update_status)
    } else {
      if (status === true) {
        if (isAgentView === true) {
          const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=fda`);
          if (response && response.data) {
            openNotificationWithIcon('success', response.message);
            this.submitForm(false)
            this.editPortExpense(port_call_id)
          } else {
            openNotificationWithIcon('error', response.message);
          }
        } else {
          this.submitForm(false)
          this.props.reSubmitForm(port_call_id)
        }
      } else {
        if (editportexpData['port_expense'].pda_submit == 0) {
          const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pda`);
          if (response && response.data) {
            openNotificationWithIcon('success', response.message);
            this.submitForm(false)
            this.editPortExpense(port_call_id)
          } else {
            openNotificationWithIcon('error', response.message);
          }
        }
        if (editportexpData['port_expense'].pda_submit == 1 && editportexpData['port_expense'].fda_submit == 0) {
          const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=fda`);
          if (response && response.data) {
            openNotificationWithIcon('success', response.message);
            this.submitForm(false)
            this.editPortExpense(port_call_id)
          } else {
            openNotificationWithIcon('error', response.message);
          }
        }
      }
    }
  }

  checkUpdateFields = (form_data) => {
    const { editportexpData, modelVisible } = this.state;
    let portExpenseData = editportexpData['port_expense']
    if (portExpenseData == -1) {
      this.submitForm(!modelVisible)
    } else {
      let true_objectValues = object_equals(portExpenseData, form_data)
      if (true_objectValues == false) {
        if (portExpenseData['.'].length > 0) {
          return true
        } else {
          this.submitForm(!modelVisible)
        }
      }
    }
    return false
  }

  submitForm = (st, status) => {
    const { port_call_id } = this.state;
    if (status === true) {
      this.props.submitForm(port_call_id)
    } else {
      this.setState({ modelVisible: st })
    }
  }

  notFillAllData = (port_call_id, innerCB) => {
    const { editportexpData } = this.state;
    if (editportexpData['port_info'] != -1 && editportexpData['port_activity'] != -1 && editportexpData['port_expense'] != -1) {
      if (editportexpData['port_expense']['pda_submit'] == 1 && editportexpData['port_expense']['fda_submit'] == 1) {
        this.props.submitForm(port_call_id, innerCB)
      } else {
        openNotificationWithIcon('error', 'Please submit PDA/FdA data');
      }
    } else {
      openNotificationWithIcon('error', 'Please fill all tabs informations');
    }
  }

  saveUpdateClose = () => {
    const { port_call_id } = this.state;
    this.editPortExpense(port_call_id)
    this.setState({ isShowTdeShow: false })
  }

  closeTdeModal = async (st) => {
    const { isAgentView, formData, editportexpData, tdeList } = this.state;
    let frmData = {}
    let tde_array = []
    if (tdeList) {
      if (isAgentView === false) {
        if (editportexpData['port_expense'] != -1) {
          if (tdeList.length > 0) {
            let TdeList = tdeList.filter(el => editportexpData['port_expense']['disburmnt_inv'] == el.invoice_no);
            if (TdeList.length > 0) {
              let tde_id = TdeList[0]['id']
              const editData = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${tde_id}`);
              const tdeEditData = await editData['data']
              this.setState({ ...this.state, tde_id: tde_id, TdeList: tdeEditData })
            }
          }
        }
      }
    }
    this.setState({ isShowTdeShow: false })
  }

  onClickExtraIcon = async (action, data) => {
    const { editportexpData, isAgentView, formData, port_call_id } = this.state
    let port_expense = editportexpData['editportexpData']
    //http://127.0.0.1:5000/api/v1/port-call/perdelete
    let groupKey = action['gKey']
    let frm_code = ''
    if (isAgentView === false) {
      if (groupKey == '.') {
        frm_code = 'port_expense_pda_port_expense_adv'
      }
      if (groupKey == '--') {
        frm_code = 'port_expense_fda'
      }
    } else if (isAgentView === true) {
      if (groupKey == '.') {
        frm_code = 'PDA_ADV_AGENT'
      }
      if (groupKey == '--') {
        frm_code = 'FDA_AGENT'
      }
    }
    let delete_id = data && data.id
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        "id": delete_id,
        "frm_code": frm_code,
        "group_key": groupKey
      }
      postAPICall(`${URL_WITH_VERSION}/tr-delete`, data1, 'delete', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);

          //  this.editPortExpense(port_call_id)
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
    }
  }

  updateMesage = () => {
    this.setState({ msg: 'please fill PDA/ADV table data', modelVisible: true })
  }


  msgReturn = (type) => {
    return (
      type == 'pda' ?
        <div>
          <p>Pls fill below field for PDA approval</p>
          <ul>
            <li>Disburmnt INV</li>
            <li> Agent Bank</li>
            <li>PDA Inv Due Date </li>
            <li> PDA/Adv Amt</li></ul>
        </div>
        :
        <div>
          {(type['fda_submit'] === 1 && !type['fda_inv_date']) ? <p>Please fill our fields for FDA Approved </p>
            : <p>Pls request agent to submit the FDA </p>}
          {type['fda_submit'] === 1 && !type['fda_inv_date'] && <ul><li>FDA Inv Date</li></ul>}
        </div>
    )
  }

  onRowDeletedClick = async (id) => {
    const { editportexpData } = this.state
    if (editportexpData['port_expense']['id'] === id) {
      let port_id = editportexpData['port_call']['id']
      let _url = `${URL_WITH_VERSION}/port-call/delete`;
      apiDeleteCall(_url, { id: port_id }, response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.props.history.push('/my-portcall');
          setInterval(() => {
            window.location.reload(false);
          }, 2000);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
    }
  };


  render() {
    const {
      isShowFdaInvoice,
      isShowPdaInvoice,
      isShowTdeShow,
      loading,
      frmName,
      formData,
      isShowPortExpenseReport,
      port_call_id,
      isAgentView,
      editportexpData,
      modelVisible,
      reportData,
      msg,
      TdeList,
      resubmitModal,
      pdaApproveModal,
      approveType,
      isResubmitButton,
      deleteModal,
      delete_port_id
    } = this.state;
    let update = editportexpData['port_expense'] != -1 ? true : false;
    let pda_update = editportexpData['port_expense'] != -1 ? editportexpData['port_expense']['pda_submit'] == 1 ? true : false : false
    let fda_update = editportexpData['port_expense'] != -1 ? editportexpData['port_expense']['fda_submit'] == 1 ? true : false : false
    let freez_button = editportexpData['port_expense'] != -1 ? editportexpData['port_expense']['pda_submit'] == 1 && editportexpData['port_expense']['fda_submit'] == 1 ? true : false : false
    let disabled_tbs = {}
    if (isResubmitButton === true) {
      //disabled_tbs = [{ 'tabName': 'PDA/ADV', 'groupKey': '.', 'message': 'PDA Submit' }, { 'tabName': 'FDA', 'groupKey': '--', 'message': 'FDA Submit mmm' }]
    } else if (isAgentView === true) {
      if (pda_update === false) {
        disabled_tbs = [{ 'tabName': 'FDA', 'groupKey': '--', 'message': 'PDA Not Submit By You' }]
      } else if (pda_update === true && fda_update === false) {
        disabled_tbs = [{ 'tabName': 'PDA/ADV', 'groupKey': '.', 'message': 'PDA Submit' }]
      } else if (pda_update === true && fda_update === true) {
        disabled_tbs = [{ 'tabName': 'PDA/ADV', 'groupKey': '.', 'message': 'PDA Submit' }, { 'tabName': 'FDA', 'groupKey': '--', 'message': 'FDA Submit  kkk' }]
      }
    } else {
      if (pda_update === false) {
        disabled_tbs = [{ 'tabName': 'PDA/ADV', 'groupKey': '.', 'message': 'PDA Not Submit By Agent' }, { 'tabName': 'FDA', 'groupKey': '--', 'message': 'FDA Not Submit By Agent' }]
      } else if (pda_update === true && fda_update === false) {
        disabled_tbs = [{ 'tabName': 'FDA', 'groupKey': '--', 'message': 'FDA Not Submit By Agent' }]
      } else if (fda_update === true) {
        disabled_tbs = [{ 'tabName': 'PDA/ADV', 'groupKey': '.', 'message': 'PDA Submit' }]
      }
    }
    return (
      <>
        <div className="wrap-rightbar full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">

                    <article className="article toolbaruiWrapper">
                      <div className="box box-default">
                        <div className="box-body">
                          {(loading === true) &&
                            <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formData}
                              showForm={true} frmCode={frmName} addForm={true}
                              showButtons={
                                isAgentView === true ?
                                  freez_button == false ?
                                    [
                                      { "id": `${update === true ? "update" : "save"}`, "title": `${update === true ? "Update" : "Save"}`, "type": "primary", "event": (data, innerCB) => { update === true ? this.onUpdate(data) : this.savePDAFormData(data, innerCB) } },
                                      { "id": "submit", "title": pda_update === true ? 'FDA submit' : 'PDA submit', "type": "success", "event": (data, innerCB) => { formData['.'] && formData['.'].length > 0 ? pda_update === true && fda_update == false ? this.submitForm(!modelVisible) : this.submitPDAData(port_call_id, freez_button, data) : this.updateMesage() } }
                                    ]
                                    :
                                    [
                                      { "id": `${update === true ? "update" : "save" }`, "title": `${update === true ? "Update" : "Save" }`, "type": "primary", "event": (data, innerCB) => { update === true ? this.onUpdate(data, innerCB) :this.savePDAFormData(data, innerCB) } },
                                      { "id": "submit", "title": 'submit', "type": "success", "event": (data, innerCB) => { this.submitForm(!modelVisible, true) } }
                                    ]
                                  :
                                  isAgentView === false && (pda_update === true || fda_update === true) &&
                                  [
                                    { "id": `${update === true ? "update" : "save"}`, "title": `${update === true ? "Update" : "Save"}`, "type": "primary", "event": (data, innerCB) => { update === true ? this.onUpdate(data) : this.savePDAFormData(data, innerCB) } },
                                    formData['fda_status'] == 144 && formData['pda_adv_status'] == 144 ?
                                      { "id": "resave", "title": "resubmit", "type": "disabled", "event": (data, innerCB) => { } }
                                      :
                                      { "id": "resave", "title": "resubmit", "type": "primary", "event": (data, innerCB) => { this.resubmitForm(!resubmitModal) } },
                                    formData['p_status'] != 212 && formData['pda_adv_status'] != 144 ?
                                      {
                                        "id": "save", "title": "PDA Approved", "type": "primary", "event": (data, innerCB) => {
                                          formData['disburmnt_inv'] && formData['pda_inv_date'] && formData['agent_bank'] && (parseFloat(formData['pda_adv_amt']) > 0) ?
                                            this.pdaApproved(data, innerCB)
                                            :
                                            this.approveModal(!pdaApproveModal, 'pda')
                                        }
                                      }
                                      :
                                      { "id": "pdasave", "title": "PDA Approved", "type": "disabled", "event": (data, innerCB) => { } },
                                    

                                      
                                    
                                      formData['p_status'] == 212 && formData['pda_adv_status'] == 144 && formData['fda_status'] != 144 ?
                                      {
                                        "id": "fdasave", "title": "FDA Approved", "type": "primary", "event": (data, innerCB) => {
                                          formData['fda_inv_date'] && fda_update === true ?
                                            this.fdaApproved(data, innerCB)
                                            :
                                            this.approveModal(!pdaApproveModal, 'fda')
                                        }
                                      }
                                      :
                                      { "id": "fdasave", "title": "FDA Approved", "type": "disabled", "event": (data, innerCB) => { } }
                                  ]
                              }
                              showToolbar={
                                [
                                  {
                                    isLeftBtn:
                                      [{
                                        isSets: [
                                          { id: '3', key: 'save', type: <SaveOutlined />, withText: '', "event": (data, innerCB) => { freez_button === true ? fda_update === true ? this.submitForm(!modelVisible, true) : this.props.submitForm(port_call_id, innerCB) : this.notFillAllData(port_call_id, innerCB) } },
                                          isAgentView === false && { id: '4', key: 'delete', type: <DeleteOutlined />, withText: '', 'event': (data, innerCB) => this.setState({ deleteModal: true, delete_port_id: formData['id'] }) }
                                        ],
                                      }]
                                    ,
                                    isRightBtn: [
                                      {
                                        isSets: [
                                          {
                                            key: 'report',
                                            isDropdown: 0,
                                            withText: 'Report',
                                            type: '',
                                            menus: null,
                                            event: key => this.reportAPI('isShowPortExpenseReport'),
                                          },
                                          isAgentView === false && {
                                            key: 'invoice',
                                            isDropdown: 1,
                                            withText: 'Create Invoice',
                                            type: '',
                                            menus: [
                                              {
                                                href: null,
                                                icon: null,
                                                label: 'PDA Invoice',
                                                modalKey: 'pda_invoice',
                                                event: key => this.reportAPI('showPdaInvoice'),

                                              },
                                              {
                                                href: null,
                                                icon: null,
                                                label: 'FDA Invoice',
                                                modalKey: 'search_tci',
                                                event: key => this.reportAPI('showFdaInvoice'),
                                              },
                                            ],
                                          },
                                          {
                                            key: 'attachment',
                                            isDropdown: 0,
                                            withText: ' Attachment',
                                            type: '',
                                            menus: null,
                                          },
                                          isAgentView === false && {
                                            key: 'tde',
                                            isDropdown: 0,
                                            withText: ' TDE',
                                            type: '',
                                            menus: null,
                                            event: key => this.reportAPI('isShowTdeShow'),
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ]}
                              summary={[
                                { "gKey": ".", "showTotalFor": ["quoted", "agreed_adv", "diff", "axct_tax"] },
                                { "gKey": "--", "showTotalFor": ["total_quoted", "total_agreed", "fda_amount", "diff", "act_tax"] }
                              ]}
                              inlineLayout={true}
                              tabEvents={[
                                {
                                  tabName: 'PDA/ADV',
                                  event: {
                                    type: 'rename-button',
                                    key: 'submit',
                                    title: isAgentView == true && freez_button ? 'submit' : pda_update === true ? "FDA Submit iiii" : "PDA Submit"
                                  }
                                },
                                {
                                  tabName: 'FDA',
                                  event: {
                                    type: 'rename-button',
                                    key: 'submit',
                                    title: isAgentView == true && freez_button ? 'submit' : pda_update === true ? "FDA Submit  ffff" : "PDA Submit"
                                  }
                                }
                              ]}
                              disableTabs={disabled_tbs}
                              tableRowDeleteAction={(action, data) => this.onClickExtraIcon(action, data)}

                            // extraTableButton={{
                            //   '.': [
                            //     { icon: 'delete', class: 'cancel', onClickAction: (action, data) => this.onClickExtraIcon(action, data, 'PDA') }
                            //   ],
                            //   '--': [
                            //     { icon: 'delete', class: 'cancel', onClickAction: (action, data) => this.onClickExtraIcon(action, data, 'FDA') }
                            //   ]
                            // }}
                            />
                          }
                        </div>
                      </div>
                    </article>
                    {modelVisible &&
                      <Modal
                        title="Alert"
                       open={modelVisible}
                        onOk={() => (msg == 'please fill PDA/ADV table data' || !formData['disburmnt_inv']) ? this.submitForm(!modelVisible) : this.submitPDAData(port_call_id, freez_button)}
                        onCancel={() => this.submitForm(!modelVisible)}
                      >
                        <p>{msg}</p>
                      </Modal>
                    }

                    <Modal
                      title="Confirmation"
                     open={resubmitModal}
                      onOk={() => this.updateByUser()}
                      onCancel={() => this.resubmitForm(!resubmitModal)}
                    >
                      <p>{fda_update === true ? 'Are You Sure You Want to Resubmit PDA/FDA to Agent' : 'Are You Sure You Want to Resubmit PDA to Agent'}</p>
                    </Modal>

                    <Modal
                     open={pdaApproveModal}
                      onOk={() => this.approveModal(!pdaApproveModal)}
                      onCancel={() => this.approveModal(!pdaApproveModal)}
                    >
                      {approveType == 'pda' ? this.msgReturn('pda') : this.msgReturn(formData)}
                    </Modal>

                    <Modal
                      title="Alert"
                     open={deleteModal}
                      onOk={() => formData && formData['-'] ? this.setState({ deleteModal: false }) : this.onRowDeletedClick(delete_port_id)}
                      onCancel={() => this.setState({ deleteModal: false })}
                    >
                      <p>{formData && formData['-'] ? 'Can not Delete this Port-Call Appoinment' : 'Are sure Delete this Port-Call Appoinment'}</p>
                    </Modal>

                  </div>
                </div>
              </Content>
            </Layout>

          </Layout>

          {isShowFdaInvoice ? (
            <Modal
              style={{ top: '2%' }}
              title="FDA Invoice"
             open={isShowFdaInvoice}
              onOk={this.handleOk}
              onCancel={() => this.setState({ isShowFdaInvoice: false })}
              width="95%"
              footer={null}
            >
              <CreateInvoice  type={"fdaInvoice"} PortExpensePDA={reportData} />
            </Modal>
          ) : (
            undefined
          )}

          {isShowPdaInvoice ? (
            <Modal
              className="page-container"
              style={{ top: '2%' }}
              title="PDA Invoice"
             open={isShowPdaInvoice}
              onOk={this.handleOk}
              onCancel={() => this.setState({ isShowPdaInvoice: false })}
              width="95%"
              footer={null}
            >
              <CreateInvoice type={"pdaInvoice"} PortExpensePDA={reportData}  />
            </Modal>
          ) : (
            undefined
          )}

          {isShowTdeShow ? (
            <Modal
              style={{ top: '2%' }}
              title="Tde"
             open={isShowTdeShow}
              onOk={this.handleOk}
              onCancel={() => this.closeTdeModal(false)}
              width="95%"
              footer={null}
            >
              <Tde invoiceType="port_expense" isEdit={TdeList == null ? false : true} formData={TdeList != null && TdeList.id ? TdeList : reportData } PortExpense={editportexpData} deleteTde = {()=> this.saveUpdateClose()}saveUpdateClose={() => this.saveUpdateClose()} modalCloseEvent={() => this.closeTdeModal(false)} />
            </Modal>
          ) : (
            undefined
          )}

          {isShowPortExpenseReport ? (
            <Modal
              style={{ top: '2%' }}
              title="Report"
             open={isShowPortExpenseReport}
              onOk={this.handleOk}
              onCancel={() => this.setState({ isShowPortExpenseReport: false })}
              width="95%"
              footer={null}
            >
              <PortExpenseReport PortExpense={reportData} />
            </Modal>
          ) : (
            undefined
          )}
        </div>

      </>
    );
  }
}

export default PortExpense;
