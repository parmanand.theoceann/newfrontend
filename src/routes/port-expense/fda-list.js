import React, { Component } from 'react';
import { Table, Popconfirm, Input, Button, Row, Col, Form, Select } from 'antd';
import { SaveOutlined,DeleteOutlined , EditOutlined} from '@ant-design/icons';

// Start table section
const data = [];
for (let i = 0; i < 6; i++) {
  data.push({
    key: i.toString(),
    costitems: 'Cost Items',
    quoted: 'Quoted',
    agreedadv: 'Agreed Adv',
    diff: 'Diff',
    axcttax: 'Axct Tax',
    remark: 'Remark',
    attachment: 'Attachment',
  });
}

const { Column } = Table;
const dataSet = [
  {
    costitemstotal: 'Total FDA',
    quotetotal: '0.00',
    agreedtotal: '0.00',
    difftotal: '0.00',
    axcttotal: '0.00',
    remarktotal: '0.00',
  },
];

const dataSourcePay = [
  {
    key: '1',
    invtype: 'Inv Type',
    invno: 'Inv No',
    invdate: 'Inv Date',
    duedate: 'Due Date',
  },
  {
    key: '1',
    invtype: 'Inv Type',
    invno: 'Inv No',
    invdate: 'Inv Date',
    duedate: 'Due Date',
  },
  {
    key: '1',
    invtype: 'Inv Type',
    invno: 'Inv No',
    invdate: 'Inv Date',
    duedate: 'Due Date',
  },
  {
    key: '1',
    invtype: 'Inv Type',
    invno: 'Inv No',
    invdate: 'Inv Date',
    duedate: 'Due Date',
  },
];

const columnsPay = [
  {
    title: 'Inv Type',
    dataIndex: 'invtype',
    key: 'invtype',
  },
  {
    title: 'Amount (USD)',
    dataIndex: 'amuntusd',
    key: 'amuntusd',
  },
  {
    title: 'Inv No',
    dataIndex: 'invno',
    key: 'invno',
  },
  {
    title: 'Inv Date',
    dataIndex: 'invdate',
    key: 'invdate',
  },
  {
    title: 'Due Date',
    dataIndex: 'duedate',
    key: 'duedate',
  },
];

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const InputGroup = Input.Group;
const FormItem = Form.Item;
const Option = Select.Option;

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable ? (
      <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
    ) : (
      value
    )}
  </div>
);
// End table section

class FDAList extends Component {
  constructor(props) {
    super(props);
    // Start table section
    this.columns = [
      {
        title: 'Cost items',
        dataIndex: 'costitems',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'costitems'),
      },
      {
        title: 'Ttl Quoted',
        dataIndex: 'ttlquoted',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'ttlquoted'),
      },
      {
        title: 'Ttl Agreed',
        dataIndex: 'ttlagreed',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'ttlagreed'),
      },
      {
        title: 'FDA Amount',
        dataIndex: 'fdaamount',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'fdaamount'),
      },
      {
        title: 'Diff',
        dataIndex: 'diff',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'diff'),
      },
      {
        title: 'Act Tax.',
        dataIndex: 'acttax',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'acttax'),
      },
      {
        title: 'attchment',
        dataIndex: 'attachment',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'attachment'),
      },
      {
        title: 'remark',
        dataIndex: 'remark',
        // width: 150,
        render: (text, record) => this.renderColumns(text, record, 'remark'),
      },
      {
        title: 'Action',
        dataIndex: 'action',
        width: 100,
        fixed: 'right',
        render: (text, record) => {
          const { editable } = record;
          return (
            <div className="editable-row-operations">
              {editable ? (
                <span>
                  <span className="iconWrapper save" onClick={() => this.save(record.key)}>
                  <SaveOutlined />

                  </span>
                  <span className="iconWrapper cancel">
                    <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                    <DeleteOutlined />

                    </Popconfirm>
                  </span>
                </span>
              ) : (
                <span className="iconWrapper edit" onClick={() => this.edit(record.key)}>
              <EditOutlined />

                </span>
              )}
            </div>
          );
        },
      },
    ];
    this.state = { data };
    this.cacheData = data.map(item => ({ ...item }));
    // End table section
  }

  // Start table section
  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  edit(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ data: newData });
    }
  }

  save(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  cancel(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ data: newData });
    }
  }
  // End table section

  render() {
    return (
      <>
        <Table
          bordered
          dataSource={this.state.data}
          columns={this.columns}
          scroll={{ y: 205 }}
          className="activeScrollWrap"
          size="small"
          pagination={false}
          // title={() => <div className="bold-title-wrapper">
          // <span>Cargoes</span>
          // </div>
          // }
          footer={() => (
            <div className="text-center">
              <Button type="link">Add New</Button>
            </div>
          )}
        />

        <Table
          size="small"
          className="hidecolumn-wrap"
          bordered
          pagination={false}
          dataSource={dataSet}
        >
          <Column title="Cost Items Total" dataIndex="costitemstotal" width="80px" />
          <Column title="Quote Total" dataIndex="quotetotal" width="80px" />
          <Column title="Agreed ADV. Total" dataIndex="agreedtotal" width="80px" />
          <Column title="Diff Total" dataIndex="difftotal" width="80px" />
          <Column title="Axct Tax Total" dataIndex="axcttotal" width="80px" />
          <Column title="Remark Total" dataIndex="remarktotal" width="80px" />
          <Column title="Attchment Total" dataIndex="attachmentotal" width="80px" />
          <Column title="Attchment Total" dataIndex="attachmentotal1" width="80px" />
          <Column title="action" dataIndex="action" width="50px" />
        </Table>

        {/* <div className="common-fields-wrapper m-t-2">
<Row gutter={16}>
<Col xs={24} sm={24} md={6} lg={6} xl={6}>
<FormItem
{...formItemLayout}
label="Total FDA">
<InputGroup compact>
<Input size="default" style={{ width: '70%' }} placeholder="" />
<Input size="default" style={{ width: '30%' }} placeholder="USD" disabled />
</InputGroup>
</FormItem>
</Col>
</Row>
</div> */}

        <hr />

        <div className="common-fields-wrapper m-t-2">
          <Row gutter={16}>
            <Col xs={24} sm={24} md={16} lg={16} xl={16}>
              <Table
                size="small"
                bordered
                pagination={false}
                columns={columnsPay}
                dataSource={dataSourcePay}
              />

              <div className="common-fields-wrapper m-t-2">
                <Row gutter={16}>
                  <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                    <FormItem {...formItemLayout} label="Total Paid">
                      <InputGroup compact>
                        <Input size="default" style={{ width: '60%' }} placeholder="" />
                        <Input size="default" style={{ width: '40%' }} placeholder="USD" disabled />
                      </InputGroup>
                    </FormItem>
                  </Col>
                </Row>
              </div>
            </Col>

            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
              <FormItem {...formItemLayout} label="Our Exchange">
                <Input size="default" defaultValue="" />
              </FormItem>

              <FormItem {...formItemLayout} label="Agent Exchange">
                <Input size="default" defaultValue="" />
              </FormItem>

              <FormItem {...formItemLayout} label="Total FDA">
                <InputGroup compact>
                  <Input size="default" style={{ width: '60%' }} placeholder="" />
                  <Input size="default" style={{ width: '40%' }} placeholder="USD" disabled />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Due to Agent">
                <InputGroup compact>
                  <Input size="default" style={{ width: '60%' }} placeholder="" />
                  <Select style={{ width: '40%' }} defaultValue="Payable">
                    <Option value="1">Payable</Option>
                    <Option value="2">Receivable</Option>
                  </Select>
                </InputGroup>
              </FormItem>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default FDAList;
