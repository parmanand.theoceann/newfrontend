import React, { Component } from 'react';
import { Table,  Modal } from 'antd';
import URL_WITH_VERSION, { getAPICall, ResizeableTitle, openNotificationWithIcon, objectToQueryStringFunc} from '../../shared';
import PortExpense from '../port-expense';

class PortExpenseList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
               {
                title: 'Agent Name',
                dataIndex: 'agent_name',
              },
              {
                title: 'portcall Refer No',
                dataIndex: 'port_call_ref_id',
                render: (text, row, index, data) => {
                    return <a href='#' style={{color: "#1890ff"}} onClick={e => this.edagenrowRedirect(e, row, data)}>{text}</a>;
                  },
              },
              {
                title:'Port Status',
                dataIndex:'port_status'
        
              },
              {
                title: 'Port Name',
                dataIndex: 'portcall_port_name',
              },
              {
                title: 'Created Date',
                dataIndex: 'created_on',
              }
      ],
      modals: { showBirthInformation: false, },
      responseData : [],
      pageOptions: { pageIndex: 1, pageLimit: 40, totalRows: 0 },
      isShowportExpense:false,
      show_port_call_for_user:[],
      editportexpData:[]
    };
  }

  componentDidMount = async() => {
    this.showPortDetails()
    this.getTableData()
  }
  portExpense = showPopup => this.setState({ ...this.state, isShowportExpense: showPopup });
  showPortDetails = async () => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/list?us=1`);
    const data = await response['data'];
    this.setState({...this.state, show_port_call_for_user : data})
   }

  edagenrowRedirect = async (event, row, data) => {
    const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${row.id}`)
    const editData = await Editreponse['data'];
    this.setState({ isShowportExpense : true, editportexpData : editData })
  };


  getTableData = async(search = {}) => {
     const voyID = this.props.voyID && this.props.voyID ? this.props.voyID : null;
    
    // const response = await getAPICall(`${URL_WITH_VERSION}/port-call/list`);
    // const responseData = response['data']
    // let newArray = [];
    //  if(responseData){
    //      if(voyID){
    //         responseData.map(e=>{ 
    //             if(voyID == e.voyage_manager_id){
    //                newArray.push(e)
    //                this.setState({...this.state, responseData:newArray})
    //             }
    //         })
    //      }else{
    //         this.setState({...this.state, responseData:responseData})  
    //      }
    // }
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/port-call/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    let newArray = [];

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
   
    if (dataArr.length > 0) {
        if(voyID){
          dataArr.map(e=>{ 
                if(voyID == e.voyage_manager_id){
                  
                   newArray.push(e)
                 
                 }
            })
             state['responseData'] = newArray
        }else{
          state['responseData'] = dataArr;
        }
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false
    });
  }


  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  submitFormData = async(port_call_id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pc`);
    if (response && response.data) {
        openNotificationWithIcon('success', response.message);
          setInterval(() => {
            window.location.reload(false);
          }, 2000);
      } else {
        openNotificationWithIcon('error', response.message);
      }
  }

  reSubmitForm = async(port_call_id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/resubmit?e=${port_call_id}`);
     if (response && response.data) {
      openNotificationWithIcon('success', response.message);
        setInterval(() => {
          window.location.reload(false);
        }, 2000);
    } else {
      openNotificationWithIcon('error', response.message);
    }
  }

  render() {
    const { columns, responseData, pageOptions, search, show_port_call_for_user, isShowportExpense, editportexpData } = this.state;
    const tableColumns = columns
    .filter(col => (col && col.invisible !== 'true' ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: this.handleResize(index),
      }),
    }));
  
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Port Expense List</span>
                  </h4>
                </div>
              </div>
              {/* <ToolbarUI
                    routeUrl={'port-info-list-toolbar'}
                    optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                    callback={e => this.callOptions(e)}
                /> */}
              <div className="row">
                <div className="col-md-12">
                  <Table
                    bordered
                    size="small"
                    scroll={{ x: 'max-content' }}
                    columns={tableColumns}
                    dataSource={responseData}
                    pagination={false}
                    footer={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
        {isShowportExpense ? (
            <Modal
              className="page-container"
              style={{ top: '2%' }}
              title="Port Expense "
             open={isShowportExpense}
              onOk={this.handleOk}
              onCancel={() => this.portExpense(false)}
              width="90%"
              footer={null}
            >
               <PortExpense voyID={this.props.voyID} isUserView={show_port_call_for_user} isAgentView={false} editportexpData={editportexpData} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm} />
            </Modal>
          ) : (
            undefined
          )}
      </div>
    );
  }
}

export default PortExpenseList;
