import React, { Component } from 'react';
import { Modal } from 'antd';
import PortActivityReport from '../operation-reports/PortActivityReport';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  postAPICall,
  apiDeleteCall,
} from '../../shared';

class PortActivityList extends Component {
  constructor(props) {
    super(props);
    // Start table section
      this.state = {
      frmName: "port_activity_form",
      formData: {},
      loading: true,
      modals: {
        PortActivityReport: false,
      },
      editportexpData: this.props.editportexpData,
      port_call_id:this.props.editportexpData && this.props.editportexpData['port_call'] && this.props.editportexpData['port_call']['id'],
      isUserView : this.props.isUserView,
      isResubmitButton : false,
      isAgentView : this.props.isAgentView === true ? true : false,
      reportData:[],
      modelVisible:false
    };
     // End table section
  }

  componentDidMount = async () => {
    const { editportexpData } = this.state;
    this.updateFormData(editportexpData)
    this.showPortDetails()
  }

  showPortDetails = () => {
    const { isUserView, editportexpData } = this.state;
    let port_call_ref_id = editportexpData['port_call']['port_call_ref_id']
    if(isUserView.length > 0){
      isUserView.map(val=>{
        if(val.port_call_ref_id == port_call_ref_id){
          this.setState({ isResubmitButton : true})
        }
      })
    }

  }

  updateFormData = (editportexpData) => {
    const { isAgentView} = this.state;
    let frmData = {};  
   
    if(editportexpData && editportexpData['port_activity'] != -1){
      if(isAgentView === false){
        if(editportexpData['port_activity']['-'] && editportexpData['port_activity']['-'].length > 0){
            editportexpData['port_activity']['-'].map(e=>{
             e['disablefield'] = ['activity', 'at', 'bl_code', 'cargo', 'date_from', 'remarks', 'time'] 
         })
        }else{  editportexpData['port_activity']['-'] = [] } 
        editportexpData['port_activity'] = Object.assign({}, editportexpData['port_activity'],{'disablefield' : ['arrival_draft_aft_m', 'arrival_draft_fwd_m', 'depart_draft_aft_m', 'depart_draft_fwd_m', 'last_update_dt']})
      }
      this.setState({ ...this.state, loading: false, formData : editportexpData['port_activity'] });
    }else{
      if(isAgentView === false){
        frmData['-'] = []
        frmData = Object.assign({}, frmData ,{'disablefield' : ['arrival_draft_aft_m', 'arrival_draft_fwd_m', 'depart_draft_aft_m', 'depart_draft_fwd_m', 'last_update_dt']})
      }
      frmData['port_name'] = editportexpData['port_call']['portcalldetails']['port']
      this.setState({ ...this.state, formData :frmData, loading: false });
    }

  }

  showHideModal = async(visible, modal) => {
    const { modals, port_call_id, modelVisible } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  onDelete = (key) => {
    const dataSource1 = [...this.state.dataSource1];
    this.setState({ dataSource1: dataSource1.filter(item => item.key !== key) });
  }

  editPortActivity = async(portexpID) => {
    const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portexpID}`)
    const editData = await Editreponse['data'];
    this.state.editportexpData = editData;
    this.state.formData = {};
    this.setState({ ...this.state, loading: true, editportexpData: editData, formData: {} })
    this.updateFormData(editData)
  }

  saveFormData = (data, innerCB) => {
    const { frmName, port_call_id } = this.state;
    //data['last_update_dt'] = moment().format('dd-mm-yyyy');
    if(port_call_id){
      data['port_call_id'] = port_call_id;
    }
    if(data['.']) {
      delete data['.']
    }
    if(data['..']){
      delete data['..']
    }
    if(data['...']){
      delete data['...']
    }
    if(data['id']){
       postAPICall( `${URL_WITH_VERSION}/port-call/pa/update?frm=${frmName}`, data, 'PUT', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.editPortActivity(port_call_id)
           } else {
          openNotificationWithIcon('error', response.message);
        }
    });
    }else{
        postAPICall( `${URL_WITH_VERSION}/port-call/pa/save?frm=${frmName}`, data, 'POST', response => {
          if (response && response.data) {
            this.editPortActivity(port_call_id)
            openNotificationWithIcon('success', response.message);
          } else {
            openNotificationWithIcon('error', response.message);
          }
        });
      }
  }

  onDeleteFormData = (data, innerCB) => {
    const { saveIID } = this.state
    apiDeleteCall(`${URL_WITH_VERSION}/port-call/pa/delete`, { "id": data.id }, respon2 => {
      if (respon2.data === true) {
        openNotificationWithIcon('success', respon2.message);
      } else {
        openNotificationWithIcon('error', respon2.message);
      }
    });
  };

  notFillAllData = (port_call_id, innerCB) => {
    const { editportexpData } = this.state;
    if( editportexpData['port_info'] != -1 &&  editportexpData['port_activity'] != -1 && editportexpData['port_expense'] != -1){
      if(editportexpData['port_expense']['pda_submit'] == 1 && editportexpData['port_expense']['fda_submit'] == 1){
        this.props.submitForm(port_call_id, innerCB)
      }else{
        openNotificationWithIcon('error', 'Please submit PDA/FdA data');
      }
    }else{
      openNotificationWithIcon('error', 'Please fill all tabs informations');
    }
  }

  render() {
    const { frmName, formData, loading, isResubmitButton, isAgentView, editportexpData,modelVisible } = this.state;
    let update = editportexpData['port_activity'] != -1 ? true : false;
    return (
      <>
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
            {loading === false && (isResubmitButton === true || isAgentView === true) && 
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span
                          className="text-bt"
                          onClick={() => this.showHideModal(true, 'PortActivityReport')}
                        >
                          Report
                        </span>
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
              }
              {loading === false && 
                <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formData} showForm={true} frmCode={frmName} addForm={true} 
                    showButtons={ 
                      isAgentView === true &&
                      [
                        { "id": `${update === true ? "update" : "save" }`, "title": `${update === true ? "Update" : "Save" }`, "type": "primary", "event": (data, innerCB) => { this.saveFormData(data, innerCB) }  },
                      ]
                  }
                //     showToolbar={
                //       [
                //         {
                //           isLeftBtn: [
                //             {
                //               isSets: [
                //                 { id: '3', key: 'save', type: <SaveOutlined />, withText: '', "event": (data, innerCB) => { this.notFillAllData(port_call_id, innerCB) } },
                //               ],
                //             },
                //           ],
                //           isRightBtn: []
                //         },
                //     ] 
                // }
                    inlineLayout={true}
                    disableTabs={isAgentView === false && [{'tabName':'', 'groupKey':'-'}] }
                 />}
            </div>
          </div>
        </article>
        <Modal
          title="Alert"
         open={modelVisible}
          onOk={()=> this.setState({modelVisible: !modelVisible})}
          onCancel={()=>this.setState({modelVisible :!modelVisible})}
        >
          <p>{'data not available'}</p>
      </Modal>
        <Modal
          className="page-container"
          style={{ top: '2%' }}
          title="Reports"
         open={this.state.modals['PortActivityReport']}
          onCancel={() => this.showHideModal(false, 'PortActivityReport')}
          width="95%"
          footer={null}
        >
          <PortActivityReport {...editportexpData} />
        </Modal>
      </>
    );
  }
}

export default PortActivityList;
