import React, { Component } from 'react'

import { Row, Layout, Modal, Col } from 'antd';
import { Map, Marker, GoogleApiWrapper, Polyline, Polygon } from 'google-maps-react';
import mapboxgl, { MapWheelEvent } from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
const { Content } = Layout;

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

class PortAnalytics extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coordinates: [],
      lng: this.props.data && this.props.data.length > 0 ? this.props.data[0].vessel_lat : 0.0,
      lat: this.props.data && this.props.data.length > 0 ? this.props.data[0].vessel_lon : 0.0,
      zoom: 1.27,
      visibleModal: false
    }
    this.mapContainer = React.createRef();

  }
  componentDidMount = () => {
    this.maprender();
  }
  maprender() {
    let that = this
    const geojson = {
      type: "FeatureCollection",
      features: [{
        type: "Feature",
        geometry: {
          type: "LineString",
          properties: {},
          coordinates: this.state.coordinates ? this.state.coordinates : [],
        },
      },],
    };
    const map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
      center: this.state.coordinates ? this.state.coordinates[0] : [],
      zoom: this.state.zoom,
    });
    map.on("load", () => {
      map.addSource("LineString", {
        type: "geojson",
        data: geojson,
      });
      map.on('click', 'points', function (e) {

        // console.log(e.point);
        that.setState({ visibleModal: true })
      });

      map.addLayer({
        id: "LineString",
        interactive: true,
        type: "line",
        source: "LineString",
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": "#2414E2",
          "line-width": 4,
          "line-dasharray": [0, 1.5],
        },
      });

      map.loadImage(
        "https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png",
        (error, image) => {
          if (error) throw error;
          map.addImage("custom-marker", image);
          map.on('move', () => {
            this.setState({
              lng: map.getCenter().lng.toFixed(4),
              lat: map.getCenter().lat.toFixed(4),
              zoom: map.getZoom().toFixed(2)
            });
          });
          // Add a GeoJSON source with 2 points
          map.addSource("points", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: this.props.data && this.props.data.length > 0 ?
                [{
                  // feature for Mapbox DC
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [this.props.data[0].vessel_lat, this.props.data[0].vessel_lon],
                  },
                  properties: {
                    title: this.props.data[0].vessel_name,
                  },
                },
                ] : [],

            },
          });

          // Add a symbol layer
          map.addLayer({
            id: "points",
            type: "symbol",
            source: "points",
            layout: {
              "icon-image": "custom-marker",
              // get the title name from the source's "title" property
              "text-field": ["get", "title"],
              "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
              "text-offset": [0, 1.25],
              "text-anchor": "top",
            },
          });
        }
      );
    });
  }

  showHideModal = (val, data) =>
    this.setState({ visibleModal: val })


  render() {
    const { visibleModal } = this.state
    let weatherData = this.props.data && this.props.data.length > 0 ? this.props.data[0] : {}
    return (
      <>
        <div className="wrap-rightbar full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <section className="map-wrapper-container">
                  <div className="fieldscroll-wrap">
                    <article className="article">
                      <div className="box box-default">
                        <Row style={{ minHeight: '600px' }}>
                          <div ref={this.mapContainer} className="map-container" style={{ width: "100%", height: "600px" }} />
                        </Row>
                      </div>
                      {visibleModal ? (
                        <Modal
                          style={{ top: '2%' }}
                          title="Vessel Details"
                         open={visibleModal}
                          onCancel={() => this.showHideModal(false, null)}
                          width="70%"
                          footer={null}
                        >
                          <div className="wrap-rightbar full-wraps">
                            <Layout className="layout-wrapper">
                              <Layout>
                                <Content className="content-wrapper">
                                  <section className="map-wrapper-container">
                                    <div className="fieldscroll-wrap">
                                      <article className="article">
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Vessel Name</strong>:</span>&nbsp; {weatherData && weatherData.vessel_name ? weatherData.vessel_name : '--'}</p>
                                          </Col>
                                          <Col span={12}>
                                            <p><span><strong>Vessel Status</strong>:</span>&nbsp; {weatherData && weatherData.vessel_name ? weatherData.vessel_name : '--'}</p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Vessel Latitude</strong>:</span>&nbsp; {weatherData && weatherData.vessel_lat ? weatherData.vessel_lat : '--'}</p>
                                          </Col>
                                          <Col span={12}>
                                            <p><span><strong>Vessel Longitude</strong>:</span>&nbsp; {weatherData && weatherData.vessel_lon ? weatherData.vessel_lon : '--'}</p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Vessel Last position</strong>:</span>&nbsp; {weatherData && weatherData.vessel_last_pos ? weatherData.vessel_last_pos : '--'}</p>
                                          </Col>
                                          <Col span={12}>
                                            <p><span><strong>Vessel speed</strong>:</span>&nbsp; {weatherData && weatherData.speed ? weatherData.speed : '--'}</p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Current Port Name</strong>:</span>&nbsp; {weatherData && weatherData.currentPortName ? weatherData.currentPortName : '--'}</p>
                                          </Col>
                                          <Col span={12}>
                                            <p><span><strong>Degree</strong>:</span>&nbsp; {weatherData && weatherData.degree ? weatherData.degree : '--'}</p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Area Name</strong>:</span>&nbsp; {weatherData && weatherData.areaName ? weatherData.areaName : '--'}</p>
                                          </Col>
                                          <Col span={12}>
                                            <p><span><strong>Area Code</strong>:</span>&nbsp; {weatherData && weatherData.areaCode ? weatherData.areaCode : '--'}</p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p><span><strong>Imo No</strong>:</span>&nbsp; {weatherData && weatherData.imo_no ? weatherData.imo_no : '--'}</p>
                                          </Col>
                                        </Row>
                                      </article>
                                    </div>
                                  </section>
                                </Content>
                              </Layout>
                            </Layout>
                          </div>
                        </Modal>
                      ) : (
                        undefined
                      )}
                    </article>
                  </div>
                </section>
              </Content>
            </Layout>
          </Layout>
        </div>
      </>
    );
  }
}
export default PortAnalytics