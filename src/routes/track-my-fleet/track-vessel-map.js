//---------------------------------------------------------------------------------------------------
// vessel mapfrom live vessel page

import React, { Component } from "react";
import { Row, Layout, Modal, Col, Icon, Spin } from "antd";
import {
  Map,
  Marker,
  GoogleApiWrapper,
  Polyline,
  Polygon,
} from "google-maps-react";
import mapboxgl, { MapWheelEvent } from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import '../../styles/mapstyle.css';
import "react-virtualized/styles.css";
import URL_WITH_VERSION, {
  openNotificationWithIcon,
  postAPICall,
  getAPICall,
  IMAGE_PATH
} from "../../shared";
import SelectedVesselView from "../track-vessel/selected-vessel-view";

import FilterVessel from "../track-vessel/filterVessel";

const { Content } = Layout;

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

const FILTER_TYPES = {
  liveSearch: "live_search",
  localSearch: "local_search",
  allDataFilter: "all_data_filter",
};

const searchLabel = "Search vessel by it's name or IMO number";
class TrackVesselMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      map: {},
      coordinates: [],
      featuresData: [],
      searchedItem: this.props.searchedItem,
      lng:
        this.props.data && this.props.data.length > 0
          ? this.props.data[0].vessel_lat
          : 0.0,
      lat:
        this.props.data && this.props.data.length > 0
          ? this.props.data[0].vessel_lon
          : 0.0,
      zoom: 1.27,
      visibleModal: false,
      data: this.props.mapData,
      filterData: this.props.mapData,
      allData: this.props,
      selectedData: null,
      selectedPropData: null,
      serchedData: "all",
      filterType: "",
      liveSearchValue: "",
      liveSearchList: [],
      isLiveDetailLoading: false,
      liveSearchResult: searchLabel,
      isShowLocationFromtable: this.props.isShowLocationFromtable || false,
    };
    this.mapContainer = React.createRef();
    this.mapObject = React.createRef();
  }

  showMarker(item) {
    const coordinates = item.geometry.coordinates.slice();
    const description = item.properties.description;
    new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(description)
      .addTo(this.state.map);
  }

  selectedListPopup = new mapboxgl.Popup({});

  getMapPlaceItem = (item) => {
    return {
      type: "Feature",
      properties: {
        dataset: item,
        rotation: Number.isNaN(item.degree) ? 0 : Number(item.degree),
        description: "",
        //  description: `<div>
        // <p className="map_p"><strong>Vessel Name: </strong><span style="font-weight:700;color:#FF0000;">${item.vessel_name ? item.vessel_name : "NA"}</span></p>
        // <p className="map_p"><strong>Current Port: </strong><span style="font-weight:700;">${item.current_port_name ? item.current_port_name : "NA"}</span></p>
        // <p className="map_p"><strong>Imo No: </strong><span style="font-weight:700;">${item.imo_no ? item.imo_no : "NA"}</span></p>
        // <p className="map_p"><strong>Last pos Latitude: </strong><span style="font-weight:700;">${item.vessel_lat ? item.vessel_lat : "NA"}</span></p>
        // <p className="map_p"><strong>Last pos Langitide: </strong><span style="font-weight:700;">${item.vessel_lon ? item.vessel_lon : "NA"}</span></p>
        // <p className="map_p"><strong>Status: </strong><span style="font-weight:700;font-style: italic;color:#33cc33;">${item.vessel_status ? item.vessel_status : "NA"}</span>
        // <p className="map_p"><strong>Speed: </strong><span style="font-weight:700;">${item.speed ? item.speed : "NA"}</span>
        // </p><p className="map_p"><strong>Degree: </strong><span style="font-weight:700;">${item.degree ? item.degree : "NA"}</span></p></p>
        // </div>`,

        hoverProperties: `
          <div class="hover-content-map-item">
            <div>
              <p class="map_p"><b>Vessel Name: </b><span class="red">${
                item.vessel_name ? item.vessel_name : "NA"
              }</span></p>
              <p class="map_p"><b>Status: </b> <span>${
                item.status ? item.status : "NA"
              }</span></p>
            </div>
            <div>
              <p class="map_p"><b>speed: </b><span>${
                item.speed ? item.speed : "NA"
              }</span></p>
              <p class="map_p"><span style="visibility: hidden;">---</span></p>
            </div>
          </div>
          `,
        icon: "harbor-15",
      },
      geometry: {
        type: "Point",
        coordinates: [
          item.last_pos_lon ? item.last_pos_lon : 0.0,
          item.last_pos_lat ? item.last_pos_lat : 0.0,
        ],
      },
    };
  };




  componentDidMount = () => {
    const { searchedItem, isShowLocationFromtable } = this.state;

    if (isShowLocationFromtable) {
      this.getVesselIdByImoNumber(searchedItem.imo_no, searchedItem, true);
    }

    let mapLocation = [];
    this.state.data.map((item) => {
      return mapLocation.push(this.getMapPlaceItem(item));
    });
    this.setState({ featuresData: mapLocation }, () => {
      this.maprender();
    });
  };






  maprender = () => {
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "point",
            properties: {},
            coordinates: this.state.coordinates ? this.state.coordinates : [],
          },
        },
      ],
    };
    const map = new mapboxgl.Map({
      container: this.mapContainer.current,
      projection: "mercator",
      style: "mapbox://styles/mapbox/light-v11",
      center: this.state.coordinates ? this.state.coordinates[0] : [],
      zoom: 1,
    });
    this.setState({ map: map });
    this.mapObject.current = map;

    map.on("load", () => {
      // add mapbox terrain dem source for 3d terrain rendering
      // mapDiv.style.width = '100%';
      map.addSource("LineString", {
        type: "geojson",
        data: geojson,
      });

      map.on("click", (e) => {
        // console.log(e);
      });

      map.addLayer({
        id: "LineString",
        type: "line",
        source: "LineString",
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": "#2414E2",
          "line-width": 4,
          "line-dasharray": [0, 1.5],
        },
      });

      map.loadImage(IMAGE_PATH+"icons/mapArrowYellow.png", (error, image) => {
        if (error) {
          return;
        }
        map.addImage("yellowIcon", image);
      });

      map.loadImage( IMAGE_PATH+"icons/mapArrowUnknown.png", (error, image) => {
        if (error) {
          console.log(">>>>> ", error);
          return;
        }
        map.addImage("unknownIcon", image);
      });

      map.loadImage(IMAGE_PATH+"icons/mapArrowOrange.png", (error, image) => {
        if (error) {
          return;
        }
        map.addImage("orangeIcon", image);
      });

      map.loadImage(IMAGE_PATH+"icons/mapArrowGreen.png", (error, image) => {
        if (error) {
          return;
        }
        map.addImage("greenIcon", image);
      });

      map.loadImage(IMAGE_PATH+"icons/map-icon-pointer.png", (error, image) => {
        if (error) throw error;
        map.addImage("custom-marker", image);

        map.addSource("places", {
          // This GeoJSON contains features that include an "icon"
          // property. The value of the "icon" property corresponds
          // to an image in the Mapbox Streets style's sprite.
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: this.state.featuresData,
          },
        });

        // Add a layer showing the places.
        map.addLayer({
          id: "places",
          type: "symbol",
          source: "places",
          layout: {
            "icon-image": "greenIcon",
            "icon-allow-overlap": true,
            "icon-rotate": ["get", "rotation"],
            "icon-ignore-placement": true,
          },
        });

        // Create a popup, but don't add it to the map yet.
        const hoverpopup = new mapboxgl.Popup({
          maxWidth: "10vw",
          closeButton: false,
          closeOnClick: false,
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map.on("click", "places", (e) => {
          // Copy coordinates array.
          try {
            const dataset = JSON.parse(e.features[0].properties.dataset) || {};
            if (Object.keys(dataset).length) {
              this.getVesselIdByImoNumber(dataset.imo_no, dataset, true);
            }

            const addedMarker = document.getElementById("dottedArrowMarker");

            if (addedMarker) {
              addedMarker.remove();
            }
            const markerImage = document.createElement("img");
            markerImage.src = IMAGE_PATH+"icons/dottedArrow.png";
            markerImage.width = 140;
            markerImage.height = 20;
            markerImage.id = "dottedArrowMarker";

            new mapboxgl.Marker(markerImage, {
              rotation: e.features[0].properties.rotation,
            })
              .setLngLat([
                parseFloat(dataset.last_pos_lon),
                parseFloat(dataset.last_pos_lat),
              ])
              .addTo(map);
          } catch (error) {
            console.log(">>> ERROR", error);
          }
        });

        map.addSource("liveVessel", {
          // This GeoJSON contains features that include an "icon"
          // property. The value of the "icon" property corresponds
          // to an image in the Mapbox Streets style's sprite.

          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [],
          },
        });
        // Add a layer showing the places.
        map.addLayer({
          id: "liveVessel",
          type: "symbol",
          source: "liveVessel",
          layout: {
            "icon-image": "yellowIcon",
            "icon-allow-overlap": true,
          },
        });
        map.on("click", "liveVessel", (e) => {
          // Copy coordinates array.
          try {
            const dataset = JSON.parse(e.features[0].properties.dataset) || {};
            if (Object.keys(dataset).length) {
              this.getVesselIdByImoNumber(dataset.imo_no, dataset, true);
            }
          } catch (error) {
            console.log(">>> ERROR", error);
          }
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map.on("mouseenter", "places", (e) => {
          const mapCoordinates = e.features[0].geometry.coordinates.slice();
          const description = e.features[0].properties.hoverProperties;

          // Ensure that if the map is zoomed out such that multiple
          // copies of the feature are visible, the popup appears
          // over the copy being pointed to.
          while (Math.abs(e.lngLat.lng - mapCoordinates[0]) > 180) {
            mapCoordinates[0] += e.lngLat.lng > mapCoordinates[0] ? 360 : -360;
          }

          hoverpopup
            .setLngLat(mapCoordinates)
            .setHTML(description)
            .addTo(map);

          map.getCanvas().style.cursor = "pointer";
        });

        // Change it back to a pointer when it leaves.
        map.on("mouseleave", "places", () => {
          map.getCanvas().style.cursor = "";
          hoverpopup.remove();
        });
      });
    });
  };

  showHideModal = (val, data) => this.setState({ visibleModal: val });

  /* get selected data from api */
  getSelectedData = async (dataset) => {
    this.selectedListPopup.remove();
    const {
      vessel_name,
      current_port_name,
      degree,
      imo_no,
      last_pos_lat,
      last_pos_lon,
      speed,
      status,
    } = dataset;

    // const cordinates = [last_pos_lat.toLowerCase() !== "none" ? last_pos_lat : 0, last_pos_lon.toLowerCase() !== "none" ? last_pos_lon : 0];
    const description = `<div> <p className="map_p"><strong>Vessel Name: </strong><span style="font-weight:700;color:#FF0000; text-transform:uppercase;">${
      vessel_name ? vessel_name : "vessel name"
    }</span></p> <p className="map_p"><strong>Current Port: </strong><span style="font-weight:700;">${
      current_port_name ? current_port_name : "NA"
    }</span></p> <p className="map_p"><strong>Imo No: </strong><span style="font-weight:700;">${
      imo_no ? imo_no : "NA"
    }</span></p> <p className="map_p"><strong>Last pos Latitude: </strong><span style="font-weight:700;">${
      last_pos_lat ? last_pos_lat : "NA"
    }</span></p> <p className="map_p"><strong>Last pos Langitide: </strong><span style="font-weight:700;">${
      last_pos_lon ? last_pos_lon : "NA"
    }</span></p> <p className="map_p"><strong>Status: </strong><span style="font-weight:700;font-style: italic;color:#33cc33;">${
      status ? status : "NA"
    }</span> <p className="map_p"><strong>Speed: </strong><span style="font-weight:700;">${
      speed ? speed : "NA"
    }</span> </p><p className="map_p"><strong>Degree: </strong><span style="font-weight:700;">${
      degree ? degree : "NA"
    }</span></p></p> </div>`;
    // this.selectedListPopup
    //   .setLngLat(cordinates)
    //   .setHTML(description)
    //   .addTo(this.state.map);

    let tempSelectedData = {
      imo_number: imo_no,
      last_port: {
        ata: "NA",
        atd: "NA",
        locode: "NA",
        name: "NA",
      },
      mmsi_number: "NA",
      name: vessel_name,
      next_port: null,
      position: {
        course_over_ground: 188.3,
        latitude: last_pos_lat,
        location_str: current_port_name,
        longitude: last_pos_lon,
        nav_status: "NA",
        received: "NA",
        speed: speed,
        true_heading: 10,
      },
      vessel_id: "NA",
      voyage: {
        destination: "NA",
        draught: "NA",
        eta: "NA",
        received: "NA",
      },
    };

    if (imo_no) {
      try {
        const response = await getAPICall(
          `${process.env.REACT_APP_URL_NEW}/VesselDetail/find/${imo_no}`
        );
        const data = await response;

        if (data.length > 0) {
          tempSelectedData = {
            imo_number: imo_no,
            last_port: {
              ata: "NA",
              atd: "NA",
              locode: "NA",
              name: "NA",
            },
            mmsi_number: data[0].maritimemobileserviceidentitymmsinumber,
            name: vessel_name,
            next_port: null,
            position: {
              course_over_ground: 188.3,
              latitude: last_pos_lat,
              location_str: current_port_name,
              longitude: last_pos_lon,
              nav_status: data[0].shipstatuss,
              received: "NA",
              speed: speed,
              true_heading: 10,
            },
            vessel_id: "NA",
            voyage: {
              destination: "NA",
              draught: data[0].draught,
              eta: "NA",
              received: "NA",
            },
          };

          this.setState({
            selectedData: tempSelectedData,
            selectedPropData: dataset,
            filterType: "",
            visibleLiveVessel: true,
            loading: false,
          });
        } else {
          openNotificationWithIcon("err", "Imo No is not available", 3);
        }
      } catch (err) {
        // console.log(err);
        openNotificationWithIcon("error", "Something Went wrong.", 3);

        this.setState({
          selectedData: tempSelectedData,
          selectedPropData: dataset,
          filterType: "",
          visibleLiveVessel: false,
          loading: false,
        });
      }
    } else {
      openNotificationWithIcon("err", "Imo No is not available", 3);
    }
  };

  // searchIconClick = () => {
  //   this.setState({
  //     filterType: FILTER_TYPES.liveSearch,
  //     liveSearchValue: "",
  //     selectedData: null,
  //   });
  // };

  // filterIconClick = () => {
  //   this.setState({ filterType: FILTER_TYPES.localSearch, selectedData: null });
  // };

  // onSearchDbData = () => {
  //   this.setState({
  //     filterType: FILTER_TYPES.allDataFilter,
  //     serchedData: "all",
  //     selectedData: null,
  //   });
  // };

  onCloseFilter = () => {
    this.setState(
      {
        filterType: "",
        liveSearchValue: "",
        selectedData: null,
        liveSearchResult: searchLabel,
        liveSearchList: [],
        isShowLocationFromtable: false,
      },
      () => {
        this.props.loadpage();
        if (typeof this.props.modalCloseEvent == "function") {
          this.props.modalCloseEvent();
        }
      }
    );
  };

  // getVesselImageByImoNo = async (imoNumber, vesselId) => {
  //   if (!imoNumber && !vesselId) {
  //     return false;
  //   }

  //   let params = "";
  //   if (imoNumber) {
  //     params = `imo_number=${imoNumber}`;
  //   } else {
  //     params = `vessel_id=${vesselId}`;
  //   }

  //   const url = `${process.env.REACT_APP_VESSEL_PHOTO}/thumbnail?apikey=${
  //     process.env.REACT_APP_VESSEL_API_KEY
  //   }&${params}`;

  //   try {
  //     return await fetch(url, {
  //       method: "GET",
  //       headers: {
  //         "access-control-allow-origin": "*",
  //         Accept: "application/json",
  //         "Content-Type": "application/json",
  //       },
  //     })
  //       .then((res) => res.json())
  //       .then((res) => {
  //         return res;
  //       });
  //   } catch (error) {
  //     console.log("> IMAGE NOT AVAILBLE FOR THIS VESSEL");
  //     return false;
  //   }
  // };

  getVesselIdByImoNumber = async (imoNumber, localDataset, isNotShowInMap) => {
    this.setState({ isLiveDetailLoading: true, selectedData: {} });
    try {
      const url = `${process.env.REACT_APP_VESSEL_SEARCH}?apikey=${
        process.env.REACT_APP_VESSEL_API_KEY
      }&imo_number=${imoNumber}`;
      const result = await fetch(url, {
        method: "GET",
        headers: {
          "access-control-allow-origin": "*",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }).then((res) => res.json());

      const dataset = result && result.vessels && result.vessels[0];

      if (dataset && dataset.vessel_id) {
        // this.setState({ isLiveDetailLoading: false });
        await this.handleGetLiveVesselItemData(dataset, isNotShowInMap);
      } else {
        // If not found the fetch from the-oceann db
        this.getSelectedData(localDataset);
      }
    } catch (e) {
      this.setState({ selectedData: null });
    }
    //this.setState({ isLiveDetailLoading: false });
  };

  /* handle click on searched data */
  handleGetLiveVesselItemData = async (dataset, isNotShowInMap) => {
    const { name, imo_number, vessel_id, vt_verbose } = dataset;

    const tempSelectedData = {
      imo_number: 0,
      last_port: {
        ata: "2022-12-01T10:24:33Z",
        atd: "2022-12-01T10:24:33Z",
        locode: "string",
        name: "string",
      },
      mmsi_number: 0,
      name: "",
      next_port: {
        eta_calc: "2022-12-01T10:24:33Z",
        locode: "",
        name: "",
        travel_distance_nm: 0,
        travel_time_h: 0,
      },
      position: {
        course_over_ground: 0,
        latitude: 0,
        location_str: "",
        longitude: 0,
        nav_status: "",
        received: "2022-12-01T10:24:33Z",
        speed: 0,
        true_heading: 0,
      },
      request_limit_info: {
        left_requests: 0,
        max_requests: 0,
        used_requests: 0,
      },
      vessel_id: 0,
      voyage: {
        destination: "",
        draught: 0,
        eta: "2022-12-01T10:24:33Z",
        received: "2022-12-01T10:24:33Z",
      },
    };

    const tempSelectedPropData = {
      vessel_name: name,
      imo_no: imo_number,
      current_port_name: "NA",
      last_pos_lat: "NA",
      last_pos_lon: "NA",
    };
    //  this.setState({ isLiveDetailLoading: true });
    try {
      let url = `${process.env.REACT_APP_VESSEL_DETAILS}/${vessel_id}?apikey=${
        process.env.REACT_APP_VESSEL_API_KEY
      }`;

      //const imgData = await this.getVesselImageByImoNo(imo_number, vessel_id);
      fetch(url, {
        method: "GET",
        headers: {
          "access-control-allow-origin": "*",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          this.setState({
            selectedData: data,
            selectedPropData: tempSelectedPropData,
            filterType: "",
            isLiveDetailLoading: false,
          });
          this.selectedListPopup.remove();
          const cordinates = [
            data.position
              ? data.position.latitude > -90 && data.position.latitude < 90
                ? data.position.latitude
                : 0
              : 0,
            data.position
              ? data.position.longitude > -90 && data.position.longitude < 90
                ? data.position.longitude
                : 0
              : 0,
          ];

          // const description = `<div> <p className="map_p"><strong>SS Vessel Name: </strong><span style="font-weight:700;color:#FF0000; text-transform:uppercase;">${data.name ? data.name : "vessel name"}</span></p> <p className="map_p"><strong>Current Port: </strong><span style="font-weight:700;">${data.next_port !== null ? data.next_port.name : "NA"}</span></p> <p className="map_p"><strong>Imo No: </strong><span style="font-weight:700;">${data.imo_number || data.imo_number !== null ? data.imo_number : "NA"}</span></p> <p className="map_p"><strong>Last port: </strong><span style="font-weight:700;">${data.last_port !== null ? data.last_port.name : "NA"}</span></p> <p className="map_p"><strong>Last pos code: </strong><span style="font-weight:700;">${data.last_port !== null ? data.last_port.locode : "NA"}</span></p> <p className="map_p"><strong>Draught: </strong><span style="font-weight:700;font-style: italic;color:#33cc33;">${data.voyage !== null ? data.voyage.draught : "NA"}</span> <p className="map_p"><strong>Speed: </strong><span style="font-weight:700;">${data.position ? data.position.speed : "NA"}</span> </p><p className="map_p"><strong>Destination: </strong><span style="font-weight:700;">${data.voyage !== null ? data.voyage.destination : "NA"}</span></p></p> </div>`
          // this.selectedListPopup
          //   .setLngLat(cordinates)
          //   .setHTML(description)
          //   .addTo(this.state.map);

          // Add to map and update the map

          this.liveVesselUpdateinTable(data);
          if (!isNotShowInMap && data.imo_number && data.name) {
            const mapDataItem = {
              imo_no: `${data.imo_number}`,
              vessel_name: data.name,
              speed: data.position.speed,
              vessel_lat: data.position.latitude || null,
              vessel_lon: data.position.longitude || null,
              vessel_status: data.position.nav_status || null,
              current_port_name: data.current_port_name || null,
              last_pos_lon: data.position.longitude,
              last_pos_lat: data.position.latitude,
            };
            this.addNewSourceOnMapData(mapDataItem);
          }
        });
    } catch (error) {
      //  console.log(error);
      this.setState({
        selectedData: tempSelectedData,
        selectedPropData: tempSelectedPropData,
        filterType: "",
        isLiveDetailLoading: false,
      });
    }

    this.setState({
      selectedData: tempSelectedData,
      selectedPropData: tempSelectedPropData,
      filterType: "",
    });
  };

  addNewSourceOnMapData = (newItem) => {
    const { selectedData } = this.state;


    if (!newItem || !newItem.imo_no) {
      return;
    }
    // const hasVessel = selectedData.some(i => `${i.imo_no}` === `${newItem.imo_no}`);

    // console.log('newItem', newItem);
    // if (hasVessel) {
    //   // return if already have this vessel item in map
    //   // return;
    // }

    const mapData = [newItem];
    // Add popup
    this.addVesselDetailPopup(newItem);

    this.updateMapData(mapData, "liveVessel");
  };

  updateMapData = (mapData = [], sourceType = "places") => {
    let mapLocation = [];
    mapData.map((item) => {
      return mapLocation.push(this.getMapPlaceItem(item));
    });
    try {
      this.mapObject.current.getSource(sourceType).setData({
        type: "FeatureCollection",
        features: mapLocation,
      });
    } catch (err) {
      console.log("err", err);
    }
    this.setState({ featuresData: mapLocation });
  };

  addVesselDetailPopup = (newItem) => {
    if (!newItem) {
      return;
    }
   
    // Add popup
    const markPopup = new mapboxgl.Popup({
      offset: 25,
      maxWidth: "200px",
      closeButton: true,
      closeOnClick: true,
    });

    const popupCordinate = [
      newItem.last_pos_lon ? newItem.last_pos_lon : 0.0,
      newItem.last_pos_lat ? newItem.last_pos_lat : 0.0,
    ];

    this.mapObject.current.flyTo({
      center: popupCordinate,
    });

    markPopup
      .setLngLat(popupCordinate)
      //.setHTML(`
      // <div class="live-popup">

      //   <div class="live-popup-wp">
      //     <div class="title-block">
      //       <div><b>Vessel Name: </b></div>
      //       <div><span>${newItem.vessel_name ? newItem.vessel_name : "NA"}</span></div>
      //     </div>
      //     <div class="title-block">
      //       <div><b>IMO: </b></div>
      //       <div><span>${newItem.imo_no ? newItem.imo_no : "NA"}</span></div>
      //     </div>
      //   </div>
      //   <div class="live-popup-wp">
      //     <div class="title-block">
      //       <div><b>Dead Weight: </b></div>
      //       <div><span>${newItem.dead_weight ? newItem.dead_weight : "NA"}</span></div>
      //     </div>
      //     <div class="title-block">
      //       <div><b>Type: </b></div>
      //       <div><span>${newItem.vt_verbose ? newItem.vt_verbose : "NA"}</span></div>
      //     </div>
      //   </div>

      //   <div class="port-detail">
      //     <div class="">
      //       <div><b>Last Port: </b> <span>${newItem.last_port && newItem.last_port.name ? newItem.last_port.name : "NA"}</span></div>
      //       <div><b>ATD: </b><span> ${newItem.last_port && newItem.last_port.atd ? newItem.last_port.atd : "NA"} </span></div>
      //     </div>
      //     <div class="">
      //       <div><b>Next Port: </b> <span>${newItem.next_port && newItem.next_port.name ? newItem.next_port.name : "NA"}</span></div>
      //       <div><b>ETA: </b><span>${newItem.voyage && newItem.voyage.eta ? newItem.voyage.eta : "NA"}</span></div>
      //     </div>
      //   </div>
      //   <div class="port-detail">
      //     <div class="">
      //       <div><b>Current Position: </b> <span span class="green">${newItem.position && newItem.position.location_str ? newItem.position.location_str : "NA"}</span></div>
      //     </div>
      //     <div class="">
      //       <div><b>Destination: </b> <span span class="red"><b>${newItem.voyage && newItem.voyage.destination ? newItem.voyage.destination : "NA"}</b></span></div>
      //     </div>
      //   </div>

      //   <div class="cii-block">
      //     <div class="cii-title">
      //       <div><b>CII Simulator: </b></div>
      //       </div>

      //       <div class="cii-item">
      //         <div class="cii-col">
      //           <b>Distance travelled: </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //         <div class="cii-col">
      //           <b>Co2 factor (g/t): </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //       </div>
      //       <div class="cii-item">
      //         <div class="cii-col">
      //           <b>Fuel consumed: </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //         <div class="cii-col">
      //           <b>Co2 emission: </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //       </div>
      //       <div class="cii-item">
      //         <div class="cii-col">
      //           <b>CII Rating YTD: </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //         <div class="cii-col">
      //           <b>CII Voyage Rating: </b>
      //           <span><i class="fas fa-lock yellow-icon-color"></i></span>
      //         </div>
      //       </div>

      //   </div>

      // </div>
      //  `)
      // .setHTML(`
      //   <div class="live-popup">
      //     <div>
      //       <p class="map_p"><strong>Vessel Name: </strong><span class="red">${newItem.vessel_name ? newItem.vessel_name : "NA"}</span></p>
      //       <p class="map_p"><strong>Current Port: </strong><span>${newItem.current_port_name ? newItem.current_port_name : "NA"}</span></p>
      //       <p class="map_p"><strong>Speed: </strong><span>${newItem.speed ? newItem.speed : "NA"}</span>
      //       <p class="map_p"><strong>ETA: </strong><span class="red">${newItem.voyage && newItem.voyage.eta ? newItem.voyage.eta : "NA"}</span></p>
      //     </div>
      //     <div>
      //       <p class="map_p"><strong>Imo No: </strong><span>${newItem.imo_no ? newItem.imo_no : "NA"}</span></p>
      //       <p class="map_p"><strong>Next Port: </strong><span class="red">${newItem.next_port && newItem.next_port.name ? newItem.next_port.name : "NA"}</span></p>
      //       <p class="map_p"><strong>Destination: </strong><span class="red">${newItem.voyage && newItem.voyage.destination ? newItem.voyage.destination : "NA"}</span></p>
      //       <p class="map_p"><strong>Type: </strong><span>${newItem.vt_verbose ? newItem.vt_verbose : "NA"}</span></p>
      //     </div>
      //   </div>
      // `)
      .addTo(this.mapObject.current);
  };

  liveVesselUpdateinTable = async (data) => {
    const mapDataItem = {
      imo_no: `${data.imo_number}`,
      vessel_name: data.name,
      speed: data.position.speed,
      vessel_lat: data.position.latitude || null,
      vessel_lon: data.position.longitude || null,
      vessel_status: data.position.nav_status || null,
      current_port_name: data.current_port_name || null,
      last_pos_lon: data.position.longitude,
      last_pos_lat: data.position.latitude,
      degree: data.position.true_heading || null,
    };
    this.addNewSourceOnMapData(mapDataItem);
    let url = `${URL_WITH_VERSION}/vessel/live-vessel/update`;

    try {
      await postAPICall(url, data, "put", (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message, 3);
        } else {
          openNotificationWithIcon("error", response.message, 3);
        }
      });
    } catch (err) {
      openNotificationWithIcon("error", "something went wrong", 3);
    }
  };










  
  render() {
    const { visibleModal, isLiveDetailLoading } = this.state;
    let weatherData =
      this.props.data && this.props.data.length > 0 ? this.props.data[0] : {};
    return (
      <>
        <FilterVessel>
          {/* showing single selected veseldetail */}
          <SelectedVesselView
            vesselDetails={this.state.selectedData}
            onClose={this.onCloseFilter}
            isLiveDetailLoading={isLiveDetailLoading}
          />
        </FilterVessel>

        <div className="wrap-rightbar full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <section className="map-wrapper-container">
                  <div className="fieldscroll-wrap">
                    <article className="article">
                      <div className="box box-default map-box-wrapper">
                        <Row style={{ minHeight: "600px" }}   ref={this.mapContainer}>
                          {/* <div
                          
                            className="map-container"
                            style={{ width: "100%", height: "600px" }}
                          /> */}
                        </Row>
                      </div>

                      {visibleModal ? (
                        <Modal
                          style={{ top: "2%" }}
                          title="Vessel Details"
                         open={visibleModal}
                          onCancel={() => this.showHideModal(false, null)}
                          width="70%"
                          footer={null}
                        >
                          <div className="wrap-rightbar full-wraps">
                            <Layout className="layout-wrapper">
                              <Layout>
                                <Content className="content-wrapper">
                                  <section className="map-wrapper-container">
                                    <div className="fieldscroll-wrap">
                                      <article className="article">
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Vessel Name</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.vessel_name
                                                ? weatherData.vessel_name
                                                : "--"}
                                            </p>
                                          </Col>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Vessel Status</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.vessel_name
                                                ? weatherData.vessel_name
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Vessel Latitude</strong>
                                                :
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.vessel_lat
                                                ? weatherData.vessel_lat
                                                : "--"}
                                            </p>
                                          </Col>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>
                                                  Vessel Longitude
                                                </strong>
                                                :
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.vessel_lon
                                                ? weatherData.vessel_lon
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>
                                                  Vessel Last position
                                                </strong>
                                                :
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.vessel_last_pos
                                                ? weatherData.vessel_last_pos
                                                : "--"}
                                            </p>
                                          </Col>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Vessel speed</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData && weatherData.speed
                                                ? weatherData.speed
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>
                                                  Current Port Name
                                                </strong>
                                                :
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.currentPortName
                                                ? weatherData.currentPortName
                                                : "--"}
                                            </p>
                                          </Col>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Degree</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData && weatherData.degree
                                                ? weatherData.degree
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Area Name</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.areaName
                                                ? weatherData.areaName
                                                : "--"}
                                            </p>
                                          </Col>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Area Code</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData &&
                                              weatherData.areaCode
                                                ? weatherData.areaCode
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col span={12}>
                                            <p>
                                              <span>
                                                <strong>Imo No</strong>:
                                              </span>
                                              &nbsp;{" "}
                                              {weatherData && weatherData.imo_no
                                                ? weatherData.imo_no
                                                : "--"}
                                            </p>
                                          </Col>
                                        </Row>
                                      </article>
                                    </div>
                                  </section>
                                </Content>
                              </Layout>
                            </Layout>
                          </div>
                        </Modal>
                      ) : (
                        undefined
                      )}
                    </article>
                  </div>
                </section>
              </Content>
            </Layout>
          </Layout>
        </div>
      </>
    );
  }
}
export default TrackVesselMap;
