import React, { Component } from 'react';
import { Form, Select, Modal,Spin,Alert } from 'antd';
import { Gantt } from '@dhtmlx/trial-react-gantt';
import PortScheduleReport from '../operation-reports/PortScheduleReport';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import URL_WITH_VERSION, { getAPICall, postAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon, ResizeableTitle } from '../../shared';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import { CloseOutlined, SaveOutlined } from '@ant-design/icons';

// const FormItem = Form.Item;
// const Option = Select.Option;

// const formItemLayout = {
//   labelCol: {
//     xs: { span: 24 },
//     sm: { span: 10 },
//   },
//   wrapperCol: {
//     xs: { span: 24 },
//     sm: { span: 14 },
//   },
// };

const scales = [
  { unit: 'month', step: 1, format: 'MMMM yyy' },
  { unit: 'day', step: 1, format: 'd' },
];

const columns = [{ name: 'port', label: 'Ports', width: '100%' }];


const links = [{ source: 2, target: 1, type: 0 }];

class PortShedule extends Component {
  state = {
    filter:false,
    filterData:null,
    frmName: 'port_vessel_schedule',
    formData: {},
    modals: {
      PortScheduleReport: false,
    },
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    modals: {
      VesselScheduleReport: false,
    },
    data: [],
    loadingComp: false,
    columns: [
      {
        title: 'Port',
        dataIndex: 'port',
      },
      {
        title: 'Port Id',
        dataIndex: 'port_id',
      },
    ],
  };

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async (search = {}) => {
    // console.log("search..",search)
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers ='';

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      headers={}
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc = { OR: {} };
        wc['OR'][search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    if (
      search &&
      search.hasOwnProperty('OR')
    ){
      headers={}
      headers['where'] = search;
    }


    this.setState({
      data: [],
    }, () => {
      this.setState({
        loadingComp: true,
      })
    });

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/vessel-schedule/port-list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;

    let arr = []
    if (data && data.data.length > 0) {
      data.data.map((val, index) => {
        let date1 = new Date(val.commence_date && val.commence_date.split(':')[0])
        let date2 = new Date(val.departure && val.departure.split(':')[0])
        let diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);
        let obj = {
          id: val.id,
          start_date: val.commence_date && val.commence_date.split(':')[0],
          duration: diffDays,
          port: val.port,
          port_id: val.port_id,
          miles: val.miles,
        }
        arr.push(obj)
      })
    }
    let state = {
      loadingComp: false,
    };
    if (arr.length > 0 && totalRows > this.state.data.length) {
      state['data'] = arr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loadingComp: false,
    });
    // this.setState({
    //   data:arr
    // })
  }

  callOptionsComp = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.fetchData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);;

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
      })
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }
      if(this.state.filter){
        this.setState({ ...this.state, pageOptions: pageOptions }, () => {
          this.fetchData(this.state.filterData);
        });
      }
      else{
        this.setState({ ...this.state, pageOptions: pageOptions }, () => {
          this.fetchData();
        });
      }
     
    }
  };


  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  saveFormData = (postData, innerCB) => {
    if(Object.keys(postData).length!=0){
      let wc = {};
      wc = { OR: {} };
      Object.keys(postData).map((key,ind)=>{
        wc['OR'][key] = { l: postData[key]};
  
      })
      this.setState({
        filter:true,
        filterData:wc
      },()=>this.fetchData(this.state.filterData))
      
    }
  
  };

  clearFilter = () => {
    this.setState({
      filterData:null,
      filter:false
    },()=>this.fetchData())
    
  }

  render() {
    const { formData, frmName, loadingComp, pageOptions, search } = this.state;

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <h4>Port Schedule</h4>
                    </ul>
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span
                          className="text-bt"
                          onClick={() => this.showHideModal(true, 'PortScheduleReport')}
                        >
                          Reports
                        </span>
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
              <hr />

              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body">
                      <NormalFormIndex
                        key={'key_' + frmName + '_0'}
                        formClass="label-min-height"
                        formData={formData}
                        showForm={true}
                        frmCode={frmName}
                        addForm={true}
                        showToolbar={[
                          {
                            isLeftBtn: [
                              {
                                key: 's2',
                                isSets: [
                                  {
                                    id: '3',
                                    key: 'save',
                                    type: <SaveOutlined />,
                                    withText: '',
                                    event: (key, data, innerCB) => this.saveFormData(data, innerCB),
                                  },
                                  {
                                    id: '4',
                                    key: 'close',
                                    type:  <CloseOutlined />,
                                    withText: '',
                                    event: (key, data, innerCB) => this.clearFilter(data, innerCB),
                                  },
                                ],
                              },
                            ],
                            isRightBtn: [],
                            isResetOption: false,
                          },
                        ]}
                        inlineLayout={true}
                      />
                    </div>
                  </div>
                </article>
              </div>

              {/* <div className="row p10">
                <div className="col-md-12">
                  <Form>
                    <div className="row">
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Date from/To">
                          <Select defaultValue="1">
                            <Option value="1">Month</Option>
                            <Option value="2">Week</Option>
                            <Option value="3">Year</Option>
                          </Select>
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Country">
                          <Input size="default" />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Port">
                          <Input size="default" />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Port Region">
                          <Input size="default" />
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Trade Area">
                          <Input size="default" />
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Vessel Type">
                          <Input size="default" />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Voyage Status">
                          <Select defaultValue="1">
                            <Option value="1">Schedule</Option>
                            <Option value="2">Commence</Option>
                            <Option value="3">Completed</Option>
                          </Select>
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Vessel Status">
                          <Input size="default" />
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Port Function">
                          <Input size="default" />
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Charter">
                          <Input size="default" />
                        </FormItem>
                      </div>
                    </div>
                  </Form>
                </div>
              </div> */}

              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loadingComp === false ?
                    <ToolbarUI
                      routeUrl={'Vessel-schedule-list-toolbar'}
                      optionValue={{ pageOptions: pageOptions, columns: this.state.columns, search: search }}
                      callback={e => this.callOptionsComp(e)}
                    />
                    :
                    undefined
                }
              </div>
              <div className="row p10">
                <div className="col-md-12">
                  <div className="border p-2 wx-default">
                  {
                  loadingComp === false ?
                    <Gantt scales={scales} columns={columns} tasks={this.state.data} links={links} />
                 :
                 <Spin tip="Loading...">
                 <Alert
                   message=" "
                   description="Please wait..."
                   type="info"
                 />
               </Spin>
  }
                    </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <Modal
          style={{ top: '2%' }}
          title="Reports"
         open={this.state.modals['PortScheduleReport']}
          onCancel={() => this.showHideModal(false, 'PortScheduleReport')}
          width="95%"
          footer={null}
        >
          <PortScheduleReport />
        </Modal>
      </div>
    );
  }
}

export default PortShedule;

