import React from "react";
import { Component } from "react";
import { Card, Typography, Row, Col} from "antd";
import {DollarOutlined} from '@ant-design/icons';
const { Title, Text } = Typography;
class DashCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: this.props.title,
            current_price: this.props.current_price,
            present: this.props.present,
            style:this.props.style
        }


    }

    render() {

        const { title, current_price, present ,style} = this.state
       
  

        return <div className="body-wrapper modalWrapper">
            <article className="article toolbaruiWrapper">
                <div className="box box-default">
                    <div className="box-body1">
                       
                        <Card bordered={false}>
                            <div className="number">
                                <Row align="middle" >
                                    <Col xs={18}>
                                        <span>{title}</span>

                                        <Title level={3}>
                                            {current_price} <small className={style}>
                                                {present}
                                            </small>
                                        </Title>
                                    </Col>

                                    <Col xs={6}>
                                        <div className="icon-box">
                                        <DollarOutlined />
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Card>
                    </div>
                </div>
            </article>
        </div>;
    }



};

export default DashCard;