import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

//const Option = Select.Option;

let pie2 = {};

// pie2.option = {
//   tooltip: {
//     trigger: 'item',
//     formatter: '{a} <br/>{b} : {c} ({d}%)',
//   },
//   legend: {
//     orient: 'verticle',
//     x: 'right',
//     data: ['Direct', 'Email', 'Affiliate', 'Video Ads', 'Search'],
//     textStyle: {
//       color: CHARTCONFIG.color.text,
//     },
//   },

//   calculable: true,
//   series: [
//     {
//       name: 'Vessel Filters',
//       type: 'pie',
//       radius: ['50%', '70%'],
//       color:['#1E90FF','#FFFF33','#FDBD01','#46C7C7','#E30B5D'],
//       itemStyle: {
//         normal: {
//           label: {
//             show: false,
//           },
//           labelLine: {
//             show: false,
//           },
//         },
//         emphasis: {
//           label: {
//             show: true,
//             position: 'center',
//             textStyle: {
//               fontSize: '15',
//               fontWeight: 'bold',
//             },
//           },
//         },
//       },
//       data: [
//         { value: 335, name: 'Direct' },
//         { value: 310, name: 'Email' },
//         { value: 234, name: 'Affiliate' },
//         { value: 135, name: 'Video Ads' },
//         { value: 1548, name: 'Search' },
//       ],
//     },
//   ],
// };

pie2.option = {
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b}: {c} ({d}%)'
  },
  legend: {
    data: [
      'Direct',
      'Marketing',
      'Search Engine',
      'Email',
      'Union Ads',
      'Video Ads',
      'Baidu',
      'Google',
      'Bing',
      'Others'
    ]
  },
  series: [
    {
      name: 'Access From',
      type: 'pie',
      selectedMode: 'single',
      radius: [0, '30%'],
      label: {
        position: 'inner',
        fontSize: 14
      },
      labelLine: {
        show: false
      },
      data: [
        { value: 1548, name: 'Search Engine' },
        { value: 775, name: 'Direct' },
        { value: 679, name: 'Marketing', selected: true }
      ]
    },
    {
      name: 'Access From',
      type: 'pie',
      radius: ['45%', '60%'],
      labelLine: {
        length: 30
      },
      label: {
        formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
        backgroundColor: '#F6F8FC',
        borderColor: '#8C8D8E',
        borderWidth: 1,
        borderRadius: 4,
        rich: {
          a: {
            color: '#6E7079',
            lineHeight: 22,
            align: 'center'
          },
          hr: {
            borderColor: '#8C8D8E',
            width: '100%',
            borderWidth: 1,
            height: 0
          },
          b: {
            color: '#4C5058',
            fontSize: 14,
            fontWeight: 'bold',
            lineHeight: 33
          },
          per: {
            color: '#fff',
            backgroundColor: '#4C5058',
            padding: [3, 4],
            borderRadius: 4
          }
        }
      },
      data: [
        { value: 1048, name: 'Baidu' },
        { value: 335, name: 'Direct' },
        { value: 310, name: 'Email' },
        { value: 251, name: 'Google' },
        { value: 234, name: 'Union Ads' },
        { value: 147, name: 'Bing' },
        { value: 135, name: 'Video Ads' },
        { value: 102, name: 'Others' }
      ]
    }
  ]
};



const Chart = () => (
  <div className="box box-default mb-4">
    
    <div className="box-body">
      <ReactEcharts option={pie2.option} theme={'macarons'} style={{height:'833px'}} />
      {/* <p className="text-center m-0">Profit & Loss Analytics</p> */}
    </div>
  </div>
);

export default Chart;
