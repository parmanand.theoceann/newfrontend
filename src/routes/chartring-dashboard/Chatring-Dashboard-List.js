import React, { useEffect } from "react";
import { Table, Popconfirm, Card, Col, Row, Select, Button, Flex } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import { useNavigate } from "react-router-dom";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";
import PieChart from "../dashboard/charts/PieChart";
import DoughNutChart from "../dashboard/charts/DoughNutChart";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import RoundBarChart from "../dashboard/charts/RoundBarChart";

const ChatringDashList = () => {
  const totalDashboarddat = [
    {
      title: "Count Of Vessel",
      value: "abcd",
    },
    {
      title: " Total Voyage days",
      value: "abcd",
    },
    {
      title: "Avg Sea Cons. ",
      value: "abcd",
    },
    {
      title: "Avg Port Cons.",
      value: "abcd",
    },
    {
      title: "Total Bunker Cons.",
      value: "abcd",
    },
    {
      title: "Total Revenue",
      value: "abcd",
    },
    {
      title: "Total Expense",
      value: "abcd",
    },
    {
      title: " Net Result",
      value: "abcd",
    },
    {
      title: "Avg Daily Profit/Loss",
      value: "abcd",
    },
  ];
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
    graphCardData: {},
  });

  const navigate = useNavigate();

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
                <Popconfirm
                  title="Are you sure, you want to delete it?"
                  onConfirm={() => onRowDeletedClick(record)}
                >
                 <DeleteOutlined /> />
                </Popconfirm>
              </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["chat-dash-list"]
        ? FIELDS["chat-dash-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let qParamsGraph = { p: pageOptions.pageIndex, l: 0 };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let qParamStringGraph = objectToQueryStringFunc(qParamsGraph);
    // let _url = `${URL_WITH_VERSION}/tcov/list?${qParamString}`;
    let _url = `${URL_WITH_VERSION}/chattering-dashboard/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    let _urlGrpah = `${URL_WITH_VERSION}/chattering-dashboard/list?${qParamStringGraph}`;
    let responseGraph = await getAPICall(_urlGrpah, headers);
    let dataGraph = await responseGraph;
   
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];

    let _dataArr = [];
    let graphData = {};

    for (let i = 0; i < data?.data?.actual.length; i++) {
      let actual = data.data.actual[i];
      let dataObj = {};
      for (let key in actual) {
        if (
          key !== "expenses" &&
          key !== "revenue" &&
          key !== "voyage_result"
        ) {
          dataObj[key] = actual[key];
        }
      }

      _dataArr.push(dataObj);

      let graph = data.data.graph;

      for (let key in graph) {
        if (key === "vessel_count") {
          graphData[key] = graph[key];
        } else if (key === "total_voyage_days") {
          graphData[key] = graph[key];
        } else if (key === "avg_sea_cons") {
          graphData[key] = graph[key];
        } else if (key === "avg_port_cons") {
          graphData[key] = graph[key];
        } else if (key === "total_bunker_cons") {
          graphData[key] = graph[key];
        } else if (key === "total_revenue") {
          graphData[key] = graph[key];
        } else if (key === "total_expenses") {
          graphData[key] = graph[key];
        } else if (key === "net_result") {
          graphData[key] = graph[key];
        } else if (key === "avg_daily_profit_loss") {
          graphData[key] = graph[key];
        }
      }
    }

    let _state = { loading: false };
    if (_dataArr.length > 0) {
      _state["responseData"] = _dataArr;
    }

    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      graphCardData: graphData,
      loading: false,
    }));
  };
  const redirectToAdd = (e, record = null) => {
    navigate(`/edit-voyage-estimate/${record.estimate_id}`);
  };

  const onCancel = () => {
    // getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  };

  /*
      onRowDeletedClick = (record) => {
        if ( record && record['tcov_status'] && record['tcov_status'].toUpperCase() === 'PENDING' ) {
          let _url = `${URL_WITH_VERSION}/tcov/delete`;
          apiDeleteCall(_url, { id: record.id }, (response) => {
            if (response && response.data) {
              openNotificationWithIcon('success', response.message);
              getTableData(1);
            } else {
              openNotificationWithIcon('error', response.message);
            }
          });
        } else {
          openNotificationWithIcon('error', "You can't delete TCOV Record as it is fixed ( OR Voyage Is Generated )");
        }
      };
    
    */

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    sidebarVisible,
    search,
    formData,
    isShowClaimInvoice,
    laytimeID,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "TCOVFULL304",
    "TCOVFULL305",
    "TCOVFULL306",
    "TCOVFULL307",
    "TCOVFULL308",
  ];
  const StackHorizontalChartyAxis = [
    "BALTIC VOYAGER",
    "CARIBBEN PRINCESS",
    "PACIFIC EXPLORER",
    "GOODWYN ISLAND",
  ];
  const StackHorizontalChartSeries = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
    {
      name: "Total Port Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 777],
    },
    {
      name: "Total Bunkar Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [220, 182, 191, 888],
    },
  ];
  const ClusterDataSeries = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
    {
      name: " Total Expense,",
      type: "bar",

      data: [220, 182, 191, 234, 290],
    },
    {
      name: "Net Revenue",
      type: "bar",
      data: [150, 232, 201, 154, 190],
    },
  ];
  const PieChartData = [
    { value: 40, name: "OCEANIC MAJESTY" },
    { value: 30, name: "PACIFIC EXPLORER" },
    { value: 13, name: "CARIBBEN PRINCESS" },
    { value: 10, name: "CARGO SHIP" },
    { value: 7, name: "The Atlantic" },
  ];
  const RoundBarChartxAxis = [
    "TCOV-FULL-01077",
    "TCTO-FULL-00368",
    "TCOV-FULL-01022",
    "TCOV-FULL-01024",
    "TCTO-FULL-00369",
    'TCOV-FULL-01084',
    'TCOV-FULL-01082',
  ];
  const RoundBarChartSeriesData = [120, 200, 150, 80, 70, 110, 130];
  const DoughNutChartSeriesData = [
    { value: 50, name: "Terneuzen" },
    { value: 13, name: "CARIBBEN PRINCESS" },
    { value: 20, name: "PACIFIC EXPLORER" },
    { value: 10, name: "GOODWYN ISLAND" },
    { value: 7, name: "The Atlantic" },
  ];

  return (
    <>
      {/* <div className="d-flex">
        <div className="container-fluid">
          <div className="row mt-2 " style={{ width: "75vw" }}>
            {totalDashboarddat.map((items, index) => (
              <div
                key={index}
                className="col-12 col-sm-6 col-md-6 col-lg-4 mb-2"
              >
                <div className="card ">
                  <div className="card-body">
                    <h5 className="card-title">{items.title}</h5>
                    <h3 className="card-subtitle mb-2 text-muted">
                      {items.value}
                    </h3>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div> */}
      <Row
        gutter={[16, 16]}
        style={{ justifyContent: "space-between", margin: "1rem 1rem" }}
      >
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Count of Vessel
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.vessel_count}
            </p>
          </Card>
        </Col>
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Voyage days
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.total_voyage_days}
            </p>
          </Card>
        </Col>
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Avg Sea Cons.
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.avg_sea_cons} mt
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Avg Port Cons.
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.avg_port_cons} mt
            </p>
          </Card>
        </Col>
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Bunker Cons.
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.total_bunker_cons} mt
            </p>
          </Card>
        </Col>
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Revenue
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.total_revenue} $
            </p>
          </Card>
        </Col>
        <Col span={2.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Expense
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.total_expenses} $
            </p>
          </Card>
        </Col>
      </Row>
      <Row gutter={[16]} style={{ margin: "1rem 1rem", gap: "6px" }}>
        <Col span={3.5}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Net Result
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.net_result} $
            </p>
          </Card>
        </Col>
        <Col span={3}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Avg Daily Profit/Loss
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              {state.graphCardData.avg_daily_profit_loss} $
            </p>
          </Card>
        </Col>
        <Col span={15}>
          <Row
            gutter={[16, 16]}
            style={{ justifyContent: "space-between", marginTop: "1rem" }}
          >
            <Col span={4}>Vessel</Col>
            <Col span={4}>Voyage no.</Col>
            <Col span={4}>Cargo Name</Col>
            <Col span={4}>Commence date</Col>
            <Col span={4}>completion date</Col>
            <Col span={4}>Status</Col>
          </Row>
          <Row
            gutter={[16, 16]}
            style={{ justifyContent: "space-between", marginTop: "1rem" }}
          >
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "SUPRAMAX",
                    label: "SUPRAMAX",
                  },
                  {
                    value: "PANAMAX",
                    label: "PANAMAX",
                  },
                  {
                    value: "KAMSARMAX",
                    label: "KAMSARMAX",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-25",
                    label: "2024-01-25",
                  },
                  {
                    value: "2024-01-24",
                    label: "2024-01-24",
                  },
                  {
                    value: "2024-01-15",
                    label: "2024-01-15",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-08-13",
                    label: "2024-08-13",
                  },
                  {
                    value: "2024-01-24",
                    label: "2024-01-24",
                  },
                  {
                    value: "2024-01-15",
                    label: "2024-01-15",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PENDING",
                    label: "PENDING",
                  },
                  {
                    value: "FIX",
                    label: "FIX",
                  },
                  {
                    value: "SCHEDUED",
                    label: "SCHEDUED",
                  },
                ]}
              />
            </Col>
          </Row>
        </Col>
        <Col span={2}>
          <Button>Reset</Button>
        </Col>
      </Row>

      <div>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              style={{ padding: "0" }}
              Heading={"P & L Per Voyage"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </Col>
          <Col span={12}>
            <StackHorizontalChart
              Heading={"Bunker Consp. Days Per Vessel"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              StackHorizontalChartSeries={StackHorizontalChartSeries}
              style={{ padding: "0" }}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12} style={{ padding: "0" }}>
            <PieChart
              PieChartData={PieChartData}
              Heading={"Total Voyage Days Per Vessel"}
            />
          </Col>
          <Col span={12}>
            <DoughNutChart
              // style={{ padding: "0" }}
              DoughNutChartSeriesData={DoughNutChartSeriesData}
              DoughNutChartSeriesRadius={["40%", "70%"]}
              Heading={"Total Bunker Consumption per Vessel"}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <RoundBarChart
              style={{ padding: "0",  }}
              RoundBarChartxAxis={RoundBarChartxAxis}
              maxValueyAxis={"500"}
              RoundBarChartSeriesData={RoundBarChartSeriesData}
              Heading={"Net result (profit & Loss) VS Voyage number"}
            />
          </Col>
        </Row>
        {/* <div className="row">
          <div className="col-md-6">
            <ClusterColumnChart
              Heading={"P & L Per Voyage"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </div>
          <div className="col-md-6">
            <StackHorizontalChart
              Heading={"Bunker Consp. Days Per Vessel"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              StackHorizontalChartSeries={StackHorizontalChartSeries}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <PieChart
              PieChartData={PieChartData}
              Heading={"Total Voyage Days Per Vessel"}
            />
          </div>
          <div className="col-md-6">
            <DoughNutChart
              DoughNutChartSeriesData={DoughNutChartSeriesData}
              DoughNutChartSeriesRadius={["40%", "70%"]}
              Heading={"Total Bunker Consumption per Vessel"}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <RoundBarChart
              RoundBarChartxAxis={RoundBarChartxAxis}
              maxValueyAxis={"500"}
              RoundBarChartSeriesData={RoundBarChartSeriesData}
              Heading={"Net result (profit & Loss) VS Voyage number"}
            />
          </div>
        </div> */}
      </div>

      {/* <ChatringDashList /> */}
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span> Chartring Dashboard List</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {state.loading === false ? (
                  <ToolbarUI
                    routeUrl={"chartring-dashboard-list"}
                    optionValue={{
                      pageOptions: state.pageOptions,
                      columns: state.columns,
                      search: state.search,
                    }}
                    callback={(e) => callOptions(e)}
                    // filter={filter}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "tcov"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "tcov"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "tcov"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableColumns}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={tableColumns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
      </div>
    </>
  );
};

export default ChatringDashList;
