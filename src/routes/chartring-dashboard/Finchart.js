import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

import { Select } from 'antd';
import { objectOf } from 'prop-types';

const Option = Select.Option;

class FinLineChart extends Component {
    constructor(props) {
        super(props);
        let graphData = {

        }
        this.state = {
            // graphData: this.props.chartData,

        }



        this.option = {
            xAxis: {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [820, 932, 901, 934, 1290, 1330, 1320],
                    type: 'line',
                    smooth: true
                }
            ]
        };


    }

    render() {

        const { graphData } = this.state;
        return (
            <div className="box box-default mb-4">
                {/* <div className="box-header">
      <h4 className='Char-dashboard'>
        Estimate VS Actual Compare
        <span className="float-right">
          <Select size="default" defaultValue="1">
            <Option value="1">Profit & Loss</Option>
            <Option value="2">Profit</Option>
            <Option value="3">Loss</Option>
          </Select>
        </span>
      </h4>
    </div> */}
                <div className="box-body">

                    <ReactEcharts
                        option={this.option}
                        // data={graphData.estimate_actual_compare.vessel_name} 
                        theme={'macarons'} />
                </div>
            </div>
        );
    }
}

export default FinLineChart;




export class FinRevExpChart extends Component {
    constructor(props) {
        super(props)
        let graphData = {

        }
        this.state = {
            // graphData: this.props.chartData,
        }
        this.option = {
            title: {
                text: 'Revenue vs Expense',
                subtext: 'Monthly Data'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['Revenue', 'Expense']
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: { show: true, readOnly: false },
                    magicType: { show: true, type: ['line', 'bar'] },
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    // prettier-ignore
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: 'Revenue',
                    type: 'bar',
                    data: [
                        2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3
                    ],
                    markPoint: {
                        data: [
                            { type: 'max', name: 'Max' },
                            { type: 'min', name: 'Min' }
                        ]
                    },
                    markLine: {
                        data: [{ type: 'average', name: 'Avg' }]
                    }
                },
                {
                    name: 'Expense',
                    type: 'bar',
                    data: [
                        2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3
                    ],
                    markPoint: {
                        data: [
                            { name: 'Max', value: 182.2, xAxis: 7, yAxis: 183 },
                            { name: 'Min', value: 2.3, xAxis: 11, yAxis: 3 }
                        ]
                    },
                    markLine: {
                        data: [{ type: 'average', name: 'Avg' }]
                    }
                }
            ]
        };
    }

    render() {
        return (
            <div className="box box-default mb-4">

                <div className="box-body">

                    <ReactEcharts
                        option={this.option}
                        // data={graphData.estimate_actual_compare.vessel_name} 
                        theme={'macarons'} />
                </div>
            </div>
        )
    }


}



export class FinPieChart extends Component {
    constructor(props) {
        super(props)
        this.state = {
          innerdata:Object.assign([],this.props.innerdata ||[]),
          outerdata:Object.assign([],this.props.outerdata ||[])
        }

        this.option = {
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            legend: {
                
                  legend: {
                orient: 'vertical',
                left: 'left'
            },


            },
            series: [
                {
                   name: 'Data From',
                    type: 'pie',
                    selectedMode: 'single',
                    radius: [0, '30%'],
                    // label: {
                    //     position: 'inner',
                    //     fontSize: 14
                    // },
                    // labelLine: {
                    //     show: false
                    // },
                     emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    data:this.state.innerdata
                },





                
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: ['45%', '60%'],
                    labelLine: {
                        length: 30
                    },
                    // label: {
                    //     formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
                    //     backgroundColor: '#F6F8FC',
                    //     borderColor: '#8C8D8E',
                    //     borderWidth: 1,
                    //     borderRadius: 4,
                    //     rich: {
                    //         a: {
                    //             color: '#6E7079',
                    //             lineHeight: 22,
                    //             align: 'center'
                    //         },
                    //         hr: {
                    //             borderColor: '#8C8D8E',
                    //             width: '100%',
                    //             borderWidth: 1,
                    //             height: 0
                    //         },
                    //         b: {
                    //             color: '#4C5058',
                    //             fontSize: 14,
                    //             fontWeight: 'bold',
                    //             lineHeight: 33
                    //         },
                    //         per: {
                    //             color: '#fff',
                    //             backgroundColor: '#4C5058',
                    //             padding: [3, 4],
                    //             borderRadius: 4
                    //         }
                    //     }
                    // },
                    data: this.state.outerdata
                }
            ]
        };



    }

    render() {
        return (
            <div className="box box-default mb-4">

                <div className="box-body">
                    <ReactEcharts option={this.option} theme={'macarons'} style={{ height: '833px' }} />
                  
                </div>
            </div>

        )
    }



}




export class FinPieChart1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            graphdata: this.props.graphdata||{}
        }

        this.option = {
            title: {
                text: 'Total Expenses By',
                //subtext: 'Fake Data',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    //name: 'Access From',
                    type: 'pie',
                    radius: '50%',
                    data: this.state.graphdata,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };



    }

    render() {
        return (
            <div className="box box-default mb-4">

                <div className="box-body">
                    <ReactEcharts option={this.option} theme={'macarons'} style={{ height: '433px' }} />

                </div>
            </div>

        )
    }
}


export class FinPieChart2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            graphdata: this.props.graphdata
        }

        this.option = {
            title: {
                text: 'Total Invoice Due',
                //subtext: 'Fake Data',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
      
                    return `${params.seriesName}<br />
                            ${params.name}: ${params.data.value}<br />
                            ${params.data.name1}: ${params.data.value1}`;
                  }
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    name: 'Invoice Due By',
                    type: 'pie',
                    radius: '50%',
                    data: this.props.graphdata,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };



    }

    render() {
        return (
            <div className="box box-default mb-4">

                <div className="box-body">
                    <ReactEcharts option={this.option} theme={'macarons'} style={{ height: '433px' }} />

                </div>
            </div>

        )
    }



}

