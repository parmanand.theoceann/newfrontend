import React, {Component}from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

import CHARTCONFIG from '../../constants/chartConfig';
import { Select } from 'antd';

const Option = Select.Option;

class BarChart extends Component {
  constructor(props) {
    super(props);
    let graphData ={
      
    }
    this.state={
      graphData: this.props.chartData,
      
    }
    

this.option = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
  },
  legend: {
    data: ['Estimate', 'Actual Compare'],
    textStyle: {
      color: CHARTCONFIG.color.text,
    },
  },
  calculable: true,
  xAxis: [
    {
      type: 'category',
      data: this.state.graphData,
      axisLabel: {
        textStyle: {
          color: CHARTCONFIG.color.text,
        },
      },
      splitLine: {
        lineStyle: {
          color: CHARTCONFIG.color.splitLine,
        },
      },
    },
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          color: CHARTCONFIG.color.text,
        },
      },
      splitLine: {
        lineStyle: {
          color: CHARTCONFIG.color.splitLine,
        },
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: CHARTCONFIG.color.splitArea,
        },
      },
    },
  ],
  series: [
    {
      name: 'Estimate',
      type: 'bar',
   //   data: this.state.graphData.profit_loss,
    },
    {
      name: 'Actual Compare',
      type: 'bar',
      data: [320, 332, 301, 334, 390, 330, 320],
      color: '#967BB6',
    },
  ],

    }
  }

    render() {
      
      const{graphData}= this.state;
      return (
  <div className="box box-default mb-4">
    <div className="box-header">
      <h4 className='Char-dashboard'>
        Estimate VS Actual Compare
        <span className="float-right">
          <Select size="default" defaultValue="1">
            <Option value="1">Profit & Loss</Option>
            <Option value="2">Profit</Option>
            <Option value="3">Loss</Option>
          </Select>
        </span>
      </h4>
    </div>
    <div className="box-body">
      
      <ReactEcharts 
      option={this.option} 
      // data={graphData.estimate_actual_compare.vessel_name} 
      theme={'macarons'} />
    </div>
  </div>
);
}
}

export default BarChart;
