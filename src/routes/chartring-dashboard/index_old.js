// import React, { Component, useState } from 'react';
// import { Form, Input, Select, Pagination, Icon, Progress, Modal, Menu, Dropdown, DatePicker } from 'antd';
// import URL_WITH_VERSION, { getAPICall, ResizeableTitle } from '../../shared';
// import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
// import ShipDetail from '../dynamic-vspm/ShipDetail';
// import BarChart from './BarChart';
// import Paichart from './Paichart';
// import ChartringMap from './ChartringMap';
// // import { isDate } from 'moment';
// const FormItem = Form.Item;
// const Option = Select.Option;
// const { RangePicker, dateRanges } = DatePicker;
// const AnyReactComponent = ({ text }) => (
//   <div style={{
//     color: 'white',
//     padding: '15px 10px',
//     display: 'inline-flex',
//     textAlign: 'center',
//     alignItems: 'center',
//     justifyContent: 'center',
//     transform: 'translate(-50%, -50%)'
//   }}>
//     <img alt="Ship"
//       src="../../assets/ship.png"
//       height="30"
//       className="map-ship2"
//       onClick={() => text(true, 'ShipDetails1')}
//     />
//   </div>
// );

// class ChartringDashboard extends Component {

//   constructor(props) {
//     super(props);

//     this.state = {
//       data: [],
//       visible: false,
//       countries: [],
//       tradeAreas: [],
//       tradeUser: [],
//       voyOps: [],
//       items: [],
//       DataisLoaded: false,
//       cargo: [],
//       vessel: [],
//       VesselFixed: [],
//       vesselValue: 'Vessel type',
//       tradeAreasValue: 'Trade area',
//       tradeUserValue: 'Trade User',
//       VoyOpsValue: 'Voy Ops Type',
//       vesselName: 'Vessel Name',
//       selectVesselValue: 'Select Vessel',
//       voyageActual: [],
//       chattering: [],
//       chartData: {},
//       modals: {
//         ShipDetails1: false,
//       },
//       title: `vessel Name:VSL1412211\nlat:18.5204\nlng:73.8567`,
//       mapSettings: {
//         center: { lat: 20.5937, lng: 78.9629 },
//         zoom: 2
//       },
//       latlng: [],
//       shipData: null,
//       vessel_name: 'Spring'
//     };
//   }


//   components = {
//     header: {
//       cell: ResizeableTitle,
//     },
//   };


//   componentDidMount = async () => {

//     this.setState({
//       loading: true,
//     });
//     var requestOptions = {
//       method: "GET",
//       redirect: "follow",
//     };

//     fetch(
//       `${process.env.REACT_APP_URL}v1/vessel/live/list?p=0&l=2`,
//       requestOptions
//     )
//       .then((response) => response.json())
//       .then((result) => {
//         this.setState({
//           data: result.data,
//           loading: false,
//         });
//       })
//       .catch((error) => undefined);












//     //List of Trade Areas
//     let _urlTrade = `${URL_WITH_VERSION}/master/list?t=trade&l=0`;
//     let resquestTrade = await getAPICall(_urlTrade);
//     let responseTrade = await resquestTrade;
//     this.setState({ ...this.state, tradeAreas: responseTrade['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     //List of Trade User
//     let _urlUser = `${URL_WITH_VERSION}/vessel/list?t=trade&l=0`;
//     let resquestUser = await getAPICall(_urlUser);
//     let responseUser = await resquestUser;
//     this.setState({ ...this.state, tradeUser: responseUser['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );

//     //List of Vessel
//     let _urlVessel = `${URL_WITH_VERSION}/vessel/list?t=trade&l=0`;
//     let resquestVessel = await getAPICall(_urlVessel);
//     let responseVessel = await resquestVessel;
//     this.setState({ ...this.state, vessel: responseVessel['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     //List of OPS
//     let _urlOps = `${URL_WITH_VERSION}/vessel/list?t=trade&l=0`;
//     let resquestOps = await getAPICall(_urlOps);
//     let responseOps = await resquestOps;
//     this.setState({ ...this.state, voyOps: responseOps['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     //List of OPS
//     let _urlVesselId = `${URL_WITH_VERSION}/vessel/list?t=trade&l=1`;
//     let resquestVesselId = await getAPICall(_urlVesselId);
//     let responseVesselId = await resquestVesselId;
//     this.setState({ ...this.state, vesselId: responseVesselId['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     //List of Vessel Name
//     let _urlVesselNameId = `${URL_WITH_VERSION}/vessel/list?p=1&l=20`;
//     let resquestVesselNameId = await getAPICall(_urlVesselNameId);
//     let responseVesselNameId = await resquestVesselNameId;
//     this.setState({ ...this.state, vesselnameId: responseVesselNameId['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     //List of Chattering-dashboard 
//     let _urlChatteringId = `${URL_WITH_VERSION}/chattering-dashboard/list`;
//     let resquestChatteringId = await getAPICall(_urlChatteringId);
//     let responseChatteringId = await resquestChatteringId;
//     this.setState({ ...this.state, chattering: responseChatteringId['data'] }, () =>
//       this.setState({ ...this.state, visible: true })
//     );
//     // google-map
//     let headers = { order_by: { id: 'desc' } };
//     let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/graph?`;
//     const response = await getAPICall(_url, headers);
//     const data = await response;
//     this.setState({
//       ...this.state,
//       latlng: data['data']
//     });
//   };


//   VesselNameClick = (value) => {
//     this.setState({ vesselName: value.vessel_name });
//   };
//   VesselNameList = () => {
//     const { tradeUser } = this.state;
//     return (
//       <Menu>
//         {tradeUser.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.VesselNameClick(x)}>{x.vessel_name}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     )
//   }
//   tradeAreasClick = (value) => {
//     this.setState({ tradeAreasValue: value.description });
//   };
//   getInput = () => {
//     const { inputType } = this.props;
//     switch (inputType) {
//       case "dropdown": return this.getSelectField()
//       case "number": return this.getInputNumberField()
//       default: return this.getInputField()
//     }
//   };
//   // function for List of Trade Areas
//   tradeAreasList = () => {
//     const { tradeAreas } = this.state;
//     return (
//       <Menu>
//         {tradeAreas.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.tradeAreasClick(x)}>{x.description}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     );
//   };
//   tradeUserClick = (value) => {
//     this.setState({ tradeUserValue: value.description });
//   };
//   tradeUserList = () => {
//     const { tradeUser } = this.state;
//     return (
//       <Menu>
//         {tradeUser.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.tradeUserClick(x)}>{x.description}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     )
//   }

//   voyOpsList = () => {
//     const { voyOps } = this.state;
//     return (
//       <Menu>
//         {voyOps.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.voyOpsClick(x)}>{x.description}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     )
//   }

//   vesselClick = (value) => {
//     this.setState({ vesselValue: value.vessel_type_name });
//   };

//   // function for List of vessel
//   vesselList = () => {
//     const { vessel } = this.state;
//     return (
//       <Menu>
//         {vessel.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.vesselClick(x)}>{x.vessel_type_name}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     );
//   };

//   // function for vessel value

//   vesselSelectList = () => {
//     const { vessel } = this.state;
//     return (
//       <Menu>
//         {vessel.map((x, index) => (
//           <Menu.Item key={index}>
//             <a onClick={() => this.vesselSelectClick(x)}>{x.description}</a>
//           </Menu.Item>
//         ))}
//       </Menu>
//     )
//   }

//   vesselSelectClick = (value) => {
//     this.setState({ vesselSelectValue: value.vessel_type_name });
//   };
//   showHideModal = (visible, modal, id) => {
//     const { modals } = this.state;
//     let _modal = {};
//     _modal[modal] = visible;
//     this.setState({
//       ...this.state,
//       modals: Object.assign(modals, _modal),
//       shipData: id
//     });
//   };

//   render() {
//     const { vesselValue, tradeAreasValue, tradeUserValue, VoyOpsValue, mapSettings, shipData,
//       chattering, vesselName, vesselSelectValue, chartData } = this.state;
//     const style = {

//       // position: 'absolute',
//       height: '91%',
//       width: '96%'

//     };
//     return (

//   <div className='body-wrapper modalWrapper'>

//          <article className="article toolbaruiWrapper">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="row p10">
//                 <div className="col-12">
//                   <h4 className='Char-dashboard'>Filters</h4>
//                   <div className="card-group p-0 border-0 rounded-0">
//                     <div className="card p-0 border-0 rounded-0">
//                       <div className="card-body border-0 rounded-0 pl-0">
//                         <RangePicker />
//                       </div>
//                     </div>
//                     <div className="card p-0 border-0 rounded-0">
//                       <div className="card-body border-0 rounded-0 pl-0">
//                         <Dropdown overlay={this.VesselNameList()} trigger={['click']}>
//                           <a className="ant-dropdown-link">
//                             <Input size="default" placeholder={vesselName} />
//                           </a>
//                         </Dropdown>
//                       </div>
//                     </div>
//                     <div className="card p-0 border-0 rounded-0">
//                       <div className="card-body border-0 rounded-0">
//                         <Dropdown overlay={this.vesselList()} trigger={['click']}>
//                           <a className="ant-dropdown-link">
//                             <Input size="default" placeholder={vesselValue} />
//                           </a>
//                         </Dropdown>
//                       </div>
//                     </div>

//                     <div className="card border-0 rounded-0 p-0">
//                       <div className="card-body border-0 rounded-0">
//                         <Dropdown overlay={this.tradeAreasList()} trigger={['click']}>
//                           <a className="ant-dropdown-link">
//                             <Input size="default" placeholder={tradeAreasValue} />
//                           </a>
//                         </Dropdown>
//                       </div>
//                     </div>

//                     <div className="card border-0 rounded-0 p-0">
//                       <div className="card-body border-0 rounded-0">
//                         <Dropdown overlay={this.tradeUserList()} trigger={['click']}>
//                           <a className="ant-dropdown-link">
//                             <Input size="default" placeholder={tradeUserValue} />
//                           </a>
//                         </Dropdown>

//                       </div>
//                     </div>

//                     <div className="card border-0 rounded-0 p-0">
//                       <div className="card-body border-0 rounded-0">
//                         <Dropdown overlay={this.voyOpsList()} trigger={['click']}>
//                           <a className="ant-dropdown-link">
//                             <Input size="default" placeholder={VoyOpsValue} />
//                           </a>
//                         </Dropdown>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </article>
//         <article className="article toolbaruiWrapper">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="row p12">

//                 <div className="col-12">
//                   <h4 className='Char-dashboard'>
//                     Filter Date

//                   </h4>
//                   <div className="card-group w-100 mt-4">

//                     <div className="card">
//                       <div className="card-body dashboard-col-details">
//                         <h4 className='Char_dashboard_sub'>Fixed Vessel</h4>

//                         <h2 className="font-weight-bold mb-0">{chattering.vessel_id}</h2>

//                         <h5>
//                           TC Contract{" "} &nbsp;
//                           <span className="text-danger">
//                             8.04 % <Icon type="caret-up" />
//                           </span>
//                         </h5>
//                       </div>

//                     </div>


//                     <div className="card">
//                       <div className="card-body dashboard-col-details">
//                         <h4 className='Char_dashboard_sub'>Total Fixture</h4>
//                         <h2 className="font-weight-bold mb-0">{chattering.voyage_id}</h2>
//                         <h5>
//                           Voyage fixed{' '}&nbsp;
//                           <span className="text-danger">
//                             8.04 % <Icon type="caret-down" />
//                           </span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body dashboard-col-details">
//                         <h4 className='Char_dashboard_sub'>Fixed</h4>
//                         <h2 className="font-weight-bold mb-0">{chattering.cargo_contract_id}</h2>
//                         <h5>
//                           Cargo contract{' '}&nbsp;
//                           <span className="text-success">
//                             84.04 % <Icon type="caret-up" />
//                           </span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body dashboard-col-details">
//                         <h4 className='Char_dashboard_sub'>TTL Cargo</h4>
//                         <h2 className="font-weight-bold mb-0">{chattering.cp_qty}</h2>
//                         <h5>
//                           Excuted{' '}&nbsp;
//                           <span className="text-danger">
//                             8.04 % <Icon type="caret-down" />
//                           </span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body dashboard-col-details">
//                         <h4 className='Char_dashboard_sub'>TTL Vessel</h4>
//                         <h2 className="font-weight-bold mb-0">{''}</h2>
//                         <h5>
//                           Schedule{' '}
//                           <span className="text-success">
//                             80.04 % <Icon type="caret-up" />
//                           </span>
//                         </h5>
//                       </div>
//                     </div>

//                   </div>
//                 </div>

//               </div>
//             </div>
//           </div>
//         </article> 

//         <article className="article toolbaruiWrapper">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="row">
//                 <div className="col-md-6">
//                   <div className="border">
//                     <h4 className="mb-0  p-4 Char-dashboard">Live vessel (Commenced/Delivered)</h4>
//                     <div>
//                       {this.state.data.length > 0 ? (
//                         <ChartringMap mapData={this.state.data} />
//                       ) : null} 
//                       {/* <Map

//                         google={this.props.google}
//                         zoom={mapSettings.zoom}
//                         initialCenter={mapSettings.center}
//                         mapId="c29c09d0a6b2a312"
//                       >
//                         {this.state.latlng.map((e, ind) => {
//                           return (
//                             <Marker
//                               title={this.state.title}
//                               icon={{
//                                 url: "../../assets/ship.png",
//                                 anchor: new this.props.google.maps.Point(32, 32),
//                                 scaledSize: new this.props.google.maps.Size(30, 30)
//                               }}
//                               onClick={() => this.showHideModal(true, 'ShipDetails1', e)}
//                               position={{ lat: e.lat, lng: e.lng }}
//                             />
//                           )
//                         })}

//                       </Map>  */}
//                      </div>
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="border p-3">
//                     <h4 className="mb-3 Char-dashboard">
//                       Profit/Loss vs Revenue (Voyage)

//                     </h4>
//                     {/* {chattering.vessel_name && chattering.vessel_name.length > 0 ? chattering.vessel_name.map((e, idx) => {
//                       return (
//                         <>
//                           <div className="row w-100 mb-2" key={idx}>
//                             <div className="col-md-5">
//                               <h4>{e.vessel_name} ({e.voyage_no})</h4>
//                             </div>
//                             <div className="col-md-7 text-right">
//                               <Progress percent={90} />
//                             </div>
//                           </div>
//                         </>

//                       )
//                     }) : undefined


//                     } */}
//                       <Paichart />



//                     {/* <Pagination isPagination /> */}
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </article>

//         <article className="article toolbaruiWrapper">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="row">
//                 <div className="col-md-6">
//                   <div className="row">
//                     <div className="col-md-12">
//                       <Paichart />
//                     </div>
//                   </div>
//                 </div>
//                 <div className="col-md-6">


//                   <BarChart
//                     chartData={chattering}

//                   />

//                 </div>
//               </div>
//             </div>
//           </div>
//         </article>

//         <article className="article toolbaruiWrapper">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="row">

//                 <div className="col-md-6">
//                   <Dropdown overlay={this.vesselSelectList()} trigger={['click']}>
//                     <a className="ant-dropdown-link">
//                       <Input size="default" placeholder={vesselSelectValue} />
//                     </a>
//                   </Dropdown>

//                 </div>
//               </div>
//               <div className="row">

//                 <div className="col-md-6">
//                   <div className="card-group mt-3">
//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Hire $</h4>
//                         {chattering.payable ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.payable.hire} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Bunker $</h4>
//                         {chattering.payable ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.payable.bunker} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Portcost $</h4>
//                         {chattering.payable ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.payable.port} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Freight $</h4>
//                         {chattering.payable ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.payable.freight}</h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//                 <div className="col-md-6">
//                   <div className="card-group mt-3">
//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Hire $</h4>
//                         {chattering.receivalble ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.receivalble.hire} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Bunker $</h4>
//                         {chattering.receivalble ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.receivalble.bunker} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Portcost $</h4>
//                         {chattering.receivalble ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.receivalble.port} </h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>

//                     <div className="card">
//                       <div className="card-body">
//                         <h4 className="mb-0">Freight $</h4>
//                         {chattering.receivalble ? (
//                           <h2 className="font-weight-bold mb-0">{chattering.receivalble.freight}</h2>
//                         ) : undefined
//                         }
//                         <h5 className="mb-0">
//                           Change <span className="text-success">+5%</span> /{' '}
//                           <span className="text-danger">-5%</span>
//                         </h5>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//               </div>

//               <div className="row">
//                 <div className="col-md-6">
//                   <div className="card-group">
//                     <div className="card">
//                       <div className="card-body bg-danger p-2 text-center">
//                         <h4 className="text-white m-0">Payable</h4>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//                 <div className="col-md-6">
//                   <div className="card-group">
//                     <div className="card">
//                       <div className="card-body bg-success p-2 text-center">
//                         <h4 className="text-white m-0">Receivable</h4>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </article>
//         {this.state.modals['ShipDetails1'] ?
//           <Modal
//             className="ship-detail-modal"
//             style={{ top: '2%' }}
//             title={this.state.vessel_name}
//             visible={this.state.modals['ShipDetails1']}
//             onCancel={() => this.showHideModal(false, 'ShipDetails1')}
//             width="40%"
//             footer={null}
//             header={null}
//           >
//             <ShipDetail data={shipData} changeTitle={(data) => this.setState({ vessel_name: data })} />
//           </Modal>
//           :
//           undefined
//         }
//       </div>
//     );
//   }
// }
// export default GoogleApiWrapper({
//   apiKey: "AIzaSyCIZJxl4b3B520rAjPUqIu_YD5FHfiFQ6M"
// })(ChartringDashboard);





