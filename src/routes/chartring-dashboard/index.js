import React, { Component } from "react";
import DashCard from "./DashCard";

import moment from "moment";
import { Col, Row, Divider, DatePicker, Spin, Alert } from "antd";
import BarChart from "./BarChart";
import Chart from "./Paichart";
import FinLineChart, {
  FinPieChart,
  FinRevExpChart,
  FinPieChart1,
  FinPieChart2,
} from "./Finchart";
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
} from "../../shared";
import ChatringDashList from "./Chatring-Dashboard-List";
const { MonthPicker, RangePicker } = DatePicker;
const dateFormat = "YYYY/MM/DD HH:MM:SS";
class CharteringDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      date_from: moment().subtract(1, "months").format("YYYY-MM-DD"),
      date_to: moment(new Date()).format("YYYY-MM-DD"),
      charterData: {},
    };
    // this.datefromref=React.createRef();
    // this.datetoref=React.createRef();
  }

  onDateChange = (date, dateString) => {
    this.setState(
      {
        ...this.state,
        loading: true,
        date_from: dateString[0],
        date_to: dateString[1],
      },
      () => this.getcharteringdata()
    );
  };

  componentDidMount = () => {
    this.getcharteringdata();
    this.setState({ loading: false });
  };

  getcharteringdata = async () => {
    const { date_from, date_to } = this.state;
    let suURL = `${URL_WITH_VERSION}/chattering-dashboard/chattering/dashboard`;
    let suMethod = "POST";

    let data = {
      date_from: date_from,
      date_to: date_to,
    };
    this.setState({ ...this.state, loading: true });
    await postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        // openNotificationWithIcon("success", data.message);

        this.setState({
          ...this.state,
          loading: false,
          charterData: { ...data.row },
        });
      } else {
        openNotificationWithIcon("error", data.message);
      }
    });
  };

  render() {
    const { charterData, loading, date_from, date_to } = this.state;

    // const lastmonthfirstdate=date_from;
    // const curr_date=date_to;

    const {
      total_cargo,
      total_voyage,
      total_fixed_vessal,
      total_coa_cargo,
      avg_port_days,
      total_income,
      total_expenses,
      amount_recievable,
      amount_payable,
      total_income_freight,
      net_profit,
      total_inv_paid,
      total_inv_due,
      Bunker_invoice_due,
      total_expenses_bunker,
      total_expenses_by_hire,
      total_port_expenses,
      pda_fda_invoice_amount,
      total_expense_frieght,
      freight_total_due,
    } = charterData;

    const pieData1 = [
      { value: total_expenses_bunker, name: "Total Expenses By Bunker" },
      { value: total_expenses_by_hire, name: "Total Expenses By Hire" },
      { value: total_port_expenses, name: "Total Expenses By Port" },
      { value: total_expenses, name: "Total Expenses" },
    ];

    const innerdata = [
      { value: Bunker_invoice_due, name: "Bunker Invoice Due" },
      { value: avg_port_days, name: "Average Port Stays" },
      { value: amount_payable, name: "Amount Payable" },
    ];

    const outerdata = [
      { value: total_cargo, name: "Total Cargo By Value" },
      { value: total_coa_cargo, name: "Total Coa By Value" },

      { value: total_income_freight, name: "Total Income By Freight" },
      { value: freight_total_due, name: "Freight Invoice Due" },
      { value: total_inv_paid, name: "Total Invoice Paid " },

      { value: pda_fda_invoice_amount, name: "PDA/FDA Invoice Due" },
    ];

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Row>
                <Col span={2}>
                  <img
                    src="./assets/chartering.png"
                    style={{ width: "80px" }}
                  />
                </Col>
                <Col
                  span={16}
                  // xs={24}
                  // sm={24}
                  // md={12}
                  // lg={6}
                  // xl={6}
                  // className="mb-24"
                >
                  <h3
                    style={{
                      color: "#005baa",
                      fontWeight: "700",
                      fontSize: "30px",
                      margin: "20px 30px",
                      lineHeight: 1,
                    }}
                  >
                    CHARTERING DASHBOARD
                  </h3>
                </Col>

                <Col
                  span={6}
                  // xs={24}
                  // sm={24}
                  // md={12}
                  // lg={6}
                  // xl={6}
                  // className="mb-24"
                >
                  <RangePicker
                    onChange={this.onDateChange}
                    format="YYYY-MM-DD"
                    defaultValue={[moment(date_from), moment(date_to)]}
                  />
                </Col>
              </Row>
              {loading == false ? (
                <Row>
                  {innerdata.some((item) => item.value != undefined) ||
                  outerdata.some((item) => item.value != undefined) ? (
                    <Col span={12}>
                      <FinPieChart
                        innerdata={innerdata}
                        outerdata={outerdata}
                      />
                    </Col>
                  ) : (
                    <Spin tip="Loading...">
                      <Alert
                        message="Please wait"
                        description="Details are Loading."
                        type="info"
                      />
                    </Spin>
                  )}

                  <Col span={12}>
                    <Row>
                      <Col span={12}>
                        {!isNaN(total_voyage) && (
                          <DashCard
                            title="Total Voyage"
                            current_price={total_voyage}
                            style="redtext"
                          />
                        )}
                      </Col>

                      <Col span={12}>
                        {!isNaN(total_fixed_vessal) && (
                          <DashCard
                            title="Total Vessel Fixed By Month"
                            current_price={total_fixed_vessal}
                            style="bnb"
                          />
                        )}
                      </Col>
                    </Row>

                    <Row>
                      <Col span={12}>
                        {!isNaN(total_income) && (
                          <DashCard
                            title="Total Income"
                            current_price={total_income}
                            style="bnb"
                          />
                        )}
                      </Col>

                      <Col span={12}>
                        {!isNaN(total_expense_frieght) && (
                          <DashCard
                            title="Total expenses By Freight"
                            current_price={total_expense_frieght}
                            style="redtext"
                          />
                        )}
                      </Col>
                    </Row>

                    <Row>
                      <Col span={12}>
                        {!isNaN(amount_recievable) && (
                          <DashCard
                            title="Amount Receivable"
                            current_price={amount_recievable}
                            style="bnb"
                          />
                        )}
                      </Col>
                      <Col
                        span={12}
                        // xs={24}
                        // sm={24}
                        // md={12}
                        // lg={6}
                        // xl={6}
                        // className="mb-24"
                      >
                        {!isNaN(amount_payable) && (
                          <DashCard
                            title="Amount Payable"
                            current_price={amount_payable}
                            style="bnb"
                          />
                        )}
                      </Col>
                    </Row>

                    <Row>
                      <Col span={12}>
                        {!isNaN(net_profit) && (
                          <DashCard
                            title="Net Profit"
                            current_price={net_profit}
                            style="bnb"
                          />
                        )}
                      </Col>

                      <Col span={12}>
                        {!isNaN(total_inv_due) && (
                          <DashCard
                            title="Total Invoice Due"
                            current_price={total_inv_due}
                            style="bnb"
                          />
                        )}
                      </Col>
                    </Row>
                    <Row className="rowgap-vbox">
                      {pieData1.every((item) => item.value !== undefined) ? (
                        <Col span={24}>
                          <FinPieChart1 graphdata={pieData1} />
                        </Col>
                      ) : undefined}
                    </Row>
                  </Col>
                </Row>
              ) : (
                <Spin tip="Loading...">
                  <Alert
                    message="Please wait"
                    description="Details are Loading."
                    type="info"
                  />
                </Spin>
              )}
            </div>

            {/* <ChatringDashList /> */}
          </div>
        </article>
      </div>
    );
  }
}

export default CharteringDashboard;

