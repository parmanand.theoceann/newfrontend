import React, { useEffect } from 'react';
import { useState } from 'react';
import { Row, Col, Input, Button} from 'antd';
import {UploadOutlined} from '@ant-design/icons';
import Documents from '../documents';
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon } from '../../../shared';


const SharedDocuments = (props) => {

  const [accessAllowed, setAccessAllowed] = useState(false);
  const [passcode, setPasscode] = useState('');
  const [id, setId] = useState(null);
  const [vesselId, setVesselId] = useState();

  useEffect(() => {
    const id = props.match.params.id;
    setId(id);
  }, []);

  const handlePasscodeChange = e => {
    let value = e.target.value;
    if (value.length === 0) {
      return setPasscode('')
    }
    if (!Number(value) || value.length > 6) {
      return;
    }
    setPasscode(value)
  };

  async function validateUser(passcode) {
    const response = await getAPICall(`${URL_WITH_VERSION}/vessel/vesselattachment/validate?id=${id}&passcode=${passcode}`);
    const success = response['data']['success'];
    if (success) {
      setVesselId(response['data']['vessel_id']);
      setAccessAllowed(true);
    } else {
      openNotificationWithIcon('error', 'Please enter valid passcode.', 5);
    }
  }

  const checkPasscode = () => {
    if (passcode.length === 6) {
      validateUser(passcode)
    }
    else
      setAccessAllowed(false);
  }

  const formData = {
    'id': 0, 'portconsp.tableperday': [
      { 'editable': true, 'index': 0, 'con_type': 3, 'con_g': 2, 'id': (-9e6 + 0), 'con_unit': 2 },
      { 'editable': true, 'index': 1, 'con_type': 10, 'con_g': 4, 'id': (-9e6 + 1), 'con_unit': 2 },
      { 'editable': true, 'index': 2, 'con_type': 5, 'con_g': 3, 'id': (-9e6 + 2), 'con_unit': 2 },
      { 'editable': true, 'index': 3, 'con_type': 7, 'con_g': 4, 'id': (-9e6 + 3), 'con_unit': 2 },
      { 'editable': true, 'index': 4, 'con_type': 4, 'con_g': 3, 'id': (-9e6 + 4), 'con_unit': 2 }
    ],
      'id': 1, 'seaspdconsp.tableperday': [
        { 'editable': true, 'index': 1, 'speed_type': 1, 'ballast_laden': 1, 'id': (-9e6 + 10), 'engine_load': 85 },
        { 'editable': true, 'index': 2, 'speed_type': 1, 'ballast_laden': 2, 'id': (-9e6 + 11), 'engine_load': 85 }
      ],
  }

  return (
    <div key="1">
      <section className="form-card-page form-card row no-gutters">
        {
          accessAllowed && <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center">
            <Documents frmData={formData} vesselId={vesselId} />
          </div>
        }
        {
          
          !accessAllowed && <Row type="flex" align="middle" justify='center' gutters={16}>
            <Col>
              <Row>
                <Col><div>{'Enter Passcode'}</div></Col>
              </Row>
              <Row gutters={16}>
                <Col span={12}>
                  <Input
                    className="form-control"
                    name="passcode"
                    value={passcode}
                    onChange={handlePasscodeChange}
                  />
                </Col>
                <Col span={12}>
                  <Button onClick={checkPasscode}>
                <UploadOutlined /> Submit
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        }
      </section>
    </div>
  )
}

export default SharedDocuments;
