import React, { Component } from 'react';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, { postAPICall, getAPICall, openNotificationWithIcon } from '../../shared';
import { Button } from 'antd';

import { Modal } from 'antd';
import AttachmentFile from '../../shared/components/Attachment';


class Documents extends Component {
  constructor(props) {
    super(props)
    this.state = {
      frmName: "vessel_attachment_form",
      formData: {},
      loadForm: false,
      isUpdate: false,
      visibleVesselAttachment: false,
      imageFile: [],
      indexNumber: 0,
      shareViewVisible: false,
      vesselId: null,
      vessel: {
        vessel_name: '',
        vessel_type: '',
        imo_no: '',
        vessel_dwt: ''
      },
      uploadVisible: false,
    }
  }

  componentDidMount = () => {
    const vesselId = this.props.vesselId;
    this.setState({ ...this.state, vesselId });
    this.prepaireFormData(vesselId);
    this.getVesselInformation(vesselId);
  }

  getVesselInformation = async (id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${id}`);
      const respData = await response['data'];
      this.setState({ ...this.state, vessel: respData });
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  }

  prepaireFormData = async (id) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/vesselattachment/edit?e=${id}`);
      const respData = await response['data'];
      if (respData) {
        this.setState({ formData: respData, loadForm: true, isUpdate: true })
      } else {
        this.setState({ loadForm: true, isUpdate: false })
      }
    }
  }

  saveFormData = (data) => {
    const { frmName, vesselId, imageFile } = this.state;
    let vessel_form_id = vesselId;
    if (imageFile && imageFile.length > 0) {
      imageFile.map(val => {
        data['..'].map((e, index) => {
          if (val.indexNumber === index) {
            e['attachment'] = val.fileName
          }
          return true;
        })
      })
    }
    if (data['..'] && data['..'].length > 0) {
      data['..'].map(e => {
        if (!e['vessel_id']) { e['vessel_id'] = vessel_form_id }
        delete e.editable
        delete e.ocd
        delete e.id
        delete e.index
        delete e.key
      })
    }
    let suURL = `${URL_WITH_VERSION}/vessel/vesselattachment/save?frm=${frmName}`;
    let suMethod = 'POST';
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon('success', data.message);
        if (vessel_form_id) {
          this.setState({ loadForm: false })
          this.prepaireFormData(vessel_form_id);
        }
      } else {
        openNotificationWithIcon('error', data.message)
      }
    });
  }

  updateFormData = (data) => {
    const { frmName, vesselId, imageFile } = this.state;
    let vessel_form_id = vesselId;
    if (imageFile && imageFile.length > 0) {
      imageFile.map(val => {
        data['..'].map((e, index) => {
          if (val.indexNumber === index) {
            e['attachment'] = val.fileName
            e['share_link'] = val.url
          }
          return true;
        })
      })
    }
    if (data['..'] && data['..'].length > 0) {
      data['..'].map(e => {
        if (!e['vessel_id']) { e['vessel_id'] = vessel_form_id }
        //if (e.id < 0) { delete e.id }
        delete e.editable
        delete e.ocd
        delete e.index
        delete e.key
      })
    }

    let suURL = `${URL_WITH_VERSION}/vessel/vesselattachment/update?frm=${frmName}`;
    let suMethod = 'PUT';
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon('success', data.message);
        if (vessel_form_id) {
          this.setState({ loadForm: false })
          this.prepaireFormData(vessel_form_id);
        }
      } else {
        openNotificationWithIcon('error', data.message)
      }
    });
  }

  deleteTableData = async (id) => {
    const { vesselId } = this.state;
    let delete_data = {
      'id': id
    }
    postAPICall(`${URL_WITH_VERSION}/vessel/vesselattachment/delete`, delete_data, 'delete', response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.prepaireFormData(vesselId);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  }
  onClickExtraIcon = async (action, data) => {
    if (Math.sign(data.id) > 0) {
      this.deleteTableData(data.id)
    }
  }

  onUploadVesselDoc = (action, data) => {
    this.setState({ indexNumber: action.index, visibleVesselAttachment: true })
  }

  openShareView = async (data) => {
    this.setState({ ...this.state, shareViewVisible: true });
  }

  isVesselDocCancel = () => this.setState({ uploadVisible: false });

  isVesselDoc = () => this.setState({ uploadVisible: false });

  closeShareView = () => this.setState({ shareViewVisible: false });

  uploadedFiles = (data, index) => {
    const images = data.map((item, index) => {
      return {
        indexNumber: index,
        fileName: item.fileName,
        url: item.url,
        contentType: item.contentType
      }
    })
    this.setState({ ...this.state, imageFile: images })
  }

  deleteAttachment = () => {
    this.setState({ ...this.state, imageFile: [] })
  }

  render() {
    const { frmName, formData, isUpdate, loadForm, visibleVesselAttachment, indexNumber } = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span>Vessel Name</span> <span>{this.state.vessel.vessel_name}</span></h4>
                </div>
                <div className="form-heading">
                  <h4 className="title"><span>IMO Number</span> <span>{this.state.vessel.imo_no}</span></h4>
                </div>
                <div className="form-heading">
                  <h4 className="title"><span>DWT</span> <span>{this.state.vessel.vessel_dwt}</span></h4>
                </div>
                <div className="form-heading">
                  <h4 className="title"><span>Vessel Type</span> <span>{this.state.vessel.vessel_type}</span></h4>
                </div>
                <div className="action-btn">
                  <Button type="primary">More Information</Button>
                </div>
              </div>
            </div>
          </div>
        </article>
        {loadForm &&
          <NormalFormIndex
            key={'key_' + frmName + '_0'}
            formClass="label-min-height"
            formData={formData}
            frmCode={'vessel_attachment_form'}
            showForm={true}
            showToolbar={[{
              isLeftBtn: [
                {
                  key: 's1',
                  isSets: [
                    { id: '1', key: 'save', type: isUpdate === true ? 'save' : 'save', withText: '', "event": (event, data) => { isUpdate === true ? this.updateFormData(data) : this.saveFormData(data) } },
                    // { id: '2', key: 'share', type: 'share-alt', withText: '', "event": (event, data) => { this.openShareView(data) } }
                  ]
                }
              ],
              isRightBtn: [{ key: 's1', isSets: [] }],
              isResetOption: false,
            },
            ]}
            extraTableButton={{
              "..": [{ "icon": "upload", "onClickAction": (action, data) => { this.setState({ ...this.state, uploadVisible: true}) } }],
            }}
            tableRowDeleteAction={(action, data) => this.onClickExtraIcon(action, data)}
            inlineLayout={true}
          />
        }
        <Modal
          open={this.state.uploadVisible}
          title="Upload Attachment"
          onOk={this.isVesselDoc}
          onCancel={this.isVesselDocCancel}
          footer={null}
          width={1000}
          maskClosable={false}
        >
          <AttachmentFile
            uploadType="Address Book"
            directory={formData['..']}
            attachments={formData['..']}
            deleteAttachment={file => this.deleteAttachment(file.url, file.name)} 
            onCloseUploadFileArray={fileArr => this.uploadedFiles(fileArr)}
            tableId={indexNumber}
          />
        </Modal>
      </div>
    );
  }
}

export default Documents;
