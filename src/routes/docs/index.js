import React from 'react';
import { Route } from 'react-router-dom';

import SharedDocuments from './routes/SharedDocuments'
import './styles.scss';


const Page = ({ match }) => (
  <div>
    <Route path={`${match.url}/shared/:id`} component={SharedDocuments}/>
  </div>
)

export default Page;
