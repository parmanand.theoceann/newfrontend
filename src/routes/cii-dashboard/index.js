import { Layout, Table, Typography, Input, Row, Select, Button, Icon, Col, Divider, Modal, message } from 'antd';
import React, { Component } from 'react'
import URL_WITH_VERSION, {
    objectToQueryStringFunc,
    getAPICall
} from '../../shared';

const { Content } = Layout;
const { Title } = Typography;
const { Option } = Select;

class FleetPerformance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    key: '1',
                    data: 'Dummy Data',
                },
                {
                    key: '2',
                    data: 'Dummy Data',
                },
                {
                    key: '3',
                    data: 'Dummy Data',
                },
            ],
            mapData: null,
            loading: false,
            filterVessels: [],
            columns: [
                {
                    title: 'Vessel Name',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'IMO',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Deadweight (mt) ',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'No of Report',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Distance sailed',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Distance deducted',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Co2 Emitted',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Co2 Deducted',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'CII calculation',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'CII Attained',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'CII Required',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Deviation %',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Attained Rating YTD(AER) 2023',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'Proj 2024 CII',
                    dataIndex: 'data',
                    key: 'sn',
                },
                {
                    title: 'More details',
                    dataIndex: 'more_details',
                    key: 'sn',
                }
            ],

        }
    }
    handleChange = (value) => {
        console.log(`selected ${value}`);
    }
    render() {
        return (
            <div className="tcov-wrapper full-wraps">
                <Layout className="layout-wrapper">
                    <Layout>
                        <Content className="content-wrapper">
                            <div className="fieldscroll-wrap">
                                <div className="body-wrapper">
                                    <article className="article">
                                        <div className="box box-default">
                                            <div className="box-body common-fields-wrapper">
                                                <table style={{ width: '100%' }}>
                                                    <tr>
                                                        <td>
                                                            <Title level={2}>CII Dashboard</Title>
                                                        </td>
                                                        <td valign='right' style={{ textAlign: 'right' }}>
                                                            <Input placeholder="Search Vessel Name" style={{ marginTop: 10, marginBottom: 10 }} />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <Divider></Divider>

                                                <Table
                                                    bordered
                                                    columns={this.state.columns}
                                                    dataSource={this.state.data}
                                                    scroll={{
                                                        x: 1800,
                                                        y: 400
                                                    }}
                                                    className="inlineTable resizeableTable"
                                                    size="small"
                                                    rowClassName={(r, i) =>
                                                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                                                    }
                                                    style={{ marginTop: 30 }}
                                                />
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </Content>
                    </Layout>
                </Layout>
            </div>
        )
    }
}
export default FleetPerformance;
