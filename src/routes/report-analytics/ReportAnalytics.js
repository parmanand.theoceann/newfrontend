import React, { Component } from 'react';
import {
  Form,
  Input,
  Table,
  Tabs,
  Drawer,
  Layout,
  Select,
  DatePicker,
  Row,
  Col,
  Switch,
  Modal,
} from 'antd';

import RightBarUI from '../../components/RightBarUI';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;
const { Content } = Layout;
const Option = Select.Option;
const { Column, ColumnGroup } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class ReportAnalytics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
    };
  }

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  onClickRightMenu = (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (
      key
      //   case 'portinformation':
      //     loadComponent = <TctoPortInformation />;
      //     break;
      //   case 'pl-summary':
      //     loadComponent = <PL />;
      //     break;
      //   case 'estimates-summary':
      //     loadComponent = <EstimateSummary />;
      //     break;

      //   case 'properties':
      //     loadComponent = <Properties />;
      //     break;
      //   case 'search':
      //     loadComponent = <RightSearch />;
      //     break;
    ) {
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };

  render() {
    const { loadComponent, title, visibleDrawer } = this.state;
    return (
      <>
        <div className="wrap-rightbar full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">
                    <article className="article">
                      <div className="box box-default">
                        <div className="box-body common-fields-wrapper">
                          <div className="row">
                            <div className="col-md-2 pr0 fieldscroll-wraps-scroll">
                              <div className="normal-heading">Report List</div>
                              <div className="fieldscroll-wraps-list">
                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>

                                <article className="article">
                                  <div className="box box-default">
                                    <div className="bunkerInvoiceWrapper pl-2">
                                      <span className="heading">
                                        <strong>D02482-o0001</strong>
                                      </span>
                                      <span className="sub-heading">N/A</span>
                                      <br />
                                      <span className="value">Uni Challence</span>
                                    </div>
                                  </div>
                                </article>
                              </div>
                            </div>
                            <div className="col-md-8">
                              <Form>
                                <div className="normal-heading">New Report</div>
                                <Row gutter={16}>
                                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                    <FormItem {...formItemLayout} label="Paper Trade">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Port">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Revision">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="User">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Voyage">
                                      <Input size="default" placeholder="" />
                                    </FormItem>
                                  </Col>
                                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                    <FormItem {...formItemLayout} label="Payment">
                                      <Input size="default" placeholder="" />
                                    </FormItem>
                                    <FormItem {...formItemLayout} label="Position">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Time Charter">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Vessel">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="Voyage Estimate">
                                      <Input size="default" placeholder="" />
                                    </FormItem>

                                    {/* <FormItem {...formItemLayout} label="Reposition Port ">
                                      <Select size="default" defaultValue="1">
                                        <Option value="1">Option</Option>
                                        <Option value="2">Option</Option>
                                        <Option value="3">Option</Option>
                                      </Select>
                                    </FormItem>

                                    <FormItem {...formItemLayout} label="CP Form">
                                      <InputGroup compact>
                                        <Select
                                          size="default"
                                          defaultValue="1"
                                          style={{ width: '50%' }}
                                        >
                                          <Option value="1">Option</Option>
                                          <Option value="2">Option</Option>
                                          <Option value="3">Option</Option>
                                        </Select>
                                        <Input type="date" style={{ width: '50%' }} />
                                      </InputGroup>
                                    </FormItem> */}
                                  </Col>
                                </Row>
                              </Form>
                            </div>

                            <div className="col-md-2 fieldscroll-wraps-scroll">
                              <div className="normal-heading">Report Designer</div>

                              <div className="report-designner-list">
                                  <ul>
                                      <li>Paper Trade</li>
                                      <li>Payment</li>
                                      <li>Port</li>
                                      <li>Position</li>
                                      <li>Revision</li>
                                      <li>Time Charter</li>
                                      <li>User</li>
                                      <li>Vessel</li>
                                      <li>Voyage</li>
                                      <li>Voyage Estimate</li>
                                  </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </Content>
            </Layout>
            <RightBarUI
              pageTitle="report-analytics-righttoolbar"
              callback={(data, options) => this.onClickRightMenu(data, options)}
            />
            {loadComponent !== undefined && title !== undefined && visibleDrawer === true ? (
              <Drawer
                title={this.state.title}
                placement="right"
                closable={true}
                onClose={this.onCloseDrawer}
               open={this.state.visibleDrawer}
                getContainer={false}
                style={{ position: 'absolute' }}
                width={this.state.width}
                maskClosable={false}
                className="drawer-wrapper-container"
              >
                <div className="tcov-wrapper">
                  <div className="layout-wrapper scrollHeight">
                    <div className="content-wrapper noHeight">{this.state.loadComponent}</div>
                  </div>
                </div>
              </Drawer>
            ) : (
              undefined
            )}
          </Layout>
        </div>
      </>
    );
  }
}

export default ReportAnalytics;
