import React, { Component, useEffect } from 'react';
import { Button, Table,  Popconfirm, Modal, Layout } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import CoaVci from '../chartering/routes/coa-vci';
import { DeleteOutlined ,EditOutlined} from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
const { Content } = Layout;

const  ListCoaVci = (props) =>  {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={e => redirectToAdd(e, record.contract_id)}>
            <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined/>
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['list-coa-vci'] ? FIELDS['list-coa-vci']['tableheads'] : []
    );
    tableHeaders.push(tableAction);
    const [state, setState] = useStateCallback({
      loading: false,
      columns: tableHeaders,
      // responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
    })

    const navigate  = useNavigate();


  useEffect(() => {
    getTableData()
  }, [])

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
    
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
      }
    
      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }
    
      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }
    

    setState(prev => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/coavci/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state['responseData'] = dataArr;
    }
    setState(prev => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const redirectToAdd = async (e, id = null) => {
    if (id) {
      navigate(`/edit-coa-vci/${id}`);
    } else {
      setState(prev => ({ ...prev, isAdd: true, isVisible: true }));
    }
  };

  const onCancel = () => {
    getTableData();
    setState(prev => ({ ...prev, isAdd: true, isVisible: false }));
  };

  const onRowDeletedClick = id => {
    let _url = `${URL_WITH_VERSION}/coavci/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  const callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      setState(prev => ({ ...prev, search: search, pageOptions: pageOptions }), () => {
        getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = state.pageOptions;
      pageOptions['pageIndex'] = 1;
      setState(prev => ({ ...prev, search: {}, pageOptions: pageOptions }), () => {
        getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let columns = Object.assign([], state.columns);
      setState(prev => ({
        ...prev,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      setState(prev => ({ ...prev, pageOptions: pageOptions }), () => {
        getTableData();
      });
    }
  };

  //resizing function
  const handleResize = index => (e, { size }) => {
    setState((prev) => {
      const nextColumns = [...prev.columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { ...prev, columns: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onClickRightMenu = key => {};

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    console.log('qqqqqqq',qParams)
    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }


  

  
    const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isAdd,
      isVisible,
      formDataValues,
      sidebarVisible,
    } = state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));
    return (
      <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body">
                      <div className="form-wrapper">
                        <div className="form-heading">
                          <h4 className="title">
                            <span>COA(VC)-List</span>
                          </h4>
                        </div>
                        <div className="action-btn">
                          <Button type="primary" onClick={e => redirectToAdd(e)}>
                            Add COA (VC)
                          </Button>
                        </div>
                      </div>
                      <div
                        className="section"
                        style={{
                          width: '100%',
                          marginBottom: '10px',
                          paddingLeft: '15px',
                          paddingRight: '15px',
                        }}
                      >
                        {loading === false ? (
                          <ToolbarUI
                            routeUrl={'list-coa-vci-toolbar'}
                            optionValue={{
                              pageOptions: pageOptions,
                              columns: columns,
                              search: search,
                            }}
                            callback={e => callOptions(e)}
                            dowloadOptions={[
                              {title:'CSV', event: () => onActionDonwload('csv', 'coavci')},
                              {title:'PDF', event: () => onActionDonwload('pdf', 'coavci')},
                              {title:'XLS', event: () => onActionDonwload('xlsx', 'coavci')}
                            ]}
                          />
                        ) : (
                          undefined
                        )}
                      </div>
                      <div>
                        <Table
                          // rowKey={record => record.id}
                          className="inlineTable editableFixedHeader resizeableTable"
                          bordered
                          components={components}
                          scroll={{ x: 'max-content' }}
                          columns={tableColumns}
                          dataSource={responseData}
                          loading={loading}
                          pagination={false}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? 'table-striped-listing'
                              : 'dull-color table-striped-listing'
                          }
                        />
                      </div>
                    </div>
                  </div>
                </article>

                {
                  isVisible === true ?
                    <Modal
                      title={(isAdd === false ? "Edit" : "Add") + " COA(VCI) Form"}
                     open={isVisible}
                      width='90%'
                      onCancel={onCancel}
                      style={{ top: '10px' }}
                      bodyStyle={{ height: 600, overflowY: 'auto', padding: '0.5rem' }}
                      footer={null}
                    >
                      <div className="body-wrapper">
                        <article className="article">
                          <div className="box box-default" style={{ padding: '15px' }}>
                            {
                              isAdd ?
                                <CoaVci formData={{}} modalCloseEvent={onCancel} showSideListBar={false} /> :
                                <CoaVci formData={formDataValues} modalCloseEvent={onCancel} showSideListBar={false} />
                            }

                          </div>
                        </article>
                      </div>
                    </Modal> : undefined
                }
                {/* column filtering show/hide */}
                {sidebarVisible ? (
                  <SidebarColumnFilter
                    columns={columns}
                    sidebarVisible={sidebarVisible}
                    callback={e => callOptions(e)}
                  />
                ) : null}
              </div>
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }


export default ListCoaVci;
