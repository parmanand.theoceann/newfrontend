import React, { useEffect } from 'react';
import { Button, Table,  Popconfirm, Modal } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon, ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import COAContract from '../coa-contract';
import { useNavigate } from 'react-router-dom';



const COASList  = () => {
  const [state, setState] = useStateCallback({
    // frmName:"cao_form",
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    reportformDataValue: {},
    typesearch:{},
    donloadArray: []
  });
  const navigate = useNavigate();

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => redirectToAdd(e, record.contract_id)}>
            <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        )
      }
    };
    let tableHeaders = Object.assign([], FIELDS && FIELDS['coas-list'] ? FIELDS['coas-list']["tableheads"] : [])
    tableHeaders.push(tableAction)

    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);


  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let _search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : state.typesearch;

    if (
      _search &&
      _search.hasOwnProperty("searchValue") &&
      _search.hasOwnProperty("searchOptions") &&
      _search["searchOptions"] !== "" &&
      _search["searchValue"] !== ""
    ) {
      let wc = {};
      _search["searchValue"] = _search["searchValue"].trim();

      if (_search["searchOptions"].indexOf(";") > 0) {
        let so = _search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: _search["searchValue"] }));
      } else {
        wc = { "OR": {} };
        wc['OR'][_search['searchOptions']] = { "l": _search['searchValue'] }
      }

      headers["where"] = wc;
      state.typesearch = {
        searchOptions: _search.searchOptions,
        searchValue: _search.searchValue,
      };
    }
    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = {},
      dataArr = [],
      totalRows = 0;
    let _url = `${URL_WITH_VERSION}/coa/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const respData = await response;
    let donloadArr = [];
    if (respData && respData.data) {
      totalRows = respData?.total_rows ? respData.total_rows : 0;
      dataArr = respData.data;
      if (dataArr.length>0&&totalRows > 0) {
        dataArr.forEach((d) => donloadArr.push(d["id"]));
        _state["responseData"] = dataArr;
      }
    }

    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };



  const redirectToAdd = async (e, id = null) => {
    if (id) {
      navigate(`/edit-coa-contract/${id}`);
    } else {
      setState({ ...state, isAdd: true, isVisible: true });
    }
    
  }

  const onCancel = () => {
   // getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  }

  const modalClose = () => {
     getTableData();
     setState({ ...state, isAdd: true, isVisible: false });
   }


   /*
  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/coa/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })
  }

  */

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  //resizing function
  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };
  
  const tableColumns = state.columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span>COA(Cargo) List</span></h4>
                </div>
                <div className="action-btn">
                  <Button type="primary" onClick={(e) => redirectToAdd(e)}>Add COA</Button>
                </div>
              </div>
              <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                {
                  state.loading === false ?
                    <ToolbarUI routeUrl={'coa-list-toolbar'} optionValue={{ 'pageOptions': state.pageOptions, 'columns': state.columns, 'search': state.search }} callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {title:'CSV', event: () => onActionDonwload('csv', 'coa')},
                      {title:'PDF', event: () => onActionDonwload('pdf', 'coa')},
                      {title:'XLS', event: () => onActionDonwload('xlsx', 'coa')}
                    ]}
                    /> 
                    : undefined
                }
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  components={components}
                  scroll={{ x: 'max-content' }}
                  columns={tableColumns}
                  // size="small"
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                />
              </div>
            </div>
          </div>
        </article>

        {
          state.isVisible === true ?
            <Modal
              title={(state.isAdd === false ? "Edit" : "Add") + " COA Form"}
              open={state.isVisible}
              width='95%'
              onCancel={onCancel}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
            >
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default" style={{ padding: '15px' }}>
                    {
                      state.isAdd === false ?
                        <COAContract formData={state.formDataValues} reportFormData={state.reportformDataValue} modalCloseEvent={modalClose} showSideListBar={false} />
                        :
                        <COAContract modalCloseEvent={modalClose} showSideListBar={false} />
                    }
                  </div>
                </article>
              </div>
            </Modal>
            : undefined
        }
        {/* column filtering show/hide */}
        {
          state.sidebarVisible ? <SidebarColumnFilter columns={state.columns} sidebarVisible={state.sidebarVisible} callback={(e) => callOptions(e)} /> : null
        }
      </div>
    )
  }


export default COASList;