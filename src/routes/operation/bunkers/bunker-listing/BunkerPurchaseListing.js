import React, { Component } from "react";
import { Table, Popconfirm, Modal, Col, Row, Select } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from "../../../../shared";
import { FIELDS } from "../../../../shared/tableFields";
import ToolbarUI from "../../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../../shared/SidebarColumnFilter";
import BunkerPurchasedOrder from "../bunker-purchased-order/components/BunkerPurchasedOrder";
import PieChart from "../../../dashboard/charts/PieChart";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import LineChart from "../../../dashboard/charts/LineChart";
import BumpChartRanking from "../../../dashboard/charts/BumpChartRanking";
class BunkerPurchaseListing extends Component {
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  constructor(props) {
    super(props);
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            {" "}
            <span
              className="iconWrapper"
              onClick={(e) => this.redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => this.onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    this.ClusterDataxAxis = [
      "MSC LORETO",
      "PACIFIC EXPLORER",
      "OCEANIC MAJESTY",
      "CARIBBEN PRINCESS",
      "BALTIC VOYAGER",
    ];
    this.ClusterDataSeries = [
      {
        name: "Total Revenue",
        type: "bar",
        barGap: 0,

        data: [320, 332, 301, 334, 390],
      },
    ];
    this.totalDashboarddat = [
      {
        title: "Total vessels",
        value: "abcd",
      },
      {
        title: "Total amount",
        value: "abcd",
      },
      {
        title: "Total invoice",
        value: "abcd",
      },
    ];

    this.LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
    this.LineCharxAxisData = [
      "Jan",
      "Fab",
      "Mar",
      "April",
      "May",
      "Jun",
      "Jul",
    ];
    this.PieChartData = [
      { value: 70, name: "Receivable 70k" },
      { value: 30, name: "Payable 30k" },
    ];
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["bunker-purchase-list"]
        ? FIELDS["bunker-purchase-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    this.state = {
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      showBunkerPurchaseModal: false,
      typesearch: {},
      donloadArray: [],
    };
  }
  componentDidMount = () => {
    this.getTableData();
  };


  handleCancel = () => {
    this.setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  getTableData = async (searchtype = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : this.state.typesearch;

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      this.state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    let donloadArr = [];
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  redirectToAdd = async (e, record = null) => {
    const { formDataValues } = this.state;

    try {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/bunker-pur/edit?e=${record.id}`
      );
      const respdata = await response["data"];

      this.setState({
        ...this.state,
        formDataValues: { ...respdata },
        showBunkerPurchaseModal: true,
      });
    } catch (err) {
      openNotificationWithIcon("error", err.message, 3);
    }
  };

  onCancel = () => {
    // this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  modalCloseEvent = () => {
    this.setState({ ...this.state, showBunkerPurchaseModal: false }, () =>
      this.getTableData()
    );
  };

  // onRowDeletedClick = (id) => {
  //   let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/delete`;
  //   apiDeleteCall(_url, { id: id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon("success", response.message);
  //       this.getTableData(1);
  //     } else {
  //       openNotificationWithIcon("error", response.message);
  //     }
  //   });
  // };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions, typesearch: {} },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  handleResize =
    (index) =>
    (e, { size }) => {
      this.setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }

    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  render() {
    const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isVisible,
      sidebarVisible,
      showBunkerPurchaseModal,
      formDataValues,
    } = this.state;
    const tableCol = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));

    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-wrapper">
                  <div className="form-heading">
                    <h4 className="title">
                      <span>Bunker Purchase List</span>
                    </h4>
                  </div>
                  {/* <div className="action-btn">
                    <Link to="add-tcov">Add Bunker Requirement</Link>
                  </div> */}
                </div>
                <div
                  className="section"
                  style={{
                    width: "100%",
                    marginBottom: "10px",
                    paddingLeft: "15px",
                    paddingRight: "15px",
                  }}
                >
                  {loading === false ? (
                    <ToolbarUI
                      routeUrl={"bunker-purchase-toolbar"}
                      optionValue={{
                        pageOptions: pageOptions,
                        columns: columns,
                        search: search,
                      }}
                      showGraph={this.showGraphs}
                      callback={(e) => this.callOptions(e)}
                      dowloadOptions={[
                        {
                          title: "CSV",
                          event: () => this.onActionDonwload("csv", "bunpur"),
                        },
                        {
                          title: "PDF",
                          event: () => this.onActionDonwload("pdf", "bunpur"),
                        },
                        {
                          title: "XLS",
                          event: () => this.onActionDonwload("xlsx", "bunpur"),
                        },
                      ]}
                    />
                  ) : undefined}
                </div>
                <div>
                  <Table
                    // rowKey={record => record.id}
                    className="inlineTable resizeableTable"
                    bordered
                    columns={tableCol}
                    components={this.components}
                    size="small"
                    scroll={{ x: "max-content" }}
                    dataSource={responseData}
                    loading={loading}
                    pagination={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0
                        ? "table-striped-listing"
                        : "dull-color table-striped-listing"
                    }
                  />
                </div>
              </div>
            </div>
          </article>

          {isVisible === true ? (
            <>Add Bunker Purchase</>
          ) : /* <Modal
                title={(isAdd===false?"Edit":"Add")+" TCI Form"}
               open={isVisible}
                width='95%'
                onCancel={this.onCancel}
                style={{top: '10px'}}
                bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
                footer={null}
              >
                {
                  isAdd === false ?
                  <TCI formData={formDataValues} modalCloseEvent={this.onCancel} />
                  :
                  <TCI formData={{}} modalCloseEvent={this.onCancel} />
                }
              </Modal> */
          undefined}

          {showBunkerPurchaseModal ? (
            <Modal
              className="page-container"
              style={{ top: "2%" }}
              title="Bunker Purchase Order - "
              open={showBunkerPurchaseModal}
              onCancel={() =>
                this.setState({ ...this.state, showBunkerPurchaseModal: false })
              }
              width="90%"
              footer={null}
            >
              <BunkerPurchasedOrder
                formData={formDataValues}
                modalCloseEvent={this.modalCloseEvent}
              />
            </Modal>
          ) : undefined}

          {/* column filtering show/hide */}
          {sidebarVisible ? (
            <SidebarColumnFilter
              columns={columns}
              sidebarVisible={sidebarVisible}
              callback={(e) => this.callOptions(e)}
            />
          ) : null}
        </div>
      </>
    );
  }
}
export default BunkerPurchaseListing;
