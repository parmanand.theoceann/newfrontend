import React, { Component } from 'react';
import BunkerReqSummaryList from '../../../voyage-bunker-plan/bunker-req-summary-list';

class BunkerRequirementListing extends Component {
   render(){
     return(
       <BunkerReqSummaryList listType="all"/>
     )
   }
}

export default BunkerRequirementListing;
