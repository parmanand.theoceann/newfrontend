import React, { useRef, useState } from "react";
import { Table, Popconfirm, Modal, Select, Row, Col } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../../../shared";
import { FIELDS } from "../../../../shared/tableFields";
import ToolbarUI from "../../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../../shared/SidebarColumnFilter";
import BunkerInvoice from "../bunker-invoice";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import PieChart from "../../../dashboard/charts/PieChart";
import LineChart from "../../../dashboard/charts/LineChart";
import BumpChartRanking from "../../../dashboard/charts/BumpChartRanking";
import DoughNutChart from "../../../dashboard/charts/DoughNutChart";
//import { Link } from 'react-router-dom';

const BunkerInvoiceListing = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    invoceModal: false,
    selectedListData: {},
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
    ClusterDataxAxis: [],
    clusterMinMax: [],
    ClusterDataSeries: [],
    sortedData: [],
    LineCharSeriesData: [],
    LineCharxAxisData: [],
    DoughNutChartSeriesData: [],
    PieChartData: [],
    allVessels: null,
    selectedFilter: null,
    filteredSelectedData: [],
    lastfilter: '',
  });

  const allData = useRef();
  const listOfVessel = useRef({ "All": "All" });
  const listOfDate = useRef({ "All": "All" });
  const listOfInvoice = useRef({ "All": "All" });
  const appliedFilter = useRef(["All", "All", "All", "All", "All"])
  const [modelData, setModelData] = useState({
    totalVessel: 0,
    totalAmount: 0,
    totalInvoice: 0,
    totalRecive: 0,
    totalPay: 0,
    statusAmount: {},
    piChartValue: NaN,
    ClusterDataxAxis: NaN,
    ClusterDataSeries: NaN,
    DoughNutChartSeriesData: NaN,
    LineCharxAxisData: NaN,
    LineCharSeriesData: NaN
  })

  const getAlldata = () => {
    let qParams = { p: 1, l: 0 };
    let _url = `${URL_WITH_VERSION}/bunker-invoice/list?${objectToQueryStringFunc(qParams)}`;
    let headers = { order_by: { id: "desc" } };
    // if (params?.id && params.id.includes("TCOV-FULL")) {
    //   headers = {
    //     order_by: { id: "desc" },
    //     where: { OR: { voy_no: `'${params.id}'` } },
    //   };
    // }

    headers = {
      order_by: { id: "desc" },
    };

    getAPICall(_url, headers)
      .then((res) => {

        // console.log('res', res);
        allData.current = res;
        if (allData.current) {
          setFilterData(allData.current.data)
          generateFilter(allData.current.data)
        }
      })
  }

  function generateFilter(listData) {
    for (let data of listData) {
      let isVessleInList = !(data["vessel_name"] in listOfVessel.current)
      let isDateInList = !(data["invoice_date"] in listOfDate.current)
      let isInvoiceInList = !(data["invoice_no"] in listOfInvoice.current)
      if (isDateInList) {
        listOfDate.current[data["invoice_date"]] = 0
      }
      if (isVessleInList) {
        listOfVessel.current[data["vessel_name"]] = [convert_to_float(data["grand_total"])]
      } else {
        listOfVessel.current[data["vessel_name"]].push(convert_to_float(data["grand_total"]))
      }
      // if (isInvoiceInList) {
      //   listOfInvoice.current[data["invoice_no"]] = [convert_to_float(data["invoice_total"])]
      // } else {
      //   listOfInvoice.current[data["invoice_no"]].push(convert_to_float(data["invoice_total"]))
      // }
    }
    // console.log(listOfVessel,
    //   listOfDate,
    //   listOfInvoice);
  }


  function setFilterData(listData) {

    let totalAmount = 0
    let totalRecive = 0
    let totalPay = 0
    let statusAmount = {}
    let listOfVessel = {}
    let listOfDate = {}
    let listOfInvoice = {}
    let listOfFuel = {}
    for (let data of listData) {
      // console.log('data["grand_total"]', data["grand_total"]);
      let isVessleInList = !(data["vessel_name"] in listOfVessel)
      let isDateInList = !(data["invoice_date"] in listOfDate)
      let isInvoiceInList = !(data["invoice_no"] in listOfInvoice)
      totalAmount += convert_to_float(data["grand_total"])
      if (data.vessel_name === 'ATHERINA') {
        // console.log(data);
      }
      if (data["acc_type"] === "PAYABLE") {
        totalPay += 1
      }
      if (data["acc_type"] === "RECEIVABLE") {
        totalRecive += 1
      }
      if (isDateInList) {
        listOfDate[data["invoice_date"]] = 0
      }
      if (isVessleInList) {
        listOfVessel[data["vessel_name"]] = [convert_to_float(data["grand_total"])]
      } else {
        listOfVessel[data["vessel_name"]].push(convert_to_float(data["grand_total"]))
      }
      if (isInvoiceInList) {
        listOfInvoice[data["invoice_no"]] = [convert_to_float(data["grand_total"])]
      } else {
        listOfInvoice[data["invoice_no"]].push(convert_to_float(data["grand_total"]))
      }

      // data["...."] &&
      //   data["...."].length > 0 &&
      //   data["...."].map((val, indx) => {
      //     if (val.fuel_type) {
      //       console.log(val, indx);
      //       if (isFuelInList) {
      //         listOfFuel[val['fuel_type']] = [convert_to_float(val['invoice_qty'], val.fuel_type)]
      //       } else {
      //         listOfFuel[val['fuel_type']].push(convert_to_float(val['invoice_qty'], val.fuel_type))
      //       }
      //     }

      //   });

      if (data["...."] &&
        data["...."].length > 0) {
        data["...."].map((val) => {
          if (val.fuel_type) {
            if (listOfFuel[val['fuel_type']] === undefined) {
              listOfFuel[val['fuel_type']] = [convert_to_float(val['invoice_qty'])]
            } else {
              listOfFuel[val['fuel_type']].push(convert_to_float(val['invoice_qty']))
            }
          }
        })
      }

      // if (data["invoice_status_name"] in statusAmount) {
      //   statusAmount[data["invoice_status_name"]] += convert_to_float(data["grand_total"])
      // } else {
      //   statusAmount[data["invoice_status_name"]] = convert_to_float(data["grand_total"])
      // }

    }

    console.log(listOfFuel);

    // console.log(listOfVessel, listOfDate, listOfInvoice);
    // console.log('fuel....', listOfFuel);
    setModelData(data => {
      let totalVessel = Object.keys(listOfVessel).length;
      // let totalInvoice = Object.keys(listOfInvoice).length;
      let ClusterDataxAxis = getTopFiveKeysBySum(listOfVessel).map(data => data.key)
      let ClusterDataSeries = [
        {
          name: "Total Amount",
          type: "bar",
          barGap: 0,
          data: getTopFiveKeysBySum(listOfVessel).map(data => data.sum),
        },
      ];

      let LineCharxAxisData = getTopFiveKeysBySum(listOfInvoice).map(data => data.key);
      // let LineCharSeriesData = [
      //   {
      //     name: "Total Amount",
      //     type: "Line",
      //     barGap: 0,
      //     data: getTopFiveKeysBySum(listOfInvoice).map(data => data.sum),
      //   },
      // ]
      let LineCharSeriesData = getTopFiveKeysBySum(listOfInvoice).map(data => data.sum)
      // let piChartValue = preparePieChartData([{ sum: totalInvoice, key: "RECEIVABLE" },
      // { sum: totalPay, key: "PAYABLE" }])

      let DoughNutChartSeriesData = preparePieChartData(getTopFiveKeysBySum(listOfFuel))
      return {
        ...data, totalVessel,
        ClusterDataxAxis,
        totalAmount,
        ClusterDataSeries,
        LineCharxAxisData,
        LineCharSeriesData
        // DoughNutChartSeriesData,
        // totalInvoice,
        // piChartValue,
        // statusAmount
      }
    })
  }

  function getTopFiveKeysBySum(data) {
    const keySums = {};
    for (const key in data) {
      const sum = data[key].reduce((acc, value) => acc + value, 0);
      keySums[key] = sum;
    }
    const sortedKeySums = Object.entries(keySums).sort((a, b) => b[1] - a[1]);
    return sortedKeySums.slice(0, 5).map(([key, sum]) => ({ key, sum }));
  }

  function preparePieChartData(data) {
    const totalSum = data.reduce((acc, item) => acc + item.sum, 0);
    console.log('totalSum',totalSum);
    
    // return data.map((item) => {
    //   const percentage = (item.sum / totalSum) * 100;
    //   return {
    //     value: Math.round(percentage), // Round percentage to avoid decimals
    //     name: item.key,
    //   };
    // });
  }

  function convert_to_float(amount_str, fuel) {
    // console.log(fuel, amount_str);
    if (amount_str === "") {
      return 0.0;
    }
    // console.log('amount_str', amount_str);
    // Remove commas and convert to float
    // parseInt((b.grand_total).replace(/,/g, ''))
    return parseFloat(amount_str.replace(/,/g, ''));
  }

  function applyFilter(id, value) {
    appliedFilter.current[id] = value
    // console.log(appliedFilter.current);
    let filterdatag = allData.current.data.filter((item) => {
      let check = true
      for (let i = 0; i < appliedFilter.current.length; i++) {
        if (appliedFilter.current[i] !== "All") {
          if (i === 0 && item["vessel_name"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 1 && item["invoice_no"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 2 && item["account_type"] && item["account_type"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 3 && item["fia_status"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 4 && new Date(item["invoice_date"]) < new Date(appliedFilter.current[i])) {
            check = false
          }
        }
      }
      return check
    });
    setFilterData(filterdatag);
  }

  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  const navigate = useNavigate();

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      // width: 100,
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            commented As per discussion with Pallav sir
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["bunker-invoice-list"]
        ? FIELDS["bunker-invoice-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);

    setState(
      (prevState) => ({ ...prevState, columns: tableHeaders }),
      () => {
        getTableData();
      }
    );
    getAlldata()
  }, []);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;
    let qParams = { p: pageOptions.pageIndex, l: 0 };
    let headers = { order_by: { id: "desc" } };
    // let qParamsGraph = { p: pageOptions.pageIndex, l: 0 }
    let _search =
      searchtype &&
        searchtype.hasOwnProperty("searchOptions") &&
        searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : state.typesearch;

    if (
      _search &&
      _search.hasOwnProperty("searchValue") &&
      _search.hasOwnProperty("searchOptions") &&
      _search["searchOptions"] !== "" &&
      _search["searchValue"] !== ""
    ) {
      let wc = {};
      _search["searchValue"] = _search["searchValue"].trim();

      if (_search["searchOptions"].indexOf(";") > 0) {
        let so = _search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: _search["searchValue"] }));
      } else {
        wc = {
          OR: { [_search["searchOptions"]]: { l: _search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: _search.searchOptions,
        searchValue: _search.searchValue,
      };
    }

    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = {},
      dataArr = [],
      totalRows = 0;
    let _url = `${URL_WITH_VERSION}/bunker-invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    totalRows = data && data.total_rows ? data.total_rows : 0;
    dataArr = data && data.data ? data.data : [];

    let filtered = [...dataArr].sort((a, b) => {
      return parseInt((b.grand_total).replace(/,/g, '')) - parseInt((a.grand_total).replace(/,/g, ''))
    });


    let fuel = {
      ULSFO: 0,
      MGO: 0,
      LSMGO: 0,
      IFO: 0,
      VLSFO: 0,
    }
    let allVessels = new Set();
    let allInvoices = [];
    if (dataArr && dataArr.length > 0) {
      dataArr.map((e) => {
        if (e.vessel_name) allVessels.add(e.vessel_name)
        if (e.invoice_no) {
          allInvoices.push({ label: e.invoice_no, value: e.invoice_no })
        }

        let fuel_type = "";
        let i_qty = 0;
        e["...."] &&
          e["...."].length > 0 &&
          e["...."].map((val) => {
            if (val.fuel_type) {
              fuel[val.fuel_type] += parseFloat(val.invoice_qty)
            }
            fuel_type =
              fuel_type != "" && fuel_type != null
                ? fuel_type + ", " + val.fuel_type
                : val.fuel_type != null && val.fuel_type;
            i_qty += parseFloat(val.invoice_qty);
          });
        e["fuel_type"] = fuel_type;
        e["i_qty"] = i_qty;
        // fuel[fuel_type] += i_qty
      });
    }

    let vessels = [];
    for (let vessel of allVessels) {
      vessels.push({ label: vessel, value: vessel });
    }

    let DoughNut = [];
    for (let key in fuel) {
      DoughNut.push({ value: fuel[key], name: key + " " + fuel[key] + ' MT' });
    }

    let donloadArr = [];
    if (dataArr.length > 0 && totalRows > 0) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      _state["responseData"] = dataArr;
    }

    // setGraphData(filtered)
    let slicedData = [];
    let amount = []
    let series = [
      {
        name: "Total Amount",
        type: "bar",
        // barGap: 0,

        data: [],
      },
    ]
    let invoiceNo = []
    let minMax = []
    for (let i = 0; i < 5; i++) {
      slicedData.push(filtered[i].vessel_name)
      series[0].data.push(parseInt((filtered[i].grand_total).replace(/,/g, '')))
      amount.push(parseInt((filtered[i].grand_total).replace(/,/g, '')))
      invoiceNo.push(filtered[i].invoice_no)
    }


    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      sortedData: filtered,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      DoughNutChartSeriesData: [...DoughNut],
      allVessels: vessels,
      allInvoices: allInvoices,
      ClusterDataxAxis: slicedData,
      ClusterDataSeries: series,
      LineCharSeriesData: [...amount],
      LineCharxAxisData: [...invoiceNo],
      clusterMinMax: [series.length && series[0].data[4], series.length && series[0].data[0]],
      loading: false,
    }));
  };

  const handleSelect = (value, filterType) => {
    let vesselNames = new Set()
    let filteredData = null

    if (filterType === 'vesselName') {

      if (state.filteredSelectedData.length === 0) {
        filteredData = state.sortedData.filter(item => item.vessel_name === value)
        filteredData.forEach(item => {
          vesselNames.add(item.vessel_name)
        })
      }
      // else {
      //   if (state.lastfilter) {
      //     filteredData = state.sortedData.filter(item => item.vessel_name === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   } else {
      //     filteredData = state.filteredSelectedData.filter(item => item.vessel_name === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   }
      // }
    }
    else if (filterType === 'InvoiceNo') {
      if (state.filteredSelectedData.length === 0) {
        filteredData = state.sortedData.filter(item => item.invoice_no === value)
        filteredData.forEach(item => {
          vesselNames.add(item.vessel_name)
        })
      }
      // else {
      //   if (state.lastfilter) {
      //     filteredData = state.sortedData.filter(item => item.invoice_no === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   } else {
      //     filteredData = state.filteredSelectedData.filter(item => item.invoice_no === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   }
      // }
    }
    else if (filterType === 'Status') {
      if (state.filteredSelectedData.length === 0) {
        filteredData = state.sortedData.filter(item => item.invoice_status_name === value)
        filteredData.forEach(item => {
          vesselNames.add(item.vessel_name)
        })
      }
      // else {
      //   if (state.lastfilter) {
      //     filteredData = state.sortedData.filter(item => item.invoice_status_name === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   } else {
      //     filteredData = state.filteredSelectedData.filter(item => item.invoice_status_name === value)
      //     filteredData.forEach(item => {
      //       vesselNames.add(item.vessel_name)
      //     })
      //   }
      // }
    }
    // console.log('set', vesselNames);

    // console.log('filteredData', filteredData);
    let amount = []
    let series = [
      {
        name: "Total Amount",
        type: "bar",
        // barGap: 0,
        data: [0],
      },
    ]
    let invoiceNo = []
    let fuel = {
      ULSFO: 0,
      MGO: 0,
      LSMGO: 0,
      IFO: 0,
      VLSFO: 0,
    }
    let doughNut = [];
    for (let item of filteredData) {
      if (parseInt((item.grand_total).replace(/,/g, '')) !== 0) {
        amount.push(parseInt((item.grand_total).replace(/,/g, '')))
        invoiceNo.push(item.invoice_no)
      }
      item["...."] &&
        item["...."].length > 0 &&
        item["...."].map((val) => {
          if (val.fuel_type) {
            fuel[val.fuel_type] += parseFloat(val.invoice_qty)
          }
        });
      series[0].data[0] += parseInt((item.grand_total).replace(/,/g, ''))
    }
    for (let key in fuel) {
      if (fuel[key] !== 0) {
        doughNut.push({ value: fuel[key], name: key + ' ' + fuel[key] + ' MT' })
      }
    }
    // console.log(state.filteredSelectedData);

    setState(prev => ({
      ...prev,
      filteredSelectedData: filteredData,
      DoughNutChartSeriesData: [...doughNut],
      ClusterDataxAxis: [...vesselNames],
      ClusterDataSeries: series,
      LineCharSeriesData: [...amount],
      LineCharxAxisData: [...invoiceNo],
      lastfilter: filterType,
      clusterMinMax: [parseInt((filteredData[filteredData.length - 1].grand_total).replace(/,/g, '')), parseInt((filteredData[0].grand_total).replace(/,/g, ''))]
    }))
  }


  const InvoiceModal = (st, data) => {
    setState((prevState) => ({
      ...prevState,
      invoceModal: st,
      selectedListData: data,
    }));
  };

  const closeModale = (st, data) => {
    setState(
      (prevState) => ({
        ...prevState,
        invoceModal: st,
        selectedListData: data,
      }),
      () => {
        getTableData();
      }
    );
  };

  const redirectToAdd = async (e, record = null) => {
    InvoiceModal(true, record);
    // const { history } = props;
    // history.push(`/edit-bunker-invoice/${record.requirement_id}`);
  };

  const onCancel = () => {
    getTableData();
    setState((prevState) => ({ ...prevState, isAdd: true, isVisible: false }));
  };

  const onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-inv/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
      (e, { size }) => {
        setState(({ columns }) => {
          const nextColumns = [...columns];
          nextColumns[index] = {
            ...nextColumns[index],
            width: size.width,
          };
          return { ...state, columns: nextColumns };
        });
      };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const tableCol = state?.columns
    ?.filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  const PieChartData = [
    { value: 60, name: "Receivable 60k" },
    { value: 40, name: "PAYABLE 40k" },
  ];

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Bunker Invoice List</span>
                  </h4>
                </div>
                {/* <div className="action-btn">
                    <Link to="add-tcov">Add Bunker Invoice</Link>
                  </div> */}
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {state.loading === false ? (
                  <ToolbarUI
                    routeUrl={"bunker-invoice-list-toolbar"}
                    optionValue={{
                      pageOptions: state.pageOptions,
                      columns: state.columns,
                      search: state.search,
                    }}
                    showGraph={showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "bunker-invlist"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "bunker-invlist"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "bunker-invlist"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {state.isVisible === true ? (
          <>Add TCOV</>
        ) : /* <Modal
              title={(isAdd===false?"Edit":"Add")+" TCI Form"}
             open={isVisible}
              width='95%'
              onCancel={onCancel}
              style={{top: '10px'}}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
            >
              {
                isAdd === false ?
                <TCI formData={formDataValues} modalCloseEvent={onCancel} />
                :
                <TCI formData={{}} modalCloseEvent={onCancel} />
              }
            </Modal> */
          undefined}
        {/* column filtering show/hide */}
        {state.sidebarVisible ? (
          <SidebarColumnFilter
            columns={state.columns}
            sidebarVisible={state.sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
        {state.invoceModal && (
          <Modal
            style={{ top: "2%" }}
            title="Edit Bunker Invoice"
            open={state.invoceModal}
            onCancel={() => closeModale(false, null)}
            width="90%"
            footer={null}
          >
            <BunkerInvoice
              closeModale={() => {
                closeModale(false, null);
              }}
              isEdit={true}
              formData={state.selectedListData}
            />
          </Modal>
        )}
        <Modal
          title="Bunker Invoice Dashboard"
          open={state.isGraphModal}
          width="90%"
          onCancel={handleCancel}
          footer={null}
        >
          <div style={{ display: "flex", flexDirection: "column" }}>
            <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
              <Col
                xs={24}
                sm={8}
                md={8}
                lg={3}
                xl={3}
                style={{ borderRadius: "15px", padding: "8px" }}
              >
                <div
                  style={{
                    background: "#1D406A",
                    color: "white",
                    borderRadius: "15px",
                  }}
                >
                  <p>Total Vessels</p>
                  <p>{modelData.totalVessel}</p>
                </div>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={3}
                lg={3}
                xl={3}
                style={{ borderRadius: "15px", padding: "8px" }}
              >
                <div
                  style={{
                    background: "#1D406A",
                    color: "white",
                    borderRadius: "15px",
                  }}
                >
                  <p>Total Amount</p>
                  <p>{modelData.totalAmount.toFixed(3)} $</p>
                </div>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={3}
                lg={3}
                xl={3}
                style={{ borderRadius: "15px", padding: "8px" }}
              >
                <div
                  style={{
                    background: "#1D406A",
                    color: "white",
                    borderRadius: "15px",
                  }}
                >
                  <p>Total Invoice</p>
                  <p>240</p>
                </div>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={3}
                lg={3}
                xl={3}
                style={{ textAlign: "start", padding: "8px" }}
              >
                <p style={{ margin: "0" }}>Vessel Name</p>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={state.allVessels}
                  onChange={(selectedValue) => handleSelect(selectedValue, 'vesselName')}
                ></Select>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={6}
                lg={3}
                xl={3}
                style={{ textAlign: "start", padding: "8px" }}
              >
                <p style={{ margin: "0" }}>Invoice No</p>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={state.allInvoices}
                  onChange={(selectedValue) => handleSelect(selectedValue, 'InvoiceNo')}
                ></Select>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={6}
                lg={3}
                xl={3}
                style={{ textAlign: "start", padding: "8px" }}
              >
                <p style={{ margin: "0" }}>Account Type</p>
                <Select
                  placeholder="Payable"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "Payable",
                      label: "Payable",
                    },
                    {
                      value: "Receivable",
                      label: "Receivable",
                    },
                  ]}
                  onChange={(selectedValue) => handleSelect(selectedValue, 'AccountType')}
                ></Select>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={6}
                lg={3}
                xl={3}
                style={{ textAlign: "start", padding: "8px" }}
              >
                <p style={{ margin: "0" }}>Status</p>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "PREPARED",
                      label: "PREPARED",
                    },
                    {
                      value: "APPROVED",
                      label: "APPROVED",
                    },
                    {
                      value: "POSTED",
                      label: "POSTED",
                    },
                    {
                      value: "VERIFIED",
                      label: "VERIFIED",
                    },
                  ]} onChange={(selectedValue) => handleSelect(selectedValue, 'Status')}
                ></Select>
              </Col>
              <Col
                xs={24}
                sm={12}
                md={6}
                lg={3}
                xl={3}
                style={{ textAlign: "start", padding: "8px" }}
              >
                <p style={{ margin: "0" }}>Date To</p>
                <Select
                  placeholder="2024-02-18"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "2024-01-26",
                      label: "2024-01-26",
                    },
                    {
                      value: "2023-11-16",
                      label: "2023-11-16",
                    },
                    {
                      value: "2023-11-17",
                      label: "2023-11-17",
                    },
                  ]}
                ></Select>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <ClusterColumnChart
                  Heading={"Total Amount Per Vessel"}
                  ClusterDataxAxis={modelData.ClusterDataxAxis}
                  ClusterDataSeries={modelData.ClusterDataSeries}
                  maxValueyAxis={modelData.ClusterDataSeries ? Math.max(...modelData.ClusterDataSeries[0].data) : 0}
                // minValueyAxis={state.clusterMinMax[0]}
                />
              </Col>
              <Col span={12}>
                <LineChart
                  LineCharSeriesData={modelData.LineCharSeriesData}
                  LineCharxAxisData={modelData.LineCharxAxisData}
                  Heading={"Total Amount Per Invoice No."}
                />
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <DoughNutChart
                  DoughNutChartSeriesData={state.DoughNutChartSeriesData}
                  DoughNutChartSeriesRadius={["40%", "70%"]}
                  // options={{
                  //   tooltip: {
                  //     trigger: "item",
                  //     formatter: "{a} <br/>{b}: {c}",
                  //   },
                  //   grid: {
                  //     left: 30,
                  //     right: 30,
                  //     bottom: 10,
                  //     top: 10,
                  //     containLabel: true,
                  //   },
                  //   series: [
                  //     {
                  //       type: "pie",
                  //       radius: ["20%", "70%"],
                  //       avoidLabelOverlap: false,
                  //       label: {
                  //         show: true,
                  //         position: "outside",
                  //         formatter: "{b}: {d}%",
                  //         fontSize: "10",
                  //       },
                  //       labelLine: {
                  //         show: true,
                  //         length: 10,
                  //       },
                  //       emphasis: {
                  //         label: {
                  //           show: true,
                  //           fontSize: "12",
                  //         },
                  //       },

                  //       data: DoughNutChartSeriesData,
                  //     },
                  //   ],
                  // }}
                  style={{ height: "330px" }}
                  Heading={"Fuel Total Quantity"}
                />
              </Col>
              <Col span={12}>
                <PieChart
                  PieChartData={PieChartData}
                  Heading={" Inv Amount per Account Type"}
                />
              </Col>
            </Row>
          </div>
        </Modal>
      </div>
    </>
  );
};

export default BunkerInvoiceListing;
