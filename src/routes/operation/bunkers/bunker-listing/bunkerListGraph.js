
// import React, { useEffect, useState } from "react";
// import { Col, Row, Select } from "antd";
// import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
// import StackGoupColumnChart from "../../../dashboard/charts/StackNormalizationChart";
// import LineChart from "../../../dashboard/charts/LineChart";
// import PieChart from "../../../dashboard/charts/PieChart";

// const BunkerListGraph = ({stateGraph}) => {

//   return (
//     <div>

// <div style={{ display: "flex", flexDirection: "column" }}>
//             <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
//               <Col
//                 xs={24}
//                 sm={8}
//                 md={8}
//                 lg={3}
//                 xl={3}
//                 style={{ borderRadius: "15px", padding: "8px" }}
//               >
//                 <div
//                   style={{
//                     background: "#1D406A",
//                     color: "white",
//                     borderRadius: "15px",
//                   }}
//                 >
//                   <p>Total vessels</p>
//                   <p>300</p>
//                 </div>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={3}
//                 lg={3}
//                 xl={3}
//                 style={{ borderRadius: "15px", padding: "8px" }}
//               >
//                 <div
//                   style={{
//                     background: "#1D406A",
//                     color: "white",
//                     borderRadius: "15px",
//                   }}
//                 >
//                   <p>Total amount</p>
//                   <p>300 $</p>
//                 </div>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={3}
//                 lg={3}
//                 xl={3}
//                 style={{ borderRadius: "15px", padding: "8px" }}
//               >
//                 <div
//                   style={{
//                     background: "#1D406A",
//                     color: "white",
//                     borderRadius: "15px",
//                   }}
//                 >
//                   <p>Total invoice</p>
//                   <p>240</p>
//                 </div>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={3}
//                 lg={3}
//                 xl={3}
//                 style={{ textAlign: "start", padding: "8px" }}
//               >
//                 <p style={{ margin: "0" }}>vessel name</p>
//                 <Select
//                   placeholder="All"
//                   optionFilterProp="children"
//                   options={[
//                     {
//                       value: "PACIFIC EXPLORER",
//                       label: "OCEANIC MAJESTY",
//                     },
//                     {
//                       value: "OCEANIC MAJESTY",
//                       label: "OCEANIC MAJESTY",
//                     },
//                     {
//                       value: "CS HANA",
//                       label: "CS HANA",
//                     },
//                   ]}
//                 ></Select>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={6}
//                 lg={3}
//                 xl={3}
//                 style={{ textAlign: "start", padding: "8px" }}
//               >
//                 <p style={{ margin: "0" }}>Invoice No</p>
//                 <Select
//                   placeholder="All"
//                   optionFilterProp="children"
//                   options={[
//                     {
//                       value: "TCE02-24-01592",
//                       label: "TCE02-24-01592",
//                     },
//                     {
//                       value: "TCE01-24-01582",
//                       label: "TCE01-24-01582",
//                     },
//                     {
//                       value: "TCE01-24-01573",
//                       label: "TCE01-24-01573",
//                     },
//                   ]}
//                 ></Select>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={6}
//                 lg={3}
//                 xl={3}
//                 style={{ textAlign: "start", padding: "8px" }}
//               >
//                 <p style={{ margin: "0" }}>Account Type</p>
//                 <Select
//                   placeholder="Payable"
//                   optionFilterProp="children"
//                   options={[
//                     {
//                       value: "Payable",
//                       label: "Payable",
//                     },
//                     {
//                       value: "Receivable",
//                       label: "Receivable",
//                     },
//                   ]}
//                 ></Select>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={6}
//                 lg={3}
//                 xl={3}
//                 style={{ textAlign: "start", padding: "8px" }}
//               >
//                 <p style={{ margin: "0" }}>Status</p>
//                 <Select
//                   placeholder="PREPARED"
//                   optionFilterProp="children"
//                   options={[
//                     {
//                       value: "PREPARED",
//                       label: "PREPARED",
//                     },
//                     {
//                       value: "APPROVED",
//                       label: "APPROVED",
//                     },
//                     {
//                       value: "POSTED",
//                       label: "POSTED",
//                     },
//                     {
//                       value: "VERIFIED",
//                       label: "VERIFIED",
//                     },
//                   ]}
//                 ></Select>
//               </Col>
//               <Col
//                 xs={24}
//                 sm={12}
//                 md={6}
//                 lg={3}
//                 xl={3}
//                 style={{ textAlign: "start", padding: "8px" }}
//               >
//                 <p style={{ margin: "0" }}>Date To</p>
//                 <Select
//                   placeholder="2024-02-18"
//                   optionFilterProp="children"
//                   options={[
//                     {
//                       value: "2024-01-26",
//                       label: "2024-01-26",
//                     },
//                     {
//                       value: "2023-11-16",
//                       label: "2023-11-16",
//                     },
//                     {
//                       value: "2023-11-17",
//                       label: "2023-11-17",
//                     },
//                   ]}
//                 ></Select>
//               </Col>
//             </Row>

//             <Row gutter={16}>
//               <Col span={12}>
//                 <ClusterColumnChart
//                   Heading={"Total Amount Per Vessel"}
//                 //   ClusterDataxAxis={ClusterDataxAxis}
//                 //   ClusterDataSeries={ClusterDataSeries}
//                   maxValueyAxis={"350"}
//                 />
//               </Col>
//               <Col span={12}>
//                 <LineChart
//                 //   LineCharSeriesData={LineCharSeriesData}
//                 //   LineCharxAxisData={LineCharxAxisData}
//                   Heading={"Total Amount Per Invoice No."}
//                 />
//               </Col>
//             </Row>
//             <Row gutter={16}>
//               <Col span={12}>
//                 <DoughNutChart
//                 //   DoughNutChartSeriesData={DoughNutChartSeriesData}
//                   DoughNutChartSeriesRadius={["40%", "70%"]}
//                   options={{
//                     tooltip: {
//                       trigger: "item",
//                       formatter: "{a} <br/>{b}: {c}",
//                     },

//                     // legend: {
//                     //   orient: legendhorizontal ?? "vertical",
//                     //   left: "left",
//                     // },
//                     grid: {
//                       left: 30,
//                       right: 30,
//                       bottom: 10,
//                       top: 10,
//                       containLabel: true,
//                     },
//                     series: [
//                       {
//                         // name: DoughNutChartSeriesName, // come from prop as a string
//                         type: "pie",
//                         //radius: DoughNutChartSeriesRadius,
//                         radius: ["20%", "70%"],
//                         avoidLabelOverlap: false,
//                         // label: {
//                         //   show: false,
//                         //   position: "center",
//                         // },
//                         label: {
//                           show: true,
//                           position: "outside",
//                           formatter: "{b}: {d}%", // {b} refers to the name of each slice, {d} refers to the percentage
//                           fontSize: "10",
//                         },
//                         labelLine: {
//                           show: true,
//                           length: 10,
//                         },
//                         emphasis: {
//                           label: {
//                             show: true,
//                             fontSize: "12",
//                             // fontWeight: "bold",
//                           },
//                         },

//                         // data: DoughNutChartSeriesData,
//                       },
//                     ],
//                   }}
//                   style={{ height: "330px" }}
//                   Heading={"Fuel Total Quantity"}
//                 />
//               </Col>
//               <Col span={12}>
//                 <PieChart
//                 //   PieChartData={PieChartData}
//                   Heading={" Inv Amount per Account Type"}
//                 />
//               </Col>
//             </Row>
//           </div>

//     </div>
//   )
// }


// export default BunkerListGraph






import React, { useEffect, useState } from "react";
import { Col, Row, Select } from "antd";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import StackGoupColumnChart from "../../../dashboard/charts/StackNormalizationChart";
import LineChart from "../../../dashboard/charts/LineChart";
import PieChart from "../../../dashboard/charts/PieChart";
import URL_WITH_VERSION, { objectToQueryStringFunc, postAPICall } from "../../../../shared";
const BunkerListGraph = ({ stateGraph }) => {

    const [state, setState] = useState({
        loading: false,
        responseData: [],
        isVisible: false,
        donloadArray: [],
        vessel_name: [],
        invoice_no: [],
        topFiveTotal_amount: [],
        statusTotals: [],
        filterData: {
            vesselName: "",
            invoiceNumber: "",
            DAteTo: "",
            invoice_status_name: "",

        }
    });

    useEffect(() => {
        TotalTopFiveTotalAmount();
        totalStatus();
        getVesselConsumption(state.filterData);
    }, []);


    const handleReset = () => {
        setState((pre) => ({
            ...pre,
            formData: {

            },
        }))
    }


    const totalStatus = () => {
        const statusTotals = {
            Prepared: 0,
            Verified: 0,
            Approved: 0,
            Posted: 0,
        };

        stateGraph?.responseGraphData?.forEach((item) => {
            const { invoice_status_name, total_amount } = item;
            if (statusTotals.hasOwnProperty(invoice_status_name)) {
                // Remove commas and convert total_amount to number
                const amount = parseFloat(total_amount.replace(/,/g, ""));
                statusTotals[invoice_status_name] += amount;
            }
        });

        // Convert the statusTotals object into an array of objects
        const ClusterDataSeries = Object.keys(statusTotals).map((status) => ({
            name: status,
            value: statusTotals[status],
        }));
        setState((pre) => ({
            ...pre,
            statusTotals: ClusterDataSeries,
        }));
    };



    const getVesselConsumption = async (payload) => {
        // const query = {
        //     inv: 174,
        // };
        // let qParamString = objectToQueryStringFunc(query);
        setState((pre) => ({
            ...pre,
            responseData: [],
            graphVisible: false,
        }));
        try {
            let suURL = `${URL_WITH_VERSION}/chattering-dashboard/invoice/filter`;
            let suMethod = "POST";
            await postAPICall(suURL, payload ?? null, suMethod, (data) => {
                const respdata = data;
                console.log("respdata", respdata);
                const InvoiceNumbers = [];
                const uniqueVesselNames = new Set(); // Using a Set to collect unique vessel names

                // Iterate over each item in the data array
                respdata?.data?.forEach((item) => {
                    InvoiceNumbers.push(item.invoice_no);
                    uniqueVesselNames.add(item.vessel_name); // Adding vessel names to the Set
                });

                // Convert Set to array for vessel names
                const vesselNames = [...uniqueVesselNames];

                setState((pre) => ({
                    ...pre,
                    vessel_name: vesselNames,
                    invoice_no: InvoiceNumbers,
                    responseData: respdata,
                    graphVisible: true,
                }));
            });
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };


    const TotalTopFiveTotalAmount = () => {
        const sortedData = stateGraph?.responseGraphData?.sort(
            (a, b) => a.total_amount - b.total_amount
        ); // Sort in ascending order
        const highestFive = sortedData?.slice(-5); // Get the last 5 elements

        setState((prev) => ({ ...prev, topFiveTotal_amount: highestFive }));
    };

    const ClusterDataSeries = [
        {
            name: "Total Amount",
            type: "bar",
            barGap: 0,
            data: state.topFiveTotal_amount.map((item) =>
                item.total_amount.replace(/,/g, "")
            ),
        },
    ];
    const ClusterDataSeries2 = [
        {
            name: "Total Amount",
            type: "bar",
            barGap: 0,
            data: state.topFiveTotal_amount.map((item) =>
                item.total_amount.replace(/,/g, "")
            ),
        },
    ];
    const ClusterDataxAxis2 = ["Prepared", "Verified", "Approved", "Posted"];

    const PieChartData = state.topFiveTotal_amount.map((item) => ({
        name: item.voyage_manager_name,
        value: parseFloat(item.total_amount.replace(/,/g, "")), // Convert total_amount to a number
    }));


    return (
        <>
            <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
                <Col
                    xs={24}
                    sm={8}
                    md={8}
                    lg={3}
                    xl={3}
                    style={{ borderRadius: "15px", padding: "8px" }}
                >
                    <div
                        style={{
                            background: "#1D406A",
                            color: "white",
                            borderRadius: "15px",
                        }}
                    >
                        <p>Total vessels</p>
                        <p>300</p>
                    </div>
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={3}
                    lg={3}
                    xl={3}
                    style={{ borderRadius: "15px", padding: "8px" }}
                >
                    <div
                        style={{
                            background: "#1D406A",
                            color: "white",
                            borderRadius: "15px",
                        }}
                    >
                        <p>Total amount</p>
                        <p>300 $</p>
                    </div>
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={3}
                    lg={3}
                    xl={3}
                    style={{ borderRadius: "15px", padding: "8px" }}
                >
                    <div
                        style={{
                            background: "#1D406A",
                            color: "white",
                            borderRadius: "15px",
                        }}
                    >
                        <p>Total invoice</p>
                        <p>240</p>
                    </div>
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={3}
                    lg={3}
                    xl={3}
                    style={{ textAlign: "start", padding: "8px" }}
                >
                    <p style={{ margin: "0" }}>vessel name</p>
                    <Select
                        placeholder="All"
                        optionFilterProp="children"
                        options={stateGraph.vessel_name.map((vesselName) => ({
                            value: vesselName,
                            label: vesselName,
                        }))}
                    />
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={6}
                    lg={3}
                    xl={3}
                    style={{ textAlign: "start", padding: "8px" }}
                >
                    <p style={{ margin: "0" }}>Invoice No</p>
                    <Select
                        // placeholder="All"
                        // optionFilterProp="children"
                        // options={stateGraph.invoice_no.map((invoice_no) => ({
                        //   value: invoice_no,
                        //   label: invoice_no,
                        // }))}

                        placeholder="All"
                        optionFilterProp="children"
                        options={
                            stateGraph.invoice_no
                                ? stateGraph.invoice_no.map((invoice_no) => ({
                                    value: invoice_no,
                                    label: invoice_no,
                                }))
                                : []
                        }
                    />
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={6}
                    lg={3}
                    xl={3}
                    style={{ textAlign: "start", padding: "8px" }}
                >
                    <p style={{ margin: "0" }}>Voyage No</p>
                    <Select
                        placeholder="Payable"
                        optionFilterProp="children"
                        options={[
                            {
                                value: "Payable",
                                label: "Payable",
                            },
                            {
                                value: "Receivable",
                                label: "Receivable",
                            },
                        ]}
                    ></Select>
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={6}
                    lg={3}
                    xl={3}
                    style={{ textAlign: "start", padding: "8px" }}
                >
                    <p style={{ margin: "0" }}>Status</p>
                    <Select
                        placeholder="PREPARED"
                        optionFilterProp="children"
                        options={[
                            {
                                value: "PREPARED",
                                label: "PREPARED",
                            },
                            {
                                value: "APPROVED",
                                label: "APPROVED",
                            },
                            {
                                value: "POSTED",
                                label: "POSTED",
                            },
                            {
                                value: "VERIFIED",
                                label: "VERIFIED",
                            },
                        ]}
                    ></Select>
                </Col>
                <Col
                    xs={24}
                    sm={12}
                    md={6}
                    lg={3}
                    xl={3}
                    style={{ textAlign: "start", padding: "8px" }}
                >
                    <p style={{ margin: "0" }}>Date To</p>
                    <Select
                        placeholder="2024-02-18"
                        optionFilterProp="children"
                        options={[
                            {
                                value: "2024-01-26",
                                label: "2024-01-26",
                            },
                            {
                                value: "2023-11-16",
                                label: "2023-11-16",
                            },
                            {
                                value: "2023-11-17",
                                label: "2023-11-17",
                            },
                        ]}
                    ></Select>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={12}>
                    <ClusterColumnChart
                        Heading={"Total Amount Per Vessel"}
                        ClusterDataxAxis={state.topFiveTotal_amount.map(
                            (item) => item.vessel_name
                        )}
                        ClusterDataSeries={ClusterDataSeries}
                        maxValueyAxis={"350"}
                    />
                </Col>
                <Col span={12}>
                    <LineChart
                        //   parseFloat(item.total_amount.replace(',', ''))
                        LineCharSeriesData={state.topFiveTotal_amount.map((item) =>
                            item.total_amount.replace(/,/g, "")
                        )}
                        LineCharxAxisData={state.topFiveTotal_amount.map(
                            (item) => item.invoice_no
                        )}
                        Heading={"Total Amount Per Invoice No."}
                    />
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={12}>
                    <ClusterColumnChart
                        Heading={"Total Amount As Per Status"}
                        ClusterDataxAxis={state.statusTotals.map(
                            (item) => item.name
                        )}
                        ClusterDataSeries={ClusterDataSeries2}
                        maxValueyAxis={"500"}
                    />
                </Col>
                <Col span={12}>
                    <PieChart
                        PieChartData={PieChartData}
                        Heading={"Total Amount Per Voyage no."}
                    />
                </Col>
            </Row>
        </>
    );
};

export default BunkerListGraph;