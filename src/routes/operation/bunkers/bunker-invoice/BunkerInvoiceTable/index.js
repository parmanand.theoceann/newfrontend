import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button, Form } from 'antd';

const FormItem = Form.Item;

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        type: "Type",
        invqty: "Inv Qty",
        oprqty: "Opr Qy",
        basicprc: "Basic Prc",
        bargingprc: "Barging Prc",
        otherprc: "Other Prc",
        othcost: "Oth Cost",
        salestax: "Sales Tax",
        totalcost: "Total Cost",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class BunkerInvoiceTable extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Type',
            dataIndex: 'type',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'type'),
        },
        {
            title: 'Inv Qty',
            dataIndex: 'invqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'invqty'),
        },
        {
            title: 'Opr Qty',
            dataIndex: 'oprqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'oprqty'),
        },
        {
            title: 'Basic Prc',
            dataIndex: 'basicprc',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'minqty'),
        },
        {
            title: 'Barging Prc',
            dataIndex: 'bargingprc',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'bargingprc'),
        },
        {
            title: 'Other Prc',
            dataIndex: 'otherprc',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'otherprc'),
        },
        {
            title: 'Oth Cost',
            dataIndex: 'othcost',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'othcost'),
        },
        {
            title: 'Sales Tax',
            dataIndex: 'salestax',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'salestax'),
        },
        {
            title: 'Total Cost',
            dataIndex: 'totalcost',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'totalcost'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <a className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                    <a className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </a>
                                </span>
                                : <a className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></a>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />

                <div className="bunkerInvoiceWrapper">
                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="Total Cost">
                                <Input size="default" placeholder="0.00" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Post Charges">
                                <Input size="default" placeholder="0.00" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="GST %">
                                <Input size="default" placeholder="0.00" />
                            </FormItem>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4">
                            <FormItem label="PST %">
                                <Input size="default" placeholder="0.00" />
                            </FormItem>
                        </div>

                        <div className="col-md-4">
                            <FormItem label="Grand Total">
                                <Input size="default" placeholder="0.00" />
                            </FormItem>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default BunkerInvoiceTable;