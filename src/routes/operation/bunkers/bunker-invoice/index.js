import React, { Component, useEffect, useRef } from "react";
import { Modal, Layout, Row, Col } from "antd";
//import Tde from "../../../tde/Tde";
import Tde from "../../../tde/";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import CreateInvoice from "../../../create-invoice/CreateInvoice";
import URL_WITH_VERSION, { apiDeleteCall, postAPICall, openNotificationWithIcon, getAPICall, useStateCallback, } from "../../../../shared";
import Attachment from "../../../../shared/components/Attachment";
import { uploadAttachment, deleteAttachment, getAttachments } from "../../../../shared/attachments";
import InvoicePopup from "../../../create-invoice/InvoicePopup";
import Remarks from "../../../../shared/components/Remarks";
import { DeleteOutlined, SaveOutlined, SyncOutlined, EditOutlined } from "@ant-design/icons";
const { Content } = Layout;

const BunkerInvoice = (props) => {
  const [state, setState] = useStateCallback({
    modals: {
      InvoiceModal: false,
      TdeShow: false,
      BunkerReport: false,
      InvoicePopup: false,
    },
    frmName: "bunker_invoice",
    formData: props.formData,
    frmVisible: true,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    isShowVoyageFixtureReport: false,
    frmOptions: [],
    modelVisible: false,
    msg: "",
    TdeList: null,
    invoiceReport: null,
    isSaved: false,
    popupdata: null,
    isEdit: props.isEdit || false,
    isShowAttachment: false,
    isRemarkModel: false,
    loadComponent: undefined,
  })

  let formDatref = useRef();

  let formref = useRef(null);

  const reFreshForm = () => {
    setState(
      (prevState) => ({
        ...prevState,
        formData: { ...formref.current },
        frmVisible: false,
      }),
      () => {
        setState({ ...state, frmVisible: true });
      }
    );
  };

  useEffect(() => {
    formref.current = Object.assign({}, state.formData);
  }, []);

  const setEditData = async (editData) => {
    let _formData = {};
    setState(prevState => ({ ...prevState, frmVisible: false }));
    const response1 = await getAPICall(
      `${URL_WITH_VERSION}/bunker-invoice/edit?e=${editData.requirement_id}`
    );
    let respData = response1["data"];


    if (respData && respData.hasOwnProperty("invoice_no")) {
      _formData = respData;

      _formData["-"] = {
        grand_total: respData["grand_total"],
        gst: respData["gst"],
      };
      delete respData.gst;
      delete respData.grand_total;
    }

    setState(prevState => ({ ...prevState, formData: _formData, frmVisible: true }));
  };


  useEffect(() => {
    const fetchData = async () => {
      const { formData, isEdit } = props;
      let _formData = {};
      if (formData) {

        if (isEdit === true) {

          setEditData(formData);
        } else {
          if (
            formData.hasOwnProperty("-----") ||
            formData.hasOwnProperty(".....") ||
            formData.hasOwnProperty("--------")
          ) {
            // let _url = `${URL_WITH_VERSION}/address/ri-list/${formData["....."] &&
            //   formData["....."].my_company}`;
            // const response = await getAPICall(_url);
            // const data = await response["data"];
            // const response1 = await getAPICall(
            //   `${URL_WITH_VERSION}/bunker-invoice/edit?e=${formData["....."] &&
            //   formData["....."].requirement_id}`
            // );
            // console.log("response1",response1);
            // let respData = response1["data"];

            // if (respData && respData.hasOwnProperty("invoice_no")) {
            //   _formData = respData;
            // _formData["-"] = {
            //   grand_total: respData["grand_total"],
            //   gst: respData["gst"],
            // };
            //} 
            //else {
            let arrayData = [];
            formData["--------"] &&
              formData["--------"].length > 0 &&
              formData["--------"].map((val) => {
                arrayData.push({
                  barge_cost: val.barge_cost,
                  barge_rate: val.barge_rate,
                  bunk_id: val.bunk_id,
                  editable: true,
                  fuel_cost: val.fuel_cost,
                  fuel_type: val.fuel_type,
                  grade: val.grade,
                  index: val.index,
                  key: val.key,
                  net_cost: val.net_cost,
                  other_cost: val.other_cost,
                  recieved_qty: val.recieved_qty,
                  sales_tax: val.sales_tax,
                  sulphur: val.sulphur,
                });
                return true;
              });
            _formData = {
              vendor: formData["....."].vendor,
              requirement_id: formData["....."].requirement_id,
              my_company: formData["....."] && formData["....."].my_company,
              voyage_no: formData["....."] && formData["....."].voyage_no,
              vessel: formData["....."] && formData["....."].vessel_id,
              port: formData["....."] && formData["....."].port,
              po_number: formData["....."] && formData["....."].po_number,
              brocker: formData["....."] && formData["....."].brocker,
              invoice_no: formData["-----"] && formData["-----"].invoice_no,
              invoice_status:
                formData["-----"] && formData["-----"].invoice_status,
              invoice_date: formData["-----"] && formData["-----"].invoice_date,
              exch_rate: 11, //formData['-----']&&formData['-----'].exch_rate,
              // currency:'USD',
              //"--------": arrayData,
              "-": {
                // "grand_total":'',
                // "gst":''
              },
            };
            //}

            setState(prevState => ({
              ...prevState,
              formData: _formData,

              //frmOptions: [{ "key": "remittance_bank", "data": data }]
            }));

          }
        }
      }
    }
    fetchData();
  }, [])

  // const showHideModal = (visible, modal) => {
  //   if (visible) {
  //     const { modals } = state;
  //     let _modal = {};
  //     _modal[modal] = visible;

  //     setState(prevState => ({
  //       ...prevState,
  //       TdeList: null,
  //       modals: Object.assign(modals, _modal),
  //     }));
  //   }
  // };



  const showHideModal = (visible, modal) => {
    // if (visible) {
    //   const { modals } = state;
    //   let _modal = {};
    //   _modal[modal] = visible;

    //   setState(prevState => ({
    //     ...prevState,
    //     TdeList: null,
    //     modals: Object.assign(modals, _modal),
    //   }));
    // }

    const { modals } = state;
    let _modal = {};
    if (visible) {
      _modal[modal] = visible;
    }
    else {
      _modal = {
        InvoiceModal: false,
        TdeShow: false,
        BunkerReport: false,
        InvoicePopup: false,
      };

      _modal[modal] = visible;
    }

    setState(prevState => ({
      ...prevState,
      TdeList: null,
      modals: Object.assign(modals, _modal),
    }));
  };



  const tdeModalOpen = async (invoice_no, modal) => {
    const { formData, modelVisible } = state;
    let tde_id = 0;

    try {
      if (invoice_no) {
        let _modal = {};
        _modal[modal] = true;
        let voyageData = null;
        let voyage_manager_id = formData.voyage_no;
        if (voyage_manager_id) {
          const request = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/edit?ae=${voyage_manager_id}`);
          voyageData = await request["data"];
        }

        const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
        const tdeList = response["data"];
        let TdeList = tdeList && tdeList.length > 0 ? tdeList.filter((el) => invoice_no === el.invoice_no) : [];

        if (TdeList && TdeList.length > 0) {
          tde_id = TdeList[0]["id"];
        }
        if (tde_id !== 0) {
          const editData = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${tde_id}`);
          const tdeEditData = await editData["data"];

          let rem_data = tdeEditData["----"][0];
          tdeEditData["----"] = { total_due: rem_data["total_due"], total: rem_data["total"], remittance_bank: rem_data["remittance_bank"], };
          if (formData["...."] && formData["...."].length > 0) {
            tdeEditData["accounting"] = [];
            formData["...."].map((e, index) => {
              tdeEditData["accounting"].push({
                vessel_name: tdeEditData["vessel"],
                voyage: voyageData["voyage_number"],
                ap_ar_acct: tdeEditData["ar_pr_account_no"],
                vessel_code: voyageData["vessel_code"],
                company: tdeEditData["bill_via"],
                amount: e.invoice_total,
                description: `bunker-invoive @ ${e.invoice_total}`,
                lob: voyageData && voyageData.hasOwnProperty("company_lob") ? voyageData["company_lob"] : undefined,
                port: voyageData["b_port_name"],
                id: -9e6 + index,
              });
              return true;
            });
          }

          setState(prevState => ({ ...prevState, TdeList: tdeEditData, modals: _modal, }));

        } else {
          let frmData = {};

          const responseData = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${voyageData.my_company_lob}`);
          const responseAddressData = responseData["data"];
          let account_no = responseAddressData && responseAddressData["bank&accountdetails"] && responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["account_no"]
            : "";

          let swift_code =
            responseAddressData && responseAddressData["bank&accountdetails"] && responseAddressData["bank&accountdetails"].length > 0
              ? responseAddressData["bank&accountdetails"][0] &&
              responseAddressData["bank&accountdetails"][0]["swift_code"]
              : "";

          frmData = {
            bill_via: voyageData["my_company_lob"],
            invoice: formData["acc_type"],
            invoice_no: formData["invoice_no"],
            invoice_date: formData["invoice_date"],

            received_date: formData["recieved_date"],
            vessel: formData["vessel"],
            vendor: formData["vendor"],
            voyage_manager_id: voyageData["id"],
            // inv_status: formData["inv_status_name"],
            inv_status: formData["invoice_status"],
            invoice_amount: formData["grand_total"] || formData["-"] && formData["-"]["grand_total"],
            account_base: formData["grand_total"] || formData["-"] && formData["-"]["grand_total"],
            ar_pr_account_no: account_no,
            //voy_no: formData["voyage_no"],
            po_number: formData["po_number"],
            payment_term: formData["payment_terms"],
            accounting: [],
          };
          if (formData["...."] && formData["...."].length > 0) {
            formData["...."].map((e, index) => {
              frmData["accounting"].push({
                vessel_name: formData["vessel"],
                voyage: formData["voyage_no"],
                ap_ar_acct: account_no,
                vessel_code: voyageData["vessel_code"],
                company: voyageData["my_company_lob"],
                amount: e.invoice_total,
                account: swift_code,
                description: `bunker-invoive @ ${e.invoice_total}`,
                port: formData["port_name"],
                lob:
                  voyageData && voyageData.hasOwnProperty("company_lob")
                    ? voyageData["company_lob"]
                    : undefined,
                id: -9e6 + index,
              });
              return true;
            });
          }

          setState(prevState => ({ ...prevState, modals: _modal, TdeList: frmData }));
        }
      } else {

        setState(prevState => ({
          ...prevState,
          msg: `Without "Invoice No" you cant't access the TDE Form`,
          modelVisible: !modelVisible,
        }));

      }
    } catch (err) {
      // console.log(err);
      openNotificationWithIcon("error", "Something went wrong", 5);
    }
  };

  const saveFormData = async (data) => {

    setState(prevState => ({
      ...prevState,
      frmVisible: false
    }));

    if (
      data &&
      data.hasOwnProperty("-") &&
      data["-"].hasOwnProperty("invoice_total")
    )
      delete data["-"].invoice_total;
    const { frmName } = state;
    let url = "save";
    let _method = "post";
    if (data.hasOwnProperty("id") && data.id > 0) {
      url = "update";
      _method = "put";
    }
    formDatref = data;
    if (data) {
      let _url = `${URL_WITH_VERSION}/bunker-invoice/${url}?frm=${frmName}`;
      await postAPICall(`${_url}`, data, _method, (response) => {
        if (response && response.data === true) {
          openNotificationWithIcon("success", response.message, 5);

          setState(prevState => ({
            ...prevState,
            frmVisible: true,
            formData: response && response.row ? response.row : data,
          }));

          // setState({...state,frmVisible:true})
          // props.closeModale();
          setEditData(state.formData);

        } else if (response && response.data == false) {

          setState(prevState => ({ ...prevState, frmVisible: true, }));
          if (typeof response.message === "string") {
            openNotificationWithIcon("error", response.message);
          } else {
            openNotificationWithIcon("error", "Server Error");
          }
        }
      });
    }
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.hasOwnProperty("id") && postData["id"] > 0) {
      Modal.confirm({
        title: "Confirm",
        content: "Are you sure, you want to delete it?",
        onOk: () => _onDelete(postData),
      });
    } else {
      openNotificationWithIcon("error", "Please generate Invoice No!");
    }
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/bunker-invoice/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        props.closeModale();
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const invoiceModal = async (data = {}) => {
    let { formData, invoiceReport } = state;
    try {
      if (Object.keys(data).length == 0) {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/bunker-invoice/report?ae=${formData.requirement_id
          }`
        );
        const respData = await response["data"];
        if (respData) {

          setState(prevState => ({
            ...prevState,
            invoiceReport: respData,
            popupdata: respData,
          }));

          showHideModal(true, "InvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry,Unable to find invoice.", 3);
        }
      } else {

        setState(prevState => ({
          ...prevState,
          invoiceReport: { ...invoiceReport, ...data },
        }));

      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  const handleok = () => {
    const { invoiceReport } = state;
    if (invoiceReport["isSaved"]) {
      showHideModal(false, "InvoicePopup");
      setTimeout(() => {
        showHideModal(true, "InvoiceModal");
      }, 2000);

      setState(prevState => ({
        ...prevState,
        invoiceReport: invoiceReport,
      }));
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "....") {
      groupKey = "....";
      frm_code = "bunker_invoice";
    }

    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
        key: data.key,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "bunker-invoice");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "bunker-invoice")
          }
          tableId={0}
        />
      )
      setState(prevState => ({
        ...prevState,
        isShowAttachment: isShowAttachment, loadComponent: loadComponent
      }));
    } else {

      setState(prevState => ({
        ...prevState,
        isShowAttachment: isShowAttachment, loadComponent: undefined
      }));
    }
  }

  const {
    frmName,
    formData,
    frmOptions,
    modelVisible,
    msg,
    TdeList,
    invoiceReport,
    frmVisible,
    VoyID,
    popupdata,
    isShowAttachment,
    isRemarkModel
  } = state;

  const handleRemark = () => {
    setState(prevState => ({
      ...prevState,
      isRemarkModel: true,
    }));
  }

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                      <div className="box-body">
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            frmOptions={frmOptions}
                            showToolbar={[
                              {
                                isLeftBtn: [
                                  {
                                    isSets: [
                                      {
                                        id: "3",
                                        key: "save",
                                        type: <SaveOutlined />,
                                        withText: "Save",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          saveFormData(data),
                                      },
                                      {
                                        id: "4",
                                        key: "delete",
                                        type: <DeleteOutlined />,
                                        withText: "Delete",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          _onDeleteFormData(data),
                                      },
                                      {
                                        id: "20",
                                        key: "refresh",
                                        type: <SyncOutlined />,
                                        withText: "Refresh",
                                        showToolTip: true,
                                        event: () => {
                                          reFreshForm()
                                        },
                                      },
                                      {
                                        id: "21",
                                        key: "edit",
                                        type: <EditOutlined />,
                                        withText: "Remark",
                                        showToolTip: true,
                                        event: (key, data) => {

                                          if (data?.row?.id || data?.id > 0) {
                                            handleRemark(data);
                                          } else {
                                            openNotificationWithIcon(
                                              "info",
                                              "Please save Invoice first",
                                              2
                                            );
                                          }
                                        },
                                      }
                                    ],
                                  },
                                ],
                                isRightBtn: [
                                  {
                                    isSets: [
                                      {
                                        key: "invoice",
                                        isDropdown: 0,
                                        withText: "Create Invoice",
                                        type: "",
                                        menus: null,
                                        event: (key) => invoiceModal(),
                                      },
                                      {
                                        key: "attachment",
                                        isDropdown: 0,
                                        withText: "Attachment",
                                        type: "",
                                        menus: null,
                                        event: (key, data) => {
                                          data &&
                                            data.hasOwnProperty("id") &&
                                            data["id"] > 0
                                            ? ShowAttachment(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Please save Bunker Invoice First.",
                                              3
                                            );
                                        }
                                      },

                                      {
                                        key: "tde",
                                        isDropdown: 0,
                                        withText: "TDE",
                                        type: "",
                                        menus: null,
                                        event: (key) => {
                                          //formData && formData["invoice_no"] ? tdeModalOpen(formData["invoice_no"],"TdeShow")
                                          // : openNotificationWithIcon("error","Something went wrong.",5),
                                          if (formData?.invoice_no) {
                                            showHideModal(true, "TdeShow");
                                          } else {
                                            openNotificationWithIcon("error", "Something went wrong.", 5)
                                          }
                                        }
                                      },
                                    ],
                                  },
                                ],
                              },
                            ]}
                            inlineLayout={true}
                            isShowFixedColumn={"...."}
                            // showSideListBar={null}
                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                          />
                        ) : (
                          undefined
                        )}
                      </div>
                    </div>{" "}
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
      {invoiceReport && (
        <Modal
          style={{ top: "2%" }}
          title="Bunker Invoice"
          open={state.modals["InvoiceModal"]}
          onCancel={() => showHideModal(false, "InvoiceModal")}
          width="95%"
          footer={null}
        >
          <CreateInvoice type="bunkerInvoice" bunkerInvoice={invoiceReport} />
        </Modal>
      )}
      {state.modals["TdeShow"] && (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="TDE"
          open={state.modals["TdeShow"]}
          onCancel={() => showHideModal(false, "TdeShow")}
          width="95%"
          footer={null}
        >
          <Tde
            invoiceType="bunker-invoice"
            // VoyID={TdeList}
            // isEdit={TdeList != null && TdeList.id && TdeList.id > 0 ? true : false}
            // formData={TdeList}
            // modalCloseEvent={() => showHideModal(false, "TdeShow")}
            // saveUpdateClose={() => showHideModal(false, "TdeShow")}
            formData={formData}
            invoiceNo={formData.invoice_no}
          />
        </Modal>
      )}

      <Modal
        title="Alert"
        open={modelVisible}

        onOk={() => setState(prevState => ({
          ...prevState,
          isShowAttachment: isShowAttachment, loadComponent: undefined
        }))}

        onCancel={() => setState(prevState => ({
          ...prevState,
          modelVisible: !modelVisible
        }))}
      >
        <p>{msg}</p>
      </Modal>
      {state.modals["InvoicePopup"] ? (
        <Modal
          style={{ top: "2%" }}
          title="Invoice"
          open={state.modals["InvoicePopup"]}
          onCancel={() => showHideModal(false, "InvoicePopup")}
          width="95%"
          okText="Create PDF"
          onOk={handleok}
        >
          <InvoicePopup
            data={popupdata}
            updatepopup={(data) => invoiceModal(data)}
          />
        </Modal>
      ) : (
        undefined
      )}
      {isShowAttachment ? (
        <Modal
          style={{ top: "2%" }}
          title="Upload Attachment"
          open={isShowAttachment}
          onCancel={() =>
            ShowAttachment(false)
          }
          width="50%"
          footer={null}
        >
          {state.loadComponent}
        </Modal>) : (
        undefined)}

      {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() => setState(prevState => ({ ...prevState, isRemarkModel: false }))}
          footer={false}
        >
          <Remarks
            remarksID={props.formData.invoice_no}
            remarkType="bunker-invoice"
          // idType="Bunker_no"
          />


        </Modal>
      )}

    </div>
  );
}

export default BunkerInvoice;
