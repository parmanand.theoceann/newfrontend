import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'INV ID',
    dataIndex: 'invid',
  },

  {
    title: 'INV Child ID',
    dataIndex: 'invchildid',
  },

  {
    title: 'INV Ammount',
    dataIndex: 'invammount',
  },

  {
    title: 'Requiemnt',
    dataIndex: 'requirement',
  },

  {
    title: 'PO No.',
    dataIndex: 'ponumber',
  },

  {
    title: 'Port',
    dataIndex: 'port',
  },

  {
    title: 'Vendor',
    dataIndex: 'vendor',
  },

  {
    title: 'Status',
    dataIndex: 'status',
  },

  {
    title: 'Due Date',
    dataIndex: 'duedate',
  },
];
const data = [
  {
    key: '1',
    invid: 'INV ID',
    invchildid: 'INV Child ID',
    invammount: 'INV Ammount',
    requirement: 'Requirement',
    ponumber: 'PO No',
    port: 'Port',
    vendor: 'Vendor',
    status: 'Status',
    duedate: 'Due Date',
  },
  {
    key: '2',
    invid: 'INV ID',
    invchildid: 'INV Child ID',
    invammount: 'INV Ammount',
    requirement: 'Requirement',
    ponumber: 'PO No',
    port: 'Port',
    vendor: 'Vendor',
    status: 'Status',
    duedate: 'Due Date',
  },

  {
    key: '3',
    invid: 'INV ID',
    invchildid: 'INV Child ID',
    invammount: 'INV Ammount',
    requirement: 'Requirement',
    ponumber: 'PO No',
    port: 'Port',
    vendor: 'Vendor',
    status: 'Status',
    duedate: 'Due Date',
  },

  {
    key: '4',
    invid: 'INV ID',
    invchildid: 'INV Child ID',
    invammount: 'INV Ammount',
    requirement: 'Requirement',
    ponumber: 'PO No',
    port: 'Port',
    vendor: 'Vendor',
    status: 'Status',
    duedate: 'Due Date',
  },
];

class BukerInvoiceTab extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Table
                bordered
                columns={columns}
                dataSource={data}
                pagination={false}
                scroll={{ x: 1500 }}
                footer={() => (
                  <div className="text-center">
                    <Button type="link">Add New</Button>
                  </div>
                )}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default BukerInvoiceTab;
