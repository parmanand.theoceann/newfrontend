import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Type',
    dataIndex: 'type',
  },

  {
    title: 'Grade',
    dataIndex: 'grade',
  },

  {
    title: 'Sulphar %',
    dataIndex: 'sulpher',

  },

  {
    title: 'Order Qty',
    dataIndex: 'orderqty',
  },

  {
    title: 'Reced Qty',
    dataIndex: 'operqty',
  },

  {
    title: 'Fuel Cost',
    dataIndex: 'fuelcost',
    width: 100,
  },

  {
    title: 'Barge Rate',
    dataIndex: 'bargerate',
    width: 100,
  },

  {
    title: 'Barge Cost',
    dataIndex: 'bargecost',
    width: 100,
  },

  {
    title: 'Other Costs',
    dataIndex: 'othercost',
    width: 100,
  },
  {
    title: 'Sales Tax',
    dataIndex: 'salestax',
    width: 100,
  },

  {
    title: 'Net Cost',
    dataIndex: 'netcost',
    width: 80,
  },

  {
    title: 'Total Grid Cost (USD)',
    dataIndex: 'totalgc',
    width: 200,
  },
];
const data = [
  {
    key: '1',
    type: 'Type',
    grade: 'Grade',
    sulpher: 'Sulphar %',
    orderqty: 'Order Qty',
    operqty: 'Reced Qty',
    fuelcost: 'Fuel Cost',
    bargerate: 'Barge Rate',
    bargecost: 'Barge Cost',
    othercost: 'Other Costs',
    salestax: 'Sales Tax',
    netcost: 'Net Cost',
    totalgc: 'Total Grid Cost ( USD )',
  },

  {
    key: '2',
    type: 'Type',
    grade: 'Grade',
    sulpher: 'Sulphar %',
    orderqty: 'Order Qty',
    operqty: 'Reced Qty',
    fuelcost: 'Fuel Cost',
    bargerate: 'Barge Rate',
    bargecost: 'Barge Cost',
    othercost: 'Other Costs',
    salestax: 'Sales Tax',
    netcost: 'Net Cost',
    totalgc: 'Total Grid Cost ( USD )',
  },
];

class OrderList extends Component {
  render() {
    return (
      <Table
        bordered
        columns={columns}
        dataSource={data}
        pagination={false}
        scroll={{ x: 1500 }}
        footer={() => (
          <div className="text-center">
            <Button type="link">Add New</Button>
          </div>
        )}
      />
    );
  }
}

export default OrderList;
