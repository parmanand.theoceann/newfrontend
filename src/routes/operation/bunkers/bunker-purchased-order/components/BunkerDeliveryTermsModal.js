import React, { Component } from 'react';
import { Form, Input, Button, Switch, Icon } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;


class BunkerDeliveryTermsModal extends Component {
  render() {
    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <Form>
                  <div className="row">
                    
                    <div className="col-md-12">

                    <FormItem label="Delivery Terms">
                        <InputGroup compact>
                          <Input size="default" />
                        </InputGroup>
                      </FormItem>

                      <FormItem >
                        <div className="row">
                          <div className="col-md-6">
                            <Switch
                              size="small" className="mr-2"
                              checkedChildren={<CheckOutlined />}
                              unCheckedChildren={<CloseOutlined />}
                            />
                            Density Claim
                          </div>
                          <div className="col-md-6">
                            <Switch
                              size="small" className="mr-2"
                              checkedChildren={<CheckOutlined />}
                              unCheckedChildren={<CloseOutlined />}
                            />
                            Shortage Claim
                          </div>
                        </div>
                      </FormItem>

                      <FormItem >
                        <div className="row">
                          <div className="col-md-6">
                            <Switch
                              size="small" className="mr-2"
                              checkedChildren={<CheckOutlined />}
                              unCheckedChildren={<CloseOutlined />}
                            />
                            Offspec Claim
                          </div>
                          <div className="col-md-6">
                            <Switch
                              size="small" className="mr-2"
                              checkedChildren={<CheckOutlined />}
                              unCheckedChildren={<CloseOutlined />}
                            />
                            Demmurrage Claim
                          </div>
                        </div>
                      </FormItem>

                      

                      <FormItem label="Claim Amt Curr/Base">
                        <InputGroup compact>
                          <Input style={{ width: '50%' }} defaultValue="0.00"/>
                          <Input style={{ width: '50%' }} defaultValue="0.00"/>
                          
                        </InputGroup>
                      </FormItem>

                      <FormItem label="Claim Detail">
                        <InputGroup>
                        <Input size="default"  />
                        </InputGroup>
                      </FormItem>

                      
                    </div>
                  </div>

                  <div className="action-btn text-right mt-3">
                  <Button className="ant-btn ant-btn-primary mr-2">Submit</Button>
                  <Button className="ant-btn ant-btn-danger">Cancel</Button>
                </div>
                </Form>
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }
}

export default BunkerDeliveryTermsModal;
