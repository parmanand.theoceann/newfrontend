import React, { Component } from 'react';
import { Form, Input, DatePicker, Switch, Icon } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class BunkerPurchageTabForm extends Component {
  render() {
    return (
      <>
        <Form>
          <div className="row">
            <div className="col-md-4">
              <FormItem {...formItemLayout} label="Fee Currency">
                <InputGroup compact>
                  <Input size="default" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Ordered Qty">
                <InputGroup compact>
                  <Input size="default" defaultValue="1,000.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Min/Max Qty">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} />
                  <Input style={{ width: '50%' }} />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Invoiced Qty">
                <InputGroup compact>
                  <Input size="default" defaultValue="1,000.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Net Fuil Cost/Curr">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.00" />
                  <Input style={{ width: '50%' }} defaultValue="0.00" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Barge Name">
                <InputGroup compact>
                  <Input size="default" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Barge Arrival">
                <InputGroup compact>
                  <DatePicker />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Barge Departure">
                <InputGroup compact>
                  <DatePicker />
                </InputGroup>
              </FormItem>
            </div>

            <div className="col-md-4">
              <FormItem {...formItemLayout} label="BW Spot Price/Curr">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="BW Cntr Price/Curr">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Receive Qty">
                <InputGroup compact>
                  <Input size="default" defaultValue="0.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Bunkering Start">
                <InputGroup compact>
                  <DatePicker />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Bunkering End">
                <InputGroup compact>
                  <DatePicker />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Bunkering Time (mins)">
                <InputGroup compact>
                  <Input size="default" type="time" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Bunkering Rate (MT/hr)">
                <InputGroup compact>
                  <Input size="default" />
                </InputGroup>
              </FormItem>
            </div>

            <div className="col-md-4">
              <FormItem {...formItemLayout} label="Test">
                <div className="row">
                  <div className="col-md-6">
                    <Switch
                      size="small" className="mr-2"
                      checkedChildren={<CheckOutlined />}
                      unCheckedChildren={<CloseOutlined />}
                    />
                    Pre
                  </div>
                  <div className="col-md-6">
                    <Switch
                      size="small" className="mr-2"
                      checkedChildren={<CheckOutlined />}
                      unCheckedChildren={<CloseOutlined />}
                    />
                    Post
                  </div>
                </div>
              </FormItem>

              <FormItem {...formItemLayout} label="Test Days">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0" />
                  <Input style={{ width: '50%' }} defaultValue="0" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Test Due">
                <InputGroup compact>
                  <DatePicker style={{ width: '50%' }} />
                  <DatePicker style={{ width: '50%' }} />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Test Received">
                <InputGroup compact>
                  <DatePicker style={{ width: '50%' }} />
                  <DatePicker style={{ width: '50%' }} />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Density">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Diff Density">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                  <Input style={{ width: '50%' }} defaultValue="0.000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Diff Ammount">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.00" />
                  <Input style={{ width: '50%' }} defaultValue="0.00" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Sulfur %">
                <InputGroup compact>
                  <Input style={{ width: '50%' }} defaultValue="0.0000" />
                  <Input style={{ width: '50%' }} defaultValue="0.0000" />
                </InputGroup>
              </FormItem>

              <FormItem {...formItemLayout} label="Inspector">
                <InputGroup compact>
                  <Input size="default" />
                </InputGroup>
              </FormItem>
            </div>
          </div>
        </Form>
      </>
    );
  }
}

export default BunkerPurchageTabForm;
