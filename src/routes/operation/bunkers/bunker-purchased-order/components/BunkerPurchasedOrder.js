import React, { useEffect } from "react";
import { Button, Modal } from "antd";
import URL_WITH_VERSION, {
  postAPICall,
  getAPICall,
  apiDeleteCall,
  openNotificationWithIcon,
  useStateCallback,
} from "../../../../../shared";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import BunkerInvoice from "../../../../../routes/operation/bunkers/bunker-invoice/index";
import BunkerPurchasedReport from "../../../../operation-reports/BunkerPurchasedReport";
import Remarks from "../../../../../shared/components/Remarks";
import { DeleteOutlined, EditOutlined, SaveOutlined } from "@ant-design/icons";

const _formData = { id: 0 };

const BunkerPurchasedOrder = (props) => {
  const [state, setState] = useStateCallback({
    frmName: "bunker_purchased_order_form",
    responseData: props.formData || {},
    formData: Object.assign(_formData, props.formData || {}),
    formReportdata: Object.assign(_formData, props.reportFormData || {}),
    visibleSummary: false,
    visibleMakePayment: false,
    paymentData: {},
    commissionEntry: {},
    visiblehirePayment: false,
    hmPay: {},
    popupFroms: props.popupFroms || {},
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    isShowSearchTci: false,
    isShowSearchTco: false,
    requirement_id: props.formData && props.formData.id,
    isDoSchedule: false,
    rightMenuButtons: [],
    frmVisible: true,
    invoceModal: false,
    isRemarkModel: false,
    showSideListBar:
      props.showSideListBar === false ? props.showSideListBar : true,
    selectedListData: null,

  });

  useEffect(() => {
    getFormData();
  }, []);

  const getFormData = async () => {
    const { formData } = props;


    let _formData = {},
      ifo = {},
      ulsfo = {},
      lsg = {};
    setState((prevState) => ({ ...prevState, frmVisible: false }));

    if (
      formData &&
      formData.hasOwnProperty("-----") &&
      formData.hasOwnProperty(".....")
    ) {
      if (formData && formData.hasOwnProperty("--------")) {
        formData["--------"].length > 0 &&
          formData["--------"].map((val) => {
            if (val.fuel_type == "3") {
              ifo = {
                sulphur1: val.sulphur,
                recieved_qty: val.recieved_qty,
                ordered_qty: val.order_qty,
                net_fuel_cost: val.fuel_cost,
              };
            }
            if (val.fuel_type == "10") {
              ulsfo = {
                sulphur1: val.sulphur,
                recieved_qty: val.recieved_qty,
                ordered_qty: val.order_qty,
                net_fuel_cost: val.fuel_cost,
              };
            }
            if (val.fuel_type == "6") {
              lsg = {
                sulphur1: val.sulphur,
                recieved_qty: val.recieved_qty,
                ordered_qty: val.order_qty,
                net_fuel_cost: val.fuel_cost,
              };
            }
          });
      }
      _formData = formData;
      if (ifo && ifo.hasOwnProperty("recieved_qty")) _formData["."] = ifo;
      if (ulsfo && ulsfo.hasOwnProperty("recieved_qty"))
        _formData["......"] = ulsfo;
      if (lsg && lsg.hasOwnProperty("recieved_qty")) _formData[".."] = lsg;
      // _formData['...'] = {
      //   fuel_type: 'fuel_type',
      //   grade: 'grade',
      //   sulphur: 'sulphur',
      //   order_qty: 'order_qty',
      //   recieved_qty: 'recieved_qty',
      //   fuel_cost: 'fuel_cost',
      // }
      // const response1 = await getAPICall(`${URL_WITH_VERSION}/bunker-invoice/edit?e=${formData['.....'] && formData['.....'].requirement_id}`);
      // let respData = response1['data']
      // if (respData && respData.hasOwnProperty['invoice_no'] && respData.hasOwnProperty['....']) {
      //   _formData['-------'] = respData['....']
      // }
      // console.log("resp..._formData", _formData)
    } else {
      let plannedliftings = [];
      props.formData &&
        props.formData["plannedliftings"] &&
        props.formData["plannedliftings"].length > 0 &&
        props.formData["plannedliftings"].map((val, ind) => {
          if (val.fuel_type == "3") {
            ifo = {
              sulphur1: val.sulphur,
              recieved_qty: val.recieved_qty,
              ordered_qty: val.bnd_qty,
              net_fuel_cost: 0,
            };
          }
          if (val.fuel_type == "10") {
            ulsfo = {
              sulphur1: val.sulphur,
              recieved_qty: val.recieved_qty,
              ordered_qty: val.bnd_qty,
              net_fuel_cost: 0,
            };
          }
          if (val.fuel_type == "6") {
            lsg = {
              sulphur1: val.sulphur,
              recieved_qty: val.recieved_qty,
              ordered_qty: val.bnd_qty,
              net_fuel_cost: 0,
            };
          }

          plannedliftings.push({
            fuel_type: val.fuel_type,
            grade: val.grade,
            sulphur: val.sulphur,
            order_qty: val.bnd_qty,
            recieved_qty: val.recieved_qty,
            fuel_cost: 0,
            barge_rate: 0,
            barge_cost: 0,
            other_cost: 0,
            sales_tax: 0,
            net_cost: 0,
            total_grid_cost: 0,
            editable: true,
            id: val.id,
          });
        });


      _formData = {
        ".....": {
          vessel_id: props.formData && props.formData["vessel"],
          requirement_id: props.formData && props.formData["requirement_id"],
          agent: props.formData && props.formData["agent"],
          port: props.formData && props.formData["port"],
          voyage_no: props.formData && props.formData["voyage_no"],
          etb: props.formData && props.formData["eta"],
          tci_owner: props.formData && props.formData["tci_owner"],
          charterer: props.formData && props.formData["charterer"],
          my_company: props.formData && props.formData["my_company"],
          delivary_from: props.formData && props.formData["delivary_from"],
          delivary_to: props.formData && props.formData["delivary_to"],
          request_status: props.formData && props.formData["request_status"],
          voyage_manager_id:
            props.formData && props.formData["voyage_manager_id"],
        },
        "--------": plannedliftings,
      };
      if (ifo && ifo.hasOwnProperty("recieved_qty")) _formData["."] = ifo;
      if (ulsfo && ulsfo.hasOwnProperty("recieved_qty"))
        _formData["......"] = ulsfo;
      if (lsg && lsg.hasOwnProperty("recieved_qty")) _formData[".."] = lsg;
    }
    setState((prevState) => ({
      ...prevState,
      formData:
        _formData /*frmOptions: [ {"key": "port", "data": portOptions} ]*/,
      frmVisible: true,
    }));

  };

  const openBunkerPurchasedReport = async (
    showBunkerPurchasedReport,
    id = null
  ) => {
    if (showBunkerPurchasedReport == true && id) {
      try {
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/bunker-pur/report?e=${id}`
        );
        const respDataReport = await responseReport["data"];

        if (respDataReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowBunkerPurchasedReport: showBunkerPurchasedReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (error) {
        openNotificationWithIcon("error", "Something Went Wrong", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowBunkerPurchasedReport: showBunkerPurchasedReport,
      }));
    }
  };

  const setFormData = async (id) => {
    let _formData = {},
      ifo = {},
      ulsfo = {},
      lsg = {};
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    try {
      let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/edit?e=${id}`;
      const response = await getAPICall(_url);
      const respData = await response["data"];
      if (
        respData &&
        respData.hasOwnProperty("-----") &&
        respData.hasOwnProperty(".....")
      ) {
        if (respData && respData.hasOwnProperty("--------")) {
          respData["--------"].length > 0 &&
            respData["--------"].map((val) => {
              if (val.fuel_type == "3") {
                ifo = {
                  sulphur1: val.sulphur,
                  recieved_qty: val.recieved_qty,
                  ordered_qty: val.order_qty,
                  net_fuel_cost: val.fuel_cost,
                };
              }
              if (val.fuel_type == "10") {
                ulsfo = {
                  sulphur1: val.sulphur,
                  recieved_qty: val.recieved_qty,
                  ordered_qty: val.order_qty,
                  net_fuel_cost: val.fuel_cost,
                };
              }
              if (val.fuel_type == "6") {
                lsg = {
                  sulphur1: val.sulphur,
                  recieved_qty: val.recieved_qty,
                  ordered_qty: val.order_qty,
                  net_fuel_cost: val.fuel_cost,
                };
              }
            });
        }
        _formData = respData;
        if (ifo && ifo.hasOwnProperty("recieved_qty")) _formData["."] = ifo;
        if (ulsfo && ulsfo.hasOwnProperty("recieved_qty"))
          _formData["......"] = ulsfo;
        if (lsg && lsg.hasOwnProperty("recieved_qty")) _formData[".."] = lsg;
      }
      setState((prevState) => ({
        ...prevState,
        formData: _formData,
        responseData: respData,
        frmVisible: true,
      }));
    } catch (error) {
      openNotificationWithIcon("error", "something went wrong", 2);
      setState((prevState) => ({ ...prevState, frmVisible: true }));
    }
  };

  const saveFormData = async (data) => {
    const { frmName } = state;
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let url = "save";
    let _method = "post";
    if (data.hasOwnProperty("id") && data.id > 0) {
      url = "update";
      _method = "put";
    }
    if (data) {

      let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/${url}?frm=${frmName}`;
      await postAPICall(`${_url}`, data, `${_method}`, (response) => {
        if (response && response.data == true) {
          setState((prevState) => ({
            ...prevState,
            frmVisible: true,
            formData: response && response.row ? response.row : data,
          }));
          openNotificationWithIcon("success", response.message, 2);
          if (url === "save") {
            window.emitNotification({
              n_type: "PO Created",
              msg: window.notificationMessageCorrector(`PO for Bunker, Req(${data["....."]["requirement_id"]}) is added, for Voyage(${data["....."]["voyage_no"]}), by ${window.userName}`),
            });
          } else {
            window.emitNotification({
              n_type: "PO Updated",
              msg: window.notificationMessageCorrector(`PO for Bunker, Req(${data["....."]["requirement_id"]}) is updated, for Voyage(${data["....."]["requirement_id"]}), by ${window.userName}`),
            });
          }

          if (props.hasOwnProperty("modalCloseEvent")) {
            setTimeout(() => {
              props.modalCloseEvent();
            }, 3000);
          } else if (response.row && response.row.id) {
            setFormData(response.row.id);
          }
        } else {
          openNotificationWithIcon("error", response.message);
          setState((prevState) => ({
            ...prevState,
            frmVisible: true,
            formData: data,
          }));
        }
      });
    }
  };

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {};
    _modal[modal] = visible;
    setState((prevState) => ({
      ...prevState,
      modals: Object.assign(modals, _modal),
    }));
  };

  const checkBunkerInvoice = async (st, requirement_id) => {
    const { responseData } = state;
    let _url = `${URL_WITH_VERSION}/bunker-invoice/list`;
    const response = await getAPICall(_url);
    const data = await response["data"];
    if (data && data.length > 0) {
      let filterData = data.filter((el) => requirement_id == el.requirement_id);
      if (filterData && filterData.length > 0) {
        setState((prevState) => ({
          ...prevState,
          selectedListData: filterData[0],
          invoceModal: true,
          isEdit: true,
        }));
      } else {
        setState((prevState) => ({
          ...prevState,
          selectedListData: responseData,
          invoceModal: true,
          isEdit: false,
        }));
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        selectedListData: responseData,
        invoceModal: true,
        isEdit: false,
      }));
    }
  };

  const InvoiceModal = async (st) => {
    setState((prevState) => ({ ...prevState, invoceModal: st }));
  };

  const syncData = async () => {
    const { formData } = props;
    let data = { ...state.formData },
      plannedliftings = [];
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    if (
      formData &&
      formData.hasOwnProperty(".....") &&
      formData["....."].hasOwnProperty("requirement_id")
    ) {
      let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/edit?r=${formData["....."]["requirement_id"]}`;
      const response = await getAPICall(_url);
      const respData = await response["data"];
      respData &&
        respData["plannedliftings"] &&
        respData["plannedliftings"].length > 0 &&
        respData["plannedliftings"].map((val, ind) => {
          plannedliftings.push({
            fuel_type: val.fuel_type,
            grade: val.grade,
            sulphur: val.sulphur,
            order_qty: val.bnd_qty,
            recieved_qty: val.recieved_qty,
            fuel_cost: 0,
            barge_rate: 0,
            barge_cost: 0,
            other_cost: 0,
            sales_tax: 0,
            net_cost: 0,
            total_grid_cost: 0,
            editable: true,
            id: val.id,
          });
        });
      data["--------"] = plannedliftings;
    } else {
      if (formData && formData.hasOwnProperty("requirement_id")) {
        let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/edit?r=${formData["requirement_id"]}`;
        const response = await getAPICall(_url);
        const respData = await response["data"];
        respData &&
          respData["plannedliftings"] &&
          respData["plannedliftings"].length > 0 &&
          respData["plannedliftings"].map((val, ind) => {
            plannedliftings.push({
              fuel_type: val.fuel_type,
              grade: val.grade,
              sulphur: val.sulphur,
              order_qty: val.bnd_qty,
              recieved_qty: val.recieved_qty,
              fuel_cost: 0,
              barge_rate: 0,
              barge_cost: 0,
              other_cost: 0,
              sales_tax: 0,
              net_cost: 0,
              total_grid_cost: 0,
              editable: true,
              id: val.id,
            });
          });
        data["--------"] = plannedliftings;
      }
    }

    setState((prevState) => ({
      ...prevState,
      formData: data,
      frmVisible: true,
    }));
  };

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "--------") {
      groupKey = "--------";
      frm_code = "tab_bunker_purchased_order_form";
    }

    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
        key: data.key,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const _onDeleteFormData = (postData) => {

    if (postData && postData.id === undefined) {
      openNotificationWithIcon(
        "error",
        "Cannot Delete Bunker Purchased Order Before Creation"
      );
      return;
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        window.emitNotification({
          n_type: "PO Deleted",
          msg: window.notificationMessageCorrector(`PO for Bunker requirement Id(${postData["....."]["requirement_id"]}) is deleted, for Voyage(${postData["....."]["voyage_no"]}), by ${window.userName}`),
        });
        setState(
          (prevState) => ({ ...prevState, frmVisible: false }),
          (prevState) => ({
            ...prevState,
            formData: _formData,
            frmVisible: true,
          })
        );

        if (
          props.modalCloseEvent &&
          typeof props.modalCloseEvent === "function"
        ) {
          props.modalCloseEvent();
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const {
    frmName,
    formData,
    invoceModal,
    frmVisible,
    isShowBunkerPurchasedReport,
    reportFormData,
    selectedListData,
    isRemarkModel,
  } = state;

  const handleRemark = () => {
    setState((prevState) => ({
      ...prevState,
      isRemarkModel: true,
    }));
  };

  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            {frmVisible ? (
              <NormalFormIndex
                key={"key_" + frmName + "_0"}
                formClass="label-min-height"
                formData={formData}
                showForm={true}
                frmCode={frmName}
                addForm={true}
                showToolbar={[
                  {
                    isLeftBtn: [
                      {
                        key: "s1",
                        isSets: [
                          {
                            id: "1",
                            key: "save",
                            type: <SaveOutlined />,
                            withText: "Save",
                            showToolTip: true,
                            event: (key, data) => saveFormData(data),
                          },
                          formData &&
                          formData["id"] && {
                            id: "2",
                            key: "delete",
                            type: <DeleteOutlined />,
                            withText: "Delete",
                            showToolTip: true,
                            event: (key, data) => _onDeleteFormData(data),
                          },

                          {
                            id: "3",
                            key: "edit",
                            type: <EditOutlined />,
                            withText: "Remark",
                            showToolTip: true,
                            event: (key, data) => {

                              if (data?.row?.id || data?.id > 0) {
                                handleRemark(data);
                              } else {
                                openNotificationWithIcon(
                                  "info",
                                  "Please save Invoice first",
                                  2
                                );
                              }
                            },
                          },
                        ],
                      },
                    ],
                    isRightBtn: [
                      {
                        key: "s2",
                        isSets: [
                          {
                            key: "create_bunker_invoice",
                            isDropdown: 0,
                            withText: "Create Bunker Invoice",
                            type: "",
                            menus: null,
                            event: (key) =>
                              checkBunkerInvoice(
                                true,
                                formData["....."] &&
                                formData["....."].requirement_id
                              ),
                          },
                          {
                            key: "address_commission_invoice",
                            isDropdown: 0,
                            withText: "Address Commission Invoice",
                            type: "",
                            menus: null,
                          },
                          {
                            key: "report",
                            isDropdown: 0,
                            withText: "Report",
                            type: "",
                            menus: null,
                            event: (key, data) =>
                              data && data.id > 0
                                ? openBunkerPurchasedReport(true, data.id)
                                : openNotificationWithIcon(
                                  "info",
                                  "Create first then click on report!"
                                ),
                          },
                        ],
                      },
                    ],
                    isResetOption: false,
                  },
                ]}
                tabEvents={[
                  {
                    tabName: "IFO",
                    event: {
                      type: "copy",
                      from: "Order",
                      fields: {
                        fuel_type: "fuel_type",
                        grade: "grade",
                        sulphur: "sulphur",
                        order_qty: "order_qty",
                        recieved_qty: "recieved_qty",
                        fuel_cost: "fuel_cost",
                        barge_rate: "barge_rate",
                      },
                      group: { to: "...", from: "--------" },
                      condition: {
                        columnName: "fuel_type",
                        columnValue: "3",
                      },
                      showSingleIndex: true,
                    },
                  },
                ]}
                inlineLayout={true}
                tableRowDeleteAction={(action, data) =>
                  onClickExtraIcon(action, data)
                }
                isShowFixedColumn={["------", "-------", "--------"]}
              />
            ) : undefined}

            <span className="float-right">
              <Button type="primary" onClick={(e) => syncData()}>
                Sync Data
              </Button>
            </span>
          </div>
        </div>
      </article>

      {isShowBunkerPurchasedReport ? (
        <Modal
          style={{ top: "2%" }}
          title="Report"
          open={isShowBunkerPurchasedReport}
          onCancel={() => openBunkerPurchasedReport(false)}
          width="90%"
          footer={null}
          maskClosable={false}
        >
          <BunkerPurchasedReport data={reportFormData} />
        </Modal>
      ) : undefined}
      {invoceModal ? (
        <Modal
          style={{ top: "2%" }}
          title="Bunker Invoice"
          open={invoceModal}
          onCancel={() =>
            setState((prevState) => ({
              ...prevState,
              invoceModal: false,
              selectedListData: null,
            }))
          }
          width="90%"
          footer={null}
          maskClosable={false}
        >
          <BunkerInvoice
            closeModale={() => {
              setState((prevState) => ({
                ...prevState,
                invoceModal: false,
                selectedListData: null,
              }));
            }}
            isEdit={state.isEdit}
            formData={
              selectedListData != null ? selectedListData : props.formData

            }

          />

        </Modal>
      ) : undefined}

      {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isRemarkModel: false }))
          }
          footer={false}
        >
          <Remarks
            remarksId={formData.id}
            // remarksId={formData?.["....."]?.voyage_no}
            remarkType="Bunker"
            idType="Bunker_no"
          />
        </Modal>
      )}
    </div>
  );
};

export default BunkerPurchasedOrder;
