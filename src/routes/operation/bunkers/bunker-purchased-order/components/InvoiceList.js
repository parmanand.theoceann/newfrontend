import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Type',
    dataIndex: 'type',
  },

  {
    title: 'Grade',
    dataIndex: 'grade',
  },

  {
    title: 'Sulphar %',
    dataIndex: 'sulpher',
  },

  {
    title: 'Invoice Qty',
    dataIndex: 'invoiceqty',
  },

  {
    title: 'Reced Qty',
    dataIndex: 'operqty',
  },

  {
    title: 'Fuel Cost',
    dataIndex: 'fuelcost',
  },

  {
    title: 'Barge Rate',
    dataIndex: 'bargerate',
  },

  {
    title: 'Barge Cost',
    dataIndex: 'bargecost',
  },

  {
    title: 'Other Costs',
    dataIndex: 'othercost',
  },
  {
    title: 'Sales Tax',
    dataIndex: 'salestax',
  },

  {
    title: 'Net Cost',
    dataIndex: 'netcost',
  },

  {
    title: 'Port Charge',
    dataIndex: 'portcharge',
  },

  {
    title: 'Invoice Total',
    dataIndex: 'invoicetotal',
  },
];
const data = [
  {
    key: '1',
    type: 'Type',
    grade: 'Grade',
    sulpher: 'Sulphar %',
    invoiceqty: 'Invoice Qty',
    operqty: 'Reced Qty',
    fuelcost: 'Fuel Cost',
    bargerate: 'Barge Rate',
    bargecost: 'Barge Cost',
    othercost: 'Other Costs',
    salestax: 'Sales Tax',
    netcost: 'Net Cost',
    portcharge: 'Port Charge',
    invoicetotal: 'Invoice Total',
  },

  {
    key: '2',
    type: 'Type',
    grade: 'Grade',
    sulpher: 'Sulphar %',
    invoiceqty: 'Invoice Qty',
    operqty: 'Reced Qty',
    fuelcost: 'Fuel Cost',
    bargerate: 'Barge Rate',
    bargecost: 'Barge Cost',
    othercost: 'Other Costs',
    salestax: 'Sales Tax',
    netcost: 'Net Cost',
    portcharge: 'Port Charge',
    invoicetotal: 'Invoice Total',
  },
];

class InvoiceList extends Component {
  render() {
    return (
      <Table
        bordered
        columns={columns}
        dataSource={data}
        pagination={false}
        scroll={{ x: 1500 }}
        footer={() => (
          <div className="text-center">
            <Button type="link">Add New</Button>
          </div>
        )}
      />
    );
  }
}

export default InvoiceList;
