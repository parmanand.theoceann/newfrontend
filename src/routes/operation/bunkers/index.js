import React from "react";
import { Route } from "react-router-dom";
import BunkerInvoice from "./bunker-invoice";
import BunkerPlanningList from "./bunker-planning-list";
import BunkerPurchasedOrder from "./bunker-purchased-order";
import BunkerStatusList from "./bunker-status-list";
import MarketData from "./market-data";
import VendorsPerformanceAnalytics from "./vendors-performance-analytics";
import BunkerPlanning from "./bunker-planning";
import BunkerRequirements from "./bunker-requirements";

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/bunker-invoice`} component={BunkerInvoice} />
    <Route
      path={`${match.url}/bunker-planning-list`}
      component={BunkerPlanningList}
    />
    <Route
      path={`${match.url}/bunker-purchased-order`}
      component={BunkerPurchasedOrder}
    />
    <Route
      path={`${match.url}/bunker-status-list`}
      component={BunkerStatusList}
    />
    <Route path={`${match.url}/market-data`} component={MarketData} />
    <Route
      path={`${match.url}/vendors-performance-analytics`}
      component={VendorsPerformanceAnalytics}
    />
    <Route path={`${match.url}/bunker-planning`} component={BunkerPlanning} />
    <Route
      path={`${match.url}/bunker-requirements`}
      component={BunkerRequirements}
    />
  </div>
);

export default CardComponents;
