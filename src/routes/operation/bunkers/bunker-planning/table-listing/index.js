import React, { Component } from 'react';
import { Table, Popconfirm, Input,  Button } from 'antd';
import { SaveOutlined, EditOutlined,DeleteOutlined } from '@ant-design/icons';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        type: "Type",
        grade: "Grade",
        reqqty: "Req Qty",
        minqty: "Min Qty",
        maxqty: "Max Qty",
        oprqty: "Opr Qty",
        altqty: "Alt Qty",
        altunit: "Alt Unit",
        sulfur: "Sulfur",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class TableListing extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Type',
            dataIndex: 'type',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'type'),
        },
        {
            title: 'Grade',
            dataIndex: 'grade',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'grade'),
        },
        {
            title: 'Req Qty',
            dataIndex: 'reqqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'reqqty'),
        },
        {
            title: 'Min Qty',
            dataIndex: 'minqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'minqty'),
        },
        {
            title: 'Max Qty',
            dataIndex: 'maxqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'maxqty'),
        },
        {
            title: 'Opr Qty',
            dataIndex: 'oprqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'oprqty'),
        },
        {
            title: 'Alt Qty',
            dataIndex: 'altqty',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'altqty'),
        },
        {
            title: 'Alt Unit',
            dataIndex: 'altunit',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'altunit'),
        },
        {
            title: 'Sulfur',
            dataIndex: 'sulfur',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'sulfur'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined />
</span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                        <DeleteOutlined />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default TableListing;