import React, { Component } from 'react';
import { Form, Input, Button, DatePicker } from 'antd';
import {SearchOutlined, SaveOutlined ,FileOutlined,CaretDownOutlined,DeleteOutlined} from '@ant-design/icons';
import TableListing from './table-listing';
import InquiriesPurchases from './inquiries-purchases';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;

class BunkerPlanning extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="toolbar-ui-wrapper">
                                <div className="leftsection">
                                    <span key="first" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><SearchOutlined /></li>
                                            <li><SaveOutlined /><span className="text-bt">Save</span></li>
                                        </ul>
                                    </span>

                                    <span key="second" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><FileOutlined /><CaretDownOutlined /></li>
                                            <li><DeleteOutlined /></li>
                                        </ul>
                                    </span>
                                </div>
                                <div className="rightsection">
                                    <span className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt">Attachments</span></li>
                                            <li><span className="text-bt">Accept Alert</span></li>
                                            <li><span className="text-bt">Bunker Slip</span></li>
                                            <li><span className="text-bt">Import Vendors</span></li>
                                            <li><span className="text-bt">Email<CaretDownOutlined /></span></li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="form-wrapper">
                                <div className="form-heading">
                                    <h4 className="title"><span>Bunker Planning</span></h4>
                                </div>
                                <div className="action-btn">
                                    <Button type="primary" htmlType="submit">Save</Button>
                                    <Button>Reset</Button>
                                </div>
                            </div>
                            <Form>
                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Vessel">
                                            <InputGroup compact>
                                                <Input style={{ width: '80%' }} defaultValue="" />
                                                <Input style={{ width: '20%' }} defaultValue="AAAU" disabled />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Voy No.">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Req Delivery">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Port">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="ETA">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Delivery">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Charterer">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="ETB">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Window From">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Owner">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="ETD">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Window To">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Request Date">
                                            <DatePicker />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Agent">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Company">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Delivery Type">
                                            <DatePicker />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Last Update">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Last Update By">
                                            <Input size="default" placeholder="" disabled />
                                        </FormItem>
                                    </div>
                                </div>

                            </Form>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <TableListing />
                                </div>
                            </div>

                            <hr />

                            <div className="row">
                                <div className="col-md-6">
                                    <FormItem label="Request Note">
                                        <TextArea placeholder="" autoSize={{ minRows: 3, maxRows: 3 }} />
                                    </FormItem>
                                </div>

                                <div className="col-md-6">
                                    <FormItem label="Bunkering Details">
                                        <TextArea placeholder="" autoSize={{ minRows: 3, maxRows: 3 }} />
                                    </FormItem>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <InquiriesPurchases />
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

            </div>
        )
    }
}

export default BunkerPlanning;