import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
    {
        title: 'Type',
        dataIndex: 'type',
        width: 80,

    },
    {
        title: 'Grade',
        dataIndex: 'grade',
        width: 80,
    },

    {
        title: 'Min Qty',
        dataIndex: 'minqty',
        width: 80,
    },

    {
        title: 'Requrd Qty',
        dataIndex: 'reqqty',
        width: 80,
    },

    {
        title: 'Max Qty',
        dataIndex: 'maxqty',
        width: 80,
    },
    {
        title: 'Rcvd Qty',
        dataIndex: 'receqty',
        width: 80,
    },

    {
        title: 'BDN Qty',
        dataIndex: 'bdnqty',
        width: 80,
    },
    {
        title: 'Unit',
        dataIndex: 'unit',
        width: 80,
    },
    {
        title: 'Sulphur %',
        dataIndex: 'sulfur',
        width: 80,
    },
];

const data = [];
for (let i = 0; i < 5; i++) {
    data.push({
        key: i.toString(),
        type: "Type",
        grade: "Grade",
        reqqty: "Req Qty",
        minqty: "Min Qty",
        maxqty: "Max Qty",
        receqty: "Max Qty",
        bdnqty: "Opr Qty",
        unit: "Alt Unit",
        sulfur: "Sulfur",
    });
}



class TableListing extends Component {



    render() {
        return (
            <>
                <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    size="small"
                    //pagination={false}
                    title={() => <div className="table-header-wrapper">
                        <div className="form-heading">
                            <div className="title"><span>Planned Lifting</span></div>
                        </div>
                    </div>
                    }
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default TableListing;