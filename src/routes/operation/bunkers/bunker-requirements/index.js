import React, { Component } from "react";
import { Row, Col, Modal, Layout, Table } from "antd";
import BunkerRequirementReport from "../../../operation-reports/BunkerRequirementReport";
import BunkerPurchasedOrder from "../bunker-purchased-order/components/BunkerPurchasedOrder";
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
  apiDeleteCall,
  getAPICall,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../shared";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import { FIELDS } from "../../../../shared/tableFields";
import { DeleteOutlined, SaveOutlined, SyncOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import { useRef } from "react";

const { Content } = Layout;

const BunkerRequirements = (props) => {
  let tableHeaders = Object.assign(
    [],
    FIELDS && FIELDS["bunker-purchased-order-summery-list"]
      ? FIELDS["bunker-purchased-order-summery-list"]["tableheads"]
      : []
  );

  const [state, setState] = useStateCallback({
    frmOptions: [],
    modals: {
      BunkerReqReport: false,
      BunkerPurchasedOrderModal: false,
    },

    frmName: "bunker_requirement_form",
    formData: {},
    voyID: (props.formData && props.formData.id) || "",
    voyageData: props.voyageData || {},
    frmVisible: false,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    isShowVoyageFixtureReport: false,
    port: [],
    responseData: [],
    loading: false,
    columns: tableHeaders,
  });

  const edit = () => {
    setState((prevState) => ({ ...prevState, editable: true }));
  };

  useEffect(() => {
    getFormdata();
  }, []);

  const getFormdata = async () => {
    const { formData, ID } = props;
    const { voyageData } = state;
    let _formData = {},
      portArray = [];

    if (voyageData && voyageData.hasOwnProperty("id")) {

      _formData = {
        vessel: voyageData["vessel_id"],
        port:
          typeof voyageData["b_port"] !== "undefined"
            ? voyageData["b_port"]
            : voyageData["b_port_name"],
        // port:voyageData["portitinerary"]["port"],
        vessel_code: voyageData["vessel_code"],
        eta: voyageData["arrival_date_time"],
        voyage_no: voyageData["voyage_number"],
        etd: voyageData["departure"],
        tci_owner: voyageData["owner"],
        charterer: voyageData["charterer"],
        my_company: voyageData["my_company_lob"],
        remark: voyageData["remark"],
        voyage_manager_id: voyageData["id"],
      };
      voyageData["portitinerary"] &&
        voyageData["portitinerary"].length > 0 &&
        voyageData["portitinerary"].map((val, ind) => {
          portArray.push({
            name: val.port,
            id: val.port_id,
            eta: val.arrival_date_time,
            etd: val.departure,
          });
        });

      if (portArray.length > 0) {
        _formData["port"] = portArray[0].id;
        _formData["eta"] = portArray[0].eta;
        _formData["etd"] = portArray[0].etd;
      }

    }

    if (formData && formData.hasOwnProperty("id") && formData.id > 0) {
      if (props.portData) {
        props.portData["portitinerary"] &&
          props.portData["portitinerary"].length > 0 &&
          props.portData["portitinerary"].map((val, ind) => {


            portArray.push({
              name: val.port,
              id: val.port_id,
              eta: val.arrival_date_time,
              etd: val.departure,

            });
          });

      }
      _formData = Object.assign({}, formData);
      if (formData && formData["requirement_id"]) {
        setState((prevState) => ({ ...prevState, loading: true }));
        let headers = {
          order_by: { id: "desc" },
          where: { OR: { requirement_id: { l: formData["requirement_id"] } } },
        };
        let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/list`;
        const response = await getAPICall(_url, headers);
        const data = await response["data"];
        setState((prevState) => ({
          ...prevState,
          loading: false,
          responseData: data,
        }));
      }
    }
    setState((prevState) => ({
      ...prevState,
      formData: _formData,
      frmOptions: [{ key: "port", data: portArray }],
      frmVisible: true,
    }));
  };

  let formref = useRef(null);

  const reFreshForm = () => {
    setState(
      (prevState) => ({
        ...prevState,
        formData: { ...formref.current },
        frmVisible: false,
      }),
      () => {
        setState({ ...state, frmVisible: true });
      }
    );
  };

  useEffect(() => {
    formref.current = Object.assign({}, state.formData);
  }, []);

  const saveFormData = async (data) => {
    const { voyID } = state;
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let url = "save";
    let _method = "post";
    if (data.hasOwnProperty("id")) {
      url = "update";
      _method = "put";
    }
    if (data) {
      
      let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/${url}?frm=${state.frmName}`;
      await postAPICall(`${_url}`, data, _method, (response) => {
        // alert("test");
        if (response && response.data == true) {

          openNotificationWithIcon("success", response.message);
          if (url === "save") {
            // alert("save");
            window.emitNotification({
              n_type: "Bunker requirement Added",
              msg: window.notificationMessageCorrector(`Bunker requirement is added, for Voyage(${data.voyage_no}), by ${window.userName}`),
            });
          } else {
            window.emitNotification({
              n_type: "Bunker requirement Updated",
              msg: window.notificationMessageCorrector(`Bunker requirement is updated, for Voyage(${data.voyage_no}), by ${window.userName}`),
            });
          }
          const _voyID = (response && response.row && response.row.id) || voyID;

          const updateData = async () => {
            let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/edit?e=${_voyID}`;
            const response = await getAPICall(_url);
            const respData = await response["data"];
            setState((prevState) => ({
              ...prevState,
              formData: respData ? respData : data,
              frmVisible: true,
              voyID: respData && respData.id,
            }));

          };
          updateData();
        } else if (response && response.data == false) {
          setState((prevState) => ({
            ...prevState,
            formData: data,
            frmVisible: true,
          }));
          if (typeof response.message === "string") {
            openNotificationWithIcon("error", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      });
    }
  };
  const onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource3 = [...state.dataSource3];
      const target = dataSource3.find((item) => item.key === key);
      if (target) {
        target[dataIndex] = value;
        setState((prevState) => ({ ...prevState, dataSource3 }));
      }
    };
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Cargo Id is empty. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      // alert("test");
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        window.emitNotification({
          n_type: "Bunker requirement Deleted",
          msg: window.notificationMessageCorrector(`Bunker requirement is deleted, for Voyage(${postData.voyage_no}), by ${window.userName}`),
        });
        setState(
          (prevState) => ({ ...prevState, frmVisible: false }),
          (prevState) => ({ ...prevState, frmVisible: true })
        );
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };
  //resizing function
  const handleResize =
    (index) =>
      (e, { size }) => {
        setState(({ columns }) => {
          const nextColumns = [...columns];
          nextColumns[index] = {
            ...nextColumns[index],
            width: size.width,
          };
          return { columns: nextColumns };
        });
      };

  const showHideModal = (visible, modal) => {
    const { formData } = state;
    if (
      formData.hasOwnProperty("requirement_id") &&
      formData.requirement_id > 0
    ) {
      const { modals } = state;
      let _modal = {};
      _modal[modal] = visible;
      setState((prevState) => ({
        ...prevState,
        modals: Object.assign(modals, _modal),
      }));
    } else {
      openNotificationWithIcon(
        "error",
        "bunker requirment id is not generated"
      );
    }
  };

  const BunkerReqReport = async (showBunkerReqReport) => {
    try {
      if (showBunkerReqReport == true) {
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/bunker-req/report?e=${state.voyID}`
        );
        const respDataReport = await responseReport["data"];
        if (respDataReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowBunkerReqReport: showBunkerReqReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } else {
        setState((prevState) => ({
          ...prevState,
          isShowBunkerReqReport: showBunkerReqReport,
        }));
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong.", 3);
    }
  };

  const EstimateReport = (showEstimateReport) =>
    setState((prevState) => ({
      ...prevState,
      isShowEstimateReport: showEstimateReport,
    }));

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Planned Liftings") {
      groupKey = "plannedliftings";
      frm_code = "bunker_requirement_form";
    }

    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
        key: data.key,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const {
    frmName,
    columns,
    loading,
    responseData,
    formData,
    frmVisible,
    frmOptions,
    isShowBunkerReqReport,
    reportFormData,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  return (
    <div className="body-wrapper">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body">
                      {frmVisible ? (
                        <NormalFormIndex
                          key={"key_" + frmName + "_0"}
                          formClass="label-min-height"
                          formData={formData}
                          showForm={true}
                          frmCode={frmName}
                          //frmVisible={frmVisible}
                          frmOptions={frmOptions}
                          addForm={true}
                          showToolbar={[
                            {
                              isLeftBtn: [
                                {
                                  isSets: [
                                    {
                                      id: "3",
                                      key: "save",
                                      type: <SaveOutlined />,
                                      withText: "Save",
                                      showToolTip: true,
                                      event: (key, data) => saveFormData(data),
                                    },

                                    formData &&
                                    formData["id"] && {
                                      id: "4",
                                      key: "delete",
                                      type: <DeleteOutlined />,
                                      withText: "Delete",
                                      showToolTip: true,
                                      event: (key, data) =>
                                        _onDeleteFormData(data),
                                    },
                                    {
                                      id: "20",
                                      key: "refresh",
                                      type: <SyncOutlined />,
                                      withText: "Refresh",
                                      showToolTip: true,
                                      event: () => {
                                        reFreshForm();
                                      },
                                    },
                                  ],
                                },
                              ],
                              isRightBtn: [
                                {
                                  isSets: [
                                    {
                                      key: "BunkerPurchasedOrderModal",
                                      isDropdown: 0,
                                      withText: "Bunker Purchase order ",
                                      type: "",
                                      menus: null,
                                      event: (key) =>
                                        showHideModal(
                                          true,
                                          "BunkerPurchasedOrderModal"
                                        ),
                                    },

                                    // {
                                    //   key: 'Bunkers',
                                    //   isDropdown: 0,
                                    //   withText: 'Send Email',
                                    //   type: '',
                                    //   menus: null,
                                    //   event: (key) => showHideModal(true, 'sendEmail'),
                                    // },
                                    // {
                                    //   key: 'Bunkers',
                                    //   isDropdown: 0,
                                    //   withText: 'Attachment',
                                    //   type: '',
                                    //   menus: null,
                                    //   event: (key) => showHideModal(true, 'attachment'),
                                    // },
                                    {
                                      key: "BunkerReqReport",
                                      isDropdown: 0,
                                      withText: "Report",
                                      type: "",
                                      menus: null,
                                      event: (key, data) =>
                                        data && data.id > 0
                                          ? BunkerReqReport(true)
                                          : openNotificationWithIcon(
                                            "info",
                                            "please select any item in the list!"
                                          ),
                                    },
                                  ],
                                },
                              ],
                            },
                          ]}
                          inlineLayout={true}
                          isShowFixedColumn={[
                            "Planned Liftings",
                            "Purchase Order List",
                          ]}
                          // showSideListBar={null}
                          tableRowDeleteAction={(action, data) =>
                            onClickExtraIcon(action, data)
                          }
                        />
                      ) : undefined}
                      <Table
                        className="inlineTable editableFixedHeader resizeableTable"
                        bordered
                        scroll={{ x: "max-content" }}
                        // scroll={{ x: 1200, y: 370 }}
                        columns={tableColumns}
                        size="small"
                        dataSource={responseData}
                        loading={loading}
                        pagination={false}
                        title={() => (
                          <div className="table-header-wrapper">
                            <div className="form-heading">
                              <div className="title">
                                Bunker Purchase Order Summary
                              </div>
                            </div>
                          </div>
                        )}
                        rowClassName={(r, i) =>
                          i % 2 === 0
                            ? "table-striped-listing"
                            : "dull-color table-striped-listing"
                        }
                      />
                    </div>
                  </div>
                </article>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>

      {state.modals["BunkerPurchasedOrderModal"] ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Bunker Purchased Order"
          open={state.modals["BunkerPurchasedOrderModal"]}
          onCancel={() => showHideModal(false, "BunkerPurchasedOrderModal")}
          width="95%"
          footer={null}
        >
          <BunkerPurchasedOrder formData={formData} voyID={formData} />
        </Modal>
      ) : undefined}


      {isShowBunkerReqReport ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Report"
          open={isShowBunkerReqReport}
          // onOk={handleOk}
          onCancel={() => BunkerReqReport(false)}
          width="95%"
          footer={null}
        >
          <BunkerRequirementReport data={reportFormData} />
        </Modal>
      ) : undefined}
    </div>
  );
};

export default BunkerRequirements;
