import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button, Checkbox } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        port: "Port",
        vendor: "Vendor",
        s: "s",
        foraccount: "For Account",
        status: "Status",
        curr: "Curr",
        exchrate: "Exch Rate",
        hfoqty: "HFO Qty",
        hfoprice: "HFO Price",
        mdoqty: "MDO Qty",
        mdoprice: "MDO Price",
        mgoqty: "MGO Qty",
        mgoprice: "MGO Price",
        bargprice: "Barg Price",
        barging: "Barging",
        other: "Other",
        portcost: "Port Cost",
        tax: "Tax",
        total: "Total",
        payterms: "Pay Terms",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class InquiriesPurchases extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Port',
            dataIndex: 'port',
            width: 70,
            render: (text, record) => this.renderColumns(text, record, 'port'),
        },
        {
            title: 'Vendor',
            dataIndex: 'vendor',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'vendor'),
        },
        {
            title: 'S',
            dataIndex: 's',
            width: 50,
            render: (text, record) => this.renderColumns(text, record, 's'),
        },
        {
            title: 'For Account',
            dataIndex: 'foraccount',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'foraccount'),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            width: 80,
            render: (text, record) => this.renderColumns(text, record, 'status'),
        },
        {
            title: 'Curr',
            dataIndex: 'curr',
            width: 80,
            render: (text, record) => this.renderColumns(text, record, 'curr'),
        },
        {
            title: 'Exch Rate',
            dataIndex: 'exchrate',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'exchrate'),
        },
        {
            title: 'HFO Qty',
            dataIndex: 'hfoqty',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'hfoqty'),
        },
        {
            title: 'HFO Price',
            dataIndex: 'hfoprice',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'hfoprice'),
        },
        {
            title: 'MDO Qty',
            dataIndex: 'mdoqty',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'mdoqty'),
        },
        {
            title: 'MDO Price',
            dataIndex: 'mdoprice',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'mdoprice'),
        },
        {
            title: 'MGO Qty',
            dataIndex: 'mgoqty',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'mgoqty'),
        },
        {
            title: 'MGO Price',
            dataIndex: 'mgoprice',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'mgoprice'),
        },
        {
            title: 'Barg Price',
            dataIndex: 'bargprice',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'bargprice'),
        },
        {
            title: 'Barging',
            dataIndex: 'barging',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'barging'),
        },
        {
            title: 'Other',
            dataIndex: 'other',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'other'),
        },
        {
            title: 'Portcost',
            dataIndex: 'portcost',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'portcost'),
        },
        {
            title: 'Tax',
            dataIndex: 'tax',
            width: 50,
            render: (text, record) => this.renderColumns(text, record, 'tax'),
        },
        {
            title: 'Total Ammount',
            dataIndex: 'total',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'total'),
        },

        {
            title: 'Pay Terms',
            dataIndex: 'payterms',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'payterms'),
        },

        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                           <DeleteOutlined /> />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 1000, y: 150 }}
                    size="small"
                    pagination={false}
                    title={() => <div className="table-header-wrapper">
                        <div className="form-heading">
                            <div className="title"><span>Inquiries and Purchases</span></div>
                        </div>
                        <div className="action-btn">
                            <Checkbox>Display in Base Currency</Checkbox>
                        </div>
                    </div>
                    }

                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default InquiriesPurchases;