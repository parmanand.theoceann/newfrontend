import React from 'react';
import { Table,  Popconfirm, Modal } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import URL_WITH_VERSION, {
  getAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon

} from '../../../../shared';
import { FIELDS } from '../../../../shared/tableFields';
import BunkerPurchasedOrder from '../bunker-purchased-order/components/BunkerPurchasedOrder';
import ToolbarUI from '../../../../components/CommonToolbarUI/toolbar_index'
import SidebarColumnFilter from '../../../../shared/SidebarColumnFilter'

class BunkerPurchageOrderSummary extends React.Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => this.onRowClick(text)}>
            <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        )
      }
    };
    let tableHeaders = Object.assign([], FIELDS && FIELDS['bunker-purchased-order-summery-list'] ? FIELDS['bunker-purchased-order-summery-list']["tableheads"] : [])
    tableHeaders.push(tableAction)

    this.state = {
      frmName: 'bunker_requirement_form',
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      showModal: false,
      voyageData:this.props.voyageData || {},
      typesearch:{},
      donloadArray: []
    }

  }

  componentDidMount = () => {
    this.getTableData();
  }

  getTableData = async (searchtype = {}) => {
    const { pageOptions, voyageData } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };
    let search=searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue')?searchtype:this.state.typesearch;

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim()
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
      this.state.typesearch={'searchOptions':search.searchOptions,'searchValue':search.searchValue}

    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    let totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr1 = data && data.data ? data.data : [];
    let new_array = []
    if(dataArr1 && dataArr1.length > 0){
      dataArr1.map(e=>{
        if(e.voyage_manager_id === voyageData.id){  //tech the oceann
          new_array.push(e)
        }
        return true;
      })
    }
    let dataArr = new_array
    let state = { loading: false };
    let donloadArr = []
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      state['responseData'] = dataArr;
    }
    totalRows=dataArr.length
    this.setState({
      ...this.state,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };
  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  }
  onRowClick = async record => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/edit?e=${record['id']}`;
    const response = await getAPICall(_url);
    const respData = await response['data'];

    this.setState({ ...this.state, formDataValues: respData }, () =>
      this.setState({ ...this.state, showModal: true })
    );
  };

  // onRowDeletedClick = (id) => {
  //   let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-pur/delete`;
  //   apiDeleteCall(_url, { "id": id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon('success', response.message);
  //       this.getTableData(1);
  //     } else {
  //       openNotificationWithIcon('error', response.message)
  //     }
  //   })
  // }

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };
  hideShowModal = visible => this.setState({ ...this.state, showModal: visible });

  modalCloseEvent = () => {
    this.setState({ ...this.state, showModal: false }, ()=> this.getTableData());
  }

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  onActionDonwload = (downType, pageType) => {
      let params = `t=${pageType}`, cols = [];
      const { columns, pageOptions, donloadArray } = this.state;  



    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');

  }

  render() {
    const { columns, sidebarVisible, responseData, showModal, search, loading, formDataValues, pageOptions } = this.state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper">

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loading === false ?
                    <ToolbarUI
                      routeUrl={'bunker-purchased-order-summery-toolbar'}
                      optionValue={{ "pageOptions": pageOptions, "columns": columns, "search": search }}
                      callback={e => this.callOptions(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => this.onActionDonwload('csv', 'bunpur') },
                        { title: 'PDF', event: () => this.onActionDonwload('pdf', 'bunpur') },
                        { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'bunpur') },
                      ]}
                    />
                    :
                    undefined
                }
              </div>
              <Table
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                scroll={{ x: 'max-content' }}
                // scroll={{ x: 1200, y: 370 }}
                columns={tableColumns}
                size="small"
                components={this.components}
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
              />
            </div>
          </div>
        </article>
        {showModal ? (
          <Modal
            className="page-container"
            style={{ top: '2%' }}
            title="Bunker Purchase Order - "
           open={showModal}
            onCancel={() => this.hideShowModal(false)}
            width="90%"
            footer={null}
          >
            <BunkerPurchasedOrder formData={formDataValues} modalCloseEvent={this.modalCloseEvent}/>

          </Modal>
        ) : (
          undefined
        )}
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptions(e)} /> : null
        }
      </div>
    );
  }
}

export default BunkerPurchageOrderSummary;
