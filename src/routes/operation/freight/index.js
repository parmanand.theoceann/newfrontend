import React from 'react';
import { Route } from 'react-router-dom';

import CommissionInvoice from './commission-invoice';
import InitialFreightInvoice from './initial-freight-invoice';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/initial-freight-invoice`} component={InitialFreightInvoice} />
    <Route path={`${match.url}/commission-invoice`} component={CommissionInvoice} />
  </div>
)

export default CardComponents;