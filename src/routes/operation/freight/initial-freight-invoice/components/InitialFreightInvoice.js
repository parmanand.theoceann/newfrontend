import React, { useEffect } from "react";
import { Row, Col, Modal, Layout, notification } from "antd";
import Tde from "../../../../tde/";
import CreateInvoice from "../../../../create-invoice/CreateInvoice";
import FreightInvoiceReport from "../../../../operation-reports/FeightInvoiceReport";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  getAPICall,
  openNotificationWithIcon,
  apiDeleteCall,
  useStateCallback,
} from "../../../../../shared";
import InvoicePopup from "../../../../create-invoice/InvoicePopup";
import moment from "moment";
import { DeleteOutlined, SaveOutlined, EditOutlined } from "@ant-design/icons";
import Attachment from "../../../../../shared/components/Attachment";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../../shared/attachments";

import Remarks from "../../../../../shared/components/Remarks";

const { Content } = Layout;

const openNotification = (keyName) => {
  let msg = "Please generate the Invoice No. First";
  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const InitialFreightInvoice = (props) => {
  const [state, setState] = useStateCallback({
    modals: {
      InvoiceModal: false,
      TdeShow: false,
      FreightInvoiceReport: false,
      isInvoicePopup: false,
    },
    frmName: "INITIAL_FRIEGHT_INVOICE",
    // formData: props.formData || {},
    formData: Object.assign({}, props.formData || {}),
    frmOptions: [],
    voyID: props.voyID || 0,
    frmVisible: false,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    isShowVoyageFixtureReport: false,
    isEdit: props.isEdit || false,
    oldFormData: props.voyageData,
    invoiceReport: null,
    isSaved: false,
    popupdata: null,
    editMode: true,
    reRenderKey: 0,
  });



  useEffect(() => {
    getInitialFreightInvoice();
  }, []);

  const edit = () =>
    setState((prevState) => ({ ...prevState, editable: true }));

  const closeTde = () => {
    setState((prevState) => ({ ...prevState, isShowTde: false }));
  };

  const _onEditDataLoad = async (coaVciId) => {
    const request = await getAPICall(
      `${URL_WITH_VERSION}/freight-invoice/initial?e=${coaVciId}`
    );
    const respData = await request;

    if (respData["data"]) {
      let chartererOptions = Object.assign(
        [],
        respData["data"]["charterer_options"]
      );
      delete respData["data"]["charterer_options"];
      setState((prevState) => ({
        ...prevState,
        formData: respData["data"],
        frmOptions: [{ key: "counterparty", data: chartererOptions }],
        frmVisible: true,
      }));
    } else {
      openNotificationWithIcon("error", respData["message"]);
    }
  };

  const saveFormData = async (data) => {
    let type = "save";
    let suMethod = "POST";
    if (data?.hasOwnProperty("id")) {
      type = "update";
      suMethod = "PUT";
    }

    if (data?.[".."] && data?.[".."].length > 0) {
      for (let index in data[".."]) {
        let el = data[".."][index];
        if (Number(el["inv"]) < 0 || Number(el["inv"]) > 100) {
          openNotificationWithIcon(
            "info",
            "Please fill the invoice percentage range between 0 to 100",
            3
          );
          return;
        }
        if (el["inv"] === "") el["inv"] = 0;
        delete el["inv" + index];
        delete el[""];
      }
    }
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let _url = `${URL_WITH_VERSION}/freight-invoice/${type}?frm=${state.frmName}`;
    delete data?.["inv"];
    delete data?.[""];
    data?.["commission"] &&
      data?.["commission"].map((el) => {
        if (type === "save") {
          delete el["inv"];
          delete el["id"];
        } else {
          delete el["inv"];
          delete el["broker_name"];
        }
      });

    data?.["adjustment"] &&
      data?.["adjustment"].map((el) => {
        if (type === "save") {
          delete el["id"];
        }
      });

    await postAPICall(`${_url}`, data, suMethod, (response) => {
      if (response && response.data === true) {
        let prevData = { ...state };
        prevData.formData = data;
        setState((prevState) => ({
          ...prevState,
          formData: prevData,
          reRenderKey: prevState.reRenderKey + 1,
          frmVisible: true,
        }));
        openNotificationWithIcon("success", response.message);
        if (
          props.modalCloseEvent &&
          typeof props.modalCloseEvent === "function"
        ) {
          props.modalCloseEvent();
        }
      } else if (response && response.data === false) {
        setState((prevState) => ({
          ...prevState,
          reRenderKey: prevState.reRenderKey + 1,
          formData: data,
          frmVisible: true,
        }));
        if (typeof response.message === "string") {
          openNotificationWithIcon("error", response.message);
        } else {
          openNotificationWithIcon("error", response.message);
        }
      }
    });
  };

  const getInitialFreightInvoice = async () => {
    let { voyID, formData } = await state;
    if (voyID) _onEditDataLoad(voyID);
    let editmode = state.editMode;

    if (
      formData &&
      formData.hasOwnProperty("id") &&
      formData["id"] > 0 &&
      formData.hasOwnProperty("invoice_no")
    ) {
      editmode = false;
      setState((prevState) => ({ ...prevState, frmVisible: false }));
      const request = await getAPICall(
        `${URL_WITH_VERSION}/freight-invoice/initial?e=${formData["voy_no"]}`
      );
      const respData = await request;

      if (respData["data"]) {
        let chartererOptions = Object.assign(
          [],
          respData["data"]["charterer_options"]
        );
        state.formData.cp_date = respData["data"].cp_date;
        setState((prevState) => ({
          ...prevState,
          frmOptions: [{ key: "counterparty", data: chartererOptions }],
          editMode: editmode,
          frmVisible: true,
        }));
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    }
  };

  const onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource3 = [...state.dataSource3];
      const target = dataSource3.find((item) => item.key === key);
      if (target) {
        target[dataIndex] = value;
        setState((prevState) => ({ ...prevState, dataSource3 }));
      }
    };
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Cargo Id is empty. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/freight-invoice/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        setState(
          (prevState) => ({ ...prevState, frmVisible: false }),
          () =>
            setState((prevState) => ({
              ...prevState,
              showSideListBar: true,
              frmVisible: true,
            }))
        );
        if (
          props.modalCloseEvent &&
          typeof props.modalCloseEvent === "function"
        ) {
          props.modalCloseEvent();
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const onClickExtraIcon = async (action, data) => {
    let groupKey = action["gKey"];
    let frm_code = "INITIAL_FRIEGHT_INVOICE";

    let delete_id = data && data.id;
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey.replace(/\s/g, "").toLowerCase(),
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const handleAdd = () => {
    const { count, dataSource3 } = state;
    const newData = {
      key: count,
    };
    setState((prevState) => ({
      ...prevState,
      dataSource3: [...dataSource3, newData],
      count: count + 1,
    }));
  };

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    modals[modal] = visible;
    setState((prevState) => ({
      ...prevState,
      modals: modals,
    }));
  };

  const invoiceModal = async (data = {}) => {
    let { formData, invoiceReport } = state;
    try {
      if (Object.keys(data).length == 0) {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/freight-invoice/report?e=${formData.invoice_no}`
        );
        const respData = await response["data"];
        if (respData) {
          setState((prevState) => ({
            ...prevState,
            invoiceReport: respData,
            popupdata: respData,
          }));
          showHideModal(true, "isInvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry, Unable to show invoice", 3);
        }
      } else {
        delete data[".."];
        setState((prevState) => ({
          ...prevState,
          invoiceReport: { ...invoiceReport, ...data },
        }));
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  const handleok = () => {
    const { invoiceReport } = state;
    if (invoiceReport["isSaved"]) {
      showHideModal(false, "isInvoicePopup");
      setTimeout(() => {
        showHideModal(true, "InvoiceModal");
      }, 2000);
      setState((prevState) => ({ ...prevState, invoiceReport: invoiceReport }));
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  // const showTDE = async (key, bolVal) => {
  //   const { formData } = state;

  //   let resp = null,
  //     target = undefined,
  //     tdeData = {};
  //   let account_no = null;
  //   let accountCode = null;

  //   if (bolVal == true) {
  //     const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
  //     let respData = response["data"];

  //     if (formData && formData.invoice_no && formData.invoice_no !== "") {
  //       const responseData = await getAPICall(
  //         `${URL_WITH_VERSION}/address/edit?ae=${formData["counterparty"]}`
  //       );
  //       const responseAddressData = responseData["data"];
  //       account_no =
  //         responseAddressData &&
  //         responseAddressData["bank&accountdetails"] &&
  //         responseAddressData["bank&accountdetails"].length > 0
  //           ? responseAddressData["bank&accountdetails"][0] &&
  //             responseAddressData["bank&accountdetails"][0]["account_no"]
  //           : "";

  //       accountCode =
  //         responseAddressData &&
  //         responseAddressData["bank&accountdetails"] &&
  //         responseAddressData["bank&accountdetails"].length > 0
  //           ? responseAddressData["bank&accountdetails"][0] &&
  //             responseAddressData["bank&accountdetails"][0]["swift_code"]
  //           : "";

  //       let voyageData = null;
  //       let voyage_manager_id = formData.voy_no;

  //       if (voyage_manager_id) {
  //         const request = await getAPICall(
  //           `${URL_WITH_VERSION}/voyage-manager/edit?ae=${voyage_manager_id}`
  //         );
  //         voyageData = await request["data"];
  //       }

  //       if (respData && respData.length > 0) {
  //         target = respData.find(
  //           (item) => item.invoice_no === formData.invoice_no
  //         );
  //         if (target && target.hasOwnProperty("id") && target["id"] > 0) {
  //           resp = await getAPICall(
  //             `${URL_WITH_VERSION}/tde/edit?e=${target["id"]}`
  //           );
  //         }
  //       }

  //       let accounting = [];
  //       if (
  //         target &&
  //         resp &&
  //         resp["data"] &&
  //         resp["data"].hasOwnProperty("id")
  //       ) {
  //         tdeData = resp["data"];
  //         if (formData["-----"] && formData["-----"].length > 0) {
  //           formData["-----"].map((e, index) =>
  //             accounting.push({
  //               description: e["description"],
  //               amount: e["amount"],
  //               account: e["fc_heads"],
  //               company: target["bill_via"],
  //               lob:
  //                 voyageData && voyageData.hasOwnProperty("company_lob")
  //                   ? voyageData["company_lob"]
  //                   : undefined,
  //               vessel_code:
  //                 voyageData && voyageData.hasOwnProperty("vessel_code")
  //                   ? voyageData["vessel_code"]
  //                   : undefined,
  //               vessel_name: target["vessel"],
  //               port: "select port",
  //               ap_ar_acct: target["ar_pr_account_no"],
  //               voyage: formData["voy_no"],
  //               id: -9e6 + index,
  //             })
  //           );
  //         }

  //         tdeData["accounting"] = accounting;
  //         tdeData["--"] = { total: target["invoice_amount"] };
  //         //  tdeData['----'] = { 'total_due': target['invoice_amount'], 'total': target['invoice_amount'],"remittance_bank":resp['data']['----']["remittance_bank"]}

  //         let rem_data = resp["data"]["----"][0];

  //         tdeData["----"] = {
  //           total_due: rem_data["total_due"],
  //           total: rem_data["total"],
  //           remittance_bank: rem_data["remittance_bank"],
  //         };

  //         setState(prevState => ({
  //           ...prevState,
  //           isShowTde: bolVal,
  //           tdeData: Object.assign({}, tdeData),
  //         }));
  //       } else {
  //         let _formData = {
  //           invoice: formData["acc_type"],
  //           invoice_no: formData["invoice_no"],
  //           invoice_date: formData["due_date"],
  //           invoice_type: formData["invoice_type"],
  //           po_number: formData["po_number"],
  //           received_date: formData["received_date"],
  //           payment_term: formData["payment_terms"],
  //           vessel: formData["vessel"],
  //           vendor: formData["counterparty"],
  //           bill_via: formData["my_company"],
  //           voyage: formData["voy_no"],
  //           voyage_manager_id: formData["voyage_id"],
  //           invoice_amount: formData["invoice_total"],
  //           account_base: formData["invoice_total"],
  //           ar_pr_account_no: account_no,
  //           inv_status: formData["fia_status"],
  //           "--": { total: formData["invoice_total"] },
  //           "----": { total_due: formData["invoice_total"] },
  //           accounting: [],
  //         };

  //         if (formData["-----"] && formData["-----"].length > 0) {
  //           formData["-----"].map((e, index) =>
  //             _formData["accounting"].push({
  //               id: -9e6 + index,
  //               description: e["description"],
  //               amount: e["amount"],
  //               account: e["fc_heads"],
  //               company: formData["my_company"],
  //               lob:
  //                 voyageData && voyageData.hasOwnProperty("company_lob")
  //                   ? voyageData["company_lob"]
  //                   : undefined,
  //               vessel_code:
  //                 voyageData && voyageData.hasOwnProperty("vessel_code")
  //                   ? voyageData["vessel_code"]
  //                   : undefined,
  //               vessel_name: formData["vessel"],
  //               port: "select port",
  //               ap_ar_acct: account_no,
  //               voyage: formData["voy_no"],
  //               id: -9e6 + index,
  //             })
  //           );
  //         }
  //         setState(prevState => ({ ...prevState, isShowTde: bolVal, tdeData: _formData }));
  //       }
  //     } else {
  //       openNotificationWithIcon(
  //         "info",
  //         "Please generate the Invoice No. First"
  //       );
  //     }
  //   } else {
  //     setState(prevState => ({ ...prevState, isShowTde: bolVal }));
  //   }
  // };

  const {
    frmName,
    formData,
    frmOptions,
    frmVisible,
    isEdit,
    invoiceReport,
    isSaved,
    modals,
    isShowTde,
    popupdata,
    tdeData,
    editMode,
    isRemarkModel,
  } = state;

  

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };
  const handleRemark = () => {
    setState((prevState) => ({
      ...prevState,
      isRemarkModel: true,
    }));
  };

  return (
    <div className="body-wrapper">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                      <div className="box-body" key={state.reRenderKey}>
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            frmOptions={frmOptions}
                            showForm={true}
                            frmCode={frmName}
                            frmVisible={frmVisible}
                            addForm={true}
                            // editMode={editMode}
                            showToolbar={[
                              {
                                isLeftBtn: [
                                  {
                                    isSets: [
                                      {
                                        id: "3",
                                        key: "save",
                                        type: <SaveOutlined />,
                                        withText: "Save",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          saveFormData(data),
                                      },
                                      isEdit === true && {
                                        id: "4",
                                        key: "delete",
                                        type: <DeleteOutlined />,
                                        withText: "Delete",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          _onDeleteFormData(data),
                                      },

                                      {
                                        id: "5",
                                        key: "edit",
                                        type: <EditOutlined />,
                                        withText: "Remark",
                                        showToolTip: true,
                                        event: (key, data) => {
                                       
                                          if (data.id) {
                                            handleRemark(data);
                                          } else {
                                            openNotificationWithIcon(
                                             "info",
                                              "Please save Invoice first",
                                              2
                                            );
                                          }
                                        },
                                      },
                                    ],
                                  },
                                ],
                                isRightBtn: [
                                  {
                                    isSets: [
                                      isEdit === true && {
                                        key: "invoice",
                                        isDropdown: 0,
                                        withText: "Create Invoice",
                                        type: "",
                                        menus: null,
                                        event: (key) => invoiceModal(),
                                      },

                                      isEdit === true && {
                                        key: "tde",
                                        isDropdown: 0,
                                        withText: "TDE",
                                        type: "",
                                        menus: null,
                                        event: (key) => {
                                          // showTDE(key, true),
                                          if (formData?.invoice_no) {
                                            setState((prevState) => ({
                                              ...prevState,
                                              isShowTde: true,
                                            }));
                                          } else {
                                            openNotification("tde");
                                          }
                                        },
                                      },
                                      {
                                        key: "attachment",
                                        isDropdown: 0,
                                        withText: "Attachment",
                                        type: "",
                                        menus: null,
                                        event: (key, data) => {
                                          data &&
                                          data.hasOwnProperty("id") &&
                                          data["id"] > 0
                                            ? ShowAttachment(true)
                                            : openNotificationWithIcon(
                                                "info",
                                                "Please save Frieght Invoice First.",
                                                3
                                              );
                                        },
                                      },
                                    ],
                                  },
                                ],
                              },
                            ]}
                            inlineLayout={true}
                            isShowFixedColumn={[
                              "..",
                              "Freight Commission",
                              "Freight Adjustment",
                            ]}
                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                          />
                        ) : undefined}
                        {state.isShowAttachment ? (
                          <Modal
                            style={{ top: "2%" }}
                            title="Upload Attachment"
                            open={state.isShowAttachment}
                            onCancel={() => ShowAttachment(false)}
                            width="50%"
                            footer={null}
                          >
                            {state.loadComponent}
                          </Modal>
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
      {invoiceReport && state.modals["InvoiceModal"] && (
        <Modal
          style={{ top: "2%" }}
          title="Invoice"
          open={state.modals["InvoiceModal"]}
          onCancel={() => showHideModal(false, "InvoiceModal")}
          width="95%"
          footer={null}
          maskClosable={false}
        >
          <CreateInvoice type="frightInvoice" frightInvoice={invoiceReport} />
        </Modal>
      )}

      {state.modals["isInvoicePopup"] ? (
        <Modal
          style={{ top: "2%" }}
          title="Invoice"
          open={state.modals["isInvoicePopup"]}
          onCancel={() => showHideModal(false, "isInvoicePopup")}
          width="95%"
          okText="Create PDF"
          onOk={handleok}
          maskClosable={false}
        >
          <InvoicePopup
            data={popupdata}
            updatepopup={(data) => invoiceModal(data)}
          />
        </Modal>
      ) : undefined}

      {/* <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="TDE"
         open={state.modals["TdeShow"]}
          onCancel={() => showHideModal(false, "TdeShow")}
          width="95%"
          footer={null}
          maskClosable={false}
        >
          <Tde
            receivablePayableType="78"
            invoiceType="FreightInvoice"
            formData={formData}
          />
        </Modal> */}

      {isShowTde ? (
        <Modal
          title="TDE"
          open={isShowTde}
          width="90%"
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isShowTde: false }))
          }
          style={{ top: "10px" }}
          bodyStyle={{ maxHeight: 790, overflowY: "auto", padding: "0.5rem" }}
          footer={null}
        >
          <Tde
            invoiceType="FreightInvoice"
            invoiceNo={formData["invoice_no"]}
            formData={formData}
            closeTde={closeTde}
          />
        </Modal>
      ) : undefined}

      <Modal
        className="page-container"
        style={{ top: "2%" }}
        title="Report"
        open={state.modals["FreightInvoiceReport"]}
        onCancel={() => showHideModal(false, "FreightInvoiceReport")}
        width="95%"
        footer={null}
        maskClosable={false}
      >
        <FreightInvoiceReport />
      </Modal>

      {isRemarkModel &&  (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isRemarkModel: false }))
          }
          footer={false}
        >
          <Remarks
            remarksID={formData.voy_no}
            // remarksID={formData.voyage_id}
            remarkType="freight-invoice"
            remarkVoyId={formData.voyage_id}
            remarkInvNo={formData.invoice_no}
            // idType="Bunker_no"
          />
         
        </Modal>
      )}
    </div>
  );
};

export default InitialFreightInvoice;
