import { EditOutlined } from "@ant-design/icons";
import { Col, Modal, Row, Select, Table } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import ToolbarUI from "../../../../../components/CommonToolbarUI/toolbar_index";
import URL_WITH_VERSION, {
  ResizeableTitle,
  apiDeleteCall,
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  useStateCallback,
} from "../../../../../shared";
import SidebarColumnFilter from "../../../../../shared/SidebarColumnFilter";
import { FIELDS } from "../../../../../shared/tableFields";
import ClusterColumnChart from "../../../../dashboard/charts/ClusterColumnChart";
import DoughNutChart from "../../../../dashboard/charts/DoughNutChart";
import InitialFreightInvoice from "../../../../operation/freight/initial-freight-invoice";
import data from "../../../../dynamic-vspm/vesselCi/data";
import { chain, filter } from "lodash";
import style from './InitialFreightInvoiceSummary.module.css'

const InitialFreightInvoiceSummary = (props) => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    voyID: props.voyID || undefined,
    showPopup: false,
    popupData: {},
    oldFormData: props.voyageData,
    typesearch: {},
    donloadArray: [],
  });
  const allData = useRef();
  const listOfVessel = useRef({ "All": "All" });
  const listOfDate = useRef({ "All": "All" });
  const listOfInvoice = useRef({ "All": "All" });
  const appliedFilter = useRef(["All", "All", "All", "All", "All"])
  const isFilteractive = useRef(true);
  const [modelData, setModelData] = useState({
    totalVessel: 0,
    totalAmount: 0,
    totalInvoice: 0,
    totalRecive: 0,
    totalPay: 0,
    statusAmount: {},
    piChartValue: NaN,
    ClusterDataxAxis: NaN,
    ClusterDataSeries: NaN,
    DoughNutChartSeriesData: NaN,
  })
  const [isGraphModal, setIsGraphModalOpen] = useState(false);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const showGraphs = () => {
    setIsGraphModalOpen(true);
  };

  const handleCancel = () => {
    setIsGraphModalOpen(false);
  };
  const getAlldata = () => {
    let qParams = { p: 0, l: 0 };
    let _url = `${URL_WITH_VERSION}/freight-invoice/list?${objectToQueryStringFunc(qParams)}`;
    let headers = { order_by: { id: "desc" } };
    if (params?.id && params.id.includes("TCOV-FULL")) {
      headers = {
        order_by: { id: "desc" },
        where: { OR: { voy_no: `'${params.id}'` } },
      };
    }
    getAPICall(_url, headers)
      .then((res) => {
        allData.current = res;
        if (allData.current) {
          setFilterData(allData.current.data)
          generateFilter(allData.current.data)
        }
      })
  }
  function generateFilter(listData) {
    for (let data of listData) {
      let isVessleInList = !(data["vessel_name"] in listOfVessel.current)
      let isDateInList = !(data["invoice_date"] in listOfDate.current)
      let isInvoiceInList = !(data["invoice_no"] in listOfInvoice.current)
      if (isDateInList) {
        listOfDate.current[data["invoice_date"]] = 0
      }
      if (isVessleInList) {
        listOfVessel.current[data["vessel_name"]] = [convert_to_float(data["total_amount"])]
      } else {
        listOfVessel.current[data["vessel_name"]].push(convert_to_float(data["total_amount"]))
      }
      if (isInvoiceInList) {
        listOfInvoice.current[data["invoice_no"]] = [convert_to_float(data["invoice_total"])]
      } else {
        listOfInvoice.current[data["invoice_no"]].push(convert_to_float(data["invoice_total"]))
      }
    }
  }
  function setFilterData(listData) {
    let totalAmount = 0
    let totalRecive = 0
    let totalPay = 0
    let statusAmount = {}
    let listOfVessel = {}
    let listOfDate = {}
    let listOfInvoice = {}
    for (let data of listData) {
      let isVessleInList = !(data["vessel_name"] in listOfVessel)
      let isDateInList = !(data["invoice_date"] in listOfDate)
      let isInvoiceInList = !(data["invoice_no"] in listOfInvoice)
      totalAmount += convert_to_float(data["total_amount"])
      if (data["account_type"] === "PAYABLE") {
        totalPay += 1
      }
      if (data["account_type"] === "RECEIVABLE") {
        totalRecive += 1
      }
      if (isDateInList) {
        listOfDate[data["invoice_date"]] = 0
      }
      if (isVessleInList) {
        listOfVessel[data["vessel_name"]] = [convert_to_float(data["total_amount"])]
      } else {
        listOfVessel[data["vessel_name"]].push(convert_to_float(data["total_amount"]))
      }
      if (isInvoiceInList) {
        listOfInvoice[data["invoice_no"]] = [convert_to_float(data["invoice_total"])]
      } else {
        listOfInvoice[data["invoice_no"]].push(convert_to_float(data["invoice_total"]))
      }
      if (data["fia_status"] in statusAmount) {
        statusAmount[data["fia_status"]] += convert_to_float(data["total_amount"])
      } else {
        statusAmount[data["fia_status"]] = convert_to_float(data["total_amount"])
      }

    }
    setModelData(data => {
      let totalVessel = Object.keys(listOfVessel).length;
      let totalInvoice = Object.keys(listOfInvoice).length;
      let ClusterDataxAxis = getTopFiveKeysBySum(listOfVessel).map(data => data.key)
      let ClusterDataSeries = [
        {
          name: "Total Amount",
          type: "bar",
          barGap: 0,
          data: getTopFiveKeysBySum(listOfVessel).map(data => data.sum),
        },
      ];
      let piChartValue = preparePieChartData([{ sum: totalInvoice, key: "RECEIVABLE" },
      { sum: totalPay, key: "PAYABLE" }])

      let DoughNutChartSeriesData = preparePieChartData(getTopFiveKeysBySum(listOfInvoice))
      return {
        ...data, totalVessel,
        ClusterDataxAxis,
        totalAmount,
        ClusterDataSeries,
        DoughNutChartSeriesData,
        totalInvoice,
        piChartValue,
        statusAmount
      }
    })
  }
  function getTopFiveKeysBySum(data) {
    const keySums = {};
    for (const key in data) {
      const sum = data[key].reduce((acc, value) => acc + value, 0);
      keySums[key] = sum;
    }
    const sortedKeySums = Object.entries(keySums).sort((a, b) => b[1] - a[1]);
    if (isFilteractive.current) {
      return sortedKeySums.slice(0, 5).map(([key, sum]) => ({ key, sum }));
    }
    return sortedKeySums.map(([key, sum]) => ({ key, sum }));
  }
  function preparePieChartData(data) {
    const totalSum = data.reduce((acc, item) => acc + item.sum, 0);

    return data.map((item) => {
      const percentage = (item.sum / totalSum) * 100;
      return {
        value: Math.round(percentage), // Round percentage to avoid decimals
        name: item.key,
      };
    });
  }
  function convert_to_float(amount_str) {
    if (amount_str === "") {
      return 0.0;
    }
    // Remove commas and convert to float
    return parseFloat(amount_str.replace(",", ""));
  }
  function applyFilter(id, value) {
    appliedFilter.current[id] = value
    isFilteractive.current = appliedFilter.current.reduce((accumulator, currentValue) => {
      return accumulator && currentValue === "All";
    }, true)
    let filterdatag = allData.current.data.filter((item) => {
      let check = true
      for (let i = 0; i < appliedFilter.current.length; i++) {
        if (appliedFilter.current[i] !== "All") {
          if (i === 0 && item["vessel_name"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 1 && item["invoice_no"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 2 && item["account_type"] && item["account_type"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 3 && item["fia_status"].toLowerCase() !== appliedFilter.current[i].toLowerCase()) {
            check = false
          }
          if (i === 4 && new Date(item["invoice_date"]) < new Date(appliedFilter.current[i])) {
            check = false
          }
        }
      }
      return check
    });
    setFilterData(filterdatag);
  }
  const params = useParams();

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      // width: 100,
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["initial-freight-invoice-summary"]
        ? FIELDS["initial-freight-invoice-summary"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
    getAlldata()
  }, []);
  const getTableData = async (search = {}) => {
    const { pageOptions } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    if (params?.id && params.id.includes("TCOV-FULL")) {
      headers = {
        order_by: { id: "desc" },
        where: { OR: { voy_no: `'${params.id}'` } },
      };
    }

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/freight-invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const handleCanclePopup = () => setState({ ...state, showPopup: false });

  const closeModal = () =>
    setState({ ...state, showPopup: false }, () => getTableData());

  const redirectToAdd = async (e, record = null) => {
    let _url = `${URL_WITH_VERSION}/freight-invoice/edit?e=${record.invoice_no}`;
    const response = await getAPICall(_url);
    const respData = await response;
    if (respData["data"] && respData["data"].hasOwnProperty("id")) {
      setState((prevState) => ({
        ...prevState,
        popupData: respData["data"],
        showPopup: true,
      }));
    } else {
      openNotificationWithIcon("error", respData["message"]);
    }
  };

  const onCancel = () => {
    //getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  };

  const onRowDeletedClick = (record) => {
    if (record.fia_status === "PREPARED") {
      let _url = `${URL_WITH_VERSION}/freight-invoice/delete`;
      apiDeleteCall(_url, { id: record.id }, (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon("error", response.message);
        }
      });
    } else {
      openNotificationWithIcon(
        "info",
        "Please Change The Invoice Status To PREPARED"
      );
    }
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize = (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    isVisible,
    sidebarVisible,
    showPopup,
    popupData,
    oldFormData,
    voyID,
  } = state;

  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"freight-invoice-list-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph={props.voyID || showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "freinvo"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "freinvo"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "freinvo"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}

        {showPopup ? (
          <Modal
            style={{ top: "2%" }}
            title="Edit Freight Invoice "
            open={showPopup}
            onCancel={() => handleCanclePopup()}
            width="90%"
            footer={null}
          >
            <InitialFreightInvoice
              isEdit={true}
              modalCloseEvent={closeModal}
              formData={popupData}
              voyageData={oldFormData}
            />
          </Modal>
        ) : undefined}

        {
          <Modal
            title="Initial Freight Dashboard"
            open={isGraphModal}
            //  onOk={handleOk}
            width="90%"
            onCancel={handleCancel}
            footer={null}
          >
            <div className={style.upper_container}>
              <div className={style.sumation_conatienr}>
                <div className={style.summation}>
                  <div>Total Vessels</div>
                  <div>{modelData.totalVessel}</div>
                </div>
                <div className={style.summation}>
                  <div>Total Amount</div>
                  <div>{modelData.totalAmount.toFixed(3)} $</div>
                </div>
                <div className={style.summation}>
                  <div>Conut of invoice</div>
                  <div>{modelData.totalInvoice}</div>
                </div>
              </div>
              <div className={style.filter_conatiner}>
                <div >
                  <div style={{ margin: "0" }}>Vessel Name</div>
                  <Select
                    popupMatchSelectWidth={false}
                    placeholder="All"
                    optionFilterProp="children"
                    options={Object.keys(listOfVessel.current).map(data => {
                      return { value: data, label: data }
                    })}
                    onSelect={(data) => { applyFilter(0, data) }}></Select>
                </div>
                <div >
                  <div style={{ margin: "0" }}>Invoice No</div>
                  <Select
                    popupMatchSelectWidth={false}
                    placeholder="All"
                    optionFilterProp="children"
                    options={Object.keys(listOfInvoice.current).map(data => {
                      return { value: data, label: data }
                    })}
                    onSelect={(data) => { applyFilter(1, data) }}></Select>
                </div>
                <div

                >
                  <div style={{ margin: "0" }}>Account Type</div>
                  <Select
                    popupMatchSelectWidth={false}
                    placeholder="All"
                    optionFilterProp="children"
                    options={[
                      {
                        value: "All",
                        label: "All",
                      },
                      {
                        value: "Payable",
                        label: "Payable",
                      },
                      {
                        value: "Receivable",
                        label: "Receivable",
                      },
                    ]}
                    onSelect={(data) => { applyFilter(2, data) }}></Select>
                </div>
                <div >
                  <div style={{ margin: "0" }}>Status</div>
                  <Select
                    popupMatchSelectWidth={false}
                    placeholder="All"
                    optionFilterProp="children"
                    options={[
                      {
                        value: "All",
                        label: "All",
                      },
                      {
                        value: "APPROVED",
                        label: "APPROVED",
                      },
                      {
                        value: "POSTED",
                        label: "POSTED",
                      },
                      {
                        value: "VERIFIED",
                        label: "VERIFIED",
                      },
                    ]}
                    onSelect={(data) => { applyFilter(3, data) }}></Select>
                </div>
                <div >
                  <div style={{ margin: "0" }}>Date To</div>
                  <Select
                    popupMatchSelectWidth={false}
                    placeholder="All"
                    optionFilterProp="children"
                    options={Object.keys(listOfDate.current).map(data => {
                      return { value: data, label: data }
                    })}
                    onSelect={(data) => { applyFilter(4, data) }}></Select>
                </div>
              </div>
            </div>
            {/* charts  */}
            <div className={style.lower_container}>
              <div>
                <ClusterColumnChart
                  Heading={"Total Amount Per Vessel"}
                  ClusterDataxAxis={modelData.ClusterDataxAxis}
                  ClusterDataSeries={modelData.ClusterDataSeries}
                  maxValueyAxis={modelData.ClusterDataSeries ? Math.max(...modelData.ClusterDataSeries[0].data) : 0}
                />
              </div>
              <div>
                <DoughNutChart
                  DoughNutChartSeriesData={modelData.DoughNutChartSeriesData ? modelData.DoughNutChartSeriesData : []}
                  DoughNutChartSeriesRadius={["40%", "70%"]}
                  Heading={"Total Amount Per Vessel"}
                />
              </div>
              <div>
                <ClusterColumnChart
                  Heading={"Status wise Total Amount"}
                  ClusterDataxAxis={Object.keys(modelData.statusAmount)}
                  ClusterDataSeries={[
                    {
                      name: "Total Amount",
                      type: "bar",
                      barGap: 0,
                      data: Object.values(modelData.statusAmount),
                    },
                  ]}
                  maxValueyAxis={"350"}
                />
              </div>
              <div>
                <DoughNutChart
                  DoughNutChartSeriesData={modelData.piChartValue ? modelData.piChartValue : []}
                  DoughNutChartSeriesRadius={["40%", "70%"]}
                  Heading={"Total Amount per Account Type"}
                />
              </div>
            </div>
          </Modal>
        }
      </div>
    </>
  );
};

export default InitialFreightInvoiceSummary;
