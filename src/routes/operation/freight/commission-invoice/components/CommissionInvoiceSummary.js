import React, { Component, useState } from "react";
import { Table, Form, Input, Select, Modal, Popconfirm, Col, Row } from "antd";
import { EditOutlined } from "@ant-design/icons";
import CommissionInvoice from "./CommissionInvoice";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../../../../shared";
import { FIELDS } from "../../../../../shared/tableFields";
import ToolbarUI from "../../../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../../../shared/SidebarColumnFilter";
import { useEffect } from "react";
import PieChart from "../../../../dashboard/charts/PieChart";
import ClusterColumnChart from "../../../../dashboard/charts/ClusterColumnChart";
import DoughNutChart from "../../../../dashboard/charts/DoughNutChart";

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const CommissionInvoiceSummary = (props) => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    voyageData: props.voyageData || null,
    formDataValues: {},
    visible: false,
    showPopup: false,
    popupData: {},
    voyId: props.voyId,
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancelGraph = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      // width: 100,
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            commented As per discussion with Pallav sir
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["freight-commission-list"]
        ? FIELDS["freight-commission-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const showModal = () => {
    setState({
      ...state,
      visible: true,
    });
  };

  const handleOk = (e) => {
    // console.log(e);
    setState({
      ...state,
      visible: false,
    });
  };

  const handleCancel = (e) => {
    // console.log(e);
    setState({
      ...state,
      visible: false,
    });
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/freight-commission/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };
  const redirectToAdd = async (e, record = null) => {
    let _url = `${URL_WITH_VERSION}/freight-commission/edit?e=${record.invoice_no}`;
    const response = await getAPICall(_url);
    const respData = await response;
    let total_amt = 0;
    if (respData["data"] && respData["data"]["..."]) {
      respData["data"]["..."].map((e) => {
        total_amt += +e.commission_amount;
      });
    }

    respData["data"]["....."] = { total_amount: total_amt };

    if (respData["data"] && respData["data"].hasOwnProperty("id")) {
      setState((prevState) => ({
        ...prevState,
        popupData: respData["data"],
        showPopup: true,
      }));
    } else {
      openNotificationWithIcon("error", respData["message"]);
    }
  };

  const handleCanclePopup = () => {
    getTableData();
    setState({ ...state, showPopup: false });
  };

  const onRowDeletedClick = (record) => {
    if (record.fia_status === "PREPARED") {
      let _url = `${URL_WITH_VERSION}/freight-commission/delete`;
      apiDeleteCall(_url, { id: record.id }, (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon("error", response.message);
        }
      });
    } else {
      openNotificationWithIcon(
        "info",
        "Please Change The Invoice Status To PREPARED"
      );
    }
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    showPopup,
    voyId,
    popupData,
    voyageData,
    sidebarVisible,
  } = state;
  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "HEMINY",
    "Anr shipping",
    "VERITY",
    "DS SHIPPING",
    "ALKIVIADIS",
  ];
  const ClusterDataxAxisOne = ["PREPARED", "APPROVED", "POSTED", "VERIFIED"];
  const ClusterDataSeries = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [190, 100, 120, 80, 230, 130, 150],
    },
  ];
  const ClusterDataSeriesOne = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [120, 200, 150, 80, 70, 110, 130],
    },
  ];
  const PieChartData = [
    { value: 70, name: "RECEIVABLE" },
    { value: 30, name: "PAYABLE" },
  ];

  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];
  const DoughNutChartSeriesData = [
    { value: 30, name: "FRT-COM-00004" },
    { value: 13, name: "FRT-COM-00003" },
    { value: 20, name: "FRT-COM-00342" },
    { value: 10, name: "FRT-COM-00338" },
    { value: 7, name: "FRT-COM-00336" },
  ];

  const [pieChartOption, setPieChartOption] = useState({
    bar1: {
      option: {
        tooltip: {
          trigger: "item",
          formatter: "{a} <br/>{b}: {c}%",
        },
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 5,
          containLabel: true,
        },
        legend: {
          orient: "vertical",
          left: "left",
        },
        series: [
          {
            type: "pie",
            radius: "65%",
            // selectedMode: selectedMode,
            name: "Total Amount",
            data: PieChartData,
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
          },
        ],
      },
      legend: {
        orient: "horizontal",
        left: "left",
      },
      labelLine: {
        show: true,
        length: 20,
        length2: 20,
      },
    },
  });

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Freight Commission List</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"freight-commission-list-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph= {props.voyID || showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "freicomm"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "freicomm"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "freicomm"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>
        {showPopup ? (
          <Modal
            style={{ top: "2%" }}
            title="Edit Commission Invoice"
            open={showPopup}
            onCancel={handleCanclePopup}
            width="90%"
            footer={null}
          >
            <CommissionInvoice
              modalCloseEvent={handleCanclePopup}
              formData={popupData}
              voyagedata={voyageData}
              voyId={popupData.voyage_id}
            />
          </Modal>
        ) : undefined}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
        <Modal
          title="Freight Commission Dashboard"
          open={state.isGraphModal}
          //  onOk={handleOk}
          width="90%"
          onCancel={handleCancelGraph}
          footer={null}
        >
          <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Vessels</p>
                <p>300</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Amount</p>
                <p>300 $ </p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Invoice</p>
                <p>240</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Vessel Name</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Invoice No</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Account Type</p>
              <Select
                placeholder="Payable"
                optionFilterProp="children"
                options={[
                  {
                    value: "Payable",
                    label: "Payable",
                  },
                  {
                    value: "Receivable",
                    label: "Receivable",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Status</p>
              <Select
                placeholder="PREPARED"
                optionFilterProp="children"
                options={[
                  {
                    value: "PREPARED",
                    label: "PREPARED",
                  },
                  {
                    value: "APPROVED",
                    label: "APPROVED",
                  },
                  {
                    value: "POSTED",
                    label: "POSTED",
                  },
                  {
                    value: "VERIFIED",
                    label: "VERIFIED",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Date To</p>
              <Select
                placeholder="2024-02-18"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-26",
                    label: "2024-01-26",
                  },
                  {
                    value: "2023-11-16",
                    label: "2023-11-16",
                  },
                  {
                    value: "2023-11-17",
                    label: "2023-11-17",
                  },
                ]}
              ></Select>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount Per Vessel"}
                ClusterDataxAxis={ClusterDataxAxis}
                ClusterDataSeries={ClusterDataSeries}
                maxValueyAxis={"200"}
              />
            </Col>
            <Col span={12}>
              <DoughNutChart
                DoughNutChartSeriesData={DoughNutChartSeriesData}
                DoughNutChartSeriesRadius={["40%", "70%"]}
                Heading={"Total Amount Per Invoice Number"}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Invoice status Per Amount"}
                ClusterDataxAxis={ClusterDataxAxisOne}
                ClusterDataSeries={ClusterDataSeriesOne}
                maxValueyAxis={"200"}
              />
            </Col>
            <Col span={12}>
              <PieChart
                PieChartData={PieChartData}
                Heading={"Comm.  Amount per Account Type"}
                pieChartOptions={pieChartOption}
              />
            </Col>
          </Row>
        </Modal>
      </div>
    </>
  );
};

export default CommissionInvoiceSummary;
