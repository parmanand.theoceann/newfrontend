import React, { Component, useEffect } from "react";
import { Modal } from "antd";
import CreateInvoice from "../../../../create-invoice/CreateInvoice";
import FreightCommissionReport from "../../../../operation-reports/FreightCommissionReport";
//import Tde from "../../../../tde/Tde";
import Tde from "../../../../tde/";
import NormalFormIndex from "../../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  getAPICall,
  openNotificationWithIcon,
  apiDeleteCall,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../../shared";
import InvoicePopup from "../../../../create-invoice/InvoicePopup";
import { DeleteOutlined, SaveOutlined,EditOutlined } from "@ant-design/icons";
import Attachment from "../../../../../shared/components/Attachment";
import Remarks from "../../../../../shared/components/Remarks";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../../shared/attachments";

const  CommissionInvoice = (props) => {
  console.log("props are ------:",props)
  const [state, setState] = useStateCallback({
    modals: {
      InvoiceModal: false,
      TdeShow: false,
      FreightCommissionReport: false,
    },
    loadFrom: false,
    frmName: "freight-commission-invoice",
    formData: props.formData || {},
    frmOptions: [],
    voyageData: props.voyagedata || {},
    TdeList: props.formData && props.formData.invoice_no,
    invoiceReport: null,
    popupdata: null,
    isRemarkModel: false,
    voyid: props.voyId,
  })

  useEffect(() => {
    getCommisiondata()
  }, [])

  const getCommisiondata = async () => {
    const { voyageData, formData, voyid } = state;

    let _formData = Object.assign({}, formData);
    if (
      _formData &&
      !_formData.hasOwnProperty("id") &&
      voyageData &&
      voyageData.hasOwnProperty("id") &&
      voyageData["id"] > 0
    ) {
      const request = await getAPICall(
        `${URL_WITH_VERSION}/freight-commission/initial?e=${
          voyageData["voyage_number"]
        }`
      );
      const respData = await request;
      if (respData["data"]) {
        let brokerOptions = Object.assign(
          [],
          respData["data"]["broker_options"]
        );
        delete respData["data"]["broker_options"];
        _formData = respData["data"];
        setState(prevState => ({
          ...prevState,
          formData: _formData,
          frmOptions: [{ key: "broker", data: brokerOptions }],
          loadFrom: true
        }));
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    } else if (
      _formData &&
      _formData.hasOwnProperty("id") &&
      _formData["id"] > 0
    ) {
      const request = await getAPICall(
        `${URL_WITH_VERSION}/freight-commission/initial?ae=${voyid}`
      );
      const respData = await request;
      if (respData["data"]) {
        let brokerOptions = Object.assign(
          [],
          respData["data"]["broker_options"]
        );
        setState(prevState => ({
          ...prevState,
          formData: _formData,
          frmOptions: [{ key: "broker", data: brokerOptions }],
          loadFrom: true
        }));
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    }
  };

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {};
    _modal[modal] = visible;
    setState(prevState => ({ ...prevState, modals: Object.assign(modals, _modal) }));
  };

  const openTdeModal = async (visible, modal) => {
    const { formData, modals, voyageData } = state;
    let tde_id = 0;
    let _modal = {};
    let tdeEditData = {};
    _modal[modal] = visible;

    const responseData = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${formData["broker"]}`);
    const responseAddressData = responseData["data"];

    let account_no =
      responseAddressData &&
      responseAddressData["bank&accountdetails"] &&
      responseAddressData["bank&accountdetails"].length > 0
        ? responseAddressData["bank&accountdetails"][0] &&
          responseAddressData["bank&accountdetails"][0]["account_no"]
        : "";

    let swiftcode =
      responseAddressData &&
      responseAddressData["bank&accountdetails"] &&
      responseAddressData["bank&accountdetails"].length > 0
        ? responseAddressData["bank&accountdetails"][0] &&
          responseAddressData["bank&accountdetails"][0]["swift_code"]
        : "";

    const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
    const responseTdeData = response["data"];
    const TDE_LIST = responseTdeData;
    const Tde_List = TDE_LIST && TDE_LIST.length > 0 ? TDE_LIST.filter((el) => formData["invoice_no"] === el.invoice_no): [];

    if (Tde_List.length > 0) {
      tde_id = Tde_List[0]["id"];
      const editData = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${tde_id}`);
      tdeEditData = await editData["data"];
      tdeEditData["ap_ar_acct"] = account_no;
      tdeEditData["lob"] = props.company_lob && props.company_lob;
    } else {
      Object.assign(formData, formData, {
        ap_ar_acct: account_no,
        swift_code: swiftcode,
        bill_via: voyageData.my_company_lob && voyageData.my_company_lob,
        lob: voyageData.company_lob && voyageData.company_lob,
      });
    }
    if (formData["invoice_no"]) {
      setState(prevState => ({...prevState,modals: Object.assign(modals, _modal),TdeList: Tde_List != null && Tde_List.length > 0 ? tdeEditData : null,}));
    } else {
      openNotificationWithIcon("error",`Without "Invoice No." you cant't access TDE Form`);
    }
  };

  const saveFormData = async (data) => {
    if (data) {
      setState(prevState => ({ ...prevState, loadFrom: false }));
      let _url = `${URL_WITH_VERSION}/freight-commission/save?frm=${
        state.frmName
      }`;
      let _method = "POST";
      if (data["id"] && data["id"] > 0) {
        _url = `${URL_WITH_VERSION}/freight-commission/update?frm=${
          state.frmName
        }`;
        _method = "PUT";
      }

      try {
        await postAPICall(`${_url}`, data, _method, (response) => {
          if (response && response.data === true) {
            openNotificationWithIcon("success", response.message, 2);
            setState(prevState => ({ ...prevState, loadFrom: true }));
            setTimeout(() => {
              if (
                props.modalCloseEvent &&
                typeof props.modalCloseEvent === "function"
              ) {
                props.modalCloseEvent();
              }
            }, 3000);
          } else if (response && response.data === false) {
            setState(prevState => ({ ...prevState, formData: data, loadFrom: true }));
            if (typeof response.message === "string") {
              openNotificationWithIcon("error", response.message);
            } else {
              openNotificationWithIcon("error", response.message);
            }
          }
        });
      } catch (error) {
        openNotificationWithIcon("error", "Something went wrong.", 3);
      }
    }
  };

  const openFreightCommissionReport = async (showFreightCommissionReport) => {
    if (showFreightCommissionReport === true) {
      try {
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/freight-commission/report?e=${
            state.TdeList
          }`
        );
        const respDataReport = await responseReport["data"];
        // Reportref.current = respDataReport
        if (respDataReport) {
          setState(prevState => ({ ...prevState, reportFormData: respDataReport, isShowFreightCommissionReport: showFreightCommissionReport }))
          ;
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong.", 3);
      }
    } else {
      setState(prevState => ({
        ...prevState,
        isShowFreightCommissionReport: showFreightCommissionReport,
      }));
    }
  };

  const _onDeleteFormData = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => deleteInvoice(postData),
    });
  };

  const deleteInvoice = (data) => {
    if (data) {
      apiDeleteCall(
        `${URL_WITH_VERSION}/freight-commission/delete`,
        { id: data.id },
        (resp) => {
          if (
            resp["data"] &&
            props.modalCloseEvent &&
            typeof props.modalCloseEvent === "function"
          ) {
            props.modalCloseEvent();
          }
          openNotificationWithIcon(
            resp["data"] === true ? "success" : "error",
            resp["message"],
            5
          );
        }
      );
    }
  };

  const invoiceModal = async (data = {}) => {
    let { formData, invoiceReport } = state;
    try {
      if (Object.keys(data).length === 0) {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/freight-commission/invoice-report?e=${
            formData.invoice_no
          }`
        );
        const respData = await response["data"];
        if (respData) {
          setState(prevState => ({
            ...prevState,
            invoiceReport: respData,
            popupdata: respData,
          }));
          showHideModal(true, "InvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry, Unable to Show Invoice", 3);
        }
      } else {
        setState(prevState => ({
          ...prevState,
          invoiceReport: { ...invoiceReport, ...data },
        }));
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  const handleok = () => {
    const { invoiceReport } = state;

    if (invoiceReport["isSaved"]) {
      showHideModal(false, "InvoicePopup");
      setTimeout(() => {
        showHideModal(true, "InvoiceModal");
      }, 2000);
      setState(prevState => ({ ...prevState, invoiceReport: invoiceReport }));
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  const onClickExtraIcon = async (action, data) => {
    let groupKey = action["gKey"];
    let frm_code = "freight-commission-invoice";

    let delete_id = data && data.id;
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey.replace(/\s/g, "").toLowerCase(),
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

    const {
      frmName,
      loadFrom,
      formData,
      frmOptions,
      TdeList,
      isShowFreightCommissionReport,
      invoiceReport,
      reportFormData,
      popupdata,
      isRemarkModel
    } = state;

    const ShowAttachment = async (isShowAttachment) => {
      let loadComponent = undefined;
      const { id } = state.formData;
      if (id && isShowAttachment) {
        const attachments = await getAttachments(id, "EST");
        const callback = (fileArr) =>
          uploadAttachment(fileArr, id, "EST", "port-expense");
        loadComponent = (
          <Attachment
            uploadType="Estimates"
            attachments={attachments}
            onCloseUploadFileArray={callback}
            deleteAttachment={(file) =>
              deleteAttachment(file.url, file.name, "EST", "port-expense")
            }
            tableId={0}
          />
        );
        setState((prevState) => ({
          ...prevState,
          isShowAttachment: isShowAttachment,
          loadComponent: loadComponent,
        }));
      } else {
        setState((prevState) => ({
          ...prevState,
          isShowAttachment: isShowAttachment,
          loadComponent: undefined,
        }));
      }
    };

    const handleRemark = () => {
      setState(prevState => ({
        ...prevState,
        isRemarkModel: true,
      }));
    }

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              {loadFrom ? (
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  showForm={true}
                  frmCode={frmName}
                  formData={formData}
                  frmOptions={frmOptions}
                  addForm={true}
                  inlineLayout={true}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        {
                          isSets: [
                            {
                              id: "3",
                              key: "save",
                              type: <SaveOutlined />,
                              withText: "Save",
                              showToolTip: true,
                              event: (key, data) => saveFormData(data),
                            },
                            formData["id"] && {
                              id: "4",
                              key: "delete",
                              type: <DeleteOutlined />,
                              withText: "Delete",
                              showToolTip: true,
                              event: (key, data) =>
                                _onDeleteFormData(data),
                            },

                            {
                              id: "5",
                              key: "edit",
                              type: <EditOutlined/>,
                              withText: "",
                              showToolTip: true,
                              event: (key, data) => handleRemark(),
                            },
                          ],
                        },
                      ],
                      isRightBtn: [
                        {
                          isSets: [
                            formData["id"] && {
                              key: "tde",
                              isDropdown: 0,
                              withText: "TDE",
                              type: "",
                              menus: null,
                              event: () => {
                                //openTdeModal(true, "TdeShow")
                                if(formData?.invoice_no) {
                                  showHideModal(true,"TdeShow");
                                }else {
                                  openNotificationWithIcon("error",`Without "Invoice No." you cant't access TDE Form`);
                                }
                              },
                            },
                            formData["id"] && {
                              key: "invoice",
                              isDropdown: 0,
                              withText: "Create Invoice",
                              type: "",
                              menus: null,
                              event: () => invoiceModal(),
                            },
                            {
                              key: "report",
                              isDropdown: 0,
                              withText: "Report",
                              type: "",
                              menus: null,
                              event: (key) =>
                              openFreightCommissionReport(true),
                            },
                            {
                              key: "attachment",
                              isDropdown: 0,
                              withText: "Attachment",
                              type: "",
                              menus: null,
                              event: (key, data) => {
                                data &&
                                data.hasOwnProperty("id") &&
                                data["id"] > 0
                                  ? ShowAttachment(true)
                                  : openNotificationWithIcon(
                                      "info",
                                      "Please save Commission Invoice First.",
                                      3
                                    );
                              },
                            },
                          ],
                        },
                      ],
                    },
                  ]}
                  isShowFixedColumn={["..."]}
                  tableRowDeleteAction={(action, data) =>
                    onClickExtraIcon(action, data)
                  }
                  summary={[
                    { gKey: "...", showTotalFor: ["commission_amount"] },
                  ]}
                />
              ) : (
                undefined
              )}
            </div>
          </div>
        </article>
        {state.modals["InvoiceModal"] && (
          <Modal
            style={{ top: "2%" }}
            title="Invoice"
           open={state.modals["InvoiceModal"]}
            onCancel={() => showHideModal(false, "InvoiceModal")}
            width="95%"
            footer={null}
          >
            <CreateInvoice
              type="frightCommisionInvoice"
              frightCommisionInvoice={invoiceReport}
            />
          </Modal>
        )}

        {state.modals["InvoicePopup"] ? (
          <Modal
            style={{ top: "2%" }}
            title="Invoice"
            open={state.modals["InvoicePopup"]}
            onCancel={() => showHideModal(false, "InvoicePopup")}
            width="95%"
            okText="Create PDF"
            onOk={handleok}
          >
            <InvoicePopup
              data={popupdata}
              updatepopup={(data) => invoiceModal(data)}
            />
          </Modal>
        ) : (
          undefined
        )}

        <Modal
          style={{ top: "2%" }}
          title="Report"
          open={isShowFreightCommissionReport}
          onOk={handleok}
          onCancel={() => openFreightCommissionReport(false)}
          width="95%"
          footer={null}
        >
          <FreightCommissionReport data={reportFormData} />
        </Modal>

        {state.modals["TdeShow"] ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="TDE"
            open={state.modals["TdeShow"]}
            onCancel={() => showHideModal(false, "TdeShow")}
            width="95%"
            footer={null}
          >
            <Tde
              invoiceType="commission_invoice"
              //isEdit={TdeList === null ? false : true}
              //TdeList={TdeList}
              //formData={TdeList === null ? formData : TdeList}
               formData={formData}
              //saveUpdateClose={() => showHideModal(false, "TdeShow")}
              invoiceNo={formData.invoice_no}
            />
          </Modal>
        ) : (
          undefined
        )}
        {state.isShowAttachment ? (
          <Modal
            style={{ top: "2%" }}
            title="Upload Attachment"
            open={state.isShowAttachment}
            onCancel={() => ShowAttachment(false)}
            width="50%"
            footer={null}
          >
            {state.loadComponent}
          </Modal>
        ) : undefined}

{isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() => setState(prevState => ({ ...prevState, isRemarkModel: false }))}
          footer={false}
        >
          <Remarks
            remarksID={formData.invoice_no}
            remarkType="freight-commission"
            remarkVoyId={formData.voyage_id}
            remarkInvNo={formData.invoice_no}
          />


        </Modal>
      )}
      </div>
    );
  }

export default CommissionInvoice;
