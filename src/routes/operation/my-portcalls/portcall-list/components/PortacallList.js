import React from 'react';

const Page = () => {
  return (
    <div className="body-wrapper">
    <article className="article">
        <div className="box box-default">
            <div className="box-body">
                Port Call List
            </div>
        </div>
    </article>
</div>
  );
}

export default Page;