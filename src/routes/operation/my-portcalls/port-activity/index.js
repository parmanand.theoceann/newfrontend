import React, { Component } from 'react';
import PortActivities from './port-activities';
import TableListing from './table-listing';

import { Form, Input, Button, Tabs, DatePicker } from 'antd';

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const InputGroup = Input.Group;

function onChange(date, dateString) {
    // console.log(date, dateString);
}

class PortActivity extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 1,
        }
    }

    onChange = (e) => {
        // console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }

    render() {
        return (
            <div className="body-wrapper">
            
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Form>

                                <div className="form-wrapper">
                                    <div className="form-heading">
                                        <h4 className="title"><span>Port Activities</span></h4>
                                    </div>
                                    <div className="action-btn">
                                        <Button type="primary" htmlType="submit">Save</Button>
                                        <Button>Reset</Button>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Prev Port">
                                            <Input size="default" placeholder="Colombo" disabled />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Observed Distance">
                                            <Input size="default" placeholder="0" />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Steam Hours">
                                            <Input size="default" placeholder="101.23" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Average Speed">
                                            <Input size="default" placeholder="0.00" disabled />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Curr Port">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Distance To Go From Dep">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Projected Spd">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Distination">
                                            <Input size="default" placeholder="Singapore" disabled />
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Function">
                                            <InputGroup compact>
                                                <Input style={{ width: '80%' }} defaultValue="Loading" />
                                                <Input style={{ width: '20%' }} defaultValue="GMT" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Arrival">
                                            <InputGroup compact>
                                                <DatePicker style={{ width: '80%' }} onChange={onChange} disabled placeholder="31/03/2021 12:11 PM" />
                                                <Input style={{ width: '20%' }} defaultValue="5.5" disabled />
                                            </InputGroup>
                                        </FormItem>
                                    </div>
                                    <div className="col-md-4">
                                        <FormItem label="Departure">
                                            <InputGroup compact>
                                                <DatePicker style={{ width: '80%' }} onChange={onChange} disabled placeholder="31/03/2021 12:11 PM" />
                                                <Input style={{ width: '20%' }} defaultValue="5.5" disabled />
                                            </InputGroup>
                                        </FormItem>
                                    </div>
                                </div>

                                <hr />

                                <div className="row">
                                    <div className="col-md-12">
                                        <TableListing />
                                    </div>
                                </div>

                            </Form>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <Tabs defaultActiveKey="portactivities" size="small">
                                        <TabPane tab="Port Activities" key="portactivities">
                                            <PortActivities />
                                        </TabPane>
                                        <TabPane tab="Sort Activities" key="sortactivities">Sort Activities</TabPane>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

            </div>
        )
    }
}

export default PortActivity;