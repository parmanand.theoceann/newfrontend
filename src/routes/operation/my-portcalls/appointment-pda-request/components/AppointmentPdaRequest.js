import React from 'react';
import { FileOutlined } from '@ant-design/icons';



class AppointmentPdaRequest extends React.Component {
  render() {
    return (
      <div className="body-wrapper">
        <div className="wrap-xml-area">
          <article className="article">
            <div className="box box-default">
              <div className="card-style">
                <div className="equal-space">
                  <div className="style">
                    <div>Pro-Forma Disbursement Account Request</div>
                  </div>
                  <div className="style">
                    <div>TO : Demo Shipping Mumbai</div>
                    <div>SUBJECT : Appointment with PDA request</div>
                    <div>55666405</div>
                    <div>Please provide us PDA for the subject call</div>
                  </div>
                </div>
                <div className="equal-space wrap-btn-icons">
                  <div>
                    {/* <img alt="export" src="../../assets/images/export.svg"  /> */}
                  </div>
                  <div>Suman Singapore</div>
                  <div>Singapore</div>
                  <div>04748393382828</div>
                  <div>04748393382828</div>
                  <div>www.gde@gmail.com</div>
                </div>
              </div>
            </div>
          </article>

          <div className="">
            <div className="wrap-card-area p-r-20">

              <article className="article">
                <div className="box box-default">
                  <div className="card-style">
                    <div className="card-head">Vessel Details</div>
                    <div className="card-content">
                      <div className="wrap-label-vale">
                        <div className="label-ui">Vessel Name</div>
                        <div className="value-ui"> : morning crystal</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Vessel Type</div>
                        <div className="value-ui"> : Corgo</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">DWT</div>
                        <div className="value-ui"> : 34555</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">GRT</div>
                        <div className="value-ui"> : 34555</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">NRT</div>
                        <div className="value-ui"> : 34555</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Call Sign</div>
                        <div className="value-ui"> : 34555</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">MMSI No.</div>
                        <div className="value-ui"> : </div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">IMO No.</div>
                        <div className="value-ui"> : 344555</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Panama Canel Tonnage</div>
                        <div className="value-ui"> : </div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Swez Canal NT</div>
                        <div className="value-ui"> : </div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Swez Canal GT</div>
                        <div className="value-ui"> : </div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Official No.</div>
                        <div className="value-ui"> : </div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

              <article className="article">
                <div className="box box-default">
                  <div className="card-style">
                    <div className="card-head">Corgo / Activity Details </div>
                    <div className="card-content">
                      <div className="wrap-label-vale">
                        <div className="label-ui">Charterer's Agent</div>
                        <div className="value-ui"> : Agent list</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Quantity</div>
                        <div className="value-ui"> : 10000 - 20000</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Activity</div>
                        <div className="value-ui"> : Loading</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Name Of Corgo</div>
                        <div className="value-ui"> : Coal</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">CP Stage</div>
                        <div className="value-ui"> : 21/22/0202</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">CP Place</div>
                        <div className="value-ui"> : 21/22/0202</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Laycan Start</div>
                        <div className="value-ui"> : 21/22/0202</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Laycan End</div>
                        <div className="value-ui"> : 21/22/0202</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Remarks</div>
                        <div className="value-ui"> : </div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

            </div>

            <div className="wrap-card-area">

              <article className="article">
                <div className="box box-default">
                  <div className="card-style">
                    <div className="card-head">PortCall Details</div>
                    <div className="card-content">
                      <div className="wrap-label-vale">
                        <div className="label-ui">Country</div>
                        <div className="value-ui"> : Ind</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Port</div>
                        <div className="value-ui"> : Ind</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Voyage No.</div>
                        <div className="value-ui"> : 12333</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">ETA</div>
                        <div className="value-ui"> : 4 sep 2020</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">ETD</div>
                        <div className="value-ui"> : 5 sep 2020</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Next Port</div>
                        <div className="value-ui"> : Mumbai</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Previous Port</div>
                        <div className="value-ui"> : aust</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Arr.Draft (m)</div>
                        <div className="value-ui"> : SW:12m 12cm</div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

              <article className="article">
                <div className="box box-default">
                  <div className="card-style">
                    <div className="card-head">Person(s) Contact Details</div>
                    <div className="card-content">
                      <div className="wrap-label-vale">
                        <div className="label-ui">Name</div>
                        <div className="value-ui"> : sbms001</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Phone No.</div>
                        <div className="value-ui"> : 643333222222</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Fax No.</div>
                        <div className="value-ui"> : 956382822838</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Home Phone</div>
                        <div className="value-ui"> : 45554322222</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Mobile</div>
                        <div className="value-ui"> : 45554322222</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">E-mail Id</div>
                        <div className="value-ui"> : pallave@gmail.com</div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

              <article className="article">
                <div className="box box-default">
                  <div className="card-style">
                    <div className="card-head">Third Party Contact Details</div>
                    <div className="card-content">
                      <div className="wrap-label-vale">
                        <div className="label-ui">Charterer Address</div>
                        <div className="value-ui"> : N/A</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Charterer City</div>
                        <div className="value-ui"> : N/A</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Charterer Country</div>
                        <div className="value-ui"> : N/A</div>
                      </div>
                      <div className="wrap-label-vale">
                        <div className="label-ui">Charterer Phone</div>
                        <div className="value-ui"> : N/A</div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

            </div>

            <article className="article">
              <div className="box box-default">
                <div className="card-style">
                  <div className="card-head">Agreements/ Contracts</div>
                  <div className="card-content">
                    <div className="wrap-label-vale">
                      <div className="label-ui list-label-ui">General Instruction Set<FileOutlined /></div>
                      <div className="value-ui list-value-ui"> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</div>
                    </div>
                    <div className="wrap-label-vale">
                      <div className="label-ui list-label-ui">OBL</div>
                      <div className="value-ui list-value-ui"> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</div>
                    </div>
                    <div className="wrap-label-vale">
                      <div className="label-ui list-label-ui">NOR</div>
                      <div className="value-ui list-value-ui"> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</div>
                    </div>
                  </div>
                </div>
              </div>
            </article>

            <article className="article">
              <div className="box box-default">
                <div className="card-style">
                  <div className="card-head">Regarding Instruction</div>
                  <div className="card-content">
                    <div className="wrap-label-vale">
                      <b> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</b>
                    </div>
                  </div>
                </div>
              </div>
            </article>

          </div>

        </div>
      </div>
    )
  }
}
export default AppointmentPdaRequest