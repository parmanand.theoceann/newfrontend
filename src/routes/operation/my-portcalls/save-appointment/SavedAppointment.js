import React, { Component } from 'react';
import {  Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import { FIELDS } from '../../../../shared/tableFields';
import { Link } from 'react-router-dom';
import ToolbarUI from '../../../../components/ToolbarUI/index';
import { Modal } from 'antd';
import AgencyAppointment from '../../../operation/my-portcalls/agency-appointment/components/AgencyAppointment';

import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from '../../../../shared';



class SavedAppointment extends Component {
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => this.redirectToAdd(e, record, true)}>
            <EditOutlined/>

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
              <DeleteOutlined />

              </Popconfirm>
            </span>
          </div>
        )
      }
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['myport-calls-list'] ? FIELDS['myport-calls-list']['tableheads'] : []
    );

    tableHeaders.push(tableAction)


    this.state = {
      voyID: this.props.voyID || 0,
      loading: true,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 0, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };

  getTableData = async (search = {}) => {
    const { pageOptions, voyID } = this.state;
    let _url = `${URL_WITH_VERSION}/port-call/list`/*, state = {}*/;
    let qParamString = objectToQueryStringFunc({ "p": pageOptions.pageIndex, "l": pageOptions.pageLimit });
    let headers = { "order_by": { "id": 'desc' } };

    if (voyID && voyID !== "" && voyID !== 0) {
      headers["where"] = {"id": this.state.voyID}
      _url = [_url, voyID].join('/');
    }

    const request = await getAPICall(_url+`?${qParamString}`, headers);
    const resp = await request;

    if (resp && resp['data'] && resp['data'].length > 0) {
      if(resp.data.length > 0){
        resp.data.map(e=>{
          if(e.port_cargo_activity){
            e['port_cargo_activity'] =  e.port_cargo_activity.replace(/['"]+/g, '')
            e['port_cargo_activity'] =  e.port_cargo_activity.replace("[", '')
            e['port_cargo_activity'] =  e.port_cargo_activity.replace("]", '')
           }
          return true; 
      })
    }
      this.setState({
        ...this.state,
        'responseData': resp.data,
        'pageOptions': {
          'pageIndex': pageOptions.pageIndex,
          'pageLimit': pageOptions.pageLimit,
        }
      }, () => this.setState({...this.state, loading: false }));
    } else {
      openNotificationWithIcon('error', resp['message']);
    }
  };

  redirectToAdd = async (e, id, boolVal) =>{
    const respedtPort = await getAPICall(`${URL_WITH_VERSION}/port-call/edit?e=${id.id}`);
    const portEdit = await respedtPort['data'];
    this.setState({ ...this.state, formDataValues: portEdit,  isVisible: boolVal  });
  }
  redirectTcancel = ()=>{
    this.setState({ ...this.state, isVisible: false  });
    
  }

  onRowDeletedClick = id => {
    let _url = `${URL_WITH_VERSION}/port-call/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  onEditFormData = async data => {
    let { isAdd } = this.state;
    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/port-call/update?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'put', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
      this.setState({ ...this.state, isAdd: false, formDataValues: {} }, () =>
        this.setState({ ...this.state, isVisible: false })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  render() {
    const {
      voyID,
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isVisible,
      //sidebarVisible,
      formDataValues,

    } = this.state;


    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body pt-3 pb-3">
              <div className="form-heading">
                <h4 className="title"><span>Port-call List</span></h4>
              </div>
              <div className="action-btn">
                <Link to= "add-voyage-estimate">Add Port</Link>
              </div>
              {
                loading === false ?
                  <>
                    <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                      <ToolbarUI routeUrl={'ports-list-toolbar'} optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }} callback={(e) => this.callOptions(e)} />
                    </div>
                    <Table
                      rowKey={record => record.id}
                      className="inlineTable editableFixedHeader resizeableTable"
                      bordered
                      scroll={{ x: 'max-content' }}
                      // scroll={{ x: 1200, y: 370 }}
                      columns={tableColumns}
                      // size="small"
                      components={this.components}
                      dataSource={responseData}
                      loading={loading}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                    />
                  </>
                : undefined
              }
            </div>
          </div>
        </article>
        <Modal
          style={{ top: '2%' }}
          title="vessel-Port"
         open={isVisible}
          onOk={this.handleOk}
          onCancel={this.redirectTcancel}
          width="90%"
          footer={null}
        >
          <AgencyAppointment  voyID={voyID} editactive={true} editdata={formDataValues} cancelModel={this.redirectTcancel}/>
        </Modal>
      </div>
    );
  }
}

export default SavedAppointment;


