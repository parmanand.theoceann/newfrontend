import React from 'react';
import { Table, Input, Row, Col, Button, Tabs, Form, DatePicker, Select ,Menu, Dropdown} from 'antd';

import {PlusOutlined,EditOutlined,SearchOutlined,DownOutlined,IdcardOutlined,SaveOutlined,DeleteOutlined } from '@ant-design/icons' ;





const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const { TextArea } = Input;

const menu = (
  <Menu>
    <Menu.Item>
      <a>1st menu item</a>
    </Menu.Item>
  </Menu>
);

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const dataSource = [{
  key: '1',
  ledger: 'corgo',
  description: 'PDA advance',
  usd: '0.00',
  act: '0.00',
  diff: '0.00',
  e_tax: '0.00',
  est_tax: '0.00',
  a_tax: '0.00',
  act_tax: '0.00',
}];

const columns = [{
  title: 'Ledger',
  dataIndex: 'ledger',
  key: 'ledger',
}, {
  title: 'Description',
  dataIndex: 'description',
  key: 'description',
}, {
  title: 'EST In USD',
  dataIndex: 'usd',
  key: 'usd',
}, {
  title: 'Act In USD',
  dataIndex: 'act',
  key: 'act',
}, {
  title: 'Diff In USD',
  dataIndex: 'diff',
  key: 'diff',
}, {
  title: 'E Tax %',
  dataIndex: 'e_tax',
  key: 'e_tax',
}, {
  title: 'Est Tax',
  dataIndex: 'est_tax',
  key: 'est_tax',
}, {
  title: 'A Tax %',
  dataIndex: 'a_tax',
  key: 'a_tax',
}, {
  title: 'Act Tax',
  dataIndex: 'act_tax',
  key: 'act_tax',
}];

const PerformaDa = () => {
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <Row>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li><PlusOutlined /></li>
                    <li><SearchOutlined /></li>
                    <li><SaveOutlined />
                      <span className="text-bt">Save</span>
                    </li>
                  </ul>
                </span>
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li><DeleteOutlined /></li>
                  </ul>
                </span>
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li><EditOutlined />
                      <span className="text-bt">Ledger Expence Setup</span></li>
                    <li><IdcardOutlined />
                      <Dropdown overlay={menu}>
                        <span className="text-bt">Reports<DownOutlined /></span>
                      </Dropdown></li>
                    <li>
                      <span className="text-bt">Attachments </span></li>
                  </ul>
                </span>
              </Col>
            </Row>
          </div>
        </div>
      </article>

      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <Form>
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span></span></h4>
                </div>
                <div className="action-btn">
                  <Button type="primary" htmlType="submit">Save</Button>
                  <Button>Reset</Button>
                </div>
              </div>
              <Row gutter={16}>
                <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                  <FormItem
                    {...formItemLayout}
                    label="Vesssel"
                  >
                    <Row gutter={16}>
                      <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Input placeholder="Vesssel" defaultValue="Golden Rose" disabled />
                      </Col>
                      <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Input placeholder="No." defaultValue="D204422" disabled />
                      </Col>
                    </Row>
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Voy No."
                  >
                    <Input placeholder="Voy No." defaultValue="1" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Port"
                  >
                    <Row gutter={16}>
                      <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Input placeholder="Port" defaultValue="Singapore" disabled />
                      </Col>
                      <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                        <label>No.</label>
                      </Col>
                      <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                        <Input placeholder="No." defaultValue="12" disabled />
                      </Col>
                    </Row>
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Port Func"
                  >
                    <Input placeholder="Port Func" defaultValue="Fueling" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Arrival"
                  >
                    <DatePicker disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Departure"
                  >
                    <DatePicker disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Agent Short"
                  >
                    <Input placeholder="Agent Short" defaultValue="Andrew Moore" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Full Name"
                  >
                    <Input placeholder="Full Name" defaultValue="Andrew Moore & Associate" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Rem Bank"
                  >
                    <Select defaultValue="1">
                      <Option value="1">The Hongkong</Option>
                    </Select>
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                  <FormItem
                    {...formItemLayout}
                    label="Advance Inv No."
                  >
                    <Input placeholder="Advance Inv No." />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="PDA Status"
                  >
                    <Input placeholder="PDA Status" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Advance Sent"
                  >
                    <Input placeholder="Advance Sent" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Reference"
                  >
                    <Input placeholder="Reference" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Advance Currency"
                  >
                    <Input placeholder="Advance Currency" defaultValue="USD" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Advance Payment Terms"
                  >
                    <Input placeholder="Advance Payment Terms" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Advance Inv Due"
                  >
                    <Input placeholder="Advance Inv Due" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Exchange Rate"
                  >
                    <Input placeholder="Exchange Rate" defaultValue="10000" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Estimated Total"
                  >
                    <Input placeholder="Estimated Total" defaultValue="0.00" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="PDA Amount"
                  >
                    <Input placeholder="PDA Amount" defaultValue="0.00" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="PDA Person In Charge"
                  >
                    <Input placeholder="PDA Person In Charge" />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="PDA Received Date"
                  >
                    <DatePicker />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Inv No."
                  >
                    <Input placeholder="Disbursement Inv No." defaultValue="5543GHJJJ" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="FDA Status"
                  >
                    <Input placeholder="FDA Status" defaultValue="Posted" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Sent"
                  >
                    <DatePicker disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Currency"
                  >
                    <Input placeholder="Disbursement Currency" defaultValue="USD" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Payment Terms"
                  >
                    <Input placeholder="Disbursement Payment Terms" defaultValue="Pay Imidiately" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Inv Due"
                  >
                    <Input placeholder="Disbursement Inv Due" defaultValue="Pay Imidiately" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Disbursement Inv Due"
                  >
                    <DatePicker disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Exchange Rate"
                  >
                    <Input placeholder="Exchange Rate" defaultValue="1,0000" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="FDA Person In Charge"
                  >
                    <Input placeholder="FDA Person In Charge" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="FDA Received Date"
                  >
                    <Input placeholder="FDA Received Date" disabled />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                  <FormItem
                    {...formItemLayout}
                    label="PO No."
                  >
                    <Input placeholder="PO No." disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Payee"
                  >
                    <Input placeholder="Payee" disabled />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label="Service Date"
                  >
                    <DatePicker disabled />
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                  <FormItem
                    label="Rem Bank"
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                  >
                    <TextArea
                      placeholder="Rem Bank"
                      autoSize={{ minRows: 3, maxRows: 3 }}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                  <FormItem
                    label="Disb Remarks"
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                  >
                    <TextArea
                      placeholder="Disb Remarks"
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      disabled
                    />
                  </FormItem>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
      </article>


      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <Tabs type="card" defaultActiveKey="Advance/Di" >
              <TabPane tab="Advance/Di" key="Advance/Di">
                <Table size="small" pagination={false} dataSource={dataSource} columns={columns} />
              </TabPane>
              <TabPane tab="PDA/APR" key="PDA/APR">
                PDA/APR
           </TabPane>
            </Tabs>
          </div>
        </div>
      </article>

    </div>
  );
}

export default PerformaDa;