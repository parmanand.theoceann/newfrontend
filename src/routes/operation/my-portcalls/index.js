import React from 'react';
import { Route } from 'react-router-dom';

import PortActivity from './port-activity';
import BunkerInvoice from './bunker-invoice';
import FDAList from './fda-list';
import NewAppointment from './new-appointment';
import PDAList from './pda-list';
import PortacallList from './portcall-list';
import StatusList from './status-list';
import PerformaDa from './performa-da';
import MyPortDashboard from './my-portcall-dashboard';
import AppointmentPdaRequest from './appointment-pda-request';
import FinalDisbursement from './final-disbursement';
import AgencyAppointment from './agency-appointment';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/port-activity`} component={PortActivity} />
    <Route path={`${match.url}/bunker-invoice`} component={BunkerInvoice} />
    <Route path={`${match.url}/fda-list`} component={FDAList} />
    <Route path={`${match.url}/new-appointment`} component={NewAppointment} />
    <Route path={`${match.url}/pda-list`} component={PDAList} />
    <Route path={`${match.url}/portcall-list`} component={PortacallList} />
    <Route path={`${match.url}/status-list`} component={StatusList} />
    <Route path={`${match.url}/performa-da`} component={PerformaDa} />
    <Route path={`${match.url}/myportcall-dashboard`} component={MyPortDashboard} />
    <Route path={`${match.url}/appointment-pda-request`} component={AppointmentPdaRequest} />
    <Route path={`${match.url}/final-disbursement-amount`} component={FinalDisbursement} />
    <Route path={`${match.url}/agency-appointment`} component={AgencyAppointment} />
  </div>
)

export default CardComponents;