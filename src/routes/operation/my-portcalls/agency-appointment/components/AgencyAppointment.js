import React, { Component } from "react";
import {
  Table,
  Input,
  Row,
  Col,
  Button,
  Form,
  Checkbox,
  Select,
  DatePicker,
  Switch,
  Modal,
  Spin,
  Alert,
} from "antd";
import {
  CloudUploadOutlined,
  CheckOutlined,
  MessageOutlined,
  EditOutlined,
  DeleteOutlined,
  CloseOutlined,
  IdcardOutlined,
} from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  postAPICall,
  useStateCallback,
  //apiDeleteCall,
} from "../../../../../shared";
import AgencyAppointmentReport from "../../../../../shared/components/All-Print-Reports/AgencyAppointmentReport";
import moment from "moment";
import VesselSchedule from "../../../../../components/vessel-form/index";
import AddAddressBook from "../../../../../components/AddAddressBook";
//import { object } from 'prop-types';
import Attachment from "../../../../../shared/components/Attachment";
import { useEffect } from "react";
// import Attachment from '../shared/components/Attachment';

const FormItem = Form.Item;
const Option = Select.Option;
const InputGroup = Input.Group;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const AgencyAppointment  = (props) =>  {
  const [state, setState] = useStateCallback({
    editActive: props.editactive,
    active: false,
    voyID: props.voyID,
    editData: props.editdata,
    posts: [],
    agents: [],
    agentDetails: [],
    selectedAgent: "",
    agentData: { country_id: 0, port_id: 0 },
    portcallCountry: 0,
    vesselLists: [],
    vesselDetails: [],
    cargoData: [],
    generalIntruction: [],
    intructionTitleID: "",
    intructionData: [],
    addvisible: false,
    filesvisible: false,
    sub_title: "",
    write_up: "",
    attachment: "",
    formDataValues: "",
    pageOptions: { pageIndex: 1, pageLimit: 0, totalRows: 0 },
    companyID: { id: "" },
    companyInfo: [],
    typeofLetter: [],
    cargoeditVisible: false,
    visselEditvisible: false,
    addresseditVisible: false,
    formDataValuesAddress: {},
    visibleBankAttachment: false,
    filesvisibleall: false,
    legalentityiD: "",
    uploadFile: "",
    cargoSelected: "",
    postData: {
      agentdetails: [
        {
          agent_name: "",
          days_since_login: "",
          my_company: "",
          operator_feedback: "",
          this_port: "",
          this_vessel_type: "",
          verified: 1,
          agent_id: null,
          //agent_bank_no:'',
          //short_name:'',
        },
      ],
      cargodetails: {
        activity: [],
        cargo_name: "",
        charterer_name: "",
        cp_date: "",
        cp_place: "",
        laycan_end: "",
        laycan_start: "",
        quantity_max: "",
        quantity_min: "",
        remarks: "",
        unit: "",
      },
      portcalldetails: {
        arr_draft_cm: "",
        arr_draft_m: "",
        arr_draft_sw: "sw",
        country: "",
        eta: "",
        etd: "",
        next_port: "",
        port: "",
        previous_port: "",
        voyage_number: "",
      },
      bunkering: 0,
      canal_transit: 0,
      discharging: 0,
      drydock_repair: 0,
      instruction_set: [],
      legal_entity: "",
      loading: 0,
      modified_on: "",
      non_commercial: "",
      per_attached_files: "",
      portcall_country: "",
      portcall_port: "",
      type_of_letter: "",
      vessel: "",
      voyage_manager_id: "",
    },

    category: "",
    disabled: true,
    showForm: false,
    visible: false,
    activeToggle: false,
    toggleDisable: false,
    activeVal: "",
    instruction: [],
    genID: 0,
    agentColoumns: [
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        width: "10%",
        render: (data, row, index) => {
          return (
            <Checkbox
              checked={data ? true : false}
              onChange={(e) => rowSelection(e, index)}
            />
          );
        },
      },
      {
        title: "Agent Name",
        dataIndex: `full_name`,
        key: "agent_name",
        width: "15%",
        render: (text, row, index, data) => {
          return (
            <a onClick={(e) => edagenrowSelection(e, row, data)}>
              {text}
            </a>
          );
        },
      },
      {
        title: "Verified",
        dataIndex: "verified",
        key: "verified",
        width: "10%",
        render: (text) => (
          <div>
            <CheckOutlined />
          </div>
        ),
      },
      {
        title: "Operator Feedback",
        dataIndex: "operator_feedback",
        key: "operator_feedback",
        width: "10%",
        render: (text) => (
          <div>
            <MessageOutlined />
          </div>
        ),
      },
      {
        title: "This Port",
        dataIndex: "this_port",
        key: "this_port",
        width: "10%",
      },
      {
        title: "My Company",
        dataIndex: "my_company",
        key: "my_company",
        width: "10%",
      },
      {
        title: "This Vessel Type",
        dataIndex: "vessel_type",
        key: "vessel_type",
        width: "10%",
      },
      {
        title: "Days Since Login",
        dataIndex: "days_since_login",
        key: "days_since_login",
        width: "10%",
      },
    ],
    rowSelection: {
      onChange: (selectedRowKeys, selectedRows) => {},
    },
    checboxName: [
      {
        name: "loading",
      },
      {
        name: "discharging",
      },
      {
        name: "canal_transit",
      },
      {
        name: "bunkering",
      },
      {
        name: "drydock_repair",
      },
      {
        name: "commencing",
      },
    ],
  })


  const onUploadBankDetails = (data) => {
    showBankAttachementModal(true);
  };
  const onUploadFiles = (data) => {
    showBankonUploadFiles(true);
  };
  const showBankonUploadFiles = (filesvisibleallupload) =>
    setState(prevState => ({ ...prevState, filesvisibleall: filesvisibleallupload }));
  const showBankAttachementModal = (boolval) =>
    setState(prevState => ({ ...prevState, visibleBankAttachment: boolval }));

  const updatePostData = () => {
    const { voyID } = state;
    let CargoUnit = state.cargoData;
    // let cargonameData = state.postData.cargodetails;
    let agentData = state.postData.agentdetails;
    let getprotcallDB = state.agentDetails;
    let getvisselDB = state.vesselLists.port_itinerary;
    let protcallData = state.postData.portcalldetails;
    let id = state.agentDetails && state.agentDetails[0].id;
    const response = getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${id}`);
    // const respData = await response;
    // const vdata = await respData['data'];
    // let { agent_bank_no} = '';
    //   if(vdata['bank&accountdetails'].length> 0 ){
    //       vdata['bank&accountdetails'].map(e=>{
    //       agent_bank_no = e.account_no;
    //   })
    // }
    let voyage_number = `${voyID}`;
    let eta_Date;
    let etd_Date;
    let cp_dateData;

    CargoUnit &&
      CargoUnit.map((val, idx) => {
        val["portdatedetails"] &&
          val["portdatedetails"] &&
          val["portdatedetails"].length > 0 &&
          val["portdatedetails"].map((e) => {
            eta_Date = e.arrival_date_time;
            etd_Date = e.departure;
            return true;
          });
        val["bunkerdetails"] &&
          val["bunkerdetails"] &&
          val["bunkerdetails"].length > 0 &&
          val["bunkerdetails"].map((e) => {
            eta_Date = eta_Date;
            etd_Date = etd_Date;
            return true;
          });
        cp_dateData = val.cp_date; // tech team
        return true;
      });

    let port_name;
    let country_name;
    let agent_nameData;

    getprotcallDB &&
      getprotcallDB.map((e, idx) => {
        port_name = e.port;
        country_name = e.country;
        agent_nameData = e.full_name;
      });

    agentData &&
      agentData.map((e) => {
        e.agent_name = agent_nameData;
        e.this_port = port_name;
        e.agent_id = id;
        //e.short_name = vdata['short_name']
        //e.agent_bank_no = agent_bank_no
      });

    let next_portData;
    getvisselDB &&
      getvisselDB.map((e, idx) => {
        if (idx === 1) {
          //next_portData = e.port_name;
        }
      });

    Object.assign(
      protcallData,
      { port: port_name },
      { country: country_name },
      { voyage_number: voyage_number },
      { eta: eta_Date },
      { etd: etd_Date },
      { next_port: next_portData }
    );
  };

  useEffect(() => {
    const getFormData = async () => {
      const {
        postData,
        voyID,
        editActive,
        vesselDetails,
        companyID,
        pageOptions,
        legalentityiD,
        typeofLetter,
      } = state;
      let postVal = { country_id: 0, port_id: 0 };
      let funcName;
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/poitlist?e=${voyID}`
      );
      const vdata = await response["data"];
  
      if (
        vdata &&
        vdata.port_itinerary &&
        vdata["port_itinerary"].length > 0 &&
        typeof vdata["port_itinerary"] === "object"
      ) {
        postVal["country_id"] = vdata["port_itinerary"][0]["country_id"];
        postVal["port_id"] = vdata["port_itinerary"][0]["port_id"];
  
        funcName = vdata["port_itinerary"][0]["funct_name"]; // by default select activity
  
        // postVal={ "country_id": 186, "port_id": 5515}
        const response1 = await awaitPostAPICall(
          `${URL_WITH_VERSION}/port-call/listagent`,
          postVal
        );
        const myObjArray = await response1["data"];
        let data = [
          ...new Map(myObjArray.map((item) => [item["id"], item])).values(),
        ];
        setState(prevState => ({...prevState, vesselLists: vdata, agentDetails: data, portcallCountry: postVal["country_id"]}), () => setState(prevState => ({ ...prevState, showForm: true })))

        updatePostData();
      } else {
        setState(prevState => ({...prevState, vesselLists: vdata}), () => setState(prevState => ({ ...prevState, showForm: true })))

      }
  
      Object.assign(
        postData,
        { portcall_country: postVal.country_id },
        { portcall_port: postVal.port_id },
        { voyage_manager_id: voyID }
      );
  
      const geneintResponse = await getAPICall(
        `${URL_WITH_VERSION}/port-call/listgeninst`
      );
      const genintData = await geneintResponse["data"];
      let visselID =
        state.vesselLists && state.vesselLists.vessel_list[0].vessel_id;
      const reponseVissel = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${visselID}`
      );
      const vdetailsData = await reponseVissel["data"];
      let newvesselDetails = state.vesselDetails.push(vdetailsData);
      const corgoreponse = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/edit?ae=${props.estimateID}`
      );
      const getcargoData = await corgoreponse["data"];
      let getCargoData = state.cargoData.push(getcargoData);
      setState(prevState => ({
        ...prevState,
        newvesselDetails,
        getCargoData,
        generalIntruction: genintData,
      }));
  
      // company
      let comapyidDB;
      vesselDetails.map((e) => {
        comapyidDB = e.company;
      });
      Object.assign(companyID, { id: comapyidDB });
  
      setState(prevState => ({
        ...prevState,
        companyID: {
          ...state.companyID,
        },
      }));
  
      let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
      let headers = {
        where: { AND: { status: "1", address_type_id: { in: "(7,9,10)" } } },
      };
  
      let qParamString = objectToQueryStringFunc(qParams);
  
      let _url = `${URL_WITH_VERSION}/address/list?${qParamString}`;
      const response22 = await getAPICall(_url, headers);
      const companyData = await response22["data"];
      let legalentityiD1;
      if (companyData.length > 0) {
        legalentityiD1 = companyData[0]["id"];
      }
      postData["legal_entity"] = legalentityiD1;
      setState(prevState => ({ ...prevState, companyInfo: companyData, postData }));
  
      //PDA
      let headers1 = { where: { AND: { sl_type: "'PORTCALL_LETTER_TYPE'" } } };
      let qParamString1 = objectToQueryStringFunc(qParams);
      let _url1 = `${URL_WITH_VERSION}/master/list?t=prtclt&${qParamString1}`;
      const lttertyReposnse = await getAPICall(_url1, headers1);
      const tpLetterData = await lttertyReposnse["data"];
      let type_letter1;
  
      if (tpLetterData.length > 0) {
        type_letter1 = tpLetterData[0]["id"];
      }
      postData["type_of_letter"] = type_letter1;
      setState(prevState => ({ ...prevState, typeofLetter: tpLetterData, postData }));
      //PDA
  
      let PostDBvis = state.postData;
      Object.assign(PostDBvis, { vessel: visselID });
  
      let CargoUnit = state.cargoData;
      let cargonameData = state.postData.cargodetails;
      let agentData = state.postData.agentdetails;
      let getprotcallDB = state.agentDetails;
      let getvisselDB = state.vesselLists.port_itinerary;
      let protcallData = state.postData.portcalldetails;
  
      let voyage_number = getcargoData["voyage_number"];
      let eta_Date;
      let etd_Date;
      let cp_dateData;
      CargoUnit &&
        CargoUnit.map((val, idx) => {
          // voyage_number = val.voyage_manager_id
  
          val["portdatedetails"] &&
            val["portdatedetails"] &&
            val["portdatedetails"].length > 0 &&
            val["portdatedetails"].map((e) => {
              eta_Date = e.arrival_date_time;
              etd_Date = e.departure;
            });
          val["bunkerdetails"] &&
            val["bunkerdetails"] &&
            val["bunkerdetails"].length > 0 &&
            val["bunkerdetails"].map((e) => {
              eta_Date = eta_Date;
              etd_Date = etd_Date;
              //cp_dateData = e.cp_date;
            });
          cp_dateData = val.cp_date;
        });
  
      let port_name;
      let country_name;
      let agent_nameData;
  
      getprotcallDB &&
        getprotcallDB.map((e, idx) => {
          port_name = e.port;
          country_name = e.country;
          agent_nameData = e.full_name;
        });
  
      agentData &&
        agentData.map((e) => {
          e.agent_name = agent_nameData;
          e.this_port = port_name;
        });
      let next_portData;
      getvisselDB &&
        getvisselDB.map((e, idx) => {
          if (idx === 1) {
            next_portData = e.port;
          }
        });
  
      Object.assign(
        protcallData,
        { port: port_name },
        { country: country_name },
        { voyage_number: voyage_number },
        { eta: eta_Date },
        { etd: etd_Date },
        { next_port: next_portData }
      );
  
      let PostDB = state.postData;
      let activityData = postData.cargodetails.activity;
  
      if (
        editActive === undefined ||
        editActive === null ||
        editActive === false
      ) {
        if (funcName !== null && funcName !== undefined) {
          Object.assign(PostDB, {
            [`${funcName}`]: funcName !== undefined ? 1 : 0,
          });
          //activityData.push(funcName);
          setState(prevState => ({
            ...prevState,
            activeVal: funcName,
            postData: {
              ...state.postData,
              ...PostDB,
              cargodetails: {
                ...state.postData.cargodetails,
              },
            },
          }));
        }
      }
  
      const onChangeToggle = (checked, typeData) => {
        let type = typeData;
        if (checked) {
          activityData.push(typeData);
          // Object.assign(PostDB, { [`${type}`]: checked === true ? 1 : '0' });
          // setState({
          //   ...state,
          //   postData: {
          //     ...state.postData,
          //     ...PostDB,
          //     cargodetails: {
          //       ...state.postData.cargodetails,
          //     }
          //   }
          // });
        } else {
          activityData.pop(typeData);
          Object.assign(PostDB, { [`${type}`]: checked === false ? 0 : "0" });
          setState(prevState => ({
            ...prevState,
            postData: {
              ...PostDB,
              ...state.postData,
              cargodetails: {
                ...state.postData.cargodetails,
              },
            },
          }));
        }
      };
  
      //edit Api
      if (editActive) {
        const { editData, voyID } = state;
        const editportID = editData.id;
        const editagentData = editData.agentdetails;
        const editacrgoData = editData.cargodetails;
        const editportData = editData.portcalldetails;
        const intructionData = editData.instruction_set;
        Object.assign(postData, { id: editportID });
        setState(prevState => ({
          ...prevState,
          intructionData: intructionData,
          postData: {
            ...state.postData,
            agentdetails: editagentData,
            cargodetails: editacrgoData
              ? editacrgoData
              : state.postData.cargodetails,
            portcalldetails: editportData,
            voyage_manager_id: voyID,
          },
          active: true,
        }));
        if (CargoUnit && CargoUnit[0].cargos && CargoUnit[0].cargos.length > 0) {
          let cargoValues = props.editdata;
          if (cargoValues && cargoValues.cargodetails) {
            CargoUnit[0].cargos.map((val, idx) => {
              if (val.charterer_name === cargoValues.cargodetails.charterer_name) {
                editSelectCargo(val.id, true);
              }
            });
          }
        }
      } else {
        setState(prevState => ({
          ...prevState,
          active: false,
        }));
      }
  
      const response4 = await getAPICall(
        `${URL_WITH_VERSION}/port-call/listgeninst?l=0`
      );
      const data45 = await response4["data"];
      setState(prevState => ({ ...prevState, instruction: data45 }));
    };
    getFormData()
  }, [])

  // changeCountry = event => setState({ value: event.target.value });
  // changePort = event => setState({ value: event.target.value });
  const toggleClass = () => {
    const currentState = state.active;
    setState(prevState => ({ ...prevState, active: !currentState }));
  }

  const edagenrowSelection = async (event, row, data) => {
    let id = state.agentDetails[0].id;
    const response = await getAPICall(
      `${URL_WITH_VERSION}/address/edit?ae=${id}`
    );
    const respData = await response.data;
    setState(prevState => ({...prevState, formDataValuesAddress: respData}), () => setState(prevState => ({ ...prevState, addresseditVisible: true })))

  };

  const edagenrowselectionCancel = () => {
    setState(prevState => ({
      ...prevState,
      addresseditVisible: false,
    }));
  };

  const rowSelection = (event, index) => {
    let agentDetails = Object.assign([], state.agentDetails);
    let { postData } = state;
    let enableButtion = false;
    agentDetails.forEach((item) => (item["action"] = false));
    agentDetails[index]["action"] = event["target"]["checked"];

    let selectAgent = agentDetails[index];

    if (agentDetails[index]["action"] === true) {
      enableButtion = true;
    } else {
      agentDetails.map((e) => {
        if (enableButtion === false && e.action === true) {
          enableButtion = true;
        }
      });
    }

    if (postData && postData.hasOwnProperty("agentdetails")) {
      if (postData.agentdetails) {
        postData.agentdetails[0]["agent_id"] = selectAgent.id;
      } else {
        postData.agentdetails = selectAgent;
        postData.agentdetails["agent_id"] = selectAgent.id;
      }
    }

    setState(prevState => ({
      ...prevState,
      agentDetails: agentDetails,
      selectedAgent: selectAgent,
      postData,
      disabled: !enableButtion,
    }));
  };

  const handleSelect = (e) => {
    const { genID } = state;

    setState(prevState => ({ ...prevState, genID: e.key }));
  };

  const onChange = async (event, name) => {
    const { vesselLists, agentData } = state;
    let al = []; // Agent List
    let eventVal = event.hasOwnProperty("traget") ? event.target.value : event;
    let funcName = "";

    let selectObject = vesselLists["port_itinerary"].filter(
      (e) => e.port === eventVal
    );
    let postVal = Object.assign({}, agentData);

    if (selectObject && selectObject.length > 0) {
      postVal["country_id"] = selectObject[0]["country_id"];
      postVal["port_id"] = selectObject[0]["port"];
      funcName = selectObject[0]["funct_name"];
      const response1 = await awaitPostAPICall(
        `${URL_WITH_VERSION}/port-call/listagent`,
        postVal
      );
      const myObjArray = await response1["data"];
      let data = [
        ...new Map(myObjArray.map((item) => [item["id"], item])).values(),
      ];
      // if (data && data.length > 0) {
      //   data.map(e => al.push({ id: e.id, full_name: e.full_name, country : e.country, port : e.port }));
      // }
      setState(prevState => ({
        ...prevState,
        agentDetails: data,
        portcallCountry: postVal["country_id"],
      }));
    } else if (name === "portcall_country_name") {
      setState(prevState => ({
        ...prevState,
        portcallCountry: event,
        activeVal: funcName,
      }));
    }
    updatePostData();
  };

  const editSelectCargo = (event) => {
    let cargonameData = state.postData.cargodetails;
    let editCargoDetails =
      props.editData && props.editData.cargodetails
        ? props.editData.cargodetails
        : state.postData.cargodetails;
    let CargoUnit = state.cargoData;
    let {
      cargo_name,
      cargo_unit,
      charterer,
      eta_Date,
      etd_Date,
      cp_placeData,
      laycan_fromData,
      laycan_toData,
      max_qtyData,
      mix_qtyData,
      cp_dateData,
      activity,
    } = "";

    CargoUnit &&
      CargoUnit.map((val, idx) => {
        val["bunkerdetails"] &&
          val["bunkerdetails"] &&
          val["bunkerdetails"].length > 0 &&
          val["bunkerdetails"].map((e) => {
            eta_Date = e.arrival_date_time;
            etd_Date = e.departure;
          });
        val["portdatedetails"] &&
          val["portdatedetails"] &&
          val["portdatedetails"].length > 0 &&
          val["portdatedetails"].map((e) => {
            eta_Date = e.arrival_date_time;
            etd_Date = e.departure;
          });
        cp_dateData = editCargoDetails.cp_date;
        val["cargos"] &&
          val["cargos"].map((newval) => {
            if (newval.id === event) {
              cargo_name = editCargoDetails.cargo_name;
              cargo_unit = editCargoDetails.unit;
              charterer = editCargoDetails.charterer_name;
              cp_placeData = editCargoDetails.cp_place;
              laycan_fromData = editCargoDetails.laycan_start;
              laycan_toData = editCargoDetails.laycan_end;
              max_qtyData = editCargoDetails.quantity_max;
              mix_qtyData = editCargoDetails.quantity_min;
              activity = editCargoDetails.activity;
            }
          });
      });

    Object.assign(
      cargonameData,
      { cargo_name: cargo_name },
      { unit: cargo_unit },
      { charterer_name: charterer },
      { cp_place: cp_placeData },
      { laycan_start: laycan_fromData },
      { laycan_end: laycan_toData },
      { quantity_max: max_qtyData },
      { quantity_min: mix_qtyData },
      { cp_date: cp_dateData },
      { activity: activity }
      // { eta : eta_Date},
      // { etd :etd_Date}
    );
    setState(prevState => ({ ...prevState, cargonameData, cargoSelected: event }));
  };

  const selectCargo = (event) => {
    let cargonameData = state.postData.cargodetails;
    let CargoUnit = state.cargoData;
    let {
      cargo_name,
      cargo_unit,
      charterer,
      eta_Date,
      etd_Date,
      cp_placeData,
      laycan_fromData,
      laycan_toData,
      max_qtyData,
      mix_qtyData,
      cp_dateData,
    } = "";

    CargoUnit &&
      CargoUnit.map((val, idx) => {
        val["portdatedetails"] &&
          val["portdatedetails"] &&
          val["portdatedetails"].length > 0 &&
          val["portdatedetails"].map((e) => {
            eta_Date = e.arrival_date_time;
            etd_Date = e.departure;
          });
        val["bunkerdetails"] &&
          val["bunkerdetails"] &&
          val["bunkerdetails"].length > 0 &&
          val["bunkerdetails"].map((e) => {
            eta_Date = eta_Date;
            etd_Date = etd_Date;
          });
        cp_dateData = val.cp_date;
        val["cargos"] &&
          val["cargos"].map((newval) => {
            if (newval.id === event) {
              cargo_name = newval.cargo_name1;
              cargo_unit = newval.cp_unit;
              charterer = newval.charterer_name;
              cp_placeData = newval.cp_place;
              laycan_fromData = newval.laycan_from;
              laycan_toData = newval.laycan_to;
              max_qtyData = newval.max_qty;
              mix_qtyData = newval.min_qty;
            }
          });
      });
    Object.assign(
      cargonameData,
      { cargo_name: cargo_name },
      { unit: cargo_unit },
      { charterer_name: charterer },
      { cp_place: cp_placeData },
      { laycan_start: laycan_fromData },
      { laycan_end: laycan_toData },
      { quantity_max: max_qtyData },
      { quantity_min: mix_qtyData },
      { cp_date: cp_dateData }
      // {eta :eta_Date},
      // {etd:etd_Date}
    );
    setState(prevState => ({ ...prevState, cargonameData, cargoSelected: event }));
  };

  const handleSelectNextPrePort = (event, name) => {
    let saveData = state.postData;
    let portdetailsData = Object.assign(saveData.portcalldetails, {
      [name]: event,
    });
    setState(prevState => ({
      ...prevState,
      ...saveData,
      portdetailsData,
    }));
  };

  const handleChange = (event, nameType, datefieldName) => {
    let saveData = state.postData;
    if (nameType === "portcalldetails") {
      if (moment.isMoment(event)) {
        let datenameKey = datefieldName;
        let portdetailsData = Object.assign(saveData.portcalldetails, {
          [datenameKey]: event._d
            ? moment(event._d).format("YYYY-MM-DD HH:mm")
            : moment().format("YYYY-MM-DD HH:mm"),
        });
        setState(prevState => ({
          ...prevState,
          ...saveData,
          portdetailsData,
        }));
      } else {
        let portdetailsData = Object.assign(saveData.portcalldetails, {
          [event.target.name]: event.target.value,
        });
        setState(prevState => ({
          ...prevState,
          ...saveData,
          portdetailsData,
        }));
      }
    } else if (nameType === "cargodetails") {
      if (moment.isMoment(event)) {
        let datenameKey = datefieldName;
        let cargodetailsData = Object.assign(saveData.cargodetails, {
          [datenameKey]: event._d
            ? moment(event._d).format("YYYY-MM-DD HH:mm")
            : moment().format("YYYY-MM-DD HH:mm"),
        });
        setState(prevState => ({
          ...prevState,
          ...saveData,
          cargodetailsData,
        }));
      } else {
        let cargodetailsData = Object.assign(saveData.cargodetails, {
          [event.target.name]: event.target.value,
        });
        setState(prevState => ({
          ...prevState,
          ...saveData,
          cargodetailsData,
        }));
      }
    } else {
      setState(prevState => ({
        ...prevState,
        [event.target.name]: event.target.value,
      }));
    }
  };

  const saveformData = async () => {
    const { vesselLists, postData } = state;
    let data1 = postData;
    let vessel = vesselLists.vessel_list[0]["vessel_id"];
    if (postData["vessel"] === false) {
      data1["vessel"] = vessel;
    }
    postAPICall(
      `${URL_WITH_VERSION}/port-call/save`,
      data1,
      "post",
      (respo2) => {
        if (respo2 && respo2.data) {
          openNotificationWithIcon("success", respo2.message);
          props.history.push(`/my-portcall`);
        } else {
          openNotificationWithIcon("error", respo2.message);
        }
      }
    );

    props.cancelagencyModel();
  };

  const updateportcallData = async () => {
    const { postData, vesselLists } = state;
    let vessel = vesselLists.vessel_list[0]["vessel_id"];
    if (postData["vessel"] === false) {
      postData["vessel"] = vessel;
    }
    let data3 = postData;
    postAPICall(
      `${URL_WITH_VERSION}/port-call/update`,
      data3,
      "put",
      (respo2) => {
        if (respo2 && respo2.data) {
          openNotificationWithIcon("success", respo2.message);
        } else {
          openNotificationWithIcon("error", respo2.message);
        }
      }
    );
    props.cancelModel();
  };

  const selectKey = async (e) => {
    setState(prevState => ({ ...prevState, intructionTitleID: e }));
    const response1 = await getAPICall(
      `${URL_WITH_VERSION}/port-call/stedit?e=${e}`
    );
    const vdata1 = await response1["data"];
    setState(prevState => ({
      ...prevState,
      intructionData: vdata1,
      postData: { ...state.postData, instruction_set: vdata1 },
    }));
  };

  const selectCountry = async (e) => {
    const { postData } = state;
    Object.assign(postData, { legal_entity: e });
    setState(prevState => ({
      ...prevState,
      postData: {
        ...state.postData,
      },
    }));
  };

  const selecteletterType = async (e) => {
    const { postData } = state;
    Object.assign(postData, { type_of_letter: e });
    setState(prevState => ({
      ...prevState,
      postData: {
        ...state.postData,
      },
    }));
  };

  const onAdd = () => {
    setState(prevState => ({
      ...prevState,
      addvisible: true,
    }));
  };

  const uploadedFiles = (arrr) => {
    const { uploadFile } = state;
    setState(prevState => ({ ...prevState, uploadFile: arrr }));
  }

  const intructionhandleChange = (e, id) => {
    let { intructionData } = state;
    intructionData["-"][id][e.target.name] = e.target.value;

    setState(prevState => ({
      ...prevState,
      intructionData: intructionData,
      postData: {
        ...state.postData,
        instruction_set: intructionData,
      },
    }));
  };

  const addHandleOk = (e) => {
    const { intructionData, uploadFile } = state;
    if (intructionData["-"] && intructionData["-"].length > 0) {
      intructionData["-"].push({
        sub_title: state.sub_title,
        write_up: state.write_up,
        //attachment: uploadFile.map(e => e.name),
        attachment: "",
      });
      setState(prevState => ({
        ...prevState,
        addvisible: false,
        intructionData: intructionData,
        postData: {
          ...state.postData,
          instruction_set: intructionData,
        },
      }));
    } else {
      let data = {
        "-": [
          {
            sub_title: state.sub_title,
            write_up: state.write_up,
            //attachment: uploadFile.map(e => e.name),
            attachment: "",
          },
        ],
        id: intructionData.id,
        name: intructionData.name,
        status: intructionData.status,
      };
      setState(prevState => ({
        ...prevState,
        addvisible: false,
        intructionData: data,
        postData: {
          ...state.postData,
          instruction_set: data,
        },
      }));
    }
  };

  const addCanelOK = (e) => {
    setState(prevState => ({
      ...prevState,
      addvisible: false,
    }));
  };

  const adddHandleCancel = async () => {
    let visselID = state.vesselLists.vessel_list[0].vessel_id;
    const reponseVissel = await getAPICall(
      `${URL_WITH_VERSION}/vessel/list/${visselID}`
    );
    const vdetailsData = await reponseVissel["data"];
    let newvesselDetails = state.vesselDetails.push(vdetailsData);
    setState({
      ...state,
      newvesselDetails,
      visselEditvisible: false,
    });
    // setState({
    //   visselEditvisible: false,
    //  });
  };

  const onCancel = (e) => {
    setState(prevState => ({
      ...prevState,
      addresseditVisible: false,
    }));
  };

  const onRowDelete = (e, id) => {
    const { intructionData } = state;
    const index = intructionData["-"].findIndex((item) => id === item.id);
    if (index > -1) {
      intructionData["-"].splice(index, 1);
    }
    setState(prevState => ({
      ...prevState,
      intructionData: intructionData,
      postData: {
        ...state.postData,
        instruction_set: intructionData,
      },
    }));
  };

  const editVisselhandle = async () => {
    let id = state.vesselLists.vessel_list[0].vessel_id;

    if (id) {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${id}`
      );
      const respData = await response["data"];
      respData["id"] = respData["vessel_id"];
      setState(prevState => ({...prevState, isAdd: false, formDataValues: respData}), () => setState(prevState => ({...prevState, visselEditvisible: true,vesselDetails: []})))

    } else {
      setState(prevState => ({ ...prevState, isAdd: true, visselEditvisible: true }));
    }

    // const response5 = await awaitPostAPICall(`${URL_WITH_VERSION}/port-call/save`, state);
    // const saveData = await response5['data'];
  };

  const createAgentAppoinment = async () => {
    const { postData, instruction_set, instruction, vesselLists } = state;

    let vessel = vesselLists.vessel_list[0]["vessel_id"];
    if (postData["vessel"] === false) {
      postData["vessel"] = vessel;
    }
    //postData.hasOwnProperty('Commencing') // true
    postData["loading"] = postData["Loading"];
    postData["commencing"] = postData["Commencing"];
    delete postData["commencing"];
    delete postData["Loading"];
    delete postData["Commencing"];
    delete postData["Discharging"];
    delete postData["Bunkering"];
    delete postData["Drydock_repair"];
    delete postData["Canal_transit"];
    //console.log("%c AGENT APPOINMENT", 'background : purple', postData)
    postAPICall(
      `${URL_WITH_VERSION}/port-call/save`,
      postData,
      "post",
      (respo2) => {
        if (respo2 && respo2.data) {
          openNotificationWithIcon("success", respo2.message);
          props.cancelagencyModel();
          props.history.push(`/my-portcall`);
        } else {
          openNotificationWithIcon("error", respo2.message);
        }
      }
    );
  };

  const activitySelect = (value) => {
    let activityData = state.postData.cargodetails.activity;
    if (activityData === null) {
      let newArray = [];
      newArray.push(value);
      activityData = newArray;
      state({ postData: { cargodetails: { activity: newArray } } });
    } else if (activityData.length === 0) {
      activityData.push(value);
    } else {
      if (activityData && activityData.length > 0) {
        let lastlength = value.length;
        activityData.push(value[lastlength - 1]);
      }
    }
  };

    const {
      active,
      intructionTitleID,
      visselEditvisible,
      formDataValuesAddress,
      addresseditVisible,
      filesvisibleall,
      visibleBankAttachment,
      companyInfo,
      typeofLetter,
      formDataValues,
      sub_title,
      write_up,
      intructionData,
      editActive,
      showForm,
      vesselLists,
      agentColoumns,
      agentDetails,
      vesselDetails,
      portcallCountry,
      cargoData,
      // activityData,
      postData,
      generalIntruction,
      checboxName,
      selectedAgent,
      activeVal,
      cargoSelected,
    } = state;
    return (
      <div className="body-wrapper">
        <div
          className={
            active ? "wrap-gencyappointment hide" : "wrap-gencyappointment show"
          }
        >
          <Form>
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <div className="form-wrapper mb-3">
                    <div className="form-heading">
                      <h4 className="title">
                        <span className="text-primary">
                          Please Select Below To Create Portcall
                        </span>
                      </h4>
                    </div>
                  </div>
                  <div className="wrap-table">
                    {showForm ? (
                      <>
                        <Row gutter={16}>
                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem {...formItemLayout} label="Legal Entity">
                              <Select
                                value={postData && postData.legal_entity}
                                className="border-none"
                                name="legal_entity"
                                onChange={selectCountry.bind(this)}
                              >
                                {companyInfo.map((e) => {
                                  return (
                                    <Option key={e.id} value={e.id}>
                                      {e.short_name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            </FormItem>
                          </Col>

                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              {...formItemLayout}
                              label="Type of letter"
                            >
                              <Select
                                value={postData && postData.type_of_letter}
                                className="border-none"
                                name="type_of_letter"
                                onChange={selecteletterType.bind(this)}
                              >
                                {typeofLetter &&
                                  typeofLetter.length > 0 &&
                                  typeofLetter.map((e) => {
                                    return (
                                      <Option key={e.id} value={e.id}>
                                        {e.sl_name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            </FormItem>
                          </Col>

                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem {...formItemLayout} label="Vessel">
                              <Select
                                disabled
                                value={
                                  vesselLists.vessel_list &&
                                  vesselLists.vessel_list[0]["vessel_name"]
                                }
                                className="border-none"
                                onChange={selecteletterType.bind(this)}
                              >
                                {vesselLists &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary.length > 0 &&
                                  vesselLists.port_itinerary.map((e) => {
                                    return (
                                      <Option key={e.id} value={e.vessel_id}>
                                        {e.vessel_name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            </FormItem>
                          </Col>
                        </Row>

                        <Row gutter={16}>
                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              {...formItemLayout}
                              label="Portcall Country"
                            >
                              <Select
                                disabled
                                name="portcall_country_name"
                                defaultValue={
                                  vesselLists.vessel_list &&
                                  vesselLists.port_itinerary[0]["country_name"]
                                }
                                className="border-none"
                                onChange={(e) =>
                                  onChange(e, "portcall_country_name")
                                }
                              >
                                {vesselLists &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary.length > 0 &&
                                  vesselLists.port_itinerary.map((e) => {
                                    return (
                                      <Option
                                        key={e.country_id}
                                        value={e.country_id}
                                      >
                                        {e.country_name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            </FormItem>
                          </Col>

                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem {...formItemLayout} label="Portcall Port">
                              <Select
                                name="portcall_port"
                                defaultValue={
                                  vesselLists.vessel_list &&
                                  vesselLists.port_itinerary[0]["port"]
                                }
                                className="border-none"
                                onChange={(e) =>
                                  onChange(e, "portcall_port")
                                }
                              >
                                {vesselLists &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary &&
                                  vesselLists.port_itinerary.length > 0 &&
                                  vesselLists.port_itinerary.map((e) => {
                                    return (
                                      <Option key={e.id} value={e.port}>
                                        {e.port}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            </FormItem>
                          </Col>

                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              {...formItemLayout}
                              label="Instruction Set"
                            >
                              <Select
                                value={intructionTitleID}
                                name="instruction_set"
                                className="border-none"
                                onChange={selectKey.bind(this)}
                              >
                                {generalIntruction &&
                                  generalIntruction &&
                                  generalIntruction.length > 0 &&
                                  generalIntruction.map((title) => {
                                    return (
                                      <Option key={title.id} value={title.id}>
                                        {title.name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            </FormItem>
                          </Col>
                        </Row>

                        <Row gutter={16}>
                          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <FormItem
                              {...formItemLayout}
                              labelCol={{ span: 24 }}
                              wrapperCol={{ span: 24 }}
                            >
                              <div className="form-wrapper mt-3">
                                <div className="form-heading">
                                  <h4 className="title">
                                    <span className="text-primary">
                                      Activity
                                    </span>
                                  </h4>
                                </div>
                              </div>

                              <Checkbox.Group className="w-100 mt-3">
                                <Row>
                                  {activeVal &&
                                    checboxName &&
                                    checboxName.map((e, idx) => {
                                      let name = e.name;
                                      return (
                                        <>
                                          <Col
                                            xs={24}
                                            sm={12}
                                            md={4}
                                            lg={4}
                                            xl={4}
                                          >
                                            <span className="mr-2 text-color-theme">
                                              {e.name}
                                            </span>
                                            <Switch
                                              checkedChildren={
                                                <CheckOutlined />
                                              }
                                              unCheckedChildren={
                                                <CloseOutlined />
                                              }
                                              onChange={(e) => {}
                                                // onChangeToggle(e, name)
                                              }
                                              // defaultChecked={activeVal === name ? true : false}
                                              // disabled={activeVal === name ? true : false}
                                            />
                                          </Col>
                                        </>
                                      );
                                    })}
                                </Row>
                              </Checkbox.Group>
                            </FormItem>
                          </Col>
                        </Row>
                      </>
                    ) : (
                      <div className="col col-lg-12">
                        <Spin tip="Loading...">
                          <Alert
                            message=" "
                            description="Please wait..."
                            type="info"
                          />
                        </Spin>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </article>

            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <Table
                    size="small"
                    pagination={false}
                    columns={agentColoumns}
                    dataSource={agentDetails}
                    inlineTable
                    title={() => (
                      <Row gutter={16}>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                          <b>
                            Please Select A Nominated Agent From The List Of All
                            The Active Agent On This Port <IdcardOutlined />
                          </b>
                        </Col>
                      </Row>
                    )}
                  />

                  <div className="action-btn mt-3 text-right">
                    <Button
                      type="primary"
                      disabled={state.disabled ? true : false}
                      onClick={() =>
                        setState(prevState => ({ ...prevState, active: !state.active }))
                      }
                    >
                      Next
                    </Button>
                  </div>
                </div>
              </div>
            </article>
          </Form>
        </div>
        <div
          className={
            state.active || editActive
              ? "wrap-gencyappointment show"
              : "wrap-gencyappointment hide"
          }
        >
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-wrapper">
                  <div className="form-heading">
                    <h4 className="title">
                      <span></span>
                    </h4>
                  </div>
                </div>
                <div>
                  <h3 className="inner-heading">
                    Note : All Date & Time Will be consider as UTC Date & Time
                  </h3>
                </div>
                <div>
                  <div className="equal-space">
                    <div className="mb-20 style">
                      <div>Appointment With PDA Request</div>
                      <div>Pro-Forma Disbursement Account Request</div>
                    </div>
                    <div className="style">
                      {editActive ? (
                        <div>
                          TO :{" "}
                          {postData.agentdetails &&
                            postData.agentdetails &&
                            postData.agentdetails.length > 0 &&
                            postData.agentdetails.map((e) => e.agent_name)}
                        </div>
                      ) : (
                        <div>
                          TO :{" "}
                          {selectedAgent && selectedAgent
                            ? selectedAgent.full_name
                            : ""}
                        </div>
                      )}
                      <div>
                        SUBJECT : Appointment with PDA request /
                        {vesselDetails &&
                          vesselDetails &&
                          vesselDetails.length > 0 &&
                          vesselDetails.map((e) => {
                            return (
                              <>
                                <span> {e.vessel_name} </span>
                              </>
                            );
                          })}
                        <span className="text-uppercase">
                          {postData.portcalldetails.country}
                        </span>{" "}
                        {postData.portcalldetails.port}
                      </div>
                      <div>Please provide us PDA for the subject call</div>
                    </div>
                  </div>
                  <div className="equal-space wrap-btn-icons">
                    {/* <div>
                      <img src="../../assets/images/export.svg" alt="" />
                    </div>
                    <div>Suman Singapore</div>
                    <div>Singapore</div>
                    <div>04748393382828</div> */}
                    <Button
                      type="primary"
                      className="ml-2 mt-2 mb-2"
                      onClick={() =>{}}
                    >
                      Create Report
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </article>

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <h3 className="inner-heading">
                    Vessel Details{" "}
                    <EditOutlined onClick={editVisselhandle} />
                  </h3>
                  <hr />
                  <Form>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="Vessel Name"
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Vessel Name*"
                                  defaultValue={e.vessel_name}
                                  name="vessel_name"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="NRT"
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="NRT"
                                  value={
                                    e.capacityanddraft && e.capacityanddraft
                                      ? e.capacityanddraft.nrt_intl
                                      : ""
                                  }
                                  disabled
                                  name="nrt"
                                  onChange={(e) =>
                                    handleChange(e, "vessel")
                                  }
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Fresh Draft" {...formItemLayout}>
                          <Input
                            placeholder="Fresh Draft"
                            defaultValue=""
                            name="fresh_draft"
                            onChange={handleChange}
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Suez Id" {...formItemLayout}>
                          <Input
                            placeholder="Suez Id"
                            defaultValue=""
                            name="suez_id"
                            onChange={handleChange}
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Telex" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Telex"
                                  value={
                                    e.contacts && e.contacts
                                      ? e.contacts.telex
                                      : ""
                                  }
                                  disabled
                                  name="telex"
                                  onChange={handleChange}
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="Vessel Type"
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Vessel Type"
                                  defaultValue={
                                    e.vessel_type_name && e.vessel_type_name
                                      ? e.vessel_type_name
                                      : ""
                                  }
                                  name="vessel_type"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Call Sign" {...formItemLayout}>
                          <Input
                            placeholder="Call Sign"
                            defaultValue=""
                            name="call_sign"
                            onChange={handleChange}
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Tropical Draft" {...formItemLayout}>
                          <Input
                            placeholder="Tropical Draft"
                            defaultValue=""
                            name="tropical_draft"
                            onChange={handleChange}
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Suez Canal NT" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Suez Canal NT"
                                  defaultValue={
                                    e.capacityanddraft && e.capacityanddraft
                                      ? e.capacityanddraft.net2
                                      : ""
                                  }
                                  name="suez_canal_nt"
                                  onChange={handleChange}
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="DWT"
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="DWT"
                                  defaultValue={
                                    e.vessel_dwt && e.vessel_dwt
                                      ? e.vessel_dwt
                                      : ""
                                  }
                                  name="dwt"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Class" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Class"
                                  value={e.class_society}
                                  name="class"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>

                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="MMSI No." {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="MMSI No."
                                  defaultValue={e.mmsi ? e.mmsi : ""}
                                  name="mmsi_no"
                                  onChange={handleChange}
                                  //disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Suez Canal GT" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Suez Canal GT"
                                  defaultValue={
                                    e.capacityanddraft && e.capacityanddraft
                                      ? e.capacityanddraft.suez_gross
                                      : ""
                                  }
                                  name="suez_canal_gt"
                                  onChange={handleChange}
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="GRT" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="GRT"
                                  value={
                                    e.capacityanddraft && e.capacityanddraft
                                      ? e.capacityanddraft.grt_intl
                                      : ""
                                  }
                                  name="grt"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Summer Draft" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="Summer Draft"
                                  value={
                                    e.sw_summer_draft && e.sw_summer_draft
                                      ? e.sw_summer_draft
                                      : ""
                                  }
                                  name="summer_draft"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="IMO No."
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="IMO No."
                                  defaultValue={
                                    e.imo_no && e.imo_no ? e.imo_no : ""
                                  }
                                  name="imo_no"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="SATC" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="SATC"
                                  value={
                                    e.contacts && e.contacts
                                      ? e.contacts.sat_c
                                      : ""
                                  }
                                  name="satc"
                                  onChange={handleChange}
                                  disabled
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="SATB Telefax" {...formItemLayout}>
                          {vesselDetails &&
                            vesselDetails &&
                            vesselDetails.length > 0 &&
                            vesselDetails.map((e) => {
                              return (
                                <Input
                                  placeholder="SATB Telefax"
                                  value={
                                    e.contacts && e.contacts
                                      ? e.contacts.sat_b
                                      : ""
                                  }
                                  disabled
                                  name="satb_telefax"
                                  onChange={handleChange}
                                />
                              );
                            })}
                        </FormItem>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </div>
            </div>
          </article>

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <h3 className="inner-heading">
                    PortCall Details{" "}
                    <EditOutlined onClick={edagenrowSelection} /> (Please
                    fill the Mendatory Port Details)
                  </h3>
                  <hr />
                  <Form>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          {...formItemLayout}
                          label="Country"
                          className={"astrick-on"}
                        >
                          <Select
                            value={postData.portcalldetails.country}
                            name="country"
                            className="border-none"
                            disabled
                          >
                            {agentDetails &&
                              agentDetails &&
                              agentDetails.length > 0 &&
                              agentDetails.map((e) => {
                                return (
                                  <Option
                                    key={e}
                                    value={
                                      e.country && e.country ? e.country : ""
                                    }
                                  >
                                    {e.country && e.country ? e.country : ""}
                                  </Option>
                                );
                              })}
                          </Select>
                        </FormItem>
                        <FormItem label="ETD" {...formItemLayout}>
                          <DatePicker
                            name="etd"
                            value={
                              postData.portcalldetails.etd &&
                              moment(postData.portcalldetails.etd)
                            }
                            onChange={(e) =>
                              handleChange(e, "portcalldetails", "etd")
                            }
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem
                          label="Port"
                          {...formItemLayout}
                          className={"astrick-on"}
                        >
                          <Select
                            name="portcall_port"
                            value={postData.portcalldetails.port}
                            className="border-none"
                            disabled
                          >
                            {agentDetails &&
                              agentDetails &&
                              agentDetails.length > 0 &&
                              agentDetails.map((e) => {
                                return (
                                  <Option
                                    key={e}
                                    value={e.port && e.port ? e.port : ""}
                                  >
                                    {e.port && e.port ? e.port : ""}
                                  </Option>
                                );
                              })}
                          </Select>
                        </FormItem>
                        <FormItem label="Next Port" {...formItemLayout}>
                          <Select
                            name="next_port"
                            value={postData.portcalldetails.next_port}
                            className="border-none"
                            //disabled
                            onChange={(e) =>
                              handleSelectNextPrePort(e, "next_port")
                            }
                          >
                            {vesselLists.port_itinerary &&
                              vesselLists.port_itinerary &&
                              vesselLists.port_itinerary.length > 0 &&
                              vesselLists.port_itinerary.map((e) => {
                                return (
                                  <Option
                                    key={e}
                                    value={e.port && e.port ? e.port : ""}
                                  >
                                    {e.port && e.port ? e.port : ""}
                                  </Option>
                                );
                              })}
                          </Select>
                          {/* <Input
                            placeholder="Next Port"
                            value={postData.portcalldetails.next_port}
                            name="next_port"
                            ///disabled
                            onChange={e => handleChange(e, 'portcalldetails')}
                          /> */}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Voyage No." {...formItemLayout}>
                          <Input
                            placeholder="Voyage No."
                            value={postData.portcalldetails.voyage_number}
                            name="voyage_number"
                            disabled
                            onChange={(e) =>
                              handleChange(e, "portcalldetails")
                            }
                          />
                        </FormItem>
                        <FormItem label="Previous Port" {...formItemLayout}>
                          <Select
                            name="previous_port"
                            value={postData.portcalldetails.previous_port}
                            className="border-none"
                            //disabled
                            onChange={(e) =>
                              handleSelectNextPrePort(e, "previous_port")
                            }
                          >
                            {vesselLists.port_itinerary &&
                              vesselLists.port_itinerary &&
                              vesselLists.port_itinerary.length > 0 &&
                              vesselLists.port_itinerary.map((e) => {
                                return (
                                  <Option
                                    key={e}
                                    value={e.port && e.port ? e.port : ""}
                                  >
                                    {e.port && e.port ? e.port : ""}
                                  </Option>
                                );
                              })}
                          </Select>
                          {/* <Input
                            placeholder="Previous Port"
                            value=""
                            name="previous_port"
                            onChange={e => handleChange(e, 'portcalldetails')}
                          /> */}
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        {console.log(postData.portcalldetails.eta)}
                        {console.log(postData.portcalldetails.etd)}
                        <FormItem label="ETA" {...formItemLayout}>
                          <DatePicker
                            name="eta"
                            value={
                              postData.portcalldetails.eta &&
                              moment(postData.portcalldetails.eta)
                            }
                            onChange={(e) =>
                              handleChange(e, "portcalldetails", "eta")
                            }
                          />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                        <FormItem label="Arr.Draft (m)" {...formItemLayout}>
                          <InputGroup compact>
                            <Input
                              style={{ width: "33%" }}
                              placeholder="m"
                              value={postData.portcalldetails.arr_draft_m}
                              name="arr_draft_m"
                              onChange={(e) =>
                                handleChange(e, "portcalldetails")
                              }
                            />
                            <Input
                              style={{ width: "33%" }}
                              placeholder="cm"
                              value={postData.portcalldetails.arr_draft_cm}
                              name="arr_draft_cm"
                              onChange={(e) =>
                                handleChange(e, "portcalldetails")
                              }
                            />
                            <Select
                              style={{ width: "33%" }}
                              value={postData.portcalldetails.arr_draft_sw}
                              name="arr_draft_sw"
                              onChange={(e) =>
                                handleChange(e, "portcalldetails")
                              }
                            >
                              <Option
                                value={postData.portcalldetails.arr_draft_sw}
                              >
                                {postData.portcalldetails.arr_draft_sw}
                              </Option>
                            </Select>
                          </InputGroup>
                        </FormItem>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </div>
            </div>
          </article>

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <h3 className="inner-heading">
                    Cargo / Activity Details (Please fill the Mendatory Cargo
                    Details)
                  </h3>
                  <hr />
                  <FormItem label="Select Cargo List">
                    <Select
                      placeholder="---Cargo List---"
                      value={
                        postData && postData.cargodetails
                          ? postData.cargodetails.charterer_name
                          : ""
                      }
                      name="cargo_list"
                      className="border-none"
                      onChange={selectCargo.bind(this)}
                    >
                      {cargoData[0] &&
                        cargoData[0].cargos &&
                        cargoData[0].cargos.length > 0 &&
                        cargoData[0].cargos.map((title) => {
                          return (
                            <Option key={title.id} value={title.id}>
                              {title.charterer_name}
                            </Option>
                          );
                        })}
                    </Select>
                  </FormItem>
                  {cargoSelected && postData && postData.cargodetails && (
                    <Form>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem
                            label="Charterer's Name"
                            {...formItemLayout}
                          >
                            <Input
                              placeholder="charterer name"
                              value={
                                postData.cargodetails.charterer_name &&
                                postData.cargodetails.charterer_name
                                  ? postData.cargodetails.charterer_name
                                  : ""
                              }
                              name="charterer_name"
                              onChange={(e) =>
                                handleChange(e, "cargodetails")
                              }
                              //disabled
                            />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="Quantity" {...formItemLayout}>
                            <InputGroup compact>
                              <Input
                                style={{ width: "50%" }}
                                name="quantity_min"
                                placeholder="Min"
                                value={
                                  postData.cargodetails.quantity_min
                                    ? postData.cargodetails.quantity_min
                                    : ""
                                }
                                onChange={(e) =>
                                  handleChange(e, "cargodetails")
                                }
                                //disabled
                              />
                              <Input
                                style={{ width: "50%" }}
                                name="quantity_max"
                                placeholder="Max"
                                value={
                                  postData.cargodetails.quantity_max
                                    ? postData.cargodetails.quantity_max
                                    : ""
                                }
                                onChange={(e) =>
                                  handleChange(e, "cargodetails")
                                }
                                //disabled
                              />
                            </InputGroup>
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="Unit" {...formItemLayout}>
                            <Input
                              placeholder="unit"
                              value={
                                postData.cargodetails.unit &&
                                postData.cargodetails.unit
                                  ? postData.cargodetails.unit
                                  : ""
                              }
                              name="unit"
                              onChange={(e) =>
                                handleChange(e, "cargodetails")
                              }
                              disabled
                            />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem
                            label="Activity"
                            {...formItemLayout}
                            className={"astrick-on"}
                          >
                            {postData.cargodetails.activity ? (
                              <Select
                                mode="multiple"
                                value={
                                  postData.cargodetails.activity
                                    ? postData.cargodetails.activity
                                    : ""
                                }
                                name="activity"
                                disabled
                                onChange={activitySelect}
                              >
                                {checboxName &&
                                  checboxName.map((activeVal, idx) => {
                                    return (
                                      <Option key={idx} value={activeVal.name}>
                                        {activeVal.name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            ) : (
                              <Select
                                mode="multiple"
                                value={
                                  postData.cargodetails.activity
                                    ? postData.cargodetails.activity
                                    : ""
                                }
                                name="activity"
                                //disabled
                                onChange={activitySelect}
                                //onChange={e => handleChange(e, 'cargodetails')}
                              >
                                {checboxName &&
                                  checboxName.map((activeVal, idx) => {
                                    return (
                                      <Option key={idx} value={activeVal.name}>
                                        {activeVal.name}
                                      </Option>
                                    );
                                  })}
                              </Select>
                            )}
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="Name Of Corgo" {...formItemLayout}>
                            <Input
                              placeholder="cargo name"
                              value={
                                postData.cargodetails.cargo_name &&
                                postData.cargodetails.cargo_name
                                  ? postData.cargodetails.cargo_name
                                  : ""
                              }
                              name="cargo_name"
                              onChange={(e) =>
                                handleChange(e, "cargodetails")
                              }
                              disabled
                            />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="CP Date" {...formItemLayout}>
                            <DatePicker
                              name="cp_date"
                              value={
                                postData.cargodetails.cp_date
                                  ? moment(postData.cargodetails.cp_date)
                                  : ""
                              }
                              onChange={(e) =>
                                handleChange(e, "cargodetails", "cp_date")
                              }
                              //disabled
                            />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="CP Place" {...formItemLayout}>
                            <Input
                              placeholder="CP Place"
                              value={
                                postData.cargodetails.cp_place
                                  ? postData.cargodetails.cp_place
                                  : ""
                              }
                              name="cp_place"
                              onChange={(e) =>
                                handleChange(e, "cargodetails")
                              }
                              //disabled
                            />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="Laycan Start" {...formItemLayout}>
                            <DatePicker
                              name="laycan_start"
                              value={
                                postData.cargodetails.laycan_start &&
                                moment(postData.cargodetails.laycan_start)
                              }
                              onChange={(e) =>
                                handleChange(
                                  e,
                                  "cargodetails",
                                  "laycan_start"
                                )
                              }
                              //disabled
                            />
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                          <FormItem label="Laycan End" {...formItemLayout}>
                            <DatePicker
                              name="laycan_end"
                              value={
                                postData.cargodetails.laycan_end &&
                                moment(postData.cargodetails.laycan_end)
                              }
                              onChange={(e) =>
                                handleChange(
                                  e,
                                  "cargodetails",
                                  "laycan_end"
                                )
                              }
                              //disabled
                            />
                          </FormItem>
                        </Col>
                      </Row>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                          <FormItem label="Remarks">
                            <TextArea
                              placeholder="Remarks"
                              name="remarks"
                              autoSize={{ minRows: 3, maxRows: 3 }}
                              onChange={(e) =>
                                handleChange(e, "cargodetails")
                              }
                            />
                          </FormItem>
                        </Col>
                      </Row>
                    </Form>
                  )}
                </div>
              </div>
            </div>
          </article>
          {/* 
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <h3 className="inner-heading">
                    Person(s) Contact Details <Icon type="phone" />
                  </h3>
                  <hr />
                  <Row gutter={16} className="m-b-18">
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">Name :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">sbms001</div>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">Phone No. :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">956382822838</div>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">Fax No. :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">956382822838</div>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">Home Phone No. :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">956382822838</div>
                        </Col>
                      </Row>
                    </Col>

                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">Phone :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">95628383922</div>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                      <Row className="wrap-label-value">
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="label">E-mail Id :</div>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                          <div className="value">pallave@gmail.com</div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </article> */}

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <h3 className="inner-heading">
                    Agent Instruction (Please Check the Box for Agent
                    Instruction)
                  </h3>
                  <hr />
                  <table className="table table-striped instructionset-table">
                    <thead>
                      <tr>
                        <td>Title</td>
                        <td>Write Up</td>
                        <td>Attchment</td>
                        <td>Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {intructionData &&
                      intructionData.hasOwnProperty("-") &&
                      intructionData["-"].length > 0
                        ? intructionData["-"].map((e, id) => {
                            let idx1 = e.id;
                            return (
                              <tr>
                                {/* <td>
                          <Checkbox id={idx} className="d-block mb-2"></Checkbox>
                        </td> */}
                                <td>
                                  <Input
                                    placeholder=""
                                    value={e.sub_title}
                                    name="sub_title"
                                    onChange={(e) =>
                                      intructionhandleChange(e, id)
                                    }
                                  />
                                </td>
                                <td>
                                  <TextArea
                                    placeholder="General"
                                    autoSize={{ minRows: 2, maxRows: 2 }}
                                    value={e.write_up}
                                    onChange={(e) =>
                                      intructionhandleChange(e, id)
                                    }
                                    name="write_up"
                                  />
                                </td>
                                <td>
                                  <p>
                                    {e.attachment}{" "}
                                    <button
                                      className="btn ant-btn-primary btn-sm rounded"
                                      onClick={() => onUploadFiles()}
                                    >
                                      15
                                      {/* {
                                    e.attachment.map((e, idx)=>{
                                      return(
                                        {idx}

                                      )
                                    })
                                    } */}
                                    </button>
                                  </p>
                                </td>

                                <td>
                                  <button className="btn btn-danger btn-sm">
                                    <DeleteOutlined
                                      onClick={(e) => onRowDelete(e, idx1)}
                                    />
                                  </button>
                                </td>
                              </tr>
                            );
                          })
                        : undefined}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </article>

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <div>
                    <Button type="primary" onClick={onAdd}>
                      Add
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </article>

          {/* <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div>
                  <Row gutter={16}>
                    {/* <Col xs={12} sm={12} md={12} lg={12} xl={12} >
                      <h3 className="inner-heading">Remark</h3>
                    </Col> 
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                      <h3 className="inner-heading">
                        Signature<EditOutlined />/>
                      </h3>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={16}>
                    {/* <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <TextArea
                        placeholder="Please Enter Operator Instruction Here"
                        autoSize={{ minRows: 3, maxRows: 3 }}
                      />
                    </Col> 
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                      <TextArea
                        placeholder="Please Enter"
                        autoSize={{ minRows: 3, maxRows: 3 }}
                        defaultValue="Suman Mumbai 03444333"
                      />
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </article> */}

          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="action-btn text-right">
                  {editActive ? (
                    ""
                  ) : (
                    <>
                      <Button
                        className="mr-2"
                        type="primary"
                        onClick={() =>
                          setState(prevState => ({
                            ...prevState,
                            showForm: true,
                            active: !state.active,
                          }))
                        }
                      >
                        Back
                      </Button>
                    </>
                  )}
                  {editActive ? (
                    <>
                      <Button
                        className="mr-2"
                        type="primary"
                        onClick={updateportcallData}
                      >
                        update
                      </Button>
                    </>
                  ) : (
                    <>
                      <Button
                        disabled
                        className="mr-2"
                        type="primary"
                        onClick={saveformData}
                      >
                        Save
                      </Button>
                    </>
                  )}

                  {!editActive && (
                    <>
                      <Button
                        className="mr-2"
                        type="primary"
                        onClick={createAgentAppoinment}
                        //onClick={() => setState({ active: !state.active })}
                      >
                        Save & send
                      </Button>
                    </>
                  )}

                  <Button
                    disabled
                    type="primary"
                    onClick={() =>
                      setState(prevState => ({ ...prevState, active: !state.active }))
                    }
                  >
                    Discard Port Call
                  </Button>
                </div>
              </div>
            </div>
          </article>
        </div>

        <Modal
          style={{ top: "2%" }}
          title="Agent Appointment Report"
         open={state.visible}
          // onOk={handleOk}
          // onCancel={handleCancel}
          width="90%"
          footer={null}
        >
          <AgencyAppointmentReport />
        </Modal>
        <Modal
          style={{ top: "2%" }}
          title="Add Instruction"
         open={state.addvisible}
          onOk={addHandleOk}
          onCancel={addCanelOK}
          width="40%"
        >
          <article className="article toolbaruiWrapper">
            <div className="box box-default">
              <div className="box-body">
                <Form>
                  <FormItem {...formItemLayout} label="Title">
                    <Input
                      type="text"
                      value={sub_title}
                      placeholder="title"
                      onChange={(e) =>
                        setState(prevState => ({ ...prevState, sub_title: e.target.value }))
                      }
                    />
                  </FormItem>
                  <FormItem
                    label={"Write up"}
                    colon={true}
                    labelCol={{ span: 10 }}
                    wrapperCol={{ span: 14 }}
                    className={"astrick-on"}
                  >
                    <Input
                      type="text"
                      value={write_up}
                      placeholder="write_up"
                      onChange={(e) =>
                        setState(prevState => ({ ...prevState, write_up: e.target.value }))
                      }
                    />
                  </FormItem>
                  <FormItem {...formItemLayout} label="Attchment">
                    <CloudUploadOutlined
                      onClick={() => onUploadBankDetails()}
                    />
                  </FormItem>
                </Form>
              </div>
            </div>
          </article>
        </Modal>

        <Modal
         open={filesvisibleall}
          style={{ top: "2%" }}
          title="All Uploaded Files"
          onOk={null}
          onCancel={() => showBankonUploadFiles(false)}
          width="40%"
          footer={null}
        >
          <article className="article toolbaruiWrapper">
            <div className="box box-default">
              <div className="box-body">
                <div className="row">
                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-md-6">
                    <a href="#">
                      <div className="file-upload-name p-2 border mb-3">
                        <h5 className="m-0">
                          <CloudUploadOutlined className="mr-2" />
                          File Name
                        </h5>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </Modal>
        {
          <Modal
           open={visibleBankAttachment}
            title="Upload Attachments"
            onOk={() => showBankAttachementModal(false)}
            onCancel={() => showBankAttachementModal(false)}
            footer={null}
            width={1000}
            maskClosable={false}
          >
            <Attachment
              uploadType="Portcall"
              directory={1}
              onCloseUploadFileArray={(fileArr) => uploadedFiles(fileArr)}
            />
          </Modal>
        }

        {visselEditvisible === true ? (
          <Modal
            title={"Edit Vessel"}
           open={visselEditvisible}
            width="95%"
            onCancel={adddHandleCancel}
            style={{ top: "10px" }}
            bodyStyle={{ height: 790, overflowY: "auto", padding: "0.5rem" }}
            footer={null}
          >
            <VesselSchedule formData={formDataValues} agencyVisselEdit={true} />
          </Modal>
        ) : undefined}

        {addresseditVisible === true ? (
          <Modal
            title={`Edit Address List`}
           open={addresseditVisible}
            width={"90%"}
            onCancel={edagenrowselectionCancel}
            style={{ top: "10px" }}
            bodyStyle={{ height: 790, overflowY: "auto", padding: "0.5rem" }}
            footer={null}
          >
            <AddAddressBook
              formDataValues={formDataValuesAddress}
              modalCloseEvent={onCancel}
            />
          </Modal>
        ) : undefined}
      </div>
    );
  }
export default AgencyAppointment;
