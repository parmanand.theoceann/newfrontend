import React from 'react';
import { Table, Layout } from 'antd';

import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from '../../../../shared';
import { FIELDS } from '../../../../shared/tableFields';
import DashboardChart from './components/DashboardChart';
import ToolbarUI from '../../../../components/ToolbarUI';

class PortcallList extends React.Component {
  onClickRightMenu = key => {
    if (key === 'pl-summary') {
      this.setState({
        visibleSummary: true,
        title: 'PL Summary',
      });
    } else if (key === 'estimates-summary') {
      this.setState({
        visibleSummary: true,
        title: 'Estimates Summary',
      });
    }
  };

  onCloseDrawer = () => {
    this.setState({
      visibleSummary: false,
    });
  };

  constructor(props) {
    super(props);

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['myport-calls-list'] ? FIELDS['myport-calls-list']['tableheads'] : []
    );
    // tableHeaders.push(tableAction)
    this.state = {
      frmName: 'tcto_form',
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };

  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
        {
          id: '1',
          create_date: '20 July 2021',
          portcall_refid: 'AA-180000215',
          vessel: 'MV SKYROS',
          port: 'Bombay',
          activity: 'Loading',
          agent: 'Demo Test Agent',
          status: 'Agency Appointment Accept',
          pdacost: '0.00 USD',
          fdacost: '0.00',
          etadate: '21 July 2021 02:32 PM',
          etddate: '22 July 2021 02:32 PM',
        },
      ],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/tcto/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  redirectToAdd = async (e, id = null) => {
    const { history } = this.props;
    history.push(`/edit-tc-estimate/${id.estimate_id}`);
    // return <Redirect to={`/add-tcto/${id.id}`}/>
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  onRowDeletedClick = id => {
    let _url = `${URL_WITH_VERSION}/tcto/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onSaveFormData = async data => {
    // let qParams = { "frm": "port_information" };
    // let qParamString = objectToQueryStringFunc(qParams);
    let { isAdd } = this.state;

    if (data && isAdd) {
      let _url = `${URL_WITH_VERSION}/port/save?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'post', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
      this.setState({ ...this.state, isAdd: false, formDataValues: {} }, () =>
        this.setState({ ...this.state, isVisible: false })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onEditFormData = async data => {
    let { isAdd } = this.state;

    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/tcto/update?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'put', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
      this.setState({ ...this.state, isAdd: false, formDataValues: {} }, () =>
        this.setState({ ...this.state, isVisible: false })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  render() {
    const { columns, loading, responseData, pageOptions, search } = this.state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));

    return (
      <div className="wrap-rightbar full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <div className="mb-3">
                    <ToolbarUI routeUrl="my_portcall_dashboard_menu" />
                  </div> 

                  <DashboardChart />
                  <div className="form-wrapper mt-3">
                    <div className="form-heading">
                      <h4 className="title">
                        <span>My Portcalls</span>
                      </h4>
                    </div>
                  </div>
                  <div
                    className="section"
                    style={{
                      width: '100%',
                      marginBottom: '10px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    {loading === false ? (
                      <ToolbarUI
                        routeUrl={'myport-calls-list-toolbar'}
                        optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                        callback={e => this.callOptions(e)}
                      />
                    ) : (
                      undefined
                    )}
                  </div>
                  <div>
                    <Table
                      // rowKey={record => record.id}
                      className="inlineTable editableFixedHeader resizeableTable"
                      bordered
                      scroll={{ x: 'max-content' }}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                      columns={tableColumns}
                      // size="small"
                      components={this.components}
                      dataSource={responseData}
                      loading={loading}
                      pagination={false}
                    />
                  </div>
                </div>
              </div>
            </article>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default PortcallList;
