import React from 'react';
import { Drawer, Table, Layout, Tag, Menu, Icon, Dropdown, Tooltip } from 'antd';
import DashboardChart from './DashboardChart';
import ToolbarUI2 from '../../../../../components/ToolbarUI/index';
import ToolbarUI from '../../../../../components/CommonToolbarUI/toolbar_index';
import { Button, Modal } from 'antd';
import SidebarColumnFilter from '../../../../../shared/SidebarColumnFilter';
import moment from 'moment';
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from '../../../../../shared';
import { FIELDS } from '../../../../../shared/tableFields';
import AddAddressBook from '../../../../../components/AddAddressBook';
import Barchart from './Barchart';

/* added by amar */

const voyagehistory = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/noon-verification">
        Noon verification
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/voyage-history-list">
        Voyage Historical Data
      </a>
    </Menu.Item>
  </Menu>
);

const spdconsdynamic = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/overall-performance-analysis">
        Overall Performance Analysis
      </a>
    </Menu.Item>

    {/* <Menu.Item>
      <a rel="noopener noreferrer" href="#/fuel-cons-analysis-vessel">
        Fuel Cons. Analysis Vessel
      </a>
    </Menu.Item> */}

  </Menu>
);


const activatevspm = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/vessel-activate-link">
        Vessel activate link list
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/reporting-form">
        Reporting form
      </a>
    </Menu.Item>
  </Menu>
);

/*-------------------------------*/

class MyPortDashboard extends React.Component {
  onClickRightMenu = key => {
    if (key === 'pl-summary') {
      this.setState({
        visibleSummary: true,
        title: 'PL Summary',
      });
    } else if (key === 'estimates-summary') {
      this.setState({
        visibleSummary: true,
        title: 'Estimates Summary',
      });
    }
  };

  onCloseDrawer = () => {
    this.setState({
      visibleSummary: false,
    });
  };



  constructor(props) {
    super(props);
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['myport-calls-list'] ? FIELDS['myport-calls-list']['tableheads'] : []
    );
    // tableHeaders.push(tableAction)
    this.state = {
      frmName: 'tcto_form',
      VoiID: this.props.match.params.id,
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      formDataValuesAddress: {},
      addresseditVisible: false,
      pdaCount: 0,
      fdaCount: 0,
      loadingMap: true,
      graphData: {
        vessel: [],
        pda: [],
        fda: []
      },
      agentColoumns: [
        {
          title: 'Created Date',
          dataIndex: 'created_on',
          key: 'created_on',
          invisible: 'false',
          width: 150,
          sorter: (a, b) => moment(a.created_on).unix() - moment(b.created_on).unix(),
          render(text) {
            return {
              props: {
                style: { color: '#0726ff' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'Portcall Ref. ID',
          dataIndex: 'port_call_ref_id',
          key: 'port_call_ref_id',
          invisible: 'false',
          width: 150,
          render: (text, row, index, data) => {
            return (
              <a style={{ color: '#1890ff' }} onClick={e => this.edagenrowRedirect(e, row, data)}>
                {text}
              </a>
            );
          },
        },
        {
          title: 'Vessel Name',
          dataIndex: 'vessel_name',
          key: 'vessel_name',
          invisible: 'false',
          width: 150,
          sorter: (a, b) =>a.vessel_name && b.vessel_name? a.vessel_name.localeCompare(b.vessel_name):null,
          render(text) {
            return {
              props: {
                style: { color: '#1d565c' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'Vessel Code',
          dataIndex: 'vessel_code',
          key: 'vessel_code',
          invisible: 'false',
          width: 150,
          sorter: (a, b) =>a.vessel_code && b.vessel_code? a.vessel_code.localeCompare(b.vessel_code):null,
          render(text) {
            return {
              props: {
                style: { color: '#1d565c' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'Port',
          dataIndex: 'portcall_port_name',
          key: 'portcall_port_name',
          invisible: 'false',
          width: 100,
          sorter: (a, b) => a.portcall_port_name.length - b.portcall_port_name.length,
        },
        {
          title: 'Activity',
          dataIndex: 'port_cargo_activity',
          key: 'port_cargo_activity',
          invisible: 'false',
          width: 100,
          //sorter: (a, b) => a.activity.length - b.activity.length,
        },
        {
          title: 'Agent',
          dataIndex: 'agent_name',
          key: 'agent_name',
          invisible: 'false',
          width: 150,
          render: (text, row, index, data) => {
            return <a onClick={e => this.edagenrowSelection(e, row, data)}>{text}</a>;
          },
        },
        {
          title: 'Status',
          dataIndex: 'status',
          key: 'status',
          invisible: 'false',
          width: 200,
          sorter: (a, b) => a.status.length - b.status.length,
          render: tags => (
            <span>
              <Tag
                color={
                  tags === 'SCHEDULED'
                    ? '#28a745'
                    : tags === 'SENT'
                      ? '#01bcd4'
                      : tags === 'REDELIVERED'
                        ? '#0726ff'
                        : tags === 'COMPLETED'
                          ? '#ff0000'
                          : tags === 'FIX'
                            ? '#81d742'
                            : '#9e9e9e'
                }
                key={tags}
              >
                {tags}
              </Tag>
            </span>
          ),
        },
        {
          title: 'PDA Cost',
          dataIndex: 'agreed_est_amt',
          key: 'agreed_est_amt',
          invisible: 'false',
          width: 100,
          sorter: (a, b) =>a.agreed_est_amt && b.agreed_est_amt? a.agreed_est_amt.length - b.agreed_est_amt.length:1,
          render(text) {
            return {
              props: {
                style: { color: '#28a745' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'FDA Cost',
          dataIndex: 'total_amt',
          key: 'total_amt',
          invisible: 'false',
          width: 100,
          sorter: (a, b) =>a.total_amt && b.total_amt? a.total_amt.length - b.total_amt.length:1,
          render(text) {
            return {
              props: {
                style: { color: '#28a745' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'ETA Date',
          dataIndex: 'eta',
          key: 'eta',
          invisible: 'false',
          width: 200,
          sorter: (a, b) => moment(a.eta).unix() - moment(b.eta).unix(),
          render(text) {
            return {
              props: {
                style: { color: '#0726ff' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'ETD Date',
          dataIndex: 'etd',
          key: 'etd',
          invisible: 'false',
          width: 200,
          sorter: (a, b) => moment(a.etd).unix() - moment(b.etd).unix(),
          render(text) {
            return {
              props: {
                style: { color: '#ff0000' },
              },
              children: <div>{text}</div>,
            };
          },
        },
      ],
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };

  getTableData = async (search = {}) => {
    const { pageOptions, VoiID } = this.state;
    let fda_approved_count = 0;
    let pda_approved_count = 0;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    //let headers = { order_by: { created_on: 'desc' } };
    let headers = {};

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      loadingMap: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/port-call/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    if (data.data.length > 0) {
      let cargo_activity = '';
      data.data.map(e => {
        e['status'] = e.status === 1 ? 'SENT' : e.status;
        if (e.pda_adv_status == 'Actual') {
          pda_approved_count = pda_approved_count + 1;
        }
        if (e.fda_status == 'Actual') {
          fda_approved_count = fda_approved_count + 1;
        }
        if (e.port_cargo_activity) {
          //e['port_cargo_activity'] = e.port_cargo_activity.slice(1, -1)
          e['port_cargo_activity'] = e['port_cargo_activity'].replace(/['"]+/g, '');
          e['port_cargo_activity'] = e.port_cargo_activity.replace('[', '');
          e['port_cargo_activity'] = e.port_cargo_activity.replace(']', '');
        }
      });
    }
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };

    if (dataArr.length > 0) {
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
      pdaCount: pda_approved_count,
      fdaCount: fda_approved_count
    }, () => this.graphData(dataArr));
  };

  graphData = async (data) => {
    let vessel = [], pda = [], fda = []
    if (data && data.length > 0) {
      data.map((val, ind) => {
        if (val && val.agreed_est_amt) {
          pda.push(parseFloat(val.agreed_est_amt))
        }
        else {
          pda.push(0)
        }
        if (val && val.total_amt) {
          fda.push(parseFloat(val.total_amt))
        }
        else {
          fda.push(0)
        }
        if (val && val.port_call_ref_id) {
          vessel.push(val.port_call_ref_id)
        }
        else {
          vessel.push("")
        }
      })
    }
    this.setState({
      loadingMap: false,
      graphData: {
        vessel,
        pda,
        fda
      }
    })
  }

  redirectToAdd = async (e, id = null) => {
    const { history } = this.props;
    history.push(`/edit-tc-estimate/${id.estimate_id}`);
    // return <Redirect to={`/add-tcto/${id.id}`}/>
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  onRowDeletedClick = id => {
    let _url = `${URL_WITH_VERSION}/tcto/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  edagenrowSelection = async (event, row, data) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${row.id}`);
    const respData = await response;
    this.setState({ ...this.state, formDataValuesAddress: respData['data'] }, () => {
      this.setState({ ...this.state, addresseditVisible: true });
    });
  };

  edagenrowRedirect = async (event, row, data) => {
    const { history } = this.props;
    history.push(`/portcall-details/${row.id}`);
  };
  edagenrowselectionCancel = () => {
    this.setState({
      ...this.state,
      addresseditVisible: false,
    });
  };

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onSaveFormData = async data => {
    // let qParams = { "frm": "port_information" };
    // let qParamString = objectToQueryStringFunc(qParams);
    let { isAdd } = this.state;

    if (data && isAdd) {
      let _url = `${URL_WITH_VERSION}/port/save?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'post', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
      this.setState({ ...this.state, isAdd: false, formDataValues: {} }, () =>
        this.setState({ ...this.state, isVisible: false })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onEditFormData = async data => {
    let { isAdd } = this.state;

    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/tcto/update?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'put', response => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
      this.setState({ ...this.state, isAdd: false, formDataValues: {} }, () =>
        this.setState({ ...this.state, isVisible: false })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onActionDonwload = (downType, pageType) => {
      let params = `t=${pageType}`, cols = [];
   const { columns, pageOptions } = this.state;  



    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }

  render() {
    const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      agentColoumns,
      addresseditVisible,
      formDataValuesAddress,
      sidebarVisible,
      loadingMap,
      graphData
    } = this.state;
    const tableColumns = agentColoumns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
      }));
    return (
      <div className="wrap-rightbar full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <div class="row">
                    <div className="col-md-2">
                      <div className="p-2 dynamic-vspm-left-menu-custome">
                        <ul className="p-0">
                          <li>

                            <a href="#">Portcall List</a>
                          </li>
                          <li>
                            <a href="#/port-cost" target='_blank' >PortCost Analytics</a>
                          </li>

                          <li>
                            <a href="#/pda-list" target='_blank'>Port expenses Summary</a>
                          </li>


                          <li>
                            <a href="#/port-details" target='_blank'>Port Information Data search</a>
                          </li>


                          <li>
                            <a href="#/instruction-set" target='_blank'>Instruction set</a>
                          </li>

                          <li>

                            <a href="#" >Chat agent</a>
                          </li>






                          {/* <li>
                        <Dropdown overlay={voyagehistory}>
                          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                            Voyage History <DownOutlined />
                          </a>
                        </Dropdown>
                      </li> */}

                          {/* <li>
                        <Dropdown overlay={spdconsdynamic}>
                          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                            Voyage performance report <DownOutlined />
                          </a>
                        </Dropdown>
                      </li>*/}





                          {/* <li>
                        <Dropdown overlay={activatevspm}>
                          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                            Activate VSPM
                            <DownOutlined />
                          </a>
                        </Dropdown>
                      </li>  */}
                        </ul>
                      </div>
                    </div>

                    <div class="col-md-10">
                      {/* <div className="mb-3">
                    <ToolbarUI2 routeUrl="my_portcall_dashboard_menu" />
                  </div> */}

                      <DashboardChart {...this.state} />
                      {loadingMap === false ? <Barchart data={graphData} /> : undefined}


                    </div>

                  </div>

                </div>

              </div>

              {/* <div>
                    <div className="form-heading">
                      <h4 className="title">
                        <span>My Portcalls</span>
                      </h4>
                    </div>
                  </div> */}
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={'user-port-call-list-toolbar'}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: agentColoumns,
                      search: search,
                    }}
                    callback={e => this.callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv', 'portcall') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf', 'portcall') },
                      { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'portcall') },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>


              <div>
                <Table
                  rowKey={record => record.id}
                  className="inlineTable  resizeableTable"
                  bordered
                  scroll={{ x: 'max-content' }}
                  columns={tableColumns}
                  size="small"
                  components={this.components}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                  }
                />
              </div>
            </article>
          </Layout>
        </Layout>

        {addresseditVisible === true ? (
          <Modal
            title={`Edit Address List`}
           open={addresseditVisible}
            width={'90%'}
            onCancel={this.edagenrowselectionCancel}
            style={{ top: '10px' }}
            bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
            footer={null}
          >
            <AddAddressBook
              formDataValues={formDataValuesAddress}
              modalCloseEvent={this.onCancel}
            />
          </Modal>
        ) : (
          undefined
        )}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={agentColoumns}
            sidebarVisible={sidebarVisible}
            callback={e => this.callOptions(e)}
          />
        ) : null}
      </div>
    );
  }
}

export default MyPortDashboard;
