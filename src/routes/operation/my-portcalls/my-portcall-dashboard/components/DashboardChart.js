import React, { Component } from 'react';
import { Card } from 'antd';

class DashboardChart extends Component {
    render() {
      const { pdaCount, fdaCount } = this.props;
        return (
            <>
            <div className="portcall-dashboard-wrap">
                    <Card size="small" title="New Portcall" className="card-block-wrap">
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/1.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">34</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Accepted Portcall"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/2.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">6</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="PDA Approved"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/3.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">{pdaCount}</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="FDA Approved"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/4.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">{fdaCount}</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Close Portcall"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/1.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">0</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Resubmision"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/2.png"
                            className="port-img-responsive"
                          /> */}
                        </div>
                        <div className="portcall-rate-no">0</div>
                      </div>
                    </Card>
                  </div>
                {/* <div className="portcall-dashboard-wrap">
                    <Card size="small" title="New Agency Appointment" className="card-block-wrap">
                        <div className="port-row-wrap">
                            <div className="portcall-icons">
                                <img src="assets/images-demo/portcall-dashboard/1.png" className="port-img-responsive" />
                            </div>
                            <div className="portcall-rate-no">
                                34
                            </div>
                        </div>
                    </Card>

                    <Card size="small" title="PDA / Advance Awaiting Approval" className="card-block-wrap">
                        <div className="port-row-wrap">
                            <div className="portcall-icons">
                                <img src="assets/images-demo/portcall-dashboard/2.png" className="port-img-responsive" />
                            </div>
                            <div className="portcall-rate-no">
                                6
                            </div>
                        </div>
                    </Card>

                    <Card size="small" title="FDA / Advance Awaiting Approval" className="card-block-wrap">
                        <div className="port-row-wrap">
                            <div className="portcall-icons">
                                <img src="assets/images-demo/portcall-dashboard/3.png" className="port-img-responsive" />
                            </div>
                            <div className="portcall-rate-no">
                                5
                            </div>
                        </div>
                    </Card>

                    <Card size="small" title="Additional Invoice Awaiting Approval" className="card-block-wrap">
                        <div className="port-row-wrap">
                            <div className="portcall-icons">
                                <img src="assets/images-demo/portcall-dashboard/4.png" className="port-img-responsive" />
                            </div>
                            <div className="portcall-rate-no">
                                0
                            </div>
                        </div>
                    </Card>

                    <Card size="small" title="Vendor Service" className="card-block-wrap">
                        <div className="port-row-wrap">
                            <div className="portcall-icons">
                                <img src="assets/images-demo/portcall-dashboard/5.png" className="port-img-responsive" />
                            </div>
                            <div className="portcall-rate-no">
                                0
                            </div>
                        </div>
                    </Card>
                </div> */}
            </>
        )
    }
}

export default DashboardChart;