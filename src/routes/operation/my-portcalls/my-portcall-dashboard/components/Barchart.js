import React from 'react';
import 'echarts/theme/macarons';

import CHARTCONFIG from '../../../../../constants/chartConfig';
import ReactEcharts from 'echarts-for-react';

class Bar2 extends React.Component {

constructor(props) {
  super(props);
  this.state={
    seriesSimpleBar: [{
      name: 'PDA Cost',
      data:this.props.data.pda
    }, {
      name: 'FDA Cost',
      data: this.props.data.fda
    }],
    optionsSimpleBar: {
      chart: {
        type: 'bar',
        height: 350,
        stacked: true,
        toolbar: {
          show: true
        },
        zoom: {
          enabled: true
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            position: 'bottom',
            offsetX: -10,
            offsetY: 0
          }
        }
      }],
      plotOptions: {
        bar: {
          horizontal: false,
          borderRadius: 10
        },
      },
      xaxis: {
        type: '',
        categories:this.props.data.vessel,
      },
      legend: {
        position: 'right',
        offsetY: 40
      },
      fill: {
        opacity: 1
      }
    },
    option : {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['PDA Cost', 'FDA Cost'],
        textStyle: {
          color: CHARTCONFIG.color.text
        }
      },
      calculable: true,
      xAxis: [
        {
          axisLabel: {
            interval: 0,
            rotate: 32,
            textStyle: {
              baseline: "top",
              color: "#333",
              fontSize: 10,
              fontWeight: "bold"
            }
          },
          axisLine: { lineStyle: { color: "#aaa" }, show: true },
          axisTick: { show: false },
          data: this.props.data.vessel,
          splitLine: { show: false },
          type: "category"
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: CHARTCONFIG.color.text
            }
          },
          splitLine: {
            lineStyle: {
              color: CHARTCONFIG.color.splitLine
            }
          },
          splitArea: {
            show: true,
            areaStyle: {
              color: CHARTCONFIG.color.splitArea
            }
          }
        }
      ],
      series: [
        {
          name: 'PDA Cost',
          type: 'bar',
          stack: 'Search',
          data:this.props.data.pda
        },
        {
          name: 'FDA Cost',
          type: 'bar',
          stack: 'Search',
          data:this.props.data.fda
        }
      ]
    }
  }
}

render(){
  return(
<div className="box box-default mb-4">
    <div className="box-header">
      <h4>
        Estimate VS Actual Compare
      </h4>
    </div>
    <div className="box-body">
    {/* <Chart options={this.state.optionsSimpleBar} series={this.state.seriesSimpleBar} type="bar" height={500} /> */}
    <ReactEcharts style={{height:'450px'}} option={this.state.option} theme={"macarons"} />
    </div>
  </div>
  )
}
}

export default Bar2;

