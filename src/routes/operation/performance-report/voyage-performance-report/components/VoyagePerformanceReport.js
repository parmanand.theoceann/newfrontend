import React from 'react';

const Page = () => {
  return (
    <div className="body-wrapper">
    <article className="article">
        <div className="box box-default">
            <div className="box-body">
                Voyage Performance Report
            </div>
        </div>
    </article>
</div>
  );
}

export default Page;