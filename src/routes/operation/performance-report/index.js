import React from 'react';
import { Route } from 'react-router-dom';

import BunkerSummaryReport from './bunker-summary-report';
import DelayReports from './delay-reports';
import DemurrageReport from './demurrage-report';
import PortActivityReport from './port-activity-report';
import PortExpensesSummary from './port-expenses-summary';
import VoyagePerformanceReport from './voyage-performance-report';
import VoyagePLReport from './voyage-pl-report';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/bunker-summary-report`} component={BunkerSummaryReport} />
    <Route path={`${match.url}/delay-reports`} component={DelayReports} />
    <Route path={`${match.url}/demurrage-report`} component={DemurrageReport} />
    <Route path={`${match.url}/port-activity-report`} component={PortActivityReport} />
    <Route path={`${match.url}/port-expenses-summary`} component={PortExpensesSummary} />
    <Route path={`${match.url}/voyage-performance-report`} component={VoyagePerformanceReport} />
    <Route path={`${match.url}/voyage-pl-report`} component={VoyagePLReport} />
  </div>
)

export default CardComponents;