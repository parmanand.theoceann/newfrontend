import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

import CHARTCONFIG from '../../../../../constants/chartConfig';
import { Card } from 'antd';

let line1 = {};

line1.option = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['Dem', 'Des'],
        textStyle: {
            color: CHARTCONFIG.color.text
        }
    },
    toolbox: {
        show: true,
        feature: {
            saveAsImage: { show: true, title: 'save' }
        }
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: ['Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.', 'Sun.'],
            axisLabel: {
                formatter: '{value} °C',
                textStyle: {
                    color: CHARTCONFIG.color.text
                }
            },
            splitLine: {
                lineStyle: {
                    color: CHARTCONFIG.color.splitLine
                }
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            axisLabel: {
                formatter: '{value} °C',
                textStyle: {
                    color: CHARTCONFIG.color.text
                }
            },
            splitLine: {
                lineStyle: {
                    color: CHARTCONFIG.color.splitLine
                }
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: CHARTCONFIG.color.splitArea
                }
            }
        }
    ],
    series: [
        {
            name: 'Dem',
            type: 'line',
            data: [11, 11, 15, 13, 12, 13, 10],
            markPoint: {
                data: [
                    { type: 'max', name: 'Max' },
                    { type: 'min', name: 'Min' }
                ]
            },
            markLine: {
                data: [
                    { type: 'average', name: 'Average' }
                ]
            }
        },
        {
            name: 'Des',
            type: 'line',
            data: [1, -2, 2, 5, 3, 2, 0],
            markPoint: {
                data: [
                    { name: 'Des', value: -2, xAxis: 1, yAxis: -1.5 }
                ]
            },
            markLine: {
                data: [
                    { type: 'average', name: 'Average' }
                ]
            }
        }
    ]
};

const LayTimeDeskChart = () => (
  <div className="box box-default mb-4">
    <div className="box-body">
    <Card size="small" title="Overall Claim Month Wise" extra={<span>More</span>} style={{ width: '100%' }}>
      <ReactEcharts option={line1.option} theme={"macarons"} style={{ height: '450px' }} />
      </Card>
    </div>
  </div>
)

export default LayTimeDeskChart;