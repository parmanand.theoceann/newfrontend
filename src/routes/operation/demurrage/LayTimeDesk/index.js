import React, { Component } from 'react';
import LayTimeDeskChart from './LayTimeDeskChart.js';

class LayTimeDesk extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <div className="custom-row-wrap">
                    <div className="custom-col-wrap">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/negotiation.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">File Under Negotiation</h4>
                                            <div className="icon-card__content text-bold">46</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="custom-col-wrap">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/invoiced.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Invoiced & Agreed</h4>
                                            <div className="icon-card__content text-bold">1</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="custom-col-wrap">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/dispute.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Dispute File</h4>
                                            <div className="icon-card__content text-bold">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="custom-col-wrap">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/payment-received.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Payment Received</h4>
                                            <div className="icon-card__content text-bold">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="custom-col-wrap">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/claim-settled.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Claim Settled File</h4>
                                            <div className="icon-card__content text-bold">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xl-3">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/owner.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Claim with Owner</h4>
                                            <div className="icon-card__content text-bold">$ 3.1841.6667</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="col-xl-3">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/purchaser.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Claim with Purchaser</h4>
                                            <div className="icon-card__content text-bold">$ 0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="col-xl-3">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/seller.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Claim with Seller</h4>
                                            <div className="icon-card__content text-bold">$ 0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div className="col-xl-3">
                        <article className="article">
                            <div className="box box-default h-100">
                                <div className="box-body">
                                    <div className="media icon-card-v1">
                                        <div className="icon-card__icon icon--circle mr-3">
                                            {/* <img src="assets/images-demo/image-icons/buyer.png" alt="icon" /> */}
                                        </div>
                                        <div className="media-body">
                                            <h4 className="icon-card__header">Claim with Buyer</h4>
                                            <div className="icon-card__content text-bold">$ 83937.0482</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <LayTimeDeskChart />
                    </div>
                </div>

            </div>
        )
    }
}

export default LayTimeDesk;