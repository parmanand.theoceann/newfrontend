import React from 'react';
import { Route } from 'react-router-dom';

import LaytimeCalculator from './laytime-calculator';
import ClaimDashboard from './claim-dashboard';
import ClaimPayableClaimStatus from './claim-payable-claim-status';
import LayTimeDesk from './LayTimeDesk';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/laytime-calculator`} component={LaytimeCalculator} />
    <Route path={`${match.url}/claim-dashboard`} component={ClaimDashboard} />
    <Route path={`${match.url}/claim-payable-claim-status`} component={ClaimPayableClaimStatus} />
    <Route path={`${match.url}/laytime-desk`} component={LayTimeDesk} />
  </div>
)

export default CardComponents;