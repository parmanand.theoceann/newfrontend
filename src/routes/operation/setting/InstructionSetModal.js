import React, { Component } from 'react';
import { Form, Input, Button, Tabs, Select } from 'antd';
import URL_WITH_VERSION, { getAPICall, postAPICall,openNotificationWithIcon } from '../../../shared';
//import axios from 'axios';


const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

class InstructionSetModal extends Component {
  constructor(props) {
    super(props);
     this.state = {
      selectedOption: [],
      value: '',
      createTitle: {
        name: '',
      },
      newTitle:'',
      instruction: this.props.instruction || [],
      genID:this.props.checkedID || 0,
      activeTab:0,
      activeKey:this.props.checkedID ? 2 : 0,
    };
  }

  
  changeTab = activeKey => {
    this.setState({
      activeTab: activeKey
    });
  };


  componentDidMount = async () => {
     const { instruction } = this.state;
    if(instruction.length < 0){
      const response = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
      const res = await response['data'];
      this.setState({ ...this.state, instruction: res });
    }
  };

  

  onSaveUpdate=()=>{
    const {activeTab} = this.state
    if(activeTab==="1"){
      this.onSave()
    }else{
      this.onUpdate()
    }
  }

  onSave = async () => {
    
    await postAPICall(
      `${URL_WITH_VERSION}/port-call/gisave`,
      this.state.createTitle,'POST',response=>{
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          this.props.parentCallback();
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
  };

  onUpdate= async() => {
    
    const {newTitle, genID} =this.state
    const data ={
      "id":genID,
      "name":newTitle
    }
    await postAPICall(`${URL_WITH_VERSION}/port-call/giupdate`,data,'put',response2=>{
      if (response2 && response2.data) {
        openNotificationWithIcon('success', response2.message);
        this.props.parentCallback();
      } else {
        openNotificationWithIcon('error', response2.message);
      }
    });
  }
  
handleSelect=(e)=>{
  this.setState({ genID: e });
}

 handleUpdateChange=(e,item)=>{
   this.setState({newTitle:e.target.value})
 }

  handleChange = e => {
    let createTitle = { name: e.target.value };
    this.setState({ ...this.state, createTitle: createTitle });
  };

 

  render() {
    const {newTitle, genID} = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Tabs activeKey={this.state.activeTab} size="small" onChange={this.changeTab}>
                <TabPane className="pt-3" tab="Create" key="1" >
                  <FormItem htmlFor="my-form" label="Create Title Name">
                    <Input type="text" name="name" size="default" onChange={this.handleChange}  />
                  </FormItem>
                </TabPane>
                <TabPane className="pt-3" tab="Update" key="2"  >
                  <FormItem label="Select Title">
                    <Select size="default" value={genID} onChange={(e) => this.handleSelect(e)}>
                      {this.state.instruction.map(e => {
                        
                        return <Option value={e.id} >{e.name}</Option>;
                        
                      })
                      
                    }
                    </Select>
                  </FormItem>

                  <FormItem label="New Title Name" >
                    <Input type="text" value={newTitle}  size="default" onChange={(e,item)=>this.handleUpdateChange(e,item)}/>
                  </FormItem>
                </TabPane>
              </Tabs>

              <FormItem>
                <div className="row p10">
                  <div className="col-md-12">
                    <div className="action-btn text-left">

                          <Button
                          className="ant-btn ant-btn-primary"
                          value="submit"
                          type="submit"
                          htmlFor="my-form"
                         onClick={this.onSaveUpdate}
                         
                        >
                          Save
                        </Button>
                        
                     
                      <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                    </div>
                  </div>
                </div>
              </FormItem>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default InstructionSetModal;
