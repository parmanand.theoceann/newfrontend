import React, { Component } from 'react';
import { Form, Input, Button, Tabs/*, Select*/ } from 'antd';
import URL_WITH_VERSION, { /*awaitPostAPICall , */openNotificationWithIcon, postAPICall } from '../../../shared';

const { TabPane } = Tabs;
const FormItem = Form.Item;

export class AddNewRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sub_title: '',
      write_up: '',
      attachment: '',
      rID: 0,
    };
  }

  anotherID = () => {
    this.setState({
      rID: this.props.instructionID,
    });
  };

  onSave = () => {
    const { /*anotherID, */sub_title, write_up, attachment } = this.state;
    const { instructionID } = this.props;

    const data = {
      g_id: instructionID,
      sub_title: sub_title,
      write_up: write_up,
      attachment: attachment,

    }
    postAPICall(`${URL_WITH_VERSION}/port-call/stsave`, data, 'POST', response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.props.rowCallBack();
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  render() {
    const { sub_title, write_up, attachment } = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Tabs defaultActiveKey="anotherone" size="small">
                <TabPane className="pt-3" tab="Create" key="000n">
                  <FormItem htmlFor="my-form" label="Add New">
                    <Input
                      type="text"
                      name="sub_title"
                      value={sub_title}
                      size="default"
                      onChange={e => this.setState({ sub_title: e.target.value })}
                    />
                    <Input
                      type="text"
                      name="write_up"
                      value={write_up}
                      size="default"
                      onChange={e => this.setState({ write_up: e.target.value })}
                    />
                    <Input
                      type="text"
                      name="attachment"
                      value={attachment}
                      size="default"
                      onChange={e => this.setState({ attachment: e.target.value })}
                    />
                  </FormItem>
                </TabPane>
              </Tabs>

              <FormItem>
                <div className="row p10">
                  <div className="col-md-12">
                    <div className="action-btn text-left">
                      <Button
                        className="ant-btn ant-btn-primary"
                        value="submit"
                        type="submit"
                        htmlFor="my-form"
                        onClick={this.onSave}
                      >
                        Save
                      </Button>
                      <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                    </div>
                  </div>
                </div>
              </FormItem>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default AddNewRow;
