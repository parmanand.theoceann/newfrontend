import React, { Component } from 'react';
import { Table, Button, Modal, Checkbox,  Input } from 'antd';
import { DeleteOutlined ,UploadOutlined,EditOutlined} from '@ant-design/icons';
import InstructionSetModal from './InstructionSetModal';
import URL_WITH_VERSION, { getAPICall, apiDeleteCall, openNotificationWithIcon, postAPICall } from '../../../shared';
import AddNewRow from './AddNewRow';


class InstructionSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
      active: false,
      instruction: [],
      subName: [],
      subTitle: [],
      details: [],
      isDisabled: true,
      checked: true,
      title: [],
      tableData: [],
      titleName: [],
      rowvisible: false,
      data: [],
      instructionID: 0,
      checkedID: 0,
      rowID: 0,
      listRes: [],
      xyz: false,
      sub_title: '',
      write_up: '',
      attachment: '',
      gID: 0,
      sID: 0,
      deleteModal: 0,
      updatedSubs: [],
      checkValueName: '',
      columns2: [
        {
          title: 'Sub Title',
          dataIndex: 'sub_title',
        },

        {
          title: 'Write Up',
          dataIndex: 'write_up',
        },

        {
          title: 'Attachment',
          dataIndex: 'attachment',
          render: () => (
            <div className="editable-row-operations">
              <span className="iconWrapper">
            <UploadOutlined />
              </span>
            </div>
          ),
        },
        {
          title: 'Active',
          dataIndex: 'attachment',
          render: (text, pd, e, record) => (
            <div className="editable-row-operations">
             <EditOutlined onClick={e => this.updateVisible(e, pd)} />
             <DeleteOutlined 

                onClick={e => this.deleteVisible(e, pd)}
                style={{ color: 'red', marginLeft: 12 }}
              />
            </div>
          ),
        },
      ],
    };
    this.onRowDelete = this.onRowDelete.bind(this);
  }

  componentDidMount = async () => {
    const response1 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const res = await response1['data'];
    this.setState({ ...this.state, instruction: res });

  }


  getList = async () => {
    const response2 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const res2 = await response2['data'];
    this.setState({ ...this.state, instruction: res2 });
  }

  onUpdate = () => {
    const { sID, gID, sub_title, write_up, attachment } = this.state;


    const data3 = [{
      id: sID,
      g_id: gID,
      sub_title: sub_title,
      write_up: write_up,
      attachment: attachment,
    }]
    postAPICall(`${URL_WITH_VERSION}/port-call/stupdate`, data3, 'put', respo2 => {
      if (respo2 && respo2.data) {
        openNotificationWithIcon('success', respo2.message);

      } else {
        openNotificationWithIcon('error', respo2.message);
      }
      this.handleCallRow()
    })

  };


  onRowDelete = () => {
    const { gID, sID } = this.state;

    let data = {
      id: sID,
      g_id: gID,
    };
    apiDeleteCall(`${URL_WITH_VERSION}/port-call/stdelete`, data, respon2 => {
      if (respon2 && respon2.data) {
        openNotificationWithIcon('success', respon2.message);

      } else {
        openNotificationWithIcon('error', respon2.message);
      }
      this.handleCall()
    });

  };

  onRowEdit = record => { };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  rowVisible = (e, id) => {
    this.setState({
      instructionID: e.id,
      rowvisible: true,
    });
  };

  updateVisible = (e, pd) => {
    this.setState({
      xyz: true,
      'gID': pd.g_id,
      'sID': pd.id,
      'sub_title': pd.sub_title,
      'write_up': pd.write_up,
      'attachment': pd.attachment
    });
  };

  deleteVisible = (e, pd) => {
    this.setState({
      deleteModal: true,
      gID: pd.g_id,
      sID: pd.id,
    });

  };

  getData = async () => {
    const response3 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const resp3 = await response3['data'];
    this.setState({ listRes: resp3 });
  };

  onDelete = async () => {
    const { checkedID } = this.state;
    let data = { id: checkedID };
    apiDeleteCall(`${URL_WITH_VERSION}/port-call/gidelete`, data, res => {
      if (res && res.data) {
        openNotificationWithIcon('success', res.message);

      } else {
        openNotificationWithIcon('error', res.message);
      }
      this.handleCall()
    });


  };


  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  rowOk = e => {
    this.setState({
      rowvisible: false,
    });
  };

  updateRowOk = e => {
    this.setState({
      xyz: false,
    });
  };

  deleteRowOk = e => {
    this.setState({
      deleteModal: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  rowCancel = e => {
    this.setState({
      rowvisible: false,
    });
  };
  updateRowCancel = e => {
    this.setState({
      xyz: false,
    });
  };

  deleteRowCancel = e => {
    this.setState({
      deleteModal: false,
    });
  };

  handleCallback = async () => {
    this.setState({
      visible: false,
    });

    const response2 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const resv = await response2['data'];
    this.setState({ ...this.state, instruction: resv });
  };

  handleCall = async () => {

    this.setState({
      deleteModal: false
    })
    const response2 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const resv = await response2['data'];
    this.setState({ ...this.state, instruction: resv });
  }

  handleCallRow = async () => {

    this.setState({
      xyz: false,
    });
    const response2 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const resv = await response2['data'];
    this.setState({ ...this.state, instruction: resv });
  }


  handleCallbackRow = async () => {
    this.setState({
      rowvisible: false,
    });

    const response2 = await getAPICall(`${URL_WITH_VERSION}/port-call/listgeninst?l=0`);
    const resv = await response2['data'];
    this.setState({ ...this.state, instruction: resv });
  };

  handleCheckboxChecked = val => {
    this.setState({ checked: !this.state.checked, checkedID: val.id });
    if (this.state.checked) {
      this.setState({ isDisabled: false, checkValueName: val.name });
    } else {
      this.setState({ isDisabled: true, checkedID: val.id });
    }
  };

  handleUpdateChange = e => {
    const { name, value } = e.target;
    this.setState({ ...this.state, [name]: value });
  };

  render() {
    const { details } = this.state;
    const { instruction, sub_title, write_up, attachment, checkValueName, checkedID } = this.state;

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Instruction Set</span>
                  </h4>
                </div>

                <div class="action-btn">
                  {/* <a href="#/user-dashboard" className="btn btn-primary pl-2 pr-2 pt-1 pb-1 mr-2">
                       <LeftCircleOutlined />
                        </a> */}
                  <button
                    type="submit"
                    class="ant-btn ant-btn-primary"
                    onClick={this.showModal}
                    onOk={this.rowOk}
                  >
                    <span>Create Title</span>
                  </button>
                  <button
                    onClick={this.onDelete}
                    onSubmit={this.refreshPage}
                    disabled={this.state.isDisabled}
                    type="button"
                    class="ant-btn ant-btn-danger"
                  >
                    <span>Delete</span>
                  </button>
                </div>
              </div>

              {this.state.instruction && this.state.instruction.length > 0 ? (
                <div className="row">
                  {this.state.instruction.map(e => {
                    return (
                      <div className="col-md-6">
                        <Table
                          bordered
                          columns={this.state.columns2}
                          dataSource={e.instruction}
                          pagination={false}
                          scroll={{ x: 'max-content' }}
                          title={() => (
                            <div className="table-header-wrapper">
                              <div className="form-heading">
                                <div className="title">
                                  <Checkbox
                                    value={this.state.checked}
                                    onChange={() => this.handleCheckboxChecked(e)}
                                  >
                                    {e.name}
                                  </Checkbox>

                                  <span className="ml-3">
                                    <b value={this.state.titleName} onChange={this.handleCallback}>
                                      {this.state.titleName}
                                    </b>
                                  </span>
                                </div>
                              </div>
                            </div>
                          )}
                          footer={() => (
                            <div className="text-center">
                              <Button type="link" onClick={() => this.rowVisible(e)}>
                                Add New
                              </Button>
                            </div>
                          )}
                          rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                        />
                      </div>
                    );
                  })}

                  {/* <div className="col-md-6">
                  <Table
                
                    bordered
                    columns={this.state.columns2}
                    dataSource=""
                    pagination={false}
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                    title={() => (
                      <div className="table-header-wrapper">
                        <div className="form-heading">
                          <div className="title">
                            <Checkbox />
                            <span className="ml-3">
                            <b></b>
                            </span>
                          </div>
                        </div>
                      </div>
                    )}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                  />
                </div> */}
                </div>
              ) : (
                undefined
              )}
            </div>
          </div>
        </article>

        <Modal
          style={{ top: '2%' }}
          title="Add New"
         open={this.state.rowvisible}
          onOk={this.rowOk}
          onCancel={this.rowCancel}
          width="40%"
          footer={null}
        >
          <AddNewRow
            instructionID={this.state.instructionID}
            rowCallBack={this.handleCallbackRow}
          />
        </Modal>

        <Modal
          style={{ top: '2%' }}
          title="Create/Update Instruction Set"
         open={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width="40%"
          footer={null}
        >
          <InstructionSetModal {...this.state} parentCallback={this.handleCallback} />
        </Modal>

        <Modal
          style={{ top: '2%' }}
          title="Edit Row"
         open={this.state.xyz}
          onOk={this.updateRowOk}
          onCancel={this.updateRowCancel}
          width="40%"
          footer={null}
        >
          <Input
            type="text"
            value={sub_title}
            onChange={e => this.setState({ sub_title: e.target.value })}
          />
          <Input
            type="text"
            value={write_up}
            onChange={e => this.setState({ write_up: e.target.value })}
          />
          <Input
            type="text"
            value={attachment}
            onChange={e => this.setState({ attachment: e.target.value })}
          />

          <Button
            className="ant-btn ant-btn-primary"
            name="attachment"
            type="submit"
            htmlFor="my-form"
            onClick={this.onUpdate}
          >
            Update
          </Button>
          <Button className="ant-btn ant-btn-danger ml-2" onCancel={this.updateRowCancel}>Cancel</Button>
        </Modal>
        <Modal
          style={{ top: '2%' }}
          title="Delete Row"
         open={this.state.deleteModal}
          onOk={this.deleteRowOk}
          onCancel={this.deleteRowCancel}
          width="40%"
          footer={null}
        >
          <Button
            className="ant-btn ant-btn-primary"
            name="attachment"
            type="submit"
            htmlFor="my-form"
            onClick={e => this.onRowDelete(e)}
          >
            Delete
          </Button>
        </Modal>
      </div>
    );
  }
}

export default InstructionSet;
