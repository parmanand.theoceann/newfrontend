import React, { useState } from "react";
import { Table, Popconfirm, Modal, Col, Row, Select } from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../../../shared";
import { FIELDS } from "../../../../shared/tableFields";
import ToolbarUI from "../../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../../shared/SidebarColumnFilter";
import OtherExpenseModal from "./OtherExpenseModal";
import { EditOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import OtherRevenueListGraph from "./OtherRevenueListGraph";

const revenueStatus = { revenue: 173, expense: 174 };

const OtherRevenueList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,

  });
const [stateGraph,setStateGraph]=useState({
  responseGraphData:[],
  vessel_name:[],
  invoice_no:[]
})
  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() =>onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["other-revenue-list"]
        ? FIELDS["other-revenue-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);

    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);
useEffect(()=>{
  getGraphData()
},[])
  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' }, "where": { "rev_exp": { "l": revenueStatus["revenue"] } } };
    
    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const getGraphData = async (search = {}) => {
    const { pageOptions } = state;

    // Set default page index and page limit
    let qParams = { p: "0", l: "0" };
    let headers = {
      order_by: { id: "desc" },
      where: { rev_exp: { l: revenueStatus["revenue"] } },
    };
    setStateGraph((prev) => ({
      ...prev,
      loading: true,
      responseGraphData: [],
    }));

    // Convert query parameters to string
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/list?${qParamString}`;

    try {
     
      const response = await getAPICall(_url, headers);
      const data = await response;

      const InvoiceNumbers = [];
      const uniqueVesselNames = new Set(); // Using a Set to collect unique vessel names

      // Iterate over each item in the data array
      data?.data?.forEach((item) => {
        InvoiceNumbers.push(item.invoice_no);
        uniqueVesselNames.add(item.vessel_name); // Adding vessel names to the Set
      });

      // Convert Set to array for vessel names
      const vesselNames = [...uniqueVesselNames];

      setStateGraph((prev) => ({
        ...prev,
        vessel_name: vesselNames,
        invoice_no: InvoiceNumbers,
        responseGraphData:data.data
      }));
    } catch (error) {
      console.error("Error fetching data:", error);
      // Handle error (e.g., set error state, display error message)
    }
  };

  const redirectToAdd = async (e, record = null) => {
    _onLeftSideListClick(record.id);
  };

  const _onLeftSideListClick = async (delayId) => {
    // setState({ ...state, isVisible: false });
    setState(prevState => ({...prevState, isVisible: false}))
    let _url = "edit?e=";
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];
    setState((prevState) => ({
      ...prevState,
      formDataValues: data,
      isVisible: true,
    }));
  };

  const onChangeStatus = (status = false) => {
    setState({ ...state, isVisible: status }, () => getTableData());
  };

  const onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    isVisible,
    sidebarVisible,
    formDataValues,
  } = state;

  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

 
  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Other Revenue List</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"other-expense-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph={showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "otherrevenue"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "otherrevenue"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "otherrevenue"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {isVisible ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Edit Invoice"
            open={isVisible}
            onCancel={() => onChangeStatus()}
            width="95%"
            footer={null}
          >
            <OtherExpenseModal
              formData={formDataValues}
              isEdit={true}
              modalCloseEvent={() => onChangeStatus()}
            />
          </Modal>
        ) : undefined}
        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
        <Modal
          title=" Other Revenue"
          open={state.isGraphModal}
          //  onOk={handleOk}
          width="90%"
          onCancel={handleCancel}
          footer={null}
        >
          <OtherRevenueListGraph stateGraph={stateGraph}/>
        </Modal>
      </div>
    </>
  );
};

export default OtherRevenueList;
