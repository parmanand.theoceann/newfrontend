import React, { useEffect, useState } from "react";
import { Col, DatePicker, Row, Select, Spin, Alert, Button } from "antd";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import StackGoupColumnChart from "../../../dashboard/charts/StackNormalizationChart";
import LineChart from "../../../dashboard/charts/LineChart";
import PieChart from "../../../dashboard/charts/PieChart";
import moment from "moment";
import URL_WITH_VERSION, {
  objectToQueryStringFunc,
  postAPICall,
} from "../../../../shared";
const OtherRevenueListGraph = () => {
  const [state, setState] = useState({
    loading: false,
    responseData: [],
    isVisible: false,
    graphVisible: false,
    donloadArray: [],
    vessel_name: [],
    invoice_no: [],
    topFiveTotal_amount: [],
    statusTotals: [],
    formData: {
      vesselName: "",
      DateTo: "",
      invoiceNumber: "",
    },
    filterData: [
      {
        vesselTotalAmounts: [],
        invoiceTotalAmounts: [],
        voyageManagerTotalAmounts: [],
        invoiceStatusTotalAmounts: [],
      },
    ],
    filterData2: {
      filteredVessels: [],
      filteredInvoices: [],
      filteredVoyageManagers: [],
      filteredInvoiceStatuses: [],
    },
  });

  useEffect(() => {
    // TotalTopFiveTotalAmount();
    totalStatus();
  }, [state.responseData.data]);
  useEffect(() => {
    getVesselConsumption(state.formData);
  }, [state.formData]);
  const totalStatus = () => {
    const statusTotals = {
      Prepared: 0,
      Verified: 0,
      Approved: 0,
      Posted: 0,
    };

    state?.responseData?.data?.forEach((item) => {
      const { invoice_status_name, total_amount } = item;
      if (statusTotals.hasOwnProperty(invoice_status_name)) {
        // Remove commas and convert total_amount to number
        const amount = parseFloat(total_amount.replace(/,/g, ""));
        statusTotals[invoice_status_name] += amount;
      }
    });

    // Convert the statusTotals object into an array of objects
    const ClusterDataSeriesStatus = Object.keys(statusTotals).map((status) => ({
      name: status,
      value: statusTotals[status],
    }));
    setState((pre) => ({
      ...pre,
      statusTotals: ClusterDataSeriesStatus,
    }));
  };

 


  const getVesselConsumption = async (payload) => {
    const query = { inv: 173 };
    let qParamString = objectToQueryStringFunc(query);
  
    setState((pre) => ({
      ...pre,
      responseData: [],
      graphVisible: false,
      filterData: [
        {
          vesselTotalAmounts: [],
          invoiceTotalAmounts: [],
          voyageManagerTotalAmounts: [],
          invoiceStatusTotalAmounts: [],
        },
      ],
    }));
  
    try {
      let suURL = `${URL_WITH_VERSION}/chattering-dashboard/invoice/filter?${qParamString}`;
      let suMethod = "POST";
  
      // Check if any key in payload has a value
      const anyKeyHasValue = Object.values(payload).some((value) => value !== "");
  
      const respdata = await postAPICall(suURL, payload ?? null, suMethod);
 
      setState((pre) => ({
        ...pre,
        responseData: respdata,
        vesselTotalAmounts: [],
        invoiceTotalAmounts: [],
        voyageManagerTotalAmounts: [],
        invoiceStatusTotalAmounts: [],
      }));
  
      if (anyKeyHasValue) {
        const vesselData = {};
        const invoiceData = {};
        const voyageManagerData = {};
        const invoiceStatusData = {};
  
        // Iterate over each item in the data array
        respdata?.data?.forEach((item) => {
          // For vessel_name
          const totalAmount = parseFloat(item.total_amount.replace(/,/g, ''));
          vesselData[item.vessel_name] = (vesselData[item.vessel_name] || 0) + totalAmount;
  
          // For invoice_no
          invoiceData[item.invoice_no] = (invoiceData[item.invoice_no] || 0) + totalAmount;
  
          // For voyage_manager_name
          voyageManagerData[item.voyage_manager_name] = (voyageManagerData[item.voyage_manager_name] || 0) + totalAmount;
  
          // For invoice_status_name
          invoiceStatusData[item.invoice_status_name] = (invoiceStatusData[item.invoice_status_name] || 0) + totalAmount;
        });
  
        setState((prev) => ({
          ...prev,
          vessel_name: Object.keys(vesselData),
          invoice_no: Object.keys(invoiceData),
          voyage_manager_name: Object.keys(voyageManagerData),
          voyageManagerTotalAmounts: voyageManagerData,
          invoice_status_name: Object.keys(invoiceStatusData),
          filterData: {
            vesselTotalAmounts: vesselData,
            invoiceTotalAmounts: invoiceData,
            voyageManagerTotalAmounts: voyageManagerData,
            invoiceStatusTotalAmounts: invoiceStatusData,
          },
          graphVisible: true,
        }));
      } else {
        const InvoiceNumbers = [];
        const uniqueVesselNames = new Set();
        respdata?.data?.forEach((item) => {
          InvoiceNumbers.push(item.invoice_no);
          uniqueVesselNames.add(item.vessel_name);
        });
        const vesselNames = [...uniqueVesselNames];
  
        const filteredVessels = {};
        const filteredInvoices = {};
        const filteredInvoiceStatuses = {};
        const filteredVoyageManagers = {};
    
        respdata.data.forEach(entry => {
          const amount = parseFloat(entry.total_amount.replace(/,/g, ''));
          // Filter and aggregate by vessel_name
          if (!filteredVessels[entry.vessel_name]) {
            filteredVessels[entry.vessel_name] = 0;
          }
          filteredVessels[entry.vessel_name] += amount
    
          // Filter and aggregate by invoice_no
          if (!filteredInvoices[entry.invoice_no]) {
            filteredInvoices[entry.invoice_no] = 0;
          }
          filteredInvoices[entry.invoice_no] += amount
    
          // Filter and aggregate by invoice_status_name
          if (!filteredInvoiceStatuses[entry.invoice_status_name]) {
            filteredInvoiceStatuses[entry.invoice_status_name] = 0;
          }
          filteredInvoiceStatuses[entry.invoice_status_name] += amount
    
          // Filter and aggregate by voyage_manager_name
          if (!filteredVoyageManagers[entry.voyage_manager_name]) {
            filteredVoyageManagers[entry.voyage_manager_name] = 0;
          }
          filteredVoyageManagers[entry.voyage_manager_name] += amount
        });
    
        // Sort and select top 5 from each category
        const topVessels = Object.entries(filteredVessels)
          .sort(([, a], [, b]) => b - a)
          .slice(0, 5)
          .map(([name, value]) => ({ name, value }));
    
        const topInvoices = Object.entries(filteredInvoices)
          .sort(([, a], [, b]) => b - a)
          .slice(0, 5)
          .map(([name, value]) => ({ name, value }));
    
        const topInvoiceStatuses = Object.entries(filteredInvoiceStatuses)
          .sort(([, a], [, b]) => b - a)
          .slice(0, 5)
          .map(([name, value]) => ({ name, value }));
    
        const topVoyageManagers = Object.entries(filteredVoyageManagers)
          .sort(([, a], [, b]) => b - a)
          .slice(-5)
          .map(([name, value]) => ({ name, value }));
    
        // Set filtered and aggregated data in state

        const sortedData = respdata?.data?.sort((a, b) => a.total_amount - b.total_amount);
        const highestFive = sortedData?.slice(-5);
  
        setState((prev) => ({
          ...prev,
          vessel_name: vesselNames,
          invoice_no: InvoiceNumbers,
          responseData: respdata,
          // topFiveTotal_amount: highestFive,
          graphVisible: true,
          filterData2: {
            filteredVessels: topVessels,
            filteredInvoices: topInvoices,
            filteredVoyageManagers: topVoyageManagers,
            filteredInvoiceStatuses: topInvoiceStatuses,
          },
        }));
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  
  const handleChange = (field, value) => {
    setState((prevState) => ({
      ...prevState,
      formData: {
        ...prevState.formData,
        [field]: value,
      },
    }));
  };
  
  const PieChartData = state.filterData2.filteredVoyageManagers.map((item) => ({
    name: item.name,
    value: parseFloat(item.value), // Convert total_amount to a number
  }));
  const handleReset = () => {
    setState((pre) => ({
      ...pre,
      formData: {
        vesselName: "",
        DateTo: "",
        invoiceNumber: "",
      },
    }));
  };

  const vessel_nameDataTotalAmounts = state.filterData.vesselTotalAmounts;
  const vessel_nameTotalAmounts = vessel_nameDataTotalAmounts
    ? Object.entries(vessel_nameDataTotalAmounts).map(([name, value]) => ({
        name,
        value,
      }))
    : [];
const invoiceDataTotalAmounts = state.filterData.invoiceTotalAmounts;
const invoiceTotalAmounts = invoiceDataTotalAmounts
  ? Object.entries(invoiceDataTotalAmounts).map(([name, value]) => ({
      name,
      value,
    }))
  : [];
  const invoiceStatusTotalAmounts = state.filterData.invoiceStatusTotalAmounts;
  const invoiceStatusGraph = invoiceStatusTotalAmounts
    ? Object.entries(invoiceStatusTotalAmounts).map(([key, value]) => ({
        key,
        value,
      }))
    : [];

    const voyageManagerTotalAmounts = state.filterData.voyageManagerTotalAmounts;
  const PichartvoyageManagerNumber = voyageManagerTotalAmounts
    ? Object.entries(voyageManagerTotalAmounts).map(([name, value]) => ({
        name,
        value:parseFloat(value),
      }))
    : [];
  return (
    <div>
      {state.graphVisible ? (
        <>
        <div style={{display:"flex",alignItems:"center"}}>
        
          <Row gutter={[16, 0]} style={{ textAlign: "center", gap: 5 }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total vessels</p>
                <p>
                  {state?.responseData?.row?.map((data) => data.vessel_count) ??
                    0}
                </p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total amount</p>
                <p>
                  {state?.responseData?.row?.map(
                    (data) => data.total_inv_amount
                  ) ?? 0}
                  $
                </p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total invoice</p>
                <p>
                  {state?.responseData?.row?.map(
                    (data) => data.total_invoices
                  ) ?? 0}
                </p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{
                textAlign: "start",
                padding: "8px",
                width: "250px",
                boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
                borderRadius: "8px",
                textAlign: "center",
              }}
            >
              <p style={{ margin: "0" }}>vessel name</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                onChange={(value) => handleChange("vesselName", value)}
                value={state.formData.vesselName}
                options={state.vessel_name.map((vesselName) => ({
                  value: vesselName,
                  label: vesselName,
                }))}
              />
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{
                textAlign: "start",
                padding: "8px",
                width: "250px",
                boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
                borderRadius: "8px",
                textAlign: "center",
              }}
            >
              <p style={{ margin: "0" }}>Invoice No</p>
              <Select
              className="otherRevenuInvoice"
                placeholder="All"
                optionFilterProp="children"
                size="large"
                onChange={(value) => handleChange("invoiceNumber", value)}
                value={state.formData.invoiceNumber}
                options={state.invoice_no.map((invoice_no) => ({
                  value: invoice_no,
                  label: invoice_no,
                }))}
              />
            </Col>

            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{
                textAlign: "start",
                padding: "8px",
                width: "250px",
                boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
                borderRadius: "8px",
                textAlign: "center",
              }}
            >
              <p style={{ margin: "0" }}>Date To</p>

              <DatePicker
                className="form-control"
                style={{ width: "110px", padding: "3px" }}
                picker="date"
                format="YYYY-MM-DD"
                onChange={(date, dateString) => {
                  handleChange("DateTo", dateString);
                }}
                value={
                  state?.formData?.DateTo
                    ? moment(state?.formData?.DateTo, "YYYY-MM-DD")
                    : ""
                }
              />
            </Col>
            
          </Row>
          <Button  onClick={handleReset} style={{height:"30px", textAline:"center"}}>
              Reset All
            </Button>
          </div>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount Per Vessel"}
                ClusterDataxAxis={vessel_nameTotalAmounts.length>0?vessel_nameTotalAmounts.map(
                  (item) => item?.name
                ):state.filterData2.filteredVessels.map(
                  (item) => item.name
                )}
                ClusterDataSeries={{
                  name: "Total Amount",
                  type: "bar",
                  barGap: 0,
                  data:vessel_nameTotalAmounts.length>0?vessel_nameTotalAmounts.map(
                    (item) =>  item.value
                    
                  ): state.filterData2.filteredVessels.map((item) =>
                    item.value
                  ),
                }}
                maxValueyAxis={"350"}
              />
            </Col>
            <Col span={12}>
              <LineChart
                //   parseFloat(item.total_amount.replace(',', ''))
                LineCharSeriesData={invoiceTotalAmounts.length>0?invoiceTotalAmounts.map((item) =>
                  item.value):state.filterData2.filteredInvoices.map((item) =>
                  item.value
                )}
                LineCharxAxisData={invoiceTotalAmounts.length>0?invoiceTotalAmounts.map((item) =>
                  item.name):state.filterData2.filteredInvoices.map(
                  (item) => item.name
                )}
                Heading={"Total Amount Per Invoice No."}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount As Per Status"}
                ClusterDataxAxis={
                  invoiceStatusGraph.length > 0
                    ? invoiceStatusGraph.map((item) => item.key)
                    :  state.filterData2.filteredInvoiceStatuses.map((item) => item.name)
                }
                ClusterDataSeries={{
                  name: "Total Amount",
                  type: "bar",
                  barGap: 0,
                  data: invoiceStatusGraph.length > 0
                    ? invoiceStatusGraph.map((item) => item.value)
                    : state.filterData2.filteredInvoiceStatuses.map((item) => item.value),
                }}
                maxValueyAxis={"500"}
              />
            </Col>
            <Col span={12}>
              <PieChart
                PieChartData={PichartvoyageManagerNumber?.length>0?PichartvoyageManagerNumber:PieChartData}
                Heading={"Total Amount Per Voyage no."}
                name= {'Total Amount Per Voyage no.'}
              />
            </Col>
          </Row>
        </>
      ) : (
        <div className="col col-lg-12">
          <Spin tip="Loading...">
            <Alert message=" " description="Please wait..." type="info" />
          </Spin>
        </div>
      )}
    </div>
  );
};

export default OtherRevenueListGraph;
