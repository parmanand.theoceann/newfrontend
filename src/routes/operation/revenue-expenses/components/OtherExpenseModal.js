import React, { useRef, useEffect } from "react";
import { Layout, Row, Col, Modal } from "antd";
//import Tde from "../../../tde/Tde";
import Tde from "../../../tde/";
import CreateInvoice from "../../../create-invoice/CreateInvoice";
import OtherInvoiceReport from "../../../operation-reports/OtherInvoiceReport";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  postAPICall,
  apiDeleteCall,
  useStateCallback,
} from "../../../../shared";
import Attachment from "../../../../shared/components/Attachment";
import InvoicePopup from "../../../create-invoice/InvoicePopup";
import Remarks from "../../../../shared/components/Remarks";
import { DeleteOutlined, SaveOutlined, EditOutlined } from "@ant-design/icons";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../../shared/attachments";

const { Content } = Layout;

const OtherExpenseModal = (props) => {

  let formDataProps = null;
  if (!props.isEdit)
    formDataProps = Object.assign(
    
      {},
      {
        id: -1,
        vessel:
          props.formData && props.formData.vessel_id
            ? props.formData.vessel_id
            : undefined,
        voyage_manager_id:
          props.formData && props.formData.id ? props.formData.id : undefined,
        fixture_no:
          props.formData && props.formData.fixture_num
            ? props.formData.fixture_num
            : undefined,
          
         

        //due_date: moment(),
        //invoice_date: moment(),
        //service_date: moment(),
        //recieved_date: moment()
      }
    );
  else formDataProps = Object.assign({}, props.formData);

  if (formDataProps.hasOwnProperty("total_amount"))
    formDataProps["....."] = { total_amount: formDataProps["total_amount"] };

   

  const [state, setState] = useStateCallback({
    frmName: "other_expense_invoice_form",
    formData:formDataProps,
    frmVisible: false,
    isRemarkModel: false,
    modals: {
      InvoiceModal: false,
      TdeShow: false,
      OtherInvoiceReport: false,
      InvoicePopup: false,
    },
    removeAttachmentReport: props.hasOwnProperty("removeAttachmentReport")
      ? props.removeAttachmentReport
      : false,
    count: 1,
    modelVisible: false,
    msg: "",
    TdeList: null,
    isEdit: props.isEdit,
    otherID: (props.formData && props.formData.id) || null,
    invoiceReport: null,
    isSaved: false,
    popupdata: null,
  });

  let formdataref = useRef();

  useEffect(() => {
    const { formData } = state;
    formdataref.current = Object.assign({}, formData);
    setTimeout(
      () => setState((prevState) => ({ ...prevState, frmVisible: true })),
      1000
    );
  }, []);

  const openOtherInvoiceReport = async (showOtherInvoiceReport) => {
    // for report Api
    if (showOtherInvoiceReport == true) {
      try {
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/invoice/report?e=${state.otherID}`
        );
        const respDataReport = await responseReport["data"];
        if (respDataReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowOtherInvoiceReport: showOtherInvoiceReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show Report");
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong.", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowOtherInvoiceReport: showOtherInvoiceReport,
      }));
    }
  };

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {};
    _modal[modal] = visible;
    setState((prevState) => ({
      ...prevState,
      modals: Object.assign(modals, _modal),
    }));
  };

  const uploadedFiles = (data, index) => {
    const images = data.map((item, index) => {
      return {
        indexNumber: index,
        fileName: item.fileName,
        url: item.url,
      };
    });
    setState((prevState) => ({ ...prevState, imageFile: images }));
  };

  const _onLeftSideListClick = async (delayId, trans_no) => {
    setState((prevState) => ({
      ...prevState,
      frmVisible: false,
      otherID: delayId,
    }));
    let _url = "edit?e=";
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];

    if (data) {
      if (trans_no === "delete") {
        data["provision_trans_no"] = "";
      }
      if (trans_no && trans_no !== "delete") {
        data["provision_trans_no"] = trans_no;
      }
    }
    let formData = Object.assign({}, data);
    formdataref.current = Object.assign({}, data);
    setState(
      (prevState) => ({ ...prevState, frmVisible: false, formData }),
      () => {
        setState((prevState) => ({
          ...prevState,
          frmVisible: true,
          isEdit: true,
        }));
      }
    );
  };

  const saveFormData = (postData) => {
    const { frmName } = state;
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty("id") && postData["id"] > 0) {
      _url = "update";
      _method = "put";
    }
    setState((prevState) => ({ ...prevState, frmVisible: false }));

    Object.keys(postData).forEach(
      (key) => postData[key] === null && delete postData[key]
    );
    formdataref.current = Object.assign({}, postData);
    postAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url}?frm=${frmName}`,
      postData,
      _method,
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);

          if (_url === "save") {
            window.emitNotification({
              n_type: `Other ${
                postData.invoice_type === "95" ? "Revenue" : "Expense"
              } invoice Created`,
              msg: window.notificationMessageCorrector(`Other ${
                postData.invoice_type === "95" ? "Revenue" : "Expense"
              } invoice Created, for voyage Id(${
                postData.voyage_manager_id
              }), by ${window.userName}`),
            });
          } else {
            window.emitNotification({
              n_type: `Other ${
                postData.invoice_type === "95" ? "Revenue" : "Expense"
              } invoice Updated`,
              msg: window.notificationMessageCorrector(`Other ${
                postData.invoice_type === "95" ? "Revenue" : "Expense"
              } invoice Updated, for voyage Id(${
                postData.voyage_manager_id
              }), by ${window.userName}`),
            });
          }
          // let setData = Object.assign(
          //   {
          //     id: -1,
          //   }
          // )

          // setState({ ...state, frmVisible: true, formData: setData, showSideListBar: true }, () => {
          //   setState({ ...state, frmVisible: true });
          // });

          // if (props.modalCloseEvent && typeof props.modalCloseEvent === 'function') {
          //   props.modalCloseEvent();
          // }

          if (data && data.hasOwnProperty("row") && data.row["id"]) {
            if (props.hasOwnProperty("updateFormData")) {
              props.updateFormData(data.row["id"]);
            }
            _onLeftSideListClick(data.row["id"]);
          }
        } else {
          setState((prevState) => ({
            ...prevState,
            formData: data,
            frmVisible: true,
          }));
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />
          );
          //setState({ ...state, frmVisible: true })
        }
      }
    );
  };

  const saveUpdateClose = async (data) => {
    const { otherID } = state;
    if (data) {
      const editData = await getAPICall(
        `${URL_WITH_VERSION}/tde/edit?e=${data.id}`
      );
      const tdeEditData = await editData["data"];
      if (tdeEditData.trans_no) {
        _onLeftSideListClick(otherID, tdeEditData.trans_no);
        saveFormData({
          ...formdataref.current,
          provision_trans_no: tdeEditData.trans_no,
        });
      }
    }
    showHideModal(false, "TdeShow");
  };

  const deleteTdeData = () => {
    const { otherID } = state;
    _onLeftSideListClick(otherID, "delete");
    showHideModal(false, "TdeShow");
  };

  const tdeModalOpen = async (invoice_no, modal) => {
    const { formData, modelVisible, otherID } = state;

    let tde_id = 0;
    if (invoice_no) {
      let _modal = {};
      _modal[modal] = true;
      const request = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/edit?ae=${formData["estimate_id"]}`
      );
      const voyageData = await request["data"];
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      const tdeList = response["data"];
      let TdeList =
        tdeList && tdeList.length > 0
          ? tdeList.filter((el) => invoice_no === el.invoice_no)
          : [];

      if (TdeList && TdeList.length > 0) {
        tde_id = TdeList[0]["id"];
      }
      if (tde_id !== 0) {
        const editData = await getAPICall(
          `${URL_WITH_VERSION}/tde/edit?e=${tde_id}`
        );
        const tdeEditData = await editData["data"];

        tdeEditData["voyage_manager_id"] =
          tdeEditData["voy_id"] || tdeEditData["voyage"];
        if (
          formData.otherrevenueexpenses &&
          formData.otherrevenueexpenses.length > 0
        ) {
          tdeEditData["accounting"] = [];
          formData.otherrevenueexpenses.map((e, index) => {
            tdeEditData["accounting"].push({
              vessel_name: formData["vessel"],
              voyage: formData["estimate_id"],
              ap_ar_acct: tdeEditData["ar_pr_account_no"],
              account: e.code,
              vessel_code:
                voyageData && voyageData.hasOwnProperty("vessel_code")
                  ? voyageData["vessel_code"]
                  : undefined,
              company: formData["bill_via"],
              amount: e.amount,
              description: e.description,
              port: e.port_name,
              lob:
                voyageData && voyageData.hasOwnProperty("company_lob")
                  ? voyageData["company_lob"]
                  : undefined,
              id: -9e6 + index,
            });
            return true;
          });
        }
        if (!formData["trans_no"]) {
          setState((prevState) => ({ ...prevState, frmVisible: false }));
          _onLeftSideListClick(otherID, tdeEditData["trans_no"]);
        }
        if (formData["trans_no"] !== tdeEditData["trans_no"]) {
          setState((prevState) => ({ ...prevState, frmVisible: false }));
          _onLeftSideListClick(otherID, tdeEditData["trans_no"]);
        }
        tdeEditData["remittance_bank"] = formData.remittence_bank;
        tdeEditData["voyID"] = formData["estimate_id"];

        setState((prevState) => ({
          ...prevState,
          TdeList: tdeEditData,
          modals: _modal,
        }));
      } else {
        const responseData = await getAPICall(
          `${URL_WITH_VERSION}/address/edit?ae=${formData["vendor"]}`
        );
        const responseAddressData = responseData["data"];
        let account_no =
          responseAddressData &&
          responseAddressData["bank&accountdetails"] &&
          responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
              responseAddressData["bank&accountdetails"][0]["account_no"]
            : "";

        let frmData = {};
        frmData = {
          bill_via: formData["bill_via"],
          invoice: formData["acc_type"],
          invoice_no: formData["invoice_no"],
          invoice_date: formData["due_date"],
          invoice_type: formData["invoice_type"],
          // 'received_date': formData['claim_date'],
          vessel: formData["vessel"],
          vendor: formData["vendor"],
          voyage_manager_id: formData["voyage_manager_id"],
          inv_status: formData["invoice_status"],
          invoice_amount:
            formData["....."] && formData["....."]["total_amount"],
          account_base: formData["....."] && formData["....."]["total_amount"],
          ar_pr_account_no: account_no,
          voy_no: formData["voyage_manager_id"],
          po_number: formData["po_number"],
          received_date: formData["recieved_date"]
            ? formData["recieved_date"]
            : formData["claim_date"],
          payment_term: formData["payment_terms"],
          accounting: [],
        };
        if (
          formData.otherrevenueexpenses &&
          formData.otherrevenueexpenses.length > 0
        ) {
          formData.otherrevenueexpenses.map((e, index) => {
            frmData["accounting"].push({
              vessel_name: formData["vessel"],
              voyage: voyageData["voyage_number"],
              ap_ar_acct: account_no,
              vessel_code: voyageData["vessel_code"],
              company: formData["bill_via"],
              amount: e.amount,
              description: e.description,
              port: e.port_name,
              lob: voyageData["company_lob"],
              account: e.code,
              id: -9e6 + index,
            });
            return true;
          });
        }
        frmData["voyID"] = formData["estimate_id"];
        setState((prevState) => ({
          ...prevState,
          modals: _modal,
          TdeList: frmData,
        }));
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        msg: `Without "Invoice No" you cant't access the TDE Form`,
        modelVisible: !modelVisible,
      }));
    }
  };

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Other Revenue/Expenses") {
      groupKey = "otherrevenueexpenses";
      frm_code = "other_expense_invoice_form";
    }
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
        key: data.key,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const invoiceModal = async (data = {}) => {
    let { formData, invoiceReport } = state;
    try {
      if (Object.keys(data).length == 0) {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/expense-invoice/report?e=${formData.id}`
        );
        const respData = await response["data"];
        if (respData) {
          setState((prevState) => ({
            ...prevState,
            invoiceReport: respData,
            popupdata: respData,
          }));
          showHideModal(true, "InvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry, Unable to Show Invoice", 3);
        }
      } else {
        setState((prevState) => ({
          ...prevState,
          invoiceReport: { ...invoiceReport, ...data },
        }));
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  const handleok = () => {
    const { invoiceReport } = state;

    if (invoiceReport["isSaved"]) {
      showHideModal(false, "InvoicePopup");
      setTimeout(() => {
        showHideModal(true, "InvoiceModal");
      }, 2000);
      setState((prevState) => ({ ...prevState, invoiceReport: invoiceReport }));
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  const handleRemark = () => {
    setState((prevState) => ({
      ...prevState,
      isRemarkModel: true,
    }));
  };

  const _onDeleteFormData = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    const { formData } = state;
    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        window.emitNotification({
          n_type: `Other ${
            postData.invoice_type === "95" ? "Revenue" : "Expense"
          } invoice Deleted`,
          msg: window.notificationMessageCorrector(`Other ${
            postData.invoice_type === "95" ? "Revenue" : "Expense"
          } invoice Deleted, for voyage Id(${postData.voyage_manager_id}), by ${
            window.userName
          }`),
        });
        setState(
          (prevState) => ({ ...prevState, frmVisible: false }),
          (prevState) => setState({ ...prevState, frmVisible: true })
        );

        if (
          props.modalCloseEvent &&
          typeof props.modalCloseEvent === "function"
        ) {
          props.modalCloseEvent();
        }
        // if (
        //   formData &&
        //   formData.hasOwnProperty("voyage_manager_id") &&
        //   formData["voyage_manager_id"]
        // ) {
        //   _onLeftSideListClick(formData["voyage_manager_id"]);
        // }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const {
    frmVisible,
    formData,
    frmName,
    popupdata,
    removeAttachmentReport,
    isShowOtherInvoiceReport,
    reportFormData,
    modelVisible,
    msg,
    TdeList,
    isEdit,
    invoiceReport,
    isRemarkModel,
  } = state;
  let isSetsLeftBtnArr = [
    {
      id: "3",
      key: "save",
      type: <SaveOutlined />,
      withText: "Save",
      showToolTip: true,
      event: (key, data) => saveFormData(data),
    },
    isEdit === true && {
      id: "4",
      key: "delete",
      type: <DeleteOutlined />,
      withText: "Delete",
      showToolTip: true,
      event: (key, data) => _onDeleteFormData(data),
    },
    {
      id: "5",
      key: "edit",
      type: <EditOutlined />,
      withText: "Remark",
      showToolTip: true,
      event: (key, data) =>{
                
        if (data?.row?.id || data?.id > 0 ) {
          handleRemark(data);
        } else {
          openNotificationWithIcon(
           "info",
            "Please save other  Invoice first",
            2
          );
        }
      },
    },
  ];

  const isRightBtnArray = [
    isEdit === true && {
      key: "tde",
      isDropdown: 0,
      withText: "TDE",
      type: "",
      menus: null,
      //event: (key) => tdeModalOpen(formData["invoice_no"], "TdeShow"),
      event: (key) => {
        if (formData?.invoice_no) {
          //setState((prevState) => ({ ...prevState, showTDEForm: true }));
          showHideModal(true, "TdeShow");
        } else {
          setState((prevState) => ({
            ...prevState,
            msg: `Without "Invoice No" you cant't access the TDE Form`,
            modelVisible: !modelVisible,
          }));
        }
      },
    },
    isEdit === true && {
      key: "invoice",
      isDropdown: 0,
      withText: "Create Invoice",
      type: "",
      menus: null,
      event: (key) => invoiceModal(),
    },
    isEdit === true && {
      key: "attachment",
      isDropdown: 0,
      withText: "Attachment",
      type: "",
      menus: null,
      event: (key, data) => {
        data && data.hasOwnProperty("id") && data["id"] > 0
          ? ShowAttachment(true)
          : openNotificationWithIcon(
              "info",
              "Please save Expense Invoice First.",
              3
            );
      },
    },
    {
      key: "report",
      isDropdown: 0,
      withText: "Report",
      type: "",
      menus: null,
      event: (key) => openOtherInvoiceReport(true),
    },
  ];
  if (removeAttachmentReport)
    ["2", "3"].forEach((key) => delete isRightBtnArray[key]);
  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article">
                    <div className=" box-default">
                      <div className="box-body">
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            // formData={formData}
                            formData={formdataref.current}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            inlineLayout={true}
                            showToolbar={[
                              {
                                leftWidth: 8,
                                rightWidth: 16,
                                isLeftBtn: [{ isSets: isSetsLeftBtnArr }],
                                isRightBtn: [
                                  {
                                    isSets: isRightBtnArray,
                                  },
                                ],
                                isResetOption: false,
                              },
                            ]}
                            // extraTableButton={{
                            //   ".": (formData && formData.hasOwnProperty(".") && formData["."].length > 0 ? [{ "icon": "delete", "onClickAction": (action, data) => _onDeleteClick(action) }] : []),
                            // }}

                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                          />
                        ) : undefined}
                      </div>






                      



                    </div>
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
      {invoiceReport && (
        <Modal
          style={{ top: "2%" }}
          title="Download Invoice"
          open={state.modals["InvoiceModal"]}
          onCancel={() => showHideModal(false, "InvoiceModal")}
          width="95%"
          footer={null}
        >
          <CreateInvoice
            type="otherExpenseInvoice"
            otherExpenseInvoice={invoiceReport}
          />
        </Modal>
      )}

      {state.modals["InvoicePopup"] ? (
        <Modal
          style={{ top: "2%" }}
          title="Invoice"
          open={state.modals["InvoicePopup"]}
          onCancel={() => showHideModal(false, "InvoicePopup")}
          width="95%"
          okText="Create PDF"
          onOk={handleok}
        >
          <InvoicePopup
            data={popupdata}
            updatepopup={(data) => invoiceModal(data)}
          />
        </Modal>
      ) : undefined}

      {state.modals["TdeShow"] && (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="TDE"
          open={state.modals["TdeShow"]}
          onCancel={() => showHideModal(false, "TdeShow")}
          width="95%"
          footer={null}
        >
          <Tde
            invoiceType="other-expense"
            //isEdit= {TdeList != null && TdeList.id && TdeList.id > 0 ? true : false}
            //formData={TdeList}
            formData={formData}
            //deleteTde={() => deleteTdeData()}
            //saveUpdateClose={(e) => saveUpdateClose(e)}

            invoiceNo={formData.invoice_no}
          />
        </Modal>
      )}
      {state.isShowAttachment ? (
        <Modal
          style={{ top: "2%" }}
          title="Upload Attachment"
          open={state.isShowAttachment}
          onCancel={() => ShowAttachment(false)}
          width="50%"
          footer={null}
        >
          {state.loadComponent}
        </Modal>
      ) : undefined}
      {isShowOtherInvoiceReport ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Report"
          open={isShowOtherInvoiceReport}
          onOk={handleok}
          onCancel={() => openOtherInvoiceReport(false)}
          width="95%"
          footer={null}
        >
          <OtherInvoiceReport data={reportFormData} />
        </Modal>
      ) : undefined}

      {state.modals && state.modals["AttachmentModal"] ? (
        <Modal
          open={state.modals["AttachmentModal"]}
          title="Upload Attachment ( Upload Vendor Invoice Details )"
          // onOk={isContactAttachmentOk}
          onCancel={() => showHideModal(false, "AttachmentModal")}
          footer={null}
          width={1000}
          maskClosable={false}
        >
          <Attachment
            uploadType="Address Book"
            directory={
              formData.hasOwnProperty("estimate_id")
                ? formData["estimate_id"]
                : null
            }
            onCloseUploadFileArray={(fileArr) => uploadedFiles(fileArr)}
          />
        </Modal>
      ) : undefined}
      <Modal
        title="Alert"
        open={modelVisible}
        onOk={() =>
          setState((prevState) => ({
            ...prevState,
            modelVisible: !modelVisible,
          }))
        }
        onCancel={() =>
          setState((prevState) => ({
            ...prevState,
            modelVisible: !modelVisible,
          }))
        }
      >
        <p>{msg}</p>
      </Modal>

      {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isRemarkModel: false }))
          }
          footer={false}
        >
          
          <Remarks
            // remarksID={formData.estimate_id}
            remarksID={formData.invoice_no}
            
            remarkType="bunker-invoice"
            // remarkType="voyage-manager"
            // idType="Bunker_no"
          />
        </Modal>
      )}
    </div>
  );
};

export default OtherExpenseModal;
