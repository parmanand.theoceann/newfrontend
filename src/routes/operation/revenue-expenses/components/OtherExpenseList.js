import React, { useState } from "react";
import { Table, Popconfirm, Modal, Col, Select, Row } from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../../../shared";
import { FIELDS } from "../../../../shared/tableFields";
import ToolbarUI from "../../../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../../../shared/SidebarColumnFilter";
import OtherExpenseModal from "./OtherExpenseModal";
import { EditOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import ClusterColumnChart from "../../../dashboard/charts/ClusterColumnChart";
import LineChart from "../../../dashboard/charts/LineChart";
import PieChart from "../../../dashboard/charts/PieChart";

const revenueStatus = { revenue: 173, expense: 174 };

const OtherExpenseList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
  });

  const [isGraphModal, setIsGraphModalOpen] = useState(false);
  const showGraphs = () => {
    setIsGraphModalOpen(true);
  };

  const handleCancel = () => {
    setIsGraphModalOpen(false);
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            <Popconfirm
              title="Are you sure, you want to delete it?"
              onConfirm={() => onRowDeletedClick(record.id)}
            >
             <DeleteOutlined /> />
            </Popconfirm>
          </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["other-expense-list"]
        ? FIELDS["other-expense-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' }, "where": { "rev_exp": { "l": revenueStatus["expense"] } } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };
  const redirectToAdd = async (e, record = null) => {
    _onLeftSideListClick(record.id);
  };

  const onChangeStatus = (status = false) => {
    setState({ ...state, isVisible: status }, () => getTableData());
  };

  const _onLeftSideListClick = async (delayId) => {
    setState((prevState) => ({ ...prevState, isVisible: false }));
    // setState({ ...state, isVisible: false });
    let _url = "edit?e=";
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];
    setState((prevState) => ({
      ...prevState,
      formDataValues: data,
      isVisible: true,
    }));
    // setState({ ...state, isVisible: false, formDataValues }, () => {
    //   setState({ ...state, isVisible: true });
    // });
  };

  const onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };
  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    isVisible,
    sidebarVisible,
    formDataValues,
  } = state;
  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  const ClusterDataSeries = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: " Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];

  const ClusterDataxAxis = [
    "ATLANTIC VENTURE",
    "OCEANIC MAJESTY",
    "NEW RENOWN",
    "The Atlantic",
    "EMMA MAERSK",
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "INV2024-00122",
    "INV2024-00121",
    "INV2024-00120",
    "INV2024-00119",
    "INV2024-00117",
  ];
  const PieChartData = [
    { value: 40, name: "TCOV-FULL-00979 33k" },
    { value: 30, name: "TCOV-FULL-00973 44k" },
    { value: 20, name: "TCOV-FULL-00970 31k" },
    { value: 26, name: "TCOV-FULL-00950 41k" },
    { value: 16, name: "TCOV-FULL-00940 48k" },
  ];

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Other Expense List</span>
                  </h4>
                </div>
                {/* <div className="action-btn">
                    <Link to="add-tcov">Add Bunker Invoice</Link>
                  </div> */}
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"other-expense-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph={showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "otherrevenue"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "otherrevenue"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "otherrevenue"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableCol}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {isVisible ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Edit Invoice"
            open={isVisible}
            onCancel={() => onChangeStatus()}
            width="95%"
            footer={null}
          >
            <OtherExpenseModal
              formData={formDataValues}
              isEdit={true}
              modalCloseEvent={() => onChangeStatus()}
            />
          </Modal>
        ) : undefined}
        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
      </div>
      <Modal
        title="Other Expense"
        open={isGraphModal}
        //  onOk={handleOk}
        width="90%"
        onCancel={handleCancel}
        footer={null}
      >
        {/* <div>
          <div className="d-flex">
            <div className="container-fluid">
              <div className="row mt-2">
                {totalDashboarddat.map((items, index) => (
                  <div
                    key={index}
                    className="col-12 col-sm-6 col-md-4 col-lg-3 mb-2"
                  >
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">{items.title}</h5>
                        <h3 className="card-subtitle mb-2 text-muted">
                          {items.value}
                        </h3>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div>
              <div className="card mt-2 mb-2" style={{ width: "17vw" }}>
                <div className="card-body pt-0 pb-0">
                  <h4 className="card-title text-center mt-2">Vessel name</h4>
                  <select className="form-control display-5 mb-2">
                    <option>abcd1</option>
                    <option>abcd2</option>
                    <option>abcd3</option>
                  </select>
                </div>
              </div>
              <div className="card mt-2 mb-2" style={{ width: "17vw" }}>
                <div className="card-body pt-0 pb-0">
                  <h4 className="card-title text-center mt-2">Invoice date</h4>
                  <input type="date" className="form-control display-5 mb-2" />
                </div>
              </div>
              <div className="card mt-2 mb-2" style={{ width: "17vw" }}>
                <div className="card-body pt-0 pb-0">
                  <h4 className="card-title text-center mt-2">
                    {" "}
                    Invoice number
                  </h4>
                  <select className="form-control display-5 mb-2">
                    <option>abcd1</option>
                    <option>abcd2</option>
                    <option>abcd3</option>
                  </select>
                </div>
              </div>
              <div className="card mt-2 mb-2" style={{ width: "17vw" }}>
                <div className="card-body pt-0 pb-0">
                  <h4 className="card-title text-center mt-2">Voyage no</h4>
                  <select className="form-control display-5 mb-2">
                    <option>abcd1</option>
                    <option>abcd2</option>
                    <option>abcd3</option>
                  </select>
                </div>
              </div>
              <div className="card mt-2 mb-2" style={{ width: "17vw" }}>
                <div className="card-body pt-0 pb-0">
                  <h4 className="card-title text-center mt-2">Status</h4>
                  <select className="form-control display-5 mb-2">
                    <option>abcd1</option>
                    <option>abcd2</option>
                    <option>abcd3</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="container-fluid mt-5">
            <div>
              <div className="row">
                <div className="col-md-6">
                  <ClusterColumnChart
                    Heading={"Dashboard BarChart"}
                    ClusterDataxAxis={ClusterDataxAxis}
                    ClusterDataSeries={ClusterDataSeries}
                    maxValueyAxis={"500"}
                  />
                </div>
                <div className="col-md-6">
                  <LineChart
                    LineCharSeriesData={LineCharSeriesData}
                    LineCharxAxisData={LineCharxAxisData}
                    Heading={"abcd"}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <ClusterColumnChart
                    Heading={"Dashboard BarChart"}
                    ClusterDataxAxis={ClusterDataxAxis}
                    ClusterDataSeries={ClusterDataSeries}
                    maxValueyAxis={"500"}
                  />
                </div>
                <div className="col-md-6">
                  <PieChart PieChartData={PieChartData} Heading={"abcd"} />
                </div>
              </div>
            </div>
          </div>
        </div> */}
        <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
          <Col
            xs={24}
            sm={8}
            md={8}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Vessels</p>
              <p>300</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Amount</p>
              <p>300 $</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ borderRadius: "15px", padding: "8px" }}
          >
            <div
              style={{
                background: "#1D406A",
                color: "white",
                borderRadius: "15px",
              }}
            >
              <p>Total Invoice</p>
              <p>240</p>
            </div>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={3}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Vessel Name</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "PACIFIC EXPLORER",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "OCEANIC MAJESTY",
                  label: "OCEANIC MAJESTY",
                },
                {
                  value: "CS HANA",
                  label: "CS HANA",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Invoice No</p>
            <Select
              placeholder="All"
              optionFilterProp="children"
              options={[
                {
                  value: "TCE02-24-01592",
                  label: "TCE02-24-01592",
                },
                {
                  value: "TCE01-24-01582",
                  label: "TCE01-24-01582",
                },
                {
                  value: "TCE01-24-01573",
                  label: "TCE01-24-01573",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Account Type</p>
            <Select
              placeholder="Payable"
              optionFilterProp="children"
              options={[
                {
                  value: "Payable",
                  label: "Payable",
                },
                {
                  value: "Receivable",
                  label: "Receivable",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Status</p>
            <Select
              placeholder="PREPARED"
              optionFilterProp="children"
              options={[
                {
                  value: "PREPARED",
                  label: "PREPARED",
                },
                {
                  value: "APPROVED",
                  label: "APPROVED",
                },
                {
                  value: "POSTED",
                  label: "POSTED",
                },
                {
                  value: "VERIFIED",
                  label: "VERIFIED",
                },
              ]}
            ></Select>
          </Col>
          <Col
            xs={24}
            sm={12}
            md={6}
            lg={3}
            xl={3}
            style={{ textAlign: "start", padding: "8px" }}
          >
            <p style={{ margin: "0" }}>Date To</p>
            <Select
              placeholder="2024-02-18"
              optionFilterProp="children"
              options={[
                {
                  value: "2024-01-26",
                  label: "2024-01-26",
                },
                {
                  value: "2023-11-16",
                  label: "2023-11-16",
                },
                {
                  value: "2023-11-17",
                  label: "2023-11-17",
                },
              ]}
            ></Select>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Total Amount Per Vessel "}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </Col>
          <Col span={12}>
            <LineChart
              LineCharSeriesData={LineCharSeriesData}
              LineCharxAxisData={LineCharxAxisData}
              Heading={"Total Amount Per Invoice No."}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Total Amount As Per Status"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </Col>
          <Col span={12}>
            <PieChart
              PieChartData={PieChartData}
              Heading={"Total Amount Per Voyage no."}
            />
          </Col>
        </Row>
      </Modal>
    </>
  );
};

export default OtherExpenseList;
