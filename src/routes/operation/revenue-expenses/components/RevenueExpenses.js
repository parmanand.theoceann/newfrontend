import React, { useRef } from "react";
import { Layout, Row, Col, Modal } from "antd";
import OtherExpenseModal from "./OtherExpenseModal";
import OtherExpenseReport from "../../../operation-reports/OtherExpenseReport";
import NormalFormIndex from "../../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  apiDeleteCall,
  getAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../../../shared";
import { useEffect } from "react";
import { EditOutlined } from "@ant-design/icons";

const { Content } = Layout;
const revenueStatus = { revenue: 173, expense: 174 };

const RevenueExpenses = (props) => {
  let postFormData = Object.assign(
    {},
    {
      vessel: props.formData && props.formData.vessel_id,
      voyage_manager_id: props.formData && props.formData.id,
      delayId: props.formData && props.formData.id,
      voyage_number: props.formData && props.formData.voyage_number,
    }
  );

  const [state, setState] = useStateCallback({
    frmName: "revenue_expense_form",
    formData: Object.assign(
      {
        id: -1,
      },
      postFormData || {}
    ),
    vessel_code: props.formData && props.formData.vessel_code,
    voyage_number: props.formData && props.formData.voyage_number,
    frmVisible: false,
    modals: {
      InvoiceModal: false,
      OtherExpenseReport: false,
    },
    postFormArray: null,
    count: 1,
    voyID: props.voyID,
    otherID: 0,
  });

  useEffect(() => {
    getFormData();
  }, []);

  const getFormData = () => {
    const { formData } = state;

    if (
      props &&
      props.showInvoiceEditForm &&
      props.showInvoiceEditForm.hasOwnProperty("isShow") &&
      props.showInvoiceEditForm.isShow
    ) {
      _onEditClick(props.showInvoiceEditForm.data);
      setTimeout(
        () => setState((prevState) => ({ ...prevState, frmVisible: false })),
        2000
      );
      return;
    }
    if (
      formData &&
      formData.hasOwnProperty("voyage_manager_id") &&
      formData["voyage_manager_id"]
    )
      _onLeftSideListClick(formData["voyage_manager_id"]);
  };

  const _onLeftSideListClick = async (delayId, type = "VMI", cb = null) => {
   
    const { formData } = state;
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let new_array = [];
    let _url = "list?ae=";
    if (type === "CVI") _url = "edit?e=";

    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];
   
    if (data && data.length > 0) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      const tdeList = await response["data"];
     
      if (tdeList && tdeList.length > 0) {
      
        tdeList.map((tde) => {
      
          data.map((ore) => {
         
           
            if (tde.trans_no === ore.provision_trans_no) {
              let prepairedata = {
                vessel: tde.vessel_name,
                voyage_no: ore.voyage_manager_name,
                description: `other-${ore.rev_exp_name} * ${ore.total_amount}`,
                rev_exp: ore.rev_exp_name,
                invoice_date: tde.invoice_date,
                due_date: ore.due_date,
                inv_no: tde.invoice_no,
                total_amt: tde.account_base,
                inv_status: tde.inv_status,
                tde_status: tde.tde_status_name,
              };
              new_array.push(prepairedata,);
            }
          });
        });
      }
    }
    let _state = {};
   
    if (data && type === "VMI") {
      let total_revenue_usd = 0,
        total_expense_usd = 0;
      data.map((i) => {
        if (i.rev_exp === revenueStatus["revenue"])
          total_revenue_usd += parseFloat(i.total_amount.replaceAll(",", ""));
        else
          total_expense_usd += parseFloat(i.total_amount.replaceAll(",", ""));
      });

      _state["formData"] = Object.assign({
        id: -1,
        ".": data,
        tdesummary: new_array,
        total_revenue_usd,
        total_expense_usd,
        ...postFormData,
      });
    } else if (data && type === "CVI") {
      _state["postFormArray"] = Object.assign(
        { voyage_number: state.formData.voyage_number }, data
      );
      // setState(prev => ({...prev, postFormArray: {...data, voyage_number: state.formData.voyage_number}, frmVisible:true}), () => {
      //   if(cb) cb()
      // });
    }

    
   
    // setState(prevState => ({...prevState, ..._state, frmVisible: true}), () => {

    //   // if (cb) cb();
    // })
   
    setState(
      (prev) => ({ ...prev, ..._state, frmVisible: false }),
      () => {
        setState(prev => ({...prev, frmVisible: true}))
        if (cb) cb(true, "InvoiceModal", _state);
      }
    );
  };
  

  

  const showHideModal = (visible, modal, postFormArrayState) => {
    const { modals, formData} = state;
    let _modal = {};
    _modal[modal] = visible;
    setState(
      (prevState) => ({
        ...prevState,
        frmVisible: true,
        modals: Object.assign(modals, _modal),
        postFormArray: visible == true ? postFormArrayState?.postFormArray : null,
      }),
      () => {
        if (
          formData &&
          formData.hasOwnProperty("voyage_manager_id") &&
          formData["voyage_manager_id"] &&
          modal == "InvoiceModal" &&
          !visible
        )
          _onLeftSideListClick(formData["voyage_manager_id"]);
      }
    );
  };

  const edit = () => {
    setState((prevState) => ({ ...prevState, editable: true }));
  };

  const onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource1 = [...state.dataSource1];
      const target = dataSource1.find((item) => item.key === key);
      if (target) {
        target[dataIndex] = value;
        setState((prevState) => ({ ...prevState, dataSource1 }));
      }
    };
  };

  const onDelete = (key) => {
    const dataSource1 = [...state.dataSource1];
    setState((prevState) => ({
      ...prevState,
      dataSource1: dataSource1.filter((item) => item.key !== key),
    }));
  };

  const handleAdd = () => {
    const { count, dataSource1 } = state;
    const newData = {
      key: count,
    };
    setState((prevState) => ({
      ...prevState,
      dataSource1: [...dataSource1, newData],
      count: count + 1,
    }));
  };

  const _onEditClick = (data) => {
    
    _onLeftSideListClick(data.ID, "CVI", showHideModal);
    
    setState((prevState) => {
      return ({ ...prevState, otherID: data.ID })
    });
  };


  // const _onEditClick = async (data) => {
  //   await _onLeftSideListClick(data.ID, "CVI");
  //   showHideModal(true, "InvoiceModal");
  //   setState((prevState) => ({ ...prevState, otherID: data.ID }));
  // };
  

  const _onCreateClick = (data) => {
    setState(
      (prevState) => ({
        ...prevState,
        formData: data,
        postFormArray: null,
        otherID: 0,
      }),
      () => showHideModal(true, "InvoiceModal")
    );
  };

  // _onDeleteFormData = (postData) => {
  //   if (postData && !postData.ID) {
  //     openNotificationWithIcon(
  //       "error",
  //       "Vendor Id is empty. Kindly check it again!"
  //     );
  //   }
  //   Modal.confirm({
  //     title: "Confirm",
  //     content: "Are you sure, you want to delete it?",
  //     onOk: () => _onDelete(postData),
  //   });
  // };

  // _onDelete = (postData) => {
  //   const { formData } = state;
  //   let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/delete`;
  //   apiDeleteCall(_url, { id: postData.ID }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon("success", response.message);
  //       setState({ ...state, frmVisible: false }, () => {
  //         setState({ ...state, frmVisible: true });
  //       });
  //       if (
  //         formData &&
  //         formData.hasOwnProperty("voyage_manager_id") &&
  //         formData["voyage_manager_id"]
  //       ) {
  //         _onLeftSideListClick(formData["voyage_manager_id"]);
  //       }
  //     } else {
  //       openNotificationWithIcon("error", response.message);
  //     }
  //   });
  // };
  const openOtherExpenseReport = async (showOtherExpenseReport) => {
    if (showOtherExpenseReport) {
      try {
        let qParamString = objectToQueryStringFunc({ e: state.voyID });
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/rev-exp/report?e=${state.formData["voyage_manager_id"]}`
        );

        const respDataReport = await responseReport["data"];

        if (
          respDataReport &&
          respDataReport.hasOwnProperty("data") &&
          respDataReport["data"].length > 0
        ) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: Object.assign(respDataReport, {
              vessel_code: state.vessel_code,
            }),
            isShowOtherExpenseReport: showOtherExpenseReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something Went Wrong.", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowOtherExpenseReport: showOtherExpenseReport,
      }));
    }
  };

  const updateFormData = (id) => {
    _onLeftSideListClick(id, "CVI", () => {
      showHideModal(true, "InvoiceModal");
    });
    setState((prevState) => ({ ...prevState, otherID: id }));
  };

  const {
    frmVisible,
    formData,
    frmName,
    postFormArray,
    isShowOtherExpenseReport,
    reportFormData,
    otherID,
  } = state;

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article">
                    <div className=" box-default">
                      <div className="box-body">
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            inlineLayout={true}
                            showToolbar={[
                              {
                                leftWidth: 8,
                                rightWidth: 16,
                                isLeftBtn: [{ isSets: [] }],
                                isRightBtn: [
                                  {
                                    isSets: [
                                      {
                                        key: "create-vendor-invoice",
                                        isDropdown: 0,
                                        withText: "Create Vendor Invoice",
                                        type: "",
                                        menus: null,
                                        event: (action, data) =>
                                          _onCreateClick(data),
                                      },
                                      {
                                        key: "report",
                                        isDropdown: 0,
                                        withText: "Report",
                                        type: "",
                                        menus: null,
                                        event: (key) =>
                                          openOtherExpenseReport(true),
                                      },
                                    ],
                                  },
                                ],
                                isResetOption: false,
                              },
                            ]}
                            extraTableButton={{
                              ".":
                                formData &&
                                formData.hasOwnProperty(".") &&
                                formData["."].length > 0
                                  ? [
                                      {
                                        icon: <EditOutlined/>,
                                        onClickAction: (action, data) =>
                                          _onEditClick(action),
                                          
                                      },

                                      // {
                                      //   key: 2,
                                      //   icon: "delete",
                                      //   onClickAction: (action, data) =>
                                      //     _onDeleteFormData(action),
                                      // },
                                    ]
                                  : [],
                            }}
                            isShowFixedColumn={[".", "Invoice Summary"]}
                          />
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>

      {state.modals && state.modals["InvoiceModal"] ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title={postFormArray !== null ? "Edit Invoice" : "Create Invoice"}
          open={state.modals["InvoiceModal"]}
          onCancel={() => showHideModal(false, "InvoiceModal")}
          width="95%"
          footer={null}
        >
          <OtherExpenseModal
            formData={postFormArray ? postFormArray : props.formData}
            isEdit={postFormArray ? true : false}
            otherID={otherID}
            updateFormData={(e) => updateFormData(e)}
            modalCloseEvent={() => showHideModal(false, "InvoiceModal")}
          />
        </Modal>
      ) : undefined}

      {isShowOtherExpenseReport ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Reports"
          open={isShowOtherExpenseReport}
          // onOk={handleOk}
          onCancel={() => openOtherExpenseReport(false)}
          width="95%"
          footer={null}
        >
          <OtherExpenseReport data={reportFormData} />
        </Modal>
      ) : undefined}
    </div>
  );
};

export default RevenueExpenses;
