import React from 'react';
import { Route } from 'react-router-dom';

import AgencyAppointment from './agency-appointment';
import AlertSetup from './alert-setup';
import DeliveryRedeliverySetup from './delivery-redelivery-setup';
import EmailAlertSetup from './email-alert-setup';
import OperationTaskSetup from './operation-task-setup';
import PaymentSetup from './payment-setup';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/agency-appointment`} component={AgencyAppointment} />
    <Route path={`${match.url}/alert-setup`} component={AlertSetup} />
    <Route path={`${match.url}/delivery-redelivery-setup`} component={DeliveryRedeliverySetup} />
    <Route path={`${match.url}/email-alert-setup`} component={EmailAlertSetup} />
    <Route path={`${match.url}/operation-task-setup`} component={OperationTaskSetup} />
    <Route path={`${match.url}/payment-setup`} component={PaymentSetup} />
  </div>
)

export default CardComponents;