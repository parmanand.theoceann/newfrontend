import React from 'react';

const Page = () => {
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            Sea Link Reporting Form
            </div>
        </div>
      </article>
    </div>
  );
}

export default Page;