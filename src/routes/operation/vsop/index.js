import React from 'react';
import { Route } from 'react-router-dom';

import SeaLinkReportingForm from './sealink-reporting-form';
import VesselECOPerformanceMatrixReportsAnalytics from './vessel-eco-performance-matrix-reports-analytics';
import VesselMappingList from './vessel-mapping-list';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/sealink-reporting-form`} component={SeaLinkReportingForm} />
    <Route path={`${match.url}/vessel-eco-performance-matrix-reports-analytics`} component={VesselECOPerformanceMatrixReportsAnalytics} />
    <Route path={`${match.url}/vessel-mapping-list`} component={VesselMappingList} />
  </div>
)

export default CardComponents;