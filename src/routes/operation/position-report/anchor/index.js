import React, { Component } from 'react';
import { Form, Input, Button, Select, Row, Col, Layout, Table } from 'antd';

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

const FormItem = Form.Item;
const Option = Select.Option;
const InputGroup = Input.Group;
const { Content } = Layout;
const { TextArea } = Input;

const columns = [
    {
        title: '(mt)',
        dataIndex: 'mt',
        key: 'mt',
        width: 200,
    },
    {
        title: 'IFO',
        dataIndex: 'ifo',
        key: 'ifo',
    },
    {
        title: 'VLSFO',
        dataIndex: 'vlsfo',
        key: 'vlsfo',
    },
    {
        title: 'MGO',
        dataIndex: 'mgo',
        key: 'mgo',
    },
    {
        title: 'LSMGO',
        dataIndex: 'lsmgo',
        key: 'lsmgo',
    },
    {
        title: 'ULSFO',
        dataIndex: 'ulsfo',
        key: 'ulsfo',
    },
];

const data = [
    {
        key: '1',
        mt: 'BROB',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '2',
        mt: 'ME Cons SLR',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '3',
        mt: 'AE Cons SLR',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '4',
        mt: 'Boiler Cons SLR',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '5',
        mt: 'Fuel Received',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '6',
        mt: 'Fuel Debunkered',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
    {
        key: '7',
        mt: 'BROB Correction',
        ifo: '',
        vlsfo: '',
        mgo: '',
        lsmgo: '',
        ulsfo: '',
    },
];

class Anchor extends Component {
    render() {
        return (
            <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
                <Layout className="layout-wrapper" style={{ height: 'inherit', overflow: 'unset' }}>
                    <Layout>
                        <Content className="content-wrapper" style={{ height: 'inherit', overflow: 'unset' }}>
                            <Row gutter={16} style={{ marginRight: 0 }}>
                                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                    <div className="body-wrapper">
                                        <article className="article">
                                            <div className="box box-default">
                                                <div className="box-body">

                                                    <div className="form-wrapper toolbar-ui-wrapper">
                                                        <div className="form-heading">
                                                            <h4 className="title"><span>Anchor Report</span></h4>
                                                        </div>
                                                        <div className="action-btn">
                                                            <Button type="primary" htmlType="submit">Save</Button>
                                                            <Button>Reset</Button>
                                                        </div>
                                                    </div>

                                                    <Form>

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                                <div className="normal-heading wrap-group-heading no-padding m-t-0"><span>Main Information</span></div>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Vessel Name" required>
                                                                    <Input size="default" placeholder="" />
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="IMO Num." required>
                                                                    <Input size="default" placeholder="" />
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Call Sign">
                                                                    <Input size="default" placeholder="" />
                                                                </FormItem>
                                                            </Col>
                                                        </Row>


                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                                <div className="normal-heading wrap-group-heading"><span>Position and Time Information</span></div>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Latitude" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '40%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '40%' }} placeholder="" />
                                                                        <Select defaultValue="N" style={{ width: '20%' }}>
                                                                            <Option value="N">N</Option>
                                                                            <Option value="S">S</Option>
                                                                        </Select>
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Time Offset from UTC">
                                                                    <InputGroup compact>
                                                                        <Select defaultValue="Option" style={{ width: '50%' }}>
                                                                            <Option value="Option First">Option First</Option>
                                                                            <Option value="Option Second">Option Second</Option>
                                                                        </Select>
                                                                        <Input size="default" style={{ width: '50%' }} placeholder="Hr (+east, -west)" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Longitude" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '40%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '40%' }} placeholder="" />
                                                                        <Select defaultValue="E" style={{ width: '20%' }}>
                                                                            <Option value="E">E</Option>
                                                                            <Option value="W">W</Option>
                                                                        </Select>
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Event Date" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '40%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="Time" disabled />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="UTC" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                        <hr />

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                                <div className="normal-heading wrap-group-heading"><span>Event Information</span></div>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Captain" required>
                                                                    <Input size="default" placeholder="" />
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Aft Draft" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="m" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Fore Draft" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="m" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Event Type" required>
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Mid Draft" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="m" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Time SLR" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                        <hr />

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Current Speed" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kts" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Sea State">
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Swell State">
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Wind Beaufort" required>
                                                                    <Input size="default" placeholder="" />
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Air Temp." required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="°C" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Barometric Pressure Mvmt" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '70%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '30%' }} placeholder="mBars" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Current Direction" required>
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Seas Height">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="m" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Swell Height">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="m" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Wind Speed">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kts" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Sea Surface Temp." required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="°C" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Current Flow" required>
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Sea Direction">
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Swell Direction">
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Wind Direction">
                                                                    <Select defaultValue="First">
                                                                        <Option value="First">First</Option>
                                                                        <Option value="Second">Second</Option>
                                                                    </Select>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Barometric Pressure" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '70%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '30%' }} placeholder="mBars" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                        <hr />

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Boiler Running Hours SLR" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                        <hr />

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Power 1" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kW" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Power 2" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kW" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Power 3" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kW" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Power 4">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="kW" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Hours 1" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Hours 2" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Hours 3" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Hours 4">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="hrs" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Load 1" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="%" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Load 2" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="%" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Load 3" required>
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="%" disabled />
                                                                    </InputGroup>
                                                                </FormItem>

                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="AE Load 4">
                                                                    <InputGroup compact>
                                                                        <Input size="default" style={{ width: '80%' }} placeholder="" />
                                                                        <Input size="default" style={{ width: '20%' }} placeholder="%" disabled />
                                                                    </InputGroup>
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                        <hr />

                                                        <Row gutter={16}>
                                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                                <div className="normal-heading wrap-group-heading no-padding"><span>Fuel</span></div>
                                                            </Col>

                                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                                <Table bordered pagination={false} columns={columns} dataSource={data} />
                                                            </Col>

                                                            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                                <FormItem
                                                                    {...formItemLayout}
                                                                    label="Remarks">
                                                                    <TextArea placeholder="" autoSize={{ minRows: 1, maxRows: 3 }} />
                                                                </FormItem>
                                                            </Col>
                                                        </Row>

                                                    </Form>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </Col>
                            </Row>
                        </Content>
                    </Layout>
                </Layout>
            </div>
        )
    }
}

export default Anchor;