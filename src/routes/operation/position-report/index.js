import React from 'react';
import { Route } from 'react-router-dom';

import DepartureReport from './departure-report';
import COSP from './cosp';
import NoonReport from './noon-report';
import StopAtSea from './stop-at-sea';
import FuelChange from './fuel-change';
import EOSP from './eosp';
import Anchor from './anchor';
import Shifting from './shifting';
import Drifting from './drifting';
import Arrival from './arrival';
import InPort from './in-port';

const PositionReport = ({ match }) => (
  <div>
    <Route path={`${match.url}/departure-report`} component={DepartureReport} />
   <Route path={`${match.url}/cosp`} component={COSP} />
    <Route path={`${match.url}/noon-report`} component={NoonReport} />
    <Route path={`${match.url}/stop-at-sea`} component={StopAtSea} />
    <Route path={`${match.url}/fuel-change`} component={FuelChange} />
    <Route path={`${match.url}/eosp`} component={EOSP} />
    <Route path={`${match.url}/anchor`} component={Anchor} />
    <Route path={`${match.url}/shifting`} component={Shifting} />
    <Route path={`${match.url}/drifting`} component={Drifting} />
    <Route path={`${match.url}/arrival`} component={Arrival} />
    <Route path={`${match.url}/in-port`} component={InPort} /> 
  </div>
)

export default PositionReport;