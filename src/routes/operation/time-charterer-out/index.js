import React from 'react';
import { Route } from 'react-router-dom';

import Commission from './commission';
import HireStatement from './hire-statement';
import TcBillingList from './tc-billing-list';
import TimeCharterOutList from './time-charter-out-list';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/commission`} component={Commission} />
    <Route path={`${match.url}/hire-statement`} component={HireStatement} />
    <Route path={`${match.url}/tc-billing-list`} component={TcBillingList} />
    <Route path={`${match.url}/time-charter-out-list`} component={TimeCharterOutList} />
  </div>
)

export default CardComponents;