import React from 'react';
import { Route } from 'react-router-dom';

import Commission from './commission';
import OwnerCommissionList from './owner-commission-list';
import PhsFhsStatement from './phs-fhs-statement';
import TcHirePaymentScheduleList from './tc-hire-payment-schedule-list';
import TcList from './tc-list';
import TcPayment from './tc-payment';

const CardComponents = ({ match }) => (
  <div>
    <Route path={`${match.url}/commission`} component={Commission} />
    <Route path={`${match.url}/owner-commission-list`} component={OwnerCommissionList} />
    <Route path={`${match.url}/phs-fhs-statement`} component={PhsFhsStatement} />
    <Route path={`${match.url}/tc-hire-payment-schedule-list`} component={TcHirePaymentScheduleList} />
    <Route path={`${match.url}/tc-list`} component={TcList} />
    <Route path={`${match.url}/tc-payment`} component={TcPayment} />
  </div>
)

export default CardComponents;