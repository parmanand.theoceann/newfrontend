import React, { Component } from 'react';
import { Select, Form, Modal, Input, Button } from 'antd';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import PortServicedForm from '../../components/PortServicedForm';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class CompanyRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowPdmsSetupForm: false,
    };
  }
  PdmsSetupForm = showPdmsSetupForm =>
    this.setState({ ...this.state, isShowPdmsSetupForm: showPdmsSetupForm });
  render() {
    const { isShowPdmsSetupForm } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <NormalFormIndex
                frmCode={null}
                showToolbar={[
                  {
                    isLeftBtn: [
                      {
                        isSets: [
                          {
                            key: 'pdms_setup',
                            isDropdown: 0,
                            withText: 'PDMS Setup',
                            type: 'tool',
                            menus: null,
                            event: key => this.PdmsSetupForm(true),
                          },

                          {
                            key: 'print',
                            isDropdown: 0,
                            withText: ' Print',
                            type: 'solution',
                            menus: null,
                          },
                        ],
                      },
                    ],
                    isRightBtn: [],
                  },
                ]}
                inlineLayout={true}
              />
            </div>
          </div>
        </article>
        <article className="article ">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Company Type">
                      <Select size="default" defaultValue="1">
                        <Option value="1">Option</Option>
                        <Option value="2">Option</Option>
                        <Option value="3">Option</Option>
                      </Select>
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Company Short Name">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Currency">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Company (Full Name)">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Country">
                      <Select size="default" defaultValue="1">
                        <Option value="1">Option</Option>
                        <Option value="2">Option</Option>
                        <Option value="3">Option</Option>
                      </Select>
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Country Code">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Phone">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Fax">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Telex Number">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Email">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Web Page">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Main Contact">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Company S/N">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Credit Terms">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Credit Line">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="VAT No">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                </div>
              </Form>

              <div className="row p10">
                <div className="col-md-12">
                  <div className="action-btn text-right">
                    <Button className="ant-btn ant-btn-primary">Submit</Button>
                    <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        {isShowPdmsSetupForm ? (
          <Modal
            style={{ top: '2%' }}
            title="PDMS Setup Form"
            open={isShowPdmsSetupForm}
            onOk={this.handleOk}
            onCancel={() => this.PdmsSetupForm(false)}
            width="70%"
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default" style={{ padding: '15px' }}>
                  <PortServicedForm />
                </div>
              </article>
            </div>
          </Modal>
        ) : (
          undefined
        )}
      </div>
    );
  }
}

export default CompanyRegistration;
