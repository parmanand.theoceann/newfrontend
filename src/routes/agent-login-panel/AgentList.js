import React, { Component } from 'react';
import AddressList from '../address-list/index';
import { Modal, Table } from 'antd';
import URL_WITH_VERSION, { getAPICall, ResizeableTitle, objectToQueryStringFunc } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import moment from 'moment';
import PortcallDetails from '../../routes/portcall-detail/PortcallDetails';
import ToolbarUI from '../../components/CommonToolbarUI/index';
import {EyeOutlined }  from '@ant-design/icons';
class AgentList extends Component {
  constructor(props) {
    super(props);
    let tableHeaders = Object.assign([], FIELDS && FIELDS['agent-appoinment-list'] ? FIELDS['agent-appoinment-list']["tableheads"] : [])
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => this.edagenrowRedirect(e, record.id)}>
            <EyeOutlined />
            </span>
          </div>
        )
      }
    };
    tableHeaders.push(tableAction)
    this.state = {
      isShowAgentAppoinment: false,
      appoint_list: [],
      columns: tableHeaders,
      loading: false,
      isShowAgentAppoinmentDetails: false,
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    }
  }

  edagenrowRedirect = async (event, id) => {
    //const { history } = this.props;
    //history.push(`/portcall-details/${id}`);
    this.setState({ isShowAgentAppoinmentDetails: true, selectedPortId: id })
  };

  showSubAppoinmentList = async (id, newArray) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/edit?e=${id}`);
    const details = await response['data'];
    if (details) {
      if (details.id === id) {
        let data = {
          charterer_name: details.cargodetails && details.cargodetails.charterer_name ? details.cargodetails.charterer_name : 'NA',
          activity: details.cargodetails && details.cargodetails.activity ? details.cargodetails.activity : 'NA',
          port: details.portcalldetails && details.portcalldetails.port,
          country: details.portcalldetails && details.portcalldetails.country,
          eta: details.portcalldetails && moment(details.portcalldetails.eta).format('YYYY-MM-DD HH:mm'),
          etd: details.portcalldetails && moment(details.portcalldetails.etd).format('YYYY-MM-DD HH:mm'),
          status: (details && details.status === 1) ? 'Active' : 'Inactive'
        }
        newArray.push(data)
      }
    }
    this.setState({ ...this.state, appoint_list: newArray, loading: false });
  }

  showAppoinmentDetail = boolean => this.setState({ isShowAgentAppoinmentDetails: boolean })

  showAppoinmentList = async (boolean, agentId) => {
    if (agentId) {
      //this.setState({ loading: true, isShowAgentAppoinment: boolean })
      this.getTableData(agentId)
      // let newArray = []
      // const response = await getAPICall(`${URL_WITH_VERSION}/port-call/list?ag=${agentId}`);
      // const data = await response['data'];
      // if (data) {
      //   if (data.length > 0) {
      //     data && data.map((e) => {
      //       if(e.port_cargo_activity){
      //         //e['port_cargo_activity'] = e.port_cargo_activity.slice(1, -1)
      //         e['port_cargo_activity'] =  e.port_cargo_activity.replace(/['"]+/g, '')
      //         e['port_cargo_activity'] =  e.port_cargo_activity.replace("[", '')
      //         e['port_cargo_activity'] =  e.port_cargo_activity.replace("]", '')
      //        }
      //       newArray.push(e)
      //     })
      //   }
      //   this.setState({ ...this.state, appoint_list: newArray, loading: false });
      // } else {
      //   this.setState({ ...this.state, loading: false, appoint_list: [] });
      // }
    } else {
      this.setState({ ...this.state, isShowAgentAppoinment: boolean, appoint_list: [] });
    }
  }
  getTableData = async (agentId, search = {}) => {
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      appoint_list: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/port-call/list?ag=${agentId}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    if(data.data.length > 0){
        data.data.map(e=>{
          if(e.port_cargo_activity){
            e['port_cargo_activity'] =  e.port_cargo_activity.replace(/['"]+/g, '')
            e['port_cargo_activity'] =  e.port_cargo_activity.replace("[", '')
            e['port_cargo_activity'] =  e.port_cargo_activity.replace("]", '')
           }
      })
    }
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };

    if (dataArr.length > 0) {
      state['appoint_list'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
      isShowAgentAppoinment: true
    });
  };


  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let appoint_list = this.state.appoint_list;
      let columns = Object.assign([], this.state.columns);

      if (appoint_list.length > 0) {
        for (var k in appoint_list[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }
  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  render() {
    const { columns, isShowAgentAppoinment, appoint_list, loading, isShowAgentAppoinmentDetails, selectedPortId, pageOptions, search } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <>
        <AddressList childPage='agentlist' history={this.props.history} openModel={this.showAppoinmentList} />
        {isShowAgentAppoinment &&
          <Modal
            style={{ top: '2%' }}
            title="Agent Appointment List"
            open={isShowAgentAppoinment}
            //onOk={this.handleOk}
            onCancel={() => this.showAppoinmentList(false)}
            width="90%"
            footer={null}
          >
            <div className="container-fluid p-0">
            <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={'agent-appoinment-list-toolbar'}
                    optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                    callback={e => this.callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv', 'port-call') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf', 'port-call') },
                      { title: 'XLS', event: () => this.onActionDonwload('xls', 'port-call') },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>
              <Table
                rowKey='id'
                className="inlineTable resizeableTable"
                bordered
                columns={tableColumns}
                size="small"
                components={this.components}
                scroll={{ x: 'max-content' }}
                dataSource={appoint_list}
                pagination={false}
                loading={loading}
                rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
              />
            </div>

          </Modal>
        }
        {isShowAgentAppoinmentDetails &&
          <Modal
            style={{ top: '2%' }}
            title="Agent Appointment List"
            open={isShowAgentAppoinmentDetails}
            //onOk={this.handleOk}
            onCancel={() => this.showAppoinmentDetail(false)}
            width="90%"
            footer={null}
          >
            <PortcallDetails isAgentView={true} selectedPortId={selectedPortId} {...this.props} />

          </Modal>
        }
      </>
    )
  }
}
export default AgentList;