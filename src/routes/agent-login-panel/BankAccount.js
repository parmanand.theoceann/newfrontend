import React, { Component } from 'react';
import { Table, Button, Tabs } from 'antd';

const TabPane = Tabs.TabPane;

const columns = [
  {
    title: 'Beneficiary Name',
    dataIndex: 'ban_name',
  },

  {
    title: 'Beneficiary Bank',
    dataIndex: 'ban_bank',
  },

  {
    title: 'Branch',
    dataIndex: 'branch',
  },

  {
    title: 'Acc. No',
    dataIndex: 'acc_no',
  },

  {
    title: 'Swift Code',
    dataIndex: 'swift_code',
  },

  {
    title: 'Correspondent Bank',
    dataIndex: 'cor_bank',
  },

  {
    title: 'CB Swift Code',
    dataIndex: 'cb_code',
  },

  {
    title: 'IBAN',
    dataIndex: 'iban',
  },

  {
    title: 'International Bank Name',
    dataIndex: 'international_bank_name',
  },

  {
    title: 'Country',
    dataIndex: 'country',
  },

  {
    title: 'Payment Method',
    dataIndex: 'payment_method',
  },

  {
    title: 'Account Type',
    dataIndex: 'account_type',
  },
];
const data = [
  {
    key: '1',
    ban_name: 'Beneficiary Name',
    ban_bank: 'Beneficiary Bank',
    branch: 'Branch',
    acc_no: 'Acc. No',
    swift_code: 'Swift Code',
    cor_bank: 'Correspondent Bank',
    cb_code: 'CB Swift Code',
    iban: 'IBAN',
    international_bank_name: 'International Bank Name',
    country: 'Country',
    payment_method: 'PAyment Method',
    account_type: 'Account Type',
  },

  {
    key: '2',
    ban_name: 'Beneficiary Name',
    ban_bank: 'Beneficiary Bank',
    branch: 'Branch',
    acc_no: 'Acc. No',
    swift_code: 'Swift Code',
    cor_bank: 'Correspondent Bank',
    cb_code: 'CB Swift Code',
    iban: 'IBAN',
    international_bank_name: 'International Bank Name',
    country: 'Country',
    payment_method: 'PAyment Method',
    account_type: 'Account Type',
  },

  {
    key: '3',
    ban_name: 'Beneficiary Name',
    ban_bank: 'Beneficiary Bank',
    branch: 'Branch',
    acc_no: 'Acc. No',
    swift_code: 'Swift Code',
    cor_bank: 'Correspondent Bank',
    cb_code: 'CB Swift Code',
    iban: 'IBAN',
    international_bank_name: 'International Bank Name',
    country: 'Country',
    payment_method: 'PAyment Method',
    account_type: 'Account Type',
  },

  {
    key: '4',
    ban_name: 'Beneficiary Name',
    ban_bank: 'Beneficiary Bank',
    branch: 'Branch',
    acc_no: 'Acc. No',
    swift_code: 'Swift Code',
    cor_bank: 'Correspondent Bank',
    cb_code: 'CB Swift Code',
    iban: 'IBAN',
    international_bank_name: 'International Bank Name',
    country: 'Country',
    payment_method: 'PAyment Method',
    account_type: 'Account Type',
  },

  {
    key: '5',
    ban_name: 'Beneficiary Name',
    ban_bank: 'Beneficiary Bank',
    branch: 'Branch',
    acc_no: 'Acc. No',
    swift_code: 'Swift Code',
    cor_bank: 'Correspondent Bank',
    cb_code: 'CB Swift Code',
    iban: 'IBAN',
    international_bank_name: 'International Bank Name',
    country: 'Country',
    payment_method: 'PAyment Method',
    account_type: 'Account Type',
  },
];

const columns2 = [
  {
    title: 'Name',
    dataIndex: 'name',
  },

  {
    title: 'Director No',
    dataIndex: 'director_no',
  },

  {
    title: 'Fax No',
    dataIndex: 'fax_no',
  },

  {
    title: 'Home No',
    dataIndex: 'home_no',
  },

  {
    title: 'Mobile No',
    dataIndex: 'mobile_no',
  },
];
const data2 = [
  {
    key: '1',
    name: 'Name',
    director_no: 'Director No',
    fax_no: 'Fax No',
    home_no: 'Home No',
    mobile_no: 'Mobile No',
  },

  {
    key: '2',
    name: 'Name',
    director_no: 'Director No',
    fax_no: 'Fax No',
    home_no: 'Home No',
    mobile_no: 'Mobile No',
  },

  {
    key: '3',
    name: 'Name',
    director_no: 'Director No',
    fax_no: 'Fax No',
    home_no: 'Home No',
    mobile_no: 'Mobile No',
  },

  {
    key: '4',
    name: 'Name',
    director_no: 'Director No',
    fax_no: 'Fax No',
    home_no: 'Home No',
    mobile_no: 'Mobile No',
  },

  {
    key: '5',
    name: 'Name',
    director_no: 'Director No',
    fax_no: 'Fax No',
    home_no: 'Home No',
    mobile_no: 'Mobile No',
  },
];

const columns3 = [
  {
    title: 'Name',
    dataIndex: 'name',
  },

  {
    title: 'Description',
    dataIndex: 'description',
  },
];
const data3 = [
  {
    key: '1',
    name: 'Name',
    description: 'Description',
  },

  {
    key: '2',
    name: 'Name',
    description: 'Description',
  },

  {
    key: '3',
    name: 'Name',
    description: 'Description',
  },

  {
    key: '4',
    name: 'Name',
    description: 'Description',
  },

  {
    key: '5',
    name: 'Name',
    description: 'Description',
  },
];
class BankAccount extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article ">
          <div className="box box-default">
            <div className="box-body">
              <Tabs defaultActiveKey="bankaccountdetails" size="small">
                <TabPane className="pt-3" tab="Bank & Account Details" key="bankaccountdetails">
                  <Table
                    bordered
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                  />
                </TabPane>
                <TabPane className="pt-3" tab="Contacts" key="contacts">
                  <Table
                    bordered
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                    columns={columns2}
                    dataSource={data2}
                    pagination={false}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                  />
                </TabPane>

                <TabPane className="pt-3" tab="Additional Information" key="additioninformation">
                  <Table
                    bordered
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                    columns={columns3}
                    dataSource={data3}
                    pagination={false}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                  />
                </TabPane>
              </Tabs>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default BankAccount;
