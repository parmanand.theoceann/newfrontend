import React, { Component } from 'react';
import { Select, Form, Input, Button } from 'antd';


const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class CompanyRegistration extends Component {
 
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        
        <article className="article ">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="User ID">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="User Password">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Change Password">
                      <Input size="default" defaultValue="" />
                    </FormItem>
                  </div>
                  
                </div>
              </Form>

              <div className="row p10">
                <div className="col-md-12">
                  <div className="action-btn text-right">
                    <Button className="ant-btn ant-btn-primary">Submit</Button>
                    <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

      
      </div>
    );
  }
}

export default CompanyRegistration;
