import React, { useEffect } from 'react';
import { Table, Icon } from 'antd';
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon, ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';


const  RemittenseBankList = (props) => {
  const [state, setState] = useStateCallback({
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    columns: [],
    loading: false,
    bankID: props.bankID
  })

  useEffect(() => {
    const BankDetailsTableHeaders = Object.assign([], FIELDS && FIELDS['remittance-bank'] ? FIELDS['remittance-bank']["tableheads"] : [])
    setState(prevState => ({...prevState, columns: BankDetailsTableHeaders}), () => getTableData())
  }, [])


  const getTableData = async (search = {}) => {
    const { pageOptions, bankID } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    //let qParamString = objectToQueryStringFunc(qParams);
    let _state = {}, dataArr = [], totalRows = 0;
    let _url = `${URL_WITH_VERSION}/address/edit_rem?ae=${bankID}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    totalRows = data && data.total_rows ? data.total_rows : 0;
    dataArr = data && data.data ? data.data : [];

    if (dataArr.length > 0) {
      _state['responseData'] = dataArr;
    }
    setState((prevState) => ({
      ...prevState,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));

  };



  // const getTableData = async () => {
  //   const { pageOptions, bankID } = state;
  //   console.log('bankk',bankID)
  //   setState(prevState => ({ ...prevState, isloading: true }));
  //   try {
  //     const response = await fetch(
  //       `${process.env.REACT_APP_URL}v1/address/vm/edit?ae=${bankID}`
  //     );
  //     const rowData = await response.json();
  //     setState(prevState => ({...prevState,  data : rowData.data}));
  //      console.log(rowData);
  //   } catch {
  //     // openNotificationWithIcon("info", "No Data Available for this Vessel");
  //     openNotificationWithIcon("info", "No data found.");
  //   }
  //   setState(prevState => ({ ...prevState, loading: false }));
  // };
  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };



  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  //resizing function
  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };


    const { columns, responseData, loading, pageOptions, search } = state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Table
                rowKey={record => record.id}
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                scroll={{ x: 'max-content' }}
                components={components}
                columns={tableColumns}
                size="small"
                dataSource={responseData}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
              />
            </div>
          </div>
        </article>
      </div>
    );
  }

export default RemittenseBankList;