import React from 'react';
import { Table, Card, Layout } from 'antd';
const columns = [
  {
    title: 'Created Date',
    dataIndex: 'created_date',
  },

  {
    title: 'Portcall Ref. ID',
    dataIndex: 'portcall_id',
  },

  {
    title: 'Vessel',
    dataIndex: 'vessel',
  },

  {
    title: 'Port',
    dataIndex: 'port',
  },

  {
    title: 'Activity',
    dataIndex: 'activity',
  },

  {
    title: 'Agent',
    dataIndex: 'agent',
  },

  {
    title: 'Company',
    dataIndex: 'company',
  },

  {
    title: 'Status',
    dataIndex: 'status',
  },

  {
    title: 'PDA Cost',
    dataIndex: 'pda_cost',
  },

  {
    title: 'FDA Cost',
    dataIndex: 'fda_cost',
  },

  {
    title: 'ETA Date',
    dataIndex: 'eta_date',
  },

  {
    title: 'ETD Date',
    dataIndex: 'etd_date',
  },
];
const data = [
  {
    key: '1',
    created_date: 'Created Date',
    portcall_id: 'Portcall ID',
    vessel: 'Vessel',
    port: 'Port',
    activity: 'Activity',
    agent: 'Agent',
    company: 'Company',
    status: 'Status',
    pda_cost: 'PDA Cost',
    fda_cost: 'FDA Cost',
    eta_date: 'ETA Date',
    etd_date: 'ETD Date',
  },
  {
    key: '2',
    created_date: 'Created Date',
    portcall_id: 'Portcall ID',
    vessel: 'Vessel',
    port: 'Port',
    activity: 'Activity',
    agent: 'Agent',
    company: 'Company',
    status: 'Status',
    pda_cost: 'PDA Cost',
    fda_cost: 'FDA Cost',
    eta_date: 'ETA Date',
    etd_date: 'ETD Date',
  },

  {
    key: '3',
    created_date: 'Created Date',
    portcall_id: 'Portcall ID',
    vessel: 'Vessel',
    port: 'Port',
    activity: 'Activity',
    agent: 'Agent',
    company: 'Company',
    status: 'Status',
    pda_cost: 'PDA Cost',
    fda_cost: 'FDA Cost',
    eta_date: 'ETA Date',
    etd_date: 'ETD Date',
  },

  {
    key: '4',
    created_date: 'Created Date',
    portcall_id: 'Portcall ID',
    vessel: 'Vessel',
    port: 'Port',
    activity: 'Activity',
    agent: 'Agent',
    company: 'Company',
    status: 'Status',
    pda_cost: 'PDA Cost',
    fda_cost: 'FDA Cost',
    eta_date: 'ETA Date',
    etd_date: 'ETD Date',
  },

  {
    key: '5',
    created_date: 'Created Date',
    portcall_id: 'Portcall ID',
    vessel: 'Vessel',
    port: 'Port',
    activity: 'Activity',
    agent: 'Agent',
    company: 'Company',
    status: 'Status',
    pda_cost: 'PDA Cost',
    fda_cost: 'FDA Cost',
    eta_date: 'ETA Date',
    etd_date: 'ETD Date',
  },
];
class AgentDashboard extends React.Component {
  render() {
    return (
      <div className="wrap-rightbar full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <div className="portcall-dashboard-wrap">
                    <Card size="small" title="New Portcall" className="card-block-wrap">
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/1.png"
                            className="port-img-responsive" alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">34</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Accepted Portcall"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/2.png"
                            className="port-img-responsive" alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">6</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="PDA Approved"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/3.png"
                            className="port-img-responsive" alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">5</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="FDA Approved"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/4.png"
                            className="port-img-responsive" alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">0</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Close Portcall"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/1.png"
                            className="port-img-responsive"
                            alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">0</div>
                      </div>
                    </Card>

                    <Card
                      size="small"
                      title="Resubmision"
                      className="card-block-wrap"
                    >
                      <div className="port-row-wrap">
                        <div className="portcall-icons">
                          {/* <img
                            src="assets/images-demo/portcall-dashboard/2.png"
                            className="port-img-responsive"
                            alt=""
                          /> */}
                        </div>
                        <div className="portcall-rate-no">0</div>
                      </div>
                    </Card>
                  </div>
                  <div className="form-wrapper mt-3">
                    <div className="form-heading">
                      <h4 className="title">
                        <span>My Portcalls</span>
                      </h4>
                    </div>
                  </div>

                  <Table
                    bordered
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                  />
                </div>
              </div>
            </article>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default AgentDashboard;
