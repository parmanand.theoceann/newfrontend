import React, { Component } from 'react';
import { Table, Button } from 'antd';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, ResizeableTitle, } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';

class ArchivesList extends Component {
  constructor(props) {
    super(props);
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['archive-list'] ? FIELDS['archive-list']['tableheads'] : []
    );
    this.state = {
      responseData: [],
      columns: tableHeaders,
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      loading: false
    };

  }
  componentDidMount = () => {
    this.getTableData();
  }

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }

  getTableData = async (search = {}) => {
    const { pageOptions, VoiID } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);
    //?${qParamString}
    let _url = `${URL_WITH_VERSION}/port-call/list?a=1`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    if (data.data.length > 0) {
      data.data.map(pl => {
        pl['status'] = pl['status'] == 3 ? 'Archives' : 'SENT'
      })
    }
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };

    if (dataArr.length > 0) {
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false
    });
  };


  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };


  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };
  render() {
    const { columns, responseData, loading, pageOptions, search } = this.state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article ">
          <div className="box box-default">
            <div className="box-body">
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={'archieve-list-toolbar'}
                    optionValue={{ pageOptions: pageOptions, columns: tableColumns, search: search }}
                    callback={e => this.callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv', 'archieve') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf', 'archieve') },
                      { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'archieve') },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>
              <Table
                bordered
                dataSource={responseData}
                columns={tableColumns}
                scroll={{ x: 'max-content' }}
                color={'pink'}
                size="small"
                pagination={false}
                loading={loading}
                footer={false}
                rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
              // footer={() => (
              //   <div className="text-center">
              //     <Button type="link">Add New</Button>
              //   </div>
              // )}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ArchivesList;
