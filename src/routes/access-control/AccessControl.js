import React from "react";
import { Checkbox, Table, Button } from "antd";
import URL_WITH_VERSION, {
  getAPICall,
 
  postAPICall,
 
  openNotificationWithIcon,
  
} from "../../shared";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
class AccessControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 55 },
      checkedarr: [],
      menurole: [],
      loading: false,
      responseData: [],
      responseUpdatedData: [],
      tablecolumns: [
        {
          title: "Menu",
          dataIndex: "menu_name",
          key: "menu_name",
          render: (el) => <span className="spot-first">{el}</span>,
        },
        {
          title: "Admin",
          dataIndex: "Admin_status",
          key: "Admin_status",
          render: (_, record) => (
            <Checkbox
              onChange={(e) => {
                this.handleCheck(record, "Admin_status", e);
              }}
              // checked={this.state.checkedarr.some(
              //   (item) =>
              //     item.menu_id === record.menu_id &&
              //     item.role.some((role) => role.role_id == "1")
              // )}
              defaultChecked={record.Admin_status == 1 ? true : false}
            />
          ),
        },
        {
          title: "Charter",
          dataIndex: "Charterers_status",
          key: "Charterers_status",
          width: "12%",
          render: (_, record) => (
            <Checkbox
              onChange={(e) => this.handleCheck(record, "Charterers_status", e)}
              // checked={this.state.checkedarr.some(
              //   (item) =>
              //     item.menu_id === record.menu_id &&
              //     item.role.some((role) => role.role_id == "2")
              // )}
              defaultChecked={record.Charterers_status == 1 ? true : false}
            />
          ),
        },
        {
          title: "Operation",
          dataIndex: "Operations_status",
          key: "Operations_status",
          render: (_, record) => (
            <Checkbox
              onChange={(e) => this.handleCheck(record, "Operations_status", e)}
              // checked={this.state.checkedarr.some(
              //   (item) =>
              //     item.menu_id === record.menu_id &&
              //     item.role.some((role) => role.role_id == "3")
              // )}
              defaultChecked={record.Operations_status == 1 ? true : false}
            />
          ),
        },
        {
          title: "Accounting",
          dataIndex: "Account_status",
          key: "Account_status",
          width: "12%",
          render: (_, record) => (
            <Checkbox
              onChange={(e) => this.handleCheck(record, "Account_status", e)}
              // checked={this.state.checkedarr.some(
              //   (item) =>
              //     item.menu_id === record.menu_id &&
              //     item.role.some((role) => role.role_id == "4")
              // )}
              defaultChecked={record.Account_status == 1 ? true : false}
            />
          ),
        },
        {
          title: "Finance",
          dataIndex: "Finance_status",
          key: "Finance_status",
          width: "12%",
          render: (_, record) => (
            <span>
              <Checkbox
                onChange={(e) => this.handleCheck(record, "Finance_status", e)}
                // checked={this.state.checkedarr.some(
                //   (item) =>
                //     item.menu_id === record.menu_id &&
                //     item.role.some((role) => role.role_id == "5")
                // )}

                defaultChecked={record.Finance_status == 1 ? true : false}
              />
            </span>
          ),
        },
      ],

 
     };
   }

  componentDidMount = () => {
     this.getTableData();
  
  };

 
 

 getTableData = async () => {
    const { pageOptions,checkedarr } = this.state;
    let headers = { order_by: { id: "desc" } };
    this.setState({
      ...this.state,
      loading: true,
      responseUpdatedData: [],
    });
    let updata = [];
    let state = { loading: false };

          let _url1=`${URL_WITH_VERSION}/userauth/edit/list`;
           const updatedresponse = await getAPICall(_url1, headers);
          const updateddata = await updatedresponse;
          const totalRows = updateddata && updateddata.total_rows ? updateddata.total_rows : 0

     let data = updateddata && updateddata.data ? updateddata.data : [];
    //-------------------------------------

    let updateddataArr = [...data];

    data.map((el1) => {
      let obj = {};
      obj["menu_id"] = el1.menu_id;
      obj["menu_name"] = el1.menu_name;
      let arr = [];
      el1["role"].map((item1) => {
        if (item1.status == 1) {
          let obj1 = {};
          obj1["role_id"] = item1.role_id;
          obj1["status"] = item1.status;
          arr.push(obj1);
        }
      });
      obj["role"] = [...arr];
      checkedarr.push(obj);

      el1["children"] &&
        el1["children"].length > 0 &&
        el1["children"].map((el2) => {
          let obj = {};
          obj["menu_id"] = el2.menu_id;
          obj["menu_name"] = el2.menu_name;
          let arr = [];
          el2["role"].map((item2) => {
            if (item2.status == 1) {
              let obj1 = {};
              obj1["role_id"] = item2.role_id;
              obj1["status"] = item2.status;
              arr.push(obj1);
            }
          });
          if (el1["children"] && arr.length > 0) {
            obj["role"] = [...arr];
          } else {
            obj["role"] = [];
          }
          checkedarr.push(obj);
        });
    });

    if (checkedarr.length > 0) {
      state["checkedarr"] = [...checkedarr];
    }

    data.map((el) => {
      let obj = {};
      obj["menu_name"] = el.menu_name;
      obj["menu_id"] = el.menu_id;
      obj["short_code"] = el.short_code;
      obj['key']=el.menu_id
      el["role"].map((item) => {
        obj[`${item.role_name}_status`] = item.status;
      });
      let childrenarr = [];
      el["children"] &&
        el["children"].length > 0 &&
        el["children"].map((item) => {
          let obj1 = {};
          obj1["menu_name"] = item.menu_name;
          obj1["menu_id"] = item.menu_id;
          obj1["short_code"] = item.short_code;
          obj1['key']=item.menu_id
          item["role"].map((item) => {
            obj1[`${item.role_name}_status`] = item.status;
          });
          childrenarr.push(obj1);
        });
      if (el["children"] && childrenarr.length > 0) {
        obj["children"] = [...childrenarr];
      }

      updata.push(obj);
    });

  
    if (updateddataArr.length > 0) {
      state["responseUpdatedData"] = [...updata];
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        //  totalRows: totalRows,
      },
      loading: false,
    });
  };

  handleCheck = (record, key, event) => {
    switch (key) {
      case "Admin_status":
        let checkeddata = {
          role_id: 1,
          status: event.target.checked === true ? 1 : 0,
        };
        this.handlechekrole(record, checkeddata);

        break;

      case "Charterers_status":
        checkeddata = {
          role_id: 2,
          status: event.target.checked == true ? 1 : 0,
        };

        this.handlechekrole(record, checkeddata);
        break;

      case "Operations_status":
        checkeddata = {
          role_id: 3,
          status: event.target.checked === true ? 1 : 0,
        };

        this.handlechekrole(record, checkeddata);
        break;

      case "Account_status":
        checkeddata = {
          role_id: 4,
          status: event.target.checked == true ? 1 : 0,
        };
        this.handlechekrole(record, checkeddata);

        break;

      case "Finance_status":
        checkeddata = {
          role_id: 5,
          status: event.target.checked == true ? 1 : 0,
        };
        this.handlechekrole(record, checkeddata);

        break;

      default:
        return null;
    }
  };

  handlechekrole = (record, checkeddata) => {
    const { checkedarr } = this.state;
    let role = [];
    let checkedobj = {
      menu_id: "",
    };
    let filteredcheck = checkedarr.filter(
      (item) => item.menu_id == record.menu_id
    );

    let unfilteredcheck = checkedarr.filter(
      (item) => item.menu_id !== record.menu_id
    );

    if (filteredcheck && filteredcheck.length > 0) {
      if (checkeddata.status !== 0) {
        filteredcheck[0]["role"].push({ ...checkeddata });
      } else {
        filteredcheck[0].role.map((el) => {
          if (el.role_id == checkeddata.role_id) {
            el.status = checkeddata.status;
          }
        });
      }
      this.setState({
        ...this.state,
        checkedarr: [...unfilteredcheck, ...filteredcheck],
      });
    } else {
      // if (checkeddata.status !== 0) {
      role.push(checkeddata);
      //}
      checkedobj = {
        menu_id: record.menu_id,
        menu_name: record.menu_name,
        role: [...role],
      };
      checkedarr.push(checkedobj);
      this.setState({ ...this.state, checkedarr: [...checkedarr] });
    }
  };

  handlesave = () => {
    const { checkedarr } = this.state;

    let data1 = [...checkedarr];
    let suMethod = "put";
    this.setState({...this.state, loading: true})
    postAPICall(
      `${URL_WITH_VERSION}/userauth/update`,
      data1,
      suMethod,
      (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
          
          this.getTableData();
        } else {
          openNotificationWithIcon("error", response.message);
        }
      }
    );
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  handleResize = (index) => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  render() {
    const {
      responseData,
      loading,
      pageOptions,
      responseUpdatedData,
      data,
      tablecolumns: columns,
    } = this.state;

    const tableCol = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          // onResize: this.handleResize(index),
        }),
      }));

    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-wrapper">
                  <div className="form-heading">
                    <h4 className="title">
                      <span>Menu List</span>
                    </h4>
                  </div>
                  <div className="action-btn">
                    <Button onClick={this.handlesave}>Save MenuList</Button>
                  </div>
                </div>
                <div
                  className="section"
                  style={{
                    width: "100%",
                    marginBottom: "10px",
                    paddingLeft: "15px",
                    paddingRight: "15px",
                  }}
                >
                  {loading === false ? (
                    <ToolbarUI
                      routeUrl={"access-control-menulist-toolbar"}
                      optionValue={{
                        pageOptions: pageOptions,
                        columns: columns,
                        // search: search
                      }}
                      callback={(e) => this.callOptions(e)}
                      // dowloadOptions={[
                      //   {title:'CSV', event: () => this.onActionDonwload('csv', 'vessel')},
                      //   {title:'PDF', event: () => this.onActionDonwload('pdf', 'vessel')},
                      //   {title:'XLS', event: () => this.onActionDonwload('xlsx', 'vessel')}
                      // ]}
                    />
                  ) : (
                    undefined
                  )}
                </div>
                <div>
                  <Table
                    indentSize={30}
                    rowKey={record => record.id}
                    className="inlineTable resizeableTable"
                    bordered
                    columns={tableCol}
                    // components={this.components}
                    size="small"
                    scroll={{ x: "max-content" }}
                    dataSource={responseUpdatedData}
                     loading={loading}
                    pagination={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0
                        ? "table-striped-listing"
                        : "dull-color table-striped-listing"
                    }
                  />
                </div>
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }
}

export default AccessControl;
