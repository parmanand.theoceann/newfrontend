import React, { Component } from 'react';


class FuelConsAnalysisVessel extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              
              <div className="row form-wrapper">
                <div className="col-6 form-heading">
                  <h4 class="title">                    
                    <span>Fuel Cons. Analysis Vessel</span>
                  </h4>
                </div>
                <div className="col-6 text-right">
                <h5 class="title"> 
                  <span><a href="#/dynamic-vspm">Back</a></span>
                  </h5>
                </div>
              </div>

              <div className="row p10 graph-image-height">
                <div className="col-md-9">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="top-graph border p-2 rounded">
                        <h5>
                          <b>Total sea cons. (MT)</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        <h2 className="text-center text-primary">104</h2>
                      </div>
                    </div>

                    <div className="col-md-3">
                      <div className="top-graph border p-2 rounded">
                        <h5>
                          <b>Total CO2 Emi.</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        <h2 className="text-center text-warning">104</h2>
                      </div>
                    </div>

                    <div className="col-md-3">
                      <div className="top-graph border p-2 rounded">
                        <h5>
                          <b> Total Port cons</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        <h2 className="text-center text-success">104</h2>
                      </div>
                    </div>

                    <div className="col-md-3">
                      <div className="top-graph border p-2 rounded">
                        <h5>
                          <b> Total Port Co2 Emi</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        <h2 className="text-center text-danger">104</h2>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Speed vs Time</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/line1.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Fuel vs Time</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/line2.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Speed Vs Fuel Perday</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <SpeedFuelPerdayChart /> */}
                        {/* <img alt="Fuel Chart" src="../../assets/bar1.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Spd + Current</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <SpdCurrentChart /> */}
                        {/* <img alt="Fuel Chart" src="../../assets/bar2.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Spd + Sea State</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/bar3.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>SPD +RPM</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/bar4.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Speed Vs Passage</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <SpdCurrentChart /> */}
                        {/* <img alt="Fuel Chart" src="../../assets/line3.jpg" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b>Fuel Vs Passage</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/line5.png" className="w-100 img-fluid" /> */}
                      </div>
                    </div>

                    <div className="col-4">
                      <div className="middel-graph border rounded p-2 mt-5">
                        <h5>
                          <b> SPd/Fuel Vs passage</b>
                        </h5>
                        <hr className="mt-1 mb-1" />
                        {/* <img alt="Fuel Chart" src="../../assets/line6.jpeg" className="w-100 img-fluid" /> */}
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="middel-graph border rounded p-2">
                    <h5>
                      <b>Bad weather day %</b>
                    </h5>
                    <hr className="mt-1 mb-1" />
                    {/* <SpeedTimeChart /> */}
                    {/* <img alt="Fuel Chart" src="../../assets/chart1.png" className="w-100 img-fluid" /> */}
                  </div>

                  <div className="middel-graph border rounded p-2 mt-3">
                    <h5>
                      <b>Generator effiecncy</b>
                    </h5>
                    <hr className="mt-1 mb-1" />
                    {/* <SpeedTimeChart /> */}
                    {/* <img alt="Fuel Chart" src="../../assets/chart2.png" className="w-100 img-fluid" /> */}
                  </div>

                  <div className="middel-graph border rounded p-2 mt-3">
                    <h5>
                      <b>Ave RPM</b>
                    </h5>
                    <hr className="mt-1 mb-1" />
                    {/* <SpeedTimeChart /> */}
                    {/* <img alt="Fuel Chart" src="../../assets/chart3.jpeg" className="w-100 img-fluid" /> */}
                  </div>

                  <div className="middel-graph border rounded p-2 mt-3">
                    <h5>
                      <b>Total Bunker consumed (Break up)</b>
                    </h5>
                    <hr className="mt-1 mb-1" />
                    {/* <SpeedTimeChart /> */}
                    {/* <img alt="Fuel Chart" src="../../assets/chart4.webp" className="w-100 img-fluid" /> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default FuelConsAnalysisVessel;
