import React, { Component } from 'react';
import { Table, Form, Input,  Popconfirm, Select } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
} from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import Departure from '../reporting-form/components/Departure';
import NoonReport from '../reporting-form/components/NoonReport';
import Arrival from '../reporting-form/components/Arrival';
import ModalAlertBox from '../../shared/ModalAlertBox';
import { DeleteOutlined, EditFilled } from '@ant-design/icons';

const FormItem = Form.Item;
const { Option } = Select
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};


class PortInformation extends Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Action',
      key: 'action',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper"
              onClick={(e) => this.redirectToAdd(e, record)}
            >
          <EditFilled/>

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?"
              // onConfirm={() => this.onRowDeletedClick(record.id)}
              >
             <DeleteOutlined/>
              </Popconfirm>
            </span>
          </div>
        )
      }
    };

    let tableHeaders = Object.assign([], FIELDS && FIELDS['noon-verification-list'] ? FIELDS['noon-verification-list']["tableheads"] : [])

    tableHeaders.push(tableAction);
    this.state = {
      responseData: [],
      columns: tableHeaders,
      loading: false,
      vessel_code: "",
      vessel_name: "",
      voyage_number: "",
      voyage_status: "",
      passage: [],
      modalInformation: {
        modalStatus: false,
        modalBody: undefined,
        modalHeader: undefined,
        modalFooter: undefined,
        modalWidth: '90%'
      }
    }
  }
  componentDidMount = () => {
    this.getApiData();
  };

  getApiData = async () => {
    if (this.props && this.props.formData && this.props.formData.id) {
      this.setState({
        loading: true
      })
      let _url = `${URL_WITH_VERSION}/voyage-manager/position-report?e=${this.props.formData.id}`;
      const response = await getAPICall(_url);
      const data = await response['data'];
      this.setState({
        responseData: data['report'],
        loading: false,
        vessel_code: data['vessel_code'],
        vessel_name: data['vessel_name'],
        voyage_number: data['voyage_number'],
        voyage_status: data['voyage_status'],
        passage: [...new Set(data['passage'])]
      })
    }


  }
  redirectToAdd = async (e, record = null) => {
    let _modalInformation = { modalStatus: true, modalHeader: undefined, modalFooter: undefined, modalWidth: '90%', modalBody: undefined };
    if (record && record.report_no) {
      switch (record.report_types.toLowerCase()) {
        case 'noon':
          _modalInformation['modalBody'] = () => <NoonReport reportNo={record.report_no} showTopBar={false} />;
          _modalInformation['modalHeader'] = 'Edit Noon Report';
          break;
        case 'departure':
          _modalInformation['modalBody'] = () => <Departure reportNo={record.report_no} />;
          _modalInformation['modalHeader'] = 'Edit Departure Report';
          break;
        case 'arrival':
          _modalInformation['modalBody'] = () => <Arrival reportNo={record.report_no} showTopBar={false} />;
          _modalInformation['modalHeader'] = 'Edit Arrival Report';
          break;
      }

      this.setState({ ...this.state, modalInformation: _modalInformation });
    }
  }

  onCloseModal = () => {
    this.setState({
      ...this.state, modalInformation: {
        modalStatus: false,
        modalBody: undefined,
        modalHeader: undefined,
        modalFooter: undefined,
        onCancelFunc: undefined,
        modalWidth: '90%'
      }
    });
    this.getApiData();
  }
  render() {
    const { modalInformation, columns, loading, responseData, vessel_code, vessel_name, voyage_number, voyage_status, passage } = this.state;
    const tableColumns = columns.filter((col) => (col && col.invisible !== "true") ? true : false)
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Vessel Name">
                      <Input size="default" value={vessel_name} disabled={true} />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Vessel Code">
                      <Input size="default" value={vessel_code} disabled={true} />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Voy no">
                      <Input size="default" value={voyage_number} disabled={true} />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Passage Type">
                      <Select mode="tags" style={{ width: '100%' }} placeholder="Passage Type"
                        value={passage}
                      >
                        {passage && passage.length > 0 && passage.map((val, ind) => {
                          return (
                            <Option key={ind}>{val}</Option>
                          )
                        })}
                      </Select>
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Voyage status">
                      <Input size="default" value={voyage_status} disabled={true} />
                    </FormItem>
                  </div>

                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Last Update">
                      <Input type="date" size="default" defaultValue="" />
                    </FormItem>
                  </div>
                </div>
              </Form>

              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    className="inlineTable editableFixedHeader resizeableTable"
                    bordered
                    columns={tableColumns}
                    scroll={{ x: 'max-content' }}
                    dataSource={responseData}
                    loading={loading}
                    pagination={false}
                    rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                  />
                </div>
              </div>
              {
                modalInformation.modalStatus ?
                  <ModalAlertBox
                    modalStatus={modalInformation.modalStatus}
                    modalHeader={modalInformation.modalHeader}
                    modalBody={modalInformation.modalBody}
                    modalFooter={modalInformation.modalFooter}
                    onCancelFunc={() => this.onCloseModal()}
                    modalWidth={modalInformation.modalWidth}
                  />
                  : undefined
              }
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortInformation;
