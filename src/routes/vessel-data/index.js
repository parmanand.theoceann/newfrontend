import React, { Component } from 'react'
import {
    Table, Modal,  Input
} from 'antd';
import {EyeOutlined} from '@ant-design/icons';
import URL_WITH_VERSION, {
    objectToQueryStringFunc,
    getAPICall
} from '../../shared';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import VesselDetails from './vesselDetails'
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';

class VesselData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    title: 'Ship Name',
                    dataIndex: 'shipname',
                    key: 'shipname',
                },
                {
                    title: 'IMO Ship No',
                    dataIndex: 'lrimoshipno',
                    key: 'lrimoshipno',
                },
                {
                    title: 'Status',
                    dataIndex: 'shipstatus',
                    key: 'shipstatus',
                },
                {
                    title: 'Gross Tonnage',
                    dataIndex: 'grosstonnage',
                    key: 'grosstonnage',
                },
                {
                    title: 'Dead Weight',
                    dataIndex: 'deadweight',
                    key: 'deadweight',
                },
                {
                    title: 'Port of Registry',
                    dataIndex: 'portofregistry',
                    key: 'portofregistry',
                },
                {
                    title: 'Date of Build',
                    dataIndex: 'dateofbuild',
                    key: 'dateofbuild',
                },
                {
                    title: 'Operator',
                    dataIndex: 'operator',
                    key: 'operator',
                },
                {
                    title: 'Operator Country of Registration',
                    dataIndex: 'operatorcountryofregistration',
                    key: 'operatorcountryofregistration',
                },
                {
                    title: 'Registere Downer',
                    dataIndex: 'registeredowner',
                    key: 'registeredowner',
                },
                {
                    title: 'Speed',
                    dataIndex: 'speed',
                    key: 'speed',
                },
                {
                    title: 'Technical Manager',
                    dataIndex: 'technicalmanager',
                    key: 'technicalmanager',
                },
                {
                    title: 'Ship Builder',
                    dataIndex: 'shipbuilder',
                    key: 'shipbuilder',
                },
                {
                    title: 'Ship type Group',
                    dataIndex: 'shiptypegroup',
                    key: 'shiptypegroup',
                },
                {
                    title: 'Year of Build',
                    dataIndex: 'yearofbuild',
                    key: 'yearofbuild',
                },
                {
                    title: 'Action',
                    key: 'operation',
                    fixed: 'right',
                    width: 100,
                    render: (data) => {
                        return (
                            <div className="editable-row-operations">
                                <span className="iconWrapper" onClick={(e) => this.setState({ id: data.maritimemobileserviceidentitymmsinumber }, () => this.setState({ visibleviewmodal: true }))}>
                                <EyeOutlined />
                                </span>
                            </div>
                        )
                    }
                },

            ],
            pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
            data: [],
            loading: false,
            visibleviewmodal: false,
            searchVisible: false,
            sidebarVisible: false,
            searchData: []
        }
    }

    componentDidMount = async () => {
        this.getTableData();
    }
    onChange = async (data) => {
        if (data && data.length >= 3) {
            try {
                const response = await fetch(`${process.env.REACT_APP_URL_NEW}VesselDetail/find/${data}`, { mode: 'cors' });
                const respdata = await response.json();
                if (respdata && respdata.length < 1) {
                    this.setState({
                        searchVisible: true,
                        searchData: []
                    })
                }
                else {
                    this.setState({
                        searchVisible: true,
                        searchData: [respdata[0]]
                    })
                }
            }
            catch (e) {
                // console.log("e")
                this.setState({
                    searchVisible: false,
                    searchData: []
                })
            }
        }
        else {
            this.setState({
                searchVisible: false,
                searchData: []
            })
        }

    }

    getTableData = async (search = {}) => {
        const { pageOptions } = this.state;
        let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
        let headers = { order_by: { id: 'desc' } };
        if (
            search &&
            search.hasOwnProperty('searchValue') &&
            search.hasOwnProperty('searchOptions') &&
            search['searchOptions'] !== '' &&
            search['searchValue'] !== ''
        ) {
            let wc = {};
            if (search['searchOptions'].indexOf(';') > 0) {
                let so = search['searchOptions'].split(';');
                wc = { OR: {} };
                so.map((e) => (wc['OR'][e] = { l: search['searchValue'] }));
            } else {
                wc[search['searchOptions']] = { l: search['searchValue'] };
            }

            headers['where'] = wc;
        }
        // console.log(search);
        this.setState({
            ...this.state,
            loading: true,
            data: [],
        });
        let qParamString = objectToQueryStringFunc(qParams);
        let _url = `${process.env.REACT_APP_URL_NEW}VesselDetail/list?${qParamString}`;
        const response = await getAPICall(_url);
        const data = await response;
        let state = { loading: false };
        state['data'] = data && data.vessels ? data.vessels : []
        let totalRows = data.meta.totCount
        this.setState({
            ...this.state,
            ...state,
            pageOptions: {
                pageIndex: pageOptions.pageIndex,
                pageLimit: pageOptions.pageLimit,
                totalRows: totalRows,
            },
            loading: false,
        });
    };

    onActionDonwload = (downType, pageType) => {
        // let params = `t=${pageType}`, cols = [];
        // const { columns } = this.state;
        // const { pageOptions } = this.state;
    
        // let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
        // columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
        // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
      }

    callOptions = (evt) => {
        if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
            let pageOptions = this.state.pageOptions;
            let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
            pageOptions['pageIndex'] = 1;
            this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
                this.onChange(search.searchValue);
            });
        } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
            let pageOptions = this.state.pageOptions;
            pageOptions['pageIndex'] = 1;
            this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
                this.getTableData();
            });
        } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
            // column filtering show/hide
            let data = this.state.data;
            let columns = Object.assign([], this.state.columns);

            if (data.length > 0) {
                for (var k in data[0]) {
                    let index = columns.some(
                        (item) =>
                            (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
                            (item.hasOwnProperty('key') && item.key === k)
                    );
                    if (!index) {
                        let title = k
                            .split('_')
                            .map((snip) => {
                                return snip[0].toUpperCase() + snip.substring(1);
                            })
                            .join(' ');
                        let col = Object.assign(
                            {},
                            {
                                title: title,
                                dataIndex: k,
                                key: k,
                                invisible: 'true',
                                isReset: true,
                            }
                        );
                        columns.splice(columns.length - 1, 0, col);
                    }
                }
            }
            this.setState({
                ...this.state,
                sidebarVisible: evt.hasOwnProperty('sidebarVisible')
                    ? evt.sidebarVisible
                    : !this.state.sidebarVisible,
                columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
            });
        } else {
            let pageOptions = this.state.pageOptions;
            pageOptions[evt['actionName']] = evt['actionVal'];

            if (evt['actionName'] === 'pageLimit') {
                pageOptions['pageIndex'] = 1;
            }

            this.setState({ ...this.state, pageOptions: pageOptions }, () => {
                this.getTableData();
            });
        }
    };

    //resizing function
    handleResize = index => (e, { size }) => {
        this.setState(({ columns }) => {
            const nextColumns = [...columns];
            nextColumns[index] = {
                ...nextColumns[index],
                width: size.width,
            };
            return { columns: nextColumns };
        });
    };

    render() {
        const {
            columns,
            loading,
            data,
            pageOptions,
            filter,
            search,
            visibleviewmodal,
            searchData,
            searchVisible,
            sidebarVisible
        } = this.state;
        const tableColumns = columns
            .filter((col) => (col && col.invisible !== "true") ? true : false)
            .map((col, index) => ({
                ...col,
                onHeaderCell: column => ({
                    width: column.width,
                    onResize: this.handleResize(index),
                }),
            }));
        return (
            <div className="body-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Input onChange={(e) => this.onChange(e.target.value)} placeholder='search' style={{ width: '150px', float: 'right' }} />

                            <div
                                className="section"
                                style={{
                                    width: '100%',
                                    marginBottom: '10px',
                                    paddingLeft: '15px',
                                    paddingRight: '15px',
                                    marginTop: '50px'
                                }}
                            >
                                {loading === false ? (
                                    <ToolbarUI
                                        routeUrl={'vesseldata-list-toolbar'}
                                        optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }}
                                        callback={(e) => this.callOptions(e)}
                                        // filter={filter}
                                        dowloadOptions={[
                                            { title: 'CSV', event: () => this.onActionDonwload('csv', 'vessel') },
                                            { title: 'PDF', event: () => this.onActionDonwload('pdf', 'vessel') },
                                            { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'vessel') }
                                        ]}
                                    />
                                ) : undefined}
                            </div>
                            <div>
                                <Table
                                    className="inlineTable resizeableTable"
                                    columns={tableColumns}
                                    dataSource={searchVisible ? searchData : data}
                                    pagination={false}
                                    loading={loading}
                                    size="small"
                                    bordered
                                    scroll={{ x: 'max-content' }}
                                    rowClassName={(r, i) =>
                                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                                    }
                                />
                            </div>

                            {


                                visibleviewmodal ?
                                    <Modal
                                        style={{ top: '2%' }}
                                        title={"Vessel Details"}
                                       open={visibleviewmodal}
                                        onCancel={() => this.setState({ visibleviewmodal: false })}
                                        width="95%"
                                        footer={null}
                                    >
                                        <VesselDetails id={this.state.id} />
                                    </Modal>
                                    : undefined
                            }
                        </div>
                    </div>
                </article>
                {
                    sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptions(e)} /> : null
                }
            </div>
        )
    }

}

export default VesselData
