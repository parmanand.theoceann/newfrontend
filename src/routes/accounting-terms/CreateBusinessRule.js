import React from "react";
import { Input, Select, Layout, Form, Button, Row, Col } from "antd";

const { Option } = Select;

const lederSubCategoryArr = [
  "Port Expense",
  "Cargo Expense",
  "Supplemental Contract Expense",
  "Rebillable Expense",
  "Surveys",
  "Tolls",
  "Other Voyage Expenses",
  "Recoverable Expense",
  "Advance Rebillable Expense",
  "Others",
];

const accountCodeInfo = [
  { code: "100001", main_head: "Demurrage", category: "Revenue" },
  { code: "200001", main_head: "TCO Hire", category: "Expenses" },
  { code: "300001", main_head: "Other Voyage Revenue", category: "Revenue" },
];

const countryInfo = [
  { id: "1", name: "India" },
  { id: "2", name: "USA" },
  { id: "3", name: "UAE" },
];

const lederMainCategoryArr = ["Operational", "Finance", "General"];
const lobArr = [
  "Bulker Division",
  "Container Division",
  "Oil & Gas",
  "Shipping",
  "Ship Management",
  "Tanker Division",
];

export default function CreateBusinessRule() {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values:", values);
  };

  const onValuesChange = (changedValues, allValues) => {
    // If Account Code changes, update Account Head Name and Category
    if (changedValues.account_code) {
      const selectedCodeInfo = accountCodeInfo.find(
        (item) => item.code === changedValues.account_code
      );

      if (selectedCodeInfo) {
        form.setFieldsValue({
          account_head_name: selectedCodeInfo.main_head,
          category: selectedCodeInfo.category,
        });
      }
    }
  };

  return (
    <div>
      <h4>Business Rules Cost/ Items</h4>

      <Form
        form={form}
        onFinish={onFinish}
        autoComplete="off"
        style={{ marginTop: "1rem" }}
        onValuesChange={onValuesChange}
      >
        <Row gutter={16}>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Account Description"
              name="account_description"
              rules={[
                {
                  // required: true,
                  // message: "Please input your name!",
                },
              ]}
              style={{ marginBottom: "1rem" }}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Source Code"
              name="source_code"
              rules={[
                {
                  // required: true,
                  // message: "Please input your email!",
                },
              ]}
              style={{ marginBottom: "1rem" }}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Ledger Sub Category"
              name="ledger_sub_category"
              rules={[
                {
                  message: "Select Ledger Sub Category",
                },
              ]}
              style={{ marginBottom: "1rem" }}
            >
              <Select>
                {lederSubCategoryArr.map((item) => (
                  <Option value={item} key={item}>
                    {item}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Account Code"
              name="account_code"
              rules={[{}]}
              style={{ marginBottom: "1rem" }}
            >
              <Select>
                {accountCodeInfo.map((item) => (
                  <Option value={item.code} key={item.code}>
                    {item.code} - {item.main_head} - {item.category}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Account Head Name"
              name="account_head_name"
              rules={[{}]}
              style={{ marginBottom: "1rem" }}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Category"
              name="category"
              rules={[{}]}
              style={{ marginBottom: "1rem" }}
            >
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Country of Account"
              name="country_account"
              rules={[{}]}
              style={{ marginBottom: "1rem" }}
            >
              <Select>
                {countryInfo.map((item) => (
                  <Option value={item.id} key={item.id}>
                    {item.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Ledger Main Category"
              name="ledger_main_category"
              rules={[
                {
                  message: "Select Ledger Main Category",
                },
              ]}
              style={{ marginBottom: "1rem" }}
            >
              <Select>
                {lederMainCategoryArr.map((item) => (
                  <Option value={item} key={item}>
                    {item}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="LOB"
              name="lob"
              rules={[
                {
                  message: "Select LOB",
                },
              ]}
              style={{ marginBottom: "1rem" }}
            >
              <Select>
                {lobArr.map((item) => (
                  <Option value={item} key={item}>
                    {item}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        {/* <Form.Item wrapperCol={{ offset: 8, span: 16 }} >
          <Button type="primary" htmlType="submit" style={{float:"right"}}>
            Submit
          </Button>
        </Form.Item> */}

        <Row gutter={16}>
          <Col xs={24} sm={24} md={24}>
          <Button type="primary" htmlType="submit" style={{float:"right"}}>Save</Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
