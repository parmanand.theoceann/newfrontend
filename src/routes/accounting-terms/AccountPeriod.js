import React, { useState } from 'react';
import { Table, DatePicker, Select, Button, Input } from 'antd';
import moment from 'moment';

const { Option } = Select;

const AccountPeriod = () => {
  const [dataSource, setDataSource] = useState([
    {
      key: '0',
      year: '1981',
      closingDate: '21-11-23',
      status: 'Partial',
      openMonth: '22-02-23',
      closeMonth: '25-02-23',
      myCompany: 'c1',
      AP: 'text1',
      AR: 'text2',
      intercompany: 'Y.',
      editing: false, // Initial editing state for the first row
    },
  ]);
  const [editingKey, setEditingKey] = useState('');

  const columns = [
    {
      title: 'Year',
      dataIndex: 'year',
      key: 'year',
      editable: true,
      render: (_, record) => {
        return isEditing(record) ? (
          <Input
            value={record.year}
            onChange={(e) => handleInputChange(e, 'year', record.key)}
            disabled={!record.editing}
          />
        ) : (
          record.year
        );
      },
    },
    {
      title: 'Closing Date',
      dataIndex: 'closingDate',
      key: 'closingDate',
      editable: true,
      render: (_, record) => {
        return (
          <DatePicker
            format="DD-MM-YY"
            value={moment(record.closingDate, 'DD-MM-YY')}
            onChange={(date) => handleDateChange(date, 'closingDate', record.key)}
            disabled={!record.editing}
          />
        );
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      editable: true,
      render: (_, record) => (
        <Select
          value={record.status}
          onChange={(value) => handleSelectChange(value, 'status', record.key)}
          disabled={!record.editing}
        >
          <Option value="Partial">Partial</Option>
          <Option value="Complete">Complete</Option>
        </Select>
      ),
    },
    {
      title: 'Open Month',
      dataIndex: 'openMonth',
      key: 'openMonth',
      editable: true,
      render: (_, record) => {
        return (
          <DatePicker
            format="DD-MM-YY"
            value={moment(record.openMonth, 'DD-MM-YY')}
            onChange={(date) => handleDateChange(date, 'openMonth', record.key)}
            disabled={!record.editing}
          />
        );
      },
    },
    {
      title: 'Close Month',
      dataIndex: 'closeMonth',
      key: 'closeMonth',
      editable: true,
      render: (_, record) => {
        return (
          <DatePicker
            format="DD-MM-YY"
            value={moment(record.closeMonth, 'DD-MM-YY')}
            onChange={(date) => handleDateChange(date, 'closeMonth', record.key)}
            disabled={!record.editing}
          />
        );
      },
    },
    {
      title: 'My Company',
      dataIndex: 'myCompany',
      key: 'myCompany',
      editable: true,
      render: (_, record) => (
        <Select
          value={record.myCompany}
          onChange={(value) => handleSelectChange(value, 'myCompany', record.key)}
          disabled={!record.editing}
        >
          <Option value="c1">Company 1</Option>
          <Option value="c2">Company 2</Option>
        </Select>
      ),
    },
    {
      title: 'AP',
      dataIndex: 'AP',
      key: 'AP',
      editable: true,
      render: (_, record) => {
        return isEditing(record) ? (
          <Input
            value={record.AP}
            onChange={(e) => handleInputChange(e, 'AP', record.key)}
            disabled={!record.editing}
          />
        ) : (
          record.AP
        );
      },
    },
    {
      title: 'AR',
      dataIndex: 'AR',
      key: 'AR',
      editable: true,
      render: (_, record) => {
        return isEditing(record) ? (
          <Input
            value={record.AR}
            onChange={(e) => handleInputChange(e, 'AR', record.key)}
            disabled={!record.editing}
          />
        ) : (
          record.AR
        );
      },
    },
    {
      title: 'Intercompany',
      dataIndex: 'intercompany',
      key: 'intercompany',
      editable: true,
      render: (_, record) => (
        <Select
          value={record.intercompany}
          onChange={(value) => handleSelectChange(value, 'intercompany', record.key)}
          disabled={!record.editing}
        >
          <Option value="Y.">Yes</Option>
          <Option value="N.">No</Option>
        </Select>
      ),
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Button onClick={() => save(record.key)} type="link">
              Save
            </Button>
            <Button onClick={cancel} type="link">
              Cancel
            </Button>
          </span>
        ) : (
          <Button disabled={editingKey !== ''} onClick={() => edit(record)} type="link">
            Edit
          </Button>
        );
      },
    },
  ];

  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    setEditingKey(record.key);
    setDataSource((prevData) =>
      prevData.map((item) => ({
        ...item,
        editing: item.key === record.key, // Enable editing for the selected row
      }))
    );
  };

  const cancel = () => {
    setEditingKey('');
    setDataSource((prevData) =>
      prevData.map((item) => {
        if (item.key === editingKey) {
          // Revert changes for the currently editing row
          return { ...item, editing: false };
        }
        return item;
      })
    );
  };
  

  const save = (key) => {
    // Save the edited record
    setEditingKey('');
    setDataSource((prevData) =>
      prevData.map((item) => ({
        ...item,
        editing: false, // Disable editing for all rows after saving
      }))
    );
  };

  const handleDateChange = (date, dataIndex, key) => {
    // Handle date changes
    const newData = [...dataSource];
    const target = newData.find((item) => key === item.key);
    if (target) {
      target[dataIndex] = date ? date.format('DD-MM-YY') : '';
      setDataSource(newData);
    }
  };

  const handleInputChange = (e, dataIndex, key) => {
    // Handle input changes
    const newData = [...dataSource];
    const target = newData.find((item) => key === item.key);
    if (target) {
      target[dataIndex] = e.target.value;
      setDataSource(newData);
    }
  };

  const handleSelectChange = (value, dataIndex, key) => {
    // Handle select changes
    const newData = [...dataSource];
    const target = newData.find((item) => key === item.key);
    if (target) {
      target[dataIndex] = value;
      setDataSource(newData);
    }
  };

  const handleAddRow = () => {
    // Add a new row
    const newData = {
      key: (dataSource.length + 1).toString(),
      year: '',
      closingDate: '',
      status: '',
      openMonth: '',
      closeMonth: '',
      myCompany: '',
      AP: '',
      AR: '',
      intercompany: '',
      editing: true, // Enable editing for the new row
    };
    setDataSource([...dataSource, newData]);
    setEditingKey(newData.key);
  };

  return (
    <div>
      <Button onClick={handleAddRow} type="primary" style={{ marginBottom: 16 }}>
        Add Row
      </Button>
      <Table
        bordered
        dataSource={dataSource}
        columns={columns}
        rowClassName="editable-row"
        pagination={false}
      />
    </div>
  );
};

export default AccountPeriod;
