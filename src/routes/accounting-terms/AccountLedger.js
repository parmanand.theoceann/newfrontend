import React, { useEffect, useState } from "react";
import { Table, Checkbox, Input, Button, Card, Modal, Layout } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const dataSource = Array.from({ length: 10 }, (_, index) => ({
  key: index.toString(),
  checkbox1: false,
  checkbox2: false,
  checkbox3: false,
  checkbox4: false,
  checkbox5: false,
  text1: `Text 1 - Row ${index + 1}`,
  text2: `Text 2 - Row ${index + 1}`,
  text3: `Text 3 - Row ${index + 1}`,
}));

const cellPaddingStyle = { paddingLeft: "10px" };
const { Content } = Layout;

const AccountLedger = () => {
  const [data, setData] = useState(dataSource);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [rowToDeleteKey, setRowToDeleteKey] = useState(null);

  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [editedRow, setEditedRow] = useState(null);
  const [enabledCheckboxes, setEnabledCheckboxes] = useState({});
  const [isNewlyAdded, setIsNewlyAdded] = useState(false);

  const handleDeleteModalConfirm = () => {
    const newData = data.filter((item) => item.key !== rowToDeleteKey);
    setData(newData);
    setIsDeleteModalVisible(false);
  };

  useEffect(() => {
    handleDeleteModalConfirm();
  }, [rowToDeleteKey]);

  const handleEdit = (record) => {
    setIsEditModalVisible(true);
    setEditedRow(record);
    setEnabledCheckboxes({ ...enabledCheckboxes, [record.key]: true });
  };

  const handleDelete = (key) => {
    Modal.confirm({
      title: "Are you sure want to delete this item?",
      content: "This action cannot be undone.",
      onOk: () => {
        setRowToDeleteKey(key);
      },
      onCancel: () => {
        setIsDeleteModalVisible(false);
      },
    });
  };

  const handleCheckboxChange = (record, columnName) => {
    const updatedData = data.map((item) =>
      item.key === record.key
        ? { ...item, [columnName]: !item[columnName] }
        : item
    );
    setData(updatedData);
  };

  const handleInputChange = (e, record, columnName) => {
    const updatedData = data.map((item) =>
      item.key === record.key ? { ...item, [columnName]: e.target.value } : item
    );
    setData(updatedData);
  };

  const handleAddRow = () => {
    const newRow = {
      key: (data.length + 1).toString(),
      checkbox1: false,
      checkbox2: false,
      checkbox3: false,
      checkbox4: false,
      checkbox5: false,
      text1: "",
      text2: "",
      text3: "",
    };
    setData([...data, newRow]);
    setIsEditModalVisible(true);
    setEditedRow(newRow);
    setEnabledCheckboxes({ ...enabledCheckboxes, [newRow.key]: true });
    setIsNewlyAdded(true);
  };

  const handleSave = () => {
    setIsEditModalVisible(false);
    setEnabledCheckboxes({ ...enabledCheckboxes, [editedRow.key]: false });
    setIsNewlyAdded(false);
    setEditedRow(null);
  };

  const handleCancelEdit = () => {
    setIsEditModalVisible(false);
    setEnabledCheckboxes({ ...enabledCheckboxes, [editedRow.key]: false });

    if (editedRow && enabledCheckboxes[editedRow.key] && isNewlyAdded) {
      const newData = data.filter((item) => item.key !== editedRow.key);
      setData(newData);
    } else if (editedRow && enabledCheckboxes[editedRow.key]) {
      const newData = data.map((item) =>
        item.key === editedRow.key ? { ...item, ...editedRow } : item
      );
      setData(newData);
    }

    setEditedRow(null);

    if (isNewlyAdded) {
      setIsNewlyAdded(false);
    }
  };

  const columns = [
    {
      title: "Vessel",
      dataIndex: "checkbox1",
      render: (_, record) => (
        <Checkbox
          checked={record.checkbox1}
          onChange={() => handleCheckboxChange(record, "checkbox1")}
          style={cellPaddingStyle}
          disabled={!enabledCheckboxes[record.key]}
        />
      ),
    },
    {
      title: "Port",
      dataIndex: "checkbox2",
      render: (_, record) => (
        <Checkbox
          checked={record.checkbox2}
          onChange={() => handleCheckboxChange(record, "checkbox2")}
          style={cellPaddingStyle}
          disabled={!enabledCheckboxes[record.key]}
        />
      ),
    },
    {
      title: "Voyage Items",
      dataIndex: "checkbox3",
      render: (_, record) => (
        <Checkbox
          checked={record.checkbox3}
          onChange={() => handleCheckboxChange(record, "checkbox3")}
          style={cellPaddingStyle}
          disabled={!enabledCheckboxes[record.key]}
        />
      ),
    },
    {
      title: "Bank",
      dataIndex: "checkbox4",
      render: (_, record) => (
        <Checkbox
          checked={record.checkbox4}
          onChange={() => handleCheckboxChange(record, "checkbox4")}
          style={cellPaddingStyle}
          disabled={!enabledCheckboxes[record.key]}
        />
      ),
    },
    {
      title: "Inter com",
      dataIndex: "checkbox5",
      render: (_, record) => (
        <Checkbox
          checked={record.checkbox5}
          onChange={() => handleCheckboxChange(record, "checkbox5")}
          style={cellPaddingStyle}
          disabled={!enabledCheckboxes[record.key]}
        />
      ),
    },
    {
      title: "Account Main Head",
      dataIndex: "text1",
      render: (_, record) => (
        <div style={cellPaddingStyle}>
          {editedRow && editedRow.key === record.key ? (
            <Input
              value={record.text1}
              onChange={(e) => handleInputChange(e, record, "text1")}
            />
          ) : (
            record.text1
          )}
        </div>
      ),
    },
    {
      title: "Category",
      dataIndex: "text2",
      render: (_, record) => (
        <div style={cellPaddingStyle}>
          {editedRow && editedRow.key === record.key ? (
            <Input
              value={record.text2}
              onChange={(e) => handleInputChange(e, record, "text2")}
            />
          ) : (
            record.text2
          )}
        </div>
      ),
    },
    {
      title: "Accont Code",
      dataIndex: "text3",
      render: (_, record) => (
        <div style={cellPaddingStyle}>
          {editedRow && editedRow.key === record.key ? (
            <Input
              value={record.text3}
              onChange={(e) => handleInputChange(e, record, "text3")}
            />
          ) : (
            record.text3
          )}
        </div>
      ),
    },
    {
      title: "Action",
      dataIndex: "actions",
      render: (_, record) => (
        <div className="editable-row-operations" style={{ padding: "0.7rem" }}>
          {editedRow && editedRow.key === record.key ? (
            <>
              <Button type="primary" onClick={handleSave}>
                Save
              </Button>
              <Button onClick={handleCancelEdit}>Cancel</Button>
            </>
          ) : (
            <>
              <span className="iconWrapper" onClick={() => handleEdit(record)}>
                <EditOutlined />
              </span>
              <span
                className="iconWrapper cancel"
                onClick={() => handleDelete(record.key)}
              >
                <DeleteOutlined />
              </span>
            </>
          )}
        </div>
      ),
    },
  ];

  return (
    <div className="wrap-rightbar full-wraps testing">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <article className="article">
              <div className="box box-default">
                <div className="box-body">

                  <>
                    <div>
                      <>
                        <Button>Report</Button>
                      </>

                      <Table
                        dataSource={data}
                        columns={columns}
                        bordered={true}
                        pagination={false}
                      />
                    </div>
                    <Button onClick={handleAddRow}>Add Row</Button>
                  </>

                </div>
              </div>
            </article>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default AccountLedger;
