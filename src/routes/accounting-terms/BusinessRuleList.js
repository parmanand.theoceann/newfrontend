import React, { useEffect, useState } from "react";
import { Col, Form, Input, InputNumber, Popconfirm, Row, Table, Typography } from "antd";
import { Button, Modal, Layout, Select } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import CreateBusinessRule from "./CreateBusinessRule";

const { Content } = Layout;
const { Option } = Select;

const cellPaddingStyle = { paddingLeft: "10px !important"};

const originData = [
  {
    key: "0",
    account_description: "desc test",
    source_code: "0001",
    lob: "Bulker Division",
    account_code: "100001",
    account_head_name: "Demurrage",
    category: "Revenue",
    ledger_type: "Operational",
  },

  {
    key: "1",
    account_description: "desc test2",
    source_code: "0002",
    lob: "Container Division",
    account_code: "200001",
    account_head_name: "TCO Hire",
    category: "Expenses",
    ledger_type: "Finance",
  },
];

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === "number" ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>

      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};
const BusinessRuleList = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState("");
  const [selectedColumn, setSelectedColumn] = useState(null);
  const [filterValue, setFilterValue] = useState("");
  const [isNewModalVisible, setIsNewModalVisible] = useState(false);

  const isEditing = (record) => record.key === editingKey;
  const edit = (record) => {
    form.setFieldsValue({
      account_description: "",
      source_code: "",
      lob: "",
      account_code: "",
      account_head_name: "",
      category: "",
      ledger_type: "",
      ...record,
    });
    setEditingKey(record.key);
  };
  const cancel = () => {
    setEditingKey("");
  };

  const handleColumnChange = (value) => {
    if (value === 'all') {
      // Reset the filter when "All" is selected
      handleResetFilter();
    } else {
      setSelectedColumn(value);
    }
  };

  const handleFilterValueChange = (e) => {
    setFilterValue(e.target.value);
  };
  const filteredData = data.filter((record) => {
    if (!selectedColumn) return true;

    const columnValue =
      record[selectedColumn] && record[selectedColumn].toString().toLowerCase();
    return columnValue.includes(filterValue.toLowerCase());
  });

  const handleResetFilter = () => {
    setSelectedColumn(null);
    setFilterValue("");
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingKey("");
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const handleDelete = (key) => {
    const newData = [...data];
    const index = newData.findIndex((item) => key === item.key);
    newData.splice(index, 1);
    setData(newData);
  };

  const handleNewBusinessRule = () => {
    setIsNewModalVisible(true);
  };
  const handleCancelNewRule = () => {
    setIsNewModalVisible(false);
  };

  const columns = [
    {
      title: "Account Description",
      dataIndex: "account_description",
      // width: '25%',
      editable: true,
    },
    {
      title: "Source Code",
      dataIndex: "source_code",
      // width: '15%',
      editable: true,
    },
    {
      title: "LOB",
      dataIndex: "lob",
      // width: '15%',
      editable: true,
    },
    {
      title: "Account Code",
      dataIndex: "account_code",
      // width: '15%',
      editable: true,
    },
    {
      title: "Account Head Name",
      dataIndex: "account_head_name",
      // width: '20%',
      editable: true,
    },
    {
      title: "Category",
      dataIndex: "category",
      // width: '20%',
      editable: true,
    },
    {
      title: "Ledger Type",
      dataIndex: "ledger_type",
      // width: '20%',
      editable: true,
    },
    {
      title: "Action",
      dataIndex: "operation",
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <div className="editable-row-operations" style={{ padding: "0.7rem" }}>
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span></div>
        ) : (
          <div className="editable-row-operations" style={{ padding: "0.7rem" }}>
          <span>
            <Typography.Link
              disabled={editingKey !== ""}
              onClick={() => edit(record)}
              style={{ marginRight: 8 }}
            >
             <span className="iconWrapper"><EditOutlined /></span> 
            </Typography.Link>
            <Popconfirm
              title="Sure to delete?"
              onConfirm={() => handleDelete(record.key)}
            >
              {/* <a>Delete</a> */}
              <span className="iconWrapper cancel"> <DeleteOutlined /></span>
            </Popconfirm>
          </span></div>
        );
      },
    },
  ];
  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
        //className:{cellPaddingStyle}
      }),
    };
  });
  return (
    <div className="wrap-rightbar full-wraps testing">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <div style={{ marginBottom: "0.5rem", float: "right" }}>
                      <Button onClick={handleNewBusinessRule}>
                        Create New Business Rule
                      </Button>
                    </div>

                  <Row gutter={16} style={{marginBottom:"0.6rem"}}> <Col>
                    <Select
                      style={{ width: 168 }}
                      onChange={handleColumnChange}
                      placeholder="Select Column"
                      value={selectedColumn}
                      name="columnName"
                    >
                      <Option key="all" value="all">
                        All
                      </Option>
                      {data.length > 0 &&
                        columns
                          .filter((col) => col.dataIndex !== "operation")
                          .map((col) => (
                            <Option key={col.dataIndex} value={col.dataIndex}>
                              {col.title}
                            </Option>
                          ))}
                    </Select></Col>
                    <Col>
                    <Input
                      placeholder="Enter Value for filter"
                      value={filterValue}
                      onChange={handleFilterValueChange}
                    /></Col>
                    {/* <Button onClick={handleResetFilter}>Reset Filter</Button> */}
                  </Row>

                  <Form form={form} component={false}>
                    <Table
                      components={{
                        body: {
                          cell: EditableCell,
                        },
                      }}
                      bordered
                      dataSource={filteredData}
                      columns={mergedColumns}
                      rowClassName="editable-row"
                      pagination={false}
                    />
                  </Form>
                </div>
              </div>
            </article>
          </Content>
        </Layout>
      </Layout>

      {/* Modal for creating new business rule */}
      <Modal
        title="Create New Business Rule"
        open={isNewModalVisible}
        onCancel={handleCancelNewRule}
        width="max-content"
        style={{ top: 0, left: 0, marginTop: "1rem" }}
        footer={null}
      >
        <CreateBusinessRule />
      </Modal>
    </div>
  );
};
export default BusinessRuleList;
//2
