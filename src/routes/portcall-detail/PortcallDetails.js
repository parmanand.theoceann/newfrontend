import React, { Component } from 'react';
import { Form, Input, Button, Tabs, Upload, TimePicker, Select, Modal } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import PortInformation from '../port-information';
import PortActivityList from '../port-expense/port-activity-list';
import PortExpense from '../port-expense';
import AgencyAppointment from '../operation/my-portcalls/agency-appointment/components/AgencyAppointment';
import AgencyAppointmentReport from '../../shared/components/All-Print-Reports/AgencyAppointmentReport';
import AddAddressBook from '../../components/AddAddressBook';
import URL_WITH_VERSION, {
  getAPICall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  postAPICall,
  apiDeleteCall,
} from '../../shared';
import moment from 'moment';

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};


class PortcallDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      portID:  this.props.match.params.id || this.props.selectedPortId,
      portexpID: this.props.match.params.id || this.props.selectedPortId,
      responseData: [],
      editportexpData: null,
      addresseditVisible: false,
      formDataValuesAddress: {},
      isAgentView: this.props.isAgentView || false,
      show_port_call_for_user:[],
      tde_list: null
     };
  }

  componentDidMount = async () => {
      this.showPortDetails()
      this.tdeList()
     if(this.props.match && this.props.match.url){
       if((this.props.match.url == `/edit-portcall-details/${this.state.portID}`) || (this.props.match.url == `/agent-list`)){
        
        this.setState({ ...this.state, loading: true }, () => this.getportExpenseData());
        }else{
          this.setState({ ...this.state, loading: true }, () => this.getPortData());
        }
     }
    this.setState({ ...this.state, loading: true }, () => this.getPortData());

  };

  tdeList = async()=>{
    const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
    const responseTdeData = response['data']&&response['data'].length>0?response['data']:[];
    this.setState({ tde_list : responseTdeData})
  }


  showPortDetails = async () => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/list?us=1`);
    const data = await response['data'];
    this.setState({ show_port_call_for_user : data})
   }

  getportExpenseData = async () => {
    const {portID, portexpID} = this.state;
    const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portexpID}`)
    //const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/peedit?e=${portexpID}`);
    const editData = await Editreponse['data'];

    console.log(editData)
    if(editData){
      this.setState({
        ...this.state,
        editportexpData: editData,
      }, () => this.setState({ ...this.state, loading: false }));
    }else{
      this.setState({ ...this.state, loading: true }, () => this.getPortData());
    }

   }

   getPortData = async () => {
    const {portID, portexpID} = this.state;
    if(this.props.match && (this.props.match.path == "/edit-portcall-details/:id") || (this.props.match.path == "/agent-list")){   
     const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portexpID}`)
     //const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/peedit?e=${portexpID}`);
     const editData = await Editreponse['data'];
     if(editData){
      this.setState({
        ...this.state,
        editportexpData: editData,
      }, () => this.setState({ ...this.state, loading: false }));
     }
     
     else{
      const response = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portID}`)
      const data = await response['data'];

      
      this.setState({
        ...this.state,
        responseData: data,
      }, () => this.setState({ ...this.state, loading: false }));
     }
     
    }else{
     const response = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portID}`)
     const data = await response['data'];
     console.log(data)
      this.setState({
       ...this.state,
       responseData: data,
     }, () => this.setState({ ...this.state, loading: false }));
     }

     

   }
   
  

  edagenrowSelection = async (e, id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${id}`);
    const respData = await response;
    this.setState({ ...this.state, formDataValuesAddress: respData['data'] }, () => {
      this.setState({ ...this.state, addresseditVisible: true })
    });
  }

  edagenrowselectionCancel = () => {
    this.setState({
      ...this.state,
      addresseditVisible: false,
    })
  }


  _onPressBack = () => {
    const { history } = this.props;
    const { responseData } = this.state;
    history.push("/operation/my-portcall/" + responseData['port_call'].voyage_manager_id)
  }

  submitFormData = async(port_call_id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pc`);
    if (response && response.data) {
        openNotificationWithIcon('success', response.message);
          // setInterval(() => {
          //   window.location.reload(false);
          // }, 2000);
      } else {
        openNotificationWithIcon('error', response.message);
      }
  }

  reSubmitForm = async(port_call_id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/resubmit?e=${port_call_id}`);
     if (response && response.data) {
      openNotificationWithIcon('success', response.message);
        // setInterval(() => {
        //   window.location.reload(false);
        // }, 2000);
    } else {
      openNotificationWithIcon('error', response.message);
    }
  }

  render() {
    const { responseData, isAgentView, loading, formDataValuesAddress, addresseditVisible, portexpID, editportexpData, show_port_call_for_user, tde_list } = this.state

    return (
      <>
        {loading === false &&
          responseData['port_call'] && responseData['port_call'] &&
          responseData['port_call'].hasOwnProperty('portcalldetails') &&
          responseData['port_call'].hasOwnProperty('agentdetails') && responseData['port_call'].hasOwnProperty('cargodetails') ?
          <div className="body-wrapper modalWrapper">
            <article className="article toolbaruiWrapper">
              <div className="box box-default">
                <div className="box-body">
                  <div className="form-wrapper">
                    <div className="row p12">
                      <div className="col-md-2">
                        <Button type="link" onClick={() => this._onPressBack()} className="btn-route">
                      <LeftOutlined />
                        </Button>
                      </div>
                      <div className="col-md-10">
                        <div className="form-heading">
                          <h4 className="title">
                            <span>Portcall Details</span>
                          </h4>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Form>
                    <div className="row p10">
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Portcall Ref">
                          <Input size="default" defaultValue={responseData['port_call'].port_call_ref_id} disabled />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Port">
                          <Input size="default" defaultValue={responseData['port_call'].portcalldetails.port} disabled />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem label="Activity" {...formItemLayout}>
                          <Select
                            mode="multiple"
                            defaultValue={responseData['port_call'].cargodetails.activity}
                            name="activity"
                            disabled
                          >
                          </Select>
                        </FormItem>
                        {/* <FormItem {...formItemLayout} label="Activity">
                               <Input size="defaulvessel_idt" defaultValue="" />
                             </FormItem> */}
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="ETA">
                          <Input size="default" defaultValue={moment(responseData['port_call'].portcalldetails.eta).format('YYYY-MM-DD')} disabled />
                        </FormItem>
                      </div>
                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Status">
                          <Input size="default" defaultValue="Sent" />
                        </FormItem>
                      </div>

                      <div className="col-md-4">
                        <FormItem {...formItemLayout} label="Agent Name" onClick={e => this.edagenrowSelection(e, responseData['port_call']['id'])}>
                          {responseData['port_call'].agentdetails &&  responseData['port_call'].agentdetails[0] && <Input size="default" defaultValue={ responseData['port_call'].agentdetails[0]['agent_name']} disabled />}  
                          {/* {responseData.agentdetails.map((e) => <Input size="default" defaultValue={e.agent_name} disabled />)} */}
                        </FormItem>
                      </div>

                    </div>
                  </Form>
                  
                  <div className="row p10">
                    <div className="col-md-12">
                      <article className="article">
                        <div className="box box-default">
                          <div className="box-body">
                             <Tabs defaultActiveKey="agencyappointment" size="small">
                              <TabPane className="pt-3" tab="Agency Appointment" key="agencyappointment" >
                                <AgencyAppointmentReport responseData={responseData['port_call']} />
                              </TabPane>
                              <TabPane className="pt-3" tab="Port Expense" key="portexpense">
                               
                               
                                
                                
                                <PortExpense isUserView={show_port_call_for_user} isAgentView={isAgentView} tdeList={tde_list} editportexpData={responseData} {...this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm}/>
                              </TabPane>
                              <TabPane className="pt-3" tab="Port Activity" key="portectivity">
                                <PortActivityList isUserView={show_port_call_for_user} editportexpData={responseData} tdeList={tde_list} {...this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm}/>
                              </TabPane>
                              <TabPane className="pt-3" tab="Port Information" key="portinformation">
                                <PortInformation isUserView={show_port_call_for_user} editportexpData={responseData} tdeList={tde_list} {...this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm} />
                              </TabPane>
                            </Tabs> 
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>


                </div>
              </div>
            </article>
          </div>
          : ""

        }
        { loading  === false  && editportexpData && editportexpData ?
                  <div className="body-wrapper modalWrapper">
                      <article className="article toolbaruiWrapper">
                        <div className="box box-default">
                          <div className="box-body">
                          <Form>
                            <div className="row p10">
                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Portcall Ref">
                                  <Input size="default" defaultValue={editportexpData['port_call'].port_call_ref_id} disabled />
                                </FormItem>
                              </div>
                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Port">
                                  <Input size="default" defaultValue={editportexpData['port_call'].portcalldetails && editportexpData['port_call'].portcalldetails.port} disabled />
                                </FormItem>
                              </div>
                              <div className="col-md-4">
                                <FormItem label="Activity" {...formItemLayout}>
                                  <Select
                                    mode="multiple"
                                    defaultValue={editportexpData['port_call'].cargodetails && editportexpData['port_call'].cargodetails.activity}
                                    name="activity"
                                    disabled
                                  >
                                  </Select>
                                </FormItem>
                                {/* <FormItem {...formItemLayout} label="Activity">
                                      <Input size="defaulvessel_idt" defaultValue="" />
                                    </FormItem> */}
                              </div>
                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="ETA">
                                  <Input size="default" defaultValue={moment(editportexpData['port_call'].portcalldetails.eta).format('YYYY-MM-DD')} disabled />
                                </FormItem>
                              </div>
                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Status">
                                  <Input size="default" defaultValue="Sent" />
                                </FormItem>
                              </div>

                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Agent Name" onClick={e => this.edagenrowSelection(e, editportexpData['port_call']['id'])}>
                                  {editportexpData['port_call'].agentdetails &&  editportexpData['port_call'].agentdetails[0] && <Input size="default" defaultValue={ editportexpData['port_call'].agentdetails[0]['agent_name']} disabled />}  
                                  {/* {responseData.agentdetails.map((e) => <Input size="default" defaultValue={e.agent_name} disabled />)} */}
                                </FormItem>
                              </div>

                            </div>
                          </Form>
                            <div className="row p10">
                              <div className="col-md-12">
                                <article className="article">
                                  <div className="box box-default">
                                    <div className="box-body">
                                      <Tabs defaultActiveKey="agencyappointment" size="small">
                                        <TabPane className="pt-3" tab="Agency Appointment" key="agencyappointment">
                                           <AgencyAppointmentReport responseData = {editportexpData['port_call']}/>
                                        </TabPane>
                                        
                                        <TabPane className="pt-3" tab="Port Expense" key="portexpense">
                                           <PortExpense isAgentView={isAgentView} tdeList={tde_list} isUserView={show_port_call_for_user} editportexpData={editportexpData}  props={this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm}/>
                                        </TabPane>
                                        <TabPane className="pt-3" tab="Port Activity" key="portectivity">
                                          <PortActivityList isAgentView={isAgentView} tdeList={tde_list} isUserView={show_port_call_for_user} editportexpData={editportexpData} props={this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm}/>
                                        </TabPane>
                                        <TabPane className="pt-3" tab="Port Information" key="portinformation">
                                          <PortInformation isAgentView={isAgentView} tdeList={tde_list} isUserView={show_port_call_for_user} editportexpData={editportexpData} props={this.props} submitForm={this.submitFormData} reSubmitForm={this.reSubmitForm}/>
                                        </TabPane>
                                      </Tabs>
                                    </div>
                                  </div>
                                </article>
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
                    </div> 
                    : ""

            }
        {
          addresseditVisible === true ?
            <Modal
              title={`Edit Address List`}
             open={addresseditVisible}
              width={'90%'}
              onCancel={this.edagenrowselectionCancel}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
            >
              <AddAddressBook formDataValues={formDataValuesAddress} modalCloseEvent={this.onCancel} />
            </Modal>
            : undefined
        }
      </>


    );
  }
}

export default PortcallDetails;
