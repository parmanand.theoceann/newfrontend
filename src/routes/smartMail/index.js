import React, { useState } from "react";
import {  Input, Button } from "antd";
import { SearchOutlined, StarOutlined, StarFilled } from "@ant-design/icons";
import './smartmail.css';






const EmailTable = () => {






  const [isFilled, setIsFilled] = useState(false);
  const handleStarClick = () => {
    setIsFilled(!isFilled);
  }
  return (
    <>

      <div className="form-wrapper" style={{ margin: "2% 0% 0% 1%", }} >
        <div className="form-heading" style={{
          display: "flex",
          gap: "24px",

        }}
        >
          <Input
            type="search"
            placeholder="Search emails"
            prefix={<SearchOutlined style={{ color: "#e0e0e0" }} />}
            style={{
              borderRadius: "12px",
              padding: "8px 24px 8px 24px",
              width: "84%",
              border: "1px solid #e0e0e0",
              boxShadow: "none",

            }}
          />

          <Button type="primary" onClick={(e) => { }}>+ Compose</Button>

        </div>

        <div className="emailDiv" style={{ width: "96%" }}>
          <div className="rowContainer" style={{ display: "flex" }}>
            <div className="emailRow" style={{ display: "flex", justifyContent: "space-between", width: "100%", backgroundColor: "white", padding: "6px 16px 6px 16px" }}>
              <div className="emailLeftpart" style={{ display: "flex", alignItems: "center", gap: "1rem" }}>
                <input type="checkbox" />
                <span onClick={handleStarClick} style={{ cursor: 'pointer' }}>
                  {isFilled ? <StarFilled style={{ color: 'gold', fontSize: '18px' }} /> : <StarOutlined style={{ fontSize: '18px' }} />}
                </span>
                <span style={{ fontWeight: "bold" }}>Leslie Alexander</span>
              </div>
              <div className="emailRightpart" style={{ display: "flex", alignItems: "center", gap: "1rem" }}>
                <span style={{ backgroundColor: "#98D7E4", borderRadius: "6px", padding: "2px 6px 2px 6px" }}>Operation</span>
                <span style={{ fontWeight: "bold" }}>Your Ship is ready!!..</span>
                <span style={{ whiteSpace: "nowrap", overflow: "hidden", textOverflow: 'ellipsis', WebkitLineClamp: "1", width: "600px" }}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam aperiam molestiae id dolore, dolorum corporis
                </span>
                <span>10:30am</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="popup">
        <div className="popupcontent">

        </div>
      </div>
    </>
  );
};

export default EmailTable;



