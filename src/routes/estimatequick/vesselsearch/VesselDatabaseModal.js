import React, { useState, useEffect } from "react";
import { Button, Input, Loader } from "antd";
import { SearchOutlined, CloseOutlined } from "@ant-design/icons";
import VesselLargeListFilter from "./VesselList";
import URL_WITH_VERSION, { getAPICall} from "../../../shared";
import useDebounce from "../../port-to-port/customhook";

const VesselDatabaseModal = ({ type, updateform, vesseldata }) => {
 // const [loader, setLoader] = useState(false);
  const [vesselFilteredList, setVesselFilteredList] = useState(vesseldata);
  // const [searchStatus, setSearchStatus] = useState("");
  // const debouncedSearchTerm = useDebounce(searchText, 1500);

  const handleSelectVessel = async (dataset) => {
    let getfullvesseldata;

    let blast_laden_consarr = [];
    let ifo_blast = [],
      mgo_blast = [],
      lsmgo_blast = [],
      vlsfo_blast = [],
      ulsfo_blast = [];
    let ifo_laden = [],
      mgo_laden = [],
      lsmgo_laden = [],
      vlsfo_laden = [],
      ulsfo_laden = [];

    if (type == "local") {
      getfullvesseldata = await getLocalVesselFullDetails(dataset.vessel_id);

      getfullvesseldata["seaspdconsp.tableperday"].map((el) => {
        let { ballast_laden, ifo, lsmgo, vlsfo, ulsfo, mgo } = el;

        if (ballast_laden == 1) {
          ifo_blast.push(ifo);
          mgo_blast.push(mgo);
          lsmgo_blast.push(lsmgo);
          vlsfo_blast.push(vlsfo);
          ulsfo_blast.push(ulsfo);
        } else {
          ifo_laden.push(ifo);
          mgo_laden.push(mgo);
          lsmgo_laden.push(lsmgo);
          vlsfo_laden.push(vlsfo);
          ulsfo_laden.push(ulsfo);
        }
      });
    } else {
      getfullvesseldata = dataset;
    }

    updateform(
      {
        vessel_name: type == "local" ? dataset.vessel_name : dataset.name,
        vessel_code: type == "local" ? dataset.vessel_code : dataset.callsign,
        vessel_id: type == "local" ? dataset.vessel_id : dataset.vessel_id,
        dwt: type == "local" ? dataset.vessel_dwt : dataset.dwt,
        laden_spd: type == "local" ? dataset.spd_laden : dataset.spd_laden,
        ballast_spd:
          type == "local" ? dataset.spd_ballast : dataset.spd_ballast,
      },
      {
        ifo_blast,
        mgo_blast,
        lsmgo_blast,
        vlsfo_blast,
        ulsfo_blast,
        ifo_laden,
        mgo_laden,
        lsmgo_laden,
        vlsfo_laden,
        ulsfo_laden,
      }
    );
  };

  const getLocalVesselFullDetails = async (id) => {
    const data = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${id}`);
    const respdata = await data.data;
    return respdata;
  };

  
 

  return (
    <div
      style={{
        width: "450px",
        height: "500px",
        padding: "10px",
      }}
    >
      <div style={{ fontSize: "20px", fontWeight: "600" }}>
        <span>
          {type == "local"
            ? "TheOceann Database vessel Search"
            : "Search Global Database vessel"}
        </span>
        {"   "}
      </div>

      <VesselLargeListFilter
        listData={vesselFilteredList}
        onSelectVessel={handleSelectVessel}
        type={type}
       
      />
    </div>
  );
};

export default VesselDatabaseModal;
