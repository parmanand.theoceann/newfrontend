import React, { useState, useEffect } from "react";
import "../../track-vessel/mapstyle.css";

import { List, AutoSizer, ScrollSync } from "react-virtualized";

const FilterLocalDbViewItem = ({ dataset, onSelectVessel, style, type }) => {
  const handleSelect = () => {
    onSelectVessel(dataset);
  };
// console.log(dataset,"66666666666");
  return (
   <>
    <div
      onClick={handleSelect}
      style={{
        backgroundColor: "light gray",
        borderTop: "1px solid #e0e0e0",
        borderRadius: "4px",
        padding: "10px",
      }}
    >
      {type == "local" ? (
        <span
          style={{
            fontSize: "12px",
            margin: "15px 10px",
            fontWeight: "regular",
            cursor: "pointer",
          }}
        >
          <b style={{ fontWeight: "bold" }}>Vessel </b>
          {"-" + dataset.vessel_name ?? "N/A" + " "}
          {"|"}
          <b style={{ fontWeight: "bold" }}> dwt</b>
          {"-" + dataset.vessel_dwt ?? "N/A"}
          {"|"}
          <b style={{ fontWeight: "bold" }}> IMO</b>
          {"-" + dataset.imo_no ?? "N/A"}
          {"|"}
          <b style={{ fontWeight: "bold" }}>type </b>{" "}
          {"-" + dataset.vessel_type_name ?? "N/A"}
          {"|"}
          <b style={{ fontWeight: "bold" }}> B.speed</b>
          {"-" + dataset.spd_ballast ?? "N/A"}
          {"|"}
          <b style={{ fontWeight: "bold" }}> L.speed</b>
          {"-" + dataset.spd_laden ?? "N/A"}
          {"|"}
        </span>
      ) : (
        <>
          <span
            style={{
              fontSize: "12px",
              margin: "15px 10px",
              fontWeight: "bold",
              cursor: "pointer",
            }}
          >
            Vessel Name-{dataset.name}
          </span>
          {"  "}
          <span
            style={{
              fontSize: "12px",
              margin: "15px 10px",
              fontWeight: "bold",
            }}
          >
            IMO no-{dataset.imo_number}
          </span>
        </>
      )}

    </div>
   
   </>
  )
};

const VesselLargeListFilter = ({ listData = [], onSelectVessel, type }) => {
  const [vesseldata, setVesselData] = useState(listData);
  const [searchText, setsearchText] = useState("");

  const renderRow = ({ index, key, style }) => {
    const dataset = vesseldata[index];
    return (
      <FilterLocalDbViewItem
        key={key}
        style={style}
        dataset={dataset}
        onSelectVessel={onSelectVessel}
        type={type}
      />
    );
  };

  const handleSearchValue = (e) => {
    setsearchText(e.target.value);
  };

  const LocalVesselDataFilter = () => {
    if (searchText > 3) {
      console.log("Search", searchText, vesseldata);
      let filteredlist = vesseldata.filter((el) =>
        el.vessel_name.toLowerCase().includes(searchText.toLowerCase())
      );

      setVesselData(filteredlist);
    }
  };

  useEffect(() => {
    LocalVesselDataFilter();
  }, [searchText]);

  return (
    <div className="filter-search-list" style={{ width: "450px" }}>
      <div className="close-icon">
        <input
          type="text"
          onChange={handleSearchValue}
          placeholder="Search vessel list by name"
        />
      </div>

      <div className="list-data-wrapper">
        <ScrollSync>
          {({ onScroll, scrollTop, isScrolling }) => (
            <div
              className="list-data"
              style={{ height: "calc(100vh - 320px)" }}
            >
              <AutoSizer>
                {({ width, height }) => {
                  return (
                    <List
                      width={width}
                      height={height}
                      rowHeight={140}
                      onScroll={onScroll}
                      rowRenderer={renderRow}
                      rowCount={vesseldata.length}
                      overscanRowCount={15}
                      scrollTop={scrollTop}
                      isScrolling={isScrolling}
                    />
                  );
                }}
              </AutoSizer>
            </div>
          )}
        </ScrollSync>

        {!listData.length && <div>no data found</div>}
      </div>
    </div>
  );
};

export default VesselLargeListFilter;
