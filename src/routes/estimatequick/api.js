import URL_WITH_VERSION, { getAPICall } from "../../shared";

export const localvesseldataApi = async () => {
  const resp = await getAPICall(`${URL_WITH_VERSION}/vessel/list?p=1&l=0`);
  const data = await resp;
  return data.data;
};

export const getGlobalVesselData = async (searchtext) => {
  const resp = await getAPICall(
    `https://apiv2.fleetmon.com/vesselsearch/?apikey=6c1fefe7c524d96d0036f0a31a45bc57&name=${searchtext}`
  );
  const data = await resp;
  return data.vessel;
};
