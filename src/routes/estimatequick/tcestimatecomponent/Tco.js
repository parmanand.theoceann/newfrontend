import React, { useState, useRef, useEffect } from "react";
import { Row, Col, Input, DatePicker } from "antd";
import moment from "moment";
import dayjs from "dayjs";
const TCODetails = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
}) => {
  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCO Duration :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="tco_duration"
              onChange={(e) =>
                handleChange("tco_details", "tco_duration", e.target.value)
              }
              value={formdata.tco_details.tco_duration}
             placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCO D/Hire :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="tco_d_hire"
              onChange={(e) =>
                handleChange("tco_details", "tco_d_hire", e.target.value)
              }
              value={formdata.tco_details.tco_d_hire}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Add/Bro % :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{borderRight:'1px solid black'}}>
                <Input
                type={'number'}
                  name="tco_add_comm"
                  onChange={(e) =>
                    handleChange("tco_details", "tco_add_per", e.target.value)
                  }
                  value={formdata.tco_details.tco_add_per}
                  placeholder="0.00"
                />
              </Col>

              <Col span={12}>
                <Input
                 type={'number'}
                  name="tco_bro_comm"
                  onChange={(e) =>
                    handleChange("tco_details", "tco_bro_per", e.target.value)
                  }
                  value={formdata.tco_details.tco_bro_per}
                  placeholder="0.00"
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Mis Revenue % :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
            type={"number"}
              name="misc_rev"
              onChange={(e) =>
                handleChange("tco_details", "mis_revenue", e.target.value)
              }
              placeholder="0.00"
              value={formdata.tco_details.misc_rev}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCO BB :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
            type={'number'}
              name="tco_bb"
              onChange={(e) => handleChange("tco_details", "tco_bb", e.target.value)}
              placeholder="0.00"
              value={formdata.tco_details.tco_bb}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Port days/Sea days :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{borderRight:'1px solid black'}}>
                <Input
                 type={'number'}
                 placeholder="0.00"
                  name="port_days"
                  onChange={(e) =>
                    handleChange("tco_details", "port_days", e.target.value)
                  }
                  value={formdata.tco_details.port_days}
                />
              </Col>
              <Col span={12}>
                <Input
                type={'number'}
                placeholder="0.00"
                  name="sea_days"
                  onChange={(e) =>
                    handleChange("tco_details", "sea_days", e.target.value)
                  }
                  value={formdata.tco_details.sea_days}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Commenced date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
             showTime={{ format: "HH:mm:ss" }}
              format="YYYY-MM-DD HH:mm:ss"
              name="commence_date"
              style={{width:"100%"}}
              onChange={(date, dateString) =>
                handleselect(dayjs(dateString), "commence_date")
              }
              value={dayjs(formdata.tco_details.commence_date,'YYYY-MM-DD HH:mm:ss')}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Completed date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
              format="YYYY-MM-DD HH:mm:ss"
              showTime
              disabled
              style={{width:'100%'}}
              name="completed_date"
              onChange={(date, dateString) =>
                handleselect(moment(dateString), "completed_date")
              }
              value={moment(formdata.tco_details.completed_date,'YYYY-MM-DD HH:mm:ss')}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Voyage days :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
            placeholder="0.00"
              onChange={(e) =>
                handleChange("tco_details", "total_voyage_days", e.target.value)
              }
              name="total_voyage_days"
              value={formdata.tco_details.total_voyage_days}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default TCODetails;
