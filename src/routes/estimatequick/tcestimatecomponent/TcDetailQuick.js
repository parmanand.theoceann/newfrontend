import React, { useState, useRef, useEffect } from "react";

import {
  Row,
  Col,
  Input,
  Select,
  InputNumber,
  Modal,
  Button,
  Tooltip,
} from "antd";
import { SearchOutlined, ExclamationCircleOutlined } from "@ant-design/icons";

import PortSelection from "../../port-selection/PortSelection";

import VesselDatabaseModal from "../vesselsearch/VesselDatabaseModal";
import PortModalForm from "../components/PortModalForm";
import { openNotificationWithIcon } from "../../../shared";
import { useQuickEstimateContext } from "../quickEstimateContext";

const TCIDetailQuick = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  openModal,
  updateQuickFromdatabase,
  modalCloseEvent,
  updatequickfrom,
  localvesseldata,
  handleSelectVesselNameFromDropDown,
}) => {
  const {
    ladenSpeedDropDown,
    selectedBallastSpeed,
    ballastSpeedDropDown,
    selectedLadenSpeed,
    setSelectedLadenSpeed,
    setSelectedBallastSpeed,
  } = useQuickEstimateContext();

  const vesselDropDownOptions = localvesseldata?.map((el, indx) => ({
    label: `${el.vessel_name} DWT ${el.vessel_dwt} IMO ${el.imo_no} ${el.vessel_type_name} BS spd ${el.spd_ballast} LS spd ${el.spd_laden}`,
    value: el.vessel_id,
    key: indx,
  }));

  const filterOption = (input, option) =>
    (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

  const handleSelectLadenSpeed = (value, option) => {
    handleChange("tci_details", "laden_spd", value);
    setSelectedLadenSpeed(option);

    handleselect(
      formdata.tci_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      true,
      false
    );
    handleselect(
      formdata.tci_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      true,
      false
    );
  };

  const handleSelectBallastSpeed = (value, option) => {
    handleChange("tci_details", "ballast_spd", value);
    setSelectedBallastSpeed(option);
    handleselect(
      formdata.tci_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      false,
      true
    );
    handleselect(
      formdata.tci_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      false,
      true
    );
  };


  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            <a
              onClick={() => {
                if (!formdata.tci_details.vessel_id) {
                  openNotificationWithIcon("error", "select the vessel first");
                  return;
                }
                handleOpenModal("openVesselModalForm", true);
              }}
              style={{ textDecoration: "none" }}
              className="required-label"
            >
              Vessel Name
            </a>
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              showSearch
              placeholder="Select a Vessel"
              optionFilterProp="children"
              // onChange={(e) =>
              //   handleChange("tci_details", e.target.name, e.target.value)
              // }
              onChange={handleSelectVesselNameFromDropDown}
              value={formdata.tci_details.vessel_name}
              dropdownStyle={{ width: "450px" }}
              filterOption={filterOption}
              options={vesselDropDownOptions}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCI Daily Cost:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              type={"number"}
              placeholder="0.00"
              name="tci_daily_cost"
              onChange={(e) =>
                handleChange("tci_details", "tci_daily_cost", e.target.value)
              }
              value={formdata.tci_details.tci_daily_cost}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCI Add /Bro com % :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="tci_add_com"
                  onChange={(e) =>
                    handleChange("tci_details", "tci_add_comm", e.target.value)
                  }
                  value={formdata.tci_details.tci_add_comm}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  onChange={(e) =>
                    handleChange("tci_details", "tci_bro_comm", e.target.value)
                  }
                  value={formdata.tci_details.tci_bro_comm}
                  name="tci_bro_com"
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCI BB/Other Cost:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="blast_bonus"
                  onChange={(e) =>
                    handleChange("tci_details", "blast_bonus", e.target.value)
                  }
                  value={formdata.tci_details.blast_bonus}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  name="other_cost"
                  onChange={(e) =>
                    handleChange("tci_details", "other_cost", e.target.value)
                  }
                  value={formdata.tci_details.other_cost}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Speed L/B:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Select
                  placeholder="0.00"
                  name="laden_spd"
                  // onChange={(e) =>
                  //   handleChange("tci_details", "laden_spd", e.target.value)
                  // }
                  onChange={handleSelectLadenSpeed}
                  // value={formdata.tci_details.laden_spd}
                  value={selectedLadenSpeed}
                  options={ladenSpeedDropDown}
                />
              </Col>
              <Col span={12}>
                <Select
                  placeholder="0.00"
                  name="ballast_spd"
                  // onChange={(e) =>
                  //   handleChange("tci_details", "ballast_spd", e.target.value)
                  // }
                  // value={formdata.tci_details.ballast_spd}
                  onChange={handleSelectBallastSpeed}
                  value={selectedBallastSpeed}
                  options={ballastSpeedDropDown}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Consp. Fuel per MT/D:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <div>
              <Row>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    Grade
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={10}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Select
                    style={{
                      width: 78,
                    }}
                    value={formdata.tci_details.vlsfo_fuel}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "vlsfo_fuel")
                    }
                    options={[
                      {
                        value: 5,
                        label: "VLSFO",
                      },
                      {
                        value: 10,
                        label: "ULSFO",
                      },
                      {
                        value: 2,
                        label: "HFO",
                      },
                    ]}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Select
                    style={{
                      width: 78,
                    }}
                    value={formdata.tci_details.lsmgo_fuel}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "lsmgo_fuel")
                    }
                    name="lsmgo_fuel"
                    options={[
                      {
                        value: 7,
                        label: "LSMGO",
                      },
                      {
                        value: 4,
                        label: "MGO",
                      },
                    ]}
                  />
                </Col>
              </Row>

              <Row style={{ marginTop: "5px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    B
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={10}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Input
                    placeholder="0.00"
                    name="vlsfo_cons_b"
                    onChange={(e) =>
                      handleChange(
                        "tci_details",
                        "vlsfo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.tci_details.vlsfo_cons_b}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="lsmgo_cons_b"
                    onChange={(e) =>
                      handleChange(
                        "tci_details",
                        "lsmgo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.tci_details.lsmgo_cons_b}
                  />
                </Col>
              </Row>

              <Row style={{ marginTop: "5px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    L
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={10}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="vlsfo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "tci_details",
                        "vlsfo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.tci_details.vlsfo_cons_l}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    placeholder="0.00"
                    name="lsmgo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "tci_details",
                        "lsmgo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.tci_details.lsmgo_cons_l}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Ballast Port:
          </Col>
          <Col span={11} style={{ marginRight: "0px" }}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openBalastModal", true)}
              name="ballast_port"
              value={formdata.tci_details.ballast_port}
            />
            <Modal
              open={openModal.openBalastModal}
              onCancel={() => handleOpenModal("openBalastModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={null}
                modalCloseEvent={(data) => {
                  handleOpenModal("openBalastModal", false);
                  modalCloseEvent("ballast_port", data);
                }}
              />
            </Modal>
          </Col>

          <Col span={2}>
            <Tooltip
              title="first select the port thenn fill the port iteniary form."
              placement="topLeft"
              color="cyan"
            >
              <Button
                onClick={() => handleOpenModal("openBalastModalForm", true)}
                type="secondary"
                disabled={formdata.tci_details.ballast_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </Button>
            </Tooltip>

            <Modal
              open={openModal.openBalastModalForm}
              onCancel={() => handleOpenModal("openBalastModalForm", false)}
              footer={null}
              width="80%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openBalastModalForm", false);

                  updatequickfrom("blastformdata", data);
                }}
                port={formdata.tci_details.ballast_port}
                formdata={formdata.blastformdata}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Delivery Port :
          </Col>
          <Col className="" span={11} style={{ marginRight: "0px" }}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openDeliveryModal", true)}
              name="delivery_port"
              value={formdata.tci_details.delivery_port}
            />
            <Modal
              open={openModal.openDeliveryModal}
              onCancel={() => handleOpenModal("openDeliveryModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.tci_details.ballast_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openDeliveryModal", false);
                  modalCloseEvent("delivery_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="first select the port thenn fill the port iteniary form."
              placement="topLeft"
              color="cyan"
            >
              <Button
                onClick={() => {
                  handleOpenModal("openDeliveryModalForm", true);
                }}
                type="secondary"
                disabled={formdata.tci_details.delivery_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </Button>
            </Tooltip>

            {openModal.openDeliveryModalForm && (
              <Modal
                open={openModal.openDeliveryModalForm}
                onCancel={() => handleOpenModal("openDeliveryModalForm", false)}
                footer={null}
                width="80%"
                maskClosable={false}
              >
                <PortModalForm
                  updateform={(data) => {
                    handleOpenModal("openDeliveryModalForm", false);
                    updatequickfrom("deliveryformdata", data);
                  }}
                  port={formdata.tci_details.delivery_port}
                  formdata={formdata.deliveryformdata}
                />
              </Modal>
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Redelivery port :
          </Col>
          <Col className="" span={11} style={{ marginRight: "0px" }}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openRedeliveryModal", true)}
              name="redelivery_port"
              value={formdata.tci_details.redelivery_port}
            />

            {openModal.openRedeliveryModal && (
              <Modal
                open={openModal.openRedeliveryModal}
                onCancel={() => handleOpenModal("openRedeliveryModal", false)}
                footer={null}
                width="75%"
              >
                <PortSelection
                  fromPortID={formdata.tci_details.delivery_port}
                  modalCloseEvent={(data) => {
                    handleOpenModal("openRedeliveryModal", false);
                    modalCloseEvent("redelivery_port", data);
                  }}
                />
              </Modal>
            )}
          </Col>

          <Col span={2}>
            <Tooltip
              title="first select the port then fill the port iteniary form."
              placement="topLeft"
              color="cyan"
            >
              <Button
                onClick={() => {
                  handleOpenModal("openRedeliveryModalForm", true);
                }}
                type="secondary"
                disabled={formdata.tci_details.redelivery_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </Button>
            </Tooltip>

            {openModal.openRedeliveryModalForm && (
              <Modal
                open={openModal.openRedeliveryModalForm}
                onCancel={() =>
                  handleOpenModal("openRedeliveryModalForm", false)
                }
                footer={null}
                width="80%"
                maskClosable={false}
              >
                <PortModalForm
                  updateform={(data) => {
                    handleOpenModal("openRedeliveryModalForm", false);

                    updatequickfrom("redeliveryformdata", data);
                  }}
                  port={formdata.tci_details.redelivery_port}
                  formdata={formdata.redeliveryformdata}
                />
              </Modal>
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Repos Port:
          </Col>
          <Col className="" span={11} style={{ marginRight: "0px" }}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openReposModal", true)}
              name="repos_port"
              value={formdata.tci_details.repos_port}
            />
            <Modal
              open={openModal.openReposModal}
              onCancel={() => handleOpenModal("openReposModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.tci_details.redelivery_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openReposModal", false);
                  modalCloseEvent("repos_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="first select the port thenn fill the port iteniary form."
              placement="topLeft"
              color="cyan"
            >
              <Button
                onClick={() => handleOpenModal("openReposModalForm", true)}
                type="secondary"
                disabled={formdata.tci_details.repos_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </Button>
            </Tooltip>

            {openModal.openReposModalForm && (
              <Modal
                open={openModal.openReposModalForm}
                onCancel={() => handleOpenModal("openReposModalForm", false)}
                footer={null}
                width="80%"
                maskClosable={false}
              >
                <PortModalForm
                  updateform={(data) => {
                    handleOpenModal("openReposModalForm", false);

                    updatequickfrom("reposformdata", data);
                  }}
                  port={formdata.tci_details.repos_port}
                  formdata={formdata.reposformdata}
                />
              </Modal>
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Bunker Price $/MT:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row gutter={8}>
              <Col className="gutter-row" span={12}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="bunker_oil_price"
                  onChange={(e) =>
                    handleChange(
                      "tci_details",
                      "bunker_oil_price",
                      e.target.value
                    )
                  }
                  value={formdata.tci_details.bunker_oil_price}
                />
              </Col>
              <Col className="gutter-row" span={12}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="bunker_gas_price"
                  onChange={(e) =>
                    handleChange(
                      "tci_details",
                      "bunker_gas_price",
                      e.target.value
                    )
                  }
                  value={formdata.tci_details.bunker_gas_price}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Routing:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              bordered={false}
              suffixIcon={null}
              style={{
                width: 180,
                borderBottom: "1px solid black",
              }}
              value={formdata?.tci_details?.routing}
              onChange={(selectedOption) =>
                handleselect(selectedOption, "routing")
              }
              options={[
                {
                  key: 1,
                  value: "1",
                  label: "ECA",
                },
                {
                  key: 2,
                  value: "2",
                  label: "Piracy",
                },
              ]}
            />
          </Col>
        </Row>
      </Col>

      {openModal.openLocalVesselModal && (
        <Modal
          open={openModal.openLocalVesselModal}
          footer={null}
          onCancel={() => handleOpenModal("openLocalVesselModal", false)}
        >
          <VesselDatabaseModal
            type="local"
            updateform={(data, fulldata) =>
              updateQuickFromdatabase("local", data, fulldata)
            }
          />
        </Modal>
      )}

      {openModal.openGlobalVesselModal && (
        <Modal
          open={openModal.openGlobalVesselModal}
          onCancel={() => handleOpenModal("openGlobalVesselModal", false)}
          footer={null}
        >
          <VesselDatabaseModal
            type="global"
            updateform={(data) => updateQuickFromdatabase("global", data)}
          />
        </Modal>
      )}
    </Row>
  );
};

export default TCIDetailQuick;
