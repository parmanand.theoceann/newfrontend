import React, { useState, useRef, useEffect, useReducer, useMemo } from "react";
import {
  Row,
  Col,
  Input,
  Select,
  Layout,
  Typography,
  Divider,
  Button,
  Collapse,
  Skeleton,
} from "antd";
import { useLocation } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import dayjs from "dayjs";
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  postAPICall,
  apiDeleteCall,
} from "../../shared";
import VesselDetailQuick from "./voyagereletcomponent/VesselDetailQuick";
import Cargodetails from "./voyagereletcomponent/Cargodetails";
import SaleFreight from "./voyagereletcomponent/SaleFreight";
import Voyageresult from "./voyagereletcomponent/VoyageResult";
import Co2view from "./components/Co2view";
import TopHeader from "./TopHeader";
import { initform, estimateReducer } from "./reducers/voyageReletReducer";
import { nextdate, cargoList } from "./helperfunction";

import "./estimatequick.css";
import { localvesseldataApi } from "./api";
import { QuickEstimateProvider, useQuickEstimateContext } from "./quickEstimateContext";
import { europeanCountryCodes } from "../../constants/countrycode";
const { Title } = Typography;
const { Content } = Layout;
const { Option } = Select;

const initPortModal = {
  openBalastModal: false,
  openBalastModalForm: false,
  openLoadModal: false,
  openLoadModalForm: false,
  openDischargeModal: false,
  openDischargeModalForm: false,
  openReposModal: false,
  openReposModalForm: false,
  openLocalVesselModal: false,
  openGlobalVesselModal: false,
  openVesselModalForm: false,
};

const initformitems = [{ ...initform, new_id: uuidv4() }];
const VoyageReletForm = ({
  addnewform,
  deleteform,
  totalform,
  formdata,
  addcopy,
  formitems
}) => {
  const [state, dispatch] = useReducer(estimateReducer, formdata);
  const location = useLocation();
  const fuel_cons_arrref = useRef(null);
  const [openModal, setOpenModal] = useState(initPortModal);
  const {setLadenSpeedDropDown,  setSelectedLadenSpeed, setBallastSpeedDropDown, setSelectedBallastSpeed, selectedLadenSpeed, selectedBallastSpeed} = useQuickEstimateContext()
  const [isDataPosted, setIsDataPosted] = useState(false)
  let [cargoarr, setCargoarr] = useState([]);
  const [localvesseldata, setLocalVesselData] = useState(null);
  const getcargos = async () => {
    const data = await cargoList();
    setCargoarr(data);
  };

  cargoarr = useMemo(() => cargoarr, [cargoarr]);

  let blasttoload_ref = useRef(0);
  let loadtodischarge_ref = useRef(0);
  let dischargetorepos_ref = useRef(0);

  const [loading, setLoading] = useState(false);
  const handleOpenModal = (key, value) => {
    setOpenModal({
      ...openModal,
      [key]: value,
    });
  };

  useEffect(() => {
    const id = location.state && location.state.data;
    const vessel_id = location.state && location.state.data.vessel_id
    const copyVesselId = state.vessel_details.vessel_id;
    if (location.pathname === `/edit-voy-relet/${id}` && formitems.length <= 1) {
      editVoyageReletQuick(id);
      getDataForSelectedVessel(vessel_id)
      setIsDataPosted(false);
    } else if (copyVesselId){
      getDataForSelectedVessel(state.vessel_details.vessel_id);
    }
    else  {

      if(id){
        getDataForSelectedVessel(state.vessel_details.vessel_id)
      }

      let totalvoyagedays =
        parseInt(state.vessel_details.sea_days) +
        parseInt(state.vessel_details.port_days);
      let commence_date = state.voyage_results.commence_date;
      let completed_date = nextdate(commence_date, totalvoyagedays);

      dispatch({ type: "total_voyage_days", payload: totalvoyagedays });
      dispatch({
        type: "completed_date",
        payload: dayjs(completed_date).format("YYYY-MM-DD HH:mm:ss"),
      });
    }
    getLocalVesselData();
  }, []);

  const getLocalVesselData = async () => {
    const data = await localvesseldataApi();
    setLocalVesselData(data);
  };

  useEffect(() => {
    getcargos();
  }, []);
  // const modalCloseEvent = (key, data) => {
  //   if (data.distance) {
  //     let totaldistance = Number(
  //       data?.distance?.features[0]?.properties?.total_length
  //     );

  //     switch (key) {
  //       case "load_port":
  //         blasttoload_ref.current = totaldistance;
  //         break;

  //       case "discharge_port":
  //         loadtodischarge_ref.current = totaldistance;
  //         break;
  //       case "repos_port":
  //         dischargetorepos_ref.current = totaldistance;

  //         break;
  //       default:
  //         return null;
  //     }
  //   }

  //   updateform({ [key]: data.port.port_name });
  // };

  const modalCloseEvent = (key, data) => {
    console.log({
      key, data
    });
    let eu_country = europeanCountryCodes.includes(data.port.country_code)
      ? true
      : false;

    if (data.distance) {
      let totaldistance = Number(
        data?.distance?.features[0]?.properties?.total_length
      );
      console.log('totaldistance', totaldistance);
      switch (key) {
        case "load_port":
          blasttoload_ref.current = totaldistance;
          dispatch({ type: "blasttoloadDistance", payload: totaldistance });
          break;

        case "discharge_port":
          loadtodischarge_ref.current = totaldistance;
          dispatch({ type: "loadTodischargeDistance", payload: totaldistance });
          break;
        case "repos_port":
          dischargetorepos_ref.current = totaldistance;
          dispatch({
            type: "dischargetoreposDistance",
            payload: totaldistance,
          });
          break;
        default:
          return null;
      }
    }
    dispatch({
      type: "eu_country",
      payload: { [key + "_" + "eu_country"]: eu_country },
    });
    updateform({ [key]: data.port.port_name });
  };
  const updateform = (data) => {
    dispatch({
      type: Object.keys(data)[0],
      payload: {
        port_name: Object.values(data)[0],
        portdistance: {
          blasttoload_ref,
          loadtodischarge_ref,
          dischargetorepos_ref,
        },
      },
    });
  };

  const updateQuickFromdatabase = (type, data, fulldata) => {
    fuel_cons_arrref.current = fulldata;
    dispatch({ type: "vessel_name", payload: data });
    dispatch({ type: "cons_fuel", payload: fulldata });
    if (type == "local") {
      handleOpenModal("openLocalVesselModal", false);
    } else {
      handleOpenModal("openGlobalVesselModal", false);
    }
  };

  const handleChange = (group, name, value) => {
    if (name == "bunker_gas_price" || name == "bunker_oil_price") {
      dispatch({
        type: name,
        payload: {
          value: value,
          portdistance: {
            blasttoload_ref,
            loadtodischarge_ref,
            dischargetorepos_ref,
          },
        },
      });
    } else {
      dispatch({
        type: name,
        payload:
          typeof value === "string" || value === "object"
            ? value
            : parseInt(value),
      });
    }
  };

  


  const handleDate = (value, name) => {
    dispatch({ type: name, payload: value });
  };

  
  const handleselect = (value, name, flag, index, laden = false, ballast = false) => {
    console.log({
      value, name, flag, index, laden, ballast
    });
    let multiplier = 0;
    if (name === "vlsfo_fuel" || name === "lsmgo_fuel") {
      switch (value) {
        case 5:
          multiplier = 3.15;
          break;
        case 10:
          multiplier = 3.15;
          break;
        case 11:
          multiplier = 3.114;
          break;
        case 7:
          multiplier = 3.2;
          break;
        case 4:
          multiplier = 3.2;
      }
      if(flag && laden){
        if(name === 'vlsfo_fuel'){
         
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden : index,
              indexBallast : selectedBallastSpeed.key,
            },
          });
        }else{
         
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden : index,
              indexBallast : selectedBallastSpeed.key,
            },
          });
        }
        return  
      }else if(flag && ballast){
        if(name === 'vlsfo_fuel'){
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden : selectedLadenSpeed.key,
              indexBallast : index,
            },
          });
        }else{
        
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden : selectedLadenSpeed.key,
              indexBallast : index,
            },
          });
        }
        return 
      }

      dispatch({
        type: name,
        payload: {
          fuelarr: fuel_cons_arrref.current,
          value: value,
          multiplier: multiplier,
          indexLaden : selectedLadenSpeed.key,
          indexBallast : selectedBallastSpeed.key

        },
      });
      return;
    }
    dispatch({ type: name, payload: value });
  };


  const handleselectCargo = (value, name) => {
    dispatch({ type: name, payload: parseInt(value) });
  };

  const updatequickfrom = (group, data) => {
    dispatch({ type: group, payload: data });
  };

  const saveFormData = () => {
    if(!state.vessel_details.vessel_id) {
      openNotificationWithIcon("error", "select the vessel first");
      return
    }
    let vData = state;
    delete vData.co2_calc_value;
    delete vData.blasttoloadDistance;
    delete vData.loadTodischargeDistance;
    delete vData.dischargetoreposDistance;
    let type = "save";
    let suMethod = "POST";
    if (vData["id"] || vData["estimate_id"]) {
      type = "update";
      suMethod = "PUT";
    }
    delete vData["new_id"];
    let suURL = `${URL_WITH_VERSION}/voyage-relet/quick-${type}`;
    setLoading(true);
    // stateref.current=vData;
    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        editVoyageReletQuick(data.row.estimate_id);
        setLoading(false);
        setIsDataPosted(false);
      } else {
        openNotificationWithIcon("error", data.message);
        setLoading(false);
        // setState(prevState => ({...prevState, loadFrm: true,formData:stateref.current}))
      }
    });
  };

  const getDataForSelectedVessel = async (vessel_id) => 
  {
    setLoading(true)
    let ifo_blast = [],
      mgo_blast = [],
      lsmgo_blast = [],
      vlsfo_blast = [],
      ulsfo_blast = [],
      speed_blast = [];
    let ifo_laden = [],
      mgo_laden = [],
      lsmgo_laden = [],
      vlsfo_laden = [],
      ulsfo_laden = [],
      speed_laden = [];

    let updatedObj = {};
    let fuelData = {};
    const res = await getAPICall(
      `${URL_WITH_VERSION}/vessel/list/${vessel_id}`
    );
    const VesselData = await res.data;
    VesselData["seaspdconsp.tableperday"].map((el) => {
      let { ballast_laden, ifo, lsmgo, vlsfo, ulsfo, mgo, speed } = el;

      if (ballast_laden == 1) {
        ifo_blast.push(ifo);
        mgo_blast.push(mgo);
        lsmgo_blast.push(lsmgo);
        vlsfo_blast.push(vlsfo);
        ulsfo_blast.push(ulsfo);
        speed_blast.push(speed ? speed : '0.00');
      } else {
        ifo_laden.push(ifo);
        mgo_laden.push(mgo);
        lsmgo_laden.push(lsmgo);
        vlsfo_laden.push(vlsfo);
        ulsfo_laden.push(ulsfo);
        speed_laden.push(speed ? speed : '0.00');
      }
      
      setSelectedLadenSpeed({label: speed_laden[0], value : speed_laden[0], key : 0})
      setSelectedBallastSpeed({label : speed_blast[0], value : speed_blast[0], key: 0})
      // handleChange("vessel_details", 'vessel_name', updatedObj)
      setLadenSpeedDropDown(speed_laden.map((e, i) => ({label: e, value: e, key : i})));
      setBallastSpeedDropDown(speed_blast.map((e, i) => ({label : e, value : e, key : i})))

      updatedObj["dwt"] = VesselData.vessel_dwt;
      updatedObj["vessel_code"] = VesselData.vessel_code;
      updatedObj["laden_spd"] = speed_laden[0];
      updatedObj["ballast_spd"] = speed_blast[0];
      updatedObj["vessel_id"] = VesselData.vessel_id;
      updatedObj["vessel_name"] = VesselData.vessel_name;
     
      fuelData = {
        ifo_blast,
        mgo_blast,
        lsmgo_blast,
        vlsfo_blast,
        ulsfo_blast,
        ifo_laden,
        mgo_laden,
        lsmgo_laden,
        vlsfo_laden,
        ulsfo_laden,
      };
     
    });
   
    updateQuickFromdatabase('local', updatedObj, fuelData)
    setLoading(false)
  };

  const handleSelectVesselNameFromDropDown = (value, option) => {
    getDataForSelectedVessel(value);
  };

  const editVoyageReletQuick = async (id) => {
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-relet/quick-edit?ae=${id}`
    );
    const respdata = await response["data"];
    totalform(respdata);
    dispatch({ type: "updateestimate", payload: respdata });
    setLoading(false);
    setIsDataPosted(true)
  };

  const items = [
    {
      key: "1",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          Vessel Details
        </span>
      ),
      children: (
        <VesselDetailQuick
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
          openModal={openModal}
          modalCloseEvent={modalCloseEvent}
          updatequickfrom={updatequickfrom}
          updateQuickFromdatabase={updateQuickFromdatabase}
          localvesseldata={localvesseldata}
          handleSelectVesselNameFromDropDown={
            handleSelectVesselNameFromDropDown
          }
        />
      ),
    },

    {
      key: "2",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          Sale Freight
        </span>
      ),
      children: (
        <SaleFreight
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          cargoarr={cargoarr}
          handleOpenModal={handleOpenModal}
          handleselectCargo={handleselectCargo}
        />
      ),
    },

    {
      key: "3",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          Cargo (Buy Freight)
        </span>
      ),
      children: (
        <Cargodetails
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          cargoarr={cargoarr}
          handleOpenModal={handleOpenModal}
          handleselectCargo={handleselectCargo}
        />
      ),
    },

    {
      key: "4",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          Voyage Results
        </span>
      ),
      children: (
        <Voyageresult
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
          handleDate={handleDate}
        />
      ),
    },

    {
      key: "5",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          CII dynamics
        </span>
      ),
      children: (
        <Co2view
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
        />
      ),
    },
  ];

  const handleadd = () => {
    if (isDataPosted) {
      return;
    }
    addnewform();
  };

  const handledelete = () => {
    console.log(state.id);
    deleteform(state.id);
  };

  const handleReset = () => {
    if (state.id || state.estimate_id) {
      return;
    }
    setSelectedLadenSpeed({label : '0.00', value: 0})
    setSelectedBallastSpeed({label : '0.00', value: 0})
    setLadenSpeedDropDown(null)
    setBallastSpeedDropDown(null)

    dispatch({ type: "reset", payload: initform });
  };

  const handlecopy = () => {
    // dispatch({ type: "copy", payload: state });

    addcopy(state);
  };

  return (
    <>
      <TopHeader
        saveformdata={saveFormData}
        handleadd={handleadd}
        handlecopy={handlecopy}
        handledelete={handledelete}
        handlereset={handleReset}
        quickestimateid={state.estimate_id}
        formdata={state}
        extraMenu={[
          {
            label: "Go to Voyage Estimate",
            key: "voyage_estimate",
          },
          {
            label: "Go to TC Estimate",
            key: "tc_estimate",
          },
        ]}
      />
      <Skeleton loading={loading} active>
        <Row>
          <Col className="bold" span={10} style={{ paddingLeft: "17px" }}>
            Voyage Relet ID :
          </Col>
          <Col
            className=""
            span={14}
            style={{ paddingRight: 2, paddingBottom: 5 }}
          >
            <Input
              type="text"
              disabled
              name="estimate_id"
              value={state.estimate_id}
            />
          </Col>
        </Row>
        <Collapse
          items={items}
          defaultActiveKey={"1"}
          style={{ backgroundColor: "#12406a" }}
          expandIconPosition="end"
        />
      </Skeleton>

      <Divider />
      <Row>
        <button
          type="primary"
          onClick={saveFormData}
          style={{
            backgroundColor: "rgb(18, 64, 106)",
            color: "white",
            padding: "2px 15px",
            borderRadius: "6px",
            marginRight: "10px",
          }}
        >
          Submit
        </button>
        <button
          type="primary"
          onClick={handleReset}
          style={{
            backgroundColor: "rgb(18, 64, 106)",
            color: "white",
            padding: "2px 15px",
            borderRadius: "6px",
          }}
        >
          Reset
        </button>
      </Row>
    </>
  );
};

const VoyageReletQuick = () => {
  const [formitems, setFormitems] = useState(initformitems);

  const handleadd = () => {
    setFormitems([...formitems, { ...initform, new_id: uuidv4() }]);
  };

  const handledelete = (id , index) => {
   
    let URL = `${URL_WITH_VERSION}/voyage-relet/quick-delete`;
    if(id){
      apiDeleteCall(URL, { id: id }, (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          remainingformitem(index);
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      })
    }else{
      remainingformitem(index)
    }
  };

  const remainingformitem = (index) => {
    let allformitems = [...formitems];
    if (allformitems.length > 1) {
      allformitems.splice(index, 1);
      setFormitems([...allformitems]);
    } else {
      return;
    }
  };

  const totalForm = (data, index) => {
    let allformitems = [...formitems];
    allformitems[index] = { ...data };
    setFormitems([...allformitems]);
  };

  const handleaddcopy = (data, index) => {
    setFormitems([
      ...formitems,
      { ...data, estimate_id: "", id: null, new_id: uuidv4() },
    ]);
  };

  return (
    <div className="tcov-wrapper">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div
                      className="box-body common-fields-wrapper"
                      style={{
                        maxWidth: "fit-content",
                        minWidth: "90vw",
                        overflowX: "scroll",
                        display: "flex",
                      }}
                    >
                      {formitems.map((el, index) => (
                        <div
                          style={{
                            minWidth: "360px",
                            padding: "10px 0px",
                            borderLeft: "1px solid black",
                            marginRight: "10px",
                          }}
                          key={index}
                        >
                          <QuickEstimateProvider>
                            <VoyageReletForm
                              addnewform={handleadd}
                              deleteform={(id) => handledelete(id, index)}
                              totalform={(data) =>
                                totalForm(
                                  { ...data, new_id: el?.new_id },
                                  index
                                )
                              }
                              addcopy={(data) =>
                                handleaddcopy(
                                  { ...data, new_id: el.new_id },
                                  index
                                )
                              }
                              formdata={el}
                              formitems = {formitems}
                            />
                          </QuickEstimateProvider>
                        </div>
                      ))}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default VoyageReletQuick;
