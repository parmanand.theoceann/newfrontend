import React, { createContext, useContext, useState } from "react";

const QuickEstimateContext = createContext();

const QuickEstimateProvider = ({ children }) => {
  const [ladenSpeedDropDown, setLadenSpeedDropDown] = useState();
  const [ballastSpeedDropDown, setBallastSpeedDropDown] = useState();
  const [selectedLadenSpeed, setSelectedLadenSpeed] = useState({
    label: "0.00",
    value: 0,
  });
  const [selectedBallastSpeed, setSelectedBallastSpeed] = useState({
    label: "0.00",
    value: 0,
  });

  return (
    <QuickEstimateContext.Provider
      value={{
        ballastSpeedDropDown,
        ladenSpeedDropDown,
        selectedLadenSpeed,
        selectedBallastSpeed,
        setLadenSpeedDropDown,
        setBallastSpeedDropDown,
        setSelectedLadenSpeed,
        setSelectedBallastSpeed,
      }}
    >
      {children}
    </QuickEstimateContext.Provider>
  );
};

const useQuickEstimateContext = () => {
  return useContext(QuickEstimateContext);
};

export { QuickEstimateProvider, useQuickEstimateContext };
