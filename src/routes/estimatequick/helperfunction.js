import moment from "moment";
import URL_WITH_VERSION, { getAPICall } from "../../shared";
import dayjs from "dayjs";

export const nextdate = (currentdate, days) => {
  let sf = dayjs(currentdate);
  let ad = null,
    vs = [],
    d = 0,
    h = 0,
    m = 0;
  if (sf && sf !== "") {
    sf = typeof sf === "string" ? dayjs(sf) : sf;
    days = days.toString();
    vs = days.split(".");

    if (vs.length === 2 && parseInt(vs[1]) > 0) {
      d = vs[0];
      h = parseInt(parseFloat("0." + vs[1]) * 24);
      m = parseInt((parseInt(parseFloat("0." + vs[1]) * 24) - h) * 60);
      ad = sf.add(d, "day").add(h, "hour").add(m, "minute");
    } else {
      ad = sf.add(days, "day");
    }
  }

  return ad;
};

// export const nextdate = (currentdate, days) => {
//   let sf = moment(currentdate);
//   let ad = null,
//     vs = [],
//     d = 0,
//     h = 0,
//     m = 0;
//   if (sf && sf !== "") {
//     sf = typeof sf === "string" ? moment(sf) : sf;
//     days = days.toString();
//     vs = days.split(".");

//     if (vs.length === 2 && parseInt(vs[1]) > 0) {
//       d = vs[0];
//       h = parseInt(parseFloat("0." + vs[1]) * 24);
//       m = parseInt((parseInt(parseFloat("0." + vs[1]) * 24) - h) * 60);
//       ad = sf.add(d, "days").add(h, "hours").add(m, "minutes");
//     } else {
//       ad = sf.add(days, "days");
//     }
//   }

//   return ad;
// };

export const voyageEstimateCalculation = (
  state,
  blasttoLoadDistance = 0,
  loadtoDischargeDistance = 0,
  dischargetoReposDistance = 0,
  group = null,
  name = null,
  value = null
) => {
  if (group && name && value) {
    state = {
      ...state,
      [group]: {
        ...state[group],
        [name]: value,
      },
    };
  } else if (!group && name && value) {
    state = {
      ...state,
      [name]: value,
    };
  }

  const {
    vessel_details,
    cargo_details,
    voyage_results,
    co2_view,
    blastPortExpenses = 0,
    loadPortExpenses = 0,
    dischargePortExpenses = 0,
    reposPortExpenses = 0,
  } = state;
  let {
    tci_daily_cost = 0,
    total_voyage_days = 0,
    other_cost = 0,
    blast_bonus,
    tci_add_comm,
    ballast_spd,
    laden_spd,
    port_days,
    vlsfo_cons_b,
    vlsfo_cons_l,
    bunker_oil_price,
    lsmgo_cons_l,
    lsmgo_cons_b,
    bunker_gas_price,
    commence_date,
  } = vessel_details;
  let grossIncome = 0,
    netIncome = 0,
    totalPortExpenses = 0,
    totalexpense = 0,
    totalProfit = 0,
    dailyProfit = 0,
    cpQty = 0,
    frtRate = 0,
    commission = 0,
    frtType = 0,
    vesselHire = 0,
    seaDays = 0,
    totalVoyageDays = 0,
    tce = 0;

  cpQty = cargo_details.cp_qty;
  frtRate = cargo_details.frt_rate;
  commission = cargo_details.commission;
  frtType = cargo_details.frt_type;
  // -------------------------------Revenue Calculations----------------------------------------------
  grossIncome = Number(frtType == 38 ? cpQty * frtRate : frtRate);

  //" netincome formula": "((Q*FR)-(((Q*FR)*CR)*0.01) + OR)", Q=cpqty, FR=frtRate, CR=commission, OR=otherrevenue
  netIncome = grossIncome - grossIncome * commission * 0.01;

  //----------------------------Expense Calculations----------------------
  // vessel hire
  vesselHire = tci_daily_cost * total_voyage_days;
  // seadays calculation
  let seadays1 = 0,
    seadays2 = 0,
    seadays3 = 0;
  seadays1 =
    blasttoLoadDistance && ballast_spd
      ? blasttoLoadDistance / (ballast_spd * 24)
      : 0;
  seadays2 =
    loadtoDischargeDistance && ballast_spd
      ? loadtoDischargeDistance / (laden_spd * 24)
      : 0;
  seadays3 =
    dischargetoReposDistance && ballast_spd
      ? dischargetoReposDistance / (ballast_spd * 24)
      : 0;

  seaDays = seadays1 + seadays2 + seadays3;
  totalVoyageDays = seaDays + Number(port_days);

  // bunker expense calculation (totalvlsfoprice +totallsmgoprice)

  // total vlsfo calculation (consumption and price)
  // totalvlsfo fuel consumption "formula": "((((BD/BS)+(RD/BS))/24)*BC) + (((LD/LS)/24)*LC)",

  let totalvlsfoCons = 0;
  totalvlsfoCons =
    ((blasttoLoadDistance + dischargetoReposDistance) / (ballast_spd * 24)) *
      vlsfo_cons_b +
    (loadtoDischargeDistance / (laden_spd * 24)) * vlsfo_cons_l;
  let totalvlsfoPrice = totalvlsfoCons * bunker_oil_price;

  // total lsmgo calculation (consumption and price)
  // total lsmgo consumption "formula": "(((LD/LS)/24)*LC) + ((((BD/BS)+(RD/BS))/24)*BC)",
  let totallsmgocons = 0;
  totallsmgocons =
    (loadtoDischargeDistance / (laden_spd * 24)) * lsmgo_cons_l +
    ((blasttoLoadDistance + dischargetoReposDistance) / (ballast_spd * 24)) *
      lsmgo_cons_b;

  let totallsmgoprice = 0,
    totalBunkerExpenses = 0;
  totallsmgoprice = totallsmgocons * bunker_gas_price;
  totalBunkerExpenses = totalvlsfoPrice + totallsmgoprice;

  // portexpense

  totalPortExpenses =
    Number(blastPortExpenses) +
    Number(loadPortExpenses) +
    Number(dischargePortExpenses) +
    Number(reposPortExpenses);

  //"total expenseformula": "((VD*H)+MC-(((AP*H*VD)/100)+((BP*H*VD)/100)) + BE + PE+BB)",  pE=portexpense;

  totalexpense =
    Number(vesselHire) +
    Number(other_cost) -
    tci_add_comm * vesselHire * 0.01 +
    Number(totalBunkerExpenses) +
    Number(totalPortExpenses) +
    Number(blast_bonus);

  // profitloss calculation

  totalProfit = netIncome - totalexpense;
  dailyProfit = totalProfit / totalVoyageDays;

  // tce calculation
  // "originalformula": "((NI-(H*VD+PI+OE+BB+SC-(H*VD+BB+OE-H*VD*AP*0.01)))/VD)",
  // mathematically modified formula : ((NI-PI-SC)/VD)+H*AP*0.01
  // NI=netIncome, PI=PortExpense,SC=BunkerExpense,VD=TotalVoyageDays, H=tcidailycost, AP=tciaddcomm

  tce =
    (netIncome - totalPortExpenses - totalBunkerExpenses) / totalVoyageDays +
    tci_daily_cost * tci_add_comm * 0.01;

  // total port expenses = Ballast-port-expenses + Load-Port + Discharge-Port + Repos-Port

  let completedDate = nextdate(commence_date, totalVoyageDays);
  completedDate = dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss");

  return {
    seaDays,
    totalVoyageDays,
    completedDate,
    grossIncome,
    netIncome,
    totalBunkerExpenses,
    totalPortExpenses,
    vesselHire,
    totalexpense,
    totalProfit,
    dailyProfit,
    tce,
  };
};

export const tcEstimateCalculation = (
  state,
  blasttoDeliveryDistance = 0,
  deliverytoRedeliveryDistance = 0,
  redeliverytoReposDistance = 0,
  group = null,
  name = null,
  value = null
) => {
  if (group && name && value) {
    state = {
      ...state,
      [group]: {
        ...state[group],
        [name]: value,
      },
    };
  } else if (!group && name && value) {
    state = {
      ...state,
      [name]: value,
    };
  }
  let totalVoyageDays = 0,
    grossIncome = 0,
    netIncome = 0,
    totalExpenses = 0,
    totalPortExpenses = 0,
    totalProfit = 0,
    dailyProfit = 0,
    vesselHire = 0,
    seaDays = 0,
    tce = 0,
    totalBunkerExpenses = 0;
  let {
    tci_details,
    tco_details,
    voyage_results,
    co2_view,
    blastPortExpenses,
    deliveryPortExpenses,
    redeliveryPortExpenses,
    reposPortExpenses,
    blastformdata,
    deliveryformdata,
    redeliveryformdata,
    reposformdata,
  } = state;

  blastPortExpenses = blastPortExpenses
    ? blastPortExpenses
    : blastformdata?.port_expense ?? 0;
  deliveryPortExpenses = deliveryPortExpenses
    ? deliveryPortExpenses
    : deliveryformdata?.port_expense ?? 0;
  redeliveryPortExpenses = redeliveryPortExpenses
    ? redeliveryPortExpenses
    : redeliveryformdata?.port_expense ?? 0;
  reposPortExpenses = reposPortExpenses
    ? reposPortExpenses
    : reposformdata?.port_expense ?? 0;

  const {
    tci_daily_cost,
    tci_add_comm,
    tci_bro_comm,
    ballast_spd,
    laden_spd,
    vlsfo_cons_b,
    vlsfo_cons_l,
    lsmgo_cons_l,
    other_cost,
    lsmgo_cons_b,
    bunker_oil_price,
    bunker_gas_price,
    blast_bonus,
  } = tci_details;
  const {
    total_voyage_days,
    tco_d_hire,
    mis_revenue,
    tco_bb,
    tco_add_per,
    tco_bro_per,
    port_days,
    commence_date,
  } = tco_details;

  totalVoyageDays = total_voyage_days;

  //-------------------Revenue calculation------------------------
  // " gross incomeformula": "((VD*H)+OR+BB)", VD=voyagedays,H=tcodhire,OR=otherRevenue,BB=tcobb
  grossIncome = total_voyage_days * tco_d_hire + mis_revenue * 1 + tco_bb * 1;
  // "netincomeformula": "(((VD*TH)+OR+BB)-((VD*TH*TA*0.01)+(VD*TH*TB*0.01)))",
  // VD=totalvoyagedays, TH=tcodhire, OR=tco_other_rev, BB=tco_bb,TA=tco_add_percentage, TB=tco_bro_percentage
  netIncome =
    grossIncome -
    total_voyage_days * tco_d_hire * 0.01 * (tco_add_per * 1 + tco_bro_per * 1);

  //-----------------------Expense Calculation--------------------------------------

  vesselHire = tci_daily_cost * totalVoyageDays;
  //seadays formula"formula": "((BD/(BS*24)) + (LD/(LS*24)) + (RD/(BS*24)))", BD=blasttoDeliveryDistance,LD=deliverytoRedeliveryDistance,RD=redeliverytoReposDistance
  //BS=blastspeed, ls=ladenspeed

  let seaDays1 = 0,
    seaDays2 = 0,
    seaDays3 = 0;
  seaDays1 = blasttoDeliveryDistance / (ballast_spd * 24);
  seaDays2 = deliverytoRedeliveryDistance / (laden_spd * 24);
  seaDays3 = redeliverytoReposDistance / (ballast_spd * 24);
  seaDays = seaDays1 + seaDays2 + seaDays3;
  totalVoyageDays = Number(port_days) + seaDays;

  /*************************Vlsfo total consumption******************** */
  // " vlsfo total consumptionformula": "((((BD/BS)+(RD/BS))/24)*BC) + (((LD/LS)/24)*LC)",

  let totalvlsfocons = 0,
    totalVlsfoPrice = 0;
  totalvlsfocons =
    ((blasttoDeliveryDistance * 1 + redeliverytoReposDistance * 1) /
      (ballast_spd * 24)) *
      vlsfo_cons_b +
    (deliverytoRedeliveryDistance / (laden_spd * 24)) * vlsfo_cons_l;
  totalVlsfoPrice = totalvlsfocons * bunker_oil_price;

  //********************************Mgo total consumption*****************/
  //"totalmgo consformula": "(((LD/LS)/24)*LC) + ((((BD/BS)+(RD/BS))/24)*BC)",
  let totalmgoCons = 0,
    totalmgoPrice = 0;
  totalmgoCons =
    (deliverytoRedeliveryDistance / (laden_spd * 24)) * lsmgo_cons_l +
    ((blasttoDeliveryDistance * 1 + redeliverytoReposDistance * 1) /
      (ballast_spd * 24)) *
      lsmgo_cons_b;

  totalmgoPrice = totalmgoCons * bunker_gas_price;

  totalBunkerExpenses = totalVlsfoPrice + totalmgoPrice;

  // portexpenses

  totalPortExpenses =
    Number(blastPortExpenses) +
    Number(deliveryPortExpenses) +
    Number(redeliveryPortExpenses) +
    Number(reposPortExpenses);

  // "total_expenseformula": "((VD*H)+MC -(((VD*AP*H)/100)+((VD*BP*H)/100)) + BE + PE+BB)",
  // MC=other_cost, ap=tci_add_comm,bp=tci_bro_comm, BE=totalbubkerexp, pe=portexp,BB=blast_bonus

  totalExpenses =
    Number(vesselHire) +
    other_cost * 1 -
    totalVoyageDays *
      tci_daily_cost *
      0.01 *
      (tci_add_comm * 1 + tci_bro_comm * 1) +
    Number(totalBunkerExpenses) +
    Number(totalPortExpenses) +
    Number(blast_bonus);
  //----------------------------------------------------------------Profit loss calculation--------------------------------

  totalProfit = netIncome - totalExpenses;
  dailyProfit = totalProfit / totalVoyageDays;

  //----------------------------------------------------------------TCE Calculations--------------------------------
  //"tce formula": "((NI-(H*VD+PI+OE+BB+SC-(H*VD+BB+OE-H*VD*AP*0.01)))/VD)",
  // mathematicalLY modified formula:((NI-PI-SC)/VD)+H*AP*0.01;
  // NI=netincome, PI=portexpense,sc=totalbunkerexpense,VD=totalvoyagedays,H=tcidhire,AP=addpercentage

  tce =
    (netIncome - totalPortExpenses - totalBunkerExpenses) / totalVoyageDays +
    tci_daily_cost * tci_add_comm * 0.01;

  let completedDate = nextdate(commence_date, totalVoyageDays);
  completedDate = dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss");

  return {
    seaDays,
    grossIncome,
    netIncome,
    completedDate,
    totalExpenses,
    totalBunkerExpenses,
    totalPortExpenses,
    totalProfit,
    dailyProfit,
    totalVoyageDays,
    tce,
    vesselHire,
  };
};

export const VoyageReletCalculation = (
  state,
  blasttoLoadDistance = 0,
  loadtoDischargeDistance = 0,
  dischargetoReposDistance = 0,
  group = null,
  name = null,
  value = null
) => {
  if (group && name && value) {
    state = {
      ...state,
      [group]: {
        ...state[group],
        [name]: value,
      },
    };
  } else if (!group && name && value) {
    state = {
      ...state,
      [name]: value,
    };
  }

  let vesselHire = 0,
    totalVoyageDays = 0,
    cpQty = 0,
    frtRate = 0,
    otherRev = 0,
    grossIncome = 0,
    netIncome = 0,
    totalProfit = 0,
    seaDays = 0,
    totalBunkerExpenses = 0,
    totalExpenses = 0,
    scpQty = 0,
    sfrtRate = 0,
    sotherExp = 0,
    grossExpense = 0,
    snetExpense = 0,
    sfrtType = 0,
    scommission = 0,
    totalPortExpenses = 0,
    dailyProfit = 0;


  const {
    cargo_details,
    co2_view,
    sale_freight,
    vessel_details,
    voyage_results,
    blastPortExpenses = 0,
    loadPortExpenses = 0,
    dischargePortExpenses = 0,
    reposPortExpenses = 0,
  } = state;

  const {
    tci_daily_cost,
    ballast_spd,
    laden_spd,
    port_days,
    vlsfo_cons_b,
    vlsfo_cons_l,
    bunker_oil_price,
    lsmgo_cons_l,
    lsmgo_cons_b,
    bunker_gas_price,
    tci_add_comm,
    tci_bro_comm,
  } = vessel_details;
  const { commission, cp_qty, frt_rate, frt_type, other_rev } = cargo_details;
  const { total_voyage_days, commence_date } = voyage_results;
  const { s_other_exp, s_cp_qty, s_frt_rate, s_frt_type, s_commission } =
    sale_freight;

  // Revenue calculation
  cpQty = cp_qty;
  frtRate = frt_rate;
  otherRev = other_rev;
  grossIncome = cpQty * frtRate + otherRev * 1;
  netIncome =
    cpQty * frtRate - cpQty * frtRate * commission * 0.01 + otherRev * 1;

  // expense calculation

  totalVoyageDays = total_voyage_days;
  vesselHire = tci_daily_cost * totalVoyageDays;

  // seadays calculation
  let seadays1 =
    blasttoLoadDistance && ballast_spd
      ? blasttoLoadDistance / (ballast_spd * 24)
      : 0;
  let seadays2 =
    loadtoDischargeDistance && ballast_spd
      ? loadtoDischargeDistance / (laden_spd * 24)
      : 0;
  let seadays3 =
    dischargetoReposDistance && ballast_spd
      ? dischargetoReposDistance / (ballast_spd * 24)
      : 0;

  seaDays = seadays1 + seadays2 + seadays3;
  totalVoyageDays = seaDays + Number(port_days);

  // bunker expense calculation (totalvlsfoprice +totallsmgoprice)

  // total vlsfo calculation (consumption and price)
  // totalvlsfo fuel consumption "formula": "((((BD/BS)+(RD/BS))/24)*BC) + (((LD/LS)/24)*LC)",

  let totalvlsfoCons =
    ((blasttoLoadDistance + dischargetoReposDistance) / (ballast_spd * 24)) *
      vlsfo_cons_b +
    (loadtoDischargeDistance / (laden_spd * 24)) * vlsfo_cons_l;
  let totalvlsfoPrice = totalvlsfoCons * bunker_oil_price;

  // total lsmgo calculation (consumption and price)
  // total lsmgo consumption "formula": "(((LD/LS)/24)*LC) + ((((BD/BS)+(RD/BS))/24)*BC)",

  let totallsmgocons =
    (loadtoDischargeDistance / (laden_spd * 24)) * lsmgo_cons_l +
    ((blasttoLoadDistance + dischargetoReposDistance) / (ballast_spd * 24)) *
      lsmgo_cons_b;

  let totallsmgoprice = totallsmgocons * bunker_gas_price;
  totalBunkerExpenses = totalvlsfoPrice + totallsmgoprice;


  // portexpense

  totalPortExpenses =
    Number(blastPortExpenses) +
    Number(loadPortExpenses) +
    Number(dischargePortExpenses) +
    Number(reposPortExpenses);

  // sale freight expense

  scpQty = s_cp_qty;
  sfrtRate = s_frt_rate;
  sotherExp = s_other_exp;
  sfrtType = s_frt_type;
  scommission = s_commission;
  if (sfrtType == 5) {
    snetExpense =
      scpQty * sfrtRate -
      scpQty * sfrtRate * scommission * 0.01 +
      sotherExp * 1;
  } else if (sfrtType == 10) {
    snetExpense = sfrtRate - sfrtRate * scommission * 0.01 + sotherExp * 1;
  } else {
    snetExpense =
      scpQty * sfrtRate -
      scpQty * sfrtRate * scommission * 0.01 +
      sotherExp * 1;
  }

  //"total expenseformula": "((VD*H)+MC-(((AP*H*VD)/100)+((BP*H*VD)/100)) + BE + PE+BB)",  pE=portexpense;

  vesselHire = isNaN(vesselHire) ? 0 : vesselHire;
  totalBunkerExpenses = isNaN(totalBunkerExpenses) ? 0 : totalBunkerExpenses;
  snetExpense = isNaN(snetExpense) ? 0 : snetExpense;

  totalExpenses =
    vesselHire * 1 -
    tci_add_comm * vesselHire * 0.01 -
    tci_bro_comm * vesselHire * 0.01 +
    totalBunkerExpenses * 1 +
    Number(totalPortExpenses) +
    snetExpense * 1;

  // profit-loss calculation

  netIncome = isNaN(netIncome) ? 0 : netIncome;
  totalExpenses = isNaN(totalExpenses) ? 0 : totalExpenses;
  totalProfit = netIncome - totalExpenses;
  dailyProfit = totalProfit / totalVoyageDays;

  let completedDate = nextdate(commence_date, totalVoyageDays);
  completedDate = dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss");

  return {
    seaDays,
    totalVoyageDays,
    grossIncome,
    netIncome,
    totalExpenses,
    totalProfit,
    totalPortExpenses,
    dailyProfit,
    completedDate,
  };
};

export const Co2Calculation = (
  state,
  group = null,
  name = null,
  value = null
) => {
  let blastSpeed = 0,
    ladenSpeed = 0,
    dwt = 0,
    vlsfoConsBlast = 0,
    vlsfoConsLaden = 0,
    lsmgoConsBlast = 0,
    lsmgoConsLaden = 0,
    vlsfofuelmultiplier = 1,
    lsmgofuelmultiplier = 1,
    ballast_port_eu_country,
    load_port_eu_country,
    discharge_port_eu_country,
    repos_port_eu_country,
    blasttoLoadDistance = 0,
    loadtoDischargeDistance = 0,
    dischargetoReposDistance = 0;
  blastSpeed = state?.vessel_details?.ballast_spd;
  ladenSpeed = state?.vessel_details?.laden_spd;
  dwt = state?.vessel_details?.dwt;
  vlsfoConsBlast = state?.vessel_details?.vlsfo_cons_b;
  vlsfoConsLaden = state?.vessel_details?.vlsfo_cons_l;
  lsmgoConsBlast = state?.vessel_details?.lsmgo_cons_b;
  lsmgoConsLaden = state?.vessel_details?.lsmgo_cons_l;
  vlsfofuelmultiplier = state?.co2_calc_value?.vlsfo_fuel_multiplier;
  lsmgofuelmultiplier = state?.co2_calc_value?.lsmgo_fuel_multiplier;
  ballast_port_eu_country = state?.co2_calc_value?.ballast_port_eu_country;
  load_port_eu_country = state?.co2_calc_value?.load_port_eu_country;
  discharge_port_eu_country = state?.co2_calc_value?.redelivery_port_eu_country;
  repos_port_eu_country = state?.co2_calc_value?.repos_port_eu_country;
  blasttoLoadDistance = state?.blasttoloadDistance;
  loadtoDischargeDistance = state?.loadTodischargeDistance;
  dischargetoReposDistance = state?.dischargetoreposDistance;

  if (group && name && value) {
    state = {
      ...state,
      [group]: {
        ...state[group],
        [name]: value,
      },
    };
  }

  let totalBunkerCons = 0,
    totalco2 = 0,
    totaleuts = 0,
    totalEmmission_mt = 0,
    totalDistance = 0,
    transportWork = 0,
    totalAttaindCii = 0;
  // ciiBand = "";

  const blastToloadConsValues = portToPortConsumption(
    blasttoLoadDistance,
    blastSpeed,
    vlsfoConsBlast,
    lsmgoConsBlast,
    vlsfofuelmultiplier,
    lsmgofuelmultiplier,
    ballast_port_eu_country,
    load_port_eu_country
  );
  const loadtoDischargeconsValues = portToPortConsumption(
    loadtoDischargeDistance,
    ladenSpeed,
    vlsfoConsLaden,
    lsmgoConsLaden,
    vlsfofuelmultiplier,
    lsmgofuelmultiplier,
    load_port_eu_country,
    discharge_port_eu_country
  );
  const dischargetoReposConsValues = portToPortConsumption(
    dischargetoReposDistance,
    blastSpeed,
    vlsfoConsBlast,
    lsmgoConsBlast,
    vlsfofuelmultiplier,
    lsmgofuelmultiplier,
    discharge_port_eu_country,
    repos_port_eu_country
  );

  totalBunkerCons =
    blastToloadConsValues.totalBunkerConsumption +
    loadtoDischargeconsValues.totalBunkerConsumption +
    dischargetoReposConsValues.totalBunkerConsumption;
  totalco2 =
    blastToloadConsValues.totalco2emmited +
    loadtoDischargeconsValues.totalco2emmited +
    dischargetoReposConsValues.totalco2emmited;
  totalEmmission_mt =
    blastToloadConsValues.totalco2emmited_mt +
    loadtoDischargeconsValues.totalco2emmited_mt +
    dischargetoReposConsValues.totalco2emmited_mt;

  totalDistance =
    blasttoLoadDistance + loadtoDischargeDistance + dischargetoReposDistance;
  transportWork = totalDistance * dwt;

  totalAttaindCii = (totalco2 / transportWork).toFixed(2);
  // ciiBand = CIIbandCalculator(totalAttaindCii);

  totaleuts =
    blastToloadConsValues.totaleuts +
    loadtoDischargeconsValues.totaleuts +
    dischargetoReposConsValues.totaleuts;
  return {
    totalBunkerCons,
    totalco2: totalco2 / 1000000,
    totaleuts,
    totalEmmission_mt,
    totalDistance,
    transportWork,
    totalAttaindCii,
    eeoi: totalAttaindCii,
  };
};

const portToPortConsumption = (
  portToPortDistance = 0,
  Speed = 0,
  vlsfoConsBlast = 0,
  lsmgoConsBlast = 0,
  vlsfomultiplier = 1,
  lsmgomultiplier = 1,
  prevEu_country = false,
  currentEu_country = false
) => {
  let seadays =
    portToPortDistance && Speed ? portToPortDistance / (Speed * 24) : 0;
  let vlsfocons = seadays * vlsfoConsBlast;
  let lsmgoCons = seadays * lsmgoConsBlast;
  let totalBunkerConsumption = vlsfocons + lsmgoCons;
  let totalco2emmited_mt =
    vlsfocons * vlsfomultiplier + lsmgoCons * lsmgomultiplier;
  let totalco2emmited = totalco2emmited_mt * 1000000;
  let totaleuts = 0;

  if (prevEu_country && currentEu_country) {
    totaleuts = totalco2emmited_mt * 1 * 0.4;
  } else {
    totaleuts = totalco2emmited_mt * 0.5 * 0.4;
  }

  return {
    totalBunkerConsumption,
    totalco2emmited,
    totalco2emmited_mt,
    totaleuts,
  };
};

// const CIIbandCalculator = (cii) => {
//   if (cii < 4) {
//     return { type: "A", color: "#28B463" };
//   } else if (cii > 4 && cii <= 9) {
//     return { type: "B", color: "#82E0AA" };
//   } else if (cii > 9 && cii <= 16) {
//     return { type: "C", color: "#F9E79F" };
//   } else if (cii > 16 && cii <= 23) {
//     return { type: "D", color: "#F39C12" };
//   } else if (cii > 23) {
//     return { type: "E", color: "#FF0000" };
//   }
// };

export const cargoList = async () => {
  const request = await getAPICall(
    `${URL_WITH_VERSION}/master/list?t=carli&p=1&l=0`
  );
  const respdata = await request;
  const cargoarr = [];
  respdata.data.map((el) => {
    let obj = {
      value: el.id,
      label: el.full_name,
    };
    cargoarr.push(obj);
  });
  return cargoarr;
};
