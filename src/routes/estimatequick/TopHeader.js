import React, { useEffect, useState } from "react";
import { Menu, Tooltip, Select } from "antd";
import {
  AppstoreOutlined,
  DeleteOutlined,
  LockOutlined,
  MailOutlined,
  PlusOutlined,
  RestOutlined,
  SaveOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const { SubMenu } = Menu;

const menuChildren = [
  {
    label: "Go to Full estimate",
    key: "Full_estimate",
  },
  {
    label: "Copy New Estimate",
    key: "copy_estimate",
  },
  {
    label: "Bunker Optimization",
    key: "bunker_optimization",
  },

  {
    label: "Route Optimization",
    key: "route_optimization",
  },

  {
    label: "CO2 Optimization",
    key: "co2_optimization",
  },

  {
    label: "Bunker Plan",
    key: "bunker_plan",
  },

  {
    label: "Speed Optimization",
    key: "speed_optimization",
  },

  {
    label: "Weather Optimization",
    key: "weather_optimization",
  },
];

const TopHeader = ({
  handleadd,
  handlecopy,
  handledelete,
  saveformdata,
  handlereset,
  quickestimateid,
  formdata,
  extraMenu,
}) => {
  const navigate = useNavigate();

  const handleChange = (value) => {
    if (value == "tc_estimate") {
      navigate("/add-TC-estimate");
    } else if (value == "voy_relet") {
      navigate("/voy-relet");
    } else if (value == "copy_estimate") {
      handlecopy();
    } else if (value == "voyage_estimate") {
      navigate("/quick-estimate");
    }
  };

  const handleFullEstimate = () => {
        if (quickestimateid && quickestimateid.includes("TCE")) {
      if (formdata["full_estimate_id"]) {
        navigate(`/edit-voyage-estimate/${formdata["full_estimate_id"]}`, {
          state: { data: formdata },
        });
      } else {
        navigate(`/add-voyage-estimate`, {
          state: { data: formdata },
        });
      }
    } else if (quickestimateid && quickestimateid.includes("TCR")) {
      navigate(`/tc-est-fullestimate`, {
        state: { data: formdata },
      });
    } else if (quickestimateid && quickestimateid.includes("VOY")) {
      navigate(`/voy-relet-full-estimate-edit/${quickestimateid}`, {
        state: { data: formdata },
      });
    }
  };

  const handleAI = () => {};

  return (
    <div className="full-width">
      <div
        className="ant-row css-1ck3jst"
        style={{ display: "flex", alignItems: "baseline" }}
      >
        <div
          className="ant-col ant-col-xs-24 ant-col-sm-8 ant-col-md-5 ant-col-lg-5 ant-col-xl-5 css-1ck3jst"
          style={{ width: "50%" }}
        >
          <span className="wrap-bar-menu">
            <ul className="wrap-bar-ul">
              <li>
                <span
                  role="img"
                  aria-label="close"
                  className="anticon anticon-close"
                >
                  <Tooltip title="Add New">
                    <PlusOutlined onClick={() => handleadd()} />
                  </Tooltip>
                </span>
              </li>
              <li>
                <span
                  role="img"
                  aria-label="save"
                  className="anticon anticon-save"
                >
                  <Tooltip title="Save">
                    <SaveOutlined
                      onClick={() => saveformdata()}
                      style={{ width: "1em", height: "1em" }}
                    />
                  </Tooltip>
                </span>
              </li>

              <li>
                <span
                  role="img"
                  aria-label="delete"
                  className="anticon anticon-delete"
                >
                  <Tooltip title="Lock">
                    <LockOutlined style={{ width: "20px", height: "20px" }} />
                  </Tooltip>
                </span>
              </li>
              <li>
                <span
                  role="img"
                  aria-label="close"
                  className="anticon anticon-close"
                >
                  <Tooltip title="Delete">
                    <DeleteOutlined
                      onClick={() => handledelete()}
                      style={{ width: "20px", height: "20px" }}
                    />
                  </Tooltip>
                </span>
              </li>

              <li>
                <span
                  role="img"
                  aria-label="close"
                  className="anticon anticon-close"
                >
                  <Tooltip title="Reset">
                    <RestOutlined
                      onClick={() => handlereset()}
                      style={{ width: "20px", height: "20px" }}
                    />
                  </Tooltip>
                </span>
              </li>
            </ul>
          </span>
        </div>
        <div
          className="ant-col ant-col-xs-24 ant-col-sm-16 ant-col-md-19 ant-col-lg-19 ant-col-xl-19 css-1ck3jst"
          style={{ width: "50%" }}
        >
          <div className="ant-row ant-row-end css-1ck3jst">
            <span
              className="wrap-bar-menu1"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                fontSize: "13px",
                fontWeight: "600",
              }}
            >
              <span
                style={{ borderRight: "1px solid black", cursor: "pointer" }}
                onClick={() => handleAI()}
              >
                AI&nbsp;
              </span>

              <span
                style={{
                  borderRight: "1px solid black",
                  cursor: "pointer",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onClick={() => handleFullEstimate()}
              >
                Full Estimate &nbsp;
              </span>

              <span>
                <Menu
                  mode="horizontal"
                  expandIcon={null}
                  onClick={({ key }) => handleChange(key)}
                  items={[
                    {
                      label: "Menu",
                      key: "menu",
                      children: [...extraMenu, ...menuChildren],
                    },
                  ]}
                />
              </span>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopHeader;
