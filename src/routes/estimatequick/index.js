import React from "react";
import { useLocation } from "react-router-dom";
import VoyageEstimateQuick from "./VoyageEstimateQuick";
import TCEstimateQuick from "./TCEstimateQuick";
import VoyageReletQuick from "./VoyageReletQuick";
const Index = () => {
  const location = useLocation();

  if ((location.pathname =="/quick-estimate")) {
    return <VoyageEstimateQuick />;
  } else if ((location.pathname == "/add-TC-estimate")) {
    return <TCEstimateQuick />;
  } else if ((location.pathname == "/voy-relet")) {
    return <VoyageReletQuick />;
  }
};

export default Index;
