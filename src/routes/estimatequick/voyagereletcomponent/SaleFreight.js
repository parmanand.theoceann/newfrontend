import React, { useState, useRef, useEffect } from "react";

import { Row, Col, Input, Select, InputNumber } from "antd";

const SaleFreight = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  handleselectCargo,
  cargoarr,
}) => {
    return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Cargo Name :
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Select
              value={formdata.sale_freight.frt_type}
              onChange={(selectedOption) =>
                handleselectCargo(selectedOption, "s_cargo")
              }
              options={cargoarr}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            CP Qty/ Unit :
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={14} style={{ borderRight: "1px solid black" }}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="s_cp_qty"
                  onChange={(e) =>
                    handleChange("sale_freight", "s_cp_qty", e.target.value)
                  }
                  value={formdata.sale_freight.s_cp_qty}
                />
              </Col>
              <Col span={10}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  disabled
                  name="s_cp_qty_type"
                  onChange={(e) =>
                    handleChange("sale_freight", "s_cp_qty_type", e.target.value)
                  }
                  value={formdata.sale_freight.s_cp_qty_type}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Freight Type/Unit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={14} style={{borderRight:"1px solid black"}}>
                <Select
                  value={formdata.sale_freight.s_frt_type}
                  onChange={(selectedOption) =>
                    handleselectCargo(selectedOption, "s_frt_type")
                  }
                  options={[
                    {
                      value:5,
                      label: "Frt Rate",
                    },
                    {
                      value: 10,
                      label: "Lumpsum",
                    },
                    {
                      value: 11,
                      label: "Worldscale",
                    },
                  ]}
                />
              </Col>
              <Col span={10} style={{ marginTop: "auto" }}>
                <Input
                  disabled
                  name="s_frt_unit"
                  onChange={(value) =>
                    handleChange("sale_freight", "s_frt_unit", value)
                  }
                  value={formdata.sale_freight.s_frt_unit}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Freight rate :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="s_frt_rate"
              onChange={(e) =>
                handleChange("sale_freight", "s_frt_rate", e.target.value)
              }
              value={formdata.sale_freight.s_frt_rate}
              type={"number"}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Commission% :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="s_commission"
              onChange={(e) =>
                handleChange("sale_freight", "s_commission", e.target.value)
              }
              type={"number"}
              placeholder="0.00"
              value={formdata.sale_freight.s_commission}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Other Exp :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="s_other_exp"
              onChange={(e) =>
                handleChange("sale_freight", "s_other_exp", e.target.value)
              }
              type={"number"}
              placeholder="0.00"
              value={formdata.sale_freight.s_other_exp}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default  SaleFreight;
