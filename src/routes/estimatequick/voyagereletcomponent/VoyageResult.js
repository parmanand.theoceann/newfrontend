import React, { useState, useRef, useEffect } from "react";
import { Row, Col, Input, DatePicker } from "antd";

import moment from "moment";
import dayjs from "dayjs";

const Voyageresult = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  handleDate,
}) => {
  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Gross Income :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="gross_income"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "gross_income", value)
              }
              value={formdata.voyage_results.gross_income}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Net Income :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="net_income"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "net_income", value)
              }
              value={formdata.voyage_results.net_income}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Port expenses :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="total_port_expenses"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "total_port_expenses", value)
              }
              value={formdata.voyage_results.total_port_expenses}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Other expenses :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="other_expenses"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "other_expenses", value)
              }
              value={formdata.voyage_results.other_expenses}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Total Expense :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="total_expenses"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "total_expenses", value)
              }
              value={formdata.voyage_results.total_expenses}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        {/* <Row>
          <Col className="bold" span={10}>
            Vessel Hire:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="vessel_hire"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "vessel_hire", value)
              }
              value={formdata.voyage_results.vessel_hire}
              placeholder="Vessel Hire"
            />
          </Col>
        </Row> */}

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Total Profit/Loss :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="total_profit"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "total_profit", value)
              }
              value={formdata.voyage_results.total_profit}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Daily Profit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="daily_profit"
              disabled
              onChange={(value) =>
                handleChange("voyage_results", "daily_profit", value)
              }
              value={formdata.voyage_results.daily_profit}
              type={'number'}
              placeholder="0.00"
            />
          </Col>
        </Row>

        {/* <Row>
          <Col className="bold" span={10}>
            TCE:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              name="tce"
              disabled
              onChange={(value) => handleChange("voyage_results", "tce", value)}
              value={formdata.voyage_results.tce}
              placeholder="Total Expenses"
            />
          </Col>
        </Row> */}

<Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Commenced date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
              showTime={{ format: "HH:mm:ss" }}
              picker="date"
              format="YYYY-MM-DD HH:mm:ss"
              style={{ width: "100%" }}
              name="commence_date"
              onChange={(date, dateString) =>
                handleselect(dayjs(dateString), "commence_date")
              }
              value={dayjs(
                formdata.voyage_results.commence_date,
                "YYYY-MM-DD HH:mm:ss"
              )}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Completed date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
              format="YYYY-MM-DD HH:mm:ss"
              showTime
              style={{ width: "100%" }}
              name="completed_date"
              onChange={(date, dateString) =>
                handleselect(moment(dateString), "completed_date")
              }
              value={moment(
                formdata.voyage_results.completed_date,
                "YYYY-MM-DD HH:mm:ss"
              )}
              disabled
            />
          </Col>
        </Row>

        {/* <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Port days/Sea days:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row gutter={8}>
              <Col span={12}>
              <Input
                  placeholder="Port days"
                  name="port_days"
                  onChange={(value) =>
                    handleChange("voyage_results", "port_days", value)
                  }
                  value={formdata.voyage_results.port_days}
                />
              </Col>
              <Col span={12}>
              <Input
                  placeholder="sea days"
                  name="sea_days"
                  onChange={(value) =>
                    handleChange("voyage_results", "sea_days", value)
                  }
                  value={formdata.voyage_results.sea_days}
                />
              </Col>
            </Row>
          </Col>
        </Row> */}

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Total Voyage days :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Input
              type={'number'}
              placeholder="0.00"
              onChange={(e) =>
                handleChange("voyage_results", "total_voyage_days", e.target.value)
              }
              name="total_voyage_days"
              value={formdata.voyage_results.total_voyage_days}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Voyageresult;
