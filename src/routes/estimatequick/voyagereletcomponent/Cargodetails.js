import React, { useState, useRef, useEffect } from "react";
import { Row, Col, Input, Select, InputNumber } from "antd";
import URL_WITH_VERSION, { getAPICall } from "../../../shared";

const Cargodetails = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  handleselectCargo,
  cargoarr,
}) => {
  const [ownerData, setOwnerData] = useState([]);

  useEffect(() => {
    getOwnerList();
  }, []);

  const getOwnerList = async () => {
    const response = await getAPICall(
      `${URL_WITH_VERSION}/address/list?p=1&l=0`
    );
    const respdata = await response;

    let arr = [];
    respdata.data.map((el) => {
      const { id, full_name } = el;
      let obj = {
        value: id,
        label: full_name,
      };
      arr.push(obj);
    });

    setOwnerData([...arr]);
  };

    return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Cargo Name :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              
              value={formdata.cargo_details.cargo}
              onChange={(selectedOption) =>
                handleselectCargo(selectedOption, "cargo")
              }
              options={cargoarr}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            CP Qty/ Unit :
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Row >
              <Col span={14} style={{borderRight:"1px solid black"}}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="cp_qty"
                  onChange={(e) =>
                    handleChange("cargo_details", "cp_qty", e.target.value)
                  }
                  value={formdata.cargo_details.cp_qty}
                />
              </Col>
              <Col span={10}>
                <Input
                  type={"number"}
                  placeholder="cp qty unit"
                  disabled
                  name="cp_qty_type"
                  onChange={(e) =>
                    handleChange("cargo_details", "cp_qty_type", e.target.value)
                  }
                  value={formdata.cargo_details.cp_qty_type}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Freight Type/Unit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row >
              <Col span={14} style={{borderRight:"1px solid black"}}>
                <Select
                  style={{
                    //width: 156,
                  }}
                  value={formdata.cargo_details.frt_type}
                  onChange={(selectedOption) =>
                    handleselectCargo(selectedOption, "frt_type")
                  }
                  options={[
                    {
                      value: 5,
                      label: "Frt Rate",
                    },
                    {
                      value: 10,
                      label: "Lumpsum",
                    },
                    {
                      value: 11,
                      label: "WorldScale",
                    },
                  ]}
                />
              </Col>
              <Col span={10} style={{marginTop:'auto'}}>
                <Input
                  disabled
                  name="frt_unit"
                  onChange={(value) =>
                    handleChange("cargo_details", "frt_unit", value)
                  }
                  value={formdata.cargo_details.frt_unit}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Freight rate :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="frt_rate"
              onChange={(e) =>
                handleChange("cargo_details", "frt_rate", e.target.value)
              }
              value={formdata.cargo_details.frt_rate}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Commission% :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              type={"number"}
              name="0.00"
              onChange={(e) =>
                handleChange("cargo_details", "commission", e.target.value)
              }
              placeholder="commission"
              value={formdata.cargo_details.commission}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Other Rev :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              type={"number"}
              name="other_rev"
              onChange={(e) =>
                handleChange("cargo_details", "other_rev", e.target.value)
              }
              placeholder="Other Revenue"
              value={formdata.cargo_details.other_rev}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Owner :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              
              value={formdata.cargo_details.owner}
              onChange={(selectedOption) =>
                handleselectCargo(selectedOption, "owner")
              }
              options={ownerData}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default  Cargodetails;
