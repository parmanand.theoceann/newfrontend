import React, { useState, useRef, useEffect } from "react";

import {
  Row,
  Col,
  Input,
  Select,
  InputNumber,
  Modal,
  Button,
  Tooltip,
} from "antd";
import { SearchOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import PortSelection from "../../port-selection/PortSelection";
import VesselDatabaseModal from "../vesselsearch/VesselDatabaseModal";
import PortModalForm from "../components/PortModalForm";
import { useQuickEstimateContext } from "../quickEstimateContext";

const VesselDetailQuick = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  openModal,
  updateQuickFromdatabase,
  modalCloseEvent,
  updatequickfrom,
  localvesseldata,
  handleSelectVesselNameFromDropDown,
}) => {
  const {
    ladenSpeedDropDown,
    selectedBallastSpeed,
    ballastSpeedDropDown,
    selectedLadenSpeed,
    setSelectedLadenSpeed,
    setSelectedBallastSpeed,
  } = useQuickEstimateContext();

  const vesselDropDownOptions = localvesseldata?.map((el, indx) => ({
    label: `${el.vessel_name} DWT ${el.vessel_dwt} IMO ${el.imo_no} ${el.vessel_type_name} BS spd ${el.spd_ballast} LS spd ${el.spd_laden}`,
    value: el.vessel_id,
    key: indx,
  }));

  const filterOption = (input, option) =>
    (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

  const handleSelectLadenSpeed = (value, option) => {
  
    handleChange("vessel_details", "laden_spd", value);
    setSelectedLadenSpeed(option);
    console.log("option", option);
    handleselect(
      formdata.vessel_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      true,
      false
    );
    handleselect(
      formdata.vessel_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      true,
      false
    );
  };

  const handleSelectBallastSpeed = (value, option) => {
    handleChange("vessel_details", "ballast_spd", value);
    setSelectedBallastSpeed(option);
    handleselect(
      formdata.vessel_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      false,
      true
    );
    handleselect(
      formdata.vessel_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      false,
      true
    );
  };
  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            <a
              onClick={() => handleOpenModal("openVesselModalForm", true)}
              style={{ textDecoration: "none" }}
              className="required-label"
            >
              Vessel Name
            </a>
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              showSearch
              placeholder="Select a Vessel"
              optionFilterProp="children"
              onChange={handleSelectVesselNameFromDropDown}
              value={formdata.vessel_details.vessel_id}
              options={vesselDropDownOptions}
              dropdownStyle={{ width: "450px" }}
              filterOption={filterOption}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            TCI Daily Cost:
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Input
              type={"number"}
              placeholder="0.00"
              name="tci_daily_cost"
              onChange={(e) =>
                handleChange("vessel_details", "tci_daily_cost", e.target.value)
              }
              value={formdata.vessel_details.tci_daily_cost}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            TCI Add /Bro com % :
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Row gutter={8}>
              <Col span={12}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="tci_add_com"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "tci_add_comm",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.tci_add_comm}
                />
              </Col>
              <Col span={12}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "tci_bro_comm",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.tci_bro_comm}
                  name="tci_bro_comm"
                />
              </Col>
            </Row>
          </Col>
        </Row>

        {/* <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            TCI BB/Other Cost:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={11}>
                <Input
                  placeholder="blast bonus"
                  name="blast_bonus"
                  onChange={(value) =>
                    handleChange("vessel_details", "blast_bonus", value)
                  }
                  value={formdata.vessel_details.blast_bonus}
                />
              </Col>
              <Col span={11}>
                <Input
                  placeholder="other cost"
                  name="other_cost"
                  onChange={(value) =>
                    handleChange("vessel_details", "other_cost", value)
                  }
                  value={formdata.vessel_details.other_cost}
                />
              </Col>
            </Row>
          </Col>
        </Row> */}

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Speed L/B:
          </Col>
          <Col span={14} style={{ paddingRight: 10 }}>
            <Row gutter={8}>
              <Col span={12}>
                <Select
                  placeholder="0.00"
                  type={"number"}
                  name="laden_spd"
                  onChange={handleSelectLadenSpeed}
                  value={selectedLadenSpeed}
                  options={ladenSpeedDropDown}
                />
              </Col>
              <Col span={12}>
                <Select
                  placeholder="0.00"
                  name="ballast_spd"
                  type={"number"}
                  onChange={handleSelectBallastSpeed}
                  value={selectedBallastSpeed}
                  options={ballastSpeedDropDown}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Consp. Fuel per MT/D:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <div>
              <Row gutter={16}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    Grade
                  </div>
                </Col>
                <Col className="gutter-row" span={10}>
                  <Select
                    style={{
                      width: 120,
                    }}
                    value={formdata.vessel_details.vlsfo_fuel}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "vlsfo_fuel")
                    }
                    options={[
                      {
                        value: 5,
                        label: "VLSFO",
                      },
                      {
                        value: 10,
                        label: "ULSFO",
                      },
                      {
                        value: 2,
                        label: "IFO",
                      },
                    ]}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Select
                    style={{
                      width: 120,
                    }}
                    value={formdata.vessel_details.lsmgo_fuel}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "lsmgo_fuel")
                    }
                    name="lsmgo_fuel"
                    options={[
                      {
                        value: 7,
                        label: "LSMGO",
                      },
                      {
                        value: 4,
                        label: "MGO",
                      },
                    ]}
                  />
                </Col>
              </Row>

              <Row gutter={8} style={{ marginTop: "5px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    B
                  </div>
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="vlsfo_cons_b"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "vlsfo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.vlsfo_cons_b}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="lsmgo_cons_b"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "lsmgo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.lsmgo_cons_b}
                  />
                </Col>
              </Row>

              <Row style={{ marginTop: "5px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                    }}
                  >
                    L
                  </div>
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="vlsfo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "vlsfo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.vlsfo_cons_l}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Input
                    type={"number"}
                    placeholder="0.00"
                    name="lsmgo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "lsmgo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.lsmgo_cons_l}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Ballast Port:
          </Col>
          <Col span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openBalastModal", true)}
              name="ballast_port"
              value={formdata.vessel_details.ballast_port}
            />
            <Modal
              open={openModal.openBalastModal}
              onCancel={() => handleOpenModal("openBalastModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={null}
                modalCloseEvent={(data) => {
                  handleOpenModal("openBalastModal", false);
                  modalCloseEvent("ballast_port", data);
                }}
              />
            </Modal>
          </Col>

          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openBalastModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.ballast_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            <Modal
              open={openModal.openBalastModalForm}
              onCancel={() => handleOpenModal("openBalastModalForm", false)}
              footer={null}
              width="75%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openBalastModalForm", false);

                  updatequickfrom("blastformdata", data);
                }}
                formdata={formdata.blastformdata}
                port={formdata.vessel_details.ballast_port}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Load Port:
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openLoadModal", true)}
              name="load_port"
              value={formdata.vessel_details.load_port}
            />

            <Modal
              open={openModal.openLoadModal}
              onCancel={() => handleOpenModal("openLoadModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.ballast_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openLoadModal", false);
                  modalCloseEvent("load_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openLoadModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.load_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>
            <Modal
              open={openModal.openLoadModalForm}
              onCancel={() => handleOpenModal("openLoadModalForm", false)}
              footer={null}
              width="75%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openLoadModalForm", false);
                  updatequickfrom("loadformdata", data);
                }}
                port={formdata.vessel_details.load_port}
                formdata={formdata.loadformdata}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Discharge Port:
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openDischargeModal", true)}
              name="discharge_port"
              value={formdata.vessel_details.discharge_port}
            />
            <Modal
              open={openModal.openDischargeModal}
              onCancel={() => handleOpenModal("openDischargeModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.load_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openDischargeModal", false);
                  modalCloseEvent("discharge_port", data);
                }}
              />
            </Modal>
          </Col>

          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openDischargeModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.discharge_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            {openModal.openDischargeModalForm && (
              <Modal
                open={openModal.openDischargeModalForm}
                onCancel={() =>
                  handleOpenModal("openDischargeModalForm", false)
                }
                footer={null}
                width="75%"
                maskClosable={false}
              >
                <PortModalForm
                  updateform={(data) => {
                    handleOpenModal("openDischargeModalForm", false);

                    updatequickfrom("dischargeformdata", data);
                  }}
                  port={formdata.vessel_details.discharge_port}
                  formdata={formdata.dischargeformdata}
                />
              </Modal>
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Repos Port:
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openReposModal", true)}
              name="repos_port"
              value={formdata.vessel_details.repos_port}
            />
            <Modal
              open={openModal.openReposModal}
              onCancel={() => handleOpenModal("openReposModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.discharge_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openReposModal", false);
                  modalCloseEvent("repos_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openReposModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.repos_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            <Modal
              open={openModal.openReposModalForm}
              onCancel={() => handleOpenModal("openReposModalForm", false)}
              footer={null}
              width="75%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openReposModalForm", false);

                  updatequickfrom("reposformdata", data);
                }}
                port={formdata.vessel_details.repos_port}
                formdata={formdata.reposformdata}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Routing:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              bordered={false}
              suffixIcon={null}
              style={{
                width: 180,
                borderBottom: "1px solid black",
              }}
              value={formdata?.vessel_details?.routing}
              onChange={(selectedOption) =>
                handleselect(selectedOption, "routing")
              }
              options={[
                {
                  key: 1,
                  value: "1",
                  label: "ECA",
                },
                {
                  key: 2,
                  value: "2",
                  label: "Piracy",
                },
              ]}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Bunker Price $/MT:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row gutter={8}>
              <Col className="gutter-row" span={12}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="bunker_oil_price"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "bunker_oil_price",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.bunker_oil_price}
                />
              </Col>
              <Col className="gutter-row" span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="bunker_gas_price"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "bunker_gas_price",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.bunker_gas_price}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Port days/Sea days:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="port_days"
                  onChange={(e) =>
                    handleChange("vessel_details", "port_days", e.target.value)
                  }
                  value={formdata.vessel_details.port_days}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="sea_days"
                  onChange={(e) =>
                    handleChange("vessel_details", "sea_days", e.target.value)
                  }
                  value={formdata.vessel_details.sea_days}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        {/* <Row style={{ marginTop: "10px" }}>
          <Col className="bold" span={10}>
            Total Voyage days:
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              type="text"
              placeholder="Total Voyage days"
              onChange={(value) =>
                handleChange("vessel_details", "total_voyage_days", value)
              }
              name="total_voyage_days"
              value={formdata.vessel_details.total_voyage_days}
            />
          </Col>
        </Row> */}
      </Col>

      {openModal.openLocalVesselModal && (
        <Modal
          open={openModal.openLocalVesselModal}
          footer={null}
          onCancel={() => handleOpenModal("openLocalVesselModal", false)}
        >
          <VesselDatabaseModal
            type="local"
            updateform={(data, fulldata) =>
              updateQuickFromdatabase("local", data, fulldata)
            }
          />
        </Modal>
      )}

      {openModal.openGlobalVesselModal && (
        <Modal
          open={openModal.openGlobalVesselModal}
          onCancel={() => handleOpenModal("openGlobalVesselModal", false)}
          footer={null}
        >
          <VesselDatabaseModal
            type="global"
            updateform={(data) => updateQuickFromdatabase("global", data)}
          />
        </Modal>
      )}
    </Row>
  );
};

export default VesselDetailQuick;
