import React, { useState, useRef, useEffect } from "react";

import { Row, Col, InputNumber, Input } from "antd";

const Co2view = ({ handleChange, handleselect, formdata, handleOpenModal }) => {
  const formatInputValue = (value) => {
    // Your formatting logic here
    // This example limits to 2 decimal places
    return String(value)
      .replace(/[^0-9.]/g, "") // Remove non-numeric characters except dot
      .replace(/(\..*?)\..*/g, "$1") // Remove extra dots
      .replace(/^(\d*\.\d{2}).*$/g, "$1"); // Limit to 2 decimal places
  };

  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Bunker cons(Mt) :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="total_bunker_cons"
              onChange={(e) =>
                handleChange("co2_view", "total_bunker_cons", e.target.value)
              }
              value={formdata.co2_view.total_bunker_cons}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total CO2 (Mt) :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="total_co2"
              onChange={(e) =>
                handleChange("co2_view", "total_co2", e.target.value)
              }
              value={formdata?.co2_view?.total_co2}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            EU ETS Co2(MT) :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="euets"
              onChange={(e) =>
                handleChange("co2_view", "euets", e.target.value)
              }
              value={formdata.co2_view.euets}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Emmission(Mt) :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="emmission_mt"
              onChange={(e) =>
                handleChange("co2_view", "emmission_mt", e.target.value)
              }
              value={formdata.co2_view.emmission_mt}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            DWT :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="dwt"
              onChange={(e) =>
                handleChange("co2_view", "co2_dwt", e.target.value)
              }
              value={formdata.co2_view.co2_dwt}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Distance :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="total_distance"
              onChange={(e) =>
                handleChange("co2_view", "total_distance", e.target.value)
              }
              value={formdata.co2_view.total_distance}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Transport Work :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="transport_work"
              onChange={(e) =>
                handleChange("co2_view", "transport_work", e.target.value)
              }
              value={formatInputValue(formdata.co2_view.transport_work)}
              placeholder="0.00"
              addonAfter={"ton-miles"}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Attained CII :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="attained_cii"
              onChange={(e) =>
                handleChange("co2_view", "attained_cii", e.target.value)
              }
              value={formdata.co2_view.attained_cii}
              placeholder="0.00"
              addonAfter={"g/ton-mile"}
            />
          </Col>
        </Row>

        {/* <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            CII Band:
          </Col>
          <Col span={4} style={{ paddingRight: 10 }}>
            <Input
              name="cii_band"
              onChange={(value) => handleChange("co2_view", "cii_band", value)}
              value={formdata.co2_view.cii_band}
              placeholder="CII Band"
              style={{
                backgroundColor: formdata.co2_calc_value.ciibandcolor,
                fontSize: "14px",
                textAlign: "center",
                fontWeight: "bold",
              }}
            />
          </Col>
        </Row> */}

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            EEOI :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              disabled={true}
              name="eeoi"
              onChange={(e) => handleChange("co2_view", "eeoi", e.target.value)}
              value={formdata.co2_view.eeoi}
              placeholder="0.00"
              addonAfter={"g/ton-miles"}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Co2view;
