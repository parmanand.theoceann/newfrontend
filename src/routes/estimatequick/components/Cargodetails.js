import React, { useState, useRef, useEffect } from "react";

import { Row, Col, Input, Select, InputNumber } from "antd";

const Cargodetails = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  handleselectCargo,
}) => {
  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            CP Qty/ Unit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={16}>
                <Input
                  type={"number"}
                  placeholder="0.00"
                  name="cp_qty"
                  onChange={(e) =>
                    handleChange("cargo_details", "cp_qty", e.target.value)
                  }
                  value={formdata.cargo_details.cp_qty}
                />
              </Col>
              <Col span={8}>
                <Input
                  placeholder="0.00"
                  disabled
                  name="cp_qty_type"
                  addonAfter={"MT"}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Freight Type/Unit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={16}>
                <Select
                  bordered={false}
                  style={{
                    // width: 177,
                    borderBottom: "1px solid black",
                  }}
                  value={formdata.cargo_details.frt_type}
                  onChange={(selectedOption) =>
                    handleselectCargo(selectedOption, "frt_type")
                  }
                  options={[
                    {
                      value: 38,
                      label: "Frt Rate",
                    },
                    {
                      value: 104,
                      label: "Lumpsum",
                    },
                  ]}
                />
              </Col>
              <Col span={8} style={{marginTop:'auto'}}>
                <Input
                  disabled
                  name="frt_unit"
                  addonAfter={"MT"}
                 
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Freight rate :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="frt_rate"
              onChange={(e) =>
                handleChange("cargo_details", "frt_rate", e.target.value)
              }
              value={formdata.cargo_details.frt_rate}
              placeholder="0.00"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Commission% :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="commission"
              onChange={(e) =>
                handleChange("cargo_details", "commission", e.target.value)
              }
              placeholder="0.00"
              value={formdata.cargo_details.commission}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Cargodetails;
