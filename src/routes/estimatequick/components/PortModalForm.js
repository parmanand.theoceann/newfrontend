import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Input,
  Select,
  Layout,
  Typography,
  InputNumber,
  DatePicker,
  Divider,
  Button,
} from "antd";
import Moment from "moment";
import dayjs from "dayjs";
import NormalFormIndex from "../../../shared/NormalForm/normal_from_index";
import { useStateCallback } from "../../../shared";
import { SaveOutlined } from "@ant-design/icons";
const { Content } = Layout;

const initform = {
  frmName: "quick_estimate_port_form",
  frmVisible: true,
  port_name: "",
  port_func: "1",
  crane_use: "1",
  ld_qty_unit: "",
  ld_rate_per_day: "",
  ld_rate_per_hour: "",
  term: "1",
  turn_time: 0,
  port_days: 0,
  xpd: 0,
  id: null,
  estimate_port_days: 0,
  dem_rate_perday: 0,
  des_rate_perday: 0,
  dem_final_day: 0,
  des_final_day: 0,
  dem_final_amount: 0,
  des_final_amount: 0,
  laycan_from: dayjs(),
  laycan_to: dayjs(),
  draft: 0,
  salinity: 0,
  max_lift_Qty: "",
};

const PortModalForm = (props) => {
    const [state, setState] = useStateCallback({
    frmName: "quick_estimate_port_form",
    frmVisible: true,
    formdata: {
     
      ...props.formdata,
      port_name: props.port,
    },
  });

  // useEffect(() => {
  //   setState(prev => ({
  //     ...prev, formData : {...prev.formData,  port_name: props.port,
  //       // port_func: formdata.port_func ?? "",
  //       // crane_use: formdata.crane_use ?? "",
  //        ld_qty_unit: props.formdata.ld_qty_unit ?? "",
  //        ld_rate_per_day: props.formdata.ld_rate_per_day ?? "",
  //        ld_rate_per_hour: props.formdata.ld_rate_per_hour ?? "",
  //       // term: formdata.term ?? "",
  //        turn_time: props.formdata.turn_time ?? '0',
  //        port_days: props.formdata.port_days ?? '0',
  //        xpd: props.formdata.xpd ?? '0',
  //        id: props.formdata.id ?? null,
  //        estimate_port_days: props.formdata.estimate_port_days ?? '0',
  //        dem_rate_perday: props.formdata.dem_rate_perday ?? '0',
  //        des_rate_perday: props.formdata.des_rate_perday ?? '0',
  //        dem_final_day: props.formdata.dem_final_day ?? '0',
  //        des_final_day: props.formdata.des_final_day ?? '0',
  //        dem_final_amount: props.formdata.dem_final_amount ?? '0',
  //        des_final_amount: props.formdata.des_final_amount ?? '0',

  //        draft: props.formdata.draft ?? '0',
  //        salinity: props.formdata.salinity ?? '0',
  //        max_lift_Qty: props.formdata.max_lift_Qty ?? "",
  //  }
  //   }));
  // }, []);

  const handleChange = (name, value) => {
    setState({ ...state, [name]: value });
  };








  const saveFormData = (vData) => {
  
    props.updateform(vData);
  };













  return (
    <div>
      <div className="tcov-wrapper">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="fieldscroll-wrap">
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div
                        className="box-body common-fields-wrapper"
                        style={{
                          maxWidth: "fit-content",
                          minWidth: "70vw",
                        }}
                      >
                        {state.frmName && state.frmVisible ? (
                          <article className="article">
                            <div className="box box-default">
                              <div className="box-body">
                                <NormalFormIndex
                                  key={"key_" + state.frmName + "_0"}
                                  formClass="label-min-height"
                                  formData={state.formdata}
                                  showForm={true}
                                  frmCode={state.frmName}
                                  formDevideInCols={2}
                                  showToolbar={[
                                    {
                                      isLeftBtn: [
                                        {
                                          key: 's1',
                                          isSets: [  {
                                            id: "1",
                                            key: "save",
                                            type: <SaveOutlined />,
                                            withText: "Save",
                                            showToolTip: true,
                                            event: (key, data) =>
                                              saveFormData(data),
                                          }],
                                        
                                        }
                                      ],
                                      isRightBtn: [],
                                      isResetOption: false,
                                    },
                                  ]}
                                  inlineLayout={true}
                                />
                               
                              </div>
                            </div>
                          </article>
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Content>
          </Layout>
        </Layout>
      </div>
    </div>
  );
};

export default PortModalForm;
