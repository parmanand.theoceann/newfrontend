import React, { useState, useRef, useEffect, useContext } from "react";

import {
  Row,
  Col,
  Input,
  Select,
  Layout,
  Typography,
  InputNumber,
  Form,
  Space,
  DatePicker,
  Modal,
  Divider,
  Button,
  Collapse,
  Anchor,
  Tooltip,
} from "antd";
import { SearchOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import moment from "moment";
import PortSelection from "../../port-selection/PortSelection";
import VesselDatabaseModal from "../vesselsearch/VesselDatabaseModal";
import PortModalForm from "../components/PortModalForm";
import VesselSchedule from "../../../components/vessel-form";
import { useQuickEstimateContext } from "../quickEstimateContext";
import dayjs from "dayjs";
import { openNotificationWithIcon } from "../../../shared";

const { Title } = Typography;
const { Content } = Layout;
const { Option } = Select;

const VesselDetailQuick = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
  openModal,
  updateQuickFromdatabase,
  modalCloseEvent,
  updatequickfrom,
  localvesseldata,
  handleSelectVesselNameFromDropDown,
}) => {
  const {
    ladenSpeedDropDown,
    selectedBallastSpeed,
    ballastSpeedDropDown,
    selectedLadenSpeed,
    setSelectedLadenSpeed,
    setSelectedBallastSpeed,
  } = useQuickEstimateContext();

  const vesselDropDownOptions = localvesseldata?.map((el, indx) => ({
    label: `${el.vessel_name} - DWT ${el.vessel_dwt} - IMO ${el.imo_no} - ${el.vessel_type_name} - BS spd ${el.spd_ballast} - LS spd ${el.spd_laden}`,
    value: el.vessel_id,
    key: indx,
  }));

  const filterOption = (input, option) =>
    (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

  const handleSelectLadenSpeed = (value, option) => {
    handleChange("vessel_details", "laden_spd", value);
    setSelectedLadenSpeed(option);

    handleselect(
      formdata.vessel_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      true,
      false
    );
    handleselect(
      formdata.vessel_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      true,
      false
    );
  };

  const handleSelectBallastSpeed = (value, option) => {
    handleChange("vessel_details", "ballast_spd", value);
    setSelectedBallastSpeed(option);
    handleselect(
      formdata.vessel_details.vlsfo_fuel,
      "vlsfo_fuel",
      true,
      option.key,
      false,
      true
    );
    handleselect(
      formdata.vessel_details.lsmgo_fuel,
      "lsmgo_fuel",
      true,
      option.key,
      false,
      true
    );
  };

  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            <a
              onClick={() => {
                if (!formdata.vessel_details.vessel_id) {
                  openNotificationWithIcon("error", "select the vessel first");
                  return;
                }
                handleOpenModal("openVesselModalForm", true);
              }}
              style={{ textDecoration: "none" }}
              className="required-label"
            >
              Vessel Name
            </a>
          </Col>

          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              showSearch
              placeholder="Select a Vessel"
              optionFilterProp="children"
              onChange={handleSelectVesselNameFromDropDown}
              value={formdata.vessel_details.vessel_id}
              options={vesselDropDownOptions}
              dropdownStyle={{ width: "450px" }}
              filterOption={filterOption}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            D.Hire/Add Comm. :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="tci_daily_cost"
                  onChange={(e) => {
                    handleChange(
                      "vessel_details",
                      "tci_daily_cost",
                      e.target.value
                    );
                  }}
                  value={formdata.vessel_details.tci_daily_cost}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="tci_add_comm"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "tci_add_comm",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.tci_add_comm}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            DWT/WF% :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  placeholder="0.00"
                  name="dwt"
                  type={"number"}
                  onChange={(e) =>
                    handleChange("vessel_details", "dwt", e.target.value)
                  }
                  value={formdata.vessel_details.dwt}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  onChange={(e) =>
                    handleChange("vessel_details", "wf", e.target.value)
                  }
                  value={formdata.vessel_details.wf}
                  name="wf"
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            BB/Other Cost :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="blast_bonus"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "blast_bonus",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.blast_bonus}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="other_cost"
                  onChange={(e) =>
                    handleChange("vessel_details", "other_cost", e.target.value)
                  }
                  value={formdata.vessel_details.other_cost}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Speed L/B :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Select
                  placeholder="0.00"
                  type={"number"}
                  name="laden_spd"
                  onChange={handleSelectLadenSpeed}
                  value={selectedLadenSpeed}
                  options={ladenSpeedDropDown}
                />
              </Col>
              <Col span={12}>
                <Select
                  placeholder="0.00"
                  name="ballast_spd"
                  type={"number"}
                  onChange={handleSelectBallastSpeed}
                  value={selectedBallastSpeed}
                  options={ballastSpeedDropDown}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Consp. Fuel(MT/D) :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <div>
              <Row style={{ marginTop: "10px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                      marginLeft: "-10px",
                    }}
                  >
                    Grade&nbsp;
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={8}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Select
                    bordered={false}
                    suffixIcon={null}
                    style={{
                      width: 135,
                      borderBottom: "1px solid black",
                    }}
                    value={formdata?.vessel_details?.vlsfo_fuel?.toString()}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "vlsfo_fuel")
                    }
                    options={[
                      {
                        key: 1,
                        value: "5",
                        label: "VLSFO",
                      },
                      {
                        key: 2,
                        value: "10",
                        label: "ULSFO",
                      },
                      {
                        key: 3,
                        value: "11",
                        label: "IFO",
                      },
                    ]}
                  />
                </Col>
                <Col className="gutter-row" span={10}>
                  <Select
                    bordered={false}
                    suffixIcon={null}
                    style={{
                      width: 91.5,
                      borderBottom: "1px solid black",
                    }}
                    value={formdata?.vessel_details?.lsmgo_fuel?.toString()}
                    onChange={(selectedOption) =>
                      handleselect(selectedOption, "lsmgo_fuel")
                    }
                    name="lsmgo_fuel"
                    options={[
                      {
                        value: "7",
                        label: "LSMGO",
                      },
                      {
                        value: "4",
                        label: "MGO",
                      },
                    ]}
                  />
                </Col>
              </Row>

              <Row style={{ marginTop: "10px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                      marginLeft: "-9px",
                    }}
                  >
                    B
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={8}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Input
                    placeholder="0.00"
                    type={"number"}
                    name="vlsfo_cons_b"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "vlsfo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.vlsfo_cons_b}
                  />
                </Col>
                <Col className="gutter-row" span={12}>
                  <Input
                    placeholder="0.00"
                    name="lsmgo_cons_b"
                    type={"number"}
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "lsmgo_cons_b",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.lsmgo_cons_b}
                  />
                </Col>
              </Row>

              <Row style={{ marginTop: "10px" }}>
                <Col className="gutter-row" span={4}>
                  <div
                    style={{
                      fontWeight: "700",
                      textAlign: "center",
                      marginLeft: "-9px",
                    }}
                  >
                    L
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={8}
                  style={{ borderRight: "1px solid black" }}
                >
                  <Input
                    placeholder="0.00"
                    type={"number"}
                    name="vlsfo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "vlsfo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.vlsfo_cons_l}
                  />
                </Col>
                <Col className="gutter-row" span={12}>
                  <Input
                    placeholder="0.00"
                    type={"number"}
                    name="lsmgo_cons_l"
                    onChange={(e) =>
                      handleChange(
                        "vessel_details",
                        "lsmgo_cons_l",
                        e.target.value
                      )
                    }
                    value={formdata.vessel_details.lsmgo_cons_l}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Ballast Port :
          </Col>
          <Col span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openBalastModal", true)}
              name="ballast_port"
              value={formdata.vessel_details.ballast_port}
            />
            <Modal
              open={openModal.openBalastModal}
              onCancel={() => handleOpenModal("openBalastModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={null}
                modalCloseEvent={(data) => {
                  handleOpenModal("openBalastModal", false);
                  modalCloseEvent("ballast_port", data);
                }}
              />
            </Modal>
          </Col>

          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openBalastModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.ballast_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            <Modal
              open={openModal.openBalastModalForm}
              onCancel={() => handleOpenModal("openBalastModalForm", false)}
              footer={null}
              width="80%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openBalastModalForm", false);

                  updatequickfrom("blastformdata", data);
                }}
                formdata={formdata.blastformdata}
                port={formdata.vessel_details.ballast_port}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Load Port :
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openLoadModal", true)}
              name="load_port"
              value={formdata.vessel_details.load_port}
            />

            <Modal
              open={openModal.openLoadModal}
              onCancel={() => handleOpenModal("openLoadModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.ballast_port}
                modalCloseEvent={(data) => {
              
                  handleOpenModal("openLoadModal", false);
                  modalCloseEvent("load_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openLoadModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.load_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>
            <Modal
              open={openModal.openLoadModalForm}
              onCancel={() => handleOpenModal("openLoadModalForm", false)}
              footer={null}
              width="75%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openLoadModalForm", false);
                  updatequickfrom("loadformdata", data);
                }}
                port={formdata.vessel_details.load_port}
                formdata={formdata.loadformdata}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Discharge Port :
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openDischargeModal", true)}
              name="discharge_port"
              value={formdata.vessel_details.discharge_port}
            />
            <Modal
              open={openModal.openDischargeModal}
              onCancel={() => handleOpenModal("openDischargeModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.load_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openDischargeModal", false);
                  modalCloseEvent("discharge_port", data);
                }}
              />
            </Modal>
          </Col>

          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openDischargeModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.discharge_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            {openModal.openDischargeModalForm && (
              <Modal
                open={openModal.openDischargeModalForm}
                onCancel={() =>
                  handleOpenModal("openDischargeModalForm", false)
                }
                footer={null}
                width="75%"
                maskClosable={false}
              >
                <PortModalForm
                  updateform={(data) => {
                    handleOpenModal("openDischargeModalForm", false);

                    updatequickfrom("dischargeformdata", data);
                  }}
                  port={formdata.vessel_details.discharge_port}
                  formdata={formdata.dischargeformdata}
                />
              </Modal>
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Repos Port :
          </Col>
          <Col className="" span={10}>
            <Input
              type="text"
              onClick={() => handleOpenModal("openReposModal", true)}
              name="repos_port"
              value={formdata.vessel_details.repos_port}
            />
            <Modal
              open={openModal.openReposModal}
              onCancel={() => handleOpenModal("openReposModal", false)}
              footer={null}
              width="75%"
            >
              <PortSelection
                fromPortID={formdata.vessel_details.discharge_port}
                modalCloseEvent={(data) => {
                  handleOpenModal("openReposModal", false);
                  modalCloseEvent("repos_port", data);
                }}
              />
            </Modal>
          </Col>
          <Col span={2}>
            <Tooltip
              title="First select the port then fill the port iteniary form."
              placement="topLeft"
              color="rgb(18, 64, 106)"
            >
              <button
                onClick={() => handleOpenModal("openReposModalForm", true)}
                type="primary"
                style={{
                  backgroundColor: "rgb(18, 64, 106)",
                  color: "white",
                  padding: "2px 15px",
                  borderRadius: "6px",
                }}
                disabled={formdata.vessel_details.repos_port ? false : true}
              >
                <ExclamationCircleOutlined />
              </button>
            </Tooltip>

            <Modal
              open={openModal.openReposModalForm}
              onCancel={() => handleOpenModal("openReposModalForm", false)}
              footer={null}
              width="75%"
              maskClosable={false}
            >
              <PortModalForm
                updateform={(data) => {
                  handleOpenModal("openReposModalForm", false);

                  updatequickfrom("reposformdata", data);
                }}
                port={formdata.vessel_details.repos_port}
                formdata={formdata.reposformdata}
              />
            </Modal>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Routing :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Select
              bordered={false}
              suffixIcon={null}
              style={{
                width: 180,
                borderBottom: "1px solid black",
              }}
              value={formdata?.vessel_details?.routing}
              onChange={(selectedOption) =>
                handleselect(selectedOption, "routing")
              }
              options={[
                {
                  key: 1,
                  value: "1",
                  label: "ECA",
                },
                {
                  key: 2,
                  value: "2",
                  label: "Piracy",
                },
              ]}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "5px" }}>
          <Col className="form-label" span={10}>
            Bunker Price $/MT :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row style={{ marginTop: "10px" }}>
              <Col
                className="gutter-row"
                span={12}
                style={{ borderRight: "1px solid black" }}
              >
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="bunker_oil_price"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "bunker_oil_price",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.bunker_oil_price}
                />
              </Col>
              <Col className="gutter-row" span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="bunker_gas_price"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "bunker_gas_price",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.bunker_gas_price}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Commenced date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
              showTime={{ format: "HH:mm:ss" }}
              picker="date"
              format="YYYY-MM-DD HH:mm:ss"
              style={{ width: "100%" }}
              name="commence_date"
              onChange={(date, dateString) =>
                handleselect(dayjs(dateString), "commence_date")
              }
              value={dayjs(
                formdata.vessel_details.commence_date,
                "YYYY-MM-DD HH:mm:ss"
              )}
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Completed date :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <DatePicker
              format="YYYY-MM-DD HH:mm:ss"
              showTime
              style={{ width: "100%" }}
              name="completed_date"
              onChange={(date, dateString) =>
                handleselect(moment(dateString), "completed_date")
              }
              // value={moment(
              //   formdata.vessel_details.completed_date,
              //   "YYYY-MM-DD HH:mm:ss"
              // )}
              value={
                formdata.vessel_details.completed_date ===
                  "0000-00-00 00:00:00" ||
                formdata.vessel_details.completed_date === null
                  ? moment().add(4, "days")
                  : moment(
                      formdata.vessel_details.completed_date,
                      "YYYY-MM-DD HH:mm:ss"
                    )
              }
              // value={
              //   formdata.vessel_details.completed_date ===
              //     "0000-00-00 00:00:00" ||
              //   formdata.vessel_details.completed_date === null
              //     ? moment().add(4, "days")
              //     : moment(formdata.vessel_details.completed_date,"YYYY-MM-DD HH:mm:ss")
              // }

              disabled
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Port days/Sea days :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Row>
              <Col span={12} style={{ borderRight: "1px solid black" }}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="port_days"
                  onChange={(e) =>
                    handleChange("vessel_details", "port_days", e.target.value)
                  }
                  value={formdata.vessel_details.port_days}
                />
              </Col>
              <Col span={12}>
                <Input
                  placeholder="0.00"
                  type={"number"}
                  name="sea_days"
                  onChange={(e) =>
                    handleChange("vessel_details", "sea_days", e.target.value)
                  }
                  value={formdata.vessel_details.sea_days}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Voyage days :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              type={"number"}
              placeholder="Total Voyage days"
              onChange={(e) =>
                handleChange(
                  "vessel_details",
                  "total_voyage_days",
                  e.target.value
                )
              }
              name="total_voyage_days"
              value={formdata.vessel_details.total_voyage_days}
            />
          </Col>
        </Row>
      </Col>

      {openModal.openLocalVesselModal && (
        <Modal
          open={openModal.openLocalVesselModal}
          footer={null}
          onCancel={() => handleOpenModal("openLocalVesselModal", false)}
        >
          <VesselDatabaseModal
            type="local"
            vesseldata={localvesseldata}
            updateform={(data, fulldata) =>
              updateQuickFromdatabase("local", data, fulldata)
            }
          />
        </Modal>
      )}

      {openModal.openGlobalVesselModal && (
        <Modal
          open={openModal.openGlobalVesselModal}
          onCancel={() => handleOpenModal("openGlobalVesselModal", false)}
          footer={null}
        >
          <VesselDatabaseModal
            type="global"
            updateform={(data) => updateQuickFromdatabase("global", data)}
          />
        </Modal>
      )}

      {openModal.openVesselModalForm && (
        <Modal
          open={openModal.openVesselModalForm}
          onCancel={() => handleOpenModal("openVesselModalForm", false)}
          footer={null}
          title={"Edit Vessel Form"}
          width="95%"
          style={{ top: "10px" }}
          bodyStyle={{ height: 790, overflowY: "auto", padding: "0.5rem" }}
          maskClosable={false}
        >
          <VesselSchedule formData={{}} onEdit={true} showSideListBar={false} />
        </Modal>
      )}
    </Row>
  );
};

export default VesselDetailQuick;
