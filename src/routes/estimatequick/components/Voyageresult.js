import React, { useState, useRef, useEffect } from "react";
import { Row, Col, Input} from "antd";

const Voyageresult = ({
  handleChange,
  handleselect,
  formdata,
  handleOpenModal,
}) => {
  return (
    <Row>
      <Col className="gutter-row" span={24}>
        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Gross Income :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="gross_income"
              type={"number"}
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "gross_income", e.target.value)
              }
              value={formdata.voyage_results.gross_income}
              placeholder="Gross Income"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label"span={10}>
            Net Income :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="net_income"
              type={"number"}
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "net_income", e.target.value)
              }
              value={formdata.voyage_results.net_income}
              placeholder="net Income"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Port expense :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="total_port_expenses"
              disabled
              type={"number"}
              onChange={(e) =>
                handleChange(
                  "voyage_results",
                  "total_port_expenses",
                  e.target.value
                )
              }
              value={formdata.voyage_results.total_port_expenses}
              placeholder="Port Expenses"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label"span={10}>
            Bunker Expense :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="total_bunker_expenses"
              disabled
              type={"number"}
              onChange={(e) =>
                handleChange(
                  "voyage_results",
                  "total_bunker_expenses",
                  e.target.value
                )
              }
              value={formdata.voyage_results.total_bunker_expenses}
              placeholder="Bunker Expenses"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Vessel Hire :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="vessel_hire"
              type={"number"}
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "vessel_hire", e.target.value)
              }
              value={formdata.voyage_results.vessel_hire}
              placeholder="Vessel Hire"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Expense :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="total_expenses"
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "total_expenses", e.target.value)
              }
              value={formdata.voyage_results.total_expenses}
              placeholder="Total Expenses"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Total Profit/Loss :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="total_profit"
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "total_profit", e.target.value)
              }
              value={formdata.voyage_results.total_profit}
              placeholder="Total Expenses"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            Daily Profit :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="daily_profit"
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "daily_profit", e.target.value)
              }
              value={formdata.voyage_results.daily_profit}
              placeholder="Total Expenses"
            />
          </Col>
        </Row>

        <Row style={{ marginTop: "10px" }}>
          <Col className="form-label" span={10}>
            TCE :
          </Col>
          <Col className="" span={14} style={{ paddingRight: 10 }}>
            <Input
              name="tce"
              disabled
              onChange={(e) =>
                handleChange("voyage_results", "tce", e.target.value)
              }
              value={formdata.voyage_results.tce}
              placeholder="Total Expenses"
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Voyageresult;
