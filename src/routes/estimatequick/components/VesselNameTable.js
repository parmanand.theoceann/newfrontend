import React, { useState } from "react";
import { Col, Input, Modal, Row, Select, Table } from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
} from "../../../shared";

export default function VesselNameTable({
  handleOpenModal,
  handleChange,
  handleselect,
  formdata,
  ballastSpeedOption,
  setBallastSpeedOption,
  ladenSpeed,
  handleSelectChangeSpeed,
  ballastSpeed,
  ladenSpeedOption,
  handleSelectChangeSpeedBallast,
  handleSelectChange,
  localvesseldata,
}) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  
  const [modalStyle, setModalStyle] = useState({});

  const showModal = () => {
    setIsModalVisible(true);

    const inputElement = document.getElementById('myInput');
    if (inputElement) {
      const rect = inputElement.getBoundingClientRect();
      setModalStyle({
        // top: rect.bottom,
        // left: rect.left,
      });
    }
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const options = localvesseldata?.map((item, ind) => {
    return {
      key: ind,
      label:
        item.vessel_name +
        " - " +
        "DWT " +
        item.vessel_dwt +
        " - " +
        "IMO " +
        item.imo_no +
        " - " +
        "BS " +
        item.spd_ballast +
        " - " +
        "LS " +
        item.spd_laden,
      value: item.vessel_id,
    };
  });

  //console.log("localvesseldata :", localvesseldata);

  const columns = [
    {
      title: "Vessel Name",
      dataIndex: "vessel_name",
      key: "vessel_name",
    },
    {
      title: "DWT",
      dataIndex: "vessel_dwt",
      key: "vessel_dwt",
    },
    {
      title: "IMO NO",
      dataIndex: "imo_no",
      key: "imo_no",
    },
    {
      title: "Blast Speed",
      dataIndex: "spd_ballast",
      key: "spd_ballast",
    },
    {
      title: "Laden Speed",
      dataIndex: "spd_laden",
      key: "spd_laden",
    },
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
  };

  return (
    <>
      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          <a
            onClick={() => {
              if (!formdata.vessel_details.vessel_id) {
                openNotificationWithIcon("error", "select the vessel Name");
                return;
              }
              handleOpenModal("openVesselModalForm", true);
            }}
            style={{ textDecoration: "none" }}
            className="required-label"
          >
            Vessel Name
          </a>
        </Col>

        <Col className="" span={14} style={{ paddingRight: 10 }}>
          {/* <Select
              value={formdata.vessel_details.vessel_id}
              onChange={handleSelectChange}
              options={options}
              placeholder="Select Vessel Name"
              dropdownStyle={{ width: '450px' }}
            /> */}
          <Input onClick={showModal} placeholder="Click me to open modal" id="myInput" />
        </Col>
      </Row>

      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          D.Hire/Add Comm. :
        </Col>
        <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Row>
            <Col span={12} style={{ borderRight: "1px solid black" }}>
              <Input
                placeholder="0.00"
                type={"number"}
                name="tci_daily_cost"
                onChange={(e) => {
                  handleChange(
                    "vessel_details",
                    "tci_daily_cost",
                    e.target.value
                  );
                }}
                value={formdata.vessel_details.tci_daily_cost}
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="0.00"
                type={"number"}
                name="tci_add_comm"
                onChange={(e) =>
                  handleChange("vessel_details", "tci_add_comm", e.target.value)
                }
                value={formdata.vessel_details.tci_add_comm}
              />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          DWT/WF% :
        </Col>
        <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Row>
            <Col span={12} style={{ borderRight: "1px solid black" }}>
              <Input
                placeholder="0.00"
                name="dwt"
                type={"number"}
                onChange={(e) =>
                  handleChange("vessel_details", "dwt", e.target.value)
                }
                value={formdata.vessel_details.dwt}
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="0.00"
                type={"number"}
                onChange={(e) =>
                  handleChange("vessel_details", "wf", e.target.value)
                }
                value={formdata.vessel_details.wf}
                name="wf"
              />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          BB/Other Cost :
        </Col>
        <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Row>
            <Col span={12} style={{ borderRight: "1px solid black" }}>
              <Input
                placeholder="0.00"
                type={"number"}
                name="blast_bonus"
                onChange={(e) =>
                  handleChange("vessel_details", "blast_bonus", e.target.value)
                }
                value={formdata.vessel_details.blast_bonus}
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="0.00"
                type={"number"}
                name="other_cost"
                onChange={(e) =>
                  handleChange("vessel_details", "other_cost", e.target.value)
                }
                value={formdata.vessel_details.other_cost}
              />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          Speed L/B :
        </Col>
        <Col className="" span={14} style={{ paddingRight: 10 }}>
          <Row>
            <Col span={12} style={{ borderRight: "1px solid black" }}>
              <Select
                suffixIcon={false}
                removeIcon={null}
                value={ladenSpeed}
                onChange={handleSelectChangeSpeed}
                options={ladenSpeedOption}
              />
            </Col>
            <Col span={12}>
              <Select
                suffixIcon={false}
                removeIcon={null}
                value={ballastSpeed}
                // value={formdata.vessel_details.vessel_id}
                onChange={handleSelectChangeSpeedBallast}
                options={ballastSpeedOption}
              />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: "10px" }}>
        <Col className="form-label" span={10}>
          Consp. Fuel(MT/D) :
        </Col>
        <Col className="" span={14} style={{ paddingRight: 10 }}>
          <div>
            <Row style={{ marginTop: "10px" }}>
              <Col className="gutter-row" span={4}>
                <div
                  style={{
                    fontWeight: "700",
                    textAlign: "center",
                    marginLeft: "-10px",
                  }}
                >
                  Grade&nbsp;
                </div>
              </Col>
              <Col
                className="gutter-row"
                span={8}
                style={{ borderRight: "1px solid black" }}
              >
                <Select
                  bordered={false}
                  suffixIcon={null}
                  style={{
                    width: 135,
                    borderBottom: "1px solid black",
                  }}
                  value={formdata.vessel_details.vlsfo_fuel.toString()}
                  onChange={(selectedOption) =>
                    handleselect(selectedOption, "vlsfo_fuel")
                  }
                  options={[
                    {
                      key: 1,
                      value: "5",
                      label: "VLSFO",
                    },
                    {
                      key: 2,
                      value: "10",
                      label: "ULSFO",
                      disabled: true,
                    },
                    {
                      key: 3,
                      value: "11",
                      label: "HFO",
                      disabled: true,
                    },
                  ]}
                />
              </Col>
              <Col className="gutter-row" span={10}>
                <Select
                  bordered={false}
                  suffixIcon={null}
                  style={{
                    width: 91.5,
                    borderBottom: "1px solid black",
                  }}
                  value={formdata.vessel_details.lsmgo_fuel.toString()}
                  onChange={(selectedOption) =>
                    handleselect(selectedOption, "lsmgo_fuel")
                  }
                  name="lsmgo_fuel"
                  options={[
                    {
                      value: "7",
                      label: "LSMGO",
                    },
                    {
                      value: "4",
                      label: "MGO",
                      disabled: true,
                    },
                  ]}
                />
              </Col>
            </Row>

            <Row style={{ marginTop: "10px" }}>
              <Col className="gutter-row" span={4}>
                <div
                  style={{
                    fontWeight: "700",
                    textAlign: "center",
                    marginLeft: "-9px",
                  }}
                >
                  B
                </div>
              </Col>
              <Col
                className="gutter-row"
                span={8}
                style={{ borderRight: "1px solid black" }}
              >
                <Input
                  disabled={true}
                  placeholder="0.00"
                  name="vlsfo_cons_b"
                  onChange={(e) =>
                    handleChange(
                      "vessel_details",
                      "vlsfo_cons_b",
                      e.target.value
                    )
                  }
                  value={formdata.vessel_details.vlsfo_cons_b}
                />
              </Col>
              <Col className="gutter-row" span={12}>
                <Input
                  disabled={true}
                  type={"number"}
                  placeholder="0.00"
                  name="lsmgo_cons_b"
                  onChange={(value) =>
                    handleChange("vessel_details", "lsmgo_cons_b", value)
                  }
                  value={formdata.vessel_details.lsmgo_cons_b}
                ></Input>
              </Col>
            </Row>

            <Row style={{ marginTop: "10px" }}>
              <Col className="gutter-row" span={4}>
                <div
                  style={{
                    fontWeight: "700",
                    textAlign: "center",
                    marginLeft: "-9px",
                  }}
                >
                  L
                </div>
              </Col>
              <Col
                className="gutter-row"
                span={8}
                style={{ borderRight: "1px solid black" }}
              >
                <Input
                  disabled={true}
                  type={"number"}
                  placeholder="0.00"
                  name="vlsfo_cons_l"
                  onChange={(value) =>
                    handleChange("vessel_details", "vlsfo_cons_l", value)
                  }
                  value={formdata.vessel_details.vlsfo_cons_l}
                ></Input>
              </Col>
              <Col className="gutter-row" span={12}>
                <Input
                  disabled={true}
                  placeholder="0.00"
                  name="lsmgo_cons_l"
                  onChange={(value) =>
                    handleChange("vessel_details", "lsmgo_cons_l", value)
                  }
                  value={formdata.vessel_details.lsmgo_cons_l}
                ></Input>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>

      <Modal
        open={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        getContainer={false}
        // width="max-content"
        width="926px"
      >
        <Table
          rowSelection={{
            type: "radio",
            ...rowSelection,
          }}
          columns={columns}
          dataSource={localvesseldata}
          rowKey={(record) => record.vessel_id}
        />
      </Modal>
    </>
  );
}
