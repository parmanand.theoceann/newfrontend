import { v4 as uuidv4 } from "uuid";
import {
  nextdate,
  voyageEstimateCalculation,
  Co2Calculation,
} from "../helperfunction";
import moment from "moment";
import dayjs from "dayjs";
export const initform = {
  estimate_id: "",
  id: null,
  full_estimate_id: null,
  vessel_details: {
    //estimate_id: "",
    vessel_name: "",
    vessel_code: "",
    tci_daily_cost: "0.00",
    tci_add_comm: "0.00",
    dwt: "",
    wf: "5",
    blast_bonus: "0.00",
    other_cost: "0.00",
    laden_spd: "0.00",
    ballast_spd: "0.00",
    vlsfo_fuel: "5",
    lsmgo_fuel: "7",
    vlsfo_cons_b: "0.00",
    lsmgo_cons_b: "0.00",
    vlsfo_cons_l: "0.00",
    lsmgo_cons_l: "0.00",
    bunker_oil_price: "0.00",
    bunker_gas_price: "0.00",
    ballast_port: "",
    load_port: "",
    discharge_port: "",
    repos_port: "",
    routing: "1",
    commence_date: dayjs(),
    completed_date: null,
    port_days: "4",
    sea_days: "0.00",
    total_voyage_days: "0.00",
  },
  cargo_details: {
    cp_qty: "0.00",
    cp_qty_type: "MT",
    frt_type: 38,
    frt_unit: "USD",
    frt_rate: "0.00",
    commission: "0.00",
  },
  voyage_results: {
    gross_income: "0.00",
    net_income: "0.00",
    total_port_expenses: "0.00",
    total_bunker_expenses: "0.00",
    vessel_hire: "0.00",
    total_expenses: "0.00",
    total_profit: "0.00",
    daily_profit: "0.00",
    tce: "0.00",
  },
  co2_view: {
    total_bunker_cons: 0.0,
    total_co2: 0,
    euets: 0,
    emmission_mt: "",
    co2_dwt: 0,
    total_distance: 0,
    transport_work: 0,
    attained_cii: 0,
    eeoi: 0,
  },

  co2_calc_value: {
    vlsfo_fuel_multiplier: 3.15,
    lsmgo_fuel_multiplier: 3.15,
    ballast_port_eu_country: false,
    load_port_eu_country: false,
    redelivery_port_eu_country: false,
    repos_port_eu_country: false,
  },

  blastformdata: {
    port_expense: 0,
  },
  loadformdata: {
    port_expense: 0,
  },
  dischargeformdata: {
    port_expense: 0,
  },
  reposformdata: {
    port_expense: 0,
  },

  seca_crossed: {
    blseca: 0,
    blcrossed: "",
    ldseca: 0,
    ldcrossed: "",
    drseca: 0,
    drcrossed: "",
  },

  //  blasttoloadDistance: 0,
  loadTodischargeDistance: 0,
  dischargetoreposDistance: 0,
  blastPortExpenses: 0,
  loadPortExpenses: 0,
  dischargePortExpenses: 0,
  reposPortExpenses: 0,
};

export const estimateReducer = (state, action) => {
  switch (action.type) {
    case "copy": {
      const { vessel_details, cargo_details, voyage_results, co2_view } =
        action.payload;

      return {
        ...state,
        vessel_details: { ...vessel_details },
        cargo_details: { ...cargo_details },
        voyage_results: { ...voyage_results },
        co2_view: { ...co2_view },
        estimate_id: "",
        id: "",
      };
    }

    case "estimate_id":
      return {
        ...state,
        estimate_id: action.payload.estimate_id ?? "",
        id: action.payload.quickid ?? "",
        full_estimate_id: action.payload.fullEstid ?? "",
      };

    case "updatefull":
      const {
        _fullvessel,
        estimateId,
        _cargo,
        _blastformdata,
        _loadformdata,
        _dischargeformdata,
        _reposformdata,
        _blasttoloadDistance,
        _loadTodischargeDistance,
        _dischargetoreposDistance,
        ballast_port_eu_country,
        load_port_eu_country,
        redelivery_port_eu_country,
        repos_port_eu_country,
      } = action.payload;

      return {
        ...state,
        estimate_id: estimateId,
        vessel_details: { ...state.vessel_details, ..._fullvessel },
        cargo_details: { ...state.cargo_details, ..._cargo },
        blastformdata: { ..._blastformdata },
        loadformdata: { ..._loadformdata },
        dischargeformdata: { ..._dischargeformdata },
        reposformdata: { ..._reposformdata },
        blasttoloadDistance: _blasttoloadDistance,
        loadTodischargeDistance: _loadTodischargeDistance,
        dischargetoreposDistance: _dischargetoreposDistance,
        co2_calc_value: {
          ...state.co2_calc_value,
          ballast_port_eu_country: ballast_port_eu_country,
          load_port_eu_country: load_port_eu_country,
          redelivery_port_eu_country: redelivery_port_eu_country,
          repos_port_eu_country: repos_port_eu_country,
        },
      };

    case "updatecalculation": {
      const { calculationdata, co2calculation } = action.payload;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = calculationdata;

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = co2calculation;

      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "blasttoloadDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;
      return {
        ...state,
        blasttoloadDistance: totaldistance,
        seca_crossed: {
          ...state.seca_crossed,
          blseca: secaLength,
          blcrossed: crossed,
        },
      };
    }

    case "loadTodischargeDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;
      return {
        ...state,
        loadTodischargeDistance: totaldistance,
        seca_crossed: {
          ...state.seca_crossed,
          ldseca: secaLength,
          ldcrossed: crossed,
        },
      };
    }

    case "dischargetoreposDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;
      return {
        ...state,
        dischargetoreposDistance: totaldistance,
        seca_crossed: {
          ...state.seca_crossed,
          drseca: secaLength,
          drcrossed: crossed,
        },
      };
    }

    case "vessel_name": {
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vessel_name: action.payload.vessel_name,
          vessel_code: action.payload.vessel_code,
          vessel_id: action.payload.vessel_id,
          dwt: action.payload.dwt,
          laden_spd: action.payload.laden_spd,
          ballast_spd: action.payload.ballast_spd,
        },
        co2_view: {
          ...state.co2_view,
          co2_dwt: action.payload.dwt,
        },
      };
    }

    case "tci_daily_cost": {
      let dailyHire = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "tci_daily_cost",
        dailyHire
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          tci_daily_cost: dailyHire,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
          total_port_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
        },
      };
    }

    case "tci_add_comm": {
      let addPercentage = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "tci_add_comm",
        addPercentage
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          tci_add_comm: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "dwt":
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          dwt: action.payload,
        },
      };
    case "wf":
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          wf: action.payload,
        },
      };
    case "blast_bonus": {
      let blastBonus = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "blast_bonus",
        blastBonus
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          blast_bonus: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "other_cost": {
      let otherCost = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "other_cost",
        otherCost
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          other_cost: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "laden_spd": {
      let ladenSpeed = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "laden_spd",
        ladenSpeed
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "laden_spd", ladenSpeed);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          laden_spd: action.payload,
          sea_days: seaDays.toFixed(2),
          total_voyage_days: totalVoyageDays.toFixed(2),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "ballast_spd": {
      let blastSpeed = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "ballast_spd",
        blastSpeed
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "ballast_spd", blastSpeed);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          ballast_spd: action.payload,
          sea_days: seaDays.toFixed(2),
          total_voyage_days: totalVoyageDays.toFixed(2),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }
    case "cons_fuel": {
      let vlsfo_blast = +action?.payload?.vlsfo_blast[0];
      let vlsfo_laden = +action?.payload?.vlsfo_laden[0];
      let lsmgo_blast = +action?.payload?.lsmgo_blast[0];
      let lsmgo_laden = +action?.payload?.lsmgo_laden[0];
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_cons_b: vlsfo_blast,
          lsmgo_cons_b: lsmgo_blast,
          vlsfo_cons_l: vlsfo_laden,
          lsmgo_cons_l: lsmgo_laden,
        },
      };
    }

    case "vlsfo_fuel": {
      let vlsfo_blast = 0,
        vlsfo_laden = 0;
      let currentIndexOfSpeedLaden = action?.payload?.indexLaden;
      let currentIndexOfSpeedBallast = action?.payload?.indexBallast;

      if (action.payload.value == 10) {
        // ulsfo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.ulsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.ulsfo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 11) {
        // hfo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.ifo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.ifo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 5) {
        // vlsfo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.vlsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.vlsfo_laden[currentIndexOfSpeedLaden];
      } else {
        vlsfo_blast = 0;
        vlsfo_laden = 0;
      }

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_fuel: action.payload.value,
          vlsfo_cons_b: vlsfo_blast,
          vlsfo_cons_l: vlsfo_laden,
        },
        co2_calc_value: {
          ...state.co2_calc_value,
          vlsfo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "lsmgo_fuel": {
      let lsmgo_blast = 0,
        lsmgo_laden = 0;
      let currentIndexOfSpeedBallast = action?.payload?.indexBallast;
      let currentIndexOfSpeedLaden = action?.payload?.indexLaden;
      if (action.payload.value == 7) {
        // LSMGO fuel consumption
        lsmgo_blast =
          action?.payload?.fuelarr?.lsmgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden =
          action?.payload?.fuelarr?.lsmgo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 4) {
        // MGO fuel consumption
        lsmgo_blast =
          action?.payload?.fuelarr?.mgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden =
          action?.payload?.fuelarr?.mgo_laden[currentIndexOfSpeedLaden];
      } else {
        lsmgo_blast = 0;
        lsmgo_laden = 0;
      }

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          lsmgo_fuel: action.payload.value,
          lsmgo_cons_b: lsmgo_blast,
          lsmgo_cons_l: lsmgo_laden,
        },
        co2_calc_value: {
          ...state.co2_calc_value,
          lsmgo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "vlsfo_cons_b": {
      let vlsfoConsBlast = action.payload;

      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "vlsfo_cons_b",
        vlsfoConsBlast
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(
        state,
        "vessel_details",
        "vlsfo_cons_b",
        vlsfoConsBlast
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_cons_b: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "lsmgo_cons_b": {
      let lsmgoConsBlast = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "lsmgo_cons_b",
        lsmgoConsBlast
      );

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(
        state,
        "vessel_details",
        "lsmgo_cons_b",
        lsmgoConsBlast
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          lsmgo_cons_b: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "vlsfo_cons_l": {
      let vlsfoConsladen = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "vlsfo_cons_l",
        vlsfoConsladen
      );

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(
        state,
        "vessel_details",
        "vlsfo_cons_l",
        vlsfoConsladen
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_cons_l: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },

        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "lsmgo_cons_l": {
      let lsmgoConsladen = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "lsmgo_cons_l",
        lsmgoConsladen
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(
        state,
        "vessel_details",
        "lsmgo_cons_l",
        lsmgoConsladen
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          lsmgo_cons_l: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "eu_country": {
      return {
        ...state,
        co2_calc_value: {
          ...state.co2_calc_value,
          ...action.payload,
        },
      };
    }

    case "ballast_port": {
      const { portdistance, port_name } = action.payload;

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          ballast_port: port_name,
        },
      };
    }

    case "load_port": {
      const { portdistance, port_name } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;

      const {
        //    blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "load_port",
        port_name
      );

      let commenceDate = state.vessel_details.commence_date;
      let completedDate = nextdate(commenceDate, totalVoyageDays);

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "load_port", port_name);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          load_port: port_name,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "discharge_port": {
      const { portdistance, port_name } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;

      const { dischargetoreposDistance } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "discharge_port",
        port_name
      );

      let commenceDate = state.vessel_details.commence_date;
      let completedDate = nextdate(commenceDate, totalVoyageDays);

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "discharge_port", port_name);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          discharge_port: port_name,

          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },

        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "repos_port": {
      const { portdistance, port_name } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "repos_port",
        port_name
      );

      let commenceDate = state.vessel_details.commence_date;
      let completedDate = nextdate(commenceDate, totalVoyageDays);

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "repos_port", port_name);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          repos_port: port_name,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "routing":
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          routing: action.payload,
        },
      };

    case "bunker_oil_price": {
      const { value, portdistance } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "bunker_oil_price",
        value
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "bunker_oil_price", value);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          bunker_oil_price: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "bunker_gas_price": {
      const { value, portdistance } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "bunker_gas_price",
        value
      );

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "bunker_gas_price", value);
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          bunker_gas_price: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "commence_date": {
      let totalVoyageDays = state.vessel_details.total_voyage_days;
      let commence_date = state.vessel_details.commence_date;
      let completedDate = nextdate(commence_date, totalVoyageDays);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          commence_date: action.payload,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "completed_date": {
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          completed_date: action.payload,
        },
      };
    }

    case "port_days": {
      let portDays = action.payload;
      let seaDays = Number(state.vessel_details.sea_days);
      let totalVoyageDays = seaDays + parseFloat(portDays);
      let commence_date = state.vessel_details.commence_date;
      let completedDate = nextdate(commence_date, totalVoyageDays);

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "port_days", portDays);
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          port_days: action.payload,
          total_voyage_days: totalVoyageDays,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "sea_days": {
      let seaDays = action.payload;
      let portDays = Number(state.vessel_details.port_days);
      let totalVoyageDays = Number(seaDays) + portDays;
      let commence_date = state.vessel_details.commence_date;
      let completedDate = nextdate(commence_date, totalVoyageDays);

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "sea_days", portDays);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          sea_days: action.payload,
          total_voyage_days: totalVoyageDays,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "total_voyage_days": {
      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(
        state,
        "vessel_details",
        "total_voyage_days",
        action.payload
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          total_voyage_days: action.payload,
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "cp_qty": {
      let cpqty = action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "cargo_details",
        "cp_qty",
        cpqty
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          cp_qty: cpqty,
        },

        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "cp_qty_type":
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          cp_qty_type: action.payload,
        },
      };

    case "frt_type": {
      let value = action.payload;

      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "cargo_details",
        "frt_type",
        value
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_type: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }
    case "frt_unit":
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_unit: action.payload,
        },
      };

    case "frt_rate": {
      let rate = action.payload;

      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "cargo_details",
        "frt_rate",
        rate
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_rate: action.payload,
        },

        voyage_results: {
          ...state.voyage_results,

          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }
    case "commission": {
      let commission = +action.payload;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalPortExpenses,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "cargo_details",
        "commission",
        commission
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          commission: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }
    // voyage results

    // co2 view

    case "total_bunker_cons": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: value,
        },
      };
    }

    case "total_co2": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_co2: value,
        },
      };
    }

    case "euets": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          euets: value,
        },
      };
    }

    case "carbon_exposer": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          carbon_exposer: value,
        },
      };
    }

    case "cii_rating": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          cii_rating: value,
        },
      };
    }

    // case "cii_band": {
    //   return {
    //     ...state,
    //     co2_view: {
    //       ...state.co2_view,
    //       cii_band: action.payload,
    //     },
    //   };
    // }

    case "co2_dwt": {
      //  const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          co2_dwt: action.payload,
        },
      };
    }

    case "total_distance": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_distance: value,
        },
      };
    }

    case "emission": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          emission: value,
        },
      };
    }

    case "aer_score": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          aer_score: value,
        },
      };
    }

    case "adjustment": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          adjustment: value,
        },
      };
    }

    case "transport_work": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          transport_work: value,
        },
      };
    }

    case "eeoi": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          eeoi: value,
        },
      };
    }
    case "blastformdata": {
      let blastPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "blastPortExpenses",
        blastPortExpenses
      );

      return {
        ...state,
        blastformdata: { ...action.payload },
        blastPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "loadformdata": {
      let loadPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "loadPortExpenses",
        loadPortExpenses
      );

      return {
        ...state,
        loadformdata: { ...action.payload },
        loadPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "dischargeformdata": {
      let dischargePortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "dischargePortExpenses",
        dischargePortExpenses
      );

      return {
        ...state,
        dischargeformdata: { ...action.payload },
        dischargePortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "reposformdata": {
      let reposPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = voyageEstimateCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "reposPortExpenses",
        reposPortExpenses
      );

      return {
        ...state,
        reposformdata: { ...action.payload },
        reposPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "updateestimate": {
      const {
        vessel_details,
        cargo_details,
        voyage_results,
        co2_view,
        blastformdata,
        loadformdata,
        dischargeformdata,
        reposformdata,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = action.payload;

      return {
        ...state,
        vessel_details: { ...vessel_details },
        cargo_details: { ...cargo_details },
        voyage_results: { ...voyage_results },
        co2_view: { ...co2_view },
        estimate_id: action.payload.estimate_id,
        id: action.payload.id,
        blastformdata: { ...blastformdata },
        loadformdata: { ...loadformdata },
        dischargeformdata: { ...dischargeformdata },
        reposformdata: { ...reposformdata },
        blasttoloadDistance: blasttoloadDistance,
        loadTodischargeDistance: loadTodischargeDistance,
        dischargetoreposDistance: dischargetoreposDistance
          ? dischargetoreposDistance
          : 0,
      };
    }

    case "reset": {
      let state1 = action.payload;
      return { ...state1 };
    }

    case "spd_laden": {
      const { laden_spd } = action.payload;
      const { vessel_details } = state;
      return {
        ...state,
        vessel_details: {
          ...vessel_details,
          laden_spd: laden_spd,
        },
      };
    }

    case "spd_ballast": {
      const { ballast_spd } = action.payload;
      const { vessel_details } = state;
      return {
        ...state,
        vessel_details: {
          ...vessel_details,
          ballast_spd: ballast_spd,
        },
      };
    }
    default:
      return state;
  }
};
