import dayjs from "dayjs";
import { VoyageReletCalculation, nextdate } from "../helperfunction";
import moment from "moment";
import { Co2Calculation } from "../helperfunction";
export const initform = {
  cargo_details: {
    cargo: "Select Cargo",
    commission: "0.00",
    cp_qty: "0.00",
    cp_qty_type: "MT",
    frt_rate: "0.00",
    frt_type: 5,
    frt_unit: "USD",
    owner: "Select Owner",
    other_rev: "0.00",
  },
  co2_view: {
    total_bunker_cons: "0.00",
    total_co2: "0.00",
    euets: "0.00",
    emmission_mt: "",
    co2_dwt: "0.00",
    total_distance: "0.00",
    transport_work: "0.00",
    attained_cii: "0.00",

    eeoi: "0.00",
  },
  estimate_id: "",
  sale_freight: {
    s_cargo: "Select Cargo",
    s_commission: "0.00",
    s_cp_qty: "0.00",
    s_cp_qty_type: "MT",
    s_frt_rate: "0.00",
    s_frt_type: 5,
    s_frt_unit: "USD",
    s_other_exp: "0.00",
  },
  vessel_details: {
    ballast_port: "",
    ballast_spd: "0.00",
    bunker_gas_price: "0.00",
    bunker_oil_price: "0.00",
    discharge_port: "",
    laden_spd: "0.00",
    load_port: "",
    lsmgo_cons_b: "0.00",
    lsmgo_cons_l: "0.00",
    lsmgo_fuel: 7,
    port_days: 4,
    repos_port: "",
    routing: "1",
    sea_days: "0.00",
    tci_add_comm: "",
    tci_bro_comm: "",
    tci_daily_cost: "0.00",
    vessel_name: "",
    vlsfo_cons_b: "0.00",
    vlsfo_cons_l: "0.00",
    vlsfo_fuel: 5,
  },
  voyage_results: {
    daily_profit: "0.00",
    gross_income: "0.00",
    net_income: "0.00",
    other_expenses: "0.00",
    total_expenses: "0.00",
    total_port_expenses: "0.00",
    total_profit: "0.00",
    commence_date: dayjs(),
    completed_date: "",
    total_voyage_days: "0.00",
  },
  co2_calc_value: {
    vlsfo_fuel_multiplier: 3.15,
    lsmgo_fuel_multiplier: 3.15,
    ballast_port_eu_country: false,
    load_port_eu_country: false,
    redelivery_port_eu_country: false,
    repos_port_eu_country: false,
  },

  blastformdata: {port_expense: 0,},
  loadformdata: {port_expense: 0,},
  dischargeformdata: {port_expense: 0,},
  reposformdata: {port_expense: 0,},
  blasttoloadDistance: 0,
  loadTodischargeDistance: 0,
  dischargetoreposDistance: 0,
  blastPortExpenses: 0,
  loadPortExpenses: 0,
  dischargePortExpenses: 0,
  reposPortExpenses: 0,
};

export const estimateReducer = (state, action) => {
  switch (action.type) {
    case "estimate_id":
      return {
        ...state,
        estimate_id: action.payload.estimate_id,
      };

    case "vessel_name":
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vessel_name: action.payload.vessel_name,
          vessel_code: action.payload.vessel_code,
          vessel_id: action.payload.vessel_id,
          dwt: action.payload.dwt,
          laden_spd: action.payload.laden_spd,
          ballast_spd: action.payload.ballast_spd,
        },
        co2_view: {
          ...state.co2_view,
          co2_dwt: action.payload.dwt,
        },
      };

    case "tci_daily_cost": {
      let dailyHire = action.payload;

      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "vessel_details",
        "tci_daily_cost",
        dailyHire
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          tci_daily_cost: dailyHire,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
        },
      };
    }

    case "tci_add_comm": {
      const value = action.payload;

      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "vessel_details",
        "tci_add_comm",
        value
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          tci_add_comm: value,
        },
        voyage_results: {
          ...state.voyage_results,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "tci_bro_comm": {
      const value = action.payload;

      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "vessel_details",
        "tci_bro_comm",
        value
      );
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          tci_bro_comm: value,
        },
        voyage_results: {
          ...state.voyage_results,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "laden_spd": {
      const value = action.payload;
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          laden_spd: value,
        },
      };
    }

    case "ballast_spd": {
      const value = action.payload;
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          ballast_spd: value,
        },
      };
    }

    case "cons_fuel": {
      let vlsfo_blast = +action?.payload?.vlsfo_blast[0];
      let vlsfo_laden = +action?.payload?.vlsfo_laden[0];
      let lsmgo_blast = +action?.payload?.lsmgo_blast[0];
      let lsmgo_laden = +action?.payload?.lsmgo_laden[0];

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_cons_b: vlsfo_blast,
          lsmgo_cons_b: lsmgo_blast,
          vlsfo_cons_l: vlsfo_laden,
          lsmgo_cons_l: lsmgo_laden,
        },
      };
    }

    case "vlsfo_fuel": {
      let vlsfo_blast = 0,
        vlsfo_laden = 0;
        let currentIndexOfSpeedLaden = action?.payload?.indexLaden
        let currentIndexOfSpeedBallast = action?.payload?.indexBallast
       
      if (action.payload.value == 10) {
        // ulsfo fuel consumption
        vlsfo_blast = action?.payload?.fuelarr?.ulsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden = action?.payload?.fuelarr?.ulsfo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 2) {
        // hfo fuel consumption
        vlsfo_blast = action?.payload?.fuelarr?.ulsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden = action?.payload?.fuelarr?.ulsfo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 5) {
        // vlsfo fuel consumption
        vlsfo_blast = action?.payload?.fuelarr?.vlsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden = action?.payload?.fuelarr?.vlsfo_laden[currentIndexOfSpeedLaden];
      } else {
        vlsfo_blast = 0;
        vlsfo_laden = 0;
      }
      
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          vlsfo_fuel: action.payload.value,
          vlsfo_cons_b: vlsfo_blast,
          vlsfo_cons_l: vlsfo_laden,
        },
        co2_calc_value: {
          ...state.co2_calc_value,
          vlsfo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "lsmgo_fuel": {
      let lsmgo_blast = 0,
        lsmgo_laden = 0;
        let currentIndexOfSpeedBallast = action?.payload?.indexBallast
        let currentIndexOfSpeedLaden = action?.payload?.indexLaden
      if (action.payload.value == 7) {
        // LSMGO fuel consumption
        lsmgo_blast = action?.payload?.fuelarr?.lsmgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden = action?.payload?.fuelarr?.lsmgo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 4) {
        // MGO fuel consumption
        lsmgo_blast = action?.payload?.fuelarr?.mgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden = action?.payload?.fuelarr?.mgo_laden[currentIndexOfSpeedLaden];
      } else {
        lsmgo_blast = 0;
        lsmgo_laden = 0;
      }

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          lsmgo_fuel: action.payload.value,
          lsmgo_cons_b: lsmgo_blast,
          lsmgo_cons_l: lsmgo_laden,
        },
        co2_calc_value: {
          ...state.co2_calc_value,
          lsmgo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "ballast_port": {
      const { portdistance, port_name } = action.payload;

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          ballast_port: port_name,
        },
      };
    }

    case "load_port": {
      const { portdistance, port_name } = action.payload;

     
      let blasttoloadDistance = portdistance.blasttoload_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        0,
        0,
        "vessel_details",
        "load_port",
        port_name
      );

     
      let commenceDate = state.vessel_details.commence_date;
      let completedDate = nextdate(commenceDate, totalVoyageDays);

      // co2 calculation
      
      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = Co2Calculation(state, "vessel_details", "load_port", port_name);

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          load_port: port_name,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "discharge_port": {
      const { portdistance, port_name } = action.payload;

      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        0,
        "vessel_details",
        "discharge_port",
        port_name
      );

      // c02 calculations
      let blastSpeed = +state.vessel_details.ballast_spd;
      let ladenSpeed = +state.vessel_details.laden_spd;
      let dwt = +state.vessel_details.dwt;
      let vlsfoConsBlast = +state.vessel_details.vlsfo_cons_b;
      let vlsfoConsLaden = +state.vessel_details.vlsfo_cons_l;
      let lsmgoConsBlast = +state.vessel_details.lsmgo_cons_b;
      let lsmgoConsLaden = +state.vessel_details.lsmgo_cons_l;
      let vlsfofuelmultiplier = +state?.co2_calc_value?.vlsfo_fuel_multiplier;
      let lsmgofuelmultiplier = +state?.co2_calc_value?.lsmgo_fuel_multiplier;
      let ballast_port_eu_country =
        state.co2_calc_value.ballast_port_eu_country;
      let load_port_eu_country = state.co2_calc_value.load_port_eu_country;
      let discharge_port_eu_country =
        state.co2_calc_value.discharge_port_eu_country;
      let repos_port_eu_country = state.co2_calc_value.repos_port_eu_country;

      const c02values = Co2Calculation(
        blastSpeed,
        ladenSpeed,
        dwt,
        vlsfoConsBlast,
        vlsfoConsLaden,
        lsmgoConsBlast,
        lsmgoConsLaden,
        vlsfofuelmultiplier,
        lsmgofuelmultiplier,
        ballast_port_eu_country,
        load_port_eu_country,
        discharge_port_eu_country,
        repos_port_eu_country,
        blasttoloadDistance,
        loadTodischargeDistance
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          discharge_port: port_name,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(c02values.totalBunkerCons)
            ? 0
            : c02values.totalBunkerCons.toFixed(2),
          total_distance: isNaN(c02values.totalDistance)
            ? 0
            : c02values.totalDistance.toFixed(2),
          total_co2: isNaN(c02values.totalco2)
            ? 0
            : c02values.totalco2.toFixed(2),
          euets: isNaN(c02values.totaleuts)
            ? 0
            : c02values.totaleuts.toFixed(2),
          emmission_mt: isNaN(c02values.totalEmmission_mt)
            ? 0
            : c02values.totalEmmission_mt.toFixed(2),
          transport_work: isNaN(c02values.transportWork)
            ? 0
            : c02values.transportWork.toFixed(2),
          attained_cii: isNaN(c02values.totalAttaindCii)
            ? 0
            : c02values.totalAttaindCii,
          eeoi: isNaN(c02values.eeoi) ? 0 : c02values.eeoi,
        },
      };
    }

    case "repos_port": {
      const { portdistance, port_name } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "repos_port",
        port_name
      );

      // c02 calculation
      let blastSpeed = +state.vessel_details.ballast_spd;
      let ladenSpeed = +state.vessel_details.laden_spd;
      let dwt = +state.vessel_details.dwt;
      let vlsfoConsBlast = +state.vessel_details.vlsfo_cons_b;
      let vlsfoConsLaden = +state.vessel_details.vlsfo_cons_l;
      let lsmgoConsBlast = +state.vessel_details.lsmgo_cons_b;
      let lsmgoConsLaden = +state.vessel_details.lsmgo_cons_l;
      let vlsfofuelmultiplier = +state?.co2_calc_value?.vlsfo_fuel_multiplier;
      let lsmgofuelmultiplier = +state?.co2_calc_value?.lsmgo_fuel_multiplier;
      let ballast_port_eu_country =
        state.co2_calc_value.ballast_port_eu_country;
      let load_port_eu_country = state.co2_calc_value.load_port_eu_country;
      let discharge_port_eu_country =
        state.co2_calc_value.discharge_port_eu_country;
      let repos_port_eu_country = state.co2_calc_value.repos_port_eu_country;

      const c02values = Co2Calculation(
        blastSpeed,
        ladenSpeed,
        dwt,
        vlsfoConsBlast,
        vlsfoConsLaden,
        lsmgoConsBlast,
        lsmgoConsLaden,
        vlsfofuelmultiplier,
        lsmgofuelmultiplier,
        ballast_port_eu_country,
        load_port_eu_country,
        discharge_port_eu_country,
        repos_port_eu_country,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          repos_port: port_name,
          sea_days: seaDays,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          total_voyage_days: totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(c02values.totalBunkerCons)
            ? 0
            : c02values.totalBunkerCons.toFixed(2),
          total_distance: isNaN(c02values.totalDistance)
            ? 0
            : c02values.totalDistance.toFixed(2),
          total_co2: isNaN(c02values.totalco2)
            ? 0
            : c02values.totalco2.toFixed(2),
          euets: isNaN(c02values.totaleuts)
            ? 0
            : c02values.totaleuts.toFixed(2),
          emmission_mt: isNaN(c02values.totalEmmission_mt)
            ? 0
            : c02values.totalEmmission_mt.toFixed(2),
          transport_work: isNaN(c02values.transportWork)
            ? 0
            : c02values.transportWork.toFixed(2),
          attained_cii: isNaN(c02values.totalAttaindCii)
            ? 0
            : c02values.totalAttaindCii,
          eeoi: isNaN(c02values.eeoi) ? 0 : c02values.eeoi,
        },
      };
    }

    case "eu_country": {
      return {
        ...state,
        co2_calc_value: {
          ...state.co2_calc_value,
          ...action.payload,
        },
      };
    }

    case "bunker_oil_price": {
      const { value, portdistance } = action.payload;

      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "bunker_oil_price",
        value
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          bunker_oil_price: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "bunker_gas_price": {
      const { value, portdistance } = action.payload;
      let blasttoloadDistance = portdistance.blasttoload_ref.current;
      let loadTodischargeDistance = portdistance.loadtodischarge_ref.current;
      let dischargetoreposDistance = portdistance.dischargetorepos_ref.current;

      const {
        seaDays,
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        "vessel_details",
        "bunker_oil_price",
        value
      );

      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          bunker_gas_price: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "blasttoloadDistance": {
      return {
        ...state,
        blasttoloadDistance: action.payload,
      };
    }

    case "blastformdata": {
      let blastPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "blastPortExpenses",
        blastPortExpenses
      );


      return {
        ...state,
        blastformdata: { ...action.payload },
        blastPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "loadformdata": {
      
      let loadPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "loadPortExpenses",
        loadPortExpenses
      );
      return {
        ...state,
        loadformdata: { ...action.payload },
        loadPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "dischargeformdata": {
      let dischargePortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "dischargePortExpenses",
        dischargePortExpenses
      );
      return {
        ...state,
        dischargeformdata: { ...action.payload },
        dischargePortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "reposformdata": {
      let reposPortExpenses = +action.payload.port_expense;
      const {
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
      } = VoyageReletCalculation(
        state,
        blasttoloadDistance,
        loadTodischargeDistance,
        dischargetoreposDistance,
        null,
        "reposPortExpenses",
        reposPortExpenses
      );
      
      return {
        ...state,
        reposformdata: { ...action.payload },
        reposPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),

          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalexpense) ? 0 : totalexpense.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "routing":
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          routing: action.payload,
        },
      };

    case "port_days": {
      let portDays = action.payload;
      let seaDays = Number(state.vessel_details.sea_days);
      let totalVoyageDays = seaDays + Number(portDays);
      let commence_date = state.voyage_results.commence_date;
      let completedDate = nextdate(commence_date, totalVoyageDays);
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          port_days: portDays,
        },
        voyage_results: {
          ...state.voyage_results,
          port_days: portDays,
          total_voyage_days: totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "sea_days": {
      let seaDays = action.payload;
      let portDays = Number(state.vessel_details.port_days);
      let totalVoyageDays = Number(seaDays) + portDays;
      let commence_date = state.voyage_results.commence_date;
      let completedDate = nextdate(commence_date, totalVoyageDays);
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
          sea_days: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          total_voyage_days: totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "total_voyage_days": {
      const value = action.payload;
      return {
        ...state,
        vessel_details: {
          ...state.vessel_details,
        },
        voyage_results: {
          ...state.voyage_results,
          total_voyage_days: value,
        },
      };
    }

    // sale freight

    case "s_cargo": {
      const value = action.payload;
      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_cargo: value,
        },
      };
    }

    case "s_cp_qty": {
      const value = action.payload;

      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_cp_qty: value,
        },
      };
    }

    case "s_cp_qty_type": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_cp_qty_type: value,
        },
      };
    }

    case "s_frt_type": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "sale_freight",
        "s_frt_rate",
        value
      );

      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_frt_type: value,
        },
        voyage_results: {
          ...state.voyage_results,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "s_frt_unit": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_frt_unit: value,
        },
      };
    }

    case "s_frt_rate": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "sale_freight",
        "s_frt_rate",
        value
      );

      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_frt_rate: value,
        },
        voyage_results: {
          ...state.voyage_results,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "s_commission": {
      const value = action.payload;

      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "sale_freight",
        "s_commission",
        value
      );

      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_commission: value,
        },
        voyage_results: {
          ...state.voyage_results,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "s_other_exp": {
      const value = action.payload;

      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "sale_freight",
        "s_other_exp",
        value
      );

      return {
        ...state,
        sale_freight: {
          ...state.sale_freight,
          s_other_exp: value,
        },
        voyage_results: {
          ...state.voyage_results,
          other_expenses: value,
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    // cargo buy freight

    case "cargo": {
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          cargo: action.payload,
        },
      };
    }

    case "cp_qty": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "cargo_details",
        "cp_qty",
        value
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          cp_qty: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "cp_qty_type": {
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          cp_qty_type: action.payload,
        },
      };
    }

    case "frt_type": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_type: value,
        },
      };
    }

    case "frt_unit": {
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_unit: action.payload,
        },
      };
    }

    case "frt_rate": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "cargo_details",
        "frt_rate",
        value
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          frt_rate: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "commission": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "cargo_details",
        "commission",
        value
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          commission: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "other_rev": {
      const value = action.payload;
      const {
        totalVoyageDays,
        grossIncome,
        netIncome,
        totalExpenses,
        totalProfit,
        dailyProfit,
        completedDate,
      } = VoyageReletCalculation(
        state,
        0,
        0,
        0,
        "cargo_details",
        "other_rev",
        value
      );

      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          other_rev: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
        },
      };
    }

    case "owner": {
      return {
        ...state,
        cargo_details: {
          ...state.cargo_details,
          owner: action.payload,
        },
      };
    }

    // voyage results

    case "gross_income": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          gross_income: action.payload,
        },
      };
    }

    case "net_income": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          net_income: action.payload,
        },
      };
    }

    case "total_port_expenses": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          total_port_expenses: action.payload,
        },
      };
    }

    case "other_expenses": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          other_expenses: action.payload,
        },
      };
    }

    case "total_expenses": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          total_expenses: action.payload,
        },
      };
    }

    case "total_profit": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          total_profit: action.payload,
        },
      };
    }

    case "daily_profit": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          daily_profit: action.payload,
        },
      };
    }

    case "commence_date": {
      const value = action.payload;
      let totalvoyagedays = +state.voyage_results.total_voyage_days;
      let completedDate = nextdate(value, totalvoyagedays);

      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          commence_date: dayjs(value).format("YYYY-MM-DD HH:mm:ss"),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "completed_date": {
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          completed_date: action.payload,
        },
      };
    }

    case "total_voyage_days": {
      const value = action.payload;
      let commenceDate = state.voyage_results.commence_date;
      let completedDate = nextdate(commenceDate, value);
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
          total_voyage_days: value,
        },
        
      };
    }

    // co2 view

    case "total_bunker_cons": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: value,
        },
      };
    }

    case "total_co2": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_co2: value,
        },
      };
    }

    case "euets": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          euets: value,
        },
      };
    }

    case "carbon_exposer": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          carbon_exposer: value,
        },
      };
    }

    case "cii_rating": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          cii_rating: value,
        },
      };
    }

    // case "cii_band": {
    //   const value = isNaN(action.payload) ? 0 : action.payload;
    //   return {
    //     ...state,
    //     co2_view: {
    //       ...state.co2_view,
    //       cii_band: value,
    //     },
    //   };
    // }

    case "co2_dwt": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          co2_dwt: value,
        },
      };
    }

    case "total_distance": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_distance: value,
        },
      };
    }

    case "emission": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          emission: value,
        },
      };
    }

    case "aer_score": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          aer_score: value,
        },
      };
    }

    case "adjustment": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          adjustment: value,
        },
      };
    }

    case "transport_work": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          transport_work: value,
        },
      };
    }

    case "eeoi": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          eeoi: value,
        },
      };
    }

    case "updateestimate": {
      const {
        vessel_details,
        cargo_details,
        sale_freight,
        voyage_results,
        co2_view,
        blastformdata,
        dischargeformdata,
        loadformdata,
        reposformdata,
      } = action.payload;
      return {
        ...state,
        vessel_details: { ...vessel_details },
        cargo_details: { ...cargo_details },
        voyage_results: { ...voyage_results },
        co2_view: { ...co2_view },
        blastformdata: { ...blastformdata },
        dischargeformdata: { ...dischargeformdata },
        loadformdata: { ...loadformdata },
        reposformdata: { ...reposformdata },
        sale_freight: { ...sale_freight },
        estimate_id: action.payload.estimate_id,
        id: action.payload.id,
      };
    }

    case "reset": {
      let state1 = action.payload;
      return { ...state1 };
    }

    case 'spd_laden': {
      const {laden_spd, vlsfo_cons_l, lsmgo_cons_l} = action.payload;
    
      const {vessel_details} = state;
     
      return {
        ...state,
        vessel_details : {
          ...vessel_details, laden_spd : laden_spd,vlsfo_cons_l: vlsfo_cons_l, lsmgo_cons_l : lsmgo_cons_l
        }
      }
    }

    case 'spd_ballast': {
      const {ballast_spd,lsmgo_cons_b,vlsfo_cons_b} = action.payload;
      const {vessel_details} = state;
     
      return {
        ...state,
        vessel_details : {
          ...vessel_details, ballast_spd : ballast_spd, lsmgo_cons_b : lsmgo_cons_b,vlsfo_cons_b : vlsfo_cons_b
        }
      }
    }

   
  }
  return state;
};
