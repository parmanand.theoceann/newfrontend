import dayjs from "dayjs";
import moment from "moment";
import { nextdate } from "../helperfunction";
import { tcEstimateCalculation } from "../helperfunction";
import { ZoomInOutlined } from "@ant-design/icons";
import { TcCoCo2Calculation } from "../tcHelper";

export const initform = {
  estimate_id: "",
  tci_details: {
    vessel_name: "",
    dwt: "",
    laden_spd: "0.00",
    ballast_spd: "0.00",
    tci_daily_cost: "0.00",
    tci_add_comm: "0.00",
    tci_bro_comm: "0.00",
    blast_bonus: "0.00",
    other_cost: "0.00",
    vlsfo_fuel: 5,
    lsmgo_fuel: 7,
    vlsfo_cons_b: "0.00",
    lsmgo_cons_b: "0.00",
    lsmgo_cons_l: "0.00",
    vlsfo_cons_l: "0.00",
    bunker_oil_price: "0.00",
    bunker_gas_price: "0.00",
    ballast_port: "",
    delivery_port: "",
    redelivery_port: "",
    repos_port: "",
    routing: "1",
  },
  tco_details: {
    tco_duration: "0.00",
    tco_d_hire: "0.00",
    tco_add_per: "22",
    tco_bro_per: "11",
    mis_revenue: "0.00",
    tco_bb: "0.00",
    port_days: 4,
    sea_days: "0.00",
    commence_date: dayjs(),
    completed_date: "",
    total_voyage_days: "0.00",
  },
  voyage_results: {
    gross_income: "0.00",
    net_income: "0.00",
    total_port_expenses: "0.00",
    total_bunker_expenses: "0.00",
    vessel_hire: "0.00",
    total_expenses: "0.00",
    total_profit: "0.00",
    daily_profit: "0.00",
    tce: "0.00",
  },
  co2_view: {
    total_bunker_cons: "0.00",
    total_co2: "0.00",
    euets: "0.00",
    emmission_mt: "",
    co2_dwt: "0.00",
    total_distance: "0.00",
    transport_work: "0.00",
    attained_cii: "0.00",
    eeoi: "0.00",
  },

  co2_calc_value: {
    vlsfo_fuel_multiplier: 3.15,
    lsmgo_fuel_multiplier: 3.15,
    ballast_port_eu_country: false,
    load_port_eu_country: false,
    redelivery_port_eu_country: false,
    repos_port_eu_country: false,
  },

  blastformdata: { port_expense: 0 },
  deliveryformdata: { port_expense: 0 },
  redeliveryformdata: { port_expense: 0 },
  reposformdata: { port_expense: 0 },
  seca_crossed: {
    blseca: 0,
    blcrossed: "",
    ldseca: 0,
    ldcrossed: "",
    drseca: 0,
    drcrossed: "",
  },

  blastTodeliveryDistance: 0,
  deliveryToredeliveryDistance: 0,
  redeliveryToreposDistance: 0,
  blastPortExpenses: 0,
  deliveryPortExpenses: 0,
  redeliveryPortExpenses: 0,
  reposPortExpenses: 0,
};

export const estimateReducer = (state, action) => {
  switch (action.type) {
    case "estimate_id":
      return {
        ...state,
        estimate_id: action.payload.estimate_id,
      };

    case "updatefull": {
      const {
        tcidetails,
        tcodetails,
        _blastformdata,
        _deliveryformdata,
        _redeliveryformdata,
        _reposformdata,
        _blastTodeliveryDistance,
        _deliveryToredeliveryDistance,
        _redeliveryToreposDistance,
        ballast_port_eu_country,
        load_port_eu_country,
        redelivery_port_eu_country,
        repos_port_eu_country,
      } = action.payload;
      return {
        ...state,
        tci_details: { ...state.tci_details, ...tcidetails },
        tco_details: { ...state.tco_details, ...tcodetails },
        blastformdata: { ...state.blastformdata, ..._blastformdata },
        deliveryformdata: { ...state.deliveryformdata, ..._deliveryformdata },
        redeliveryformdata: {
          ...state.redeliveryformdata,
          ..._redeliveryformdata,
        },
        reposformdata: { ...state.reposformdata, ..._reposformdata },

        blastTodeliveryDistance: _blastTodeliveryDistance,
        deliveryToredeliveryDistance: _deliveryToredeliveryDistance,
        redeliveryToreposDistance: _redeliveryToreposDistance,
        co2_calc_value: {
          ...state.co2_calc_value,
          ballast_port_eu_country: ballast_port_eu_country,
          load_port_eu_country: load_port_eu_country,
          redelivery_port_eu_country: redelivery_port_eu_country,
          repos_port_eu_country: repos_port_eu_country,
        },
      };
    }

    case "updatecalculation": {
      const { calculationdata, co2calculation } = action.payload;

      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = calculationdata;

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = co2calculation;
      return {
        ...state,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(portExpenses) ? 0 : portExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },

        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    // tci details
    case "vessel_name": {
      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          vessel_name: action.payload.vessel_name,
          vessel_code: action.payload.vessel_code,
          vessel_id: action.payload.vessel_id,
          dwt: action.payload.dwt,
          laden_spd: action.payload.laden_spd,
          ballast_spd: action.payload.ballast_spd,
        },
        co2_view: {
          ...state.co2_view,
          co2_dwt: action.payload.dwt,
        },
      };
    }
    case "blastTodeliveryDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;

      return {
        ...state,
        blastTodeliveryDistance: totaldistance,
        seca_crossed: {
          ...state.seca_crossed,
          blseca: secaLength,
          blcrossed: crossed,
        },
      };
    }

    case "deliveryToredeliveryDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;

      return {
        ...state,
        deliveryToredeliveryDistance: totaldistance,

        seca_crossed: {
          ...state.seca_crossed,
          ldseca: secaLength,
          ldcrossed: crossed,
        },
      };
    }

    case "redeliveryToreposDistance": {
      const { totaldistance, secaLength, crossed } = action.payload;
   
      return {
        ...state,
        redeliveryToreposDistance: totaldistance,
        seca_crossed: {
          ...state.seca_crossed,
          drseca: secaLength,
          drcrossed: crossed,
        },
      };
    }

    case "tci_daily_cost": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "tci_daily_cost",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          tci_daily_cost: value,
        },

        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(portExpenses) ? 0 : portExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "tci_add_comm": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "tci_add_comm",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          tci_add_comm: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "tci_bro_comm": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "tci_bro_comm",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          tci_bro_comm: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "blast_bonus": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "blast_bonus",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          blast_bonus: value,
        },

        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "other_cost": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "blast_bonus",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          other_cost: value,
        },

        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "laden_spd": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "laden_spd",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          laden_spd: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "ballast_spd": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "laden_spd",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          ballast_spd: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "cons_fuel": {
      let vlsfo_blast = +action?.payload?.vlsfo_blast?.[0] ?? 0;
      let vlsfo_laden = +action?.payload?.vlsfo_laden?.[0] ?? 0;

      let lsmgo_blast = +action?.payload?.lsmgo_blast?.[0] ?? 0;
      let lsmgo_laden = +action?.payload?.lsmgo_laden?.[0] ?? 0;

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          vlsfo_cons_b: vlsfo_blast,
          lsmgo_cons_b: lsmgo_blast,
          vlsfo_cons_l: vlsfo_laden,
          lsmgo_cons_l: lsmgo_laden,
        },
      };
    }

    case "vlsfo_fuel": {
      let vlsfo_blast = 0,
        vlsfo_laden = 0;
      let currentIndexOfSpeedLaden = action?.payload?.indexLaden;
      let currentIndexOfSpeedBallast = action?.payload?.indexBallast;
      if (action.payload.value == 10) {
        // ulsfo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.ulsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.ulsfo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 2) {
        // ifo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.ifo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.ifo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 5) {
        // vlsfo fuel consumption
        vlsfo_blast =
          action?.payload?.fuelarr?.vlsfo_blast[currentIndexOfSpeedBallast];
        vlsfo_laden =
          action?.payload?.fuelarr?.vlsfo_laden[currentIndexOfSpeedLaden];
      } else {
        vlsfo_blast = 0;
        vlsfo_laden = 0;
      }

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          vlsfo_fuel: action.payload.value,
          vlsfo_cons_b: vlsfo_blast,
          vlsfo_cons_l: vlsfo_laden,
        },
        co2_calc_value: {
          ...state.co2_calc_value,
          vlsfo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "lsmgo_fuel": {
      let lsmgo_blast = 0,
        lsmgo_laden = 0;
      let currentIndexOfSpeedBallast = action?.payload?.indexBallast;
      let currentIndexOfSpeedLaden = action?.payload?.indexLaden;
      if (action.payload.value == 7) {
        // LSMGO fuel consumption
        lsmgo_blast =
          action?.payload?.fuelarr?.lsmgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden =
          action?.payload?.fuelarr?.lsmgo_laden[currentIndexOfSpeedLaden];
      } else if (action.payload.value == 4) {
        // MGO fuel consumption
        lsmgo_blast =
          action?.payload?.fuelarr?.mgo_blast[currentIndexOfSpeedBallast];
        lsmgo_laden =
          action?.payload?.fuelarr?.mgo_laden[currentIndexOfSpeedLaden];
      } else {
        lsmgo_blast = 0;
        lsmgo_laden = 0;
      }

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          lsmgo_fuel: action.payload.value,
          lsmgo_cons_b: lsmgo_blast,
          lsmgo_cons_l: lsmgo_laden,
        },

        co2_calc_value: {
          ...state.co2_calc_value,
          lsmgo_fuel_multiplier: action.payload.multiplier,
        },
      };
    }

    case "vlsfo_cons_b": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "vlsfo_cons_b",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          vlsfo_cons_b: isNaN(value) ? 0 : value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "vlsfo_cons_l": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "vlsfo_cons_l",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          vlsfo_cons_l: isNaN(value) ? 0 : value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "lsmgo_cons_b": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "lsmgo_cons_b",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          lsmgo_cons_b: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "lsmgo_cons_l": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "lsmgo_cons_b",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          lsmgo_cons_l: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "ballast_port": {
      const { port_name, portdistance } = action.payload;

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          ballast_port: port_name,
        },
      };
    }

    case "delivery_port": {
      const { port_name, portdistance } = action.payload;
      const blasttoDeliveryDistance = portdistance.blastTodelivery_ref.current;

      const value = port_name;
      const { deliveryToredeliveryDistance, redeliveryToreposDistance } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blasttoDeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "delivery_port",
        value
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = TcCoCo2Calculation(state, "tci_details", "delivery_port", value);

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          delivery_port: port_name,
        },
        tco_details: {
          ...state.tco_details,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: completedDate,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },

        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "redelivery_port": {
      const { port_name, portdistance } = action.payload;
      const blasttoDeliveryDistance = portdistance.blastTodelivery_ref.current;
      const deliveryToRedeliveryDistance =
        portdistance.deliveryToredelivery_ref.current;

      const value = port_name;
      const { redeliveryToreposDistance } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blasttoDeliveryDistance,
        deliveryToRedeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "redelivery_port",
        value
      );

      // co2 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = TcCoCo2Calculation(state, "tci_details", "redelivery_port", value);

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          redelivery_port: port_name,
        },
        tco_details: {
          ...state.tco_details,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: completedDate,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "eu_country": {
      return {
        ...state,
        co2_calc_value: {
          ...state.co2_calc_value,
          ...action.payload,
        },
      };
    }

    case "repos_port": {
      const { port_name, portdistance } = action.payload;

      const blasttoDeliveryDistance = portdistance.blastTodelivery_ref.current;
      const deliveryToRedeliveryDistance =
        portdistance.deliveryToredelivery_ref.current;
      const redeliveryToreposDistance =
        portdistance.redeliveryTorepos_ref.current;

      const value = port_name;

      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blasttoDeliveryDistance,
        deliveryToRedeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "repos_port",
        value
      );

      // c02 calculation

      const {
        totalBunkerCons,
        totalco2,
        totaleuts,
        totalEmmission_mt,
        totalDistance,
        transportWork,
        totalAttaindCii,
        eeoi,
      } = TcCoCo2Calculation(state, "tci_details", "redelivery_port", value);

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          repos_port: port_name,
        },
        tco_details: {
          ...state.tco_details,
          sea_days: isNaN(seaDays) ? 0 : seaDays.toFixed(2),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: completedDate,
        },
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: isNaN(totalBunkerCons)
            ? 0
            : totalBunkerCons.toFixed(2),
          total_distance: isNaN(totalDistance) ? 0 : totalDistance.toFixed(2),
          total_co2: isNaN(totalco2) ? 0 : totalco2.toFixed(2),
          euets: isNaN(totaleuts) ? 0 : totaleuts.toFixed(2),
          emmission_mt: isNaN(totalEmmission_mt)
            ? 0
            : totalEmmission_mt.toFixed(2),
          transport_work: isNaN(transportWork) ? 0 : transportWork.toFixed(2),
          attained_cii: isNaN(totalAttaindCii) ? 0 : totalAttaindCii,
          eeoi: isNaN(eeoi) ? 0 : eeoi,
        },
      };
    }

    case "bunker_oil_price": {
      const { value, portdistance } = action.payload;

      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalPortExpenses,
        totalProfit,
        dailyProfit,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "bunker_oil_price",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          bunker_oil_price: value,
        },

        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "bunker_gas_price": {
      const { value, portdistance } = action.payload;

      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        totalPortExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tci_details",
        "bunker_gas_price",
        value
      );

      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          bunker_gas_price: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(totalPortExpenses)
            ? 0
            : totalPortExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "routing": {
      return {
        ...state,
        tci_details: {
          ...state.tci_details,
          routing: action.payload,
        },
      };
    }

    // tco details

    case "tco_duration": {
      const value = action.payload;

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          tco_duration: value,
        },
      };
    }

    case "tco_d_hire": {
      const value = action.payload;

      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tco_details",
        "tco_d_hire",
        value
      );

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          tco_d_hire: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "tco_add_per": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tco_details",
        "tco_add_per",
        value
      );

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          tco_add_per: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "tco_bro_per": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tco_details",
        "tco_bro_per",
        value
      );
      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          tco_bro_per: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "mis_revenue": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tco_details",
        "mis_revenue",
        value
      );

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          mis_revenue: value,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "tco_bb": {
      const value = action.payload;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        grossIncome,
        netIncome,
        completedDate,
        totalExpenses,
        totalBunkerExpenses,
        totalProfit,
        dailyProfit,
        portExpenses,
        totalVoyageDays,
        tce,
        vesselHire,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        "tco_details",
        "tco_bb",
        value
      );

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          tco_bb: action.payload,
        },
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome.toFixed(2),
          net_income: isNaN(netIncome) ? 0 : netIncome.toFixed(2),
          total_port_expenses: isNaN(portExpenses)
            ? 0
            : portExpenses.toFixed(2),
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses.toFixed(2),
          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses.toFixed(2),
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "port_days": {
      let seaDays = +state.tco_details.sea_days;
      let portDays = action.payload;
      let commenceDate = state.tco_details.commence_date;
      let totalVoyageDays = Number(seaDays) + Number(portDays);
      let completedDate = nextdate(commenceDate, totalVoyageDays);

      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          port_days: portDays,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
        },
      };
    }

    case "sea_days": {
      let portDays = +state.tco_details.port_days;
      let seaDays = action.payload;
      let commenceDate = state.tco_details.commence_date;
      let totalVoyageDays = Number(seaDays) + Number(portDays);
      let completedDate = nextdate(commenceDate, totalVoyageDays);
      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          sea_days: seaDays,
          total_voyage_days: isNaN(totalVoyageDays)
            ? 0
            : totalVoyageDays.toFixed(2),
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "commence_date": {
      let commenceDate = isNaN(action.payload) ? "" : action.payload;
      let totalVoyageDays = +state.tco_details.total_voyage_days;
      let completedDate = nextdate(commenceDate, totalVoyageDays);
      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          commence_date: action.payload,
          completed_date: dayjs(completedDate).format("YYYY-MM-DD HH:mm:ss"),
        },
      };
    }

    case "completed_date": {
      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          completed_date: action.payload,
        },
      };
    }

    case "total_voyage_days": {
      return {
        ...state,
        tco_details: {
          ...state.tco_details,
          total_voyage_days: action.payload,
        },
      };
    }

    // co2 view

    case "total_bunker_cons": {
      const value = action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: value,
        },
      };
    }

    case "total_co2": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_co2: value,
        },
      };
    }

    case "euets": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          euets: value,
        },
      };
    }

    case "carbon_exposer": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          carbon_exposer: value,
        },
      };
    }

    case "cii_rating": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          cii_rating: value,
        },
      };
    }

    case "co2_dwt": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          co2_dwt: value,
        },
      };
    }

    case "total_distance": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_distance: value,
        },
      };
    }

    case "emission": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          emission: value,
        },
      };
    }

    case "aer_score": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          aer_score: value,
        },
      };
    }

    case "adjustment": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          adjustment: value,
        },
      };
    }

    case "transport_work": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          transport_work: value,
        },
      };
    }

    case "eeoi": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          eeoi: value,
        },
      };
    }

    case "blastformdata": {
      let blastPortExpenses = +action.payload.port_expense;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalExpenses,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        null,
        "blastPortExpenses",
        blastPortExpenses
      );

      // blastPortExpenses: 0,
      // deliveryPortExpenses: 0,
      // redeliveryPortExpenses: 0,
      // reposPortExpenses: 0,
      return {
        ...state,
        blastformdata: { ...action.payload },
        blastPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(totalPortExpenses) ? 0 : totalPortExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "deliveryformdata": {
      let deliveryPortExpenses = +action.payload.port_expense;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalExpenses,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        null,
        "deliveryPortExpenses",
        deliveryPortExpenses
      );
      return {
        ...state,
        deliveryformdata: { ...action.payload },
        deliveryPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(totalPortExpenses) ? 0 : totalPortExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "redeliveryformdata": {
      let redeliveryPortExpenses = +action.payload.port_expense;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalExpenses,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        null,
        "redeliveryPortExpenses",
        redeliveryPortExpenses
      );

      return {
        ...state,
        redeliveryformdata: { ...action.payload },
        redeliveryPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(totalPortExpenses) ? 0 : totalPortExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    case "reposformdata": {
      let reposPortExpenses = +action.payload.port_expense;
      const {
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
      } = state;
      const {
        seaDays,
        totalVoyageDays,
        completedDate,
        grossIncome,
        netIncome,
        totalBunkerExpenses,
        totalPortExpenses,
        vesselHire,
        totalexpense,
        totalProfit,
        dailyProfit,
        tce,
        totalExpenses,
      } = tcEstimateCalculation(
        state,
        blastTodeliveryDistance,
        deliveryToredeliveryDistance,
        redeliveryToreposDistance,
        null,
        "reposPortExpenses",
        reposPortExpenses
      );
      return {
        ...state,
        reposformdata: { ...action.payload },
        reposPortExpenses: action.payload.port_expense,
        voyage_results: {
          ...state.voyage_results,
          gross_income: isNaN(grossIncome) ? 0 : grossIncome,
          net_income: isNaN(netIncome) ? 0 : netIncome,
          total_port_expenses: isNaN(totalPortExpenses) ? 0 : totalPortExpenses,
          total_bunker_expenses: isNaN(totalBunkerExpenses)
            ? 0
            : totalBunkerExpenses,

          vessel_hire: isNaN(vesselHire) ? 0 : vesselHire.toFixed(2),
          total_expenses: isNaN(totalExpenses) ? 0 : totalExpenses,
          total_profit: isNaN(totalProfit) ? 0 : totalProfit.toFixed(2),
          daily_profit: isNaN(dailyProfit) ? 0 : dailyProfit.toFixed(2),
          tce: isNaN(tce) ? 0 : tce.toFixed(2),
        },
      };
    }

    // tco details

    case "tco_duration": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, tco_duration: value },
      };
    }

    case "tco_d_hire": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, tco_d_hire: value },
      };
    }

    case "tco_add_per": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, tco_add_per: value },
      };
    }

    case "tco_bro_per": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, tco_bro_per: value },
      };
    }

    case "mis_revenue": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, mis_revenue: value },
      };
    }

    case "tco_bb": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, tco_bb: value },
      };
    }

    case "port_days": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, port_days: value },
      };
    }

    case "sea_days": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, sea_days: value },
      };
    }

    // case "commence_date": {
    //   const value = action.payload;
    //   return {
    //     ...state,
    //     tco_details: { ...state.tco_details, commence_date: value },
    //   };
    // }

    // case "completed_date": {
    //   const value = action.payload;
    //   return {
    //     ...state,
    //     tco_details: { ...state.tco_details, completed_date: value },
    //   };
    // }

    case "total_voyage_days": {
      const value = action.payload;
      return {
        ...state,
        tco_details: { ...state.tco_details, total_voyage_days: value },
      };
    }

    // co2 view

    case "total_bunker_cons": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_bunker_cons: value,
        },
      };
    }

    case "euets": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          euets: value,
        },
      };
    }

    case "carbon_exposer": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          carbon_exposer: value,
        },
      };
    }

    case "cii_rating": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          cii_rating: value,
        },
      };
    }

    // case "cii_band": {
    //   const value = isNaN(action.payload) ? 0 : action.payload;
    //   return {
    //     ...state,
    //     co2_view: {
    //       ...state.co2_view,
    //       cii_band: value,
    //     },
    //   };
    // }

    case "co2_dwt": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          co2_dwt: value,
        },
      };
    }

    case "total_distance": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          total_distance: value,
        },
      };
    }

    case "emission": {
      const value = isNaN(action.payload) ? 0 : action.payload;

      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          emission: value,
        },
      };
    }

    case "aer_score": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          aer_score: value,
        },
      };
    }

    case "adjustment": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          adjustment: value,
        },
      };
    }

    case "transport_work": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          transport_work: value,
        },
      };
    }

    case "eeoi": {
      const value = isNaN(action.payload) ? 0 : action.payload;
      return {
        ...state,
        co2_view: {
          ...state.co2_view,
          eeoi: value,
        },
      };
    }

    case "updateestimate": {
      const {
        tci_details,
        tco_details,
        voyage_results,
        co2_view,
        blastformdata,
        deliveryformdata,
        redeliveryformdata,
        reposformdata,
        blastTodeliveryDistance = 0,
        deliveryToredeliveryDistance = 0,
        redeliveryToreposDistance = 0,
      } = action.payload;
      return {
        ...state,
        tci_details: { ...tci_details },
        tco_details: { ...tco_details },
        voyage_results: { ...voyage_results },
        co2_view: { ...co2_view },
        blastformdata: { ...blastformdata },
        deliveryformdata: { ...deliveryformdata },
        redeliveryformdata: { ...redeliveryformdata },
        reposformdata: { ...reposformdata },
        estimate_id: action.payload.estimate_id,
        id: action.payload.id,
        blastTodeliveryDistance: blastTodeliveryDistance,
        deliveryToredeliveryDistance: deliveryToredeliveryDistance,
        redeliveryToreposDistance: redeliveryToreposDistance,
      };
    }

    case "reset": {
      let state1 = action.payload;
      return { ...state1 };
    }

    case "spd_laden": {
      const { laden_spd } = action.payload;
      const { tci_details } = state;
      return {
        ...state,
        tci_details: {
          ...tci_details,
          laden_spd: laden_spd,
        },
      };
    }

    case "spd_ballast": {
      const { ballast_spd } = action.payload;
      const { tci_details } = state;
      return {
        ...state,
        tci_details: {
          ...tci_details,
          ballast_spd: ballast_spd,
        },
      };
    }
  }

  return state;
};
