
export const TcCoCo2Calculation = (state,
    group = null,
    name = null,
    value = null) => {
      let blastSpeed = 0,
      ladenSpeed = 0,
      dwt = 0,
      vlsfoConsBlast = 0,
      vlsfoConsLaden = 0,
      lsmgoConsBlast = 0,
      lsmgoConsLaden = 0,
      vlsfofuelmultiplier = 1,
      lsmgofuelmultiplier = 1,
      ballast_port_eu_country,
      load_port_eu_country,
      discharge_port_eu_country,
      repos_port_eu_country,
      blasttoLoadDistance = 0,
      loadtoDischargeDistance = 0,
      dischargetoReposDistance = 0;
    blastSpeed = state?.tci_details?.ballast_spd;
    ladenSpeed = state?.tci_details?.laden_spd;
    dwt = state?.tci_details?.dwt;
    vlsfoConsBlast = state?.tci_details?.vlsfo_cons_b;
    vlsfoConsLaden = state?.tci_details?.vlsfo_cons_l;
    lsmgoConsBlast = state?.tci_details?.lsmgo_cons_b;
    lsmgoConsLaden = state?.tci_details?.lsmgo_cons_l;
    vlsfofuelmultiplier = state?.co2_calc_value?.vlsfo_fuel_multiplier;
    lsmgofuelmultiplier = state?.co2_calc_value?.lsmgo_fuel_multiplier;
    ballast_port_eu_country = state?.co2_calc_value?.ballast_port_eu_country;
    load_port_eu_country = state?.co2_calc_value?.delivery_port_eu_country;
    discharge_port_eu_country =
      state?.co2_calc_value?.redelivery_port_eu_country;
    repos_port_eu_country = state?.co2_calc_value?.repos_port_eu_country;
    blasttoLoadDistance = state?.blastTodeliveryDistance;
    loadtoDischargeDistance = state?.deliveryToredeliveryDistance;
    dischargetoReposDistance = state?.redeliveryToreposDistance;
  
    if (group && name && value) {
      state = {
        ...state,
        [group]: {
          ...state[group],
          [name]: value,
        },
      };
    }
  
    let totalBunkerCons = 0,
      totalco2 = 0,
      totaleuts = 0,
      totalEmmission_mt = 0,
      totalDistance = 0,
      transportWork = 0,
      totalAttaindCii = 0;
    // ciiBand = "";
   
    const blastToloadConsValues = portToPortConsumption(
      blasttoLoadDistance,
      blastSpeed,
      vlsfoConsBlast,
      lsmgoConsBlast,
      vlsfofuelmultiplier,
      lsmgofuelmultiplier,
      ballast_port_eu_country,
      load_port_eu_country
    );

    const loadtoDischargeconsValues = portToPortConsumption(
      loadtoDischargeDistance,
      ladenSpeed,
      vlsfoConsLaden,
      lsmgoConsLaden,
      vlsfofuelmultiplier,
      lsmgofuelmultiplier,
      load_port_eu_country,
      discharge_port_eu_country
    );
    const dischargetoReposConsValues = portToPortConsumption(
      dischargetoReposDistance,
      blastSpeed,
      vlsfoConsBlast,
      lsmgoConsBlast,
      vlsfofuelmultiplier,
      lsmgofuelmultiplier,
      discharge_port_eu_country,
      repos_port_eu_country
    );
    
    totalBunkerCons =
      blastToloadConsValues.totalBunkerConsumption +
      loadtoDischargeconsValues.totalBunkerConsumption +
      dischargetoReposConsValues.totalBunkerConsumption;
    totalco2 =
      blastToloadConsValues.totalco2emmited +
      loadtoDischargeconsValues.totalco2emmited +
      dischargetoReposConsValues.totalco2emmited;
    totalEmmission_mt =
      blastToloadConsValues.totalco2emmited_mt +
      loadtoDischargeconsValues.totalco2emmited_mt +
      dischargetoReposConsValues.totalco2emmited_mt;
  
      
    totalDistance =
      blasttoLoadDistance + loadtoDischargeDistance + dischargetoReposDistance;
    transportWork = parseFloat(totalDistance) * parseFloat(dwt);
  
    totalAttaindCii = (totalco2 / transportWork).toFixed(2);
    // ciiBand = CIIbandCalculator(totalAttaindCii);
  
    totaleuts =
      blastToloadConsValues.totaleuts +
      loadtoDischargeconsValues.totaleuts +
      dischargetoReposConsValues.totaleuts;
    
    return {
      totalBunkerCons,
      totalco2:(totalco2/1000000),
      totaleuts,
      totalEmmission_mt,
      totalDistance,
      transportWork,
      totalAttaindCii,
      eeoi: totalAttaindCii,
    };
  }


  const portToPortConsumption = (
    portToPortDistance = 0,
    Speed = 0,
    vlsfoConsBlast = 0,
    lsmgoConsBlast = 0,
    vlsfomultiplier = 1,
    lsmgomultiplier = 1,
    prevEu_country = false,
    currentEu_country = false
  ) => {
    let seadays =
      portToPortDistance && Speed ? portToPortDistance / (Speed * 24) : 0;
    let vlsfocons = seadays * vlsfoConsBlast;
    let lsmgoCons = seadays * lsmgoConsBlast;
    let totalBunkerConsumption = vlsfocons + lsmgoCons;
    let totalco2emmited_mt =
      vlsfocons * vlsfomultiplier + lsmgoCons * lsmgomultiplier;
    let totalco2emmited = totalco2emmited_mt * 1000000;
    let totaleuts = 0;
  
    if (prevEu_country && currentEu_country) {
     
      totaleuts = totalco2emmited_mt * 1 * 0.40;
    } else {
      totaleuts = totalco2emmited_mt * 0.5 * 0.40;
    }
    
    return {
      totalBunkerConsumption,
      totalco2emmited,
      totalco2emmited_mt,
      totaleuts,
    };
  };