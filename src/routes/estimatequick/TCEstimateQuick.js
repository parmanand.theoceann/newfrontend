import React, { useState, useRef, useEffect, useReducer } from "react";
import {
  Row,
  Col,
  Input,
  Select,
  Layout,
  Typography,
  Divider,
  Button,
  Collapse,
  Skeleton,
} from "antd";
import { v4 as uuidv4 } from "uuid";
import { useLocation } from "react-router-dom";
import dayjs from "dayjs";
import URL_WITH_VERSION, {
  openNotificationWithIcon,
  postAPICall,
  getAPICall,
  apiDeleteCall,
} from "../../shared";
import {
  Co2Calculation,
  nextdate,
  tcEstimateCalculation,
} from "./helperfunction";
import TCIDetailQuick from "./tcestimatecomponent/TcDetailQuick";
import TCODetail from "./tcestimatecomponent/Tco";
import Voyageresult from "./components/Voyageresult";
import Co2view from "./components/Co2view";
import TopHeader from "./TopHeader";
import { initform, estimateReducer } from "./reducers/tcEstimateReducer";
import { europeanCountryCodes } from "../../constants/countrycode";
import "./estimatequick.css";
import {
  QuickEstimateProvider,
  useQuickEstimateContext,
} from "./quickEstimateContext";
import { localvesseldataApi } from "./api";
import { TcCoCo2Calculation } from "./tcHelper";

const { Title } = Typography;
const { Content } = Layout;
const { Option } = Select;

const initPortModal = {
  openBalastModal: false,
  openBalastModalForm: false,
  openDeliveryModal: false,
  openDeliveryModalForm: false,
  openRedeliveryModal: false,
  openRedeliveryModalForm: false,
  openReposModal: false,
  openReposModalForm: false,
  openLocalVesselModal: false,
  openGlobalVesselModal: false,
  openVesselModalForm: false,
};

const initformitems = [{ ...initform, new_id: uuidv4() }];

const TCEstimateForm = ({
  addnewform,
  deleteform,
  totalform,
  formdata,
  addcopy,
  formitems,
}) => {
  const [state, dispatch] = useReducer(estimateReducer, formdata);
  const fuel_cons_arrref = useRef(null);
  const location = useLocation();
  const [openModal, setOpenModal] = useState(initPortModal);
  const [loading, setLoading] = useState(false);
  let blastTodelivery_ref = useRef(0);
  let deliveryToredelivery_ref = useRef(0);
  let redeliveryTorepos_ref = useRef(0);
  const [localvesseldata, setLocalVesselData] = useState(null);
  const {
    setLadenSpeedDropDown,
    setSelectedLadenSpeed,
    setBallastSpeedDropDown,
    setSelectedBallastSpeed,
    selectedLadenSpeed,
    selectedBallastSpeed,
  } = useQuickEstimateContext();
  const [isDataPosted, setIsDataPosted] = useState(false);
  const handleOpenModal = (key, value) => {
    setOpenModal({
      ...openModal,
      [key]: value,
    });
  };

  /*useEffect(() => {
    console.log('location :',location);
    const id = location.state && location.state.data;
    const vessel_id = location.state && location.state.data.vessel_id;
    const copyVesselId = state?.tci_details?.vessel_id;
    if (
      location.pathname === `/edit-tc-estimate/${id}` &&
      formitems.length <= 1
    ) {
      editTcestimateQuick(id);
      getDataForSelectedVessel(vessel_id);
      setIsDataPosted(false);
    } else if (copyVesselId) {
      getDataForSelectedVessel(copyVesselId);
    } else {
      if (vessel_id) {
        getDataForSelectedVessel(vessel_id);
      }
      let totalvoyagedays =
        parseInt(state.tco_details.sea_days) +
        parseInt(state.tco_details.port_days);
      let commence_date = state.tco_details.commence_date;
      let completed_date = nextdate(commence_date, totalvoyagedays);

      dispatch({ type: "total_voyage_days", payload: totalvoyagedays });
      dispatch({
        type: "completed_date",
        payload: dayjs(completed_date).format("YYYY-MM-DD HH:mm:ss"),
      });
    }
    getLocalVesselData();
  }, [location]);
  */

  useEffect(() => {
    (async () => {
      //setLoading(true)
      const { data = {}, fullEstimate } = location?.state || {};
      const id = fullEstimate ? data.quick_estimate_id : data.estimate_id;
      const vessel_id = location?.state?.data?.vessel_id;
      const quickid = data?.quick_id;

      if (location.pathname === `/edit-tc-estimate/${id}` && !fullEstimate) {
        await editTcestimateQuick(id);
        await getDataForSelectedVessel(vessel_id);
      } else {
        if (id) {
          await getDataForSelectedVessel(vessel_id);
        }

        let portdays = data?.totalitinerarysummary?.totalt_port_days
          ? parseFloat(data?.totalitinerarysummary?.totalt_port_days)
          : 4;
        let seadays = data?.totalitinerarysummary?.total_tsd
          ? parseFloat(data?.totalitinerarysummary?.total_tsd)
          : 0;

        let totalvoyagedays = portdays + seadays;
        let commence_date = dayjs(data?.commence_date);
        let completed_date = nextdate(commence_date, totalvoyagedays);

        let tcidetails = {
          //vessel_name: data?.vessel_id?.toString(),
          vessel_name: data?.vessel_name,
          dwt: data?.dwt,
          // laden_spd: "0.00",
          // ballast_spd: "0.00",
          tci_daily_cost: data?.tci_d_hire,
          tci_add_comm: data?.add_percentage_tci,
          tci_bro_comm: data?.bro_percentage_tci,
          blast_bonus: data?.bb,
          other_cost: data?.mis_cost,
          // vlsfo_fuel: 5,
          // lsmgo_fuel: 7,
          // vlsfo_cons_b: "0.00",
          // lsmgo_cons_b: "0.00",
          // lsmgo_cons_l: "0.00",
          // vlsfo_cons_l: "0.00",
          // bunker_oil_price: "0.00",
          // bunker_gas_price: "0.00",
          ballast_port: data?.portitinerary?.[0]?.["port"] ?? "",
          delivery_port: data?.portitinerary?.[1]?.["port"] ?? "",
          redelivery_port: data?.portitinerary?.[2]?.["port"] ?? "",
          repos_port: data?.portitinerary?.[3]?.["port"] ?? "",
        };

        let tcodetails = {
          tco_duration: data?.tcoterm?.[0]?.duration || 0,
          tco_d_hire: data?.tcoterm?.[0]?.dailyhire || 0,
          tco_add_per: data?.tcoterm?.[0]?.acom || 0,
          tco_bro_per: data?.tcoterm?.[0]?.bcom || 0,
          tco_bb: data?.tcoterm?.[0]?.bb || 0,
          mis_revenue: (
            parseFloat(data?.tcoterm?.[0]?.dailyhire || 0) *
            parseFloat(data?.tcoterm?.[0]?.duration || 0)
          ).toFixed(2),
          port_days: portdays || 0,
          sea_days: seadays || 0,
          commence_date,
        };

        let _blastformdata = {
          port_name: data?.portitinerary?.[0]?.port,
          ld_rate_per_day: data?.portitinerary?.[0]?.l_d_rate,
          ld_rate_per_hour: data?.portitinerary?.[0]?.l_d_rate1,
          ld_qty_unit: data?.portitinerary?.[0]?.l_d_qty,
          term: data?.portitinerary?.[0]?.l_d_term,
          port_days: data?.portitinerary?.[0]?.days,
          port_func: data?.portitinerary?.[0]?.funct,
          turn_time: data?.portitinerary?.[0]?.turn_time,
          xpd: data?.portitinerary?.[0]?.xpd,
          dem_final_amount:
            data?.portitinerary?.[0]?.dem_disp == "Demmurage"
              ? data?.portitinerary?.[0]["dem_disp_amt"]
              : 0,
          des_final_amount:
            data?.portitinerary?.[0]?.dem_disp == "Despatch"
              ? data?.portitinerary?.[0]["dem_disp_amt"]
              : 0,
        };

        let _deliveryformdata = {
          port_name: data?.portitinerary?.[1]?.port,
          ld_rate_per_day: data?.portitinerary?.[1]?.l_d_rate,
          ld_rate_per_hour: data?.portitinerary?.[1]?.l_d_rate1,
          ld_qty_unit: data?.portitinerary?.[1]?.l_d_qty,
          term: data?.portitinerary?.[1]?.l_d_term,
          port_days: data?.portitinerary?.[1]?.days,
          port_func: data?.portitinerary?.[1]?.funct,
          turn_time: data?.portitinerary?.[1]?.turn_time,
          xpd: data?.portitinerary?.[1]?.xpd,
          dem_final_amount:
            data?.portitinerary?.[1]?.dem_disp == "Demmurage"
              ? data?.portitinerary?.[1]["dem_disp_amt"]
              : 0,
          des_final_amount:
            data?.portitinerary?.[1]?.dem_disp == "Despatch"
              ? data?.portitinerary?.[1]["dem_disp_amt"]
              : 0,
        };

        let _redeliveryformdata = {
          port_name: data?.portitinerary?.[2]?.port,
          ld_rate_per_day: data?.portitinerary?.[2]?.l_d_rate,
          ld_rate_per_hour: data?.portitinerary?.[2]?.l_d_rate1,
          ld_qty_unit: data?.portitinerary?.[2]?.l_d_qty,
          term: data?.portitinerary?.[2]?.l_d_term,
          port_days: data?.portitinerary?.[2]?.days,
          port_func: data?.portitinerary?.[2]?.funct,
          turn_time: data?.portitinerary?.[2]?.turn_time,
          xpd: data?.portitinerary?.[2]?.xpd,
          dem_final_amount:
            data?.portitinerary?.[2]?.dem_disp == "Demmurage"
              ? data?.portitinerary?.[2]["dem_disp_amt"]
              : 0,
          des_final_amount:
            data?.portitinerary?.[2]?.dem_disp == "Despatch"
              ? data?.portitinerary?.[2]["dem_disp_amt"]
              : 0,
        };

        let _reposformdata = {
          port_name: data?.portitinerary?.[3]?.port,
          ld_rate_per_day: data?.portitinerary?.[3]?.l_d_rate,
          ld_rate_per_hour: data?.portitinerary?.[3]?.l_d_rate1,
          ld_qty_unit: data?.portitinerary?.[3]?.l_d_qty,
          term: data?.portitinerary?.[3]?.l_d_term,
          port_days: data?.portitinerary?.[3]?.days,
          port_func: data?.portitinerary?.[3]?.funct,
          turn_time: data?.portitinerary?.[3]?.turn_time,
          xpd: data?.portitinerary?.[3]?.xpd,
          dem_final_amount:
            data?.portitinerary?.[3]?.dem_disp == "Demmurage"
              ? data?.portitinerary?.[3]["dem_disp_amt"]
              : 0,
          des_final_amount:
            data?.portitinerary?.[3]?.dem_disp == "Despatch"
              ? data?.portitinerary?.[3]["dem_disp_amt"]
              : 0,
        };

        let _blastTodeliveryDistance = data?.portitinerary?.[1]?.["miles"] ?? 0;
        let _deliveryToredeliveryDistance =
          data?.portitinerary?.[2]?.["miles"] ?? 0;
        let _redeliveryToreposDistance =
          data?.portitinerary?.[3]?.["miles"] ?? 0;

        let ballast_port_eu_country =
          data?.portitinerary?.[0]?.is_eur === "true" ? true : false;
        let load_port_eu_country =
          data?.portitinerary?.[1]?.is_eur === "true" ? true : false;
        let redelivery_port_eu_country =
          data?.portitinerary?.[2]?.is_eur === "true" ? true : false;
        let repos_port_eu_country =
          data?.portitinerary?.[3]?.is_eur === "true" ? true : false;

        dispatch({
          type: "updatefull",
          payload: {
            tcidetails,
            tcodetails,
            _blastformdata,
            _deliveryformdata,
            _redeliveryformdata,
            _reposformdata,
            _blastTodeliveryDistance,
            _deliveryToredeliveryDistance,
            _redeliveryToreposDistance,
            ballast_port_eu_country,
            load_port_eu_country,
            redelivery_port_eu_country,
            repos_port_eu_country,
          },
        });

        dispatch({
          type: "total_voyage_days",
          payload: totalvoyagedays.toFixed(2),
        });

        dispatch({
          type: "completed_date",
          payload: dayjs(completed_date).format("YYYY-MM-DD HH:mm:ss"),
        });
      }
      await getLocalVesselData();
      //setLoading(false)
    })();
  }, [location]);

  // useEffect(() => {
  //   if (localvesseldata && localvesseldata?.length > 0) {
  //     const calculationdata = tcEstimateCalculation(state);
  //     const co2calculation = TcCoCo2Calculation(state);

  //     dispatch({
  //       type: "updatecalculation",
  //       payload: { calculationdata, co2calculation },
  //     });
  //   }
  // }, [localvesseldata]);

  const getLocalVesselData = async () => {
    const data = await localvesseldataApi();
    setLocalVesselData(data);
  };

  const modalCloseEvent = (key, data) => {
    let eu_country = europeanCountryCodes.includes(data.port.country_code)
      ? true
      : false;
    if (data.distance) {
      let totaldistance = Number(
        data?.distance?.features[0]?.properties?.total_length
      );

      let secaLength = Number(
        data?.distance?.features[0]?.properties?.seca_length
      );
      let crossed = data?.distance?.features[0]?.properties?.crossed[0];

      switch (key) {
        case "delivery_port":
          blastTodelivery_ref.current = totaldistance;
          dispatch({
            type: "blastTodeliveryDistance",
            payload: { totaldistance, secaLength, crossed },
          });
          break;
        case "redelivery_port":
          deliveryToredelivery_ref.current = totaldistance;
          dispatch({
            type: "deliveryToredeliveryDistance",
            payload: { totaldistance, secaLength, crossed },
          });
          break;
        case "repos_port":
          redeliveryTorepos_ref.current = totaldistance;
          dispatch({
            type: "redeliveryToreposDistance",
            payload: { totaldistance, secaLength, crossed },
          });

          break;
        default:
          return null;
      }
      dispatch({
        type: "eu_country",
        payload: { [key + "_" + "eu_country"]: eu_country },
      });
    }
    updateform({ [key]: data.port.port_name });
  };

  const updateform = (data) => {
    dispatch({
      type: Object.keys(data)[0],
      payload: {
        port_name: Object.values(data)[0],
        portdistance: {
          blastTodelivery_ref,
          deliveryToredelivery_ref,
          redeliveryTorepos_ref,
        },
      },
    });
  };

  const updateQuickFromdatabase = (type, data, fulldata) => {
    fuel_cons_arrref.current = fulldata;
    dispatch({ type: "vessel_name", payload: data });
    dispatch({ type: "cons_fuel", payload: fulldata });
    if (type == "local") {
      handleOpenModal("openLocalVesselModal", false);
    } else {
      handleOpenModal("openGlobalVesselModal", false);
    }
  };

  const handleChange = (group, name, value) => {
    if (name == "bunker_gas_price" || name == "bunker_oil_price") {
      dispatch({
        type: name,
        payload: {
          value: value,
          portdistance: {
            blastTodelivery_ref,
            deliveryToredelivery_ref,
            redeliveryTorepos_ref,
          },
        },
      });
    } else {
      dispatch({
        type: name,
        payload: typeof value === "string" ? value : parseInt(value),
      });
    }
  };

  const handleselect = (
    value,
    name,
    flag,
    index,
    laden = false,
    ballast = false
  ) => {
    let multiplier = 0;
    if (name === "vlsfo_fuel" || name === "lsmgo_fuel") {
      switch (value) {
        case 5:
          multiplier = 3.15;
          break;
        case 10:
          multiplier = 3.15;
          break;
        case 11:
          multiplier = 3.114;
          break;
        case 7:
          multiplier = 3.2;
          break;
        case 4:
          multiplier = 3.2;
      }
      if (flag && laden) {
        if (name === "vlsfo_fuel") {
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden: index,
              indexBallast: selectedBallastSpeed.key,
            },
          });
        } else {
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden: index,
              indexBallast: selectedBallastSpeed.key,
            },
          });
        }
        return;
      } else if (flag && ballast) {
        if (name === "vlsfo_fuel") {
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden: selectedLadenSpeed.key,
              indexBallast: index,
            },
          });
        } else {
          dispatch({
            type: name,
            payload: {
              fuelarr: fuel_cons_arrref.current,
              value: value,
              multiplier: multiplier,
              indexLaden: selectedLadenSpeed.key,
              indexBallast: index,
            },
          });
        }
        return;
      }

      dispatch({
        type: name,
        payload: {
          fuelarr: fuel_cons_arrref.current,
          value: value,
          multiplier: multiplier,
          indexLaden: selectedLadenSpeed.key,
          indexBallast: selectedBallastSpeed.key,
        },
      });
      return;
    }
    dispatch({ type: name, payload: value });
  };

  const handleselectTCO = (value, name) => {
    dispatch({ type: name, payload: value });
  };

  const updatequickfrom = (group, data) => {
    dispatch({ type: group, payload: data });
  };

  const saveFormData = () => {
    if (!state.tci_details.vessel_id) {
      openNotificationWithIcon("error", "select the vessel first");
      return;
    }
    let vData = state;
    delete vData.co2_calc_value;
    // delete vData.blastTodeliveryDistance;
    // delete vData.deliveryToredeliveryDistance;
    // delete vData.redeliveryToreposDistance;

    delete vData.blastPortExpenses;
    delete vData.deliveryPortExpenses;
    delete vData.redeliveryPortExpenses;
    delete vData.reposPortExpenses;

    // setState(prevState => ({...prevState,  loadFrm: false}))
    let type = "save";
    let suMethod = "POST";
    if (vData["id"] || vData["estimate_id"]) {
      type = "update";
      suMethod = "PUT";
    }
    setLoading(true);
    delete vData["new_id"];
    let suURL = `${URL_WITH_VERSION}/tcto/quick-${type}`;

    // formdataref.current=vData;
    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        editTcestimateQuick(data.row.estimate_id);
        setLoading(false);
        setIsDataPosted(false);
      } else {
        openNotificationWithIcon("error", data.message);
        setLoading(false);
        // setState(prevState => ({...prevState, loadFrm: true,formData:formdataref.current}))
      }
    });
  };

  const getDataForSelectedVessel = async (vessel_id) => {
    setLoading(true);
    let ifo_blast = [],
      mgo_blast = [],
      lsmgo_blast = [],
      vlsfo_blast = [],
      ulsfo_blast = [],
      speed_blast = [];
    let ifo_laden = [],
      mgo_laden = [],
      lsmgo_laden = [],
      vlsfo_laden = [],
      ulsfo_laden = [],
      speed_laden = [];

    let updatedObj = {};
    let fuelData = {};
    try {
      const res = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/${vessel_id}`
      );
      const VesselData = await res.data;
      VesselData["seaspdconsp.tableperday"].map((el) => {
        let { ballast_laden, ifo, lsmgo, vlsfo, ulsfo, mgo, speed } = el;

        if (ballast_laden == 1) {
          ifo_blast.push(ifo);
          mgo_blast.push(mgo);
          lsmgo_blast.push(lsmgo);
          vlsfo_blast.push(vlsfo);
          ulsfo_blast.push(ulsfo);
          speed_blast.push(speed ? speed : "0.00");
        } else {
          ifo_laden.push(ifo);
          mgo_laden.push(mgo);
          lsmgo_laden.push(lsmgo);
          vlsfo_laden.push(vlsfo);
          ulsfo_laden.push(ulsfo);
          speed_laden.push(speed ? speed : "0.00");
        }

        setSelectedLadenSpeed({
          label: speed_laden[0],
          value: speed_laden[0],
          key: 0,
        });
        setSelectedBallastSpeed({
          label: speed_blast[0],
          value: speed_blast[0],
          key: 0,
        });
        // handleChange("tci_details", 'vessel_name', updatedObj)
        setLadenSpeedDropDown(
          speed_laden.map((e, i) => ({ label: e, value: e, key: i }))
        );
        setBallastSpeedDropDown(
          speed_blast.map((e, i) => ({ label: e, value: e, key: i }))
        );

        updatedObj["dwt"] = VesselData.vessel_dwt;
        updatedObj["vessel_code"] = VesselData.vessel_code;
        updatedObj["laden_spd"] = speed_laden[0];
        updatedObj["ballast_spd"] = speed_blast[0];
        updatedObj["vessel_id"] = VesselData.vessel_id;
        updatedObj["vessel_name"] = VesselData.vessel_name;

        fuelData = {
          ifo_blast,
          mgo_blast,
          lsmgo_blast,
          vlsfo_blast,
          ulsfo_blast,
          ifo_laden,
          mgo_laden,
          lsmgo_laden,
          vlsfo_laden,
          ulsfo_laden,
        };
      });

      updateQuickFromdatabase("local", updatedObj, fuelData);
      setLoading(false);
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 2);
      setLoading(false);
    }
  };

  const handleSelectVesselNameFromDropDown = (value, option) => {
    getDataForSelectedVessel(value);
  };

  const editTcestimateQuick = async (id) => {
    const response = await getAPICall(
      `${URL_WITH_VERSION}/tcto/quick-edit?ae=${id}`
    );
    const respdata = await response["data"];
    totalform(respdata);
    dispatch({ type: "updateestimate", payload: respdata });
    setLoading(false);
  };

  const items = [
    {
      key: "1",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          TCI Details
        </span>
      ),
      children: (
        <TCIDetailQuick
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
          openModal={openModal}
          updateQuickFromdatabase={updateQuickFromdatabase}
          modalCloseEvent={modalCloseEvent}
          updatequickfrom={updatequickfrom}
          localvesseldata={localvesseldata}
          handleSelectVesselNameFromDropDown={
            handleSelectVesselNameFromDropDown
          }
        />
      ),
    },

    {
      key: "2",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          TCO
        </span>
      ),
      children: (
        <TCODetail
          handleChange={handleChange}
          handleselect={handleselectTCO}
          formdata={state}
          handleOpenModal={handleOpenModal}
        />
      ),
    },

    {
      key: "3",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          Voyage Results
        </span>
      ),
      children: (
        <Voyageresult
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
        />
      ),
    },

    {
      key: "4",
      label: (
        <span style={{ fontSize: "14px", fontWeight: "500", color: "white" }}>
          {" "}
          CII Dynamics
        </span>
      ),
      children: (
        <Co2view
          handleChange={handleChange}
          handleselect={handleselect}
          formdata={state}
          handleOpenModal={handleOpenModal}
        />
      ),
    },
  ];

  const handleadd = () => {
    if (isDataPosted) {
      return;
    }
    addnewform();
  };

  const handlecopy = () => {
    // dispatch({ type: "copy", payload: state });

    addcopy(state);
  };

  const handledelete = () => {
    deleteform(state.tci_details.vessel_id);
  };

  const handleReset = () => {
    if (state.id || state.estimate_id) {
      return;
    }

    setSelectedLadenSpeed({ label: "0.00", value: 0 });
    setSelectedBallastSpeed({ label: "0.00", value: 0 });
    setLadenSpeedDropDown(null);
    setBallastSpeedDropDown(null);

    dispatch({ type: "reset", payload: initform });
  };

  return (
    <>
      <TopHeader
        saveformdata={saveFormData}
        handleadd={handleadd}
        handlecopy={handlecopy}
        handledelete={handledelete}
        handlereset={handleReset}
        quickestimateid={state.estimate_id}
        formdata={state}
        extraMenu={[
          {
            label: "Go to Voyage Estimate",
            key: "voyage_estimate",
          },
          {
            label: "Go to Voy relet",
            key: "voy_relet",
          },
        ]}
      />
      <Skeleton loading={loading} active>
        <Row>
          <Col className="bold" span={10} style={{ paddingLeft: "17px" }}>
            TC Estimate ID :
          </Col>
          <Col
            className=""
            span={14}
            style={{ paddingRight: 2, paddingBottom: 5 }}
          >
            <Input
              type="text"
              disabled
              name="estimate_id"
              value={state.estimate_id}
            />
          </Col>
        </Row>
        <Collapse
          items={items}
          defaultActiveKey={"1"}
          style={{ backgroundColor: "#12406a" }}
          expandIconPosition="end"
        />
      </Skeleton>
      <Divider />
      <Row>
        <Button type="primary" onClick={saveFormData}>
          Submit
        </Button>
        <Button onClick={handleReset} type="secondary">
          Reset
        </Button>
      </Row>
    </>
  );
};

const TCEstimateQuick = () => {
  const [formitems, setFormitems] = useState(initformitems);

  const handleadd = () => {
    setFormitems([...formitems, { ...initform, new_id: uuidv4() }]);
  };

  const handledelete = (id, index) => {
    let URL = `${URL_WITH_VERSION}/tcto/quick-delete`;
    if (id) {
      apiDeleteCall(URL, { id: id }, (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          remainingformitem(index);
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      });
    } else {
      remainingformitem(index);
    }
  };

  const remainingformitem = (index) => {
    let allformitems = [...formitems];
    if (allformitems.length > 1) {
      allformitems.splice(index, 1);
      setFormitems([...allformitems]);
    } else {
      return;
    }
  };

  const totalForm = (data, index) => {
    let allformitems = [...formitems];
    allformitems[index] = { ...data };
    setFormitems([...allformitems]);
  };

  const handleaddcopy = (data, index) => {
    setFormitems([
      ...formitems,
      { ...data, estimate_id: "", id: null, new_id: uuidv4() },
    ]);
  };

  return (
    <div className="tcov-wrapper">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div
                      className="box-body common-fields-wrapper"
                      style={{
                        maxWidth: "fit-content",
                        minWidth: "90vw",
                        overflowX: "scroll",
                        display: "flex",
                      }}
                    >
                      {formitems.map((el, index) => (
                        <div
                          style={{
                            minWidth: "360px",
                            padding: "10px 0px",
                            borderLeft: "1px solid black",
                            marginRight: "10px",
                          }}
                          key={el?.new_id}
                        >
                          <QuickEstimateProvider>
                            <TCEstimateForm
                              addnewform={handleadd}
                              deleteform={(id) => handledelete(id, index)}
                              totalform={(data) =>
                                totalForm(
                                  { ...data, new_id: el?.new_id },
                                  index
                                )
                              }
                              addcopy={(data) =>
                                handleaddcopy(
                                  { ...data, new_id: el.new_id },
                                  index
                                )
                              }
                              formdata={el}
                              formitems={formitems}
                            />
                          </QuickEstimateProvider>
                        </div>
                      ))}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default TCEstimateQuick;
