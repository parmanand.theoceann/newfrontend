import React, { Component, useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Pagination,
  Progress,
  Modal,
  Menu,
  Dropdown,
  DatePicker,
} from "antd";
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, { getAPICall, ResizeableTitle } from "../../shared";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import { useStateCallback } from "../../shared";
import ShipDetail from "../dynamic-vspm/ShipDetail";
import BarChart from "./RoundBarChart";
import Piechart from "./PieChart";
import VoyageHistoryMap from "./VoyageHistoryMap";
import GaugeChart from "./GaugeChart";
// import { isDate } from 'moment';
const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker, dateRanges } = DatePicker;

const PerformanceDashboard = () => {
  const [state, setState] = useStateCallback({
    data: [],
    chartdata: {
      total_vessels: 100,
      total_bunker_consumption: 100,
      average_sea_consumptions: 100,
      average_port_consumptions: 100,
      sea_cons_heading: "Sea Consumption Graph",
      port_cons_heading: "Port Consumption Graph",
      av_sea_cons_heading: "Avg. At Sea Consumption(Mt)",
      av_port_cons_heading: "Avg. At Port Consumption(Mt)",
      av_sea_emmission_heading: "Avg. At Sea Emissions (Co2)",
      av_port_emmission_heading: "Avg. in Port Emissions (Co2)",
      fuel_sea_cons: [1200, 300, 150, 80, 70],
      fuel_port_cons: [120, 300, 150, 80, 70],
    },
    filterData: {
      commence_date: "",
      completed_date: "",
      fuel_type: [],
    },
  });

  useEffect(() => {
    getallVessel();
  }, []);

  const getallVessel = () => {};

  const {
    vesselValue,
    tradeAreasValue,
    tradeUserValue,
    VoyOpsValue,
    mapSettings,
    shipData,
    chattering,
    vesselName,
    vesselSelectValue,
    chartData,
  } = state;
  const style = {
    // position: 'absolute',
    height: "91%",
    width: "96%",
  };

  const onChange = (value, dateString) => {
    setState((prevState) => ({
      ...prevState,
      filterData: {
        ...state.filterData,
        commence_date: dateString[0],
        completed_date: dateString[1],
      },
    }));
  };

  const handlefilter = () => {
  //console.log('filter')
  };

  const handleselect = (value) => {
    let aa = value.join("@");
    aa = aa.trim().split("@");
    setState((prevState) => ({ ...prevState, fuel_type: [...aa] }));
  };

  return (


    
    <div className="body-wrapper modalWrapper">
      {/*  filter types */}
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row p10">
              <div className="col-12">
                <h4 className="Char-dashboard">Filters</h4>

                <div className="card-group p-0 border-0 rounded-0">
                  <div className="card p-0 border-0 rounded-0">
                    <div className="card-body border-0 rounded-0 pl-0">
                      <RangePicker
                        placeholder={["Commence Date", "Completed Date"]}
                        onChange={onChange}
                      />
                    </div>
                  </div>
                  <div className="card p-0 border-0 rounded-0">
                    <div className="card-body border-0 rounded-0 pl-0">
                      {/* <Dropdown trigger={["click"]}>
                        <a className="ant-dropdown-link">
                          <Input size="default" placeholder={vesselName} />
                        </a>
                      </Dropdown> */}

                      <Input size="default" placeholder="vessel Name" />
                    </div>
                  </div>
                  <div className="card p-0 border-0 rounded-0">
                    <div className="card-body border-0 rounded-0">
                      {/* <Dropdown trigger={["click"]}>
                        <a className="ant-dropdown-link">
                          <Input size="default" placeholder={vesselValue} />
                        </a>
                      </Dropdown> */}

                      <Input size="default" placeholder="Voyage No" />
                    </div>
                  </div>

                  <div className="card border-0 rounded-0 p-0">
                    <div className="card-body border-0 rounded-0">
                      {/* <Dropdown trigger={["click"]}>
                        <a className="ant-dropdown-link">
                          <Input size="default" placeholder={tradeUserValue} />
                        </a>
                      </Dropdown> */}

                      <Select
                        mode="multiple"
                        allowClear
                        style={{
                          width: "100%",
                        }}
                        placeholder="Please select"
                        // defaultValue={["a10", "c12"]}
                        onChange={handleselect}
                        options={[
                          {
                            value: 3,
                            label: "IFO",
                          },
                          {
                            group: 3,
                            value: 4,
                            label: "MGO",
                          },
                          {
                            value: 5,
                            label: "VLSFO",
                          },
                          {
                            value: 7,
                            label: "LSMGO",
                          },
                          {
                            value: 10,
                            label: "ULSFO",
                          },
                        ]}
                      />
                    </div>
                  </div>
                </div>

                
              </div>
            </div>
          </div>
        </div>
      </article>

      <button onClick={handlefilter}>filter</button>
      {/*  summary data */}
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row p12">
              <div className="col-12">
                <div className="card-group w-100 mt-4">
                  <div className="card">
                    <div className="card-body dashboard-col-details">
                      <h4 className="Char_dashboard_sub">Total Vessels</h4>
                      <h2 className="font-weight-bold mb-0">
                        {state?.chartdata?.total_vessels ?? "0"}
                      </h2>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body dashboard-col-details">
                      <h4 className="Char_dashboard_sub">
                        Total Bunker Consumption
                      </h4>
                      <h2 className="font-weight-bold mb-0">
                        {state?.chartdata?.total_bunker_consumption ?? ""}
                      </h2>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body dashboard-col-details">
                      <h4 className="Char_dashboard_sub">
                        Average Sea Consumption
                      </h4>
                      <h2 className="font-weight-bold mb-0">
                        {state?.chartdata?.average_sea_consumptions ?? ""}
                      </h2>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body dashboard-col-details">
                      <h4 className="Char_dashboard_sub">
                        Average Port Consumption
                      </h4>
                      <h2 className="font-weight-bold mb-0">
                        {state?.chartdata?.average_port_consumptions ?? ""}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

      {/*  sea consumption and port cons graph */}

      {/*  barchart fuel ordering  ["IFO", "LSMGO", "VLSFO", "ULSFO", "MGO"], */}

      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row">
              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <BarChart
                      Heading={state?.chartdata?.sea_cons_heading}
                      fuelData={state.chartdata.fuel_sea_cons}
                      maxValue={Math.max(...state.chartdata.fuel_sea_cons)}
                    />
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <BarChart
                      Heading={state?.chartdata?.port_cons_heading}
                      fuelData={state.chartdata.fuel_port_cons}
                      maxValue={Math.max(...state.chartdata.fuel_port_cons)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

      {/*    port emision and sea emision gauge chart */}

      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row">
              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <GaugeChart
                      Heading={state?.chartdata?.sea_cons_heading}
                      name="Sea Cons"
                      value={"13000"}
                    />
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <GaugeChart
                      Heading={state?.chartdata?.port_cons_heading}
                      name="Port Cons"
                      value={"13000"}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

      {/* <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row">
              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <GaugeChart
                      Heading={state?.chartdata?.av_sea_emmission_heading}
                    />
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <GaugeChart
                      Heading={state?.chartdata?.av_port_emmission_heading}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article> */}

      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row">
              <div className="col-md-12">
                <div className="border">
                  <h4 className="mb-0  p-4 Char-dashboard">
                    Live vessel (Commenced/Delivered)
                  </h4>

                  {/* {state.data.length > 0 ? (
                      <ChartringMap mapData={state.data} />
                    ) : null} */}

                  <VoyageHistoryMap />
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="row">
              <div className="col-md-6">
                <div className="border p-3">
                  <h4 className="mb-3 Char-dashboard">
                    Profit/Loss vs Revenue (Voyage)
                  </h4>
                  <Piechart />
                </div>
              </div>

              <div className="col-md-6">
                <div className="border p-3">
                  <h4 className="mb-3 Char-dashboard">
                    Profit/Loss vs Revenue (Voyage)
                  </h4>
                  <BarChart chartData={chattering} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>
    </div>





  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyCIZJxl4b3B520rAjPUqIu_YD5FHfiFQ6M",
})(PerformanceDashboard);
