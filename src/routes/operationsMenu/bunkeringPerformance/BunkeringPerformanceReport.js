import React from "react";
import moment from "moment";

import { Table, Input, Icon } from "antd";
import BarChart from "../operationchart";
class BunkeringPerformanceReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Vessel Name",
          //   dataIndex: "port",
          align: "left",
          //   render:(el)=><span className="spot-first" >{el}</span>
        },
        {
          title: "Voyage No.",
          //   dataIndex: "region",
          align: "left",
        },
        {
          title: "bunkering Port",
          //   dataIndex: "grade",
          align: "left",
        },

        {
          title: "Function",
          //   dataIndex: "country",
          align: "left",
        },
        {
          title: "DWT",
          //   dataIndex: "currentPrice",
          align: "left",
        },
        {
          title: "Vessel Type",
          //   dataIndex: "lastUpdatedOnUTC",
          //   render: (date) => date && moment(date).format("LLL"),
          align: "left",
        },
        {
          title: "Bunker Requirement ID",
          //   dataIndex: "isHolidayInPort",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Fuel Type",
          //   dataIndex: "fuelAvailibility",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "BDN Qty",
          //   dataIndex: "netChange",
        },
        {
          title: "Received Qty",
          //   dataIndex: "netChange",
        },
        {
          title: "Net Amount($)",
          //   dataIndex: "netChange",
        },

        {
          title: "Total Invoice Amount",
          //   dataIndex: "netChange",
        },
      ],
    };
  }

  render() {
    const { columns } = this.state;

    return (
      <>
        {/* <div style={{ padding: "20px" }}>
          <div style={{ marginBottom: "20px" }}>
            <Input
              placeholder="Search"
              style={{ width: "300px", margin: "0 auto" }}
              onChange={this.onsearchfilter}
            />
          </div>

          <Table
            className="spot voyage-operation"
            columns={columns}
            //   dataSource={portdata}
            size="medium"
            //   pagination={{
            //     pageSize: 50,
            //     showSizeChanger: true,
            //     total: portdata.length,
            //   }}
          />
        </div> */}

        <div class="shadow-lg rounded-lg overflow-hidden">
          <div>
            <iframe
              title="Bunker Performance Matrix"
              style={{ width: "100%", height: "110vh" }}
              src="https://app.powerbi.com/view?r=eyJrIjoiYWI4OWMwMTgtYzcxNC00ZmE4LTlmMWItODJhZDAzMjM4ZWZmIiwidCI6IjA0YWVkYTA0LWExYTctNDZiOC1hNTgyLTJhOTVjY2Q4ZmUzMiJ9"
              frameborder="0"
              allowFullScreen={true}
            />
          </div>
        </div>




      </>
    );
  }
}

export default BunkeringPerformanceReport;
