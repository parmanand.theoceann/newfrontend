import React from "react";
import moment from "moment";

import { Table, Input, Icon } from "antd";
import BarChart from "../operationchart";
class BunkeringPerformanceReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Participant Name",
          //   dataIndex: "port",
          align: "left",
          //   render:(el)=><span className="spot-first" >{el}</span>
        },
        {
          title: "Vessel Name",
          //   dataIndex: "region",
          align: "left",
        },
        {
          title: "Point",
          //   dataIndex: "grade",
          align: "left",
        },

        {
          title: "Pool Entry Date",
          //   dataIndex: "country",
          align: "left",
        },
        {
          title: "Pool Exit Date",
          //   dataIndex: "currentPrice",
          align: "left",
        },
        {
          title: "Effective Start Date",
          //   dataIndex: "lastUpdatedOnUTC",
          //   render: (date) => date && moment(date).format("LLL"),
          align: "left",
        },
        {
          title: "Effective End Date",
          //   dataIndex: "isHolidayInPort",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Income Before Pooling",
          //   dataIndex: "fuelAvailibility",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Revenue After Pooling",
          //   dataIndex: "netChange",
        },
        {
          title: "Miss Revenue",
          //   dataIndex: "netChange",
        },
        {
          title: "Mng fee",
          //   dataIndex: "netChange",
        },

        
      ],
    };
  }



  render() {
    const { columns } = this.state;

    return (
      <>
        
        <div style={{ padding: "20px"}}>
          <div style={{ marginBottom: "20px" }}>
            <Input
              placeholder="Search"
              style={{ width: "300px", margin: "0 auto"}}
              onChange={this.onsearchfilter}
            />
          </div>

          <Table
            className="spot voyage-operation"
            columns={columns}
            //   dataSource={portdata}
            size="medium"
            //   pagination={{
            //     pageSize: 50,
            //     showSizeChanger: true,
            //     total: portdata.length,
            //   }}
          />
        </div>
        
        
        <div class="shadow-lg rounded-lg overflow-hidden">
  <div class="py-3 px-5 bg-gray-50"></div>
<div >
  <BarChart/>
  </div></div>


      </>
    );
  }
}

export default BunkeringPerformanceReport;
