import React from "react";
import moment from "moment";

 import { Table, Input, Icon } from "antd";
 import BarChart from "../operationchart";
class CargoPerformance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Voyage No.",
          //   dataIndex: "port",
          align: "left",
          //   render:(el)=><span className="spot-first" >{el}</span>
        },
        {
          title: "Cargo Name",
          //   dataIndex: "region",
          align: "left",
        },
        {
          title: "Cargo Qty",
          //   dataIndex: "grade",
          align: "left",
        },

        {
          title: "Load Port",
          //   dataIndex: "country",
          align: "left",
        },
        {
          title: "Discharge Port",
          //   dataIndex: "currentPrice",
          align: "left",
        },
        {
          title: "World Scale Rate",
          //   dataIndex: "lastUpdatedOnUTC",
          //   render: (date) => date && moment(date).format("LLL"),
          align: "left",
        },
        {
          title: "Total Freight Amount",
          //   dataIndex: "isHolidayInPort",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "commence Date",
          //   dataIndex: "fuelAvailibility",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Completed Date",
          //   dataIndex: "netChange",
        },
        {
          title: "Payable",
          //   dataIndex: "netChange",
        },
        {
          title: "Receivable",
          //   dataIndex: "netChange",
        },

       
      ],
    };
  }



  render() {
    const { columns } = this.state;

    return (
      <>
        
        <div style={{ padding: "20px"}}>
          <div style={{ marginBottom: "20px" }}>
            <Input
              placeholder="Search"
              style={{ width: "300px", margin: "0 auto"}}
              onChange={this.onsearchfilter}
            />
          </div>

          <Table
            className="spot voyage-operation"
            columns={columns}
            //   dataSource={portdata}
            size="medium"
            //   pagination={{
            //     pageSize: 50,
            //     showSizeChanger: true,
            //     total: portdata.length,
            //   }}
          />
        </div>
        
        <div class="shadow-lg rounded-lg overflow-hidden">
  <div class="py-3 px-5 bg-gray-50"></div>
<div >
 <BarChart/> 
  </div></div>

      </>
    );
  }
}

export default CargoPerformance;
