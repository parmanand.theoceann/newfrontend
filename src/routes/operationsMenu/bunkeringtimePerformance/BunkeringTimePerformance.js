import React from "react";
import moment from "moment";

import { Table, Input, Icon } from "antd";
 import BarChart from "../operationchart";
class BunkeringTimePerformance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Vessel Name",
          //   dataIndex: "port",
          align: "left",
          //   render:(el)=><span className="spot-first" >{el}</span>
        },
        {
          title: "Vessel Type",
          //   dataIndex: "region",
          align: "left",
        },
        {
          title: "DWT",
          //   dataIndex: "grade",
          align: "left",
        },

        {
          title: "Bunkering Port",
          //   dataIndex: "country",
          align: "left",
        },
        {
          title: "Arrival Date/Time",
          //   dataIndex: "currentPrice",
          align: "left",
        },
        {
          title: "Bunkering Start time",
          //   dataIndex: "lastUpdatedOnUTC",
          //   render: (date) => date && moment(date).format("LLL"),
          align: "left",
        },
        {
          title: "Bunkering End Time",
          //   dataIndex: "isHolidayInPort",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Total Bunker Time",
          //   dataIndex: "fuelAvailibility",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Port Arrival Time",
          //   dataIndex: "netChange",
        },
        {
          title: "Port Departure Time",
          //   dataIndex: "netChange",
        },
        {
          title: "Total Port time",
          //   dataIndex: "netChange",
        },

       
      ],
    };
  }



  render() {
    const { columns } = this.state;

    return (
      <>
        
        <div style={{ padding: "20px"}}>
          <div style={{ marginBottom: "20px" }}>
            <Input
              placeholder="Search"
              style={{ width: "300px", margin: "0 auto"}}
              onChange={this.onsearchfilter}
            />
          </div>

          <Table
            className="spot voyage-operation"
            columns={columns}
            //   dataSource={portdata}
            size="medium"
            //   pagination={{
            //     pageSize: 50,
            //     showSizeChanger: true,
            //     total: portdata.length,
            //   }}
          />
        </div>
        
        <div class="shadow-lg rounded-lg overflow-hidden">
  <div class="py-3 px-5 bg-gray-50"></div>
<div >
  <BarChart/> 
  </div></div>
    

      </>
    );
  }
}

export default BunkeringTimePerformance;
