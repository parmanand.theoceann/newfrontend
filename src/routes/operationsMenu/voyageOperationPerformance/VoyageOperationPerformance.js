import React from "react";
import moment from "moment";

import { Table, Input, Icon } from "antd";
import BarChart from "../operationchart";
class VoyageOperationPerformance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Voyage No.",
          //   dataIndex: "port",
          align: "left",
          //   render:(el)=><span className="spot-first" >{el}</span>
        },
        {
          title: "Vessel Name",
          //   dataIndex: "region",
          align: "left",
        },
        {
          title: "Voyage type",
          //   dataIndex: "grade",
          align: "left",
        },

        {
          title: "Cargo Name",
          //   dataIndex: "country",
          align: "left",
        },
        {
          title: "Total Sea Cons.",
          //   dataIndex: "currentPrice",
          align: "left",
        },
        {
          title: "Total Port cons.",
          //   dataIndex: "lastUpdatedOnUTC",
          //   render: (date) => date && moment(date).format("LLL"),
          align: "left",
        },
        {
          title: "Commence Date",
          //   dataIndex: "isHolidayInPort",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "completed Date",
          //   dataIndex: "fuelAvailibility",
          //   render: (data) => (data == false ? "No" : "Yes"),
          align: "left",
        },
        {
          title: "Total Voyage Date",
          //   dataIndex: "netChange",
        },
        {
          title: "Total Port Expenses",
          //   dataIndex: "netChange",
        },
        {
          title: "Total Revenue",
          //   dataIndex: "netChange",
        },

        {
          title: "Total Expenses",
          //   dataIndex: "netChange",
        },
        {
            title: "Net Result",
            //   dataIndex: "netChange",
          },
          {
            title: "Daily Profit",
            //   dataIndex: "netChange",
          },
      ],
    };
  }



  render() {
    const { columns } = this.state;

    return (
      <>
        
        <div style={{ padding: "20px"}}>
          <div style={{ marginBottom: "20px" }}>
            <Input
              placeholder="Search"
              style={{ width: "300px", margin: "0 auto"}}
              onChange={this.onsearchfilter}
            />
          </div>

          <Table
            className="spot voyage-operation"
            columns={columns}
            //   dataSource={portdata}
            size="medium"
            //   pagination={{
            //     pageSize: 50,
            //     showSizeChanger: true,
            //     total: portdata.length,
            //   }}
          />
        </div>
        
        
        <div class="shadow-lg rounded-lg overflow-hidden">
  <div class="py-3 px-5 bg-gray-50"></div>
<div >
  <BarChart/>
  </div></div>
    

      </>
    );
  }
}

export default VoyageOperationPerformance;
