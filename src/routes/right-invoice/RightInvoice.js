import React, { Component } from 'react';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc } from '../../shared';
import { Collapse, Spin, Alert } from 'antd';
import moment from 'moment';

const { Panel } = Collapse;

class RightInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      frmVisible: false,
      invData: null,
      commData: null,
      tcCommissionData: [],
      showPopup: false,
      popupData: {},
      expandIconPosition: 'right',
      portExpInvioces: [],
      voyID:this.props.voyage_number,
    }
  }

  componentDidMount() {
    if (this.props && this.props.voyageManagerId) {
      this._onListClick(this.props.voyageManagerId)
    }
  }



  _onListClick = async vmId => {
    this.setState({ ...this.state, frmVisible: false });
    let _url = `${URL_WITH_VERSION}/freight-invoice/list?l=0`;
    let headers = { 'order_by': { 'id': 'desc' }, 'where': { 'voyage_id': vmId } }
    const response = await getAPICall(_url, headers);
    const invData = await response['data'];
    _url = `${URL_WITH_VERSION}/freight-commission/list?l=0`;
    const commresponse = await getAPICall(_url, headers);
    const commData = await commresponse['data'];

    const tcComResponse = await getAPICall(`${URL_WITH_VERSION}/commission/list?l=0`, headers)
    const tcCommission = await tcComResponse;

    let _tcCommission = []
    if (tcCommission && tcCommission['data'] && typeof tcCommission === 'object' && tcCommission['data'].length > 0) {
      _tcCommission = tcCommission['data']
    }


    const PortExpResponse = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/port-expenses/list?e=${this.state.voyID}`);
    const PortExp = await PortExpResponse['data'];
  

    this.setState({ ...this.state, frmVisible: false, invData: invData, commData: commData, tcCommissionData: _tcCommission,portExpInvioces:PortExp }, () => {
      this.setState({ ...this.state, frmVisible: true });
    });
  };

  onPositionChange = expandIconPosition => {
    this.setState({ expandIconPosition });
  }

  render() {
    const { frmVisible, invData, commData, expandIconPosition, tcCommissionData, portExpInvioces } = this.state;
    return (
      <div className="body-wrapper modalWrapper m-0 fieldscroll-wraps-scroll collapse-panel-heading">
        <Collapse defaultActiveKey={['1']} accordion expandIconPosition={expandIconPosition}>
          <Panel header="Freight Invoice" key="1">
            {
              frmVisible ?
                invData.map((item, i) => <div className="box box-default mb-2" key={i}>
                  <div className="box-body" style={{ cursor: 'pointer' }} onClick={() => this.props.showInvoice(item.invoice_no,'Freight Invoice')}>
                    <h4 className="m-0 text-primary"><strong>Inv. No.:</strong> {item.invoice_no}</h4>
                    <p className="m-0"><strong>Amount:</strong> {item.total_amount}</p>
                    <p className="m-0"><strong>Status:</strong> {item.fia_status}, <strong>Due:</strong> {item.due_date}</p>
                  </div>
                </div>
                ) : <div className="col col-lg-12">
                  <Spin tip="Loading...">
                    <Alert
                      message=" "
                      description="Please wait..."
                      type="info"
                    />
                  </Spin>
                </div>
            }
          </Panel>
          <Panel header="Freight Commission Invoice" key="2">
            {
              frmVisible ?
                commData.map((item, i) => <div className="box box-default mb-2" key={i}>
                  <div className="box-body" style={{ cursor: 'pointer' }} onClick={() => this.props.showCommission(item.invoice_no,"Freight Commission Invoice")}>
                    <h4 className="m-0 text-primary"><strong>Inv. No.:</strong> {item.invoice_no}</h4>
                    <p className="m-0"><strong>Amount:</strong> {item.commission_amount}</p>
                    <p className="m-0"><strong>Status:</strong> {item.com_status}, <strong>Due:</strong> {moment(item.due_date).format('YYYY-MM-DD')}</p>
                  </div>
                </div>
                ) : <div className="col col-lg-12">
                  <Spin tip="Loading...">
                    <Alert
                      message=" "
                      description="Please wait..."
                      type="info"
                    />
                  </Spin>
                </div>
            }
          </Panel>
          <Panel header="TC Commission" key="3">
            {
              frmVisible ?
                tcCommissionData.map((item, i) => <div className="box box-default mb-2" key={i}>
                  <div className="box-body" style={{ cursor: 'pointer' }} onClick={() => this.props.showCommission(item.invoice_no,'TC Commission')}>
                    <h4 className="m-0 text-primary"><strong>Inv. No.:</strong> {item.invoice_no}</h4>
                    <p className="m-0"><strong>Amount:</strong> {item.inv_total}</p>
                    <p className="m-0"><strong>Status:</strong> {item.inv_status_name}, <strong>Due:</strong> {moment(item.due_date).format('YYYY-MM-DD')}</p>
                  </div>
                </div>
                )
                : <div className="col col-lg-12">
                  <Spin tip="Loading...">
                    <Alert
                      message=" "
                      description="Please wait..."
                      type="info"
                    />
                  </Spin>
                </div>
            }
          </Panel>
          <Panel header="Port-Expense Invoice" key="4">
            {
              frmVisible ?
              portExpInvioces &&  portExpInvioces.map((item, i) => <div className="box box-default mb-2" key={i}>
                  <div className="box-body" style={{ cursor: 'pointer' }} onClick={() => this.props.showPortExpense(item)}>
                    <h4 className="m-0 text-primary"><strong>Inv. No.:</strong> {item.disburmnt_inv}</h4>
                    <p className="m-0"><strong>Amount:</strong> {item.invoice_type === 209 ? item.agreed_est_amt : item.total_amt}</p>
                    <p className="m-0"><strong>Status:</strong> {item.pda_adv_status == '145' ? 'SENT' : item.pda_adv_status == '144' ? 'ACTUAL' : 'PENDING'}, <strong>Due:</strong> {moment(item.pda_inv_date).format('YYYY-MM-DD')}</p>
                  </div>
                </div>
                )
                : <div className="col col-lg-12">
                  <Spin tip="Loading...">
                    <Alert
                      message=" "
                      description="Please wait..."
                      type="info"
                    />
                  </Spin>
                </div>
            }
          </Panel>
        </Collapse>
      </div>
    );
  }
}

export default RightInvoice;


