import React, { Component } from "react";
import {
  Spin,
  Alert,
  Steps,
  Input,
  Menu,
  Modal,
  Progress,
  Pagination,
  Tooltip,
  Table,
  DatePicker,
  Form,
  Select,
  Dropdown,
} from "antd";
import URL_WITH_VERSION, { getAPICall } from "../../shared";
//import VoyageOperation from '../voyage-list/components/VoyageOperation';
//import ToolbarUI from 'components/ToolbarUI';
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import ShipDetail from "../dynamic-vspm/ShipDetail";
import Pie1 from "./Pie1";
import Pie2 from "./Pie2";
import AnalyticalMap from "./AnalyticalMap";
import {
  ScheduleOutlined,
  CompassOutlined,
  ExportOutlined,
  ClockCircleOutlined,
  CheckCircleOutlined,
  LineChartOutlined,
  ShrinkOutlined,
  CloseCircleOutlined,
  BarChartOutlined,
  CalendarOutlined,
} from "@ant-design/icons";

// const FormItem = Form.Item;
// const Option = Select.Option;
const { RangePicker } = DatePicker;

const { Step } = Steps;

const columns = [
  {
    title: "Invoice type",
    dataIndex: "invoice_type",
  },

  {
    title: "Due date",
    dataIndex: "due_date",
  },

  {
    title: "Status",
    dataIndex: "status",
  },

  {
    title: "Vessel",
    dataIndex: "vessel",
  },
];
const data = [
  {
    key: "1",
    invoice_type: "Hire payment",
  },

  {
    key: "2",
    invoice_type: "Vendor payment",
  },

  {
    key: "3",
    invoice_type: "Port payment",
  },

  {
    key: "4",
    invoice_type: "Bunker payment",
  },

  {
    key: "5",
    invoice_type: "Frieght payment",
  },

  {
    key: "6",
    invoice_type: "Hire receivable",
  },
];

const columns1 = [
  {
    title: "Laytime claim",
    dataIndex: "laytime_claim",
  },

  {
    title: "Receivable",
    dataIndex: "receiable",
  },

  {
    title: "Payable",
    dataIndex: "payable",
  },

  {
    title: "Disputed",
    dataIndex: "disputed",
  },
];
const data1 = [
  {
    key: "1",
    laytime_claim: "Demurrage",
  },

  {
    key: "2",
    laytime_claim: "Despacth",
  },
];
// const AnyReactComponent = ({ text }) => (
//   <div style={{
//     color: 'white',
//     padding: '15px 10px',
//     display: 'inline-flex',
//     textAlign: 'center',
//     alignItems: 'center',
//     justifyContent: 'center',
//     transform: 'translate(-50%, -50%)'
//   }}>
//     <img alt="Ship"
//       src="../../assets/ship.png"
//       height="30"
//       className="map-ship2"
//       onClick={() => text(true, 'ShipDetails1')}
//      />
//   </div>
// );

class AnalyticalDashboard extends Component {
  callback = (evt) => {};
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      data: [],
      countries: [],
      tradeAreas: [],
      tradeUser: [],
      items: [],
      DataisLoaded: false,
      cargo: [],
      vessel: [],
      voyage: [],
      ops: [],
      opsValue: "Ops user",
      voyageValue: "voyage type",
      vesselValue: "Vessel Name",
      CharteringValue: "Chartering user",
      VesselTypeValue: "Vessel Type",
      formData: this.props.formData || {},
      showPL: false,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || [
        "Estimate View",
        "Actual &  Operation View",
        "Account View",
      ],
      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: "",
          children: [
            { key: "rev11", description: "Freight", estimate: 0 },
            { key: "rev12", description: "Freight Commission", estimate: 0 },
            { key: "rev17", description: "Mis Revenue", estimate: 0 },
            { key: "rev18", description: "Gross Revenue", estimate: 0 },
            { key: "rev19", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses.",
          estimate: "",
          children: [
            {
              key: "ex11",
              description: "Vessel expenses",
              estimate: "",
              children: [
                { key: "ex111", description: "Hire Expenses", estimate: 0 },
                { key: "ex112", description: "Ballast Bonus", estimate: 0 },
              ],
            },
            { key: "ex12", description: "TCI Add. Comm.", estimate: 0 },
            { key: "ex13", description: "TCI Broker Comm.", estimate: 0 },
            { key: "ex14", description: "Port Expenses", estimate: 0 },
            {
              key: "ex15",
              description: "Bunker Expenses",
              estimate: 0,
              children: [
                {
                  key: "ex151",
                  description: "Sea Consumption",
                  estimate: 0,
                  children: [
                    { key: "ex1511", description: "IFO", estimate: 0 },
                    { key: "ex1512", description: "MGO", estimate: 0 },
                    { key: "ex1513", description: "VLSFO", estimate: 0 },
                    { key: "ex1514", description: "LSMGO", estimate: 0 },
                  ],
                },
                {
                  key: "ex152",
                  description: "Port Consumption",
                  estimate: 0,
                  children: [
                    { key: "ex1521", description: "IFO", estimate: 0 },
                    { key: "ex1522", description: "MGO", estimate: 0 },
                    { key: "ex1523", description: "VLSFO", estimate: 0 },
                    { key: "ex1524", description: "LSMGO", estimate: 0 },
                    { key: "ex1525", description: "ULSFO", estimate: 0 },
                  ],
                },
              ],
            },
            { key: "ex17", description: "Mis Expenses", estimate: 0 },
            { key: "ex18", description: "Total Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr11", description: "Profit (Loss)", estimate: 0 },
            { key: "vr12", description: "Daily Profit (Loss)", estimate: 0 },
            { key: "vr13", description: "TCE Hire ( Net Daily )", estimate: 0 },
            { key: "vr14", description: "Gross TCE", estimate: 0 },
            { key: "vr15", description: "Freight rate ($/T)", estimate: 0 },
            {
              key: "vr16",
              description: "Breakeven & Freight rate ($/T)",
              estimate: 0,
            },
            { key: "vr17", description: "Net Voyage Days", estimate: 0 },
            { key: "vr18", description: "Off Hire Days", estimate: 0 },
            { key: "vr19", description: "Total Sea Days", estimate: 0 },
            { key: "vr20", description: "Total Port Days", estimate: 0 },
          ],
        },
      ],
      modals: {
        ShipDetails1: false,
      },
      title: `vessel Name:VSL1412211\nlat:18.5204\nlng:73.8567`,
      mapSettings: {
        center: { lat: 20.5937, lng: 78.9629 },
        zoom: 2,
      },
      latlng: [],
      shipData: null,
      loading: false,
      vessel_name: "Spring",
    };
  }
  componentDidMount = async () => {
    this.setState({
      loading: true,
    });
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(`${URL_WITH_VERSION}/vessel/live/list?p=0&l=2`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          data: result.data,
          loading: false,
        });
      })
      .catch((error) => undefined);

    //List of Vessel
    let _urlVessel = `${URL_WITH_VERSION}/vessel/list?l=0`;
    let resquestVessel = await getAPICall(_urlVessel);
    let responseVessel = await resquestVessel;
    this.setState({ ...this.state, vessel: responseVessel["data"] }, () =>
      this.setState({ ...this.state, visible: true })
    );
    //List of Voyage Type
    let _urlVoyage = `${URL_WITH_VERSION}/voyage-manager/list?l=0`;
    let resquestVoyage = await getAPICall(_urlVoyage);
    let responseVoyage = await resquestVoyage;
    this.setState({ ...this.state, voyage: responseVoyage["data"] }, () =>
      this.setState({ ...this.state, visible: true })
    );
    //List of Vessel
    let _urlVesselType = `${URL_WITH_VERSION}/vessel/list?l=0`;
    let resquestVesselType = await getAPICall(_urlVesselType);
    let responseVesselType = await resquestVesselType;
    this.setState(
      { ...this.state, vesselType: responseVesselType["data"] },
      () => this.setState({ ...this.state, visible: true })
    );
    //List of Ops user
    let _urlOps = `${URL_WITH_VERSION}/vessel/list?l=0`;
    let resquestOps = await getAPICall(_urlOps);
    let responseOps = await resquestOps;
    this.setState({ ...this.state, ops: responseOps["data"] }, () =>
      this.setState({ ...this.state, visible: true })
    );
    //List of Ops user
    let _urlChartering = `${URL_WITH_VERSION}/vessel/list?l=0`;
    let resquestChartering = await getAPICall(_urlChartering);
    let responseChartering = await resquestChartering;
    this.setState(
      { ...this.state, chartering: responseChartering["data"] },
      () => this.setState({ ...this.state, visible: true })
    );
    let dollarUSLocale = Intl.NumberFormat("en-US");

    let { estimateData } = this.state;
    let totalVoyageDays = this.state.formData["total_days"]
      ? isNaN(("" + this.state.formData["total_days"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + this.state.formData["total_days"]).replaceAll(",", "") * 1
      : 0;

    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      comm = 0,
      grossRevenue = 0,
      netRevenue = 0,
      demmurage = 0,
      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      totalExpenses = 0,
      cpQty = 0;
    let bb = this.state.formData["bb"]
      ? isNaN(("" + this.state.formData["bb"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + this.state.formData["bb"]).replaceAll(",", "") * 1
      : 0;
    let misCost = this.state.formData["mis_cost"]
      ? isNaN(("" + this.state.formData["mis_cost"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + this.state.formData["mis_cost"]).replaceAll(",", "") * 1
      : 0;
    let hire = this.state.formData["tci_d_hire"]
      ? (this.state.formData["tci_d_hire"] + "").replaceAll(",", "") * 1
      : 0;
    let addPercentage = this.state.formData["add_percentage"]
      ? (this.state.formData["add_percentage"] + "").replaceAll(",", "") * 1
      : 0;
    let broPercentage = this.state.formData["bro_percentage"]
      ? (this.state.formData["bro_percentage"] + "").replaceAll(",", "") * 1
      : 0;
    let portExpenses = { IFO: 0, mgo: 0, vlsfo: 0, lsmgo: 0, ulsfo: 0 },
      seaExpenses = { IFO: 0, mgo: 0, vlsfo: 0, lsmgo: 0, ulsfo: 0 };
    let totalSeaConsumption = 0,
      totalPortConsumption = 0;

    if (
      this.state.formData &&
      this.state.formData.hasOwnProperty("---------")
    ) {
      let _tsd = this.state.formData["---------"]["tsd"];
      tsd = _tsd && _tsd > 0 ? _tsd : 0;
    }

    if (
      this.state.formData &&
      this.state.formData.hasOwnProperty("portdatedetails")
    ) {
      let portDateDetails = this.state.formData["portdatedetails"];
      portDateDetails.map(
        (e) => (tpd += (e.pdays + "").replaceAll(",", "") * 1)
      );
    }

    if (
      this.state.formData &&
      this.state.formData.hasOwnProperty("bunkerdetails")
    ) {
      let bunkerDetails = this.state.formData["bunkerdetails"];
      bunkerDetails.map((e) => {
        seaExpenses["IFO"] += e["IFO"]
          ? isNaN(("" + e["IFO"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["IFO"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["mgo"] += e["mgo"]
          ? isNaN(("" + e["mgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["mgo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["vlsfo"] += e["vlsfo"]
          ? isNaN(("" + e["vlsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["vlsfo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["lsmgo"] += e["lsmgo"]
          ? isNaN(("" + e["lsmgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["lsmgo"]).replaceAll(",", "") * 1
          : 0;

        portExpenses["IFO"] += e["pc_ifo"]
          ? isNaN(("" + e["pc_IFO"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_IFO"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["mgo"] += e["pc_mgo"]
          ? isNaN(("" + e["pc_mgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_mgo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["vlsfo"] += e["pc_vlsfo"]
          ? isNaN(("" + e["pc_vlsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_vlsfo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["lsmgo"] += e["pc_lsmgo"]
          ? isNaN(("" + e["pc_lsmgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_lsmgo"]).replaceAll(",", "") * 1
          : 0;
        return true;
      });
    }

    if (this.state.formData && this.state.formData.hasOwnProperty(".")) {
      let cpData = this.state.formData["."];
      cpData.map((e) => {
        switch (e.fuel_code) {
          case "IFO":
            seaExpenses["IFO"] =
              seaExpenses["IFO"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            portExpenses["IFO"] =
              portExpenses["IFO"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            totalPortConsumption += portExpenses["IFO"];
            totalSeaConsumption += seaExpenses["IFO"];
            break;
          case "MGO":
            seaExpenses["hmgo"] =
              seaExpenses["mgo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            portExpenses["mgo"] =
              portExpenses["mgo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            totalPortConsumption += portExpenses["mgo"];
            totalSeaConsumption += seaExpenses["mgo"];
            break;
          case "VLSFO":
            seaExpenses["vlsfo"] =
              seaExpenses["vlsfo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            portExpenses["vlsfo"] =
              portExpenses["vlsfo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            totalPortConsumption += portExpenses["vlsfo"];
            totalSeaConsumption += seaExpenses["vlsfo"];
            break;
          case "LSMGO":
            seaExpenses["lsmgo"] =
              seaExpenses["lsmgo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            portExpenses["lsmgo"] =
              portExpenses["lsmgo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            totalPortConsumption += portExpenses["lsmgo"];
            totalSeaConsumption += seaExpenses["lsmgo"];
            break;
          case "ULSFO":
            seaExpenses["ulsfo"] =
              seaExpenses["ulsfo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            portExpenses["ulsfo"] =
              portExpenses["ulsfo"] *
              (e["cp_price"]
                ? isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
                  ? 0
                  : ("" + e["cp_price"]).replaceAll(",", "") * 1
                : 0);
            totalPortConsumption += portExpenses["ulsfo"];
            totalSeaConsumption += seaExpenses["ulsfo"];
            break;
          default:
        }
        return true;
      });
      // google-map
      let headers = { order_by: { id: "desc" } };
      let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/graph?`;
      const response = await getAPICall(_url, headers);
      const data = await response;
      this.setState({
        ...this.state,
        latlng: data["data"],
      });
    }

    // console.log(portExpenses, seaExpenses, totalSeaConsumption, totalPortConsumption);
    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;

    if (
      this.state.formData &&
      this.state.formData.hasOwnProperty("portitinerary")
    ) {
      let portitinerary = this.state.formData["portitinerary"];
      portitinerary.map(
        (e) =>
          (pi += isNaN(("" + e.p_exp).replaceAll(",", "") * 1)
            ? 0
            : ("" + e.p_exp).replaceAll(",", "") * 1)
      );
    }

    if (this.state.formData && this.state.formData.hasOwnProperty("cargos")) {
      let cargos = this.state.formData["cargos"];
      cargos.map((e, i) => {
        mr += e.extra_rev ? (e.extra_rev + "").replaceAll(",", "") * 1 : 0;
        cpQty += e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0;

        if (parseFloat(e.option_percentage) !== 0) {
          let frtRate =
            ((e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) +
              ((e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) *
                (e.option_percentage
                  ? (e.option_percentage + "").replaceAll(",", "")
                  : 0)) /
                100) *
            (e.freight_rate
              ? (e.freight_rate + "").replaceAll(",", "") * 1
              : 0);
          let commission =
            (frtRate *
              1 *
              (e.comm_per ? (e.comm_per + "").replaceAll(",", "") * 1 : 0)) /
            100;
          fr += frtRate;
          comm += commission;
        } else {
          let frtRate =
            (e.cp_qty ? (e.cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (e.freight_rate
              ? (e.freight_rate + "").replaceAll(",", "") * 1
              : 0);
          let commission =
            (frtRate *
              1 *
              (e.comm_per ? (e.comm_per + "").replaceAll(",", "") * 1 : 0)) /
            100;
          fr += frtRate;
          comm += commission;
        }
        return true;
      });
    }

    grossRevenue = fr + mr + demmurage;
    netRevenue =
      grossRevenue - (freightCommission + demmurageCommission + dispatch);
    totalExpenses =
      totalVoyageDays * hire +
      bb +
      pi +
      misCost +
      ((addPercentage * hire) / 100) * -1 +
      ((broPercentage * hire) / 100) * -1 +
      0;
    estimateData[0]["children"][0]["estimate"] = dollarUSLocale.format(
      fr.toFixed(2)
    );
    estimateData[0]["children"][1]["estimate"] = dollarUSLocale.format(
      comm.toFixed(2)
    );
    estimateData[0]["children"][2]["estimate"] = dollarUSLocale.format(
      mr.toFixed(2)
    );
    estimateData[0]["children"][3]["estimate"] = dollarUSLocale.format(
      grossRevenue.toFixed(2)
    );
    estimateData[0]["children"][4]["estimate"] = dollarUSLocale.format(
      netRevenue.toFixed(2)
    );

    estimateData[1]["children"][0]["children"][0]["estimate"] =
      dollarUSLocale.format((totalVoyageDays * hire).toFixed(2));
    estimateData[1]["children"][0]["children"][1]["estimate"] =
      dollarUSLocale.format(bb.toFixed(2));
    estimateData[1]["children"][1]["estimate"] = dollarUSLocale.format(
      (((addPercentage * hire) / 100) * -1).toFixed(2)
    );
    estimateData[1]["children"][2]["estimate"] = dollarUSLocale.format(
      (((broPercentage * hire) / 100) * -1).toFixed(2)
    );
    estimateData[1]["children"][3]["estimate"] = dollarUSLocale.format(
      pi.toFixed(2)
    );
    estimateData[1]["children"][4]["estimate"] = dollarUSLocale.format(
      (totalSeaConsumption + totalPortConsumption).toFixed(2)
    );

    estimateData[1]["children"][4]["children"][0]["estimate"] =
      dollarUSLocale.format(totalSeaConsumption.toFixed(2));
    estimateData[1]["children"][4]["children"][0]["children"][0]["estimate"] =
      dollarUSLocale.format(seaExpenses["IFO"].toFixed(2));
    estimateData[1]["children"][4]["children"][0]["children"][1]["estimate"] =
      dollarUSLocale.format(seaExpenses["mgo"].toFixed(2));
    estimateData[1]["children"][4]["children"][0]["children"][2]["estimate"] =
      dollarUSLocale.format(seaExpenses["vlsfo"].toFixed(2));
    estimateData[1]["children"][4]["children"][0]["children"][3]["estimate"] =
      dollarUSLocale.format(seaExpenses["lsmgo"].toFixed(2));
    estimateData[1]["children"][4]["children"][1]["estimate"] =
      dollarUSLocale.format(totalPortConsumption.toFixed(2));
    estimateData[1]["children"][4]["children"][1]["children"][0]["estimate"] =
      dollarUSLocale.format(portExpenses["IFO"].toFixed(2));
    estimateData[1]["children"][4]["children"][1]["children"][1]["estimate"] =
      dollarUSLocale.format(portExpenses["mgo"].toFixed(2));
    estimateData[1]["children"][4]["children"][1]["children"][2]["estimate"] =
      dollarUSLocale.format(portExpenses["vlsfo"].toFixed(2));
    estimateData[1]["children"][4]["children"][1]["children"][3]["estimate"] =
      dollarUSLocale.format(portExpenses["lsmgo"].toFixed(2));
    estimateData[1]["children"][5]["estimate"] = dollarUSLocale.format(
      misCost.toFixed(2)
    );
    estimateData[1]["children"][6]["estimate"] = dollarUSLocale.format(
      totalExpenses.toFixed(2)
    );

    let itemValue = netRevenue - totalExpenses;
    estimateData[2]["estimate"] =
      itemValue >= 0
        ? itemValue.toFixed(2)
        : "(" + (itemValue * -1).toFixed(2) + ")";
    estimateData[2]["children"][0]["estimate"] = estimateData[2]["estimate"];
    estimateData[2]["children"][1]["estimate"] = (
      itemValue /
      (totalVoyageDays - 0)
    ).toFixed(2);
    estimateData[2]["children"][2]["estimate"] = (
      itemValue -
      (totalExpenses -
        (bb + pi + 0 + misCost + ((addPercentage * hire) / 100) * -1)) /
        (itemValue / (totalVoyageDays - 0))
    ).toFixed(2);
    estimateData[2]["children"][3]["estimate"] = (
      addPercentage > 0
        ? estimateData[2]["children"][2]["estimate"] / (1 - addPercentage)
        : 0
    ).toFixed(2);
    estimateData[2]["children"][5]["estimate"] =
      totalExpenses / cpQty >= 0
        ? (totalExpenses / cpQty).toFixed(2)
        : "(" + ((totalExpenses / cpQty) * -1).toFixed(2) + ")"; // Profit is 0 since we are doing Breakeven
    estimateData[2]["children"][6]["estimate"] = (totalVoyageDays - 0).toFixed(
      2
    ); // OFF Hire Day => 0
    estimateData[2]["children"][8]["estimate"] = (tsd * 1).toFixed(2);
    estimateData[2]["children"][9]["estimate"] = (tpd * 1).toFixed(2);

    this.setState({ ...this.state, estimateData: estimateData }, () =>
      this.setState({ ...this.state, showPL: true })
    );
  };
  getInput = () => {
    const { inputType } = this.props;
    switch (inputType) {
      case "dropdown":
        return this.getSelectField();
      case "number":
        return this.getInputNumberField();
      default:
        return this.getInputField();
    }
  };
  vesselClick = (value) => {
    this.setState({ OpsValue: value.status_list });
  };
  vesselName = () => {
    const { vessel } = this.state;
    return (
      <Menu>
        {vessel.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.vesselClick(x)}>{x.vessel_name}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };
  voyageList = () => {
    const { voyage } = this.state;
    return (
      <Menu>
        {voyage.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.voyageClick(x)}>{x.ops_type}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };
  voyageClick = (value) => {
    this.setState({ OpsValue: value.status_list });
  };
  vesselList = () => {
    const { vessel } = this.state;
    return (
      <Menu>
        {vessel.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.vesselTypeClick(x)}>{x.ops_type}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };

  voyageClick = (value) => {
    this.setState({ OpsValue: value.status_list });
  };
  opsList = () => {
    const { voyage } = this.state;
    return (
      <Menu>
        {voyage.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.voyageClick(x)}>{x.status_list}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };
  charteringClick = (value) => {
    this.setState({ CharteringValue: value.status_list });
  };
  charteringList = () => {
    const { voyage } = this.state;
    return (
      <Menu>
        {voyage.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.charteringClick(x)}>{x.status_list}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };
  vesselType = () => {
    const { vessel } = this.state;
    return (
      <Menu>
        {vessel.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.vesselTypeClick(x)}>{x.vessel_type_name}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
  };
  vesselTypeClick = (value) => {
    this.setState({ OpsValue: value.vessel_type });
  };
  getInput = () => {
    const { inputType } = this.props;
    switch (inputType) {
      case "dropdown":
        return this.getSelectField();
      case "number":
        return this.getInputNumberField();
      default:
        return this.getInputField();
    }
  };
  state = {
    current: 0,
  };

  onChange = (current) => {
    this.setState({ current });
  };

  render() {
    const {
      hire,
      totalVoyageDays,
      mapSettings,
      shipData,
      itemValue,
      bunkerDetails,
      estimateData,
      showPL,
      grossRevenue,
      vesselValue,
      current,
      voyageValue,
      opsValue,
      CharteringValue,
      VesselTypeValue,
    } = this.state;
    const style = {
      position: "absolute",
      height: "91%",
      width: "96%",
    };
    return (
      // <div className="body-wrapper modalWrapper dashbord-cominsoonmain">

      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-12">
                  <h4 style={{ fontWeight: "600", fontSize: "24px" }}>
                    Filters
                  </h4>
                  <div className="card-group p-0 border-0 rounded-0">
                    <div className="card p-0 border-0 rounded-0">
                      <div className="card-body border-0 rounded-0 pl-0">
                        <RangePicker />
                      </div>
                    </div>

                    <div className="card p-0 border-0 rounded-0">
                      <div className="card-body border-0 rounded-0">
                        <Dropdown
                          overlay={this.vesselName()}
                          trigger={["click"]}
                        >
                          <a className="ant-dropdown-link">
                            <Input size="default" placeholder={vesselValue} />
                          </a>
                        </Dropdown>
                      </div>
                    </div>

                    <div className="card border-0 rounded-0 p-0">
                      <div className="card-body border-0 rounded-0">
                        <Dropdown
                          overlay={this.voyageList()}
                          trigger={["click"]}
                        >
                          <a className="ant-dropdown-link">
                            <Input size="default" placeholder={voyageValue} />
                          </a>
                        </Dropdown>
                      </div>
                    </div>

                    <div className="card border-0 rounded-0 p-0">
                      <div className="card-body border-0 rounded-0">
                        <Dropdown overlay={this.opsList()} trigger={["click"]}>
                          <a className="ant-dropdown-link">
                            <Input size="default" placeholder={opsValue} />
                          </a>
                        </Dropdown>
                      </div>
                    </div>

                    <div className="card border-0 rounded-0 p-0">
                      <div className="card-body border-0 rounded-0">
                        <Dropdown
                          overlay={this.charteringList()}
                          trigger={["click"]}
                        >
                          <a className="ant-dropdown-link">
                            <Input
                              size="default"
                              placeholder={CharteringValue}
                            />
                          </a>
                        </Dropdown>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <h4 className="mb-3 Char_dashboard">Voyage Tracking</h4>
              <Steps current={current} onChange={this.onChange}>
                <Step
                  title="Schedule"
                  description="12/07/1997"
                  icon={<ScheduleOutlined />}
                />
                <Step
                  title="Voyage commence"
                  description="12/07/1997"
                  icon={<CompassOutlined />}
                />
                <Step
                  title="1 Port Arrival"
                  description="12/07/1997"
                  icon={<ExportOutlined />}
                />
                <Step
                  title="Final Port Departure"
                  description="12/07/1997"
                  icon={<ClockCircleOutlined />}
                />
                <Step
                  title="Voyage completed"
                  description="12/07/1997"
                  icon={<CheckCircleOutlined />}
                />
                <Step
                  title="Voyage closed"
                  description="12/07/1997"
                  icon={<CloseCircleOutlined />}
                />
              </Steps>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-12">
                  <h4 className="Char_dashboard">
                    Heading
                    <span className="float-right">
                      <Dropdown overlay={this.vesselType()} trigger={["click"]}>
                        <a className="ant-dropdown-link">
                          <Input size="default" placeholder={VesselTypeValue} />
                        </a>
                      </Dropdown>
                    </span>
                  </h4>
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <div className="card-group mt-3">
                    <div className="card rounded-0">
                      <div className="card-body">
                        <h4 className="mb-0">Overall profit $</h4>
                        <h3 className="font-weight-bold mb-0">10 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">+5%</span> /{" "}
                          <span className="text-danger">-5%</span>
                        </h5>
                      </div>
                    </div>

                    <div className="card rounded-0">
                      <div className="card-body">
                        <h4 className="mb-0">Net revenue $</h4>
                        <h3 className="font-weight-bold mb-0">20 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">+2%</span> /{" "}
                          <span className="text-danger">-2%</span>
                        </h5>
                      </div>
                    </div>

                    <div className="card rounded-0">
                      <div className="card-body">
                        <h4 className="mb-0">Total expense $</h4>
                        <h3 className="font-weight-bold mb-0">2 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">5M</span> /{" "}
                          <span className="text-danger">-5%</span>
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card-group mt-3">
                    <div className="card rounded-0">
                      <div className="card-body">
                        <h4 className="mb-0">Overall profit $</h4>
                        <h3 className="font-weight-bold mb-0">10 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">+5%</span> /{" "}
                          <span className="text-danger">-5%</span>
                        </h5>
                      </div>
                    </div>

                    <div className="card rounded-0">
                      <div className="card-body ">
                        <h4 className="mb-0">Net revenue $</h4>
                        <h3 className="font-weight-bold mb-0">20 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">+4%</span> /{" "}
                          <span className="text-danger">-4%</span>
                        </h5>
                      </div>
                    </div>

                    <div className="card rounded-0">
                      <div className="card-body">
                        <h4 className="mb-0">Total expense $</h4>
                        <h3 className="font-weight-bold mb-0">5 M</h3>
                        <h5 className="mb-0">
                          Last Week <span className="text-success">2%</span> /{" "}
                          <span className="text-danger">-2%</span>
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <div className="card-group">
                    <div className="card">
                      <div className="card-body bg-success p-2 text-center">
                        <h4 className="text-white m-0 Char_dashboard_head">
                          Dry Vessel
                        </h4>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="card-group">
                    <div className="card">
                      <div className="card-body bg-success p-2 text-center">
                        <h4 className="text-white m-0 Char_dashboard_head">
                          Tanker Vessel
                        </h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="border p-3 rounded">
                    <div className="row">
                      <div className="col-md-4">
                        <h4 className="text-center Char_dashboard_head">
                          Vessel List
                        </h4>
                      </div>

                      <div className="col-md-8">
                        <h4 className="text-center Char_dashboard_head">
                          Timeline
                        </h4>
                      </div>
                    </div>
                    <hr />
                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>

                    <div className="row w-100 mb-2">
                      <div className="col-md-4">
                        <h4>Vessel</h4>
                      </div>
                      <div className="col-md-8">
                        <Tooltip title="3 Port Day / 3 Sea Day">
                          <Progress
                            percent={100}
                            successPercent={60}
                            format={() => "Days"}
                            strokeColor={
                              this.state.progress < 50 ? "yellow" : "#6960EC"
                            }
                          />
                        </Tooltip>
                      </div>
                    </div>
                    <hr />
                    <Pagination defaultCurrent={1} total={50} />
                  </div>
                </div>

                {showPL === true ? (
                  <div className="col-md-6">
                    <h4 className="Char_dashboard_head">
                      P&L Daily Variance
                      <span className="float-right">
                        <Input type="date" />
                      </span>
                    </h4>

                    <div className="card-group mt-3">
                      <div className="card rounded-0">
                        <div className="card-body text-center pnl-icons">
                          <LineChartOutlined />
                          <h4 className="mb-0">Gross revenue</h4>
                          <h2 className="font-weight-bold mb-0">200</h2>
                        </div>
                      </div>

                      <div className="card rounded-0">
                        <div className="card-body text-center pnl-icons">
                          <ShrinkOutlined />
                          <h4 className="mb-0">Hire exp.</h4>
                          <h2 className="font-weight-bold mb-0">300</h2>
                        </div>
                      </div>

                      <div className="card rounded-0">
                        <div className="card-body text-center pnl-icons">
                          <ExportOutlined />
                          <h4 className="mb-0">Bunker Exp.</h4>
                          <h2 className="font-weight-bold mb-0">400</h2>
                        </div>
                      </div>
                    </div>

                    <div className="card-group mt-3">
                      <div className="card rounded-0">
                        <div className="card-body text-center pnl-icons">
                          <BarChartOutlined />
                          <h4 className="mb-0">Profit/Loss</h4>
                          <h2 className="font-weight-bold mb-0">400</h2>
                        </div>
                      </div>

                      <div className="card rounded-0">
                        <div className="card-body text-center pnl-icons">
                          <CalendarOutlined />
                          <h4 className="mb-0">Voyage days</h4>
                          <h2 className="font-weight-bold mb-0">30</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="col col-lg-12">
                    <Spin tip="Loading...">
                      <Alert
                        message=" "
                        description="Please wait..."
                        type="info"
                      />
                    </Spin>
                  </div>
                )}
              </div>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="border">
                    <h4 className="mb-0  p-2 Char_dashboard_head">
                      Live vessel (Commenced/Delivered)
                    </h4>
                    <div>
                      {this.state.data.length > 0 ? (
                        <AnalyticalMap mapData={this.state.data} />
                      ) : null}
                      {/* <Map

                        google={this.props.google}
                        zoom={mapSettings.zoom}
                        initialCenter={mapSettings.center}
                        mapId="c29c09d0a6b2a312"
                      >
                        {this.state.latlng.map((e, ind) => {
                          return (
                            <Marker
                              title={this.state.title}
                              icon={{
                                url: "../../assets/ship.png",
                                anchor: new this.props.google.maps.Point(32, 32),
                                scaledSize: new this.props.google.maps.Size(30, 30)
                              }}
                              onClick={() => this.showHideModal(true, 'ShipDetails1', e)}
                              position={{ lat: e.lat, lng: e.lng }}
                            />
                          )
                        })}

                      </Map> */}
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="border p-3 rounded">
                    <div className="row">
                      <div className="col-md-6">
                        <h4 className="Char_dashboard_head">Ops Users</h4>
                      </div>

                      <div className="col-md-6">
                        <h4 className="Char_dashboard_head">Vessel Counts</h4>
                      </div>
                    </div>
                    <hr />

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>

                    <div className="row mb-2">
                      <div className="col-md-6">
                        <h4>User 1</h4>
                      </div>

                      <div className="col-md-6">
                        <h4>
                          8{" "}
                          <span className="float-right">
                            {/* <img
                              src="assets/images-demo/portcall-dashboard/1.png"
                              height="30"
                              width="60"
                              alt=""
                            /> */}
                          </span>
                        </h4>
                      </div>
                    </div>
                    <hr />
                    <Pagination defaultCurrent={1} total={50} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <Pie1 />
                </div>

                <div className="col-md-6">
                  <Pie2 />
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="border p-2 rounded">
                    <h4 className="mb-3 Char_dashboard_head">
                      Payment schedule
                    </h4>
                    <Table
                      bordered
                      size="small"
                      pagination={false}
                      dataSource={data}
                      scroll={{ x: "max-content" }}
                      columns={columns}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                      footer={false}
                    />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="border p-2 rounded">
                    <h4 className="mb-3 Char_dashboard_head">
                      Claim{" "}
                      <span className="float-right">
                        <Input type="time" />
                      </span>
                    </h4>
                    <Table
                      bordered
                      size="small"
                      pagination={false}
                      dataSource={data1}
                      scroll={{ x: "max-content" }}
                      columns={columns1}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                      footer={false}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
        {this.state.modals["ShipDetails1"] ? (
          <Modal
            className="ship-detail-modal"
            style={{ top: "2%" }}
            title={this.state.vessel_name}
            open={this.state.modals["ShipDetails1"]}
            onCancel={() => this.showHideModal(false, "ShipDetails1")}
            width="40%"
            footer={null}
            header={null}
          >
            <ShipDetail
              data={shipData}
              changeTitle={(data) => this.setState({ vessel_name: data })}
            />
          </Modal>
        ) : undefined}
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyCIZJxl4b3B520rAjPUqIu_YD5FHfiFQ6M",
})(AnalyticalDashboard);
