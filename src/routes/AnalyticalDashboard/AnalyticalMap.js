import React, { Component } from 'react'
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import "./Style.css";
import URL_WITH_VERSION, { IMAGE_PATH} from "../../shared";


const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

export default class AnalyticalMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coordinates: [],
      featuresData: [],
      lng:
        this.props.data && this.props.data.length > 0
          ? this.props.data[0].vessel_lat
          : 0.0,
      lat:
        this.props.data && this.props.data.length > 0
          ? this.props.data[0].vessel_lon
          : 0.0,
      zoom: .5,
      visibleModal: false,
      data: this.props.mapData,
    };
    this.mapContainer = React.createRef();
  }

  componentDidMount() {
    let mapLocation = [];
    this.state.data.map((item) => {
      return mapLocation.push({
        type: "Feature",
        properties: {
          description:
            `<strong>${item.vessel_owner_name}</strong><p>${item.vessel_status}</p>`,
          icon: "harbor-15",
        },
        geometry: {
          type: "Point",
          coordinates: [item.vessel_lat ? item.vessel_lat : 0.0, item.vessel_lon ? item.vessel_lon : 0.0],
        },
      });
    });
    this.setState({ featuresData: mapLocation })
    this.maprender();
  }

  maprender() {
    let that = this;
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "LineString",
            properties: {},
            coordinates: this.state.coordinates ? this.state.coordinates : [],
          },
        },
      ],
    };
    const map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
      center: this.state.coordinates ? this.state.coordinates[0] : [],
      zoom: this.state.zoom,
    });
    map.on("load", () => {
      // add mapbox terrain dem source for 3d terrain rendering
      map.addSource("LineString", {
        type: "geojson",
        data: geojson,
      });

      map.on("click", (e) => {
        // console.log(e);
      });

      map.addLayer({
        id: "LineString",
        type: "line",
        source: "LineString",
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": "#2414E2",
          "line-width": 4,
          "line-dasharray": [0, 1.5],
        },
      });

      map.loadImage(
        IMAGE_PATH+"icons/mapicon.png",
        (error, image) => {

          if (error) throw error;
          map.addImage("custom-marker", image);

          map.addSource("places", {
            // This GeoJSON contains features that include an "icon"
            // property. The value of the "icon" property corresponds
            // to an image in the Mapbox Streets style's sprite.

            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: this.state.featuresData,
            },

          });

          // Add a layer showing the places.
          map.addLayer({
            id: "places",
            type: "symbol",
            source: "places",
            layout: {
              "icon-image": "custom-marker",
              "icon-allow-overlap": true,
            },
          });

          // When a click event occurs on a feature in the places layer, open a popup at the
          // location of the feature, with description HTML from its properties.
          map.on("click", "places", (e) => {
            // Copy coordinates array.
            const coordinates = e.features[0].geometry.coordinates.slice();
            const description = e.features[0].properties.description;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
              coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
              .setLngLat(coordinates)
              .setHTML(description)
              .addTo(map);
          });

          // Change the cursor to a pointer when the mouse is over the places layer.
          map.on("mouseenter", "places", () => {
            map.getCanvas().style.cursor = "pointer";
          });

          // Change it back to a pointer when it leaves.
          map.on("mouseleave", "places", () => {
            map.getCanvas().style.cursor = "";
          });
        });
    });
  }
  render() {
    const { lng, lat, zoom } = this.state;
    return (
      <div>

        <div ref={this.mapContainer} className="map-container" />
      </div>
    );
  }
}
