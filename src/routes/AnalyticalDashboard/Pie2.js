import React, { Component, useState } from 'react';
import { Form, Input, Select, Pagination, Icon, Progress, Menu, Dropdown, DatePicker } from 'antd';
import URL_WITH_VERSION, { getAPICall, ResizeableTitle } from '../../shared';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from '../../constants/chartConfig';



class Chart extends Component {
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  constructor(props) {
    super(props);
    const pie2 = {
      option: {
        title: {
          text: 'India',
          x: 'center'
        },
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          x: 'left',
          data: ['Alumina', 'Iron', 'Coal', 'Cement', 'Oil'],
          textStyle: {
            color: CHARTCONFIG.color.text
          }
        },
        calculable: true,
        series: [
          {
            name: 'Vist source',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            color:['#3EA99F','#FFFF00','#38ACEC', '#7FE817','#FF1493'],
            data: [
              { value: 335, name: 'Alumina' },
              { value: 310, name: 'Iron' },
              { value: 234, name: 'Coal' },
              { value: 135, name: 'Cement' },
              { value: 1548, name: 'Oil' }
            ]
          }
        ]
      }
    };

    this.state = {
      visible: false,
      country: [],
      pie2: pie2,
      cargo: [],
      CargoValue: 'Cargo Name',
      CountryValue: 'Country Name',
    };
  }

  componentDidMount = async () => {
    //List of Country
    let _urlCountry = `${URL_WITH_VERSION}/master/list?t=couli&p=1&l=20`;
    let resquestCountry = await getAPICall(_urlCountry);
    let responseCountry = await resquestCountry;
    this.setState({ ...this.state, country: responseCountry['data'] }, () =>
      this.setState({ ...this.state, visible: true })
    );
    //List of CargoName
    let _urlCargo = `${URL_WITH_VERSION}/cargo/list?p=1&l=20`;
    let resquestCargo = await getAPICall(_urlCargo);
    let responseCargo = await resquestCargo;
    this.setState({ ...this.state, cargo: responseCargo['data'] }, () =>
      this.setState({ ...this.state, visible: true })
    );
  };
  getInput = () => {
    const { inputType } = this.props;
    switch (inputType) {
      case "dropdown": return this.getSelectField()
      case "number": return this.getInputNumberField()
      default: return this.getInputField()
    }
  };
  // country List
  CountryClick = (value) => {
    this.setState({ CountryValue: value.country_name });
  };
  countryList = () => {
    const { country } = this.state;
    return (
      <Menu>
        {country.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.CountryClick(x)}>{x.country_name}</a>
          </Menu.Item>
        ))}
      </Menu>
    )
  }
  // country List
  CargoClick = (value) => {
    this.setState({ CargoValue: value.charterer_name });
  };
  cargoList = () => {
    const { cargo } = this.state;
    return (
      <Menu>
        {cargo.map((x, index) => (
          <Menu.Item key={index}>
            <a onClick={() => this.CargoClick(x)}>{x.charterer_name}</a>
          </Menu.Item>
        ))}
      </Menu>
    )
  }

  render() {
    const { CountryValue, pie2, CargoValue } = this.state;
    return (
      //const Chart = () => (
      <div className="box box-default mb-4 p-3">
        <div className="box-header">
          <h4 style={{fontWeight:"600",fontSize:"21px"}}>
            Country of Discharge
            <Dropdown overlay={this.countryList()} trigger={['click']}>
              <a className="ant-dropdown-link">
                <Input size="default" placeholder={CountryValue} />
              </a>
            </Dropdown>
            <Dropdown overlay={this.cargoList()} trigger={['click']}>
              <a className="ant-dropdown-link">
                <Input size="default" placeholder={CargoValue} />
              </a>
            </Dropdown>
          </h4>
        </div>
        <div className="box-body">
          <ReactEcharts option={pie2.option} theme={"macarons"} />
          <p className="m-0 text-center">Cargo (MT) Vs Country</p>
        </div>
      </div>

    );
  }
}


export default Chart;