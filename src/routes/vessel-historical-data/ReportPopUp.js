import React, { Component } from "react";
import { Form, Input, Select, Button, Modal } from "antd";
import VoyagePerformanceReport from "../operation-reports/VoyagePerformanceReport";
import { getAPICall, openNotificationWithIcon, postAPICall } from "../../shared";
import EmissionReport from "../operation-reports/EmissionReport";
import ConsolidateReport from "../operation-reports/ConsolidateReport";

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class ReportPopUp extends Component {
  constructor(props) {
    super(props);
    // Access props and initialize state if needed
    console.log("ReportPopUp", this.props); // Access props here
  }
  state = {
    modals: {
      VoyagePerformanceReport: false,
      EmissionReport: false,
      ConsolidateReport: false,
    },
    voyageNoData: [],
    pdfData: [],
    keyNames: [],
    sumData: this.props.data,
    voyNo: '',
    vesselName: '',
    reportType: "",
    passageType: ""
  };

  showHideModal = 
  (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  getVoyageno = async (value) => {
    try{
      let url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyage--historical-list-filter?name=${value}`;
      const data = await getAPICall(url);
      this.setState((prev) => ({
      ...prev,
      pdfData: data,
      keyNames : Object.keys(data.data),
      vesselName: value
    }))
    }
    catch (error){
      console.log("error", error);
    }
  };
  
  handleSelect = (value) => {
    console.log("selected numberrrr====>>>>", value)
    const { pdfData } = this.state;
    this.setState((prev) => ({
      ...prev,
      voyageNoData: pdfData.data[value],
      voyNo: value,
    }))

  };

  onSelectPassage = (value) => {
    this.state.voyageNoData.passageType = value;
    this.setState((prev) => ({
      ...prev,
      passageType: value,
    }))
  }
  onReportType = (value) => {
    this.state.voyageNoData.reportType = value;
    this.setState((prev) => ({
      ...prev,
      reportType: value,
    }))
  }

  searchFunc = async () => {
    this.showHideModal(true, `${this.props.type}`)

    const {reportType, passageType, voyNo, vesselName, pdfData} = this.state;
    console.log(reportType, "==", passageType, "==", voyNo, "==", vesselName, pdfData);
    const report_data = {};
    report_data.report_data = pdfData.data;
    report_data.vessel_name = vesselName;
    report_data.voy_no = voyNo;
    report_data.report_type = this.props.type;
    report_data.passage_type = passageType;

    console.log("updated pdfData", report_data);
    const _method = "POST"
    const url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyagereportsave`;
    const response = await postAPICall(url, report_data, _method)
  }

  render() {
    const {voyageNoData, keyNames, sumData, voyNo} = this.state;
    console.log("voyageNoData", voyageNoData);
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row">
                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Vessel Name">
                      <Select
                      showSearch
                        placeholder="Select Vessel"
                        onSelect={(value) => this.getVoyageno(value)}
                      >
                        {this.props.data
                          .filter((element, index, array) => {
                            // Filter out only the first occurrence of each vesselName
                            return (
                              array.findIndex(
                                (item) => item.vesselName === element.vesselName
                              ) === index
                            );
                          })
                          .map((e) => (
                            <Option key={e.vesselName} value={e.vesselName}>
                              {e.vesselName}
                            </Option>
                          ))}
                      </Select>
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Voy. No">
                      <Select 
                        onChange={(value) => this.handleSelect(value)}
                        disabled={keyNames.length === 0 ? true : false} 
                        showSearch
                        value={keyNames.length === 0 ? "Choose Vessel First" : undefined}
                        placeholder={keyNames.length === 0 ? "Choose Vessel First" : "Select Voyage No."}>
                        {keyNames.map((e) => (
                          <Option value={e}>{e}</Option>
                        ))}
                      </Select>
                    </FormItem>
                  </div>

                  {/* <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Report Type">
                      <Select size="default" placeholder="Select Report Type" onSelect={(value) => this.onReportType(value)}>
                        <Option value="At Sea">At Sea</Option>
                        <Option value="In Port">In Port</Option>
                      </Select>
                    </FormItem>
                  </div> */}

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Passage Type">
                      <Select 
                      size="default" 
                      placeholder="Select Passage Type"
                      onSelect={(value) => this.onSelectPassage(value)}
                      >
                        <Option value="Laden">Laden</Option>
                        <Option value="Ballast">Ballast</Option>
                        <Option value="In Port">In Port</Option>
                      </Select>
                    </FormItem>
                  </div>
                </div>
                <Button
                  disabled={voyageNoData.length === 0 ? true : false}
                  className="btn ant-btn-primary btn-sm"
                  onClick={() =>
                    this.searchFunc()
                  }
                >
                  Search
                </Button>
              </Form>
            </div>
          </div>
        </article>
        <Modal
          style={{ top: "2%" }}
          title="Voyage Performance Report"
          open={this.state.modals["VoyagePerformanceReport"]}
          onCancel={() => this.showHideModal(false, "VoyagePerformanceReport")}
          width="95%"
          footer={null}
        >
         <VoyagePerformanceReport type="Voyage Performance Report" voyNo={this.state.voyNo} sumData={this.props.data} data={voyageNoData} />
        </Modal>
        <Modal
          style={{ top: "2%" }}
          title="Co2 Emission Report"
          open={this.state.modals["EmissionReport"]}
          onCancel={() => this.showHideModal(false, "EmissionReport")}
          width="95%"
          footer={null}
        >
         <EmissionReport type="CO2 Emission Report" voyNo={this.state.voyNo} sumData={this.props.data} data={voyageNoData} />
        </Modal>
        <Modal
          style={{ top: "2%" }}
          title="Consolidate Voyage Report"
          open={this.state.modals["ConsolidateReport"]}
          onCancel={() => this.showHideModal(false, "ConsolidateReport")}
          width="95%"
          footer={null}
        >
         <ConsolidateReport type="Consolidate Voyage Report" voyNo={this.state.voyNo} sumData={this.props.data} data={voyageNoData} />
        </Modal>
      </div>
    );
  }
}

export default ReportPopUp;
