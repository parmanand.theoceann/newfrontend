import React, { Component } from "react";
import {
  Table,
  Form,
  Input,
  Switch,
  Col,
  Row,
  Checkbox,
  Modal,
  DatePicker,
} from "antd";
import EmissionReport from "../operation-reports/EmissionReport";
import ReportPopUp from "./ReportPopUp";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
} from "../../shared";
// import SideNavdVspm from "../dynamic-vspm/sidenavdvspm";
import { CheckOutlined, CloseOutlined, EditOutlined } from "@ant-design/icons";
import moment from "moment";
const FormItem = Form.Item;
const { Column, ColumnGroup } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class VesselHistoricalData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      loading: true,
      sidebarVisible: false,
      modals: {
        EmissionReport: false,
        ReportFilter: false,
        ShipDetails1: false,
      },
      title: `vessel Name:VSL1412211\nlat:18.5204\nlng:73.8567`,
      columnVisibility: {
        distanceVessel: false,
        fuelCons: false,
        generatorEff: false,
        co2Emi: false,
      },
      data: [],
      filteredData: [],
      latlng: [],
      shipData: null,
      vessel_name: "Spring",
      loading: false,
      isShowVesselModal: false,
      isShowVesselCiiModal: false,
      isShowVesselDetailsModal: false,
    };
    this.columns = [
      {
        title: "Vessel Name",
        dataIndex: "vesselName",
      },

      {
        title: "IMO No.",
        dataIndex: "vessleImo",
      },
      {
        title: "Voyage No",
        dataIndex: "voyageNo",
      },
      {
        title: "Arrival Port",
        dataIndex: "arrivalandDeparturePort",
        render: (text, record) =>
          record.arrivalandDeparturePort.arrival == "No data"
            ? "--"
            : record.arrivalandDeparturePort.arrival,
      },
      {
        title: "Arrival Date",
        dataIndex: "arrivalandDeparturePortDate.arrival",
        render: (text, record) =>
          record.arrivalandDeparturePortDate.arrival == "No data"
            ? "--"
            : moment(record.arrivalandDeparturePortDate.arrival).format(
              "DD-MM-YYYY HH:mm:ss"
            ),
      },
      {
        title: "Departure Port",
        dataIndex: "arrivalandDeparturePort",
        render: (text, record) =>
          record.arrivalandDeparturePort.departure == "No data"
            ? "--"
            : record.arrivalandDeparturePort.departure,
      },
      {
        title: "Departure Date",
        dataIndex: "arrivalandDeparturePortDate.departure",
        render: (text, record) =>
          record.arrivalandDeparturePortDate.departure == "No data"
            ? "--"
            : moment(record.arrivalandDeparturePortDate.departure).format(
              "DD-MM-YYYY HH:mm:ss"
            ),
      },
      {
        title: "Passage type",
        dataIndex: "passageType",
        render: (text, record) => {
          const passageTypes = record.passageType;
          if (passageTypes && passageTypes.length > 0 && passageTypes != null) {
            return passageTypes.join(", ");
          }
          return "--"; // Default value if no passage type is available
        },
      },
      {
        title: "Sea cons",
        dataIndex: "seaCons",
        render: (text, record) => {
          return <span>{text ? text.toFixed(2) : "--"}</span>;
        },
      },
      // {
      //   title: 'Action',
      //   dataIndex: 'action',

      //   render: (text, record) => {
      //     return (
      //       <div className="editable-row-operations">
      //         <span className="iconWrapper">
      //         <EditOutlined onClick={() => null} type="edit" />
      //         </span>
      //       </div>
      //     )
      //   }
      // },
    ];
  }

  componentDidMount = async () => {
    this.fetchData();
  };

  fetchData = async (search = {}) => {
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }
      headers["where"] = wc;
    }
    this.setState({
      ...this.state,
      loading: true,
      data: [],
    });
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/voyage-historical-list?${qParamString}`;
    const response = await getAPICall(_url);
    const data = await response;
    // console.log(">>>>> ", data);
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    if (dataArr.length > 0) {
      console.log(dataArr);
      state["data"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.fetchData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.fetchData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    }
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  handleSwitchChange = (columnName, checked) => {
    this.setState((prevState) => ({
      columnVisibility: {
        ...prevState.columnVisibility,
        [columnName]: checked,
      },
    }));
  };

  onActionDonwload = () => { };

  handleDateRangeChange = (dates) => {
    if (dates && dates.length === 2) {
      const [startDate, endDate] = dates;

      const filteredData = this.state.data.filter((item) => {
        const arrivalDate = item.arrivalandDeparturePortDate.arrival
          ? new Date(item.arrivalandDeparturePortDate.arrival)
          : null;
        const departureDate = item.arrivalandDeparturePortDate.departure
          ? new Date(item.arrivalandDeparturePortDate.departure)
          : null;

        return (
          !arrivalDate ||
          (arrivalDate >= startDate && arrivalDate <= endDate) ||
          !departureDate ||
          (departureDate >= startDate && departureDate <= endDate)
        );
      });

      this.setState({
        filteredData: filteredData,
      });
      if (filteredData.length === 0) {
        openNotificationWithIcon("info", "Data is not available for the selected date range.")
      }
    } else {
      this.setState((prev) => ({
        ...prev,
        filteredData: this.state.data,
      }));
    }
  };

  render() {
    const {
      data,
      loading,
      search,
      pageOptions,
      sidebarVisible,
      columnVisibility,
      filteredData,
    } = this.state;
    const { RangePicker } = DatePicker;
    console.log("data ==>>>> ", data);
    const tableColumns = this.columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
      }));

    if (columnVisibility.co2Emi == true) {
      tableColumns.splice(tableColumns.length, 0, {
        title: "Total CO2 Emission",
        children: [
          {
            title: "IFO",
            dataIndex: "ifo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "VLSFO",
            dataIndex: "vlsfo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "ULSFO",
            dataIndex: "ulsfo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "MGO",
            dataIndex: "mgo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "LSMGO",
            dataIndex: "lsmgo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
        ],
      });
    }
    if (columnVisibility.distanceVessel == true) {
      tableColumns.splice(tableColumns.length, 0, {
        title: "Vessel & Distance",
        children: [
          {
            title: "Ordered Spd",
            dataIndex: "orderedSpeed",
            render: (text, record) => {
              const orderedSpeeds = record.orderedSpeed;
              if (orderedSpeeds && orderedSpeeds.length > 0) {
                return orderedSpeeds.join(", ");
              }
              return "--"; // Default value if no ordered speed is available
            },
          },
          {
            title: "Ovrall Perf. Spd",
            dataIndex: "overallPreSpeed",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Total distance",
            dataIndex: "totalDist",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "TTL steaming hrs",
            dataIndex: "steamHr",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },

          {
            title: "TTL Bad wthr Hr",
            dataIndex: "badWetherHrs",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "TTL Bad weather distance",
            dataIndex: "badWeatherDistance",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Eng dist.",
            dataIndex: "enggdist",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Ave Slip",
            dataIndex: "avgSlip",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Ave BHP",
            dataIndex: "avgBhp",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Ave. RPM",
            dataIndex: "avgRpm",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Ave air pres",
            dataIndex: "airPress",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Avg.Temp",
            dataIndex: "airTempt",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
        ],
      });
    }
    if (columnVisibility.fuelCons == true) {
      tableColumns.splice(
        tableColumns.length,
        0,
        {
          title: "Total  Cons. M/E propulsion",
          children: [
            {
              title: "IFO",
              dataIndex: "me_ifo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "VLSFO",
              dataIndex: "me_vlsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "ULSFO",
              dataIndex: "me_ulsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "MGO",
              dataIndex: "me_mgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "LSMGO",
              dataIndex: "me_lsmgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
          ],
        },
        {
          title: "Total  Cons. Generator",
          children: [
            {
              title: "IFO",
              dataIndex: "generator_ifo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "VLSFO",
              dataIndex: "generator_vlsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "ULSFO",
              dataIndex: "generator_ulsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "MGO",
              dataIndex: "generator_mgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "LSMGO",
              dataIndex: "generator_lsmgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
          ],
        },
        {
          title: "Total  Cons. Aux eng.",
          children: [
            {
              title: "IFO",
              dataIndex: "aux_ifo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "VLSFO",
              dataIndex: "aux_vlsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "ULSFO",
              dataIndex: "aux_ulsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "MGO",
              dataIndex: "aux_mgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "LSMGO",
              dataIndex: "aux_lsmgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
          ],
        },
        {
          title: "Total Port Cons",
          children: [
            {
              title: "IFO",
              dataIndex: "manoever_ifo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "VLSFO",
              dataIndex: "manoever_vlsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "ULSFO",
              dataIndex: "manoever_ulsfo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "MGO",
              dataIndex: "manoever_mgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
            {
              title: "LSMGO",
              dataIndex: "manoever_lsmgo",
              render: (text, record) => {
                return <span>{text != null ? text.toFixed(2) : "--"}</span>;
              },
            },
          ],
        }
      );
    }
    if (columnVisibility.generatorEff == true) {
      tableColumns.splice(tableColumns.length, 0, {
        title: "Generator and efficiency",
        children: [
          {
            title: "Gen 1",
            dataIndex: "genone",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Gen 2",
            dataIndex: "gentwo",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Gen 3",
            dataIndex: "genthree",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "Gen 4",
            dataIndex: "genfour",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
          {
            title: "M/E",
            dataIndex: "meKwhr",
            render: (text, record) => {
              return <span>{text != null ? text.toFixed(2) : "--"}</span>;
            },
          },
        ],
      });
    }

    return (
      <div className="body-wrapper modalWrapper">
        {console.log(
          "this.state.columnVisibility",
          this.state.columnVisibility
        )}


  
          <div className="box box-default">
            <div className="box-body">
              <div className="row"  style={{alignItems:"start"}}>
                <div className="col-md-1" style={{ height: "fit-content" }}>
                  {/* <SideNavdVspm /> */}
                </div>
                <div className="col-md-11">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body">
                        <div className="toolbar-ui-wrapper">
                          <div className="leftsection">
                            <span key="first" className="wrap-bar-menu">
                              <ul className="wrap-bar-ul">
                                <h4 className="title m-0">
                                  <span>Voyage History List</span>
                                </h4>
                              </ul>
                            </span>
                          </div>
                          <div className="rightsection">
                            <span className="wrap-bar-menu">
                              <ul className="wrap-bar-ul">
                                <li>
                                  <span
                                    className="text-bt"
                                    onClick={() =>
                                      this.showHideModal(true, "ReportFilter")
                                    }
                                  >
                                    Voyage Performance Report
                                  </span>
                                </li>
                                <li>
                                  <span
                                    className="text-bt"
                                    onClick={() =>
                                      this.showHideModal(true, "EmissionReport")
                                    }
                                  >
                                    CO2 Emission Report
                                  </span>
                                </li>
                                <li>
                                  <span
                                    className="text-bt"
                                    onClick={() =>
                                      this.showHideModal(true, "ConsolidateReport")
                                    }
                                  >
                                    Consolidate Voyage Reports
                                  </span>
                                </li>
                                <li>
                                  <span className="text-bt">
                                    <a href="#/dynamic-vspm">Back</a>
                                  </span>
                                </li>
                              </ul>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>
                  <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                      <div className="box-body">
                        <Form>
                          <Row>
                            <Col className="col-md-6">
                              <FormItem {...formItemLayout} label="Time Frame">
                                <RangePicker
                                  showTime
                                  onChange={(dates) => this.handleDateRangeChange(dates)}
                                />
                              </FormItem>
                            </Col>
                          </Row>
                          <Row
                            xs={24}
                            sm={12}
                            md={4}
                            lg={4}
                            xl={4}
                            style={{ alignItems: "center", paddingBlock: "10px" }}
                          >
                            <Col xs={24} sm={12} md={4} lg={4} xl={4}>
                              <Switch
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                                checked={columnVisibility.distanceVessel}
                                onChange={(checked) =>
                                  this.handleSwitchChange("distanceVessel", checked)
                                }
                              />
                              <span className="ml-2 text-color-theme">
                                Distance & Vessel
                              </span>
                            </Col>
                            <Col xs={24} sm={12} md={4} lg={4} xl={4}>
                              <Switch
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                                checked={columnVisibility.fuelCons}
                                onChange={(checked) =>
                                  this.handleSwitchChange("fuelCons", checked)
                                }
                              />
                              <span className="ml-2">Fuel & Cons</span>
                            </Col>
                            <Col xs={24} sm={12} md={4} lg={4} xl={4}>
                              <Switch
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                                onChange={(checked) =>
                                  this.handleSwitchChange("generatorEff", checked)
                                }
                                checked={columnVisibility.generatorEff}
                              />
                              <span className="ml-2">Generator Efficiency</span>
                            </Col>
                            <Col xs={24} sm={12} md={4} lg={4} xl={4}>
                              <Switch
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                                onChange={(checked) =>
                                  this.handleSwitchChange("co2Emi", checked)
                                }
                                checked={columnVisibility.co2Emi}
                              />
                              <span className="ml-2">Co2 Emi</span>
                            </Col>
                            {/* <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Passage">
                                  <Input size="default" defaultValue="" />
                                </FormItem>
                              </div>
                              <div className="col-md-4">
                                <FormItem {...formItemLayout} label="Vessel Type">
                                  <Input size="default" defaultValue="" />
                                </FormItem>
                              </div> */}
                          </Row>
                          <Checkbox.Group className="w-100 mt-3">
                            <Row>
                              {/* <Col xs={24} sm={12} md={4} lg={4} xl={4}>
                                  <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    unChecked
                                  />
                                  <span className="ml-2">VSPM Status</span>
                                </Col> */}
                            </Row>
                          </Checkbox.Group>
                        </Form>
                        <div
                          className="section"
                          style={{
                            width: "100%",
                            marginBottom: "10px",
                            marginTop: "10px",
                            paddingLeft: "15px",
                            paddingRight: "15px",
                          }}
                        >
                          {loading === false ? (
                            <ToolbarUI
                              routeUrl={"voyage-history-list-toolbar"}
                              optionValue={{
                                pageOptions: pageOptions,
                                columns: this.columns,
                                search: search,
                              }}
                              callback={(e) => this.callOptions(e)}
                              dowloadOptions={[
                                {
                                  title: "CSV",
                                  event: () =>
                                    this.onActionDonwload("csv", "voyage-manager"),
                                },
                                {
                                  title: "PDF",
                                  event: () =>
                                    this.onActionDonwload("pdf", "voyage-manager"),
                                },
                                {
                                  title: "XLS",
                                  event: () =>
                                    this.onActionDonwload("xls", "voyage-manager"),
                                },
                              ]}
                            />
                          ) : undefined}
                        </div>
                        <div className="row p10">
                          <div className="col-md-12">
                            {console.log("Table filteredData", filteredData)}
                            <Table
                              bordered
                              loading={loading}
                              dataSource={filteredData.length > 0 ? filteredData : data}
                              pagination={false}
                              footer={false}
                              columns={tableColumns}
                              scroll={{ x: "max-content" }}
                              rowClassName={(r, i) =>
                                i % 2 === 0
                                  ? "table-striped-listing"
                                  : "dull-color table-striped-listing"
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>




        <Modal
          style={{ top: "2%" }}
          title="Emission Report"
          open={this.state.modals["EmissionReport"]}
          onCancel={() => this.showHideModal(false, "EmissionReport")}
          width="60%"
          centered={true}
          footer={null}
        >
          <ReportPopUp type={"EmissionReport"} data={data} />
        </Modal>

        <Modal
          style={{ top: "2%" }}
          title="Voyage Performance Report"
          open={this.state.modals["ReportFilter"]}
          onCancel={() => this.showHideModal(false, "ReportFilter")}
          width="60%"
          centered={true}
          footer={null}
        >
          <ReportPopUp type={"VoyagePerformanceReport"} data={data} />
        </Modal>
        <Modal
          style={{ top: "2%" }}
          title="Consolidate Voyage Report"
          open={this.state.modals["ConsolidateReport"]}
          onCancel={() => this.showHideModal(false, "ConsolidateReport")}
          width="60%"
          centered={true}
          footer={null}
        >
          <ReportPopUp type={"ConsolidateReport"} data={data} />
        </Modal>
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={this.columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => this.callOptions(e)}
          />
        ) : null}
      </div>
    );
  }
}

export default VesselHistoricalData;
