import React, { Component } from "react";
import { Upload, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
export default class UploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <div className="col-md-6 cp">
          <span>
            <label>Name:</label>
          </span>
          <span>
            {" "}
            <input className="ant-input" placeholder="Enter Name" />
          </span>
        </div>

        <div>
          <span>
            <Upload>
              <Button>
              <UploadOutlined /> Upload File
              </Button>
            </Upload>
          </span>
        </div>
      </div>
    );
  }
}
