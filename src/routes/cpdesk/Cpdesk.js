import { Dropdown } from "antd";
import React, { Component } from "react";
import { Table, Icon, Popconfirm, Modal, Layout } from "antd";
import { Select } from "antd";

import { Button, Upload } from "antd";
import UploadFile from "./UploadFile";
const { Option } = Select;
export default class Cpdesk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isadd: true,
      isVisible: false,
    };
  }
  onCancel = () => {
    //this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  render() {
    function onChange(value) {
      console.log(`selected ${value}`);
    }

    function onBlur() {
      console.log("blur");
    }

    function onFocus() {
      console.log("focus");
    }

    function onSearch(val) {
      console.log("search:", val);
    }

    return (
      <>
        <div>
          <form>
            <div className="row cpform">
              <div className="col-md-6 cp">
                <span>
                  <label>Fixture No. :</label>
                </span>
                <span>
                  {" "}
                  <input className="ant-input" />
                </span>
              </div>

              <div className="col-md-6 cp">
                <span>
                  {" "}
                  <label>CP Date :</label>
                </span>
                <span>
                  <input className="ant-input  cpwidth" type="date" id="date" />
                </span>
              </div>

              <div className="col-md-6 cp">
                <span>
                  {" "}
                  <label>File No. :</label>
                </span>
                <span>
                  <input className="ant-input" />
                </span>
              </div>

              <div className="col-md-6 cp">
                <span>
                  {" "}
                  <label>Refrence No. :</label>
                </span>
                <span>
                  {" "}
                  <input className="ant-input" />
                </span>
              </div>

              <div className="col-md-6 cp">
                <span>
                  <label>Description :</label>
                </span>
                <span>
                  {" "}
                  <input className="ant-input" />
                </span>
              </div>
              <div className="col-md-6 cp">
                <span>
                  <label>Contract Type:</label>
                </span>
                <span>
                  {" "}
                  <Select
                    showSearch
                    style={{ width: 170 }}
                    placeholder="Contract Type"
                    optionFilterProp="children"
                    onChange={onChange}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    className="forminput"
                    onSearch={onSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    <Option value="TC">TC</Option>
                    <Option value="COA">COA</Option>
                    <Option value="Cargo">Cargo</Option>
                  </Select>
                </span>
              </div>
              <div className="col-md-6 cp">
                <span>
                  {" "}
                  <label>Cargo :</label>
                </span>
                <span>
                  {" "}
                  <input className="ant-input " />
                </span>
              </div>
              <div className="col-md-6 cp">
                <span>
                  <label>Vessel Name:</label>
                </span>
                <span>
                  {" "}
                  <input className="ant-input" />
                </span>
              </div>
            </div>

            <div className="col-md-6">
              <button className="cpbutton">Submit</button>&nbsp;&nbsp;&nbsp;
              <button className="cpbutton">Save</button>
            </div>
          </form>
          <div className="col-md-6">
            <Button
              onClick={() => this.setState({ ...this.state, isVisible: true })}
              className="cpbutton"
            >
              {" "}
              Attachment
            </Button>
          </div>
        </div>

        {this.state.isadd == true ? (
          <Modal
            title={"Upload files"}
            open={this.state.isVisible}
            width="30%"
            onCancel={this.onCancel}
            style={{ top: "10px" }}
            bodyStyle={{ height: 600, overflowY: "auto", padding: "0.5rem" }}
            footer={null}
          >
            <UploadFile
              modalCloseEvent={this.onCancel}
              showSideListBar={false}
            />
          </Modal>
        ) : (
          undefined
        )}
      </>
    );
  }
}
