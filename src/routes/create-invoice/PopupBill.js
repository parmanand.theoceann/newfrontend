import React, { Component } from 'react';
import { Form, Select, Button} from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class PopupBill extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">

              <Form>
                <div className="row p10">
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Bill To">
                    <Select defaultValue="1" size="default">
                        <Option value="1">Select</Option>
                        <Option value="2">Option 1</Option>
                        <Option value="3">Option 2</Option>
                      </Select>
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Remit To">
                    <Select defaultValue="1" size="default">
                        <Option value="1">Select</Option>
                        <Option value="2">Option 1</Option>
                        <Option value="3">Option 2</Option>
                      </Select>
                    </FormItem>
                  </div>
                  <div className="col-md-4">
                    <FormItem {...formItemLayout} label="Select Bank">
                    <Select defaultValue="1" size="default">
                        <Option value="1">Select</Option>
                        <Option value="2">Option 1</Option>
                        <Option value="3">Option 2</Option>
                      </Select>
                    </FormItem>
                  </div>
                </div>
              </Form>

           
              <div className="row p10">
                <div className="col-md-12">
                  <div className="action-btn text-right">
                    <Button className="ant-btn ant-btn-primary">Change</ Button>
                    <Button className="ant-btn ant-btn-danger ml-2">Skip</Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PopupBill;
