import React, { Component } from "react";

import {
  Layout,

  Typography,
 
} from "antd";
const { Title } = Typography;
const { Content } = Layout;

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      columns: [
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "name",
        },
        {
          title: "Vessel type",
          dataIndex: "imo_no",
          key: "imo_no",
        },
        {
          title: "DWT",
          dataIndex: "maxdwt",
          key: "maxdwt",
        },
        {
          title: "GRT",
          dataIndex: "dwt",
          key: "dwt",
        },
        {
          title: "Cargo Name",
          dataIndex: "destination_port",
          key: "destination_port",
        },
        {
          title: "Port Name",
          dataIndex: "portname",
          key: "portname",
        },
        {
          title: "Port Activity",
          dataIndex: "port_activity",
          key: "port_activity",
        },
        {
          title: "PDA/FDA type",
          dataIndex: "fda_type",
          key: "fda_type",
        },
        {
          title: "Ttl Port Cost($)",
          dataIndex: "ttl_port_cost",
          key: "ttl_port_cost",
        },
        {
          title: "Total Port days",
          dataIndex: "total_p_days",
          key: "total_p_days",
        },
        {
          title: "Days",
          dataIndex: "days",
          key: "days",
        },
        {
          title: "Bench",
          dataIndex: "bench",
          key: "bench",
        },
        {
          title: "Towage",
          dataIndex: "towage",
          key: "towage",
        },
        {
          title: "Compare to",
          dataIndex: "compare_to",
          key: "compare_to",
        },
      ],
      columnsPortCost: [
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "name",
        },
        {
          title: "Vessel type",
          dataIndex: "imo_no",
          key: "imo_no",
        },
        {
          title: "DWT",
          dataIndex: "type",
          key: "type",
        },
        {
          title: "GRT",
          dataIndex: "dwt",
          key: "dwt",
        },
        {
          title: "Cargo Name",
          dataIndex: "destination_port",
          key: "destination_port",
        },
        {
          title: "Port Name",
          dataIndex: "port_name",
          key: "port_name",
        },
        {
          title: "Port Activity",
          dataIndex: "port_activity",
          key: "port_activity",
        },
        {
          title: "PDA/FDA type",
          dataIndex: "fda_type",
          key: "fda_type",
        },
        {
          title: "Ttl Port Cost($)",
          dataIndex: "ttl_port_cost",
          key: "ttl_port_cost",
        },
        {
          title: "Total Port days",
          dataIndex: "ttl_p_days",
          key: "ttl_p_days",
        },
        {
          title: "Days",
          dataIndex: "days",
          key: "days",
        },
        {
          title: "Bench",
          dataIndex: "bench",
          key: "bench",
        },
        {
          title: "Towage",
          dataIndex: "towage",
          key: "towage",
        },
        {
          title: "Compare to",
          dataIndex: "compare_to",
          key: "compare_to",
        },
      ],
    };
  }
  componentDidMount = async () => {
    const { id } = this.props;
    try {
      const response = await fetch(
        `${process.env.REACT_APP_URL_NEW}PortDetail/list?p=1&l=10`,
        { mode: "cors" }
      );
      const data1 = await response.json();

      this.setState({
        data: data1.ports,
      });
    } catch (e) {
      console.log("e");
    }
  };
  render() {
    const { data } = this.state;
    return (
      <div className="tcov-wrapper full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="fieldscroll-wrap">
                <div className="body-wrapper">
                  {/* <article className="article">
                    <div className="box box-default">
                      <div className="box-body common-fields-wrapper">
                        <br />
                        <table style={{ width: "100%" }}>
                          <tr>
                            <td>
                              <Title level={4}>Port Cost</Title>
                            </td>
                          </tr>
                        </table>
                        <Divider />

                        <Row style={{ marginTop: 80 }}>
                          <Col span={8}>
                            <div id="chart1">
                              <Chart
                                options={{
                                  chart: {
                                    height: 350,
                                    type: "line",
                                    zoom: {
                                      enabled: false,
                                    },
                                  },
                                  dataLabels: {
                                    enabled: false,
                                  },
                                  stroke: {
                                    curve: "straight",
                                  },
                                  title: {
                                    text: "Portcost vs time vs dwt",
                                    align: "center",
                                  },
                                  grid: {
                                    row: {
                                      colors: ["#f3f3f3", "transparent"],
                                      opacity: 0.5,
                                    },
                                  },
                                  xaxis: {
                                    categories: [
                                      "Jan",
                                      "Feb",
                                      "Mar",
                                      "Apr",
                                      "May",
                                      "Jun",
                                      "Jul",
                                      "Aug",
                                      "Sep",
                                    ],
                                  },
                                }}
                                series={[
                                  {
                                    name: "Port Cost",
                                    data: [10, 41, 35, 51, 49, 62, 69, 91, 148],
                                  },
                                ]}
                                type="line"
                                height={350}
                                width="90%"
                              />
                            </div>
                          </Col>
                          <Col span={8}>
                            <div id="chart2">
                              <Chart
                                options={{
                                  chart: {
                                    width: 380,
                                    type: "pie",
                                  },
                                  labels: [
                                    "Team A",
                                    "Team B",
                                    "Team C",
                                    "Team D",
                                    "Team E",
                                  ],
                                  responsive: [
                                    {
                                      breakpoint: 480,
                                      options: {
                                        chart: {
                                          width: 200,
                                        },
                                        legend: {
                                          position: "bottom",
                                        },
                                      },
                                    },
                                  ],
                                }}
                                series={[44, 55, 13, 43, 22]}
                                type="pie"
                                height={350}
                                width="90%"
                              />
                            </div>
                          </Col>
                          <Col span={8}>
                            <Chart
                              options={{
                                chart: {
                                  id: "basic-bar",
                                },
                                xaxis: {
                                  categories: [
                                    1991,
                                    1992,
                                    1993,
                                    1994,
                                    1995,
                                    1996,
                                    1997,
                                    1998,
                                    1999,
                                  ],
                                },
                              }}
                              series={[
                                {
                                  name: "series-1",
                                  data: [30, 40, 45, 50, 49, 60, 70, 91],
                                },
                              ]}
                              height={350}
                              type="bar"
                              width="90%"
                            />
                          </Col>
                        </Row>

                        <Divider />
                        <Table
                          bordered
                          columns={this.state.columns}
                          scroll={{
                            x: 1800,
                            y: 400,
                          }}
                          loading={this.state.loading}
                          dataSource={data}
                          className="inlineTable resizeableTable"
                          size="small"
                          pagination={false}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                        />
                      </div>
                    </div>
                  </article> */}

                  <iframe
                    title="Report Section"
                    style={{ width: "100%", height: "110vh" }}
                    src="https://app.powerbi.com/view?r=eyJrIjoiNjRmZmJkMjEtZDJmMC00MWUzLTg4OGItNmIyYWFmNzcwOWUzIiwidCI6IjA0YWVkYTA0LWExYTctNDZiOC1hNTgyLTJhOTVjY2Q4ZmUzMiJ9"
                    frameborder="0"
                    allowFullScreen={true}
                  />
                </div>
              </div>
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}
