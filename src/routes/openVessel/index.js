import React, { Component } from "react";
import {
  Table,
  Spin,
  Alert,
  Input,
  Col,
  Tooltip,
  Row,
  Pagination,
  Checkbox,
  Button
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,IMAGE_PATH,
  objectToQueryStringFunc,
} from "../../shared";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { Select } from "antd";
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

const { Option } = Select;
export default class OpenVessel extends Component {
  constructor() {
    super();
    this.state = {
      lng: 41.15653,
      lat: 41.32111,
      zoom: 2,
      routeUrl: null,
      search: { searchOptions: "all", searchValue: "" },

      vesselDataArr: [],
      loading: false,
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      search: {},
      columns: [
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          width: "200px",
          paddingTop: "20px",

          render: (el) => {
            return <span className="openvessel-firstcol">{el}</span>;
          },
        },
        {
          title: "IMO No",
          dataIndex: "imo_no",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Deadweight",
          dataIndex: "Deadweight",
           width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "GRT",
          dataIndex: "GRT",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Location",
          dataIndex: "location",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Status",
          dataIndex: "status",
          width: "200px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Waiting Days",
          dataIndex: "waiting_day",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Last Port Lat",
          dataIndex: "last_port_lat",
          width:"200px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Last Port Long",
          dataIndex: "last_port_long",
          width:"200px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Next Port",
          dataIndex: "next_port",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        {
          title: "Next ETA",
          dataIndex: "next_eta",
          width:"200px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
        // {
        //   title: "Current Port Name",
        //   dataIndex: "current_port_name",
        //   render: (el) => {
        //     return <span className="openvessel-othercols">{el}</span>;
        //   },
        // },
        // {
        //   title: "Speed",
        //   dataIndex: "speed",
        //   render: (el) => {
        //     return <span className="openvessel-othercols">{el}</span>;
        //   },
        // },
        {
          title: "Destination",
          dataIndex: "destination",
          width: "150px",
          render: (el) => {
            return <span className="openvessel-othercols">{el}</span>;
          },
        },
      ],
    };
    this.mapContainer = React.createRef();
  }

  componentDidMount = async () => {
    this.maprender();
    this.setState({
      loading: true,
    });
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      `${process.env.REACT_APP_URL}v1/` + "vessel/live/data?p=" + 0,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          vesselDataArr: result.data,
          loading: false,
        });
      })
      .catch((error) => console.log(error));
  };

  //   getTableData = async (search = {}) => {
  //     const { pageOptions } = this.state;
  //     let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
  //     let headers = {};
  //     if (
  //       search &&
  //       search.hasOwnProperty("searchValue") &&
  //       search.hasOwnProperty("searchOptions") &&
  //       search["searchOptions"] !== "" &&
  //       search["searchValue"] !== ""
  //     ) {
  //       let wc = {};
  //       if (search["searchOptions"].indexOf(";") > 0) {
  //         let so = search["searchOptions"].split(";");
  //         wc = { OR: {} };
  //         so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
  //       } else {
  //         wc[search["searchOptions"]] = { l: search["searchValue"] };
  //       }

  //       headers["where"] = wc;
  //     }
  //     // console.log(search);
  //     this.setState({
  //       ...this.state,
  //       loading: true,
  //       data: [],
  //     });
  //     let qParamString = objectToQueryStringFunc(qParams);
  //     let _url = `${process.env.REACT_APP_URL}v1/vessel/live/data?${qParamString}`;

  //     const response = await getAPICall(_url, headers);
  //     const data = await response;
  //     let state = { loading: false };
  //     state["data"] = data && data.data ? data.data : [];
  //     // console.log("**** data.total_rows;", data.total_rows);
  //     let totalRows = data.total_rows;
  //     this.setState({
  //       ...this.state,
  //       ...state,
  //       pageOptions: {
  //         pageIndex: pageOptions.pageIndex,
  //         pageLimit: pageOptions.pageLimit,
  //         totalRows: totalRows,
  //       },
  //       loading: false,
  //     });
  //   };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  getSearchtableData = async (q) => {
    if (q.length > 3) {
      let fArr = this.state.vesselDataArr.filter((item) => {
        if (
          item["vessel_name"].toLowerCase().includes(q.toLowerCase()) ||
          item["imo_no"].includes(q.toLowerCase())
        ) {
          if (
            item["last_pos_lat"].toLowerCase() !== "none" &&
            item["last_pos_lon"].toLowerCase() !== "none"
          ) {
            return item;
          }
        }
      });

      this.setState({
        ...this.state,
        vesselDataArr: fArr,
      });
    }
  };

  onSearch = (e) => {
    const { name, value } = e.target;

    if (value.length > 3) {
      this.getSearchtableData(value);
    } else if (value == " ") {
      window.location.reload(true);
    }
  };

  handleChange = (value) => {};

  onClickButton = (val, type) => {
    const { vesselDataArr } = this.state;

    if (type === "searchOptions") {
      let search = this.state.search;
      search["searchOptions"] = val;
      this.setState({ ...this.state, search: search });
    } else if (type === "searchValue") {
      let search = this.state.search;
      search["searchValue"] = val;
      if (!search["searchOptions"] || search["searchOptions"] === "") {
        search["searchOptions"] =
          vesselDataArr["isLeftSection"][0]["defaultDropdownValue"];
      }

      this.setState({ ...this.state, search: search }, () => {
        this.props.callback(this.state.search);
      });
    } else {
      let opt = {
        actionName: type,
        actionVal: val,
        vesselDataArr: vesselDataArr,
      };
      if (type === "reset-serach") {
        this.setState(
          { ...this.state, search: { searchOptions: "all", searchValue: "" } },
          () => {
            if (typeof this.props.callback === "function") {
              this.props.callback(opt);
            }
          }
        );
      } else if (typeof this.props.callback === "function") {
        this.props.callback(opt);
      }
    }
  };

  maprender() {
    let features = [];
    if (this.props && this.props.data != undefined) {
      this.props.data.length > 0 &&
        this.props.data.map((obj) => {
          let d = obj && obj.length > 0 ? obj[0] : null;
          if (d != null) {
            features.push({
              type: "Feature",
              geometry: {
                type: "Point",
                coordinates: [d.longitude, d.latitude],
              },
              properties: {
                title: d.port,
              },
            });
          }
        });
    }
    let that = this;
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "LineString",
            properties: {},
            coordinates: [],
          },
        },
      ],
    };
    const map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
      center: [this.state.lng, this.state.lat],
      zoom: this.state.zoom,
    });
    map.on("load", () => {
      map.addSource("LineString", {
        type: "geojson",
        data: geojson,
      });
      map.on("click", "places", (e) => {});
      map.loadImage(
        // "https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png",
        <img src={IMAGE_PATH+"icons/harbor.png"} height="50" />,
        (error, image) => {
          if (error) throw error;
          map.addImage("custom-marker", image);
          map.addSource("places", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: features,
            },
          });

          // Add a symbol layer
          map.addLayer({
            id: "places",
            type: "symbol",
            source: "places",
            layout: {
              "icon-image": "custom-marker",
              // get the title name from the source's "title" property
              "text-field": ["get", "title"],
              "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
              "text-offset": [0, 1.25],
              "text-anchor": "top",
            },
          });
        }
      );
    });
  }
// page start
  render() {
    const { loading, columns, vesselDataArr, pageOptions } = this.state;

    const { lng, lat, zoom } = this.state;
    return (
      <div>
        {/*         
        {loading === false ? (
          <ToolbarUI
            routeUrl={"track-my-fleet"}
            optionValue={{
              pageOptions: pageOptions,
              columns: columns,
              // search: search,
            }}
            callback={(e) => this.callOptions(e)}
          />
        ) : (
          undefined
        )} */}
        <Row gutter={16}>
         
          <div>
             <h4 className="openvessel-head">Vessel Open Position
             <hr></hr></h4>
             <div className="openvessel-form">   
                <label>Vessel Name :</label>
               
                      <Select
                className="openvessel-select openvessel-select3"
                placeholder="Select Vessel name"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>


                &nbsp; &nbsp; &nbsp; &nbsp; <label>IMO No:</label>
                
                  <Select
                className="openvessel-select openvessel-select3"
                placeholder="Search IMO No"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>


                 &nbsp; &nbsp; &nbsp; &nbsp;  <label>Deadweight : &nbsp; &nbsp; </label>
                   
                       &nbsp; &nbsp;<Select
                className="openvessel-select openvessel-select3"
                placeholder="Select deadweight"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>

                    
                    </div>
                   <div className="openvessel-form">  

                 <label>GRT :</label>
                 &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; 
               
                         <Select
                className="openvessel-select openvessel-select3"
                placeholder=" Select GRT"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>


                &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<label>Next Port: </label>
                 
                       <Select
                className="openvessel-select openvessel-select3"
                placeholder="Select Next Port"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>


                &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<label>Destination Port :&nbsp;</label>
                 
                       <Select
                className="openvessel-select openvessel-select3"
                placeholder="Select destination port"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>

                      </div>   
                        <div className="openvessel-form"> 
                
                        <label>Status : &nbsp; &nbsp; &nbsp;</label>
                 
                          &nbsp; &nbsp;<Select
                className="openvessel-select openvessel-select3"
                placeholder="Status"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>

                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<label>Location :</label>
               &nbsp;<Select
                className="openvessel-select openvessel-select3 "
                placeholder="Select location"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option></Select>
                      
               <div className="opentable-btn">
            <Button icon="search" key={"btn"}>Search</Button>
            </div> 
          </div>   
         </div>


       
        </Row>
        <div style={{ display: "flex" }}>
          <Row gutter={16}>
           
            <Tooltip title="Please enter a value  to search.">
              <Input
                placeholder="All"
                onChange={(ev) => this.onSearch(ev)}
                className="openvessel-input"
              />
            </Tooltip>
          
            
            <Tooltip title="Please Select a value.">
              <Select
                className="openvessel-select openvessel-select2 "
                placeholder="Search"
                size="large"
                onChange={this.handleChange}
              >
             
                 <Button icon="search" key={"btn"}>Search</Button>
                <Option key="vessel_name">Vessel Name</Option>
                <Option key="imo_no">Imo No</Option>
                <Option key="deadweight">Deadweight</Option>
                <Option key="GRT">GRT</Option>
                <Option key="next_port">Next Port</Option>
                <Option key="destination_port">Destination Port</Option>
                <Option key="status">Status</Option>
                <Option key="location">Location</Option>
              </Select>
            </Tooltip>
       

            <Row
            type="flex"
            align="middle"
            justify="center"
            className="openvessel-flex-middle "
          >
            <span>
              <b>Total :</b> {vesselDataArr.length}
            </span>
            <Pagination
              simple
              defaultCurrent={0}
              total={vesselDataArr.length}
              defaultPageSize={50}
              onChange={(ev) => this.onClickButton(ev, "pageIndex")}
            />
            <Select
              defaultValue={10}
              style={{ width: 120 }}
              onChange={(ev) => this.onClickButton(ev, "pageLimit")}
            >
              <Option value="10">10 Per Page</Option>
              <Option value="20">20 Per Page</Option>
              <Option value="30">30 Per Page</Option>
              <Option value="40">40 Per Page</Option>
            </Select>
          </Row>

          
          </Row>

      
        </div>
          <br></br>


        <Table
          columns={columns}
          dataSource={vesselDataArr}
          pagination={{
            pageSize: 10,
            showSizeChanger: true,
            total: vesselDataArr.length,
          }}
          bordered
          scroll={{
            x: 1200,
            y: 400,
          }}
          loading={loading}
          className="inlineTable resizeableTable openvessel-box"
          size="small"
          rowClassName={(r, i) =>
            i % 2 === 0
              ? "table-striped-listing"
              : "dull-color table-striped-listing"
          }
        /> 
           {/* <Row
            type="flex"
            align="middle"
            justify="center"
            className="openvessel-flex-middle "
          >
            <span>
              <b>Total :</b> {vesselDataArr.length}
            </span>
            <Pagination
              simple
              defaultCurrent={0}
              total={vesselDataArr.length}
              defaultPageSize={50}
              onChange={(ev) => this.onClickButton(ev, "pageIndex")}
            />
            <Select
              defaultValue={10}
              style={{ width: 120 }}
              onChange={(ev) => this.onClickButton(ev, "pageLimit")}
            >
              <Option value="10">10 Per Page</Option>
              <Option value="20">20 Per Page</Option>
              <Option value="30">30 Per Page</Option>
              <Option value="40">40 Per Page</Option>
            </Select>
          </Row> */}

        <div>
        <div ref={this.mapContainer} className="map-container openvessel-map" />
        </div>
      
      </div>
    );
  }
}
