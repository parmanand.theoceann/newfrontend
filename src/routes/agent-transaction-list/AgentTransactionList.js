

import React, { Component } from 'react';
import { Button, Table,  Popconfirm, Modal, Input } from 'antd';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon, ResizeableTitle } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import Tde from '../tde/Tde';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import {EditOutlined,DeleteOutlined} from '@ant-design/icons';
class AgentTransactionList extends Component {
  constructor(props) {
    super(props);
    let tableHeaders = Object.assign([], FIELDS && FIELDS['agent-transcation-list'] ? FIELDS['agent-transcation-list']["tableheads"] : [])
    tableHeaders.push({
      title: 'Action',
      key: 'action',
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" 
            onClick={(e) => 
              //this.redirectToAdd(e, record.id)
              openNotificationWithIcon('error','Not able to open.',5)
            }
            >
           <EditOutlined />

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
              <DeleteOutlined />

              </Popconfirm>
            </span>
          </div>
        )
      }
    })

    this.state = {
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {}
    }
  }

  componentDidMount = () => {
    this.getTableData();
  }

  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    let headers = { "order_by": { "id": "desc" } };

    if (search && search.hasOwnProperty('searchValue') && search.hasOwnProperty('searchOptions') && search['searchOptions'] !== '' && search['searchValue'] !== '') {
      let wc = {}
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { "OR": {} };
        so.map(e => wc['OR'][e] = { "l": search['searchValue'] });
      } else {
        wc[search['searchOptions']] = { "l": search['searchValue'] }
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: []
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/port-call/pelist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = (data && data.data ? data.data : []);
    let state = { loading: false }
    if (dataArr.length > 0 && (totalRows > this.state.responseData.length)) {
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: { pageIndex: pageOptions.pageIndex, pageLimit: pageOptions.pageLimit, totalRows: totalRows },
      loading: false
    });
  }




  redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${id}`);
      const respData = await response['data'];
      this.setState({ ...this.state, isAdd: false, formDataValues: respData }, () => this.setState({ ...this.state, isVisible: true }));
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  }


  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  }

  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/tde/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })
  }

  callOptions = (evt) => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { 'searchOptions': evt['searchOptions'], 'searchValue': evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);;

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
      })
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData()
      })
    }
  }

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }

  render() {
    const { columns, loading, responseData, pageOptions, search, isAdd, isVisible, formDataValues, sidebarVisible } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span>Agent Transaction List</span></h4>
                </div>
              </div>
              <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                {
                  loading === false ?
                    <ToolbarUI routeUrl={'vendor-transaction-toolbar'} 
                      optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }} 
                      callback={(e) => this.callOptions(e)}
                      dowloadOptions={[
                        {title:'CSV', event: () => this.onActionDonwload('csv', 'tde')},
                        {title:'PDF', event: () => this.onActionDonwload('pdf', 'tde')},
                        {title:'XLS', event: () => this.onActionDonwload('xlsx', 'tde')}
                      ]}
                    />
                    : undefined
                }
              </div>
              <div>
                <Table
                  rowKey={record => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  scroll={{ x: 'max-content' }}
                  // scroll={{ y: 370 }}
                  components={this.components}
                  columns={tableColumns}
                  size="small"
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                />
              </div>
            </div>
          </div>
        </article>

        {
          isVisible === true ?
            <Modal
              className="page-container"
              style={{ top: '2%' }}
              title="TDE"
              open={isVisible}
              onCancel={this.onCancel}
              width="95%"
              footer={null}
            >
              <Tde receivablePayableType="78" invoiceType="TDE" formData={formDataValues} isEdit={true}  saveUpdateClose={() => this.setState({ isVisible: false })} />
            </Modal>
          : undefined }

        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptions(e)} /> : null
        } 
      </div>
    )
  }
}

export default AgentTransactionList;
