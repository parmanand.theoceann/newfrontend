import React, { Component } from 'react';
import { Form, Input, Select, Modal } from 'antd';
import { Gantt } from '@dhtmlx/trial-react-gantt';
import VesselScheduleReport from '../operation-reports/VesselScheduleReport';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const scales = [
  { unit: 'month', step: 1, format: 'MMMM yyy' },
  { unit: 'day', step: 1, format: 'd' },
];

const columns = [
  { name: 'text', label: 'Vessel', width: '100%' },
  { name: 'company', label: 'Company', width: '100%' },
];

const tasks = [
  {
    id: 1,
    start_date: '2020-11-06',
    duration: 8,
    text: 'MV CALYPSO',
    company: 'ABC',
  },
  {
    id: 2,
    start_date: '2020-11-06',
    duration: 4,
    text: 'NANOS',
    company: 'DEFG',
  },

  {
    id: 3,
    start_date: '2020-12-06',
    duration: 2,
    text: 'NANOS',
    company: 'XYZ',
  },

  {
    id: 4,
    start_date: '2020-11-04',
    duration: 3,
    text: 'NANOS',
    company: 'DEFG',
  },
];

const links = [{ source: 2, target: 1, type: 0 }];

class VesselSchedule extends Component {
  state = {
    modals: {
      VesselScheduleReport: false,
    },
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <h4>Vessel Schedule</h4>
                    </ul>
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span
                          className="text-bt"
                          onClick={() => this.showHideModal(true, 'Report')}
                        >
                          Reports
                        </span>
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
              <hr />

              <div className="row p10">
                <div className="col-md-12">
                  <Form>
                    <div className="row">
                      <div className="col-md-3">
                        <FormItem {...formItemLayout} label="Date From/TO">
                          <Input
                          key="search"
                          type="Date"
                          t_date=""
                          name="search"
                          placeholder="Filter"
                          //value={searchField}
                          /*onChange={(field) => this.onSearchField(field.target.value)}*/
                          />
                        </FormItem>
                      </div>
                      <div className="col-md-3">
                        <FormItem {...formItemLayout} label="Vessel Name">
                          <Input size="default" />
                        </FormItem>
                      </div>
                      <div className="col-md-3">
                        <FormItem {...formItemLayout} label="Vessel Type">
                          <Input size="default" />
                        </FormItem>
                      </div>
                      <div className="col-md-3">
                        <FormItem {...formItemLayout} label="Status">
                          <Select defaultValue="1">
                            <Option value="1">FIXED</Option>
                            <Option value="2">Schedule</Option>
                            <Option value="3">Delivered</Option>
                            <Option value="4">Commence</Option>
                            <Option value="5">Completed</Option>
                            <Option value="6">TCI</Option>
                            <Option value="7">TCO</Option>
                            <Option value="8">VCI-VCO</Option>
                          </Select>
                        </FormItem>
                      </div>
                    </div>
                  </Form>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <div className="border p-2 wx-default">
                    <Gantt scales={scales} columns={columns} tasks={tasks} links={links} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
        <Modal
          style={{ top: '2%' }}
          title="Reports"
         open={this.state.modals['VesselScheduleReport']}
          onCancel={() => this.showHideModal(false, 'VesselScheduleReport')}
          width="95%"
          footer={null}
        >
          <VesselScheduleReport />
        </Modal>
      </div>
    );
  }
}

export default VesselSchedule;
