import React from 'react'
import { Table, Modal, Popconfirm, Col, Row, Select } from "antd";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import LineChart from "../dashboard/charts/LineChart";
import PieChart from "../dashboard/charts/PieChart";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";
const PdaListGraph = () => {



  const ClusterDataxAxis = [
    "Emma Maers",
    "Calma123",
    "Ocean Cobra",
    "SOUTH LOYALTY",
    "CS HANA",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amount",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "BUNK-PE-00259",
    "BUNK-PE-00257",
    "BUNK-PE-00256",
    "BUNK-PE-00255",
    "BUNK-PE-00249",
  ];
  const PieChartData = [
    { value: 20, name: "Posted" },
    { value: 10, name: "Verified" },
    { value: 5, name: "Prepared" },
    { value: 20, name: "Approved" },
  ];

  const StackHorizontalChartyAxis2 = [
    "TCE09-23-01082",
    "TCE09-23-01054",
    "TCE09-23-00909",
    "TCE09-23-00928",
  ];
  const StackHorizontalChartSeries2 = [
    {
      name: "PDA Amount",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
  ];
  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];
  return (
    <>
    <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
    <Col
      xs={24}
      sm={8}
      md={8}
      lg={3}
      xl={3}
      style={{ borderRadius: "15px", padding: "8px" }}
    >
      <div
        style={{
          background: "#1D406A",
          color: "white",
          borderRadius: "15px",
        }}
      >
        <p>Total Vessels</p>
        <p>300</p>
      </div>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={3}
      lg={3}
      xl={3}
      style={{ borderRadius: "15px", padding: "8px" }}
    >
      <div
        style={{
          background: "#1D406A",
          color: "white",
          borderRadius: "15px",
        }}
      >
        <p>Total Amount</p>
        <p>300 $</p>
      </div>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={3}
      lg={3}
      xl={3}
      style={{ borderRadius: "15px", padding: "8px" }}
    >
      <div
        style={{
          background: "#1D406A",
          color: "white",
          borderRadius: "15px",
        }}
      >
        <p>Total Invoice</p>
        <p>240</p>
      </div>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={3}
      lg={3}
      xl={3}
      style={{ textAlign: "start", padding: "8px" }}
    >
      <p style={{ margin: "0" }}>Vessel Name</p>
      <Select
        placeholder="All"
        optionFilterProp="children"
        options={[
          {
            value: "PACIFIC EXPLORER",
            label: "OCEANIC MAJESTY",
          },
          {
            value: "OCEANIC MAJESTY",
            label: "OCEANIC MAJESTY",
          },
          {
            value: "CS HANA",
            label: "CS HANA",
          },
        ]}
      ></Select>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={6}
      lg={3}
      xl={3}
      style={{ textAlign: "start", padding: "8px" }}
    >
      <p style={{ margin: "0" }}>Invoice No</p>
      <Select
        placeholder="All"
        optionFilterProp="children"
        options={[
          {
            value: "TCE02-24-01592",
            label: "TCE02-24-01592",
          },
          {
            value: "TCE01-24-01582",
            label: "TCE01-24-01582",
          },
          {
            value: "TCE01-24-01573",
            label: "TCE01-24-01573",
          },
        ]}
      ></Select>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={6}
      lg={3}
      xl={3}
      style={{ textAlign: "start", padding: "8px" }}
    >
      <p style={{ margin: "0" }}>Account Type</p>
      <Select
        placeholder="Payable"
        optionFilterProp="children"
        options={[
          {
            value: "Payable",
            label: "Payable",
          },
          {
            value: "Receivable",
            label: "Receivable",
          },
        ]}
      ></Select>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={6}
      lg={3}
      xl={3}
      style={{ textAlign: "start", padding: "8px" }}
    >
      <p style={{ margin: "0" }}>Status</p>
      <Select
        placeholder="PREPARED"
        optionFilterProp="children"
        options={[
          {
            value: "PREPARED",
            label: "PREPARED",
          },
          {
            value: "APPROVED",
            label: "APPROVED",
          },
          {
            value: "POSTED",
            label: "POSTED",
          },
          {
            value: "VERIFIED",
            label: "VERIFIED",
          },
        ]}
      ></Select>
    </Col>
    <Col
      xs={24}
      sm={12}
      md={6}
      lg={3}
      xl={3}
      style={{ textAlign: "start", padding: "8px" }}
    >
      <p style={{ margin: "0" }}>Date To</p>
      <Select
        placeholder="2024-02-18"
        optionFilterProp="children"
        options={[
          {
            value: "2024-01-26",
            label: "2024-01-26",
          },
          {
            value: "2023-11-16",
            label: "2023-11-16",
          },
          {
            value: "2023-11-17",
            label: "2023-11-17",
          },
        ]}
      ></Select>
    </Col>
  </Row>
  <Row gutter={16}>
    <Col span={12}>
      <ClusterColumnChart
        Heading={"Total Amount Per Vessel"}
        ClusterDataxAxis={ClusterDataxAxis}
        ClusterDataSeries={ClusterDataSeries}
        maxValueyAxis={"350"}
      />
    </Col>
    <Col span={12}>
      <LineChart
        LineCharSeriesData={LineCharSeriesData}
        LineCharxAxisData={LineCharxAxisData}
        Heading={"Total Amount Per Invoice No."}
      />
    </Col>
  </Row>
  <Row gutter={16}>
    <Col span={12}>
      <PieChart
        PieChartData={PieChartData}
        Heading={"Total Pda Amount on the basis of Status"}
      />
    </Col>
    <Col span={12}>
      <StackHorizontalChart
        Heading={"Total Pda Amount Per Voyage Number"}
        StackHorizontalChartyAxis={StackHorizontalChartyAxis2}
        StackHorizontalChartSeries={StackHorizontalChartSeries2}
      />
    </Col>
  </Row>
    </>
  )
}

export default PdaListGraph
