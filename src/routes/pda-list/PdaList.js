import React, { Component } from "react";
import { Table, Modal, Popconfirm, Col, Row, Select } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  apiDeleteCall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  useStateCallback,
} from "../../shared";
import BunkerPe from "../voyage-list/components/BunkerPe";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { FIELDS } from "../../shared/tableFields";

import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useRef } from "react";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import LineChart from "../dashboard/charts/LineChart";
import PieChart from "../dashboard/charts/PieChart";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";
import PdaListGraph from './PdaListGraph'
const PdaList = (props) => {
  const [state, setState] = useStateCallback({
    sidebarVisible: false,
    isshowportexpensedetails: false,
    columns: [],
    responseData: null,
    loading: true,
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    editportexid: null,
    voyID: props.voyID || null,
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });

  const navigate = useNavigate();
  const tableref = useRef();

  useEffect(() => {
    const tableAction = {
      title: "Action",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: "70",

      render: (el, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper edit"
              onClick={(ev) => port_expenses_detail(true, record.id)}
            >
              <EditOutlined />
            </span>

            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["pda-list"] ? FIELDS["pda-list"]["tableheads"] : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData(state.voyID);
    });
  }, []);

  const port_expenses_detail = async (showportexpensedetails, voyid = null) => {
    if (voyid !== null) {
      setState((prevState) => ({
        ...prevState,
        editportexid: voyid,
        isshowportexpensedetails: showportexpensedetails,
        voyId: voyid,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isshowportexpensedetails: showportexpensedetails,
      }));
    }
  };

  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/pdalist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const onRowDeletedClick = (id) => {
    const { voyID } = state;

    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/delete?e=${id}`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(voyID);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const handleclosePEDetail = (showPeDetail) => {
    setState({ ...state, isshowportexpensedetails: showPeDetail }, () =>
      getTableData()
    );
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const {
    isshowportexpensedetails,
    responseData,
    loading,
    pageOptions,
    search,
    columns,
    editportexpenseData,
    sidebarVisible,
    editportexid,
  } = state;

  const tableColumns = state.columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "Emma Maers",
    "Calma123",
    "Ocean Cobra",
    "SOUTH LOYALTY",
    "CS HANA",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amount",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "BUNK-PE-00259",
    "BUNK-PE-00257",
    "BUNK-PE-00256",
    "BUNK-PE-00255",
    "BUNK-PE-00249",
  ];
  const PieChartData = [
    { value: 20, name: "Posted" },
    { value: 10, name: "Verified" },
    { value: 5, name: "Prepared" },
    { value: 20, name: "Approved" },
  ];

  const StackHorizontalChartyAxis2 = [
    "TCE09-23-01082",
    "TCE09-23-01054",
    "TCE09-23-00909",
    "TCE09-23-00928",
  ];
  const StackHorizontalChartSeries2 = [
    {
      name: "PDA Amount",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
  ];
  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];
  return (
    <>
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title" />
              </div>
            </div>
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {loading === false ? (
                <ToolbarUI
                  routeUrl={"pda-list-toolbar"}
                  optionValue={{
                    pageOptions: pageOptions,
                    columns: columns,
                    search: search,
                  }}
                  showGraph={showGraphs}
                  callback={(e) => callOptions(e)}
                  // here pdf is downloading wrong  may be backend problem.
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "pda-list"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "pda-list"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "pda-list"),
                    },
                  ]}
                />
              ) : undefined}
            </div>
            <div>
              <Table
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                columns={tableColumns}
                scroll={{ x: "max-content" }}
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? "table-striped-listing"
                    : "dull-color table-striped-listing"
                }
              />
            </div>
          </div>
        </div>
      </article>

      {sidebarVisible ? (
        <SidebarColumnFilter
          columns={columns}
          sidebarVisible={sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
      {isshowportexpensedetails ? (
        <Modal
          className="page-container"
          style={{ top: "4%" }}
          title="Port Expense Summary"
          open={isshowportexpensedetails}
          // onOk={handleOk}
          onCancel={() => port_expenses_detail(false)}
          width="90%"
          footer={null}
        >
          <BunkerPe
            id={editportexid}
            modalCloseEvent={(showPeDetail) =>
              handleclosePEDetail(showPeDetail)
            }
          />
        </Modal>
      ) : undefined}
      <Modal
        title="PDA Dashboard"
        open={state.isGraphModal}
        //  onOk={handleOk}
        width="90%"
        onCancel={handleCancel}
        footer={null}
      >
        <PdaListGraph/>
      </Modal>
    </>
  );
};

export default PdaList;
