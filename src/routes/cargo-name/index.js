import React, { useEffect, useState } from "react";
import { Table, Button, Popconfirm, Spin, Alert } from "antd";
import { EditOutlined } from "@ant-design/icons";

import URL_WITH_VERSION, {
  CALL_MASTER_API,
  openNotificationWithIcon,
  getAPICall,
  ResizeableTitle,
  useStateCallback,
  objectToQueryStringFunc,
} from "../../shared";
import { CARGO_LIST } from "../../shared/constants";
import { FIELDS } from "../../shared/tableFields";
import ModalAlertBox from "../../shared/ModalAlertBox";
import AddCargoName from "./add-cargo-name";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
const INITIAL_MODAL = {
  modalStatus: false,
  modalHeader: null,
  modalBody: null,
  modalFooter: null,
};

const CargoName = () => {
  const [state, setState] = useState({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    selectedValue: "all",
    formDataValues: null,
    pageLimit: 10,
    pageIndex: 1,
    totalRows: 0,
    modal: INITIAL_MODAL,
    isView: false,
    typesearch: {},
    donloadArray: [],
    sidebarVisible: false, // column filtering show/hide
  });

  useEffect(() => {
    const tableAction = {
      title: "Action ",
      key: "action",
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper edit"
              onClick={(ev) => editTableData(record.id)}
            >
              <EditOutlined />  
            </span>
            {/* <span className="iconWrapper cancel">
            <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
             <DeleteOutlined /> />
            </Popconfirm>
          </span> */}
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["cargo-name"] ? FIELDS["cargo-name"]["tableheads"] : []
    );
    let indexOf = tableHeaders.findIndex((item) => item.key === "short_name");
    if (indexOf > -1) {
      tableHeaders[indexOf] = {
        ...tableHeaders[indexOf],
        render: (text, record) => (
          <a href="#" onClick={() => onRowFieldClick(record.id)}>
            {text}
          </a>
        ),
      };
    }
    tableHeaders.push(tableAction);

    setState(
      (prevState) => ({ ...prevState, columns: tableHeaders }),
      // () => {
      //   getTableData();
      // }
    );
    getTableData();
  }, []);



  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let _search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : state.typesearch;

    if (
      _search &&
      _search.hasOwnProperty("searchValue") &&
      _search.hasOwnProperty("searchOptions") &&
      _search["searchOptions"] !== "" &&
      _search["searchValue"] !== ""
    ) {
      let wc = {};
      _search["searchValue"] = _search["searchValue"].trim();

      if (_search["searchOptions"].indexOf(";") > 0) {
        let so = _search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: _search["searchValue"] }));
      } else {
        wc[_search["searchOptions"]] = { l: _search["searchValue"] };
      }

      headers["where"] = wc;
      state.typesearch = {
        searchOptions: _search.searchOptions,
        searchValue: _search.searchValue,
      };
    }
    setState((prevState) => ({
      ...prevState,
      loading: true,
      // responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _state = {},
      dataArr = [],
      totalRows = 0;
    //let _url = `${URL_WITH_VERSION}/address/list?${qParamString}`;

    let _url = `${URL_WITH_VERSION}/master/list?t=${CARGO_LIST}&${qParamString}`;

    const response = await getAPICall(_url, headers);
    const respData = await response;
    let donloadArr = [];
    if (respData && respData.data) {
      totalRows = respData?.total_rows ? respData.total_rows : 0;
      dataArr = respData.data;
      if (dataArr.length > 0 && totalRows > 0) {
        dataArr.forEach((d) => donloadArr.push(d["id"]));
        _state["responseData"] = dataArr;
      }
    }

    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const onRowFieldClick = (id) => {
    setState(
      (prevState) => ({
        ...prevState,
        isView: true,
      }),
      () => editTableData(id)
    );
  };

  /*
 const onRowDeletedClick = (id) => {
    let postData = {};
    if (id) {
      postData["id"] = id;

      CALL_MASTER_API('delete', CARGO_LIST, postData, null, (data) => {
        if (data && data.data) {
          openNotificationWithIcon('success', data.message)
          setState((prevState)=>({
            ...prevState,
            responseData: []
          }), () => {
            getTableData()
          });
        } else {
          openNotificationWithIcon('error', data.message)
        }
      })
    }
  }
*/

  const editTableData = async(id) => {
    
    let headers = { where: { id: id } };
    
    if (CARGO_LIST) {
      setState(prevState => ({...prevState, loading: true}))

      CALL_MASTER_API("get", CARGO_LIST, null, null, headers, (respData) => {
        let _state = { loading: false },
          edit = false;

        if (respData.total_rows > 0) {
          const updatedFormDataValues = respData["data"][0];
          edit = true;
        
          setState((prev)=>({...prev, loading:false, edit:true, "formDataValues":updatedFormDataValues}))
         
        }

        
        
      });
    }
  };

  


  useEffect(()=>{
  if(state.edit){
    onShowModal();
  }
  },[state.loading])

  const onShowModal = () => {
    const { isView, formDataValues } = state;
    
    let modalBody = () => (
      <AddCargoName
        formDataValues={formDataValues}

        modalCloseEvent={(e) => onCancel(e)}
        isView={isView}
      />
      
    );
    let modal = {
      modalStatus: true,
      modalHeader: "Cargo Information",
      modalBody,
    };
    setState(prevState => ({ ...prevState, modal }),
    ()=>setState((prevState)=>({ ...prevState,modalStatus: false})))
  };

  const onCancel = (e) =>
    setState(prevState => ({
        ...prevState,
        modal: INITIAL_MODAL,
        responseData: [],
        formDataValues: null,
        isView: false,
        edit:false
      }),
       getTableData()
    );

  const callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: search,
          pageOptions: pageOptions,
        }),
        
          getTableData(evt)
        
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: {},
          pageOptions: pageOptions,
          typesearch: {},
        }),
        
          getTableData()
        
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      setState({
        ...state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        );
        getTableData()
    }
  };

  const handleResize =(index) =>(e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    modal,
    pageOptions,
    search,
    sidebarVisible,
  } = state;

  const { modalStatus, modalBody, modalHeader, modalFooter } = modal;
  // column filtering show/hide
  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title">
                  <span>Cargo Listing</span>
                </h4>
              </div>
              {/* <div className="action-btn">
                <Button type="primary" onClick={() => this.onShowModal()}>New Cargo</Button>
              </div> */}
            </div>
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {loading === false ? (
                <ToolbarUI
                  routeUrl={"new-cargo-list-toolbar"}
                  optionValue={{
                    pageOptions: pageOptions,
                    columns: columns,
                    search: search,
                  }}
                  callback={(e) => callOptions(e)}
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "cargos-list"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "cargos-list"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "cargos-list"),
                    },
                  ]}
                />
              ) : undefined}
            </div>

            <div>
              <Table
                className="inlineTable resizeableTable"
                bordered
                components={components}
                columns={tableColumns}
                // size="small"
                scroll={{ x: "max-content" }}
                rowKey={(record, index) =>
                  (record.id ? record.id : "") + "-" + index
                }
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? "table-striped-listing"
                    : "dull-color table-striped-listing"
                }
              />
            </div>
          </div>
        </div>
      </article>
      {modalStatus ? (
        <ModalAlertBox
          modalStatus={modalStatus}
          modalHeader={modalHeader}
          modalBody={modalBody}
          modalFooter={modalFooter}
          onCancelFunc={(e) => onCancel(e)}
        />
      ) : null}
      {/* column filtering show/hide */}
      {sidebarVisible ? (
        <SidebarColumnFilter
          columns={columns}
          sidebarVisible={sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
    </div>
  );
};
export default CargoName;
