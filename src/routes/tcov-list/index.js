import React, { useEffect } from "react";
import { Table, Popconfirm } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import { useNavigate } from "react-router-dom";

const TCOVList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
  });

  const navigate = useNavigate();

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  const { loading, responseData, pageOptions, typesearch, donloadArray } = state
  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            <Popconfirm
              title="Are you sure, you want to delete it?"
              onConfirm={() => onRowDeletedClick(record)}
            >
             <DeleteOutlined /> />
            </Popconfirm>
          </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["tcov-list"] ? FIELDS["tcov-list"]["tableheads"] : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  // const getTableData = async (search = {}) => {
  //   const { pageOptions } = state;

  //   let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
  //   let headers = { order_by: { id: 'desc' } };

  //   if (
  //     search &&
  //     search.hasOwnProperty("searchValue") &&
  //     search.hasOwnProperty("searchOptions") &&
  //     search["searchOptions"] !== "" &&
  //     search["searchValue"] !== ""
  //   ) {
  //     let wc = {};
  //     search["searchValue"] = search["searchValue"].trim();
    
  //     if (search["searchOptions"].indexOf(";") > 0) {
  //       let so = search["searchOptions"].split(";");
  //       wc = { OR: {} };
  //       so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
  //     } else {
  //       wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
     
  //     }
    
  //     if (headers.hasOwnProperty("where")) {
  //       // If "where" property already exists, merge the conditions
  //       headers["where"] = { ...headers["where"], ...wc };
  //     } else {
  //       // If "where" property doesn't exist, set it to the new condition
  //       headers["where"] = wc;
  //     }
    
  //     state.typesearch = {
  //       searchOptions: search.searchOptions,
  //       searchValue: search.searchValue,
  //     };
  //   }
    

  //   setState(prev => ({
  //     ...prev,
  //     loading: true,
  //     responseData: [],
  //   }));

  //   let qParamString = objectToQueryStringFunc(qParams);

  //   let _url = `${URL_WITH_VERSION}/tcov/list?${qParamString}`;
  //   const response = await getAPICall(_url, headers);
  //   const data = await response;

  //   const totalRows = data && data.total_rows ? data.total_rows : 0;
  //   let dataArr = data && data.data ? data.data : [];
  //   let _state = { loading: false };
  //   if (dataArr.length > 0) {
  //     _state['responseData'] = dataArr;
  //   }
  //   setState(prev => ({
  //     ...prev,
  //     ..._state,
  //     pageOptions: {
  //       pageIndex: pageOptions.pageIndex,
  //       pageLimit: pageOptions.pageLimit,
  //       totalRows: totalRows,
  //     },
  //     loading: false,
  //   }));
  // };


;

const getTableData = async (searchtype = {}) => {
  const { pageIndex, pageLimit } = pageOptions;

  let qParams = { p: pageIndex, l: pageLimit };
  let headers = { order_by: { id: 'desc' } };
  let search = searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue') ? searchtype : typesearch;

  if (
    search &&
    search.hasOwnProperty('searchValue') &&
    search.hasOwnProperty('searchOptions') &&
    search['searchOptions'] !== '' &&
    search['searchValue'] !== ''
  ) {
    let wc = {};
    search['searchValue'] = search['searchValue'].trim();

    if (search['searchOptions'].indexOf(';') > 0) {
      let so = search['searchOptions'].split(';');
      wc = { OR: {} };
      so.map((e) => (wc['OR'][e] = { l: search['searchValue'] }));
    } else {
      wc[search['searchOptions']] = { l: search['searchValue'] };
    }

    headers['where'] = wc;
    setState((prevState) => ({ ...prevState, typesearch: { searchOptions: search.searchOptions, searchValue: search.searchValue } }));
  }

  setState((prevState) => ({ ...prevState, loading: true, responseData: [] }));

  let qParamString = objectToQueryStringFunc(qParams);
  let _url = `${URL_WITH_VERSION}/tcov/list?${qParamString}`;
  const response = await getAPICall(_url, headers);
  const data = await response;

  const totalRows = data && data.total_rows ? data.total_rows : 0;
  let dataArr = data && data.data ? data.data : [];
  let state = { loading: false };
  let donloadArr = [];

  if (dataArr.length > 0 && totalRows > responseData.length) {
    dataArr.forEach((d) => donloadArr.push(d["id"]));
    state['responseData'] = dataArr;
  }

  setState((prevState) => ({
    ...prevState,
    ...state,
    donloadArray: donloadArr,
    pageOptions: {
      pageIndex: pageIndex,
      pageLimit: pageLimit,
      totalRows: totalRows,
    },
    loading: false,
  }));
};

 


  const redirectToAdd = async (e, record = null) => {
    navigate(`/edit-voyage-estimate/${record.estimate_id}`);
  };
  const onCancel = () => {
    // getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  };

  /*
  onRowDeletedClick = (record) => {
    if ( record && record['tcov_status'] && record['tcov_status'].toUpperCase() === 'PENDING' ) {
      let _url = `${URL_WITH_VERSION}/tcov/delete`;
      apiDeleteCall(_url, { id: record.id }, (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
    } else {
      openNotificationWithIcon('error', "You can't delete TCOV Record as it is fixed ( OR Voyage Is Generated )");
    }
  };

*/

  // const callOptions = (evt) => {
  //   let _search = {
  //     searchOptions: evt["searchOptions"],
  //     searchValue: evt["searchValue"],
  //   };
  //   if (
  //     evt.hasOwnProperty("searchOptions") &&
  //     evt.hasOwnProperty("searchValue")
  //   ) {
  //     let pageOptions = state.pageOptions;

  //     pageOptions["pageIndex"] = 1;
  //     setState(
  //       (prevState) => ({
  //         ...prevState,
  //         search: _search,
  //         pageOptions: pageOptions,
  //       }),
  //       () => {
  //         getTableData(_search);
  //       }
  //     );
  //   } else if (
  //     evt &&
  //     evt.hasOwnProperty("actionName") &&
  //     evt["actionName"] === "reset-serach"
  //   ) {
  //     let pageOptions = state.pageOptions;
  //     pageOptions["pageIndex"] = 1;

  //     setState(
  //       (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
  //       () => {
  //         getTableData();
  //       }
  //     );
  //   } else if (
  //     evt &&
  //     evt.hasOwnProperty("actionName") &&
  //     evt["actionName"] === "column-filter"
  //   ) {
  //     // column filtering show/hide
  //     let responseData = state.responseData;
  //     let columns = Object.assign([], state.columns);

  //     if (responseData.length > 0) {
  //       for (var k in responseData[0]) {
  //         let index = columns.some(
  //           (item) =>
  //             (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
  //             (item.hasOwnProperty("key") && item.key === k)
  //         );
  //         if (!index) {
  //           let title = k
  //             .split("_")
  //             .map((snip) => {
  //               return snip[0].toUpperCase() + snip.substring(1);
  //             })
  //             .join(" ");
  //           let col = Object.assign(
  //             {},
  //             {
  //               title: title,
  //               dataIndex: k,
  //               key: k,
  //               invisible: "true",
  //               isReset: true,
  //             }
  //           );
  //           columns.splice(columns.length - 1, 0, col);
  //         }
  //       }
  //     }

  //     setState((prevState) => ({
  //       ...prevState,
  //       sidebarVisible: evt.hasOwnProperty("sidebarVisible")
  //         ? evt.sidebarVisible
  //         : !prevState.sidebarVisible,
  //       columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
  //     }));
  //   } else {
  //     let pageOptions = state.pageOptions;
  //     pageOptions[evt["actionName"]] = evt["actionVal"];

  //     if (evt["actionName"] === "pageLimit") {
  //       pageOptions["pageIndex"] = 1;
  //     }

  //     setState(
  //       (prevState) => ({ ...prevState, pageOptions: pageOptions }),
  //       () => {
  //         getTableData();
  //       }
  //     );
  //   }
  // };

  
  
  
  
   const callOptions = (evt) => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      setState({ ...state, search: search, pageOptions: pageOptions }, () => {
        getTableData(search);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = state.pageOptions;
      pageOptions['pageIndex'] = 1;
      setState({ ...state, search: {}, pageOptions: pageOptions, typesearch: {} }, () => {
        getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      setState({
        ...state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      setState({ ...state, pageOptions: pageOptions }, () => {
        getTableData();
      });
    }
  };
  
  const handleResize = (index) => (e, { size }) => {
    setState((prevState) => {
      const nextColumns = [...prevState.columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { ...prevState, columns: nextColumns };
    });
  };

  // const onActionDonwload = (downType, pageType) => {
  //  
  //   let params = `t=${pageType}`,
  
  //     cols = [];
  //   const { columns, pageOptions, donloadArray } = state;
  //   
  //   let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
  //   columns.map((e) =>
  //     e.invisible === "false" && e.key !== "action"
  //       ? cols.push(e.dataIndex)
  //       : false
  //   );
  //   if (cols && cols.length > 0) {
  //     params = params + '&c=' + cols.join(',')
  //   }
  //   const filter = donloadArray.join();
  //   // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
   
  //   window.open(
  //     `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
  //     "_blank"
  //   );

  //   // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  // };



  
  const onActionDonwload = (downType, pageType) => {
 
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = state;

   
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    if (cols && cols.length > 0) {
      params = params + '&c=' + cols.join(',')
    }
    const filter = donloadArray.join()
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  };

  const getdropDownFilter = (val) => {
    setState(prevState => ({ ...prevState, dropDownfilter: val }));
  };

  const getSearchData = (searchTerm) => {
    let donloadArr = [];
    const { responseData, dropDownfilter } = state;

    if (searchTerm) {
      const filterdata = responseData.filter((currentVessel) => {
        let isPresent = false;
        if (dropDownfilter === '' || dropDownfilter.split(';').length > 1) {
          for (let key of Object.keys(currentVessel)) {
            isPresent = typeof currentVessel[key] === 'string' && currentVessel[key] && currentVessel[key].toLowerCase().includes(searchTerm.toLowerCase());
            if (isPresent) break;
          }
        } else {
          for (let key of Object.keys(currentVessel)) {
            if (key === dropDownfilter) {
              isPresent = typeof currentVessel[key] === 'string' && currentVessel[key] && currentVessel[key].toLowerCase().includes(searchTerm.toLowerCase());
              if (isPresent) break;
            }
          }
        }
        return isPresent;
      });

      if (filterdata.length > 0) {
        filterdata.forEach((d) => donloadArr.push(d["id"]));
      }

      setState((prev) => ({
        ...prev,
        searchQuery: searchTerm,
        donloadArray: donloadArr,
        filterData: filterdata,
      }));
    } else if (searchTerm.length === 0) {
      setState((prev) => ({ ...prev, searchQuery: "" }));
    }
  };
  const tableCol = state.columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title">
                  <span>Voyage Estimate List</span>
                </h4>
              </div>
            </div>
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {state.loading === false ? (
                <ToolbarUI
                  routeUrl={"tcov-list-toolbar2"}
                  optionValue={{
                    pageOptions: state.pageOptions,
                    columns: state.columns,
                    search: state.search,
                  }}
                  callback={(e) => callOptions(e)}
                  // filter={filter}
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "tcov"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "tcov"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "tcov"),
                    },
                  ]}
                />
              ) : undefined}
            </div>
            <div>
              <Table
                // rowKey={record => record.id}
                className="inlineTable resizeableTable"
                bordered
                columns={tableCol}
                components={components}
                size="small"
                scroll={{ x: "max-content" }}
                dataSource={state.responseData}
                loading={state.loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? "table-striped-listing"
                    : "dull-color table-striped-listing"
                }
              />
            </div>
          </div>
        </div>
      </article>

      {state.isVisible === true ? <>Add TCOV</> : /*  */ undefined}
      {/* column filtering show/hide */}
      {state.sidebarVisible ? (
        <SidebarColumnFilter
          columns={state.columns}
          sidebarVisible={state.sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
    </div>
  );
};

export default TCOVList;







