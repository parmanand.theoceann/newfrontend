import React, { useEffect } from 'react';
import { Button, Table, Icon, Popconfirm, Modal, Tooltip } from 'antd';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon, ResizeableTitle, useStateCallback, IMAGE_PATH } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import VesselSchedule from '../../components/vessel-form/index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import { EditOutlined } from '@ant-design/icons';
import VMInfoVesselList from './VMInfoVesselList';

const VesselList = () => {

  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
    vmInfoData: "",
    vmInfoModal: false
  })



  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => redirectToAdd(e, record.vessel_id)}>
              <EditOutlined />
            </span>
            <Tooltip title="Voyage Manager Info">
              <span
                className="iconWrapper rounded-circle"
                onClick={(e) => redirectToVMList(e, record.vessel_id)}
              >
                <img style={{ width: "20px", height: "14px" }} src={IMAGE_PATH + "svgimg/vm.svg"} alt="vmsvg" />
              </span>
            </Tooltip>
          </div>
        )
      }
    };
    let tableHeaders = Object.assign([], FIELDS && FIELDS['vessel-list'] ? FIELDS['vessel-list']["tableheads"] : [])
    tableHeaders.push(tableAction)
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, [])




  const getTableData = async (searchtype = {}) => {

    const { pageOptions } = state;
    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    let headers = { "order_by": { "vessel_id": "desc" } };

    let _search = searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue') ? searchtype : state.typesearch;
    if (_search && _search.hasOwnProperty('searchValue') && _search.hasOwnProperty('searchOptions') && _search['searchOptions'] !== '' && _search['searchValue'] !== '') {
      let wc = {}
      _search['searchValue'] = _search['searchValue'].trim()

      if (_search['searchOptions'].indexOf(';') > 0) {
        let so = _search['searchOptions'].split(';');
        wc = { "OR": {} };
        so.map(e => wc['OR'][e] = { "l": _search['searchValue'] });
      } else {
        wc = { "OR": {} };
        wc['OR'][_search['searchOptions']] = { "l": _search['searchValue'] }
      }
      headers['where'] = wc;

      state.typesearch = { 'searchOptions': _search.searchOptions, 'searchValue': _search.searchValue }

    }

    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = {},
      dataArr = [],
      totalRows = 0;
    let _url = `${URL_WITH_VERSION}/vessel/list?${qParamString}`;

    const response = await getAPICall(_url, headers);
    const data = await response;

    totalRows = data && data.total_rows ? data.total_rows : 0;
    dataArr = (data && data.data ? data.data : []);
    let donloadArr = []
    if (dataArr.length > 0 && (totalRows > 0)) {
      dataArr.forEach(d => donloadArr.push(d["vessel_id"]))
      _state["responseData"] = dataArr;
    }
    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  }

  const redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${id}`);
      const respData = await response['data'];
      respData['id'] = respData['vessel_id'];
      setState(prevState => ({ ...prevState, isAdd: false, formDataValues: respData, isVisible: true }))
    } else {
      setState(prevState => ({ ...prevState, isAdd: true, isVisible: true }))

    }
  }
  const redirectToVMList = async (e, id) => {
    try {
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/vm/edit?ae=${id}`);
      const respData = await response['data'];
      if (respData) {
        setState(prevState => ({ ...prevState, vmInfoData: respData, vmInfoModal: true }))
      } else {
        openNotificationWithIcon("error", "No VM Data is available for this Vessel")
      }
    } catch (err) {
      console.log("error", err);
      openNotificationWithIcon("error", "Something Went wrong", 3);
    }
  }
  const onCancel = () => {

    setState({ ...state, isAdd: true, isVisible: false }, () => getTableData());
  }

  // onRowDeletedClick = (id) => {
  //   let _url = `${URL_WITH_VERSION}/vessel/delete`;
  //   apiDeleteCall(_url, { "id": id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon('success', response.message);
  //       getTableData(1);
  //     } else {
  //       openNotificationWithIcon('error', response.message)
  //     }
  //   })
  // }



  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  //resizing function
  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const { vmInfoModal, vmInfoData, columns, loading, responseData, pageOptions, search, isAdd, isVisible, formDataValues, sidebarVisible } = state;
 
  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true") ? true : false)
    .map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title"><span>Vessel List</span></h4>
              </div>
              <div className="action-btn">
                <Button type="primary" onClick={(e) => redirectToAdd(e)}>Add Vessel Form</Button>
              </div>
            </div>
            <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
              {
                loading === false ?
                  <ToolbarUI routeUrl={'vessel-list-toolbar'}
                    optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => onActionDonwload('csv', 'vessel') },
                      { title: 'PDF', event: () => onActionDonwload('pdf', 'vessel') },
                      { title: 'XLS', event: () => onActionDonwload('xlsx', 'vessel') }
                    ]}
                  />
                  : undefined
              }
            </div>
            <div>
              <Table
                rowKey={record => record.vessel_id}
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                scroll={{ x: 'max-content' }}
                // scroll={{ y: 370 }}
                components={components}
                columns={tableColumns}
                size="small"
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
              />
            </div>
          </div>
        </div>
      </article>

      {
        isVisible === true ?
          <Modal
            title={(isAdd === false ? "Edit" : "Add") + " Vessel Form"}
            open={isVisible}
            width='95%'
            onCancel={onCancel}
            style={{ top: '10px' }}
            bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
            footer={null}
            maskClosable={false}
          >
            {
              isAdd === false ?
                <VesselSchedule formData={formDataValues} modalCloseEvent={onCancel} onEdit={true} showSideListBar={false} />
                :
                <VesselSchedule formData={{}} modalCloseEvent={onCancel} />
            }
          </Modal>
          : undefined
      }
      {/* column filtering show/hide */}
      {
        sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => callOptions(e)} /> : null
      }


      {
        vmInfoModal ?
          <Modal
            title={" Voyage Manager Info"}
            open={vmInfoModal}
            width='95%'
            height="auto"
            onCancel={() => setState((pre) => ({ ...pre, vmInfoModal: false }))}
            style={{ top: '10px' }}
            bodyStyle={{  overflowY: 'auto', padding: '0.5rem' }}
            footer={null}
            maskClosable={false}
          >
            <VMInfoVesselList vmInfoData={vmInfoData} />
          </Modal>
          : undefined
      }

    </div>
  )
}

export default VesselList;
