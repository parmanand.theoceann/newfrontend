import React, { Component } from 'react'
import {
    Table, Modal, Input
} from 'antd';
import {EyeOutlined} from '@ant-design/icons';
import URL_WITH_VERSION, {
    objectToQueryStringFunc,
    getAPICall
} from '../../shared';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import PortDetails from './portDetails'
class PortData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    title: 'Port Name',
                    dataIndex: 'portname',
                    key: 'portname',
                },
                {
                    title: 'UNLOCODE',
                    dataIndex: 'unlocode',
                    key: 'unlocode',
                },
                {
                    title: 'Status',
                    dataIndex: 'status',
                    key: 'status',
                },
                {
                    title: 'Port Id',
                    dataIndex: 'port_id',
                    key: 'port_id',
                },
                {
                    title: 'Master Port ID',
                    dataIndex: 'masterpoid',
                    key: 'masterpoid',
                },
                {
                    title: 'Global Port ID',
                    dataIndex: 'globalportid',
                    key: 'globalportid',
                },
                {
                    title: 'Full Port Style',
                    dataIndex: 'fullportstyle',
                    key: 'fullportstyle',
                },
                {
                    title: 'Country',
                    dataIndex: 'country',
                    key: 'country',
                },
                {
                    title: 'Country Code',
                    dataIndex: 'countrycode',
                    key: 'countrycode',
                },
                {
                    title: 'World Port Number',
                    dataIndex: 'worldportnumber',
                    key: 'worldportnumber',
                },
                {
                    title: 'Action',
                    key: 'operation',
                    fixed: 'right',
                    width: 100,
                    render: (data) => {
                        return (
                            <div className="editable-row-operations">
                                <span className="iconWrapper" onClick={(e) => this.setState({ id: data.worldportnumber }, () => this.setState({ visibleviewmodal: true }))}>
                                <EyeOutlined />
                                </span>
                            </div>
                        )
                    }
                },
            ],
            pageOptions: { pageIndex: 0, pageLimit: 10, totalRows: 0 },
            data: [],
            loading: false,
            visibleviewmodal: false,
            searchVisible: false,
            searchData: []
        }
    }



    componentDidMount = async () => {
        this.getTableData();
    }

    onChange = async (data) => {
        if (data && data.length >= 3) {
            try {
                const response = await fetch(`${process.env.REACT_APP_URL_NEW}PortDetail/find/${data}`, { mode: 'cors' });
                const respdata = await response.json();
                if (respdata && respdata.data == false) {
                    this.setState({
                        searchVisible: true,
                        searchData: []
                    })
                }
                else {
                    this.setState({
                        searchVisible: true,
                        searchData: [respdata]
                    })
                }
            }
            catch (e) {
                console.log("e")
                this.setState({
                    searchVisible: false,
                    searchData: []
                })
            }
        }
        else {
            this.setState({
                searchVisible: false,
                searchData: []
            })
        }

    }

    getTableData = async (search = {}) => {
        const { pageOptions } = this.state;
        let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
        let headers = { order_by: { id: 'desc' } };
        if (
            search &&
            search.hasOwnProperty('searchValue') &&
            search.hasOwnProperty('searchOptions') &&
            search['searchOptions'] !== '' &&
            search['searchValue'] !== ''
        ) {
            let wc = {};
            if (search['searchOptions'].indexOf(';') > 0) {
                let so = search['searchOptions'].split(';');
                wc = { OR: {} };
                so.map((e) => (wc['OR'][e] = { l: search['searchValue'] }));
            } else {
                wc[search['searchOptions']] = { l: search['searchValue'] };
            }

            headers['where'] = wc;
        }
        // console.log(search);
        this.setState({
            ...this.state,
            loading: true,
            data: [],
        });
        let qParamString = objectToQueryStringFunc(qParams);
        let _url = `${process.env.REACT_APP_URL_NEW}PortDetail/list?${qParamString}`;
        const response = await getAPICall(_url, headers);
        const data = await response;
        let state = { loading: false };
        state['data'] = data && data.ports ? data.ports : []
        let totalRows = data.meta.totCount
        this.setState({
            ...this.state,
            ...state,
            pageOptions: {
                pageIndex: pageOptions.pageIndex,
                pageLimit: pageOptions.pageLimit,
                totalRows: totalRows,
            },
            loading: false,
        });
    };

    callOptions = (evt) => {
        if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
            let pageOptions = this.state.pageOptions;
            let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
            pageOptions['pageIndex'] = 1;
            this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
                this.onChange(search.searchValue);
            });
        } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
            let pageOptions = this.state.pageOptions;
            pageOptions['pageIndex'] = 1;
            this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
                this.getTableData();
            });
        } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
            // column filtering show/hide
            let data = this.state.data;
            let columns = Object.assign([], this.state.columns);

            if (data.length > 0) {
                for (var k in data[0]) {
                    let index = columns.some(
                        (item) =>
                            (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
                            (item.hasOwnProperty('key') && item.key === k)
                    );
                    if (!index) {
                        let title = k
                            .split('_')
                            .map((snip) => {
                                return snip[0].toUpperCase() + snip.substring(1);
                            })
                            .join(' ');
                        let col = Object.assign(
                            {},
                            {
                                title: title,
                                dataIndex: k,
                                key: k,
                                invisible: 'true',
                                isReset: true,
                            }
                        );
                        columns.splice(columns.length - 1, 0, col);
                    }
                }
            }
            this.setState({
                ...this.state,
                sidebarVisible: evt.hasOwnProperty('sidebarVisible')
                    ? evt.sidebarVisible
                    : !this.state.sidebarVisible,
                columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
            });
        } else {
            let pageOptions = this.state.pageOptions;
            pageOptions[evt['actionName']] = evt['actionVal'];

            if (evt['actionName'] === 'pageLimit') {
                pageOptions['pageIndex'] = 1;
            }

            this.setState({ ...this.state, pageOptions: pageOptions }, () => {
                this.getTableData();
            });
        }
    };


    onActionDonwload = (downType, pageType) => {
        // let params = `t=${pageType}`, cols = [];
        // const { columns, pageOptions, donloadArray } = this.state;  
        // let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
        // columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
        // // if (cols && cols.length > 0) {
        // //   params = params + '&c=' + cols.join(',')
        // // }
        // const filter = donloadArray.join()
        // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
      }

    render() {
        const {
            columns,
            loading,
            data,
            pageOptions,
            filter,
            search,
            visibleviewmodal,
            searchData,
            searchVisible
        } = this.state;
        return (
            <div className="body-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Input onChange={(e) => this.onChange(e.target.value)} placeholder='search' style={{ width: '150px', float: 'right' }} />
                            <div
                                className="section"
                                style={{
                                    width: '100%',
                                    marginBottom: '10px',
                                    paddingLeft: '15px',
                                    paddingRight: '15px',
                                    marginTop: '50px'
                                }}
                            >
                                {loading === false ? (
                                    <ToolbarUI
                                        routeUrl={'portdata-list-toolbar'}
                                        optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }}
                                        callback={(e) => this.callOptions(e)}
                                        filter={filter}
                                        dowloadOptions={[
                                            { title: 'CSV', event: () => this.onActionDonwload('csv', 'tcov') },
                                            { title: 'PDF', event: () => this.onActionDonwload('pdf', 'tcov') },
                                            { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'tcov') }
                                        ]}
                                    />
                                ) : undefined}
                            </div>
                            <div>
                                <Table
                                    className="inlineTable resizeableTable"
                                    columns={columns}
                                    dataSource={searchVisible ? searchData : data}
                                    pagination={false}
                                    loading={loading}
                                    size="small"
                                    bordered
                                    scroll={{ x: 'max-content' }}
                                    rowClassName={(r, i) =>
                                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                                    }
                                />
                            </div>
                            {


                                visibleviewmodal ?
                                    <Modal
                                        style={{ top: '2%' }}
                                        title={"Port Details"}
                                       open={visibleviewmodal}
                                        onCancel={() => this.setState({ visibleviewmodal: false })}
                                        width="95%"
                                        footer={null}
                                    >
                                        <PortDetails id={this.state.id} />
                                    </Modal>
                                    : undefined
                            }
                        </div>
                    </div>
                </article>
            </div>
        )
    }

}

export default PortData