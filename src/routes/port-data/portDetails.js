import { Layout, Table, Typography, Input, Row, Col, Button, Divider, Tabs, Select } from 'antd';
import React, { Component } from 'react'



import PortDataMap from './PortDataMap';

const { Title } = Typography;
const { Content } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;

class PortDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            terminalListColumns: [
                {
                    title: 'Port Id',
                    dataIndex: 'port_id',
                    key: 'port_id',
                },
                {
                    title: 'Terminal Name',
                    dataIndex: 'terminal_name',
                    key: 'terminal_name',
                },
                {
                    title: 'Terminal (Facility Type)',
                    dataIndex: 'facility_type',
                    key: 'facility_type',
                },
                {
                    title: 'Latitude Degree',
                    dataIndex: 'latitude_degrees',
                    key: 'latitude_degrees',
                },
                {
                    title: 'Longitude Degree',
                    dataIndex: 'longitude_degrees',
                    key: 'longitude_degrees',
                },
                {
                    title: 'Dec Latitude',
                    dataIndex: 'dec_latitude',
                    key: 'dec_latitude',
                },
                {
                    title: 'Dec Longitude',
                    dataIndex: 'dec_longitude',
                    key: 'dec_longitude',
                },
                {
                    title: 'Terminal Status',
                    dataIndex: 'terminal_status',
                    key: 'terminal_status',
                },
            ],
            berthListColumns: [
                {
                    title: 'Berth ID',
                    dataIndex: 'berth_id',
                    key: 'berth_id',
                },
                {
                    title: 'Terminal ID',
                    dataIndex: 'terminal_id',
                    key: 'terminal_id',
                },
                {
                    title: 'Berth Name',
                    dataIndex: 'berth_name',
                    key: 'berth_name',
                },
                {
                    title: 'Berth No',
                    dataIndex: 'berth_no',
                    key: 'berth_no',
                },
                {
                    title: 'Latitude Degrees',
                    dataIndex: 'latitude_degrees',
                    key: 'latitude_degrees',
                },
                {
                    title: 'Longitude Degrees',
                    dataIndex: 'longitude_degrees',
                    key: 'longitude_degrees',
                },
                {
                    title: 'Dec Latitude',
                    dataIndex: 'dec_latitude',
                    key: 'dec_latitude',
                },
                {
                    title: 'Dec Longitude',
                    dataIndex: 'dec_longitude',
                    key: 'dec_longitude',
                },
                {
                    title: 'Berth Type',
                    dataIndex: 'berth_type',
                    key: 'berth_type',
                },
                {
                    title: 'Facility Type',
                    dataIndex: 'facility_type',
                    key: 'facility_type',
                },
                {
                    title: 'Berth UKC',
                    dataIndex: 'berth_ukc_min',
                    key: 'berth_ukc_min',
                },
                {
                    title: 'Dry Bulk',
                    dataIndex: 'dry_bulk',
                    key: 'dry_bulk',
                },
                {
                    title: 'Ship DWT Max',
                    dataIndex: 'ship_dwt_max',
                    key: 'ship_dwt_max',
                },
                {
                    title: 'Ship DWT Min',
                    dataIndex: 'ship_dwt_min',
                    key: 'ship_dwt_min',
                }
            ],
            columnsInArrival: [
                {
                    title: 'Vessel Name',
                    dataIndex: 'vessel_name',
                    key: 'name',
                },
                {
                    title: 'IMO No',
                    dataIndex: 'imo_no',
                    key: 'imo_no',
                },
                {
                    title: 'Type',
                    dataIndex: 'type',
                    key: 'type',
                },
                {
                    title: 'DWT',
                    dataIndex: 'dwt',
                    key: 'dwt',
                },
                {
                    title: 'Destination Port',
                    dataIndex: 'destination_port',
                    key: 'destination_port',
                },
                {
                    title: 'Arival Time',
                    dataIndex: 'arival_time',
                    key: 'arival_time',
                },
            ],
            columnsPortCost: [
                {
                    title: 'Vessel Name',
                    dataIndex: 'vessel_name',
                    key: 'name',
                },
                {
                    title: 'Vessel type',
                    dataIndex: 'imo_no',
                    key: 'imo_no',
                },
                {
                    title: 'DWT',
                    dataIndex: 'type',
                    key: 'type',
                },
                {
                    title: 'GRT',
                    dataIndex: 'dwt',
                    key: 'dwt',
                },
                {
                    title: 'Cargo Name',
                    dataIndex: 'destination_port',
                    key: 'destination_port',
                },
                {
                    title: 'Port Name',
                    dataIndex: 'port_name',
                    key: 'port_name',
                },
                {
                    title: 'Port Activity',
                    dataIndex: 'port_activity',
                    key: 'port_activity',
                },
                {
                    title: 'PDA/FDA type',
                    dataIndex: 'fda_type',
                    key: 'fda_type',
                },
                {
                    title: 'Ttl Port Cost($)',
                    dataIndex: 'ttl_port_cost',
                    key: 'ttl_port_cost',
                },
                {
                    title: 'Total Port days',
                    dataIndex: 'ttl_p_days',
                    key: 'ttl_p_days',
                },
                {
                    title: 'Days',
                    dataIndex: 'days',
                    key: 'days',
                },
                {
                    title: 'Bench',
                    dataIndex: 'bench',
                    key: 'bench',
                },
                {
                    title: 'Towage',
                    dataIndex: 'towage',
                    key: 'towage',
                },
                {
                    title: 'Compare to',
                    dataIndex: 'compare_to',
                    key: 'compare_to',
                }
            ]
        }
    }
    componentDidMount = async () => {
        const { id } = this.props
        try {
            const response = await fetch(`${process.env.REACT_APP_URL_NEW}PortDetail/find/${id}`, { mode: 'cors' });
            const data = await response.json();
            this.setState({
                data
            })
        }
        catch (e) {
            console.log("e")
        }
    }


    render() {
        const { data } = this.state
        const size = "small"
        return (
            <>
                <div className="tcov-wrapper full-wraps">
                    <Layout className="layout-wrapper">
                        <Layout>
                            <Content className="content-wrapper">
                                <div className="fieldscroll-wrap">
                                    <div className="body-wrapper">
                                        <article className="article">
                                            <div className="box box-default">
                                                <div className="box-body common-fields-wrapper">
                                                    <Title level={4}>Port Information</Title>
                                                    <Divider></Divider>

                                                    <Tabs defaultActiveKey="1" type="card" size={size}>
                                                        <TabPane tab="Port Info" key="1">
                                                            <Divider><Title level={4}>General details</Title></Divider>
                                                            <Row>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Port Name</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.portname && data.portname != 'null' ? data.portname : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Port Id</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.port_id && data.port_id != 'null' ? data.port_id : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Latitude (N/S)</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.latitudedecimal && data.latitudedecimal != 'null' ? data.latitudedecimal : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Longitude (E/W)</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Country Name</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.country && data.country != 'null' ? data.country : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Country Code</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.countrycode && data.countrycode != 'null' ? data.countrycode : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>No of Terminal</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>No of Birth</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10} style={{ textDecoration: 'underline' }}>Status</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.status && data.status != 'null' ? data.status : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>UNLOCODE</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.unlocode && data.unlocode != 'null' ? data.unlocode : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Timezone</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.timezone && data.timezone != 'null' ? data.timezone : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Local Time</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={''} />
                                                                        </Col>
                                                                    </Row>

                                                                </Col>
                                                            </Row>
                                                            <Divider><Title level={4}>Overall Restriction</Title></Divider>
                                                            <Row>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col span={12} className="head"> Type </Col>
                                                                        <Col span={12} className="head"> Value (MT/Meters)</Col>
                                                                    </Row>

                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max Beam</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxbeam && data.maxbeam != 'null' ? data.maxbeam : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max DWT</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxdwt && data.maxdwt != 'null' ? data.maxdwt : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max LOA</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxloa && data.maxloa != 'null' ? data.maxloa : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max Offshore BCM</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxoffshorebcm && data.maxoffshorebcm != 'null' ? data.maxoffshorebcm : ''} />
                                                                        </Col>
                                                                    </Row>

                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max Offshore Draught</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxoffshoredraught && data.maxoffshoredraught != 'null' ? data.maxoffshoredraught : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max Offshore DWT</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxoffshoredwt && data.maxoffshoredwt != 'null' ? data.maxoffshoredwt : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Max Offshore LOA</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maxoffshoreloa && data.maxoffshoreloa != 'null' ? data.maxoffshoreloa : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Maximum Draft</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.maximumdraft && data.maximumdraft != 'null' ? data.maximumdraft : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                            </Row>
                                                            <Divider><Title level={4}>Facilities</Title></Divider>
                                                            <Row>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Multi Purpose Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input
                                                                                type="text" value={
                                                                                    data.multipurposefacilities == 0 ? 'No' : 'Yes'
                                                                                }

                                                                            />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Dry Bulk Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.drybulkfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Dry Dock Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.drydockfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Gas Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.gasfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>ISPS Compliant</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.ispscompliant == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Liquid Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.liquidfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Passenger Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.passengerfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>RoRo Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.rorofacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col className="gutter-row" span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Breakbulk Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.breakbulkfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>Container Facilities</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.containerfacilities == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Row>
                                                                        <Col className='bold' span={10}>CSI Compliant</Col>
                                                                        <Col className='' span={14} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data.csicompliant == 0 ? 'No' : 'Yes'} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                            </Row>
                                                            <Divider></Divider>
                                                            <Row>
                                                                <Col span={10}>
                                                                    <PortDataMap />
                                                                    {/* <div class="mapouter" style={{ paddingRight: 10 }}><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=bombay%20port&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                                                                    </div> */}
                                                                </Col>
                                                                <Col span={14}>
                                                                    <Tabs defaultActiveKey="1" type="card" size={size}>
                                                                        <TabPane tab="Terminal List" key="1">
                                                                            <Table
                                                                                className="inlineTable resizeableTable"
                                                                                dataSource={data != null && data.PortTerminals && data.PortTerminals.length > 0 ? data.PortTerminals : []}
                                                                                columns={this.state.terminalListColumns}
                                                                                pagination={false}
                                                                                scroll={{
                                                                                    x: 1600,
                                                                                    y: 400
                                                                                }}
                                                                                rowClassName={(r, i) =>
                                                                                    i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                                                                                }
                                                                            />
                                                                        </TabPane>
                                                                        <TabPane tab="Berth List" key="2">
                                                                            <Table
                                                                                className="inlineTable resizeableTable"
                                                                                dataSource={data != null && data.PortBerths && data.PortBerths.length > 0 ? data.PortBerths : []}
                                                                                columns={this.state.berthListColumns}
                                                                                pagination={false}
                                                                                scroll={{
                                                                                    x: 1600,
                                                                                    y: 400
                                                                                }}
                                                                                rowClassName={(r, i) =>
                                                                                    i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                                                                                }
                                                                            />
                                                                        </TabPane>
                                                                    </Tabs>
                                                                </Col>
                                                            </Row>
                                                        </TabPane>
                                                        <TabPane tab="Port Congestion" key="2" style={{ border: '1px solid #f0f0f0', borderTop: 0, padding: 15, marginTop: -16 }}>
                                                            <Row>
                                                                <Col span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={5}>Port Name</Col>
                                                                        <Col className='' span={19} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.portname && data.portname != 'null' ? data.portname : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={5}>Port ID</Col>
                                                                        <Col className='' span={19} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={data != null && data.port_id && data.port_id != 'null' ? data.port_id : ''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                                <Col span={8}>
                                                                    <Row>
                                                                        <Col className='bold' span={5}>Local time</Col>
                                                                        <Col className='' span={19} style={{ paddingRight: 10 }}>
                                                                            <Input type="text" value={''} />
                                                                        </Col>
                                                                    </Row>
                                                                </Col>
                                                            </Row>

                                                            <Row span={24} style={{ marginTop: 30, marginBottom: 20 }}>
                                                                <Col>
                                                                    <label className='bold' style={{ fontSize: 14, marginRight: 25 }}>
                                                                        In Arrival
                                                                    </label>
                                                                    <label className='bold' style={{ fontSize: 14 }}>
                                                                        In Port
                                                                    </label>
                                                                </Col>
                                                            </Row>

                                                            <Table dataSource={[]} columns={this.state.columnsInArrival} />

                                                            <Row span={24} style={{ marginTop: 30, marginBottom: 20 }}>
                                                                <Col>
                                                                    <label className='bold' style={{ fontSize: 14, marginRight: 25 }}>
                                                                        Arrived
                                                                    </label>
                                                                    <label className='bold' style={{ fontSize: 14 }}>
                                                                        In Port
                                                                    </label>
                                                                </Col>
                                                            </Row>

                                                            <Table dataSource={[]} columns={this.state.columnsInArrival} />



                                                        </TabPane>

                                                    </Tabs>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </Content>
                        </Layout>
                    </Layout>
                </div>

            </>
        )
    }
}

export default PortDetails;