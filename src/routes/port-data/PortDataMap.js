import React, { Component } from 'react'
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import "./Style.css";

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

export default class PortDataMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
        lng: 41.15653,
        lat: 41.32111,
        zoom: 3
        };
        this.mapContainer = React.createRef();
        }
        componentDidMount() {
        const { lng, lat, zoom } = this.state;
        const map = new mapboxgl.Map({
        container: this.mapContainer.current,
        style: 'mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6',
        center: [lng, lat],
        zoom: zoom
        });
         
        map.on('move', () => {
        this.setState({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
        });
        });
        }
    render() {
        const { lng, lat, zoom } = this.state;
    return (
        <div>

<div ref={this.mapContainer} className="map-container" />
</div>
    );
  }
}
