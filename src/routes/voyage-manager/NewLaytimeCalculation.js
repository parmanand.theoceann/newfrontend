import { Row, Col, Layout, Input, Form, Modal, Spin, Alert } from "antd";
import React, { Component } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  awaitPostAPICall,
  postAPICall,
  apiDeleteCall,
  openNotificationWithIcon,
  getAPICall,
} from "../../shared";
import { OriginalState } from "../../services/dynamic_form_fields";
import PortLaytimeReports from "./laytime-report/PortLaytimeReports";
import SummaryReports from "./laytime-report/SummaryReports";
import PortCalcullationReport from "./laytime-report/PortCalcullationReport";
import ClaimInvoice from "../claim-invoice/ClaimInvoice";
import _ from "lodash";
import moment from "moment";
import { ErrorEvent } from "mapbox-gl";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
const { Content } = Layout;
const FormItem = Form.Item;
const TextArea = Input.TextArea;

export default class NewLaytimeCalculation extends Component {
  constructor(props) {
    super(props);
  
    
    const dots = this.props.formData["."].map(({ key, ...rest }, i) => {
      return rest;
    });

    const newactivity = [
      {
        activity_id: "42",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6
      },

      {
        activity_id: "1",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+1
      },

      {
        activity_id: "91",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+2
      },
      {
        activity_id: "34",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+3
      },

      {
        activity_id: "35",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+4
      },

      {
        activity_id: "86",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+5
      },

      {
        activity_id: "87",
        port_id: "",
        from_date: "",
        remark: "",
        id:-9e6+6
      },
    ];

    
    const activity =
      this.props.formData &&
      this.props.formData["activities"] != undefined && this.props.formData.id 
     &&this.props.formData["activities"].length > 0
      && this.props.formData["activities"].some((el) => el.id)
        ? this.props.formData["activities"]
        : newactivity;

    this.state = {
      visible: false,
      visibleEstimate: false,
      isVisible: false,
      frmName: "new_laytime_calculation_form",
      formData: this.props.formData
        ? {
            ...this.props.formData,
            ".": dots,
            'operation_type':this.props.formData['operation_type']||this.props.formData['ops_type'],
            activities: activity,
          }
        : {},
      extraFormFields: null,
      extraFieldValues: {
        counterparty_qty_used: 0,
        total_qty_used: 0,
        total_qty_allowed: 0,
        allocation_percent_qty_used: 0,
        allocation_percent_qty_allowed: 0,
        allocated_used: 0,
        allocated_allowed: 0,
        minimum_allowed: 0,
        grace_time: "",
        dispatch_time: "",
        warranty_floor: 0,
        warranty_percentage: 0,
        warranty_ceilling: 0,
        difference_reason: "",
        calculataion_remarks: "",
        counterparty_qty_allowed: 0,
      },
      visibleAttachment: false,
      loading: false,
      voyageData: this.props.voyageData,
      fileArr: [],
      visisbleCargoChild: false,
      frmChange: false,
      oldFormData: this.props.frmData || {},
      totalInfo: this.props.summary || {},
      balance: 5,
      dem_dispatch: 0,
      tabs: [],
      tabNames: [],
      frmOptions: [],
      extraTableButtons: {},
      voyID: this.props.formData["id"],
      checkEdit:
        typeof props.formData["type"] !== "undefined"
          ? props.formData["type"]
          : "new",
      subData: [],
      OG: {},
      isShowPortLaytimeReports: false,
      isShowSummaryReports: false,
      isShowClaimInvoice: false,
      isShowPortCalcullationReport: false,
      refreshForm: false,
      laytimeID:this.props.formData["type"]==='edit'?this.props.formData['id'] : null
    };
    this.formDataref = React.createRef();
  }

  // PortLaytimeReports = showPortLaytimeReports =>
  //   this.setState({ ...this.state, isShowPortLaytimeReports: showPortLaytimeReports });

  ClaimInvoice = (showClaimInvoice) => {
  
    let formType =
      typeof this.state.formData["formType"] !== "undefined"
        ? this.state.formData["formType"]
        : "edit";
    if (formType === "new") {
      openNotificationWithIcon(
        "error",
        <div
          dangerouslySetInnerHTML={{
            __html:
              "claim this can only be generated once the new laytime is generated",
          }}
        />,
        5
      );
    } else {
      this.setState({ ...this.state, isShowClaimInvoice: showClaimInvoice });
    }
  };

  PortCalcullationReport = (showPortCalcullationReport) =>
    this.setState({
      ...this.state,
      isShowPortCalcullationReport: showPortCalcullationReport,
    });

  componentDidMount = async () => {
    const { oldFormData, formData, frmOptions } = this.state;

    let tabNames = ["."];
    let multipleTabs = [],
      extraTableButton = {};
    let post_data = {},
      extraFieldValues = {};
    
    if (formData && formData.hasOwnProperty("id")) {
      formData["."].map((e) => {
        tabNames.push(e["port_name"]);
        extraTableButton[e.port_name] = [
          {
            icon: <PlusOutlined/>,
            internal: true,
            callType: "ADD_BETWEEN",
            inLastRow: false,
          },
        ];
        multipleTabs.push({
          tabName: e.port_name,
          tabGroup: e.port_name,
        });
        return true;
      });
      // if (formData['.'].length > 1) {
      //   multipleTabs.push({ tabName: 'All Port', tabGroup: 'All Port' });
      // }
      formData["calculation"] = parseInt(formData["calculation"]);

      post_data = formData;
      extraFieldValues =
        typeof post_data["efv"] !== "undefined"
          ? post_data["efv"]
          : this.state.extraFormFields;
    } else {
      let all_ports = Object.assign([], oldFormData["all_port"]);
      let load_disc = Object.assign([], oldFormData["load_disc"]);
      let checkboxes = Object.assign({}, oldFormData["...."]);

      post_data = {
        voyage_manager_id: oldFormData["voyno"],
        vessel_id: oldFormData["vessel_id"],
        port_cargo_id: [],
        charterer_id: 0,
      };

      all_ports.map((e) => {
        if (e["s"] === true) {
          post_data["port_cargo_id"].push(e["cargo"]);
        }
        return true;
      });
      oldFormData["."].map((e) => (post_data["charterer_id"] = e.id));
      load_disc.map((e) => {
        if (e["s"] === true && post_data["port_cargo_id"].indexOf(e) === -1) {
          post_data["port_cargo_id"].push(e["cargo"]);
        }
        return true;
      });

      this.setState({ ...this.state, refreshForm: true });
      const response = await awaitPostAPICall(
        `${URL_WITH_VERSION}/laytime-calculator/edit`,
        post_data
      );
      const respData = await response["data"];
 
      post_data = Object.assign({}, post_data, respData);
      oldFormData["."].map((e) => {
        if (e && e["charterer"] && e["s"] === true) {
          post_data["charterer"] = e.charterer;
        }
        return true;
      });
      post_data["voyno"] = oldFormData["voyno"];
      post_data["."] = Object.assign([], post_data["port_cargo"]);
      // post_data['port_cargo'].map(e => post_data["."].push(e));
      delete post_data["port_cargo"];

      if (post_data.hasOwnProperty("tab_names")) {
        post_data["tab_names"].map((e) => tabNames.push(e));
        delete post_data["tab_names"];
      }

      post_data["."].map((e) => {
        extraTableButton[e.port_name] = [
          {
            icon: "plus",
            internal: true,
            callType: "ADD_BETWEEN",
            inLastRow: false,
          },
        ];
        multipleTabs.push({ tabName: e.port_name, tabGroup: e.port_name });
        return true;
      });
      if (post_data["."].length > 1) {
        multipleTabs.push({ tabName: "All Port", tabGroup: "All Port" });
      }

      // post_data["..."] = Object.assign({}, post_data["..."], {
      //   laytime_expires:
      //     checkboxes.hasOwnProperty("laytime_expires") &&
      //     checkboxes["laytime_expires"] === true
      //       ? true
      //       : false,
      //   once_always_on_dem:
      //     checkboxes.hasOwnProperty("once_dem") &&
      //     checkboxes["once_dem"] === true
      //       ? true
      //       : false,
      // });
    }
    let originalData = Object.assign({}, post_data);
    const originalObj = new OriginalState(originalData);
    this.formDataref.current = post_data;

  
    this.setState({
      ...this.state,
      formData: post_data,
      //OG: originalObj,
      tabs: multipleTabs,
      tabNames: tabNames,
      extraTableButtons: extraTableButton,
      extraFieldValues: extraFieldValues,
      refreshForm: false,
      //  frmOptions: [{ key: "port_id", data: port_dropdownarr }],
      extraFormFields: {
        isShowInMainForm: true,
        content: this.getExternalFormFields(
          post_data.hasOwnProperty("efv") ? post_data["efv"] : {}
        ),
      },
    });
  };

  SummaryReports = async (showSummaryReports) => {
    // for report Api
    if(!this.state.laytimeID) {
      openNotificationWithIcon("info", "Please Create Laytime First.", 5);
      return
    }
    const responseReport = await getAPICall(
      `${URL_WITH_VERSION}/laytime-calculator/summaryreport?e=${
        this.state.laytimeID
      }`
    );
    let respDataReport = await responseReport["data"];

    respDataReport = { ...respDataReport, formData: this.state.formData };

    if (respDataReport) {
      this.setState({ ...this.state, reportFormData: respDataReport }, () =>
        this.setState({
          ...this.state,
          isShowSummaryReports: showSummaryReports,
        })
      );
    } else {
      openNotificationWithIcon("error", "Unable to show report", 5);
    }

    // thia.reportDataFormat()
  };

  onClickExtraIcon = async (action, data) => {
    
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
   
    // groupKey = "---";
    // frm_code = "tab_new_laytime_port_form_all_ports";
    if(!delete_id || delete_id<0) {
      return;
    }
    if(groupKey=='Activities'){
      groupKey = "activities";
      frm_code = "new_laytime_calculation_form";
    }

    if(groupKey=='---'){
      groupKey = "---";
      frm_code = "tab_new_laytime_port_form_all_ports";
    }

    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  PortLaytimeReports = async (showPortLaytimeReports) => {
    // for report Api
    if(!this.state.laytimeID) {
      openNotificationWithIcon("info", "Please Create Laytime First.", 5);
      return
    }
    const responseReport = await getAPICall(
      `${URL_WITH_VERSION}/laytime-calculator/port_laytime_report?e=${
        this.state.laytimeID
      }`
    );
    let respDataReport = await responseReport["data"];
    respDataReport = {
      ...respDataReport,
      extradata: this.state.formData[".."],
    };
    if (responseReport && responseReport['data']) {
      this.setState({ ...this.state, reportFormData: respDataReport }, () =>
        this.setState({
          ...this.state,
          isShowPortLaytimeReports: showPortLaytimeReports,
        })
      );
    } else {
      openNotificationWithIcon("error", "Unable to show report", 5);
    }
  };


  componentDidUpdate = async (prevProps, prevState) => {
    if (prevState.formData !== this.state.formData) {
      const { oldFormData } = this.state;
      let charter = oldFormData["all_port"].map(
        (e) => (e && e["s"] ? e : undefined)
      );
      let a = charter.filter((element) => {
        return element !== undefined;
      });
      charter["."] = a;

      charter["."].map((item) => {
        item.port_name = item.port_id;
        item.quantity = item.bl_qty;
        item.ld_rate = item.ld_rate_d;
        item.terms = item.term;

        return true;
      });
    }
  };

  saveFormData = async (postData, innerCB) => {
    const { frmName, extraFieldValues } = this.state;
    let _url = "save";
    let _method = "post";
    // let newData = Object.assign({}, postData, { efv: extraFieldValues });
    let newData = Object.assign({}, postData);
    let ports = [];

    
    newData["."].map((e, i) => {
    

      // const{ld_rate,funct,portid:port_id,port_name,quantity,cargo,terms,term_factor,allowed,dem_rate,dispatch_rate,balance,deduction,counting_time,used,lay_id}=e

      // ports.push({ld_rate,funct,port_id,port_name,quantity,cargo,terms,term_factor,allowed,dem_rate,dispatch_rate,balance,deduction,counting_time,used,lay_id})

      let keyVal = {};
      keyVal.lay_id = e.lay_id;
      keyVal.index = i
      keyVal.port_name = e.port_name;
      keyVal.cargo = e.cargo;
      keyVal.quantity = e.quantity;
      keyVal.funct = e.funct;
      keyVal.ld_rate = e.ld_rate;
      keyVal.terms = e.terms;
      keyVal.dem_rate = e.dem_rate;
      keyVal.dispatch_rate = e.dispatch_rate;
      keyVal.term_factor =
        typeof e.term_factor !== "undefined" ? e.term_factor : "";
      keyVal.counting_time =
        typeof e.counting_time !== "undefined" ? e.counting_time : "";

      if (this.state.checkEdit === "edit") {
        keyVal.id = e.id;
        keyVal.port_id = e.portid;
      } else {
        keyVal.port_id = e.portid;
      }

      keyVal.allowed = typeof e.allowed !== "undefined" ? e.allowed : 0;
      keyVal.used = typeof e.used !== "undefined" ? e.used : 0;
      keyVal.deduction = typeof e.deduction !== "undefined" ? e.deduction : 0;
      keyVal.balance = typeof e.balance !== "undefined" ? e.balance : 0;
      keyVal.lacyan_from =
        typeof e.lacyan_from !== "undefined" ? e.lacyan_from : "";
      keyVal.lacyan_to = typeof e.lacyan_to !== "undefined" ? e.lacyan_to : "";
      keyVal.turntime = typeof e.turntime !== "undefined" ?e.turntime : "";
      ports.push(keyVal);
    });


    delete newData['.'];
    let calculationImports = {};
    calculationImports.calculation =
      typeof newData["..."].calculation !== "undefined"
        ? newData["..."].calculation
        : 0;
    // calculationImports.laytime_expires =
    //   typeof newData["..."].laytime_expires !== "undefined"
    //     ? newData["..."].laytime_expires
    // //     : false;
    // calculationImports.once_always_on_dem =
    //   typeof newData["..."].once_always_on_dem !== "undefined"
    //     ? newData["..."].once_always_on_dem
    //     : false;
    // calculationImports.include_in_pl =
    //   typeof newData["..."].include_in_pl !== "undefined"
    //     ? newData["..."].include_in_pl
    //     : 0;
    // calculationImports.continues_laytime =
    //   typeof newData["..."].continues_laytime !== "undefined"
    //     ? newData["..."].continues_laytime
    //     : 0;
    // calculationImports.hhmm_format =
    //   typeof newData["..."].hhmm_format !== "undefined"
    //     ? newData["..."].hhmm_format
    //     : 0;
    // calculationImports.net_used_time =
    //   typeof newData["..."].net_used_time !== "undefined"
    //     ? newData["..."].net_used_time
    //     : 0;
    calculationImports.allowed_days =
      typeof newData["..."].allowed_days !== "undefined"
        ? newData["..."].allowed_days
        : 0;
    calculationImports.used_days =
      typeof newData["..."].used_days !== "undefined"
        ? newData["..."].used_days
        : 0;
    calculationImports.balance_days =
      typeof newData["..."].balance_days !== "undefined"
        ? newData["..."].balance_days
        : 0;
    calculationImports.origional_claim_amount =
      typeof newData["..."].origional_claim_amount !== "undefined"
        ? newData["..."].origional_claim_amount
        : 0;
    // calculationImports.currency =
    //   typeof newData["..."].currency !== "undefined"
    //     ? newData["..."].currency
    //     : 0;
    // calculationImports.exch_rate =
    //   typeof newData["..."].exch_rate !== "undefined"
    //     ? newData["..."].exch_rate
    //     : 0;
    calculationImports.amount_paid_to_owner =
      typeof newData["..."].amount_paid_to_owner !== "undefined"
        ? newData["..."].amount_paid_to_owner
        : 0;
    calculationImports.demurrage_amount =
      typeof newData["..."].demurrage_amount !== "undefined"
        ? newData["..."].demurrage_amount
        : 0;
    // calculationImports.agreed_demurrage =
    //   typeof newData["..."].agreed_demurrage !== "undefined"
    //     ? newData["..."].agreed_demurrage
    //     : 0;
    // calculationImports.agreed_despatch =
    //   typeof newData["..."].agreed_despatch !== "undefined"
    //     ? newData["..."].agreed_despatch
    //     : 0;
    // calculationImports.difference =
    //   typeof newData["..."].difference !== "undefined"
    //     ? newData["..."].difference
    //     : 0;
    // calculationImports.difference_reason =
    //   typeof newData["..."].difference_reason !== "undefined"
    //     ? newData["..."].difference_reason
    //     : "";
    // calculationImports.calc_remarks =
    //   typeof newData["..."].calc_remarks !== "undefined"
    //     ? newData["..."].calc_remarks
    //     : "";
    calculationImports.remark =
      typeof newData["..."].remark !== "undefined"
        ? newData["..."].remark
        : "";        
    calculationImports.demurrage_dispatch =
      typeof newData["..."].demurrage_dispatch !== "undefined"
        ? newData["..."].demurrage_dispatch
        : "";
    calculationImports.id =
      typeof newData["..."].id !== "undefined" ? Number(newData["..."].id) :0;

      
    calculationImports.final_total_claim =
      typeof newData["..."].final_total_claim !== "undefined"
        ? newData["..."].final_total_claim
        : "";

    calculationImports.other_extra_time =
      typeof newData["..."].other_extra_time !== "undefined"
        ? newData["..."].other_extra_time
        : "";

    let totalSum = [
      {
        editable: true,
        index: 0,
        key: newData[".."][0].key,
        total_allowed: newData[".."][0].total_allowed,
        total_balance: newData[".."][0].total_balance,
        total_deduction: newData[".."][0].total_deduction,
        total_used: newData[".."][0].total_used,
      },
    ];


   let data1=[]
    newData["activities"].map(el=>{
      let el1={
        ...el,
      'from_date':  moment(el.from_date).format('YYYY-MM-DD HH:mm'),
      }
      data1.push(el1);
    })
    newData['activities']=[...data1]
  
  
    let sendPostData = {
      // id: newData.id,

      l_status: newData.l_status,
      currency: newData.currency,
     "...": calculationImports,
      "..": totalSum,
      ".": ports.map((a) => ({ ...a })),
      activities: newData.activities,
      ports: ports,
      charterer: newData.charterer,
      charterer_id:
        typeof newData.charterer !== "undefined" ? newData.charterer : null,
      cp_date: moment(newData.cp_date).format(),
      cp_form: newData.cp_form,
      dem_disch_rate:
        typeof newData.dem_disch_rate !== "undefined"
          ? newData.dem_disch_rate
          : 0,
      dem_load_rate:
        typeof newData.dem_load_rate !== "undefined"
          ? newData.dem_load_rate
          : 0,
      laytime_commence:
        typeof newData.laytime_commence !== "undefined"
          ? newData.laytime_commence
          : null,
      laytime_completed:
        typeof newData.laytime_completed !== "undefined"
          ? newData.laytime_completed
          : null,
      operation_type:
        typeof newData.operation_type !== "undefined"
          ? newData.operation_type
          : null,
      vessel_code:
        typeof newData.vessel_code !== "undefined" ? newData.vessel_code : null,
      vessel_id:
        typeof newData.vessel_id !== "undefined" ? newData.vessel_id : null,
      operation_type:
        typeof newData.ops_type !== "undefined" ? newData.ops_type : null,
      voyno:
        typeof newData.voyno !== "undefined" ? newData.voyno : null,
      calculation: typeof newData.calculation !== NaN ? newData.calculation : 0,
      analyst: typeof newData.analyst !== "undefined" ? newData.analyst : null,
      counting: typeof newData.counting !== "undefined" ? newData.counting : 0,
      invoice_number:
        typeof newData.invoice_number !== "undefined"
          ? newData.invoice_number
          : 0,
      totalsum:
        typeof newData.totalsum !== "undefined" ? newData.totalsum : null,
      efv: extraFieldValues,
      currency:
        typeof newData.currency !== "undefined" ? newData.currency : null,
      exch_rate:
        typeof newData.exch_rate !== "undefined" ? newData.exch_rate : null,
      laycan_from:
        typeof newData.laycan_from !== "undefined" ?newData.laycan_from: null,
      laycan_to:
        typeof newData.laycan_to !== "undefined" ?newData.laycan_to: null,
    };

    // let sendPostDataTemp = Object.assign({}, sendPostData, { efv: extraFieldValues });
    
    this.state.tabNames.map((e, i) => {
      for (var key in newData) {
        if (
          e
            .split(" ")
            .join("")
            .toLowerCase() == key
        ) {
          sendPostData[key] = newData[key];
        }
      }
    });

    if (this.state.checkEdit === "edit") {
      sendPostData.id = this.state.voyID;

      if (postData.hasOwnProperty("cp_date") && postData["id"] > 0) {
        _url = "update";
        _method = "PATCH";
      }
    } else {
      let responseReport = await getAPICall(
        `${URL_WITH_VERSION}/make_payment/generate/invoice_no`
      );
      let respDataReport = await responseReport["data"];
      sendPostData.invoice_number = respDataReport.generatedCode;

      if (postData.hasOwnProperty("cp_date") && postData["id"] > 0) {
        _url = "save";
        _method = "post";
      }
    }
    this.formDataref.current = Object.assign({}, sendPostData);
    this.setState({ ...this.state, refreshForm: true });
   
    postAPICall(
      `${URL_WITH_VERSION}/laytime-calculator/${_url}?frm=${frmName}`,
      sendPostData,
      _method,
      (data) => {
        
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          this.setState({...this.state,refreshForm:false,laytimeID:data.row && data.row.lay_id,checkEdit:"edit"})

           if (
            this.props.modalCloseEvent &&
            typeof this.props.modalCloseEvent === "function"
          ) {
              this.props.modalCloseEvent();
            }
          
          // if (
          //   this.props.modalCloseEvent &&
          //   typeof this.props.modalCloseEvent === "function"
          // ) {
          
          //   this.props.modalCloseEvent();
          //   // if (this.props.fullEstimate) {
          //   //   this.props.getCargo(data.row);
          //   // }
          // } else if (innerCB && typeof innerCB === "function") {
          //   this.props.modalCloseEvent();
          // }
          this.setState({ ...this.state, refreshForm: false });
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />,
            5
          );
          this.setState({ ...this.state, refreshForm: false });
        }
       
      }
    );
  };

  onChangeEvt = (event) => {
    const { extraFieldValues } = this.state;
    let extraFieldValuesSet = extraFieldValues;
    extraFieldValuesSet[event.target.name] = event.target.value;
    this.setState({ ...this.state, extraFieldValues: extraFieldValuesSet });
  };

  getExternalFormFields = (frmData) => {
    return <></>;
  };

  _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Laytime Id is empty. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => this._onDelete(postData),
    });
  };

  _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/laytime-calculator/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        if (
          this.props.modalCloseEvent &&
          typeof this.props.modalCloseEvent === "function"
        ) {
          this.props.modalCloseEvent();
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };


  render() {
    const {
      frmName,
      formData,
      extraFormFields,
      OG,
      tabs,
      tabNames,
      extraTableButtons,
      isShowClaimInvoice,
      isShowPortLaytimeReports,
      isShowSummaryReports,
      isShowPortCalcullationReport,
      reportFormData,
      oldFormData,
      refreshForm,
      frmOptions,
      laytimeID
    } = this.state;

    return (
      <>
        {extraFormFields ? (
          <div className="tcov-wrapper full-wraps">
            <Layout className="layout-wrapper">
              <Layout>
                <Content className="content-wrapper">
                  <div className="fieldscroll-wrap">
                    <div className="body-wrapper">
                      <article className="article">
                        <div className="box box-default">
                          <div className="box-body common-fields-wrapper roushan">
                            {/* <h4 className="cust-header-title">
                              <b>New Laytime / {formData["voyage_no"]}</b>
                            </h4> */}

                            {refreshForm == false ? (
                          
                              <NormalFormIndex
                                key={"key_" + frmName + "_0"}
                                formClass="label-min-height"
                                formData={Object.assign(
                                  { id: 0 },
                                  this.formDataref.current
                                )}
                                showForm={true}
                                frmCode={frmName}
                                addForm={true}
                                extraFormFields={extraFormFields}
                                editable={true}
                                frmOptions={frmOptions}
                                showButtons={[
                                  {
                                    id: "cancel",
                                    title: "Cancel",
                                    type: "danger",
                                    event: "convertToDays()",
                                  },
                                  {
                                    id: "save",
                                    title: "Save",

                                    type: "primary",
                                    event: (data, innerCB) => {
                                      this.saveFormData(data, innerCB);
                                    },
                                  },
                                ]}
                                showToolbar={[
                                  {
                                    isLeftBtn: [
                                      // {
                                      //   isSets: [
                                      //     { id: '3', key: 'save', type: <SaveOutlined />, withText: '', event: (data, innerCB) => { this.saveFormData(data, innerCB); } },
                                      //     { id: '5', key: 'delete', type: <DeleteOutlined />, withText: '' },
                                      //   ],
                                      // },
                                      {
                                        isSets: [
                                          laytimeID&&{
                                            id: "4",
                                            key: "delete",
                                            type: <DeleteOutlined />,
                                            withText: "Delete",
                                            showToolTip: true,
                                            event: (key, data) =>
                                              this._onDeleteFormData(data),
                                          },
                                          { id: '3', key: 'save', type: '', withText: `New Laytime / ${formData&&formData["voyno"]}`,}
                                        
                                        ]
                                      },
                                    ],

                                    isRightBtn: [
                                      {
                                        isSets: [
                                          {
                                            key: "reports",
                                            isDropdown: 1,
                                            withText: "Reports",
                                            type: "",
                                            menus: [
                                              {
                                                href: null,
                                                icon: null,
                                                label: " Port Laytime Reports",
                                                modalKey: "port_laytim_report",
                                                event: (key) =>
                                                  this.PortLaytimeReports(true),
                                              },
                                              {
                                                href: null,
                                                icon: null,
                                                label: " Summary Reports",
                                                modalKey: "summary_report",
                                                event: (key) =>
                                                  this.SummaryReports(true),
                                              },
                                            ],
                                          },
                                          {
                                            key: "claim_invoice",
                                            isDropdown: 0,
                                            withText: "Claim Invoice",
                                            type: "",
                                            menus: null,
                                            event: (key) =>
                                              this.ClaimInvoice(true),
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ]}
                                inlineLayout={true}
                                isShowFixedColumn={tabNames}
                                formGroupDevideInCols={{ "....": "1" }}
                                dyncTabs={{
                                  copyTab:
                                    "tab_new_laytime_port_form_all_ports",
                                  tabs: tabs,
                                }}
                               extraTableButton={extraTableButtons}
                                tableRowDeleteAction={(action, data) =>
                                  this.onClickExtraIcon(action, data)
                                }

                                //ogState={OG}
                                // summary={[{
                                //   "gKey": ".", "showTotalFor": ["allowed", "used", "deduction", "balance"], "dhm": true
                                // }]}
                              />
                             
                            ) : (
                              // <Spin tip="Loading...">
                              //   <Alert
                              //     message=" "
                              //     description="Please wait..."
                              //     type="info"
                              //   />
                              // </Spin>
                              undefined
                            )}
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>
                </Content>
              </Layout>
            </Layout>
          </div>
        ) : (
          <div />
        )}
        {isShowPortLaytimeReports ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title={
              <h2
                style={{
                  fontWeight: "600",
                  // textTransform: "uppercase",
                  textAlign: "center",
                }}
              >
                
              </h2>
            }
           open={isShowPortLaytimeReports}
            onOk={this.handleOk}
            onCancel={() => this.setState({ isShowPortLaytimeReports: false })}
            width="90%"
            footer={null}
          >
            <PortLaytimeReports {...reportFormData} />
          </Modal>
        ) : (
          undefined
        )}

        {isShowSummaryReports ? (
          <Modal
            style={{ top: "2%" }}
            title="Summary Report"
           open={isShowSummaryReports}
            onOk={this.handleOk}
            onCancel={() => this.setState({ isShowSummaryReports: false })}
            width="90%"
            footer={null}
          >
            <SummaryReports {...reportFormData} />
          </Modal>
        ) : (
          undefined
        )}

        {isShowClaimInvoice ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Create Invoice"
           open={isShowClaimInvoice}
            onOk={this.handleOk}
            onCancel={() => this.ClaimInvoice(false)}
            width="90%"
            footer={null}
          >
            {/* <OtherExpenseModal /> */}
            <ClaimInvoice
              formDataValues={formData}
              frmData={oldFormData}
              closeModal={() => this.ClaimInvoice(false)}
              laytimeID={laytimeID}
            />
          </Modal>
        ) : (
          undefined
        )}

        {isShowPortCalcullationReport ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Port Calculations Report"
           open={isShowPortCalcullationReport}
            onOk={this.handleOk}
            onCancel={() => this.PortCalcullationReport(false)}
            width="90%"
            footer={null}
          >
            <PortCalcullationReport
              formDataValues={formData}
              frmData={oldFormData}
            />
          </Modal>
        ) : (
          undefined
        )}
      </>
    );
  }
}
