import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    
    const formReportdata ={}
    
    this.state = {
      
      
      formReportdata:Object.assign(formReportdata, this.props.data || {}),
    }
    
  }

  componentDidMount = (props) => {
    console.log(this.state.formReportdata)
  }

  render() {
    const{formReportdata} = this.state
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Counter Party :</td>
                      <td>{formReportdata.counterparty_qty}</td>

                      <td className="font-weight-bold">Demurrage Analyst :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Claim Number :</td>
                      <td>{formReportdata.analyst}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">B/L Date :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Report Date (GMT) :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Total Qty :</td>
                      <td>{formReportdata.analyst}</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Loading Port :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Cargo :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Load Rate :</td>
                      <td>{formReportdata.analyst}</td>

                      <td className="font-weight-bold">Terms :</td>
                      <td>{formReportdata.terms}</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Port Activity</th>
                      <th>Remark</th>
                      <th>Date Time</th>
                    </tr>
                  </thead>
                  <tbody>
                  {formReportdata.singapore && formReportdata.singapore && 
                    formReportdata.singapore.length > 0 ? formReportdata.singapore.map((e, idx)=>{
                        return (
                          <>
             
                <tr key={idx}>
                      <td>{e.activity}</td>
                      <td>{e.remarks}</td>
                      <td>{e.last_updated_by}</td>
                    </tr>

                    </>
                 )
                    }):undefined

                    }
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Laytime Calculation</th>
                      <th>Start (Date/Time)</th>
                      <th>End (Date Time)</th>
                      <th>%</th>
                      <th>Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Cargo Allocation</th>
                      <th>Counter Party (Qty)</th>
                      <th>Total (Qty)</th>
                      <th>Percent</th>
                      <th>Total</th>
                      <th>Allocated</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <th>Used Laytime</th>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <th>Allowed Laytime</th>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <th>Minimum Allowed Laytime</th>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <th>Grace Time</th>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <th>Demurrage Time</th>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <th>Demurrage Due</th>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class PortCalcullationReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  componentDidMount = (props) => {
    console.log(this.props)
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                          <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
            <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.formDataValues} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortCalcullationReport;
