import React, { Component } from "react";
import ReactToPrint from "react-to-print";
import { PrinterOutlined,FilePdfOutlined } from '@ant-design/icons';
import "../../../shared/components/All-Print-Reports/print-report.scss";
import jsPDF from "jspdf";
import { Modal , Spin } from "antd";

import html2canvas from "html2canvas";
import moment from "moment";
import Email from "../../../components/Email";
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activity: Object.assign([], this.props.port_activities || []),
      cal_imports: Object.assign({}, this.props.cal_imports || {}),
      cargo: Object.assign([], this.props.cargo || []),
      laytime: Object.assign({}, this.props.laytime || {}),
      extra_data: Object.assign({}, this.props.extradata[0] || {}),
    };
  }

  getTotalDays(durationString) {
    const durationRegex = /^-?\d+D:-?\d+H:-?\d+M$/;
    if (!durationRegex.test(durationString)) return 0;

    const durationParts = durationString.split(":");
    const days = parseInt(durationParts[0].replace("D", ""));
    const hours = parseInt(durationParts[1].replace("H", ""));
    const minutes = parseInt(durationParts[2].replace("M", ""));
    const totalDays = (days + hours / 24 + minutes / (24 * 60)).toFixed(2);

    return totalDays;
  }

  filterZeroDay(day) {
    if (!day || day === "0000-00-00 00:00:00") return "N/A";
    return day;
  }

  render() {
    const {
      activity,
      cal_imports,
      cargo,
      laytime,
      other_details,
      extra_data,
    } = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-wrapper">
              <section className="invoice-container" id="invoice">
                <div className="row laytime-heading">
                  <div className="col-12">
                    <p className="text-center font-weight-bold h2 text-decoration-underline">
                      REPORT
                    </p>
                  </div>
                </div>
                <div className="invoice-inner ">
                  <div className="row laytime-heading">
                    <div className="col-12 ">
                      <p className="main-heading laytime-subheading laytime-port-heading">
                        Laytime Summary Report
                      </p>
                    </div>
                  </div>
                  <div className="row view-list laytime-report">
                    <div className="col-6">
                      <table className="custom-table-bordered">
                        <thead className="laytime-port-table">
                          <tr className="HeaderBoxText">
                            <th>Port Name</th>
                            <th>Demurrage</th>
                            <th>Dispatch</th>
                          </tr>
                        </thead>
                        <tbody>
                          {cargo && cargo.length > 0
                            ? cargo.map((e, idx) => {
                                return (
                                  <tr key={idx}>
                                    <td>{e.port_name ? e.port_name : "N/A"}</td>
                                    <td>{e.dem_rate ? e.dem_rate : "N/A"}</td>
                                    <td>
                                      {e.dispatch_rate
                                        ? e.dispatch_rate
                                        : "N/A"}
                                    </td>
                                  </tr>
                                );
                              })
                            : undefined}
                        </tbody>
                      </table>
                    </div>
                    <div className="col-6">
                      <table className="custom-table-bordered ">
                        <thead className="laytime-port-table">
                          <tr className="HeaderBoxText">
                            <th className="laytime-port-table">
                              Calculation Term
                            </th>
                            <th className="laytime-port-table">Vessel Name</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              {cal_imports["calculation_name"]
                                ? cal_imports["calculation_name"]
                                : "N/A"}
                            </td>

                            <td>
                              {laytime["vessel_name"]
                                ? laytime["vessel_name"]
                                : "N/A"}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div className="row view-list laytime-report">
                    <div className="row laytime-heading">
                      <div className="col-12 ">
                        <p className="main-heading laytime-subheading laytime-port-heading">
                          Load & Discharge Port
                        </p>
                      </div>
                    </div>
                    <table className="custom-table-bordered ">
                      <thead className="laytime-port-table">
                        <tr className="HeaderBoxText">
                          <th>Activity</th>
                          <th>Port</th>
                          <th>CP Qty</th>
                          <th>L/D Qty(Day)</th>
                          <th>Allowed L/D Time</th>
                          <th>Allowed Days</th>
                          <th>Terms</th>
                          <th>Balance Time (Over/Under)</th>
                          <th>Balance (Days)</th>
                          <th>Turntime</th>
                        </tr>
                      </thead>
                      <tbody>
                        {cargo && cargo.length > 0
                          ? cargo.map((e, idx) => {
                              return (
                                <tr key={idx}>
                                  <td>
                                    {e["funct_desc_name"]
                                      ? e["funct_desc_name"]
                                      : "N/A"}
                                  </td>
                                  <td>
                                    {e["port_name"] ? e["port_name"] : "N/A"}
                                  </td>
                                  <td>
                                    {e["quantity"] ? e["quantity"] : "N/A"}
                                  </td>
                                  <td>{e["ld_rate"] ? e["ld_rate"] : "N/A"}</td>
                                  <td>{e["allowed"] ? e["allowed"] : "N/A"}</td>
                                  <td>
                                    {e["allowed"]
                                      ? this.getTotalDays(e["allowed"])
                                      : "N/A"}
                                  </td>
                                  <td>
                                    {e["short_name"] ? e["short_name"] : "N/A"}
                                  </td>
                                  <td>{e["balance"] ? e["balance"] : "N/A"}</td>
                                  <td>
                                    {e["balance"]
                                      ? this.getTotalDays(e["balance"])
                                      : "N/A"}
                                  </td>
                                  <td>
                                    {e["turntime"] ? e["turntime"] : "N/A"}
                                  </td>
                                </tr>
                              );
                            })
                          : undefined}
                      </tbody>
                    </table>

                    <div className="col-12 invoice-sum ">
                      <ul className="list-unstyled float-right laytime-port-list">
                        <li className="laytime-invoice-list">
                          <span>Total Demurrage Time (Days):</span>{" "}
                          <span>
                            {cal_imports["allowed_days"]
                              ? cal_imports["allowed_days"]
                              : "N/A"}
                          </span>
                        </li>
                        <li className="laytime-invoice-list">
                          <span>Final Disp./Dem. Amount:</span>{" "}
                          <span>
                            {cal_imports["demurrage_amount"]
                              ? cal_imports["demurrage_amount"]
                              : "N/A"}{" "}
                            /{" "}
                            {cal_imports["demurrage_dispatch_name"]
                              ? cal_imports["demurrage_dispatch_name"]
                              : "N/A"}
                          </span>
                        </li>
                        <li className="laytime-invoice-list">
                          <span>Other Adjustment:</span>{" "}
                          <span>
                            {cal_imports["amount_paid_to_owner"]
                              ? cal_imports["amount_paid_to_owner"]
                              : "N/A"}
                          </span>
                        </li>
                        <li className="laytime-invoice-list">
                          <span>Other Extra Time/Amount:</span>{" "}
                          <span>
                            {cal_imports["other_extra_time"]
                              ? cal_imports["other_extra_time"]
                              : "N/A"}
                            /
                            {cal_imports["origional_claim_amount"]
                              ? cal_imports["origional_claim_amount"]
                              : "N/A"}
                          </span>
                        </li>
                        <li className="laytime-invoice-list">
                          <span>Final Total Claim:</span>{" "}
                          <span>
                            {cal_imports["final_total_claim"]
                              ? cal_imports["final_total_claim"]
                              : "N/A"}
                          </span>
                        </li>
                        <li className="laytime-invoice-list">
                          <span>Remarks:</span>{" "}
                          <span>
                            {cal_imports["remark"]
                              ? cal_imports["remark"]
                              : "N/A"}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  
                    <div className="row laytime-heading">
                      <div className="col-12">
                        <p className="sub-heading laytime-subheading laytime-port-heading">
                          Voyage Details
                        </p>
                      </div>
                      <div className="row view-list laytime-report">
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Charterers</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {laytime["charterer_name"]
                                    ? laytime["charterer_name"]
                                    : "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Owners</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {laytime["owner"] ? laytime["owner"] : "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Laycan From</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {this.filterZeroDay(laytime["laycan_from"])}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Laycan To</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {this.filterZeroDay(laytime["laycan_to"])}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <div className="row view-list laytime-report">
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Charter Party</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {laytime["cp_from_name"]
                                    ? laytime["cp_from_name"]
                                    : "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>CP/BL Dated</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {laytime["cp_date"]
                                    ? laytime["cp_date"]
                                    : "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3">
                          <table className="custom-table-bordered ">
                            <thead className="laytime-port-table">
                              <tr className="HeaderBoxText">
                                <th>Voyage No</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  {laytime["voyage_no"]
                                    ? laytime["voyage_no"]
                                    : "N/A"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div className="col-3" />
                      </div>
                    </div>
                  
                  {cargo && cargo.length > 0
                    ? cargo.map((e, idx) => {
                        return (
                          <div className="row laytime-heading" key={idx}>
                            <div className="col-12">
                              <div className="sub-heading">
                                <p className="laytime-port-name">
                                  {e["port_name"] ? e["port_name"] : ""} -{" "}
                                  {e["funct_desc_name"]
                                    ? e["funct_desc_name"]
                                    : ""}
                                </p>
                              </div>
                            </div>
                            <div className="row view-list laytime-report">
                              <div className="col-4">
                                <table className="custom-table-bordered ">
                                  <thead className="laytime-port-table">
                                    <tr className="HeaderBoxText">
                                      <th>Laytime Allowed (Days)</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["allowed"]
                                          ? this.getTotalDays(e["allowed"])
                                          : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                  <thead className="laytime-port-table">
                                    <tr className="HeaderBoxText">
                                      <th>Used time (Time Taken)</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["used"]
                                          ? this.getTotalDays(e["used"])
                                          : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div className="col-4">
                                <table className="custom-table-bordered ">
                                  <thead className="laytime-port-table">
                                    <tr className="HeaderBoxText">
                                      <th>Laytime Commence</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["calculation"] &&
                                        e["calculation"].length > 0
                                          ? e["calculation"]
                                              .map((c, idx) => {
                                                return c["activity_name"] ===
                                                  "LAYTIME COMMENCED"
                                                  ? c["from_date"]+" "+this.filterZeroDay(
                                                      c["from_time"]
                                                    )
                                                  : "";
                                              })
                                              .join("") || "N/A"
                                          : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                  <thead className="laytime-port-table">
                                    <tr className="HeaderBoxText">
                                      <th>Balance Days(Over/Under)</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["balance"] ? e["balance"] : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div className="col-4">
                                <table className="custom-table-bordered ">
                                  <thead className="laytime-port-table">
                                    <tr className="HeaderBoxText">
                                      <th>Laytime Completed</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["calculation"] &&
                                        e["calculation"].length > 0
                                          ? e["calculation"]
                                              .map((c, idx) => {
                                                return c["activity_name"] ===
                                                  "LAYTIME COMPLETED"
                                                  ? c["to_date"]+" "+c["to_time"]
                                                  : "";
                                              })
                                              .join("") || "N/A"
                                          : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                  <thead className="laytime-port-table">
                                    <tr>
                                      <th>B/L Weight/Quantity</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {e["quantity"] ? e["quantity"] : "N/A"}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        );
                      })
                    : undefined}
                  {cargo && cargo.length > 0
                    ? cargo.map((e, idx) => {
                        return (
                          <div
                            key={idx}
                            style={{
                              pageBreakInside: "avoid",
                              pageBreakAfter: "always",
                            }}
                          >
                            <div className="row laytime-heading">
                              <div className="col-12">
                                <p className="sub-heading laytime-subheading laytime-port-heading">
                                  Laytime Calculaiton Report
                                </p>
                              </div>

                              <div className="row view-list laytime-report">
                                <div className="col-4">
                                  <table className="custom-table-bordered ">
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Port Function</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["funct_desc_name"]
                                            ? e["funct_desc_name"]
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Laytime Allowed (Days)</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["allowed"]
                                            ? this.getTotalDays(e["allowed"])
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Used time/Time Taken (Days)</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["used"]
                                            ? this.getTotalDays(e["used"])
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr>
                                        <th>Vessel Arrival Time</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["calculation"] &&
                                          e["calculation"].length > 0
                                            ? e["calculation"]
                                                .map((c, idx) => {
                                                  return c["activity_name"] ===
                                                    "VESSEL ARRIVED"
                                                    ? this.filterZeroDay(
                                                        c["to_time"]
                                                      )
                                                    : "";
                                                })
                                                .join("") || "N/A"
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div className="col-4">
                                  <table className="custom-table-bordered ">
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Port</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["port_name"]
                                            ? e["port_name"]
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Laytime Starts</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["calculation"] &&
                                          e["calculation"].length > 0
                                            ? e["calculation"]
                                                .map((c, idx) => {
                                                  return c["activity_name"] ===
                                                    "LAYTIME COMMENCED"
                                                    ? c["from_date"]+" "+this.filterZeroDay(
                                                        c["from_time"]
                                                      )
                                                    : "";
                                                })
                                                .join("") || "N/A"
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Balance Time(Time Over/Under )</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          <div class="row">
                                            <div class="col">
                                              <span>
                                                {e["balance"]
                                                  ? this.getTotalDays(
                                                      e["balance"]
                                                    )
                                                  : "N/A"}
                                              </span>
                                            </div>
                                            <div class="col">
                                              <span>|</span>
                                            </div>
                                            <div class="col">
                                              <span>
                                                {e["balance"]
                                                  ? e["balance"]
                                                  : "N/A"}
                                              </span>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Commencement</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["calculation"] &&
                                          e["calculation"].length > 0
                                            ? e["calculation"]
                                                .map((c, idx) => {
                                                  return c["activity_name"] ===
                                                    "HOSES CONNECTED"
                                                    ? c["to_date"]+" "+this.filterZeroDay(
                                                        c["to_time"]
                                                      )
                                                    : "";
                                                })
                                                .join("") || "N/A"
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div className="col-4">
                                  <table className="custom-table-bordered ">
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Cargo</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["cargo_name"]
                                            ? e["cargo_name"]
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Laytime Ends</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["calculation"] &&
                                          e["calculation"].length > 0
                                            ? e["calculation"]
                                                .map((c, idx) => {
                                                  return c["activity_name"] ===
                                                    "LAYTIME COMPLETED"
                                                    ? c["to_date"]+" "+c["to_time"]
                                                    : "";
                                                })
                                                .join("") || "N/A"
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Turn Time (H)</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["turntime"]
                                            ? e["turntime"]
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                    <thead className="laytime-port-table">
                                      <tr className="HeaderBoxText">
                                        <th>Completion</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          {e["calculation"] &&
                                          e["calculation"].length > 0
                                            ? e["calculation"]
                                                .map((c, idx) => {
                                                  return c["activity_name"] ===
                                                    "HOSES DISCONNECTED"
                                                    ? c["to_date"]+" "+this.filterZeroDay(
                                                        c["to_time"]
                                                      )
                                                    : "";
                                                })
                                                .join("") || "N/A"
                                            : "N/A"}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                            <div className="row  laytime-heading">
                              <table className="custom-table-bordered">
                                <thead className="laytime-port-table">
                                  <tr className="HeaderBoxText">
                                    <th>Days</th>
                                    <th>Start Date</th>
                                    <th>Time</th>
                                    <th>End Date</th>
                                    <th>Time</th>
                                    <th>To Count %</th>
                                    <th>Activity </th>
                                    <th>Total Time</th>
                                    <th>Counting Time</th>
                                    <th>Remark</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {e.calculation && e.calculation.length > 0
                                    ? e.calculation.map((a, idx) => {
                                        return (
                                          <tr
                                            key={idx}
                                            style={{
                                              backgroundColor:
                                                a.activity_name ===
                                                  "LAYTIME COMPLETED" ||
                                                a.activity_name ===
                                                  "LAYTIME COMMENCED"
                                                  ? "#80ff00"
                                                  : a.activity_name ===
                                                    "LAYTIME EXPIRES"
                                                    ? "#ff8080"
                                                    : "",
                                            }}
                                          >
                                            <td>
                                              {a["from_date"]
                                                ? moment(a["from_date"]).format(
                                                    "ddd"
                                                  )
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["from_date"]
                                                ? a["from_date"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["from_time"]
                                                ? a["from_time"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["to_date"]
                                                ? a["to_date"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["to_time"]
                                                ? a["to_time"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["counting"]
                                                ? a["counting"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["activity_name"]
                                                ? a["activity_name"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["duration"]
                                                ? a["duration"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["counting_time"]
                                                ? a["counting_time"]
                                                : "N/A"}
                                            </td>
                                            <td>
                                              {a["remarks"]
                                                ? a["remarks"]
                                                : "N/A"}
                                            </td>
                                          </tr>
                                        );
                                      })
                                    : undefined}
                                </tbody>
                              </table>
                            </div>
                          </div>
                        );
                      })
                    : undefined}
                </div>
              </section>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class PortLaytimeReports extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "Printer",
      pdfData:"",
      userInput: "",
      emailModal:false,
      loading:false,
      mailTitlePayload:{},
    };
  }

  printDocument() {
    var quotes = document.getElementById("divToPrint");

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true,
      scale: 2,
    }).then(function(canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF("p", "mm");
      var position = 20;
      var pageData = canvas.toDataURL("image/jpeg", 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          imgData,
          "PNG",
          5,
          position + 20,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save("Port_Laytime_Report.pdf");
    });
  }

  printReceipt() {
    window.print();
  }



   sendEmailReportModal = async () => {
    try {
      
      this.setState({ loading: true });
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      
  
      this.setState({
        loading: false,
        pdfData: blob,
        emailModal: true,
      });
  
    } catch (error) {
      console.error('Error:', error);
      this.setState({ loading: false });
      // Handle errors here
    }
  };


  render() {
    // console.log('portttt',this.props.data.port_name);
    // console.log('daaaa',this.props);
    // console.log('portt22',this.props.laytime.vessel_name);
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">

                    <li onClick={this.sendEmailReportModal}>Send Email</li>

                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                          <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                      {/* <li>
                        <span className="text-bt">
                          <Icon type="schedule" /> Preview
                        </span>
                      </li> */}
                      {/* <li>
                        <span className="text-bt">
                          <SaveOutlined /> Save
                        </span>
                      </li> */}
                    </ul>
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      
                      <li onClick={this.printDocument}>
                        <span className="text-bt">
                        <FilePdfOutlined />Download PDF
                        </span>
                      </li>
                      {/* <li>
                        <span className="text-bt">
                          <Icon type="file-excel" /> Download Excel
                        </span>
                      </li>
                      <li>
                        <span className="text-bt">
                          <MailOutlined />/> Email
                        </span>
                      </li> */}
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                ref={(el) => (this.componentRef = el)}
                {...this.props}
              />
            </div>
          </div>
        </article>
        {this.state.emailModal && (
        <Modal
          title="New Message"
          visible={this.state.emailModal}
          onOk={() => {
            this.setState({ emailModal: false });
          }}
          onCancel={() => {
            this.setState({ emailModal: false });
          }}
          footer={null}
        >
          {this.state.pdfData &&<Email
           handleClose={
            () => {
              this.setState(prevState=>({
              
                emailModal:false,
              }))
              

            }
            
          }
            attachmentFile={this.state.pdfData} 
            title={window.corrector(`Port_Laytime_Report||${this.props.laytime.charterer_name}||${this.props.laytime.vessel_name}` ) }
            
            // title={`Port_Laytime_Report|| ${this.props.laytime.charterer_name} || ${this.props.laytime.vessel_name}`}
         
          />}
        </Modal>
      )}                   
          {
          this.state.loading && (
            <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
              <Spin size="large" />
            </div>
          )
        }   

      </div>
    );
  }
}

export default PortLaytimeReports;
