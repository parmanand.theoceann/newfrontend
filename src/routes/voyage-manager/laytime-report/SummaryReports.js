import React, { Component } from 'react';
import ReactToPrint from "react-to-print";
import { PrinterOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import { Modal , Spin } from "antd";
import Email from '../../../components/Email';
import '../../../shared/components/All-Print-Reports/print-report.scss';
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";
import html2canvas from "html2canvas";
import moment from "moment";
const invoicecolumns = [

    {
        title: 'Port',
        dataIndex: 'port_name',
        key: 'port_name',
        render: (text) => <span>{text[0].toUpperCase() + text.slice(1)}</span>
    },
    {
        title: 'Cargo',
        dataIndex: 'cargo_name',
        key: 'cargo_name',
        render: (text) => <span>{text[0].toUpperCase() + text.slice(1)}</span>
    },


    {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity',
        render: (data) => <span>{data.replace(",", "")}</span>
    },

    {
        title: 'L/D Rate',
        dataIndex: 'ld_rate',
        key: 'ld_rate',
    },

    {
        title: 'Term',
        dataIndex: 'short_name',
        key: 'short_name',
    },




    {
        title: 'Dem Rate',
        dataIndex: 'dem_rate',
        key: 'dem_rate',
        render: (data) => <span>{data.replace(",", "")}</span>
    },

    {
        title: 'Des Rate',
        dataIndex: 'dispatch_rate',
        key: 'dispatch_rate',
        render: (data) => <span>{data.replace(",", "")}</span>
    },








    {
        title: 'Time Allowed',
        dataIndex: 'allowed',
        key: 'allowed',
    },

    {
        title: 'Balance Day',
        dataIndex: 'balance',
        key: 'balance',
    },
    {
        title: 'Time Used',
        dataIndex: 'used',
        key: 'used',
    },

];



class ComponentToPrint extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            laytime: Object.assign({}, this.props.laytime || {}),
            FormData: Object.assign([], this.props.cargo || []),
            cal_imports: Object.assign({}, this.props.cal_imports || {}),
            other_details: Object.assign({}, this.props.other_details || {}),

        }

    }
    componentDidMount = () => {
        const { FormData } = this.state


        //     let temp = []
        //     FormData['.'].map((e, i) => {
        //         temp.push({
        //             key: i,
        //             port: e.port_name,
        //             ddrate: e.dem_rate,
        //             timeallowed: e.allowed,
        //             gracetime: e.balance,
        //             timeused: e.used,
        //             despatch: e.dispatch_rate
        //         })
        //     })
        //    // FormData['.'] = temp
        //     this.setState({...this.state,'.':temp  })

    }



    render() {
        const { laytime, FormData, cal_imports, cargo } = this.state

        return <article className="article">
            <div className="box box-default print-wrapper">
                <div className="box-body" id="divToPrint" >
                    <div className="invoice-wrapper">
                        <section className="invoice-container" id="invoice">
                            <div className="invoice-inner">
                                <div className="row laytime-heading">
                                    <div className="col-12">

                                        <p className="main-heading laytime-subheading">
                                            SUMMARY REPORT
                                        </p>
                                    </div>
                                </div>

                                <div className="row view-list">
                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled ">
                                            <li className="laytime-list my-4">
                                                <span>Vessel:</span><span>
                                                    {laytime.vessel_name ? laytime.vessel_name : " "}
                                                </span>
                                            </li>

                                            <li className="laytime-list">
                                                <span>Voyage No.:</span> <span>
                                                    {laytime.voyage_no ? laytime.voyage_no : ' '}
                                                </span>
                                            </li>

                                            {/* <li className="laytime-list">
                                                <span>Charterer:</span> <span>
                                                    {laytime.charterer_name?laytime.charterer_name:" "}
                                                </span>
                                            </li> */}
                                        </ul>
                                    </div>

                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled">
                                            <li className="laytime-list  my-4">
                                                <span>Vessel Code:</span>
                                                <span>
                                                    {laytime.vessel_code
                                                        ? laytime.vessel_code
                                                        : " "}
                                                </span>
                                            </li>

                                            <li className="laytime-list">
                                                <span>CP Date:</span>
                                                <span>
                                                    {laytime.cp_date
                                                        ? moment(laytime.cp_date).format(
                                                            "YYYY-MM-DD"
                                                        )
                                                        : " "}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled">

                                            <li className="laytime-list  my-4">
                                                <span>CP Form:</span> <span>
                                                    {laytime.cp_form_name}
                                                </span>
                                            </li>
                                            <li className="laytime-list">
                                                <span>Charterer:</span> <span>
                                                    {laytime.charterer_name ? laytime.charterer_name : " "}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>


                                </div>

                                <div className="divider my-2" />

                                <div className="row">

                                    <div className="col-md-12">
                                        <Table className="invoice-table" columns={invoicecolumns} pagination={false}
                                            dataSource={FormData}
                                        />
                                    </div>

                                </div>

                                <div className="row view-list">
                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled">
                                            <li className="laytime-invoice-list  my-4">
                                                <span>Allowed Days:</span> <span>
                                                    {cal_imports && cal_imports.allowed_days
                                                        ? cal_imports.allowed_days
                                                        : " "}
                                                </span>
                                            </li>

                                            <li className="laytime-invoice-list">
                                                <span>Used Days:</span> <span>
                                                    {" "}
                                                    {cal_imports && cal_imports.used_days
                                                        ? cal_imports.used_days
                                                        : " "}
                                                </span>
                                            </li>

                                            {/* <li className="laytime-invoice-list">
                                                <span>Balance Days:</span> <span>
                                                    {" "}
                                                    {cal_imports && cal_imports.balance_days
                                                        ? cal_imports.balance_days
                                                        : " "}
                                                </span>
                                            </li> */}
                                        </ul>
                                    </div>

                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled">
                                            <li className="laytime-invoice-list my-4">
                                                <span>Other Extra  Time/ Amount:</span> <span>
                                                    {" "}
                                                    {cal_imports &&
                                                        cal_imports.origional_claim_amount
                                                        ? cal_imports.origional_claim_amount
                                                        : " "}
                                                </span>
                                            </li>


                                            <li className="laytime-invoice-list">
                                                <span>Other adjustment:</span> <span>
                                                    {" "}
                                                    {cal_imports && cal_imports.amount_paid_to_owner
                                                        ? cal_imports.amount_paid_to_owner
                                                        : " "}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="col-4 text-left">
                                        <ul className="list-unstyled">
                                            <li className="laytime-invoice-list my-4">
                                                <span>Final Disp./Dem. Amount:</span> <span>
                                                    {" "}
                                                    {cal_imports && cal_imports.demurrage_amount
                                                        ? cal_imports.demurrage_amount
                                                        : " "}
                                                </span>
                                            </li>
                                            <li className="laytime-invoice-list">
                                                <span>Balance Days:</span> <span>
                                                    {" "}
                                                    {cal_imports && cal_imports.balance_days
                                                        ? cal_imports.balance_days
                                                        : " "}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </article>;
    }
}

class SummaryReports extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Printer',
            pdfData: "",
            userInput: "",
            emailModal: false,
            loading: false,
            mailTitlePayload: {},
        };
    }

    printReceipt() {
        window.print();
    }

    componentDidMount = (props) => {
    }



     sendEmailReportModal = async () => {
        try {
          
          this.setState({ loading: true });
      
          const quotes = document.getElementById('divToPrint');
      
          const canvas = await html2canvas(quotes, {
            logging: true,
            letterRendering: 1,
            useCORS: true,
            allowTaint: true,
            scale: 2,
          });
      
          const imgWidth = 210;
          const pageHeight = 290;
          const imgHeight = canvas.height * imgWidth / canvas.width;
          let heightLeft = imgHeight;
      
          const doc = new jsPDF('p', 'mm');
          let position = 25;
          const pageData = canvas.toDataURL('image/jpeg', 1.0);
          doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
          doc.setLineWidth(5);
          doc.setDrawColor(255, 255, 255);
          doc.rect(0, 0, 210, 295);
          heightLeft -= pageHeight;
      
          while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            doc.addPage();
            doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
            doc.setLineWidth(5);
            doc.setDrawColor(255, 255, 255);
            doc.rect(0, 0, 210, 295);
            heightLeft -= pageHeight;
          }
      
          // Create Blob
          const blob = doc.output('blob');
      
          // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
          
      
          this.setState({
            loading: false,
            pdfData: blob,
            emailModal: true,
          });
      
        } catch (error) {
          console.error('Error:', error);
          this.setState({ loading: false });
          // Handle errors here
        }
      };


    render() {
      
        return (
            <div className="body-wrapper modalWrapper">

                <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="toolbar-ui-wrapper">
                                <div className="leftsection">
                                    <span key="first" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">

                                            <li onClick={this.sendEmailReportModal}>Send Email</li>

                                            <li><ReactToPrint
                                                trigger={() => <span className="text-bt"><PrinterOutlined /> Print</span>}
                                                content={() => this.componentRef}
                                            /></li>
                                            {/* <li><span className="text-bt"><Icon type="schedule" /> Preview</span></li>
                                            <li><span className="text-bt"><SaveOutlined /> Save</span></li> */}
                                        </ul>
                                    </span>
                                </div>
                                <div className="rightsection">
                                    <span className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            {/* <li onClick={this.printDocument}>
                        <span className="text-bt">
                          <Icon type="file-pdf" /> Download PDF
                        </span>
                      </li> */}
                                            {/* <li><span className="text-bt"><Icon type="file-excel" /> Excel</span></li>
                                            <li><span className="text-bt"><MailOutlined />/> Email</span></li> */}
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <ComponentToPrint ref={el => (this.componentRef = el)} {...this.props} />
                        </div>
                    </div>
                </article>

                {this.state.emailModal && (
                    <Modal
                        title="New Message"
                        visible={this.state.emailModal}
                        onOk={() => {
                            this.setState({ emailModal: false });
                        }}
                        onCancel={() => {
                            this.setState({ emailModal: false });
                        }}
                        footer={null}
                    >
                        {this.state.pdfData && <Email
                            handleClose={
                                () => {
                                    this.state.emailModal(false);
                                }

                            }
                            attachmentFile={this.state.pdfData}

                            title={window.corrector(`Summary_Laytime_Report||${this.props.laytime.charterer_name}||${this.props.laytime.vessel_name}` ) }

                            // title={`Summary_Laytime_Report|| ${this.props.laytime.charterer_name} || ${this.props.laytime.vessel_name}`}

                        />}
                    </Modal>
                )}
                 {
          this.state.loading && (
            <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
              <Spin size="large" />
            </div>
          )
        }

            </div>
        )
    }
}

export default SummaryReports;