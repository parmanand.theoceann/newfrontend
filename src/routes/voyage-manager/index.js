import React, { useEffect } from "react";
import { Layout, Drawer, Button, Spin, Alert,Modal } from "antd";
import VoyageOperation from "../voyage-list/components/VoyageOperation";
import URL_WITH_VERSION, { getAPICall, useStateCallback } from "../../shared";
import IFOList from "../voyage-bunker-plan/ifo";

import { useLocation, useNavigate, useParams } from "react-router-dom";
import { calculateTotalSummary, calculateTotalaverage } from "../chartering/routes/utils";


const { Content } = Layout;

const VoyageManger = () => {
  const [state, setState] = useStateCallback({
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    voyageId: {},
    frmVisible: true,
    voyID: null,

    formData: null,
    sendBackData: { show: false },
    // isShowVoyageBunkerPlan: false,
  });

  const params = useParams();
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    let voyID = params.id;
    if (voyID) {
      setState(
        (prevState) => ({ ...prevState,voyID: voyID, frmVisible: false }),
        () => _onEditDataLoad(voyID)
      );
    }
  }, []);

  const _onEditDataLoad = async (coaVciId) => {
    let response = null,
      vmData = undefined,
      vesselData = undefined;
    let _detailData = Object.assign({});

    response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/edit?ae=${coaVciId}`
    );
    vmData = await response["data"];

    if (vmData && !vmData.hasOwnProperty("portitinerary")) {
      vmData["portitinerary"] = [{ port: "Select Port", index: 0 }];
    }

    if (
      vmData["-"] &&
      vmData["-"].voyage_manager_id == "" &&
      vmData["."].voyage_manager_id == ""
    ) {
      // Get CP ECHO Data
      response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/cp_echo/${vmData.vessel_id}`
      );
      vesselData = await response["data"];

      _detailData = Object.assign(vmData, vesselData);
      _detailData["-"] = vmData["-"];
      _detailData["."] = vmData["."]["eco_data"];
    }

    if (
      vmData &&
      vmData["voyage_number"] &&
      vmData["voyage_number"].includes("EST-TCTO") == false
    ) {
      vmData["disablefield"] = [
        "tco_code",
        "tco_bb",
        "tco_d_hire",
        "tco_add_percentage",
        "tco_bro_percentage",
      ];
    }

    let toitinaryfields = [
      "total_distance",
      "total_tsd",
      "total_gsd",
      "total_seca",
      "total_seca_days",
      "total_xsd",
      "total_lq",
      "total_turntime",
      "totalt_port_days",
      "total_xpd",
      "total_exp",
      "total_port_pdays",
    ];

    let fromitinaryfields = [
      "miles",
      "tsd",
      "gsd",
      "seca_length",
      "eca_days",
      "xsd",
      "l_d_qty",
      "turn_time",
      "days",
      "xpd",
      "p_exp",
      "t_port_days",
    ];

    let totalitenary = calculateTotalSummary(
      fromitinaryfields,
      toitinaryfields,
      "portitinerary",
      vmData
    );

    const totalspeed = calculateTotalaverage(
      "speed",
      "portitinerary",
      vmData
    );

    const totaleffspeed = calculateTotalaverage(
      "eff_speed",
      "portitinerary",
      vmData
    );

    totalitenary = {
      ...totalitenary,
      total_speed: isNaN(totalspeed) ? 0 : totalspeed,
      total_eff_spd: isNaN(totaleffspeed) ? 0 : totaleffspeed,
    };

    
        
    

    

    let tobunkerarr = [
      "ttl_miles",
      "ttl_seca_length",
      "ttl_eca_days",
      "ttl_speed",
      "ttl_ec_ulsfo",
      "ttl_ec_lsmgo",
      "ttl_ifo",
      "ttl_vlsfo",
      "ttl_lsmgo",
      "ttl_mgo",
      "ttl_ulsfo",
      "ttl_pc_ifo",
      "ttl_pc_vlsfo",
      "ttl_pc_lsmgo",
      "ttl_pc_mgo",
      "ttl_pc_ulsfo",
    ];

    let frombunkerarr = [
      "miles",
      "seca_length",
      "eca_days",
      "speed",
      "eca_consp",
      // "eca_consp",
      "ifo",
      "vlsfo",
      "lsmgo",
      "mgo",
      "ulsfo",
      "pc_ifo",
      "pc_vlsfo",
      "pc_lsmgo",
      "pc_mgo",
      "pc_ulsfo",
    ];

    const totalbunker = calculateTotalSummary(
      frombunkerarr,
      tobunkerarr,
      "bunkerdetails",
      vmData
    );

    const fromciiarr = [
      "ifo",
      "vlsfo",
      "lsmgo",
      "mgo",
      "ulsfo",
      "pc_ifo",
      "pc_vlsfo",
      "pc_lsmgo",
      "pc_mgo",
      "pc_ulsfo",
      "co2_emission_total",
    ];

    const tociiarr = [
      "ttl_ifo_con",
      "ttl_vlsfo_con",
      "ttl_lsmgo_con",
      "ttl_mgo_con",
      "ttl_ulsfo_con",
      "ttl_pc_ifo_con",
      "ttl_pc_vlsfo_con",
      "ttl_pc_lsmgo_con",
      "ttl_pc_mgo_con",
      "ttl_pc_ulsfo_con",
      "ttl_co_emi",
    ];

    const totalcii = calculateTotalSummary(
      fromciiarr,
      tociiarr,
      "ciidynamics",
      vmData
    );

    const totalciiVal = {
      ttl_vlsfo_con: (
        parseFloat(totalcii["ttl_vlsfo_con"]) +
        parseFloat(totalcii["ttl_pc_vlsfo_con"])
      ).toFixed(2),
      ttl_lsmgo_con: (
        parseFloat(totalcii["ttl_lsmgo_con"]) +
        parseFloat(totalcii["ttl_pc_lsmgo_con"])
      ).toFixed(2),
      ttl_ulsfo_con: (
        parseFloat(totalcii["ttl_ulsfo_con"]) +
        parseFloat(totalcii["ttl_pc_ulsfo_con"])
      ).toFixed(2),
      ttl_mgo_con: (
        parseFloat(totalcii["ttl_mgo_con"]) +
        parseFloat(totalcii["ttl_pc_mgo_con"])
      ).toFixed(2),
      ttl_ifo_con: (
        parseFloat(totalcii["ttl_ifo_con"]) +
        parseFloat(totalcii["ttl_pc_ifo_con"])
      ).toFixed(2),
      ttl_co_emi: totalcii["ttl_co_emi"],
    };

    const fromeuetcs = [
      "sea_ems",
      "port_ems",
      "ttl_ems",
      "sea_ets_ems",
      "port_ets_ems",
      "ttl_eu_ets",
      "ttl_eu_ets_exp",
    ];

    const toeuetcs = [
      "ttl_sea_emi",
      "ttl_port_emi",
      "ttl_emi",
      "ttl_sea_ets_emi",
      "ttl_port_ets_emi",
      "ttl_ets_emi",
      "ttl_eu_ets_emi",
    ];
    const totaleuets = calculateTotalSummary(
      fromeuetcs,
      toeuetcs,
      "euets",
      vmData
    );

    let totalVoyDays=(totalitenary?.['total_port_pdays']??0)*1 + (totalitenary?.['total_tsd']??0)*1;
  
    vmData['total_days']=isNaN(totalVoyDays)?0:parseFloat(totalVoyDays).toFixed(2);

    vmData["totalitinerarysummary"] = { ...totalitenary };
    vmData["totalbunkersummary"] = { ...totalbunker };
    vmData["totalciidynamicssummary"] = { ...totalciiVal };
    vmData["totaleuetssummary"] = { ...totaleuets };
    setState((prevState) => ({
      ...prevState,
      formData: vmData,
      frmVisible: true,
    }));

    // setState((prevState) => ({
    //   ...prevState,
    //   formData: vmData,
    //   frmVisible: true,
    // }));
  };

  

  

 

  
  return (
    <div className="wrap-rightbar full-wraps">
      <div className="fieldscroll-wrap">
     
    
        {state.frmVisible ? (
          <VoyageOperation
            //voyID={state.voyID}
            //voyID={state?.formData?.estimate_id}
            // voyID={params?.id}            
          
            frmVisible={state.frmVisible}
            formData={state.formData}
            sendBackData={state.sendBackData}
           
          />
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </div>
    </div>
  );
};


export default VoyageManger;
