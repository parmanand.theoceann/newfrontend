import { Button, Layout, Modal } from "antd";
import React, { Component, useEffect } from "react";
import URL_WITH_VERSION, {
  getAPICall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../shared";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import NewLaytimeCalculation from "./NewLaytimeCalculation";
import { DoubleLeftOutlined } from "@ant-design/icons";
const { Content } = Layout;

const NewLaytime = (props) => {
 
  const [state, setState] = useStateCallback({
    visible: false,
    visibleEstimate: false,
    isVisible: false,
    frmName: "new_laytime_calculation_setup_form",
    formData: {
      id: 0,
      vessel_id: props.voyageData["vessel_id"],
      voyno: props.voyageData["voyage_number"],
      voyage_no: props.voyageData["voyage_number"],
      vessel_code:props.voyageData["vessel_code"],
      ops_type:props.voyageData["ops_type"]
    },
    voyageData: props.voyageData || {},
    visibleAttachment: false,
    finalFormData: {},
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    loading: false,
    fileArr: [],
    visisbleCargoChild: false,
    frmChange: false,
    oldPageData: [],
    showModal: false,
    formDataValues: [],
    formDataValue: [],
  });

  useEffect(() => {   
    let _voyageData = props.voyageData;
    if (Object.keys(_voyageData).length > 0 && _voyageData["id"] > 0) {
      let charternames = [];
      _voyageData["cargos"] &&
        _voyageData["cargos"].length > 0 &&
        _voyageData["cargos"].map((item) => {
          charternames.push({
            charterer: item.charterer,
          });
        });

      let TempVoigePort = [];
      _voyageData["portitinerary"] &&
        _voyageData["portitinerary"].length > 0 &&
        _voyageData["portitinerary"].map((item) => {
          let pushVal = {
            port: item.port,
            bl_qty: item.l_d_qty,
            ld_rate: item.l_d_rate,
            funct: item.funct,
            port_id: item.port,
            portid: item.port_id,
            unit: "2",
          };
          TempVoigePort.push(pushVal);
        });

      let _formData = Object.assign({}, state.formData);
      _formData["."] = charternames;
      _formData[".."] = TempVoigePort;
      setState((prevState) => ({
        ...prevState,
        formData: { ..._formData },
        visible: true,
      }));
    } else {
      setState({ ...state, visible: true });
    }
    // loadSummery() no use of this function because one group is disabled.
  }, []);

  const hideShowModal = () => {
    setState({ ...state, showModal: false });
    if (props.modalCloseEvent && typeof props.modalCloseEvent === "function") {
      props.modalCloseEvent();
    }
  };

  /*
  const loadSummery = async () => {
    let qParams = {
      p: state.pageOptions.pageIndex,
      l: state.pageOptions.pageLimit,
    };
    let headers = { order_by: { id: "desc" } };
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = "";
    _url = `${URL_WITH_VERSION}/laytime-calculator/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    let temp = state.formData;
    let pushDataValue = [];
    data.data &&
      data.data.length > 0 &&
      data.data.map((item) => {
        let dataTemp = [];
        dataTemp["laytime_id"] = item.id;
        dataTemp["counter_party"] = item.charterer;
        dataTemp["type"] = item.vessel;
        dataTemp["cp_form"] = item.cp_form;
        dataTemp["dem_desp"] = item.demurrage_dispatch;
        dataTemp["invoice_date"] = item.created_on;
        dataTemp["amount"] = item.demurrage_amount;
        dataTemp["status"] = item.l_status;
        dataTemp["last_modified_date"] = item.modified_on;
        pushDataValue.push(dataTemp);
      });
    temp["..."] = pushDataValue;
    setState({ ...state, formData: temp });
  };

*/

  const formChangeEvent = async (data) => {
    let port_length,
      charterer_length,
      port_ids = [],
      charterer_ids = [];
    let finalFormDataTemp = state.formData;
    charterer_length = data["."].filter((e) => e["s"] === true).length;
    port_length = data[".."].filter((e) => e["s"] === true).length;

    data[".."]?.map((e) =>
      e["s"] === true ? port_ids.push(e["port_id"]) : undefined
    );
    data["."]?.map((e) =>
      e["s"] === true ? charterer_ids.push(e["charterer"]) : undefined
    );

    if (charterer_length == 0 || charterer_length > 1) {
      openNotificationWithIcon(
        "error",
        <div
          className="notify-error"
          dangerouslySetInnerHTML={{
            __html: "Please select only one charterer",
          }}
        />,
        5
      );
    } else {
      if (port_length > 0 && charterer_length > 0) {
        let arr = [];
        finalFormDataTemp.charterer = data["."].filter(
          (e) => e["s"] === true
        )[0].charterer;
        data[".."]
          ?.filter((e) => e["s"] === true)
          ?.map((e) => {
            e.port_name = e.port_id;
            e.quantity = e.bl_qty ? e.bl_qty : 0.0;
            e.ld_rate = e.ld_rate ? e.ld_rate : 0.0;

            arr.push(e);
          });
        finalFormDataTemp["."] = arr;
        finalFormDataTemp["formType"] = "new";

        let _url = `${URL_WITH_VERSION}/laytime-calculator/validate`;
        let _data = {
          port_ids: port_ids,
          charterer_ids: charterer_ids,
          voyage_no: data["voyno"],
          vessel_id: data["vessel_id"],
        };
        const response = await awaitPostAPICall(_url, _data);
        const respData = await response["data"];

        setState((prevState) => ({
          ...prevState,
          finalFormData: finalFormDataTemp,
        }));

        if (respData === true) {
          setState((prevState)=>({ ...prevState, frmChange: false, oldPageData: data }));
          Modal.confirm({
            title: "Confirm",
            content:
              "We have found Laytime for selected Charterer and selected Port. Do you want to do new entry ?",
            okText: "OK",
            cancelText: "Cancel",
            onOk(e) {
              e();
            setState((prevState)=>({ ...prevState, frmChange: true }));
            },
          });
        } else {
          setState( (prevState)=>({ ...prevState, frmChange: true, oldPageData: data }));
        }
      } else {
        openNotificationWithIcon(
          "error",
          <div
            className="notify-error"
            dangerouslySetInnerHTML={{
              __html: "Please select atleast one port and one charterer",
            }}
          />,
          5
        );
      }
    }
    // setState({ ...state, frmChange: true, oldPageData: data })
  };

  return (
    <>
      {state.frmChange === false ? (
        <div className="tcov-wrapper full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">
                    <article className="article">
                      <h4 className="cust-header-title">
                        <Button
                          type="dashed"
                          shape="circle"
                          icon={<DoubleLeftOutlined />}
                          size={"default"}
                          //  onClick={onClickBack}
                        />

                        <b>New Laytime / {state.formData["voyno"]}</b>
                      </h4>
                      <div className="box box-default">
                        <div className="box-body common-fields-wrapper">
                          {state.visible ? (
                            <NormalFormIndex
                              formClass="label-min-height"
                              formData={Object.assign(
                                { id: 0 },
                                state.formData
                              )}
                              showForm={true}
                              frmCode={state.frmName}
                              addForm={true}
                              showButtons={[
                                {
                                  id: "save",
                                  title: "Next",
                                  type: "primary",
                                  event: (data, innerCB) => {
                                    formChangeEvent(data);
                                  },
                                },
                              ]}
                              inlineLayout={true}
                              isShowFixedColumn={["...", ".."]}
                              // formGroupDevideInCols={{ "....": "1" }}
                            />
                          ) : undefined}
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </Content>
            </Layout>
          </Layout>
        </div>
      ) : (
        <div>
          {state.frmChange === true ? (
            <NewLaytimeCalculation
              modalCloseEvent={() => hideShowModal(false)}
              // frmData={state.finalFormData}
              formData={state.finalFormData}
            />
          ) : undefined}
        </div>
      )}

      {state.showModal ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Laytime Calculation - "
          open={state.showModal}
          onCancel={() => hideShowModal(false)}
          width="90%"
          footer={null}
        >
          <NewLaytimeCalculation
            modalCloseEvent={() => hideShowModal(false)}
            formData={state.formDataValues}
            // frmData={state.formDataValue}
          />
        </Modal>
      ) : undefined}
    </>
  );
};

export default NewLaytime;
