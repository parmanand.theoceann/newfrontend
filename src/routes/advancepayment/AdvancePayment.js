import React, { useState, useEffect, useRef } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import { Layout, Row, Col, Drawer, Modal, Spin, Alert } from "antd";
import {
  postAPICall,
  openNotificationWithIcon,
  getAPICall,
  apiDeleteCall,
  awaitPostAPICall,
  useStateCallback,
} from "../../shared";
import URL_WITH_VERSION from "../../shared";



import {
  DeleteOutlined,
  MenuFoldOutlined,
  PlusOutlined,
  SaveOutlined,
} from "@ant-design/icons";

const { Content } = Layout;

const AdvancePayment = (props) => {
  const [state, setState] = useStateCallback({
    frmName: "adv_pay_and_recipt_form",
    formData: Object.assign({}, props.formdata || {}),
    frmVisible: true,
    showTDE: false,
    showDownloadInvoice: false,
    tdeData: null,
    showInvoicePopup: false,
    invoiceReport: null,
    isSaved: false,
    popupdata: null,
    loadComponent: false,
    title: undefined,
    isshowVesselFileReport: false,
    reportFormData: null,
    isVesselfileFixed: false,
    isVesselFileSchedule: false,
  });

  let formDataref = useRef(null);
  const _onCreateFormData = () => {
    setState((prevState)=>({ ...prevState, frmVisible: false }),()=>{
      setState({...state, frmVisible: true, formData:{}})
    });
  };

  const saveFormData = (vData) => {
    let type = "save";
    const { frmName } = state;
    let suMethod = "POST";
    if (vData.hasOwnProperty("id") && vData["id"]) {
      type = "update";
      suMethod = "PUT";
    }
    formDataref.current = vData;
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let suURL = `${URL_WITH_VERSION}/advance-payment/${type}?frm=adv_pay_and_recipt_form`;
    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);

        setState(
          (prevState) => ({ ...prevState, formData: { id: data.row.adv_id } }),
          () => {
            EditFormData(data.row.adv_id);
          }
        );
      } else {
        openNotificationWithIcon("error", data.message);
        setState((prevState) => ({
          ...prevState,
          formData: Object.assign({}, formDataref.current),
        }));
      }
    });
  };

  const EditFormData = async (id) => {
    const { formData } = state;

    if (id) {
      setState((prevState) => ({ ...prevState, frmVisible: false }));
      const response = await getAPICall(
        `${URL_WITH_VERSION}/advance-payment/edit?e=${id}`
      );
      const respData = await response["data"];
      setState((prevState) => ({
        ...prevState,
        formData: respData,
        frmVisible: true,
      }));
    } else {
      openNotificationWithIcon("error", "Something went wrong", 4);
      setState((prevState) => ({ ...prevState, frmVisible: true }));
    }
  };

  const _onDeleteFormData = (id) => {
    try {
      let URL = `${URL_WITH_VERSION}/advance-payment/delete`;
      apiDeleteCall(URL, { id: id }, (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          if(props.modalCloseEvent && typeof props.modalCloseEvent=="function"){
            props.modalCloseEvent()
          }
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      });
    } catch (err) {
      openNotificationWithIcon("error", "Something Went wrong", 4);
    }
  };

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
      <Layout className="layout-wrapper">
        <Content className="content-wrapper">
          <div className="body-wrapper">
            <article className="article">
              <div className=" box-default">
                <div className="box-body">
                  {state.frmVisible ? (
                    <NormalFormIndex
                      key={"key_" + state.frmName + "_0"}
                      formClass="label-min-height"
                      formData={state.formData}
                      showForm={true}
                      frmCode={state.frmName}
                      addForm={true}
                      inlineLayout={true}
                      showToolbar={[
                        {
                          leftWidth: 8,
                          rightWidth: 16,
                          isLeftBtn: [
                            {
                              key: "s1",
                              isSets: [
                                {
                                  id: "2",
                                  key: "add",
                                  type: <PlusOutlined />,
                                  withText: "Add New",
                                  showToolTip: true,
                                  event: (key, data) => _onCreateFormData(),
                                },

                                {
                                  id: "3",
                                  key: "save",
                                  type: <SaveOutlined />,
                                  withText: state.formData.id
                                    ? "Update"
                                    : "Save",
                                  showToolTip: true,
                                  event: (key, data) => saveFormData(data),
                                },
                                {
                                  id: "4",
                                  key: "delete",
                                  type: <DeleteOutlined />,
                                  withText: "Delete",
                                  showToolTip: true,
                                  event: (key, data) => {
                                    if (data.id) {
                                      _onDeleteFormData(data.id);
                                    } else {
                                      openNotificationWithIcon(
                                        "info",
                                        "Please Save The form First",
                                        3
                                      );
                                    }
                                  },
                                },
                              ],
                            },
                          ],
                          isRightBtn: [
                            {
                              key: "s2",
                              isSets: [
                                {
                                  key: "cancelpayment",
                                  isDropdown: 0,
                                  withText: "Cancel Payment/Receipt",
                                  type: "",
                                  menus: null,
                                  event: (key) => {
                                    //console.log(key);
                                  },
                                },

                                {
                                  key: "report",
                                  isDropdown: 0,
                                  withText: "Report",
                                  type: "",
                                  menus: null,
                                  event: (key) => {},
                                },

                                {
                                  key: "banktransfer",
                                  isDropdown: 0,
                                  withText: "Finalise Bank Transfer",
                                  type: "",
                                  menus: null,
                                  event: (key) => {},
                                },
                              ],
                            },
                          ],
                          isResetOption: false,
                        },
                      ]}
                    />
                  ) : (
                    <div className="col col-lg-12">
                      <Spin tip="Loading...">
                        <Alert
                          message=" "
                          description="Please wait..."
                          type="info"
                        />
                      </Spin>
                    </div>
                  )}
                </div>
              </div>
            </article>
          </div>
        </Content>
      </Layout>
    </div>
  );
};

export default AdvancePayment;
