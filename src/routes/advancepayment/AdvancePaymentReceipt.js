
import React, { useState, useEffect, useRef } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import { Layout, Row, Col, Drawer, Modal, Spin, Alert, Button } from "antd";

const { Content } = Layout;

const AdvancePaymentReceipt = () => {
  const [state, setState] = useState({
    frmName: "advance_payment_receipt_form",
    formData: {},
    frmVisible: true,
  });
  const {
   
    frmName
    
  } = state;
  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
 
      <Layout className="layout-wrapper">
        <Content className="content-wrapper">
          <div className="body-wrapper">
            <article className="article">
              <div className=" box-default">
                <div className="box-body">
                  {state.frmVisible ? (
                    <NormalFormIndex
                      key={"key_" + frmName + "_0"}
                      formClass="label-min-height"
                      formData={state.formData}
                      showForm={true}
                      frmCode={state.frmName}
                      addForm={true}
                      inlineLayout={true}
                    />

                  ) : (
                    <div className="col col-lg-12">
                      <Spin tip="Loading...">
                        <Alert
                          message=" "
                          description="Please wait..."
                          type="info"
                        />
                      </Spin>
                    </div>
                  )}
                  {
                    state.frmVisible && <Button type="primary">Allocate </Button>
                  }
                 
                </div>
              </div>
            </article>
          </div>
        </Content>
      </Layout>
    </div>
  );
};

export default AdvancePaymentReceipt;
