import React, { Component } from 'react';
import { Checkbox, Input, Button } from 'antd';
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon } from '../../shared';

class RightTaskAlert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      instruction: [],
      update: {},
      id:null
    }
  }
  componentDidMount = () => {
    this.getApiData();
  }

  getApiData = async () => {
    const { formData } = this.props//EST-TCOV-03220-48 formData.estimate_id
    const ediData = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/task-alert/edit?ae=${formData.voyage_number}`);
    const resp = await ediData['data']//['checkbox_value'];
    if(resp.hasOwnProperty('id')){
      this.setState({
      instruction:resp.checkbox_value,
      id:resp.id
    })
    }
    else{
      const respTitle = await getAPICall(`${URL_WITH_VERSION}/master/list?t=voydtt&p=1&l=0`);
      const dataTitle = await respTitle['data'];
      const response = await getAPICall(`${URL_WITH_VERSION}/master/list?t=voydts&p=1&l=0`);
      const data = await response['data'];
  
      let instructionArray = {}, counter = 1;
      if (dataTitle.length > 0) {
        dataTitle.map((val, ind) => {
          instructionArray[val.name.toLowerCase().replaceAll(' ', '_')] = {}
          counter = 1
          data && data.length > 0 && data.filter((val1) => {
            if (val.name == val1.group_name) {
              instructionArray[val.name.toLowerCase().replaceAll(' ', '_')]['check_' + (counter++)] = {
                "date": "",
                "name": val1.name,
                "value": val1.checked
              }
            }
          });
        });
      }
      this.setState({ instruction: instructionArray });
    }
    
  }

  onSave = async () => {
    const { instruction, id } = this.state
    const { formData } = this.props
    let checkbox_value = Object(instruction)
    let data1 = {
      "data": {
        checkbox_value,
        "voyage_manager_id": formData.id
      }
    }
    let url = 'save';
    let _method = 'post';
    if (id!=null) {
      url = 'update';
      _method = 'put';
      data1['data'].id=id
    }
    if (data1) {
      let _url = `${URL_WITH_VERSION}/voyage-manager/task-alert/${url}?frm=task_alert`;
      await postAPICall(`${_url}`, data1.data, `${_method}`, (response) => {
        if (response && response.data == true) {
          openNotificationWithIcon('success', response.message);
        } else
          openNotificationWithIcon('error', response.message);
      });
    }
  }
  onChange = (group_name, sub_grp) => {
    let data = { ...this.state.instruction }
    data[group_name][sub_grp].value = data[group_name][sub_grp].value == 0 ? 1 : 0
    this.setState({
      instruction: data
    })
  }

  onChangeDate = (event, group_name, sub_grp) => {
    let data = { ...this.state.instruction }
    data[group_name][sub_grp].date = event.target.value
    this.setState({
      instruction: data
    })
  }

  render() {
    const { instruction } = this.state
    return (
      <div>
        <article>
          <div>
            <div>
              <div className="row p10">
                {Object.keys(instruction).length > 0 && Object.keys(instruction).map((val) => {
                  return (
                    <div className="col-md-6">
                      <h4 className="text-primary">{val}</h4>
                      <hr />
                      {Object.keys(instruction[val]) && Object.keys(instruction[val]).length > 0 && Object.keys(instruction[val]).map((ins) => {
                        return (
                          <div>
                            <p className="">
                              {instruction[val][ins].name}
                              <span className="float-right d-flex">
                                <Checkbox onChange={(event) => { this.onChange(val, ins) }} checked={instruction[val][ins].value == 0 ? false : true} />
                                <span className="ml-2"><Input defaultValue={instruction[val][ins].date && instruction[val][ins].date.split(' ')[0]} onChange={(event) => { this.onChangeDate(event, val, ins) }} type="date" /></span>
                              </span>
                            </p>
                          </div>
                        )
                      })}
                    </div>
                  )
                })}
              </div>
              <div className="action-btn text-left">
                <Button
                  className="ant-btn ant-btn-primary"
                  value="submit"
                  type="submit"
                  htmlFor="my-form"
                  onClick={this.onSave}
                >
                  Save
                </Button>
              </div>
            </div>
          </div>
        </article>
      </div>

    );
  }
}

export default RightTaskAlert;
