import {
  Layout,
  Table,
  Typography,
  Input,
  Row,
  Icon,
  Col,
  Divider,
  Modal,
  message,
} from "antd";
import React, { Component } from "react";
import URL_WITH_VERSION, {
  objectToQueryStringFunc,
  getAPICall,
  useStateCallback,
} from "../../shared";

const { Content } = Layout;
const { Title } = Typography;


const CargoTonnage = () => {
  const [state, setState] = useStateCallback({
    data: [],
    mapData: null,
    loading: false,
    filterVessels: [],
    columns: [
      {
        title: "S/N",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Cargo C/P Qty",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Unit",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Load Port",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Dis Port",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Freight $",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Laycan",
        dataIndex: "sn",
        key: "sn",
      },
    ],

    columnsTonnage: [
      {
        title: "S/N",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Vessel Name",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Type",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "DWT",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Open Port",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Open Date",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Last Port",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "ATA",
        dataIndex: "sn",
        key: "sn",
      },
      {
        title: "Hire Rate",
        dataIndex: "sn",
        key: "sn",
      },
    ],
  });

  return (
    <div className="tcov-wrapper full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <div className="fieldscroll-wrap">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body common-fields-wrapper">
                      <Title level={4}>My Cargo Tonnage Schedule</Title>
                      <Divider></Divider>

                      <Row>
                        <Col span={11}>
                          <Title level={4}>Cargo Trade Schedule</Title>
                          <Input
                            placeholder="Search"
                            style={{ marginTop: 10, marginBottom: 10 }}
                          />
                          <Table
                            columns={state.columns}
                            className="inlineTable resizeableTable"
                            size="small"
                            pagination={false}
                            rowClassName={(r, i) =>
                              i % 2 === 0
                                ? "table-striped-listing"
                                : "dull-color table-striped-listing"
                            }
                          />
                          ;
                        </Col>
                        <Col span={1}></Col>
                        <Col span={12}>
                          <Title level={4}>Tonnage Trade Schedule</Title>
                          <Input
                            placeholder="Search"
                            style={{ marginTop: 10, marginBottom: 10 }}
                          />
                          <Table
                            columns={state.columnsTonnage}
                            className="inlineTable resizeableTable"
                            size="small"
                            pagination={false}
                            rowClassName={(r, i) =>
                              i % 2 === 0
                                ? "table-striped-listing"
                                : "dull-color table-striped-listing"
                            }
                          />
                        </Col>
                      </Row>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default CargoTonnage;
