import React, { Component } from 'react';
import { Form, Input, Button, Tabs, Select } from 'antd';
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon } from '../../shared';
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

class InstructionSetModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            newTitle: '',
            instruction: [],
            id: 0,
            activeTab: 'create',
        };
    }


    changeTab = activeKey => {
        this.setState({
            activeTab: activeKey
        });
    };


    componentDidMount = async () => {
        const response = await getAPICall(`${URL_WITH_VERSION}/master/list?t=voydtt&p=1&l=0`);
        const res = await response['data'];
        this.setState({ ...this.state, instruction: res });
    };



    onSaveUpdate = async () => {
        const { activeTab,newTitle, name, id } = this.state
        let url = 'save';
        let _method = 'post';
        let data = {
            "name": name
        }
        if (activeTab == "update") {
            url = 'update';
            _method = 'put';
            data['id'] = id
            data['name']=newTitle
        }

        if (data) {
            let _url = `${URL_WITH_VERSION}/master/${url}?t=voydtt`;
            await postAPICall(`${_url}`, data, `${_method}`, (response) => {
                if (response && response.data == true) {
                    openNotificationWithIcon('success', response.message);
                    this.props.parentCallback();
                } else
                    openNotificationWithIcon('error', response.message);
            });
        }
    }


    handleSelect = (e) => {
        this.setState({ id: e.key })
    }

    handleUpdateChange = (e, item) => {

        this.setState({ newTitle: e.target.value })

    }

    handleChange = e => {
        this.setState({ ...this.state, name: e.target.value });
    };



    render() {
        const { newTitle } = this.state;
        return (
            <div className="body-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Tabs activeKey={this.state.activeTab} size="small" onChange={this.changeTab}>
                                <TabPane className="pt-3" tab="Create" key="create" >
                                    <FormItem htmlFor="my-form" label="Create Title Name">
                                        <Input type="text" name="name" size="default" onChange={this.handleChange} />
                                    </FormItem>
                                </TabPane>
                                <TabPane className="pt-3" tab="Update" key="update"  >
                                    <FormItem label="Select Title">
                                        <Select size="default"  >
                                            {this.state.instruction.map(e => {

                                                return <Option value={e.id} onClick={this.handleSelect}>{e.name}</Option>;

                                            })

                                            }
                                        </Select>
                                    </FormItem>

                                    <FormItem label="New Name" >
                                        <Input type="text" value={newTitle} size="default" onChange={(e, item) => this.handleUpdateChange(e, item)} />
                                    </FormItem>
                                </TabPane>
                            </Tabs>

                            <FormItem>
                                <div className="row p10">
                                    <div className="col-md-12">
                                        <div className="action-btn text-left">

                                            <Button
                                                className="ant-btn ant-btn-primary"
                                                value="submit"
                                                type="submit"
                                                htmlFor="my-form"
                                                onClick={this.onSaveUpdate}

                                            >
                                                Save
                                            </Button>


                                            <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                                        </div>
                                    </div>
                                </div>
                            </FormItem>
                        </div>
                    </div>
                </article>
            </div>
        );
    }
}

export default InstructionSetModal;
