import React, { Component } from 'react';
import { Form, Input, Button, Tabs } from 'antd';
import URL_WITH_VERSION, { openNotificationWithIcon, postAPICall } from '../../shared';
const FormItem = Form.Item;

export class AddNewRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name:(this.props.record && this.props.record.name) ? this.props.record.name : '',
            record:this.props.record ? this.props.record : null,
            instructionID:this.props.instructionID ? this.props.instructionID : ''
        };
    }

    onSave = async () => {
        const { name, instructionID,record } = this.state
        let url = 'save';
        let _method = 'post';
        let data = {
            'group_name':instructionID,
            'name': name
        }
        if(instructionID=='' && record.hasOwnProperty('id')){
            url = 'update';
            _method = 'put';
            data['group_name']=record.group_name
            data['id']=record.id
        }
        if (data) {
            let _url = `${URL_WITH_VERSION}/master/${url}?t=voydts`;
            await postAPICall(`${_url}`, data, `${_method}`, (response) => {
                if (response && response.data == true) {
                    openNotificationWithIcon('success', response.message);
                    this.props.rowCallBack();
                } else
                    openNotificationWithIcon('error', response.message);
            });
        }
    }

    render() {
        const { name } = this.state;
        return (
            <div className="body-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <FormItem htmlFor="my-form" label="Name">
                                <Input
                                    type="text"
                                    placeholder='Name'
                                    value={name}
                                    size="default"
                                    onChange={e => this.setState({ name: e.target.value })}
                                />
                            </FormItem>

                            <FormItem>
                                <div className="row p10">
                                    <div className="col-md-12">
                                        <div className="action-btn text-left">
                                            <Button
                                                className="ant-btn ant-btn-primary"
                                                value="submit"
                                                type="submit"
                                                htmlFor="my-form"
                                                onClick={this.onSave}
                                            >
                                                Save
                                            </Button>
                                            <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                                        </div>
                                    </div>
                                </div>
                            </FormItem>
                        </div>
                    </div>
                </article>
            </div>
        );
    }
}

export default AddNewRow;
