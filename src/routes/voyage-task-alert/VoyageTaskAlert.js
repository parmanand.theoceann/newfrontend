import React, { Component } from 'react';
import { Table, Modal, Button, Popconfirm} from 'antd';
import CreateTitle from './createTitle'
import AddNewRow from './addRow'
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon } from '../../shared';
import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
class VoyageTaskAlert extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      rowvisible: false,
      instruction: [],
      id: '',
      record: null,
      columns: [
        {
          title: 'Name',
          dataIndex: 'name',
        },
        // {
        //   title: 'Select',
        //   dataIndex: 'select',
        //   render: () => {
        //     return <Checkbox />;
        //   },
        //   width: 60,
        // },
        // {
        //   title: 'Date',
        //   dataIndex: 'date',
        //   render: () => {
        //     return <Input type="date" />;
        //   },
        //   width: 100,
        // },
        {
          title: 'Action',
          dataIndex: 'Action',
          render: (data, record) => (
            <div className="editable-row-operations">
              <span className="iconWrapper" onClick={() => this.updateVisible(record)}>
              <EditOutlined />

              </span>
              <span className="iconWrapper cancel">
                <Popconfirm
                  title="Are you sure, you want to delete it?"
                  onConfirm={() => this.deleteName(data, record.id)}
                >
             <DeleteOutlined />

                </Popconfirm>
              </span>

            </div>
          ),
        },
      ],
    }
  }

  componentDidMount = () => {
    this.getApiData();
  }

  getApiData = async () => {
    const respTitle = await getAPICall(`${URL_WITH_VERSION}/master/list?t=voydtt&p=1&l=0`);
    const dataTitle = await respTitle['data'];
    const response = await getAPICall(`${URL_WITH_VERSION}/master/list?t=voydts&p=1&l=0`);
    const data = await response['data'];

    let instructionArray = []
    if (dataTitle.length > 0) {
      dataTitle.map((val, ind) => {
        instructionArray.push(
          {
            id: val.id,
            name: val.name,
            status: val.status,
            instruction: data && data.length > 0 ? data.filter((val1) => val.name === val1.group_name) : []
          }
        )
        return true;
      })
    }
    // console.log("array....",instructionArray)
    this.setState({ ...this.state, instruction: instructionArray });
  }

  updateVisible = (record) => {
    this.setState({
      record: record,
    }, () => {
      this.setState({
        rowvisible: true
      })
    });
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };


  handleCallback = async () => {
    this.setState({
      visible: false,
    }, () => {
      this.getApiData();
    });
  };

  rowVisible = (e, id) => {
    this.setState({
      id: e.name,
    }, () => {
      this.setState({
        rowvisible: true
      })
    });
  };


  rowCancel = e => {
    this.setState({
      rowvisible: false,
      id: '',
      record: null
    });
  };

  handleCallbackRow = async () => {
    this.setState({
      rowvisible: false,
      id: '',
      record: null
    }, () => {
      this.getApiData();
    });
  };

  deleteName = async (e, id) => {
    let url = 'delete';
    let _method = 'delete';
    let data = {
      "id": id
    }
    let _url = `${URL_WITH_VERSION}/master/${url}?t=voydts`;
    await postAPICall(`${_url}`, data, `${_method}`, (response) => {
      if (response && response.data === true) {
        openNotificationWithIcon('success', response.message);
        this.getApiData()
      } else
        openNotificationWithIcon('error', response.message);
    });
  }

  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Task And Alert</span>
                  </h4>
                </div>

                <div class="action-btn">
                  <button
                    type="submit"
                    class="ant-btn ant-btn-primary"
                    onClick={this.showModal}
                    onOk={this.rowOk}
                  >
                    <span>Create Title</span>
                  </button>
                </div>
              </div>

              {this.state.instruction && this.state.instruction.length > 0 ? (
                <div className="row">
                  {this.state.instruction.map(e => {
                    return (
                      <div className="col-md-6">
                        <Table
                          className="mb-3"
                          bordered
                          size="small"
                          columns={this.state.columns}
                          dataSource={e.instruction}
                          pagination={false}
                          scroll={{ x: 'max-content' }}
                          title={() => (
                            <div className="table-header-wrapper">
                              <div className="form-heading">
                                <div className="title">
                                  {e.name}
                                  <span className="ml-3">
                                    <b value={this.state.titleName} onChange={this.handleCallback}>
                                      {this.state.titleName}
                                    </b>
                                  </span>
                                </div>
                              </div>
                            </div>
                          )}
                          footer={() => (
                            <div className="text-center">
                              <Button type="link" onClick={() => this.rowVisible(e)}>
                                Add New
                              </Button>
                            </div>
                          )}
                          rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                        />
                      </div>
                    );
                  })}


                </div>
              ) : (
                undefined
              )}
            </div>
          </div>
        </article>
        {this.state.rowvisible === true ?
          <Modal
            style={{ top: '2%' }}
            title="Add/Edit Row"
           open={this.state.rowvisible}
            onCancel={this.rowCancel}
            width="40%"
            footer={null}
          >
            <AddNewRow
              instructionID={this.state.id}
              record={this.state.record}
              rowCallBack={this.handleCallbackRow}
            />
          </Modal>
          : undefined}
        {this.state.visible === true ?
          <Modal
            style={{ top: '2%' }}
            title="Create/Update Task And Alert Title"
           open={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            width="40%"
            footer={null}
          >
            <CreateTitle parentCallback={this.handleCallback} />
          </Modal>
          : undefined}
      </div>
    );
  }
}

export default VoyageTaskAlert;
