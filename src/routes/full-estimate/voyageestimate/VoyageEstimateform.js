import React from "react";
import "./voyes.css";

const VoyageEstimateform = () => {
  return (
    <div>
      <h1 class="heading">Voyage Estimate</h1>
      <div class="vetopnav">
        <div class="velefticon">
          <div class="veicon">
            <img src={IMAGE_PATH+"fullve-icons/list_icon.svg"} alt="" />
          </div>
          <div class="veicon">
            <img src={IMAGE_PATH+"fullve-icons/save_icon.svg"} alt="" />
          </div>

          <div class="veicon">
            <img src={IMAGE_PATH+"fullve-icons/dustbin_icon.svg"} alt="" />
          </div>

          <div class="veicon">
            <img src={IMAGE_PATH+"fullve-icons/refresh_icon.svg"} alt="" />
          </div>
        </div>

        <div class="vetoprighticon">
          <div class="venavtext">Quick</div>
          <div class="venavtext">Add Cargo</div>

          <div class="venavtext">Fix</div>

          <div class="venavtext">Report</div>
          <div class="venavtext">Lock</div>

          <div class="venavtextlast">Share</div>
        </div>
      </div>

      <div class="belowcontent">
        <div class="maincontent2">
          <div class="maincontent">
            <div class="topcontent">
              <div class="firstdiv">
                <div class="item">
                  <div class="text">Voy Est. ID</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="TCE10-23-01216"
                      class="inputbox"
                      disabled
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Vessel Name/Code</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <div class="twoinput">
                      <div class="box">
                        <input
                          type="text"
                          placeholder="Dropdown"
                          class="inputbox2"
                        />
                      </div>
                      <div class="box">
                        <input
                          type="text"
                          placeholder="DES-OS231"
                          class="inputbox2"
                          disabled
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item">
                  <div class="text">DWT</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input type="text" placeholder="" class="inputbox" />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Daily Hire Rate</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Daily Hire Rate"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Add Com./ Borker%</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <div class="twoinput">
                      <div class="box">
                        <input type="text" placeholder="XX" class="inputbox2" />
                      </div>
                      <div class="box">
                        <input type="text" placeholder="XX" class="inputbox2" />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item">
                  <div class="text">WF(%)</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input type="text" placeholder="5" class="inputbox" />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Trade Area</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Dropdown"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">TCI Code</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Dropdown"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Routing Type</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Dropdown"
                      class="inputbox"
                    />
                  </div>
                </div>
              </div>

              <div class="seconddiv">
                <div class="item">
                  <div class="text">My Company/LOB</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <div class="twoinput">
                      <div class="box">
                        <input
                          type="text"
                          placeholder="Dropdown"
                          class="inputbox2"
                        />
                      </div>
                      <div class="box">
                        <input
                          type="text"
                          placeholder="Dropdown"
                          class="inputbox2"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item">
                  <div class="text">C/P Date</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Select Date/Time"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Commence Date</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Select Date/Time"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Completing Date</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Select Date/Time"
                      class="inputbox"
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Total Voyage Days</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="34"
                      class="inputbox"
                      disabled
                    />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Other Cost</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input type="text" placeholder="" class="inputbox" />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Ballast Bonus</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input type="text" placeholder="" class="inputbox" />
                  </div>
                </div>

                <div class="item">
                  <div class="text">Fixed Bylops Users</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <div class="twoinput">
                      <div class="box">
                        <input
                          type="text"
                          placeholder="Dropdown"
                          class="inputbox2"
                        />
                      </div>
                      <div class="box">
                        <input
                          type="text"
                          placeholder="Dropdown"
                          class="inputbox2"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item">
                  <div class="text">Voyage OPS Type</div>
                  <div class="hrline"></div>

                  <div class="inputboxdiv">
                    <input
                      type="text"
                      placeholder="Dropdown"
                      class="inputbox"
                    />
                  </div>
                </div>
              </div>
              <div class="thirddiv">
                <div class="toptable">
                  <div class="header">
                    <div class="headeritem">Speed</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem">ECO</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem">CP</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem">Others</div>
                  </div>
                  <div class="headercontent">
                    <div class="headercontentitem">Ballast</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                  </div>

                  <div class="headercontent">
                    <div class="headercontentitem"> Laden</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem2">XX</div>
                  </div>
                </div>

                <div class="belowtable">
                  <div class="header2">
                    <div class="headeritem">Fuel Grade</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem3">CPS</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem">Sea Cons B</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem3">Load</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem3">Disch.</div>
                    <div class="hrwhiteline"></div>
                    <div class="headeritem3">Idle</div>
                  </div>
                  <div class="headercontent2">
                    <div class="headercontentitem1">IFO</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem4">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                  </div>

                  <div class="headercontent2">
                    <div class="headercontentitem1">VLSFO</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem4">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                  </div>

                  <div class="headercontent2">
                    <div class="headercontentitem1">ULSFO</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem4">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                  </div>

                  <div class="headercontent2">
                    <div class="headercontentitem1">LSMGO</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem4">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                  </div>

                  <div class="headercontent2">
                    <div class="headercontentitem1">MGO</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem4">XX</div>
                    <div class="hrwhiteline"></div>

                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                    <div class="headercontentitem3">XX</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="bottomcontent">
              <div class="belowtable1">
                <h5>Cargo ID</h5>
                <div class="cargotabheader">
                  <div class="largewidthheader">Cargo ID</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Charter Party</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">CP Qty</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">Unit</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">Opt%</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Opt Type</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Fright Type</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Frt rate ($/MT)</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Lumpsum</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">B.Comm</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Extra Rev</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Currency</div>

                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Action</div>
                </div>
                <div class="cargotabitemdiv">
                  <div class="largewidthitem">32243245</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">Dropdown</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">MT</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">Dropdown</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">Dropdown</div>
                  <div class="hrline"></div>

                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>

                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">
                    <img src="edit.svg" alt="" class="actionicon" />{" "}
                    <img src="Delete.svg" alt="" class="actionicon" />
                  </div>
                </div>
              </div>
              <div class="belowtable2">
                <h5>Port Itineray</h5>

                <div class="Portiternary">
                  <div class="mediumwidthheader">Port</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Port Bunker</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Port Date</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">EUETS</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Passage Type</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Function</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Speed Type</div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargewidthheader">ECA/SECA Miles</div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargewidthheader">ECA/SECA Days</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">Miles</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">WF%</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">Speed</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Eff Speed</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">GSD</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">TSD</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">XSD</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">L/D Qty</div>
                  <div class="hrwhiteline"></div>
                  <div class="medium-largewidthheader">L/D Rate(D)</div>
                  <div class="hrwhiteline"></div>
                  <div class="medium-largewidthheader">L/D Rate(h)</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">L/D Term</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Turn Time</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">P Days</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">XPD</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">P Exp</div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargewidthheader">Est. Port Days</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Demm./Day</div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader">Dem/Des Final Days</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Laycan From</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Laycan To</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">Draft</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Salinity</div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader">Draft Restruction</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Max lift Qty</div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader">Currency</div>
                </div>

                <div class="Portiternaryitemdiv">
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="medium-largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="medium-largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                </div>

                <button>Add New</button>
              </div>

              <div class="belowtable3">
                <h5>Total</h5>

                <div class="totaltable">
                  <div class="smallwidthheader">TPD</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">TSD</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">T ECA/SECA</div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader">GSD</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Total Load</div>
                  <div class="hrwhiteline"></div>
                  <div class="largewidthheader">Total Distance</div>
                </div>

                <div class="totaltableitemdiv">
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="largewidthitem">XX</div>
                </div>
              </div>
              <div class="belowtable4">
                <h5>Bunker Detail</h5>

                <div class="bunkerdetail">
                  <div class="smallwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="mediumwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="smallwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="extralargestwidthheader"></div>
                  <div class="hrwhiteline"></div>
                  <div class="doublemedium">ECA/SECA Consp (MT)</div>
                  <div class="hrwhiteline"></div>
                  <div class="fivewidthdiv">Arrival RoB</div>
                  <div class="hrwhiteline"></div>
                  <div class="fivewidthdiv">Sea Consumption (MT)</div>
                  <div class="hrwhiteline"></div>
                  <div class="fivewidthdiv">Port Consumption (MT)</div>
                  <div class="hrwhiteline"></div>
                  <div class="fivewidthdiv">Received Fuel</div>
                  <div class="hrwhiteline"></div>
                  <div class="fivewidthdiv">Departure RoB</div>
                </div>
                <div class="bunkerdetail2">
                  <div class="smallwidthheader">Port</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">Miles</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">Function</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">Passage</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">TSD</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthheader">ECA/SECA Miles</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthheader">ECA/SECA Days</div>
                  <div class="hrline"></div>

                  <div class="extralargestwidthheader">Arrival Date/Time</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthheader">Departure Date/Time</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">IFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">VLSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">MGO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">IFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">VLSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">MGO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">IFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">VLSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">MGO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">IFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">VLSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">MGO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">IFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">VLSFO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthheader">MGO</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthheader">ULSFO</div>
                </div>

                <div class="bunkerdetailitem">
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>

                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="extralargestwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">LSMGO</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="smallwidthitem">XX</div>
                  <div class="hrline"></div>
                  <div class="mediumwidthitem">XX</div>
                </div>

                <div class="belowtable6">
                  <div class="bunkerdetail6">
                    <div class="smallwidthheader">Port</div>
                    <div class="hrline"></div>
                    <div class="smallwidthheader">Miles</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthheader">Function</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthheader">Passage</div>
                    <div class="hrline"></div>
                    <div class="extralargewidthheader">Speed Type</div>
                    <div class="hrline"></div>
                    <div class="smallwidthheader">Speed</div>
                    <div class="hrline"></div>
                    <div class="smallwidthheader">WF%</div>
                    <div class="hrline"></div>
                    <div class="smallwidthheader">TSD</div>
                    <div class="hrline"></div>
                    <div class="extralargewidthheader">Arrival date/time</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthheader">Ttl P Days</div>
                    <div class="hrline"></div>
                    <div class="extramostwidthheader">Departure Date/Time</div>
                  </div>

                  <div class="bunkerdetailitem6">
                    <div class="smallwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="smallwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="extralargewidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="smallwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="smallwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="smallwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="extralargewidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="mediumwidthitem">XX</div>
                    <div class="hrline"></div>
                    <div class="extramostwidthitem">XX</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="rightnavbar">
            <div class="verightnavicondiv">
              <div class="verighticon">
                <img src={IMAGE_PATH+"fullve-icons/pl_summary_icon.svg"} alt="" />
              </div>

              <div class="verightcon">
                <img src={IMAGE_PATH+"fullve-icons/attachment_icon.svg"}alt="" />
              </div>
              <div class="veirightcon">
                <img src={IMAGE_PATH+"fullve-icons/edit_icon.svg"} alt="" />
              </div>
              <div class="veirighticon">
                <img src={IMAGE_PATH+"fullve-icons/cii_icon.svg"} alt="" />
              </div>
              <div class="veirighticon">
                <img
                  src={IMAGE_PATH+"fullve-icons/map_intelligence_icon.svg"}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VoyageEstimateform;
