import React, { useEffect, useRef, useState } from "react";
import {
  Form,
  Input,
  Table,
  Tabs,
  Layout,
  Select,
  Row,
  Col,
  Modal,
  Button,
  Divider,
  Radio,
  InputNumber,
  List,
  Alert,
} from "antd";
import "../../styles/googlemap.scss";
import axios from "axios";
import mapboxgl from "mapbox-gl";
import "../../styles/style.scss";
//import "../track-my-fleet/mapstyle.css";
import "../track-vessel/mapstyle.css";
import "mapbox-gl/dist/mapbox-gl.css";
import Cookies from "universal-cookie";
import "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css";
import MapboxDraw from "@mapbox/mapbox-gl-draw";
import PortResultCalculation from "./portResultCalculation";
import StaticMode from "@mapbox/mapbox-gl-draw-static-mode";
import ShipDetails from "./ShipDetails";
import { getAPICall, useStateCallback } from "../../shared";
import {
  TO_FIXED_2_NUM,
  EMPTY_2_NUM,
  removeCommon,
  ldTermsOptions,
  FECTOR_VAL,
  DEFAULT_VALUE,
  addMapLayers,
  updateMapLineLayer,
  updateMapPointLayer,
  initformdata,
  getCo2ValFormula,
  tableColumns,
  formItemLayout,
  defRouteOptions,
  canalPassoptions,
  piracyoptions,
  routeOptions,
  getMapPlaceItem,
} from "./portUtil.js";
import URL_WITH_VERSION, {
  postAPICall,
  IMAGE_PATH,
  openNotificationWithIcon,
} from "../../shared";
import { LockOutlined } from "@ant-design/icons";
import useDebounce from "./customhook";
import SelectedVesselView from "../track-vessel/selected-vessel-view.js";
import FilterVessel from "../track-vessel/filterVessel.js";
import LiveVesselSearch from "../track-vessel/live-vessel-search.js";
import ToolBar from "./ToolBar.js";
import VesselLargeListFilter from "../track-vessel/vessel-large-list-filter.js";
import GenerateCargoEnquiry from "../chartering/routes/tcov/modals/GenerateCargoEnquiry.js";
import GenerateTonnageEnquiry from "../chartering/routes/tcov/modals/GenerateTonnageEnquiry.js";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index.js";


const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass =
  require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

const { Content } = Layout;
// const Option = Select.Option;
const { Option } = Select;

let allCoordinates = [];
let pointLayerData = [];
let mapClickTableData = [];

const FILTER_TYPES = {
  liveSearch: "live_search",
  localSearch: "local_search",
  allDataFilter: "all_data_filter",
};

const searchLabel = "Search Vessel by it's Name or IMO Number";

const PortMap = (props) => {

  const [portData, setPortData] = useStateCallback({
    checkedList: defRouteOptions,
    visibleModal: false,
    title: "",
    mapSettings: {
      center: { lat: 20.5937, lng: 78.9629 },
      zoom: 1,
    },
    triangleCoords: [],
    poly_lat_lng: [],
    loading: false,
    shipData: null,
    vessel_name: "Spring",
    port_from: "",
    port_to: "",
    distanceData: [],
    etaCalculated: "",
    spd: "",
    hraRadio: 1,
    ecaRadio: 1,
    fetchInProgress: false,
    loadPortInProgress: false,
    selectOptions: [],
    finalPortFrom: "",
    finalPortTo: "",
    coordinates: [],
    lng: 0.0,
    lat: 0.0,
    zoom: 1.06,
    portList: [], // Port list for dropdown and get detail for tables
    portPoints: [], // Port points for call APIs and set into table
    tableData: [], // Display table data
    mapBoxPoints: [],
    allCoordinates: [],
    port_lng: 0.0,
    port_lat: 0.0,
    map: {},
    coordinates: [],
    searchedItem: props.searchedItem,
    lng: props.data && props.data.length > 0 ? props.data[0].vessel_lat : 0.0,
    lat: props.data && props.data.length > 0 ? props.data[0].vessel_lon : 0.0,
    visibleModal: false,
    data: props.mapData,
    filterData: props.mapData,
    allData: props,
    selectedData: null,
    selectedPropData: null,
    serchedData: "all",
    filterType: "",
    liveSearchValue: "",
    liveSearchList: [],
    isLiveDetailLoading: false,
    liveSearchResult: searchLabel,
    showlivelocationdata: props.livemapiconid,
    isShowLocationFromtable: props.isShowLocationFromtable || false,
    isGlobalShipOpen: false,
    isVesselListShow: false,
    opencargoenquiryModal: false,
    openTonnageEnquiryModal: false,
  });
  const [projections, setprojections] = useState("");
  const [searchport, setSearchport] = useState("");
  const map = useRef(null);
  const MapRef = useRef(null);
  const drawRef = useRef(null);
  const [showDrawButtons, setShowDrawButtons] = useState(false);
  const [formdata, setFormData] = useState(initformdata);
  const [isDistancCalModal, setDistanceModal] = useState(false);
  const debouncedSearchTerm = useDebounce(searchport, 1000);
  const [isLoadedMap, setIsLoadedMap] = useState(false);
  const [featuresData, setFeaturesData] = useState([]);
  useEffect(() => {
    // Render map
    // map.current = new mapboxgl.Map({
    //   container: MapRef.current,
    //   projection: "mercator",
    //   style:  "mapbox://styles/mapbox/streets-v11",
    //   center: [-70.9, 42.35],
    //   zoom: 1,
    // });
    initilLoad();
    // return()=>{
    //   map.current?.remove();  // take care of this line
    // }
  }, [props?.mapData?.length]);



  useEffect(() => {
    const dataset = props.searchedItem;
    if (props.isShowLocationFromtable && props.searchedItem) {
      getVesselIdByImoNumber(dataset.imo_no, dataset, true);

    }
  }, [props.isShowLocationFromtable])



  const initilLoad = () => {
    let mapLocation = [];
    props?.mapData?.map((item) => {
      return mapLocation.push(getMapPlaceItem(item));
    });
    setFeaturesData(mapLocation);
    setIsLoadedMap(true);
  };

  useEffect(() => {
    if (isLoadedMap) {
      maprender();
    }
    return () => {
      setIsLoadedMap(false);
    };
  }, [isLoadedMap]);

  useEffect(() => {
    if (debouncedSearchTerm) {
      loadAPI(debouncedSearchTerm);
    }
  }, [debouncedSearchTerm]);

  const drawPolygon = (e) => {
    // console.log(e);
  };

  const showMarker = (item) => {
    const coordinates = item.geometry.coordinates.slice();
    const description = item.properties.description;
    new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(description)
      .addTo(portData.map);
  };

  const selectedListPopup = new mapboxgl.Popup({});

  const addVesselDetailPopup = (newItem) => {
    if (!newItem) {
      return;
    }

    // Add popup
    const markPopup = new mapboxgl.Popup({
      offset: [-50, 0],
      maxWidth: "200px",
      closeButton: true,
      closeOnClick: true,
    });

    const popupCordinate = [
      newItem.last_pos_lon ? newItem.last_pos_lon : 0.0,
      newItem.last_pos_lat ? newItem.last_pos_lat : 0.0,
    ];

    map.current.flyTo({
      center: popupCordinate,
    });

    markPopup
      .setLngLat(popupCordinate)
      .setHTML(
        ` <div class="live-popup">

     <div class="live-popup-wp">
       <div class="title-block">
      
         <div><b>Vessel Name: </b></div>
         <div><span>${newItem.vessel_name ? newItem.vessel_name : "NA"
        }</span></div>
       </div>
       <div class="title-block">
        
         <div><b>IMO: </b></div>
         <div><span>${newItem.imo_no ? newItem.imo_no : "NA"}</span></div>
       </div>
     </div>
     <div class="live-popup-wp">
      
       <div class="title-block">
         <div><b>Dead Weight: </b></div>
         <div><span>${newItem.dead_weight ? newItem.dead_weight : "NA"
        }</span></div>
       </div>
       
       <div class="title-block">
         <div><b>Type: </b></div>
         <div><span>${newItem.vt_verbose ? newItem.vt_verbose : "NA"
        }</span></div>
       </div>
     </div>

     <div class="port-detail">
       <div class="">
        
         <div><b>Last Port: </b> <span>${newItem.last_port && newItem.last_port.name
          ? newItem.last_port.name
          : "NA"
        }</span></div>
         <div><b>ATD: </b><span> ${newItem.last_port && newItem.last_port.atd
          ? newItem.last_port.atd
          : "NA"
        } </span></div>
       </div>
       <div class="">
         <div><b>Next Port: </b> <span>${newItem.next_port && newItem.next_port.name
          ? newItem.next_port.name
          : "NA"
        }</span></div>
         <div><b>ETA: </b><span>${newItem.voyage && newItem.voyage.eta ? newItem.voyage.eta : "NA"
        }</span></div>
       </div>
     </div>
     <div class="port-detail">
       <div class="">
         <div><b>Current Position: </b> <span span class="green">${newItem.position && newItem.position.location_str
          ? newItem.position.location_str
          : "NA"
        }</span></div>
       </div>
       <div class="">
         <div><b>Destination: </b> <span span class="red"><b>${newItem.voyage && newItem.voyage.destination
          ? newItem.voyage.destination
          : "NA"
        }</b></span></div>
       </div>
     </div>

     <div class="cii-block">
       <div class="cii-title">
         <div><b>CII Simulator: </b></div>
         </div>

         <div class="cii-item">
           <div class="cii-col">
             <b>Distance travelled: </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
           <div class="cii-col">
             <b>Co2 factor (g/t): </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
         </div>
         <div class="cii-item">
           <div class="cii-col">
             <b>Fuel consumed: </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
           <div class="cii-col">
             <b>Co2 emission: </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
         </div>
         <div class="cii-item">
           <div class="cii-col">
             <b>CII Rating YTD: </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
           <div class="cii-col">
             <b>CII Voyage Rating: </b>
             <span><i class="fas fa-lock yellow-icon-color"></i></span>
           </div>
         </div>

     </div>

   </div>`
      )
      .addTo(map.current);
  };

  const updateMapData = (mapData = [], sourceType = "places") => {
    let mapLocation = [];
    mapData.map((item) => {
      return mapLocation.push(getMapPlaceItem(item));
    });

    map.current.getSource(sourceType).setData({
      type: "FeatureCollection",
      features: mapLocation,
    });
    setPortData(
      (prev) => ({ ...prev, featuresData: mapLocation }),
      () => maprender()
    );
  };

  // const postVesselData = async (dataset = {}) => {
  //   // console.log(">>>>>> dataset", dataset);
  //   if (!dataset.imo_number) {
  //     return;
  //   }

  //   const postData = {
  //     imo_no: `${dataset.imo_number}`,
  //     vessel_name: dataset.name,
  //     vessel_id: dataset.vessel_id || null,
  //     speed: dataset.position ? dataset.position.speed : "",
  //     last_pos_lat: dataset.position ? dataset.position.latitude : null,
  //     last_pos_lon: dataset.position ? dataset.position.longitude : null,
  //     status: dataset.position ? dataset.position.nav_status : null,
  //     current_port_name: dataset.position
  //       ? dataset.position.location_str
  //       : null,
  //     vessel_last_pos: dataset.vessel_last_pos || null,
  //     mmsi_number: dataset.mmsi_number || null,
  //     view: 1,
  //     my_fleet: 0,
  //     degree: 0,
  //     toc_vessel_details: 0,
  //     deadWeight: null,
  //     new_name: dataset.name,
  //     vessel_type: dataset.vt_verbose || null,
  //     ata: dataset.last_port ? dataset.last_port.ata : null,
  //     atd: dataset.last_port ? dataset.last_port.atd : null,
  //     last_port: dataset.last_port ? dataset.last_port.name : null,
  //     next_port: dataset.next_port ? dataset.next_port.name : null,
  //     eta_calc: dataset.next_port ? dataset.next_port.eta_calc : null,
  //     destination: dataset.voyage ? dataset.voyage.destination : null,
  //     eta: dataset.voyage ? dataset.voyage.eta : null,
  //     received: dataset.voyage ? dataset.voyage.received : null,
  //   };

  //   const url = `${URL_WITH_VERSION}/vessel/livedetails/save`;
  //   await postAPICall(url, postData, "post", (res) => res);
  // };

  const getSelectedData = async (dataset) => {
    selectedListPopup.remove();
    const {
      vessel_name,
      current_port_name,
      degree,
      imo_no,
      last_pos_lat,
      last_pos_lon,
      speed,
      status,
      mmsi,
    } = dataset;

    let tempSelectedData = {
      IMO: imo_no,
      last_port: {
        ata: "NA",
        atd: "NA",
        locode: "NA",
        name: "NA",
      },
      MMSI: mmsi,
      SHIPNAME: vessel_name,
      NEXT_PORT_NAME: null,
      position: {
        course_over_ground: 188.3,
        LAT: last_pos_lat,
        location_str: current_port_name,
        LON: last_pos_lon,
        nav_status: "NA",
        received: "NA",
        SPEED: speed,
        HEADING: 10,
      },
      vessel_id: "NA",
      voyage: {
        DESTINATION: "NA",
        DRAUGHT: "NA",
        ETA: "NA",
        received: "NA",
      },
    };

    if (imo_no) {
      try {
        const response = await getAPICall(
          `https://apiservices.theoceann.com/marine/vessel-position-imo/${imo_no}`
        );
        const data = await response;
        console.log("responseeeee", response);

        if (data.length > 0) {
          tempSelectedData = {
            IMO: imo_no,
            last_port: {
              ata: "NA",
              atd: "NA",
              locode: "NA",
              name: "NA",
            },
            MMSI: data[0].maritimemobileserviceidentitymmsinumber,
            SHIPNAME: vessel_name,
            NEXT_PORT_NAME: null,
            position: {
              course_over_ground: 188.3,
              LAT: last_pos_lat,
              location_str: current_port_name,
              LON: last_pos_lon,
              nav_status: data[0].shipstatuss,
              received: "NA",
              SPEED: speed,
              HEADING: 10,
            },
            vessel_id: "NA",
            voyage: {
              DESTINATION: "NA",
              DRAUGHT: data[0].draught,
              ETA: "NA",
              received: "NA",
            },
          };

          setPortData((prev) => ({
            ...prev,
            selectedData: tempSelectedData,
            selectedPropData: dataset,
            filterType: "",
          }));
        } else {
          openNotificationWithIcon("err", "Imo No is not available", 3);
        }
      } catch (err) {
        // console.log(err);
        openNotificationWithIcon("error", "Something Went wrong.", 3);
        setPortData((prev) => ({
          ...prev,
          selectedData: tempSelectedData,
          selectedPropData: dataset,
          filterType: "",
        }));
      }
    } else {
      openNotificationWithIcon("err", "Imo No is not available", 3);
    }
  };

  const searchIconClick = () => {
    setPortData((prev) => ({
      ...prev,
      filterType: FILTER_TYPES.liveSearch,
      liveSearchValue: "",
      selectedData: null,
      isGlobalShipOpen: true,
    }));
  };

  const filterIconClick = () => {
    setPortData((prev) => ({
      ...prev,
      filterType: FILTER_TYPES.localSearch,
      selectedData: null,
    }));
  };

  const onSearchDbData = () => {
    setPortData((prev) => ({
      ...prev,
      filterType: FILTER_TYPES.allDataFilter,
      serchedData: "all",
      selectedData: null,
    }));
  };

  const getVesselImageByImoNo = async (imoNumber, vesselId) => {
    if (!imoNumber && !vesselId) {
      return false;
    }

    let params = "";
    if (imoNumber) {
      params = `imo_number=${imoNumber}`;
    } else {
      params = `vessel_id=${vesselId}`;
    }

    const url = `https://apiservices.theoceann.com/marine/vessel-position-imo/${params}`;

    try {
      return await fetch(url, {
        method: "GET",
        headers: {
          "access-control-allow-origin": "*",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((res) => {
          return res;
        });
    } catch (error) {
      //  console.log("> IMAGE NOT AVAILBLE FOR THIS VESSEL");
      return false;
    }
  };

  const getVesselIdByImoNumber = async (
    imoNumber,
    localDataset,
    isNotShowInMap
  ) => {
    setPortData((prev) => ({
      ...prev,
      isLiveDetailLoading: true,
      selectedData: {},
      showlivelocationdata: true,
    }));
    try {
      const url = `https://apiservices.theoceann.com/marine/vessel-position-imo/${imoNumber}`;
      const result = await getAPICall(url
      )

      console.log("resultt", result);
      const dataset = result && result.vessels && result.vessels[0];
      if (dataset && dataset.vessel_id) {
        setPortData({
          //  isLiveDetailLoading: false,
          showlivelocationdata: false,
        });

        await handleGetLiveVesselItemData(dataset, isNotShowInMap);
      } else {
        // If not found the fetch from the-oceann db
        // getSelectedData(localDataset);
      }
    } catch (e) {
      setPortData((prev) => ({ ...prev, selectedData: null }));
    }
    setPortData((prev) => ({ ...prev, showlivelocationdata: false }));
  };

  const handleGetLiveVesselItemData = async (dataset, isNotShowInMap) => {
    const { SHIPNAME, IMO, vessel_id, vt_verbose ,MMSI} = dataset;
    console.log("mmsi",MMSI);

    const tempSelectedData = {
      IMO: 0,
      last_port: {
        ata: "2022-12-01T10:24:33Z",
        atd: "2022-12-01T10:24:33Z",
        locode: "",
        name: "",
      },
      MMSI: 0,
      SHIPNAME: "",
      next_port: {
        ETA_CALC: "2022-12-01T10:24:33Z",
        locode: "",
        name: "",
        DISTANCE_TRAVELLED: 0,
        travel_time_h: 0,
      },
      position: {
        course_over_ground: 0,
        LAT: 0,
        location_str: "",
        LON: 0,
        nav_status: "",
        received: "2022-12-01T10:24:33Z",
        SPEED: 0,
        HEADING: 0,
      },
      request_limit_info: {
        left_requests: 0,
        max_requests: 0,
        used_requests: 0,
      },
      vessel_id: 0,
      voyage: {
        DESTINATION: "",
        DRAUGHT: 0,
        ETA: "2022-12-01T10:24:33Z",
        received: "2022-12-01T10:24:33Z",
      },
    };

    const tempSelectedPropData = {
      vessel_name: SHIPNAME,
      imo_no: IMO,
      current_port_name: "NA",
      last_pos_lat: "NA",
      last_pos_lon: "NA",
      mmsi: MMSI,
    };
    const mmsi = tempSelectedPropData.mmsi;
    console.log("mmsi2",mmsi);
    setPortData((prev) => ({ ...prev, isLiveDetailLoading: true }));
     
    try {
      let url = `https://apiservices.theoceann.com/marine/vessel-position-imo/${mmsi}`;

      const data = await getAPICall(url)


      console.log("dataaa", data);

      // this.postVesselData({ ...data, vt_verbose });
      setPortData((prev) => ({
        ...prev,
        selectedData: data,
        selectedPropData: tempSelectedPropData,
        filterType: "",
        isLiveDetailLoading: false,
      }));


      selectedListPopup.remove();
      // const cordinates = [
      //   data.position
      //     ? data.position.latitude > -90 && data.position.latitude < 90
      //       ? data.position.latitude
      //       : 0
      //     : 0,
      //   data.position
      //     ? data.position.longitude > -90 && data.position.longitude < 90
      //       ? data.position.longitude
      //       : 0
      //     : 0,
      // ];

      // Add to map and update the map
      if (!isNotShowInMap && data.IMO && data.SHIPNAME) {
        const mapDataItem = {
          imo_no: `${data.IMO}`,
          vessel_name: data.SHIPNAME,
          speed: data.position.SPEED,
          vessel_lat: data.position.LAT || null,
          vessel_lon: data.position.LON || null,
          vessel_status: data.position.nav_status || null,
          current_port_name: data.CURRENT_PORT || null,
          last_pos_lon: data.position.LON,
          last_pos_lat: data.position.LAT,
        };

        addNewSourceOnMapData(mapDataItem);
      }

      //  liveVesselUpdateinTable(data);

    } catch (error) {
      openNotificationWithIcon("error", "something went wrong", 3);
      //   console.log(error);
      setPortData((prev) => ({
        ...prev,
        selectedData: tempSelectedData,
        selectedPropData: tempSelectedPropData,
        filterType: "",
        isLiveDetailLoading: false,
      }));
    }

    setPortData((prev) => ({
      ...prev,
      selectedData: tempSelectedData,
      selectedPropData: tempSelectedPropData,
      filterType: "",
      isGlobalShipOpen: false,
    }));
  };

  const liveVesselUpdateinTable = async (data) => {
    const mapDataItem = {
      imo_no: `${data.IMO}`,
      vessel_name: data.SHIPNAME,
      speed: data.position.SPEED,
      vessel_lat: data.position.LAT || null,
      vessel_lon: data.position.LON || null,
      vessel_status: data.position.nav_status || null,
      current_port_name: data.CURRENT_PORT || null,
      last_pos_lon: data.position.LON,
      last_pos_lat: data.position.LAT,
      lastPort: data.last_port.LAST_PORT,
      ATA: data.last_port.ata,
      ETA: data.voyage.ETA,
      destination: data.voyage.DESTINATION,
      nextPort: data.NEXT_PORT_NAME,
    };

    addNewSourceOnMapData(mapDataItem);

    let url = `https://apiservices.theoceann.com/marine/vessel-position-imo/${mapDataItem.imo_no}`;

    try {
      await postAPICall(url, data, "put", (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message, 3);
          console.log("error in post call");
        } else {
          console.log("error in post call");
          openNotificationWithIcon("error", response.message, 3);
        }
      });
    } catch (err) {
      openNotificationWithIcon("error", "something went wrong", 3);
      console.log("error in post call");
    }
  };

  const addNewSourceOnMapData = (newItem) => {
    const { selectedData } = portData;
    if (!newItem || !newItem.imo_no) {
      return;
    }

    const mapData = [newItem];
    // Add popup

    addVesselDetailPopup(newItem);

    updateMapData(mapData, "liveVessel");
  };

  const handleLiveSearchInput = async (e) => {

    setPortData((prev) => ({
      ...prev,
      liveSearchValue: e.target.value,
      liveSearchList: [],
      isLiveSearchLoading: true,
      liveSearchResult: "Searching your request",
    }));
    let url = "";
    if (isNaN(e.target.value)) {
      if (e.target.value.length > 3) {
        console.log('hello')
        // url = `${URL_WITH_VERSION}/VesselTrackApis/search-vessal?vessal_name=queen`;
        url = `https://apiservices.theoceann.com/marine/get-vessels-name/${e.target.value}`
      }
    } else {
      // url = `${process.env.REACT_APP_VESSEL_SEARCH}?apikey=${process.env.REACT_APP_VESSEL_API_KEY}&imo_number=${e.target.value}`;
      console.log("cant find")
      url = `https://apiservices.theoceann.com/marine/vessel-position-imo/${e.target.value}`
    }

    if (!url) {
      return;
    }


    try {
      setPortData((prev) => ({ ...prev, isLiveSearchLoading: false }));
      // fetch(url, {
      //   method: "GET",
      //   headers: {
      //     "access-control-allow-origin": "*",
      //     Accept: "application/json",
      //     "Content-Type": "application/json",
      //   },
      // })
      //   .then((res) => res.json())
      //   .then((data) => {
      //     if (data.status !== "error") 
      //    {
      //       if (data.vessels.length > 0) {
      //         setPortData((prev) => ({
      //           ...prev,
      //           liveSearchList: data.vessels,
      //         }));
      //       } else {
      //         setPortData((prev) => ({
      //           ...prev,
      //           liveSearchResult: "No data found",
      //         }));
      //       }
      //     }
      //     setPortData((prev) => ({
      //       ...prev,
      //       isLiveSearchLoading: false,
      //     }));
      //   });
      console.log("url", url);
      const response = await getAPICall(url);
      console.log('response', response)
      // const data=(response.data);

      if (response.status !== "error") {
        console.log("hsvsj");

        if (response.length > 0) {
          setPortData((prev) => ({
            ...prev,
            liveSearchList: response,
          }));
        } else {
          setPortData((prev) => ({
            ...prev,
            liveSearchResult: "No data found",
          }));
        }
      }
      setPortData((prev) => ({
        ...prev,
        isLiveSearchLoading: false,
      }));
    } catch (error) {
      //  console.log(error);
      setPortData((prev) => ({
        ...prev,
        liveSearchList: [],
        isLiveSearchLoading: false,
        liveSearchResult: "searching...",
      }));
    }
  };

  const handleLocalDataFilter = () => {
    const searchText = portData.serchedData;

    const filterData = portData.data.filter(
      (v) =>
        searchText.toLowerCase() === "all" ||
        v.imo_no.includes(searchText) ||
        v.vessel_name.toLowerCase().includes(searchText.toLowerCase())
    );
    setPortData((prev) => ({ ...prev, filterData }));
    updateMapData(filterData);
  };

  let searchTimer = null;

  const handleSearchValue = (event) => {
    const serchedData =
      event.target.value.length > 0 ? event.target.value : "all";

    setPortData((prev) => ({ ...prev, serchedData }));
    clearTimeout(searchTimer);
    searchTimer = setTimeout(() => {
      handleLocalDataFilter();
    }, 1000);
  };

  const addmyfleet = async (IMO) => {
    setPortData((prev) => ({ ...prev, isLiveDetailLoading: true }));
    if (IMO) {
      try {
        await postAPICall(
          `${URL_WITH_VERSION}/vessel/add-my-fleet`,
          { IMO: IMO.toString(), fleet_value: 1 },
          "post",
          (data) => {
            if (data && data.data) {
              openNotificationWithIcon("success", data.message, 5);
              setPortData((prev) => ({ ...prev, isLiveDetailLoading: false }));
            }
          }
        );
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong", 5);
      }
    } else {
      openNotificationWithIcon("error", "Something went wrong.", 5);
    }
  };

  const maprender = () => {
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "LineString",
            properties: {},
            coordinates: portData.allCoordinates ? portData.allCoordinates : [],
          },
        },
      ],
    };

    map.current = new mapboxgl.Map({
      container: MapRef.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: { lat: 20.5937, lng: 78.9629 },
      zoom: 1.5,
      projection: projections,
    });

    map.current.on("load", () => {
      map.current.loadImage(
        IMAGE_PATH + "icons/mapArrowYellow.png",
        (error, image) => {
          if (error) {
            return;
          }
          map.current.addImage("yellowIcon", image);
        }
      );

      map.current.loadImage(
        IMAGE_PATH + "icons/mapArrowUnknown.png",
        (error, image) => {
          if (error) {
            return;
          }
          map.current.addImage("unknownIcon", image);
        }
      );

      map.current.loadImage(IMAGE_PATH + "icons/marker.png", (error, image) => {
        if (error) {
          return;
        }
        map.current.addImage("orangeIcon", image);
      });

      map.current.loadImage(
        IMAGE_PATH + "icons/mapArrowGreen.png",
        (error, image) => {
          if (error) {
            return;
          }
          map.current.addImage("greenIcon", image);
        }
      );

      map.current.loadImage(
        IMAGE_PATH + "icons/map-icon-pointer.png",
        (error, image) => {
          if (error) throw error;
          map.current.addImage("custom-marker", image);

          map.current.addSource("places", {
            // This GeoJSON contains features that include an "icon"
            // property. The value of the "icon" property corresponds
            // to an image in the Mapbox Streets style's sprite.

            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: featuresData,
            },
          });

          // Add a layer showing the places.
          map.current.addLayer({
            id: "places",
            type: "symbol",
            source: "places",
            layout: {
              "icon-image": "greenIcon",
              "icon-allow-overlap": true,
              "icon-rotate": ["get", "rotation"],
              "icon-ignore-placement": true,
            },
          });

          // Create a popup, but don't add it to the map yet.

          const hoverpopup = new mapboxgl.Popup({
            maxWidth: "10vw",
            closeButton: false,
            closeOnClick: false,
          });

          // When a click event occurs on a feature in the places layer, open a popup at the
          // location of the feature, with description HTML from its properties.

          map.current.on("click", "places", (e) => {
            // Copy coordinates array.

            try {
              const dataset =
                JSON.parse(e.features[0].properties.dataset) || {};
              if (Object.keys(dataset).length) {
                getVesselIdByImoNumber(dataset.imo_no, dataset, true);
              }

              const addedMarker = document.getElementById("dottedArrowMarker");

              if (addedMarker) {
                addedMarker.remove();
              }
              const markerImage = document.createElement("img");
              markerImage.src = IMAGE_PATH + "icons/dottedArrow.png";
              markerImage.width = 140;
              markerImage.height = 20;
              markerImage.id = "dottedArrowMarker";

              new mapboxgl.Marker(markerImage, {
                rotation: e.features[0].properties.rotation,
              })
                .setLngLat([
                  parseFloat(dataset.last_pos_lon),
                  parseFloat(dataset.last_pos_lat),
                ])
                .addTo(map.current);
            } catch (error) {
              console.log(">>> ERROR", error);
            }
          });

          map.current.addSource("liveVessel", {
            // This GeoJSON contains features that include an "icon"
            // property. The value of the "icon" property corresponds
            // to an image in the Mapbox Streets style's sprite.

            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [],
            },
          });

          // Add a layer showing the places.
          map.current.addLayer({
            id: "liveVessel",
            type: "symbol",
            source: "liveVessel",
            layout: {
              "icon-image": "yellowIcon",
              "icon-allow-overlap": true,
            },
          });

          map.current.on("click", "liveVessel", (e) => {
            // Copy coordinates array.
            try {
              const dataset =
                JSON.parse(e.features[0].properties.dataset) || {};
              if (Object.keys(dataset).length) {
                getVesselIdByImoNumber(dataset.imo_no, dataset, true);
              }
            } catch (error) {
              //  console.log(">>> ERROR", error);
            }
          });

          // Change the cursor to a pointer when the mouse is over the places layer.

          map.current.on("mouseenter", "places", (e) => {
            const coordinates = e.features[0].geometry.coordinates.slice();
            const description = e.features[0].properties.hoverProperties;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
              coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            hoverpopup
              .setLngLat(coordinates)
              .setHTML(description)
              .addTo(map.current);

            map.current.getCanvas().style.cursor = "pointer";
          });

          // Change it back to a pointer when it leaves.
          map.current.on("mouseleave", "places", () => {
            map.current.getCanvas().style.cursor = "";
            hoverpopup.remove();
          });
        }
      );
    });

    const modes = MapboxDraw.modes;
    modes.static = StaticMode;

    drawRef.current = new MapboxDraw({
      displayControlsDefault: false,
      controls: {},
      modes: modes,
    });

    map.current.addControl(drawRef.current);

    map.current.on("draw.create", drawPolygon);
    map.current?.on("draw.update", drawPolygon);
    map.current.on("draw.delete", drawPolygon);

    map.current.on("load", () => {
      drawRef.current.changeMode("static");
      addMapLayers(map.current);
    });

    map.current.on("click", "points_layer", (e) => {
      e.clickOnLayer = true;
      setPortData((prev) => ({
        ...prev,
        visibleModal: true,
        port_lat: e.lngLat.lat,
        port_lng: e.lngLat.lng,
      }));
    });

    map.current.on("mousemove", "points_layer", (e) => {
      map.current.getCanvas().style.cursor = "pointer";
      e.clickOnLayer = true;
    });

    map.current.on("mouseleave", "points_layer", (e) => {
      map.current.getCanvas().style.cursor = "";
    });

    map.current.on("move", () => {
      setPortData((prev) => ({
        ...prev,
        lng: map.current.getCenter().lng.toFixed(4),
        lat: map.current.getCenter().lat.toFixed(4),
        zoom: map.current.getZoom().toFixed(2),
      }));
    });

    map.current.on("click", async (e) => {
      if (e.clickOnLayer) return;
      if (drawRef.current.getMode() !== "static") return;

      const pointData = {
        type: "Feature",
        geometry: { type: "Point", coordinates: [e.lngLat.lng, e.lngLat.lat] },
        properties: {},
      };

      pointLayerData.push(pointData);

      if (pointLayerData.length > 5) {
        showError("maximum 5 poins allowed");
        return;
      }

      const portPoint = {
        PortID: "custom",
        country_code: "",
        country_id: "",
        created_on: "",
        eca: 0,
        id: "custom_" + e.lngLat.lng.toString(),
        latitude: e.lngLat.lat.toFixed(2).toString(),
        locode: "",
        longitude: e.lngLat.lng.toFixed(2).toString(),
        low_sulphur: "",
        port_name: "custom",
        port_type: "water",
        status: "",
      };

      mapClickTableData.push({
        id: "custom_" + e.lngLat.lng.toFixed(2).toString(),
        PortID: "",
        port_name: "custom",
        distance: "0.000",
        crossed: " - ",
        eca: "0.000",
        seca: "0.000",
        spd: "0.000",
        seaDays: "0",
        vlsfoPerDay: "0.000",
        seaCons: "0.000",
        lsmgo: 0,
        portCons: 0,
        ttlBunkercons: 0,
        cargo: 0,
        ldRate: 0,
        ldTerm: "",
        pDays: 0,
        totalVDays: 0,
        portExp: 0,
        co2: 0,
        serialNumber: mapClickTableData.length + 1,
      });

      setPortData((prev) => ({
        ...prev,
        mapBoxPoints: pointLayerData,
        // portPoints: [...prev.portPoints, portPoint],
      }));

      if (pointLayerData.length >= 2 && pointLayerData.length <= 5) {
        const data = pointLayerData.slice(-2);
        const from_long = data[0].geometry.coordinates[0];
        const from_lat = data[0].geometry.coordinates[1];
        const to_long = data[1].geometry.coordinates[0];
        const to_lat = data[1].geometry.coordinates[1];

        const _method = "post";
        // const constraints = getContainerStringFromChecklist();
        const constraints = "canal_pass_code=111";
        const postData = {
          from_lat: from_lat.toFixed(6).toString(),
          from_long: from_long.toFixed(6).toString(),
          to_lat: to_lat.toFixed(6).toString(),
          to_long: to_long.toFixed(6).toString(),
          constraints,
        };

        postAPICall(
          `${URL_WITH_VERSION}/port/distanceaqua`,
          postData,
          _method,
          (data) => {
            const features = data.data.features[0];
            if (features.geometry) {
              const switchLngLat = features.geometry.coordinates.map(
                (coord) => {
                  const lngLat = [coord[1], coord[0]];
                  return lngLat;
                }
              );

              allCoordinates.push(...switchLngLat);

              mapClickTableData = mapClickTableData.filter(
                (res) =>
                  res.id !== "custom_" + e.lngLat.lng.toFixed(2).toString()
              );

              mapClickTableData.push({
                id: "custom_" + e.lngLat.lng.toFixed(2).toString(),
                PortID: "",
                port_name: "custom",
                distance: Math.round(features.properties.total_length),
                crossed: " - ",
                eca: "0.000",
                seca: features.properties.seca_length,
                spd: features.properties.eta.averageVesselSpeedOverGround,
                seaDays: Math.round(features.properties.eta.totalDurationDays),
                vlsfoPerDay: "0.000",
                seaCons: "0.000",
                lsmgo: 0,
                portCons: 0,
                ttlBunkercons: 0,
                cargo: 0,
                ldRate: 0,
                ldTerm: "",
                pDays: 0,
                totalVDays: 0,
                portExp: 0,
                co2: 0,
                serialNumber: mapClickTableData.length + 1,
              });

              const footerCol = getFooterColumn(mapClickTableData);

              setPortData((prev) => ({
                ...prev,
                allCoordinates,
                tableData: [...mapClickTableData, footerCol],
              }));
            }
            if (data.data.error) {
              showError(
                "Please select another coordinate or select port from dropdown or clear the map"
              );
              pointLayerData.pop();
              const portPoints = portData.portPoints;
              if (portPoints.length) {
                portPoints.pop();
              }

              mapClickTableData = mapClickTableData.filter(
                (res) =>
                  res.id !== "custom_" + e.lngLat.lng.toFixed(2).toString()
              );

              setPortData((prev) => ({
                ...prev,
                mapBoxPoints: pointLayerData,
                portPoints,
                tableData: mapClickTableData,
              }));
            }
          }
        );
      }
    });
  };

  const canalOnChange = (e) => {
    setFormData((prev) => ({ ...prev, canalOptions: e.target.value }));
  };

  const piracyOnChange = (e) => {
    setFormData((prev) => ({ ...prev, piracyArea: e.target.value }));
  };

  useEffect(() => {
    if (!map.current) return;
    if (portData.allCoordinates) {
      updateMapLineLayer({
        map: map.current,
        lineLayerData: portData.allCoordinates,
      });
    }

    if (portData.mapBoxPoints) {
      updateMapPointLayer({
        map: map.current,
        pointLayerData: portData.mapBoxPoints,
      });
    }
  }, [portData?.allCoordinates?.length, portData?.mapBoxPoints?.length]);

  const onClearHandler = () => {
    allCoordinates = [];
    pointLayerData = [];
    mapClickTableData = [];
    setPortData((prev) => ({
      ...prev,
      allCoordinates,
      mapBoxPoints: pointLayerData,
      tableData: mapClickTableData,
      portPoints: [],
    }));
  };

  const onMapHandler = () => {
    if (projections == "mercator") {
      setprojections("globe");
      map.current.setProjection("globe");
    } else {
      setprojections("mercator");
      map.current.setProjection("mercator");
    }
  };

  const loadAPI = (portName) => {

    setPortData({ ...portData, loadPortInProgress: true });
    const token = localStorage.getItem("oceanToken");
    const lastClickedItem = localStorage.getItem("lastClickedItem");
    let getHeaders = {};
    getHeaders["CONTENT-TYPE"] = "application/json";
    getHeaders["Access-Control-Allow-Origin"] = "*";
    getHeaders["Authorization"] = token;
    getHeaders["endpoint"] = lastClickedItem;
    // getHeaders['Authorization'] = ""
    /* not this use authToken way because tomorrow it  will use authToken way */
    if (portName && portName.length > 2)
      getHeaders["X-FILTER"] = JSON.stringify(loadHeaders(portName));

    axios({
      method: "GET",
      url: `${process.env.REACT_APP_URL}v1/port/list?t=port&p=1&l=10`,
      headers: getHeaders,
    })
      .then((res) => {
        let dataRes = [];
        if (
          (res != undefined || res.data != undefined) &&
          res.data.data != false
        ) {
          res.data.data.forEach((element) => {
            dataRes.push({
              value: element.port_name,
              label: element.port_name,
            });
          });

          //setPortData({ ...portData, selectOptions: [...dataRes], portList: res.data.data })



          portData.selectOptions = dataRes;
          portData.portList = res.data.data;

          setPortData(
            (prevState) => ({
              ...prevState,
              selectOptions: portData.selectOptions,
              portList: portData.portList,
            }),
            () => {
              setPortData({ ...portData, loadPortInProgress: false });
            }
          );
        }
        // setPortData({ ...portData, loadPortInProgress: false });   //not updated



      })
      .catch((err) => {
        setPortData({ ...portData, loadPortInProgress: false });
      });
    setIsLoadedMap(true);
  };

  const showHideModal = (val, data) =>
    setPortData((prev) => ({
      ...prev,
      visibleModal: val,
      shipData: data,
    }));

  const onChange = (checkedList) => {
    setPortData({
      ...portData,
      checkedList,
      indeterminate:
        !!checkedList.length && checkedList.length < routeOptions.length,
      checkAll: checkedList.length === routeOptions.length,
    });
  };

  // Set table list for all points
  const getTableDataFromAndToItem = (
    distanceData,
    fromPortId,
    toPortId,
    isFirstItem
  ) => {
    const portPoints = portData.portPoints;

    const fromPortPoint = portPoints.find(
      (pointDetail) => pointDetail.PortID === fromPortId
    );
    const toPortPoint = portPoints.find(
      (pointDetail) => pointDetail.PortID === toPortId
    );
    const items = [];

    const vlsfoPerDay = DEFAULT_VALUE.vlsfo;
    const lsmgo = DEFAULT_VALUE.lsmgo;

    // From data
    if (fromPortPoint && isFirstItem) {
      items.push({
        id: fromPortPoint.id,
        PortID: fromPortPoint.PortID,
        port_name: fromPortPoint.port_name,
        distance: "0.000",
        crossed: " - ",
        eca: "0.000",
        seca: "0.000",
        spd: "0.000",
        seaDays: "0",
        vlsfoPerDay: "0.000",
        seaCons: "0.000",
        lsmgo: 0,
        portCons: 0,
        ttlBunkercons: 0,
        cargo: 0,
        ldRate: 0,
        ldTerm: ldTermsOptions[0].value,
        pDays: 0,
        totalVDays: 0,
        portExp: 0,
        co2: 0,
      });
    }

    if (toPortPoint) {
      const spd =
        Number(distanceData.spd) > 0 ? distanceData.spd : DEFAULT_VALUE.speed;
      const seaDays =
        distanceData.distance > 0
          ? Number(distanceData.distance / Number(Number(spd) * 24))
          : 0.0;
      const seaCons = seaDays > 0 ? Number(seaDays) * vlsfoPerDay : 0.0;

      const co2 = getCo2ValFormula({
        seaDays,
        vlsfoPerDay,
        lsmgo,
        portCons: 0,
      });

      items.push({
        id: toPortPoint.id,
        PortID: toPortPoint.PortID,
        port_name: toPortPoint.port_name,
        distance: distanceData.distance,
        eca: distanceData.eca,
        seca: distanceData.seca,
        spd: spd,
        eta: distanceData.eta,
        hra: distanceData.hra,
        crossed: distanceData.crossed || " - ",
        seaDays: Number(seaDays).toFixed(TO_FIXED_2_NUM),
        vlsfoPerDay,
        seaCons: Number(seaCons).toFixed(TO_FIXED_2_NUM),
        lsmgo,
        portCons: 0,
        ttlBunkercons: Number(seaDays).toFixed(TO_FIXED_2_NUM), // seaCons + portCons (seaCons + 0) = seaCons
        cargo: 0,
        co2,
        ldRate: 0,
        ldTerm: ldTermsOptions[0].value,
        pDays: 0,
        totalVDays: Number(seaDays).toFixed(TO_FIXED_2_NUM), // seaDays + pDay
        portExp: 0,
      });
    }

    return items;
  };

  // Set port point
  const setPortPoints = (portName) => {
    const portPoints = portData?.portPoints || [];
    if (portPoints?.length >= 5) {
      showError("Port point must be less then 5");
      return;
    }
    const portList = portData.portList;
    const pointDetail = portList.find((i) => i.port_name === portName);
    if (!pointDetail || !pointDetail.id) {
      return;
    }

    const portPointItem = { ...pointDetail };

    setPortData({
      ...portData,
      portPoints: [...portPoints, portPointItem],
    });
  };

  const onChangeAddDistances = (value, ss) => {
    setPortPoints(value);
    setFormData((prev) => ({
      ...prev,
      portsInfo: [...formdata.portsInfo, value],
    }));
  };

  const onSearchPortList = (value) => {
    setSearchport(value);
  };

  const onChangelocalEcaRadio = (e) => {
    setFormData((prev) => ({
      ...prev,
      localEca: e.target.value,
    }));
  };

  const onChangeEcaRadio = (e) => {
    setFormData((prev) => ({
      ...prev,
      seca: e.target.value,
    }));
  };

  function convertToDirection(latitude, longitude) {
    let latDirection = latitude >= 0 ? "N" : "S";
    let longDirection = longitude >= 0 ? "E" : "W";

    return {
      latDirection: latDirection,
      longDirection: longDirection,
    };
  }

  const showError = (errors) => {
    const errorMsg = (
      <div className="notify-error">
        <div className="row">
          {Array.isArray(errors) ? (
            errors.map((msg, index) => (
              <div className="col-sm-12" key={index}>
                {msg}
              </div>
            ))
          ) : (
            <div className="col-sm-12">{errors}</div>
          )}
        </div>
      </div>
    );

    openNotificationWithIcon("error", errorMsg);
  };

  // Get continers string from check list and HRA, ECA
  const getContainerStringFromChecklist = () => {
    const checkedList = portData.checkedList;
    const selectedRoutes = routeOptions.filter((routepoints) => {
      let arr = checkedList.filter(
        (checkedRoute) => checkedRoute === routepoints
      );
      return !(arr.length === 0);
    });

    const routeConstraints = selectedRoutes.map((data) => {
      return `${data}=true`
        .replace(" Canal", "")
        .replace(" Passage", "")
        .replaceAll(" ", "")
        .toLowerCase();
    });
    const unmatchConstraints = removeCommon(routeOptions, checkedList).map(
      (data) => {
        return `${data}=false`
          .replace(" Canal", "")
          .replace(" Passage", "")
          .replaceAll(" ", "")
          .toLowerCase();
      }
    );
    // const routeOptionsList = [...routeConstraints, ...unmatchConstraints] // Not used code
    // fetch hra/eca/jcw addon options
    let strExtConstraints = "";
    if (portData.hraRadio == 2) {
      strExtConstraints += "&hra=minimize";
    }
    if (portData.ecaRadio == 2) {
      strExtConstraints += "&eca=minimize";
    }
    const finalListforAPI = [...routeConstraints, ...unmatchConstraints];

    const constraints = finalListforAPI.join("&") + strExtConstraints;
    return constraints;
  };

  const getFromAndToSetsForAPI = () => {
    const portPoints = portData.portPoints;
    const apiFormData = [];
    portPoints.map((item, index) => {
      const nextItem = portPoints[index + 1];

      if (!nextItem) {
        return;
      }

      apiFormData.push({
        from: item.port_name,
        fromPortId: item.PortID,
        to: nextItem.port_name, // to port name
        toPortId: nextItem.PortID, // to port id
        sort: index + 1,
      });
    });

    return apiFormData.sort((a, b) => a.sort - b.sort);
  };

  const getMapBoxFromAndToData = (portFrom, portTo, coordinates) => {
    return [
      {
        // feature for Mapbox DC
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: coordinates[0],
        },
        properties: {
          title: portFrom,
        },
      },
      {
        // feature for Mapbox SF
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: coordinates[coordinates.length - 1],
        },
        properties: {
          title: portTo,
        },
      },
    ];
  };

  const onSearchClick = async () => {
    const formDataArray = getFromAndToSetsForAPI();
    if (!formDataArray.length) {
      showError("Select Port points. Minimum 2 points requred to search");
      return;
    }
    setPortData((prev) => ({ ...prev, fetchInProgress: true }));

    const promiseArray = formDataArray.map(async (item) =>
      getCoordinateAPI(item)
    );

    const results = await Promise.all(promiseArray)
      .then((values) => values)
      .catch((error) => {
        showError(error.message || "Something wrong please try again");
      });

    const mapBoxPoints = [];
    const allCoordinates = [];
    let tableData = [];

    // Check any error found on these APIs
    const errors = [];
    results.map((item) => {
      if (!item.status) {
        errors.push(item.response);
      }
    });

    // Show error if found in APIs and return
    if (errors.length) {
      showError(errors);
      setPortData((prev) => ({
        ...prev,
        fetchInProgress: false,
        mapBoxPoints,
        allCoordinates,
        tableData,
      }));

      return;
    }

    // Get responses
    const responses = results.map(({ response }) => {
      return response;
    });

    responses.map((response, index) => {
      const mapPointFromAndTo = getMapBoxFromAndToData(
        response.from,
        response.to,
        response.coordinates
      );
      mapBoxPoints.push(...mapPointFromAndTo);
      allCoordinates.push(...response.coordinates);

      const tblItem = getTableDataFromAndToItem(
        response.distanceData,
        response.fromPortId,
        response.toPortId,
        index === 0
      );

      tableData.push(...tblItem);
    });

    const footerCol = getFooterColumn(tableData);
    tableData = tableData.map((i, index) => ({
      ...i,
      serialNumber: index + 1,
    }));
    portData.allCoordinates = allCoordinates;
    portData.mapBoxPoints = mapBoxPoints;
    portData.tableData = [...tableData, footerCol];
    portData.fetchInProgress = false;
    setPortData({
      ...portData,
      fetchInProgress: false,
      mapBoxPoints,
      allCoordinates,
      tableData: [...tableData, footerCol],
    });

    setPortData((prev) => ({
      ...prev,
      allCoordinates,
      mapBoxPoints,
      tableData: [...tableData, footerCol],
      fetchInProgress: false,
    }));

    setDistanceModal(false);
  };

  const getCoordinateAPI = async (formData) => {
    const _method = "post";
    const constraints = getContainerStringFromChecklist();
    const postData = {
      from: formData.from,
      to: formData.to,
      //constraints,
      localEca: formdata.localEca,
      seca: formdata.seca,
      canalOptions: formdata.canalOptions,
      piracyArea: formdata.piracyArea,
    };

    return await new Promise((resolve) => {
      postAPICall(
        `${URL_WITH_VERSION}/port/distance`,
        postData,
        _method,
        (data) => {
          if (data.data && data.data.features[0].properties.total_length > 0) {
            const respData = data.data.features[0].properties;
            let distData = {},
              latlngarr = [],
              arr = [];
            const responseData = {};
            if (respData) {
              distData["distance"] = Number(respData.total_length).toFixed(
                TO_FIXED_2_NUM
              );
              distData["eca"] = EMPTY_2_NUM;
              // distData['eca'] = Number(respData.seca_length).toFixed(TO_FIXED_2_NUM);
              distData["seca"] = Number(respData.seca_length).toFixed(
                TO_FIXED_2_NUM
              );
              distData["hra"] = Number(respData.hra_length).toFixed(
                TO_FIXED_2_NUM
              );
              distData["crossed"] = Array.isArray(respData.crossed)
                ? respData.crossed.join(", ")
                : "";
              distData["spd"] = Number(
                respData.eta.averageVesselSpeedOverGround
              ).toFixed(TO_FIXED_2_NUM);
              distData["eta"] = Number(respData.eta.totalDurationDays).toFixed(
                TO_FIXED_2_NUM
              );

              let lat_lanf_frm =
                respData &&
                respData.lnglat_frm &&
                respData.lnglat_frm.split("/");
              if (lat_lanf_frm && lat_lanf_frm.length >= 2) {
                latlngarr.push({
                  latitude: lat_lanf_frm[0],
                  longitude: lat_lanf_frm[1],
                  port: portData.port_from,
                });
                arr.push({
                  lat: Number(lat_lanf_frm[0]),
                  lng: Number(lat_lanf_frm[1]),
                });
              }
              let lat_lanf_to =
                respData && respData.lnglat_to && respData.lnglat_to.split("/");
              if (lat_lanf_to && lat_lanf_to.length >= 2) {
                latlngarr.push({
                  latitude: lat_lanf_to[0],
                  longitude: lat_lanf_to[1],
                  port: portData.port_to,
                });
                arr.push({
                  lat: Number(lat_lanf_to[0]),
                  lng: Number(lat_lanf_to[1]),
                });
              }

              responseData.distanceData = distData;
              responseData.poly_lat_lng = latlngarr;
              responseData.triangleCoords = arr;
            }
            const cordinatesRes = data.data.features[0].geometry.coordinates;
            const resultData = {
              ...responseData,
              coordinates: cordinatesRes,
              lng: cordinatesRes[0],
              lat: cordinatesRes[cordinatesRes.length - 1],
              fromPortId: formData.fromPortId,
              toPortId: formData.toPortId,
              from: formData.from,
              to: formData.to,
            };

            // Return response with status
            resolve({ status: true, response: resultData });
            return;
          } else {
            const dataInner = data.data;
            const dataMessage =
              dataInner["error"] == undefined
                ? data.message
                : dataInner["error"];
            const msgError =
              typeof dataMessage !== "string"
                ? dataMessage["reason"]
                : dataMessage;

            // Return error response with status
            resolve({ status: false, response: msgError });
          }
        }
      );
    });
  };

  const loadHeaders = (portName) => {
    let headers = {
      order_by: {
        port_name: "ASC",
      },
      where: {
        OR: {
          id: {
            eq: "'" + portName + "'",
          },
          PortID: {
            l: portName,
          },
          port_name: {
            l: portName,
          },
          latitude: {
            l: portName,
          },
          longitude: {
            l: portName,
          },
          country_code: {
            l: portName,
          },
        },
      },
    };
    return headers;
  };

  const updateTableDataWithFooter = (updateTableData = []) => {
    const updataData = updateTableData.filter((i) => !i.isFooterCol);

    const footerCol = getFooterColumn(updataData);
    setPortData({ ...portData, tableData: [...updataData, footerCol] });
  };

  // Speed calculation
  const handleSpeedChange = (value, record) => {
    const seaDays =
      record.distance > 0
        ? Number(record.distance / Number(value * 24))
        : EMPTY_2_NUM;
    const totalVDays = value
      ? Number(Number(record.pDays) + seaDays)
      : EMPTY_2_NUM;

    const co2 = getCo2ValFormula({
      seaDays,
      vlsfoPerDay: record.vlsfoPerDay,
      lsmgo: record.lsmgo,
      portCons: record.portCons,
    });

    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          spd: value,
          seaDays: value
            ? Number(seaDays).toFixed(TO_FIXED_2_NUM)
            : EMPTY_2_NUM,
          totalVDays: Number(totalVDays).toFixed(TO_FIXED_2_NUM),
          co2,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const handleVLSFOChange = (value, record, name) => {
    const seaCons =
      record.seaDays > 0
        ? Number(record.seaDays) * value +
        Number(record.seaDays) * Number(record.lsmgo)
        : 0.0;
    const ttlBunkercons = Number(
      Number(seaCons) + Number(record.ttlBunkercons)
    ).toFixed(TO_FIXED_2_NUM);

    const co2 = getCo2ValFormula({
      seaDays: record.seaDays,
      vlsfoPerDay: value || 0,
      lsmgo: record.lsmgo,
      portCons: record.portCons,
    });

    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          [name]: value,
          seaCons: value
            ? Number(seaCons).toFixed(TO_FIXED_2_NUM)
            : EMPTY_2_NUM,
          ttlBunkercons,
          co2,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const handleLsmgoChange = (value, record, name) => {
    const seaCons =
      record.seaDays > 0
        ? Number(record.seaDays) * Number(record.vlsfoPerDay) +
        Number(record.seaDays) * value
        : 0.0;
    const ttlBunkercons = Number(
      Number(seaCons) + Number(record.ttlBunkercons)
    ).toFixed(TO_FIXED_2_NUM);

    const co2 = getCo2ValFormula({
      seaDays: record.seaDays,
      vlsfoPerDay: record.vlsfoPerDay,
      lsmgo: value || 0,
      portCons: record.portCons,
    });

    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          [name]: value,
          seaCons: value
            ? Number(seaCons).toFixed(TO_FIXED_2_NUM)
            : EMPTY_2_NUM,
          ttlBunkercons,
          co2,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const handleTtlBunkerconsChange = (value, record, name) => {
    const ttlBunkercons =
      record.seaCons >= 0
        ? Number(record.seaCons) + Number(value || 0)
        : EMPTY_2_NUM;

    const co2 = getCo2ValFormula({
      seaDays: record.seaDays,
      vlsfoPerDay: record.vlsfoPerDay,
      lsmgo: value || 0,
      portCons: name === "portCons" && value ? value : 0,
    });

    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          [name]: value,
          ttlBunkercons: Number(ttlBunkercons).toFixed(TO_FIXED_2_NUM),
          co2,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const handlePDayChange = (value, record, name) => {
    // if(record.serialNumber == 1) {
    //   return;
    // }

    let pDays = 0;
    const cargo = name === "cargo" ? value : record.cargo;
    const ldRate = name === "ldRate" ? value : record.ldRate;
    const ldTerm = name === "ldTerm" ? value : record.ldTerm;
    const ldTermObj = ldTermsOptions.find((i) => i.value === ldTerm);
    const ldTermVal = ldTermObj && ldTermObj.value ? ldTermObj.numVal : 0;

    pDays = (Number(cargo) / Number(ldRate)) * Number(ldTermVal);
    if (!Number.isFinite(pDays) || Number.isNaN(pDays)) {
      pDays = 0;
    }

    const totalVDays = Number(pDays + Number(record.seaDays)).toFixed(
      TO_FIXED_2_NUM
    );

    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          pDays: Number(pDays).toFixed(TO_FIXED_2_NUM),
          cargo,
          ldRate,
          ldTerm,
          totalVDays,
          [name]: value,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const handlePortExpChange = (value, record, name) => {
    const updataData = portData.tableData.map((item) => {
      if (item.id === record.id) {
        return {
          ...item,
          [name]: value,
        };
      }

      return item;
    });

    updateTableDataWithFooter(updataData);
  };

  const editableInput = (text, record, name) => {
    if (record.isFooterCol) {
      return " ";
    }

    return (
      <InputNumber
        style={{ width: "100px" }}
        value={Number(text)}
        onChange={(val) => {
          if (name == "spd") {
            handleSpeedChange(val, record, name);
          } else if (name === "vlsfoPerDay") {
            handleVLSFOChange(val, record, name);
          } else if (name === "lsmgo") {
            handleLsmgoChange(val, record, name);
          } else if (name === "portCons") {
            handleTtlBunkerconsChange(val, record, name);
          } else if (name === "cargo" || name === "ldRate") {
            handlePDayChange(val, record, name);
          } else if (name === "portExp") {
            handlePortExpChange(val, record, name);
          }
        }}
        min={0}
      />
    );
  };

  const editableSelect = (text, record, name, options) => {
    if (record.isFooterCol) {
      return " ";
    }

    return (
      <Select
        showSearch
        style={{ width: "100px" }}
        value={text}
        placeholder=" "
        optionFilterProp="children"
        onChange={(val) => {
          if (name === "ldTerm") {
            handlePDayChange(val, record, name);
          }
        }}
      >
        {options.map((element, i) => {
          return (
            <Option key={element.label} value={element.value}>
              {element.label}
            </Option>
          );
        })}
      </Select>
    );
  };

  const columnConfiguration = (columns) => {
    return columns.map((item, index) => {
      const isFirstCol = index === 0;
      if (item.isEditable && item.type == "numberInput") {
        return {
          ...item,
          render: (text, record) =>
            record.isFooterCol
              ? text
              : editableInput(text, record, item.dataIndex),
        };
      }

      if (item.isEditable && item.type == "select" && !isFirstCol) {
        return {
          ...item,
          render: (text, record) =>
            record.isFooterCol
              ? text
              : editableSelect(text, record, item.dataIndex, item.options),
        };
      }

      return item;
    });
  };

  const handleRemovePortPoint = (record) => {
    const filterPortPoints = portData.portPoints.filter(
      (i) => i.id !== record.id
    );

    pointLayerData = pointLayerData.filter(
      (data) =>
        data?.geometry?.coordinates[0]?.toFixed(2).toString() !==
        record.longitude
    );

    setPortData({
      ...portData,
      portPoints: filterPortPoints,
      mapBoxPoints: pointLayerData,
    });
  };

  const getFooterColumn = (tableData = []) => {
    const initialValue = {};
    tableColumns.map((item) => {
      if (item.isCalculateSum) {
        initialValue[item.dataIndex] = 0;
      }
    });

    const footerVal = tableData.reduce((acc, currentVal) => {
      const allVals = {};
      Object.keys(initialValue).map((key) => {
        allVals[key] = Number(acc[key]) + Number(currentVal[key]);
      });
      return allVals;
    }, initialValue);

    Object.keys(footerVal).map((key) => {
      footerVal[key] = Number(footerVal[key]).toFixed(TO_FIXED_2_NUM);
      if (Number.isNaN(footerVal[key])) {
        delete footerVal[key];
      }
    });

    footerVal.id = tableData.length;
    footerVal.isFooterCol = true; // set as footer flag

    return footerVal;
  };

  const {
    shipData,
    mapSettings,
    visibleModal,
    triangleCoords,
    poly_lat_lng,
    distanceData,
    etaCalculated,
    spd,
    portPoints,
    checkedList,
    tableData,
  } = portData;
  var lineSymbol = {
    path: "M 0,-1 0,1",
    strokeOpacity: 1,
    scale: 4,
  };

  //const filteredRouteOptions = routeOptions.filter(i => !checkedList.includes(i));
  // const portDataListOnly = tableData.filter((i) => !i.isFooterCol);

  // const filteredPiracyOptions = piracyoptions.filter(
  //   (i) => !checkedList.includes(i)
  // );

  const onCloseFilter = () => {
    setPortData(
      (prev) => ({
        ...prev,
        filterType: "",
        liveSearchValue: "",
        selectedData: null,
        liveSearchResult: searchLabel,
        liveSearchList: [],
        isShowLocationFromtable: false,
        isGlobalShipOpen: false,
      }),
      () => {
        if (typeof props.modalCloseEvent == "function") {
          props.modalCloseEvent();
        }
      }
    );
  };

  const renderDrawButtons = () => {
    return (
      <>
        {showDrawButtons && (
          <div className="draw-container">
            <span className="draw-mode-text">Draw Mode</span>
            <div className="draw-btn-container">
              <button
                className="btns-draw"
                onClick={() => {
                  if (drawRef.current) {
                    drawRef.current.changeMode("draw_polygon");
                  }
                }}
              >
                <i className="fas fa-plus" />
              </button>
              <button
                className="btns-draw"
                onClick={() => {
                  if (drawRef.current) {
                    drawRef.current.trash();
                  }
                }}
              >
                <i className="fas fa-trash" />
              </button>
              <button
                className="btns-draw"
                onClick={() => {
                  setShowDrawButtons(false);
                  if (drawRef.current) {
                    drawRef.current.changeMode("static");
                  }
                }}
              >
                <i className="far fa-times" />
              </button>
            </div>
          </div>
        )}
        <button
          className="btn-draw-polygon"
          onClick={() => {
            setShowDrawButtons(!showDrawButtons);
            if (drawRef.current) {
              drawRef.current.changeMode("simple_select");
            }
          }}
        >
          <svg
            height="100%"
            viewBox="0 0 512 512"
            width="100%"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="m234.558594 114.238281-136 296.960938-29.117188-13.4375 136-296.960938zm0 0"
              fill="#073352"
            />
            <path
              d="m427.039062 242.078125-23.359374 21.761719-142.71875-153.917969 23.359374-21.761719zm0 0"
              fill="#073352"
            />
            <path
              d="m409.921875 321.121094-295.523437 123.199218-12.320313-29.441406 295.523437-123.199218zm0 0"
              fill="#073352"
            />
            <path
              d="m205.441406 100.800781 29.117188 13.4375-19.359375 42.402344c-10.558594-2.71875-20.480469-7.199219-29.277344-13.28125zm0 0"
              fill="#073352"
            />
            <path
              d="m316.160156 122.398438c-6.558594 8.480468-14.558594 16-23.519531 21.761718l-31.679687-34.238281 23.359374-21.761719zm0 0"
              fill="#073352"
            />
            <path
              d="m304 64c0 35.347656-28.652344 64-64 64s-64-28.652344-64-64 28.652344-64 64-64 64 28.652344 64 64zm0 0"
              fill="#073352"
            />
            <path
              d="m118.078125 368.640625-19.519531 42.558594-29.117188-13.4375 19.359375-42.402344c10.558594 2.71875 20.480469 7.199219 29.277344 13.28125zm0 0"
              fill="#073352"
            />
            <path
              d="m157.441406 426.398438-43.042968 17.921874-12.320313-29.441406 43.042969-17.917968c5.757812 8.960937 10.078125 18.878906 12.320312 29.4375zm0 0"
              fill="#073352"
            />
            <path
              d="m128 448c0 35.347656-28.652344 64-64 64s-64-28.652344-64-64 28.652344-64 64-64 64 28.652344 64 64zm0 0"
              fill="#073352"
            />
            <path
              d="m397.601562 291.679688 12.320313 29.441406-43.042969 17.917968c-5.757812-8.960937-10.078125-18.878906-12.320312-29.4375zm0 0"
              fill="#073352"
            />
            <path
              d="m403.679688 263.839844-31.839844-34.238282c6.558594-8.480468 14.558594-16 23.519531-21.761718l31.679687 34.238281zm0 0"
              fill="#073352"
            />
            <path
              d="m512 288c0 35.347656-28.652344 64-64 64s-64-28.652344-64-64 28.652344-64 64-64 64 28.652344 64 64zm0 0"
              fill="#073352"
            />
            <g fill="#fff">
              <path d="m224 48h32v32h-32zm0 0" />
              <path d="m432 272h32v32h-32zm0 0" />
              <path d="m48 432h32v32h-32zm0 0" />
            </g>
          </svg>
        </button>
      </>
    );
  };

  const portDistancePopUp = (
    <div>
      <div>
        <label style={{ fontWeight: "bold" }}>
          Search Place
          <span style={{ color: "#ff0000" }}>
            <b />
          </span>
          :
        </label>
      </div>
      <Row
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <Col span={18}>
          <Select
            showSearch
            style={{
              width: "100%",
            }}
            value={portData.port_from}
            placeholder="Search to Select"
            optionFilterProp="children"
            onChange={onChangeAddDistances}
            onSearch={onSearchPortList}
            filterOption={(input, option) =>
              option.props.children.toLowerCase().includes(input.toLowerCase())
            }
            loading={portData.loadPortInProgress}
          >
            {portData.selectOptions &&
              portData.selectOptions.map((element, i) => {
                return (
                  <Option key={i} value={element.value}>
                    {element.label}
                  </Option>
                );
              })}
          </Select>
        </Col>
        <Col span={6}>
          <Button
            style={{
              float: "right",
              // marginTop: "15px",
              // marginBottom: "15px",
              display: "flex",
              alignItems: "center",
            }}
            type="primary"
            icon={<LockOutlined />}
            onClick={(e) => {
              e.stopPropagation();
              onSearchClick(e);
            }}
            loading={portData.fetchInProgress}
          >
            Search
          </Button>
        </Col>
      </Row>
      <Row style={{ margin: "12px 0" }}>
        {portPoints?.length > 0 && (
          <Col
            span={24}
            style={{
              display: "flex",
              justifyContent: "space-around",
              width: "100%",
            }}
          >
            {portPoints.map((item) => (
              <div style={{ display: "flex", gap: "12px" }}>
                <span>{item.port_name}</span>
                {portPoints.length > 1 &&
                  portPoints.length < portPoints.length && (
                    <>
                      <span
                        style={{
                          height: "5px",
                          width: "20px",
                          background: "blue",
                        }}
                      ></span>
                    </>
                  )}
                <span
                  style={{
                    cursor: "pointer",
                    userSelect: "none",
                    fontWeight: "bold",
                  }}
                  onClick={() => handleRemovePortPoint(item)}
                >
                  x
                </span>
              </div>
            ))}
          </Col>
        )}
      </Row>
      <Row>
        <Col>
          <div style={{ fontWeight: "bold", marginBottom: "4px" }}>
            Canal Options :
          </div>
          <Col>
            <Radio.Group
              // mode="multiple"
              style={{ display: "flex", flexDirection: "column", gap: "6px" }}
              allowClear
              // style={{ width: "100%" }}
              placeholder="Please select Canal Options"
              value={formdata.canalOptions}
              onChange={canalOnChange}
            >
              {canalPassoptions.map((el) => {
                return (
                  <Radio key={el.key} value={el.value}>
                    {el.label}
                  </Radio>
                );
              })}
            </Radio.Group>
          </Col>
        </Col>

        <Col>
          <div style={{ fontWeight: "bold", marginBottom: "4px" }}>
            Piracy Area :
          </div>

          <Col>
            <Radio.Group
              // mode="multiple"
              style={{ display: "flex", flexDirection: "column", gap: "6px" }}
              allowClear
              // style={{ width: "100%" }}
              placeholder="Please select piracy area"
              value={formdata.piracyArea}
              onChange={piracyOnChange}
            >
              {piracyoptions.map((el) => {
                return (
                  <Radio key={el.key} value={el.value}>
                    {el.label}
                  </Radio>
                );
              })}
            </Radio.Group>
          </Col>
        </Col>

        <Col>
          <div style={{ fontWeight: "bold", marginBottom: "4px" }}>SECA:</div>
          <Col>
            <Radio.Group
              style={{ display: "flex", flexDirection: "column", gap: "6px" }}
              name="secaGroup"
              onChange={onChangeEcaRadio}
              value={formdata.seca}
            >
              <Radio value={1}>None</Radio>
              <Radio value={2}>Normal</Radio>
              <Radio value={3}>Shortest</Radio>
              <Radio value={4}>Optimized</Radio>
            </Radio.Group>
          </Col>
        </Col>

        <Col>
          <div style={{ fontWeight: "bold", marginBottom: "4px" }}>
            Local ECA:
          </div>
          <Col>
            <Radio.Group
              style={{ display: "flex", flexDirection: "column", gap: "6px" }}
              name="localecaGroup"
              onChange={onChangelocalEcaRadio}
              value={formdata.localEca}
            >
              <Radio value={1}>True</Radio>
              <Radio value={0}>False</Radio>
            </Radio.Group>
          </Col>
        </Col>
      </Row>

      <Divider />
    </div>
  );

  let globalSearch = (
    <FilterVessel
      onSearchLiveData={searchIconClick}
      onSearchDbData={onSearchDbData}
      // onFilterData={filterIconClick}
      hideIcons={true}
      right={"-35px"}
    >
      {/* Single vessel view   */}


      <SelectedVesselView
        vesselDetails={portData.selectedData}
        onClose={onCloseFilter}
        isLiveDetailLoading={portData.isLiveDetailLoading}
        // addmyfleet={(data) => addmyfleet(data)}
      />


      {portData.isGlobalShipOpen && (
        <LiveVesselSearch
          onCloseFilter={onCloseFilter}
          listData={portData.liveSearchList}
          onChangeLiveSearchInput={handleLiveSearchInput}
          onLiveSearchDataClick={handleGetLiveVesselItemData}
          isLoading={portData.isLiveSearchLoading}
          liveSearchResult={portData.liveSearchResult}
        />
      )}

      {portData.isVesselListShow && (
        <VesselLargeListFilter
          onGetSelectedData={getSelectedData}
          onCloseFilter={onCloseFilter}
          handleSearchValue={handleSearchValue}
          listData={portData.filterData}
          closeVesselList={() =>
            setPortData((prev) => ({ ...prev, isVesselListShow: false }))
          }
        />
      )}
    </FilterVessel>
  );




  return (
    <>
      <ToolBar
        map={map}
        openGlobalSearch={() =>
          setPortData((prev) => ({
            ...prev,
            isGlobalShipOpen: true,
            isVesselListShow: false,
          }))
        }
        openMarketOrder={() =>
          setPortData((prev) => ({
            ...prev,
            opencargoenquiryModal: true,
          }))
        }

        openTonnageOrder={() =>
          setPortData((prev) => ({
            ...prev,
            openTonnageEnquiryModal: true,
          }))
        }
        moveToTable={props.moveToTable}
        isPortDistanceVisible={isDistancCalModal}
        setIsPortDistModal={setDistanceModal}
        onSearchDbData={onSearchDbData}
        switchTable={props.switchTable}
      />
      <div className="wrap-rightbar full-wraps port-to-port-page">
        {globalSearch}
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <section className="map-wrapper-container">
                <div className="fieldscroll-wrap">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body ant-form">
                        <Row className="ant-form-item-label">
                          <Col
                            span={24}
                            style={{ minHeight: "500px" }}
                            className="map-padding"
                          >
                            {/* <MapRender port_from={portDatafinalPortFrom} port_to={portDatafinalPortTo} /> */}
                            <div className="sidebar">
                              Longitude: {portData.lng} | Latitude:
                              {portData.lat} | Zoom: {portData.zoom}
                            </div>
                            {renderDrawButtons()}
                            <button
                              onClick={onClearHandler}
                              className="btn-clear"
                              style={{ marginTop: "1rem" }}
                            >
                              Clear
                            </button>
                            <button
                              onClick={onMapHandler}
                              className="btn-clear"
                              style={{ marginTop: "5rem" }}
                            >
                              Map
                            </button>
                            <div
                              ref={MapRef}
                              className="map-container"
                              style={{ width: "100%", height: "100vh" }}
                            />
                          </Col>
                          {isDistancCalModal && (
                            <Modal
                              title=""
                              open={true}
                              onCancel={() => setDistanceModal(false)}
                              width={800}
                              footer={null}
                            >
                              {portDistancePopUp}
                            </Modal>
                          )}
                        </Row>

                        <Row>
                          <Col span={24}>
                            <div style={{ width: "100%" }}>
                              <Table
                                columns={columnConfiguration(tableColumns)}
                                dataSource={tableData}
                                rowKey="id"
                                pagination={false}
                                align={"left"}
                              />
                            </div>
                          </Col>
                        </Row>
                        {visibleModal ? (
                          <Modal
                            style={{ top: "2%" }}
                            title="Port Route Details"
                            open={visibleModal}
                            onCancel={() => showHideModal(false, null)}
                            width="100%"
                            footer={null}
                          >
                            <ShipDetails
                              data={shipData}
                              lat={portData.port_lat}
                              lng={portData.port_lng}
                            />
                          </Modal>
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </section>
            </Content>
          </Layout>
        </Layout>

        {portData.opencargoenquiryModal ? (
          <Modal
            style={{ top: "2%" }}
            title="Market Order"
            open={portData.opencargoenquiryModal}
            onCancel={() =>
              setPortData((prev) => ({
                ...prev,
                opencargoenquiryModal: false,
              }))
            }
            width="45%"
            footer={null}
          >
            <GenerateCargoEnquiry />
          </Modal>
        ) : undefined}

        {portData.openTonnageEnquiryModal ? (
          <Modal
            style={{ top: "2%" }}
            title="Tonnage Order"
            open={portData.openTonnageEnquiryModal}
            onCancel={() =>
              setPortData((prevState) => ({
                ...prevState,
                openTonnageEnquiryModal: false,
              }))
            }
            width="45%"
            footer={null}
          >
            <GenerateTonnageEnquiry />
          </Modal>
        ) : undefined}






      </div>
    </>
  );
};

export default PortMap;
