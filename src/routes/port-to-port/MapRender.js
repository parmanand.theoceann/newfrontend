/* eslint-disable no-unused-expressions */
import React, { Component, setState } from 'react'
import mapboxgl, { MapWheelEvent } from "mapbox-gl";
import { data } from "./data";
import axios from "axios";
import { get, isEmpty } from "lodash";
import "mapbox-gl/dist/mapbox-gl.css";



mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default; // eslint-disable-line
mapboxgl.accessToken = "pk.eyJ1IjoiYXZpMDMwNiIsImEiOiJjbDFhZHAya3AwaHFwM21tbHUxdDF6OGEyIn0.FGJi729e2aC6cbZPWLxngw";

class MapRender extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coordinates: [],
      port_from: '',
      port_to: '',
    };

  }
  componentDidMount = (props) => {
    this.maprender();
    // console.log(this.props.finalPortFrom);
    this.loadAPI(this.props.finalPortFrom, this.props.finalPortTo);
  }

  maprender() {
    const geojson = {
      type: "FeatureCollection",
      features: [{
        type: "Feature",
        geometry: {
          type: "LineString",
          properties: {},
          coordinates: this.state.coordinates ? this.state.coordinates : [],
        },
      },],
    };
    const map = new mapboxgl.Map({
      container: "map",
      // style: 'mapbox://styles/mapbox/streets-v11',
      style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
      center: this.state.coordinates ? this.state.coordinates[0] : [],
      zoom: 1.8,
    });

    map.on("load", () => {
      // add mapbox terrain dem source for 3d terrain rendering
      map.addSource("LineString", {
        type: "geojson",
        data: geojson,
      });

      map.addLayer({
        id: "LineString",
        type: "line",
        source: "LineString",
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": "#2414E2",
          "line-width": 4,
          "line-dasharray": [0, 1.5],
        },
      });

      map.loadImage(
        "https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png",
        (error, image) => {
          if (error) throw error;
          map.addImage("custom-marker", image);
          // Add a GeoJSON source with 2 points
          map.addSource("points", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: this.state.coordinates ?
                [{
                  // feature for Mapbox DC
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: this.state.coordinates[0],
                  },
                  properties: {
                    title: this.state.port_from,
                  },
                },
                {
                  // feature for Mapbox SF
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: this.state.coordinates[this.state.coordinates.length - 1],
                  },
                  properties: {
                    title: this.state.port_to,
                  },
                },
                ] : [],

            },
          });

          // Add a symbol layer
          map.addLayer({
            id: "points",
            type: "symbol",
            source: "points",
            layout: {
              "icon-image": "custom-marker",
              // get the title name from the source's "title" property
              "text-field": ["get", "title"],
              "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
              "text-offset": [0, 1.25],
              "text-anchor": "top",
            },
          });
        }
      );
    });
  }

  loadAPI = (from, to) => {
    let headers = {
      "Content-Type": "application/json",
      Authorization: "",
    };
    // console.log(this.state.port_from)
    axios
      .post(`${URL_WITH_VERSION}/port/distance`, {
        from: from,
        to: to
      }, headers)
      .then((res) => {
        let cordinatesRes = res.data.data.features[0].geometry.coordinates;
        this.setState({ port_from: from, port_to: to, coordinates: cordinatesRes })
        this.maprender();
        // console.log(this.state)
      })
      .catch((err) => {
        console.log(err)
      });
  }

  render() {

    return (
      <>
        <div id="map"
          style={{ width: "100%", height: "70vh", }}
        />
      </>
    );
  }
}


export default MapRender;