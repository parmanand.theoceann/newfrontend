import React, { useEffect, useState } from "react";
import { Menu, Dropdown, Checkbox, Button } from "antd";
import mapboxgl from "mapbox-gl";
import URL_WITH_VERSION, { getAPICall, useStateCallback,IMAGE_PATH} from "../../shared";
import "../../styles/toolbar.css";
import '../../styles/mapstyle.css';
import { eca_polygons, high_risk_area } from "./portUtil";
import MenuItem from "antd/es/menu/MenuItem";

const ToolBar = ({
  switchTable,
  isPortDistModal,
  setIsPortDistModal,
  map,
  openGlobalSearch,
  openMarketOrder,
  openTonnageOrder,
  
}) => {
  const [isEcaVisible, setIsEcaVisible] = useState(false);
  const [isHraVisible, setIsHraVisible] = useState(false);
  const [isPortNameVisible, setPortNameVisible] = useState(false);
  const [ports, setports] = useState([]);
 
  useEffect(() => {
    getportdata();
  }, []);

  const getportdata = async () => {
    try {
      const data = await getAPICall(
        `${URL_WITH_VERSION}/port-bunker-activity/spotprice`
      );
      const respdata = await data;

      let allPorts = {};
      for (let item of respdata.BunkerExSpotPrices) {
        allPorts[item.Port] = {
          portname: item.Port,
          fuelType: "",
          fuelprice: "",
          portCoordinates: [item.Longitude, item.Latitude],
        };
      }

      for (let item of respdata.BunkerExSpotPrices) {
        if (allPorts[item.Port]) {
          allPorts[item.Port].fuelType += item.Grade + " ";
          allPorts[item.Port].fuelprice += item.Price + " ";
        }
      }

      let PortNameAndCoordinateArr = [];
      for (let key in allPorts) {
        PortNameAndCoordinateArr.push(allPorts[key]);
      }

      setports(PortNameAndCoordinateArr);
    } catch (err) {
      console.log("something went wrong", err);
    }
  };

  const showEcaArea = (coordinates) => {
    const ECASource = map.current.getSource("ECA");
    if (!ECASource) {
      map.current.addSource("ECA", {
        type: "geojson",
        data: {
          type: "Feature",
          geometry: {
            type: "MultiPolygon",
            coordinates: coordinates,
          },
        },
      });
    }

    const ECAFillLayer = map.current.getLayer("ECA_FILL");
    const ECAOutlineLayer = map.current.getLayer("ECA_OUTLINE");
    const ECALabelLayer = map.current.getLayer("ECA_LABEL");
    if (!ECAFillLayer) {
      // Add a new layer to visualize the polygon.
      map.current.addLayer({
        id: "ECA_FILL",
        type: "fill",
        source: "ECA", // reference the data source
        layout: {},
        paint: {
          "fill-color": "red", // red color fill
          "fill-opacity": 0.4,
        },
      });
    }

    if (!ECAOutlineLayer) {
      // Add a black outline around the polygon.
      map.current.addLayer({
        id: "ECA_OUTLINE",
        type: "line",
        source: "ECA",
        layout: {},
        paint: {
          "line-color": "red",
          "line-width": 1,
        },
      });
    }

    if (!ECALabelLayer) {
      map.current.addLayer({
        id: "ECA_LABEL",
        type: "symbol",
        source: "ECA", // reference the label source
        layout: {
          "text-field": "ECA", // Text to display
          "text-size": 8, // Size of the text
          // You can add more layout properties as needed
        },
        paint: {
          // Any paint properties for styling the text
        },
      });
    }

    setIsEcaVisible(true);
  };
  const hideEcaArea = () => {
    if(!map.current.getSource('ECA')) return 
    map.current.removeSource("ECA");
    map.current.removeLayer("ECA_FILL");
    map.current.removeLayer("ECA_OUTLINE");
    map.current.removeLayer("ECA_LABEL");
    setIsEcaVisible(false);
  };

  const showHighRiskArea = (coordinates) => {
    const hraSource = map.current.getSource("HRA");

    if (!hraSource) {
      map.current.addSource("HRA", {
        type: "geojson",
        data: {
          type: "Feature",
          geometry: {
            type: "MultiPolygon",
            // These coordinates outline Maine.
            coordinates: coordinates,
          },
        },
      });
    }

    const hraFillLayer = map.current.getLayer("HRA_FILL");
    const hraOutlineLayer = map.current.getLayer("HRA_OUTLINE");
    const hraLabelLayer = map.current.getLayer("HRA_LABEL");
    if (!hraFillLayer) {
      // Add a new layer to visualize the polygon.
      map.current.addLayer({
        id: "HRA_FILL",
        type: "fill",
        source: "HRA", // reference the data source
        layout: {},
        paint: {
          "fill-color": "green", // red color fill
          "fill-opacity": 0.4,
        },
      });
    }

    if (!hraOutlineLayer) {
      // Add a black outline around the polygon.
      map.current.addLayer({
        id: "HRA_OUTLINE",
        type: "line",
        source: "HRA",
        layout: {},
        paint: {
          "line-color": "green",
          "line-width": 1,
        },
      });
    }

    if (!hraLabelLayer) {
      map.current.addLayer({
        id: "HRA_LABEL",
        type: "symbol",
        source: "HRA", // reference the label source
        layout: {
          "text-field": "HRA", // Text to display
          "text-size": 8, // Size of the text
          // You can add more layout properties as needed
        },
        paint: {
          // Any paint properties for styling the text
        },
      });
    }

    setIsHraVisible(true);
  };

  const hideHighRiskArea = () => {
    if(!map.current.getSource('HRA')) return 
    map.current.removeSource("HRA");
    map.current.removeLayer("HRA_FILL");
    map.current.removeLayer("HRA_OUTLINE");
    map.current.removeLayer("HRA_LABEL");
    setIsHraVisible(false);
  };
  const showPortName = (PortData) => {
    const portsSource = map.current.getSource("ports");

    if (!portsSource) {
      map.current.loadImage(IMAGE_PATH+"icons/ports.png", (error, image) => {
        if (error) {
          return;
        }
        map.current.addImage("marker-15", image);
      });
      map.current.addSource("ports", {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: PortData.map((point, index) => {
            return {
              type: "Feature",
              geometry: {
                type: "Point",
                coordinates: point.portCoordinate,
              },
              properties: {
                id: index, // You can assign a unique ID to each point
              },
            };
          }),
        },
      });
    }

    map.current.addLayer({
      id: "ports",
      type: "symbol",
      source: "ports",
      layout: {
        "icon-image": "marker-15", // You can customize the marker icon
        "text-field": ["get", "name"],
        "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
        "text-offset": [0, 0.6],
        "text-anchor": "top",
        "text-size": 8,
      },
    });

    setPortNameVisible(true);
  };

  const hidePortName = () => {
    map.current.removeSource("ports");
    map.current.removeLayer("ports");
    setPortNameVisible(false);
  };
  const showBunkerPrice = (PortData) => {
    const bunkerPriceSource = map.current.getSource("bunkerPrice");

    if (!bunkerPriceSource) {
      map.current.addSource("bunkerPrice", {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: PortData.map((point, index) => {
            let fuelType = point.fuelType.trimEnd().split(' ').map((type) => `<span class='fuel'>${type}</span>`).join('')
            // let fuelTypeJsx = `<div>${FuelType}</span>`
            let fuelPrice = point.fuelprice.trimEnd().split(' ').map(price => `<span class='price'>${price}</span>`).join('')
            // let fuelPriceJsx = `<span>${fuelPrice}</span>`
            // console.log(fuelType);
            return {
             

              type: "Feature",
              geometry: {
                type: "Point",
                coordinates: point.portCoordinates,
              },
              properties: {
                id: index,
                description: `<div>
                <p> PortName : ${point.portname}</p>
                
                <div class='fuel-type'> Fuel : ${fuelType}</div>
                <div class="fuel-price"> Price : ${fuelPrice}</div>
              </div>`,
              },
            };
          }),
        },
      });
    }

    map.current.addLayer({
      id: "bunkerPrice",
      type: "symbol",
      source: "bunkerPrice",
      layout: {
        "icon-image": "orangeIcon", // You can customize the marker icon
        "text-field": ["get", "name"],
        "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
        "text-offset": [0, 0.6],
        "text-anchor": "top",
        "text-size": 8,
      },
    });

    const popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });
    map.current.on("mouseenter", "bunkerPrice", (e) => {
      map.current.getCanvas().style.cursor = "pointer";
      const coordinates = e.features[0].geometry.coordinates.slice();
      const description = e.features[0].properties.description;

      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }
      popup.setLngLat(coordinates).setHTML(description).addTo(map.current);
    });

    map.current.on("mouseleave", "bunkerPrice", () => {
      map.current.getCanvas().style.cursor = "";
      popup.remove();
    });

    setPortNameVisible(true);
  };

  const hideBunkerPrice = () => {
    if(!map.current.getSource('bunkerPrice')) return 
    map.current.removeSource("bunkerPrice");
    map.current.removeLayer("bunkerPrice");
    setPortNameVisible(false);
  };

  const moveToTable = () => {
    //setIsTableVisible(true)
    window.scrollBy(0, 750);
  };

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <ul className="toolbar">
        <Dropdown
            overlay={
              <Menu>
                <MenuItem>
                <Button
                onClick={(e)=>{
                  e.stopPropagation();
                  openMarketOrder();
                  }}
                >Market Order</Button>
                </MenuItem>
                <MenuItem>
                 <Button
                 onClick={(e)=>{
                  e.stopPropagation();
                  openTonnageOrder();
                  }}
                 >Tonnage Order</Button>
                </MenuItem>
              </Menu>
            }
        ><li>Market/Tonnage Order</li></Dropdown>
        

        <Dropdown
          overlay={
            <Menu>
              <Menu.Item>
                {/* <button onClick={() => openGlobalSearch()}></button> */}
                <Button
                  onClick={(e) => {
                    e.stopPropagation();
                    openGlobalSearch();
                  }}
                >
                  Global Search
                </Button>
              </Menu.Item>
              <Menu.Item>
                <Button onClick={() => {
                  switchTable('global')
                  moveToTable()
                }}>My Fleet For Tracking</Button>
              </Menu.Item>
            </Menu>
          }
        >
          <li>Global Ships Search</li>
        </Dropdown>
        <li onClick={() => {
          switchTable('postion') 
          moveToTable()
        }}>Port Position</li>
        <li onClick={() => setIsPortDistModal(!isPortDistModal)}>
          Port Distance
        </li>
        <li>Voyage Estimate</li>
        {/* <li>Map tools</li> */}
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item>
                <Checkbox
                  onChange={(e) => {
                    e.stopPropagation();
                    if (map?.current) {
                      if (isEcaVisible) {
                        hideEcaArea();
                      } else {
                        showEcaArea(eca_polygons);
                      }
                    }
                  }}
                >
                  ECA
                </Checkbox>
              </Menu.Item>
              <Menu.Item>
                <Checkbox
                  onChange={(e) => {
                    e.stopPropagation();
                    if (map?.current) {
                      if (isHraVisible) {
                        hideHighRiskArea();
                      } else {
                        showHighRiskArea(high_risk_area);
                      }
                    }
                  }}
                >
                  HRA
                </Checkbox>
              </Menu.Item>
              {/* <Menu.Item>
                <Checkbox
                  onChange={(e) => {
                    e.stopPropagation();
                    if (map?.current) {
                      if (isPortNameVisible) {
                        hidePortName();
                      } else {
                        showPortName(ports);
                      }
                    }
                  }}
                >
                  Port Name
                </Checkbox>
              </Menu.Item> */}
            </Menu>
          }
        >
          <li className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
            Map Tools
          </li>
        </Dropdown>
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item>
                <Checkbox
                  onChange={(e) => {
                    e.stopPropagation();
                    if (map?.current) {
                      if (isPortNameVisible) {
                        hideBunkerPrice();
                      } else {
                        showBunkerPrice(ports);
                      }
                    }
                  }}
                >
                  Bunker Price
                </Checkbox>
              </Menu.Item>
            </Menu>
          }
        >
          <li className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
            Bunker Price
          </li>
        </Dropdown>
      </ul>
    </div>
  );
};

export default ToolBar;
