import React, { Component } from 'react'
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import "./Style.css";
import URL_WITH_VERSION, {IMAGE_PATH} from "../../shared";

const REACT_APP_MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default; // eslint-disable-line
mapboxgl.accessToken = REACT_APP_MAPBOX_TOKEN;

export default class SpotPriceMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lng: 41.15653,
            lat: 41.32111,
            zoom: 2
        };
        this.mapContainer = React.createRef();
    }
    componentDidMount() {
        this.maprender()
    }
    maprender() {
        // console.log("shipData", this.props.data);
        let features = []
        if (this.props && this.props.data != undefined) {
            this.props.data.length > 0 && this.props.data.map((obj) => {
                let d = obj && obj.length > 0 ? obj[0] : null
                if (d != null) {
                    features.push({
                        type: "Feature",
                        geometry: {
                            type: "Point",
                            coordinates: [d.longitude, d.latitude],
                        },
                        properties: {
                            title: d.port,
                        },
                    })
                }
            })
        }
        let that = this
        const geojson = {
            type: "FeatureCollection",
            features: [{
                type: "Feature",
                geometry: {
                    type: "LineString",
                    properties: {},
                    coordinates: [],
                },
            },],
        };
        const map = new mapboxgl.Map({
            container: this.mapContainer.current,
            style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom,
        });
        map.on("load", () => {
            map.addSource("LineString", {
                type: "geojson",
                data: geojson,
            });
            map.on('click', 'places', (e) => {
                // console.log("e....",e)
            });
            map.loadImage(
                // "https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png",
                IMAGE_PATH+"icons/harbor.png",
                (error, image) => {
                    if (error) throw error;
                    map.addImage("custom-marker", image);
                    map.addSource('places', {
                        'type': 'geojson',
                        'data': {
                            'type': 'FeatureCollection',
                            features: features
                        }
                    });

                    // Add a symbol layer
                    map.addLayer({
                        'id': 'places',
                        'type': 'symbol',
                        'source': 'places',
                        layout: {
                            "icon-image": "custom-marker",
                            // get the title name from the source's "title" property
                            "text-field": ["get", "title"],
                            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
                            "text-offset": [0, 1.25],
                            "text-anchor": "top",
                        },
                    });
                }
            );
        });
    }
    render() {
        const { lng, lat, zoom } = this.state;
        return (
            <div>

                <div ref={this.mapContainer} className="map-container" />
            </div>
        );
    }
}
