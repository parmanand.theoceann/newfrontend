import React, { useEffect } from "react";
import URL_WITH_VERSION, {
  apiDeleteCall,
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  useStateCallback,
} from "../../shared";
import { Popconfirm, Table } from "antd";
import { DeleteOutlined, EnvironmentOutlined } from "@ant-design/icons";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";

export default function TrackMyFleetTable(props) {
  const tableAction = {
    title: "Action",
    key: "action",
    fixed: "right",
    width: 50,

    render: (el, record) => {
      return (
        <div className="editable-row-operations">
          <span className="iconWrapper cancel">
            <Popconfirm
              title="Are you sure, you want to delete it?"
              onConfirm={() => onRowDeletedClick(record.id)}
            >
              <DeleteOutlined />
            </Popconfirm>
          </span>
        </div>
      );
    },
  };
  let tableHeaders = [
    {
      title: "Vessel Name",
      dataIndex: "vessel_name",
      key: "vessel_name",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Live Vessel Name",
      dataIndex: "new_name",
      key: "new_name",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "IMO No",
      dataIndex: "imo_no",
      key: "imo_no",
      ellipsis: "true",
      width: "100px",
    },
    {
      title: "Vessel Last Port.",
      dataIndex: "last_port",
      key: "last_port",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Current Port Name",
      dataIndex: "current_port_name",
      key: "current_port_name",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Speed",
      dataIndex: "speed",
      key: "speed",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Last pos. Latitude",
      dataIndex: "last_pos_lat",
      key: "last_pos_lat",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Last Pos Longitude",
      dataIndex: "last_pos_lon",
      key: "last_pos_lon",
      ellipsis: "true",
      width: "200px",
    },
    {
      title: "Degree",
      dataIndex: "degree",
      key: "degree",
      ellipsis: "true",
      width: "100px",
    },
    {
      title: "Live On Map",
      width: "100px",
      fixed: "right",
      align: "center",
      render: (data) => {
        let icon = (
          <EnvironmentOutlined
            onClick={() => trackVessel(data)}
            style={{
              fontSize: "25px",
              color: "red",
              alignSelf: "center",
              height: "19px",
            }}
          />
        );
        return data.last_pos_lat &&
          data.last_pos_lon &&
          data.last_pos_lat != "None" &&
          data.last_pos_lon != "None"
          ? icon
          : null;
      },
    },
  ];
  tableHeaders.push(tableAction);
  const [trackMyfleetState, setTrackMyfleetState] = useStateCallback({
    data: [],
    mapDataArr: [],
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    columns: tableHeaders,
    mapData: null,
    loading: false,
    filterVessels: [],
    isAdd: true,
    isVisible: false,
    formDataValues: {},
    localvesselList: [],
    isVisibleDetails: false,
    formDetailsValues: {},
    visibleLiveVessel: false,
    isLiveDetailLoading: false,
    typesearch: {},
    donloadArray: [],
    isShowLocationFromtable: false,
    showvesseldetailfromtable: {},
  });

  useEffect(() => {
    getTableData();
  }, []);

  const trackVessel = async (dataset) => {
  
    setTrackMyfleetState((prev) => ({
      ...prev,
      showvesseldetailfromtable: dataset,
      isShowLocationFromtable: true,
    }),()=>{
      props.showlocation(true);
      props.searchedVessel(dataset);
    });
  };

  const getlocalvessellist = () => {
    // from here we will get the all vessel list created by us manually.
    setTrackMyfleetState((prev) => ({
      ...prev,
      loading: true,
    }));
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    try {
      fetch(
        `http:${process.env.REACT_APP_URL}v1/` + "vessel/live/list?p=" + 0,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => {
          setTrackMyfleetState((prev) => ({
            ...prev,
            localvesselList: result.data,
          }));
        })
        .catch((error) => undefined);
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 5);
    }

    setTrackMyfleetState((prev) => ({ ...prev, loading: false }));
  };

  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = trackMyfleetState;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};
    let search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : trackMyfleetState.typesearch;

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
      trackMyfleetState.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setTrackMyfleetState((prev) => ({
      ...prev,
      loading: true,
      data: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${process.env.REACT_APP_URL}v1/vessel/my-fleet?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    let donloadArr = [];
    props.alltabledata(dataArr)
    if (dataArr.length > 0 ) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      state["data"] = dataArr;
    }

    

    setTrackMyfleetState((prev) => ({
      ...prev,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
       
      let pageOptions = trackMyfleetState.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
    
      setTrackMyfleetState(
        (prev) => ({
          ...prev,
          search: trackMyfleetState.search,
          pageOptions: trackMyfleetState.pageOptions,
        }),
        () => {
          getTableData(search);
        }
      );

      
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = trackMyfleetState.pageOptions;
      pageOptions["pageIndex"] = 1;
      setTrackMyfleetState(
        (prev) => ({
          ...prev,
          search: {},
          pageOptions: trackMyfleetState.pageOptions,
          typesearch: {},
        }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = trackMyfleetState.data;
      let columns = Object.assign([], trackMyfleetState.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      setTrackMyfleetState((prev) => ({
        ...prev,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !trackMyfleetState.sidebarVisible,
        columns: evt.hasOwnProperty("columns")
          ? evt.columns
          : trackMyfleetState.columns,
      }));
    } else {
      let pageOptions = trackMyfleetState.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setTrackMyfleetState(
        (prev) => ({ ...prev, pageOptions: trackMyfleetState.pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = trackMyfleetState;

    let qParams = {
      p: pageOptions.pageIndex,
      l: trackMyfleetState.pageOptions.pageLimit,
    };

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );

    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const onRowDeletedClick = (id) => {
    const { data, typesearch } = trackMyfleetState;
    let _url = `${URL_WITH_VERSION}/vessel/my-fleet/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        if (data.length === 1) {
          setTrackMyfleetState(
            (prev) => ({ ...prev, typesearch: {} }),
            () => {
              getTableData(1);
            }
          );
        } else {
          getTableData(1);
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      trackMyfleetState((prev) => {
        const nextColumns = [...prev.columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const tableCol = trackMyfleetState.columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <div>
      {trackMyfleetState.loading === false ? (
       <div style={{width : '98%'}}> <ToolbarUI
          
       routeUrl={"track-my-fleet"}
       optionValue={{
         pageOptions: trackMyfleetState.pageOptions,
         columns: trackMyfleetState.columns,
         search: trackMyfleetState.search,
       }}
       callback={(e) => callOptions(e)}
       filter={trackMyfleetState.filter}
       dowloadOptions={[
         {
           title: "CSV",
           event: () => onActionDonwload("csv", "track-vessel"),
         },
         {
           title: "PDF",
           event: () => onActionDonwload("pdf", "track-vessel"),
         },
         {
           title: "XLS",
           event: () => onActionDonwload("xlsx", "track-vessel"),
         },
       ]}
     /></div>
      ) : null}
      <Table
        bordered
        columns={tableCol}
        dataSource={trackMyfleetState.data}
        scroll={{ x: "max-content" }}
        loading={trackMyfleetState.loading}
        className="inlineTable resizeableTable"
        size="small"
        pagination={false}
        rowClassName={(r, i) =>
          i % 2 === 0
            ? "table-striped-listing"
            : "dull-color table-striped-listing"
        }
      />
    </div>
  );
}
