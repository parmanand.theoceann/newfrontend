import React, { Component, PureComponent } from 'react';
import { Row, Col, InputNumber } from 'antd';
import { TO_FIXED_2_NUM, EMPTY_2_NUM, FECTOR_VAL } from './portUtil';

class InputNumberWrapper extends PureComponent {

  handleChange = (value) => {
    if (typeof this.props.onChange === "function") {
      this.props.onChange(value, this.props.name);
    }
  }

  render() {
    return (
      <InputNumber
        size="small"
        placeholder=" "
        name={this.props.name}
        value={this.props.value}
        onChange={this.handleChange}
        min={this.props.min}
        max={this.props.max}
      />
    );
  }
};

class TextContent extends PureComponent {

  render() {
    const { text = "", className = "" } = this.props;

    return (
      <div className={`tbl-form-content ${className}`} title={text}>
        {text}
      </div>
    )
  }
}

const ATTR_NAME = "data-content-id";

const DEFAULT_VALUE = {
  bunkerExpenseVlsfo: 500,
  bunkerExpenseLsmgo: 600,

  hirePerday: 10000
}

class PortResultCalculation extends Component {

  constructor(props) {
    super(props);

    this.state = {
      bunkerExpenseVlsfo: DEFAULT_VALUE.bunkerExpenseVlsfo,
      bunkerExpenseLsmgo: DEFAULT_VALUE.bunkerExpenseLsmgo,

      bunkerValueLsmgo: 0,
      bunkerValueLsmgo: 0,

      otherExpenseCve: 0,
      otherExpenseBallastBonus: 0,
      otherExpenseRoutingExp: 0,
      otherExpenseIlohc: 0,
      otherExpenseDemDes: 0,
      otherExpenseOtherBrokerage: 0,

      hirePerday: DEFAULT_VALUE.hirePerday,
      hireCommi: 0,
      freightFRate: 0,
      freightComm: 0,

      plOtherRevenue: 0,
      plOtherExpense: 0,
    }

    this.otherExpensesRelated = [
      'otherExpenseCve',
      'otherExpenseBallastBonus',
      'otherExpenseRoutingExp',
      'otherExpenseIlohc',
      'otherExpenseDemDes',
      'otherExpenseOtherBrokerage',
    ]
  }

  componentDidMount() {
    this.setFieldUniqueId();
    this.setFieldHeight();
  }

  componentDidUpdate() {
    this.setFieldHeight();
  }

  setOtherExpenses = () => {
    let sumVal = 0;

    this.otherExpensesRelated.map(i => {
      sumVal += Number(this.state[i]) || 0;
    });

    this.setState({ plOtherExpense: sumVal });
  }

  setFieldUniqueId = () => {
    const blockSelector = document.querySelector(".port-to-port-calc");
    const blockItems = blockSelector.querySelectorAll(".tbl-form-block");

    // Set field unique ID
    blockItems.forEach((el) => {
      const tblItems = el.querySelectorAll(".tbl-form-item");
      const rendomNum = Math.floor(Math.random() * 99999);
      tblItems.forEach((tblItem) => {
        const contents = tblItem.querySelectorAll(".tbl-form-content");

        contents.forEach((con, index) => {
          con.setAttribute(ATTR_NAME, `${rendomNum}-${index}`);
        });
      });
    });
  }

  setFieldHeight = () => {
    const blockSelector = document.querySelector(".port-to-port-calc");
    const contentItems = blockSelector.querySelectorAll(`[${ATTR_NAME}]`);
    contentItems.forEach((item) => {
      const contentId = item.getAttribute(ATTR_NAME);
      const conItem = blockSelector.querySelectorAll(`[${ATTR_NAME}='${contentId}']`);
      let contentHeight = item.offsetHeight;

      conItem.forEach((el) => {
        if(contentHeight < el.offsetHeight) {
          contentHeight = el.offsetHeight;
        }
      });

      item.style.minHeight = `${contentHeight}px`;
    });
  }

  handleChange = (value, name) => {
    const hasChangeOtherExpenses = this.otherExpensesRelated.includes(name);
    this.setState({ [name]: value }, () => {
      if(hasChangeOtherExpenses) {
        this.setOtherExpenses();
      }
    });
    

  }

  getBunkerValueVlsfo = () => {
    const { bunkerExpenseVlsfo } = this.state;
    const { portDataList = [] } = this.props;
    let sum = 0;
    portDataList.map(item => {
      const val = Number(item.seaDays) * Number(item.vlsfoPerDay);
      sum += val;
    });

    // Formula Ex. ( (sea days * vlsfo) + (sea days * vlsfo) ) * bunker expense Vlsfo
    let value = sum * bunkerExpenseVlsfo;
    value = Number.isNaN(value) ? EMPTY_2_NUM : Number(value).toFixed(TO_FIXED_2_NUM)
    return bunkerExpenseVlsfo ? value : EMPTY_2_NUM;
  }

  getBunkerValueLsmgo = () => {
    const { bunkerExpenseLsmgo } = this.state;
    const { portDataList = [] } = this.props;
    let sum = 0;
    portDataList.map(item => {
      const val = Number(item.seaDays) * Number(item.lsmgo);
      sum += val;
    });

    // Formula Ex. ( (sea days * lsmgo) + (sea days * lsmgo) ) * bunker expense Lsmgo
    let value = sum * bunkerExpenseLsmgo;
    value = Number.isNaN(value) ? EMPTY_2_NUM : Number(value).toFixed(TO_FIXED_2_NUM)
    return bunkerExpenseLsmgo ? value : EMPTY_2_NUM;
  }

  getHireComValue = () => {
    const { totalDistanceData = {} } = this.props;
    const { hirePerday, hireCommi} = this.state;
    const totalVDays = Number(totalDistanceData.totalVDays);
    let val = totalVDays * ( (hirePerday * hireCommi) / 100 );
    val = Number.isNaN(val) ? EMPTY_2_NUM : Number(val).toFixed(TO_FIXED_2_NUM);
    return val;
  };

  getHireNetValue = () => {
    const hireComValue = Number(this.getHireComValue());
    const { totalDistanceData = {} } = this.props;
    const { hirePerday } = this.state;
    const totalVDays = Number(totalDistanceData.totalVDays);
    let val = (hirePerday * totalVDays) - hireComValue;
    val = Number.isNaN(val) ? EMPTY_2_NUM : Number(val).toFixed(TO_FIXED_2_NUM);
    return val;
  }

  getFreightValue = () => {
    const { totalDistanceData = {} } = this.props;
    const { freightFRate } = this.state;
    const cargo = Number(totalDistanceData.cargo);
    const value = Number(cargo * freightFRate) || EMPTY_2_NUM;
    return Number.isNaN(value) ? EMPTY_2_NUM : Number(value).toFixed(TO_FIXED_2_NUM);
  }

  getFreightCommValue = () => {
    const freightValue = Number(this.getFreightValue())
    const { freightComm } = this.state;
    // freightComm=percentage value
    const value = Number((freightValue * freightComm) / 100) || EMPTY_2_NUM;
    return Number.isNaN(value) ? EMPTY_2_NUM : Number(value).toFixed(TO_FIXED_2_NUM);
  }

  getFreightNetFreight = () => {
    const freightValue = Number(this.getFreightValue())
    const freightCommValue = Number(this.getFreightCommValue())
    const value = Number(freightValue - freightCommValue) || EMPTY_2_NUM;
    return Number.isNaN(value) ? EMPTY_2_NUM : Number(value).toFixed(TO_FIXED_2_NUM);
  }

  getPlFualExpense = () => {
    const bunkerValueVlsfo = Number(this.getBunkerValueVlsfo());
    const bunkerValueLsmgo = Number(this.getBunkerValueLsmgo());

    return Number(bunkerValueVlsfo + bunkerValueLsmgo).toFixed(TO_FIXED_2_NUM);
  }

  getProfitLoss = () => {
    const { totalDistanceData = {} } = this.props;
    const { plOtherRevenue, plOtherExpense } = this.state;
    const freightNetFreight = Number(this.getFreightNetFreight());
    const hireComValue = Number(this.getHireComValue());
    const portExp = Number(totalDistanceData.portExp);
    const plFualExpense = Number(this.getPlFualExpense());

    // Formula = ( net freight + PL other revenue ) - ( hire com value + total port expense + fual expense + other expences )
    const value = (freightNetFreight +  plOtherRevenue) - ( hireComValue + portExp + plFualExpense + plOtherExpense);

    return Number.isFinite(value) && !Number.isNaN(value) ? Number(value).toFixed(TO_FIXED_2_NUM) : EMPTY_2_NUM;
  }

  getDailyProfitLoss = () => {
    const { totalDistanceData = {} } = this.props;
    const totalVDays = Number(totalDistanceData.totalVDays);
    const profitLoss = Number(this.getProfitLoss());
    const value = profitLoss / totalVDays;
    return Number.isFinite(value) && !Number.isNaN(value) ? Number(value).toFixed(TO_FIXED_2_NUM) : EMPTY_2_NUM;
  }

  render() {
    const { totalDistanceData = {} } = this.props;
    const freightNetFreight = this.getFreightNetFreight();
    const hireComValue = this.getHireComValue();
    const hireNetValue = this.getHireNetValue();
    const bunkerValueVlsfo = this.getBunkerValueVlsfo();
    const bunkerValueLsmgo = this.getBunkerValueLsmgo();
    const freightValue = this.getFreightValue();
    const freightCommValue = this.getFreightCommValue();
    const plFualExpense = this.getPlFualExpense();
    const profitLoss = this.getProfitLoss();
    const dailyProfitLoss = this.getDailyProfitLoss();

    const profitLossClass = Number(profitLoss) > 0 ? 'tbl-profit' : Number(profitLoss) != 0 ? 'tbl-loss' : 'fixed-text';
    const dailyProfitLossCalss = Number(dailyProfitLoss) > 0 ? 'tbl-profit' : Number(dailyProfitLoss) != 0 ? 'tbl-loss' : 'fixed-text';

    return (
      <Row className='port-to-port-calc'>
        <Col span={24}>
          <Row>
            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Co2 Emi Factor</div>
                  <div className='tbl-form-content fixed-text'>VLSFO</div>
                  <div className='tbl-form-content fixed-text'>LSMGO</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title fixed-text'>Price Factor</div>
                  <div className='tbl-form-content fixed-text'>{FECTOR_VAL.vlsfo}</div>
                  <div className='tbl-form-content fixed-text'>{FECTOR_VAL.lsmgo}</div>
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Bunker Expenses</div>
                  <div className='tbl-form-content'>VLSFO ($/MT)</div>
                  <div className='tbl-form-content'>LSMGO ($/MT)</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Price ($/MT)</div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="bunkerExpenseVlsfo"
                      value={this.state.bunkerExpenseVlsfo}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="bunkerExpenseLsmgo"
                      value={this.state.bunkerExpenseLsmgo}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Bunker cons</div>
                  <div className='tbl-form-content'>Ttl Sea cons</div>
                  <div className='tbl-form-content'>Ttl Port cons</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>MT</div>
                  <div className='tbl-form-content' title={totalDistanceData.seaCons || ''}>
                    {totalDistanceData.seaCons || ' - '}
                  </div>
                  <div className='tbl-form-content' title={totalDistanceData.portCons || ''}>
                    {totalDistanceData.portCons || ' - '}
                  </div>
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Bunker Value $</div>
                  <div className='tbl-form-content'>VLSFO</div>
                  <div className='tbl-form-content'>LSMGO</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Value</div>
                  <TextContent
                    text={bunkerValueVlsfo}
                  />
                  <TextContent
                    text={bunkerValueLsmgo}
                  />
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Other Expenses</div>
                  <div className='tbl-form-content'>CVE</div>
                  <div className='tbl-form-content'>Ballast Bonus</div>
                  <div className='tbl-form-content'>Routing Exp.</div>
                  <div className='tbl-form-content'>ILOHC</div>
                  <div className='tbl-form-content'>Dem/Des</div>
                  <div className='tbl-form-content'>Other Brokerage</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Value($)</div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseCve"
                      value={this.state.otherExpenseCve}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseBallastBonus"
                      value={this.state.otherExpenseBallastBonus}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseRoutingExp"
                      value={this.state.otherExpenseRoutingExp}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseIlohc"
                      value={this.state.otherExpenseIlohc}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseDemDes"
                      value={this.state.otherExpenseDemDes}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="otherExpenseOtherBrokerage"
                      value={this.state.otherExpenseOtherBrokerage}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Total Hire value</div>
                  <div className='tbl-form-content'>Hire Per day($)</div>
                  <div className='tbl-form-content'>Hire commi %</div>
                  <div className='tbl-form-content'>Total V days</div>
                  <div className='tbl-form-content'>Hire com Value</div>
                  <div className='tbl-form-content'>Hire net Value</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Value($)</div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="hirePerday"
                      value={this.state.hirePerday}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="hireCommi"
                      value={this.state.hireCommi}
                      onChange={this.handleChange}
                      min={0}
                      max={100}
                    />
                  </div>
                  <TextContent
                    text={`$ ${totalDistanceData.totalVDays || 0}`}
                  />
                  <TextContent
                    text={`$ ${hireComValue}`}
                  />
                  <TextContent
                    text={`$ ${hireNetValue}`}
                  />
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Freight Value</div>
                  <div className='tbl-form-content'>TTl Quantity($)</div>
                  <div className='tbl-form-content'>F rate $/mt</div>
                  <div className='tbl-form-content'>Comm(%)</div>
                  <div className='tbl-form-content'>Freight Value</div>
                  <div className='tbl-form-content'>Comm value</div>
                  <div className='tbl-form-content'>Net freight</div>
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Value($)</div>
                  <TextContent
                    text={`$ ${totalDistanceData.cargo || ''}`}
                  />
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="freightFRate"
                      value={this.state.freightFRate}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="freightComm"
                      value={this.state.freightComm}
                      onChange={this.handleChange}
                      min={0}
                      max={100}
                    />
                  </div>
                  <TextContent
                    text={`$ ${freightValue}`}
                  />
                  <TextContent
                    text={`$ ${freightCommValue}`}
                  />
                  <TextContent
                    text={`$ ${freightNetFreight}`}
                  />
                </div>
              </div>
            </Col>

            <Col span={6}>
              <div className='tbl-form-block'>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>P/L Summary</div>
                  <div className='tbl-form-content'>Freight Value/ TCO Rev.</div>
                  <div className='tbl-form-content'>Other revenue</div>
                  <div className='tbl-form-content'>Vessel Hire</div>
                  <div className='tbl-form-content'>Port exp.</div>
                  <div className='tbl-form-content'>Fuel expense</div>
                  <div className='tbl-form-content'>Other expenses</div>
                  <TextContent
                    text={(<b>Profit/Loss</b>)}
                    className={profitLossClass}
                  />
                  <TextContent
                    text={(<b>Daily profit/loss</b>)}
                    className={dailyProfitLossCalss}
                  />
                </div>
                <div className='tbl-form-item'>
                  <div className='tbl-form-title'>Values($)</div>
                  <TextContent
                    text={`$ ${freightNetFreight}`}
                  />
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="plOtherRevenue"
                      value={this.state.plOtherRevenue}
                      onChange={this.handleChange}
                      />
                  </div>
                  <TextContent
                    text={`$ ${hireComValue}`}
                  />
                  <TextContent
                    text={`$ ${totalDistanceData.portExp || ''}`}
                  />
                  <TextContent
                    text={`$ ${plFualExpense}`}
                  />
                  <div className='tbl-form-content'>
                    <InputNumberWrapper
                      name="plOtherExpense"
                      value={this.state.plOtherExpense}
                      onChange={this.handleChange}
                    />
                  </div>
                  <TextContent
                    text={`$ ${profitLoss}`}
                    className={profitLossClass}
                  />
                  <TextContent
                    text={`$ ${dailyProfitLoss}`}
                    className={dailyProfitLossCalss}
                  />
                </div>
              </div>
            </Col>
          </Row>

        </Col>
      </Row>
    );
  }
}

PortResultCalculation.defaultProps = {
  totalDistanceData: {},
  portDataList: []
};

export default PortResultCalculation;
