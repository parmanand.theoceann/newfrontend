import React, { useEffect, useState } from "react";
import moment from "moment";
import { Table, Input, Button,Modal } from "antd";
import {
  ArrowUpOutlined,
  ArrowDownOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import URL_WITH_VERSION, { getAPICall } from "../../shared";
import FuturePrice from "./FuturePrice";


const columns = [
  {
    title: "Port",
    dataIndex: "port",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Country",
    dataIndex: "country",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Grade",
    dataIndex: "grade",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Price($)",
    dataIndex: "price",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Latitude",
    dataIndex: "latitude",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Longitude",
    dataIndex: "longitude",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Price Type",
    dataIndex: "priceType",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Last Updated",
    dataIndex: "lastUpdated",
    align: "left",
    render: (el) => <span className="spot-first">{el}</span>,
  },
  {
    title: "Net Change($)",
    dataIndex: "netChange",
    render: (el) => {
      if (el && el > 0) {
        return (
          <span
            className="spot-first"
            style={{ color: "#16F529", align: "middle" }}
          >
            {"$ "}
            {el.toFixed(2)}{" "}
            <ArrowUpOutlined color="green" className="hack_arrow" />
          </span>
        );
      } else if (el && el < 0) {
        return (
          <span className="spot-first" style={{ color: "red" }}>
            {"$ "}
            {el.toFixed(2)}
            &nbsp; <ArrowDownOutlined color="red" className="hack_arrow" />
          </span>
        );
      } else {
        return "---";
      }
    },
  },
];
const SpotPrice = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [data, setData] = useState([{}]);
  const [futurePricedata, setFuturePriceData] = useState([{}]);
  const [filteredData, setFilteredData] = useState([{}]);
  const [pageSize, setPageSize] = useState(50);

  const getportdata = async () => {
    let dataArr = [];
    try {
      setIsLoading(true);
      const data = await getAPICall(`${URL_WITH_VERSION}/port-bunker-activity/spotprice`);
      const respdata = await data;

      if ( respdata.BunkerExSpotPrices && respdata.BunkerExSpotPrices.length > 0 ) {
        respdata.BunkerExSpotPrices.map((el) => {
          dataArr.push({
            port: el.Port,
            country: el.Country,
            grade: el.Grade,
            price: el.Price,
            latitude: el.Latitude,
            longitude: el.Longitude,
            priceType: el.PriceType,
            lastUpdated: moment(el.LastUpdatedOnUTC).format("YYYY-MM-DD HH:mm"),
            netChange: el.NetChange,
          });
        });
        setData(dataArr);
        setFilteredData(dataArr);
        setIsLoading(false);
      }

      let futureDataArr=[];
      if ( respdata.BunkerExFuturePrices && respdata.BunkerExFuturePrices.length > 0 ) {
        setIsLoading(true);
        respdata.BunkerExFuturePrices.map((el) => {
          futureDataArr.push({
            port:el.Port,
            futureMonth: moment(el.FutureMonth).format("YYYY-MM-DD"),
            grade: el.Grade,
            lastUpdated: moment(el.LastUpdatedOnUTC).format("YYYY-MM-DD HH:mm"),
            price: el.Price,
          });
        });
        setFuturePriceData(futureDataArr);
        setIsLoading(false);
      }


    } catch (err) {
      console.log("something went wrong");
    }
  };

  const handleFuturePrice = () => {
    setModalVisible(true);
  };
  const handleCancelFuturePrice = () => {
    setModalVisible(false);
  };

  useEffect(() => {
    getportdata();
  }, []);

  const onSearchFilter = (e) => {
    const { value } = e.target;

    if (value.length > 2) {
      const filtered = data.filter((item) => {
        return (
          item.port.toLowerCase().includes(value.toLowerCase()) ||
          item.country.toLowerCase().includes(value.toLowerCase()) ||
          item.grade.toLowerCase().includes(value.toLowerCase()) ||
          (item.price !== null &&
            item.price !== undefined &&
            item.price.toString().includes(value)) ||
          (item.netChange !== null &&
            item.netChange !== undefined &&
            item.netChange.toString().includes(value))
        );
      });

      setFilteredData(filtered);
    } else if (value.length === 0) {
      setFilteredData([]);
    }
  };


  const handleTableChange = (pagination) => {
    if (pagination.pageSize) {
      setPageSize(pagination.pageSize);
    }
  };

  return (
    <div style={{ padding: "20px" }}>
      <div
        style={{
          marginBottom: "20px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <div>
          <Input
            placeholder="Search"
            name="filter"
            prefix={<SearchOutlined className="site-form-item-icon" />}
            style={{ width: "300px", margin: "0 auto" }}
            onChange={onSearchFilter}
          />
        </div>

        <div>
          <Button type="primary" onClick={handleFuturePrice}>Future Price</Button>
        </div>
      </div>

      <Table
        className="spot"
        columns={columns}
        dataSource={filteredData.length > 0 ? filteredData : data}
        size="medium"
        loading={isLoading}
        // pagination={{
        //   pageSize: 50,
        //   showSizeChanger: true,
        //   total: filteredData.length > 0 ? filteredData.length : data.length,
        // }}

        pagination={{
          pageSize: pageSize,
          showSizeChanger: true,
          onShowSizeChange: (current, size) =>{
            setPageSize(size)
          } ,
          total: filteredData.length > 0 ? filteredData.length : data.length,
        }}

        onChange={handleTableChange}
      />

      <Modal
        title={<p style={{textAlign:"center",fontSize:"x-large"}}>Future Price</p>}
        open={modalVisible}
        onCancel={handleCancelFuturePrice}
        width="max-content"
        style={{ top: 0, left: 0, marginTop: "1rem" }}
        footer={null}
      >
        <FuturePrice futurePricedata={futurePricedata} />
      
      </Modal>
    </div>
  );
};

export default SpotPrice;
