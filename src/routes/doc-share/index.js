import { Layout, Table, Drawer,  Modal } from "antd";
import React, { Component } from "react";
import { ShareAltOutlined ,UploadOutlined} from '@ant-design/icons';
import ShareLink from "../../shared/ShareLink";
import Attachment from "../../components/vessel-form/Attachment";
import URL_WITH_VERSION, {
  objectToQueryStringFunc,
  getAPICall,
} from "../../shared";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";

const { Content } = Layout;

class DocShare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      search: {},
      columns: [
        {
          title: "Upload",
          width: 100,
          render: (text, record) => {
            return (
              <div className="editable-row-operations">
                <span
                  className="iconWrapper"
                  onClick={() => this.openDocumentsUpload(record)}
                >
                 <UploadOutlined />
                </span>
              </div>
            );
          },
        },
        {
          title: "Share",
          // dataIndex: "address",
          // key: "address",
          width: 100,
          render: (text, record) => {
            return (
              <div className="editable-row-operations">
                <span
                  className="iconWrapper"
                  onClick={() => this.openShareView(record)}
                >
                <ShareAltOutlined />
                </span>
              </div>
            );
          },
        },
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "vessel_name",
          width:"200px",
        },
        {
          title: "DWT",
          dataIndex: "vessel_dwt",
          key: "vessel_dwt",
          width:"100px",
        },
        {
          title: "TYPE",
          dataIndex: "vessel_type",
          key: "vessel_type",
          width:"100px",
        },
        {
          title: "IMO Number",
          dataIndex: "imo_no",
          key: "imo_no",
          width:"100px",
        },
        {
          title: "Last Modify Date",
          dataIndex: "created_on",
          key: "created_on",
          width:"100px",
        },
        {
          title: "Certificate expired",
          dataIndex: "address",
          key: "address",
        },
      ],
      mapData: null,
      loading: false,
      filterVessels: [],
      loadComponent: undefined,
      drawerVisible: false,
      shareViewVisible: false,
      selectedVessel: null,
      formData: {
        id: 0,
        "portconsp.tableperday": [
          {
            editable: true,
            index: 0,
            con_type: 3,
            con_g: 2,
            id: -9e6 + 0,
            con_unit: 2,
          },
          {
            editable: true,
            index: 1,
            con_type: 10,
            con_g: 4,
            id: -9e6 + 1,
            con_unit: 2,
          },
          {
            editable: true,
            index: 2,
            con_type: 5,
            con_g: 3,
            id: -9e6 + 2,
            con_unit: 2,
          },
          {
            editable: true,
            index: 3,
            con_type: 7,
            con_g: 4,
            id: -9e6 + 3,
            con_unit: 2,
          },
          {
            editable: true,
            index: 4,
            con_type: 4,
            con_g: 3,
            id: -9e6 + 4,
            con_unit: 2,
          },
        ],
        id: 1,
        "seaspdconsp.tableperday": [
          {
            editable: true,
            index: 1,
            speed_type: 1,
            ballast_laden: 1,
            id: -9e6 + 10,
            engine_load: 85,
          },
          {
            editable: true,
            index: 2,
            speed_type: 1,
            ballast_laden: 2,
            id: -9e6 + 11,
            engine_load: 85,
          },
        ],
      },
    };
  }

  openDocumentsUpload = async (record) => {
    const { formData } = this.state;

    // const response = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${record.vessel_id}`);
    // const respData = response['data'];
    record["id"] = record["vessel_id"];
    let loadComponent = undefined;
    loadComponent = <Attachment frmData={record} />;

    this.setState({
      ...this.state,
      drawerVisible: true,
      loadComponent: loadComponent,
      width: 1200,
    });
  };

  onCloseDrawer = () =>
    this.setState({ ...this.state, drawerVisible: false, loadComponent: null });

  componentDidMount = async () => {
    this.setState({
      loading: true,
    });
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let qParamString = objectToQueryStringFunc(qParams);


    fetch(`${process.env.REACT_APP_URL}v1/vessel/list?${qParamString}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          data: result.data,
          loading: false,
          pageOptions: {
            pageIndex: result.page_number,
            totalRows: result.total_rows,
            pageLimit: 10,
          },
        });
      })
      .catch((error) => undefined);
    this.getTableData();
  };

  openShareView = async (selectedVessel) => {
    this.setState({
      ...this.state,
      shareViewVisible: true,
      selectedVessel: selectedVessel.vessel_id,
    });
  };

  closeShareView = () => this.setState({ shareViewVisible: false });
  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};
    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim()

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
    }
    // console.log(search);
    this.setState({
      ...this.state,
      loading: true,
      data: [],
    });
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${process.env.REACT_APP_URL}v1/vessel/list?${qParamString}`;

    const response = await getAPICall(_url, headers);
    const data = await response;
    let state = { loading: false };
    state["data"] = data && data.data ? data.data : [];
    let totalRows = data.total_rows;
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };
  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }

  //resizing function
  handleResize = (index) => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };
  render() {
    const {
      loadComponent,
      drawerVisible,
      pageOptions,
      columns,
      search,
      sidebarVisible,
      filter,
      loading,
    } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="tcov-wrapper full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="fieldscroll-wrap">
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body common-fields-wrapper">
                        {loading === false ? (
                          <ToolbarUI
                            routeUrl={"tcov-list-toolbar1"}
                            optionValue={{
                              pageOptions: pageOptions,
                              columns: columns,
                              search: search,
                            }}
                            callback={(e) => this.callOptions(e)}
                            filter={filter}
                            // dowloadOptions={[
                            //   {
                            //     title: "CSV",
                            //     event: () =>
                            //       this.onActionDonwload("csv", "tcov"),
                            //   },
                            //   {
                            //     title: "PDF",
                            //     event: () =>
                            //       this.onActionDonwload("pdf", "tcov"),
                            //   },
                            //   {
                            //     title: "XLS",
                            //     event: () =>
                            //       this.onActionDonwload("xlsx", "tcov"),
                            //   },
                            // ]}
                          />
                        ) : (
                          undefined
                        )}
                        {/* <ToolbarUI
                            routeUrl={"tcov-list-toolbar"}
                            optionValue={{
                              pageOptions: pageOptions,
                              columns: columns,
                              search: search,
                            }}
                            callback={(e) => this.callOptions(e)}
                            filter={filter}
                            dowloadOptions={[
                              {
                                title: "CSV",
                                event: () =>
                                  this.onActionDonwload("csv", "tcov"),
                              },
                              {
                                title: "PDF",
                                event: () =>
                                  this.onActionDonwload("pdf", "tcov"),
                              },
                              {
                                title: "XLS",
                                event: () =>
                                  this.onActionDonwload("xlsx", "tcov"),
                              },
                            ]}
                          /> */}
                        <Table
                          bordered
                          columns={this.state.columns}
                          dataSource={this.state.data}
                          // scroll={{
                          //   x: 3700,
                          //   y: 800,
                          // }}
                          scroll={{ x: "max-content" }}
                          pagination={false}
                          loading={this.state.loading}
                          className="inlineTable resizeableTable"
                          size="small"
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                        />
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Content>
            {loadComponent && drawerVisible && (
              <Drawer
                title={"Document Upload"}
                placement="right"
                closable={true}
                onClose={this.onCloseDrawer}
                open={this.state.drawerVisible}
                getContainer={false}
                style={{ position: "absolute" }}
                width={"70%"}
                maskClosable={false}
                className="drawer-wrapper-container"
              >
                <div className="tcov-wrapper">
                  <div className="layout-wrapper scrollHeight">
                    <div className="content-wrapper noHeight">
                      {this.state.loadComponent}
                    </div>
                  </div>
                </div>
              </Drawer>
            )}
            <Modal
              open={this.state.shareViewVisible}
              title="Share"
              onOk={this.closeShareView}
              onCancel={this.closeShareView}
              footer={null}
              width={1000}
              maskClosable={false}
            >
              <ShareLink vesselId={this.state.selectedVessel} />
            </Modal>
          </Layout>
        </Layout>
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => this.callOptions(e)}
          />
        ) : null}
      </div>
    );
  }
}
export default DocShare;
