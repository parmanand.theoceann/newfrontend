import React from 'react';
import { Table, Icon, Modal, Tooltip } from 'antd';
import AddAddressBook from '../../components/AddAddressBook';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc,  openNotificationWithIcon,  ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import RemittanceBank from '../agent-login-panel/RemittanceBankList';
import {
  EditOutlined,
  InboxOutlined,
  OrderedListOutlined,
} from "@ant-design/icons";
import { useEffect } from 'react';
import { color } from 'echarts';


const  AddressList = (props) =>  {

  const [state, setState] = useStateCallback({
    visible: false,
    loading: false,
    columns: [],
    responseData: [],
    contactColumn: [],
    contactResponse: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    selectedValue: "all",
    isVisibleContact: false,
    isVisibleRemittance:false,
    formDataValues: props.formData || props.formDataValues || {},
    isAdd: false,
    sidebarVisible: false,
    bank_id: false,
    bankAccount_id:null,
    typesearch:{},
    donloadArray: []
  })

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper edit" onClick={(ev) => editTableData(record.id)}>
            <EditOutlined />
            </span>
            
            <span className="iconWrapper inbox" onClick={(ev) => 
              onShowRemittanceInformation(record.id,record.bankAccount_id)
              //  console.log("record",record)

            }
              >
              <Tooltip title="Remittance Information">
              <InboxOutlined />
              </Tooltip>
            </span>
            {props.childPage === 'agentlist' && 
            <span className="iconWrapper inbox" onClick={(ev) => props.openModel(true ,record.id)}>
              <Tooltip title="Check Appoinments">
              <OrderedListOutlined />
              </Tooltip>
            </span>
            }
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(record.id)}>
                <DeleteOutlined />
              </Popconfirm>
            </span> */}
          </div>
        )
      }
    };

    let tableHeaders = Object.assign([], FIELDS && FIELDS['address-list-type'] ? FIELDS['address-list-type']["tableheads"] : [])  
    
    let indexOf = tableHeaders.findIndex(item => item.key === "short_name")
    if (indexOf > -1) {
      tableHeaders[indexOf] = {
        ...tableHeaders[indexOf],
        render: (text, record) => <a style={{color:"#1890ff"}} onClick={() => onRowContact(record.id)}>{text}</a>,
      }
    }
    tableHeaders.push(tableAction)    
    let contactTableHeaders = Object.assign([], FIELDS && FIELDS['contact-list'] ? FIELDS['contact-list']["tableheads"] : [])

    setState(prevState => ({...prevState, columns: tableHeaders, contactColumn: contactTableHeaders}), () => getTableData())
  }, [])


  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;
   let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
   let headers = { "order_by": { "id": "desc" } };

   setState((prevState) => ({
    ...prevState,
    loading: true,
    responseData: [],
  }));

  let qParamString = objectToQueryStringFunc(qParams);

  let _url = `${URL_WITH_VERSION}/address/list?${qParamString}`;


       // added for vendor listing
    if (props.match && props.match.path === "/vendor") {
      let _where = { "address_type_id": "12" }
      headers["where"] = (headers.hasOwnProperty("where") ? Object.assign({}, headers["where"], _where) : _where)
    }
    //end

    //=========call by orgnization list================== //
    if(props.childPage === 'orgnization') {
     // let _where = {"address_type_id" : { "in" : "(13 , 7, 9)" }}
    //  let _where = {}
    //  headers["where"] = (headers.hasOwnProperty("where") ? Object.assign({}, headers["where"], _where) : _where)
      _url = `${URL_WITH_VERSION}/address/comp/list?${qParamString}`
     
    }
    if(props.childPage === 'agentlist') {
      let _where = { address_type_id : "1" }
      headers["where"] = (headers.hasOwnProperty("where") ? Object.assign({}, headers["where"], _where) : _where)
    }
    //===============end=================================//

    let search=searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue')?searchtype:state.typesearch;
    
    
    if (search && search.hasOwnProperty('searchValue') && search.hasOwnProperty('searchOptions') && search['searchOptions'] !== '' && search['searchValue'] !== '') {
      let wc = {}
      search['searchValue'] = search['searchValue'].trim()

      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { "OR": {} };
        so.map(e => wc['OR'][e] = { "l": search['searchValue'] });
      } else {
        wc = { "OR": {} };
        wc['OR'][search['searchOptions']] = { "l": search['searchValue'] }
      }

      headers['where'] = wc;
      state.typesearch={'searchOptions':search.searchOptions,'searchValue':search.searchValue}
      
    }

    const response = await getAPICall(_url, headers);
    const data = await response;
 
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = {}
    let donloadArr = []
    if (dataArr.length > 0 && (totalRows > 0)) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      _state["responseData"] = dataArr;
    }

    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  }



  const editTableData = async (id) => {
    const response = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${id}`);
    const respData = await response.data;
    setState(prevState => ({...prevState, formDataValues: respData, isAdd: false}), () => setState(prevState => ({...prevState, visible: true})))
  }

  // const onShowRemittanceInformation = (id,bankAccount_id) => {
  //   setState(prevState => ({ ...prevState, isVisibleRemittance: bankAccount_id ? true : false, bank_id: id ,bankAccount_id:bankAccount_id}));
  // };

  const onShowRemittanceInformation = (id, bankAccount_id) => {
    if (bankAccount_id) {
      setState(prevState => ({
        ...prevState,
        isVisibleRemittance: true,
        bank_id: id,
        bankAccount_id: bankAccount_id,
      }));
    } else {
      openNotificationWithIcon('info', 'No Remittance Data is available for this Bank');
    }
  };

  const onCancelRemittance = (st) => {
    setState(prevState => ({ ...prevState, isVisibleRemittance: st, bank_id : st }))
  };

  const getContact = async (id) => {
    let { contactResponse } = state;
    let qParams = {};
    qParams['ad_id'] = id;
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/address/director/list?${qParamString}`;
    let response = await getAPICall(_url, undefined);
    if (response.data.length > 0) {
      contactResponse = response.data;
    }
    else {
      contactResponse = []
    }
    setState(prevState => ({
      ...prevState, contactResponse
    }))
  }

  const onRowContact = (id) => {
    getContact(id);
    setState(prevState => ({
      ...prevState,
      isVisibleContact: true,
    }))
  }

  const onKeypress = (e) => {
    let headers = {};
    if (e && e.hasOwnProperty('searchValue') && e.hasOwnProperty('searchOptions') && e['searchOptions'] !== '' && e['searchValue'] !== '') {
      if (e['searchOptions'].indexOf(';') > 0) {
        let so = e['searchOptions'].split(';');
        headers = { "OR": {} };
        so.map(el => headers['OR'][el] = { "l": e['searchValue'] });
      } else {
        headers[e['searchOptions']] = { "l": e['searchValue'] }
      }
    }

    getTableData(1, { "where": headers, "order_by": { "id": "DESC" } });
    
  };


  const onCancelContact = (e) => setState(prevState => ({ ...prevState, isVisibleContact: false }));

  const onSelectChange = (value) => setState(prevState => ({ ...prevState, selectedValue: value }));

  const onShowModal = (canAdd) => setState(prevState => ({ ...prevState, isAdd: canAdd, visible: true }));

  const onSave = (e) => setState(prevState => ({ ...prevState, isAdd: false, visible: false }));

  const onChange = (page) => getTableData(page, { "order_by": { "id": "DESC" } });

  const onCancel = (e) => {
    getTableData(1, { "order_by": { "id": "DESC" } });
    setState(prevState => ({ ...prevState, isAdd: false, visible: false }));
  }

  const sizeChanger = (value) => {
    let pageOptions = { ...state.pageOptions, pageLimit: value }
    setState(prevState => ({...prevState, pageOptions: pageOptions}), () => getTableData(1, { "order_by": { "id": "DESC" } }))
  }

  const callOptions = (evt) => {
   
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = state.pageOptions;
      let search = { 'searchOptions': evt['searchOptions'], 'searchValue': evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      setState(prevState => ({...prevState, search: search, pageOptions: pageOptions}), () => getTableData(evt))

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = state.pageOptions;
      pageOptions['pageIndex'] = 1;
      setState(prevState => ({...prevState, search: {}, pageOptions: pageOptions,typesearch:{}}), () => getTableData())
    }else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'new-entry') {
      onShowModal(true)
    }
    else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);;

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      setState(prevState => ({
        ...prevState,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
      }))
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }
      setState(prevState => ({ ...prevState, pageOptions: pageOptions }), () => getTableData())
    }
  }
  
 
 
 
  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const handleContactResize = index => (e, { size }) => {
    setState(({ contactColumn }) => {
      const nextColumns = [...contactColumn];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { contactColumn: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  
  const onActionDonwload =async (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = state;  


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }


    const { columns, loading, responseData, contactColumn, contactResponse, isVisibleRemittance, pageOptions, formDataValues, isAdd, search, sidebarVisible, bank_id,bankAccount_id } = state;

    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

    const tableContactColumn = contactColumn.map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: handleContactResize(index),
      }),
    }));

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="p-b-15">
                {
                  loading === false ?
                    <ToolbarUI routeUrl={'address-list-toolbar'} optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }} callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {title:'CSV', event: () => onActionDonwload('csv', 'address')},
                      {title:'PDF', event: () => onActionDonwload('pdf', 'address')},
                      {title:'XLS', event: () => onActionDonwload('xlsx', 'address')}
                    ]}
                    />
                    : undefined
                }
              </div>
              <div>
                <Table
                  rowKey='id'
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableColumns}
                  // size="small"
                  components={components}
                  scroll={{ x: 'max-content' }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                />
              </div>
            </div>
          </div>
        </article>
        {
          state.visible === true ?
            <Modal
              
              title={isAdd? (props.childPage === 'agentlist')? `New Agent` : `Add Address List`: (props.childPage === 'agentlist')? `Edit Agent` :`Edit Address List` }
              open={state.visible}
              width={'90%'}
              onCancel={onCancel}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
              maskClosable={false}
            >
              {
                isAdd === true ?
                  <AddAddressBook showSideListBar={false}  formDataValues={props.childPage==='orgnization'?{} : {companytype: { disablefield: ["BC", "W", "Z"] }}} modalCloseEvent={onCancel}  />
                  :
                  <AddAddressBook showSideListBar={false} formDataValues={props.childPage==='agentlist'?Object.assign({"companytype" : {"disablefield": ["BS","PS","C","B","RR","W","Z","SH","OW","BC","CE","M","T"]}},formDataValues):
                                                  props.childPage==='orgnization'?Object.assign({"companytype" : {"disablefield": ["BS","PS","C","B","RR","PA","SH","OW","CE","M","T"]}}, formDataValues) :
                                                   formDataValues} modalCloseEvent={onCancel} activeAgent={(props.childPage === 'agentlist')? true : false} />
              }
            </Modal>
            : undefined
        }
        <Modal
          title="Contact List"
          open={state.isVisibleContact}
          width={'90%'}
          onCancel={onCancelContact}
          footer={null}
        >
          <div className="body-wrapper">
            <article className="article">
              <div className="box box-default">
                <div className="box-body">
                  <Table
                    rowKey='id'
                    className="inlineTable resizeableTable"
                    bordered
                    components={components}
                    scroll={{ x: 'max-content' }}
                    columns={tableContactColumn}
                    // size="small"
                    dataSource={contactResponse}
                    loading={loading}
                    pagination={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                  />
                </div>
              </div>
            </article>
          </div>
        </Modal>
        {isVisibleRemittance === true && bank_id && bankAccount_id ?
  <Modal
    title="Bank And Account Info"
    visible={isVisibleRemittance}
    width={'90%'}
    onCancel={() => onCancelRemittance(false)}
    footer={null}
    maskClosable={false}
  >
    <RemittanceBank bankID={bank_id} />
  </Modal>
  :null
 
   
}

        {/* column filtering show/hide */}
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => callOptions(e)} /> : null
        }
      </div>
    )
  }

export default AddressList;
