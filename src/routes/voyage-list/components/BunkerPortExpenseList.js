import React, { Component } from "react";
import { Table, Modal, Popconfirm } from "antd";

import URL_WITH_VERSION, { getAPICall, apiDeleteCall, objectToQueryStringFunc, openNotificationWithIcon } from "../../../shared";
//import BunkerPeDetail from "./BunkerPeDetail";
import BunkerPe from "./BunkerPe";
import ToolbarUI from "../../../components/CommonToolbarUI/toolbar_index";
import { FIELDS } from "../../../shared/tableFields";
import { EditOutlined } from "@ant-design/icons";
import SidebarColumnFilter from "../../../shared/SidebarColumnFilter";

class BunkerPortExpenseList extends Component {
  constructor(props) {
    super(props);

    const tableAction = {
      title: "Action",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: "70",

      render: (el, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper edit"
              onClick={(ev) => this.port_expenses_detail(true, record.id)}
            >
         <EditOutlined />

            </span>

            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };


    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["port-expense-lists"]
        ? FIELDS["port-expense-lists"]["tableheads"]
        : []
    );

    tableHeaders.push(tableAction);

    this.state = {
      sidebarVisible: false,
      isshowportexpensedetails: false,
      columns: tableHeaders,
      responseData: null,
      loading: true,
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      editportexid:null,
      voyID: this.props.voyID || null,
      
      donloadArray: []
    };

    this.tableref = React.createRef();
  }

  port_expenses_detail = async (showportexpensedetails, voyid = null) => {
  
    if(voyid !==null){
    
         this.setState({
        ...this.state,
        editportexid: voyid,
        isshowportexpensedetails: showportexpensedetails,
        voyId: voyid,
      });
    }else{
       this.setState({
        ...this.state,
        isshowportexpensedetails: showportexpensedetails,
      });
    }
  };

  componentDidMount = () => {
    this.setState({ ...this.state, loading: true }, () => this.getTableData(this.state.voyID));
  };



  getTableData = async (search = {}) => {
    const { pageOptions, voyID } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: +pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString;
    if (search === voyID) {
      qParamString = `e=${voyID}`;
    }else {
      qParamString = objectToQueryStringFunc(qParams);
    }
    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];

    let state = { loading: false };
    let donloadArr = []
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
    
  };



  // onRowDeletedClick = (id) => {
  //   const {voyID } = this.state;

  //   let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/delete?e=${id}`;
  //   apiDeleteCall(_url, { id: id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon('success', response.message);
  //       this.getTableData(voyID);
  //     } else {
  //       openNotificationWithIcon('error', response.message);
  //     }
  //   });
  // };







  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty('sidebarVisible')
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };



  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = this.state;  



  let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

  columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
  // if (cols && cols.length > 0) {
  //   params = params + '&c=' + cols.join(',')
  // }
  const filter = donloadArray.join()
  // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
}


  handleclosePEDetail = (showPeDetail) => {
    this.setState({ isshowportexpensedetails: showPeDetail },()=>this.getTableData(this.state.voyID))
  }
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };
  render() {
    const {
      isshowportexpensedetails,
      responseData,
      loading,
      pageOptions,
      search,
      columns,
      editportexpenseData,
      sidebarVisible,
      editportexid,
    } = this.state;



    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));

    return (
      <>
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"port-expenses-list-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => this.callOptions(e)}
                    // here pdf is downloading wrong  may be backend problem.
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv', 'bunexp') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf', 'bunexp') },
                      { title: 'XLS', event: () => this.onActionDonwload('xlsx', 'bunexp') }
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>
              <div>
                <Table
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableColumns}
                  scroll={{ x: "max-content" }}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => this.callOptions(e)}
          />
        ) : null}
        {isshowportexpensedetails ? (
          <Modal
            className="page-container"
            style={{ top: "4%" }}
            title="Port Expense Summary"
           open={isshowportexpensedetails}
            onOk={this.handleOk}
            onCancel={() => this.port_expenses_detail(false)}
            width="90%"
            footer={null}
          >
            <BunkerPe id={editportexid} modalCloseEvent={(showPeDetail) => this.handleclosePEDetail(showPeDetail)} />
          </Modal>
        ) : (
          undefined
        )}
      </>
    );
  }
}

export default BunkerPortExpenseList;
