import React, { useState } from "react";
import VesselMap from "./VesselMap";
import { Tabs, Card, Grid, Typography, Accordion, Row, Col,Collapse } from 'antd';



const { TabPane } = Tabs;
const { Panel } = Collapse;

const ParsedInfo =()=>{
    const [activeTab, setActiveTab] = useState("vessel_particulars");
   
    const [vesseldata, setVesselData] = useState({

        vesselinfo: {
          shipname: 'SUNNY STAR',
          dwt: '37857',
          imo: '9470959',
          passage_type: 'N/A'
        },
        vessel_particulars: {
          shipname: 'N/A',
          dwt: 'N/A',
          mmsi: 'N/A',
          imo: '2020-10-14T12:00:00.000Z',
          ship_id: 'N/A',
          speed:'N/A' ,
          status:'N/A',
          ship_type:'N/A',
          call_sign:'N/A',
          flag:'N/A',
          length:'N/A',
          breadth:'N/A',
          grt:'N/A',
          year_built:'N/A',
          ship_yard:'N/A',
          eedi:'N/A',
          rightship_safety_score:'N/A',
          rightship_ghg_rating:'N/A',

        },

        live_position:
        {
            latitude: 'N/A',
            longitude: 'N/A',
            speed: 'N/A',
            avg_speed: 'N/A',
            max_speed: 'N/A',
            heading: 'N/A',
            course: 'N/A',
            status: 'N/A',
            destination: 'N/A',
            eta: 'N/A',
            distance_to_go: 'N/A',
            distance_travelled: 'N/A',


        },

        port_congestion:
        {
              port: 'N/A',
              country: 'N/A',
              un_locode: 'N/A',
              vessels_in_port: 'N/A',
              departures_last_24_hrs: 'N/A',
              arrivals_last_24_hrs: 'N/A',
              global_area: 'N/A',
              time_anch: 'N/A',
              time_port: 'N/A',
              vessels: 'N/A',
              calls: 'N/A',
              time_anch_stdev: 'N/A',
              time_anch_diff: 'N/A',
              time_port_stdev: 'N/A',
              time_port_diff: 'N/A',
              shipname: 'N/A',
              ship_type: 'N/A',
              capacity: 'N/A',
              reported_eta: 'N/A',
        },

    })

    const handleChange = (newValue) => {
        setActiveTab(newValue);
    };
    const mapStyle = 'mapbox://styles/mapbox/streets-v11' 
    const center= [-74.5, 40] 
    const zoom = 9

    const layoutCSS = {
        display: 'flex',
        flexDirection: 'row', 
        height: '100%',
      };

    return(
        <>
        <div style={{ ...layoutCSS, background: 'inherit', width: '100vw',maxWidth:'100%'}}>
            <VesselMap mapStyle={mapStyle} center={center} zoom={zoom} />
            <hr />
        </div>


        <div style={{ background: 'inherit'}}>
            <div>
                <Card style={{ maxWidth:'100%', marginTop:'20px' }}>
                <h3>Vessel Information</h3> 
                <div>
                    <Row gutter={[10,10]}>
                   
                    <Col span={5}>
                        Vessel Name
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vesselinfo?.shipname ?? 'N/A'}
                    </Col>
                       
                    
                    
                    <Col span={5}>
                        Dwt
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vesselinfo?.dwt ?? 'N/A'}
                    </Col>
                    
                    
                    <Col span={5}>
                        Imo
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vesselinfo?.imo ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Passage type
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vesselinfo?.passage_type ?? 'N/A'}
                    </Col>
                    {/* Replace other Col components similarly */}
                    </Row>
                </div>
                </Card>
            </div>
        </div>


        <Tabs activeKey={activeTab} onChange={handleChange} tabBarGutter={50} tabBarStyle={{ fontWeight: 'bold' }} >
        <TabPane tab="Particulars" key="vessel_particulars">
        <div style={{ background: 'inherit'}}>
            <div>
                <Card style={{ maxWidth:'100%', marginTop:'20px' }}>
                <h3>Particulars</h3> 
                <div>
                    <Row gutter={[10,10]}>
                    <Col span={5}>
                        Vessel Name:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.shipname ?? 'N/A'}
                    </Col>
                    
                    <Col span={5}>
                        Dwt:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.dwt ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                       Mmsi:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.mmsi ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Imo:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.imo ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Ship id:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.ship_id ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Speed:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.speed?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Status:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.status ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Ship type:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.ship_type ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Call sign:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.call_sign ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Flag:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.flag ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Length:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.length ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Breadth:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.breadth?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        GRT:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.grt ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Year built:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.year_built ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Ship yard:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.ship_yard ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        EEDI:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.eedi ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Right ship safety score:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.rightship_safety_score ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Right ship ghg rating:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.vessel_particulars?.rightship_ghg_rating ?? 'N/A'}
                    </Col>
                    
                    </Row>
                </div>
                </Card>
            </div>
        </div>
        </TabPane>
        <TabPane tab="Live Position" key="live_position">
        <div style={{ background: 'inherit'}}>
            <div>
                <Card style={{ maxWidth:'100%', marginTop:'20px' }}>
                <h3>Live Position</h3> 
                <div>
                    <Row gutter={[10,10]}>
                    <Col span={5}>
                        Latitude:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.latitude ?? 'N/A'}
                    </Col>
                    
                    <Col span={5}>
                        Longitude:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.longitude ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Speed:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.speed ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Average speed:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.avg_speed ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Max speed:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.max_speed ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Heading:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.heading ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Course:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.course ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Status:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.status ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Destination:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.destination ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        ETA:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.eta ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Distance to go:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.distance_to_go ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Distance travelled:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.live_position?.distance_travelled ?? 'N/A'}
                    </Col>
                    {/* Replace other Col components similarly */}
                    </Row>
                </div>
                </Card>
            </div>
        </div>
            
        </TabPane>
        <TabPane tab="Port Congestion" key="port_congestion">
        <div style={{ background: 'inherit'}}>
            <div>
                <Card style={{ maxWidth:'100%', marginTop:'20px' }}>
                <h3>Port Congestion</h3> 
                <div>
                    <Row gutter={[10,10]}>
                    <Col span={5}>
                        Port:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.port ?? 'N/A'}
                    </Col>
                    
                    <Col span={5}>
                        Country:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.country ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Unlocode:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.un_locode ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Vessels in port:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.vessels_in_port ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Departure last 24 hrs:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.departures_last_24_hrs ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Arrivals last 24 hrs:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.arrivals_last_24_hrs ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Global Area:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.global_area ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time anch:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_anch ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time port:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_port ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Vessles:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.vessels ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Calls:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.calls ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time anch stdev:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_anch_stdev ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time anch diff:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_anch_diff?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time port stdev:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_port_stdev ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Time port diff:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.time_port_diff ?? 'N/A'}
                    </Col>
                    <Row gutter={[16,16]}>
                    <Col span={24}> 
                        <h4 style={{ fontWeight: '600', paddingRight:"400px" }}>Expected Arrival:</h4>
                    </Col>
                    </Row>
                    
                    <Col span={5}>
                        Vessel name:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.shipname ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Ship type:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.ship_type ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Capacity:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.capacity ?? 'N/A'}
                    </Col>
                    <Col span={5}>
                        Reported ETA:
                    </Col>
                    <Col span={5}>
                      ----------
                    </Col>
                    <Col span={10}>
                        {vesseldata?.port_congestion?.reported_eta ?? 'N/A'}
                    </Col>

                    
                    
                    </Row>
                </div>
                </Card>
            </div>
        </div>
        </TabPane>
        <TabPane tab="Voyage-history" key="voyage_history">
        <Collapse accordion>
        <Panel header="Atlanta/Hamsburg" key="1">
            <p>Start</p>
            <p>Anchored</p>
            <p>Anchored</p>
            <p>Load</p>
            <p>Discharge</p>
        </Panel>
        <Panel header="Tampa/Altmira" key="2">
            <p>Start</p>
            <p>Anchored</p>
            <p>Anchored</p>
            <p>Load</p>
            <p>Discharge</p>
        </Panel>
        <Panel header="Amsterdam/Itacoatiara" key="3">
            <p>Start</p>
            <p>Anchored</p>
            <p>Anchored</p>
            <p>Load</p>
            <p>Discharge</p>
        </Panel>
        <Panel header="Gdansk/Amsterdam" key="4">
            <p>Start</p>
            <p>Anchored</p>
            <p>Anchored</p>
            <p>Load</p>
            <p>Discharge</p>
        </Panel>
        <Panel header="Poerto Cortes/Gdansk" key="5">
            <p>Start</p>
            <p>Anchored</p>
            <p>Anchored</p>
            <p>Load</p>
            <p>Discharge</p>
        </Panel>
        
        </Collapse>
        </TabPane>
        
        
        </Tabs>

        </>
    )
}

export default ParsedInfo;

