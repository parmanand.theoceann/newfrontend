import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';

// CSS file for mapbox-gl styles
import 'mapbox-gl/dist/mapbox-gl.css';



const MAPBOXTOKEN=process.env.NEXT_PUBLIC_IMAGE_PATH;

const VesselMap = ({ mapStyle, center, zoom }) => {
    const mapContainer = useRef(null);

  useEffect(() => {
    mapboxgl.accessToken = 'pk.eyJ1IjoidGVjaHRoZW9jZWFuIiwiYSI6ImNsNnl2M2tpOTAwM28za3F0a3pyaW4zY3IifQ.4WYqoGc0-pmPqhkP4sYMjQ';

    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: mapStyle,
      center: center,
      zoom: zoom,
    });

    // Cleanup function to remove the map on component unmount
    return () => map.remove();
  }, [mapStyle, center, zoom]);

  return <div ref={mapContainer} style={{ width: '100vw', height: '100vh' }}/>;
};

export default VesselMap;
