import React, { useEffect, lazy, Suspense, useRef } from "react";
import { Modal, Layout, Drawer, notification, Spin, Alert } from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  apiDeleteCall,
  useStateCallback,
} from "../../../shared";
import moment from "moment";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../shared/attachments";
import {
  DeleteOutlined,
  EyeOutlined,
  MenuFoldOutlined,
  SaveOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { useLocation, useParams, useNavigate } from "react-router-dom";
import {
  calculateTotalSummary,
  calculateTotalaverage,
} from "../../chartering/routes/utils";
import MapIntellegence from "../../port-to-port/MapIntellegence";
import SpotPrice from "../../port-to-port/SpotPrice";
import DynamicVspm from "../../dynamic-vspm/DynamicVspm";
import PNLSaveHistory from "./PNLSaveHistory";

//import VoygenBunkerPlan from "../../voyage-bunker-plan";

const VoygenBunkerPlan = lazy(() => import("../../voyage-bunker-plan/index"));
const MemoizedVoygenBunkerPlan = React.memo(VoygenBunkerPlan);

const openNotification = (keyName) => {
  let msg = `Please save Voyage Manager Form first, and then click on ${keyName}`;

  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const openNotification1 = (keyName) => {
  let msg = `${keyName}`;

  notification.info({
    message: `Can't Open`,
    description: msg,
    placement: "topRight",
  });
};

const { Content } = Layout;

const ModalAlertBox = lazy(() => import("../../../shared/ModalAlertBox"));
const FreightCommission = lazy(() =>
  import("../../../shared/components/FreightCommission")
);
const CommissionSummary = lazy(() =>
  import("../../../shared/components/CommissionSummary")
);
const LayTime = lazy(() => import("../../../shared/components/LayTime"));
const NormalFormIndex = lazy(() =>
  import("../../../shared/NormalForm/normal_from_index")
);
//const CargoChildForm=lazy(()=>import('../../chartering/routes/cargo/cargo-child'));
const PortExpenseSummary = lazy(() =>
  import("../../../shared/PortExpenseModal/expensemodal")
);
const AgencyAppointment = lazy(() =>
  import(
    "../../operation/my-portcalls/agency-appointment/components/AgencyAppointment"
  )
);
const BunkerReqSummaryList = lazy(() =>
  import("../../voyage-bunker-plan/bunker-req-summary-list")
);
const CommissionInvoice = lazy(() =>
  import(
    "../../operation/freight/commission-invoice/components/CommissionInvoice"
  )
);
const CommissionInvoiceSummary = lazy(() =>
  import(
    "../../operation/freight/commission-invoice/components/CommissionInvoiceSummary"
  )
);
const Delays = lazy(() => import("../../delays/Delays"));
const RevenueExpenses = lazy(() =>
  import("../../operation/revenue-expenses/components/RevenueExpenses")
);
const SavedAppointment = lazy(() =>
  import("../../operation/my-portcalls/save-appointment/SavedAppointment")
);
const BunkerRequirements = lazy(() =>
  import("../../operation/bunkers/bunker-requirements")
);
const InitialFreightInvoice = lazy(() =>
  import("../../operation/freight/initial-freight-invoice")
);
const InitialFreightInvoiceSummary = lazy(() =>
  import(
    "../../operation/freight/initial-freight-invoice/components/InitialFreightInvoiceSummary"
  )
);
// const VoygenBunkerPlan = lazy(() => import("../../voyage-bunker-plan"));
const LaytimeSummary = lazy(() =>
  import("../../voyage-manager/LaytimeSummary")
);
const NewLaytime = lazy(() => import("../../voyage-manager/NewLaytime"));
const BunkerPurchageOrderSummary = lazy(() =>
  import(
    "../../operation/bunkers/bunker-purchage-order-summary/BunkerPurchageOrderSummary"
  )
);

const VoyageManagementReport = lazy(() =>
  import("../../operation-reports/VoyageManagementReport")
);
const TCOV = lazy(() => import("../../../routes/chartering/routes/tcov/index"));
const TCTO = lazy(() =>
  import("../../../routes/chartering/routes/tcto/FullEstimate/index")
);
const EstimateSummary = lazy(() =>
  import("../../../shared/components/Estimates/EstimateSummary")
);
const PL = lazy(() => import("../../../shared/components/PL"));
const RightLog = lazy(() => import("../../right-log/RightLog"));
const ParsedInfo = lazy(() => import("./ParsedInfo"));
const RightContactList = lazy(() =>
  import("../../right-contact/RightContactList")
);
const RightInvoice = lazy(() => import("../../right-invoice/RightInvoice"));
const RightProperties = lazy(() =>
  import("../../right-properties/RightProperties")
);
const RightTaskAlert = lazy(() =>
  import("../../right-task-alert/RightTaskAlert")
);
const PositionReport = lazy(() =>
  import("../../position-report/PositionReport")
);
const RightBarUI = lazy(() => import("../../../components/RightBarUI"));
const FullEstimate = lazy(() =>
  import("../../chartering/routes/voy-relet/QuickEstimate")
);
const ProfitLossReport = lazy(() =>
  import("../../form-reports/TctoProfitLossReport")
);
const TcovEstimateDetail = lazy(() =>
  import("../../form-reports/TcovEstimateDetail")
);
const CommissionPayment = lazy(() =>
  import("../../../shared/components/CommissionPayment")
);
const PortExpenseList = lazy(() =>
  import("../../../routes/port-expense/port-expense-list")
);
const PortActivityDetail = lazy(() =>
  import("../../../shared/components/PortActivityDetail")
);
const PortDistance = lazy(() => import("../../port-to-port/PortAnalytics"));
const ShipDistance = lazy(() => import("../../port-to-port/ShipDetails"));
const CargoCargoContract = lazy(() =>
  import("../../../routes/chartering/routes/cargo-contract")
);
const AttachmentFile = lazy(() =>
  import("../../../shared/components/Attachment")
);
const Remarks = lazy(() => import("../../../shared/components/Remarks"));
const BunkerPortExpenseList = lazy(() => import("./BunkerPortExpenseList"));
const BunkerPe = lazy(() => import("./BunkerPe"));
const CargoDetails = lazy(() =>
  import("../../chartering/routes/voyage-cargo-in")
);
const CargoContract = lazy(() =>
  import("../../../routes/chartering/routes/cargo-contract")
);
const OffHireDeviation = lazy(() => import("../../delays/OffHireDeviation"));
const OffHireDeviationList = lazy(() =>
  import("../../delays/OffhireDeviationlist")
);
const BunkerPeDetail = lazy(() => import("./BunkerPeDetail"));

//-------------

const VoyageOperation = (props) => {

  const [state, setState] = useStateCallback({
    isBunker: false,
    isweather: false,
    // isNoon: false,
    isMap: false,
    isSideMap: false,
    frmName: "voyage_manager_form",
    portItin: [],
    isShowPortDistance: false,
    portData: null,
    isShowPortRoute: false,
    formData: Object.assign({ id: -1 }, props.formData || {}),
    //voyID: props.voyID || 0,
    voyID: props.formData?.estimate_id || 0,
    cargoData: {},
    visisbleCargoChild: false,
    fullFormData: {},
    editable: false,
    count: 1,
    modalStatus: false,
    modalBody: null,
    modalFooter: null,
    modalHeader: null,
    routeUrl: "voyage-list",
    isVisibleLaytime: false,
    isShowPortExpenseSummary: false,
    isShowportExpense: false,
    isShowAgencyAppointment: false,
    isShowBunkerRequirementSummary: false,
    isShowCommissionInvoice: false,
    isShowFreightCommissionInvoiceSummary: false,
    isShowDelayModal: false,
    isShowOtherRevExp: false,
    isShowSavedAppointmentPortcall: false,
    //isShowInitialFreightInvoice: false,
    isShowInitialFreightInvoiceSummary: false,
    isShowVoyageBunkerPlan: false,
    isShowBunkerRequerment: false,
    isShowPortBunkerActivity: false,
    isShowBunkerPurchageOrderSummary: false,
    isShowLaytimeSummary: false,
    isShowNewLayTime: false,
    portlists: [],
    frmVisible: props.frmVisible || true,
    extraFormFields: null,
    isShowVoyageManagementReport: false,
    postFormData: null,
    isShowEstimate: false,
    estimateID: undefined,
    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    sendBackData: { show: false },
    showInvoiceEditForm: { isShow: false },
    isShowEstimateReport: false,
    isShowProfitLossReport: false,
    showPopup: false,
    popupData: {},
    initialPrice: null,
    portEditData: null,
    isShowportExp: false,
    show_port_call_for_user: [],
    editportexpData: [],
    TCIDATA: null,
    selectedID: null,
    showEstimatePl: false,
    isupdatedbunkerportactivity: false,
    viewData: {},
    isShowOffhireDeviation: false,
    isShowOffhireDeviationList: false,
    isShowsideFrtCmsnInvoice: false,
    isShowSideportExp: false,
  });

  const location = useLocation();
  const params = useParams();
  const navigate = useNavigate();
  let formref = useRef(null);





  const reFreshForm = async () => {
    // setState(
    //   (prevState) => ({
    //     ...prevState,
    //     frmVisible: false,
    //   }),
    //   () => {
    //     setState((prev) => ({
    //       ...prev,
    //       frmVisible: true,
    //     }));
    //   }
    // );
    _onEditDataLoad(params.id);
  };

  // useEffect(()=>{
  //   state.formData['mis_cost']='0.00';
  // },[]);

  useEffect(() => {
    if (params?.id) {
      getestimatedata();
      _onEditDataLoad(params?.id)
    }
  }, [params.id]);


  useEffect(() => {
    const observerCallback = (mutations) => {
      mutations.forEach((mutation) => {
        mutation.addedNodes.forEach((node) => {
          if (params.id) {
            if (params.id.includes("TCOV-FULL")) {
              hideElementByClassName(node, "table-tcoterm");
              hideElementByClassName(node, "table-term");
            } else if (params.id.includes("TCTO-FULL")) {
              hideElementByClassName(node, "table-cargos");
            }
          }
        });
      });
    };

    const observer = new MutationObserver(observerCallback);

    observer.observe(document.body, {
      childList: true,
      subtree: true,
    });

    return () => {
      observer.disconnect();
    };
  }, [params.id]);

  const hideElementByClassName = (node, className) => {
    if (node.classList && node.classList.contains(className)) {
      node.style.display = "none";
    }
  };

  const CreateOffhire_deviation = (showdeviation) => {
    const { formData } = props;
    let _formdata = Object.assign({}, formData);
    _formdata["voyage_manager_id"] = formData["id"];

    setState((prevState) => ({
      ...prevState,
      isShowOffhireDeviation: showdeviation,
      formData: _formdata,
    }));
  };

  const showOffhireDeviation_list = (showdeviationlist) => {
    setState((prevState) => ({
      ...prevState,
      isShowOffhireDeviationList: showdeviationlist,
    }));
  };

  const _onEditDataLoad = async (coaVciId) => {
    setState(
      (prevState) => ({
        ...prevState,
        frmVisible: false,
      })
      //() => navigate(`/voyage-manager/${coaVciId}`)
    );

    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/edit?ae=${coaVciId}`
    );
    const data = await response["data"];

    if (data && !data.hasOwnProperty("portitinerary")) {
      data["portitinerary"] = [{ port: "Select Port", index: 0 }];
    }


    let toitinaryfields = [
      "total_distance",
      "total_tsd",
      "total_gsd",
      "total_seca",
      "total_seca_days",
      "total_xsd",
      "total_lq",
      "total_turntime",
      "totalt_port_days",
      "total_xpd",
      "total_exp",
      "total_port_pdays",
    ];

    let fromitinaryfields = [
      "miles",
      "tsd",
      "gsd",
      "seca_length",
      "eca_days",
      "xsd",
      "l_d_qty",
      "turn_time",
      "days",
      "xpd",
      "p_exp",
      "t_port_days",
    ];

    let totalitenary = calculateTotalSummary(
      fromitinaryfields,
      toitinaryfields,
      "portitinerary",
      data
    );

    const totalspeed = calculateTotalaverage("speed", "portitinerary", data);

    const totaleffspeed = calculateTotalaverage(
      "eff_speed",
      "portitinerary",
      data
    );

    totalitenary = {
      ...totalitenary,
      total_speed: isNaN(totalspeed) ? 0 : totalspeed,
      total_eff_spd: isNaN(totaleffspeed) ? 0 : totaleffspeed,
    };

    let tobunkerarr = [
      "ttl_miles",
      "ttl_seca_length",
      "ttl_eca_days",
      "ttl_speed",
      "ttl_ec_ulsfo",
      "ttl_ec_lsmgo",
      "ttl_ifo",
      "ttl_vlsfo",
      "ttl_lsmgo",
      "ttl_mgo",
      "ttl_ulsfo",
      "ttl_pc_ifo",
      "ttl_pc_vlsfo",
      "ttl_pc_lsmgo",
      "ttl_pc_mgo",
      "ttl_pc_ulsfo",
    ];


    let frombunkerarr = [
      "miles",
      "seca_length",
      "eca_days",
      "speed",
      "eca_consp",
      "eca_consp",
      "ifo",
      "vlsfo",
      "lsmgo",
      "mgo",
      "ulsfo",
      "pc_ifo",
      "pc_vlsfo",
      "pc_lsmgo",
      "pc_mgo",
      "pc_ulsfo",
    ];

    const totalbunker = calculateTotalSummary(
      frombunkerarr,
      tobunkerarr,
      "bunkerdetails",
      data
    );

    const fromciiarr = [
      "ifo",
      "vlsfo",
      "lsmgo",
      "mgo",
      "ulsfo",
      "pc_ifo",
      "pc_vlsfo",
      "pc_lsmgo",
      "pc_mgo",
      "pc_ulsfo",
      "co2_emission_total",
    ];

    const tociiarr = [
      "ttl_ifo_con",
      "ttl_vlsfo_con",
      "ttl_lsmgo_con",
      "ttl_mgo_con",
      "ttl_ulsfo_con",
      "ttl_pc_ifo_con",
      "ttl_pc_vlsfo_con",
      "ttl_pc_lsmgo_con",
      "ttl_pc_mgo_con",
      "ttl_pc_ulsfo_con",
      "ttl_co_emi",
    ];

    const totalcii = calculateTotalSummary(
      fromciiarr,
      tociiarr,
      "ciidynamics",
      data
    );

    const totalciiVal = {
      ttl_vlsfo_con: (
        parseFloat(totalcii["ttl_vlsfo_con"]) +
        parseFloat(totalcii["ttl_pc_vlsfo_con"])
      ).toFixed(2),
      ttl_lsmgo_con: (
        parseFloat(totalcii["ttl_lsmgo_con"]) +
        parseFloat(totalcii["ttl_pc_lsmgo_con"])
      ).toFixed(2),
      ttl_ulsfo_con: (
        parseFloat(totalcii["ttl_ulsfo_con"]) +
        parseFloat(totalcii["ttl_pc_ulsfo_con"])
      ).toFixed(2),
      ttl_mgo_con: (
        parseFloat(totalcii["ttl_mgo_con"]) +
        parseFloat(totalcii["ttl_pc_mgo_con"])
      ).toFixed(2),
      ttl_ifo_con: (
        parseFloat(totalcii["ttl_ifo_con"]) +
        parseFloat(totalcii["ttl_pc_ifo_con"])
      ).toFixed(2),
      ttl_co_emi: totalcii["ttl_co_emi"],
    };

    const fromeuetcs = [
      "sea_ems",
      "port_ems",
      "ttl_ems",
      "sea_ets_ems",
      "port_ets_ems",
      "ttl_eu_ets",
      "ttl_eu_ets_exp",
    ];

    const toeuetcs = [
      "ttl_sea_emi",
      "ttl_port_emi",
      "ttl_emi",
      "ttl_sea_ets_emi",
      "ttl_port_ets_emi",
      "ttl_ets_emi",
      "ttl_eu_ets_emi",
    ];
    const totaleuets = calculateTotalSummary(
      fromeuetcs,
      toeuetcs,
      "euets",
      data
    );

    let totalVoyDays =
      (totalitenary?.["total_port_pdays"] ?? 0) * 1 +
      (totalitenary?.["total_tsd"] ?? 0) * 1;
    data["totalitinerarysummary"] = { ...totalitenary };
    data["totalbunkersummary"] = { ...totalbunker };
    data["totalciidynamicssummary"] = { ...totalciiVal };
    data["totaleuetssummary"] = { ...totaleuets };

    data["total_days"] = isNaN(totalVoyDays)
      ? 0
      : parseFloat(totalVoyDays).toFixed(2);





    setState((prevState) => ({
      ...prevState,
      formData: data,
      selectedID: coaVciId,
      frmVisible: true,
    }));

    navigate(`/voyage-manager/${coaVciId}`, { replace: true });
  };

  const OtherRevExp = (showOtherRevExp) => {
    const { formData } = state;
    if (formData && formData.hasOwnProperty("id") && formData["id"] > 0) {
      setState((prevState) => ({
        ...prevState,
        isShowOtherRevExp: showOtherRevExp,
      }));
    } else openNotification("Other Rev/Exp");
  };

  const DelayModal = (showDelayModal) => {
    const { formData } = state;
    if (formData && formData.hasOwnProperty("id") && formData["id"] > 0) {
      let postFormData = {
        vessel: formData.vessel_id,
        vessel_code: formData.vessel_code,
        commencing: formData.commence_date,
        Completing: formData.completing_date,
        voyage_manager_id: formData.id,
      };
      setState((prevState) => ({
        ...prevState,
        postFormData,
        isShowDelayModal: showDelayModal,
      }));
    } else openNotification("Delays");
  };

  const handleCancel = () =>
    setState((prevState) => ({ ...prevState, isVisibleLaytime: false }));
  const edit = () =>
    setState((prevState) => ({ ...prevState, editable: true }));

  const AgencyAppointment = (showAgencyPopup) => {
    const voy_ID = state.formData.id;
    if (voy_ID) {
      setState((prevState) => ({
        ...prevState,
        isShowAgencyAppointment: showAgencyPopup,
      }));
    } else {
      openNotificationWithIcon("error", "please select Voyage Manager!");
    }
  };

  const port_expenses_list = (show_portexpenses_list) => {
    const voy_ID = state.formData.id;
    if (voy_ID) {
      setState((prevState) => ({
        ...prevState,
        isShowportExpenseslist: show_portexpenses_list,
      }));
    } else {
      openNotificationWithIcon("error", "please select Voyage Manager!");
    }
  };

  const AgencyAppointmentCancel = () =>
    setState((prevState) => ({ ...prevState, isShowAgencyAppointment: false }));

  const laytimeSummary = (showLaytimesummary) =>
    setState((prevState) => ({
      ...prevState,
      isShowLaytimeSummary: showLaytimesummary,
    }));

  const PortBunkerActivity = (showPortBunkerActivity, data) => {
    setState((prevState) => ({
      ...prevState,
      isShowPortBunkerActivity: showPortBunkerActivity,
      portEditData: null,
      portbunkeract: data,
    }));
  };

  const bunkerPurchageOrderSummary = (showBunkerPurchageOrderSummary) =>
    setState((prevState) => ({
      ...prevState,
      isShowBunkerPurchageOrderSummary: showBunkerPurchageOrderSummary,
    }));

  const openCommissionInvoice = (showCommissionInvoice) =>
    setState((prevState) => ({
      ...prevState,
      isShowCommissionInvoice: showCommissionInvoice,
    }));

  const FreightCommissionInvoiceSummary = (
    showFreightCommissionInvoiceSummary
  ) => {
    setState((prevState) => ({
      ...prevState,
      isShowFreightCommissionInvoiceSummary:
        showFreightCommissionInvoiceSummary,
    }));
  };

  const portExpenseSummary = (showPopup) => {
    setState((prevState) => ({
      ...prevState,
      isShowPortExpenseSummary: showPopup,
    }));
  };

  const portExpense = (showPopup) =>
    setState({ ...state, isShowportExpense: showPopup });
  const BunkerRequirementSummary = (showBunkerSummary) => {
    setState((prevState) => ({
      ...prevState,
      isShowBunkerRequirementSummary: showBunkerSummary,
    }));
  };

  const initialFreightInvoice = (showInitialFreightInvoice) => {
    setState((prevState) => ({
      ...prevState,
      showPopup: showInitialFreightInvoice,
    }));
  };

  const initialFreightInvoiceSummary = (showInitialFreightInvoiceSummary) => {
    setState((prevState) => ({
      ...prevState,
      isShowInitialFreightInvoiceSummary: showInitialFreightInvoiceSummary,
    }));
  };

  const VoyageBunkerPlan = (showVoyageBunkerPlan) => {
    setState((prevState) => ({
      ...prevState,
      isShowVoyageBunkerPlan: showVoyageBunkerPlan,

    }));
  };

  const BunkerRequirement = (showBunkerRequirement) => {
    setState((prevState) => ({
      ...prevState,
      isShowBunkerRequerment: showBunkerRequirement,
    }));
  };

  const openVoyageManagementReport = async (showVoyageManagementReport, estimate_id = params.id) => {
    if (showVoyageManagementReport == true) {
      try {
        let qParamString = objectToQueryStringFunc({ ae: estimate_id });
        // for report Api
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/report?${qParamString}`
        );
        const respDataReport = await responseReport["data"];
        if (responseReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowVoyageManagementReport: showVoyageManagementReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong.", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowVoyageManagementReport: showVoyageManagementReport,
      }));
    }
  };

  const NewLayTime = (showNewlaytime) =>
    setState((prevState) => ({
      ...prevState,
      isShowNewLayTime: showNewlaytime,
    }));

  const SavedAppointmentPortcall = (sAPortcall) =>
    setState((prevState) => ({
      ...prevState,
      isShowSavedAppointmentPortcall: sAPortcall,
    }));

  const showMyPortcallDashboard = () => navigate(`/my-portcall`);
  const showPortInformationList = () =>
    navigate("/port-information-list", { state: state.formData.id });

  const onCancel = () =>
    setState((prevState) => ({ ...prevState, visisbleCargoChild: false }));
  const onopenModal = () =>
    setState((prevState) => ({
      ...prevState,
      modalStatus: !state.modalStatus,
      modalHeader: "Freight Commission",
    }));
  const closeOpenModal = (status) =>
    setState((prevState) => ({
      ...prevState,
      modalHeader: null,
      modalBody: null,
      modalStatus: status,
      modalFooter: null,
    }));

  const showEstimate = (bolVal, estimateID) => {
    setState((prevState) => ({
      ...prevState,
      estimateID: estimateID || params.id,
      isShowEstimate: bolVal,
    }));

    // if(estimateID.includes("TCOV")) {
    //    window.open(
    //     `${window.location.origin}/#/edit-voyage-estimate/${estimateID}`,
    //     "_blank"
    //   );
    // }

    // if(estimateID.includes("TCTO")) {
    //    window.open(
    //     `${window.location.origin}/#/edit-tc-est-fullestimate/${estimateID}`,
    //     "_blank"
    //   );
    // }
  };
  const onCloseDrawer = () => {
    setState((prevState) => ({
      ...prevState,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    }));
  };

  const EstimateReport = async (showEstimateReport) => {
    if (showEstimateReport == true) {
      try {
        let qParamString = objectToQueryStringFunc({ ae: state.voyID });
        // for report Api
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/estimatereport?${qParamString}`
        );
        const respDataReport = await responseReport["data"];

        if (respDataReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowEstimateReport: showEstimateReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong.", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowEstimateReport: showEstimateReport,
      }));
    }
  };

  const profitLossReport = async (showProfitLossReport) => {
    if (showProfitLossReport == true) {
      const { formData } = state;
      try {
        let qParamString = objectToQueryStringFunc({
          ae: formData.estimate_id,
        });
        // for report Api
        const responseReport = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/profit-loss/report?${qParamString}`
        );
        const respDataReport = await responseReport["data"];

        if (respDataReport) {
          setState((prevState) => ({
            ...prevState,
            reportFormData: respDataReport,
            isShowProfitLossReport: showProfitLossReport,
          }));
        } else {
          openNotificationWithIcon("error", "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon("error", "Something went wrong.", 5);
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowProfitLossReport: showProfitLossReport,
      }));
    }
  };

  const handleCanclePopup = () =>
    setState((prevState) => ({ ...prevState, showPopup: false }));

  const onClickAddCargo = (cargoContractData = {}, data = {}) => {
    setState((prevState) => ({
      ...prevState,
      cargoData: cargoContractData,
      //fullFormData: data,
      visisbleCargoChild: true,
    }));
  }


  const onDelete = (key) => {
    const dataSource1 = [...state.dataSource1];
    setState((prevState) => ({
      ...prevState,
      dataSource1: dataSource1.filter((item) => item.key !== key),
    }));
  };

  const callback = (evt) => {
    if (evt === "new_laytime") {
      setState((prevState) => ({ ...prevState, isVisibleLaytime: true }));
    } else if (evt === "create_agency_appointment") {
    } else if (evt === "saved_appointment") {
    } else if (evt === "myportcall") {
      // <CommissionSummary/>
    } else if (evt === "port_expense_summary") {
    } else if (evt === "port_information") {
    } else {
      let modalBody = null,
        Freight = () => <FreightCommission />,
        Commission = () => (
          <CommissionSummary frmCode="tc_commission_summary" />
        ),
        modalHeader = null,
        modalFooter = [null];

      switch (evt) {
        case "freight-commission":
          modalBody = Freight;
          modalHeader = "Freight/Relet Commission";
          break;

        case "commission-payment":
          modalBody = Commission;
          modalHeader = "TC Commission Summary";
          break;

        default:
          modalBody = null;
      }

      //setState({...state,modalBody,modalStatus: true,modalHeader,modalFooter,});
      setState((prevState) => ({
        ...prevState,
        modalStatus: true,
        modalHeader,
        modalFooter,
      }));
    }
  };

  const PortActivityDetailsPopUp = async (event, data) => {
    //setState({portEditData: event,},() => {setState({isShowPortBunkerActivity: true,});});

    setState((prevState) => ({
      ...prevState,
      portEditData: event,
      isShowPortBunkerActivity: true,
    }));
  };

  const onClickExtraIcon = async (event, data) => {
    if (event.gKey) {
      let da = {};
      let key = event.gKey.replaceAll(" ", "").toLowerCase();

      if (key === "cargos" && data &&
        data.hasOwnProperty(key) &&
        JSON.stringify("{}") !== JSON.stringify(data[key]) &&
        data[key][event.index]
      ) {
        let dataNew =
          data && data["cargos"].length > 0 ? data["cargos"][event.index] : {};
        if (data[key][event.index].hasOwnProperty("id")) {
          if (data[key][event.index]["sp_type"] == 186) {
            const response = await getAPICall(
              `${URL_WITH_VERSION}/cargo/edit?ae=${data[key][event.index]["cargo_contract_id"]
              }`
            );
            da = await response["data"];
            da["sp_type"] = 186;
          } else if (data[key][event.index]["sp_type"] == 187) {
            const response = await getAPICall(
              `${URL_WITH_VERSION}/voyagecargo/edit?ae=${data[key][event.index]["cargo_contract_id"]
              }`
            );
            da = await response["data"];
            da["sp_type"] = 187;
          }
        }

        // setState({...state,editCargoIndex: event.index,viewData: da,},
        //   () => onClickAddCargo(da, state.fullFormData)
        // );

        setState(
          (prevState) => ({
            ...prevState,
            editCargoIndex: event.index,
            viewData: da,
          }),
          () => {
            onClickAddCargo(da, state.formData);
          }
        );
      }

      if (event.gKey == "Port Itinerary") {
        let groupKey = "portitinerary";
        let frm_code = "voyage_manager_port_itinerary";
        let port_id = data && data.port_id;
        let voyage_manager_id = data && data.voyage_manager_id;
        if (groupKey && port_id && Math.sign(port_id) > 0 && frm_code) {
          let data1 = {
            port_id: port_id,
            voyage_manager_id: voyage_manager_id,
            frm_code: frm_code,
            group_key: groupKey,
          };

          postAPICall(
            `${URL_WITH_VERSION}/tr-delete-vm`,
            data1,
            "delete",
            (response) => {
              if (response && response.data) {
                openNotificationWithIcon("success", response.message);
              } else {
                openNotificationWithIcon("error", response.message);
              }
            }
          );
        }

        groupKey = "bunkerdetails";
        frm_code = "voyage_manager_bunker_details";
        port_id = data && data.port_id;
        voyage_manager_id = data && data.voyage_manager_id;
        if (groupKey && port_id && Math.sign(port_id) > 0 && frm_code) {
          let data1 = {
            port_id: port_id,
            voyage_manager_id: voyage_manager_id,
            frm_code: frm_code,
            group_key: groupKey,
          };

          postAPICall(
            `${URL_WITH_VERSION}/tr-delete-vm`,
            data1,
            "delete",
            (response) => {
              if (response && response.data) {
                openNotificationWithIcon("success", response.message);
              } else {
                openNotificationWithIcon("error", response.message);
              }
            }
          );
        }
        groupKey = "portdatedetails";
        frm_code = "voyage_manager_port_date_details";
        port_id = data && data.port_id;
        voyage_manager_id = data && data.voyage_manager_id;
        if (groupKey && port_id && Math.sign(port_id) > 0 && frm_code) {
          let data1 = {
            port_id: port_id,
            voyage_manager_id: voyage_manager_id,
            frm_code: frm_code,
            group_key: groupKey,
          };

          postAPICall(
            `${URL_WITH_VERSION}/tr-delete-vm`,
            data1,
            "delete",
            (response) => {
              if (response && response.data) {
                openNotificationWithIcon("success", response.message);
              } else {
                openNotificationWithIcon("error", response.message);
              }
            }
          );
        }
      } else if (key === "bunkerdetails") {
        if (event.eventType === "pba") {
        } else if (event.eventType === "pe") {
          // setState({
          //   ...state,
          //   isShowportExp: true,
          //   portexpid: event.eventData.port_id,
          //   port_name: event.eventData.port,
          //   Pe_arrival: event.eventData.arrival_date_time,
          //   pe_departure: event.eventData.departure,
          // });

          setState((prevState) => ({
            ...prevState,
            isShowportExp: true,
            portexpid: event.eventData.port_id,
            port_name: event.eventData.port,
            Pe_arrival: event.eventData.arrival_date_time,
            pe_departure: event.eventData.departure,
          }));
        }
      }
      // else {
      //   openNotificationWithIcon(
      //     "error",
      //     <div
      //       className="notify-error"
      //       dangerouslySetInnerHTML={{
      //         __html: `Please perform entry OR Add ${event.gKey
      //           } details from above`,
      //       }}
      //     />,
      //     5
      //   );
      // }
    }
  };

  const getCargoImport = (data) => {
    const { fullFormData, index } = state;
    let frmData = Object.assign({}, fullFormData);
    let dataNew =
      frmData && frmData["cargos"].length > 0 ? frmData["cargos"][index] : {};
    /*
    setState(
      {
        refreshForm: true,
        visibleviewmodal: false,
      },
      () => {
        dataNew["cargo_name"] = data && data.cargo_name;
        dataNew["charterer"] = data && data.charterer;
        dataNew["f_rate"] = data && data.f_rate;
        dataNew["f_type"] = data && data.f_type;
        dataNew["curr"] = data && data.curr;
        frmData["cargos"][index] = dataNew;
        setState({
          ...state,
          refreshForm: false,
          formData: frmData,
          fullFormData: frmData,
        });
      }
    );
    */

    setState(
      (prevState) => ({
        ...prevState,
        refreshForm: true,
        visibleviewmodal: false,
      }),
      () => {
        dataNew["cargo_name"] = data && data.cargo_name;
        dataNew["charterer"] = data && data.charterer;
        dataNew["f_rate"] = data && data.f_rate;
        dataNew["f_type"] = data && data.f_type;
        dataNew["curr"] = data && data.curr;
        frmData["cargos"][index] = dataNew;
        setState((prevState) => ({
          ...prevState,
          refreshForm: false,
          formData: frmData,
          fullFormData: frmData,
        }));
      }
    );
  };

  const saveFormData = async (postData) => {
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    const { frmName } = state;
    const estimateId = params.id;
    let _url = "save";
    let _method = "post";
    if (
      postData.hasOwnProperty("user_name") &&
      postData["user_name"] == "None"
    ) {
      delete postData["user_name"];
    }
    if (postData.hasOwnProperty("id") && postData["id"] > 0) {
      _url = "update";
      _method = "put";
    }

    if (
      postData.hasOwnProperty("bunkerdetails") &&
      !postData["bunkerdetails"].length > 0
    ) {
      delete postData["bunkerdetails"];
    }
    if (postData.hasOwnProperty("currency")) {
      delete postData["currency"];
    }
    if (postData.hasOwnProperty("cp_unit")) {
      delete postData["cp_unit"];
    }
    if (postData.hasOwnProperty("curr")) {
      delete postData["curr"];
    }
    if (postData.hasOwnProperty(".tci_d_hire")) {
      delete postData[".tci_d_hire"];
    }
    if (!postData["commence_date"]) {
      postData["commence_date"] = moment(new Date()).format(
        "YYYY-MM-DDTHH:mm:ss"
      );
    } else {
      postData["commence_date"] = moment(postData["commence_date"]).format(
        "YYYY-MM-DDTHH:mm:ss"
      );
    }

    if (!postData["completing_date"]) {
      postData["completing_date"] = moment(new Date()).format(
        "YYYY-MM-DDTHH:mm:ss"
      );
    } else {
      postData["completing_date"] = moment(postData["completing_date"]).format(
        "YYYY-MM-DDTHH:mm:ss"
      );
    }
    if (!postData["cp_date"] || postData["cp_date"] == "0000-00-00 00:00") {
      postData["cp_date"] = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    } else {
      postData["cp_date"] = moment(postData["cp_date"]).format(
        "YYYY-MM-DDTHH:mm:ss"
      );
    }

    // if fuel_code is present in any row
    if (postData.hasOwnProperty(".")) {
      for (const obj of postData["."]) {
        if (!obj.hasOwnProperty("fuel_code")) {
          openNotificationWithIcon(
            "error",
            "Fuel Type can not be unselected",
            3
          );
          return;
        }
      }
    }

    Object.keys(postData).forEach(
      (key) => postData[key] === null && delete postData[key]
    );

    try {
      postAPICall(
        `${URL_WITH_VERSION}/voyage-manager/${_url}?frm=${frmName}`,
        postData,
        _method,
        (data) => {
          if (data.data) {
            openNotificationWithIcon("success", data.message);

            if (_url === "save") {
              window.emitNotification({
                n_type: "VM Created",
                msg: window.notificationMessageCorrector(
                  `VM is added, for Voyage Id(${postData.voyage_number}), for  vessel(${postData.vessel_code}), by ${window.userName}`
                ),
              });
            } else {
              window.emitNotification({
                n_type: "VM Updated",
                msg: window.notificationMessageCorrector(
                  `VM is added, for Voyage Id(${postData.voyage_number}), for vessel(${postData.vessel_code}), by ${window.userName}`
                ),
              });
            }
            setState((prevState) => ({
              ...prevState,
              frmVisible: true,
              formData: Object.assign(
                {
                  id: -1,
                },
                props.formData || {}
              ),
            }));
            if (
              props.modalCloseEvent &&
              typeof props.modalCloseEvent === "function"
            ) {
              props.modalCloseEvent();
            }
            // navigate("/empty", { replace: true });
            // setTimeout(() => {
            //   navigate(`/voyage-manager/${estimateId}`, { replace: true });
            // }, 5);

            _onEditDataLoad(estimateId);
          } else {
            //setState({ ...setState, frmVisible: true });
            setState((prevState) => ({ ...prevState, frmVisible: true }));
            let dataMessage = data.message;
            let msg = "<div className='row'>";

            if (typeof dataMessage !== "string") {
              Object.keys(dataMessage).map(
                (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
              );
            } else {
              msg += dataMessage;
            }

            msg += "</div>";
            openNotificationWithIcon(
              "error",
              <div dangerouslySetInnerHTML={{ __html: msg }} />
            );
          }
        }
      );
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  const onClickRightMenu = async (key, options) => {
    const { formData } = state;

    onCloseDrawer();
    let loadComponent = undefined;
    let up = { show: false };
    switch (key) {
      case "port_route_details":
        portDistance(true, state.formData, key);
        break;
      case "port_distance":
        portDistance(true, state.formData, key);
        break;
      case "pl-summary":
        up = { show: true, options: options };
        break;
      case "estimates-summary":
        loadComponent = <EstimateSummary />;
        break;
      case "log":
        loadComponent = <RightLog />;
        break;
      case "invoice":
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = (
            <RightInvoice
              voyageManagerId={formData.id}
              voyage_number={formData.voyage_number}
              showInvoice={(invoice_no, type) =>
                redirectToAdd(invoice_no, type)
              }
              showCommission={(invoice_no, type) =>
                redirectToAdd(invoice_no, type)
              }
              showPortExpense={(e) => redirectToAdd(e, "port-expense")}
              reload={false}
            />
          );
        else openNotification("Voyage Invoice");
        break;
      case "contact":
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = (
            <RightContactList voyageManagerId={formData.voyage_number} />
          );
        else openNotification("Voyage Contact List");
        break;

      case "remark":
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = (
            <Remarks
              //remarksId={formData.id}
              remarksID={formData.estimate_id}
              // remarkInvNo={formData.estimate_id}
              remarkType="voyage-manager"
              idType="voy_id"
            />
          );
        else openNotificationWithIcon("info", "Remarks");
        break;
      case "proprties":
        loadComponent = <RightProperties />;
        break;
      case "task-alert":
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = <RightTaskAlert formData={formData} />;
        else openNotification1("Select Voyage First Then Click");
        break;
      case "position-report":
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          loadComponent = <PositionReport formData={formData} />;
        else openNotification1("Select Voyage First Then Click");
        break;
      case "attachment":
        const { estimate_id } = formData;
        if (estimate_id) {
          const attachments = await getAttachments(estimate_id, "EST");
          const callback = (fileArr) =>
            uploadAttachment(fileArr, estimate_id, "EST", "voyage-manager");
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) =>
                deleteAttachment(file.url, file.name, "EST", "voyage-manager")
              }
              tableId={0}
            />
          );
          break;
        } else {
          openNotificationWithIcon("info", "Attachments are not allowed here.");

        }
      case "bunker": {
        setState((prev) => ({ ...prev, isBunker: true }));
        break;
      }

      case "map": {
        setState((prev) => ({ ...prev, isMap: true }));
        break;
      }
      case "weather": {
        setState((prev) => ({ ...prev, isweather: true }));
        break;
      }

      case "sideMap":
        loadComponent = <ParsedInfo />
        break;
      case "vmpnl":
        if(formData && formData.hasOwnProperty('id')) {
          loadComponent = <PNLSaveHistory id={formData.id}/>
        }else{
          openNotificationWithIcon("info", "PNL Saved History List");
        }
        break;
      default:
        break;
    }

    setState((prevState) => ({
      ...prevState,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      sendBackData: up,
      width: options.width && options.width > 0 ? options.width : 1200,
    }));
  };

  const changeInvoiceTypeFunc = (vcId) => {
    let showInvoiceEditForm = { isShow: true, data: { ID: vcId } };
    setState((prevState) => ({
      ...prevState,
      showInvoiceEditForm,
      isShowOtherRevExp: true,
    }));
  };

  const sideCommissionInvoice = (isShow, key) => {
    if (key === "isShowsideTcCommision") {
      setState((prevState) => ({
        ...prevState,
        isShowsideTcCommision: isShow,
      }));
    } else if (key === "isShowsideFrtInvoice") {
      setState((prevState) => ({ ...prevState, isShowsideFrtInvoice: isShow }));
    } else if (key === "isShowsideFrtcmsnInvoice") {
      setState((prevState) => ({
        ...prevState,
        isShowsideFrtCmsnInvoice: isShow,
      }));
    } else if (key === "isShowSideportExp") {
      setState((prevState) => ({ ...prevState, isShowSideportExp: isShow }));
    }
  };

  const redirectToAdd = async (invoice_no, type) => {
    const { formData } = state;
    if (type.replace(/[^A-Z0-9]/gi, "").toLowerCase() === "freightinvoice") {
      let _url = `${URL_WITH_VERSION}/freight-invoice/edit?e=${invoice_no}`;
      const response = await getAPICall(_url);
      const respData = await response;
      if (respData["data"] && respData["data"].hasOwnProperty("id")) {
        setState(
          (prevState) => ({ ...prevState, popupData: respData["data"] }),
          () => sideCommissionInvoice(true, "isShowsideFrtInvoice")
        );
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    } else if (
      type.replace(/[^A-Z0-9]/gi, "").toLowerCase() ===
      "freightcommissioninvoice"
    ) {
      let _url = `${URL_WITH_VERSION}/freight-commission/edit?e=${invoice_no}`;
      const response = await getAPICall(_url);
      const respData = await response;
      if (respData["data"] && respData["data"].hasOwnProperty("id")) {
        setState(
          (prevState) => ({ ...prevState, popupData: respData["data"] }),
          () => sideCommissionInvoice(true, "isShowsideFrtcmsnInvoice")
        );
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    } else if (
      type.replace(/[^A-Z0-9]/gi, "").toLowerCase() === "tccommission"
    ) {
      let _url = `${URL_WITH_VERSION}/commission/edit?e=${invoice_no}`;
      const response = await getAPICall(_url);
      const respData = await response["data"];

      const response1 = await getAPICall(

        `${URL_WITH_VERSION}/accounts/borker/commission?t=tci&e=${respData.tci_id}`
      );

      const respSData = await response1["data"];
      respData["mycompny_id"] = respSData["mycompny_id"];

      const frmOptions = [
        { key: "broker", data: respSData["brokers"] },
        { key: "remmitance_bank", data: respSData["banks"] },
      ];
      if (respData && respData.hasOwnProperty("id")) {
        setState(
          (prevState) => ({
            ...prevState,
            popupData: respData,
            sidetc_frmOptions: frmOptions,
          }),
          () => sideCommissionInvoice(true, "isShowsideTcCommision")
        );
      } else {
        openNotificationWithIcon("error", respData["message"]);
      }
    } else if (type.toLowerCase() === "port-expense") {
      setState(
        (prevState) => ({ ...prevState, portexpid: invoice_no["id"] }),
        () => sideCommissionInvoice(true, "isShowSideportExp")
      );
    }
  };

  const submitFormData = async (port_call_id) => {
    const response = await getAPICall(
      `${URL_WITH_VERSION}/port-call/submit?e=${port_call_id}&pt=pc`
    );
    if (response && response.data) {
      openNotificationWithIcon("success", response.message);
      setInterval(() => {
        window.location.reload(false);
      }, 2000);
    } else {
      openNotificationWithIcon("error", response.message);
    }
  };

  const reSubmitForm = async (port_call_id) => {
    const response = await getAPICall(
      `${URL_WITH_VERSION}/port-call/resubmit?e=${port_call_id}`
    );
    if (response && response.data) {
      openNotificationWithIcon("success", response.message);
      setInterval(() => {
        window.location.reload(false);
      }, 2000);
    } else {
      openNotificationWithIcon("error", response.message);
    }
  };

  const getestimatedata = async () => {
    const estimateID = params.id;
    let qParams = { ae: estimateID };
    let qParamString = objectToQueryStringFunc(qParams);
    let Response;
    if (
      estimateID.includes("EST-TCTO") == true ||
      estimateID.includes("TCR") == true
    ) {
      Response = await getAPICall(
        `${URL_WITH_VERSION}/tcto/quick-edit?${qParamString}&pl=1`
      );
    } else if (
      estimateID.includes("EST-TCOV") == true ||
      estimateID.includes("TCE") == true ||
      estimateID.includes("TCOV-FULL") == true
    ) {
      //Response = await getAPICall(`${URL_WITH_VERSION}/tcov/quick-edit?${qParamString}&pl=1`);
      Response = await getAPICall(
        `${URL_WITH_VERSION}/tcov/edit?${qParamString}&pl=1`
      );

    } else if (
      estimateID.includes("TCTO-FULL") == true
    ) {
      Response = await getAPICall(
        `${URL_WITH_VERSION}/tcto/edit?${qParamString}&pl=1`
      );

    }

    else if (estimateID.includes("EST-VOYR") == true) {
      Response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-relet/quick-edit?${qParamString}&pl=1`
      );
    }
    const respDataC = await Response;
    const respData = respDataC ? respDataC["data"] : null;
    setState((prevState) => ({
      ...prevState,
      estimateData: respData,
      showEstimatePl: true,
    }));
  };

  const triggerEvent = async (data) => {
    const { sendBackData, formData, estimateData, showEstimatePl } = state;
    if (data && sendBackData["show"] === true) {
      onCloseDrawer();
      // if ((formData['ops_type'] * 1) === 20) {
      //   let expCount = 0, revCount = 0, revFrtRate = 0, expFrtRate = 0;
      //   let plData = {
      //     'showExactPL': true, 'dataPL': {
      //       'revenue': {
      //         "freight": 0,
      //         "freightCommission": 0,
      //         "misRevenue": 0,
      //         "grossRevenue": 0,
      //         "netRevenue": 0
      //       },
      //       'expenses': {
      //         "freightExpenses": 0,
      //         "brokerComm": 0,
      //         "misExpenses": 0,
      //         "totalExpenses": 0
      //       },
      //       'voyageResult': {
      //         "profitLoss": 0,
      //         "dailyProfitLoss": 0,
      //         "freightRate": 0,
      //         "breakevenFreightRate": 0,
      //         "netVoyageDays": 0,
      //         "offHireDays": 0
      //       }
      //     }
      //   };

      //   data['cargos'].map(e => {
      //     let amount = 0
      //     if (e['sp_type'] === 186) {
      //       revFrtRate = revFrtRate + (("" + e['frat_rate']).replaceAll(',', '') * 1);
      //       revCount = revCount + 1;
      //       amount = ((("" + e['frat_rate']).replaceAll(',', '') * 1) * (("" + e['cp_qty']).replaceAll(',', '') * 1))
      //       plData['dataPL']['revenue']['freight'] = plData['dataPL']['revenue']['freight'] + amount;
      //       plData['dataPL']['revenue']['freightCommission'] = plData['dataPL']['revenue']['freightCommission'] + ((amount * (e['comm_per'].replaceAll(',', '') * 1)) / 100);
      //       plData['dataPL']['revenue']['misRevenue'] = plData['dataPL']['revenue']['misRevenue'] + (e['extra_rev'].replaceAll(',', '') * 1);
      //     } else if (e['sp_type'] === 187) {
      //       expFrtRate = expFrtRate + (("" + e['frat_rate']).replaceAll(',', '') * 1);
      //       expCount = expCount + 1;
      //       amount = ((("" + e['frat_rate']).replaceAll(',', '') * 1) * (("" + e['cp_qty']).replaceAll(',', '') * 1));
      //       plData['dataPL']['expenses']['freightExpenses'] = plData['dataPL']['expenses']['freightExpenses'] + amount;
      //       plData['dataPL']['expenses']['brokerComm'] = plData['dataPL']['expenses']['brokerComm'] + ((amount * (e['comm_per'].replaceAll(',', '') * 1)) / 100);
      //       plData['dataPL']['expenses']['misExpenses'] = plData['dataPL']['expenses']['misExpenses'] + (e['extra_rev'].replaceAll(',', '') * 1);
      //     }
      //   });

      //   plData['dataPL']['revenue']['grossRevenue'] = plData['dataPL']['revenue']['freight'] + plData['dataPL']['revenue']['misRevenue'];
      //   plData['dataPL']['revenue']['netRevenue'] = plData['dataPL']['revenue']['grossRevenue'] - plData['dataPL']['revenue']['freightCommission'];
      //   plData['dataPL']['expenses']['totalExpenses'] = plData['dataPL']['expenses']['freightExpenses'] + plData['dataPL']['expenses']['misExpenses'] - plData['dataPL']['expenses']['brokerComm'];

      //   plData['dataPL']['voyageResult']['profitLoss'] = plData['dataPL']['revenue']['netRevenue'] - plData['dataPL']['expenses']['totalExpenses'];
      //   plData['dataPL']['voyageResult']['dailyProfitLoss'] = plData['dataPL']['voyageResult']['profitLoss'] / (data['total_days'].replaceAll(',', '') * 1);
      //   plData['dataPL']['voyageResult']['netVoyageDays'] = (("" + data['total_days']).replaceAll(',', '') * 1);
      //   plData['dataPL']['voyageResult']['freightRate'] = revFrtRate / revCount;
      //   plData['dataPL']['voyageResult']['breakevenFreightRate'] = expFrtRate / expCount;

      //   data = plData;
      // }

      let loadComponent = estimateData ? (
        <PL
          formData={Object.assign({ showExactPL: true }, data)}
          estimateData={estimateData}
          showEstimatePl={showEstimatePl}
          viewTabs={["Actual &  Operation View"]}
        />
      ) : (
        openNotificationWithIcon("info", "Please wait...")
      );
      setState((prevState) => ({
        ...prevState,
        visibleDrawer: true,
        title: sendBackData.options.title,
        loadComponent: loadComponent,
        width:
          sendBackData.options.width && sendBackData.options.width > 0
            ? sendBackData.options.width
            : 1200,
        sendBackData: { show: false },
      }));
    }
  };


  useEffect(() => {
    const calculateExtraFormFields = async () => {


      if (state.formData && state.formData.tci_code) {
        try {
          const response = await getAPICall(
            `${URL_WITH_VERSION}/tci/edit?e=${state.formData.tci_code}`
          );

          const tciEditData = response.data;  // Remove unnecessary square brackets


          setState((prevState) => ({
            ...prevState,
            TCIDATA: tciEditData,
          }));
        } catch (error) {

          // Handle the error as needed (e.g., show a user-friendly message)
        }
      }
    };

    calculateExtraFormFields();
  }, [state.formData]);  // Add state.formData as a dependency to useEffect


  const onClickCommission = async (key) => {
    const { formData, TCIDATA } = state;

    let modalBody = null,
      Commission = undefined,
      modalHeader = null,
      modalFooter = [null];
    let makeCommissionEntry = {
      id: 0,
      vessel_id: formData.vessel_id,
      tc_code: formData.tci_code,
    },
      makeCommissionData = {
        id: 0,
        vessel_name: formData.vessel_id,
        vessel_code: formData.vessel_code,
        tc_code: formData.tci_code,
      };
    let response = await getAPICall(
      `${URL_WITH_VERSION}/accounts/borker/commission?t=tci&e=${formData.tci_code}`
    );
    let respSData = await response["data"];

    if (!respSData) {
      openNotificationWithIcon("info", "Linked Tci has been deleted.");
      return;
    }
    let makeCommissionData2 = "";


    let data1 = {
      tci_id: TCIDATA && TCIDATA["id"],
      broker: respSData && respSData["brokers"] && respSData["brokers"][0] && respSData["brokers"][0]["code_name"],
    };




    postAPICall(
      `${URL_WITH_VERSION}/accounts/borker/commission/summary`,
      data1,
      "POST",
      (data) => {
        if (data && data.data) {
          makeCommissionData2 = data.data;
        }
      }
    );

    // makeCommissionEntry['tci_id'] = formData['id'];
    // makeCommissionEntry['payment_terms'] = formData['payment_term'];
    makeCommissionEntry["payment_terms2"] =
      respSData && respSData["ptd"] && respSData["ptd"]["description"];

    if (key === "commission_creation") {

      // makeCommissionEntry["tc_code"] = TCIDATA["tc_code"];
      makeCommissionEntry["tc_code"] = TCIDATA && TCIDATA["tc_code"];

      if (!makeCommissionEntry.tci_id) {
        makeCommissionEntry["tci_id"] = TCIDATA["id"];
      }
      Commission = () => (
        <CommissionPayment
          commissionData={makeCommissionEntry}
          oldFromData={formData}
          frmOptions={[
            { key: "broker", data: respSData["brokers"] },
            { key: "remmitance_bank", data: respSData["banks"] },
          ]}
        />
      );
      modalBody = Commission;
      modalHeader = "TC Commission Payment";
    }
    // if (key === "commission_creation") {
    //   console.log('TCIDATA:', TCIDATA);

    //   if (TCIDATA && TCIDATA["tc_code"]) {
    //     makeCommissionEntry["tc_code"] = TCIDATA["tc_code"];
    //   } else {
    //     // Handle the case where TCIDATA or TCIDATA["tc_code"] is null or undefined
    //     console.error("TCIDATA or tc_code is null or undefined");
    //     // Log additional information for debugging
    //     console.log('TCIDATA:', TCIDATA);
    //     console.log('TCIDATA["tc_code"]:', TCIDATA && TCIDATA["tc_code"]);
    //     // You might want to return or handle this case appropriately based on your application's logic
    //     return;
    //   }

    //   if (!makeCommissionEntry.tci_id && TCIDATA && TCIDATA["id"]) {
    //     makeCommissionEntry["tci_id"] = TCIDATA["id"];
    //   }

    //   Commission = () => (
    //     <CommissionPayment
    //       commissionData={makeCommissionEntry}
    //       oldFromData={formData}
    //       frmOptions={[
    //         { key: "broker", data: respSData["brokers"] },
    //         { key: "remmitance_bank", data: respSData["banks"] },
    //       ]}
    //     />
    //   );
    //   modalBody = Commission;
    //   modalHeader = "TC Commission Payment";
    // }


    else if (key === "commission_summary") {
      // makeCommissionData["tc_code"] = TCIDATA["tc_code"];
      makeCommissionData["tc_code"] = TCIDATA && TCIDATA["tc_code"];

      //makeCommissionData['broker'] = respSData['brokers'] && respSData['brokers'][0] && respSData['brokers'][0]['id']
      if (!makeCommissionData.tci_id) {
        makeCommissionData["tci_id"] = TCIDATA["id"];
      }
      if (makeCommissionData2) {
        makeCommissionData["-"] = makeCommissionData2;
        makeCommissionData["broker"] =
          respSData &&
          respSData["brokers"] &&
          respSData["brokers"][0] &&
          respSData["brokers"][0]["id"];
      }
      Commission = () => (
        <CommissionSummary
          frmCode="tc_commission_summary"
          commissionData={makeCommissionEntry}
          formData={makeCommissionData}
          frmOptions={[{ key: "broker", data: respSData["brokers"] }]}
          oldFromData={formData}
        />
      );
      modalBody = Commission;
      modalHeader = "TC Commission Summary";
    }

    setState((prevState) => ({
      ...prevState,
      commissionEntry: makeCommissionEntry,
      modalBody,
      modalStatus: true,
      modalHeader,
      modalFooter,
    }));
  };


  const portDistance = (val, data, key) => {
    if (key == "port_route_details") {
      if (
        data &&
        data.hasOwnProperty("portitinerary") &&
        data["portitinerary"].length > 0
      ) {
        setState(
          (prevState) => ({
            ...prevState,
            portData: data.portitinerary[0],
            portItin: data.portitinerary,
          }),
          //() => setState({ isShowPortRoute: val })
          () =>
            setState((prevState) => ({ ...prevState, isShowPortRoute: val }))
        );
      } else {
        setState((prevState) => ({ ...prevState, isShowPortRoute: val }));
      }
    } else {
      if (key == "port_distance") {
        setState((prevState) => ({ ...prevState, isShowPortDistance: val }));
      }
    }
  };

  const deleteData = (id, tcov_id, innerCB, saveData) => {
    apiDeleteCall(
      `${URL_WITH_VERSION}/voyage-manager/delete`,
      { id: id },
      (resData) => {
        if (resData.data === true) {
          alert("test");
          openNotificationWithIcon("success", resData.message);
          window.emitNotification({
            n_type: "VM Deleted",
            msg: window.notificationMessageCorrector(
              `VM is added, for Voyage(${saveData.voyage_number}) vessel(${saveData.vessel_code}), by ${window.userName}`
            ),
          });
          props.history.push("/voyage-manager-list");
        } else {
          openNotificationWithIcon("error", resData.message);
        }
      }
    );
  };
  const handleclosePE = (showPEform) =>
    setState((prevState) => ({ ...prevState, isShowportExp: showPEform }));

  let isSetsLeftBtn = [
    {
      id: "7",
      key: "menu-fold",
      type: <MenuFoldOutlined />,
      withText: "List",
      showToolTip: true,
      event: "menu-fold",
    },
    {
      id: "3",
      key: "save",
      type: <SaveOutlined />,
      withText: "Save",
      showToolTip: true,
      event: (key, data) => saveFormData(data),
    },

    {
      id: "4",
      key: "delete",
      type: <DeleteOutlined />,
      showToolTip: true,
      withText: "Delete",
      event: (key, saveData, innerCB) => {
        if (saveData["id"] && saveData["id"] > 0) {
          Modal.confirm({
            title: "Confirm",
            content: "Are you sure, you want to delete it?",
            onOk: () =>
              deleteData(
                saveData["id"],
                saveData["tcov_id"],
                innerCB,
                saveData
              ),
          });
        }
      },
    },

    {
      id: "20",
      key: "refresh",
      type: <SyncOutlined />,
      withText: "Refresh",
      showToolTip: true,
      event: () => {
        reFreshForm();
      },
    },
  ];
  const isSetsLeftBtnArr = isSetsLeftBtn.filter(
    (item) =>
      !(state?.formData?.id && state.formData.id <= 0 && item.key === "save")
  );

  return (
    <>
      <div className="wrap-rightbar full-wraps testing">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    {state.frmVisible ? (
                      <NormalFormIndex
                        key={"key_" + state.frmName + "_0"}
                        formClass="label-min-height"
                        formData={state.formData}
                        showForm={true}
                        frmCode={state.frmName}
                        addForm={true}
                        showToolbar={[
                          {
                            isLeftBtn: [{ isSets: isSetsLeftBtnArr }],
                            isRightBtn: [
                              {
                                isSets: [
                                  {
                                    key: "Estimate",
                                    isDropdown: 0,
                                    withText: "Estimate",
                                    type: "",
                                    menus: null,
                                    event: () =>
                                      showEstimate(
                                        true,
                                        state.formData["estimate_id"]
                                      ),
                                  },
                                  {
                                    key: "Freight",
                                    isDropdown: 1,
                                    withText: "Freight/Relet",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Freight Invoice",
                                        modalKey: "freight-invoice",
                                        event: (key) =>
                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0 &&
                                            !state.formData[
                                              "estimate_id"
                                            ].includes("TCR")
                                            ? initialFreightInvoice(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Can not Create Freight Invoice"
                                            ),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Freight Invoice Summary",
                                        modalKey: "freight_invoice_summary",
                                        event: (key) =>
                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0
                                            ? initialFreightInvoiceSummary(true)
                                            : openNotification(
                                              "Freight Invoice Summary"
                                            ),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Freight Commission Invoice",
                                        modalKey: "freight_commission_invoice",
                                        event: (key) =>
                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0 &&
                                            !state.formData[
                                              "estimate_id"
                                            ].includes("TCR")
                                            ? openCommissionInvoice(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Can not Create Freight Commission"
                                            ),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Freight Commission Summary",
                                        modalKey:
                                          "freight_commission_invoice_summary",
                                        event: (key) =>
                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0
                                            ? FreightCommissionInvoiceSummary(
                                              true
                                            )
                                            : openNotification(
                                              "Freight Commission Summary"
                                            ),
                                      },
                                    ],
                                  },
                                  {
                                    key: "TC Commission",
                                    isDropdown: 1,
                                    withText: "TC Commission",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "TC Commission Creation",
                                        modalKey: "commission_creation",
                                        event: (key) => {

                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0 &&
                                            state.formData["tci_code"] * 1 > 0
                                            ? onClickCommission(key)
                                            : // :formData['estimate_id'].includes('TCE')? openNotification("TC Commission" ):openNotificationWithIcon('info','HF/TCI Not Available for VM.',3);
                                            openNotificationWithIcon(
                                              "info",
                                              "HF/TCI Not Available for VM.",
                                              3
                                            );
                                        },
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "TC Commission Summary",
                                        modalKey: "commission_summary",
                                        event: (key) => {
                                          state.formData &&
                                            state.formData.hasOwnProperty("id") &&
                                            state.formData["id"] > 0 &&
                                            state.formData["tci_code"] * 1 > 0
                                            ? onClickCommission(key)
                                            : // :formData['estimate_id'].includes('TCE')? openNotification("TC Commission" ):openNotificationWithIcon('info','HF/TCI Not Available for VM.',3);
                                            openNotificationWithIcon(
                                              "info",
                                              "HF/TCI Not Available for VM.",
                                              3
                                            );
                                        },
                                      },
                                    ],
                                  },

                                  {
                                    key: "Other Rev/Exp",
                                    isDropdown: 1,
                                    withText: "Other Rev/Exp",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Other Rev/Exp",
                                        modalKey: "other_rev_exp",
                                        event: (key) => OtherRevExp(true),
                                      },
                                    ],
                                  },

                                  {
                                    key: "Laytime",
                                    isDropdown: 1,
                                    withText: "Laytime",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Create New Laytime",
                                        modalKey: "new_laytime",
                                        event: (key) => {
                                          NewLayTime(true);
                                        },
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Laytime Summary",
                                        modalKey: "laytime_summary",
                                        event: (key) => {
                                          laytimeSummary(true);
                                        },
                                      },
                                    ],
                                  },
                                  {
                                    key: "Delays",
                                    isDropdown: 1,
                                    withText: "Delays",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Delays",
                                        modalKey: "delay",
                                        event: (key) => DelayModal(true),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "OffHire/Deviation Invoice",
                                        modalKey: "OffHire/DeviationInvoice",
                                        event: (key) =>
                                          CreateOffhire_deviation(true),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "OffHire/Deviation List",
                                        modalKey: "OffHire/DeviationList",
                                        event: (key) =>
                                          showOffhireDeviation_list(true),
                                      },
                                    ],
                                  },
                                  {
                                    key: "Bunkers",
                                    isDropdown: 1,
                                    withText: "Bunkers",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Voyage Bunker Plan",
                                        modalKey: "voyage_bunker_plan",
                                        event: (key) => {
                                          VoyageBunkerPlan(true);
                                        },
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "New Bunker Requirement",
                                        modalKey: "new_bunker_requirement",
                                        event: (key) => {
                                          BunkerRequirement(true);
                                        },
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Bunker Requirement Summary",
                                        modalKey: "bunker_requirement-summary",
                                        event: (key) =>
                                          BunkerRequirementSummary(true),
                                      },
                                      // { "href": null, "icon": null, "label": "Port Bunker & Activity", modalKey: "port_bunker_activity", "event": (key) => { PortBunkerActivity(true) } },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Bunker Purchase Order Summary",
                                        modalKey:
                                          "bunker_purchage_order-summary",
                                        event: (key) => {
                                          bunkerPurchageOrderSummary(true);
                                        },
                                      },
                                    ],
                                  },

                                  // {
                                  //   key: "portcall",
                                  //   isDropdown: 1,
                                  //   withText: "Port Call",
                                  //   type: "",
                                  //   menus: [
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "Create Agency Appointment",
                                  //       modalKey: "create_agency_appointment",
                                  //       event: (key) =>
                                  //         AgencyAppointment(true),
                                  //     },
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "Saved appointment",
                                  //       modalKey: "saved_appointment",
                                  //       event: (key) =>
                                  //         SavedAppointmentPortcall(true),
                                  //     },
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "My Portcall",
                                  //       modalKey: "myportcall",
                                  //       event: (key) => {
                                  //         showMyPortcallDashboard();
                                  //       },
                                  //     },
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "Port Expense",
                                  //       modalKey: "port_expense",
                                  //       event: (key) =>
                                  //         portExpense(true),
                                  //     },
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "Port Expense Summary",
                                  //       modalKey: "port_expense_summary",
                                  //       event: (key) =>
                                  //         portExpenseSummary(true),
                                  //     },
                                  //     {
                                  //       href: null,
                                  //       icon: null,
                                  //       label: "Port Information List",
                                  //       modalKey: "port_information_list",
                                  //       event: (key) => {
                                  //         showPortInformationList();
                                  //       },
                                  //     },
                                  //   ],
                                  // },

                                  {
                                    key: "portexpenses",
                                    isDropdown: 1,
                                    withText: "Port Expenses",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Port Expenses Summary",
                                        modalKey: "port_expenses_list",
                                        event: (key) =>
                                          port_expenses_list(true),
                                      },
                                    ],
                                  },
                                  {
                                    key: "report",
                                    isDropdown: 1,
                                    withText: "Reports",
                                    type: "",
                                    menus: [
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Voyage Operation Report",
                                        modalKey: "new_laytime",
                                        event: (key, data) => {

                                          openVoyageManagementReport(true, data["estimate_id"])
                                        }
                                        //openVoyageManagementReport(true),
                                      },
                                      {
                                        href: null,
                                        icon: null,
                                        label: "Voyage Profit and Loss Report",
                                        modalKey: "voy_profit_loss_report",
                                        event: (key) => profitLossReport(true),
                                      },
                                      // {
                                      //   href: null,
                                      //   icon: null,
                                      //   label: "Voyage Estimate Report",
                                      //   modalKey: "voy_estimate_repot",
                                      //   event: (key) =>
                                      //     EstimateReport(true),
                                      // },
                                      // {
                                      //   href: null,
                                      //   icon: null,
                                      //   label: "Voyage Performance Report",
                                      //   modalKey: "voy_performance_report",
                                      // },
                                    ],
                                  },
                                  // {
                                  //   key: 'port_distance',
                                  //   isDropdown: 0,
                                  //   withText: 'Port Distance',
                                  //   type: '',
                                  //   menus: null,
                                  //   event: (key,data) => portDistance(true,data,key),
                                  // },
                                  // {
                                  //   key: 'port_route_details',
                                  //   isDropdown: 0,
                                  //   withText: 'Port Route Details',
                                  //   type: '',
                                  //   menus: null,
                                  //   event: (key,data) => portDistance(true,data,key),
                                  // },
                                ],
                              },
                            ],
                          },
                        ]}
                        sideList={{
                          selectedID: state.selectedID,
                          showList: true,
                          title: "Voyage List",
                          uri: "/voyage-manager/list?l=0&type=2",
                          rowClickEvent: (evt) =>
                            _onEditDataLoad(evt.voyage_number),
                          icon: true,
                          columns: [
                            ["vessel_code", "my_company_name"],
                            "vm_status1",
                            "voyage_number",
                          ],
                        }}
                        showSideListBar={!props.formData}
                        inlineLayout={true}
                        isShowFixedColumn={[
                          "Port Date Details",
                          "Bunker Details",
                          "Port Itinerary",
                          "Cargos",
                          "Port Cargo",
                          ".",
                          "Total Bunker Details",
                          "CII Dynamics",
                          "EU ETS",
                          "Tco Term"
                        ]}
                        tabEvents={[
                          {
                            tabName: "Bunker Details",
                            event: {
                              type: "copy",
                              from: "Port Itinerary",
                              fields: {
                                port_id: "port_id",
                                port: "port",
                                funct: "funct",
                                passage: "passage",
                                s_type: "spd_type",
                                seca_length: "seca_length",
                                eca_days: "eca_days",
                                speed: "speed",
                                miles: "miles",
                                tsd: "tsd",
                              },
                              calculations: [
                                "purchase_data",
                                ["ifo", "vlsfo", "lsmgo", "mgo", "ulsfo"],
                                ["eco", "cp"],
                                ["ballast", "laden"],
                              ],
                              showSingleIndex: false,
                            },
                          },
                          {
                            tabName: "Port Date Details",
                            event: {
                              type: "copy",
                              from: "Port Itinerary",
                              fields: {
                                port_id: "port_id",

                                port: "port",
                                funct: "funct",
                                passage: "passage",
                                s_type: "spd_type",
                                speed: "speed",
                                miles: "miles",
                                wf_per: "wf_per",
                                tsd: "tsd",
                                xsd: "xsd",
                                day: "days",
                                pdays: "pdays",
                                //s_type: 's_type',
                              },
                              calculations: [
                                "purchase_data",
                                ["ifo", "vlsfo", "lsmgo", "mgo", "ulsfo"],
                                ["eco", "cp"],
                                ["ballast", "laden"],
                              ],
                              showSingleIndex: false,
                            },
                          },

                          {
                            tabName: "CII Dynamics",
                            event: {
                              type: "copy",
                              from: "Bunker Details",
                              page: "tcov",
                              fields: {
                                port: "port",
                                funct: "funct",
                                ifo: "ifo",
                                vlsfo: "vlsfo",
                                ulsfo: "ulsfo",
                                lsmgo: "lsmgo",
                                mgo: "mgo",
                                pc_ifo: "pc_ifo",
                                pc_vlsfo: "pc_vlsfo",
                                pc_ulsfo: "pc_ulsfo",
                                pc_lsmgo: "pc_lsmgo",
                                pc_mgo: "pc_mgo",
                              },
                              showSingleIndex: false,
                            },
                          },

                          {
                            tabName: "EU ETS",
                            event: {
                              type: "copy",
                              from: "Bunker Details",
                              page: "tcov",
                              fields: {
                                port: "port",
                                funct: "funct",
                                ifo: "ifo",
                                vlsfo: "vlsfo",
                                ulsfo: "ulsfo",
                                lsmgo: "lsmgo",
                                mgo: "mgo",
                                pc_ifo: "pc_ifo",
                                pc_vlsfo: "pc_vlsfo",
                                pc_ulsfo: "pc_ulsfo",
                                pc_lsmgo: "pc_lsmgo",
                                pc_mgo: "pc_mgo",
                              },
                              showSingleIndex: false,
                            },
                          },

                        ]}
                        tableRowDeleteAction={(action, data) =>
                          onClickExtraIcon(action, data)
                        }
                        extraTableButton={{
                          Cargos: [
                            {
                              icon: <EyeOutlined />,
                              onClickAction: (action, data) =>
                                onClickExtraIcon(action, data),
                            },
                          ],
                          "Bunker Details": [
                            {
                              isMenu: true,
                              options: [
                                {
                                  text: "Port Bunker & Activity",
                                  key: "pba",
                                  onClickAction: (action, data) =>
                                    Modal.confirm({
                                      title: "Confirm",
                                      content:
                                        "Are You sure want to edit the data?Please reset for all port to get the correct data.",
                                      okText: "OK",
                                      cancelText: "Cancel",
                                      onOk: (e) =>
                                        PortActivityDetailsPopUp(action, data),
                                    }),

                                  // PortActivityDetailsPopUp(
                                  //   action,
                                  //   data
                                  // ),
                                },
                                {
                                  text: "Port Expenses",
                                  key: "pe",
                                  onClickAction: (action, data) =>
                                    onClickExtraIcon(action, data),
                                },
                              ],
                            },
                          ],
                        }}
                        sendBackData={state.sendBackData}
                        //triggerEvent={state.triggerEvent}
                        triggerEvent={triggerEvent}
                      />
                    ) : (
                      <div className="col col-lg-12">
                        <Spin tip="Loading...">
                          <Alert
                            message=" "
                            description="Please wait..."
                            type="info"
                          />
                        </Spin>
                      </div>
                    )}
                  </div>
                </div>
              </article>
            </Content>
          </Layout>
          {state.frmVisible ? (
            <RightBarUI
              pageTitle="voyage-manager-righttoolbar"
              callback={(data, options) => onClickRightMenu(data, options)}
            />
          ) : undefined}
          {state.loadComponent !== undefined &&
            state.title !== undefined &&
            state.visibleDrawer === true ? (
            <Drawer
              title={state.title}
              placement="right"
              closable={true}
              onClose={onCloseDrawer}
              open={state.visibleDrawer}
              getContainer={false}
              style={{ position: "absolute" }}
              width={state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  {state.loadComponent}
                </div>
              </div>
            </Drawer>
          ) : undefined}
        </Layout>

        {/* <ModalAlertBox
            modalStatus={modalStatus}
            modalHeader={modalHeader}
            modalBody={modalBody}
            modalFooter={modalFooter}
          /> */}

        <Suspense fallback="Loading....">
          <ModalAlertBox
            modalStatus={state.modalStatus}
            modalHeader={state.modalHeader}
            modalBody={state.modalBody}
            modalFooter={state.modalFooter}
            onCancelFunc={() => closeOpenModal()}
          />
        </Suspense>

        <Modal
          title={false}
          open={state.isVisibleLaytime}
          width={1800}
          onCancel={handleCancel}
          footer={false}
          maskClosable={false}
        >
          <Suspense fallback="Loading....">
            <LayTime />
          </Suspense>
        </Modal>

        {state.isShowPortDistance ? (
          <Modal
            style={{ top: "2%" }}
            title="Port Distance"
            open={state.isShowPortDistance}
            // onOk={handleOk}
            onCancel={() => portDistance(false, {}, "port_distance")}
            width="95%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <PortDistance />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowPortRoute ? (
          <Modal
            style={{ top: "2%" }}
            title="Port Route Details"
            open={state.isShowPortRoute}
            //  onOk={handleOk}
            onCancel={() => portDistance(false, {}, "port_route_details")}
            width="80%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <ShipDistance
                data={state.portData}
                is_port_to_port={true}
                portItin={state.portItin}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {/*              All Left side menus starts from here */}

        {state.isShowEstimate ? (
          <Modal
            style={{ top: "2%" }}
            open={state.isShowEstimate}
            title="Estimate Detail"
            onCancel={() => showEstimate(false, undefined)}
            footer={null}
            width={"90%"}
            maskClosable={false}
          >
            {
              // formData["ops_type"] * 1 === 20 ? (
              //   <FullEstimate estimateID={estimateID} vmCheck={true} />
              // ) :
              state.formData["estimate_id"].includes("TCTO") ? (
                <Suspense fallback="Loading....">
                  <TCTO
                    estimateID={state.estimateID}
                    vmCheck={true}
                  //history={{ ...props.history }}
                  />
                </Suspense>
              ) : (
                // <TCOV estimateID={estimateID} />

                <Suspense fallback="Loading....">
                  <TCOV
                    estimateID={state.estimateID}
                    vmCheck={true}
                    history={{ ...props.history }}
                  />
                </Suspense>
              )
            }
          </Modal>
        ) : undefined}

        {/* Freight relight Menu starts from here  */}

        {state.showPopup ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Initial Freight Invoice"
            open={state.showPopup}
            onCancel={() => handleCanclePopup()}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <InitialFreightInvoice
                modalCloseEvent={handleCanclePopup}
                formData={state.popupData}
                voyageData={state.formData}
                voyID={state.formData["voyage_number"]}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowInitialFreightInvoiceSummary ? (
          <Modal
            style={{ top: "2%" }}
            title="Initial Freight Invoice Summary"
            open={state.isShowInitialFreightInvoiceSummary}
            // onOk={handleOk}
            onCancel={() => initialFreightInvoiceSummary(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <InitialFreightInvoiceSummary voyID={state.voyID} />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowCommissionInvoice ? (
          <Modal
            style={{ top: "2%" }}
            title="Freight Commission Invoice"
            open={state.isShowCommissionInvoice}
            // onOk={handleOk}
            onCancel={() => openCommissionInvoice(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <CommissionInvoice
                modalCloseEvent={() => openCommissionInvoice(false)}
                formData={state.popupData}
                voyagedata={state.formData}
                voyId={state.formData.id}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowFreightCommissionInvoiceSummary ? (
          <Modal
            style={{ top: "2%" }}
            title="Freight Commission Summary"
            open={state.isShowFreightCommissionInvoiceSummary}
            //  onOk={handleOk}
            onCancel={() => FreightCommissionInvoiceSummary(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <CommissionInvoiceSummary
                voyageData={state.formData}
                voyID={state.voyID}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {/* Freight relight Menu ends from here  */}

        {state.visisbleCargoChild ? (
          <Modal
            style={{ top: "2%" }}
            open={state.visisbleCargoChild}
            //   title={spType === "purchase" ? "Purchase Cargo" : "Sales Cargo"}
            title={
              state.viewData && state.viewData.sp_type == 187
                ? "VC (Purchase)"
                : " Edit Voyage Charter"
            }
            onCancel={onCancel}
            footer={null}
            width={"90%"}
            maskClosable={false}
          >
            {state.viewData && state.viewData.sp_type == 187 ? (
              <Suspense fallback="Loading....">
                <CargoDetails
                  formData={state.viewData}
                  cargoImport={(data) => getCargoImport(data)}
                />
              </Suspense>
            ) : (
              <Suspense fallback="Loading....">
                <CargoContract
                  formData={state.viewData}
                  cargoImport={(data) => getCargoImport(data)}
                />
              </Suspense>
            )}
          </Modal>
        ) : undefined}

        {state.isShowOffhireDeviation ? (
          <Modal
            style={{ top: "2%" }}
            title="Create Offhire/Deviation Invoice"
            open={state.isShowOffhireDeviation}
            //  onOk={handleOk}
            onCancel={() => CreateOffhire_deviation(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <OffHireDeviation
                formdata={state.formData}
                modalCloseEvent={() => CreateOffhire_deviation(false)}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowOffhireDeviationList ? (
          <Modal
            style={{ top: "2%" }}
            title="Offhire/Deviation Invoice List"
            open={state.isShowOffhireDeviationList}
            // onOk={handleOk}
            onCancel={() => showOffhireDeviation_list(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <OffHireDeviationList voyID={state.voyID} />
            </Suspense>
          </Modal>
        ) : undefined}

        {/* 
 no use
          {isShowPortExpenseSummary ? (
            <Modal
              style={{ top: "2%" }}
              title="Port Expense Summary"
             open={isShowPortExpenseSummary}
              onOk={handleOk}
              onCancel={() => portExpenseSummary(false)}
              width="90%"
              footer={null}
              maskClosable={false}
            >
              <PortExpenseSummary voyID={voyID} history={props.history} />
            </Modal>
          ) : (
            undefined
          )} */}

        {/* 
          {isShowportExpense ? (
             no any use
            <Modal
              className="page-container"
              style={{ top: "2%" }}
              title="Port Expense "
             open={isShowportExpense}
              onOk={handleOk}
              onCancel={() => portExpense(false)}
              width="90%"
              footer={null}
              maskClosable={false}
            >
              <PortExpenseList voyID={voyID} history={props.history} />
            </Modal>
          ) : (
            undefined
          )} */}

        {state.isShowportExpenseslist ? (
          <Modal
            // className="page-container"
            style={{ top: "4%" }}
            title="Port Expense List "
            open={state.isShowportExpenseslist}
            // onOk={handleOk}
            onCancel={() => port_expenses_list(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerPortExpenseList
                voyID={state.voyID}
                history={props.history}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowAgencyAppointment ? (
          <Modal
            style={{ top: "2%" }}
            title="Agent Appointment"
            open={state.isShowAgencyAppointment}
            //  onOk={handleOk}
            onCancel={AgencyAppointmentCancel}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <AgencyAppointment
                voyID={state.formData.id}
                estimateID={state.formData.estimate_id}
                history={props.history}
                cancelagencyModel={AgencyAppointmentCancel}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowBunkerRequirementSummary ? (
          <Modal
            style={{ top: "2%" }}
            title="Bunker Requirement Summary"
            open={state.isShowBunkerRequirementSummary}
            //  onOk={handleOk}
            onCancel={() => BunkerRequirementSummary(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerReqSummaryList
                formData={state.formData}
                voyageData={state.formData}



              />
            </Suspense>

          </Modal>
        ) : undefined}


        {state.isShowDelayModal ? (
          <Modal
            // className="page-container"
            style={{ top: "2%" }}
            title="Delay"
            open={state.isShowDelayModal}
            // onOk={handleOk}
            onCancel={() => DelayModal(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <Delays formData={state.postFormData} voyID={state.voyID} />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowOtherRevExp ? (
          <Modal
            // className="page-container"
            style={{ top: "2%" }}
            title="Other Rev/Exp"
            open={state.isShowOtherRevExp}
            // onOk={handleOk}
            onCancel={() => OtherRevExp(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <RevenueExpenses
                formData={state.formData}
                showInvoiceEditForm={state.showInvoiceEditForm}
                voyID={state.voyID}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {/*voyage manager  right side elemnts starts from here  */}

        {state.isShowsideFrtInvoice ? (
          <Modal
            style={{ top: "2%" }}
            title="Edit Freight Invoice"
            open={state.isShowsideFrtInvoice}
            // onOk={handleOk}
            onCancel={() =>
              sideCommissionInvoice(false, "isShowsideFrtInvoice")
            }
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <InitialFreightInvoice
                isEdit={true}
                modalCloseEvent={() =>
                  setState((prevState) => ({
                    ...prevState,
                    isShowsideFrtInvoice: false,
                    loadComponent: undefined,
                    visibleDrawer: false,
                  }))
                }
                formData={state.popupData}
                voyageData={state.formData}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowsideFrtCmsnInvoice ? (
          <Modal
            style={{ top: "2%" }}
            title="Freight Commission Invoice"
            open={state.isShowsideFrtCmsnInvoice}
            // onOk={handleOk}
            onCancel={() =>
              sideCommissionInvoice(false, "isShowsideFrtcmsnInvoice")
            }
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <CommissionInvoice
                modalCloseEvent={() =>
                  setState((prevState) => ({
                    ...prevState,
                    isShowsideFrtCmsnInvoice: false,
                    loadComponent: undefined,
                    visibleDrawer: false,
                  }))
                }
                formData={state.popupData}
                voyagedata={state.formData}
                voyId={state.formData.id}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowsideTcCommision ? (
          <Modal
            style={{ top: "2%" }}
            title="Time Charter Commission Payment"
            open={state.isShowsideTcCommision}
            //  onOk={handleOk}
            onCancel={() =>
              sideCommissionInvoice(false, "isShowsideTcCommision")
            }
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <CommissionPayment
                isEdit={true}
                frmOptions={state.sidetc_frmOptions}
                commissionData={state.popupData}
                closeModal={() =>
                  setState((prevState) => ({
                    ...prevState,
                    isShowsideTcCommision: false,
                    loadComponent: undefined,
                    visibleDrawer: false,
                  }))
                }
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowSideportExp ? (
          <Modal
            className="page-container"
            style={{ top: "4%" }}
            title="Port Expense"
            open={state.isShowSideportExp}
            //   onOk={handleOk}
            onCancel={() => sideCommissionInvoice(false, "isShowSideportExp")}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerPe
                voyID={state.voyID}
                id={state.portexpid}
                modalCloseEvent={() =>
                  setState((prevState) => ({
                    ...prevState,
                    isShowSideportExp: false,
                    loadComponent: undefined,
                    visibleDrawer: false,
                  }))
                }
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {/*voyage manager  right side elemnts end here.  */}

        {/* {isShowInitialFreightInvoice ? (
            <Modal
              // className="page-container"
              style={{ top: '2%' }}
              title="Initial Freight Invoice"
             open={isShowInitialFreightInvoice}
              onOk={handleOk}
              onCancel={() => InitialFreightInvoice(false)}
              width="90%"
              footer={null}
            >
              <InitialFreightInvoice modalCloseEvent={InitialFreightInvoice} voyID={formData['voyage_number']} />
            </Modal>
          ) : (
            undefined
          )} */}

        {state.isShowSavedAppointmentPortcall ? (
          <Modal
            style={{ top: "2%" }}
            title="Save Appointment"
            open={state.isShowSavedAppointmentPortcall}
            // onOk={handleOk}
            onCancel={() => SavedAppointmentPortcall(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <SavedAppointment
                voyID={state.formData.id}
                history={props.history}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowVoyageBunkerPlan ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Voyage Bunker Plan"
            open={state.isShowVoyageBunkerPlan}
            //onOk={handleOk}
            onCancel={() => VoyageBunkerPlan(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <MemoizedVoygenBunkerPlan
                data={state?.formData?.bunkerdetails}

              // days={state.extraFormFieldsData}
              // initialPrice={state.initialPrice}
              />
            </Suspense>


          </Modal>
        ) : undefined}

        {state.isShowBunkerRequerment ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Bunker Requirement"
            open={state.isShowBunkerRequerment}
            //   onOk={handleOk}
            onCancel={() => BunkerRequirement(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerRequirements
                voyageData={state.formData}
                voyID={state.formData}


              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowPortBunkerActivity ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Port Bunker And Activity Details"
            open={state.isShowPortBunkerActivity}
            // onOk={handleOk}
            onCancel={() => PortBunkerActivity(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <PortActivityDetail
                data={state.formData}
                editData={state.portEditData}
                // updateportbunker={(data) => {
                //   handleupdatePortBunker(data);
                // }}
                modalCloseEvent={() => PortBunkerActivity(false)}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowBunkerPurchageOrderSummary ? (
          <Modal
            style={{ top: "2%" }}
            title="Bunker Purchase Order Summary"
            open={state.isShowBunkerPurchageOrderSummary}
            //  onOk={handleOk}
            onCancel={() => bunkerPurchageOrderSummary(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerPurchageOrderSummary
                formData={state.postFormData}
                voyageData={state.formData}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowLaytimeSummary ? (
          <Modal
            style={{ top: "2%" }}
            title="Laytime Summary"
            open={state.isShowLaytimeSummary}
            //   onOk={handleOk}
            onCancel={() => laytimeSummary(false)}
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <LaytimeSummary
                modalCloseEvent={() => laytimeSummary(false)}
                formData={state.formData}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowNewLayTime ? (
          <Modal
            style={{ top: "2%" }}
            title="New Laytime"
            open={state.isShowNewLayTime}
            // onOk={handleOk}
            onCancel={() => NewLayTime(false)}
            width="95%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <NewLaytime
                voyageData={state.formData}
                modalCloseEvent={() => NewLayTime(false)}
              />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowVoyageManagementReport ? (
          <Modal
            style={{ top: "2%" }}
            title="Reports"
            open={state.isShowVoyageManagementReport}
            //  onOk={handleOk}
            onCancel={() => openVoyageManagementReport(false)}
            width="95%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <VoyageManagementReport data={state.reportFormData} />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowEstimateReport ? (
          <Modal
            style={{ top: "2%" }}
            title="Estimate Detail Report"
            open={state.isShowEstimateReport}
            // onOk={handleOk}
            onCancel={() => EstimateReport(false)}
            width="95%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading">
              <TcovEstimateDetail data={state.reportFormData} />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowProfitLossReport ? (
          <Modal
            style={{ top: "2%" }}
            title="Voyage Profit & Loss Report"
            open={state.isShowProfitLossReport}
            //  onOk={handleOk}
            onCancel={() => profitLossReport(false)}
            width="95%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <ProfitLossReport data={state.reportFormData} />
            </Suspense>
          </Modal>
        ) : undefined}

        {state.isShowportExp ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Port Expenses"
            open={state.isShowportExp}
            // onOk={handleOk}
            onCancel={() =>
              setState((prevState) => ({ ...prevState, isShowportExp: false }))
            }
            width="90%"
            footer={null}
            maskClosable={false}
          >
            <Suspense fallback="Loading....">
              <BunkerPe
                isEdit={true}
                formData={{
                  ...state.formData,
                  vessel: state.estimateData?.fix?.vessel_name
                    ? state.estimateData.fix.vessel_name
                    : "",
                  voyage_manager_id: state.voyID,
                  port: state.port_name,
                  portid: state.portexpid,
                  arrival: state.Pe_arrival,
                  departure: state.pe_departure,
                }}
                modalCloseEvent={(showPEform) => handleclosePE(showPEform)}
              />
            </Suspense>
          </Modal>
        ) : undefined}


        {state.isBunker && (
          <Modal
            style={{ top: "2%" }}
            title=""
            open={state.isBunker}
            onCancel={() => setState((prev) => ({ ...prev, isBunker: false }))}
            width="80%"
            footer={null}
          >
            <SpotPrice />
          </Modal>
        )}


        {/* {state.isNoon && (
          <Modal
            style={{ top: "2%" }}
            title=""
            open={state.isNoon}
            onCancel={() => setState((prev) => ({ ...prev, isNoon: false }))}
            width="80%"
            footer={null}
          >
            <DynamicVspm />
          </Modal>
        )} */}


        {state.isMap && (
          <Modal
            style={{ top: "2%" }}
            title="Map Report"
            open={state.isMap}
            onCancel={() => setState((prev) => ({ ...prev, isMap: false }))}
            width="90%"
            footer={null}
          >
            <MapIntellegence />
          </Modal>
        )}

        {state.isweather && (
          <Modal
            style={{ top: "2%" }}
            title="Weather Report"
            open={state.isweather}
            onCancel={() => setState((prev) => ({ ...prev, isweather: false }))}
            width="90%"
            footer={null}
          >
            {/* <Map /> */}

            <iframe
              src="https://www.ventusky.com/?p=19.1;72.9;5&l=radar"
              title="Report Section"
              style={{ width: "100%", height: "100vh" }}
              frameborder="0"
              allowFullScreen={true}
            />
          </Modal>
        )}




      </div>
    </>
  );
};

export default VoyageOperation;
