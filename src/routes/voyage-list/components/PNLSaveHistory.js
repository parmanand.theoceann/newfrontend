import React, { useEffect } from "react";
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, useStateCallback } from "../../../shared";
import { Alert, Spin, Table } from "antd";
import { EyeFilled } from "@ant-design/icons";
import { Link } from "react-router-dom";

const PNLSaveHistory = ({ id }) => {
    const [state, setState] = useStateCallback({
        frmName: "pnl_save_history",
        tableData: {},
        showTable: false
    })


    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        setState((prevSate) => ({ ...prevSate, showTable: false }))
        try {
            let qParamString = objectToQueryStringFunc({ ae: id });

            let url = `${URL_WITH_VERSION}/voyage-manager/voyage-pnl/list?l=0&p=1&${qParamString}`;
            const response = await getAPICall(url);
            setState(
                (prevState) => ({
                    ...prevState,
                    tableData: response.data,
                }),
                () => {
                    setState((prev) => ({
                        ...prev,
                        showTable: true,
                    }));
                }
            );

        } catch (err) {
            console.log(err);
            setState((prevSate) => ({ ...prevSate, showTable: true }))
        }

    }

    const columns = [
        {
            title: 'SN.',
            dataIndex: 'sn',
            key: 'sn',
            render: (_, record, index) => (index + 1)
        },
        {
            title: 'Voyage No.',
            dataIndex: 'voyage_no',
            key: 'voyage_no',
        },
        {
            title: 'Date and Time',
            dataIndex: 'created_at',
            key: 'created_at',
        },
        {
            title: 'Saved By',
            dataIndex: 'user',
            key: 'user',
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (_, record) => (
                <Link target={"_blank"} to={record.scr_url}><EyeFilled style={{ fontSize: 'x-large' }} /></Link>
            )
        }
    ];
    const { showTable } = state;

    return (

        <>
            {
                showTable ? <Table
                    dataSource={state.tableData}
                    columns={columns}
                    rowClassName={(r, i) =>
                        i % 2 === 0
                            ? "table-striped-listing"
                            : "dull-color table-striped-listing"
                    }
                /> : <div className="col col-lg-12">
                    <Spin tip="Loading...">
                        <Alert message=" " description="Please wait..." type="info" />
                    </Spin>
                </div>
            }

        </>

    )

}

export default PNLSaveHistory;