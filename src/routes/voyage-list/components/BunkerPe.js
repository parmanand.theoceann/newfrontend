import React, { useState, useRef, useEffect } from "react";
import { Modal, Layout, Row, Col, notification } from "antd";
import NormalFormIndex from "../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  openNotificationWithIcon,
  postAPICall,
  getAPICall,
  apiDeleteCall,
  useStateCallback,
} from "../../../shared";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../../shared/attachments";
import Attachment from "../../../shared/components/Attachment";
import CreateInvoice from "../../create-invoice/CreateInvoice";
import Remarks from "../../../shared/components/Remarks";
import Tde from "../../tde/";
import InvoicePopup from "../../create-invoice/InvoicePopup";

import _ from "lodash";
import { DeleteOutlined, SaveOutlined, EditOutlined } from "@ant-design/icons";

const openNotification = (keyName) => {
  let msg = `Please save Port Expense Form first, and then click on ${keyName}`;

  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const { Content } = Layout;

const BunkerPe = (props) => {
  const [state, setState] = useStateCallback({
    frmName: "port_expenses",
    isRemarkModel: false,
    formData: {
      "-": [{}, {}],
      "--": [
        {
          act_tax: "",
          attchment: "",
          cost_item: "",
          diff: 0,
          fda_amount: 0,
          remark: "",
          total_agreed: 0,
          total_quoted: 0,
        },
      ],
      ".": [
        {
          agreed_adv: "",
          attachment: "",
          axct_tax: "",
          cost_item: "",
          diff: "",
          id: -9e6,
          quoted: 0,
          remark: "",
          short_code: "",
        },
      ],
      "..": {
        agreed_est_amt: "",
        diff: "",
        our_exchange: "",
        total_agreed_adv: 0,
        total_diff: 0,
        total_quoted: 0,
        total_tax: 0,
      },
      "...": {
        agent_exchange: "",
        diff: "",
        our_exchange: "",
        total_fda: "",
        total_fda_wex: 0,
      },
      arrival: props.formData && props.formData.arrival,
      departure: props.formData && props.formData.departure,
      port: props.formData && props.formData.port,
      portid: props.formData && props.formData.portid,
      vessel: props.formData && props.formData.vessel,
      vessel_code: props.formData && props.formData.vessel_code,
      voyage_manager_id: props.formData && props.formData.voyage_manager_id,
    },
    frmOptions: [],
    portexpid: props.id,
    reportData: null,
    popupdata: null,
    isShowAttachment: false,
    loadComponent: undefined,
  });
  const [frmVisible, setFrmVisible] = useState(true);
  const [modal, setModal] = useState({
    showTdeModal: false,
    showInvoiceModal: false,
    showPdaInvoiceModal: false,
    showFdaInvoiceModal: false,
    isShowPDAInvoice: false,
    isShowFDAInvoice: false,
  });

  let formdataref = useRef(null);

  useEffect(() => {
    editPortExpense();
  }, []);

  const editPortExpense = async () => {
    const { portexpid } = state;
    if (portexpid) {
      try {
        const editportexpense = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/port-expenses/edit?e=${portexpid}`
        );
        const resp = await editportexpense;
        const data = resp["data"];

        let _data = Object.assign({}, data);
        if (_data && _data.hasOwnProperty(".") && _data["."].length == 0) {
          _data["."] = [
            {
              agreed_adv: "",
              attachment: "",
              axct_tax: "",
              cost_item: "",
              diff: "",
              id: -9e6,
              quoted: 0,
              remark: "",
              short_code: "",
            },
          ];
        }
        if (_data && _data.hasOwnProperty("--") && _data["--"].length == 0) {
          _data["--"] = [
            {
              act_tax: "",
              attchment: "",
              cost_item: "",
              diff: 0,
              fda_amount: 0,
              remark: "",
              total_agreed: 0,
              total_quoted: 0,
            },
          ];
        }

        setState(
          (prevState) => ({ ...prevState, formData: Object.assign({}, _data) }),
          () => {
            setFrmVisible(true);
          }
        );
      } catch (err) {
        openNotificationWithIcon("error", "Something Went Wrong", 3);
      }
    } else {
      setFrmVisible(true);
    }
  };


  const reportAPI = async (show, data = {}) => {
    const { formData, reportData } = state;
    // let reportData = {};

    if (show === "showPdaInvoice") {
      if (Object.keys(data).length == 0) {
        try {
          const reponse = await getAPICall(
            `${URL_WITH_VERSION}/voyage-manager/port-expenses/pda-report?e=${formData.id}`
          );
          const respdata = await reponse["data"];
          if (respdata) {
            setState(
              (prevState) => ({
                ...prevState,
                popupdata: respdata,
                reportData: respdata,
              }),
              () =>
                setModal((prevState) => ({
                  ...prevState,
                  showPdaInvoiceModal: true,
                }))
            );
          } else {
            openNotificationWithIcon("error", "Unable to show invoices.", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "Something went wrong.", 5);
        }
      } else {
        setState((prevState) => ({
          ...prevState,
          reportData: { ...reportData, ...data },
        }));
      }
    } else if (show === "showFdaInvoice") {
      if (Object.keys(data).length == 0) {
        try {
          const reponse = await getAPICall(
            `${URL_WITH_VERSION}/voyage-manager/port-expenses/fda-report?e=${formData.id}`
          );
          const respdata = await reponse["data"];
          if (respdata) {
            setState(
              (prevState) => ({
                ...prevState,
                popupdata: respdata,
                reportData: respdata,
              }),
              () =>
                setModal((prevState) => ({
                  ...prevState,
                  showFdaInvoiceModal: true,
                }))
            );
          } else {
            openNotificationWithIcon("error", "Unable to show invoices.", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "Something went wrong.", 5);
        }
      } else {
        setState(
          (prevState) => ({
            ...prevState,
            reportData: { ...reportData, ...data },
          })
          // () => this.showHideModal(true, "isShowFDAInvoice")
        );
      }
    }
  };

  const handleok = (type) => {
    const { reportData } = state;

    if (type === "showPdaInvoice") {
      if (reportData["isSaved"]) {
        setModal((prevState) => ({
          ...prevState,
          showPdaInvoiceModal: false,
        }));
        setTimeout(() => {
          setModal((prevState) => ({
            ...prevState,
            isShowPDAInvoice: true,
          }));
        }, 2000);
        setState((prevState) => ({ ...prevState, reportData: reportData }));
      } else {
        openNotificationWithIcon(
          "info",
          "Please click on Save to generate invoice.",
          3
        );
      }
    } else if (type === "showFdaInvoice") {
      if (reportData["isSaved"]) {
        setModal((prevState) => ({
          ...prevState,
          showFdaInvoiceModal: false,
        }));
        setTimeout(() => {
          setModal((prevState) => ({
            ...prevState,
            isShowFDAInvoice: true,
          }));
        }, 2000);
        setState((prevState) => ({ ...prevState, reportData: reportData }));
      } else {
        openNotificationWithIcon(
          "info",
          "Please click on Save to generate invoice.",
          3
        );
      }
    }
  };

  const saveFormData = async (data) => {
    const { formData } = state;
    setFrmVisible(false);
    let _url = "save";
    let _method = "post";
    let fdaarr = [];

    if (formData.hasOwnProperty("id") && formData.id) {
      _url = "update";
      _method = "PUT";

      let pdaarr = [];
      data &&
        data["."] &&
        data["."].length > 0 &&
        data["."].map((val) => {
          const {
            agreed_adv,
            cost_item,
            diff,
            port_exp_id,
            short_code,
            axct_tax,
            attachment,
            remark,
            quoted,
            id = -9e6 + 1,
            ocd,
          } = val;

          if (cost_item && cost_item !== "") {
            pdaarr.push({
              agreed_adv,
              cost_item,
              diff,
              //port_exp_id,
              short_code,
              axct_tax,
              attachment,
              remark,
              quoted,
              id,
            });
          }
        });

      data["."] = pdaarr;
      data &&
        data["--"] &&
        data["--"].length > 0 &&
        data["--"].map((val, index) => {
          const {
            diff,
            total_quoted,
            total_agreed,
            act_tax,
            attchment,
            remark,
            cost_item,
            fda_amount,
            id = -9e6 + 1,
          } = val;

          if (cost_item && cost_item !== "") {
            fdaarr.push({
              diff,
              total_quoted,
              total_agreed,
              act_tax,
              attchment,
              remark,
              cost_item,
              fda_amount,
              id,
            });
          }
        });

      delete data["p_status_name"];
      delete data["fda_status_name"];
      delete data["vessel_id"];
    } else {
      let pdaarr = [];
      data &&
        data["."].length > 0 &&
        data["."].map((val) => {
          const {
            diff,
            quoted,
            agreed_adv,
            short_code,
            cost_item,
            axct_tax,
            attachment,
            remark,
          } = val;

          if (cost_item && cost_item !== "") {
            pdaarr.push({
              diff,
              quoted,
              agreed_adv,
              short_code,
              cost_item,
              axct_tax,
              attachment,
              remark,
            });
            fdaarr.push({
              diff,
              total_quoted: quoted,
              total_agreed: agreed_adv,
              act_tax: axct_tax,
              attchment: attachment,
              remark,
              cost_item,
              fda_amount: "0.00",
            });
          }
        });
      data["."] = pdaarr;

      data &&
        data["--"] &&
        data["--"].length > 0 &&
        data["--"].forEach((val, index) => {
          const {
            diff,
            total_quoted,
            total_agreed,
            act_tax,
            attchment,
            remark,
            cost_item,
            fda_amount,
          } = val;

          if (cost_item && cost_item !== "") {
            fdaarr.push({
              diff,
              total_quoted,
              total_agreed,
              act_tax,
              attchment,
              remark,
              cost_item,
              fda_amount,
            });
          }
        });
    }

    formdataref = _.cloneDeep(data);
    data["--"] = fdaarr;

    try {
      await postAPICall(
        `${URL_WITH_VERSION}/voyage-manager/port-expenses/${_url}`,
        data,
        _method,
        (data) => {
          if (data.data) {
            openNotificationWithIcon("success", data.message);
            if (data.row && data.row.id) {
              setState({ ...state, portexpid: data.row.id }, () => {
                editPortExpense();
              });
            } else {
              setState({ ...state, frmVisible: true });
              setTimeout(() => {
                props.modalCloseEvent(false);
              }, 4000);
            }
          } else {
            openNotificationWithIcon("error", data.message);
            setState({
              ...state,
              frmVisible: true,
              formData: Object.assign({}, formdataref),
            });
          }
        }
      );
    } catch (err) {
      openNotificationWithIcon("error", "Unable to Save.", 5);
    }
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Port Expense is not created. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => _onDelete(postData),
    });
  };

  const _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/delete?e=${postData.id}`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        setState({ ...state }, () => {
          setFrmVisible(true);
        });
        if (
          props.modalCloseEvent &&
          typeof props.modalCloseEvent === "function"
        ) {
          props.modalCloseEvent(false);
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const onClickExtraIcon = async (action, data) => {
    let groupKey = action["gKey"];
    let frm_code = "";

    if (groupKey == ".") {
      groupKey = ".";
      frm_code = "port_expense_pda_port_expense_adv";
    }
    if (groupKey == "--") {
      groupKey = "--";
      frm_code = "port_expense_fda";
    }

    let delete_id = data && data.id;
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
            editPortExpense();
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }

    setFrmVisible(true);
  };

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };

  const handleRemark = () => {
    setState(prevState => ({
      ...prevState,
      isRemarkModel: true,
    }));
  }

  return (
    <div className="tcov-wrapper full-wraps voyage-fix-form-wrap">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                      <div className="box-body">
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + state.frmName + "_0"}
                            formClass="label-min-height"
                            formData={state.formData}
                            showForm={true}
                            frmCode={state.frmName}
                            addForm={true}
                            frmOptions={state.frmOptions}
                            showToolbar={[
                              {
                                isLeftBtn: [
                                  {
                                    isSets: [
                                      {
                                        id: "3",
                                        key: "save",
                                        type: <SaveOutlined />,
                                        withText:
                                          state.formData && state.formData["id"]
                                            ? "Update"
                                            : "Save",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          saveFormData(data),
                                      },
                                      state.formData &&
                                      state.formData["id"] && {
                                        id: "4",
                                        key: "delete",
                                        type: <DeleteOutlined />,
                                        withText: "Delete",
                                        showToolTip: true,
                                        event: (key, data) => {
                                          data &&
                                            data.hasOwnProperty("id") &&
                                            data["id"] > 0
                                            ? _onDeleteFormData(data)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Please save a Port Expense First, then Delete.",
                                              3
                                            );
                                        },
                                      },
                                      {
                                        id: "5",
                                        key: "edit",
                                        type: <EditOutlined />,
                                        withText: "Remark",
                                        showToolTip: true,
                                        event: (key, data) =>{
                                          console.log('dataa',data)
                                          if (data?.row?.id || data?.id > 0 ) {
                                            handleRemark(data);
                                          } else {
                                            openNotificationWithIcon(
                                             "info",
                                              "Please save Invoice first",
                                              2
                                            );
                                          }
                                        },
                                      }
                                    ],
                                  },
                                ],

                                isRightBtn: [
                                  {
                                    isSets: [
                                      {
                                        key: "invoice",
                                        isDropdown: 1,
                                        withText: "Create Invoice",
                                        type: "",

                                        menus: [
                                          {
                                            href: null,
                                            icon: null,
                                            label: "PDA Invoice",
                                            modalKey: "pda-invoice",
                                            event: (key) =>
                                              state.formData &&
                                                state.formData.hasOwnProperty(
                                                  "id"
                                                ) &&
                                                state.formData["id"] > 0
                                                ? reportAPI(
                                                  "showPdaInvoice",
                                                  {}
                                                )
                                                : openNotification(
                                                  "PDA Invoice"
                                                ),
                                          },
                                          {
                                            href: null,
                                            icon: null,
                                            label: "FDA Invoice",
                                            modalKey: "fda-invoice",
                                            event: (key) =>
                                              state.formData &&
                                                state.formData.hasOwnProperty(
                                                  "id"
                                                ) &&
                                                state.formData["id"] > 0
                                                ? reportAPI(
                                                  "showFdaInvoice",
                                                  {}
                                                )
                                                : openNotification(
                                                  "FDA Invoice"
                                                ),
                                          },
                                        ],
                                      },

                                      {
                                        key: "tde",
                                        isDropdown: 0,
                                        withText: "TDE",
                                        type: "",
                                        menus: null,
                                        event: (key) =>
                                          state?.formData?.disburmnt_inv
                                            ? setModal((prevState) => ({ ...prevState, showTdeModal: true }))
                                            : openNotification("Tde"),
                                      },
                                      {
                                        key: "attachment",
                                        isDropdown: 0,
                                        withText: "Attachment",
                                        type: "",
                                        menus: null,
                                        event: (key, data) => {
                                          data &&
                                            data.hasOwnProperty("id") &&
                                            data["id"] > 0
                                            ? ShowAttachment(true)
                                            : openNotificationWithIcon(
                                              "info",
                                              "Please save Port Expense First.",
                                              3
                                            );
                                        },
                                      },
                                    ],
                                  },
                                ],
                              },
                            ]}
                            inlineLayout={true}
                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                            isShowFixedColumn={[".", "--"]}
                          />
                        ) : undefined}
                      </div>
                    </div>{" "}


                    {modal["showTdeModal"] && (
                      <Modal
                        className="page-container"
                        style={{ top: "2%" }}
                        title="TDE"
                        open={modal["showTdeModal"]}
                        onCancel={() =>
                          setModal((prevState) => ({
                            ...prevState,
                            showTdeModal: false,
                          }))
                        }
                        width="95%"
                        footer={null}
                      >
                        <Tde
                          invoiceType="portExpense"
                          invoiceNo={state.formData["disburmnt_inv"]}
                          formData={state.formData}
                        // isEdit={
                        //   TdeList != null && TdeList.id && TdeList.id > 0
                        //     ? true
                        //     : false
                        // }
                        // formData={TdeList}
                        // deleteTde={() => {}}
                        // saveUpdateClose={() =>
                        //   setModal((prevState) => ({
                        //     ...prevState,
                        //     showTdeModal: false,
                        //   }))
                        // }
                        />
                      </Modal>



                    )}





                    {modal["isShowPDAInvoice"] ? (
                      <Modal
                        style={{ top: "2%" }}
                        title="PDA Invoice"
                        open={modal["isShowPDAInvoice"]}
                        onCancel={() =>
                          setModal((prevState) => ({
                            ...prevState,
                            isShowPDAInvoice: false,
                          }))
                        }
                        width="95%"
                        footer={null}
                      >
                        <CreateInvoice
                          type={"pdaInvoice"}
                          PortExpensePDA={state.reportData}
                        />
                      </Modal>
                    ) : undefined}
                    {modal["showPdaInvoiceModal"] ? (
                      <Modal
                        style={{ top: "2%" }}
                        title="Invoice"
                        open={modal["showPdaInvoiceModal"]}
                        onCancel={() =>
                          setModal((prevState) => ({
                            ...prevState,
                            showPdaInvoiceModal: false,
                          }))
                        }
                        width="95%"
                        okText="Create PDF"
                        onOk={() => handleok("showPdaInvoice")}
                      >
                        <InvoicePopup
                          data={state.popupdata}
                          updatepopup={(data) =>
                            reportAPI("showPdaInvoice", data)
                          }
                        />
                      </Modal>
                    ) : undefined}
                    {modal["showFdaInvoiceModal"] ? (
                      <Modal
                        style={{ top: "2%" }}
                        title="Invoice"
                        open={modal["showFdaInvoiceModal"]}
                        onCancel={() =>
                          setModal((prevState) => ({
                            ...prevState,
                            showFdaInvoiceModal: false,
                          }))
                        }
                        width="95%"
                        okText="Create PDF"
                        onOk={() => handleok("showFdaInvoice")}
                      >
                        <InvoicePopup
                          data={state.popupdata}
                          updatepopup={(data) =>
                            reportAPI("showFdaInvoice", data)
                          }
                        />
                      </Modal>
                    ) : undefined}
                    {modal["isShowFDAInvoice"] ? (
                      <Modal
                        style={{ top: "2%" }}
                        title="FDA Invoice"
                        open={modal["isShowFDAInvoice"]}
                        onCancel={() =>
                          setModal((prevState) => ({
                            ...prevState,
                            isShowFDAInvoice: false,
                          }))
                        }
                        width="95%"
                        footer={null}
                      >
                        <CreateInvoice
                          type={"fdaInvoice"}
                          PortExpensePDA={state.reportData}
                        />
                      </Modal>
                    ) : undefined}
                    {state.isShowAttachment ? (
                      <Modal
                        style={{ top: "2%" }}
                        title="Upload Attachment"
                        open={state.isShowAttachment}
                        onCancel={() => ShowAttachment(false)}
                        width="50%"
                        footer={null}
                      >
                        {state.loadComponent}
                      </Modal>
                    ) : undefined}
                    {state.isRemarkModel && (
                      <Modal
                        width={600}
                        title="Remark"
                        open={state.isRemarkModel}
                        onOk={() => {
                          setState(prevState => ({ ...prevState, isRemarkModel: true }))
                        }}
                        onCancel={() => setState(prevState => ({ ...prevState, isRemarkModel: false }))}
                        footer={false}
                      >
                        <Remarks
                          remarksID={state.formData.voyage_manager_id}
                          remarkType="voyage-manager"
                          // idType="Bunker_no"
                        />


                      </Modal>
                    )}

                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default BunkerPe;
