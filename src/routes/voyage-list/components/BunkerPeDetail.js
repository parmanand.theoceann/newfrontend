import React, { Component } from "react";
import { Modal, Layout, Row, Col, notification } from "antd";
import NormalFormIndex from "../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  openNotificationWithIcon,
  postAPICall,
  getAPICall,
  apiDeleteCall,
} from "../../../shared";
import CreateInvoice from "../../create-invoice/CreateInvoice";
import Tde from "../../tde/Tde";
import InvoicePopup from "../../create-invoice/InvoicePopup";
import { DeleteOutlined, SaveOutlined } from "@ant-design/icons";

const openNotification = (keyName) => {
  let msg = `Please save Port Expense Form first, and then click on ${keyName}`;

  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const { Content } = Layout;
class BunkerPeDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      frmName: "port_expenses",
      modals: {
        InvoiceModal: false,
        TdeModal: false,
        showPdaInvoiceModal: false,
        showFdaInvoiceModal: false,
        isShowPDAInvoice: false,
        isShowFDAInvoice: false,
      },
      loadFrom: false,
      formData: this.props.formData || {},
      frmOptions: [],
      isShowPDAInvoice: false,
      isShowFDAInvoice: false,
      portexpid: this.props.id,
      reportData: null,
      popupdata: null,
      // showPdaInvoiceModal: false,
      // showFdaInvoiceModal: false
    };
  }

  componentDidMount = async () => {
    this.editPortExpense();
  };

  editPortExpense = async () => {
    const { portexpid } = this.state;
    if (portexpid) {
      try {
        const editportexpense = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/port-expenses/edit?e=${portexpid}`
        );
        const resp = await editportexpense;
        const data = resp["data"];
        this.setState(
          {
            ...this.state,
            formData: data,
          },
          () => {
            this.setState({ ...this.state, loadFrom: true });
          }
        );
      } catch (err) {
        openNotificationWithIcon("error", "Something Went Wrong", 3);
      }
    }
  };

  updateFormData = async (data) => {
    let type = "save";
    let suMethod = "POST";
    const { id } = this.state.formData;
    if (id) {
      type = "update";
      suMethod = "PUT";
    }

    let suURL = `${URL_WITH_VERSION}/voyage-manager/port-expenses/update`;

    let pdaarr = [];
    let fdaarr = [];
    data &&
      data["."] &&
      data["."].length > 0 &&
      data["."].map((val) => {
        const {
          agreed_adv,
          cost_item,
          diff,
          port_exp_id,
          short_code,
          axct_tax,
          attachment,
          remark,
          quoted,
          id=-9e6 + 1,
          ocd,
        } = val;
        pdaarr.push({
          agreed_adv,
          cost_item,
          diff,
          //port_exp_id,
          short_code,
          axct_tax,
          attachment,
          remark,
          quoted,
          id,
        });
        fdaarr.push({
          diff,
          total_quoted:quoted,
          total_agreed:agreed_adv,
          act_tax:axct_tax,
          attchment:attachment,
          remark,
          cost_item,
        })
      });

    data["."] = pdaarr;

    data &&
      data["--"] &&
      data["--"].length > 0 &&
      data["--"].map((val, index) => {
        if(fdaarr[index]){
          fdaarr[index]["id"] = val["id"] || -9e6 + index
          fdaarr[index]["fda_amount"] = val["fda_amount"] || 0
        }
        // delete val["index"];
        // delete val["key"];
        // delete val["editable"];
        // delete val["ocd"];
        // if(!val["id"]){
        //   val["id"] = -9e6 + 1
        // }
      });
    fdaarr.forEach(e => {
      if(!e.id) e.id =-9e6 + 1
    } ) 
    data["--"] = fdaarr
    delete data["p_status_name"];
    delete data["fda_status_name"];
    delete data["vessel_id"];
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        setTimeout(() => {
          this.props.modalCloseEvent(false);
        }, 2000);
      } else {
        openNotificationWithIcon("error", data.message);
      }
    });
  };

  _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Cargo Id is empty. Kindly check it again!"
      );
    }
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => this._onDelete(postData),
    });
  };

  _onDelete = (postData) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/port-expenses/delete?e=${
      postData.id
    }`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        this.setState({ ...this.state, loadFrom: false }, () => {
          this.setState({
            ...this.state,
            formData: this.formDataValue,
            loadFrom: true,
          });
        });
        if (this.props.modalCloseEvent &&
            typeof this.props.modalCloseEvent === "function"
          ) {
            this.props.modalCloseEvent(false);
          }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  reportAPI = async (show, data = {}) => {
    const { formData, reportData } = this.state;
    // let reportData = {};

    if (show === "showPdaInvoice") {
      if (Object.keys(data).length == 0) {
        try {
          const reponse = await getAPICall(
            `${URL_WITH_VERSION}/voyage-manager/port-expenses/pda-report?e=${
              formData.id
            }`
          );
          const respdata = await reponse["data"];
          if (respdata) {
            this.setState(
              {
                ...this.state,
                popupdata: respdata,
                reportData: respdata,
                // isShowPDAInvoice: true,
              },
              () => this.showHideModal(true, "showPdaInvoiceModal")
            );
          } else {
            openNotificationWithIcon("error", "Unable to show invoices.", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "Something went wrong.", 5);
        }
      } else {
        this.setState(
          {
            ...this.state,
            reportData: { ...reportData, ...data },
          }
          // () => this.showHideModal(true, "isShowPDAInvoice")
        );
      }
    } else if (show === "showFdaInvoice") {
      if (Object.keys(data).length == 0) {
        try {
          const reponse = await getAPICall(
            `${URL_WITH_VERSION}/voyage-manager/port-expenses/fda-report?e=${
              formData.id
            }`
          );
          const respdata = await reponse["data"];
          if (respdata) {
            this.setState(
              {
                ...this.state,
                // isShowFDAInvoice: true,
                reportData: respdata,
                popupdata: respdata,
              },
              () => this.showHideModal(true, "showFdaInvoiceModal")
            );
          } else {
            openNotificationWithIcon("error", "Unable to show invoices.", 5);
          }
        } catch (err) {
          openNotificationWithIcon("error", "Something went wrong.", 5);
        }
      } else {
        this.setState(
          {
            ...this.state,
            reportData: { ...reportData, ...data },
          }
          // () => this.showHideModal(true, "isShowFDAInvoice")
        );
      }
    }
  };

  tdeModalOpen = async (invoice_no, modal) => {
    const { formData, modelVisible } = this.state;
    let   target=null;
    let resp=null;
    let tdeData=null;

    if (invoice_no) {
      let _modal = {};
      _modal[modal] = true;
      let amount = 0;

      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      let respData = response["data"];

      const responseData = await getAPICall(
        `${URL_WITH_VERSION}/address/edit?ae=${formData.agent_full_name}`
      );
      const responseAddressData = responseData["data"];
      let account_no =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["account_no"]
          : "";
      let swift_code =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["swift_code"]
          : "";

      let voyage = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/edit?ae=${
          formData.voyage_manager_id
        }`
      );

      let voyageData = voyage["data"];

      if (respData && respData.length > 0) {
        target = respData.find(
          (item) => item.invoice_no === formData.disburmnt_inv
        );
        if (target && target.hasOwnProperty("id") && target["id"] > 0) {
          resp = await getAPICall(
            `${URL_WITH_VERSION}/tde/edit?e=${target["id"]}`
          );
        }
      }

      let accounting = [];
      if (target && resp && resp["data"] && resp["data"].hasOwnProperty("id")) {
        tdeData = resp["data"];

        if (formData["invoice_type"] == 209) {
          amount = `${formData["agreed_est_amt"]}`;
        } else if (formData["invoice_type"] == 208) {
          amount = `${formData["total_amt"]}`;
        } else if (formData["invoice_type"] == 210) {
          amount = `${formData["total_amt"]}`;
        }
     
        accounting = [
          {
            company: voyageData["my_company_lob"],
            vessel_name: voyageData["vessel_id"],
            voyage: formData["voyage_manager_id"],
            ap_ar_acct: account_no,
            vessel_code: formData["vessel_code"],
            amount: amount,
            description: `port-expense@${amount}`,
            lob: formData.company_lob,
            account: swift_code,
            port: formData["port"],
            id: -9e6,
          },
        ];

        tdeData["accounting"] = accounting;
        tdeData["--"] = { total: target["invoice_amount"] };

        let rem_data = resp["data"]["----"][0];

        tdeData["----"] = {
          total_due: rem_data["total_due"],
          total: rem_data["total"],
          remittance_bank: rem_data["remittance_bank"],
        };

        this.setState({
          ...this.state,
          modals: _modal,
          TdeList: Object.assign({}, tdeData),
        });
      } else {
        let frmData = {};
        if (formData["invoice_type"] == 209) {
          amount = `${formData["agreed_est_amt"]}`;
        } else if (formData["invoice_type"] == 208) {
          amount = `${formData["total_amt"]}`;
        } else if (formData["invoice_type"] == 210) {
          amount = `${formData["total_amt"]}`;
        }

        frmData = {
          bill_via: voyageData["my_company_lob"],
          invoice_no: formData["disburmnt_inv"],
          invoice_type: formData["invoice_type"],
          invoice_date:
            formData["invoice_type"] == 209
              ? formData["pda_inv_date"]
              : formData["fda_inv_date"],
          received_date:
            formData["invoice_type"] == 209
              ? formData["adavnce_sent_date"]
              : formData["fda_sent_date"],
          vessel: voyageData["vessel_id"],
          vendor: formData["agent_full_name"],
          inv_status:
            formData["invoice_type"] == 209
              ? formData["pda_adv_status"]
              : formData["fda_status"],
          invoice_amount: amount,
          account_base: amount,
          ar_pr_account_no: account_no,
          voyage_manager_id: voyageData["id"],
          po_number: formData["po_no"],
          accounting: [],
        };
        frmData["accounting"] = [
          {
            company: voyageData["my_company_lob"],
            vessel_name: voyageData["vessel_id"],
            voyage: formData["voyage_manager_id"],
            ap_ar_acct: account_no,
            vessel_code: formData["vessel_code"],
            amount: amount,
            description: `port-expense@${amount}`,
            lob: formData.company_lob,
            account: swift_code,
            port: formData["port"],
            id: -9e6,
          },
        ];
        this.setState({ ...this.state, modals: _modal, TdeList: frmData });
      }
    } else {
      this.setState({
        msg: `Without "Invoice No" you cant't access the TDE Form`,
        modelVisible: !modelVisible,
      });
    }
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    // if(modal == 'TdeModal'){
    //   this.setState({TdeList : null })
    // }
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  handleok = (type) => {
    const { reportData } = this.state;

    if (type === "showPdaInvoice") {
      if (reportData["isSaved"]) {
        //  this.showHideModal(false, "InvoicePopup");
        this.showHideModal(false, "showPdaInvoiceModal");
        setTimeout(() => {
          this.showHideModal(true, "isShowPDAInvoice");
        }, 2000);
        this.setState({ ...this.state, reportData: reportData });
      } else {
        openNotificationWithIcon(
          "info",
          "Please click on Save to generate invoice.",
          3
        );
      }
    } else if (type === "showFdaInvoice") {
      if (reportData["isSaved"]) {
        //  this.showHideModal(false, "InvoicePopup");
        this.showHideModal(false, "showFdaInvoiceModal");
        setTimeout(() => {
          this.showHideModal(true, "isShowFDAInvoice");
        }, 2000);
        this.setState({ ...this.state, reportData: reportData });
      } else {
        openNotificationWithIcon(
          "info",
          "Please click on Save to generate invoice.",
          3
        );
      }
    }
  };

  onClickExtraIcon = async (action, data) => {
    let groupKey = action["gKey"];
    let frm_code = "";

    if (groupKey == "pdas") {
      frm_code = "port_expense_pda_port_expense_adv";
    }
    if (groupKey == "--") {
      frm_code = "port_expense_fda";
    }

    let delete_id = data && data.id;
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
            // this.editPortExpense()
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
    this.setState({ loadFrom: true });
  };

  render() {
    const {
      formData,
      frmOptions,
      popupdata,

      TdeList,
      reportData,
      isShowFDAInvoice,
      isShowPDAInvoice,
      editportexpData,
      frmName,
      loadFrom,
      showPdaInvoiceModal,
    } = this.state;

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              {loadFrom ? (
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  showForm={true}
                  frmCode={frmName}
                  formData={formData}
                  frmOptions={frmOptions}
                  addForm={true}
                  inlineLayout={true}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        {
                          isSets: [
                            {
                              id: "3",
                              key: "save",
                              type: <SaveOutlined />,
                              withText: "Save",
                              showToolTip: true,
                              event: (key, data) => this.updateFormData(data),
                            },
                            {
                              id: "4",
                              key: "delete",
                              type: <DeleteOutlined />,
                              withText: "Delete",
                              showToolTip: true,
                              event: (key, data) =>
                                this._onDeleteFormData(data),
                            },
                          ],
                        },
                      ],
                      isRightBtn: [
                        {
                          isSets: [
                            {
                              key: "invoice",
                              isDropdown: 1,
                              withText: "Create Invoice",
                              type: "",

                              //event: (key) => this.invoiceModal(true),
                              menus: [
                                {
                                  href: null,
                                  icon: null,
                                  label: "PDA Invoice",
                                  modalKey: "pda-invoice",
                                  event: (key) =>
                                    formData &&
                                    formData.hasOwnProperty("id") &&
                                    formData["id"] > 0
                                      ? this.reportAPI("showPdaInvoice", {})
                                      : openNotification("PDA Invoice"),
                                },

                                {
                                  href: null,
                                  icon: null,
                                  label: "FDA Invoice",
                                  modalKey: "fda-invoice",
                                  event: (key) =>
                                    formData &&
                                    formData.hasOwnProperty("id") &&
                                    formData["id"] > 0
                                      ? this.reportAPI("showFdaInvoice", {})
                                      : openNotification("FDA Invoice"),
                                },
                              ],
                            },

                            {
                              key: "tde",
                              isDropdown: 0,
                              withText: "TDE",
                              type: "",
                              menus: null,
                              event: (key) =>
                                formData && formData["disburmnt_inv"]
                                  ? this.tdeModalOpen(
                                      formData["disburmnt_inv"],
                                      "TdeModal"
                                    )
                                  : openNotificationWithIcon(
                                      "error",
                                      "Something went wrong.",
                                      5
                                    ),
                            },
                          ],
                        },
                      ],
                    },
                  ]}
                  isShowFixedColumn={[".", "--"]}
                  tableRowDeleteAction={(action, data) =>
                    this.onClickExtraIcon(action, data)
                  }
                />
              ) : (
                undefined
              )}
            </div>
          </div>
        </article>

        {this.state.modals["TdeModal"] && (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="TDE"
           open={this.state.modals["TdeModal"]}
            onCancel={() => this.showHideModal(false, "TdeModal")}
            width="95%"
            footer={null}
          >
            <Tde
              invoiceType="portExpense"
              isEdit={
                TdeList != null && TdeList.id && TdeList.id > 0 ? true : false
              }
              formData={TdeList}
              deleteTde={() =>{}}
              saveUpdateClose={() => this.showHideModal(false, "TdeModal")}
            />
          </Modal>
        )}

        {this.state.modals["isShowPDAInvoice"] ? (
          <Modal
            style={{ top: "2%" }}
            title="PDA Invoice"
           open={this.state.modals["isShowPDAInvoice"]}
            onCancel={() => this.showHideModal(false, "isShowPDAInvoice")}
            width="95%"
            footer={null}
          >
            <CreateInvoice type={"pdaInvoice"} PortExpensePDA={reportData} />
          </Modal>
        ) : (
          undefined
        )}

        {this.state.modals["showPdaInvoiceModal"] ? (
          <Modal
            style={{ top: "2%" }}
            title="Invoice"
           open={this.state.modals["showPdaInvoiceModal"]}
            onCancel={() => this.showHideModal(false, "showPdaInvoiceModal")}
            width="95%"
            okText="Create PDF"
            onOk={() => this.handleok("showPdaInvoice")}
          >
            <InvoicePopup
              data={popupdata}
              updatepopup={(data) => this.reportAPI("showPdaInvoice", data)}
            />
          </Modal>
        ) : (
          undefined
        )}

        {this.state.modals["showFdaInvoiceModal"] ? (
          <Modal
            style={{ top: "2%" }}
            title="Invoice"
           open={this.state.modals["showFdaInvoiceModal"]}
            onCancel={() => this.showHideModal(false, "showFdaInvoiceModal")}
            width="95%"
            okText="Create PDF"
            onOk={() => this.handleok("showFdaInvoice")}
          >
            <InvoicePopup
              data={popupdata}
              updatepopup={(data) => this.reportAPI("showFdaInvoice", data)}
            />
          </Modal>
        ) : (
          undefined
        )}

        {this.state.modals["isShowFDAInvoice"] ? (
          <Modal
            style={{ top: "2%" }}
            title="FDA Invoice"
           open={this.state.modals["isShowFDAInvoice"]}
            onCancel={() => this.showHideModal(false, "isShowFDAInvoice")}
            width="95%"
            footer={null}
          >
            <CreateInvoice type={"fdaInvoice"} PortExpensePDA={reportData} />
          </Modal>
        ) : (
          undefined
        )}
      </div>
    );
  }
}

export default BunkerPeDetail;
