import React from 'react';
import { Modal, Table } from 'antd';
import CommonToolbarUI from '../../../components/ToolbarUI';
import VoyageOperation from './VoyageOperation';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
];

const responseData = [
  {
    key: '1',
    name: 'Mike',
    age: 32,
    address: '10 Downing Street',
  },
  {
    key: '2',
    name: 'John',
    age: 42,
    address: '10 Downing Street',
  },
];

class VoyageList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
    };
  }

  callOptions = evt => {
    if (evt === 'voyage-operation') {
      this.setState({
        visible: true,
      });
    }
  };

  onCancel = e => this.setState({ ...this.state, visible: false });

  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="p-b-15">
                <CommonToolbarUI
                  routeUrl={'voyage-toolbar'}
                  callback={e => this.callOptions(e)}
                ></CommonToolbarUI>
              </div>
              <div>
                <Table
                  rowKey="id"
                  className="inlineTable"
                  bordered
                  columns={columns}
                  size="small"
                  dataSource={responseData}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {this.state.visible === true ? (
          <Modal
            title="Voyage Operation"
           open={this.state.visible}
            width={1543}
            onCancel={this.onCancel}
            bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
            footer={null}
          >
            <VoyageOperation />
          </Modal>
        ) : (
          undefined
        )}
      </div>
    );
  }
}

export default VoyageList;
