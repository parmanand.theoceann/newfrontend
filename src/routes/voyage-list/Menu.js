import { DeleteOutlined, FilePdfOutlined, PlusOutlined, SaveOutlined, SearchOutlined } from "@ant-design/icons"

export const voyage_menu = [{
  isLeftBtn: [
    {
      isSets: [
        { id: "1", key: "add", type: <PlusOutlined />, withText: "" },
        { id: "2", key: "search", type: <SearchOutlined />, withText: "" },
        { id: "3", key: "save", type: <SaveOutlined />, withText: "" },
        { id: "4", key: "file", type: <FilePdfOutlined />, withText: "" },
        { id: "5", key: "delete", type: <DeleteOutlined />, withText: "" }
      ]
    },
  ],
  isRightBtn: [
    {
      isSets: [
        { key: "Estimate", isDropdown: 0, withText: "Estimate", type: "", menus: null },
        {
          key: "Freight", isDropdown: 1, withText: "Freight/Relet", type: "", menus: [
            { "href": null, "icon": null, "label": "Freight Invoice", modalKey: "" },
            { "href": null, "icon": null, "label": "Freight Invoice summary", modalKey: "" },
            { "href": null, "icon": null, "label": "Freight Commission Invoice", modalKey: "" },
            { "href": null, "icon": null, "label": "Freight Commission summary", modalKey: "" },
          ]
        },
        {
          key: "TC Commission", isDropdown: 1, withText: "TC Commission", type: "", menus: [
            { "href": null, "icon": null, "label": "Commission Summary", modalKey: "commission-payment" },
            { "href": null, "icon": null, "label": "Broker Commission Creation", modalKey: "" },
          ]
        },
        { key: "Other Rev/Exp", isDropdown: 1, withText: "Other Revenu Expenses", type: "", menus: null },
        {
          key: "Laytime", isDropdown: 1, withText: "Laytime", type: "", menus: [
            { "href": null, "icon": null, "label": "Create New Laytime", modalKey: "new_laytime" },
            { "href": null, "icon": null, "label": "Laytime Summary", modalKey: "" }
          ]
        },
        { key: "Delays", isDropdown: 1, withText: "Delays", type: "", menus: null },
        {
          key: "Bunkers", isDropdown: 1, withText: "Bunkers", type: "", menus: [
            { "href": null, "icon": null, "label": "Voyage bunker plan", modalKey: "voyage_bunker_plan", "event": (key) => { this.showVoyageBunkerPlan() } },
            { "href": null, "icon": null, "label": "New bunker requirement", modalKey: "new_bunker_requirement", "event": (key) => { this.shownewBunkerRequirement() } },
            { "href": null, "icon": null, "label": "Bunker requirement summary", modalKey: "bunker_requirement-summary", "event": (key) => this.BunkerRequirementSummary(true) },
            { "href": null, "icon": null, "label": "Port- bunker & activity", modalKey: "port_bunker_activity", "event": (key) => { this.showPortBunkerActivityDetails() } },
          ]
        },
        {
          key: "portcall", isDropdown: 1, withText: "Port Call", type: "", menus: [
            { "href": null, "icon": null, "label": "Create Agency Appointment", modalKey: "create_agency_appointment", "event": (key) => this.AgencyAppointment(true) },
            { "href": null, "icon": null, "label": "Saved appointment", modalKey: "saved_appointment" },
            { "href": null, "icon": null, "label": "My Portcall", modalKey: "myportcall", "event": (key) => { this.showMyPortcallDashboard() } },
            { "href": null, "icon": null, "label": "Port Expense Summary", modalKey: "port_expense_summary", "event": (key) => this.portExpenseSummary(true) },
            { "href": null, "icon": null, "label": "Port Information List", modalKey: "port_information_list", "event": (key) => { this.showPortInformationList() } },
          ]
        },
      ],
    },
  ],
  isResetOption: false
},
]