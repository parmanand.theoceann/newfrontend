import React from 'react';

const Page = () => {
  return (
    <div className="body-wrapper" >
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title">
                  <span>Open Positions</span>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </article>
    </div>
  );
}

export default Page;
