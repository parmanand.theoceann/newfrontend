import React, {  useEffect } from 'react';
import { Button, Table, Popconfirm, Modal } from 'antd';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, postAPICall, apiDeleteCall, openNotificationWithIcon, ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';

const PortsList = () => {

  const [state, setState] = useStateCallback({
    frmName: "port_information",
    visible: false,
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    selectedValue: "all",
    isVisible: false,
    formDataValues: {},
    sidebarVisible: false,
    isAdd: true,
    typesearch: {},
    donloadArray: []
  })

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => redirectToAdd(e, record.id)}>
              <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        )
      }
    };
    let tableHeaders = Object.assign([], FIELDS && FIELDS['ports-list'] ? FIELDS['ports-list']["tableheads"] : [])

    setState(prevState => ({ ...prevState, columns: tableHeaders }), () => {
      getTableData();
    });

  }, [])

  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;

    let qParams = { "t": "port", "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    let headers = { "order_by": { "id": "desc" } };
    let search = searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue') ? searchtype : state.typesearch;

    if (search && search.hasOwnProperty('searchValue') && search.hasOwnProperty('searchOptions') && search['searchOptions'] !== '' && search['searchValue'] !== '') {
      let wc = {}
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { "OR": {} };
        so.map(e => wc['OR'][e] = { "l": search['searchValue'] });
      } else {
        wc[search['searchOptions']] = { "l": search['searchValue'] }
      }

      headers['where'] = wc;
      state.typesearch = { 'searchOptions': search.searchOptions, 'searchValue': search.searchValue }

    }

    setState(prevState => ({
      ...prevState,
      loading: true,
      responseData: []
    }));


    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/port/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0; //total rows
    let dataArr = (data && data.data ? data.data : []); //page data
    let _state = { loading: false }
    let donloadArr = []
    if (dataArr.length > 0 && (totalRows > 0)) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      _state["responseData"] = dataArr;
    }

    setState(prevState => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: { pageIndex: pageOptions.pageIndex, pageLimit: pageOptions.pageLimit, totalRows: totalRows },
      loading: false
    }));

  }

  const redirectToAdd = async (e, id = null) => {
    let qParams = { "ae": id };
    let qParamString = objectToQueryStringFunc(qParams);

    if (id) {

      const response = await getAPICall(`${URL_WITH_VERSION}/port/edit?${qParamString}`);
      const respData = await response['data'];
      //setState({ ...state, isAdd: false, formDataValues: respData, isVisible: true }, () => setState({ ...state, isVisible: true }));
      setState(prevState => ({ ...prevState, isAdd: false, formDataValues: respData, isVisible: true }));
    } else {
      setState(prevState => ({ ...prevState, isAdd: true, isVisible: true }));
    }
  }

  const onCancel = () => {
    getTableData();
    setState(prevState => ({ ...prevState, isAdd: true, isVisible: false }));
  }

  const _onDeleteFormData = (data) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => onRowDeletedClick(data),
    });
  };

  const onRowDeletedClick = (data) => {
    if (data.id) {
      let _url = `${URL_WITH_VERSION}/port/delete`;
      apiDeleteCall(_url, { "id": data.id }, (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      })
    }
  }

  const oncallOptions = (evt) => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = state.pageOptions;
      let search = { 'searchOptions': evt['searchOptions'], 'searchValue': evt['searchValue'] };
      pageOptions['pageIndex'] = 1;

      setState(prevState => ({ ...prevState, search: search, pageOptions: pageOptions }), () => {
        getTableData(search);
      });


    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = state.pageOptions;
      pageOptions['pageIndex'] = 1;

      setState(prevState => ({ ...prevState, search: {}, pageOptions: pageOptions }), () => {
        getTableData();
      });

    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);;

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      // console.log(columns)

      setState(prevState => ({
        ...prevState,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
      }));

    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      setState(prevState => ({...prevState,pageOptions: pageOptions}), () => {
        getTableData()
      });
    }
  }

  const onSaveFormData = async (data) => {
    // let qParams = { "frm": "port_information" };
    // let qParamString = objectToQueryStringFunc(qParams);
    let { isAdd } = state;

    if (data && isAdd) {
      let _url = `${URL_WITH_VERSION}/port/save?frm=port_information`;
      await postAPICall(`${_url}`, data, 'post', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });

      setState(prevState => ({...prevState,isAdd: false, formDataValues: {},isVisible: false}));
    } else {
      setState(prevState => ({...prevState,isAdd: true,isVisible: true}));
    }
  }

  const onEditFormData = async (data) => {
    let { isAdd } = state;
    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/port/update?frm=port_information`;
      await postAPICall(`${_url}`, data, 'patch', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });
      
      setState(prevState => ({...prevState,isAdd: false,formDataValues: {},isVisible: false}));
    } else {
      setState(prevState => ({...prevState,isAdd: true,isVisible: true}));

    }
  }

  //resizing function
  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };
  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = state;



    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }

  const { columns, loading, responseData, pageOptions, search, isAdd, isVisible, formDataValues, frmName, sidebarVisible } = state;
  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true") ? true : false)
    .map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title"><span>Port List</span></h4>
              </div>
              {/* <div className="action-btn">
                  <Button type="primary" onClick={(e) => redirectToAdd(e)}>Add New Port</Button>
                </div> */}
            </div>
            <div className="section" style={{ width: '100%', marginBottom: '10px' }}>
              {
                loading === false ?
                  <ToolbarUI routeUrl={'ports-list-toolbar'} optionValue={{ 'pageOptions': pageOptions, 'columns': columns, 'search': search }} callback={(e) => oncallOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => onActionDonwload('csv', 'port') },
                      { title: 'PDF', event: () => onActionDonwload('pdf', 'port') },
                      { title: 'XLS', event: () => onActionDonwload('xlsx', 'port') }
                    ]}
                  />
                  : undefined
              }
            </div>
            <div>
              <Table
                // rowKey={record => record.id}
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                components={components}
                scroll={{ x: 'max-content' }}
                columns={tableColumns}
                // size="small"
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
              />
            </div>
          </div>
        </div>
      </article>

      {
        isVisible === true ?
          <Modal
            title={(isAdd === false ? "Edit" : "Add") + " Port Form"}
            open={isVisible}
            width='70%'
            onCancel={onCancel}
            style={{ top: '10px' }}
            bodyStyle={{ height: 600, overflowY: 'auto', padding: '0.5rem' }}
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default" style={{ padding: '15px' }}>
                  {
                    isAdd === true ?
                      <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height"
                        showForm={true} frmCode={frmName} addForm={true} showButtons={[
                          { "id": "cancel", "title": "Reset", "type": "danger" },
                          { "id": "save", "title": "Save", "type": "primary", "event": (data) => { onSaveFormData(data) } }
                        ]} inlineLayout={true}
                      />
                      :
                      <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formDataValues}
                        showForm={true} frmCode={frmName} addForm={true} showButtons={[
                          { "id": "cancel", "title": "Reset", "type": "danger" },
                          { "id": "save", "title": "Update", "type": "primary", "event": (data) => { onEditFormData(data) } }
                        ]} inlineLayout={true}
                        showToolbar={[
                          {
                            isLeftBtn: [
                              {
                                key: "s1",
                                isSets: [
                                  {
                                    id: '6',
                                    key: 'delete',
                                    type: <DeleteOutlined />,
                                    withText: 'Delete',
                                    showToolTip: true,
                                    event: (key, data) => _onDeleteFormData(data),
                                  },
                                ],
                              },
                            ],
                            isRightBtn: [],
                            isResetOption: false,
                          },
                        ]}
                      />
                  }
                </div>
              </article>
            </div>
          </Modal>
          : undefined
      }
      {/* column filtering show/hide */}
      {
        sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => oncallOptions(e)} /> : null
      }
    </div>
  )
}

export default PortsList;