import React, { Component } from 'react';
import ReportingMenu from './ReportingMenu';

import { Table, Select, Form, Input } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const columns = [
  {
    title: 'Type (MT)',
    dataIndex: 'type',
    width: 100,
  },

  {
    title: 'IFO',
    dataIndex: 'ifo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'VLSFO',
    dataIndex: 'vlsfo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'ULSFO',
    dataIndex: 'ulsfo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'LSMGO',
    dataIndex: 'lsmgo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'MGO',
    dataIndex: 'mgo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },
];
const data = [
  {
    key: '1',
    type: 'BROB',
  },
  {
    key: '5',
    type: 'L/D',
  },

  {
    key: '6',
    type: 'Debllast',
  },

  {
    key: '7',
    type: 'Idle',
  },

  {
    key: '8',
    type: 'Heat',
  },

  {
    key: '2',
    type: 'M/E Propulsion',
  },

  {
    key: '3',
    type: 'Manoever',
  },

  {
    key: '4',
    type: 'Generator',
  },

  {
    key: '9',
    type: 'Fuel Received',
  },

  {
    key: '10',
    type: 'Fuel Debunker',
  },

  {
    key: '11',
    type: 'Aux Exngine',
  },
];

class InPortCargoHandling extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>In Port Cargo Handling</span>
                  </h4>
                </div>
                <ReportingMenu />
              </div>

              <Form>
                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Main Information</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Vessel">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="IMO">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Time Zone">
                        <InputGroup compact>
                          <Select defaultValue="gmt" style={{ width: '40%' }}>
                            <Option value="gmt">GMT</Option>
                            <Option value="local">Local</Option>
                          </Select>
                          <Select defaultValue="plus" style={{ width: '30%' }}>
                            <Option value="plus">+</Option>
                            <Option value="minus">-</Option>
                          </Select>
                          <Input style={{ width: '30%' }} defaultValue="9H" />
                        </InputGroup>
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Call Sign">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Voyage No.">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Port Information</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Port">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Port ID">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Reporting Time">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Latitude">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="n" style={{ width: '40%' }}>
                            <Option value="n">N</Option>
                            <Option value="s">S</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Longtitute">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="e" style={{ width: '40%' }}>
                            <Option value="e">E</Option>
                            <Option value="w">W</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Next Port">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Port ID">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="ETA">
                        <Input type="date" size="default" defaultValue="" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Cargo Info</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Port activity">
                        <Select size="default" defaultValue="loading">
                          <Option value="loading">Loading</Option>
                          <Option value="discharging">Discharging</Option>
                        </Select>
                      </FormItem>

                      <FormItem {...formItemLayout} label="FW Recived">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="SLOP ROB">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Cargo ops start">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Ttl Cargo on board">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="FW consumed">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="SLOP Produced">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Cargo ops end">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Discharged qty">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="FW ROB">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Ballast Water">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>

                      <FormItem {...formItemLayout} label="Idle duration">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>
                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Vessel & Distance</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 1 Hr">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 2 Hr">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 3 Hr">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 4 Hr">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 1 KWHR">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 2 KWHR">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 3 KWHR">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Gen 4 KWHR">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                  </div>
                  <hr />
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Sea state">
                        <Select defaultValue="1" size="default">
                          <Option value="1">1</Option>
                          <Option value="2">2</Option>
                          <Option value="3">3</Option>
                          <Option value="4">4</Option>
                          <Option value="5">5</Option>
                          <Option value="6">6</Option>
                          <Option value="7">7</Option>
                          <Option value="8">8</Option>
                          <Option value="9">9</Option>
                        </Select>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Sea Direction">
                        <Select defaultValue="north" size="default">
                          <Option value="north">North</Option>
                          <Option value="south">South</Option>
                          <Option value="east">East</Option>
                          <Option value="west">West</Option>
                          <Option value="north-east">North-East</Option>
                          <Option value="east-west">East-West</Option>
                          <Option value="north-west">Noth-West</Option>
                          <Option value="south-east">South-East</Option>
                          <Option value="south-west">South-West</Option>
                        </Select>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Sea height">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Swell">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Swell Direction">
                        <Select defaultValue="north" size="default">
                          <Option value="north">North</Option>
                          <Option value="south">South</Option>
                          <Option value="east">East</Option>
                          <Option value="west">West</Option>
                          <Option value="north-east">North-East</Option>
                          <Option value="east-west">East-West</Option>
                          <Option value="north-west">Noth-West</Option>
                          <Option value="south-east">South-East</Option>
                          <Option value="south-west">South-West</Option>
                        </Select>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Swell Height">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Wind For">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Wind dire">
                        <Select defaultValue="north" size="default">
                          <Option value="north">North</Option>
                          <Option value="south">South</Option>
                          <Option value="east">East</Option>
                          <Option value="west">West</Option>
                          <Option value="north-east">North-East</Option>
                          <Option value="east-west">East-West</Option>
                          <Option value="north-west">Noth-West</Option>
                          <Option value="south-east">South-East</Option>
                          <Option value="south-west">South-West</Option>
                        </Select>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Mositure">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Air pressure">
                        <Input type="number" size="default" defaultValue="33.33" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <h4>Bunker</h4>
                    <Table
                      bordered
                      columns={columns}
                      dataSource={data}
                      pagination={false}
                      footer={false}
                    />
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <FormItem label="Remark">
                        <TextArea></TextArea>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Captain">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4"></div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="C/E">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="text-center">
                      <button className="btn ant-btn-primary mr-2">Download</button>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default InPortCargoHandling;
