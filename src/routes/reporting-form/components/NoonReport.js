import React, { Component } from 'react';
import { Spin, Alert } from 'antd';
import ReportingMenu from './ReportingMenu';
import NormalFormIndex from '../../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, { getAPICall, postAPICall, openNotificationWithIcon, objectToQueryStringFunc } from '../../../shared';

class NoonReport extends Component {
  
  constructor(props) {
    super(props);
    let id = -9e6;
    this.state = {
      frmVisible: false,
      frmName: 'noon_report_form',
      reportNo: this.props.reportNo || undefined,
      reportData : this.props.reportData != undefined ? this.props.reportData : {},
      showTopBar: (this.props.showTopBar === false ? false : true),
      formData: {
        'bunker': [
          { "type_mt": "BROB", "editable": true, "index": 0, "id": id, "key": 'table_bunker_0' },
          { "type_mt": 'M/E Propulsion', "editable": true, "index": 1, "id": id+1, "key": 'table_bunker_1' },
          { "type_mt": 'Manoever', "editable": true, "index": 2, "id": id+2, "key": 'table_bunker_2' },
          { "type_mt": 'Generator', "editable": true, "index": 3, "id": id+3, "key": 'table_bunker_3' },
          { "type_mt": 'Fuel Received', "editable": true, "index": 4, "id": id+4, "key": 'table_bunker_4' },
          { "type_mt": 'Fuel Debunker', "editable": true, "index": 5, "id": id+5, "key": 'table_bunker_5' },
          { "type_mt": 'Aux Exngine', "editable": true, "index": 6, "id": id+6, "key": 'table_bunker_6' },
        ]
      },
      InPopnpMode:
        this.props.InPopupMode != undefined ? this.props.InPopupMode : false,
    }
  }

  async componentDidMount() {
    let respData = undefined, response = undefined;
    if (this.state.reportNo) {
      let qParams = { ae: this.state.reportNo };
      let qParamString = objectToQueryStringFunc(qParams);
      response = await getAPICall(`${URL_WITH_VERSION}/noon-verification/edit?${qParamString}`);
      respData = await response;
      if (respData['data']) {
        let formData = Object.assign(this.state.formData, respData['data']);
        this.setState({ ...this.state, formData: formData, fullToQuick: false, showAddButton: false }, () =>
          this.setState({ ...this.state, frmVisible: true })
        );
      } else {
        openNotificationWithIcon('error', respData['message']);
        this.setState({ ...this.state, frmVisible: true })
      }
    } else {
      this.setState({ ...this.state, frmVisible: true })
    }
  }
  
  saveFormData = (postData) => {
    const { frmName } = this.state;
    let _url = "save";
    let _method = "post";
    if (postData.hasOwnProperty('id')) {
      _url = "update";
      _method = "put";
    }

    postData['report_types'] = 189; // Constant for Noon Report
    postAPICall(`${URL_WITH_VERSION}/noon-verification/${_url}?frm=${frmName}`, postData, _method, (data) => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        if (this.props.modalCloseEvent && typeof this.props.modalCloseEvent === 'function') {
          this.props.modalCloseEvent(data['row']);
        } 
        if (data.hasOwnProperty('row') && typeof data['row'] === 'number') {
          this.editMode(data['row']);
        }
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== "string") {
          Object.keys(dataMessage).map(i => msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>");
        } else {
          msg += dataMessage
        }

        msg += "</div>"
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />)
      }
    });
  }

  render() {
    const { frmName, formData, frmVisible, showTopBar,InPopnpMode } = this.state;

    let formData2 = Object.assign(formData, this.props.reportData != undefined ? this.props.reportData: {})
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              {
                showTopBar ?
                  <div className="form-wrapper">
                    <div className="form-heading">
                      <h4 className="title">
                        <span>Noon Report</span>
                      </h4>
                    </div>
                    {InPopnpMode ? null : <ReportingMenu />}
                  </div>
                : undefined
              }

              {
                frmVisible ?
                  <NormalFormIndex
                    key={'key_' + frmName + '_0'}
                    formClass="label-min-height"
                    formData={formData2}
                    showForm={true}
                    frmCode={frmName}
                    addForm={true}
                    inlineLayout={true}
                    showButtons={[
                      { "id": "cancel", "title": "Reset", "type": "danger" },
                      { "id": "save", "title": "Save", "type": "primary", "event": (data) => { this.saveFormData(data) } }
                    ]}
                  />
                : <div className="col col-lg-12">
                    <Spin tip="Loading...">
                      <Alert
                        message=" "
                        description="Please wait..."
                        type="info"
                      />
                    </Spin>
                  </div>
              }
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default NoonReport;
