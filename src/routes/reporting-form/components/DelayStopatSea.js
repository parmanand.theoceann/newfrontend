import React, { Component } from 'react';
import ReportingMenu from './ReportingMenu';
import { Table, Select, Form, Input } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const columns = [
  {
    title: 'Type (MT)',
    dataIndex: 'type',
    width: 100,
  },

  {
    title: 'IFO',
    dataIndex: 'ifo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'VLSFO',
    dataIndex: 'vlsfo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'ULSFO',
    dataIndex: 'ulsfo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'LSMGO',
    dataIndex: 'lsmgo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'MGO',
    dataIndex: 'mgo',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },
];
const data = [
  {
    key: '1',
    type: 'BROB',
  },

  {
    key: '2',
    type: 'M/E Propulsion',
  },

  {
    key: '3',
    type: 'Manoever',
  },

  {
    key: '4',
    type: 'Generator',
  },

  {
    key: '5',
    type: 'L/D',
  },

  {
    key: '6',
    type: 'Debllast',
  },

  {
    key: '7',
    type: 'Idle/On',
  },

  {
    key: '8',
    type: 'Heat',
  },

  {
    key: '9',
    type: 'Fuel Received',
  },

  {
    key: '10',
    type: 'Fuel Debunker',
  },

  {
    key: '11',
    type: 'Aux Exngine',
  },
];

class DelayStopatSea extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Delay-Stop at Sea</span>
                  </h4>
                </div>

                <ReportingMenu />
              </div>
              <Form>
                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Main Information</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Vessel">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="IMO">
                        <InputGroup compact>
                          <Input
                            size="default"
                            defaultValue=""
                            placeholder="IMO"
                            style={{ width: '50%' }}
                          />
                          <Input
                            size="default"
                            defaultValue=""
                            placeholder="Voyage No."
                            style={{ width: '50%' }}
                          />
                        </InputGroup>
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Time Zone">
                        <InputGroup compact>
                          <Select defaultValue="gmt" style={{ width: '40%' }}>
                            <Option value="gmt">GMT</Option>
                            <Option value="local">Local</Option>
                          </Select>
                          <Select defaultValue="plus" style={{ width: '30%' }}>
                            <Option value="plus">+</Option>
                            <Option value="minus">-</Option>
                          </Select>
                          <Input style={{ width: '30%' }} defaultValue="9H" />
                        </InputGroup>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Delay start time">
                        <Input size="default" type="date" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Lat">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="n" style={{ width: '40%' }}>
                            <Option value="n">N</Option>
                            <Option value="s">S</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Long">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="n" style={{ width: '40%' }}>
                            <Option value="n">E</Option>
                            <Option value="s">W</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Delay start End">
                        <Input size="default" type="date" />
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Lat">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="n" style={{ width: '40%' }}>
                            <Option value="n">N</Option>
                            <Option value="s">S</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Long">
                        <InputGroup compact>
                          <Input style={{ width: '60%' }} placeholder="DDMMSS" />
                          <Select defaultValue="n" style={{ width: '40%' }}>
                            <Option value="n">E</Option>
                            <Option value="s">W</Option>
                          </Select>
                        </InputGroup>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Duration">
                        <Input size="default" placeholder="DD HH MM" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Miles">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Stopage reaosn">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <h4>Bunker Consumption</h4>
                    <Table
                      bordered
                      columns={columns}
                      dataSource={data}
                      pagination={false}
                      footer={false}
                    />
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <FormItem label="Remark">
                        <TextArea></TextArea>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Captain">
                        <Input size="default" />
                      </FormItem>
                    </div>

                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="text-center">
                      <button className="btn btn-primary mr-2">Download</button>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default DelayStopatSea;
