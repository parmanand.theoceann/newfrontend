import React, { Component } from 'react';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
const reportingformdropdown = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/reporting-form">
        Departure/COSP
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/arrival-eosp">
        Arrival/EOSP
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/noon-report">
        Noon Report
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/bunker-handling">
        Bunker Handling
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/delay-stop-at-sea">
        Delay-Stop at Sea
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/inport-cargo-handling">
        In Port Cargo Handling
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/sof">
        Sof
      </a>
    </Menu.Item>
  </Menu>
);
class ReportingMenu extends Component {
  render() {
    return (
      <div className="toolbar-ui-wrapper">
        <div className="rightsection">
          <span className="wrap-bar-menu">
            <ul className="wrap-bar-ul">
              <li><a href="#/dynamic-vspm">Back</a></li>
              <li>
                <Dropdown overlay={reportingformdropdown}>
                  <span className="ant-dropdown-link text-dark" onClick={e => e.preventDefault()}>
                    Report Type <DownOutlined />
                  </span>
                </Dropdown>
              </li>
            </ul>
          </span>
        </div>
      </div>
    );
  }
}

export default ReportingMenu;
