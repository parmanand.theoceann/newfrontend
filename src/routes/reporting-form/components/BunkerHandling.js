import React, { Component } from 'react';
import ReportingMenu from './ReportingMenu';
import { Table, Select, Form, Input } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const columns = [
  {
    title: 'Fuel Type',
    dataIndex: 'f_type',
    render: () =>
      <Select size="default" value="ifo" >
        <Option value="ifo">IFO</Option>
        <Option value="lso">LSO</Option>
        <Option value="mgo">MGO</Option>
        <Option value="lsg">LSG</Option>
      </Select>

  },

  {
    title: 'Quantity Lifted',
    dataIndex: 'qtylifted',
    render: () => <Input type="number" size="default" defaultValue="33.33" />
  },


];
const data = [
  {
    key: '1',
  },




];


class BunkerHandling extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Bunker Handling</span>
                  </h4>
                </div>

                <ReportingMenu />
              </div>
              <Form>
                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Main Information</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Vessel">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="IMO">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Call Sign">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Time Zone">
                        <InputGroup compact>
                          <Select defaultValue="gmt" style={{ width: '40%' }}>
                            <Option value="gmt">GMT</Option>
                            <Option value="local">Local</Option>
                          </Select>
                          <Select defaultValue="plus" style={{ width: '30%' }}>
                            <Option value="plus">+</Option>
                            <Option value="minus">-</Option>
                          </Select>
                          <Input style={{ width: '30%' }} defaultValue="9H" />
                        </InputGroup>
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Port">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Port ETD">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Voyage No.">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Supplier">
                        <Select size="default" defaultValue="select1">
                          <Option value="select1">Select</Option>
                          <Option value="Option">Option</Option>
                        </Select>
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Bunkering Com.">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Hose con.">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Hose dis.">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Bunkering Com.">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-12">
                      <FormItem label="Remark">
                        <TextArea></TextArea>
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <Table
                      bordered
                      columns={columns}
                      dataSource={data}
                      pagination={false}
                      footer={false}
                    />
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <FormItem label="Remark">
                        <TextArea></TextArea>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Captain">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4"></div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="C/E">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="text-center">
                      <button className="btn ant-btn-primary mr-2">Download</button>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default BunkerHandling;




