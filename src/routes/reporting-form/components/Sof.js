import React, { Component } from 'react';
import ReportingMenu from './ReportingMenu';

import { Table, Select, Form, Input, Button } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const columns = [
  {
    title: 'Type (MT)',
    dataIndex: 'type',
    width: 100,
  },

  {
    title: 'Last ROB',
    dataIndex: 'lastrob',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'Received',
    dataIndex: 'received',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'Consumption',
    dataIndex: 'consumption',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },

  {
    title: 'Final ROB',
    dataIndex: 'finalrob',
    render: () => <Input type="number" size="default" defaultValue="33.33" />,
  },
];
const data = [
  {
    key: '1',
    type: 'IFO',
  },

  {
    key: '2',
    type: 'VLSFO',
  },

  {
    key: '3',
    type: 'ULSFO',
  },

  {
    key: '4',
    type: 'LSMGO',
  },

  {
    key: '5',
    type: 'MGO',
  },
];

const columns2 = [
  {
    title: 'Activity',
    dataIndex: 'activity',
    width: 100,
  },

  {
    title: 'Cargo',
    dataIndex: 'cargo',
  },

  {
    title: 'Date/time',
    dataIndex: 'datetime',
  },

  {
    title: 'Action',
    dataIndex: 'action',
  },
];

const data2 = [
  {
    key: '1',
    activity: 'Activity',
    cargo: 'Cargo',
    datetime: 'Date/Time',
    action: 'Action',
  },

  {
    key: '2',
    activity: 'Activity',
    cargo: 'Cargo',
    datetime: 'Date/Time',
    action: 'Action',
  },

  {
    key: '3',
    activity: 'Activity',
    cargo: 'Cargo',
    datetime: 'Date/Time',
    action: 'Action',
  },

  {
    key: '4',
    activity: 'Activity',
    cargo: 'Cargo',
    datetime: 'Date/Time',
    action: 'Action',
  },

  {
    key: '5',
    activity: 'Activity',
    cargo: 'Cargo',
    datetime: 'Date/Time',
    action: 'Action',
  },
];

class Sof extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>SOF</span>
                  </h4>
                </div>

                <ReportingMenu />
              </div>
              <Form>
                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <h4>Main Information</h4>
                    </div>
                  </div>
                  <div className="row p10">
                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Vessel">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="IMO">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Call Sign">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Time Zone">
                        <InputGroup compact>
                          <Select defaultValue="gmt" style={{ width: '40%' }}>
                            <Option value="gmt">GMT</Option>
                            <Option value="local">Local</Option>
                          </Select>
                          <Select defaultValue="plus" style={{ width: '30%' }}>
                            <Option value="plus">+</Option>
                            <Option value="minus">-</Option>
                          </Select>
                          <Input style={{ width: '30%' }} defaultValue="9H" />
                        </InputGroup>
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Port">
                        <Input size="default" />
                      </FormItem>
                    </div>
                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Port ETD">
                        <Input type="date" size="default" />
                      </FormItem>
                    </div>
                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Port activity">
                        <Select defaultValue="select" size="default">
                          <Option value="select">Select</Option>
                          <Option value="option">Option</Option>
                        </Select>
                      </FormItem>
                    </div>

                    <div className="col-md-3">
                      <FormItem {...formItemLayout} label="Voyage No.">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <Table
                      bordered
                      columns={columns2}
                      dataSource={data2}
                      pagination={false}
                      footer={() =>
                        <div className="text-center">
                          <Button type="link">Add New</Button>
                        </div>
                      }
                    />
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <h4>Bunker Consumption</h4>
                    <Table
                      bordered
                      columns={columns}
                      dataSource={data}
                      pagination={false}
                      footer={false}
                    />
                  </div>
                </div>

                <div className="border p-2 rounded mb-3">
                  <div className="row p10">
                    <div className="col-md-12">
                      <FormItem label="Remark">
                        <TextArea></TextArea>
                      </FormItem>
                    </div>
                  </div>

                  <div className="row p10">
                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="Captain">
                        <Input size="default" />
                      </FormItem>
                    </div>

                    <div className="col-md-4"></div>

                    <div className="col-md-4">
                      <FormItem {...formItemLayout} label="C/E">
                        <Input size="default" />
                      </FormItem>
                    </div>
                  </div>
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="text-center">
                      <button className="btn ant-btn-primary mr-2">Download</button>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default Sof;
