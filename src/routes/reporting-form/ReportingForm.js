import React, { Component } from "react";
import ReportingMenu from "./components/ReportingMenu";
import Departure from "./components/Departure";

class ReportingForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      InPopnpMode:
        this.props.InPopupMode != undefined ? this.props.InPopupMode : false,
    };
  }
  render() {
    const { InPopnpMode } = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Departure/COSP</span>
                  </h4>
                </div>
                {InPopnpMode ? null : <ReportingMenu />}
              </div>
              <Departure reportData={this.props.reportData} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ReportingForm;
