import React, { Component } from 'react';
import { Checkbox } from 'antd';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {

    }

    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }

  }
  render() {

    const { formReportdata } = this.state
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">
                    <img className='reportlogo' src={formReportdata.logo} alt="No Img"/></span>
                  <p className="sub-title m-0">{formReportdata.full_name}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata.address}</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Company (Full Name) :</td>
                      <td>{formReportdata.full_name}</td>

                      <td className="font-weight-bold">Company Short Name :</td>
                      <td>{formReportdata.short_name}</td>

                      <td className="font-weight-bold">Currency :</td>
                      <td>{formReportdata.accountinformation[0]["currency_name"]?formReportdata.accountinformation[0]["currency_name"]:"N/A"}</td> 
                  
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Country :</td>
                      <td>{formReportdata.country}</td>

                      <td className="font-weight-bold">Country Code :</td>
                      <td>{formReportdata.country_code}</td>

                      <td className="font-weight-bold">Phone :</td>
                      <td>{formReportdata['-'] && formReportdata['-'].phone_number ? formReportdata['-'].phone_number : ""}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Fax :</td>
                      <td>{formReportdata['-'].fax_number}</td>

                      <td className="font-weight-bold">Telex Number :</td>
                      <td>{formReportdata['-'].telex_number}</td>

                      <td className="font-weight-bold">Email :</td>
                      <td>{formReportdata['-'].email}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Web Page :</td>
                      <td>{formReportdata['-'].web_page}</td>

                      <td className="font-weight-bold">Main Contact:</td>
                      <td>{formReportdata['-'].main_contact}</td>

                    
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Credit Terms :</td>
                      <td>{formReportdata.credit_terms_name?formReportdata.credit_terms_name:"N/A"}</td>

                      <td className="font-weight-bold">Credit Line :</td>
                      <td>{formReportdata['.'].credit_line}</td>

                      <td className="font-weight-bold">VAT No :</td>
                      <td>{formReportdata['.'].vat_no}</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Company Type</h4>

                <table className="table border-table table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("PA") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        }
                        Port Agent
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("PS") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Service Provider
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("C") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Charterer
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("B") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } All Broker
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("RR") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Receiver
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("BS") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Bunker Supplier
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("W") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } My Company
                      </td>
                    </tr>

                    <tr>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("Z") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Own Company Branch
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("SH") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Shipper ( Supplier )
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("OW") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        } Ship Owner
                      </td>
                     
                      <td>
                        {formReportdata.companytype.hasOwnProperty("M") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        }Ship Management
                      </td>
                      <td>
                        {formReportdata.companytype.hasOwnProperty("T") ?
                          <Checkbox checked="true" />
                          :
                          <Checkbox />
                        }Misc. Company
                      </td>
                    </tr>
                  </tbody>
                </table>

                <h4>Bank & Account Details</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Benificiary Name</th>
                      <th>Benificiary Bank</th>
                      <th>Branch</th>
                      <th>Acc. No.</th>
                      <th>Swift Code</th>
                      <th>Correspondent Bank</th>
                      <th>CB Swift Code</th>
                      <th>IBAN</th>
                      <th>International Bank Name</th>
                      <th>Country</th>
                      <th>Payment Method</th>
                      <th>Account Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata["bank&accountdetails"] && formReportdata["bank&accountdetails"].length > 0 ? formReportdata["bank&accountdetails"].map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.benificiary_name}</td>
                            <td>{e.benificiary_bank_name?e.benificiary_bank_name:"N/A"}</td>
                            <td>{e.branch}</td>
                            <td>{e.account_no}</td>
                            <td>{e.swift_code}</td>
                            <td>{e.correspondent_bank_name?e.correspondent_bank_name:"N/A"}</td>
                            <td>{e.cb_swift_code}</td>
                            <td>{e.iban}</td>
                            <td>{e.ib_name}</td>
                            <td>{e.b_country_name?e.b_country_name:"N/A"}</td>
                            <td>{e.pay_method}</td>
                            <td>{e.account_type_name?e.account_type_name:"N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }


                  </tbody>
                </table>

                <h4>Contacts</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Director No</th>
                      <th>Fax No</th>
                      <th>Home No</th>
                      <th>Mobile No</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.contacts && formReportdata.contacts.length > 0 ? formReportdata.contacts.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.company_name}</td>
                            <td>{e.director_no}</td>
                            <td>{e.fax_no}</td>
                            <td>{e.home_no}</td>
                            <td>{e.mobile_no}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }
                  </tbody>
                </table>

                <h4>Additional Information</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.additionalinformation && formReportdata.additionalinformation.length > 0 ? formReportdata.additionalinformation.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.ai_name?e.ai_name:"N/A"}</td>
                            <td>{e.ai_description?e.ai_description:"N/A"}</td>
                          </tr>
                        </>
                      )
                     
                    }) : undefined

                    }


                  </tbody>
                </table>

                <h4>Sub Company</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Short Code</th>
                      <th>Company Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.subcompany && formReportdata.subcompany.length > 0 ? formReportdata.subcompany.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.short_code}</td>
                            <td>{e.company_name?e.company_name:"N/A"}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }


                  </tbody>
                </table>

                <h4>Account Information</h4>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Currency</th>
                      <th>Acct. No.</th>
                      <th>Swift Code</th>
                      <th>Correspondent Bank</th>
                      <th>CB Swift Code</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.accountinformation && formReportdata.accountinformation.length > 0 ? formReportdata.accountinformation.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.currency_name?e.currency_name:"N/A"}</td>
                            <td>{e.acc_no}</td>
                            <td>{e.swift_code}</td>
                            <td>{e.cor_bank_name?e.cor_bank_name:"N/A"}</td>
                            <td>{e.cb_s_code}</td>
                          </tr>
                        </>
                      )
                    }) : undefined

                    }


                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class NewCompanyReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }
  printDocument() {
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save("download.pdf");
      });
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={this.printDocument}>
                        Download
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                         <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default NewCompanyReport;
