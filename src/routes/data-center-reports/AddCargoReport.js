import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";


class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {};

    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.response || {}),
    };
  }
  render() {
    const { formReportdata } = this.state;
   
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Full Name :</td>
                      <td>{formReportdata.full_name}</td>

                      <td className="font-weight-bold">Short Name :</td>
                      <td>{formReportdata.short_name}</td>

                      <td className="font-weight-bold">Cargo Group :</td>
                      <td>{formReportdata.group}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">API :</td>
                      <td>{formReportdata.c_api}</td>

                      <td className="font-weight-bold">SG :</td>
                      <td>{formReportdata.sg}</td>
{/* 
                      <td className="font-weight-bold">Group Description :</td>
                      <td>Value</td> */}
                         <td className="font-weight-bold">Cargo Type :</td>
                      <td>{formReportdata.cargo_type}</td>

                    </tr>
                 

                    <tr>
                      
                    {/* <td className="font-weight-bold">Cargo Type :</td>
                      <td>Value</td> */}
                      <td className="font-weight-bold">Cargo Class :</td>
                      <td>{formReportdata.class}</td>

                      <td className="font-weight-bold">UN Number :</td>
                      <td>{formReportdata.un_number}</td>

                      <td className="font-weight-bold">Class :</td>
                      <td>{formReportdata.class_2}</td>
                    </tr>

                    <tr>
                     

                      <td className="font-weight-bold">IMO Name :</td>
                      <td>{formReportdata.imo_name}</td>

                      <td className="font-weight-bold">Product Code :</td>
                      <td>{formReportdata.product_code}</td>

                      <td className="font-weight-bold">IBC code :</td>
                      <td>{formReportdata.ibc_code}</td>
                    </tr>

                    <tr>
                     

                      <td className="font-weight-bold">Default CP Unit :</td>
                      <td>{formReportdata.default_cp_unit}</td>

                      

                      <td className="font-weight-bold">IMSBC code :</td>
                      <td>{formReportdata.imsbc_code}</td>

                      <td className="font-weight-bold">Pre Clearance for US and Canada :</td>
                      <td>{formReportdata.pre_clearance_uscn}</td>
                    </tr>

                    <tr>
                      {/* <td className="font-weight-bold">Bill by :</td>
                      <td>Value</td> */}
{/* 
                      <td className="font-weight-bold">Description :</td>
                      <td>Value</td> */}

                     
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Dangerous :</td>
                      <td>{formReportdata.dangerous}</td>

                      {/* <td className="font-weight-bold">Inactive :</td>
                      <td>Value</td> */}

                      <td className="font-weight-bold">Special Handling Required :</td>
                      <td>{formReportdata.special_handling}</td>
                      <td className="font-weight-bold"></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class AddCargoReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }  

  printReceipt() {
    window.print();
  }

  printDocument() {
    htmlToImage
      .toPng(document.getElementById("divToPrint"), { quality: 0.95 })
      .then(function(dataUrl) {
        var link = document.createElement("a");
        link.download = "my-image-name.jpeg";
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, "PNG", 0, 0, pdfWidth, pdfHeight);
        pdf.save("Add_Cargo_Report.pdf");
      });
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      {/* <li>
                        <span className="text-bt"> Download</span>
                      </li> */}
                 <li onClick={this.printDocument}>Download</li>

                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                             <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint response={this.props.response} ref={el => (this.componentRef = el)} />
              
           
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default AddCargoReport;
