import React, { Component } from 'react';
 import {PrinterOutlined} from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
               <img  className="reportlogo" src={""} alt="No Img"/>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Port Name :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Port Type :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Country :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Time Zone Code :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">STD :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">U.N. Code :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">DST :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Latitude :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Longitude :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Bunkering Port :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Single Berth :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Waterway Port :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Remarks :</td>
                      <td>Value</td>

                      <td colSpan="6"></td>
                    </tr>
                  </tbody>
                </table>

                <h4>Berth Information</h4>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Short Name</th>
                      <th>Full Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class PortReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }
  printDocument() {
    htmlToImage
      .toPng(document.getElementById("divToPrint"), { quality: 0.95 })
      .then(function(dataUrl) {
        var link = document.createElement("a");
        link.download = "my-image-name.jpeg";
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, "PNG", 0, 0, pdfWidth, pdfHeight);
        pdf.save("Port_Report.pdf");
      });
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      
                        <li onClick={this.printDocument}>Download</li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortReport;
