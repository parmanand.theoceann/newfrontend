import React, { Component } from "react";
import { Icon } from "antd";
import ReactToPrint from "react-to-print";
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {};

    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    };
  }
  render() {
    const { formReportdata } = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">{formReportdata.logo}</span>
                  <p className="sub-title m-0">{formReportdata.full_name}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata.address}</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Vessel Name :</td>
                      <td>{formReportdata.vessel_name}</td>

                      <td className="font-weight-bold">Vessel Code :</td>
                      <td>{formReportdata.vessel_code}</td>

                      <td className="font-weight-bold">Vessel DWT :</td>
                      <td>{formReportdata.vessel_dwt}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Type Code :</td>

                      <td>
                        {formReportdata.type_code
                          ? formReportdata.type_code
                          : "N/A"}
                      </td>

                      <td className="font-weight-bold">Year Built :</td>
                      <td>{formReportdata.year_built}</td>

                      <td className="font-weight-bold">SW Summer DWT :</td>
                      <td>{formReportdata.sw_summer_draft}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Ownership :</td>
                      <td>
                        {formReportdata.owner_ship_name
                          ? formReportdata.owner_ship_name
                          : "N/A"}
                      </td>

                      <td className="font-weight-bold">Vessel Type :</td>
                      <td>{formReportdata.vessel_type_name}</td>

                      <td className="font-weight-bold">TPC :</td>
                      <td>{formReportdata.tcp}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">IMO No :</td>
                      <td>{formReportdata.imo_no}</td>

                      <td className="font-weight-bold">Vessel Fleet :</td>
                      <td>{formReportdata.vessel_fleet}</td>

                      <td className="font-weight-bold">Capacity (Bail) :</td>
                      <td>{formReportdata.capacity_bale}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Daily Cost :</td>
                      <td>{formReportdata.daily_cost}</td>

                      <td className="font-weight-bold">Trade Area :</td>
                      <td>{formReportdata.trade_area_name}</td>

                      <td className="font-weight-bold">Capacity (Grain) :</td>
                      <td>{formReportdata.capacity_grain}</td>
                    </tr>
                    <tr>
                      <td className="font-weight-bold">Speed Laden :</td>
                      <td>{formReportdata.spd_laden}</td>

                      <td className="font-weight-bold">Vessel Owner :</td>
                      <td>{formReportdata.vessel_owner_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Speed Ballast :</td>
                      <td>{formReportdata.spd_ballast}</td>

                      <td className="font-weight-bold">Class Society :</td>
                      <td>{formReportdata.class_society}</td>

                      <td className="font-weight-bold">Scrubber :</td>
                      <td>
                        {formReportdata.scrubber_name
                          ? formReportdata.scrubber_name
                          : "N/A"}
                      </td>
                    </tr>
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Consumptions</h4>

                <h5>Port Costp. Table (Per Day)</h5>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Fuel Type</th>
                      <th>Grade</th>
                      <th>Unit</th>
                      <th>Loading</th>
                      <th>Disch</th>
                      <th>Ideal On</th>
                      <th>Capacity</th>
                      <th>Margin</th>
                      <th>Heat</th>
                      <th>Heat +</th>
                      <th>Heat +2</th>
                      <th>IGS</th>
                      <th>Clean</th>
                      <th>Menu UV</th>
                      <th>AV</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata["portconsp.tableperday"] &&
                    formReportdata["portconsp.tableperday"].length > 0
                      ? formReportdata["portconsp.tableperday"].map(
                          (e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.con_g}</td>
                                  <td>{e.con_g}</td>
                                  <td>{e.con_unit}</td>
                                  <td>{e.con_loading}</td>
                                  <td>{e.con_disch}</td>
                                  <td>{e.con_ideal_on}</td>
                                  <td>{e.con_capacity}</td>
                                  <td>{e.con_margin}</td>
                                  <td>{e.con_heat}</td>
                                  <td>{e.con_heat_p}</td>
                                  <td>{e.con_heat_pp}</td>
                                  <td>{e.con_igs}</td>
                                  <td>{e.con_clean}</td>
                                  <td>{e.con_maneuv}</td>
                                  <td>{e.con_av}</td>
                                </tr>
                              </>
                            );
                          }
                        )
                      : undefined}
                  </tbody>
                </table>

                <h5>Sea Spd Consp. Table (Per Day)</h5>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Speed Type</th>
                      <th>Speed (Kt)</th>
                      <th>Passage Type</th>
                      <th>Engine Load (%)</th>
                      <th>IFO Consp.</th>
                      <th>LSFO Consp.</th>
                      <th>ULSFO Consp.</th>
                      <th>LSMGO Consp.</th>
                      <th>MGO Consp.</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata["seaspdconsp.tableperday"] &&
                    formReportdata["seaspdconsp.tableperday"].length > 0
                      ? formReportdata["seaspdconsp.tableperday"].map(
                          (e, idx) => {
                            return (
                              <>
                                <tr key={idx}>
                                  <td>{e.speed_type}</td>
                                  <td>{e.speed}</td>
                                  <td>
                                    {e.ballast_laden ? e.ballast_laden : "N/A"}
                                  </td>
                                  <td>{e.engine_load}</td>
                                  <td>{e.ifo ? e.ifo : "N/A"}</td>
                                  <td>{e.vlsfo}</td>
                                  <td>{e.ulsfo}</td>
                                  <td>{e.lsmgo}</td>
                                  <td>{e.mgo}</td>
                                </tr>
                              </>
                            );
                          }
                        )
                      : undefined}
                  </tbody>
                </table>

                <h4>Deadweight Details</h4>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>DWT(MT)</th>
                      <th>Displ(MT)</th>
                      <th>Draft(M)</th>
                      <th>TPC</th>
                      <th>Remarks</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata.vesseldwtdrafts &&
                    formReportdata.vesseldwtdrafts.length > 0
                      ? formReportdata.vesseldwtdrafts.map((e, idx) => {
                          return (
                            <>
                              <tr key={idx}>
                                <td>{e.dwt_mt}</td>
                                <td>{e.displ_mt}</td>
                                <td>{e.draft_m}</td>
                                <td>{e.tpc}</td>
                                <td>{e.remarks}</td>
                              </tr>
                            </>
                          );
                        })
                      : undefined}
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Details</h4>

                <h5>Vessel Identification</h5>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Call Letters :</td>
                      <td>
                        {formReportdata.vesselidentification.call_letters}
                      </td>

                      <td className="font-weight-bold">Operator :</td>

                      <td>{formReportdata.vesselidentification.operator}</td>

                      <td className="font-weight-bold">Former Name :</td>

                      <td>{formReportdata.vesselidentification.formar_name}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Official No :</td>

                      <td>{formReportdata.vesselidentification.official_no}</td>

                      <td className="font-weight-bold">Suez Vsl Type :</td>

                      <td>
                        {formReportdata.vesselidentification.suez_vsl_type}
                      </td>

                      <td className="font-weight-bold">PNS No :</td>

                      <td>{formReportdata.vesselidentification.pns_no}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Yard :</td>
                      <td>{formReportdata.vesselidentification.yard}</td>

                      <td className="font-weight-bold">Hatch Type :</td>
                      <td>{formReportdata.vesselidentification.hatch_type}</td>

                      <td className="font-weight-bold">Builder :</td>
                      <td>{formReportdata.vesselidentification.builder}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">H&M Value :</td>
                      <td>{formReportdata.vesselidentification.h_m_values}</td>

                      <td className="font-weight-bold">Build Details :</td>
                      <td>
                        {formReportdata.vesselidentification.build_details}
                      </td>

                      <td className="font-weight-bold">P&I Club :</td>
                      <td>{formReportdata.vesselidentification.p_i_club}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Vessel Flag :</td>
                      <td>{formReportdata.vesselidentification.vessel_flag}</td>
                      <td className="font-weight-bold">GAP Value :</td>
                      <td>{formReportdata.vesselidentification.gap_value}</td>

                      <td className="font-weight-bold">Registry :</td>
                      <td>{formReportdata.vesselidentification.registry}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Pool Point :</td>
                      <td>{formReportdata.vesselidentification.pool_point}</td>

                      <td className="font-weight-bold">Disponent Owner :</td>
                      <td>
                        {formReportdata.vesselidentification.disponent_owner}
                      </td>

                      <td className="font-weight-bold">DWT Date :</td>
                      <td>{formReportdata.vesselidentification.dwt_date}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Hull No./Type :</td>
                      <td>
                        {formReportdata.vesselidentification.hull_no}/
                        {formReportdata.vesselidentification.hull_type}
                      </td>

                      <td className="font-weight-bold">Last Dry Dock :</td>
                      <td>
                        {formReportdata.vesselidentification.last_dry_dock
                          ? formReportdata.vesselidentification.last_dry_dock
                          : "N/A"}
                      </td>

                      <td className="font-weight-bold">Cross Ref. No. :</td>
                      <td>
                        {formReportdata.vesselidentification.cross_ref_no}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Next Dry Dock :</td>
                      <td>
                        {formReportdata.vesselidentification.next_dry_dock}
                      </td>

                      <td className="font-weight-bold">Ventilation :</td>
                      <td>{formReportdata.vesselidentification.ventilation}</td>

                      <td className="font-weight-bold">Next Survey :</td>
                      <td>{formReportdata.vesselidentification.next_survey}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Ice Class :</td>
                      <td>
                        {formReportdata.vesselidentification.ice_class
                          ? formReportdata.vesselidentification.ice_class
                          : "N/A"}
                      </td>

                      <td className="font-weight-bold">Next Inspection :</td>
                      <td>
                        {formReportdata.vesselidentification.next_inspection}
                      </td>

                      <td className="font-weight-bold">Engine Make :</td>
                      <td>{formReportdata.vesselidentification.engine_make}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Last Prop Polished :</td>
                      <td>
                        {formReportdata.vesselidentification.last_prop_polished
                          ? formReportdata.vesselidentification
                              .last_prop_polished
                          : "N/A"}
                      </td>

                      <td className="font-weight-bold">Propeller Pitch :</td>
                      <td>
                        {formReportdata.vesselidentification.propeller_pitch}
                      </td>

                      <td className="font-weight-bold">Last Hull Cleaning :</td>
                      <td>
                        {formReportdata.vesselidentification.last_hull_cleaning
                          ? formReportdata.vesselidentification
                              .last_hull_cleaning
                          : "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Cargo/Gear :</td>
                      <td>{formReportdata.vesselidentification.cargo_gear}</td>

                      <td className="font-weight-bold">TVE Expires :</td>
                      <td>{formReportdata.vesselidentification.tve_expires}</td>

                      <td colSpan="2"></td>
                    </tr>
                  </tbody>
                </table>

                <h5>Capacity And Draft</h5>
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">OPA ^90 :</td>
                      <td>{formReportdata.capacityanddraft.opa_90}</td>

                      <td className="font-weight-bold">Lightship :</td>
                      <td>{formReportdata.capacityanddraft.lightship}</td>

                      <td className="font-weight-bold">Winter Draft :</td>
                      <td>{formReportdata.capacityanddraft.winter_draft}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Max Draft :</td>
                      <td>{formReportdata.capacityanddraft.max_draft}</td>

                      <td className="font-weight-bold">GRT Int'l :</td>
                      <td>{formReportdata.capacityanddraft.grt_intl}</td>

                      <td className="font-weight-bold">NRT Int'l :</td>
                      <td>{formReportdata.capacityanddraft.nrt_intl}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Panama Gross :</td>
                      <td>{formReportdata.capacityanddraft.panama_gross}</td>

                      <td className="font-weight-bold">Net :</td>
                      <td>{formReportdata.capacityanddraft.net}</td>

                      <td className="font-weight-bold">Suez Gross :</td>
                      <td>{formReportdata.capacityanddraft.suez_gross}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Net :</td>
                      <td>{formReportdata.capacityanddraft.net2}</td>

                      <td className="font-weight-bold">Grabs Qty :</td>
                      <td>{formReportdata.capacityanddraft.grabs_qty}</td>

                      <td className="font-weight-bold">Grabs Capacity :</td>
                      <td>{formReportdata.capacityanddraft.grabs_capacity}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Unit Factor :</td>
                      <td>{formReportdata.capacityanddraft.unit_factor}</td>

                      <td colSpan="6"></td>
                    </tr>
                  </tbody>
                </table>

                <h5>Dimensions</h5>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">LOA :</td>
                      <td>{formReportdata.dimensions.loa}</td>

                      <td className="font-weight-bold">Beam :</td>
                      <td>{formReportdata.dimensions.beam}</td>

                      <td className="font-weight-bold">Depth :</td>
                      <td>{formReportdata.dimensions.depth}</td>
                    </tr>
                  </tbody>
                </table>

                <h4 className="font-weight-bold">Contacts</h4>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Manager :</td>
                      <td>{formReportdata.contacts.manager}</td>

                      <td className="font-weight-bold">Sat A :</td>
                      <td>{formReportdata.contacts.sat_a}</td>

                      <td className="font-weight-bold">Sat B :</td>
                      <td>{formReportdata.contacts.sat_b}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Sat C :</td>
                      <td>{formReportdata.contacts.sat_c}</td>

                      <td className="font-weight-bold">Mini-M :</td>
                      <td>{formReportdata.contacts.mini_m}</td>

                      <td className="font-weight-bold">Telex :</td>
                      <td>{formReportdata.contacts.telex}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Fax :</td>
                      <td>{formReportdata.contacts.fax}</td>

                      <td className="font-weight-bold">Cellular :</td>
                      <td>{formReportdata.contacts.cellular}</td>

                      <td className="font-weight-bold">Master's No :</td>
                      <td>
                        {formReportdata.contacts.masters_no
                          ? formReportdata.contacts.masters_no
                          : "N/A"}
                      </td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">CCR No :</td>
                      <td>{formReportdata.contacts.ccr_no}</td>

                      <td className="font-weight-bold">Bridge No :</td>
                      <td>{formReportdata.contacts.bridge_no}</td>

                      <td className="font-weight-bold">Email :</td>
                      <td>{formReportdata.contacts.email}</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Remarks :</td>
                      <td>{formReportdata.contacts.vessel_remarks}</td>

                      <td colSpan="6"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class VesselFormReport extends Component {
  constructor() {
    super();
    this.state = {
      name: "Printer",
    };
  }

  printReceipt() {
    window.print();
  }
  printDocument() {
    htmlToImage
      .toPng(document.getElementById("divToPrint"), { quality: 0.95 })
      .then(function(dataUrl) {
        var link = document.createElement("a");
        link.download = "my-image-name.jpeg";
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, "PNG", 0, 0, pdfWidth, pdfHeight);
        pdf.save("Vessel_Form_Report.pdf");
      });
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={this.printDocument}>Download</li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                ref={(el) => (this.componentRef = el)}
                data={this.props.data}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VesselFormReport;
