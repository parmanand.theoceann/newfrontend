import React, { useEffect, useState } from "react";
import  { IMAGE_PATH } from "../../shared";
import { Col, Row, Divider, Typography, Input } from "antd";
import EditProfile from "./EditProfile";
import {  formdata1, formdata2 } from "./FormData";
const { Text } = Typography;
const Profile = () => {
  const [data, setData] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    getpostData();
  }, []);

  const getpostData = async () => {
    const token = localStorage.getItem("oceanToken").split(".");
    const userDetail = JSON.parse(atob(token[1]));
    setData(userDetail);
    
  };
  return (
    <>
      <div className="coverimgdiv position-relative">
        <div></div>
        <div className="maincontent">
          <div className="below_coverphoto">
            {/* <div className="profilephotodiv"> */}
            <div className="leftprofile">
              <div className="profilephotoandactive">
                <img
                  src={IMAGE_PATH + "gravatar/user-g.png"}
                  alt="profilephoto"
                  className="profilephoto"
                />
              </div>
              <div className="username">
                {data.first_name}{" "}
                {data.last_name}
              </div>
            </div>
            <button className="profilebutton" onClick={openModal}>
              Edit Profile
            </button>
          </div>
          {isModalOpen ? (
            <EditProfile data={data} closeModal={closeModal} />
          ) : null}
          <div className="belowprofile">
            
            <div className="leftcontent">
              <div className="topcontent">
                <div className="about">
                  <p className="about">Official Information</p>
                </div>
                <Row style={{ marginLeft: "20px" }}>
                  {formdata1.map((item, index) => (
                    <Col
                      lg={{ span: 8 }}
                      md={{ span: 12 }}
                      span={24}
                      style={{
                        fontSize: "16px",
                        color: "#12406a",
                        marginBottom: "15px",
                      }}
                      key={index}
                    >
                      {" "}
                      {item.lable}
                      <Text type="secondary" style={{ marginLeft: "10px" }} >
                        {" "}
                        {data[item["key"]]}
                      </Text>
                      <Divider style={{ marginRight: "20px" }} />
                    </Col>
                  ))}
                </Row>
              </div>
              <div className="bottomcontent passwordclass">
                <div className="about">
                  <p className="about">Login Information</p>
                </div>
                <Row style={{ marginLeft: "20px" }}>
                  {formdata2.map((item, index) => (
                    <Col
                      lg={{
                        span: 12,
                      }}
                      span={24}
                      style={{
                        fontSize: "16px",
                        color: "#12406a",
                        marginBottom: "15px",
                      }}
                      key={index}
                    >
                      {item.lable}
                      {item.key === "email" ? (
                        <Text style={{ marginLeft: "10px" }}>
                          {data[item.key]}
                        </Text>
                      ) : (
                        <Input.Password
                          type="secondary"
                          value={data[item.key]}
                          style={{ marginLeft: "10px" }}
                        ></Input.Password>
                      )}
                    </Col>
                  ))}
                </Row>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Profile;
