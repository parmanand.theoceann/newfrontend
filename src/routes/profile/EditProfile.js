import React, { useEffect, useState } from "react";

import { Form, Input, Select, DatePicker, Button, Modal } from "antd";
import moment from "moment";
// const { Option } = Select;
const { Item } = Form;

const EditProfile = ({ closeModal, data }) => {
  const [form] = Form.useForm();
  const [formState, setFormState] = useState({
    firstName: "",
    lastName:"",
    phoneNumber: "",
  });
  useEffect(() => {
    // Use the 'data' prop to set the initial values in the form
    form.setFieldsValue({
      firstName: data.first_name,
      lastName: data.last_name,
      // gender: data.gender,
      // dateOfBirth: data.dateOfBirth ? moment(data.dateOfBirth) : null,
      phoneNumber: data.phone_number,
    });
  }, [data]);

  const handleSubmit = () => {
    form
      .validateFields()
      .then((values) => {
        // Convert the dateOfBirth value to "YYYY-MM-DD" format
        const formattedDateOfBirth = moment(values.dateOfBirth).format(
          "YYYY-MM-DD"
        );

        // Update the state with the form data
        setFormState({
          ...formState,
          ...values,
          dateOfBirth: formattedDateOfBirth,
        });
        
      })
      .catch((error) => {
        console.log(error, "errorFormData");
      });
  };

  const handleCloseForm = () => {
    closeModal();
  };

  return (
    <div>
      <Modal
        open={true}
        title="Edit Profile"
        onCancel={handleCloseForm}
        footer={[
          <Button
            key="cancel"
            onClick={handleCloseForm}
            style={{ backgroundColor: "gray" }}
          >
            Close
          </Button>,
          <Button key="submit" type="primary" onClick={handleSubmit}>
            Save Changes
          </Button>,
        ]}
      >
        <Form form={form} onFinish={handleSubmit}>
          <Item
            name="firstName"
            label="Name"
            rules={[
              {
                required: true,
                message: "Name is required",
              },
            ]}
          >
            <Input
             
            />
          </Item>
          <Item
            name="lastName"
            label="LastName"
            rules={[
              {
                required: true,
                message: "lastName is required",
              },
            ]}
          >
            <Input
             
            />
          </Item>
          {/* <Item
            name="gender"
            label="Gender"
            rules={[
              {
                required: true,
                message: "Gender is required",
              },
            ]}
          >
            <Select
              placeholder="Select Gender"
             
            >
              <Option value="male">Male</Option>
              <Option value="female">Female</Option>
              <Option value="other">Other</Option>
            </Select>
          </Item> */}
          {/* <Item
            name="dateOfBirth"
            label="Date of Birth"
            rules={[
              {
                required: true,
                message: "Date of Birth is required",
              },
            ]}
          >
            <DatePicker
              style={{ width: "100%" }}
              format={moment().format("YYYY-MM-DD")}
              
            />
          </Item> */}
          <Item
            name="phoneNumber"
            label="Phone Number"
            rules={[
              {
                required: true,
                message: "Phone Number is required",
              },
            ]}
          >
            <Input
             
            />
          </Item>
        </Form>
      </Modal>
    </div>
  );
};

export default EditProfile;
