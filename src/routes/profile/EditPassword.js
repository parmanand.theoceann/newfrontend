import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';
import { CheckOutlined } from '@ant-design/icons';
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
class EditPAssword extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <p className="text-danger"><b>Note : </b> Please use special charecters Exa. A@h1254</p>
              <Form>
                <div className="row">
                  <div className="col-md-12">
                    <FormItem {...formItemLayout} label="New Password">
                      <Input size="default" placeholder="" />
                    </FormItem>
                  </div>

                  <div className="col-md-12">
                    <FormItem {...formItemLayout} label="Confirm Password">
                      <Input size="default" placeholder="" />
                    </FormItem>
                  </div>
                </div>
                <Button className="btn ant-btn-primary btn-sm"><CheckOutlined />Update</Button>
              </Form>

            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default EditPAssword;
