import React, { Component } from "react";
import { Table, Popconfirm, Modal } from "antd";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { FIELDS } from "../../shared/tableFields";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
} from "../../shared";
import CommissionInvoice from "../operation/freight/commission-invoice/components/CommissionInvoice";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";

class AddressCommissionInvoiceList extends Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => {
                this.redirectToAdd(e, record);
              }}
            >
              <EditOutlined />
            </span>
            <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => this.onRowDeletedClick(record.id)}
              >
                <DeleteOutlined />
              </Popconfirm>
            </span>
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["address-commision-invoice-list"]
        ? FIELDS["address-commision-invoice-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    this.state = {
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      voyageData: this.props.voyageData || null,
      showPopup: false,
      popupData: {},
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };

  redirectToAdd = async (e, record = null) => {
    let _url = `${URL_WITH_VERSION}/freight-commission/edit?e=${record.invoice_no}`;
    const response = await getAPICall(_url);
    const respData = await response;
    let total_amt = 0;
    respData["data"]["..."].map((e) => {
      total_amt += +e.commission_amount;
    });
    respData["data"]["....."] = { total_amount: total_amt };

    if (respData["data"] && respData["data"].hasOwnProperty("id")) {
      this.setState({ ...this.state, popupData: respData["data"] }, () =>
        this.setState({ ...this.state, showPopup: true })
      );
    } else {
      openNotificationWithIcon("error", respData["message"]);
    }
  };

  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
     
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }
    
      // state.typesearch = {
      //   searchOptions: search.searchOptions,
      //   searchValue: search.searchValue,
      // };
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/freight-commission/address/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    let totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];

    let state = { loading: false };
    if (dataArr.length > 0 ) {
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;

      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  handleCanclePopup = () => {
    this.getTableData();
    this.setState({ ...this.state, showPopup: false });
  };

  render() {
    const {
      responseData,
      columns,
      pageOptions,
      search,
      loading,
      showPopup,
      popupData,
      voyageData,
    } = this.state;

    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"address-commission-invoice-list"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => this.callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => this.onActionDonwload("csv"),
                      },
                      {
                        title: "PDF",
                        event: () => this.onActionDonwload("pdf"),
                      },
                      {
                        title: "XLS",
                        event: () => this.onActionDonwload("xls"),
                      },
                    ]}
                  />
                ) : undefined}

                <Table
                  className="mt-3"
                  bordered
                  size="small"
                  scroll={{ x: "max-content" }}
                  columns={columns}
                  loading={loading}
                  dataSource={responseData}
                  pagination={false}
                  footer={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>

              {showPopup ? (
                <Modal
                  style={{ top: "2%" }}
                  title="Edit Commission Invoice"
                 open={showPopup}
                  onCancel={this.handleCanclePopup}
                  width="90%"
                  footer={null}
                >
                  <CommissionInvoice
                    modalCloseEvent={this.handleCanclePopup}
                    formData={popupData}
                    voyagedata={voyageData}
                    voyId={popupData.voyage_id}
                  />
                </Modal>
              ) : undefined}
            </div>
          </article>
        </div>
      </>
    );
  }
}
export default AddressCommissionInvoiceList;
