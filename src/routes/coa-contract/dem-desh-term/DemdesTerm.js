import React, { Component } from 'react';
import { Form, Switch, Icon, Radio, Select, Input, Button, Table, Layout, Modal, Drawer, Row, Col, Checkbox } from 'antd';
const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
class DemdesTerm extends Component {
  render() {
    return (
      <>
        
              {/* <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Dem/des Term Tab</span>
                  </h4>
                </div>
              </div> */}
              <Form>
                <div className="row p10">
                  <div className="col-md-6">
                    <div className="dem-des-block p-2">

                      <div className="row">
                        <div className="col-md-4 border text-white text-center bg-primary">
                          <h4>Loading : </h4>
                        </div>
                        <div className="col-md-1"></div>
                        <div className="col-md-7">
                          <div className="row">
                            <div className="col-md-6 border border-primary text-center">
                              <h4>Dem</h4>
                            </div>
                            <div className="col-md-6 border border-primary text-center">
                              <h4>Des</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row mt-3">
                        <div className="col-md-5">
                        </div>
                        <div className="col-md-7">
                          <div className="row">
                            <div className="col-md-6 p-0 w-100">
                              <FormItem {...formItemLayout}>
                                <Input size="default" />
                              </FormItem>
                            </div>
                            <div className="col-md-6 p-0">
                              <FormItem {...formItemLayout}>
                                <Input size="default" />
                              </FormItem>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12 mb-5">
                              <Radio.Group >
                                <Radio value={1}>Per Day</Radio>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <Radio value={2}>Per Hour</Radio>
                              </Radio.Group>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4 border text-white text-center bg-primary">
                          <h4>Discharge : </h4>
                        </div>
                        <div className="col-md-1"></div>
                        <div className="col-md-7">
                          <div className="row">
                            <div className="col-md-6 border border-primary text-center">
                              <h4>Dem</h4>
                            </div>
                            <div className="col-md-6 border border-primary text-center">
                              <h4>Des</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row mt-3">
                        <div className="col-md-5">
                        </div>
                        <div className="col-md-7">
                          <div className="row">
                            <div className="col-md-6 p-0 w-100">
                              <FormItem {...formItemLayout}>
                                <Input size="default" />
                              </FormItem>
                            </div>
                            <div className="col-md-6 p-0">
                              <FormItem {...formItemLayout}>
                                <Input size="default" />
                              </FormItem>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12 mb-5">
                              <Radio.Group >
                                <Radio value={1}>Per Day</Radio>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <Radio value={2}>Per Hour</Radio>
                              </Radio.Group>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div className="row mt-2">
                        <div className="col-4 frm-lable">Dem/Des Currency</div>
                        <div className="col-4">
                          <FormItem {...formItemLayout}>
                            <Input size="default" placeholder="USD" />
                          </FormItem>
                        </div>
                      </div>
                      <div className="row mt-2">
                        <div className="col-4 frm-lable">Demurrage</div>
                        <div className="col-4">
                          <FormItem {...formItemLayout}>
                            <Input size="default" defaultValue="" />
                          </FormItem>
                        </div>
                        <div className="col-4 frm-lable">LInked Table</div>
                      </div>
                      <div className="row mt-2">
                        <div className="col-4 frm-lable">Laytime</div>
                        <div className="col-4">
                          <FormItem {...formItemLayout}>
                            <Input size="default" defaultValue="" />
                          </FormItem>
                        </div>
                        <div className="col-4 frm-lable">LInked Table</div>
                      </div>
                      <div className="row mt-2">
                        <div className="col-4 frm-lable">Laytime Allowed</div>
                        <div className="col-4">
                          <FormItem {...formItemLayout}>
                            <Input size="default" defaultValue="" />
                          </FormItem>
                        </div>
                        <div className="col-4">
                          <Radio.Group className="frm-lable">
                            <Radio value={1}>Days</Radio>
                            <Radio value={2}>Hours</Radio>
                          </Radio.Group>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="col-md-10 frm-panel-group">
                      <FormItem {...formItemLayout} label="Total Time Bar">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>
                    <div className="col-md-10">
                      <FormItem {...formItemLayout} label="TT Hours">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>
                    <div className="col-md-10">
                      <FormItem {...formItemLayout} label="Freight surcharge">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>
                    <div className="col-md-10">
                      <FormItem {...formItemLayout} label="Bunker surcharge">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>
                    <div className="col-md-10">
                      <FormItem {...formItemLayout} label="Admin Charges">
                        <Input size="default" defaultValue="" />
                      </FormItem>
                    </div>
                    <div className="col-md-12">
                      <FormItem {...formItemLayout} label="Dem/Des Commiable">
                        <Switch checkedChildren="No" unCheckedChildren="Yes" />
                      </FormItem>
                    </div>
                    <div className="col-md-12">
                      <FormItem {...formItemLayout} label="Reversible All Port">
                        <Switch checkedChildren="No" unCheckedChildren="Yes" />
                      </FormItem>
                    </div>
                    <div className="col-md-12">
                      <FormItem {...formItemLayout} label="NonRev. All Port">
                        <Switch checkedChildren="No" unCheckedChildren="Yes" />
                      </FormItem>
                    </div>
                  </div>
                </div>
              </Form>
           
      </>
    )
  }
}

export default DemdesTerm;