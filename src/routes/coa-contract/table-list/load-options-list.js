import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button } from 'antd';

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        loportarea: "AUSTRALIA",
        loldrate: "20,0000.00",
        lounit: "PD",
        loterms: "",
        lott: "24.00",
        loportexp: "90,000",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class LoadOptionsList extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [
            {
                title: 'Lo Port Area',
                dataIndex: 'loportarea',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'loportarea'),
            },
            {
                title: 'Lo L D Rate',
                dataIndex: 'loldrate',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'loldrate'),
            },
            {
                title: 'Lo Unit',
                dataIndex: 'lounit',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'lounit'),
            },
            {
                title: 'Lo Terms',
                dataIndex: 'loterms',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'loterms'),
            },
            {
                title: 'Lo Tt',
                dataIndex: 'lott',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'lott'),
            },
            {
                title: 'Lo Port Exp',
                dataIndex: 'loportexp',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'loportexp'),
            },
            {
                title: 'Action',
                dataIndex: 'action',
                width: 100,
                fixed: 'right',
                render: (text, record) => {
                    const { editable } = record;
                    return (
                        <div className="editable-row-operations">
                            {
                                editable ?
                                    <span>
                                        <a className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                        <a className="iconWrapper cancel">
                                            <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                               <DeleteOutlined /> />
                                            </Popconfirm>
                                        </a>
                                    </span>
                                    : <a className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></a>
                            }
                        </div>
                    );
                },
            }
        ];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 1000, y: 300 }}
                    size="small"
                    pagination={false}
                    title={() => <div className="bold-title-wrapper">
                        <span>Load Options</span>
                    </div>
                    }
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default LoadOptionsList;