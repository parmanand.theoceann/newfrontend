import React, { Component } from 'react';
import { Form, Input, Button, Icon, Row, Col, Table, Checkbox, Popconfirm } from 'antd';

const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        loadports: "AUSTRALIA",
        dischargeports: "PARADIP",
        cargo: "COAL",
        frttype: "Flat Type",
        amount: "27.0",
        table: "",
        basis: "",
        xfrt: "",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

function onChange(checkedValues) {
    // console.log('checked = ', checkedValues);
}

class PricingOneList extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [
            {
                title: 'Load Ports',
                dataIndex: 'loadports',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'loadports'),
            },

            {
                title: 'Discharge Ports',
                dataIndex: 'dischargeports',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'dischargeports'),
            },
            {
                title: 'Cargo',
                dataIndex: 'cargo',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'cargo'),
            },
            {
                title: 'Frt Type',
                dataIndex: 'frttype',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'frttype'),
            },
            {
                title: 'Amount',
                dataIndex: 'amount',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'amount'),
            },
            {
                title: 'Table',
                dataIndex: 'table',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'table'),
            },
            {
                title: 'Basis',
                dataIndex: 'basis',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'basis'),
            },
            {
                title: 'Xfrt',
                dataIndex: 'xfrt',
                width: 150,
                render: (text, record) => this.renderColumns(text, record, 'xfrt'),
            },
            {
                title: 'Action',
                dataIndex: 'action',
                width: 100,
                fixed: 'right',
                render: (text, record) => {
                    const { editable } = record;
                    return (
                        <div className="editable-row-operations">
                            {
                                editable ?
                                    <span>
                                        <a className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></a>
                                        <a className="iconWrapper cancel">
                                            <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                               <DeleteOutlined /> />
                                            </Popconfirm>
                                        </a>
                                    </span>
                                    : <a className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></a>
                            }
                        </div>
                    );
                },
            }
        ];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>
                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 1000, y: 300 }}
                    size="small"
                    pagination={false}
                    title={() => <div className="bold-title-wrapper">
                        <span>
                            <Row>
                                <Col span={8}>
                                    <Checkbox.Group style={{ width: '100%' }} onChange={onChange}>
                                        <Row>
                                            <Col span={8}>
                                                <Checkbox value="Advanced">Advanced</Checkbox>
                                            </Col>

                                            <Col span={16}>
                                                <Checkbox value="Use Booking Qty on Scale Table">Use Booking Qty on Scale Table</Checkbox>
                                            </Col>
                                        </Row>
                                    </Checkbox.Group>
                                </Col>
                                <Col span={4}>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Top-off">
                                        <Input size="default" placeholder="None" />
                                    </FormItem>
                                </Col>
                            </Row>
                        </span>
                    </div>
                    }
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                />
            </>
        )
    }
}

export default PricingOneList;