import { useEffect, useRef, useState } from "react";
import { Layout, Drawer, Modal, notification } from "antd";
import URL_WITH_VERSION, {
  postAPICall, openNotificationWithIcon, awaitPostAPICall, getAPICall, apiDeleteCall, useStateCallback,
} from "../../shared";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../shared/attachments";

import RightBarUI from "../../components/RightBarUI";
import DemdesTerm from "./dem-desh-term/DemdesTerm";
import Properties from "../chartering/routes/tcov/Properties";
import Plsummary from "./PlSummary";
import EstimateSummary from "../chartering/routes/cargo-contract/EstimateSummary";
import CargoContract from "../chartering/routes/cargo-contract";
import CoaCargoReports from "../form-reports/CoaCargoReports";
import Attachment from "../../shared/components/Attachment";
import moment from "moment";
import AttachmentFile from "../../shared/components/Attachment";
import { DeleteOutlined, MenuFoldOutlined, PlusOutlined, SaveOutlined, SyncOutlined } from "@ant-design/icons";
import { useLocation, useNavigate, useParams } from "react-router-dom";

const openNotification = (keyName, isValid = false) => {
  let msg = `Please save COA (Cargo) Form first, and then click on ${keyName}`;
  if (isValid)
    msg = `You cannot ${keyName} as it is already being attached. Kindly update it in same form.`;

  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const pattern = /^\/edit-coa-contract\/[A-Z0-9-]+$/;

const COAContract = (props) => {
  const Params = useParams(); 
  
  const estimateID = Params ? Params.id : null
  
  const location = useLocation()
  const navigate = useNavigate()
  const [item, setItem] = useState(null)
  const [refreshFormValue, setRefreshFormValue] = useState(0)
  const [state, setState] = useStateCallback({

    frmName: "coa_form",
    reRenderid: 0,
    formData: Object.assign(
      {
        id: 0,
        page_type: "COA(Cargo)",
        head_contract_type: "Sale",
        start: moment(),
        end: moment(),
        cp_date: moment(),
        comence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
        nominationtask: [{ due_date: moment(), reminder_offset: moment() }],
        broker: [
          {
            amount: "0.00",
            broker: null,
            pay_method: 0,
            rate_type: 0,
          },
        ],
        extrafreightterm: [
          {
            code: "0",
            extra_freight_term: null,
            rate_lump: "0.00",
          },
        ],
      },
      props.formData || {}
    ),
    formReportdata: Object.assign(
      {
        id: 0,
        page_type: "COA(Cargo)",
        head_contract_type: "Sale",
        start: moment(),
        end: moment(),
        cp_date: moment(),
        comence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
        nominationtask: [{ due_date: moment(), reminder_offset: moment() }],
      },

      props.reportFormData || {}
    ),

    visibleDrawer: false,
    title: undefined,
    loadComponent: undefined,
    width: 1200,
    frmVisible: true,
    showSideListBar: false,
    isSetsLeftBtn: [
      {
        id: "1",
        key: "menu-fold",
        type: <MenuFoldOutlined />,
        withText: "List",
        showToolTip: true,
        event: "menu-fold",
      },
      {
        id: "2",
        key: "add",
        type: <PlusOutlined />,
        withText: "Add New",
        showToolTip: true,
        event: (key, data) => _onCreateFormData(),
      },
      {
        id: "3",
        key: "save",
        type: <SaveOutlined />,
        withText: "Save",
        showToolTip: true,
        event: (key, data) => onSaveFormData(data),
      },
      {
        id: "4",
        key: "delete",
        type: <DeleteOutlined />,
        withText: "Delete",
        showToolTip: true,
        event: (key, data) => _onDeleteFormData(data),
      },
      {
        id: "20",
        key: "refresh",
        type: <SyncOutlined />,
        withText: "Refresh",
        showToolTip: true,
        event: (key, data) => {
          reFreshForm();
        },
      },
    ],
    isVisible: false,
    postFormData: [],
    visibleContactAttachment: false,
    isShowCoaCargoReports: false,
    selectedID: null,
    // estimateID: params ? params.id : null,
    estimateID: estimateID,
    
    
  })

  useEffect(() => {
    if (location.pathname === "/add-coa-contract") {
      setState(
        (prevState) => ({
          ...prevState,

          formData: {
            id: 0,
            page_type: "COA(Cargo)",
            head_contract_type: "Sale",
            start: moment(),
            end: moment(),
            cp_date: moment(),
            comence_date: moment(),
            billingandbankingdetails: {
              due_date: moment(),
            },
          },
          frmVisible: false,
        }),
        () => {
          setState((prev) => ({
            ...prev,
            frmVisible: true,
          }));
        }
      );
    } else {
      // _onLeftSideListClick(params.id);
      _onLeftSideListClick(Params.id);
      
    }

  }, [window.location.hash, refreshFormValue])

  const reFreshForm = () => {
    setRefreshFormValue(refreshFormValue => refreshFormValue + 1)
  }
  // const reFreshForm = async (id) => {
  //   console.log("abcd", id);
  //   console.log("location.pathname : ", location.pathname);
  //   console.log("params.id : ", params.id);
  //   if (location.pathname === "/add-coa-contract") {
  //     setState(
  //       (prevState) => ({
  //         ...prevState,
  //         formData: {},
  //         frmVisible: false,
  //       }),
  //       () => {
  //         setState((prev) => ({
  //           ...prev,
  //           frmVisible: true,
  //         }));
  //       }
  //     );
  //   } else {
  //     console.log("5555555555");
  //     _onLeftSideListClick(params.id);
  //   }
  // };
  // console.log( formref.current ,"formref.currentout");
  // useEffect(() => {
  //   formref.current = { ...state.formData }; // Using object spread syntax to create a shallow copy
  //   console.log(formref.current, "formref.current");
  // }, [state.formData]);




  const onCloseDrawer = () =>
    setState(prevState => ({
      ...prevState,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    }));

  const onClickRightMenu = async (key, options) => {
    onCloseDrawer();
    let loadComponent = undefined;
   
    switch (key) {
      case "properties":
        loadComponent = <Properties />;
        break;
      case "pl-summary":
        loadComponent = <Plsummary />;
        break;
      case "summary":
        loadComponent = <EstimateSummary />;
        break;
      case "attachment":
        // const { estimateID } = state;
        // console.log("Estimate,", estimateID)
        if (Params.id) {
        
          const attachments = await getAttachments(estimateID, "EST");
          const callback = (fileArr) =>
            uploadAttachment(fileArr, estimateID, "EST", "coa-contract");
          loadComponent = (
            <AttachmentFile
              uploadType="Estimates"
              attachments={attachments}
              onCloseUploadFileArray={callback}
              deleteAttachment={(file) =>
                deleteAttachment(file.url, file.name, "EST", "coa-contract")
              }
              tableId={0}
            />
          );
        } else {
          openNotificationWithIcon("info", "Attachments are not allowed here.");
        }

       
        break;
      default:
        break;
    }

    setState(prevState => ({
      ...prevState,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    }));
  };

  const onSaveFormData = (postData, innerCB = null) => {
    

    if (
      state.formData.coa_status == 73 &&
      (postData["coa_status"] == 112 || postData["coa_status"] == 74)
    ) {
      let msg = `You cannot change Enquiry/Confirmed after fix status.`;
      openNotificationWithIcon(
        "error",
        <div dangerouslySetInnerHTML={{ __html: msg }} />,
        5
      );
      return false;
    }
    if (postData) {
      const { frmName } = state;
      let _url = "save";
      let _method = "post";
      if (postData.hasOwnProperty("id") && postData["id"] > 0) {
        _url = "update";
        _method = "put";
      }

      Object.keys(postData).forEach(
        (key) => postData[key] === null && delete postData[key]
      );

      ["l_d_rates", "ld_ru", "ld_terms", "ld_tt", "pexp", "draft"].forEach(
        (e) => delete postData[e]
      );
      if (postData.disablefield) {
        delete postData.disablefield;
      }

      let _urlPost = `${URL_WITH_VERSION}/coa/${_url}?frm=${frmName}`;

      postAPICall(`${_urlPost}`, postData, _method, (data) => {
        setItem(data.row.contract_id);  // setState//
        if (data.data) {
          openNotificationWithIcon("success", data.message);

          if (
            props.modalCloseEvent &&
            typeof props.modalCloseEvent === "function"
          ) {
            props.modalCloseEvent();
          } else if (innerCB && typeof innerCB === "function") {
            innerCB();
          }
          let setData = Object.assign(
            {},
            {
              id: 0,
              page_type: "COA(Cargo)",
              head_contract_type: "Sale",
              start: moment(),
              end: moment(),
              cp_date: moment(),
              comence_date: moment(),
              billingandbankingdetails: {
                due_date: moment(),
              },
            },
            props.formData || {}
          );

          if ((pattern.test(location.pathname) || location.pathname === "/add-coa-contract") &&
            data.row &&
            data.row.contract_id
          ) {
            _onLeftSideListClick(data.row.contract_id);
          }

          setState(
            {
              ...state,
              formData: setData,
              reRenderid: state.reRenderid + 1,
              frmVisible: false,
              showSideListBar: true,
              frmVisible: true
            });




        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
              (msg +=
                "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />
          );
        }
      });
    }
  };







  const _onLeftSideListClick = async (coaVciId) => {
    if (pattern.test(location.pathname) || location.pathname === "/add-coa-contract") {
      navigate(`/edit-coa-contract/${coaVciId}`);
    }
    setState(prevState => ({ ...prevState, frmVisible: false }));
    const response = await getAPICall(
      `${URL_WITH_VERSION}/coa/edit?ae=${coaVciId}`
    );
    const data = await response["data"];
    if (data && !data.hasOwnProperty("--"))
      data["--"] = [
        { load_ports_name: "Select Port", discharge_ports_name: "Select Port" },
      ];

    // if (data && !data.hasOwnProperty('broker'))
    // data['broker'] = [{ "broker": "",}]

    // if (data && !data.hasOwnProperty('extrafreightterm'))
    // data['extrafreightterm'] = [{ "extra_freight_term": "Select broker","index":0  }]

    if (data.hasOwnProperty("billingandbankingdetails")) {
      for (let key in data.billingandbankingdetails) {
        if (!data["billingandbankingdetails"][key]) {
          data["billingandbankingdetails"][key] = undefined;
        }
      }
    }
    // if (data.hasOwnProperty('loadoptions') && data.loadoptions.length > 0) {
    //   data.loadoptions.map(e => {
    //     e['port_area_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('dischargeoptions') && data.dischargeoptions.length > 0) {
    //   data.dischargeoptions.map(e => {
    //     e['port_area_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('itineraryoptions') && data.itineraryoptions.length > 0) {
    //   data.itineraryoptions.map(e => {
    //     e['port_area_name'] = e.port_name
    //   })
    // }
    // if (data.hasOwnProperty('rebillsettings') && data.rebillsettings.length > 0) {
    //   data.rebillsettings.map(e => {
    //     e['cv_port_name'] = e.port_name
    //   })
    // }
    // if(data.hasOwnProperty('--') && data['--'].length > 0){
    //   data['--'].map(e=>{
    //     e['load_ports_name'] = e.load_ports_name
    //     e['discharge_ports_name']=e.discharge_ports_name
    //   })
    // }

    let setData = Object.assign(
      {},
      {
        id: 0,
        page_type: "COA(Cargo)",
        head_contract_type: "Sale",
        start: moment(),
        end: moment(),
        cp_date: moment(),
        comence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
        nominationtask: { due_date: moment(), reminder_offset: moment() },
      },
      data || {}
    );

    if (data.hasOwnProperty("is_fixed") && data["is_fixed"] === 1) {
      setData["disablefield"] = ["charterer", "coa_qty", "coa_qty_unit"];
      // "freight_rate",freight_type  will always be enabled.
    }

    setState(prevState => ({
      ...prevState,
      frmVisible: false,
      formData: setData,
      selectedID: parseInt(coaVciId.replace("COACARGO-", "")),
      frmVisible: true,
      showSideListBar: false
    }));
  };

  const _onCreateFormData = async () => {

    setState((state) => ({
      ...state, frmVisible: false,
      formData: {
        id: 0,
        page_type: "COA(Cargo)",
        head_contract_type: "Sale",
        start: moment(),
        end: moment(),
        cp_date: moment(),
        comence_date: moment(),
        billingandbankingdetails: {
          due_date: moment(),
        },
      },
    }), () => {
      setState(prevState => ({
        ...prevState,

        showSideListBar: true,
        frmVisible: true,
        selectedID: null,
      }));
    });

    setTimeout(() => {
      navigate(`/add-coa-contract`, { replace: true })
    }, 1000)
  };

  const _onDelete = (postData, type = null) => {
    let _url = `${URL_WITH_VERSION}/coa/delete`;
    apiDeleteCall(_url, { id: postData.id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        let setData = {};
        if (type === "fixed") {
          if (
            props.modalCloseEvent &&
            typeof props.modalCloseEvent === "function"
          ) {
            props.modalCloseEvent();
          }
          let setData = Object.assign(
            {},
            {
              id: 0,
              page_type: "COA(Cargo)",
              head_contract_type: "Sale",
              start: moment(),
              end: moment(),
              cp_date: moment(),
              comence_date: moment(),
              billingandbankingdetails: {
                due_date: moment(),
              },
            },
            props.formData || {}
          );

          if ((pattern.test(location.pathname) || location.pathname === "/add-coa-contract") && postData.contract_id
          ) {
            _onLeftSideListClick(postData.contract_id);
          }

          setState((pre)=>({
            ...pre,
            formData: setData,
            showSideListBar: true,
            frmVisible: true
          })
            );
        } else {
          navigate("/add-coa-contract");
        }
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const _onDeleteFormData = (postData) => {
    if (postData && postData.id <= 0) {
      openNotificationWithIcon(
        "error",
        "Cargo Id is empty. Kindly check it again!"
      );
    } else if (postData && postData["..."] && postData["..."].length > 0) {
      postData["..."].forEach((d) => {
        if (d["cargo_contract_id"]) {
          openNotificationWithIcon(
            "error",
            "Pls delete Linked VC/cargo contract Created!"
          );
        }
      });
    } else if (
      postData &&
      (postData.coa_status == 73 ||
        postData.coa_status == 188 ||
        postData.coa_status == 76)
    ) {
      Modal.confirm({
        title: "Confirm",
        content: "Are you sure to delete this COA in fixed status?",
        onOk: () => _onDelete(postData, "fixed"),
      });
    } else {
      Modal.confirm({
        title: "Confirm",
        content: "Are you sure, you want to delete it?",
        onOk: () => _onDelete(postData),
      });
    }
  };

  const toggleCargoRightMenu = (val, type = "save", data = null) => {
    const { formData } = state;
    const {
      contract_id,
      cargo_name,
      coa_status,
      currency,
      charterer,
      invoice_by,
      min_qty,
      min_inv_qty,
      freight_type,
      my_company,
      lob,
      freight_rate,
      c_name,
      cp_qty_per_lift,
      freight_bill_via,
      min_qty_lift,
      max_qty_lift,
      fixed_by_user,
      trade_area,
      cp_date,
      cp_place,
    } = formData;
    let postFormData = Object.assign({
      id: 0,
      cargo_coa: contract_id,
      cargo_group: cargo_name,
      cargo_status: coa_status,
      currency,
      charterer,
      invoice_by,
      min_inv_qty,
      freight_type,
      my_company,
      lob,
      freight_rate,
      cargo_name: c_name,
      cp_qty: cp_qty_per_lift,
      freight_bill_via,
      mix_qty: min_qty_lift ? min_qty_lift : min_qty,
      max_qty: max_qty_lift,
      // fixed_by: fixed_by_user,
      trade_area,
      cp_date,
      cp_place,
    });

    if (val) {
      if (type == "edit") {
        if (data && data.hasOwnProperty("index") && data["index"] > -1) {
          let id = formData["..."][data["index"]]["cargo_contract_id"];
          postFormData = { params: { id } };
          setState({ ...state, isVisible: val, postFormData });
        }
      } else {
        if (formData && formData.hasOwnProperty("id") && formData["id"] > 0)
          setState({ ...state, isVisible: val, postFormData });
        else openNotification("Create Cargo");
      }
    } else {
      setState({ ...state, isVisible: val, postFormData: [] });
    }
  };

  const isContactAttachmentOk = () => {
    setTimeout(() => {
      setState({ ...state, visibleContactAttachment: false });
    }, 3000);
  };

  const openCoaCargoReports = async (showCoaCargoReports, coaVciId) => {
    
    try {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/coa/report?ae=${coaVciId}`
      );
      const reportData = await responseReport["data"];
      if (reportData) {
        setState({
          ...state,
          formReportdata: reportData,
          isShowCoaCargoReports: showCoaCargoReports,
        });
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } catch (err) {
      console.log("err", err);
    }
  };
  const isContactAttachmentCancel = () =>
    setState({ ...state, visibleContactAttachment: false });

  const fixedCOAContract = (data) => {
    postAPICall(
      `${URL_WITH_VERSION}/coa/fix`,
      { cargo_contract_id: data["contract_id"] },
      "POST",
      (respData) => {
        if (respData.data) {
          openNotificationWithIcon("success", respData.message);


          //  setState((prevState) => ({
          //   ...prevState,
          //   reRenderid: prevState.reRenderid + 1,
          // }), () => {
          //   onSaveFormData(data);

          // });

          window.location.reload()


        } else {
          let dataMessage = respData.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
              (msg +=
                "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />,
            5
          );
        }
      }
    );
  };

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Load Options") {
      groupKey = "loadoptions";
      frm_code = "coa_form";
    }
    if (groupKey == "Discharge Options") {
      groupKey = "dischargeoptions";
      frm_code = "coa_form";
    }
    if (groupKey == "Itinerary Options") {
      groupKey = "itineraryoptions";
      frm_code = "tab_coa_itinerary_options_form";
    }
    if (groupKey == "--") {
      frm_code = "tab_coa_pricing_form";
    }
    if (groupKey == "Broker") {
      groupKey = "broker";
      frm_code = "tab_coa_pricing_form";
    }
    if (groupKey == "Extra Freight Term") {
      groupKey = "extrafreightterm";
      frm_code = "tab_coa_pricing_form";
    }
    if (groupKey == "Rev Exp") {
      groupKey = "revexp";
      frm_code = "tab_coa_rev_exp_form";
    }
    if (groupKey == "Rebill Settings") {
      groupKey = "rebillsettings";
      frm_code = "tab_coa_rebill_form";
    }
    if (groupKey == "Nomination Task") {
      groupKey = "nominationtask";
      frm_code = "tab_coa_nomination_tasks_form";
    }
    if (groupKey == "Cargo") {
      groupKey = "cargo";
      frm_code = "tab_coa_nomination_tasks_form";
    }
    if (groupKey == "Vessel Type") {
      groupKey = "vesseltype";
      frm_code = "tab_coa_nomination_tasks_form";
    }
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const {
    loadComponent,
    isShowCoaCargoReports,
    title,
    frmVisible,
    isVisible,
    visibleDrawer,
    frmName,
    formData,
    showSideListBar,
    isSetsLeftBtn,
    formReportdata,
    visibleContactAttachment,
    postFormData,
    selectedID,
  } = state;
  const isSetsLeftBtnArr = isSetsLeftBtn?.filter(
    (item) =>
      !(
        formData &&
        formData.hasOwnProperty("id") &&
        formData.id <= 0 &&
        item.key == "delete"
      )
  );

  return (
    <div className="tcov-wrapper full-wraps">
      <Layout className="layout-wrapper">
        <Layout>
          <div className="body-wrapper">
            <article className="article">
              <div className="box box-default">
                <div className="box-body common-fields-wrapper" key={state.reRenderid}>
                  {frmName && frmVisible ? (
                    <NormalFormIndex
                      key={"key_" + frmName + "_0"}
                      formClass="label-min-height"
                      formData={formData}
                      showForm={true}
                      frmCode={frmName}
                      addForm={true}
                      // showButtons={undefined}
                      showToolbar={[
                        {
                          leftWidth: 8,
                          rightWidth: 16,
                          isLeftBtn: [{ isSets: isSetsLeftBtnArr }],
                          isRightBtn: [
                            {
                              isSets: [
                                {
                                  key: "menu",
                                  isDropdown: 1,
                                  withText: "Menu",
                                  type: "",
                                  menus: [
                                    {
                                      href: null,
                                      icon: null,
                                      label: "Create Cargo ",
                                      modalKey: null,
                                      event: (key, data) =>
                                        data["is_fixed"] == 1
                                          ? toggleCargoRightMenu(true)
                                          : openNotificationWithIcon(
                                            "info",
                                            "Please fix first and then click on Create Cargo!"
                                          ),
                                    },
                                    {
                                      href: null,
                                      icon: null,
                                      label: "Report",
                                      modalKey: null,
                                      event: (key, data) =>
                                     
                                        data && data["contract_id"]
                                          ? openCoaCargoReports(
                                            true,
                                            data.contract_id
                                          )
                                          : openNotificationWithIcon(
                                            "info",
                                            "please select any item in the list!"
                                          ),
                                    },
                                  ],
                                },
                                {
                                  key: "create_cargo",
                                  isDropdown: 0,
                                  withText: "Create Cargo Contract",
                                  type: "",
                                  menus: null,
                                  event: (key, data) =>
                                    data["is_fixed"] == 1
                                      ? toggleCargoRightMenu(true)
                                      : openNotificationWithIcon(
                                        "info",
                                        "Please fix first and then click on Create Cargo!"
                                      ),
                                },
                                formData &&
                                  formData.hasOwnProperty("is_fixed") &&
                                  formData["is_fixed"] === 0
                                  ? {
                                    key: "fix_coa",
                                    isDropdown: 0,
                                    withText: "Fix",
                                    type: "",
                                    menus: null,
                                    event: (key, data) =>
                                      data &&
                                        data.hasOwnProperty("id") &&
                                        data["id"] > 0
                                        ? Modal.confirm({
                                          title: "Confirm",
                                          content:
                                            "Are you sure, you want to Fix it?",
                                          onOk: () =>
                                            fixedCOAContract(data),
                                        })
                                        : openNotification("Fix"),
                                  }
                                  : undefined,
                                {
                                  key: "marster_contract",
                                  isDropdown: 0,
                                  withText: "Master Contract",
                                  type: "",
                                  menus: null,
                                },
                                {
                                  key: "report",
                                  isDropdown: 0,
                                  withText: "Reports",
                                  type: "",
                                  menus: null,
                                  event: (key, data) =>
                                     
                                        data && data["contract_id"]
                                          ? openCoaCargoReports(
                                            true,
                                            data.contract_id
                                          )
                                          : openNotificationWithIcon(
                                            "info",
                                            "please select any item in the list!"
                                          ),
                                },
                              ],
                            },
                          ],
                          isResetOption: false,
                        },
                      ]}
                      inlineLayout={true}
                      isShowFixedColumn={[
                        "Load Options",
                        "Discharge Options",
                        "Itinerary Options",
                        "Broker",
                        "Rev/Exp",
                      ]}
                      // staticTabs={{
                      //   "Dem/Des Term": () => { return <DemdesTerm /> }
                      // }}
                      showSideListBar={showSideListBar}
                      sideList={{
                        selectedID: selectedID,
                        showList: true,
                        title: "COA(Cargo) - List",
                        uri: "/coa/list?l=0",
                        columns: ["contract_id", "charterer1", "status_name"],
                        icon: true,
                        rowClickEvent: (evt) =>
                          _onLeftSideListClick(evt.contract_id),
                      }}
                      extraTableButton={{
                        "...":
                          formData &&
                            formData.hasOwnProperty("...") &&
                            formData["..."].length > 0
                            ? [
                              {
                                icon: "edit",
                                onClickAction: (action, data) =>
                                  toggleCargoRightMenu(
                                    true,
                                    "edit",
                                    action
                                  ),
                              },
                            ]
                            : [],
                      }}
                      tableRowDeleteAction={(action, data) =>
                        onClickExtraIcon(action, data)
                      }
                    />
                  ) : (
                    undefined
                  )}
                </div>
              </div>
            </article>
          </div>
        </Layout>
        <RightBarUI
          pageTitle="coa-righttoolbar"
          callback={(data, options) => 
            onClickRightMenu(data, options)
          }
        />
        {isVisible === true ? (
          <Modal
            title="Create Cargo"
            open={isVisible}
            width="95%"
            onCancel={() => toggleCargoRightMenu(false)}
            style={{ top: "10px" }}
            bodyStyle={{ height: 790, overflowY: "auto" }}
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  {postFormData && postFormData.hasOwnProperty("params") ? (
                    <CargoContract
                      history={props.history}
                      match={postFormData}
                      isDisabled={true}
                      modalCloseEvent={() => toggleCargoRightMenu(false)}
                    />
                  ) : (
                    <CargoContract
                      history={props.history}
                      showSideListBar={false}
                      modalCloseEvent={() => toggleCargoRightMenu(false)}
                      formData={postFormData}
                      isDisabled={true}
                    />
                  )}
                </div>
              </article>
            </div>
          </Modal>
        ) : (
          undefined
        )}
        {loadComponent !== undefined &&
          title !== undefined &&
          visibleDrawer === true ? (
          <Drawer
            title={state.title}
            placement="right"
            closable={true}
            onClose={onCloseDrawer}
            open={state.visibleDrawer}
            getContainer={false}
            style={{ position: "absolute" }}
            width={state.width}
            maskClosable={false}
            className="drawer-wrapper-container"
          >
            <div className="tcov-wrapper">
              <div className="layout-wrapper scrollHeight">
                <div className="content-wrapper noHeight">
                  {state.loadComponent}
                </div>
              </div>
            </div>
          </Drawer>
        ) : (
          undefined
        )}
      </Layout>

      {isShowCoaCargoReports ? (
        <Modal
          style={{ top: "2%" }}
          title="Reports"
          open={isShowCoaCargoReports}
          onCancel={() => setState(prevState => ({ ...prevState, isShowCoaCargoReports: false }))}
          width="95%"
          footer={null}
        >
          <CoaCargoReports data={formReportdata} />
        </Modal>
      ) : (
        undefined
      )}

      {visibleContactAttachment ? (
        <Modal
          open={visibleContactAttachment}
          title="Upload Attachment ( Upload Contact Details )"
          onOk={isContactAttachmentOk}
          onCancel={isContactAttachmentCancel}
          footer={null}
          width={1000}
          maskClosable={false}
        >
          <Attachment
            uploadType="Address Book"
            directory={
              formData.hasOwnProperty("estimate_id")
                ? formData["estimate_id"]
                : null
            }
          // onCloseUploadFileArray={fileArr => uploadedFiles(fileArr)}
          />
        </Modal>
      ) : (
        undefined
      )}
    </div>
  );
}

export default COAContract;
