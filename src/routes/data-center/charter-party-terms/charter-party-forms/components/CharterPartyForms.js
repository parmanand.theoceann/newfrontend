import React from 'react';
import { Table, Input, Icon, Row, Popconfirm, Col, Button, Tooltip } from 'antd';

class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    editable: false,
  }
  handleChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  }
  check = () => {
    this.setState({ editable: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  }
  edit = () => {
    this.setState({ editable: true });
  }
  render() {
    const { value, editable } = this.state;
    return (
      <div className="editable-cell">
        {
          editable ? (
            <Input
              value={value}
              onChange={this.handleChange}
              onPressEnter={this.check}
              suffix={
                <Icon
                  type="check"
                  className="editable-cell-icon-check"
                  onClick={this.check}
                />
              }
            />
          ) : (
            <div style={{ paddingRight: 24 }}>
              {value || ' '}
              <Icon
                type="edit"
                className="editable-cell-icon"
                onClick={this.edit}
              />
            </div>
          )
        }
      </div>
    );
  }
}

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: 'Name',
      dataIndex: 'name',
      width: '30%',
      render: (text, record) => (
        <EditableCell
          value={text}
          onChange={this.onCellChange(record.key, 'name')}
        />
      ),
    }, {
      title: 'Description',
      dataIndex: 'des',
      width: '40%',
    }, {
      title: 'File',
      dataIndex: 'file',
      width: '10%',
    }, {
      title: 'Actions',
      dataIndex: 'action',
      render: (text, record) => {
        return (
          this.state.dataSource.length > 0 ?
            (
              <div>
                <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                  <Button type="link">Delete</Button>
                </Popconfirm>
              </div>
            ) : null
        );
      },
    }];

    this.state = {
      editable: false,
      dataSource: [{
        key: '0',
        name: 'Edward King 0',
        des: 'It is a long established fact that a reader will be distracted by the readable content of a page ',
        file: 'txt.pdf',
      }],
      count: 1,
    };
  }

  edit = () => {
    this.setState({ editable: true });
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource = [...this.state.dataSource];
      const target = dataSource.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource });
      }
    };
  }

  onDelete = (key) => {
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
  }

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King`,
      des: 'It is a long established fact that a reader will be distracted by the readable content of a page ',
      file: `txt.pdf`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  }

  render() {
    const { dataSource } = this.state;
    const columns = this.columns;
    return (
      <div className="container-fluid no-breadcrumb chapter page-wrapper white-background">
        <article className="article">
          <Row>
            <Col span={24}>
              <div className="m-b-18 button-wrap">
                <Tooltip title="Save">
                  <Button size="default" icon="save">
                  </Button>
                </Tooltip>
                <Tooltip title="Import">
                  <Button size="default" icon="import"></Button>
                </Tooltip>
                <Tooltip title="Export">
                  <Button size="default" icon="export"></Button>
                </Tooltip>
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Table bordered dataSource={dataSource} columns={columns} footer={() =>
                <div className="text-center">
                  <Button type="link" onClick={this.handleAdd}>Add New</Button>
                </div>
              } />
            </Col>
          </Row>
        </article>
      </div>
    );
  }
}

export default Page;