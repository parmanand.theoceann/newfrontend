import React from 'react';
import { Route } from 'react-router-dom';
import Master from '../masters';

const DataCenterComponents = ({ match }) => {
  
  return <Route exact   path={`${match.url}/:routeKey/:slug`} component={Master} />
}
export default DataCenterComponents;