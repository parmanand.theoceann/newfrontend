import React, { Component } from "react";
import {
  Table,
  Modal,
  Spin,
  Alert,
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  postAPICall,
  ResizeableTitle,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import DepartureReport from "../vessel-activate-list/Departure-report";
import NoonReport from "../vessel-activate-list/Noon-report-form";
import ModalAlertBox from "../../shared/ModalAlertBox";
import ArrivalReport from "../vessel-activate-list/Arrival-report";
import BunkerHandlingReport from "../vessel-activate-list/Bunker-handling-report";
import DelayStopReport from "../vessel-activate-list/Delay-stop-report";
import {
  EyeOutlined,
  LeftCircleOutlined,
} from "@ant-design/icons";
import RejectedModal from "./RejectedModal";

class VoyageRejectedList extends Component {
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  constructor(props) {
    super(props);
    const tableAction = {
      title: "Action",
      key: "action",
      width: 100,
      render: (record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => this.openView(e, record)}
            >
              <EyeOutlined />
            </span>
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["noon-verification-list"]
        ? FIELDS["noon-verification-list"]["tableheads"]
        : []
    );

    tableHeaders.push(tableAction);

    this.state = {
      loading: false,
      frmName: "NOON_VERIFICATION",
      formData: this.props.formData,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      modalInformation: {
        modalStatus: false,
        modalBody: undefined,
        modalHeader: undefined,
        modalFooter: undefined,
        modalWidth: "90%",
      },
      viewModalInformation: {},
      viewModalData: {
        vessel_name: "",
        voyage_number: "",
        imo_number: "",
        status: 0,
      },
      isViewOpen: false,
    };
  }

  componentDidMount = () => {
    this.setState({ ...this.state, loading: true }, () => this.getTableData());
  };

  getTableData = async (search = {}) => {
    try {
      this.setState({ loading: true });
      const { pageOptions } = this.state;
      let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
      let headers = {};

      if (
        search &&
        search.hasOwnProperty("searchValue") &&
        search.hasOwnProperty("searchOptions") &&
        search["searchOptions"] !== "" &&
        search["searchValue"] !== ""
      ) {
        headers = { order_by: { id: "desc" }, where: { AND: { "1": 1 } } };
        let wc = {};
        if (search["searchOptions"].indexOf(";") > 0) {
          let so = search["searchOptions"].split(";");
          wc = { OR: {} };
          so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
        } else {
          wc = { OR: {} };
          wc["OR"][search["searchOptions"]] = { l: search["searchValue"] };
        }

        headers["where"] = wc;
      }

      let qParamString = objectToQueryStringFunc(qParams);
      // voyage-manager/vessel/report-all?status=0
      // let _url = `${URL_WITH_VERSION}/noon-verification/list?${qParamString}`;
      let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/report-all?status=2&${qParamString}`;

      const request = await getAPICall(_url, headers);

      const response = await request;
      const totalRows = response && response.data ? response.data.length : 0;

      this.setState(
        {
          ...this.state,
          responseData: response && response.data ? response.data : [],
          pageOptions: {
            pageIndex: pageOptions.pageIndex,
            pageLimit: pageOptions.pageLimit,
            totalRows: totalRows,
          },
        },
        () => this.setState({ ...this.state, loading: false })
      );
    } catch (error) {
      console.log("Error While fething...", error);
    }
  };

  openView = (e, record) => {
    this.setState({
      ...this.state,
      isViewOpen: true,
      viewModalInformation: record,
    });
  };

  onCloseView = () => {
    this.setState({
      ...this.state,
      isViewOpen: false,
    });
  };

  onCloseModal = () => {
    this.setState({
      ...this.state,
      modalInformation: {
        modalStatus: false,
        modalBody: undefined,
        modalHeader: undefined,
        modalFooter: undefined,
        onCancelFunc: undefined,
        modalWidth: "90%",
      },
    });
    this.getTableData();
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`,
      "_blank"
    );
  };

  handleInputChange = (name, value) => {
    this.setState((prevState) => ({
      viewModalData: {
        ...prevState.viewModalData,
        [name]: value,
      },
    }));
  };

  handleSubmit = () => {
    const postData = {
      ...this.state.viewModalData,
      vessel_name: this.state.viewModalInformation?.vesselName || "",
      imo_number: this.state.viewModalInformation?.imo_number || "",
      voyage_number: this.state.viewModalInformation?.voyageNumber || "",
      id: this.state.viewModalInformation?.vspmId || "",
    };

    if (postData.status == true) {
      postData.status = true ? 1 : 0;
    }

    let _method = "patch";

    let suURL = `${URL_WITH_VERSION}/voyage-manager/vessel/update-report`;

    postAPICall(suURL, postData, _method, (data) => {
      // console.log('postData 2 :', postData);
    });

    this.setState(
      (prev) => ({
        ...prev,
        isViewOpen: false,
      }),
      () => {
        openNotificationWithIcon("success", "Status Updated Successfully");
        this.setState((prev) => ({
          ...prev,
          refreshCounter: this.state.refreshCounter + 1,
        }));
        this.getTableData();
      }
    );

    
  };

  render() {
    const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      sidebarVisible,
      frmName,
      formData,
      modalInformation,
      isViewOpen,
      viewModalInformation,
      viewModalData,
    } = this.state;
    const tableColumns = columns.filter((col) =>
      col && col.invisible !== "true" ? true : false
    );

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper w-100 d-block">
                <div className="form-heading">
                  <h4>
                    <span>Rejected Reports</span>
                    <span className="float-right">
                      <a
                        href="#/dynamic-vspm"
                        className="btn ant-btn-primary mb-3"
                      >
                        <LeftCircleOutlined />
                      </a>
                    </span>
                  </h4>
                </div>
              </div>
              <div className="body-wrapper">
                <div className="body-wrapper">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body common-fields-wrapper">
                        <NormalFormIndex
                          key={"key_" + frmName + "_0"}
                          formClass="label-min-height"
                          formData={formData}
                          showForm={true}
                          frmCode={"NOON_VERIFICATION"}
                          addForm={true}
                          inlineLayout={true}
                          // showButtons={[
                          //   {
                          //     id: "cancel",
                          //     title: "Search",
                          //     type: "danger",
                          //     event: (data) => this.getTableData(data),
                          //   },
                          // ]}
                          isShowFixedColumn={["...."]}
                        />
                      </div>
                    </div>
                  </article>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"voyage-manager-list-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => this.callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () =>
                          this.onActionDonwload("csv", "voyage-manager"),
                      },
                      {
                        title: "PDF",
                        event: () =>
                          this.onActionDonwload("pdf", "voyage-manager"),
                      },
                      {
                        title: "XLS",
                        event: () =>
                          this.onActionDonwload("xls", "voyage-manager"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                {loading === false ? (
                  <div key={this.state.refreshCounter}>
                    <Table
                      className="inlineTable editableFixedHeader resizeableTable"
                      bordered
                      components={this.components}
                      columns={tableColumns}
                      scroll={{ x: "max-content" }}
                      dataSource={responseData}
                      loading={loading}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                  </div>
                ) : (
                  <div className="col col-lg-12">
                    <Spin tip="Loading...">
                      <Alert
                        message=" "
                        description="Please wait..."
                        type="info"
                      />
                    </Spin>
                  </div>
                )}
              </div>
            </div>
          </div>
        </article>
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => this.callOptions(e)}
          />
        ) : undefined}

        {modalInformation.modalStatus ? (
          <ModalAlertBox
            modalStatus={modalInformation.modalStatus}
            modalHeader={modalInformation.modalHeader}
            modalBody={modalInformation.modalBody}
            modalFooter={modalInformation.modalFooter}
            onCancelFunc={() => this.onCloseModal()}
            modalWidth={modalInformation.modalWidth}
          />
        ) : undefined}
        {isViewOpen && Object.keys(viewModalInformation).length > 0 && (
          <Modal
            open={isViewOpen}
            onCancel={this.onCloseView}
            title="Send Mail"
            footer={null}
          >
            <div className="view-modal-content">
               <RejectedModal {...viewModalInformation} />
            </div>
          </Modal>
        )}
      </div>
    );
  }
}

export default VoyageRejectedList;
