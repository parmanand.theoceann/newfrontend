import React, { Component } from "react";
import {
  Form,
  Input,
  Button,
  Select,
  Upload,
  Icon,
  message,
  Checkbox,
} from "antd";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
} from "../../shared";
import { UploadOutlined } from "@ant-design/icons";
const FormItem = Form.Item;

class RejectedModal extends Component {
  constructor(props) {

      super(props);
      this.state = {
          frmName: "",
          formData: {},
          subject: 'VSPM Link rejected for position report/'+ this.props.vesselName +'/'+ this.props.voyageNumber,
          msg: 'Dear Captain User has rejected the VSPM link service for daily position report. You are requested for send daily position report as per link reporting format for each kind of report.  You are requested to submit data by using given link.',
        }
    }
    
    componentDidMount = () => {
      console.log("dddd",this.props)
    let _formData = {},
      frmName = "";
    if (this.props) {
      
      frmName = 'voyage_vessel_reject_form'
      _formData = {
        '-': {
          voyage_no: this.props.voyageNumber,
          vessel_id: this.props.vesselName,
          from: 'noreply@theoceann.com',
        //   to_date: this.props.vessel_email,
          mail_message: this.state.msg,
          mail_subject: this.state.subject,
        },
      };
    } 
    // else {
    //   if (data && data.vspm_status == "DEACTIVATED") {
    //     frmName = "voyage_vessel_activate_done_form";
    //     _formData = {
    //       "-": {
    //         vessel_id: data.vessel_name,
    //         voyage_no: data.voyage_number,
            
    //         vspm_status: "ACTIVATED",
    //         id: data.id,
    //       },
    //     };
    //   }
    // }
    this.setState({
      formData: _formData,
      frmName: frmName,
    });
  };

  saveFormData = (postData) => {
    const { frmName } = this.state;
    const { data = {} } = this.props;
    let _url = 'save';
    let _method = 'post';
    postAPICall(`${URL_WITH_VERSION}/voyage-manager/vessel/${_url}?frm=${frmName}`, postData, _method, data => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        this.fetchDataFun();
        this.closeModal();
      } else {
        openNotificationWithIcon('error', data.message);
      }
    }
    );
  };
  saveFormDataDone = (postData) => {
    let _url = 'send-email-with-link';
    let _method = 'post';
    let reportLink_noon,reportLink_arrivel,reportLink_bunker,reportLink_delay,reportLink_deputrure;
    let links=[];


    if(postData['-']['noon_report']){
      reportLink_noon = `${window.location.origin}/#/noon-report-form?&vessel_name=${postData['-'].vessel_name}&voyage_number=${postData['-'].voyage_no}`;
      reportLink_noon = encodeURI(reportLink_noon);
      links.push({
        name:'Noon Report',
        link:reportLink_noon
      })
    }
    if(postData['-']['arrivel_report']){
      reportLink_arrivel = `${window.location.origin}/#/arrival-report-form?&vessel_name=${postData['-'].vessel_name}&voyage_number=${postData['-'].voyage_no}`;
      reportLink_arrivel = encodeURI(reportLink_arrivel);
      links.push({
        name:'Arrival Report',
        link:reportLink_arrivel
      })
    }
    if(postData['-']['bunker_report']){
      reportLink_bunker = `${window.location.origin}/#/bunker-handling-report-form?&vessel_name=${postData['-'].vessel_name}&voyage_number=${postData['-'].voyage_no}`;
      reportLink_bunker = encodeURI(reportLink_bunker);
      links.push({
        name:'Bunker Handling Report',
        link:reportLink_bunker
      })
    }

    if(postData['-']['delay_report']){
      reportLink_delay = `${window.location.origin}/#/sea-delay-stop-report-form?&vessel_name=${postData['-'].vessel_name}&voyage_number=${postData['-'].voyage_no}`;
      reportLink_delay = encodeURI(reportLink_delay);
      links.push({
        name:'Delay Report',
        link:reportLink_delay
      })
    }
    if(postData['-']['dp_report']){
      reportLink_deputrure = `${window.location.origin}/#/departure-report-form?&vessel_name=${postData['-'].vessel_name}&voyage_number=${postData['-'].voyage_no}`;
      reportLink_deputrure = encodeURI(reportLink_deputrure);
      links.push({
        name:'Departer Report',
        link:reportLink_deputrure
      })
    }
    postData["-"]['links']=links
    postAPICall(`${URL_WITH_VERSION}/voyage-manager/vessel/${_url}?frm=voyage_vessel_reject_form`, postData, _method, data => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        this.fetchDataFun();
        this.closeModal();
      } else {
        openNotificationWithIcon('error', data.message);
      }
    }
    );
  };
  render() {
    const { frmName, formData } = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  formData={formData}
                  showForm={true}
                  frmCode={frmName}
                  addForm={true}
                  showButtons={frmName == "voyage_vessel_reject_form" ? [
                    {
                      id: 'save',
                      title: 'Send',
                      type: 'primary',
                      event: data => {
                        this.saveFormDataDone(data);
                      },
                    },
                  ] :
                    [{
                      id: 'save',
                      title: 'Save',
                      type: 'primary',
                      event: data => {
                        this.saveFormData(data);
                      },
                    },
                    ]
                  }
                  inlineLayout={true}
                />

                
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default RejectedModal;
