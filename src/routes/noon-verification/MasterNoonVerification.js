import React, { Component } from "react";
import { Button, Table, Popconfirm, Modal, Input } from "antd";
import { SaveOutlined, DeleteOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import Tde from "../tde/Tde";
// import VesselSchedule from '../vessel-form/index';
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";

class MasterNoonVerification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      columns: [
        {
          title: "Voyage No",
          dataIndex: "voyage_number",
          key: "voyage_number",
        },
        {
          title: "Vessel Code",
          dataIndex: "vessel_code",
          key: "vessel_code",
        },
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "vessel_name",
        },
        {
          title: "Type code",
          dataIndex: "type_code",
          key: "type_code",
        },
        {
          title: "Voyage Status",
          dataIndex: "voyage_status",
          key: "voyage_status",
        },
        {
          title: "Vessel Fleet",
          dataIndex: "vessel_fleet",
          key: "vessel_fleet",
        },
        {
          title: "Owner Ship",
          dataIndex: "owner_ship",
          key: "owner_ship",
        },
        {
          title: "Vessel DWT",
          dataIndex: "vessel_dwt",
          key: "vessel_dwt",
        },
        {
          title: "Commence Date",
          dataIndex: "commence_date",
          key: "commence_date",
        },
        {
          title: "Complete Date",
          dataIndex: "complete_date",
          key: "complete_date",
        },
        {
          title: "Tools",
          key: "action",
          render: (data) => {
            return (
              <div className="editable-row-operations">
                <span className="iconWrapper">
                  <SaveOutlined
                    onClick={(e) => this.redirectToAdd(e, data.imo_no)}
                  />
                </span>
                <span className="iconWrapper">
                  <DeleteOutlined
                    onClick={(e) => this.redirectToAdd(e, data.imo_no)}
                  />
                </span>
                <span className="iconWrapper">
                  <input type="checkbox" name="" id="" checked={false} />
                </span>
              </div>
            );
          },
        },
      ],
      responseData: [],
      voageRes: [],
      vesselRes: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
    };
  }

  componentDidMount = () => {
    this.fetchData();
  };
  fetchData = async () => {
    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let new_array = [];
    let _voyageurl = `${URL_WITH_VERSION}/voyage-manager/list?p=1&l=100`;
    let _vesselurl = `${URL_WITH_VERSION}/vessel/list?p=1&l=100`;
    const voyageResponse = await getAPICall(_voyageurl);
    const vesselResponse = await getAPICall(_vesselurl);
    const voyageResponseaData = await voyageResponse["data"];
    const vesselResponseData = await vesselResponse["data"];

    if (voyageResponseaData && voyageResponseaData.length > 0) {
      voyageResponseaData.map((voyage) => {
        vesselResponseData.map((vessel) => {
          if (voyage.vessel_id == vessel.vessel_id) {
            let obj = {
              voyage_number: voyage.voyage_number,
              vessel_name: vessel.vessel_name,
              vessel_code: voyage.vessel_code,
              type_code: vessel.type_code,
              vessel_dwt: vessel.vessel_dwt,
              voyage_status:
                voyage.voyage_number_status != undefined
                  ? voyage.voyage_number_status.split(",")[1]
                  : "",
              owner_ship: vessel.owner_ship_name,
              commence_date: voyage.commence_date,
              complete_date: voyage.completing_date,
              vessel_fleet: vessel.vessel_fleet,
            };
            new_array.push(Object.assign(obj));
          }
        });
      });
    }
    this.setState({
      responseData: new_array,
      loading: false,
    });
  };
  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/v1/voyage-manager/list?p=2&l=20`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${id}`);
      const respData = await response["data"];
      this.setState(
        { ...this.state, isAdd: false, formDataValues: respData },
        () => this.setState({ ...this.state, isVisible: true })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/tde/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  //resizing function
  handleResize =
    (index) =>
    (e, { size }) => {
      this.setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onActionDonwload = (downType, pageType) =>
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?t=${pageType}`,
      "_blank"
    );

  render() {
    const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isAdd,
      isVisible,
      formDataValues,
      sidebarVisible,
    } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Noon List</span>
                  </h4>
                </div>
              </div>
              {/* <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"transaction-summary-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => this.callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => this.onActionDonwload("csv", "transaca"),
                      },
                      {
                        title: "PDF",
                        event: () => this.onActionDonwload("pdf", "transaca"),
                      },
                      {
                        title: "XLS",
                        event: () => this.onActionDonwload("xlsx", "transaca"),
                      },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div> */}
              <div>
                <Table
                  rowKey={(record) => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  scroll={{ x: "max-content" }}
                  // scroll={{ y: 370 }}
                  components={this.components}
                  columns={tableColumns}
                  size="small"
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default MasterNoonVerification;
