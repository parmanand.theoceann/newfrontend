import React, { Component } from "react";
import {
  Button,
  Table,
  
  Popconfirm,
  Modal,
  Input,
  Menu,
  Dropdown,
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import Tde from "../tde/Tde";
// import VesselSchedule from '../vessel-form/index';
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import ReportingForm from "../../routes/reporting-form/ReportingForm";
import Arrival from "../../routes/reporting-form/components/Arrival";
import NoonReport from "../../routes/reporting-form/components/NoonReport";
import { DeleteOutlined,EditOutlined , DownOutlined} from "@ant-design/icons";

class ActivateVoyageListed extends Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: "Action",
      key: "action",
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => this.redirectToAdd(e, record)}
            >
        <EditOutlined />

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => this.onRowDeletedClick(record.id)}
              >
              <DeleteOutlined/>
              </Popconfirm>
            </span>
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["noon-verification-list"]
        ? FIELDS["noon-verification-list"]["tableheads"]
        : []
    );

    tableHeaders.push(tableAction);
    this.state = {
      loading: false,
      noonLoading: false,
      noonColumns: tableHeaders,
      noonResponseData: [],
      reportData: {},
      currentVoygeNumber: "",
      columns: [
        {
          title: "Voyage No",
          dataIndex: "voyage_number",
          key: "voyage_number",
        },
        {
          title: "Vessel Code",
          dataIndex: "vessel_code",
          key: "vessel_code",
        },
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "vessel_name",
        },
        {
          title: "Type code",
          dataIndex: "type_code",
          key: "type_code",
        },
        {
          title: "Voyage Status",
          dataIndex: "voyage_number_status",
          key: "voyage_number_status",
        },
        {
          title: "Vessel Fleet",
          dataIndex: "vessel_fleet",
          key: "vessel_fleet",
        },
        {
          title: "Owner Ship",
          dataIndex: "owner_ship_name",
          key: "owner_ship_name",
        },
        {
          title: "Vessel DWT",
          dataIndex: "vessel_dwt",
          key: "vessel_dwt",
        },
        {
          title: "Commence Date",
          dataIndex: "commence_date",
          key: "commence_date",
        },
        {
          title: "Complete Date",
          dataIndex: "completing_date",
          key: "completing_date",
        },
        {
          title: "Action",
          key: "action",
          render: (data) => {
            const reportingformdropdown = (
              <Menu>
                <Menu.Item
                  onClick={() => {
                    // this.setState({
                    //   ...this.state,
                    //   DepartureCOSP: true,
                    // });
                    this.fetchNoonData(data, "departure");
                  }}
                >
                  {"Departure/COSP"}
                </Menu.Item>

                <Menu.Item
                  onClick={() => {
                    // this.setState({
                    //   ...this.state,
                    //   Arrival: true,
                    // });
                    this.fetchNoonData(data, "arrival");
                  }}
                >
                  {"Arrival/EOSP"}
                </Menu.Item>

                <Menu.Item
                  onClick={() => {
                    // this.setState({
                    //   ...this.state,
                    //   NoonReport: true,
                    // });
                    this.fetchNoonData(data, "noon");
                  }}
                >
                  {"Noon Report"}
                </Menu.Item>
              </Menu>
            );
            return (
              <Dropdown overlay={reportingformdropdown}>
                <span
                  className="ant-dropdown-link text-dark"
                  onClick={(e) => {
                    e.preventDefault();
                    this.setState({
                      ...this.state,
                      currentVoygeNumber: data,
                    });
                  }}
                >
                  Report Type <DownOutlined />
                </span>
              </Dropdown>
            );
          },
        },
      ],
      responseData: [],
      voageRes: [],
      vesselRes: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      DepartureCOSP: false,
      Arrival: false,
      NoonReport: false,
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };
  
  fetchNoonData = async (currentData, type) => {
  let  arr  = false;
  let  dep = false; 
  let non = false;
  let _url = `${URL_WITH_VERSION}/noon-verification/active/list?vno=${currentData.voyage_number}`;
  const response = await getAPICall(_url);
  const data = await response;
  let dataArr = data && data.data ? data.data : [];
  let state = { noonLoading: false };

   if (type == "arrival") {
      
      arr = true;
    } else if (type == "departure") {
     
      dep = true;
    } else if (type == "noon") {
      
      non = true;
    }

    this.setState({
      ...this.state,
      Arrival:arr,
      DepartureCOSP:dep,
      NoonReport:non,
      noonLoading: true,
      noonResponseData: [],
      reportData: {},
    },()=>{
      this.setState({
        ...this.state,
        reportData: {
          maininformation: {
            vessel_id: currentData.vessel_id,
            voyage_no: currentData.voyage_number,
          },
        },
      },()=>{

      
        this.setState({
          ...this.state,
          ...state,
          noonResponseData: dataArr.length > 0 ? dataArr : [],
          noonLoading: false,
        });
      });
    });


    
 
    
   
  };





  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }
      if (wc.hasOwnProperty("OR")) {
        delete wc["OR"]["voyage_number_status"];
      }
      headers["where"] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/active/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${id}`);
      const respData = await response["data"];
      this.setState(
        { ...this.state, isAdd: false, formDataValues: respData },
        () => this.setState({ ...this.state, isVisible: true })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/tde/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        this.getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  //resizing function
  handleResize = (index) => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions } = this.state;


    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }
  handleCancelDepartureCOSP = () => {
    this.setState({ DepartureCOSP: false });
  };
  handleCanceArrival = () => {
    this.setState({ Arrival:false });
  };
  handleCanceNoonReport = () => {
    this.setState({  NoonReport: false });
  };

  render() {
    const {
      noonColumns,
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isAdd,
      isVisible,
      formDataValues,
      sidebarVisible,
      noonResponseData,
      reportData,
    } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    const noonTableColumns = noonColumns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>{"Activate Voyage Listed"}</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"transaction-summary-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => this.callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => this.onActionDonwload("csv", "transaca"),
                      },
                      {
                        title: "PDF",
                        event: () => this.onActionDonwload("pdf", "transaca"),
                      },
                      {
                        title: "XLS",
                        event: () => this.onActionDonwload("xlsx", "transaca"),
                      },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>
              <div>
                <Table
                  rowKey={(record) => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  scroll={{ x: "max-content" }}
                  // scroll={{ y: 370 }}
                  components={this.components}
                  columns={tableColumns}
                  size="small"
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
                <Modal
                  title={false}
                 open={this.state.DepartureCOSP}
                  width="95%"
                  onCancel={this.handleCancelDepartureCOSP}
                  footer={false}
                >
                  <ReportingForm key={reportData.maininformation} InPopupMode={true} reportData={reportData} />
                </Modal>
                <Modal
                  title={false}
                 open={this.state.Arrival}
                  width="95%"
                  onCancel={this.handleCanceArrival}
                  footer={false}
                >
                  <Arrival key={reportData.maininformation} InPopupMode={true} reportData={reportData} />
                </Modal>
                <Modal
                  title={false}
                 open={this.state.NoonReport}
                  width="95%"
                  onCancel={this.handleCanceNoonReport}
                  footer={false}
                >
                  <NoonReport key={reportData.maininformation} InPopupMode={true} reportData={reportData} />
                </Modal>
              </div>
            </div>
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>{"Nool Verification List"}</span>
                  </h4>
                </div>
              </div>

              <div>
                <Table
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  components={this.components}
                  columns={noonTableColumns}
                  scroll={{ x: "max-content" }}
                  dataSource={noonResponseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ActivateVoyageListed;
