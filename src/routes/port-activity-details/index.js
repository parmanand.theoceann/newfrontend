import React, { Component } from 'react';
import PortActivityDetail from '../../shared/components/PortActivityDetail';

class PortActivityDetails extends Component {
    render() {
        return (
            <>
                <PortActivityDetail data={this.props.data} editData={this.props.editData} />
            </>
        )
    }
}

export default PortActivityDetails;