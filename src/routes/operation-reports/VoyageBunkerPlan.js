import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';

import html2canvas from "html2canvas";
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);


    const formReportdata = {}

    this.state = {
      ifo: [],
      mgo: [],
      lsmgo: [],
      ulsfo: [],
      vlsfo: [],
      summary: Object.assign([], this.props.summary || []),
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }



  }

  componentDidMount = () => {
    const { formReportdata } = this.state;
    formReportdata.portitinerary&&formReportdata.portitinerary.length>0&&formReportdata.portitinerary.map((item, id) => {
      formReportdata.bunkerdetails&&formReportdata.bunkerdetails.length>0&&formReportdata.bunkerdetails.map((el, id) => {
        if (item.port === el.port) {
          el['avgval'] = item['tsd'];
          el['tpd'] = item['t_port_days']
        }
      })
    })
    this.assignValues();
  }

  assignValues = () => {
    const { formReportdata } = this.state;

    let IFO = []
    let MGO = []
    let LSMGO = []
    let VLSFO = []
    let ULSFO = []
    let SUMMERY = []

    let arr = [], ifo = 0.0000, pc_ifo = 0.0000,
      mgo = 0.0000, pc_mgo = 0.0000,
      lsmgo = 0.000, pc_lsmgo = 0.0000,
      vlsfo = 0.0000, pc_vlsfo = 0.0000,
      ulsfo = 0.0000, pc_ulsfo = 0.0000

      this.state.formReportdata.bunkerdetails&&this.state.formReportdata.bunkerdetails.length>0&&this.state.formReportdata.bunkerdetails.map((e, idx) => {
      let num1 = Number(e.avgval) > 0 ? Number(e.avgval) : 1;
      let num2 = Number(e.tpd) > 0 ? Number(e.tpd) : 1
      IFO.push({
        portname: e.port,
        sea_cons: parseFloat(e.ifo ? e.ifo : 0.00).toFixed(2),
        avg_sea_cons: parseFloat(e.ifo ? e.ifo / num1 : 0.00).toFixed(2),
        arob_ifo: parseFloat(e.arob_ifo ? e.arob_ifo : 0.00).toFixed(2),
        pc_ifo: parseFloat(e.pc_ifo ? e.pc_ifo : 0.00).toFixed(2),
        panned: parseFloat(e.pc_ifo ? e.pc_ifo / num2 : 0.00).toFixed(2),
        recieved: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(2),
        r_ifo: parseFloat(e.r_ifo ? e.r_ifo : 0.00).toFixed(2),
        price: parseFloat(e.price ? e.price : 0.00).toFixed(2),
        dr_ifo: parseFloat(e.dr_ifo ? e.dr_ifo : 0.00).toFixed(2),
        select: 0,
      })
      MGO.push({
        portname: e.port,
        sea_cons: parseFloat(e.mgo ? e.mgo : 0.00).toFixed(2),

        avg_sea_cons: parseFloat(e.mgo ? e.mgo / num1 : 0.00).toFixed(2),
        arob_mgo: parseFloat(e.arob_mgo ? e.arob_mgo : 0.00).toFixed(2),
        pc_mgo: parseFloat(e.pc_mgo ? e.pc_mgo : 0.00).toFixed(2),

        panned: parseFloat(e.pc_mgo ? e.pc_mgo / num2 : 0.00).toFixed(2),
        recieved: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(2),
        r_mgo: parseFloat(e.r_mgo ? e.r_mgo : 0.00).toFixed(2),
        price: parseFloat(e.price ? e.price : 0.00).toFixed(2),
        dr_mgo: parseFloat(e.dr_mgo ? e.dr_mgo : 0.00).toFixed(2),
        select: 0,
      })
      LSMGO.push({
        portname: e.port,
        sea_cons: parseFloat(e.lsmgo ? e.lsmgo : 0.00).toFixed(2),

        avg_sea_cons: parseFloat(e.lsmgo ? e.lsmgo / num1 : 0.00).toFixed(2),
        arob_lsmgo: parseFloat(e.arob_lsmgo ? e.arob_lsmgo : 0.00).toFixed(2),
        pc_lsmgo: parseFloat(e.pc_lsmgo ? e.pc_lsmgo : 0.00).toFixed(2),

        panned: parseFloat(e.pc_lsmgo ? e.pc_lsmgo / num2 : 0.00).toFixed(2),
        recieved: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(2),
        r_lsmgo: parseFloat(e.r_lsmgo ? e.r_lsmgo : 0.00).toFixed(2),
        price: parseFloat(e.price ? e.price : 0.00).toFixed(2),
        dr_lsmgo: parseFloat(e.dr_lsmgo ? e.dr_lsmgo : 0.00).toFixed(2),
        select: 0,
      })
      VLSFO.push({
        portname: e.port,
        sea_cons: parseFloat(e.vlsfo ? e.vlsfo : 0.00).toFixed(2),

        avg_sea_cons: parseFloat(e.vlsfo ? e.vlsfo / num1 : 0.00).toFixed(2),
        arob_vlsfo: parseFloat(e.arob_vlsfo ? e.arob_vlsfo : 0.00).toFixed(2),
        pc_vlsfo: parseFloat(e.pc_vlsfo ? e.pc_vlsfo : 0.00).toFixed(2),

        panned: parseFloat(e.pc_vlsfo ? e.pc_vlsfo / num2 : 0.00).toFixed(2),
        recieved: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(2),
        r_vlsfo: parseFloat(e.r_vlsfo ? e.r_vlsfo : 0.00).toFixed(2),
        price: parseFloat(e.price ? e.price : 0.00).toFixed(2),
        dr_vlsfo: parseFloat(e.dr_vlsfo ? e.dr_vlsfo : 0.00).toFixed(2),
        select: 0,
      })
      ULSFO.push({
        portname: e.port,
        sea_cons: parseFloat(e.ulsfo ? e.ulsfo : 0.00).toFixed(2),

        avg_sea_cons: parseFloat(e.ulsfo ? e.ulsfo / num1 : 0.00).toFixed(2),
        arob_ulsfo: parseFloat(e.arob_ulsfo ? e.arob_ulsfo : 0.00).toFixed(2),
        pc_ulsfo: parseFloat(e.pc_ulsfo ? e.pc_ulsfo : 0.00).toFixed(2),

        panned: parseFloat(e.pc_ulsfo ? e.pc_ulsfo / num2 : 0.00).toFixed(2),
        recieved: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(2),
        r_ulsfo: parseFloat(e.r_ulsfo ? e.r_ulsfo : 0.00).toFixed(2),
        price: parseFloat(e.price ? e.price : 0.00).toFixed(2),
        dr_ulsfo: parseFloat(e.dr_ulsfo ? e.dr_ulsfo : 0.00).toFixed(2),
        select: 0,
      })


      ifo += Number(e.ifo)
      pc_ifo += Number(e.pc_ifo)
      pc_mgo += Number(e.pc_mgo)
      mgo += Number(e.mgo)
      lsmgo += Number(e.lsmgo)
      pc_lsmgo += Number(e.pc_lsmgo)
      vlsfo += Number(e.vlsfo)
      pc_vlsfo += Number(e.pc_vlsfo)
      ulsfo += Number(e.ulsfo)
      pc_ulsfo += Number(e.pc_ulsfo)


      // SUMMERY.push({
      //   type: 'IFO',
      //   Init_Qty: parseFloat(ifo ? ifo : 0.00).toFixed(4),
      //   inc_prc: parseFloat(e.avgseacons ? e.avgseacons : 0.00).toFixed(4),
      //   sea_consComp: parseFloat(e.arob_ulsfo ? e.arob_ulsfo : 0.00).toFixed(4),
      //   avg: parseFloat(e.pc_ulsfo ? e.pc_ulsfo : 0.00).toFixed(4),
      //   portComp: parseFloat(e.avgportcons ? e.avgportcons : 0.00).toFixed(4),
      //   spd_type: parseFloat(e.reqqty ? e.reqqty : 0.00).toFixed(4),
      //   spd_type: parseFloat(e.r_ulsfo ? e.r_ulsfo : 0.00).toFixed(4),
      //   inc_prc: parseFloat(e.price ? e.price : 0.00).toFixed(4),
      //   end_qty: parseFloat(e.dr_ulsfo ? e.dr_ulsfo : 0.00).toFixed(4),

      // })

    })


    this.setState({
      ...this.state,
      ifo: IFO,
      mgo: MGO,
      lsmgo: LSMGO,
      vlsfo: VLSFO,
      ulsfo: ULSFO,

    })
  }

  render() {

    const { formReportdata, ifo, mgo, lsmgo, ulsfo, vlsfo, summary } = this.state

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                {/* <div className="col-12 text-center">
                  <img className="reportlogo" src={formReportdata.logo} alt="No Img" crossOrigin='anonymous' />
                  <p className="sub-title m-0">{formReportdata && formReportdata.full_name ? formReportdata.full_name : " "}</p>
                </div> */}
              </div>
            </div>
            <div className="row">
              {/* <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata && formReportdata.address ? formReportdata.address : " "}</p>
                </div>
              </div> */}
            </div>

            <h4 className="font-weight-bold">Initial Bunker Data</h4>

            <h4>Summary</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Init Qty</th>
                  <th>Init Prc</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>Port Cons</th>
                  <th>Req Qty</th>

                  <th>End Qty</th>
                  <th>End Prc</th>
                </tr>
              </thead>
              <tbody>

                {summary && summary.length > 0 ? summary.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.type ? e.type : " "}</td>
                        <td>{e.initQty ? parseFloat(e.initQty).toFixed(2) : " "}</td>
                        <td>{e.initPrice ? parseFloat(e.initPrice).toFixed(2) : " "}</td>
                        <td>{e.ifo ? parseFloat(e.ifo).toFixed(2) : " "}</td>
                        <td>{e.avg_seaCons ? parseFloat(e.avg_seaCons).toFixed(2) : " "}</td>
                        <td>{e.pc_ifo ? parseFloat(e.pc_ifo).toFixed(2) : " "}</td>
                        <td>{e.recQty ? parseFloat(e.recQty).toFixed(2) : " "}</td>

                        <td>{e.endQty ? parseFloat(e.endQty).toFixed(2) : " "}</td>
                        <td>{e.endPrice ? parseFloat(e.endPrice).toFixed(2) : " "}</td>
                      </tr>
                    </>
                  )
                }) : undefined

                }


              </tbody>
            </table>

            <h4>IFO</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Port Name</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>ROB Arr</th>
                  <th>Port Cons</th>
                  <th>Avg Port Cons</th>
                  {/* <th>Planed Qty</th> */}
                  <th>Receive Qty</th>
                  {/* <th>Price</th> */}
                  <th>ROB Dpt</th>
                  {/* <th>St</th> */}
                </tr>
              </thead>
              <tbody>

                {ifo && ifo && ifo.length > 0 ? ifo.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.portname ? e.portname : " "}</td>
                        <td>{e.sea_cons ? e.sea_cons : " "}</td>
                        <td>{e.avg_sea_cons ? e.avg_sea_cons : " "}</td>
                        <td>{e.arob_ifo ? e.arob_ifo : " "}</td>
                        <td>{e.pc_ifo ? e.pc_ifo : " "}</td>
                        <td>{e.panned ? e.panned : " "}</td>
                        {/* <td>{e.recieved ? e.recieved : " "}</td> */}
                        <td>{e.r_ifo ? e.r_ifo : " "}</td>
                        {/* <td>{e.price ? e.price : " "}</td> */}
                        <td>{e.dr_ifo ? e.dr_ifo : " "}</td>
                        {/* <td>{e.select}</td> */}
                      </tr>


                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>
            <h4>MGO</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Port Name</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>ROB Arr</th>
                  <th>Port Cons</th>
                  <th>Avg Port Cons</th>
                  {/* <th>Planed Qty</th> */}
                  <th>Receive Qty</th>
                  {/* <th>Price</th> */}
                  <th>ROB Dpt</th>
                  {/* <th>St</th> */}
                </tr>
              </thead>
              <tbody>

                {mgo && mgo && mgo.length > 0 ? mgo.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.portname ? e.portname : " "}</td>
                        <td>{e.sea_cons ? e.sea_cons : " "}</td>
                        <td>{e.avg_sea_cons ? e.avg_sea_cons : " "}</td>
                        <td>{e.arob_mgo ? e.arob_mgo : " "}</td>
                        <td>{e.pc_mgo ? e.pc_mgo : " "}</td>
                        <td>{e.panned ? e.panned : " "}</td>
                        {/* <td>{e.recieved ? e.recieved : " "}</td> */}
                        <td>{e.r_mgo ? e.r_mgo : " "}</td>
                        {/* <td>{e.price ? e.price : " "}</td> */}
                        <td>{e.dr_mgo ? e.dr_mgo : " "}</td>
                        {/* <td>{e.select}</td> */}
                      </tr>


                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>
            <h4>LSMGO</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Port Name</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>ROB Arr</th>
                  <th>Port Cons</th>
                  <th>Avg Port Cons</th>
                  {/* <th>Planed Qty</th> */}
                  <th>Receive Qty</th>
                  {/* <th>Price</th> */}
                  <th>ROB Dpt</th>
                  {/* <th>St</th> */}
                </tr>
              </thead>
              <tbody>

                {lsmgo && lsmgo && lsmgo.length > 0 ? lsmgo.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.portname ? e.portname : " "}</td>
                        <td>{e.sea_cons ? e.sea_cons : " "}</td>
                        <td>{e.avg_sea_cons ? e.avg_sea_cons : " "}</td>
                        <td>{e.arob_lsmgo ? e.arob_lsmgo : " "}</td>
                        <td>{e.pc_lsmgo ? e.pc_lsmgo : " "}</td>
                        <td>{e.panned ? e.panned : " "}</td>
                        {/* <td>{e.recieved ? e.recieved : " "}</td> */}
                        <td>{e.r_lsmgo ? e.r_lsmgo : " "}</td>
                        {/* <td>{e.price ? e.price : " "}</td> */}
                        <td>{e.dr_lsmgo ? e.dr_lsmgo : " "}</td>
                        {/* <td>{e.select}</td> */}
                      </tr>


                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>
            <h4>VLSFO</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Port Name</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>ROB Arr</th>
                  <th>Port Cons</th>
                  <th>Avg Port Cons</th>
                  {/* <th>Planed Qty</th> */}
                  <th>Receive Qty</th>
                  {/* <th>Price</th> */}
                  <th>ROB Dpt</th>
                  {/* <th>St</th> */}
                </tr>
              </thead>
              <tbody>

                {vlsfo && vlsfo && vlsfo.length > 0 ? vlsfo.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.portname ? e.portname : " "}</td>
                        <td>{e.sea_cons ? e.sea_cons : " "}</td>
                        <td>{e.avg_sea_cons ? e.avg_sea_cons : " "}</td>
                        <td>{e.arob_vlsfo ? e.arob_vlsfo : " "}</td>
                        <td>{e.pc_vlsfo ? e.pc_vlsfo : " "}</td>
                        <td>{e.panned ? e.panned : " "}</td>
                        {/* <td>{e.recieved ? e.recieved : " "}</td> */}
                        <td>{e.r_vlsfo ? e.r_vlsfo : " "}</td>
                        {/* <td>{e.price ? e.price : " "}</td> */}
                        <td>{e.dr_vlsfo ? e.dr_vlsfo : " "}</td>
                        {/* <td>{e.select}</td> */}
                      </tr>


                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>

            <h4>ULSFO</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Port Name</th>
                  <th>Sea Cons</th>
                  <th>Avg Sea Cons</th>
                  <th>ROB Arr</th>
                  <th>Port Cons</th>
                  <th>Avg Port Cons</th>
                  {/* <th>Planed Qty</th> */}
                  <th>Receive Qty</th>
                  {/* <th>Price</th> */}
                  <th>ROB Dpt</th>
                  {/* <th>St</th> */}
                </tr>
              </thead>
              <tbody>

                {ulsfo && ulsfo && ulsfo.length > 0 ? ulsfo.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.portname ? e.portname : " "}</td>
                        <td>{e.sea_cons ? e.sea_cons : " "}</td>
                        <td>{e.avg_sea_cons ? e.avg_sea_cons : " "}</td>
                        <td>{e.arob_ulsfo ? e.arob_ulsfo : " "}</td>
                        <td>{e.pc_ulsfo ? e.pc_ulsfo : " "}</td>
                        <td>{e.panned ? e.panned : " "}</td>
                        {/* <td>{e.recieved ? e.recieved : " "}</td> */}
                        <td>{e.r_ulsfo ? e.r_ulsfo : " "}</td>
                        {/* <td>{e.price ? e.price : " "}</td> */}
                        <td>{e.dr_ulsfo ? e.dr_ulsfo : " "}</td>
                        {/* <td>{e.select}</td> */}
                      </tr>


                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>
            {/* 
            <h4>Bunker Requirements Summary</h4>
            <table className="table custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Port</th>
                  <th>Seq</th>
                  <th>IFO Req</th>
                  <th>MGO Req</th>
                  <th>LSMGO Req</th>
                  <th>VLSFO Req</th>
                  <th>ULSFO Req</th>
                  <th>Request Status</th>
                  <th>Procurement Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>


              </tbody>
            </table> */}

            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Starting Lube (L) :</td>
                  <td>0.00</td>

                  <td className="font-weight-bold">Received :</td>
                  <td>0.00</td>

                  <td className="font-weight-bold">Ending Lube :</td>
                  <td>0.00</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Fuel Zone Set :</td>
                  <td>Fuel zone 1</td>

                  <td className="font-weight-bold">Scrubber :</td>
                  <td>true</td>
                  <td colSpan="2"></td>
                </tr>
              </tbody>
            </table>


          </div>
        </div>
      </article>
    );
  }
}

class VoyageBunkerPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Printer',
    };

  }

  printReceipt() {
    window.print();
  }

  printDocument() {

    var quotes = document.getElementById('divToPrint');

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true
    }).then(function (canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF('p', 'mm');
      var position = 30;
      var pageData = canvas.toDataURL('image/jpeg', 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(imgData, 'PNG', 5, position + 30, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save('VoyageBunkerPlanReport.pdf');

    });
  };
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={this.printDocument}>
                        Download
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                        <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} {...this.props} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VoyageBunkerPlan;
