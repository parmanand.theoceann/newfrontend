import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <h4>Main Information</h4>
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Vessel :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">IMO :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Call Sign :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Time Zone :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Start of sea Passage :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Lat :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Long :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Voyage No. :</td>
                      <td>Value</td>

                      <td colSpan="2"></td>
                    </tr>
                  </tbody>
                </table>

                <h4>Port Information</h4>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Port :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Port ID :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Arrival/Departure :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Latitude :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Longtitute :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">DTG :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Next Port :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Port ID :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">ETA</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Cargo Information</h4>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Ttl Cargo Onboard :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Displacemnt Mt :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Deck Cargo Mt :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Hold cargo Mt :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">FW consumed :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">FW ROB :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">SLOP ROB :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">SLOP Produced :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Ballast Water :</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Vessel & Distance</h4>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Passage :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Aver RPM :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 1 Hr :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Bad Weathr hrs :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Ordered SPd :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Ave BHP :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 2 Hr :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Bad weathr Dist :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Reported spd :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">FWD dft :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 3 Hr :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Sea state :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Observe distance :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Aft drft :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 4 Hr :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Sea Direction :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Engine dist. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Mid draft :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">M/E HR :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Sea height :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Main Eng Rev. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Steaming Hr :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 1 KWHR :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Swell :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Slip% :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Air press :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 2 KWHR :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Swell Direction :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Selenity :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Air Tempt. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 3 KWHR :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Swell Height :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Density :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Gen 4 KWHR :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Wind For :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">M/E KWHR :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Wind dire :</td>
                      <td>Value</td>

                      <td colSpan="6"> :</td>
                    </tr>
                  </tbody>
                </table>

                <h4>Bunker</h4>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Type (MT)</th>
                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>ULSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Remark :</td>
                    </tr>
                    <tr>
                      <td>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the
                        1500s, when an unknown printer took a galley of type and scrambled it
                      </td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Captain :</td>
                      <td>Value</td>
                      <td className="font-weight-bold">C/E :</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class ArrivalEospReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ArrivalEospReport;
