import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import moment from 'moment';

class ComponentToPrint extends React.Component {
  render() {
    const { activityData } = this.props;
    let activity_table = activityData['port_activity'] && activityData['port_activity']['-'];
    let activity_data =  activityData['port_activity'] ? activityData['port_activity'] : ''
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">{activityData && activityData.my_company_name1?activityData.my_company_name1:" "}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{activityData && activityData.my_company_name1?activityData.my_company_name1:" "}{' '}{activityData && activityData.my_company_country?activityData.my_company_country:" "}</p>
                </div>
              </div>
            </div>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Activity</th>
                  <th>At</th>
                  <th>Cargo</th>
                  <th>BL Code</th>
                  <th>Remarks</th>
                  <th>Date From</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                {activity_table && activity_table.length > 0 && 
                activity_table.map((value, index)=>{
                  return(
                      <tr key={index}>
                        <td>{value.activity?value.activity:" "}</td>
                        <td>{value.at?value.at:" "}</td>
                        <td>{value.cargo?value.cargo:" "}</td>
                        <td>{value.bl_code?value.bl_code:" "}</td>
                        <td>{value.remarks?value.remarks:" "}</td>
                        <td>{value.date_from ? moment(value.date_from).format('DD MMMM YYYY') : '--'}</td>
                        <td>{value.time?value.time:" "}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Arrival Draft Fwd (m) :</td>
                  <td>{activity_data && activity_data.arrival_draft_fwd_m ? activity_data.arrival_draft_fwd_m : 0.0}</td>

                  <td className="font-weight-bold">Arrival Draft Aft (m) :</td>
                  <td>{activity_data && activity_data.arrival_draft_aft_m  ? activity_data.arrival_draft_aft_m : 0.0}</td>

                  <td className="font-weight-bold">Depart. Draft Fwd (m) :</td>
                  <td>{activity_data && activity_data.depart_draft_fwd_m ? activity_data.depart_draft_fwd_m : 0.0}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Depart. Draft Aft (m) :</td>
                  <td>{activity_data && activity_data.depart_draft_aft_m ? activity_data.depart_draft_aft_m : 0.0}</td>

                  <td className="font-weight-bold">Last Update D/T :</td>
                  <td>{activity_data && activity_data.last_update_dt ? activity_data.last_update_dt : 0.0}</td>
                  <td colSpan="2"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class PortActivityReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Printer',
      DATA: props
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
   const { DATA } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                      <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint activityData={DATA} ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortActivityReport;
