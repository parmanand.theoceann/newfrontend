import React, { Component } from "react";
import { PrinterOutlined } from "@ant-design/icons";
import ReactToPrint from "react-to-print";
import { IMAGE_PATH } from "../../shared";
import MapComponent from "../dynamic-vspm/Newmap";
import moment from "moment";
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Printer",
      mainInformation: this.props.data,
      data: this.props.data,
      arrival_portInfo: {},
      departure_portInfo: {},
      voyageNo: this.props.voyNo,
      sumData: [],
      coordinates:[]
    };
    console.log("this.props", this.props);
  }
  componentDidMount() {
    this.filterFunc();
    this.setState((prev) => ({
      ...prev,
      coordinates: this.props.latLong
    }))
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.data !== prevProps.data ||
      this.state.report_content !== prevState.report_content
    ) {
      this.filterFunc();
    }
  }

  filterFunc() {
    const { voyageNo } = this.state;
     // Filter the data array once for type 1 reports
     const typeArrival = this.props.data?.allReport.filter(
      (obj) => obj.report_type === 1
    );
    if (typeArrival.length > 0) {
      this.setState((prev) => ({
        ...prev,
        arrival_portInfo: typeArrival[0].report_content.portInformation,
      }));
    }

    // Filter the data array once for type 4 reports
    const typeDeparture = this.props.data?.allReport.filter(
      (obj) => obj.report_type === 4
    );

    if (typeDeparture.length > 0) {
      this.setState((prev) => ({
        ...prev,
        departure_portInfo: typeDeparture[0].report_content.portInformation,
      }));
    }

    const filterSumData = this.props.sumData.filter(
      (obj) => obj.voyageNo === this.props.voyNo
    );
    this.setState((prev) => ({
      ...prev,
      sumData: filterSumData[0],
    }));
  }

  render() {
    const { data, sumData, coordinates } = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <div className="signin-logo mb-2 text-center">
                    <div className="middel-text">
                      <img src={IMAGE_PATH + "theoceann.png"} height="50" />
                      <span>Theoceann</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Maritime Info Lab Pvt Ltd, Hongkong</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Vessel Name</td>
                  <td>{sumData?.vesselName ? sumData?.vesselName : "-"}</td>
                  <td className="font-weight-bold">IMO No. </td>
                  <td>{sumData?.vessleImo ? sumData?.vessleImo : "-"}</td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Arrival Port</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.arrival
                      ? sumData?.arrivalandDeparturePort?.arrival
                      : "-"}
                  </td>
                  <td className="font-weight-bold">Arrival Date</td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.arrival
                      ? moment(
                          sumData?.arrivalandDeparturePortDate?.arrival
                        ).format("DD-MM-YYYY HH:mm:ss")
                      : "-"}
                  </td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Departure Port</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.departure == "No data"
                      ? "-"
                      : sumData?.arrivalandDeparturePort?.departure}
                  </td>

                  <td className="font-weight-bold">Departure Date</td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.departure ===
                    "No data"
                      ? "-"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.departure
                        ).format("DD-MM-YYYY HH:mm:ss")}
                  </td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Report Date</td>
                  <td>{moment().format("DD-MM-YYYY HH:mm")}</td>
                  <td className="font-weight-bold">VSPM ID</td>
                  <td>
                    {sumData?.vspm_id ? sumData?.vspm_id.join(", ") : "-"}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Report type</td>
                  <td>
                    {this.props.type}
                  </td>

                  <td className="font-weight-bold">Passage</td>
                  <td colSpan="4">
                    {this.props.data.passageType
                      ? this.props.data.passageType
                      : "-"}
                  </td>
                </tr>
              </tbody>
            </table>

            

            <h3 className="font-weight-bold text-primary text-center mb-3 mt-3">
              Voyage Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  {/* <th colSpan="2" className="text-center">
                    Distance
                  </th> */}
                  {/* <th colSpan="2" className="text-center">
                    Total Time
                  </th> */}
                  <th colSpan="5" className="text-center"></th>
                  <th colSpan="5" className="text-center">
                    Total cons. In Port/ At sea
                  </th>
                </tr>
                <tr>
                  <th>S/N</th>
                  <th>Arr/Dep port</th>
                  <th>Date time</th>
                  <th>Distance</th>
                  <th>Total time</th>
                  <th>ULSFO</th>
                  <th>VLSFO</th>
                  <th>IFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.arrival === "No data"
                      ? "N/A"
                      : sumData?.arrivalandDeparturePort?.arrival}{" "}
                    -{" "}
                    {sumData?.arrivalandDeparturePort?.departure === "No data"
                      ? "N/A"
                      : sumData?.arrivalandDeparturePort?.departure}
                  </td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.arrival === "No data"
                      ? "N/A"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.arrival
                        ).format("DD-MM-YYYY HH:mm")}{" "}
                    -{" "}
                    {sumData?.arrivalandDeparturePortDate?.departure ===
                    "No data"
                      ? "N/A"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.departure
                        ).format("DD-MM-YYYY HH:mm")}
                  </td>
                  <td>{sumData?.vesselAndDistance}</td>
                  <td>{sumData?.steamHr}</td>
                  <td>{sumData?.ulsfo}</td>
                  <td>{sumData?.vlsfo}</td>
                  <td>{sumData?.ifo}</td>
                  <td>{sumData?.lsmgo}</td>
                  <td>{sumData?.mgo}</td>
                </tr>

                <tr>
                  <th className="text-center" colSpan="3">
                    Total
                  </th>
                  <td>{sumData?.totalDist}</td>
                  <td>0</td>
                  <td className="text-center" colSpan={5}>
                    <b>
                      {parseFloat(
                        sumData?.ulsfo +
                          sumData?.vlsfo +
                          sumData?.ifo +
                          sumData?.lsmgo +
                          sumData?.mgo
                      ).toFixed(2)}
                    </b>
                  </td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Total
            </h3>

            <table className="table border-table table-striped table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Total time at sea</td>
                  <td>
                    {this.props.data?.totalSteamingHr} <b>Hr</b>
                  </td>
                  <td className="font-weight-bold">ULSFO consumed</td>
                  <td>{sumData?.ulsfo}</td>
                  <td className="font-weight-bold">VLSFO consumed</td>
                  <td>{sumData?.vlsfo}</td>
                </tr>
                <tr>
                  <td className="font-weight-bold">IFO consumed</td>
                  <td>{sumData?.ifo}</td>
                  <td className="font-weight-bold">LSMGO consumed</td>
                  <td>{sumData?.lsmgo}</td>
                  <td className="font-weight-bold">MGO consumed</td>
                  <td colSpan="6">{sumData?.mgo}</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Speed Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th></th>
                  <th>Total distance</th>
                  <th>Unit</th>
                  <th>Bad weather dist.</th>
                  <th>Unit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Distance sailed overall</th>
                  <td>{this.props.data?.totalDist}</td>
                  <td>miles</td>
                  <td>{this.props.data?.badWeatherDist}</td>
                  <td>miles</td>
                </tr>

                <tr>
                  <th>Time at sea</th>
                  <td>{this.props.data?.totalSteamingHr}</td>
                  <td>hr</td>
                  <td>{this.props.data?.badWeatherHr}</td>
                  <td>hr</td>
                </tr>

                <tr>
                  <th>Performancd SPD</th>
                  <td>{parseFloat(this.props.data?.avgSpeed).toFixed(2)}</td>
                  <td>kt</td>
                  <td>{parseFloat(this.props.data?.avgSpeed).toFixed(2)}</td>
                  <td>kt</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              GHG* Emission Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th rowSpan="2"></th>
                  <th colSpan="5" className="text-center">
                    Fuel consumption summary
                  </th>
                </tr>
                <tr>
                  <th>ULSFO</th>
                  <th>VLSFO</th>
                  <th>IFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>M/E Propuslion</th>
                  <td>{parseFloat(this.props.data?.me_ulsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_vlsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_ifo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_lsmgo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_mgo).toFixed(2)}</td>
                </tr>

                <tr>
                  <th>Maneover</th>
                  <td>
                    {parseFloat(this.props.data?.manoever_ulsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_vlsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_ifo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_lsmgo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_mgo).toFixed(2)}
                  </td>
                </tr>

                <tr>
                  <th>Generator</th>
                  <td>
                    {parseFloat(this.props.data?.generator_ulsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_vlsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_ifo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_lsmgo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_mgo).toFixed(2)}
                  </td>
                </tr>

                <tr>
                  <th>Aux Engine</th>
                  <td>{parseFloat(this.props.data?.aux_ulsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_vlsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_ifo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_lsmgo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_mgo).toFixed(2)}</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Map on Itinerary
            </h3>
            {/* <p className="text-center">
              <b>Itinerary:</b>Haldia - Kuantan
            </p> */}
            <div className="row">
              <div className="col-12" style={{minHeight: '450px', overflow: 'hidden'}}>
                <MapComponent type="voyagePerformance" coordinates={coordinates} />
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class VoyagePerformanceReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Printer",
      voyNo: this.props.voyNo,
      sumData: this.props.sumData,
    };
    console.log("Voyage performance props", this.props);
  }

  printReceipt() {
    window.print();
  }

  render() {
    const filterDepartureData = this.props.data?.allReport?.filter(obj => obj.report_type === 4);
    const firstDeparture = filterDepartureData[0];

    const departureLat = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_lat) || '';
    const departureLong = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_long) || '';

    const departureLongLat = [departureLong, departureLat]

    const filterArrivaleData = this.props.data?.allReport?.filter(obj => obj.report_type === 1);
    const lastArrivaldata = filterArrivaleData[filterArrivaleData.length - 1];

    const filterNoonData = this.props.data?.allReport?.filter(obj => obj.report_type === 5);

    const noonLat = filterNoonData?.map((e) =>  Number.parseFloat(e.report_content?.portInformation?.noon_lat))
    const noonLong = filterNoonData?.map((e) => Number.parseFloat(e.report_content?.portInformation?.noon_main_long))

    const mergredNoonArray = noonLong.map((value, index) => [value, noonLat[index]]);
    const arrivalLat = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_lat) || '';
    const arrivalLong = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_long) || '';

    const arrivalLongLat = [arrivalLong, arrivalLat]

    const startDestination = [departureLongLat, ...mergredNoonArray, arrivalLongLat]

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                type={this.props.type}
                latLong={startDestination}
                data={this.props.data}
                sumData={this.props.sumData}
                voyNo={this.props.voyNo}
                ref={(el) => (this.componentRef = el)}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VoyagePerformanceReport;
