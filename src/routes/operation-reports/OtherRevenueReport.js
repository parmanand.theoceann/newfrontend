import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
                <tbody>
                    <tr>
                        <td className="font-weight-bold">Vessel</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Voyage No.</td>
                        <td>Value</td>
                    </tr>
                </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vendor</th>
                  <th>Description</th>
                  <th>Rev/Exp</th>
                  <th>Inv No.</th>
                  <th>Inv Date</th>
                  <th>Due Date</th>
                  <th>Curr</th>
                  <th>Exch Rate</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Amount</th>
                  <th>Amt USD</th>
                  <th>Code</th>
                  <th>St</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
                <tbody>
                    <tr>
                        <td className="font-weight-bold">Total Revenue USD</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Total Expense USD</td>
                        <td>Value</td>
                    </tr>
                </tbody>
            </table>

           <h4>Other Rev/exp Invoice summary</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vessel</th>
                  <th>Voyage No.</th>
                  <th>Decription</th>
                  <th>Exp/Rev</th>
                  <th>Invoice Date</th>
                  <th>Due Date</th>
                  <th>Inv No.</th>
                  <th>Total Amount</th>
                  <th>INV Status</th>
                  <th>TDE Status.</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            
          </div>
        </div>
      </article>
    );
  }
}

class OtherRevenueReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default OtherRevenueReport;
