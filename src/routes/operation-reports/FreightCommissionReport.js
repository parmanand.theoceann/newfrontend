import React, { forwardRef, useRef, useState, useEffect } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import { Modal, Spin } from "antd";
import Email from '../../components/Email';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import html2canvas from "html2canvas";

const ComponentToPrint = forwardRef((props, ref) => {

  const [formReportdata, setFormReportdata] = useState(Object.assign({}, props.data || {}))


  return <article className="article toolbaruiWrapper" ref={ref}>
    <div className="box box-default" id="divToPrint">
      <div className="box-body">
        <div className="invoice-inner-download mt-3">
          <div className="row">
            <div className="col-12 text-center">
              <img className="reportlogo" src={formReportdata.my_company_logo} alt="no image" crossOrigin="anonymous" />

              <p className="sub-title m-0">
                {formReportdata && formReportdata.my_company_name
                  ? formReportdata.my_company_name
                  : " "}
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-10 mx-auto">
            <div className="text-center invoice-top-address">
              <p>
                {formReportdata && formReportdata.my_company_address
                  ? formReportdata.my_company_address
                  : " "}
              </p>
            </div>
          </div>
        </div>

        <table className="table custom-table-bordered tc-table">
          <tbody>
            <tr>
              <td className="font-weight-bold">Vesssel</td>
              <td>
                {formReportdata && formReportdata.vessel_name
                  ? formReportdata.vessel_name
                  : " "}
              </td>
              <td className="font-weight-bold">Invoice No.</td>
              <td>
                {formReportdata && formReportdata.invoice_no
                  ? formReportdata.invoice_no
                  : " "}
              </td>
              <td className="font-weight-bold">Payment Terms</td>
              <td>
                {formReportdata && formReportdata.payment_terms
                  ? formReportdata.payment_terms
                  : " "}
              </td>
            </tr>

            <tr>
              <td className="font-weight-bold">Broker</td>
              <td>
                {formReportdata && formReportdata.broker
                  ? formReportdata.broker
                  : " "}
              </td>
              <td className="font-weight-bold">Recieved Date</td>
              <td>
                {formReportdata && formReportdata.recieved_date
                  ? formReportdata.recieved_date
                  : " "}
              </td>
              <td className="font-weight-bold">Due Date</td>
              <td>
                {formReportdata && formReportdata.due_date
                  ? formReportdata.due_date
                  : " "}
              </td>
            </tr>

            <tr>
              <td className="font-weight-bold">Currency</td>
              <td>
                {formReportdata && formReportdata.currency_name
                  ? formReportdata.currency_name
                  : " "}
              </td>
              <td className="font-weight-bold">Status</td>
              <td>
                {formReportdata && formReportdata.com_status
                  ? formReportdata.com_status
                  : " "}
              </td>
              <td className="font-weight-bold">PO Number</td>
              <td>
                {formReportdata && formReportdata.po_number
                  ? formReportdata.po_number
                  : " "}
              </td>
            </tr>

            <tr>
              <td className="font-weight-bold">Invoice Type</td>
              <td>
                {formReportdata && formReportdata.invoice_type
                  ? formReportdata.invoice_type
                  : " "}
              </td>
              <td className="font-weight-bold">Invoice Date</td>
              <td>
                {formReportdata && formReportdata.invoice_date
                  ? formReportdata.invoice_date
                  : " "}
              </td>
              <td className="font-weight-bold">Remittance Bank</td>
              <td>
                {formReportdata && formReportdata.remittance_bank_name
                  ? formReportdata.remittance_bank_name
                  : " "}
              </td>
            </tr>

            <tr>
              {/* <td className="font-weight-bold">Filters</td>
                        <td>{formReportdata.filter}</td>
                        <td className="font-weight-bold">Invoice (S)</td>
                        <td>{formReportdata.r_bank}</td> */}
            </tr>
          </tbody>
        </table>

        <table className="table custom-table-bordered tc-table">
          <thead>
            <tr>
              <th>Voyage Number</th>
              <th>Description</th>
              <th>Cargo ID</th>
              <th>Counter Party</th>
              <th>Amount</th>
              <th>Currency (USD)</th>
              <th>Invoice %</th>
              <th>Commission %</th>
              <th>Commission Amount</th>
              <th>Select</th>
            </tr>
          </thead>
          <tbody>
            {formReportdata["..."] && formReportdata["..."].length > 0 ? formReportdata["..."].map(
              (e, idx) => {
                return (
                  <>
                    <tr key={idx}>
                      <td>
                        {e.voyage_number ? e.voyage_number : " "}
                      </td>
                      <td>{e.description ? e.description : " "}</td>
                      <td>{e.cargo_id ? e.cargo_id : " "}</td>
                      <td>
                        {e.counter_Party_name
                          ? e.counter_Party_name
                          : " "}
                      </td>
                      <td>{e.amount ? e.amount : " "}</td>
                      <td>{e.currency ? e.currency : " "}</td>
                      <td>{e.invoice ? e.invoice : " "}</td>
                      <td>{e.commission ? e.commission : " "}</td>
                      <td>
                        {e.commission_amount
                          ? e.commission_amount
                          : " "}
                      </td>
                      <td>{e["is_select"] == 1 ? "Yes" : "No"}</td>
                    </tr>
                  </>
                );
              }
            ) : undefined}
          </tbody>
        </table>
        <div style={{ display: "flex" }}>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr>
                <th>Remark</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{formReportdata.remarks}</td>
              </tr>
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr>
                <th>Total Amount</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {formReportdata["..."] &&
                    formReportdata["..."] &&
                    formReportdata["..."].length > 0
                    ? formReportdata["..."].reduce(
                      (total, e) => total + Number(e.commission_amount),
                      0
                    ).toFixed(2)
                    : undefined}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </article>;
})

const FreightCommissionReport = (props) => {
  const [pdfData, setPdfData] = useState();
  const [userInput, setUserInput] = useState()
  const [emailModal, setEmailModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [mailTitlePayload, setMailTitlePayload] = useState({})
  const [name, setName] = useState('Printer')
  const [titleArray,setTitleArray] = useState([])

  const componentRef = useRef()

  

  const printReceipt = () => {
    window.print();
  }

  
  
  useEffect(() => {
  
    // const {vessel_name,broker,invoice_no,com_status,po_number}=props.data;
    // const tempArray= [vessel_name,broker,invoice_no,com_status,po_number];
    // setTitleArray(tempArray)
    setUserInput(props.data)
    
  }, [])
  
  
  const sendEmailReportModal = async () => {
    try {
      
      setLoading(true)
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      setLoading(false)
      setPdfData(blob)
      setEmailModal(true)
  
    } catch (error) {
      console.error('Error:', error);
      setLoading(false)
      // this.setState({ loading: false });
      // Handle errors here
    }
  };


  const printDocument = () => {
    // in this method report is downloading, but logo is not coming showing err of loading image of s3.     
    html2canvas(document.getElementById("divToPrint"), {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true
    }).then(function (canvas) {
      // download image      
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      const pdf = new jsPDF();
      const imgProps = pdf.getImageProperties(canvas);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      pdf.addImage(canvas, "PNG", 0, 0, pdfWidth, pdfHeight);
      pdf.save("frightinvoicereport.pdf");
    }).catch = (err) => {
    
    };
  }


  return (
    <div className="body-wrapper modalWrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection"></div>
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li onClick={sendEmailReportModal}>Send Email</li>
                    <li onClick={printDocument}>
                      Download
                    </li>
                    <li>
                      <ReactToPrint
                        trigger={() => (
                          <span className="text-bt">
                            <PrinterOutlined />Print
                          </span>
                        )}
                        content={() => componentRef.current}
                      />
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <ComponentToPrint ref={componentRef} data={props.data} />
          </div>
        </div>
      </article>

      {emailModal && (
        <Modal
          title="New Message"
          visible={emailModal}
          onOk={() => {
            setEmailModal(false);
          }}
          onCancel={() => {
            setEmailModal(false);
          }}
          footer={null}
        >
          {pdfData && <Email
            handleClose={
              () => {
                setEmailModal(false);
              }

            }
            attachmentFile={pdfData}

            title={window.corrector(`Freight_Commission_Report||${userInput.vessel_name}||${userInput.broker}||${userInput.invoice_no}||${userInput.com_status}||${userInput.po_number}` ) }


            // title={window.emailTitltCorrectFunction('Freight_Commission_Report',titleArray) }

          />}
        </Modal>
      )}
      {
        loading && (
          <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
            <Spin size="large" />
          </div>
        )
      }
    </div>
  );
}

export default FreightCommissionReport;
