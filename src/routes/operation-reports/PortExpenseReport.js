import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    const { portExpenseData } = this.props;
    let fda_total_agreed = 0.0
    let fda_total_quoted = 0.0
    let fda_total_diff = 0.0
    let fda_total_tax = 0.0
    let fda_total_amount = 0.0
    if(portExpenseData['--'] && portExpenseData['--'].length > 0){
      portExpenseData['--'].map(val=>{
        fda_total_quoted += parseFloat(val.total_quoted);
        fda_total_agreed += parseFloat(val.total_agreed);
        fda_total_diff += parseFloat(val.diff);
        fda_total_tax += parseFloat(val.act_tax);
        fda_total_amount += parseFloat(val.fda_amount);
      })
    }
            
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">{portExpenseData && portExpenseData.my_company_name1?portExpenseData.my_company_name1:" "}</span>
                  <p className="sub-title m-0">{portExpenseData && portExpenseData.my_company_name1?portExpenseData.my_company_name1:" "}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{portExpenseData && portExpenseData.my_company_name1? portExpenseData.my_company_name1:" "} {' '} {portExpenseData && portExpenseData.my_company_country?portExpenseData.my_company_country:" "}</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Intial Est. Expesnes</td>
                  <td>{portExpenseData && portExpenseData.intial_est_expesnes?portExpenseData.intial_est_expesnes:" "}</td>
                  <td className="font-weight-bold">Disburmnt INV</td>
                  <td>{portExpenseData && portExpenseData.disburmnt_inv?portExpenseData.disburmnt_inv:" "}</td>
                  <td className="font-weight-bold">Inv Due</td>
                  <td> - </td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Vessel</td>
                  <td>{portExpenseData && portExpenseData.vessel_name?portExpenseData.vessel_name:" "}</td>
                  <td className="font-weight-bold">PO No.</td>
                  <td>{portExpenseData && portExpenseData.po_no?portExpenseData.po_no:" "}</td>
                  <td className="font-weight-bold">Other Advance</td>
                  <td>{portExpenseData && portExpenseData.other_advance? portExpenseData.other_advance:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Vessel Code</td>
                  <td>{portExpenseData && portExpenseData.vessel_code? portExpenseData.vessel_code:" "}</td>
                  <td className="font-weight-bold">Service date</td>
                  <td> - </td>
                  <td className="font-weight-bold">FDA Status</td>
                  <td>{portExpenseData && portExpenseData.fda_status_name? portExpenseData.fda_status_name:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Voy No</td>
                  <td>{portExpenseData && portExpenseData.voy_no? portExpenseData.voy_no:" "}</td>
                  <td className="font-weight-bold">PIC (User)</td>
                  <td>{portExpenseData && portExpenseData.pic_user?portExpenseData.pic_user:" "}</td>
                  <td className="font-weight-bold">FDA Sent Date</td>
                  <td>{portExpenseData && portExpenseData.fda_sent_date?portExpenseData.fda_sent_date:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Port</td>
                  <td>{portExpenseData && portExpenseData.port?portExpenseData.port:" "}</td>
                  <td className="font-weight-bold">Agent Bank</td>
                  <td>{portExpenseData && portExpenseData.branch? portExpenseData.branch:" "}</td>
                  <td className="font-weight-bold">Inv Due</td>
                  <td> - </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Arrival</td>
                  <td>{portExpenseData && portExpenseData.arrival?portExpenseData.arrival:" "}</td>
                  <td className="font-weight-bold">PDA Adv Status</td>
                  <td>{portExpenseData && portExpenseData.pda_adv_status_name?portExpenseData.pda_adv_status_name:" "}</td>
                  <td className="font-weight-bold">Exchange Rate</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Departure</td>
                  <td>{portExpenseData && portExpenseData.departure?portExpenseData.departure:" "}</td>
                  <td className="font-weight-bold">Adavnce Sent Date</td>
                  <td>{portExpenseData && portExpenseData.adavnce_sent_date?portExpenseData.adavnce_sent_date:" "}</td>
                  <td className="font-weight-bold">FDA Base Curr</td>
                  <td>{portExpenseData.fda_base_curr_name}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Agent Short Name</td>
                  <td>{portExpenseData && portExpenseData.agent_short_name?portExpenseData.agent_short_name:" "}</td>
                  <td className="font-weight-bold">Pda Receive Date</td>
                  <td>{portExpenseData && portExpenseData.pda_receive_date?portExpenseData.pda_receive_date:" "}</td>
                  <td className="font-weight-bold">FDA Currency</td>
                  <td>{portExpenseData && portExpenseData.fda_currency_name? portExpenseData.fda_currency_name:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Agent Full Name</td>
                  <td>{portExpenseData && portExpenseData.agent_full_name1?portExpenseData.agent_full_name1:" "}</td>
                  <td className="font-weight-bold">Agreed Est Amt</td>
                  <td>{portExpenseData && portExpenseData.agreed_est_amt?portExpenseData.agreed_est_amt:" "}</td>
                  <td className="font-weight-bold">Total Amnt</td>
                  <td>{portExpenseData && portExpenseData.total_amt? portExpenseData.total_amt:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Portcall refer. No</td>
                  <td>{portExpenseData && portExpenseData.portcall_refer_no? portExpenseData.portcall_refer_no:" "}</td>
                  <td className="font-weight-bold">% Adv</td>
                  <td>{portExpenseData && portExpenseData.adv_per?portExpenseData.adv_per:" "}</td>
                  <td className="font-weight-bold">PDA Sent-Approved</td>
                  <td>{portExpenseData && portExpenseData.pda_sent_approved?portExpenseData.pda_sent_approved:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Status</td>
                  <td>{portExpenseData && portExpenseData.p_status?portExpenseData.p_status:" "}</td>
                  <td className="font-weight-bold">PDA/Adv Amt</td>
                  <td>{portExpenseData && portExpenseData.pda_adv_amt?portExpenseData.pda_adv_amt:" "}</td>
                  <td className="font-weight-bold">PDA Approved- FDA Sent</td>
                  <td>{portExpenseData && portExpenseData.da_approved_fda_sent?portExpenseData.da_approved_fda_sent:" "}</td>
                </tr>
              </tbody>
            </table>

            <h4>PDA/ADV</h4>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Cost Items</th>
                  <th>Quoted</th>
                  <th>Agreed ADV.</th>
                  <th>Diff</th>
                  <th>Axct Tax</th>
                  <th>Remark</th>
                  <th>Attchment</th>
                </tr>
              </thead>
              <tbody>
                {portExpenseData['.'] && portExpenseData['.'].length > 0 && 
                portExpenseData['.'].map((e, index)=>{
                  return(
                    <tr key={index}>
                    <td>{e.cost_item_name?e.cost_item_name:" "}</td>
                    <td>{e.quoted?e.quoted:" "}</td>
                    <td>{e.agreed_adv?e.agreed_adv:" "}</td>
                    <td>{e.diff?e.diff:" "}</td>
                    <td>{e.axct_tax?e.axct_tax:" "}</td>
                    <td>{e.remark?e.remark:" "}</td>
                    <td>{e.attachment?e.attachment:" "}</td>
                  </tr>
                  )
                })}
              </tbody>
            </table>
            <table className="table border-table table-invoice-report-colum">
              <tbody>
                <tr>
                  <td>Total</td>
                  <td>{ portExpenseData && portExpenseData['..'] && portExpenseData['..']['total_quoted']?portExpenseData['..']['total_quoted']:" "}</td>
                  <td>{ portExpenseData && portExpenseData['..'] && portExpenseData['..']['total_agreed_adv']? portExpenseData['..']['total_agreed_adv']:" "}</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['total_diff']?portExpenseData['..']['total_diff']:" "}</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['total_axct_tax']?portExpenseData['..']['total_axct_tax']:" "}</td>
                  <td colSpan="2"></td>
                </tr>
              </tbody>
            </table>
            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Our exchange</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['our_exchange']?portExpenseData['..']['our_exchange']:" "}</td>
                  <td className="font-weight-bold">Due to agent</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['due_to_agent']?portExpenseData['..']['due_to_agent']:" "}</td>
                  <td className="font-weight-bold">Advance curr</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['advance_curr_name']?portExpenseData['..']['advance_curr_name']:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Agent Exchange</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['agent_exchange']?portExpenseData['..']['agent_exchange']:" "}</td>
                  <td className="font-weight-bold">PDA Base curr</td>
                  <td>{portExpenseData && portExpenseData['..'] && portExpenseData['..']['pda_base_curr_name']?portExpenseData['..']['pda_base_curr_name']:" "}</td>
                </tr>
              </tbody>
            </table>

            <h4>FDA</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Cost Items</th>
                  <th>Ttl Quoted</th>
                  <th>Ttl Agreed</th>
                  <th>FDA Amount</th>
                  <th>Diff</th>
                  <th>Axct Tax</th>
                  <th>Attchment</th>
                  <th>Remark</th>
                </tr>
              </thead>
              <tbody>
              {portExpenseData['--'] && portExpenseData['--'].length > 0 && 
                portExpenseData['--'].map((val, i)=>{
                  return(
                    <tr key={i}>
                      <td>{val.cost_item_name?val.cost_item_name:" "}</td>
                      <td>{val.total_quoted?val.total_quoted:" "}</td>
                      <td>{val.total_agreed?val.total_agreed:" "}</td>
                      <td>{val.fda_amount?val.fda_amount:" "}</td>
                      <td>{val.diff?val.diff:" "}</td>
                      <td>{val.act_tax?val.act_tax:" "}</td>
                      <td>{val.attchment?val.attchment:" "}</td>
                      <td>{val.remark?val.remark:" "}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
            <table className="table border-table table-invoice-report-colum">
              <tbody>
                <tr>
                  <td>Total FDA</td> 
                  <td>{fda_total_quoted}</td>
                  <td>{fda_total_agreed}</td>
                  <td>{fda_total_amount}</td>
                  <td>{fda_total_diff}</td>
                  <td>{fda_total_tax}</td>
                  <td colSpan="2"></td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Inv Type</th>
                  <th>Amount (USD)</th>
                  <th>Inv No</th>
                  <th>Inv Date</th>
                  <th>Due Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                </tr>
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Total Paid</td>
                  <td>{portExpenseData['...'] && portExpenseData['...']['total_paid']}</td>
                </tr>
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Our Exchange</td>
                  <td>{portExpenseData['...'] && portExpenseData['...']['our_exchange']}</td>

                  <td className="font-weight-bold">Agent Exchange</td>
                  <td>{portExpenseData['...'] && portExpenseData['...']['agent_exchange']}</td>

                  <td className="font-weight-bold">Total FDA</td>
                  <td>{portExpenseData['...'] && portExpenseData['...']['total_fda']}</td>

                  <td className="font-weight-bold">Due to Agent</td>
                  <td>{portExpenseData['...'] && portExpenseData['...']['due_to_agent']}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class PortExpenseReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Printer',
      DATA: props.PortExpense
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    const { DATA } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                           <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint  portExpenseData={DATA}  ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortExpenseReport;
