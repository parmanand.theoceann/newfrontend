import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>
            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Estimate ID :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Voy Fixt ID :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Fixture date :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Perf Vessel :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Cargo Cont ID :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Contract Type :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">CP Form :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">CP date :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Commence :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Ops User :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Ops Voy type :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Completed :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Voyage Status :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Voyage NO :</td>
                  <td>Value</td>

                  <td colSpan="2">Commence date :</td>
                </tr>
              </tbody>
            </table>

            <h5 className="font-weight-bold">Cargo</h5>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>S/N</th>
                  <th>Cargo ID</th>
                  <th>Charterer</th>
                  <th>CP Qty</th>
                  <th>Unit</th>
                  <th>Opt %</th>
                  <th>Opt Type</th>
                  <th>Freight Type</th>
                  <th>Frt Rate</th>
                  <th>Lumpsum</th>
                  <th>Comm%</th>
                  <th>Extra Rev</th>
                  <th>Curr</th>
                  <th>Dem.Rate (Day)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold">Port Itinerary</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Port ID</th>
                  <th>Port</th>
                  <th>Funct.</th>
                  <th>Miles</th>
                  <th>Passage</th>
                  <th>STYPE</th>
                  <th>WF %</th>
                  <th>SPD</th>
                  <th>Eff-SPD</th>
                  <th>GSD</th>
                  <th>TSD</th>
                  <th>XSD</th>
                  <th>L/D QTY</th>
                  <th>L/D Rate(D)</th>
                  <th>L/D Rate(H)</th>
                  <th>L/D Term</th>
                  <th>TurnTime</th>
                  <th>P Days</th>
                  <th>Xpd</th>
                  <th>P.EXP</th>
                  <th>Total Port Days</th>
                  <th>Currency</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold">Bunker Details</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th rowSpan="2">Port ID</th>
                  <th rowSpan="2">Port</th>
                  <th rowSpan="2">F</th>
                  <th rowSpan="2">Miles</th>
                  <th rowSpan="2">Passage</th>
                  <th rowSpan="2">SPD Type</th>
                  <th rowSpan="2">SPD</th>
                  <th rowSpan="2">Arrival Date-Time</th>
                  <th rowSpan="2">Departure</th>
                  <th colSpan="4">Fuel Grade (Sea Cons. In MT)</th>
                  <th colSpan="4">Arrival ROB</th>
                  <th colSpan="4">Port Cons. Fuel</th>
                  <th colSpan="4">Received</th>
                  <th colSpan="4">DEP.ROB</th>
                </tr>
                <tr>
                  <th>IFO</th>
                  <th>VLSFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                  <th>VLSFO</th>

                  <th>IFO</th>
                  <th>LSFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                  <th>VLSFO</th>

                  <th>IFO</th>
                  <th>LSFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                  <th>VLSFO</th>

                  <th>IFO</th>
                  <th>LSFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                  <th>VLSFO</th>

                  <th>IFO</th>
                  <th>LSFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                  <th>ULSFO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold">Port Date Details</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Port ID</th>
                  <th>Port</th>
                  <th>Funct.</th>
                  <th>MILES</th>
                  <th>Passage</th>
                  <th>STYPE</th>
                  <th>SPD</th>
                  <th>WF%</th>
                  <th>TSD</th>
                  <th>XSD</th>
                  <th>Arrival Date Time</th>
                  <th>Day</th>
                  <th>T.PDays</th>
                  <th>Departure</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Total Distance</th>
                  <th>TTL Port Days</th>
                  <th>TSD</th>
                  <th>TTL Qty</th>
                  <th>GSD</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    XXXXX <span className="float-right">Miles</span>
                  </td>
                  <td>
                    XXX <span className="float-right">Days</span>
                  </td>
                  <td>
                    XXXXX <span className="float-right">Days</span>
                  </td>
                  <td>
                    XX <span className="float-right">Mt</span>
                  </td>
                  <td>
                    XXX <span className="float-right">Days</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class VoyageFixtureReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                        <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VoyageFixtureReport;
