import React, { Component } from "react";
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from "react-to-print";
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";
import { Modal , Spin } from "antd";
import html2canvas from "html2canvas";
import Email from "../../components/Email";
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const reportFormData = {};

    this.state = {
      reportFormData: Object.assign(reportFormData, this.props.data || {}),
    };
  }
  render() {
    const { reportFormData } = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">{reportFormData.logo}</span>
                  <p className="sub-title m-0">
                    {reportFormData && reportFormData.full_name
                      ? reportFormData.full_name
                      : " "}
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>
                    {reportFormData && reportFormData.address
                      ? reportFormData.address
                      : " "}
                  </p>
                </div>
              </div>
            </div>
            <table className="table  custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Vessel</td>
                  <td>
                    {reportFormData && reportFormData.vessel_name
                      ? reportFormData.vessel_name
                      : " "}
                  </td>
                  <td className="font-weight-bold">Invoice No.</td>
                  <td>
                    {reportFormData && reportFormData.invoice_no
                      ? reportFormData.invoice_no
                      : " "}
                  </td>
                  <td className="font-weight-bold">Invoice Status</td>
                  <td>
                    {reportFormData && reportFormData.inv_status_name
                      ? reportFormData.inv_status_name
                      : "N/A"}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Voyage No.</td>
                  <td>
                    {reportFormData && reportFormData.estimate_id
                      ? reportFormData.estimate_id
                      : " "}
                  </td>
                  <td className="font-weight-bold">Bill via</td>
                  <td>
                    {reportFormData && reportFormData.bill_via_name
                      ? reportFormData.bill_via_name
                      : " "}
                  </td>
                  <td className="font-weight-bold">Invoice Type</td>
                  <td>
                    {reportFormData && reportFormData.invoice_type_name
                      ? reportFormData.invoice_type_name
                      : " "}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Fixture No.</td>
                  {/* {initilly there was fixture no data from api,but as per qa team recommendation it was removed} */}
                  <td>N/A</td>
                  <td className="font-weight-bold">My Company</td>
                  <td>
                    {reportFormData && reportFormData.company_via_name
                      ? reportFormData.company_via_name
                      : " "}
                  </td>
                  <td className="font-weight-bold">Due Date</td>
                  <td>
                    {reportFormData && reportFormData.due_date
                      ? reportFormData.due_date
                      : "  "}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Vendor</td>
                  <td>
                    {reportFormData && reportFormData.vendo_name
                      ? reportFormData.vendo_name
                      : " "}
                  </td>
                  {/* <td className="font-weight-bold">Remittence Type</td>
                        <td>{reportFormData && reportFormData.remittence_type_name?reportFormData.remittence_type_name:" "}</td> */}
                  <td className="font-weight-bold">Refrence</td>
                  <td>
                    {reportFormData && reportFormData.referance
                      ? reportFormData.referance
                      : " "}
                  </td>
                  <td>{" "}</td>
                  <td>{" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoice Date</td>
                  <td>
                    {reportFormData && reportFormData.invoice_date
                      ? reportFormData.invoice_date
                      : " "}
                  </td>
                  <td className="font-weight-bold">Provision Trans No.</td>
                  <td>
                    {reportFormData && reportFormData.provision_trans_no
                      ? reportFormData.provision_trans_no
                      : " "}
                  </td>
                  <td className="font-weight-bold">P.O. Number</td>
                  <td>
                    {reportFormData && reportFormData.po_number
                      ? reportFormData.po_number
                      : " "}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Service Date</td>
                  <td>
                    {reportFormData && reportFormData.service_date
                      ? reportFormData.service_date
                      : " "}
                  </td>
                  <td className="font-weight-bold">Payment Terms</td>
                  <td>
                    {reportFormData && reportFormData.payment_term_name
                      ? reportFormData.payment_term_name
                      : " "}
                  </td>
                  <td className="font-weight-bold">Received date</td>
                  <td>
                    {reportFormData && reportFormData.recieved_date
                      ? reportFormData.recieved_date
                      : " "}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Service No.</td>
                  <td>
                    {reportFormData && reportFormData.service_no
                      ? reportFormData.service_no
                      : " "}
                  </td>

                  <td className="font-weight-bold">Rev/Exp</td>
                  <td>
                    {reportFormData && reportFormData.rev_exp_name
                      ? reportFormData.rev_exp_name
                      : " "}
                  </td>
                  <td>{" "}</td>
                  <td>{" "}</td>
                  {/* <td colSpan="2"></td> */}
                </tr>
              </tbody>
            </table>
            <h4>Other Rev/Exp</h4>
            <table className="table  custom-table-bordered tc-table">
              <thead>
                <tr>
                  <th>Decription</th>
                  <th>Code</th>

                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Amount</th>
                  <th>Type</th>
                  <th>Cargo</th>
                 
                  <th>Rebill Type</th>
                </tr>
              </thead>
              <tbody>
                {reportFormData.otherrevenueexpenses &&
                reportFormData.otherrevenueexpenses &&
                reportFormData.otherrevenueexpenses.length > 0
                  ? reportFormData.otherrevenueexpenses.map((e, idx) => {
                      return (
                        <>
                          <tr key={idx}>
                            <td>{e.description ? e.description : " "}</td>
                            <td>{e.code ? e.code : " "}</td>

                            <td>{e.quantity ? e.quantity : " "}</td>
                            <td>{e.price ? e.price : " "}</td>
                            <td>{e.amount ? e.amount : " "}</td>
                            <td>{e.type ? e.type : " "}</td>
                            <td>{e.cargo_name ? e.cargo_name : " "}</td>
                          
                            <td>{e.tco_rebill ? e.tco_rebill : " "}</td>
                          </tr>
                        </>
                      );
                    })
                  : undefined}
              </tbody>
            </table>
            <table className="table  custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Final Amount(USD)</td>
                  <td>{reportFormData["....."].total_amount}</td>
                  <td className="font-weight-bold">Total Amount(Local)</td>
                  <td>{reportFormData["....."].total_amount}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class OtherInvoiceReport extends Component {
  constructor() {
    super();
    this.state = {
      name: "Printer",
      pdfData:"",
      userInput: "",
      emailModal:false,
      loading:false,
      mailTitlePayload:{},
    };
  }

  printReceipt() {
    window.print();
  }

  printDocument() {
    var quotes = document.getElementById("divToPrint");

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true,
    }).then(function(canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF("p", "mm");
      var position = 22;
      var pageData = canvas.toDataURL("image/jpeg", 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          imgData,
          "PNG",
          5,
          position + 24,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save("OtherRevExp.pdf");
    });
  }


  sendEmailReportModal = async () => {
    try {
      
      this.setState({ loading: true });
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      
  
      this.setState({
        loading: false,
        pdfData: blob,
        emailModal: true,
      });
  
    } catch (error) {
      console.error('Error:', error);
      this.setState({ loading: false });
      // Handle errors here
    }
  };

  render() {
    
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection" />
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                    <li onClick={this.sendEmailReportModal}>Send Email</li>
                      <li>
                        <span className="text-bt" onClick={this.printDocument}>
                          {" "}
                          Download
                        </span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                          <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                ref={(el) => (this.componentRef = el)}
                data={this.props.data}
              />
            </div>
          </div>
        </article>

        {this.state.emailModal && (
        <Modal
          title="New Message"
          visible={this.state.emailModal}
          onOk={() => {
            this.setState({ emailModal: false });
          }}
          onCancel={() => {
            this.setState({ emailModal: false });
          }}
          footer={null}
        >
          {this.state.pdfData &&<Email
            handleClose={
              () => {
                this.setState(prevState=>({
                
                  emailModal:false,
                }))
                

              }
              
            }
            attachmentFile={this.state.pdfData} 
            title={window.corrector(`Other_Invoice_Report||${this.props.data.estimate_id}||${this.props.data.vessel_name}||${this.props.data.invoice_type_name}||${this.props.data.company_via_name}||${this.props.data.inv_status_name}` ) }
            
            // title={`Other_Invoice_Report|| ${this.props.data.estimate_id} || ${this.props.data.vessel_name}|| ${this.props.data.invoice_type_name} || ${this.props.data.company_via_name} || ${this.props.data.inv_status_name}`}
         
          />}
        </Modal>
      )}                   
          
        {
          this.state.loading && (
            <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
              <Spin size="large" />
            </div>
          )
        }                   

      </div>
    );
  }
}

export default OtherInvoiceReport;
