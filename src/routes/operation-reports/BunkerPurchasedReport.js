import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';
import { Modal, Spin } from "antd";
import Email from '../../components/Email';
import html2canvas from 'html2canvas';
import * as htmlToImage from 'html-to-image';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {}

    this.state = {


      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }

  }
  render() {
    const { formReportdata } = this.state
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  {/* <img
                      className="reportlogo"
                      src={company_logo}                    
                      crossOrigin="anonymous"
                    /> */}
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                  {/* <p>
                    {company_name} 
                  </p>
                  <p> {company_address}</p> */}
                </div>
              </div>
            </div>

            <table className="table custom-table-bordered tc-table">
              <tbody>
                <td className="border-0">
                  <table className="table custom-table-bordered tc-table">
                    <tbody>


                      <tr>
                        <td className="font-weight-bold">Vessel/Voy :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].vessel_id ? formReportdata['.....'].vessel_id : " "}/{formReportdata && formReportdata['.....'] && formReportdata['.....'].voyage_manager_id ? formReportdata['.....'].voyage_manager_id : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">Requerement ID :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].requirement_id ? formReportdata['.....'].requirement_id : " "}</td>
                        ) : undefined
                        }
                      </tr>


                      <tr>
                        <td className="font-weight-bold">My Company :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].my_company_name ? formReportdata['.....'].my_company_name : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">Port :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].port_id_name ? formReportdata['.....'].port_id_name : " "}</td>
                        ) : undefined
                        }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Vendor :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].vendor_name ? formReportdata['.....'].vendor_name : " "}</td>
                        ) : undefined
                        }


                        <td className="font-weight-bold">Request Status :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].request_status ? formReportdata['.....'].request_status : " "}</td>
                        ) : undefined
                        }
                      </tr>


                      <tr>
                        <td className="font-weight-bold">Owner :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].tci_owner ? formReportdata['.....'].tci_owner : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">ETB :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].etb ? formReportdata['.....'].etb : " "}</td>
                        ) : undefined
                        }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Agent :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].agent_name ? formReportdata['.....'].agent_name : " "}</td>
                        ) : undefined
                        }
                        <td className="font-weight-bold">Delivery Type :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].delivary_type ? formReportdata['.....'].delivary_type : " "}</td>
                        ) : undefined
                        }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Supplier :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].supplier_name ? formReportdata['.....'].supplier_name : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">Delivery Frm/To :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].delivary_from ? formReportdata['.....'].delivary_from : " "}/{formReportdata && formReportdata['.....'] && formReportdata['.....'].delivary_to ? formReportdata['.....'].delivary_to : " "}</td>
                        ) : undefined
                        }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Broker :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].brocker_name ? formReportdata['.....'].brocker_name : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">For Account :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].for_account ? formReportdata['.....'].for_account : " "}</td>
                        ) : undefined
                        }
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Add. Commission :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].add_commission ? formReportdata['.....'].add_commission : " "}</td>
                        ) : undefined
                        }

                        <td className="font-weight-bold">Vendor Remark :</td>
                        {formReportdata['.....'] && formReportdata['.....'] ? (
                          <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].vendor_remark ? formReportdata['.....'].vendor_remark : " "}</td>
                        ) : undefined
                        }
                      </tr>
                    </tbody>
                  </table>

                  {/* <h4>Order</h4>
                  <table className="table custom-table-bordered tc-table">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Grade</th>
                        <th>Sulphar %</th>
                        <th>Order Qty</th>
                        <th>Reced Qty</th>
                        <th>Fuel Cost</th>
                        <th>Barge Rate</th>
                        <th>Barge Cost</th>
                        <th>Other Costs</th>
                        <th>Sales Tax</th>
                        <th>Net Cost</th>
                        <th>Total Grid Cost (USD)</th>
                      </tr>
                    </thead>
                    <tbody>
                      {formReportdata['--------'] && formReportdata['--------'].length > 0 ? formReportdata['--------'].map((e, idx) => {
                        return (
                          <>
                            <tr key={idx}>
                              <td>{e.fuel_type?e.fuel_type:" "}</td>
                              <td>{e.grade?e.grade:" "}</td>

                              <td>{e.sulphur?e.sulphur:" "}</td>
                              <td>{e.order_qty?e.order_qty:" "}</td>
                              <td>{e.recieved_qty?e.recieved_qty:" "}</td>
                              <td>{e.fuel_cost?e.fuel_cost:" "}</td>
                              <td>{e.barge_rate?e.barge_rate:" "}</td>
                              <td>{e.barge_cost?e.barge_cost:" "}</td>
                              <td>{e.other_cost?e.other_cost:" "}</td>
                              <td>{e.sales_tax?e.sales_tax:" "}</td>
                              <td>{e.net_cost?e.net_cost:" "}</td>
                              <td>{e.total_grid_cost?e.total_grid_cost:" "}</td>
                            </tr>
                          </>
                        )
                      }) : undefined

                      }


                    </tbody>
                  </table> */}

                  <h4 className="font-weight-bold tc-sub-header">Order</h4>
                  <table className="table custom-table-bordered tc-table">
                    <thead>
                      <tr className='HeaderBoxText'>
                        <th>Type</th>
                        <th>Grade</th>
                        <th>Sulphar %</th>
                        <th>Invoice Qty</th>
                        <th>Recieved Qty</th>
                        <th>Fuel Cost</th>
                        <th>Barge Rate</th>
                        <th>Barge Cost</th>
                        <th>Other Costs</th>
                        <th>Sales Tax</th>
                        <th>Net Cost</th>
                        <th>Port Charge</th>
                        <th>Invoice Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      {formReportdata.invoice && formReportdata.invoice.length > 0 ? formReportdata.invoice.map((e, idx) => {
                        return (
                          <>
                            <tr key={idx}>
                              <td>{e.fuel_type_name ? e.fuel_type_name : " "}</td>
                              <td>{e.grade_name ? e.grade_name : " "}</td>
                              <td>{e.sulphur_ ? e.sulphur_ : " "}</td>
                              <td>{e.recieved_qty ? e.recieved_qty : " "}</td>
                              <td>{e.invoice_qty ? e.invoice_qty : " "}</td>
                              <td>{e.fuel_cost ? e.fuel_cost : " "}</td>
                              <td>{e.barge_rate ? e.barge_rate : " "}</td>
                              <td>{e.barge_cost ? e.barge_cost : " "}</td>
                              <td>{e.other_cost ? e.other_cost : " "}</td>
                              <td>{e.sales_tax ? e.sales_tax : " "}</td>
                              <td>{e.net_cost ? e.net_cost : " "}</td>
                              <td>{e.port_charge ? e.port_charge : " "}</td>
                              <td>{e.invoice_total ? e.invoice_total : " "}</td>

                            </tr>
                          </>
                        )
                      }) : undefined

                      }

                    </tbody>
                  </table>
                </td>
                <td className="border-0">
                  <table className="table custom-table-bordered tc-table">
                    <tbody>
                      <tr>
                        <td className="font-weight-bold">Purchase Status :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].purchase_status ? formReportdata['-----'].purchase_status : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Purchase Type :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].purchase_status ? formReportdata['-----'].purchase_status : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">PO Number :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].po_number ? formReportdata['-----'].po_number : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Ops User :</td>
                        <td>{formReportdata && formReportdata['.....'] && formReportdata['.....'].ops_user_name ? formReportdata['.....'].ops_user_name : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Order Date :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].order_date ? formReportdata['-----'].order_date : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Curr/Exch Rate :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].exc_rate ? formReportdata['-----'].exc_rate : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Port Changes :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].port_change ? formReportdata['-----'].port_change : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Other Costs :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].other_cost ? formReportdata['-----'].other_cost : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Barging Ammount :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].barging_amount ? formReportdata['-----'].barging_amount : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Tax % :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].tax ? formReportdata['-----'].tax : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Total Amount :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].total_amount ? formReportdata['-----'].total_amount : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Payment Terms :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].payment_terms ? formReportdata['-----'].payment_terms : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Invoice No. :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].invoice_no ? formReportdata['-----'].invoice_no : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Invoice Status :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].invoice_status ? formReportdata['-----'].invoice_status : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Invoice Amount :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].invoice_amt ? formReportdata['-----'].invoice_amt : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Invoice Date :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].invoice_date ? formReportdata['-----'].invoice_date : " "}</td>
                      </tr>

                      <tr>
                        <td className="font-weight-bold">Invoice Due Date :</td>
                        <td>{formReportdata && formReportdata['-----'] && formReportdata['-----'].invoice_due_date ? formReportdata['-----'].invoice_due_date : " "}</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tbody>
            </table>

            <h4 className="font-weight-bold tc-sub-header">IFO</h4>
            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Fee Currency :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.fee_currency ? formReportdata.ifo.fee_currency : " "}</td>

                  <td className="font-weight-bold">BW Spot Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_spot_price ? formReportdata.ifo.bw_spot_price : " "}</td>

                  <td className="font-weight-bold">Test :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_pre ? formReportdata.ifo.test_pre : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Ordered Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.ordered_qty ? formReportdata.ifo.ordered_qty : " "}</td>

                  <td className="font-weight-bold">BW Cntr Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_cntr_price ? formReportdata.ifo.bw_cntr_price : " "}</td>

                  <td className="font-weight-bold">Test Days :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_days1 ? formReportdata.ifo.test_days1 : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Min/Max Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.min_qty ? formReportdata.ifo.min_qty : " "}/{formReportdata && formReportdata.ifo && formReportdata.ifo.max_qty ? formReportdata.ifo.max_qty : " "}</td>

                  <td className="font-weight-bold">Recieved Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.recieved_qty ? formReportdata.ifo.recieved_qty : " "}</td>

                  <td className="font-weight-bold">Test Due :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_due1 ? formReportdata.ifo.test_due1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoiced Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.invoiced_qty ? formReportdata.ifo.invoiced_qty : " "}</td>

                  <td className="font-weight-bold">Bunkering Start :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_start ? formReportdata.ifo.bunkering_start : " "}</td>


                  <td className="font-weight-bold">Test Received :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_recieved1 ? formReportdata.ifo.test_recieved1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Net Fuel Cost/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.net_fuel_cost ? formReportdata.ifo.net_fuel_cost : " "}</td>

                  <td className="font-weight-bold">Bunkering End :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_end ? formReportdata.ifo.bunkering_end : " "}</td>

                  <td className="font-weight-bold">Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.density1 ? formReportdata.ifo.density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Name :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_name ? formReportdata.ifo.barge_name : " "}</td>

                  <td className="font-weight-bold">Bunkering Time (mins) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_times ? formReportdata.ifo.bunkering_times : " "}</td>

                  <td className="font-weight-bold">Diff Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.diff_density1 ? formReportdata.ifo.diff_density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Arrival :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_arrival ? formReportdata.ifo.barge_arrival : " "}</td>

                  <td className="font-weight-bold">Bunkering Rate (MT/hr) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_rate ? formReportdata.ifo.bunkering_rate : " "}</td>

                  <td className="font-weight-bold">Sulphar % :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.sulphur_name ? formReportdata.ifo.sulphur_name : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Departure :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_departure ? formReportdata.ifo.barge_departure : " "}</td>

                  <td className="font-weight-bold">Inspector :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.inspector ? formReportdata.ifo.inspector : " "}</td>

                  <td colSpan="2"> :</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold tc-sub-header">LSMGO</h4>
            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Fee Currency :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.currency ? formReportdata.lsmgo.currency : " "}</td>

                  <td className="font-weight-bold">BW Spot Price/Curr :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bw_spot_price ? formReportdata.lsmgo.bw_spot_price : " "}</td>

                  <td className="font-weight-bold">Test :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.test_pre ? formReportdata.lsmgo.test_pre : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Ordered Qty :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.ordered_qty ? formReportdata.lsmgo.ordered_qty : " "}</td>

                  <td className="font-weight-bold">BW Cntr Price/Curr :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bw_cntr_price ? formReportdata.lsmgo.bw_cntr_price : " "}</td>

                  <td className="font-weight-bold">Test Days :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.test_days1 ? formReportdata.lsmgo.test_days1 : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Min/Max Qty :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.min_qty ? formReportdata.lsmgo.min_qty : " "}/{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.max_qty ? formReportdata.lsmgo.max_qty : " "}</td>

                  <td className="font-weight-bold">Recieved Qty :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.recieved_qty ? formReportdata.lsmgo.recieved_qty : " "}</td>

                  <td className="font-weight-bold">Test Due :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.test_due1 ? formReportdata.lsmgo.test_due1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoiced Qty :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.invoiced_qty ? formReportdata.lsmgo.invoiced_qty : " "}</td>

                  <td className="font-weight-bold">Bunkering Start :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bunkering_start ? formReportdata.lsmgo.bunkering_start : " "}</td>


                  <td className="font-weight-bold">Test Received :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.test_recieved1 ? formReportdata.lsmgo.test_recieved1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Net Fuel Cost/Curr :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.net_fuel_cost ? formReportdata.lsmgo.net_fuel_cost : " "}</td>

                  <td className="font-weight-bold">Bunkering End :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bunkering_end ? formReportdata.lsmgo.bunkering_end : " "}</td>

                  <td className="font-weight-bold">Density :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.density1 ? formReportdata.lsmgo.density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Name :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.barge_name ? formReportdata.lsmgo.barge_name : " "}</td>

                  <td className="font-weight-bold">Bunkering Time (mins) :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bunkering_times ? formReportdata.lsmgo.bunkering_times : " "}</td>

                  <td className="font-weight-bold">Diff Density :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.diff_density1 ? formReportdata.lsmgo.diff_density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Arrival :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.barge_arrival ? formReportdata.lsmgo.barge_arrival : " "}</td>

                  <td className="font-weight-bold">Bunkering Rate (MT/hr) :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.bunkering_rate ? formReportdata.lsmgo.bunkering_rate : " "}</td>

                  <td className="font-weight-bold">Sulphur % :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.sulphur_name ? formReportdata.lsmgo.sulphur_name : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Departure :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.barge_departure ? formReportdata.lsmgo.barge_departure : " "}</td>

                  <td className="font-weight-bold">Inspector :</td>
                  <td>{formReportdata && formReportdata.lsmgo && formReportdata.lsmgo.inspector ? formReportdata.lsmgo.inspector : " "}</td>

                  <td colSpan="2"> :</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold tc-sub-header">VLSFO</h4>
            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Fee Currency :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.fee_currency ? formReportdata.ifo.fee_currency : " "}</td>

                  <td className="font-weight-bold">BW Spot Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_spot_price ? formReportdata.ifo.bw_spot_price : " "}</td>

                  <td className="font-weight-bold">Test :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_pre ? formReportdata.ifo.test_pre : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Ordered Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.ordered_qty ? formReportdata.ifo.ordered_qty : " "}</td>

                  <td className="font-weight-bold">BW Cntr Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_cntr_price ? formReportdata.ifo.bw_cntr_price : " "}</td>

                  <td className="font-weight-bold">Test Days :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_days1 ? formReportdata.ifo.test_days1 : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Min/Max Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.min_qty ? formReportdata.ifo.min_qty : " "}/{formReportdata && formReportdata.ifo && formReportdata.ifo.max_qty ? formReportdata.ifo.max_qty : " "}</td>

                  <td className="font-weight-bold">Recieved Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.recieved_qty ? formReportdata.ifo.recieved_qty : " "}</td>

                  <td className="font-weight-bold">Test Due :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_due1 ? formReportdata.ifo.test_due1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoiced Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.invoiced_qty ? formReportdata.ifo.invoiced_qty : " "}</td>

                  <td className="font-weight-bold">Bunkering Start :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_start ? formReportdata.ifo.bunkering_start : " "}</td>


                  <td className="font-weight-bold">Test Received :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_recieved1 ? formReportdata.ifo.test_recieved1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Net Fuel Cost/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.net_fuel_cost ? formReportdata.ifo.net_fuel_cost : " "}</td>

                  <td className="font-weight-bold">Bunkering End :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_end ? formReportdata.ifo.bunkering_end : " "}</td>

                  <td className="font-weight-bold">Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.density1 ? formReportdata.ifo.density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Name :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_name ? formReportdata.ifo.barge_name : " "}</td>

                  <td className="font-weight-bold">Bunkering Time (mins) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_times ? formReportdata.ifo.bunkering_times : " "}</td>

                  <td className="font-weight-bold">Diff Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.diff_density1 ? formReportdata.ifo.diff_density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Arrival :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_arrival ? formReportdata.ifo.barge_arrival : " "}</td>

                  <td className="font-weight-bold">Bunkering Rate (MT/hr) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_rate ? formReportdata.ifo.bunkering_rate : " "}</td>

                  <td className="font-weight-bold">Sulphur % :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.sulphur_name ? formReportdata.ifo.sulphur_name : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Departure :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_departure ? formReportdata.ifo.barge_departure : " "}</td>

                  <td className="font-weight-bold">Inspector :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.inspector ? formReportdata.ifo.inspector : " "}</td>

                  <td colSpan="2"> :</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold tc-sub-header">MGO</h4>
            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Fee Currency :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.fee_currency ? formReportdata.ifo.fee_currency : " "}</td>

                  <td className="font-weight-bold">BW Spot Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_spot_price ? formReportdata.ifo.bw_spot_price : " "}</td>

                  <td className="font-weight-bold">Test :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_pre ? formReportdata.ifo.test_pre : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Ordered Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.ordered_qty ? formReportdata.ifo.ordered_qty : " "}</td>

                  <td className="font-weight-bold">BW Cntr Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_cntr_price ? formReportdata.ifo.bw_cntr_price : " "}</td>

                  <td className="font-weight-bold">Test Days :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_days1 ? formReportdata.ifo.test_days1 : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Min/Max Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.min_qty ? formReportdata.ifo.min_qty : " "}/{formReportdata && formReportdata.ifo && formReportdata.ifo.max_qty ? formReportdata.ifo.max_qty : " "}</td>

                  <td className="font-weight-bold">Recieved Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.recieved_qty ? formReportdata.ifo.recieved_qty : " "}</td>

                  <td className="font-weight-bold">Test Due :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_due1}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoiced Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.invoiced_qty ? formReportdata.ifo.invoiced_qty : " "}</td>

                  <td className="font-weight-bold">Bunkering Start :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_start ? formReportdata.ifo.bunkering_start : " "}</td>


                  <td className="font-weight-bold">Test Received :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_recieved1 ? formReportdata.ifo.test_recieved1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Net Fuel Cost/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.net_fuel_cost ? formReportdata.ifo.net_fuel_cost : " "}</td>

                  <td className="font-weight-bold">Bunkering End :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_end ? formReportdata.ifo.bunkering_end : " "}</td>

                  <td className="font-weight-bold">Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.density1 ? formReportdata.ifo.density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Name :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_name ? formReportdata.ifo.barge_name : " "}</td>

                  <td className="font-weight-bold">Bunkering Time (mins) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_times ? formReportdata.ifo.bunkering_times : " "}</td>

                  <td className="font-weight-bold">Diff Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.diff_density1 ? formReportdata.ifo.diff_density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Arrival :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_arrival ? formReportdata.ifo.barge_arrival : " "}</td>

                  <td className="font-weight-bold">Bunkering Rate (MT/hr) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_rate ? formReportdata.ifo.bunkering_rate : " "}</td>

                  <td className="font-weight-bold">Sulphur % :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.sulphur_name ? formReportdata.ifo.sulphur_name : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Departure :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_departure ? formReportdata.ifo.barge_departure : " "}</td>

                  <td className="font-weight-bold">Inspector :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.inspector ? formReportdata.ifo.inspector : " "}</td>

                  <td colSpan="2"> :</td>
                </tr>
              </tbody>
            </table>

            <h4 className="font-weight-bold tc-sub-header">ULSFO</h4>
            <table className="table custom-table-bordered tc-table">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Fee Currency :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.fee_currency ? formReportdata.ifo.fee_currency : " "}</td>

                  <td className="font-weight-bold">BW Spot Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_spot_price ? formReportdata.ifo.bw_spot_price : " "}</td>

                  <td className="font-weight-bold">Test :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_pre ? formReportdata.ifo.test_pre : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Ordered Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.ordered_qty ? formReportdata.ifo.ordered_qty : " "}</td>

                  <td className="font-weight-bold">BW Cntr Price/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bw_cntr_price ? formReportdata.ifo.bw_cntr_price : " "}</td>

                  <td className="font-weight-bold">Test Days :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_days1 ? formReportdata.ifo.test_days1 : " "}</td>

                </tr>

                <tr>
                  <td className="font-weight-bold">Min/Max Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.min_qty ? formReportdata.ifo.min_qty : " "}/{formReportdata && formReportdata.ifo && formReportdata.ifo.max_qty ? formReportdata.ifo.max_qty : " "}</td>

                  <td className="font-weight-bold">Recieved Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.recieved_qty ? formReportdata.ifo.recieved_qty : " "}</td>

                  <td className="font-weight-bold">Test Due :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_due1}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Invoiced Qty :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.invoiced_qty ? formReportdata.ifo.invoiced_qty : " "}</td>

                  <td className="font-weight-bold">Bunkering Start :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_start ? formReportdata.ifo.bunkering_start : " "}</td>


                  <td className="font-weight-bold">Test Received :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.test_recieved1 ? formReportdata.ifo.test_recieved1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Net Fuel Cost/Curr :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.net_fuel_cost ? formReportdata.ifo.net_fuel_cost : " "}</td>

                  <td className="font-weight-bold">Bunkering End :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_end ? formReportdata.ifo.bunkering_end : " "}</td>

                  <td className="font-weight-bold">Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.density1 ? formReportdata.ifo.density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Name :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_name ? formReportdata.ifo.barge_name : " "}</td>

                  <td className="font-weight-bold">Bunkering Time (mins) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_times ? formReportdata.ifo.bunkering_times : " "}</td>

                  <td className="font-weight-bold">Diff Density :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.diff_density1 ? formReportdata.ifo.diff_density1 : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Arrival :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_arrival ? formReportdata.ifo.barge_arrival : " "}</td>

                  <td className="font-weight-bold">Bunkering Rate (MT/hr) :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.bunkering_rate ? formReportdata.ifo.bunkering_rate : " "}</td>

                  <td className="font-weight-bold">Sulphur % :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.sulphur_name ? formReportdata.ifo.sulphur_name : " "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Barge Departure :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.barge_departure ? formReportdata.ifo.barge_departure : " "}</td>

                  <td className="font-weight-bold">Inspector :</td>
                  <td>{formReportdata && formReportdata.ifo && formReportdata.ifo.inspector ? formReportdata.ifo.inspector : " "}</td>

                  <td colSpan="2"> :</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class BunkerPurchasedReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
      pdfData: "",
      userInput: "",
      emailModal: false,
      loading: false,
      mailTitlePayload: {},
    };
  }

  printReceipt() {
    window.print();
  }
  printDocument() {
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save("Bunker_Purchase_Order.pdf");
      });
  }

  sendEmailReportModal = async () => {
    try {
      
      this.setState({ loading: true });
  
      const quotes = document.getElementById('divToPrint');
  
      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });
  
      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;
  
      const doc = new jsPDF('p', 'mm');
      let position = 25;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      doc.addImage(pageData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;
  
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(pageData, 'PNG', 5, position + 25, imgWidth - 8, imgHeight - 7);
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
  
      // Create Blob
      const blob = doc.output('blob');
  
      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      
  
      this.setState({
        loading: false,
        pdfData: blob,
        emailModal: true,
      });
  
    } catch (error) {
      console.error('Error:', error);
      this.setState({ loading: false });
      // Handle errors here
    }
  };

  render() {
    const { vessel_name, vendor_remark, my_company_name, po_number } = this.props.data["....."];
    
    console.log('this.props.data["....."]',vessel_name)
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={this.sendEmailReportModal} style={{ cursor: this.state.loading ? 'not-allowed' : 'pointer' }}>Send Email</li>
                      <li onClick={this.printDocument}>
                        Download
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data} />
            </div>
          </div>
        </article>


        {/* {this.state.emailModal && (
          <Modal
            title="New Message"
            visible={this.state.emailModal}
            onOk={() => {
              this.state.emailModal(false);
            }}
            onCancel={() => {
              this.state.emailModal(false);
            }}
            footer={null}
          >
            {this.state.pdfData && <Email
              handleClose={
                () => {
                  this.setState(prevState => ({

                    emailModal: false,
                  }))


                }

              }
              attachmentFile={this.state.pdfData}

              title={`Bunker_Purchase_Report|| ${vessel_name} || ${my_company_name} || ${po_number} || ${vendor_remark}`}

            />}
          </Modal>
        )} */}
        {this.state.emailModal && (
          <Modal
            title="New Message"
            visible={this.state.emailModal}
            onOk={() => {
              // Instead of calling this.state.emailModal, set it to false
              this.setState({ emailModal: false });
            }}
            onCancel={() => {
              // Instead of calling this.state.emailModal, set it to false
              this.setState({ emailModal: false });
            }}
            footer={null}
          >
            {this.state.pdfData && (
              <Email
                handleClose={() => {
                  this.setState({ emailModal: false });
                }}
                attachmentFile={this.state.pdfData}
                title={window.corrector(`Bunker_Purchase_Report||${vessel_name}||${my_company_name}||${po_number}||${vendor_remark}`)}

              // title={`Bunker_Purchase_Report|| ${vessel_name} || ${my_company_name} || ${po_number} || ${vendor_remark}`}
              />
            )}
          </Modal>
        )}

        {
          this.state.loading && (
            <div style={{ position: 'absolute', top: '10%', left: '50%', transform: 'translate(-50%, -50%)' }}>
              <Spin size="large" />
            </div>
          )
        }
      </div>
    );
  }
}

export default BunkerPurchasedReport;
