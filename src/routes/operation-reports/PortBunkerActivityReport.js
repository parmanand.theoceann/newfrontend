import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';
// import html2canvas from 'html2canvas';
import * as htmlToImage from 'html-to-image';
import moment from 'moment';
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);

    const formReportdata = {

    }


    this.state = {
      formReportdata: Object.assign(formReportdata, this.props.data || {}),
    }
  }

  render() {
    const { formReportdata } = this.state


    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <img className='reportlogo' src={formReportdata.comapny_logo ? formReportdata.comapny_logo : "N/A"} alt="No img" />
                  <p className="sub-title m-0">{formReportdata.company_name ? formReportdata.company_name : "N/A"}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata.company_address ? formReportdata.company_address : "N/A"}</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Prev Port</td>
                  <td>{formReportdata && formReportdata.prev_port ? formReportdata.prev_port : "N/A"}</td>
                  <td className="font-weight-bold">Current Port</td>
                  <td>{formReportdata && formReportdata.curr_port ? formReportdata.curr_port : "N/A"}</td>
                  <td className="font-weight-bold">Prev Dep Date</td>
                  <td>{formReportdata && formReportdata.prev_dep ? formReportdata.prev_dep : "N/A"}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">TSD</td>
                  <td>{formReportdata && formReportdata.tsd ? formReportdata.tsd : "N/A"}</td>
                  <td className="font-weight-bold">TPD</td>
                  <td>{formReportdata && formReportdata.tpd ? formReportdata.tpd : "N/A"}</td>
                  <td className="font-weight-bold">Destination</td>
                  <td>{formReportdata && formReportdata.destination ? formReportdata.destination : "N/A"}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Function</td>
                  <td>{formReportdata && formReportdata.function_name ? formReportdata.function_name : "N/A"}</td>
                  <td className="font-weight-bold">Arrival(LT)</td>
                  <td>{formReportdata && formReportdata.arrival_lt ? moment(formReportdata.arrival_lt).format("YYYY-MM-DD HH:mm") : "0000-00-00 00:00:00"}</td>
                  <td className="font-weight-bold">Departure(LT)</td>
                  <td>{formReportdata && formReportdata.departure_lt ? moment(formReportdata.departure_lt).format("YYYY-MM-DD HH:mm"): "0000-00-00 00:00:00"}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Distance to Go from Dep</td>
                  <td>{formReportdata && formReportdata.distance ? formReportdata.distance : "N/A"}</td>

                  <td className="font-weight-bold">Average Speed</td>
                  <td>{formReportdata && formReportdata.average_speed ? formReportdata.average_speed : "N/A"}</td>

                  <td className="font-weight-bold">.</td>
                  <td>.</td>
                </tr>
              </tbody>
            </table>

            <h4>Bunker Consumption update</h4>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>ROB Prev</th>
                  <th>Sea Cons</th>
                  <th>Cons Rate</th>
                  <th>ROB Arr</th>
                  <th>Recvd(Mt)</th>
                  <th>Port Cons</th>
                  <th>ROB Dep</th>
                </tr>
              </thead>
              <tbody>
                {formReportdata.bunkerconsumptionupdate && formReportdata.bunkerconsumptionupdate &&
                  formReportdata.bunkerconsumptionupdate.length > 0 ? formReportdata.bunkerconsumptionupdate.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.fuel_type ? e.fuel_type : " "}</td>
                          <td>{e.rob_prev ? e.rob_prev : " "}</td>
                          <td>{e.sea_consumption ? e.sea_consumption : " "}</td>
                          <td>{e.cons_rate ? e.cons_rate : " "}</td>
                          <td>{e.rob_arr ? e.rob_arr : " "}</td>
                          <td>{e.received_qty ? e.received_qty : " "}</td>
                          <td>{e.port_cons ? e.port_cons : " "}</td>
                          <td>{e.rob_departure ? e.rob_departure : " "}</td>
                        </tr>
                      </>
                    )
                  }) : undefined

                }


              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Forward Draft (M)</td>
                  <td>{formReportdata && formReportdata.forward_draft_m ? formReportdata.forward_draft_m : "N/A"}</td>
                  <td className="font-weight-bold">Mid Draft (M)</td>
                  <td>{formReportdata && formReportdata.mid_draft_m ? formReportdata.mid_draft_m : "N/A"}</td>
                  <td className="font-weight-bold">Aft Draft (M)</td>
                  <td>{formReportdata && formReportdata.after_draft_m ? formReportdata.after_draft_m : "N/A"}</td>
                </tr>
              </tbody>
            </table>

            <h4>Port Activities</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Activity</th>
                  <th>Cargo</th>
                  <th>BL Code</th>
                  <th>Remarks</th>
                  <th>Date From</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                {formReportdata.portactivity && formReportdata.portactivity.length > 0 && formReportdata.portactivity.map((el, id) => {
                  return (
                    <tr key={id}>
                      <td>{el.port_activity_name ? el.port_activity_name : "N/A"}</td>
                      <td>{el.port_cargo_name ? el.port_cargo_name : "N/A"}</td>
                      <td>{el.bl_code ? el.bl_code : "N/A"}</td>
                      <td>{el.remarks ? el.remarks : "N/A"}</td>
                      <td>{el.date_from ? el.date_from : "N/A"}</td>
                      <td>{el.from_time ? el.from_time : "N/A"}</td>
                    </tr>
                  )
                })
                }
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class PortBunkerActivityReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  printDocument() {
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save("port-bunker-activity-report.pdf");
      });

    // var quotes = document.getElementById('divToPrint');

    // html2canvas(quotes, {
    //   logging: true,
    //   letterRendering: 1,
    //   useCORS: true,
    //   allowTaint: true
    // }).then(function (canvas) {
    //   const link = document.createElement("a");
    //   link.download = "html-to-img.png";
    //   var imgWidth = 210;
    //   var pageHeight = 290;
    //   var imgHeight = canvas.height * imgWidth / canvas.width;
    //   var heightLeft = imgHeight;
    //   var doc = new jsPDF('p', 'mm');
    //   var position = 30;
    //   var pageData = canvas.toDataURL('image/jpeg', 1.0);
    //   var imgData = encodeURIComponent(pageData);
    //   doc.addImage(imgData, 'PNG', 5, position, imgWidth - 8, imgHeight - 7);
    //   doc.setLineWidth(5);
    //   doc.setDrawColor(255, 255, 255);
    //   doc.rect(0, 0, 210, 295);
    //   heightLeft -= pageHeight;

    //   while (heightLeft >= 0) {
    //     position = heightLeft - imgHeight;
    //     doc.addPage();
    //     doc.addImage(imgData, 'PNG', 5, position + 30, imgWidth - 8, imgHeight - 7);
    //     doc.setLineWidth(5);
    //     doc.setDrawColor(255, 255, 255);
    //     doc.rect(0, 0, 210, 295);
    //     heightLeft -= pageHeight;
    //   }
    //   doc.save('PortBunkerActivityReport.pdf');

    // });
  };









  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                     <li onClick={this.printDocument}>
                        Download
                      </li>

                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                    <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortBunkerActivityReport;
