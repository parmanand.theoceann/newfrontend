import React, { Component } from "react";
import { PrinterOutlined } from "@ant-design/icons";
import ReactToPrint from "react-to-print";
import URL_WITH_VERSION, { IMAGE_PATH } from "../../shared";
import { logDOM } from "@testing-library/react";
import moment from "moment";
import { filter } from "lodash";
import MapComponent from "../dynamic-vspm/Newmap";
class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Printer",
      mainInformation: this.props.data,
      data: this.props.data,
      arrival_portInfo: {},
      departure_portInfo: {},
      voyageNo: this.props.voyNo,
      sumData: [],
      coordinates:[]
    };
  }
  componentDidMount() {
    this.filterFunc();
    this.setState((prev) => ({
      ...prev,
      coordinates:this.props.latLong
    }))
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.data !== prevProps.data ||
      this.state.report_content !== prevState.report_content
    ) {
      this.filterFunc();
    }
    
  }

  filterFunc() {

    const filterSumData = this.props.sumData.filter(
      (obj) => obj.voyageNo === this.props.voyNo
    );
    this.setState((prev) => ({
      ...prev,
      sumData: filterSumData[0],
    }));
  }

  
  render() {
    const { data, sumData } = this.state;
    const filterDepartureData = this.props.data?.allReport?.filter(obj => obj.report_type === 4);

    const filterArrivaleData = this.props.data?.allReport?.filter(obj => obj.report_type === 1);
    const lastArrivaldata = filterArrivaleData[filterArrivaleData.length - 1];

    const filteredNoonData = this.props.data?.allReport?.filter(obj => obj.report_type === 5);

    const {coordinates} = this.state;

    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <div className="signin-logo mb-2 text-center">
                    <div className="middel-text">
                      <img src={IMAGE_PATH + "theoceann.png"} height="50" />
                      <span>Theoceann</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Maritime Info Lab Pvt Ltd, Hongkong</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Vessel Name</td>
                  <td>{sumData?.vesselName ? sumData?.vesselName : "-"}</td>
                  <td className="font-weight-bold">IMO No. </td>
                  <td>{sumData?.vessleImo ? sumData?.vessleImo : "-"}</td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Arrival Port</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.arrival
                      ? sumData?.arrivalandDeparturePort?.arrival
                      : "-"}
                  </td>
                  <td className="font-weight-bold">Arrival Date</td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.arrival
                      ? moment(
                          sumData?.arrivalandDeparturePortDate?.arrival
                        ).format("DD-MM-YYYY HH:mm:ss")
                      : "-"}
                  </td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Departure Port</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.departure == "No data"
                      ? "-"
                      : sumData?.arrivalandDeparturePort?.departure}
                  </td>

                  <td className="font-weight-bold">Departure Date</td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.departure ===
                    "No data"
                      ? "-"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.departure
                        ).format("DD-MM-YYYY HH:mm:ss")}
                  </td>
                </tr>
                <tr>
                  <td className="font-weight-bold">Report Date</td>
                  <td>Value</td>
                  <td className="font-weight-bold">VSPM ID</td>
                  <td>
                    {sumData?.vspm_id ? sumData?.vspm_id.join(", ") : "-"}
                  </td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Report type</td>
                  <td>
                    {this.props.type}
                  </td>

                  <td className="font-weight-bold">Passage</td>
                  <td colSpan="4">
                    {this.props.data.passageType
                      ? this.props.data.passageType
                      : "-"}
                  </td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3 mt-3">
              Voyage Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  {/* <th colSpan="2" className="text-center">
                    Distance
                  </th> */}
                  {/* <th colSpan="2" className="text-center">
                    Total Time
                  </th> */}
                  <th colSpan="5" className="text-center"></th>
                  <th colSpan="5" className="text-center">
                    Total cons. In Port/ At sea
                  </th>
                </tr>
                <tr>
                  <th>S/N</th>
                  <th>Arr/Dep port</th>
                  <th>Date time</th>
                  <th>Distance</th>
                  <th>Total time</th>
                  <th>ULSFO</th>
                  <th>VLSFO</th>
                  <th>IFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>
                    {sumData?.arrivalandDeparturePort?.arrival === "No data"
                      ? "N/A"
                      : sumData?.arrivalandDeparturePort?.arrival}{" "}
                    -{" "}
                    {sumData?.arrivalandDeparturePort?.departure === "No data"
                      ? "N/A"
                      : sumData?.arrivalandDeparturePort?.departure}
                  </td>
                  <td>
                    {sumData?.arrivalandDeparturePortDate?.arrival === "No data"
                      ? "N/A"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.arrival
                        ).format("DD-MM-YYYY HH:mm")}{" "}
                    -{" "}
                    {sumData?.arrivalandDeparturePortDate?.departure ===
                    "No data"
                      ? "N/A"
                      : moment(
                          sumData?.arrivalandDeparturePortDate?.departure
                        ).format("DD-MM-YYYY HH:mm")}
                  </td>
                  <td>{sumData?.vesselAndDistance}</td>
                  <td>{sumData?.steamHr}</td>
                  <td>{sumData?.ulsfo}</td>
                  <td>{sumData?.vlsfo}</td>
                  <td>{sumData?.ifo}</td>
                  <td>{sumData?.lsmgo}</td>
                  <td>{sumData?.mgo}</td>
                </tr>

                <tr>
                  <th className="text-center" colSpan="3">
                    Total
                  </th>
                  <td>{sumData?.totalDist}</td>
                  <td>0</td>
                  <td className="text-center" colSpan={5}>
                    <b>
                      {parseFloat(
                        sumData?.ulsfo +
                          sumData?.vlsfo +
                          sumData?.ifo +
                          sumData?.lsmgo +
                          sumData?.mgo
                      ).toFixed(2)}
                    </b>
                  </td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Total
            </h3>

            <table className="table border-table table-striped table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Total time at sea</td>
                  <td>
                    {this.props.data?.totalSteamingHr} <b>Hr</b>
                  </td>
                  <td className="font-weight-bold">ULSFO consumed</td>
                  <td>{sumData?.ulsfo}</td>
                  <td className="font-weight-bold">VLSFO consumed</td>
                  <td>{sumData?.vlsfo}</td>
                </tr>
                <tr>
                  <td className="font-weight-bold">IFO consumed</td>
                  <td>{sumData?.ifo}</td>
                  <td className="font-weight-bold">LSMGO consumed</td>
                  <td>{sumData?.lsmgo}</td>
                  <td className="font-weight-bold">MGO consumed</td>
                  <td colSpan="6">{sumData?.mgo}</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Speed Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th></th>
                  <th>Total distance</th>
                  <th>Unit</th>
                  <th>Bad weather dist.</th>
                  <th>Unit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Distance sailed overall</th>
                  <td>{this.props.data?.totalDist}</td>
                  <td>miles</td>
                  <td>{this.props.data?.badWeatherDist}</td>
                  <td>miles</td>
                </tr>

                <tr>
                  <th>Time at sea</th>
                  <td>{this.props.data?.totalSteamingHr}</td>
                  <td>hr</td>
                  <td>{this.props.data?.badWeatherHr}</td>
                  <td>hr</td>
                </tr>

                <tr>
                  <th>Performancd SPD</th>
                  <td>{parseFloat(this.props.data?.avgSpeed).toFixed(2)}</td>
                  <td>kt</td>
                  <td>{parseFloat(this.props.data?.avgSpeed).toFixed(2)}</td>
                  <td>kt</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              GHG* Emission Summary
            </h3>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th rowSpan="2"></th>
                  <th colSpan="5" className="text-center">
                    Fuel consumption summary
                  </th>
                </tr>
                <tr>
                  <th>ULSFO</th>
                  <th>VLSFO</th>
                  <th>IFO</th>
                  <th>LSMGO</th>
                  <th>MGO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>M/E Propuslion</th>
                  <td>{parseFloat(this.props.data?.me_ulsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_vlsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_ifo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_lsmgo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.me_mgo).toFixed(2)}</td>
                </tr>

                <tr>
                  <th>Maneover</th>
                  <td>
                    {parseFloat(this.props.data?.manoever_ulsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_vlsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_ifo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_lsmgo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.manoever_mgo).toFixed(2)}
                  </td>
                </tr>

                <tr>
                  <th>Generator</th>
                  <td>
                    {parseFloat(this.props.data?.generator_ulsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_vlsfo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_ifo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_lsmgo).toFixed(2)}
                  </td>
                  <td>
                    {parseFloat(this.props.data?.generator_mgo).toFixed(2)}
                  </td>
                </tr>

                <tr>
                  <th>Aux Engine</th>
                  <td>{parseFloat(this.props.data?.aux_ulsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_vlsfo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_ifo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_lsmgo).toFixed(2)}</td>
                  <td>{parseFloat(this.props.data?.aux_mgo).toFixed(2)}</td>
                </tr>
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Voyage Report Summary
            </h3>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <th>Type</th>
                <th>Date/Time</th>
                <th>Lat</th>
                <th>Long</th>
                <th>Speed</th>
                <th>Distance</th>
                <th>IFO</th>
                <th>MGO</th>
                <th>LSMGO</th>
                <th>ULSFO</th>
                <th>VLSFO</th>
                <th>Sea State</th>
                <th>Sea Height</th>
                <th>Air Press.</th>
                <th>Air Temp.</th>
                <th>Wind Dir.</th>
              </thead>
              <tbody>
                {/* Departure Row Start*/}
                <tr>
                    <td>{filterDepartureData[0]?.report_type === 4 ? "Departure" : "-"}</td>
                    <td>{filterDepartureData[0] ? moment(filterDepartureData[0]?.created_date).format("DD-MM-YYYY HH:mm") : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.mainInformation.departure_main_lat ? filterDepartureData[0]?.report_content?.mainInformation.departure_main_lat : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.mainInformation.departure_main_long ? filterDepartureData[0]?.report_content?.mainInformation.departure_main_long : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_ordered_spd ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_ordered_spd : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_observe_dist ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_observe_dist : "-"}</td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_info || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_info || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_mgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_mgo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_lsmgo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_lsmgo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_ulsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_ulsfo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_vlsfo || 0)
                    }
                    </td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_sea_state ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_sea_state : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_sea_height ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_sea_height : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_air_press ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_air_press : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_air_tempt ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_air_tempt : "-"}</td>
                    <td>{filterDepartureData[0]?.report_content?.vesselDistance?.departure_wind_dir ? filterDepartureData[0]?.report_content?.vesselDistance?.departure_wind_dir : "-"}</td>
                </tr>
                {/* Departure Row End */}
                {/* Noon Reports Start */}
                {
                    filteredNoonData?.map((e, index) => {

                        return(
                            <tr key={index}>
                                <td>{e.report_type === 5 ? "Noon" : "-"}</td>
                                <td>{e.created_date ? moment(e.created_date).format("DD-MM-YYYY HH:mm") : "-"}</td>
                                <td>{e.report_content.portInformation.noon_lat ? e.report_content.portInformation.noon_lat : "-"}</td>
                                <td>{e.report_content.portInformation.noon_main_long ? e.report_content.portInformation.noon_main_long : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_ordered_spd ? e.report_content.vesselDistance.noon_ordered_spd : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_observe_dist ? e.report_content.vesselDistance.noon_observe_dist : "-"}</td>
                                 <td>
                                    {e.report_content.bunker ? 
                                      (e.report_content.bunker.noon_aux_exngine_info ? parseInt(e.report_content.bunker.noon_aux_exngine_info) : 0) +
                                      (e.report_content.bunker.noon_boiler_info ? parseInt(e.report_content.bunker.noon_boiler_info) : 0) +
                                      (e.report_content.bunker.noon_brob_info ? parseInt(e.report_content.bunker.noon_brob_info) : 0) +
                                      (e.report_content.bunker.noon_fuel_debunker_info ? parseInt(e.report_content.bunker.noon_fuel_debunker_info) : 0) +
                                      (e.report_content.bunker.noon_fuel_receoved_info ? parseInt(e.report_content.bunker.noon_fuel_receoved_info) : 0) +
                                      (e.report_content.bunker.noon_generator_info ? parseInt(e.report_content.bunker.noon_generator_info) : 0) +
                                      (e.report_content.bunker.noon_me_propulsion_info ? parseInt(e.report_content.bunker.noon_me_propulsion_info) : 0)
                                      : "-"
                                    }
                                </td>
                                <td>
                                    {e.report_content.bunker ? 
                                      (e.report_content.bunker.noon_aux_exngine_mgo ? parseInt(e.report_content.bunker.noon_aux_exngine_mgo) : 0) +
                                      (e.report_content.bunker.noon_boiler_mgo ? parseInt(e.report_content.bunker.noon_boiler_mgo) : 0) +
                                      (e.report_content.bunker.noon_brob_mgo ? parseInt(e.report_content.bunker.noon_brob_mgo) : 0) +
                                      (e.report_content.bunker.noon_fuel_debunker_mgo ? parseInt(e.report_content.bunker.noon_fuel_debunker_mgo) : 0) +
                                      (e.report_content.bunker.noon_fuel_receoved_mgo ? parseInt(e.report_content.bunker.noon_fuel_receoved_mgo) : 0) +
                                      (e.report_content.bunker.noon_generator_mgo ? parseInt(e.report_content.bunker.noon_generator_mgo) : 0) +
                                      (e.report_content.bunker.noon_me_propulsion_mgo ? parseInt(e.report_content.bunker.noon_me_propulsion_mgo) : 0)
                                      : "-"
                                    }
                                </td>
                                <td>
                                    {e.report_content.bunker ? 
                                      (e.report_content.bunker.noon_aux_exngine_lsmgo ? parseInt(e.report_content.bunker.noon_aux_exngine_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_boiler_lsmgo ? parseInt(e.report_content.bunker.noon_boiler_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_brob_lsmgo ? parseInt(e.report_content.bunker.noon_brob_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_fuel_debunker_lsmgo ? parseInt(e.report_content.bunker.noon_fuel_debunker_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_fuel_receoved_lsmgo ? parseInt(e.report_content.bunker.noon_fuel_receoved_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_generator_lsmgo ? parseInt(e.report_content.bunker.noon_generator_lsmgo) : 0) +
                                      (e.report_content.bunker.noon_me_propulsion_lsmgo ? parseInt(e.report_content.bunker.noon_me_propulsion_lsmgo) : 0)
                                      : "-"
                                    }
                                </td>
                                <td>
                                    {e.report_content.bunker ? 
                                      (e.report_content.bunker.noon_aux_exngine_ulsfo ? parseInt(e.report_content.bunker.noon_aux_exngine_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_boiler_ulsfo ? parseInt(e.report_content.bunker.noon_boiler_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_brob_ulsfo ? parseInt(e.report_content.bunker.noon_brob_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_fuel_debunker_ulsfo ? parseInt(e.report_content.bunker.noon_fuel_debunker_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_fuel_receoved_ulsfo ? parseInt(e.report_content.bunker.noon_fuel_receoved_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_generator_ulsfo ? parseInt(e.report_content.bunker.noon_generator_ulsfo) : 0) +
                                      (e.report_content.bunker.noon_me_propulsion_ulsfo ? parseInt(e.report_content.bunker.noon_me_propulsion_ulsfo) : 0)
                                      : "-"
                                    }
                                </td>
                                <td>
                                    {e.report_content.bunker ? 
                                      (e.report_content.bunker.noon_aux_exngine_vlsfo ? parseInt(e.report_content.bunker.noon_aux_exngine_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_boiler_vlsfo ? parseInt(e.report_content.bunker.noon_boiler_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_brob_vlsfo ? parseInt(e.report_content.bunker.noon_brob_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_fuel_debunker_vlsfo ? parseInt(e.report_content.bunker.noon_fuel_debunker_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_fuel_receoved_vlsfo ? parseInt(e.report_content.bunker.noon_fuel_receoved_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_generator_vlsfo ? parseInt(e.report_content.bunker.noon_generator_vlsfo) : 0) +
                                      (e.report_content.bunker.noon_me_propulsion_vlsfo ? parseInt(e.report_content.bunker.noon_me_propulsion_vlsfo) : 0)
                                      : "-"
                                    }
                                </td>
                                <td>{e.report_content.vesselDistance.noon_sea_state ? e.report_content.vesselDistance.noon_sea_state : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_sea_height ? e.report_content.vesselDistance.noon_sea_height : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_air_press ? e.report_content.vesselDistance.noon_air_press : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_air_tempt ? e.report_content.vesselDistance.noon_air_tempt : "-"}</td>
                                <td>{e.report_content.vesselDistance.noon_wind_dir ? e.report_content.vesselDistance.noon_wind_dir : "-"}</td>
                            </tr>
                        )
                    })
                }
                {/* Noon Reports End */}
                {/* Arrival Row Start */}
                <tr>
                <td>{lastArrivaldata?.report_type === 1 ? "Arrival" : "-"}</td>
                <td>{lastArrivaldata ? moment(lastArrivaldata[0]?.created_date).format("DD-MM-YYYY HH:mm") : "-"}</td>
                <td>{lastArrivaldata?.report_content?.mainInformation?.arrival_main_lat ? lastArrivaldata?.report_content?.mainInformation?.arrival_main_lat : "-"}</td>
                <td>{lastArrivaldata?.report_content?.mainInformation?.arrival_main_long ? lastArrivaldata?.report_content?.mainInformation?.arrival_main_long : "-"}</td>
                <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_ordered_spd ? lastArrivaldata?.report_content?.vesselDistance?.arrival_ordered_spd : "-"}</td>
                <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_observe_dist ? lastArrivaldata?.report_content?.vesselDistance?.arrival_observe_dist : "-"}</td>
                <td>
                    {
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_aux_exngine_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_boiler_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_brob_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_de_bllast_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_debunker_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_receoved_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_generator_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_heat_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_idle_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_ld_info || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_me_propulsion_info || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_aux_exngine_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_boiler_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_brob_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_de_bllast_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_debunker_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_receoved_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_generator_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_heat_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_idle_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_ld_mgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_me_propulsion_mgo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_aux_exngine_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_boiler_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_brob_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_de_bllast_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_debunker_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_receoved_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_generator_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_heat_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_idle_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_ld_lsmgo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_me_propulsion_lsmgo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_aux_exngine_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_boiler_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_brob_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_de_bllast_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_debunker_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_fuel_receoved_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_generator_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_heat_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_idle_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_ld_ulsfo || 0) +
                        parseInt(lastArrivaldata?.report_content?.bunker?.arrival_me_propulsion_ulsfo || 0)
                    }
                    </td>
                    <td>
                    {
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_aux_exngine_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_boiler_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_brob_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_de_bllast_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_debunker_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_fuel_receoved_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_generator_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_heat_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_idle_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_ld_vlsfo || 0) +
                        parseInt(filterDepartureData[0]?.report_content?.bunker?.departure_me_propulsion_vlsfo || 0)
                    }
                    </td>
                    <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_sea_state ? lastArrivaldata?.report_content?.vesselDistance?.arrival_sea_state : "-"}</td>
                    <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_sea_height ? lastArrivaldata?.report_content?.vesselDistance?.arrival_sea_height : "-"}</td>
                    <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_air_press ? lastArrivaldata?.report_content?.vesselDistance?.arrival_air_press : "-"}</td>
                    <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_air_tempt ? lastArrivaldata?.report_content?.vesselDistance?.arrival_air_tempt : "-"}</td>
                    <td>{lastArrivaldata?.report_content?.vesselDistance?.arrival_wind_dir ? lastArrivaldata?.report_content?.vesselDistance?.arrival_wind_dir : "-"}</td>
                </tr>
                {/* Arrival Row End */}
              </tbody>
            </table>

            <h3 className="font-weight-bold text-primary text-center mb-3">
              Map on Itinerary
            </h3>
            <div className="row">
              <div className="col-12" style={{minHeight: '450px', overflow: 'hidden'}}>
                <MapComponent type="consolidateReport" coordinates={coordinates} />
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class ConsolidateReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Printer",
      voyNo: this.props.voyNo,
      sumData: this.props.sumData,
    };
  }

  printReceipt() {
    window.print();
  }

  render() {

    const filterDepartureData = this.props.data?.allReport?.filter(obj => obj.report_type === 4);
    const firstDeparture = filterDepartureData[0];

    const departureLat = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_lat) || '';
    const departureLong = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_long) || '';

    const departureLongLat = [departureLong, departureLat]

    const filterArrivaleData = this.props.data?.allReport?.filter(obj => obj.report_type === 1);
    const lastArrivaldata = filterArrivaleData[filterArrivaleData.length - 1];

    const filterNoonData = this.props.data?.allReport?.filter(obj => obj.report_type === 5);

    const noonLat = filterNoonData?.map((e) =>  Number.parseFloat(e.report_content?.portInformation?.noon_lat))
    const noonLong = filterNoonData?.map((e) => Number.parseFloat(e.report_content?.portInformation?.noon_main_long))

    const mergredNoonArray = noonLong.map((value, index) => [value, noonLat[index]]);
    const arrivalLat = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_lat) || '';
    const arrivalLong = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_long) || '';

    const arrivalLongLat = [arrivalLong, arrivalLat]

    const startDestination = [departureLongLat, ...mergredNoonArray, arrivalLongLat]

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                type={this.props.type}
                latLong={startDestination}
                data={this.props.data}
                sumData={this.props.sumData}
                voyNo={this.props.voyNo}
                ref={(el) => (this.componentRef = el)}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ConsolidateReport;
