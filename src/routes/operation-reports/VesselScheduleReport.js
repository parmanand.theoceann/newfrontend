import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    let formReportdata ={
      
    }

    this.state = {
      formReportdata:Object.assign(formReportdata, this.props.data || {}),
      reportFormData:{}
    }
    
  }
  render() {

    const{formReportdata} = this.state
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>
            
            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Date From/TO :</td>
                  <td>{formReportdata && formReportdata.vessel_name?formReportdata.vessel_name:" "}</td>

                  <td className="font-weight-bold">Vessel Name :</td>
                  <td>{formReportdata && formReportdata.vessel_name?formReportdata.vessel_name:" "}</td>

                  <td className="font-weight-bold">Vessel Type :</td>
                  <td>{formReportdata && formReportdata.vessel_name?formReportdata.vessel_name:" "}</td>

                  <td className="font-weight-bold">Status :</td>
                  <td>{formReportdata && formReportdata.vessel_name?formReportdata.vessel_name:" "}</td>
                </tr>

              </tbody>
            </table>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vessel</th>
                  <th>Company</th>
                  <th>From Date</th>
                  <th>To Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
                
                
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class VesselScheduleReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                           <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
            <ComponentToPrint ref={el => (this.componentRef = el)} data={this.props.data}/>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default VesselScheduleReport;
