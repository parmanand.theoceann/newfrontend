import { Modal, Form  } from 'antd';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import URL_WITH_VERSION, { awaitPostAPICall, openNotificationWithIcon, useStateCallback } from '../../shared';
import Email from '../../components/Email/index';
import moment from 'moment';

import html2canvas from "html2canvas";

import { useRef, forwardRef } from 'react';

const FormItem = Form.Item;

const  ComponentToPrint = forwardRef((props, ref) => {

  const [state,  setState] = useStateCallback({
    showEmailModal: false,
    formReportdata:Object.assign({}, props.data || {}),
  })

  const{formReportdata} = state

    return (
      <article className="article toolbaruiWrapper" ref={ref}>
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">{formReportdata.logo}</span>
                  <p className="sub-title m-0">{formReportdata && formReportdata.full_name?formReportdata.full_name:"N/A"}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata && formReportdata.address?formReportdata.address:"N/A"}</p>
                </div>
              </div>
            </div>
            
            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Vessel :</td>
                  <td>{formReportdata && formReportdata.vessel_name?formReportdata.vessel_name:"N/A"}</td>

                  <td className="font-weight-bold">Port :</td>
                  <td>{ formReportdata && formReportdata.port_name?formReportdata.port_name:"N/A"}</td>

                  <td className="font-weight-bold">Procurement Status :</td>
                  <td>{formReportdata && formReportdata.procurement_status_name?formReportdata.procurement_status_name:"N/A"}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Vessel Code :</td>
                  <td>{formReportdata && formReportdata.vessel_code?formReportdata.vessel_code:"N/A"}</td>

                  <td className="font-weight-bold">ETA :</td>
                  <td>{formReportdata && formReportdata.eta?formReportdata.eta:"N/A"}</td>

                  <td className="font-weight-bold">Request By :</td>
                  <td>{formReportdata && formReportdata.request_by?formReportdata.request_by:"N/A "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Voyage No :</td>
                  <td>{formReportdata && formReportdata.voyage_no?formReportdata.voyage_no:" "}</td>

                  <td className="font-weight-bold">ETD :</td>
                  <td>{formReportdata && formReportdata.etd?formReportdata.etd:" "}</td>

                  <td className="font-weight-bold">Delivery Type :</td>
                  <td>{formReportdata && formReportdata.delivery_type_name?formReportdata.delivery_type_name:" "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">TCI Owner :</td>
                  <td>{formReportdata && formReportdata.tci_owner?formReportdata.tci_owner:"N/A "}</td>

                  <td className="font-weight-bold">Delivery FRm/TO :</td>
                  <td>{formReportdata && formReportdata.delivary_from?formReportdata.delivary_from:" "}/{formReportdata && formReportdata.delivary_to?formReportdata.delivary_to:" "}</td>

                  <td className="font-weight-bold">Last Updated  :</td>
                  <td>{formReportdata && !formReportdata.last_updated.includes('0000')?formReportdata.last_updated:"N/A "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Charterer :</td>
                  <td>{formReportdata && formReportdata.charterer_name?formReportdata.charterer_name:"N/A "}</td>

                  <td className="font-weight-bold">Request Date :</td>
                  <td>{formReportdata && formReportdata.request_date?formReportdata.request_date:" "}</td>

                  <td className="font-weight-bold">Last Updated By :</td>
                  <td>{formReportdata && formReportdata.last_updated_by?formReportdata.last_updated_by:"N/A "}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">My Company :</td>
                  <td>{formReportdata && formReportdata.my_company_name? formReportdata.my_company_name:"N/A"}</td>

                  <td className="font-weight-bold">Request Status :</td>
                  <td>{formReportdata && formReportdata.request_status_name?formReportdata.request_status_name:"N/A"}</td>

                  <td className="font-weight-bold">Requirement Id. :</td>
                  <td>{formReportdata && formReportdata.requirement_id?formReportdata.requirement_id:"N/A"}</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Agent :</td>
                  <td>{formReportdata && formReportdata.agent_name?formReportdata.agent_name:"N/A"}</td>

                  <td className="font-weight-bold">Payment Status :</td>
                  <td>{formReportdata && formReportdata.add_payment_status_name?formReportdata.add_payment_status_name:"N/A"}</td>

                  <td className="font-weight-bold">Remark :</td>
                  <td>{formReportdata && formReportdata.remark?formReportdata.remark:"N/A"}</td>

                  {/* <td colSpan="2"></td> */}
                </tr>
              </tbody>
            </table>

            <h4>Planned Lifting</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Grade</th>
                  <th>Min Qty</th>
                  <th>Requrd Qty</th>
                  <th>Max Qty</th>
                  <th>Rcvd Qty</th>
                  <th>BDN Qty</th>
                  <th>Unit</th>
                  <th>Sulphur %</th>
                </tr>
              </thead>
              <tbody>
              {formReportdata.plannedliftings && formReportdata.plannedliftings && formReportdata.plannedliftings.length > 0 ? formReportdata.plannedliftings.map((e, idx)=>{
                              return (
                                <>
                     <tr key={idx}>
                 
                  <td>{e.fuel_type_name?e.fuel_type_name:" "}</td>
                  <td>{e.grade_name?e.grade_name:" "}</td>
                  <td>{e.min_qty?e.min_qty:" "}</td>
                  <td>{e.required_qty?e.required_qty:" "}</td>
                  <td>{e.max_qty?e.max_qty:" "}</td>
                  <td>{e.recieved_qty?e.recieved_qty:" "}</td>
                  <td>{e.bnd_qty?e.bnd_qty:" "}</td>
                  <td>{e.unit?e.unit:" "}</td>
                  <td>{e.sulphur_name?e.sulphur_name:" "}</td>
                </tr>
                </>
                        )
                    }):undefined

                    }
              </tbody>
            </table>

             <h4>Bunker Purchase Order Summary</h4>
            <table className="table border-table table-invoice-report-colum">
                 <thead>
                    <tr>
                        <th>Voyage No.</th>
                        <th>Vendor Name</th>
                        <th>Company Name</th>
                        <th>Port Name</th>
                        <th>Requirement Id</th>
                        <th>Vessel Name</th>
                        <th>Delivery From</th>
                        <th>Delivery To </th>
                        <th>Purchase Status:</th>
                        <th>Request Status</th>
                        <th>Order Date</th>
                        <th>Invoice Amt</th>
                    </tr>
                </thead>
                <tbody>
                  {formReportdata &&formReportdata.bunker_pur_summary&&formReportdata.bunker_pur_summary.length>0?formReportdata.bunker_pur_summary.map((e,idx) => (
                    <tr key={idx}>
                    <td>{ e.voyage_no ? e.voyage_no:"N/A"}</td>
                    <td>{ e.agent_name? e.agent_name:"N/A"}</td>
                    <td>{ e.my_company_name? e.my_company_name:"N/A"}</td>
                     <td>{ e.port_name? e.port_name:"N/A"}</td>
                     <td>{ e.requirement_id? e.requirement_id:"N/A"}</td>
                     <td>{ e.vessel_name? e.vessel_name:"N/A"}</td>
                     <td>{ e.delivary_from? e.delivary_from:"N/A"}</td>
                     <td>{ e.delivary_to? e.delivary_to:"N/A"}</td>
                     <td>{ e.purchase_status_name? e.purchase_status_name:"N/A"}</td>
                     <td>{ e.request_status_name? e.request_status_name:"N/A"}</td>
                     <td>{ e.order_date?moment(e.order_date).format('YYYY-MM-DD'):"N/A"}</td>
                     <td>{ e.invoice_amt? e.invoice_amt:"N/A"}</td>
                  </tr>
                  )):undefined}
                    
                </tbody>
            </table>
        {/* <h4>Inquiries and Purchases</h4>
            <table className="table border-table table-invoice-report-colum">
                <thead>
                    <tr>
                        <th>Port</th>
                        <th>Vendor</th>
                        <th>S</th>
                        <th>For Account</th>
                        <th>Status</th>
                        <th>Curr</th>
                        <th>Exch Rate</th>
                        <th>HFO Qty</th>
                        <th>HFO Price</th>
                        <th>MDO Qty</th>
                        <th>MDO Price</th>
                        <th>MGO Qty</th>
                        <th>MGO Price</th>
                        <th>Barg Price</th>
                        <th>Barging</th>
                        <th>Other</th>
                        <th>Portcost</th>
                        <th>Tax</th>
                        <th>Total Ammount</th>
                        <th>Pay Terms</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{formReportdata.port}</td>
                        <td>{formReportdata.vendor_name}</td>
                        <td>{formReportdata.supplier}</td>
                        <td>{formReportdata.for_account_name}</td>
                       
                       <td>{formReportdata.purchase_status_name}</td>
                       <td>{formReportdata.currency}</td>
                        <td>{formReportdata.exch_rate}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.barging_amount}</td>
                        <td>{formReportdata.other_cost}</td>
                        <td>{formReportdata.port_cost}</td>
                        <td>{formReportdata.tax}</td>
                        <td>{formReportdata.status}</td>
                        <td>{formReportdata.payment_terms}</td>
                    </tr>

                   
                </tbody>
            </table> */}
          </div>
        </div>
      </article>
    );
  })

const  BunkerRequirementReport = (props) =>  {

  const [state, setState] = useStateCallback({
    name: 'Printer',
  })

  const componentRef = useRef()
  const printReceipt = () => {
    window.print();
  }
      

  const printDocument=()=>{
    // in this method report is downloading, but logo is not coming showing err of loading image of s3.     

    html2canvas( document.getElementById("divToPrint"), {
       logging: true,
       letterRendering: 1,
       useCORS: true,
       allowTaint: true    }).then(function(canvas) {
       // download image      
       const link = document.createElement("a");
       link.download = "html-to-img.png";
       const pdf = new jsPDF();
       const imgProps = pdf.getImageProperties(canvas);
       const pdfWidth = pdf.internal.pageSize.getWidth();
       const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
       pdf.addImage(canvas, "PNG", 0, 0, pdfWidth, pdfHeight);

       pdf.save("bunkerRequirementReport.pdf");

   }).catch=(err)=>{
     console.log(err)
   };
 }

  const showEmailDialog = () => setState({ ...state, showEmailModal: true })
  
  const emailDocument = (email, subject, message) => {
    htmlToImage.toPng(document.getElementById('divToPrint'), { quality: 0.95 })
      .then(async function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        const pdf = new jsPDF();
        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
        // pdf.save("Bunker_requirement_report.pdf"); 
        // const data = pdf.output('datauri');
        // const datad = pdf.output('datauri');
        // console.log('@@@', datad);
        const params = {
          subject,
          message,
          recepients: [email],
          attachments: [{
            "ContentType": "application/pdf",
            "Filename": "sample.pdf",
            "Base64Content": "JVBERi0xLjMNCiXi48/TDQoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL091dGxpbmVzIDIgMCBSDQovUGFnZXMgMyAwIFINCj4+DQplbmRvYmoNCg0KMiAwIG9iag0KPDwNCi9UeXBlIC9PdXRsaW5lcw0KL0NvdW50IDANCj4+DQplbmRvYmoNCg0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0NvdW50IDINCi9LaWRzIFsgNCAwIFIgNiAwIFIgXSANCj4+DQplbmRvYmoNCg0KNCAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDw8DQovRm9udCA8PA0KL0YxIDkgMCBSIA0KPj4NCi9Qcm9jU2V0IDggMCBSDQo+Pg0KL01lZGlhQm94IFswIDAgNjEyLjAwMDAgNzkyLjAwMDBdDQovQ29udGVudHMgNSAwIFINCj4+DQplbmRvYmoNCg0KNSAwIG9iag0KPDwgL0xlbmd0aCAxMDc0ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBBIFNpbXBsZSBQREYgRmlsZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIFRoaXMgaXMgYSBzbWFsbCBkZW1vbnN0cmF0aW9uIC5wZGYgZmlsZSAtICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjY0LjcwNDAgVGQNCigganVzdCBmb3IgdXNlIGluIHRoZSBWaXJ0dWFsIE1lY2hhbmljcyB0dXRvcmlhbHMuIE1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NTIuNzUyMCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDYyOC44NDgwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjE2Ljg5NjAgVGQNCiggdGV4dC4gQW5kIG1vcmUgdGV4dC4gQm9yaW5nLCB6enp6ei4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjA0Ljk0NDAgVGQNCiggbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDU5Mi45OTIwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNTY5LjA4ODAgVGQNCiggQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA1NTcuMTM2MCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBFdmVuIG1vcmUuIENvbnRpbnVlZCBvbiBwYWdlIDIgLi4uKSBUag0KRVQNCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCjYgMCBvYmoNCjw8DQovVHlwZSAvUGFnZQ0KL1BhcmVudCAzIDAgUg0KL1Jlc291cmNlcyA8PA0KL0ZvbnQgPDwNCi9GMSA5IDAgUiANCj4+DQovUHJvY1NldCA4IDAgUg0KPj4NCi9NZWRpYUJveCBbMCAwIDYxMi4wMDAwIDc5Mi4wMDAwXQ0KL0NvbnRlbnRzIDcgMCBSDQo+Pg0KZW5kb2JqDQoNCjcgMCBvYmoNCjw8IC9MZW5ndGggNjc2ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBTaW1wbGUgUERGIEZpbGUgMiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIC4uLmNvbnRpbnVlZCBmcm9tIHBhZ2UgMS4gWWV0IG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NzYuNjU2MCBUZA0KKCBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY2NC43MDQwIFRkDQooIHRleHQuIE9oLCBob3cgYm9yaW5nIHR5cGluZyB0aGlzIHN0dWZmLiBCdXQgbm90IGFzIGJvcmluZyBhcyB3YXRjaGluZyApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY1Mi43NTIwIFRkDQooIHBhaW50IGRyeS4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NDAuODAwMCBUZA0KKCBCb3JpbmcuICBNb3JlLCBhIGxpdHRsZSBtb3JlIHRleHQuIFRoZSBlbmQsIGFuZCBqdXN0IGFzIHdlbGwuICkgVGoNCkVUDQplbmRzdHJlYW0NCmVuZG9iag0KDQo4IDAgb2JqDQpbL1BERiAvVGV4dF0NCmVuZG9iag0KDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL05hbWUgL0YxDQovQmFzZUZvbnQgL0hlbHZldGljYQ0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCg0KMTAgMCBvYmoNCjw8DQovQ3JlYXRvciAoUmF2ZSBcKGh0dHA6Ly93d3cubmV2cm9uYS5jb20vcmF2ZVwpKQ0KL1Byb2R1Y2VyIChOZXZyb25hIERlc2lnbnMpDQovQ3JlYXRpb25EYXRlIChEOjIwMDYwMzAxMDcyODI2KQ0KPj4NCmVuZG9iag0KDQp4cmVmDQowIDExDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTkgMDAwMDAgbg0KMDAwMDAwMDA5MyAwMDAwMCBuDQowMDAwMDAwMTQ3IDAwMDAwIG4NCjAwMDAwMDAyMjIgMDAwMDAgbg0KMDAwMDAwMDM5MCAwMDAwMCBuDQowMDAwMDAxNTIyIDAwMDAwIG4NCjAwMDAwMDE2OTAgMDAwMDAgbg0KMDAwMDAwMjQyMyAwMDAwMCBuDQowMDAwMDAyNDU2IDAwMDAwIG4NCjAwMDAwMDI1NzQgMDAwMDAgbg0KDQp0cmFpbGVyDQo8PA0KL1NpemUgMTENCi9Sb290IDEgMCBSDQovSW5mbyAxMCAwIFINCj4+DQoNCnN0YXJ0eHJlZg0KMjcxNA0KJSVFT0YNCg=="
          }]
        }
        const response1 = await awaitPostAPICall(`${URL_WITH_VERSION}/email/send-with-attachment`, params);
        setState({ ...state, showEmailModal: false })
        openNotificationWithIcon('success', 'The report was successfully sent to the provided email.');

        // openNotificationWithIcon('error', 'Failed to send email. Please try again.');
        // setState({ ...state, showEmailModal: false })
      })
  }


    const { showEmailModal } = state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={printDocument}>
                        Download
                      </li>
                      <li onClick={showEmailDialog}>
                        Send Mail
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => componentRef.current}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
            <ComponentToPrint ref={componentRef} data={props.data} />
            </div>
          </div>
        </article>

        <Modal
          title="Email"
          open={showEmailModal}
          onOk={() => { setState({ ...state, showEmailModal: false }) }}
          onCancel={() => { setState({ ...state, showEmailModal : false}) }}
          footer={null}
        >
          <Email emailDocument={emailDocument} />
        </Modal>

      </div>
    );
  }

export default BunkerRequirementReport;
