import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Time Frame :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Passage :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Vessel :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Fixture no :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Voy No :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Ops Type :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Voyage Status :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Ops User :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Passage Type :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Report Type :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Commence :</td>
                  <td>Value</td>

                  <td className="font-weight-bold">Competed :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td className="font-weight-bold">Last Update :</td>
                  <td>Value</td>

                  <td colSpan="4"></td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vessel Name</th>
                  <th>Vsl Type</th>
                  <th>Report Type</th>
                  <th>Report Date/Time</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th colSpan="11" className="text-center">
                    DIstance & Vessel
                  </th>
                </tr>
                <tr>
                  <th>Position (Lat/Long)</th>
                  <th>Ordered SPD</th>
                  <th>Reported SPD</th>
                  <th>% Change</th>
                  <th>Observed Dist.</th>
                  <th>Eng. Dist.</th>
                  <th>Slip %</th>
                  <th>Ave. RPM</th>
                  <th>Ave. BHP</th>
                  <th>Steaming Hr</th>
                  <th>Displacement</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th colSpan="6" className="text-center">
                    Weather
                  </th>
                </tr>
                <tr>
                  <th>Sea State</th>
                  <th>Swell Height</th>
                  <th>Wind Force</th>
                  <th>Current</th>
                  <th>Bad Weather Hr</th>
                  <th>Bad Weather Dist.</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th colSpan="10" className="text-center">
                    Bunker Cons.
                  </th>
                </tr>
                <tr>
                  <th>M/E Propulsion</th>
                  <th>Over Cons.</th>
                  <th>Maneuver</th>
                  <th>L/D</th>
                  <th>Aux Engine</th>
                  <th>Generator</th>
                  <th>DTG</th>
                  <th>Next Port</th>
                  <th>ETA</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class NonVerificationReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                         <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default NonVerificationReport;
