import React, { Component, useEffect } from "react";
import { PrinterOutlined } from "@ant-design/icons";
import { Menu, Dropdown, Modal, Spin } from "antd";
import ReactToPrint from "react-to-print";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../shared";
import jsPDF from "jspdf";
//import * as htmlToImage from 'html-to-image';
import html2canvas from "html2canvas";
import Email from "../../components/Email";
import moment from "moment";
import { useState } from "react";
import { useRef } from "react";
import { forwardRef } from "react";

const ComponentToPrint = forwardRef((props, ref) => {
  const [formReportdata, setFormReportdata] = useState(
    Object.assign({}, props.data || {})
  );

  return (
    <article className="article toolbaruiWrapper" ref={ref}>
      <div className="box box-default" id="divToPrint">
        <div className="box-body">
          <div className="invoice-inner-download mt-3">
            <div className="row">
              <div className="col-12 text-center">
                <img
                  className="reportlogo"
                  src={formReportdata.logo ? formReportdata.logo : "N/A"}
                  alt="No img"
                />
                <p className="title">
                  {formReportdata.full_name ? formReportdata.full_name : "N/A"}
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 mx-auto">
              <div className="text-center invoice-top-address">
                <p>{formReportdata.address ? formReportdata.address : "N/A"}</p>
              </div>
            </div>
          </div>
          <table className="table custom-table-bordered tc-table">
            <tbody>
              <tr>
                <td className="font-weight-bold">Vessel Name :</td>
                <td>
                  {formReportdata.vessel_name
                    ? formReportdata.vessel_name
                    : "N/A"}
                </td>

                <td className="font-weight-bold">My Company/LOB :</td>
                <td>
                  {formReportdata && formReportdata.my_company_lob_name
                    ? formReportdata.my_company_lob_name
                    : "N/A"}{" "}
                  /{" "}
                  {formReportdata && formReportdata.company_lob_name
                    ? formReportdata.company_lob_name
                    : "N/A"}
                </td>

                {/* <td className="font-weight-bold">B. Port :</td>
                <td>
                  {formReportdata.b_port_name
                    ? formReportdata.b_port_name
                    : "N/A"}
                </td> */}

                <td className="font-weight-bold">Status :</td>
                <td>
                  {formReportdata.cvm_status_name
                    ? formReportdata.cvm_status_name
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">Vessel Code / Voy No. :</td>
                <td>
                  {formReportdata.vessel_code
                    ? formReportdata.vessel_code
                    : "N/A"}
                  /
                  {formReportdata.voyage_number
                    ? formReportdata.voyage_number
                    : "N/A"}
                </td>

                <td className="font-weight-bold">C/P Date :</td>
                <td>
                  {formReportdata && formReportdata.cp_date
                    ? formReportdata.cp_date
                    : "N/A"}
                </td>

                <td className="font-weight-bold"> Voyage Ops Type :</td>
                <td>
                  {formReportdata && formReportdata.ops_type_name
                    ? formReportdata.ops_type_name
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">HF/TCI Code :</td>
                <td>
                  {formReportdata && formReportdata.tc_code
                    ? formReportdata.tc_code
                    : "N/A"}
                </td>

                <td className="font-weight-bold">WF (%) :</td>
                <td>
                  {formReportdata && formReportdata.dwf
                    ? formReportdata.dwf
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Trader / Ops User :</td>
                <td>
                  {formReportdata && formReportdata.fixed_by_user
                    ? formReportdata.fixed_by_user
                    : "N/A"}{" "}
                  /{" "}
                  {formReportdata && formReportdata.ops_user_name
                    ? formReportdata.ops_user_name
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">HF/TCI Hire Rate :</td>
                <td>
                  {formReportdata && formReportdata.tci_d_hire
                    ? formReportdata.tci_d_hire
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Other Cost:</td>
                <td>
                  {formReportdata && formReportdata.other_cost
                    ? formReportdata.other_cost
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Commence Date :</td>
                <td>
                  {formReportdata && formReportdata.commence_date
                    ? formReportdata.commence_date
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">Add/Bro(%) :</td>
                <td>
                  {formReportdata && formReportdata.add_percentage
                    ? formReportdata.add_percentage
                    : "N/A"}
                  /
                  {formReportdata && formReportdata.bro_percentage
                    ? formReportdata.bro_percentage
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Ballast Bonus :</td>
                <td>
                  {formReportdata && formReportdata.bb
                    ? formReportdata.bb
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Completing Date :</td>
                <td>
                  {formReportdata && formReportdata.completing_date
                    ? formReportdata.completing_date
                    : "N/A"}
                </td>
              </tr>
              <tr>
                <td className="font-weight-bold">TCO Code</td>
                <td>TCO Code</td>

                <td className="font-weight-bold">Other Revenue :</td>
                <td>
                  {formReportdata && formReportdata.mis_cost
                    ? formReportdata.mis_cost
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Total Voyage Days :</td>
                <td>
                  {formReportdata && formReportdata.total_days
                    ? formReportdata.total_days
                    : "N/A"}
                </td>
              </tr>

              <tr>
                <td className="font-weight-bold">Eca Fuel Grade</td>
                <td>
                  {formReportdata && formReportdata.eca_fuel_grade_name
                    ? formReportdata.eca_fuel_grade_name
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Routing Type :</td>
                <td>
                  {formReportdata && formReportdata.routing_type_name
                    ? formReportdata.routing_type_name
                    : "N/A"}
                </td>

                <td className="font-weight-bold">Trade Area :</td>
                <td>
                  {formReportdata && formReportdata.trade_area_name
                    ? formReportdata.trade_area_name
                    : "N/A"}
                </td>
              </tr>
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <tbody>
              <td>
                <h5 className="font-weight-bold">Eco/Cp speed</h5>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr className="HeaderBoxText">
                      <th rowSpan="2">SPD Type</th>
                      <th colSpan="1" className="text-center">
                        Ballast
                      </th>

                      <th colSpan="1" className="text-center">
                        Laden
                      </th>
                    </tr>
                    <tr className="HeaderBoxText">
                      <th>Kt</th>
                      {/* <th>Cons</th> */}
                      <th>Kt</th>
                      {/* <th>Cons</th> */}
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata["-"] && formReportdata["-"].length > 0
                      ? formReportdata["-"].map((e, idx) => {
                          return (
                            <>
                              <tr key={idx}>
                                <td>{e.spd_type ? e.spd_type : "N/A"}</td>
                                <td>{e.ballast_spd ? e.ballast_spd : "N/A"}</td>
                                {/* <td>{e.ballast_con ? e.ballast_con : "N/A"}</td> */}
                                <td>{e.laden_spd ? e.laden_spd : "N/A"}</td>
                                {/* <td>{e.laden_con ? e.laden_con : "N/A"}</td> */}
                              </tr>
                            </>
                          );
                        })
                      : undefined}
                  </tbody>
                </table>
              </td>
              <td>
                <h5 className="font-weight-bold">Speed/Cons and price $</h5>
                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr className="HeaderBoxText">
                      {/* <th>P$</th> */}
                      <th>CP$</th>
                      <th>Fuel</th>
                      <th>Laden</th>
                      <th>Ballast</th>
                      <th>Load</th>
                      <th>Disch</th>
                      {/* <th>Heat</th> */}
                      <th>Idle</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formReportdata["."] && formReportdata["."].length > 0
                      ? formReportdata["."].map((e, idx) => {
                          return (
                            <>
                              <tr key={idx}>
                                {/* <td>
                                  {e.purchase_price ? e.purchase_price : "N/A"}
                                </td> */}
                                <td>{e.cp_price ? e.cp_price : "N/A"}</td>
                                <td>{e.fuel_code ? e.fuel_code : "N/A"}</td>
                                <td>{e.laden_value ? e.laden_value : "N/A"}</td>
                                <td>
                                  {e.ballast_value ? e.ballast_value : "N/A"}
                                </td>
                                <td>{e.con_loading ? e.con_loading : "N/A"}</td>
                                <td>{e.con_disch ? e.con_disch : "N/A"}</td>
                                {/* <td>{e.con_heat ? e.con_heat : "N/A"}</td> */}
                                <td>
                                  {e.con_ideal_on ? e.con_ideal_on : "N/A"}
                                </td>
                              </tr>
                            </>
                          );
                        })
                      : undefined}
                  </tbody>
                </table>
              </td>
            </tbody>
          </table>

          {/*------------------------- TC-EStimate  Table--------- */}

          {formReportdata?.ops_type_name === "TC Estimate" ? (
            <>
              <h5 className="font-weight-bold tc-sub-header">Tco Term</h5>
              <table className="table custom-table-bordered tc-table">
                <thead>
                  <tr className="HeaderBoxText">
                    <th>Charterer</th>
                    <th>Broker</th>
                    <th>Duration</th>
                    <th>Daily Hire</th>
                    <th>Rate Type</th>
                    <th>Ballast Bonus</th>
                    <th>A.Com %</th>
                    <th>B.Com%</th>
                    <th>BB COMM</th>
                  </tr>
                </thead>

                <tbody>
                  {formReportdata.tcoterm && formReportdata.tcoterm.length > 0
                    ? formReportdata.tcoterm.map((e, idx) => {
                        return (
                          <>
                            <tr key={idx}>
                              <td>{e.charterer ? e.charterer : "N/A"}</td>
                              <td>{e.broker ? e.broker : "N/A"}</td>
                              <td>{e.duration ? e.duration : "N/A"}</td>
                              <td>{e.dailyhire ? e.dailyhire : "N/A"}</td>
                              <td>{e.ratetype ? e.ratetype : "N/A"}</td>
                              <td>{e.bb ? e.bb : "N/A"}</td>
                              <td>{e.acom ? e.acom : "N/A"}</td>
                              <td>{e.bcom ? e.bcom : "N/A"}</td>
                              <td>{e.bbcomm ? e.bbcomm : "N/A"}</td>
                            </tr>
                          </>
                        );
                      })
                    : undefined}
                </tbody>
              </table>

              <h5 className="font-weight-bold tc-sub-header">Term</h5>
              <table className="table custom-table-bordered tc-table">
                <thead>
                  <tr className="HeaderBoxText">
                    <th>Delivery Port</th>
                    <th>Delivery Time</th>
                    <th>Redelivery Port</th>
                    <th>Redelivery Time</th>
                  </tr>
                </thead>

                <tbody>
                  {formReportdata.term && formReportdata.term.length > 0
                    ? formReportdata.term.map((e, idx) => {
                        return (
                          <>
                            <tr key={idx}>
                              <td>{e.port_del ? e.port_del : "N/A"}</td>
                              <td>{e.del_time ? e.del_time : "N/A"}</td>
                              <td>{e.reddel_port ? e.reddel_port : "N/A"}</td>
                              <td>{e.reddel_time ? e.reddel_time : "N/A"}</td>
                            </tr>
                          </>
                        );
                      })
                    : undefined}
                </tbody>
              </table>
            </>
          ) : (
            <>
              <h5 className="font-weight-bold tc-sub-header">Cargo</h5>
              <table className="table custom-table-bordered tc-table">
                <thead>
                  <tr className="HeaderBoxText">
                    <th>Cargo ID</th>
                    <th>Charterer</th>
                    <th>Cargo Name</th>
                    <th>CP Qty</th>
                    <th>Unit</th>
                    <th>Opt %</th>
                    <th>Opt Type</th>
                    <th>Freight Type</th>
                    <th>Frt Rate</th>
                    <th>Lumpsum</th>
                    <th>World scale</th>
                    <th>Comm%</th>
                    <th>Extra Rev</th>
                    <th>Curr</th>
                  </tr>
                </thead>
                <tbody>
                  {formReportdata.cargos && formReportdata.cargos.length > 0
                    ? formReportdata.cargos.map((e, idx) => {
                        return (
                          <>
                            <tr key={idx}>
                              <td>
                                {e.cargo_contract_id
                                  ? e.cargo_contract_id
                                  : "N/A"}
                              </td>
                              <td>
                                {e.charterer_name ? e.charterer_name : "N/A"}
                              </td>
                              <td>{e.cargo_name ? e.cargo_name : "N/A"}</td>
                              <td>{e.cp_qty ? e.cp_qty : "N/A"}</td>
                              <td>{e.cp_unit_name ? e.cp_unit_name : "N/A"}</td>
                              <td>
                                {e.opt_percentage ? e.opt_percentage : "N/A"}
                              </td>
                              <td>
                                {e.opt_type_name ? e.opt_type_name : "N/A"}
                              </td>
                              <td>
                                {e.frt_type_name ? e.frt_type_name : "N/A"}
                              </td>
                              <td>{e.frat_rate ? e.frat_rate : "N/A"}</td>
                              <td>{e.lumsum ? e.lumsum : "N/A"}</td>
                              <td>NA</td>
                              <td>{e.b_commission ? e.b_commission : "N/A"}</td>
                              <td>{e.extra_rev ? e.extra_rev : "N/A"}</td>
                              <td>{e.curr ? e.curr : "USD-USA"}</td>
                            </tr>
                          </>
                        );
                      })
                    : undefined}
                </tbody>
              </table>
            </>
          )}

          <h4 className="font-weight-bold tc-sub-header">Port Itinerary</h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th>Port</th>
                <th>Passage Type</th>
                <th>Function</th>
                <th>Speed TYPE</th>
                <th>Eca Seca Miles</th>
                <th>Eca Seca Days</th>
                <th>Miles</th>
                <th>WF %</th>
                <th>SPD</th>
                <th>Eff-SPD</th>
                <th>GSD</th>
                <th>TSD</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.portitinerary &&
              formReportdata.portitinerary.length > 0
                ? formReportdata.portitinerary.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          {/* <td>{e.port_id ? e.port_id : "N/A"}</td> */}
                          <td>{e.port ? e.port : "N/A"}</td>
                          <td>{e.passagename ? e.passagename : "N/A"}</td>
                          <td>{e.funct_name ? e.funct_name : "N/A"}</td>
                          <td>{e.s_type_name ? e.s_type_name : "N/A"}</td>
                          <td>{e.seca_length ? e.seca_length : "N/A"}</td>
                          <td>{e.eca_days ? e.eca_days : "N/A"}</td>
                          <td>{e.miles ? e.miles : "0"}</td>
                          <td>{e.wf_per ? e.wf_per : "0.00"}</td>
                          <td>{e.speed ? e.speed : "0.00"}</td>
                          <td>{e.eff_speed ? e.eff_speed : "0.00"}</td>
                          <td>{e.gsd ? e.gsd : "0.00"}</td>
                          <td>{e.tsd ? e.tsd : "0.00"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th>XSD</th>
                <th>L/D QTY</th>
                <th>L/D Rate(D)</th>
                <th>L/D Rate(H)</th>
                <th>L/D Term</th>
                <th>TurnTime</th>
                <th>P Days</th>
                <th>Xpd</th>
                <th>P.EXP</th>
                <th>Total Port Days</th>
                <th>Demurrage / Dispatch</th>
                <th>Dem/Des Final Amount</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.portitinerary &&
              formReportdata.portitinerary.length > 0
                ? formReportdata.portitinerary.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.xsd ? e.xsd : "0.00"}</td>
                          <td>{e.l_d_qty ? e.l_d_qty : "0.00"}</td>
                          <td>{e.l_d_rate ? e.l_d_rate : "0.00"}</td>
                          <td>{e.l_d_rate1 ? e.l_d_rate1 : "0.00"}</td>
                          <td>{e.l_d_term_name ? e.l_d_term_name : "N/A"}</td>
                          <td>{e.turn_time ? e.turn_time : "0.00"}</td>
                          <td>
                            {e.days ? parseFloat(e.days).toFixed(2) : "0.00"}
                          </td>
                          <td>
                            {e.xpd ? parseFloat(e.xpd).toFixed(2) : "0.00"}
                          </td>
                          <td>{e.p_exp ? e.p_exp : "0.00"}</td>
                          <td>
                            {e.t_port_days
                              ? parseFloat(e.t_port_days).toFixed(2)
                              : "0.00"}
                          </td>
                          <td>{e.dem_disp}</td>
                          <td>{e.dem_disp_amt ? e.dem_disp_amt : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <h4 className="font-weight-bold tc-sub-header">Bunker Details</h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th rowSpan="2">Port</th>
                <th rowSpan="2">Miles</th>
                <th rowSpan="2">Function</th>
                <th rowSpan="2">Passage</th>
                <th rowSpan="2">TSD</th>
                <th rowSpan="2">ECA SECA MILES</th>
                <th rowSpan="2">ECA SECA Days</th>
                <th rowSpan="2">Arrival</th>
                <th rowSpan="2">Departure</th>
                <th rowspan="2">ECA SECA Cons(In MT)</th>
                <th colSpan="5">Arrival ROB</th>
                
              </tr>
              <tr className="HeaderBoxText">
                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.bunkerdetails &&
              formReportdata.bunkerdetails.length > 0
                ? formReportdata.bunkerdetails.map((e, idx) => {
                  console.log("e",e);
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.port ? e.port : "N/A"}</td>
                          <td>{e.miles ? e.miles : "0"}</td>
                          <td>{e.funct_name ? e.funct_name : "N/A"}</td>

                          <td>{e.passagename ? e.passagename : "N/A"}</td>
                          <td>{e.tsd ? e.tsd : "N/A"}</td>
                          <td>{e.seca_length ? e.seca_length : "0.00"}</td>
                          <td>{e.eca_days ? e.eca_days : "0.00"}</td>
                          <td>
                            {e.arrival_date_time &&
                            e.arrival_date_time !== "0000-00-00 00:00"
                              ? e.arrival_date_time
                              : "N/A"}
                          </td>
                          <td>
                            {e.departure && e.departure !== "0000-00-00 00:00"
                              ? e.departure
                              : "N/A"}
                          </td>
                          <td>{e.eca_consp ? e.eca_consp : "N/A"}</td>

                          <td>{e.arob_ifo ? e.arob_ifo : "0.00"}</td>
                          <td>{e.arob_vlsfo ? e.arob_vlsfo : "0.00"}</td>
                          <td>{e.arob_lsmgo ? e.arob_lsmgo : "0.00"}</td>
                          <td>{e.arob_mgo ? e.arob_mgo : "0.00"}</td>
                          <td>{e.arob_ulsfo ? e.arob_ulsfo : "0.00"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th colSpan="5">Sea Consumption (MT)</th>
                <th colSpan="5">Port Cons(MT)</th>
                <th colSpan="5">Received Fuel</th>
             
              </tr>
              <tr className="HeaderBoxText">
                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>

                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>

                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>

              </tr>
            </thead>
            <tbody>
              {formReportdata.bunkerdetails &&
              formReportdata.bunkerdetails.length > 0
                ? formReportdata.bunkerdetails.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.ifo ? e.ifo : "0.00"}</td>
                          <td>{e.vlsfo ? e.vlsfo : "0.00"}</td>
                          <td>{e.lsmgo ? e.lsmgo : "0.00"}</td>
                          <td>{e.mgo ? e.mgo : "0.00"}</td>
                          <td>{e.ulsfo ? e.ulsfo : "0.00"}</td>
                          <td>{e.pc_ifo ? e.pc_ifo : "N/A"}</td>
                          <td>{e.pc_vlsfo ? e.pc_vlsfo : "N/A"}</td>
                          <td>{e.pc_lsmgo ? e.pc_lsmgo : "N/A"}</td>
                          <td>{e.pc_mgo ? e.pc_mgo : "N/A"}</td>
                          <td>{e.pc_ulsfo ? e.pc_ulsfo : "N/A"}</td>
                          <td>{e.r_ifo ? e.r_ifo : "N/A"}</td>
                          <td>{e.r_vlsfo ? e.r_vlsfo : "N/A"}</td>
                          <td>{e.r_lsmgo ? e.r_lsmgo : "N/A"}</td>
                          <td>{e.r_mgo ? e.r_mgo : "N/A"}</td>
                          <td>{e.r_ulsfo ? e.r_ulsfo : "N/A"}</td>
                        
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th colSpan="5">DEP.ROB</th>
              </tr>
              <tr className="HeaderBoxText">
                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.bunkerdetails &&
              formReportdata.bunkerdetails.length > 0
                ? formReportdata.bunkerdetails.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          {/* <td>{e.arob_ifo ? e.arob_ifo : "0.00"}</td>
                          <td>{e.arob_vlsfo ? e.arob_vlsfo : "0.00"}</td>
                          <td>{e.arob_lsmgo ? e.arob_lsmgo : "0.00"}</td>
                          <td>{e.arob_mgo ? e.arob_mgo : "0.00"}</td>
                          <td>{e.arob_ulsfo ? e.arob_ulsfo : "0.00"}</td> */}

                          <td>{e.dr_ifo ? e.dr_ifo : "N/A"}</td>
                          <td>{e.dr_vlsfo ? e.dr_vlsfo : "N/A"}</td>
                          <td>{e.dr_lsmgo ? e.dr_lsmgo : "N/A"}</td>
                          <td>{e.dr_mgo ? e.dr_mgo : "N/A"}</td>
                          <td>{e.dr_ulsfo ? e.dr_ulsfo : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <h4 className="font-weight-bold tc-sub-header">
            Total Bunker Details
          </h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th rowSpan="2">Miles</th>
                <th rowSpan="2">Eca/Seca Dist.</th>
                <th rowSpan="2">Eca/Seca Days</th>
                <th rowSpan="2">SPD</th>
                <th rowSpan="2">ULSFO</th>
                <th rowSpan="2">ECA S.Cons</th>
                <th rowSpan="2">S. Cons IFO</th>
                <th rowSpan="2">S. Cons VLSFO</th>
                <th rowSpan="2">S. Cons LSMGO</th>
                <th rowSpan="2">S. Cons MGO</th>
                <th rowSpan="2">S. Cons ULSFO</th>
                <th rowSpan="2">P. Cons IFO</th>
                <th rowSpan="2">P. Cons VLSFO</th>
                <th rowSpan="2">P. Cons LSMGO</th>
                <th rowSpan="2">P. Cons MGO</th>
                <th rowSpan="2">P. Cons ULSFO</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.totalbunkerdetails &&
              formReportdata.totalbunkerdetails &&
              formReportdata.totalbunkerdetails.length > 0
                ? formReportdata.totalbunkerdetails.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.ttl_miles ? e.ttl_miles : "0.00"}</td>
                          <td>
                            {e.ttl_seca_length ? e.ttl_seca_length : "0.00"}
                          </td>
                          <td>{e.ttl_eca_days ? e.ttl_eca_days : "0.00"}</td>
                          <td>
                            {e.ttl_speed
                              ? parseFloat(e.ttl_speed).toFixed(2)
                              : "0.00"}
                          </td>
                          <td>NA</td>
                          <td>NA</td>
                          <td>{e.ttl_ifo ? e.ttl_ifo : "N/A"}</td>
                          <td>{e.ttl_vlsfo ? e.ttl_vlsfo : "N/A"}</td>
                          <td>{e.ttl_lsmgo ? e.ttl_lsmgo : "N/A"}</td>
                          <td>{e.ttl_mgo ? e.ttl_mgo : "N/A"}</td>
                          <td>{e.ttl_ulsfo ? e.ttl_ulsfo : "N/A"}</td>
                          <td>{e.ttl_pc_ifo ? e.ttl_pc_ifo : "N/A"}</td>
                          <td>{e.ttl_pc_vlsfo ? e.ttl_pc_vlsfo : "N/A"}</td>
                          <td>{e.ttl_pc_lsmgo ? e.ttl_pc_lsmgo : "N/A"}</td>
                          <td>{e.ttl_pc_mgo ? e.ttl_pc_mgo : "N/A"}</td>
                          <td>{e.ttl_pc_ulsfo ? e.ttl_pc_ulsfo : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          <h4 className="font-weight-bold tc-sub-header">Port Date Details</h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th>Port</th>
                <th>Funct.</th>
                <th>MILES</th>
                <th>Passage</th>
                <th>STYPE</th>
                <th>SPD</th>
                <th>WF%</th>
                <th>TSD</th>

                <th>Arrival Date Time</th>

                <th>T.PDays</th>
                <th>Departure date-time</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.portdatedetails &&
              formReportdata.portdatedetails.length > 0
                ? formReportdata.portdatedetails.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.port ? e.port : "N/A"}</td>
                          <td>{e.funct_name ? e.funct_name : "N/A"}</td>
                          <td>{e.miles ? e.miles : "0"}</td>
                          <td>{e.passagename ? e.passagename : "N/A"}</td>
                          <td>{e.s_type_name ? e.s_type_name : "N/A"}</td>
                          <td>
                            {e.speed ? parseFloat(e.speed).toFixed(2) : "N/A"}
                          </td>
                          <td>
                            {e.wf_per
                              ? parseFloat(e.wf_per).toFixed(2)
                              : "0.00"}
                          </td>
                          <td>{e.tsd ? e.tsd : "N/A"}</td>

                          <td>
                            {e.arrival_date_time &&
                            e.arrival_date_time !== "0000-00-00 00:00"
                              ? e.arrival_date_time
                              : "N/A"}
                          </td>

                          <td>{e.pdays ? e.pdays : "N/A"}</td>
                          <td>{e.departure ? e.departure : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                : undefined}
            </tbody>
          </table>
          {/* 
            as port cargo tab is removed from vm page. so we are removing this port cargo portion from report.
            <h4 className="font-weight-bold">Port Cargo</h4> 
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Port ID</th>
                  <th>Port</th>
                  <th>Funct.</th>
                  <th>TSD</th>
                  <th>Cargo</th>
                  <th>Grade</th>
                  <th>BL Qty</th>
                  <th>B/L Date</th>
                  <th>L/D Date</th>
                  <th>L/D Qty</th>
                  <th>L/D Rate(D)</th>
                  <th>L/D Rate(H)</th>
                  <th>TERM</th>
                  <th>TT</th>
                  <th>PDays</th>
                  <th>XPD</th>
                  <th>T.PDays</th>
                </tr>
              </thead>
              <tbody>
              
                {formReportdata.portdatedetails &&
                  formReportdata.portdatedetails.length > 0 ? formReportdata.portdatedetails.map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.port_id?e.port_id:""}</td>
                          <td>{e.port?e.port:""}</td>
                          <td>{e.funct_name?e.funct_name:""}</td>
                          <td>{e.tsd?e.tsd:""}</td>
                          <td>{e.cargo?e.cargo:""}</td>
                          <td>{e.grade_name?e.grade_name:""}</td>
                          <td>{e.bl_qty?e.bl_qty:""}</td>
                          <td>{e.bl_date?e.bl_date:""}</td>
                          <td>{e.ld_date?e.ld_date:""}</td>
                          <td>{e.ld_qty?e.ld_qty:""}</td>
                          <td>{e.ld_rate_d?e.ld_rate_d:""}</td>
                          <td>{e.ld_rate_h?e.ld_rate_h:""}</td>
                          <td>{e.term_name?e.term_name:""}</td>
                          <td>{e.tt?e.tt:""}</td>
                          <td>{e.p_days?e.p_days:""}</td>
                          <td>{e.xpd?e.xpd:""}</td>
                          <td>{e.t_p_days?e.t_p_days:""}</td>
                        </tr>
                      </>
                    )
                  }) : undefined

                }

              </tbody>
            </table> */}
          {/* <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th>Total Distance</th>
                <th>TTL Port Days</th>
                <th>TSD</th>
                <th>TTL Qty</th>
                <th>GSD</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>
                  {formReportdata["---------"]
                    ? formReportdata["---------"].total_distance
                    : "0"}{" "}
                  <span>Miles</span>
                </td>
                <td>
                  {formReportdata["---------"]
                    ? formReportdata["---------"].totalt_port_days
                    : ""}{" "}
                  <span>Days</span>
                </td>
                <td>
                  {formReportdata["---------"]
                    ? formReportdata["---------"].total_tsd
                    : ""}{" "}
                  <span>Days</span>
                </td>
                <td>
                  {formReportdata["---------"]
                    ? formReportdata["---------"].total_qty
                    : ""}{" "}
                  <span>Mt</span>
                </td>
                <td>
                  {formReportdata["---------"]
                    ? formReportdata["---------"].total_gsd
                    : ""}{" "}
                  <span>Days</span>
                </td>
              </tr>
            </tbody>
          </table> */}
          <h4 className="font-weight-bold tc-sub-header">CII Dynamics</h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th rowSpan="2">Year</th>
                <th rowSpan="2">Port</th>
                <th rowSpan="2">Function</th>
                <th rowSpan="2">Passage</th>
                <th colSpan="3">Operation Days</th>
                <th rowSpan="2">Total Distance Sailed(miles)</th>
                <th colSpan="5">Sea Consumption</th>
                <th colSpan="5">port Consumption</th>
                
                
               
                
                
                
              </tr>
              <tr className="HeaderBoxText">


            
                <th>Sea</th>
                <th>Port</th>
                <th>Total</th>

                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>

                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>
              </tr>
            </thead>

            <tbody>
              {formReportdata.ciidynamics &&
                formReportdata.ciidynamics.length > 0 &&
                formReportdata.ciidynamics.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.year ? e.year : "N/A"}</td>
                        <td>{e.port ? e.port : "N/A"}</td>
                        <td>{e.funct ? e.funct : "N/A"}</td>
                        <td>{e.passage_name ? e.passage_name : "N/A"}</td>
                        <td>{e.sea ? e.sea : "N/A"}</td>
                        <td>
                          {e.totalt_port_days ? e.totalt_port_days : "N/A"}
                        </td>
                        <td>{e.total ? e.total : "N/A"}</td>
                        <td>{e.dst_sailed ? e.dst_sailed : "NA"}</td>
                        <td>{e.ifo ? e.ifo : "N/A"}</td>
                        <td>{e.vlsfo ? e.vlsfo : "N/A"}</td>
                        <td>{e.lsmgo ? e.lsmgo : "N/A"}</td>
                        <td>{e.mgo ? e.mgo : "N/A"}</td>
                        <td>{e.ulsfo ? e.ulsfo : "N/A"}</td>
                        <td>{e.ttl_pc_ifo ? e.ttl_pc_ifo : "N/A"}</td>
                        <td>{e.ttl_pc_vlsfo ? e.ttl_pc_vlsfo : "N/A"}</td>
                        <td>{e.pc_ulsfo ? e.pc_ulsfo : "N/A"}</td>
                        <td>{e.ttl_pc_mgo ? e.ttl_pc_mgo : "N/A"}</td>
                        <td>{e.pc_lsmgo ? e.pc_lsmgo : "N/A"}</td>
                       
                        
                       
                      
                       
                       
                      </tr>
                    </>
                  );
                })}
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
              <th rowSpan="2">Co2 Emission VLSFO/ULSFO (MT)</th>
              <th rowSpan="2">Co2 Emission MGO/LSMGO (MT)</th>
              <th rowSpan="2">Co2 Emission IFO (MT)</th>
              <th rowSpan="2">Total Co2 Emission (MT)</th>
              <th rowSpan="2">CII Ref</th>
                <th rowSpan="2">CII Required</th>
                <th rowSpan="2">CII Attained</th>
                <th rowSpan="2">CII Rating</th>
                <th rowSpan="2">CII Band</th>
                <th rowSpan="2">Predicted Required CII 2025</th>
                <th rowSpan="2">Predicted CII Rating 2025</th>
              
           
              </tr>
              <tr className="HeaderBoxText">
                {/* <th>Sea</th>
                <th>Port</th>
                <th>Total</th> */}

                {/* <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th> */}

                {/* <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th> */}
              </tr>
            </thead>

            <tbody>
              {formReportdata.ciidynamics &&
                formReportdata.ciidynamics.length > 0 &&
                formReportdata.ciidynamics.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                      <td>{e.co2_emission_vu ? e.co2_emission_vu : "N/A"}</td>
                      <td>{e.co2_emission_ml ? e.co2_emission_ml : "N/A"}</td>
                      <td>
                          {e.co2_emission_ifo ? e.co2_emission_ifo : "N/A"}
                        </td>
                        <td>
                          {e.co2_emission_total ? e.co2_emission_total : "N/A"}
                        </td>
                        <td>{e.cii_ref ? e.cii_ref : "N/A"}</td>
                        <td>{e.cii_req ? e.cii_req : "N/A"}</td>
                        <td>{e.cii_att ? e.cii_att : "N/A"}</td>
                        <td>{e.cii_ret ? e.cii_ret : "N/A"}</td>
                        <td>{e.cii_band ? e.cii_band : "N/A"}</td> 
                        <td>{e.cii_pred ? e.cii_pred : "N/A"}</td>
                        <td>{e.cii_pred_ret ? e.cii_pred_ret : "N/A"}</td>
                        {/* <td>{e.sea ? e.sea : "N/A"}</td>
                        <td>
                          {e.totalt_port_days ? e.totalt_port_days : "N/A"}
                        </td>
                        <td>{e.total ? e.total : "N/A"}</td> */}
                        {/* <td>{e.ifo ? e.ifo : "N/A"}</td>
                        <td>{e.vlsfo ? e.vlsfo : "N/A"}</td>
                        <td>{e.lsmgo ? e.lsmgo : "N/A"}</td>
                        <td>{e.mgo ? e.mgo : "N/A"}</td>
                        <td>{e.ulsfo ? e.ulsfo : "N/A"}</td> */}
                        {/* <td>{e.ttl_pc_ifo ? e.ttl_pc_ifo : "N/A"}</td>
                        <td>{e.ttl_pc_vlsfo ? e.ttl_pc_vlsfo : "N/A"}</td>
                        <td>{e.pc_ulsfo ? e.pc_ulsfo : "N/A"}</td>
                        <td>{e.ttl_pc_mgo ? e.ttl_pc_mgo : "N/A"}</td>
                        <td>{e.pc_lsmgo ? e.pc_lsmgo : "N/A"}</td> */}
                      </tr>
                    </>
                  );
                })}
            </tbody>
          </table>
          <h4 className="font-weight-bold tc-sub-header">
            Total CII Dynamics Summary
          </h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th>VLSFO Consumption:</th>
                <th>ULSFO Consumption:</th>
                <th>IFO Consumption:</th>
                <th>LSMGO Consumption:</th>
                <th>MGO Consumption:</th>
                <th>CO2 Emission:</th>
              </tr>
            </thead>
            {formReportdata.totalciidynamicssummary &&
              formReportdata.totalciidynamicssummary.length > 0 &&
              formReportdata.totalciidynamicssummary.map((e, idx) => {
                return (
                  <>
                    <tr key={idx}>
                      <td>{e.ttl_vlsfo_con ? e.ttl_vlsfo_con : "N/A"}</td>
                      <td>{e.ttl_ulsfo_con ? e.ttl_ulsfo_con : "N/A"}</td>
                      <td>{e.ttl_ifo_con ? e.ttl_ifo_con : "N/A"}</td>
                      <td>{e.ttl_lsmgo_con ? e.ttl_lsmgo_con : "N/A"}</td>
                      <td>{e.ttl_mgo_con ? e.ttl_mgo_con : "N/A"}</td>
                      <td>
                        {e.ttl_co2_emission_total
                          ? e.ttl_co2_emission_total
                          : "N/A"}
                      </td>
                    </tr>
                  </>
                );
              })}
          </table>
          <h4 className="font-weight-bold tc-sub-header">EU ETS</h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th rowSpan="2">Year</th>
                <th rowSpan="2">Port</th>
                <th rowSpan="2">Function</th>
                <th colSpan="5">Sea Consumption</th>
                <th colSpan="5">Port Consumption</th>
                <th rowSpan="2">Sea Emission (MT)</th>
                <th rowSpan="2">Port Emission (MT)</th>
                <th rowSpan="2">Total Emission(MT)</th>
               
              </tr>

              <tr className="HeaderBoxText">
                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>

                <th>IFO</th>
                <th>VLSFO</th>
                <th>LSMGO</th>
                <th>MGO</th>
                <th>ULSFO</th>
              </tr>
            </thead>

            <tbody>
              {formReportdata.euets &&
                formReportdata.euets.length > 0 &&
                formReportdata.euets.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.year ? e.year : "N/A"}</td>
                        <td>{e.port ? e.port : "N/A"}</td>
                        <td>{e.funct_name ? e.funct_name : "N/A"}</td>
                        <td>{e.ifo ? e.ifo : "N/A"}</td>
                        <td>{e.vlsfo ? e.vlsfo : "N/A"}</td>
                        <td>{e.lsmgo ? e.lsmgo : "N/A"}</td>
                        <td>{e.mgo ? e.mgo : "N/A"}</td>
                        <td>{e.ulsfo ? e.ulsfo : "N/A"}</td>
                        <td>{e.pc_ifo ? e.pc_ifo : "N/A"}</td>
                        <td>{e.pc_vlsfo ? e.pc_vlsfo : "N/A"}</td>
                        <td>{e.pc_lsmgo ? e.pc_lsmgo : "N/A"}</td>
                        <td>{e.pc_mgo ? e.pc_mgo : "N/A"}</td>
                        <td>{e.pc_ulsfo ? e.pc_ulsfo : "N/A"}</td>
                        
                        
                        
                        
                        
                        
                        
                        
                        <td>{e.sea_ems ? e.sea_ems : "N/A"}</td>
                        <td>{e.port_ems ? e.port_ems : "N/A"}</td>
                        <td>{e.ttl_ems ? e.ttl_ems : "N/A"}</td>



                      
                      </tr>
                    </>
                  );
                })}
            </tbody>
          </table>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
              <th rowSpan="2">Phase In %</th>
                <th rowSpan="2">Sea ETS %</th>
                <th rowSpan="2">Sea ETS Emission (MT)</th>
                <th rowSpan="2">Port ETS %</th>
                <th rowSpan="2">Port ETS Emission (MT)</th>
                <th rowSpan="2">Total EU ETS (MT)</th>
                <th rowSpan="2">Total EU ETS Expense</th>
              </tr>

             
            </thead>

            <tbody>
              {formReportdata.euets &&
                formReportdata.euets.length > 0 &&
                formReportdata.euets.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                      <td>{e.phase_pre ? e.phase_pre : "N/A"}</td>
                        <td>{e.sea_pre_ets ? e.sea_pre_ets : "N/A"}</td>
                        <td>{e.sea_ets_ems ? e.sea_ets_ems : "N/A"}</td>
                        <td>{e.port_pre_ems ? e.port_pre_ems : "N/A"}</td>
                        <td>{e.port_ets_ems ? e.port_ets_ems : "N/A"}</td>
                        <td>{e.ttl_eu_ets ? e.ttl_eu_ets : "N/A"}</td>
                        <td>{e.ttl_eu_ets_exp ? e.ttl_eu_ets_exp : "N/A"}</td>
                      </tr>
                    </>
                  );
                })}
            </tbody>
          </table>
          <h4 className="font-weight-bold tc-sub-header">
            Total EU ETS Summary
          </h4>
          <table className="table custom-table-bordered tc-table">
            <thead>
              <tr className="HeaderBoxText">
                <th rowSpan="2">Total Sea Emission</th>
                <th rowSpan="2">Total Port Emission</th>
                <th rowSpan="2">Total Emission</th>
                <th rowSpan="2">Total Sea Ets Emission</th>
                <th rowSpan="2">Total Port Ets Emission</th>
                <th rowSpan="2">Total Ets Emission</th>
                <th rowSpan="2">Total EU-ETS Emission</th>
              </tr>
            </thead>
            <tbody>
              {formReportdata.totaleuetssummary &&
                formReportdata.totaleuetssummary.length > 0 &&
                formReportdata.totaleuetssummary.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.ttl_sea_emi ? e.ttl_sea_emi : "N/A"}</td>
                        <td>{e.ttl_port_emi ? e.ttl_port_emi : "N/A"}</td>
                        <td>{e.ttl_emi ? e.ttl_emi : "N/A"}</td>
                        <td>{e.ttl_sea_ets_emi ? e.ttl_sea_ets_emi : "N/A"}</td>
                        <td>
                          {e.ttl_port_ets_emi ? e.ttl_port_ets_emi : "N/A"}
                        </td>
                        <td>{e.ttl_ets_emi ? e.ttl_ets_emi : "N/A"}</td>
                        <td>{e.ttl_eu_ets_emi ? e.ttl_eu_ets_emi : "N/A"}</td>
                      </tr>
                    </>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </article>
  );
});

const VoyageManagementReport = (props) => {
  const [pdfData, setPdfData] = useState();
  const [userInput, setUserInput] = useState();
  const [loading, setLoading] = useState(false);
  const [emailModal, setEmailModal] = useState(false);
  const [mailTitlePayload, setMailTitlePayload] = useState({});
  const [name, setname] = useState("Printer");
  const [state, setState] = useStateCallback({
    name: "Printer",
  });

  const componentRef = useRef();
  useEffect(() => {
    setUserInput(props.data);
  }, []);

  const printReceipt = () => {
    window.print();
  };

  useEffect(() => {
    setUserInput(props.data);
  }, []);

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns } = state;

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}`,
      "_blank"
    );
  };

  const printDocument = () => {
    var quotes = document.getElementById("divToPrint");

    html2canvas(quotes, {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true,
    }).then(function (canvas) {
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      var imgWidth = 210;
      var pageHeight = 290;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF("p", "mm");
      var position = 20;
      var pageData = canvas.toDataURL("image/jpeg", 1.0);
      var imgData = encodeURIComponent(pageData);
      doc.addImage(imgData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          imgData,
          "PNG",
          5,
          position + 20,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }
      doc.save("operationReport.pdf");
    });
  };

  const sendEmailReportModal = async () => {
    try {
      setLoading(true);

      const quotes = document.getElementById("divToPrint");

      const canvas = await html2canvas(quotes, {
        logging: true,
        letterRendering: 1,
        useCORS: true,
        allowTaint: true,
        scale: 2,
      });

      const imgWidth = 210;
      const pageHeight = 290;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      const doc = new jsPDF("p", "mm");
      let position = 25;
      const pageData = canvas.toDataURL("image/jpeg", 1.0);
      doc.addImage(pageData, "PNG", 5, position, imgWidth - 8, imgHeight - 7);
      doc.setLineWidth(5);
      doc.setDrawColor(255, 255, 255);
      doc.rect(0, 0, 210, 295);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          pageData,
          "PNG",
          5,
          position + 25,
          imgWidth - 8,
          imgHeight - 7
        );
        doc.setLineWidth(5);
        doc.setDrawColor(255, 255, 255);
        doc.rect(0, 0, 210, 295);
        heightLeft -= pageHeight;
      }

      // Create Blob
      const blob = doc.output("blob");

      // Use the blob as needed (e.g., send it to the server, create a download link, etc.)
      setLoading(false);
      setPdfData(blob);
      setEmailModal(true);
    } catch (error) {
      console.error("Error:", error);
      setLoading(false);
      // this.setState({ loading: false });
      // Handle errors here
    }
  };
  return (
    <div className="body-wrapper modalWrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection"></div>
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li onClick={sendEmailReportModal}>Send Email</li>
                    <li onClick={printDocument}>Download</li>
                    <li>
                      <ReactToPrint
                        trigger={() => (
                          <span className="text-bt">
                            <PrinterOutlined />
                            Print
                          </span>
                        )}
                        content={() => componentRef.current}
                      />
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <ComponentToPrint ref={componentRef} data={props.data} />
          </div>
        </div>
      </article>
      {emailModal && (
        <Modal
          title="New Message"
          visible={emailModal}
          onOk={() => {
            setEmailModal(false);
          }}
          onCancel={() => {
            setEmailModal(false);
          }}
          footer={null}
        >
          {pdfData && (
            <Email
              handleClose={() => {
                setEmailModal(false);
              }}
              attachmentFile={pdfData}
              title={window.corrector(
                `Voyage Operation Report||${userInput.vessel_name}||${userInput.full_name}||${userInput.company_lob_name}||${userInput.cvm_status_name}`
              )}
              // title={`Voyage Operation Report|| (${userInput.vessel_name})||${userInput.full_name}||${userInput.company_lob_name}||${userInput.cvm_status_name}`}
            />
          )}
        </Modal>
      )}
      {loading && (
        <div
          style={{
            position: "absolute",
            top: "10%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        >
          <Spin size="large" />
        </div>
      )}
    </div>
  );
};

export default VoyageManagementReport;
