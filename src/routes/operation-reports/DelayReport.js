import React, { useRef, useState,forwardRef } from "react";
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from "react-to-print";
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";
import html2canvas from "html2canvas";


const  ComponentToPrint = forwardRef((props, ref) => {
  const [formReportdata,setFormReportdata] = useState(Object.assign({}, props.data || {}))

    return (
      <article className="article toolbaruiWrapper" ref={ref}>
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <img
                    className="reportlogo"
                    crossOrigin="anonymous"
                    src={formReportdata.logo}
                    alt="No Img"
                  />
                  <p className="sub-title m-0">{formReportdata && formReportdata.full_name ? formReportdata.full_name : "N/A"}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata && formReportdata.address ? formReportdata.address : "N/A"}</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Vessel</td>
                  <td>{formReportdata && formReportdata.vessel_name ? formReportdata.vessel_name : "N/A"}</td>
                  <td className="font-weight-bold">Vessel Code</td>
                  <td>{formReportdata && formReportdata.vessel_code ? formReportdata.vessel_code : "N/A"}</td>
                  <td className="font-weight-bold">Voy No.</td>
                  <td>{formReportdata && formReportdata.voyage_manager_name ? formReportdata.voyage_manager_name : "N/A"}</td>
                  <td className="font-weight-bold">Commencing</td>
                  <td>{formReportdata && formReportdata.commencing ? formReportdata.commencing : "N/A"}</td>
                  <td className="font-weight-bold">Completing</td>
                  <td>{formReportdata && formReportdata.completing ? formReportdata.completing : "N/A"}</td>
                </tr>
              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Activity</th>
                  <th>Reason</th>
                  <th>Form</th>
                  <th>Time Zone</th>
                  <th>To</th>
                  <th>Time Zone</th>
                  <th>Hours</th>
                  <th>Miles</th>
                  <th>MGO Qty</th>
                  <th>MGO ROB</th>
                  <th> ULSFO Qty</th>
                  <th>ULSFO$ / MT</th>
                </tr>
              </thead>
              <tbody>
                {formReportdata["."] &&
                  formReportdata["."] &&
                  formReportdata["."].length > 0
                  ? formReportdata["."].map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.activity_name ? e.activity_name : "N/A"} </td>
                          <td>{e.reason_name ? e.reason_name : "N/A"}</td>
                          <td>{e.delay_from ? e.delay_from : "N/A"}</td>
                          <td>{e.timezone ? e.timezone : "N/A"}</td>
                          <td>{e.delay_to ? e.delay_to : "N/A"}</td>
                          <td>{e.timezone2 ? e.timezone2 : "N/A"}</td>
                          <td>{e.hours ? e.hours : "N/A"}</td>
                          <td>{e.miles ? e.miles : "N/A"}</td>
                          <td>{e.mgo_qty ? e.mgo_qty : "N/A"}</td>
                          <td>{e.mgo_rob ? e.mgo_rob : "N/A"}</td>
                          <td>{e.ulsfo_qty ? e.ulsfo_qty : "N/A"}</td>
                          <td>{e.ulsfo_mt ? e.ulsfo_mt : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                  : undefined}
              </tbody>
            </table>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th> VLSFO Qty</th>
                  <th>VLSFO$ / MT</th>
                  <th> IFO Qty</th>
                  <th>IFO$ / MT</th>
                  <th>LSMGO Qty</th>
                  <th> LSMGO$ / MT</th>
                  <th>Contract type</th>
                  <th>TCI / TCO Lumpsum</th>
                  <th>TCI / TCO final hire amount</th>
                  <th> TCI / TCO bunker value</th>
                  <th>TCI Daily Cost</th>
                  <th>Remark</th>
                  <th>Last Update</th>
                  <th>Last Update by</th>
                </tr></thead>
              <tbody>
                {formReportdata["."] &&
                  formReportdata["."] &&
                  formReportdata["."].length > 0
                  ? formReportdata["."].map((e, idx) => {
                    return (
                      <>
                        <tr key={idx}>
                          <td>{e.vlsfo_qty ? e.vlsfo_qty : "0.00"}</td>
                          <td>{e.vlsfo_mt ? e.vlsfo_mt : "0.00"}</td>
                          <td>{e.ifo_qty ? e.ifo_qty : "0.00"}</td>
                          <td>{e.ifo_mt ? e.ifo_mt : "0.00"}</td>
                          <td>{e.lsmgo_qty ? e.lsmgo_qty : "0.00"}</td>
                          <td>{e.lsmgo_mt ? e.lsmgo_mt : "0.00"}</td>
                          <td>{e.contract_type_name ? e.contract_type_name : "N/A"}</td>
                          <td>{e.tci_tco_lumsum ? e.tci_tco_lumsum : "N/A"}</td>
                          <td>{e.tci_tco_final_hire_amt ? e.tci_tco_final_hire_amt : "N/A"}</td>
                          <td>{e.tci_tco_bunker_value ? e.tci_tco_bunker_value : "N/A"}</td>
                          <td>{e.tci_daily_cost ? e.tci_daily_cost : "N/A"}</td>
                          <td>{e.remarks ? e.remarks : "N/A"}</td>
                          <td>{e.last_updated ? e.last_updated : "N/A"}</td>
                          <td>{e.last_updated_by ? e.last_updated_by : "N/A"}</td>
                        </tr>
                      </>
                    );
                  })
                  : undefined}
              </tbody>

            </table>
          </div>
        </div>
      </article>
    );
})

const DelayReport=(props)=> {
  const [name, setname] = useState('Printer')
  const componentRef = useRef();
  const printReceipt=()=> {
    window.print();
  }



  const printDocument = () => {
    // in this method report is downloading, but logo is not coming showing err of loading image of s3.     
    html2canvas(document.getElementById("divToPrint"), {
      logging: true,
      letterRendering: 1,
      useCORS: true,
      allowTaint: true
    }).then(function (canvas) {
      // download image      
      const link = document.createElement("a");
      link.download = "html-to-img.png";
      const pdf = new jsPDF();
      const imgProps = pdf.getImageProperties(canvas);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      pdf.addImage(canvas, "PNG", 0, 0, pdfWidth, pdfHeight);
      pdf.save("delayReport.pdf");
    }).catch = (err) => {
      console.log(err)
    };
  }

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={printDocument}>Download</li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => componentRef.current}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint
                ref={componentRef}
                data={props.data}
              />
            </div>
          </div>
        </article>
      </div>
    );
  
}

export default DelayReport;
