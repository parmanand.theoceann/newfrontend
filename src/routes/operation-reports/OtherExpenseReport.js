import React, { useState, useRef, forwardRef } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import html2canvas from "html2canvas";


const ComponentToPrint = forwardRef((props, ref) =>  {

  const [formReportdata, setFormReportdata] = useState(Object.assign({}, props.data || {}))


     
    return (
      <article className="article toolbaruiWrapper" ref={ref}>
        <div className="box box-default" id="divToPrint">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                 <img className='reportlogo' src={formReportdata.logo} alt="No Img"  crossOrigin="anonymous"/>
                  <p className="sub-title m-0">{formReportdata &&formReportdata.full_name?formReportdata.full_name:" "}</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>{formReportdata && formReportdata.address?formReportdata.address:" "}</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>

                <tr>
                  <td className="font-weight-bold">Vessel</td>
                  <td>{formReportdata && formReportdata.data && formReportdata.data[0]&& formReportdata.data[0]['vessel_name']?formReportdata.data[0]['vessel_name']:" "}</td>
                  <td className="font-weight-bold">Voyage No.</td>
                  <td>{formReportdata && formReportdata.voyage_no?formReportdata.voyage_no:" "}</td>
                </tr>

              </tbody>
            </table>

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vendor</th>
                  <th> Trans No</th>   
                  {/* <th>Trans no.</th> */}
                  <th>Rev/Exp</th>
                  <th>Inv No.</th>       
                  {/* <th>Description</th> */}
                  {/* <th>Inv No.</th> */}
                  <th>Inv Date</th>
                  <th>Due Date</th>
                  <th>Service No</th>
                  <th>Service date</th>
                  <th>Refrence</th>
                  <th>P.O number</th>
                  <th>Payment Term</th>
                  <th>Total Amount</th>
                  <th> Invoice type</th>       
                  <th>Status</th>
                  {/* <th>Service no.</th>
                  <th>Refrence</th>
                  <th>P.O. no</th>
                  <th>Payment Term</th>
                  <th>Total amt</th>
                  <th>Invoice type</th>
                  <th>Status</th> */}
                  {/* <th>Amt USD</th>
                  <th>Voyage No</th> */}
                  {/* <th>Invoice Status</th> */}
                </tr>
              </thead>
              <tbody>
                {formReportdata.data && formReportdata.data.length > 0 ? formReportdata.data.map((e, idx) => {
                  return (
                    <>

                      <tr key={idx}>
                       
                        <td>{e.vendor_name?e.vendor_name:" "}</td>
                        <td>{e.provision_trans_no?e.provision_trans_no:" "}</td>
                     
                        {/* <td>{e.provision_trans_no}</td> */}
                       
                        <td>{e.rev_exp_name?e.rev_exp_name:" "}</td>
                        <td>{e.invoice_no?e.invoice_no:" "}</td> 
                        <td>{e.invoice_date?e.invoice_date:" "}</td>
                        <td>{e.due_date?e.due_date:" "}</td>
                        <td>{e.service_no?e.service_no:" "}</td>
                        <td>{e.service_date?e.service_date:" "}</td>
                        <td>{e.referance?e.referance:" "}</td>
                        <td>{e.po_number?e.po_number:" "}</td>
                        {/* <td>{e.payment_terms}</td> */}
                        {/* <td>{e.service_no}</td>
                        <td>{e.referance}</td>
                        <td>{e.po_number}</td> */}
                        <td>{e.payment_terms_name?e.payment_terms_name:" "}</td>
                        <td>{e.total_amount?e.total_amount:" "}</td>
                         <td>{e.invoice_type_name?e.invoice_type_name:" "}</td>
                        {/* <td>{e.invoice_type}</td>  */}
                         {/* <td>{e.invoice_status_name}</td>  */}
                     
                        {/* <td>{e.invoice_type_name}</td> */}
                         {/* <td>{e.voyage_manager_name}</td> */}
                        <td>{e.invoice_status_name?e.invoice_status_name:" "}</td>  
                      </tr>
                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td className="font-weight-bold">Total Revenue USD</td>
                  <td>{formReportdata && formReportdata.total_revenue?formReportdata.total_revenue:" "}</td>
                  <td className="font-weight-bold">Total Expense USD</td>
                  <td>{formReportdata && formReportdata.total_expense?formReportdata.total_expense:" "}</td>
                </tr>
              </tbody>
            </table>

            <h4> TDE Summary</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Vessel</th>
                  <th>Voyage No.</th>
                  <th>Decription</th>
                  <th>Exp/Rev</th>
                  <th>Invoice Date</th>
                  <th>Due Date</th>
                  <th>Inv No.</th>
                  <th>Total Amount</th>
                  <th>INV Status</th>
                  <th>TDE Status</th>
                  
                </tr>
              </thead>
              <tbody>
                {formReportdata.data && formReportdata.data && formReportdata.data.length > 0 ? formReportdata.data.map((e, idx) => {
                  return (
                    <>
                      <tr key={idx}>
                        <td>{e.vessel_name?e.vessel_name:" "}</td>
                        <td>{e.voyage_manager_name?e.voyage_manager_name:" "}</td>
                        <td>{e.invoice_type_name?e.invoice_type_name:" "}</td>
                        <td>{e.rev_exp_name?e.rev_exp_name:" "}</td>
                        <td>{e.invoice_date?e.invoice_date:" "}</td>
                        <td>{e.due_date?e.due_date:" "}</td>
                        <td>{e.invoice_no?e.invoice_no:" "}</td>
                        <td>{e.total_amount?e.total_amount:" "}</td>
                        <td>{e.invoice_status_name?e.invoice_status_name:" "}</td>
                        <td>{e.tde_status?e.tde_status:" "}</td>
                        
                      </tr>
                    </>
                  )
                }) : undefined

                }

              </tbody>
            </table>


          </div>
        </div>
      </article>
    );
  })

const OtherExpenseReport = (props) => {

  const [name, setName] = useState('Printer')
  const componentRef = useRef()

  const printReceipt = () => {
    window.print();
  }
  
  const printDocument=()=>{
    // in this method report is downloading, but logo is not coming showing err of loading image of s3.     
    html2canvas( document.getElementById("divToPrint"), {
       logging: true,
       letterRendering: 1,
       useCORS: true,
       allowTaint: true    }).then(function(canvas) {
       // download image      
       const link = document.createElement("a");
       link.download = "html-to-img.png";
       const pdf = new jsPDF();
       const imgProps = pdf.getImageProperties(canvas);
       const pdfWidth = pdf.internal.pageSize.getWidth();
       const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
       pdf.addImage(canvas, "PNG", 0, 0, pdfWidth, pdfHeight);
       pdf.save("otherexpReport.pdf");
   }).catch=(err)=>{
     console.log(err)
   };
 }

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li onClick={printDocument}>
                        Download
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                                <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => componentRef.current}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={componentRef} data={props.data} />
            </div>
          </div>

        </article>
      </div>
    );
  }

export default OtherExpenseReport;
