import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
                <tbody>
                    <tr>
                        <td className="font-weight-bold">Vesssel</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Person In Charge</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Payment Terms</td>
                        <td>Value</td>
                    </tr>

                    <tr>
                        <td className="font-weight-bold">Voy No.</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Status</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Due Date</td>
                        <td>Value</td>
                    </tr>

                    <tr>
                        <td className="font-weight-bold">Counterparty</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Invoice Date</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Reference No</td>
                        <td>Value</td>
                    </tr>

                    <tr>
                        <td className="font-weight-bold">Fixture No.</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Received Date</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Remittance Bank</td>
                        <td>Value</td>
                    </tr>

                    <tr>
                        <td className="font-weight-bold">CP Date</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Currency</td>
                        <td>Value</td>
                        <td className="font-weight-bold">P.O. Number</td>
                        <td>Value</td>
                    </tr>

                    <tr>
                        <td className="font-weight-bold">Via Company</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Invoice No.</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Invoice Type</td>
                        <td>Value</td>
                    </tr>

                </tbody>
            </table>

            

            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Corgo</th>
                  <th>Corgo ID</th>
                  <th>CP Qty</th>
                  <th>NOM Qty</th>
                  <th>BL Qty</th>
                  <th>Inv Qty</th>
                  <th>F</th>
                  <th>Frt Rate</th>
                  <th>Lumb/Daily</th>
                  <th>Inv %</th>
                  <th>Ttl %</th>
                  <th>Total Amount</th>
                  <th>No Deadfrt</th>
                  <th>Select</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4>Freight Commission</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Broker</th>
                  <th>Commission</th>
                  <th>T</th>
                  <th>F</th>
                  <th>Bill To</th>
                  <th>Total Amount</th>
                  <th>Select</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4>Freight Adjustment</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Corgo ID</th>
                  <th>Commissionable</th>
                  <th>Amount</th>
                  <th>Excl. From P&L</th>
                  <th>Actions</th>
                  <th>Select</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <h4>Freight Calculation</h4>
            <table className="table border-table table-invoice-report-colum">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Value</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Value</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
                <tbody>
                    <tr>
                        <td className="font-weight-bold">VAT/GST %</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Invoice Total</td>
                        <td>Value</td>
                        <td className="font-weight-bold">Total Amount</td>
                        <td>Value</td>
                    </tr>
                </tbody>
            </table>

            
          </div>
        </div>
      </article>
    );
  }
}

class FreightInvoiceReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                  <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default FreightInvoiceReport;
