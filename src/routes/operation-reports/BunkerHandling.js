import React, { Component } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <div className="row p10">
              <div className="col-md-12">
                <h4>Main Information</h4>
                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Vessel :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">IMO :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Call Sign :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Time Zone :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Port :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Port ETD :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Voyage No. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Supplier :</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td className="font-weight-bold">Bunkering Com. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Hose con. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Hose dis. :</td>
                      <td>Value</td>

                      <td className="font-weight-bold">Bunkering Com. :</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>


                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Remark :</td>
                    </tr>
                    <tr>
                      <td>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the
                        1500s, when an unknown printer took a galley of type and scrambled it
                      </td>
                    </tr>
                  </tbody>
                </table>

                <table className="table border-table table-invoice-report-colum">
                  <thead>
                    <tr>
                      <th>Type (MT)</th>
                      <th>Quantity Lifted</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>

                    <tr>
                      <td>Value</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Remark :</td>
                    </tr>
                    <tr>
                      <td>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the
                        1500s, when an unknown printer took a galley of type and scrambled it
                      </td>
                    </tr>
                  </tbody>
                </table>

                <table className="table table-bordered table-invoice-report-colum">
                  <tbody>
                    <tr>
                      <td className="font-weight-bold">Captain :</td>
                      <td>Value</td>
                      <td className="font-weight-bold">C/E :</td>
                      <td>Value</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class NoonReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                            <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default NoonReport