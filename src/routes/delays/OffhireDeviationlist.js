import React, { Component } from "react";
import { Table, Popconfirm, Modal, Col, Row, Select } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";

import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import OffHireDeviation from "./OffHireDeviation";
import { useEffect } from "react";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import LineChart from "../dashboard/charts/LineChart";

import StackGoupColumnChart from "../dashboard/charts/StackNormalizationChart";
import NestedPieChart from "../dashboard/charts/NestedPieChart";

const OffHireDeviationList = (props) => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    reportformDataValue: {},
    typesearch: {},
    voyID: props.voyID || null,
    isGraphModal: false,
  });

  const showGraphs = () => {
    setState((prev) => ({ ...prev, isGraphModal: true }));
  };

  const handleCancel = () => {
    setState((prev) => ({ ...prev, isGraphModal: false }));
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record.id)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["offhire-deviation-list"]
        ? FIELDS["offhire-deviation-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const getTableData = async (searchtype = {}) => {
    const { pageOptions, voyageData, listType, voyID } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let _search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : state.typesearch;
    if (
      _search &&
      _search.hasOwnProperty("searchValue") &&
      _search.hasOwnProperty("searchOptions") &&
      _search["searchOptions"] !== "" &&
      _search["searchValue"] !== ""
    ) {
      let wc = {};
      _search["searchValue"] = _search["searchValue"].trim();

      if (_search["searchOptions"].indexOf(";") > 0) {
        let so = _search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: _search["searchValue"] }));
      } else {
        // wc[_search['searchOptions']] = { l: _search['searchValue'] };

        wc = {
          OR: { [_search["searchOptions"]]: { l: _search["searchValue"] } },
        };
      }

      //  headers['where'] = wc;

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: _search.searchOptions,
        searchValue: _search.searchValue,
      };
    }

    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = {},
      dataArr = [],
      totalRows = 0;

    let _url = `${URL_WITH_VERSION}/delays/deviation-offhire/list?${qParamString}`;
    if (voyID) {
      _url = `${URL_WITH_VERSION}/delays/deviation-offhire/list?e=${voyID}`;
    }
    const response = await getAPICall(_url, headers);
    const data = await response;
    totalRows = data && data.total_rows ? data.total_rows : 0;
    dataArr = data && data.data ? data.data : [];
    let donloadArr = [];
    if (dataArr.length > 0 && totalRows > 0) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      _state["responseData"] = dataArr;
    }

    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const redirectToAdd = async (e, id = null) => {
    let qParams = { e: id };
    let qParamString = objectToQueryStringFunc(qParams);

    if (id) {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/delays/deviation-offhire/edit?${qParamString}`
      );
      const respData = await response["data"];
      setState((prevState) => ({
        ...prevState,
        isAdd: false,
        formDataValues: respData,
        isVisible: true,
      }));
    } else {
      setState((prevState) => ({ ...prevState, isAdd: true, isVisible: true }));
    }
  };

  const onCancel = () => {
    setState({ ...state, isAdd: true, isVisible: false }, () => {
      getTableData();
    });
  };

  const onRowDeletedClick = (id) => {
    let URL = `${URL_WITH_VERSION}/delays/deviation-offhire/delete`;
    apiDeleteCall(URL, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  //resizing function
  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    col,
    loading,
    responseData,
    pageOptions,
    search,
    isAdd,
    isVisible,
    formDataValues,
    reportformDataValue,
    sidebarVisible,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "DELLO SHP",
    "OCEANIC MAJESTY",
    "GOODWYN ISLAND",
    "EMMA MAERSK",
    "OCEANIC MAJESTYs",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amount",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
  ];
  const NestedPieChartData = [
    { value: 70, name: "Vessel 1" },
    { value: 30, name: "Vessel 2" },
  ];

  const totalDashboarddat = [
    {
      title: "Total vessels",
      value: "abcd",
    },
    {
      title: "Total amount",
      value: "abcd",
    },
    {
      title: "Total invoice",
      value: "abcd",
    },
  ];

  const LineCharSeriesData = [200, 400, 1200, 100, 900, 1000, 1200, 1400];
  const LineCharxAxisData = [
    "DEV-INV2024-00110",
    "DEV-INV2024-00109",
    "DEV-INV2024-00108",
    "DEV-INV2024-00107",
    "DEV-INV2024-00106",
  ];

  const StackNormalizationSeriesData = [
    {
      name: "Total Amount",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [32, 33.2, 30.1, 33.4, 39], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Total Expense",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Net Revenue",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
    },
  ];

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Offhire/Deviation Invoice List</span>
                  </h4>
                </div>
                {/* <div className="action-btn">
                  <Button type="primary" onClick={(e) => redirectToAdd(e)}>Add TC In</Button>
                </div> */}
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"offhire-deviation-list-toolbar"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    showGraph={props.voyID || showGraphs}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () =>
                          onActionDonwload("csv", "offhire-deviation"),
                      },
                      {
                        title: "PDF",
                        event: () =>
                          onActionDonwload("pdf", "offhire-deviation"),
                      },
                      {
                        title: "XLSX",
                        event: () =>
                          onActionDonwload("xlsx", "offhire-deviation"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  rowKey={(record) => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  scroll={{ x: "max-content" }}
                  columns={tableColumns}
                  // size="small"
                  components={components}
                  dataSource={responseData}
                  loading={loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {isVisible === true ? (
          <Modal
            title={
              (isAdd === false ? "Create " : "Edit ") +
              "Offhire/Deviation Invoice"
            }
            open={isVisible}
            width="95%"
            onCancel={onCancel}
            style={{ top: "2%" }}
            bodyStyle={{ height: "calc(100% - 5px)", padding: "0.5rem" }}
            footer={null}
          >
            <OffHireDeviation
              formdata={formDataValues}
              modalCloseEvent={() => onCancel()}
            />
          </Modal>
        ) : undefined}
        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
        <Modal
          title="OffHire Deviation Dashboard"
          open={state.isGraphModal}
          //  onOk={handleOk}
          width="90%"
          onCancel={handleCancel}
          footer={null}
        >
          <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Vessels</p>
                <p>300</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Amount</p>
                <p>300 $</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Invoice</p>
                <p>240</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Vessel Name</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Invoice No</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Account Type</p>
              <Select
                placeholder="Payable"
                optionFilterProp="children"
                options={[
                  {
                    value: "Payable",
                    label: "Payable",
                  },
                  {
                    value: "Receivable",
                    label: "Receivable",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Status</p>
              <Select
                placeholder="PREPARED"
                optionFilterProp="children"
                options={[
                  {
                    value: "PREPARED",
                    label: "PREPARED",
                  },
                  {
                    value: "APPROVED",
                    label: "APPROVED",
                  },
                  {
                    value: "POSTED",
                    label: "POSTED",
                  },
                  {
                    value: "VERIFIED",
                    label: "VERIFIED",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Date To</p>
              <Select
                placeholder="2024-02-18"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-26",
                    label: "2024-01-26",
                  },
                  {
                    value: "2023-11-16",
                    label: "2023-11-16",
                  },
                  {
                    value: "2023-11-17",
                    label: "2023-11-17",
                  },
                ]}
              ></Select>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount Per Vessel"}
                ClusterDataxAxis={ClusterDataxAxis}
                ClusterDataSeries={ClusterDataSeries}
                maxValueyAxis={"350"}
              />
            </Col>
            <Col span={12}>
              <LineChart
                LineCharSeriesData={LineCharSeriesData}
                LineCharxAxisData={LineCharxAxisData}
                Heading={"Total Amount Per Invoice No."}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <StackGoupColumnChart
                Heading={"Vessel wise Invoice Type and Total Amount"}
                StackNormalizationSeriesData={StackNormalizationSeriesData}
              />
            </Col>
            <Col span={12}>
              <NestedPieChart
                NestedPieChartData={NestedPieChartData}
                Heading={"Amount As per Voyage no. and Account Type"}
              />
            </Col>
          </Row>
        </Modal>
      </div>
    </>
  );
};

export default OffHireDeviationList;
