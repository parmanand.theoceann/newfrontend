import React, { useEffect, useState, useRef } from "react";
import { Layout, Row, Col, Modal, notification } from "antd";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
  getAPICall,
  apiDeleteCall,
  useStateCallback,
} from "../../shared";
//import Tde from "../tde/Tde";
import Tde from "../tde/";
import CreateInvoice from "../create-invoice/CreateInvoice";
import { objectToQueryStringFunc } from "../../shared";
import moment from "moment";
import InvoicePopup from "../create-invoice/InvoicePopup";
import { DeleteOutlined, SaveOutlined, EditOutlined } from "@ant-design/icons";
import Remarks from "../../shared/components/Remarks";
import Attachment from "../../shared/components/Attachment";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../shared/attachments";
const { Content } = Layout;

const openNotification = (keyName) => {
  let msg = "Please generate the Invoice No. First";
  notification.info({
    message: `Can't Open ${keyName}`,
    description: msg,
    placement: "topRight",
  });
};

const OffHireDeviation = (props) => {
  const [state, setState] = useStateCallback({
    frmName: "deviation_invoice_form",
    isRemarkModel: false,
    formData: {
      vessel: props.formdata && props.formdata.vessel_id,
      voyage: props.formdata && props.formdata.voyage_number,
      vessel_code: props.formdata && props.formdata.vessel_code,
      commence: props.formdata && props.formdata.commence_date,
      complate_date: props.formdata && props.formdata.completing_date,
    },
    frmVisible: true,
    showTDE: false,
    showDownloadInvoice: false,
    tdeData: null,
    showInvoicePopup: false,
    invoiceReport: null,
    isSaved: false,
    popupdata: null,
  });

  const formdataref = useRef();
  useEffect(() => {
    const { formData } = state;
    if (
      props &&
      props.formdata &&
      props.formdata.hasOwnProperty("id") &&
      props.formdata["id"] != undefined &&
      props.formdata.hasOwnProperty("inv_no")
    ) {
      const _formdata = Object.assign({}, props.formdata);
      setState((prevState) => ({ ...prevState, formData: _formdata }));
    }
  }, []);

  const _showTDE = async (key, bolVal) => {
    const { formData } = state;

    let resp = null,
      target = undefined,
      tdeData = {};
    let account_no = null;
    let accountCode = null;

    if (bolVal == true) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      let respData = response["data"];

      if (formData && formData.inv_no && formData.inv_no !== "") {
        const responseData = await getAPICall(
          `${URL_WITH_VERSION}/address/edit?ae=${formData["counter_party"]}`
        );
        const responseAddressData = responseData["data"];
        account_no =
          responseAddressData &&
          responseAddressData["bank&accountdetails"] &&
          responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
              responseAddressData["bank&accountdetails"][0]["account_no"]
            : "";

        accountCode =
          responseAddressData &&
          responseAddressData["bank&accountdetails"] &&
          responseAddressData["bank&accountdetails"].length > 0
            ? responseAddressData["bank&accountdetails"][0] &&
              responseAddressData["bank&accountdetails"][0]["swift_code"]
            : "";

        let voyageData = null;
        let voyage_manager_id = formData.voyage;
        if (voyage_manager_id) {
          const request = await getAPICall(
            `${URL_WITH_VERSION}/voyage-manager/edit?ae=${voyage_manager_id}`
          );
          voyageData = await request["data"];
        }

        if (respData && respData.length > 0) {
          target = respData.find((item) => item.invoice_no === formData.inv_no);
          if (target && target.hasOwnProperty("id") && target["id"] > 0) {
            resp = await getAPICall(
              `${URL_WITH_VERSION}/tde/edit?e=${target["id"]}`
            );
          }
        }

        let accounting = [];
        if (
          target &&
          resp &&
          resp["data"] &&
          resp["data"].hasOwnProperty("id")
        ) {
          tdeData = resp["data"];

          formData &&
            formData["description"] &&
            formData["description"].length > 0 &&
            formData["description"].map((val, ind) => {
              accounting.push({
                company: target["bill_via"],
                amount: val.final_amt,

                lob: voyageData["company_lob"],
                vessel_code: formData["vessel_code"],
                description: val.activity_name,

                editable: true,
                vessel_name: formData["vessel"],
                account: accountCode,
                ap_ar_acct: account_no,
                key: "table_row_" + ind,
                voyage: formData["voyage"],
                id: -9e6 + ind,
                // port: "",
                // ic: "",
              });
              return true;
            });
          //  tdeData['vessel']=
          tdeData["accounting"] = accounting;
          tdeData["--"] = { total: target["invoice_amount"] };
          //  tdeData['----'] = { 'total_due': target['invoice_amount'], 'total': target['invoice_amount'],"remittance_bank":resp['data']['----']["remittance_bank"]}
          let rem_data = resp["data"]["----"][0];
          tdeData["----"] = {
            total_due: rem_data["total_due"],
            total: rem_data["total"],
            remittance_bank: rem_data["remittance_bank"],
          };
        } else {
          formData &&
            formData["description"] &&
            formData["description"].length > 0 &&
            formData["description"].map((val, ind) => {
              accounting.push({
                company: voyageData["my_company_lob"],
                account: accountCode,
                lob: voyageData["company_lob"],
                vessel_code: formData["vessel_code"],
                description: val.activity_name,

                vessel_name: formData["vessel"],
                amount: val.final_amt,
                ap_ar_acct: account_no,
                voyage: formData["voyage"],
                id: -9e6 + ind,
                // port: "",
                // ic: "",
              });
              return true;
            });

          tdeData = {
            invoice: formData["account_type"],
            po_number: formData["po_number"],
            invoice_date: formData["due_date"],
            received_date: formData["received_date"],
            invoice_no: formData.inv_no,
            ar_pr_account_no: account_no,
            inv_status: formData.inv_status,
            invoice_type: formData.inv_type,
            voyage_manager_id: voyageData.id,
            vendor: "", // no vendor exist in offhire deviation
            vessel: formData["vessel"],
            invoice_amount: formData.total,
            bill_via: formData.my_company,
            account_base: formData.total,
            payment_term: formData.terms,
            accounting: accounting,
          };
        }
        setState((prevState) => ({
          ...prevState,
          showTDE: bolVal,
          tdeData: tdeData,
        }));
      } else {
        openNotificationWithIcon(
          "info",
          "Please generate the Invoice No. First"
        );
      }
    } else {
      setState((prevState) => ({ ...prevState, showTDE: bolVal }));
    }
  };

  const onClickExtraIcon = async (action, data) => {
    let delete_id = data && data.id;
    let groupKey = action["gKey"];
    let frm_code = "";
    if (groupKey == "Description") {
      groupKey = "description";
      frm_code = "deviation_invoice_form";
    }
    if (groupKey && delete_id && Math.sign(delete_id) > 0 && frm_code) {
      let data1 = {
        id: delete_id,
        frm_code: frm_code,
        group_key: groupKey,
        key: data.key,
      };
      postAPICall(
        `${URL_WITH_VERSION}/tr-delete`,
        data1,
        "delete",
        (response) => {
          if (response && response.data) {
            openNotificationWithIcon("success", response.message);
          } else {
            openNotificationWithIcon("error", response.message);
          }
        }
      );
    }
  };

  const onEditClick = async () => {
    const { formData } = state;

    if (
      formData &&
      formData.hasOwnProperty("id") &&
      formData["id"] != undefined
    ) {
      let qParams = { e: formData["id"] };
      let qParamString = objectToQueryStringFunc(qParams);

      const response = await getAPICall(
        `${URL_WITH_VERSION}/delays/deviation-offhire/edit?${qParamString}`
      );
      const respData = await response["data"];
      setState((prevState) => ({
        ...prevState,
        formData: respData,
        frmVisible: true,
      }));
    } else {
      openNotificationWithIcon("error", "Something went wrong", 4);
      setState((prevState) => ({ ...prevState, frmVisible: true }));
    }
  };

  const saveFormData = (vData) => {
    let type = "save";
    let suMethod = "POST";
    if (vData.hasOwnProperty("id")) {
      type = "update";
      suMethod = "PUT";
    }

    const { frmName } = state;
    formdataref.current = vData;
    setState((prevState) => ({ ...prevState, frmVisible: false }));
    let suURL = `${URL_WITH_VERSION}/delays/deviation-offhire/${type}?frm=${frmName}`;

    vData &&
      vData["description"].length > 0 &&
      vData["description"].map((el, index) => {
        delete el["activity_name"];
        el["frm_date_time"] = moment(el.frm_date_time);
        el["to_date_time"] = moment(el.to_date_time);
      });

    postAPICall(suURL, vData, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        if (type === "save") {
          window.emitNotification({
            n_type: "Off hire Added",
            msg: window.notificationMessageCorrector(`Off Hire is added, for Voyage(${vData.voyage}), vessel(${vData.vessel_code}), by ${window.userName}`),
          });
        } else {
          window.emitNotification({
            n_type: "Off hire Updated",
            msg: window.notificationMessageCorrector(`Off Hire is updated, for Voyage(${vData.voyage}), vessel(${vData.vessel_code}), by ${window.userName}`),
          });
        }
        //setState({...state,formData: {...formdataref,id: data.data.id,inv_no: data.data.inv_no,},});
        setState((prevState) => ({
          ...prevState,
          formData: {
            ...formdataref.current,
            id: data.data.id,
            inv_no: data.data.inv_no,
          },
        }));

        onEditClick();
      } else {
        openNotificationWithIcon("error", data.message);
        //setState({...state,frmVisible: true,formData: { ...formdataref },});
        setState((prevState) => ({
          ...prevState,
          frmVisible: true,
          formData: { ...formdataref.current },
        }));
      }
    });
  };

  const _onDeleteFormData = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => DeleteFormData(postData),
    });
  };

  const DeleteFormData = (data) => {
    const { frmName } = state;
    let URL = `${URL_WITH_VERSION}/delays/deviation-offhire/delete`;

    if (data && data["id"]) {
      apiDeleteCall(URL, { id: data["id"] }, (resp) => {
        if (resp && resp.data) {
          openNotificationWithIcon("success", resp.message);
          window.emitNotification({
            n_type: "Off hire Deleted",
            msg: window.notificationMessageCorrector(`Off Hire is deleted, for Voyage(${data.voyage}), vessel(${data.vessel_code}), by ${window.userName}`),
          });
          if (props.hasOwnProperty("modalCloseEvent")) {
            props.modalCloseEvent();
          }
        } else {
          openNotificationWithIcon("error", resp.message);
        }
      });
    } else {
      openNotificationWithIcon("info", "Please fill the form first.", 3);
    }
  };

  const invoiceModal = async (data = {}) => {
    let { formData, invoiceReport } = state;
    try {
      if (Object.keys(data).length == 0) {
        let qParams = { e: formData["id"] };
        let qParamString = objectToQueryStringFunc(qParams);
        const response = await getAPICall(
          `${URL_WITH_VERSION}/delays/deviation-offhire/invoice?${qParamString}`
        );
        const respdata = await response["data"];
        if (respdata) {
          setState((prevState) => ({
            ...prevState,
            invoiceReport: respdata,
            popupdata: respdata,
            showInvoicePopup: true,
          }));

          //showHideModal(true, "InvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry, Unable to Show Invoice", 3);
        }
      } else {
        setState((prevState) => ({
          ...prevState,
          invoiceReport: { ...invoiceReport, ...data },
        }));
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something Went Wrong", 3);
    }
  };

  const handleok = () => {
    const { invoiceReport } = state;
    if (invoiceReport["isSaved"]) {
      // showHideModal(false, "InvoicePopup");

      setState((prevState) => ({ ...prevState, showInvoicePopup: false }));
      setTimeout(() => {
        //showHideModal(true, "InvoiceModal");
        setState((prevState) => ({ ...prevState, showDownloadInvoice: true }));
      }, 2000);
      setState((prevState) => ({ ...prevState, invoiceReport: invoiceReport }));
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  const {
    frmName,
    formData,
    frmVisible,
    showTDE,
    showDownloadInvoice,
    tdeData,
    invoiceReport,
    popupdata,
    showInvoicePopup,
    isRemarkModel,
  } = state;

  const ShowAttachment = async (isShowAttachment) => {
    let loadComponent = undefined;
    const { id } = state.formData;
    if (id && isShowAttachment) {
      const attachments = await getAttachments(id, "EST");
      const callback = (fileArr) =>
        uploadAttachment(fileArr, id, "EST", "port-expense");
      loadComponent = (
        <Attachment
          uploadType="Estimates"
          attachments={attachments}
          onCloseUploadFileArray={callback}
          deleteAttachment={(file) =>
            deleteAttachment(file.url, file.name, "EST", "port-expense")
          }
          tableId={0}
        />
      );
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: loadComponent,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
        isShowAttachment: isShowAttachment,
        loadComponent: undefined,
      }));
    }
  };

  const handleRemark = () => {
    setState((prevState) => ({
      ...prevState,
      isRemarkModel: true,
    }));
  };

  return (
    <div className="body-wrapper">
      <Layout className="layout-wrapper">
        <Layout>
          <Content className="content-wrapper">
            <Row gutter={16} style={{ marginRight: 0 }}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article">
                    <div className=" box-default">
                      <div className="box-body">
                        {frmVisible ? (
                          <NormalFormIndex
                            key={"key_" + frmName + "_0"}
                            formClass="label-min-height"
                            formData={formData}
                            showForm={true}
                            frmCode={frmName}
                            addForm={true}
                            inlineLayout={true}
                            showToolbar={[
                              {
                                leftWidth: 8,
                                rightWidth: 16,
                                isLeftBtn: [
                                  {
                                    key: "s1",
                                    isSets: [
                                      {
                                        id: "1",
                                        key: "save",
                                        type: <SaveOutlined />,
                                        withText: "Save",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          saveFormData(data),
                                      },
                                      {
                                        id: "2",
                                        key: "delete",
                                        type: <DeleteOutlined />,
                                        withText: "Delete",
                                        showToolTip: true,
                                        event: (key, data) =>
                                          _onDeleteFormData(data),
                                      },

                                      {
                                        id: "3",
                                        key: "edit",
                                        type: <EditOutlined />,
                                        withText: "Remark",
                                        showToolTip: true,
                                        event: (key, data) =>{
                
                                          if (data?.row?.id || data?.id > 0 ) {
                                            handleRemark(data);
                                          } else {
                                            openNotificationWithIcon(
                                             "info",
                                              "Please save Invoice first",
                                              2
                                            );
                                          }
                                        },
                                      },
                                    ],
                                  },
                                ],
                                isRightBtn: [
                                  {
                                    key: "s2",
                                    isSets: [
                                      {
                                        key: "download_invoice",
                                        isDropdown: 0,
                                        withText: "Create Invoice",
                                        type: "",
                                        menus: null,
                                        event: (key) => invoiceModal(),
                                      },
                                      {
                                        key: "tde",
                                        isDropdown: 0,
                                        withText: "TDE",
                                        type: "",
                                        menus: null,
                                        event: (key) => {
                                          //_showTDE(key, true);
                                          if (formData?.inv_no) {
                                            setState((prevState) => ({
                                              ...prevState,
                                              showTDE: true,
                                            }));
                                          } else {
                                            openNotification("tde");
                                          }
                                        },
                                      },
                                      {
                                        key: "attachment",
                                        isDropdown: 0,
                                        withText: "Attachment",
                                        type: "",
                                        menus: null,
                                        event: (key, data) => {
                                          data &&
                                          data.hasOwnProperty("id") &&
                                          data["id"] > 0
                                            ? ShowAttachment(true)
                                            : openNotificationWithIcon(
                                                "info",
                                                "Please save Invoice First.",
                                                3
                                              );
                                        },
                                      },
                                    ],
                                  },
                                ],
                                isResetOption: false,
                              },
                            ]}
                            tableRowDeleteAction={(action, data) =>
                              onClickExtraIcon(action, data)
                            }
                            isShowFixedColumn={["Description"]}
                          />
                        ) : undefined}
                      </div>
                    </div>
                  </article>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>

      {showTDE ? (
        <Modal
          title="TDE"
          open={showTDE}
          width="90%"
          //onCancel={() => _showTDE(undefined, false)}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, showTDE: false }))
          }
          style={{ top: "10px" }}
          bodyStyle={{ maxHeight: 790, overflowY: "auto", padding: "0.5rem" }}
          footer={null}
        >
          <Tde
            invoiceType="DeviationInvoice"
            formData={formData}
            invoiceNo={formData.inv_no}
            //isEdit={ tdeData != null && tdeData.id && tdeData.id > 0 ? true : false}

            //deleteTde={() => setState(prevState => ({...prevState, showTDE: false}))}
            //modalCloseEvent={() => setState(prevState => ({...prevState, showTDE: false}))}

            //saveUpdateClose={() => setState(prevState => ({ ...prevState, isVisible: false }))}
          />
        </Modal>
      ) : undefined}
      {state.isShowAttachment ? (
        <Modal
          style={{ top: "2%" }}
          title="Upload Attachment"
          open={state.isShowAttachment}
          onCancel={() => ShowAttachment(false)}
          width="50%"
          footer={null}
        >
          {state.loadComponent}
        </Modal>
      ) : undefined}

      {showDownloadInvoice && (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Create Invoice"
          open={showDownloadInvoice}
          onCancel={() =>
            setState((prevState) => ({
              ...prevState,
              showDownloadInvoice: false,
            }))
          }
          width="95%"
          footer={null}
        >
          <CreateInvoice
            type="DeviationInvoice"
            DeviationInvoice={invoiceReport}
          />
        </Modal>
      )}

      {showInvoicePopup ? (
        <Modal
          style={{ top: "2%" }}
          title="Invoice"
          open={showInvoicePopup}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, showInvoicePopup: false }))
          }
          width="95%"
          okText="Create Pdf"
          onOk={handleok}
        >
          <InvoicePopup
            data={popupdata}
            updatepopup={(data) => invoiceModal(data)}
          />
        </Modal>
      ) : undefined}

      {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            setState({ isRemarkModel: true });
          }}
          onCancel={() =>
            setState((prevState) => ({ ...prevState, isRemarkModel: false }))
          }
          footer={false}
        >
          <Remarks
            remarksID={formData.inv_no}
            remarkType="delays/deviation-offhire"
            // idType="Bunker_no"
          />
        </Modal>
      )}
    </div>
  );
};

export default OffHireDeviation;
