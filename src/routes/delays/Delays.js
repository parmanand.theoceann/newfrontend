import React, {  useEffect } from 'react';
import { Row, Col, Layout, Form, Input, Modal } from 'antd';
import DelayReport from '../operation-reports/DelayReport'
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon, postAPICall, apiDeleteCall, objectToQueryStringFunc, useStateCallback } from '../../shared';
import { SaveOutlined } from '@ant-design/icons';

const { Content } = Layout;

const Delays = (props) => {
  const [state, setState] = useStateCallback({
    isShowDelayReport: false,
    voyID: props.voyID,
    frmName: "delay_form",
    modals: {
      DelayReport: false,
    },
    formData: Object.assign({ "id": -1 }, (props.formData || {})),
    frmVisible: false,
    reportFormData: {
      "portitinerary": [
        { "port_id": "4444", "port": "2944", "funct": 3, "s_type": 1, "wf_per": 10, "miles": "0.00", "speed": "0.00", "eff_speed": "0.00", "gsd": "0.00", "tsd": "0.00", "xsd": "0.00", "l_d_qty": "0.00", "l_d_rate": "0.00", "l_d_rate1": "0.00", "turn_time": "0.00", "days": "0.00", "xpd": "0.00", "p_exp": "0.00", "t_port_days": "0.00", "l_d_term": 1, editable: true, index: 0, ID: -9e6 },
        { "port_id": "6297", "port": "5530", "s_type": 1, "wf_per": 10, "miles": "0.00", "speed": "0.00", "eff_speed": "0.00", "gsd": "0.00", "tsd": "0.00", "xsd": "0.00", "l_d_qty": "0.00", "l_d_rate": "0.00", "l_d_rate1": "0.00", "turn_time": "0.00", "days": "0.00", "xpd": "0.00", "p_exp": "0.00", "t_port_days": "0.00", "l_d_term": 1, editable: true, index: 1, ID: -9e6 + 1 }
      ]
    },
  })


  useEffect(() => {
    getFormData()
  },[])

  const getFormData = async () =>{
    const { match, estimateID } = props;
    const { showLeftBtn, formData } = state;
    let response = null, respDataC = {}, respData = {}, fixData = null, fieldOptions = undefined, _showLeftBtn = Object.assign([], showLeftBtn);

    if (formData && formData.hasOwnProperty("voyage_manager_id") && formData["voyage_manager_id"])
      _onLeftSideListClick(formData["voyage_manager_id"])


  }

  const _onLeftSideListClick = async delayId => {
    const { formData } = state;
    setState(prevState => ({...prevState, frmVisible: false}));
    const response = await getAPICall(`${URL_WITH_VERSION}/delays/edit?e=${delayId}`);
    const data = await response['data'];
    let setData = Object.assign({}, formData);
    if (data) {
      setData = Object.assign({}, formData, data || {});
    }

    setState(prevState => ({...prevState, formData: setData, frmVisible: true}));
  }

  const saveFormData = (postData, innerCB) => {
    const { frmName } = state;
    let _url = 'save';
    let _method = 'post';
    if (postData.hasOwnProperty('id') && postData['id'] > 0) {
      _url = 'update';
      _method = 'put';
    }
    setState(prevState => ({...prevState, frmVisible: false}));

    Object.keys(postData).forEach(key => postData[key] === null && delete postData[key]);

    postAPICall(`${URL_WITH_VERSION}/delays/${_url}?frm=${frmName}`, postData, _method, data => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        let setData = Object.assign(
          {
            id: -1,
          },
          props.formData || {}
        )
      
        setState(prevState => ({...prevState, formData: setData, showSideListBar: true, frmVisible: true}));

        if (props.modalCloseEvent && typeof props.modalCloseEvent === 'function') {
          props.modalCloseEvent();
        } else if (innerCB && typeof innerCB === 'function') {
          innerCB();
        }

        if (setData && setData.hasOwnProperty("voyage_manager_id") && setData["voyage_manager_id"])
          _onLeftSideListClick(setData["voyage_manager_id"])
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
      }
    });
  }
  const openDelayReport = async (showDelayReport) => {

    if (showDelayReport) {
      try {
        const responseReport = await getAPICall(`${URL_WITH_VERSION}/delays/report?e=${state.formData["voyage_manager_id"]}`);
        const respDataReport = await responseReport['data'];

        if (respDataReport) {
          setState(prevState => ({...prevState, "reportFormData": respDataReport, isShowDelayReport: showDelayReport}));
        } else {
          openNotificationWithIcon('error', "Unable to show report", 5);
        }
      } catch (err) {
        openNotificationWithIcon('error', "Something went wrong.", 5);
      }

    } else {
      setState(prevState => ({...prevState, isShowDelayReport: showDelayReport}));
    }
  }

  // DelayReport = showDelayReport => setState({ ...state, isShowDelayReport: showDelayReport });

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {}
    _modal[modal] = visible;
    //setState({...state, modals: Object.assign(modals, _modal)});
    setState(prevState => ({...prevState, modals: Object.assign(modals, _modal)}));
  };

  const _onDeleteClick = (action) => {
    Modal.confirm({
      title: 'Confirm',
      content: 'Are you sure, you want to delete it?',
      onOk: () => _onDelete(action),
    });
  }

  const _onDelete = action => {
    let _url = `${URL_WITH_VERSION}/delays/delete/item`;
    apiDeleteCall(_url, { id: action.ID }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        let setData = Object.assign(
          {
            id: -1,
          },
          props.formData || {}
        )
        setState(prevState => ({...prevState, formData: setData, showSideListBar: true, frmVisible: true}));
        if (setData && setData.hasOwnProperty("voyage_manager_id") && setData["voyage_manager_id"])
          _onLeftSideListClick(setData["voyage_manager_id"])
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

    const { frmName, formData, frmVisible, reportFormData, isShowDelayReport } = state;
    let isSetsLeftBtnArr = [{ id: '3', key: 'save', type: <SaveOutlined />, withText: 'Save', event: (key, data) => saveFormData(data) }];

    return (
      <div className="tcov-wrapper full-wraps voyage-fix-form-wrap cargo">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <Row gutter={16} style={{ marginRight: 0 }}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="body-wrapper">
                  <article className="article">
                    <div className=" box-default">
                      <div className="box-body">
                        {
                          frmVisible ?
                            <NormalFormIndex
                              key={'key_' + frmName + '_0'}
                              formClass="label-min-height"
                              formData={formData}
                              showForm={true}
                              frmCode={frmName}
                              addForm={true}
                              inlineLayout={true}
                              showToolbar={[
                                {
                                  leftWidth: 8,
                                  rightWidth: 16,
                                  isLeftBtn: [{ isSets: isSetsLeftBtnArr }],
                                  isRightBtn: [
                                    {
                                      isSets: [
                                        {
                                          key: 'report',
                                          isDropdown: 0,
                                          withText: 'Report',
                                          type: '',
                                          menus: null,
                                          event: key => openDelayReport(true),
                                        },
                                      ],
                                    },
                                  ],
                                  isResetOption: false,
                                },
                              ]}
                              extraTableButton={{
                                ".": (formData && formData.hasOwnProperty(".") && formData["."].length > 0 ? [{ "icon": "delete", "onClickAction": (action, data) => _onDeleteClick(action) }] : []),
                              }}
                              isShowFixedColumn={[
                                '.',
                              ]}
                            /> : undefined}
                      </div>
                    </div>
                  </article>
                </div>
                </Col>
              </Row>
            </Content>
          </Layout>
        </Layout>
        {isShowDelayReport ? (
          <Modal
            style={{ top: '2%' }}
            title="Reports"
            open={isShowDelayReport}
            // onOk={handleOk}
            onCancel={() => openDelayReport(false)}
            width="95%"
            footer={null}
          >
            <DelayReport data={reportFormData} />
          </Modal>
        ) : (
          undefined
        )}

      </div>
    );
  }
export default Delays;
