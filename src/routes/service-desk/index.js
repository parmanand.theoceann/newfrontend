import React, { Component } from 'react';
import { UserOutlined, UserAddOutlined, WarningOutlined, BankOutlined  } from '@ant-design/icons';
import { IMAGE_PATH } from '../../shared';

// // const { Panel } = Collapse;
// //const { Option } = Select;

// function callback(key) {
//   console.log(key);
// }

// const genExtra = () => (
//   <UserOutlined  onClick={event => {
//     // If you don't want click extra trigger collapse, you can prevent this:
//     event.stopPropagation();
//   }}/>
// );
// const genExtra2 = () => (
//   <UserAddOutlined  onClick={event => {
//     // If you don't want click extra trigger collapse, you can prevent this:
//     event.stopPropagation();
//   }}
// />
 
// );
// const genExtra5 = () => (
//   <WarningOutlined onClick={event => {
//     // If you don't want click extra trigger collapse, you can prevent this:
//     event.stopPropagation();
//   }}/>
// );
// const genExtra6 = () => (
// < WarningOutlined onClick={event => {
//   // If you don't want click extra trigger collapse, you can prevent this:
//   event.stopPropagation();
// }}/>
// );
// const genExtra8 = () => (
//   <UserOutlined  onClick={event => {
//     // If you don't want click extra trigger collapse, you can prevent this:
//     event.stopPropagation();
//   }}/>
// );
// const genExtra11 = () => (
//   <BankOutlined   onClick={event => {
//     // If you don't want click extra trigger collapse, you can prevent this:
//     event.stopPropagation();
//   }}/>

// );
// const genExtra1 = () => (
//   <h3>
//     User <br />
//     <span className="span-text">Create, Update, Delete User</span>
//   </h3>
// );
// const genExtra3 = () => (
//   <h3>
//     Group <br />
//     <span className="span-text"> Create User’s Group</span>
//   </h3>
// );
// const genExtra4 = () => (
//   <h3>
//     Subscription <br />
//     <span className="span-text">Maintain & purchase License</span>
//   </h3>
// );
// const genExtra7 = () => (
//   <h3>
//     Access Right <br />
//     <span className="span-text">Assign access rights to user</span>
//   </h3>
// );
// const genExtra9 = () => (
//   <h3>
//     Admin Role <br />
//     <span className="span-text">Admin Details</span>
//   </h3>
// );
// const genExtra10 = () => (
//   <h3>
//     Main Organization <br />
//     <span className="span-text"> Register Own Company</span>
//   </h3>
// );

class UserDashboard extends Component {
   constructor(props){
     super(props)
     this.state = {
      expandIconPosition: 'right',
      menuArray: [
        {
          key: 'Chartering',
          name: 'Chartering',
          icon: IMAGE_PATH+"menu/chartering.png",
          hrf: "#/chartering-dashboard"
        },
        {
          key: 'Operation',
          name: 'Operation',
          icon: IMAGE_PATH+"menu/operation.png",
          hrf: "#/chartering-dashboard"
        }, {
          key: 'Finance',
          name: 'Finance',
          icon: IMAGE_PATH+"menu/finance.png",
          hrf: "#/finance-dashboard"
        }, {
          key: 'Analytical',
          name: 'Analytical',
          icon: IMAGE_PATH+"menu/analytics.png",
          hrf: "#"
        }, {
          key: 'Profile Setup',
          name: 'Profile Setup',
          icon: IMAGE_PATH+"menu/profiel-setup.png",
          hrf: "#/user-dashboard"
        }, {
          key: 'Setting Term',
          name: 'Setting Term',
          icon: IMAGE_PATH+"menu/master-setting.png",
          hrf: "#/vessel-form"
        }, {
          key: 'Vessel Register',
          name: 'Vessel Register',
          icon: IMAGE_PATH+"menu/voyage-data-search.png",
          hrf: "#/vessel-form"
        }, {
          key: 'Port To Port Distance',
          name: 'Port To Port Distance',
          icon: IMAGE_PATH+"menu/port-to-port-disatnce.png",
          hrf: "#/map-intelligence"
        }, {
          key: 'Vessel Speed Dynamics',
          name: 'Vessel Speed Dynamics',
          icon: IMAGE_PATH+"menu/vsd.png",
          hrf: "#/dynamic-vspm"
        }, {
          key: 'Track My Feet',
          name: 'Track My Feet',
          icon: IMAGE_PATH+"menu/track-my-fleet.png",
          hrf: "#/track-my-fleet"
        }, {
          key: 'Track All Feet',
          name: 'Track All Feet',
          icon: IMAGE_PATH+"menu/track-all-fleet.png",
          hrf: "#/live-vessel"
        }, {
          key: 'Bunker Planning',
          name: 'Bunker Planning',
          icon: IMAGE_PATH+"menu/bunker-planning.png",
          hrf: "#/voyage-bunker-plan"
        }, {
          key: 'Doc Share',
          name: 'Doc Share',
          icon: IMAGE_PATH+"menu/document.png",
          hrf: "#"
        }, {
          key: 'Whatsapp',
          name: 'Whatsapp',
          icon: IMAGE_PATH+"menu/whatsapp.png",
          hrf: "#"
        }, {
          key: 'Port Data Search',
          name: 'Port Data Search',
          icon: IMAGE_PATH+"menu/port-data-search.png",
          hrf: "#/port-data"
        }, {
          key: 'Ship Data Search',
          name: 'Ship Data Search',
          icon: IMAGE_PATH+"menu/voyage-data-search.png",
          hrf: "#/vessel-data"
        }, {
          key: 'Portcall',
          name: 'Portcall',
          icon: IMAGE_PATH+"menu/portcall-list.png",
          hrf: "#/port-cost"
        }, {
          key: 'Port Cost Analysis',
          name: 'Port Cost Analysis',
          icon: IMAGE_PATH+"menu/portcall-analytics.png",
          hrf: "#/port-cost"
        }, {
          key: 'Cll Dashboard',
          name: 'Cll Dashboard',
          icon: IMAGE_PATH+"menu/cii.png",
          hrf: "#"
        }, {
          key: 'Voyage Performance Analysis',
          name: 'Voyage Performance Analysis',
          icon: IMAGE_PATH+"menu/voyage-p-a.png",
          hrf: "#/overall-performance-analysis "
        }, {
          key: 'Voyage Historical Analysis',
          name: 'Voyage Historical Analysis',
          icon: IMAGE_PATH+"menu/voyage-historical-analytics.png",
          hrf: "#/voyage-history-list"
        }, {
          key: 'Time Charter',
          name: 'Time Charter',
          icon: IMAGE_PATH+"menu/time-charter.png",
          hrf: "#/tc-in-list"
        }, {
          key: 'Voyage Calculator',
          name: 'Voyage Calculator',
          icon: IMAGE_PATH+"menu/voyage-calculator.png",
          hrf: ""
        }, {
          key: 'Voyage Optimization',
          name: 'Voyage Optimization',
          icon: IMAGE_PATH+"menu/operation.png",
          hrf: "#/quick-estimate"
        }, {
          key: 'Cargo Vessel Schedule',
          name: 'Cargo Vessel Schedule',
          icon: IMAGE_PATH+"menu/cargo-vessel-schedule.png",
          hrf: "#/live-vessel"
  
        }, {
          key: 'Vetting & Q88',
          name: 'Vetting & Q88',
          icon: IMAGE_PATH+"menu/vetting-and-q88.png",
          hrf: "#"
        }, {
          key: 'IT Support',
          name: 'IT Support',
          icon: IMAGE_PATH+"menu/it-support.png",
          hrf: "#"
        }, {
          key: 'Manual',
          name: 'Manual',
          icon: IMAGE_PATH+"menu/manual.png",
          hrf: "#"
        }, {
          key: 'Buy Insurance',
          name: 'Buy Insurance',
          icon: IMAGE_PATH+"menu/buy-insurance.png",
          hrf: "#"
        }, {
          key: 'Terminal Operation System',
          name: 'Terminal Operation System',
          icon: IMAGE_PATH+"menu/tos.png",
          hrf: "#"
        }, {
          key: 'Vessel Traffic System',
          name: 'Vessel Traffic System',
          icon: IMAGE_PATH+"menu/vts.png",
          hrf: "#"
        }, {
          key: 'Freight Predict Algo',
          name: 'Freight Predict Algo',
          icon: IMAGE_PATH+"menu/frieght-predict-algo.png",
          hrf: "#"
        }, {
          key: 'Carbon Emission Trading(ETS)',
          name: 'Carbon Emission Trading(ETS)',
          icon: IMAGE_PATH+"menu/carbon-emission-trading-ets.png",
          hrf: "#"
        }, {
          key: 'Freight Container Booking',
          name: 'Freight Container Booking',
          icon: IMAGE_PATH+"menu/container-freight-booking.png",
          hrf: "#"
        }
      ]
    };
  
   }

  onPositionChange = expandIconPosition => {
    this.setState({ expandIconPosition });
  };

  render() {
    const { expandIconPosition } = this.state;
    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div class="desk-dashborad-text">Dashboard</div>
              <div className="box-body">
                
                <section className="main-user-section">
                  <div className="container-fluid">
                    <div className="row">
                      {this.state.menuArray.length > 0 && this.state.menuArray.map((data, i) => {
                        return (
                          <div className="col-4">
                            <div className="user-block">
                              <a href={data.hrf} target="_blank"  >
                                <div className="row">
                                  <div className="col-md-2 icon_col">
                                    <div className="colorsBackground">
                                      <span><img class="imagesize" src={data.icon} /></span>
                                    </div>
                                  </div>
                                  <div className="col-md-10 name_col">
                                    <div className="name_col_inner">
                                      <div className="nameBlock">{data.name}</div>

                                    </div>
                                  </div>
                                </div>
                                {/* </div> */}
                              </a>
                            </div>
                          </div>
                        )
                      })}



                    </div>
                  </div>
                </section>
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }
}
export default UserDashboard;

