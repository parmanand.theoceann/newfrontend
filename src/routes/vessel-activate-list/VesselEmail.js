import React, { Component } from 'react';
import { Form, Input, Button, Select, Upload, Icon, message } from 'antd';
import URL_WITH_VERSION, {
  postAPICall,
  openNotificationWithIcon,
} from '../../shared';
import { UploadOutlined } from '@ant-design/icons';
const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class VesselEmail extends Component {
  constructor(props) {
    super(props);
    console.log(">>>>>>", this.props.data);
    this.state ={
      vessel_name:this.props && this.props.data && this.props.data.vessel_name?this.props.data.vessel_name:'',
      email:this.props && this.props.data && this.props.data.email?this.props.data.email:'',
      from_email:'noreply@theoceann.com',
      subject:'VSPM Link activated for position report/Vessel name/Company name',
      msg:'Dear Captain User has activated the VSPM link service for daily position report. You are requested for send daily position report as per attached Zip file reporting format for each kind of report.  You are requested to send the report on below email id \n Support@vspmlink.com \nThanks and regards\nVSPM LInk',
      vspm_status:'Active'
    }
  }

handleSubmit=async()=>{
  const{vessel_name,email,from_email,subject,msg}=this.state
  if (vessel_name == '' || email == '' || from_email == '' || subject == '' || msg == '') {
    openNotificationWithIcon("error", "Please enter all mandetory fields !");
    return
  }
    let _method = "post";
    let postData={
      "email": [email],
      "report":"Position Report",
      "vessel":vessel_name,
      "company_name": "Pallav industries",
      "subject": subject,
      "msg": msg,

    }
    postAPICall(`${URL_WITH_VERSION}/noon-verification/send_email`, postData, _method, (data) => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
      } else {
        openNotificationWithIcon('error',data.message)
      }
    });

}

  render() {
  const{vessel_name,email,from_email,subject,msg,vspm_status,date}=this.state
  let status="active"
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
           
            <div className="box-body">
            <Form>

              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="Vessel Name">
                    <Input size="default" onChange={(e)=>this.setState({vessel_name:e.target.value})} disabled={true} value={vessel_name} placeholder='Vessel Name' />
                  </FormItem>
                </div>
              </div>
              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="To">
                  <Input size="default" onChange={(e)=>this.setState({email:e.target.value})}  value={email} placeholder='to' />
                  </FormItem>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="From">
                  <Input
                    disabled={true}
                    size="default"
                    value={from_email}
                    placeholder='from'
                    />
                  </FormItem>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="Subject">
                  <Input onChange={(e)=>this.setState({subject:e.target.value})} size="default" value={subject} placeholder='subject' />
                  </FormItem>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="Message">
                    <TextArea rows="5"
                    onChange={(e)=>this.setState({msg:e.target.value})}
                    value={msg} placeholder='Message'
                      />
                  </FormItem>
                </div>
              </div>
              {/* <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="VSPM Status">
                    <Select defaultValue={vspm_status} >
                      <Option value="Active">Active</Option>
                      <Option value="DeActive">DeActive</Option>
                    </Select>
                  </FormItem>
                </div>
              </div> */}
              <div className="row p10">
                <div className="col-md-12">
                  <FormItem {...formItemLayout} label="Attchment">
                    <Upload>
                      <Button>
                    <UploadOutlined /> Click to Upload
                      </Button>
                    </Upload>
                  </FormItem>
                </div>
              </div>
            </Form>

            <div className="row p10">
              <div className="col-md-12">
                <div className="action-btn">
                  <Button onClick={()=>this.handleSubmit()} className="ant-btn ant-btn-primary">Send</Button>
                  <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                </div>
              </div>
            </div>
          </div>
          
           
          </div>
        </article>
      </div>
    );
  }
}

export default VesselEmail;
