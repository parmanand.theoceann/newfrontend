import React, { Component } from 'react';
import { Table, Modal } from 'antd';
import VesselEmail from './VesselEmail';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
} from '../../shared';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import ActiveStatus from './activeStatus'
import { EditOutlined } from '@ant-design/icons';


class VesselActivateList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      loading: true,
      sidebarVisible: false,
      emailData: null,
      modals: {
        VesselEmail: false,
        vesselActive: false
      },
      columns: [
        {
          title: 'Voyage.No',
          dataIndex: 'voyage_number',
        },
        {
          title: 'Vessel Code',
          dataIndex: 'vessel_code',
        },

        {
          title: 'Vessel Name',
          dataIndex: 'vessel_name',
        },
        {
          title: 'Type code',
          dataIndex: 'type_code',
        },

        {
          title: 'Voyage Status',
          dataIndex: 'vm_status_name',
        },

        {
          title: 'Vessel Type',
          dataIndex: 'vessel_type_name',
        },

        {
          title: 'Vessel Fleet',
          dataIndex: 'vessel_fleet_name',
        },

        {
          title: 'OwnerShip',
          dataIndex: 'owner_ship_name',
        },
        {
          title: 'Vessel DWT',
          dataIndex: 'vessel_dwt',
        },
        {
          title: 'Commence Date',
          dataIndex: 'commence_date',
        },
        {
          title: 'Comepleted Date',
          dataIndex: 'completing_date',
        },
        {
          title: 'VSPM Status',
          dataIndex: 'vspm_status',
        },
        {
          title: 'Action',
          dataIndex: 'action',

          render: (text, record) => {
            return (
              <div className="editable-row-operations">
                {record && record.vspm_status && record.vspm_status !== "DONE"
                  ?
                  <span className="iconWrapper">
                    <EditOutlined onClick={() => this.getData(record, 'vesselActive')} />
                  </span>
                  :
                  <span className="iconWrapper">
                    --
                  </span>
                }
              </div>
            )
          }
        },
      ],
      data: []
    };
  }

  componentDidMount = async () => {
    this.fetchData();
  };

  fetchData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      loading: true,
      data: [], 
    });

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = { loading: false }; 
    let _url = `${URL_WITH_VERSION}/voyage-manager/vessel-list?${qParamString}`;
    const response = await getAPICall(_url);
    const data = await response 
    console.log("vessel Activate Response:", data);
    if (data && data.data) {
      const totalRows = data.total_rows || 0; 
      const dataArr = data.data || [];
      if (dataArr.length > 0) {
        _state['data'] = dataArr; 
      }
      _state['pageOptions'] = {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      };
    }
    
    this.setState(_state);
};


  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.fetchData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible),
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    }
  };


  getData = (data, type) => {
    this.setState({
      emailData: data
    }, () => this.showHideModal(true, type))
  }
  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };
  onActionDonwload = () => {

  }
  render() {
    const { columns, data, loading, pageOptions, emailData, search, sidebarVisible } = this.state

    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Activate Voyage : Vessel List</span>
                  </h4>
                </div>

                <div className="toolbar-ui-wrapper">
                  <div className="rightsection">
                    <span className="wrap-bar-menu">
                      <ul className="wrap-bar-ul">
{/* 
                        <li>
                          <span
                            className="text-bt"
                            onClick={() => this.showHideModal(true, 'VesselEmail')}
                          >
                            Vessel Email
                          </span>
                        </li> */}

                        <li>
                          
                            <a href="#/dynamic-vspm">Back</a>
                         
                        </li>
                      </ul>
                    </span>
                  </div>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loading === false ?
                    <ToolbarUI
                      routeUrl={'activate-vessel-list-toolbar'}
                      optionValue={{ "pageOptions": pageOptions, "columns": columns, "search": search }}
                      callback={e => this.callOptions(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => this.onActionDonwload('csv') },
                        { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                        { title: 'XLS', event: () => this.onActionDonwload('xls') },
                      ]}
                    />
                    :
                    undefined
                }
              </div>
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={tableColumns}
                    dataSource={data}
                    pagination={false}
                    footer={false}
                    loading={loading}
                    scroll={{ x: 'max-content' }}
                    rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
        {this.state.modals['VesselEmail'] ?
          <Modal
            style={{ top: '2%' }}
            title="Vessel Email"
           open={this.state.modals['VesselEmail']}
            onCancel={() => this.showHideModal(false, 'VesselEmail')}
            width="40%"
            footer={null}
          >
            <VesselEmail data={emailData} />
          </Modal>
          :
          undefined
        }
        {this.state.modals['vesselActive'] ?
          <Modal
            style={{ top: '2%' }}
            title="Active/Deactive Vessel "
           open={this.state.modals['vesselActive']}
            onCancel={() => this.showHideModal(false, 'vesselActive')}
            width="50%"
            footer={null}
          >
            <ActiveStatus  
            closeModal={()=>this.showHideModal(false, 'vesselActive')}
            fetchDataFun={()=>this.fetchData()} 
             data={emailData} />
          </Modal>
          :
          undefined
        }
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptions(e)} /> : null
        }
      </div>
    );
  }
}

export default VesselActivateList;
