import React, { useEffect, useState } from "react";
import "./noonReport.css";
import URL_WITH_VERSION, {
  URL_WITHOUT_VERSION,
  getAPICall,
  postAPICall,
} from "../../shared";
import { Button, Flex, Input, DatePicker, Space, Select, Row, Col } from "antd";
import { SaveOutlined } from "@ant-design/icons";


import { useLocation, useParams } from "react-router-dom";
import data from "../dynamic-vspm/vesselCi/data";
import dayjs from "dayjs";
const { Option } = Select;
const DelayStopReport = (props) => {
  const [data, setData] = useState([]);
  const [formData, setFormData] = useState({
    report_content: {
      mainInformation: {
        sea_vessel: "",
        sea_imo: "",
        sea_voyage_no: "",
        sea_delay_start_time: "",
        sea_lat: "",
        sea_long: "",
        sea_delay_end_time: "",
        sea_lat: "",
        sea_long: "",
        sea_time_zone: "",
        sea_duration: "",
        sea_miles: "",
        sea_stoppage_reason: "",
        sea_report_date: ""
      },
      bunkerConsumption: {
        sea_brob_info: "",
        sea_brob_vlsfo: "",
        sea_brob_ulsfo: "",
        sea_brob_lsmgo: "",
        sea_brob_mgo: "",
        sea_me_propulsion_info: "",
        sea_me_propulsion_vlsfo: "",
        sea_me_propulsion_ulsfo: "",
        sea_me_propulsion_lsmgo: "",
        sea_me_propulsion_mgo: "",
        sea_boiler_info: "",
        sea_boiler_vlsfo: "",
        sea_boiler_ulsfo: "",
        sea_boiler_lsmgo: "",
        sea_boiler_mgo: "",
        sea_generator_info: "",
        sea_generator_vlsfo: "",
        sea_generator_ulsfo: "",
        sea_generator_lsmgo: "",
        sea_generator_mgo: "",
        sea_LD_info: "",
        sea_LD_vlsfo: "",
        sea_LD_ulsfo: "",
        sea_LD_lsmgo: "",
        sea_LD_mgo: "",
        sea_debllast_info: "",
        sea_debllast_vlsfo: "",
        sea_debllast_ulsfo: "",
        sea_debllast_lsmgo: "",
        sea_debllast_mgo: "",
        sea_idle_info: "",
        sea_idle_vlsfo: "",
        sea_idle_ulsfo: "",
        sea_idle_lsmgo: "",
        sea_idle_mgo: "",
        sea_heat_info: "",
        sea_heat_vlsfo: "",
        sea_heat_ulsfo: "",
        sea_heat_lsmgo: "",
        sea_heat_mgo: "",
        sea_fuelReceoved_info: "",
        sea_fuel_receoved_vlsfo: "",
        sea_fuel_receoved_ulsfo: "",
        sea_fuel_receoved_lsmgo: "",
        sea_fuel_receoved_mgo: "",
        sea_fuel_debunker_info: "",
        sea_fuel_debunker_vlsfo: "",
        sea_fuel_debunker_ulsfo: "",
        sea_fuel_debunker_lsmgo: "",
        sea_fuel_debunker_mgo: "",
        sea_aux_exngine_info: "",
        sea_aux_exngine_vlsfo: "",
        sea_aux_exngine_ulsfo: "",
        sea_aux_exngine_lsmgo: "",
        sea_aux_exngine_mgo: "",
      },
      remark: {
        sea_remark: "",
        sea_captain: "",
      },
    }
  });

  const [paramsData, setParamsData] = useState({
    vessel_name: "",
    voyage_number: "",
    sea_imo: "",
  })

  const { vessel_name, voyage_number } = useParams();
  const location = useLocation();

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const urlVesselName = params.get("vessel_name");
    const urlVoyageNumber = params.get("voyage_number");



    const getFormInfo = async () => {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/voyage-data?ae=${urlVoyageNumber}`
      );
      const result = await response.data;
      let obj = result[0];
      const { imo_no } = result[0];



      setFormData((prevState) => ({
        ...prevState,
        report_content: {
          ...prevState.report_content,
          mainInformation: {
            ...prevState.report_content.mainInformation,
            sea_vessel: obj.vessel_name,
            sea_imo: obj.imo_no,
            sea_voyage_no: obj.estimate_id,
          }
        }
      }))
    };

    if (props.byEdit == true) {
      getEditData()
    } else {
      getFormInfo();
    }
  }, []);

  const handleChange = (e, group) => {
    const { name, value } = e.target;


    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [name]: value
        }
      }
    }))

  };

  const handleSelect = (value, group, field) => {


    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: value
        }
      }
    }))
  }

  const getEditData = async (id) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/vessel/edit-delayatsea-report?ae=${props.reportNo}`
    );
    const data = await response.data;
    setFormData((prevState) => ({
      ...prevState,
      // report_content:Object.assign({}, data.report_content.report_content)
      report_content: {
        ...data.report_content,

      },
      status: data.status
    }))
  };

  const saveformData = async (postData) => {
    let _url = "save";
    let _method = "post";

    if (props.byEdit == true) {
      _url = "update";
      _method = "put";
    }
    if (_url == 'update') {
      postData.id = props.reportNo;
    }

    let suURL = `${URL_WITH_VERSION}/voyage-manager/vessel/${_url}-delayatsea-report`;

    // let suMethod = "POST";
    postAPICall(suURL, postData, _method, (data) => {
      if (data && data.data) {
        // openNotificationWithIcon("success", data.message);
        // if (vessel_form_id) {
        //   this.setState({ loadForm: false });
        //   this.props.onClose();
        // }

        // getEditData(data.row.rid);
      } else {
        // openNotificationWithIcon("error", data.message);
      }
    });
  };

  const handleSelectDate = (date, dateString, group, field) => {
    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: dateString
        }
      }
    }))
  };

  return (
    <div
      className="parentBox"
    >

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <img
          src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
          width={150}
          alt="logo"
          style={{ marginLeft: "1rem" }}
        ></img>

        <Button
          type="primary"
          style={{ padding: "4px 24px" }}
          onClick={() => saveformData(formData)}
        >
          {/* <SaveOutlined />
           */}
           Send Report
        </Button>

        {/* <h1
            style={{
              textAlign: "center",
              fontSize: "1.72rem",
              marginLeft: "30%",
            }}
          >
            Maritime is Our kingdom.
          </h1> */}
      </div>
      <div className="rpbox">
        <Row>
          <h4 className="mainHeading" >Main Information</h4>
        </Row>
        <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }}>
          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={2}>
                <Col span={8}>
                  <label className="required">Vessel:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Vessel"
                    name="sea_vessel"
                    value={formData.report_content.mainInformation.sea_vessel}
                    onChange={(e) => handleChange(e, "mainInformation")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label >IMO:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="IMO"
                    name="sea_imo"
                    value={formData.report_content.mainInformation.sea_imo}
                    onChange={(e) => handleChange(e, "mainInformation")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label htmlFor="field3">Voyage NO.:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Voyage NO"
                    name="sea_voyage_no"
                    value={formData.report_content.mainInformation.sea_voyage_no}
                    onChange={(e) => handleChange(e, "mainInformation")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>
          </Row>

         <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label htmlFor="field4">Time Zone:</label>
                </Col>
                <Col span={16}>
                  <Row gutter={2}>
                    <Col span={8}>
                      <Select defaultValue="GMT"

                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "sea_time_zone"
                          )
                        }
                      >
                        <Option value="ULSFO">GMT</Option>
                        <Option value="Local">Local</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Select defaultValue="+">
                        <Option value="ULSFO">+</Option>
                        <Option value="Local">-</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Input
                        style={{ height: '100%' }}
                        placeholder="Time Zone"
                        name="sea_time_zone"
                        value={formData.report_content.mainInformation.sea_time_zone}
                        onChange={(e) =>
                          handleChange(e, "mainInformation")
                        }
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Delay start time:</label>
                </Col>
                <Col span={16} >
                  <Input
                  style={{height:"100%"}}
              
                    placeholder="Delay start time"
                    name="sea_delay_start_time"
                    value={formData.report_content.mainInformation.sea_delay_start_time}
                    onChange={(e) =>
                      handleChange(e, "mainInformation")
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Lat.</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        placeholder="DDMMSS"
                  style={{height:"100%"}}
                   
                        name="sea_lat"
                        value={formData.report_content.mainInformation.sea_lat}
                        onChange={(e) => handleChange(e, "mainInformation")}
                      />
                    </Col>

                    <Col span={12}>
                      <Select defaultValue="N"
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "sea_lat"
                          )
                        }

                      >
                        <Option value="N">N</Option>
                        <Option value="S">S</Option>
                      </Select>
                    </Col>

                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Long. :</label>
                </Col>
                <Col span={16}>
                  <Row gutter={1}>
                    <Col span={12}>
                      <Input
                  
                        placeholder="DDMMSS"
                        name="sea_long"
                        style={{height:"100%" }}
                        value={formData.report_content.mainInformation.sea_long}
                        onChange={(e) => handleChange(e, "mainInformation")}
                      />
                    </Col>
                    <Col span={12}>
                      <Select defaultValue="E"

                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "sea_long"
                          )
                        }
                      >
                        <Option value="E">E</Option>
                        <Option value="W">W</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Delay End time:</label>
                </Col>
                <Col span={16} >
                  <Input
                  style={{height:"100%"}}
                    placeholder="Delay End time"
                    name="sea_delay_end_time"
                    value={formData.report_content.mainInformation.sea_delay_end_time}
                    onChange={(e) =>
                      handleChange(e, "mainInformation")
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Lat.</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        placeholder="DDMMSS"
                        name="sea_lat"
               
                  style={{height:"100%"  }}
                        value={formData.report_content.mainInformation.sea_lat}
                        onChange={(e) => handleChange(e, "mainInformation")}
                      />
                    </Col>

                    <Col span={12}>
                      <Select defaultValue="N"
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "sea_lat"
                          )
                        }
                      >
                        <Option value="N">N</Option>
                        <Option value="S">S</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Long. :</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        placeholder="DDMMSS"

                        style={{ height:"100%"}}
                        name="sea_long"
                        value={formData.report_content.mainInformation.sea_long}
                        onChange={(e) => handleChange(e, "mainInformation")}
                      />
                    </Col>
                    <Col span={12}>
                      <Select defaultValue="E"
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "sea_long"
                          )
                        }
                      >
                        <Option value="E">E</Option>
                        <Option value="W">W</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Duration</label>
                </Col>
                <Col span={16} >
                  <Input
                  style={{height:"100%"}}
                    placeholder="DD HH MM"
                    name="sea_duration"
                    value={formData.report_content.mainInformation.sea_duration}
                    onChange={(e) => handleChange(e, "mainInformation")}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Miles:</label>
                </Col>
                <Col span={16}>
                  <Input
                  style={{height:"100%"}}

                    placeholder="Miles"
                    name="sea_miles"
                    value={formData.report_content.mainInformation.sea_miles}
                    onChange={(e) => handleChange(e, "mainInformation")}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Stopage reaosn:</label>
                </Col>
                <Col span={16}>
                  <Input
                  style={{height:"100%"}}

                    placeholder="Stopage Reaosn"
                    name="sea_stoppage_reason"
                    value={formData.report_content.mainInformation.sea_stoppage_reason}
                    onChange={(e) =>
                      handleChange(e, "mainInformation")
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label className="required">Date</label>

                </Col>
                <Col span={16} >
                  <DatePicker
                    showTime
                    format="YYYY-MM-DD HH:mm:ss"
                    name="sea_report_date"
                    id="sea_report_date"
                    style={{ width: "100%" }}
                    value={formData.report_content.mainInformation.sea_report_date ? dayjs(
                      formData.report_content.mainInformation.sea_report_date,
                      "YYYY-MM-DD HH:mm:ss"
                    ) : undefined}
                    onChange={(date, dateString) => {
                      handleSelectDate(
                        date,
                        dateString,
                        "mainInformation",
                        "sea_report_date"
                      );
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
      <div className="rpbox">
        <h4 className="mainHeading">Bunker Consumption</h4>
        <div className="row">
          <div className="col-12">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="trrow">
                    <th>Type (MT)</th>
                    <th>IFO</th>
                    <th>VLSFO</th>
                    <th>ULSFO</th>
                    <th>LSMGO</th>
                    <th>MGO</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="tableRow">
                    <td className="required">BROB:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_brob_info"
                        value={formData.report_content.bunkerConsumption.sea_brob_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"

                        placeholder="0"
                        name="sea_brob_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_brob_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_brob_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_brob_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_brob_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_brob_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_brob_mgo"
                        value={formData.report_content.bunkerConsumption.sea_brob_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>M/E Propulsion:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_me_propulsion_info"
                        value={formData.report_content.bunkerConsumption.sea_me_propulsion_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_me_propulsion_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_me_propulsion_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_me_propulsion_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_me_propulsion_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_me_propulsion_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_me_propulsion_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_me_propulsion_mgo"
                        value={formData.report_content.bunkerConsumption.sea_me_propulsion_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Boiler:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_boiler_info"
                        value={formData.report_content.bunkerConsumption.sea_boiler_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_boiler_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_boiler_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_boiler_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_boiler_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_boiler_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_boiler_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_boiler_mgo"
                        value={formData.report_content.bunkerConsumption.sea_boiler_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Generator:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_generator_info"
                        value={formData.report_content.bunkerConsumption.sea_generator_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_generator_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_generator_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_generator_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_generator_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_generator_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_generator_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_generator_mgo"
                        value={formData.report_content.bunkerConsumption.sea_generator_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>L/D:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_LD_info"
                        value={formData.report_content.bunkerConsumption.sea_LD_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_LD_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_LD_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_LD_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_LD_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_LD_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_LD_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_LD_mgo"
                        value={formData.report_content.bunkerConsumption.sea_LD_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Debllast:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_debllast_info"
                        value={formData.report_content.bunkerConsumption.sea_debllast_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_debllast_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_debllast_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_debllast_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_debllast_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_debllast_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_debllast_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_debllast_mgo"
                        value={formData.report_content.bunkerConsumption.sea_debllast_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Idle/.On:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_idle_info"
                        value={formData.report_content.bunkerConsumption.sea_idle_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_idle_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_idle_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_idle_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_idle_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_idle_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_idle_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_idle_mgo"
                        value={formData.report_content.bunkerConsumption.sea_idle_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Heat:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_heat_info"
                        value={formData.report_content.bunkerConsumption.sea_heat_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_heat_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_heat_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_heat_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_heat_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_heat_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_heat_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_heat_mgo"
                        value={formData.report_content.bunkerConsumption.sea_heat_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Fuel Receoved:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuelReceoved_info"
                        value={formData.report_content.bunkerConsumption.sea_fuelReceoved_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_receoved_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_receoved_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_receoved_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_receoved_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_receoved_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_receoved_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_receoved_mgo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_receoved_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Fuel Debunker:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_debunker_info"
                        value={formData.report_content.bunkerConsumption.sea_fuel_debunker_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_debunker_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_debunker_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_debunker_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_debunker_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_debunker_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_debunker_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_fuel_debunker_mgo"
                        value={formData.report_content.bunkerConsumption.sea_fuel_debunker_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>Aux Exngine:</td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_aux_exngine_info"
                        value={formData.report_content.bunkerConsumption.sea_aux_exngine_info}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_aux_exngine_vlsfo"
                        value={formData.report_content.bunkerConsumption.sea_aux_exngine_vlsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_aux_exngine_ulsfo"
                        value={formData.report_content.bunkerConsumption.sea_aux_exngine_ulsfo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_aux_exngine_lsmgo"
                        value={formData.report_content.bunkerConsumption.sea_aux_exngine_lsmgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                    <td>
                      <Input
                        type="text"
                        placeholder="0"
                        name="sea_aux_exngine_mgo"
                        value={formData.report_content.bunkerConsumption.sea_aux_exngine_mgo}
                        onChange={(e) =>
                          handleChange(e, "bunkerConsumption")
                        }
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="rpbox" style={{ gap: "1.5rem" }}>
        <Row gutter={[24, 24]}>
          <Col span={2}><label >Remark:</label></Col>
          <Col span={22}>
              <Input
                placeholder="Remark"
                name="sea_remark"
                value={formData.report_content.remark.sea_remark}
                onChange={(e) => handleChange(e, "remark")}
              />
          </Col>
        </Row>
        <Row gutter={[24, 24]}>
          <Col span={2}><label>Captain:</label></Col>
          <Col span={22}>
          <Input
            placeholder="Captain"
            name="sea_captain"
            value={formData.report_content.remark.sea_captain}
            onChange={(e) => handleChange(e, "remark")}
          />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DelayStopReport;
