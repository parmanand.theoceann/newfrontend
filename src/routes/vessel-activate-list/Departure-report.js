import React, { useEffect, useState } from "react";
import "./noonReport.css";
import { SaveOutlined } from "@ant-design/icons";
import { Input, Row, Col, Select, DatePicker, Button, Modal } from "antd";
import { useParams } from "react-router-dom";
import { useLocation } from "react-router-dom";
import URL_WITH_VERSION, { getAPICall, postAPICall } from "../../shared";
import moment from "moment";
import dayjs from "dayjs";
import PortSelection from "../port-selection/PortSelection";

const { Option } = Select;
const DepartureReport = (props) => {
  const [formdata, setFormdata] = useState({
    report_content: {
      mainInformation: {
        departure_vessel: "",
        departure_imo: "",
        departure_call_sign: "",
        departure_time_zone: "",
        departure_time_zone_plus: "",
        departure_9h: "",
        departure_end_of_sea_passage: "",
        departure_main_lat: "",
        departure_main_lat_dir: "",
        departure_main_long: "",
        departure_main_long_dir: "",
        departure_voyage_no: "",
        depature_report_date: "",
      },
      portInformation: {
        departure_port: "",
        departure_port_id: "",
        departure_port_lat: "",
        departure_port_lat_dir: "",
        departure_port_long: "",
        departure_port_long_dir: "",
        departure_dateee: "",
        departure_next_port: "",
        departure_next_port_id: "",
        departure_eta: "",
        departure_pob_date: "",
      },
      cargoInformation: {
        departure_cargo_ops_start: "",
        departure_cargo_ops_end: "",
        departure_cargo_loaded: "",
        departure_type_of_operation: "",
        departure_deck_cargo_mt: "",
        departure_hold_cargo_mt: "",
        departure_displacement_mt: "",
        departure_fw_received: "",
        departure_fw_rob: "",
        departure_distill_watr_rcvd: "",
        departure_slop_produced: "",
        departure_slop_rob: "",
        departure_cleaning_chemical: "",
        departure_last_line: "",
        departure_fw_consumed: "",
        departure_cargo_discharg: "",
        departure_operation_total_time: "",
        departure_operation_completion_date_time: "",
        departure_cargo_desc: "",
      },
      vesselDistance: {
        departure_passage: "",
        departure_ordered_spd: "",
        departure_reported_spd: "",
        departure_observe_dist: "",
        departure_engine_dist: "",
        departure_noon_boiler_hrs: "",
        departure_ordered_me_consumption: "",
        departure_main_eng_rev: "",
        departure_slip_percentage: "",
        departure_salinity: "",

        departure_density: "",
        departure_aver_rpm: "",
        departure_ave_bhp: "",
        departure_fwd_dft: "",
        departure_aft_drft: "",
        departure_mid_draft: "",
        departure_steaming_hr: "",
        departure_air_press: "",
        departure_air_tempt: "",
        departure_gen_one_hr: "",

        departure_gen_two_hr: "",
        departure_gen_three_hr: "",
        departure_gen_four_hr: "",
        departure_me_kw_hr: "",
        departure_gen_one_kw_hr: "",
        departure_gen_two_kw_hr: "",
        departure_gen_three_kw_hr: "",
        departure_gen_four_kw_hr: "",
        departure_me_hr: "",
        departure_sea_state: "",

        departure_sea_dir: "",
        departure_sea_height: "",
        departure_swell: "",
        departure_swell_dir: "",
        departure_swell_height: "",
        departure_wind_for: "",
        departure_wind_dir: "",
      },
      bunker: {
        departure_brob_info: "",
        departure_brob_vlsfo: "",
        departure_brob_ulsfo: "",
        departure_brob_lsmgo: "",
        departure_brob_mgo: "",
        departure_me_propulsion_info: "",
        departure_me_propulsion_vlsfo: "",
        departure_me_propulsion_ulsfo: "",
        departure_me_propulsion_lsmgo: "",
        departure_me_propulsion_mgo: "",

        departure_boiler_info: "",
        departure_boiler_vlsfo: "",
        departure_boiler_ulsfo: "",
        departure_boiler_lsmgo: "",
        departure_boiler_mgo: "",
        departure_generator_info: "",
        departure_generator_vlsfo: "",
        departure_generator_ulsfo: "",
        departure_generator_lsmgo: "",
        departure_generator_mgo: "",

        departure_ld_info: "",
        departure_ld_vlsfo: "",
        departure_ld_ulsfo: "",
        departure_ld_lsmgo: "",
        departure_ld_mgo: "",
        departure_de_bllast_info: "",
        departure_de_bllast_vlsfo: "",
        departure_de_bllast_ulsfo: "",
        departure_de_bllast_lsmgo: "",
        departure_de_bllast_mgo: "",

        departure_idle_info: "",
        departure_idle_vlsfo: "",
        departure_idle_ulsfo: "",
        departure_idle_lsmgo: "",
        departure_idle_mgo: "",
        departure_heat_info: "",
        departure_heat_vlsfo: "",
        departure_heat_ulsfo: "",
        departure_heat_lsmgo: "",
        departure_heat_mgo: "",

        departure_fuel_receoved_info: "",
        departure_fuel_receoved_vlsfo: "",
        departure_fuel_receoved_ulsfo: "",
        departure_fuel_receoved_lsmgo: "",
        departure_fuel_receoved_mgo: "",
        departure_fuel_debunker_info: "",
        departure_fuel_debunker_vlsfo: "",
        departure_fuel_debunker_ulsfo: "",
        departure_fuel_debunker_lsmgo: "",
        departure_fuel_debunker_mgo: "",

        departure_aux_exngine_info: "",
        departure_aux_exngine_vlsfo: "",
        departure_aux_engine_ulsfo: "",
        departure_aux_exngine_lsmgo: "",
        departure_aux_exngine_mgo: "",
      },
      remark: {
        departure_remark: "",
        departure_captain: "",
        departure_ce: "",
      },
    },
  });
  const [ports, setPorts] = useState(null);
  const [isPortModal, setPortModal] = useState(false);
  const [isNextPortModal, setNextPortModal] = useState(false);
  const [key, setKey] = useState("");
  const { vessel_name, voyage_number } = useParams();
  const location = useLocation();

  // console.log('bunker...', formdata.bunker.departure_brob_vlsfo);
  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const urlVesselName = params.get("vessel_name");
    const urlVoyageNumber = params.get("voyage_number");

    const getFormInfo = async () => {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/voyage-data?ae=${urlVoyageNumber}`
      );

      const data = await response.data;
      let obj = data[0];

      setFormdata((prevState) => ({
        ...prevState,
        report_content: {
          ...prevState.report_content,
          mainInformation: {
            ...prevState.report_content.mainInformation,
            departure_vessel: obj.vessel_name,
            departure_imo: obj.imo_no,
            departure_voyage_no: obj.estimate_id,
          },
        },
      }));

      const portOptions = ports?.map(({ port, port_id }) => ({
        label: port,
        value: port,
        key: port_id,
      }));
      setPorts(portOptions);
    };

    if (props.byEdit == true) {
      getEditData();
    } else {
      getFormInfo();
    }
  }, []);

  const modalCloseEvent = (data) => {
    console.log(data);
    if (key == "arrival_port") {
      setFormdata((prev) => ({
        ...prev,
        report_content: {
          ...prev.report_content,
          portInformation: {
            ...prev.report_content.portInformation,
            departure_port: data.port.port_name,
            departure_port_id: data.port.PortID,
          },
        },
      }));
    }
    if (key === "departure_next_port") {
      setFormdata((prev) => ({
        ...prev,
        report_content: {
          ...prev.report_content,
          portInformation: {
            ...prev.report_content.portInformation,
            departure_next_port: data.port.port_name,
            departure_next_port_id: data.port.PortID,
          },
        },
      }));
    }

    setPortModal(false);
    setNextPortModal(false)
  };

  const handleChange = (e, group) => {
    // console.log('group', group);
    const { name, value } = e.target;
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [name]: value,
        },
      },
    }));
  };

  const handleSelect = (value, group, field) => {
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: value,
        },
      },
    }));
  };

  const handleSelectDate = (date, dateString, group, field) => {
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: dayjs(date, "YYYY-MM-DD HH:mm:ss"),
        },
      },
    }));
  };

  const getEditData = async () => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/vessel/edit-departure-report?ae=${props.reportNo}`
    );
    let data = await response.data;
    // console.log("departure Data", data );

    setFormdata((prevState) => ({
      ...prevState,
      // report_content:Object.assign({}, data.report_content.report_content)
      report_content: {
        ...data.report_content,
      },
      status: data.status,
    }));
  };

  const saveFormData = async (postData) => {
    let _url = "save";
    let _method = "post";

    if (props.byEdit == true) {
      _url = "update";
      _method = "put";
    }

    if (_url == "update") {
      postData.id = props.reportNo;
    }

    let suURL = `${URL_WITH_VERSION}/voyage-manager/vessel/${_url}-departure-report`;

    let suMethod = "POST";
    postAPICall(suURL, postData, suMethod, (data) => {
      if (data && data.data) {
        // openNotificationWithIcon("success", data.message);
        // if (vessel_form_id) {
        //   this.setState({ loadForm: false });
        //   this.props.onClose();
        // }
        //   getEditData(data.row.rid);
      } else {
        // openNotificationWithIcon("error", data.message);
      }
    });
  };

  const portSelectionHandler = (value, group, field, currOption) => {
    if (field === "port") {
      setFormdata((prev) => ({
        ...prev,
        portInformation: {
          ...prev.portInformation,
          [field]: value,
          departure_port_id: currOption.key,
        },
      }));
    } else {
      setFormdata((prev) => ({
        ...prev,
        portInformation: {
          ...prev.portInformation,
          [field]: value,
          departure_next_port_id: currOption.key,
        },
      }));
    }
  };

  return (
    <div className="parentBox">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <img
          src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
          width={150}
          alt="logo"
          style={{ marginLeft: "1rem" }}
        ></img>


        <Button type="primary" onClick={() => saveFormData(formdata)}>
          Send Report
        </Button>

      </div>


      {/* <button style={{outline: 'none'}}>
          <SaveOutlined style={{ margin: "10px 0 0px 30px" }} />
        </button> */}


      <Row align="middle">
        <Col span={8}>

        </Col>
        <Col span={16}>

        </Col>
      </Row>
















      <div className="rpbox">
        <Row>
          <h4 className="mainHeading" >Main Information</h4>
        </Row>

        <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }} >


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label className="required">Vessel:</label>
                </Col>
                <Col span={16}>
                  <Input
                    value={
                      formdata.report_content.mainInformation
                        ?.departure_vessel
                    }
                    onChange={(e) => handleChange(e, "mainInformation")}
                    type="text"
                    name="departure_vessel"
                    id="departure_vessel"
                    required
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label className="required">IMO:</label>
                </Col>
                <Col span={16}>
                  <Input
                    value={
                      formdata.report_content.mainInformation.departure_imo
                    }
                    onChange={(e) => handleChange(e, "mainInformation")}
                    type="text"
                    name="departure_imo"
                    id="departure_imo"
                    required
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Call Sign:</label>
                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "mainInformation")}
                    type="text"
                    name="departure_call_sign"
                    id="departure_call_sign"
                    value={
                      formdata.report_content.mainInformation
                        .departure_call_sign
                    }
                  />
                </Col>
              </Row>
            </Col>



          </Row>


          <Row gutter={[80]}>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label >Time Zone:</label>
                </Col>
                <Col span={16}>
                  <Row gutter={1} align="middle">
                    <Col span={8}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "departure_time_zone"
                          )
                        }

                        defaultValue="GMT"
                        name="departure_time_zone"
                        id="departure_time_zone"
                      >
                        <Option value="GMT">GMT</Option>
                        <Option value="Local">Local</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "departure_time_zone_plus"
                          )
                        }

                        defaultValue="+"
                        name="departure_time_zone_plus"
                        id="departure_time_zone_plus"
                      >
                        <Option value="+">+</Option>
                        <Option value="-">-</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Input
                        styles={{ height: "100%" }}
                        onChange={(e) => handleChange(e, "mainInformation")}
                        type="text"
                        name="departure_9h"
                        id="departure_9h"

                        value={
                          formdata.report_content.mainInformation.departure_9h
                        }
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label className="required">Start of Sea Passage:</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    style={{ width: '100%' }}
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "mainInformation",
                        "departure_end_of_sea_passage"
                      )
                    }
                    format="YYYY-MM-DD"
                    name="departure_end_of_sea_passage"
                    id="departure_end_of_sea_passage"
                    required
                    value={
                      formdata.report_content.mainInformation
                        .departure_end_of_sea_passage
                        ? dayjs(
                          formdata.report_content.mainInformation
                            .departure_end_of_sea_passage,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    } />
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Lat. :</label>
                </Col>
                <Col span={16}>
                  <Row
                    gutter={8}
                  >
                    <Col span={12}>
                      <Input
                        style={{ height: "100%" }}
                        onChange={(e) => handleChange(e, "mainInformation")}
                        type="text"
                        placeholder="DDMMSS"
                        name="departure_main_lat"
                        id="departure_main_lat"
                        value={
                          formdata.report_content.mainInformation
                            .departure_main_lat
                        }
                      />
                    </Col>
                    <Col span={12}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "departure_main_lat_dir"
                          )
                        }
                        defaultValue="N"
                        name="departure_main_lat_dir"
                        id="departure_main_lat_dir"
                        style={{ width: "100%" }}
                      >
                        <Option value="N">N</Option>
                        <Option value="S">S</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>


          </Row>


          <Row gutter={[80]}>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Long. :</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        style={{ height: "100%" }}
                        onChange={(e) => handleChange(e, "mainInformation")}
                        type="text"
                        placeholder="DDMMSS"
                        name="departure_main_long"
                        id="departure_main_long"
                        value={
                          formdata.report_content.mainInformation
                            .departure_main_long
                        }
                      />
                    </Col>
                    <Col span={12}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "mainInformation",
                            "departure_main_long_dir"
                          )
                        }
                        defaultValue="E"
                        name="departure_main_long_dir"
                        id="departure_main_long_dir"

                      >
                        <Option value="E">E</Option>
                        <Option value="W">W</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label className="required">Voyage No. :</label>
                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "mainInformation")}
                    value={
                      formdata.report_content.mainInformation
                        .departure_voyage_no
                    }
                    type="text"
                    name="departure_voyage_no"
                    id="departure_voyage_no"
                    required
                    disabled
                  />
                </Col>
              </Row>
            </Col>



            <Col span={8}>
              <Row align="middle">
                <Col span={8}>
                  <label>Date</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "mainInformation",
                        "depature_report_date"
                      )
                    }
                    style={{ width: "100%" }}
                    format="DD/MM/YYYY"
                    name="depature_report_date"
                    id="depature_report_date"
                    value={
                      formdata.report_content.mainInformation
                        .depature_report_date
                        ? dayjs(
                          formdata.report_content.mainInformation
                            .depature_report_date,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>

      <div className="rpbox">
        <Row>
          <h4 className="mainHeading" >Port Information</h4>
        </Row>

        <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }} >


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8} >
                  <label className="required">Port</label>
                </Col>
                <Col span={16}>
                  {/* <Select
                      name="port"
                      id="port"
                      style={{ width: "100%" }}
                      onChange={(value, currOption) => {
                        portSelectionHandler(
                          value,
                          "portInformation",
                          "port",
                          currOption
                        );
                      }}
                      options={ports}
                    ></Select> */}
                  <Input
                    onClick={() => {
                      setKey("arrival_port");
                      setPortModal(true);
                    }}
                    type="text"
                    name="port"
                    id="port"
                    className="form-control"
                    value={
                      formdata.report_content.portInformation.departure_port
                    }
                    style={{ height: "100%" }}
                    placeholder="Port"
                  />
                  {isPortModal && (
                    <Modal
                      title="Select Port"
                      open={isPortModal}
                      width="80%"
                      onCancel={() => {
                        setPortModal(false);
                      }}
                      footer={null}
                    >
                      <PortSelection
                        fromPortID={null}
                        modalCloseEvent={(data) => {
                          modalCloseEvent(data);
                        }}
                      />
                    </Modal>
                  )}
                </Col>
              </Row>
            </Col>
            {/* <Col span={8}>
                <Row align="middle">
                  <Col span={8}>
                    <label>Port ID:</label>
                  </Col>
                  <Col span={16}>
                    <Input
                      onChange={(e) => handleChange(e, "portInformation")}
                      type="text"
                      name="departure_port_id"
                      id="departure_port_id"
                      value={formdata.report_content.portInformation.departure_port_id}
                    />
                  </Col>
                </Row>
              </Col> */}
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>POB:</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    style={{ width: '100%' }}
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "portInformation",
                        "departure_pob_date"
                      )
                    }

                    name="departure_pob_date"
                    id="departure_pob_date"
                    format="DD/MM/YYYY"
                    value={
                      formdata.report_content.portInformation
                        .departure_pob_date
                        ? dayjs(
                          formdata.report_content.portInformation
                            .departure_pob_date,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Latitude:</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        style={{ height: "100%" }}
                        onChange={(e) => handleChange(e, "portInformation")}
                        type="text"
                        placeholder="DDMMSS"
                        name="departure_port_lat_dir"
                        id="departure_port_lat_dir"
                        value={
                          formdata.report_content.portInformation
                            .departure_port_lat_dir
                        }
                      />
                    </Col>
                    <Col span={12}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "portInformation",
                            "departure_port_lat"
                          )
                        }
                        style={{ width: "100%" }}
                        defaultValue="N"
                        name="departure_port_lat"
                        id="departure_port_lat"
                      >
                        <Option value="N">N</Option>
                        <Option value="S">S</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]} >
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Longitude:</label>
                </Col>
                <Col span={16}>
                  <Row gutter={8}>
                    <Col span={12}>
                      <Input
                        style={{ height: "100%" }}
                        onChange={(e) => handleChange(e, "portInformation")}
                        type="text"
                        placeholder="DDMMSS"
                        name="departure_port_long"
                        id="departure_port_long"
                        value={
                          formdata.report_content.portInformation
                            .departure_port_long
                        }
                      />
                    </Col>
                    <Col span={12}>
                      <Select
                        onChange={(value) =>
                          handleSelect(
                            value,
                            "portInformation",
                            "departure_port_long_dir"
                          )
                        }
                        style={{ width: "100%" }}
                        value={
                          formdata.report_content.portInformation
                            .departure_port_long_dir
                        }
                        name="departure_port_long_dir"
                        id="departure_port_long_dir"
                      >
                        <Option value="E">E</Option>
                        <Option value="W">W</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>DTG:</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    style={{ width: '100%' }}
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "portInformation",
                        "departure_dateee"
                      )
                    }
                    value={
                      formdata.report_content.portInformation.departure_dateee
                        ? dayjs(
                          formdata.report_content.portInformation
                            .departure_dateee,
                          "YYYY-MM-DD"
                        )
                        : undefined
                    }

                    name="departure_dateee"
                    id="departure_dateee"
                  />
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Next Port:</label>
                </Col>
                <Col span={16}>
                  <Input
                    onClick={() => {
                      setKey("departure_next_port");
                      setNextPortModal(true);
                    }}
                    type="text"
                    name="departure_next_port"
                    id="departure_next_port"
                    className="form-control"
                    value={
                      formdata.report_content.portInformation.departure_next_port
                    }
                    onChange={(value) => {
                      handleChange(value, "portInformation");
                    }}
                    required
                    style={{ height: "100%" }}
                    placeholder="Next Port"
                  />
                  {isNextPortModal && (
                    <Modal
                      title="Select Port"
                      open={isNextPortModal}
                      width="80%"
                      onCancel={() => {
                        setNextPortModal(false);
                      }}
                      footer={null}
                    >
                      <PortSelection
                        fromPortID={null}
                        modalCloseEvent={(data) => {
                          modalCloseEvent(data);
                        }}
                      />
                    </Modal>
                  )}
                </Col>
              </Row>
            </Col>
          </Row>


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>ETA:</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "portInformation",
                        "departure_eta"
                      )
                    }
                    style={{ width: "100%" }}
                    value={
                      formdata.report_content.portInformation.departure_eta
                        ? dayjs(
                          formdata.report_content.portInformation
                            .departure_eta,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    name="departure_eta"
                    id="departure_eta"
                  />
                </Col>
              </Row>
            </Col>
          </Row>


        </div>
      </div>

      <div className="rpbox">
        <h4 className="mainHeading">Cargo Information</h4>
        <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }} >


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={10}>
                  <label className="label-col">Cargo Ops Start:</label>
                </Col>
                <Col span={14}>
                  <DatePicker
                    style={{ width: "100%" }}
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cargo_ops_start
                        ? dayjs(
                          formdata.report_content.cargoInformation
                            .departure_cargo_ops_start,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "cargoInformation",
                        "departure_cargo_ops_start"
                      )
                    }
                    format="DD/MM/YYYY"
                    name="departure_cargo_ops_start"
                    id="departure_cargo_ops_start"
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={10}>
                  <label className="label-col">Cargo Ops End:</label>
                </Col>
                <Col span={14}>
                  <DatePicker
                    style={{ width: "100%" }}
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "cargoInformation",
                        "departure_cargo_ops_end"
                      )
                    }
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cargo_ops_end
                        ? dayjs(
                          formdata.report_content.cargoInformation
                            .departure_cargo_ops_end,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    format="DD/MM/YYYY"
                    name="departure_cargo_ops_end"
                    id="departure_cargo_ops_end"
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label className="label-col">Cargo Loaded:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => {
                      handleChange(e, "cargoInformation");
                    }}
                    type="number"
                    name="departure_cargo_loaded"
                    id="departure_cargo_loaded"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cargo_loaded
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={10}>
                  <label>FW received:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_fw_received"
                    id="departure_fw_received"
                    data-section="cargoInformation"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_fw_received
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={10}>
                  <label>FW ROB:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_fw_rob"
                    id="departure_fw_rob"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_fw_rob
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={10}>
                  <label>Distill Watr Rcvd:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_distill_watr_rcvd"
                    id="departure_distill_watr_rcvd"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_distill_watr_rcvd
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>SLOP discharge:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_slop_produced"
                    id="departure_slop_produced"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_slop_produced
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>SLOP ROB:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: '100%' }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_slop_rob"
                    id="departure_slop_rob"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_slop_rob
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Cleaning chemical:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_cleaning_chemical"
                    id="departure_cleaning_chemical"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cleaning_chemical
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Last Line:</label>
                </Col>
                <Col span={14}>
                  <DatePicker
                    style={{ width: "100%" }}
                    onChange={(date, dateString) =>
                      handleSelectDate(
                        date,
                        dateString,
                        "cargoInformation",
                        "departure_last_line"
                      )
                    }
                    value={
                      formdata.report_content.cargoInformation
                        .departure_last_line
                        ? dayjs(
                          formdata.report_content.cargoInformation
                            .departure_last_line,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    format="DD/MM/YYYY"
                    name="departure_last_line"
                    id="departure_last_line"
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>FW consumed:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}

                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_fw_consumed"
                    id="departure_fw_consumed"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_fw_consumed
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Cargo Discharged:</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}

                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_cargo_discharg"
                    id="departure_cargo_discharg"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cargo_discharg
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>
                    Type of Operation
                  </label>
                </Col>
                <Col span={14}>
                  <Select
                    name="departure_type_of_operation"
                    id="departure_type_of_operation"
                    style={{ width: "100%" }}
                    value={
                      formdata.report_content.cargoInformation
                        .departure_type_of_operation
                    }
                    onChange={(value) => {
                      handleSelect(
                        value,
                        "cargoInformation",
                        "departure_type_of_operation"
                      );
                    }}
                  >
                    <Option value="Loading">Loading</Option>
                    <Option value="discharging">discharging</Option>
                  </Select>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Deck Cargo</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: '100%' }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_deck_cargo_mt"
                    id="departure_deck_cargo_mt"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_deck_cargo_mt
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label style={{ marginRight: "8px" }}>Hold Cargo</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_hold_cargo_mt"
                    id="departure_hold_cargo_mt"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_hold_cargo_mt
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Displacement</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_displacement_mt"
                    id="departure_displacement_mt"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_displacement_mt
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label >
                    Operation Total Time
                  </label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_operation_total_time"
                    id="departure_operation_total_time"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_operation_total_time
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8} align="middle">
                <Col span={10}>
                  <label>Operation Completion</label>
                </Col>
                <Col span={14}>
                  <DatePicker
                    showTime
                    format="YYYY-MM-DD HH:mm:ss"
                    name="departure_operation_completion_date_time"
                    id="departure_operation_completion_date_time"
                    style={{ width: "100%" }}
                    value={
                      formdata.report_content.cargoInformation
                        .departure_operation_completion_date_time
                        ? dayjs(
                          formdata.report_content.cargoInformation
                            .departure_operation_completion_date_time,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    onChange={(date, dateString) => {
                      handleSelectDate(
                        date,
                        dateString,
                        "cargoInformation",
                        "departure_operation_completion_date_time"
                      );
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={10}>
                  <label>Cargo Description</label>
                </Col>
                <Col span={14}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "cargoInformation")}
                    type="number"
                    name="departure_cargo_desc"
                    id="departure_cargo_desc"
                    value={
                      formdata.report_content.cargoInformation
                        .departure_cargo_desc
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>






      <div className="rpbox">
        <h4 className="mainHeading">Vessel & Distance</h4>
        <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }} >
          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>

              <Row align="middle">
                <Col span={8}>
                  <label>Passage:</label>
                </Col>
                <Col span={16}>
                  <Select
                    onChange={(value) =>
                      handleSelect(value, "vesselDistance", "departure_passage")
                    }
                    value={
                      formdata.report_content.vesselDistance.departure_passage
                    }
                    style={{ width: "100%" }}
                    name="departure_passage"
                    id="departure_passage"
                  >
                    <Select.Option value="Laden">Laden</Select.Option>
                    <Select.Option value="Ballast">Ballast</Select.Option>
                  </Select>
                </Col>
              </Row>







            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>

              <Row align="middle">
                <Col span={8}>
                  <label>Ordered SPd:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_ordered_spd"
                    id="departure_ordered_spd"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_ordered_spd
                    }
                  />
                </Col>
              </Row>








            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Reported spd:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_reported_spd"
                    id="departure_reported_spd"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_reported_spd
                    }
                  />
                </Col>
              </Row>

            </Col>
          </Row>



          <Row gutter={[80]}>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Observe distance:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_observe_dist"
                    id="departure_observe_dist"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_observe_dist
                    }
                  />
                </Col>
              </Row>


            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Engine dist. :</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_engine_dist"
                    id="departure_engine_dist"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_engine_dist
                    }
                  />

                </Col>
              </Row>

            </Col>



            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Main Eng Rev.:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_main_eng_rev"
                    id="departure_main_eng_rev"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_main_eng_rev
                    }
                  />

                </Col>
              </Row>


            </Col>




          </Row>


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Slip% :</label>

                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_slip_percentage"
                    id="departure_slip_percentage"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_slip_percentage
                    }
                  />
                </Col>
              </Row>

            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>




              <Row align="middle">
                <Col span={8}>
                  <label>Selenity:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_salinity"
                    id="departure_salinity"
                    value={
                      formdata.report_content.vesselDistance.departure_salinity
                    }
                  />

                </Col>
              </Row>






            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Density:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_density"
                    id="departure_density"
                    value={
                      formdata.report_content.vesselDistance.departure_density
                    }
                  />

                </Col>
              </Row>


            </Col>



          </Row>



          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Aver RPM:</label>
                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_aver_rpm"
                    id="departure_aver_rpm"
                    value={
                      formdata.report_content.vesselDistance.departure_aver_rpm
                    }
                  />
                </Col>
              </Row>




            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>

                  <label>Ave BHP:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_ave_bhp"
                    id="departure_ave_bhp"
                    value={
                      formdata.report_content.vesselDistance.departure_ave_bhp
                    }
                  />

                </Col>
              </Row>





            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>FWD dft:</label>

                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_fwd_dft"
                    id="departure_fwd_dft"
                    value={
                      formdata.report_content.vesselDistance.departure_fwd_dft
                    }
                  />
                </Col>
              </Row>




            </Col>


          </Row>

          <Row gutter={[80]}>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Aft drft:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_aft_drft"
                    id="departure_aft_drft"
                    value={
                      formdata.report_content.vesselDistance.departure_aft_drft
                    }
                  />

                </Col>
              </Row>

            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>

                  <label>Mid draft:</label>
                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_mid_draft"
                    id="departure_mid_draft"
                    value={
                      formdata.report_content.vesselDistance.departure_mid_draft
                    }
                  />
                </Col>
              </Row>


            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Steaming Hr:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_steaming_hr"
                    id="departure_steaming_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_steaming_hr
                    }
                  />

                </Col>
              </Row>




            </Col>
          </Row>


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Air press:</label>

                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_air_press"
                    id="departure_air_press"
                    value={
                      formdata.report_content.vesselDistance.departure_air_press
                    }
                  />
                </Col>
              </Row>




            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Air Tempt. :</label>

                </Col>
                <Col span={16}>

                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_air_tempt"
                    id="departure_air_tempt"
                    value={
                      formdata.report_content.vesselDistance.departure_air_tempt
                    }
                  />
                </Col>
              </Row>



            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Gen 1 Hr:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_one_hr"
                    id="departure_gen_one_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_one_hr
                    }
                  />

                </Col>
              </Row>



            </Col>
          </Row>




          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>
                  <label>Gen 2 Hr:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_two_hr"
                    id="departure_gen_two_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_two_hr
                    }
                  />

                </Col>
              </Row>





            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Gen 3 Hr:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_three_hr"
                    id="departure_gen_three_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_three_hr
                    }
                  />

                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Gen 4 Hr:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_four_hr"
                    id="departure_genFourHr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_four_hr
                    }
                  />

                </Col>
              </Row>




            </Col>

          </Row>
          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>M/E KWHR:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_me_kw_hr"
                    id="departure_me_kw_hr"
                    value={
                      formdata.report_content.vesselDistance.departure_me_kw_hr
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Gen 1 KWHR:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_one_kw_hr"
                    id="departure_gen_one_kw_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_one_kw_hr
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Gen 2 KWHR:</label>

                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_two_kw_hr"
                    id="departure_gen_two_kw_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_two_kw_hr
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Gen 3 KWHR:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_three_kw_hr"
                    id="departure_gen_three_kw_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_three_kw_hr
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Gen 4 KWHR:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_gen_four_kw_hr"
                    id="departure_gen_four_kw_hr"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_gen_four_kw_hr
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Main Engin Hr:</label>
                </Col>
                <Col span={16}>
                  <Input
                    style={{ height: "100%" }}
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_me_hr"
                    id="departure_me_hr"
                    value={
                      formdata.report_content.vesselDistance.departure_me_hr
                    }
                  />

                </Col>
              </Row>


            </Col>
            {/* <Col span={6}>
                <div className="form-group">
                  <label>Bad Weathr hrs:</label>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    value=""
                    name="departure_bad_wether_hrs"
                    id="departure_bad_wether_hrs"
                    data-section="vesselDistance"
                  />
                </div>
              </Col> */}
          </Row>




          <Row gutter={[80]}>
            {/* <Col span={6}>
                <div className="form-group">
                  <label>Bad weathr Dist:</label>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    value=""
                    name="departure_bad_wether_dist"
                    id="departure_bad_wether_dist"
                    data-section="vesselDistance"
                  />
                </div>
              </Col> */}
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Sea state:</label>

                </Col>
                <Col span={16}>

                  <Select
                    onChange={(value) =>
                      handleSelect(
                        value,
                        "vesselDistance",
                        "departure_sea_state"
                      )
                    }
                    value={
                      formdata.report_content.vesselDistance.departure_sea_state
                    }
                    style={{ width: "100%" }}
                    name="departure_sea_state"
                    id="departure_sea_state"
                  >
                    <Select.Option value="1">1</Select.Option>
                    <Select.Option value="2">2</Select.Option>
                    <Select.Option value="3">3</Select.Option>
                    <Select.Option value="4">4</Select.Option>
                    <Select.Option value="5">5</Select.Option>
                    <Select.Option value="6">6</Select.Option>
                    <Select.Option value="7">7</Select.Option>
                    <Select.Option value="8">8</Select.Option>
                    <Select.Option value="9">9</Select.Option>
                  </Select>
                </Col>
              </Row>




            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>









              <Row align="middle">
                <Col span={8}>
                  <label>Sea Direction:</label>

                </Col>
                <Col span={16}>
                  <Select
                    onChange={(value) =>
                      handleSelect(value, "vesselDistance", "departure_sea_dir")
                    }
                    value={
                      formdata.report_content.vesselDistance.departure_sea_dir
                    }
                    style={{ width: "100%" }}
                    name="departure_sea_dir"
                    id="departure_sea_dir"
                  >
                    <Select.Option value="North">North</Select.Option>
                    <Select.Option value="South">South</Select.Option>
                    <Select.Option value="East">East</Select.Option>
                    <Select.Option value="West">West</Select.Option>
                    <Select.Option value="North-East">North-East</Select.Option>
                    <Select.Option value="East-West">East-West</Select.Option>
                    <Select.Option value="North-West">North-West</Select.Option>
                    <Select.Option value="South-East">South-East</Select.Option>
                    <Select.Option value="South-West">South-West</Select.Option>
                  </Select>

                </Col>
              </Row>




            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Sea height:</label>
                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_sea_height
                    }
                    name="departure_sea_height"
                    id="departure_sea_height"
                    data-section="vesselDistance"
                  />

                </Col>
              </Row>




            </Col>
          </Row>




          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>

              <Row align="middle">
                <Col span={8}>
                  <label>Swell: </label>

                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    value={
                      formdata.report_content.vesselDistance.departure_swell
                    }
                    name="departure_swell"
                    id="departure_swell"
                    data-section="vesselDistance"
                  />

                </Col>
              </Row>



            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>


              <Row align="middle">
                <Col span={8}>

                  <label>Swell Direction:</label>
                </Col>
                <Col span={16}>
                  <Select
                    onChange={(value) =>
                      handleSelect(
                        value,
                        "vesselDistance",
                        "departure_swell_dir"
                      )
                    }
                    style={{ width: "100%" }}
                    name="departure_swell_dir"
                    id="departure_swell_dir"
                    value={
                      formdata.report_content.vesselDistance.departure_swell_dir
                    }
                  >
                    <Select.Option value="North">North</Select.Option>
                    <Select.Option value="South">South</Select.Option>
                    <Select.Option value="East">East</Select.Option>
                    <Select.Option value="West">West</Select.Option>
                    <Select.Option value="North-East">North-East</Select.Option>
                    <Select.Option value="East-West">East-West</Select.Option>
                    <Select.Option value="North-West">North-West</Select.Option>
                    <Select.Option value="South-East">South-East</Select.Option>
                    <Select.Option value="South-West">South-West</Select.Option>
                  </Select>

                </Col>
              </Row>

            </Col>


            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Swell Height:</label>

                </Col>
                <Col span={16}>

                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_swell_height"
                    id="departure_swell_height"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_swell_height
                    }
                  />
                </Col>
              </Row>



            </Col>
          </Row>





          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>

              <Row align="middle">
                <Col span={8}>
                  <label>Wind For: </label>

                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_wind_for"
                    id="departure_wind_for"
                    value={
                      formdata.report_content.vesselDistance.departure_wind_for
                    }
                  />

                </Col>
              </Row>



            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Wind dire:</label>

                </Col>
                <Col span={16}>
                  <Select
                    onChange={(value) =>
                      handleSelect(
                        value,
                        "vesselDistance",
                        "departure_wind_dir"
                      )
                    }
                    style={{ width: "100%" }}
                    name="departure_wind_dir"
                    id="departure_wind_dir"
                    value={
                      formdata.report_content.vesselDistance.departure_wind_dir
                    }
                  >
                    <Select.Option value="North">North</Select.Option>
                    <Select.Option value="South">South</Select.Option>
                    <Select.Option value="East">East</Select.Option>
                    <Select.Option value="West">West</Select.Option>
                    <Select.Option value="North-East">North-East</Select.Option>
                    <Select.Option value="East-West">East-West</Select.Option>
                    <Select.Option value="North-West">North-West</Select.Option>
                    <Select.Option value="South-East">South-East</Select.Option>
                    <Select.Option value="South-West">South-West</Select.Option>
                  </Select>

                </Col>
              </Row>





            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>



              <Row align="middle">
                <Col span={8}>
                  <label>Ordered M/E Cosumption</label>

                </Col>
                <Col span={16}>

                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_ordered_me_consumption"
                    id="departure_ordered_me_consumption"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_ordered_me_consumption
                    }
                  />
                </Col>
              </Row>





            </Col>
          </Row>


          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row align="middle">
                <Col span={8}>
                  <label>Boiler Hrs</label>
                </Col>
                <Col span={16}>
                  <Input
                    onChange={(e) => handleChange(e, "vesselDistance")}
                    type="number"
                    name="departure_noon_boiler_hrs"
                    id="departure_noon_boiler_hrs"
                    value={
                      formdata.report_content.vesselDistance
                        .departure_noon_boiler_hrs
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>

      <div className="rpbox">
        <h4 className="mainHeading">Bunker</h4>
        <div className="row">
          <div className="col-12">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="trrow">
                    <th>Type (MT)</th>
                    <th>IFO</th>
                    <th>LSFO</th>
                    <th>ULSFO</th>
                    <th>LSMGO</th>
                    <th>MDO</th>
                  </tr>
                </thead>
                <tbody>


                  <tr className="tableRow" >
                    <td className="required">BROB:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_brob_info"
                        id="departure_brob_info"
                        value={
                          formdata.report_content.bunker.departure_brob_info
                        }
                        className="form-control"
                      //   style="width: 80%;"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_brob_vlsfo"
                        value={
                          formdata.report_content.bunker.departure_brob_vlsfo
                        }
                        data-section="bunker"
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_brob_ulsfo"
                        id="departure_brob_ulsfo"
                        value={
                          formdata.report_content.bunker.departure_brob_ulsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_brob_lsmgo"
                        id="departure_brob_lsmgo"
                        value={
                          formdata.report_content.bunker.departure_brob_lsmgo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_brob_mgo"
                        id="departure_brob_mgo"
                        value={
                          formdata.report_content.bunker.departure_brob_mgo
                        }
                        className="form-control"

                      />
                    </td>
                  </tr>

                  <tr className="tableRow" >
                    <td className="required">M/E Propulsion:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_me_propulsion_info"
                        id="departure_me_propulsion_info"
                        value={
                          formdata.report_content.bunker
                            .departure_me_propulsion_info
                        }
                        className="form-control"

                        required
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_me_propulsion_vlsfo"
                        id="departure_me_propulsion_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_me_propulsion_vlsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_me_propulsion_ulsfo"
                        id="departure_me_propulsion_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_me_propulsion_ulsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_me_propulsion_lsmgo"
                        id="departure_me_propulsion_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_me_propulsion_lsmgo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_me_propulsion_mgo"
                        id="departure_me_propulsion_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_me_propulsion_mgo
                        }
                        className="form-control"

                      />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Boiler:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_boiler_info"
                        id="departure_boiler_info"
                        value={
                          formdata.report_content.bunker.departure_boiler_info
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_boiler_vlsfo"
                        id="departure_boiler_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_boiler_vlsfo
                        }
                        className="form-control"
                        tyle={{ width: "80%" }}
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_boiler_ulsfo"
                        id="departure_boiler_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_boiler_ulsfo
                        }
                        className="form-control"
                        tyle={{ width: "80%" }}
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_boiler_lsmgo"
                        id="departure_boiler_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_boiler_lsmgo
                        }
                        className="form-control"
                        tyle={{ width: "80%" }}
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_boiler_mgo"
                        id="departure_boiler_mgo"
                        value={
                          formdata.report_content.bunker.departure_boiler_mgo
                        }
                        className="form-control"
                        tyle={{ width: "80%" }}
                      />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Generator:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_generator_info"
                        id="departure_generator_info"
                        value={
                          formdata.report_content.bunker
                            .departure_generator_info
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_generator_vlsfo"
                        id="departure_generator_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_generator_vlsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_generator_ulsfo"
                        id="departure_generator_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_generator_ulsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_generator_lsmgo"
                        id="departure_generator_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_generator_lsmgo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_generator_mgo"
                        id="departure_generator_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_generator_mgo
                        }
                        className="form-control" />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>L/D:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_ld_info"
                        id="departure_ld_info"
                        value={
                          formdata.report_content.bunker.departure_ld_info
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_ld_vlsfo"
                        id="departure_ld_vlsfo"
                        value={
                          formdata.report_content.bunker.departure_ld_vlsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_ld_ulsfo"
                        id="departure_ld_ulsfo"
                        value={
                          formdata.report_content.bunker.departure_ld_ulsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_ld_lsmgo"
                        id="departure_ld_lsmgo"
                        value={
                          formdata.report_content.bunker.departure_ld_lsmgo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_ld_mgo"
                        id="departure_ld_mgo"
                        value={
                          formdata.report_content.bunker.departure_ld_mgo
                        }
                        className="form-control" />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Debllast:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_de_bllast_info"
                        id="departure_de_bllast_info"
                        value={
                          formdata.report_content.bunker
                            .departure_de_bllast_info
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_de_bllast_vlsfo"
                        id="departure_de_bllast_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_de_bllast_vlsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_de_bllast_ulsfo"
                        id="departure_de_bllast_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_de_bllast_ulsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_de_bllast_lsmgo"
                        id="departure_de_bllast_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_de_bllast_lsmgo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_de_bllast_mgo"
                        id="departure_de_bllast_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_de_bllast_mgo
                        }
                        className="form-control" />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Idle/.On:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_idle_info"
                        id="departure_idle_info"
                        value={
                          formdata.report_content.bunker.departure_idle_info
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_idle_vlsfo"
                        id="departure_idle_vlsfo"
                        value={
                          formdata.report_content.bunker.departure_idle_vlsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_idle_ulsfo"
                        id="departure_idle_ulsfo"
                        value={
                          formdata.report_content.bunker.departure_idle_ulsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_idle_lsmgo"
                        id="departure_idle_lsmgo"
                        value={
                          formdata.report_content.bunker.departure_idle_lsmgo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_idle_mgo"
                        id="departure_idle_mgo"
                        value={
                          formdata.report_content.bunker.departure_idle_mgo
                        }
                        className="form-control" />
                    </td>
                  </tr>


                  <tr className="tableRow">
                    <td>Heat:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_heat_info"
                        id="departure_heat_info"
                        value={
                          formdata.report_content.bunker.departure_heat_info
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_heat_vlsfo"
                        id="departure_heat_vlsfo"
                        value={
                          formdata.report_content.bunker.departure_heat_vlsfo
                        }
                        className="form-control" />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_heat_ulsfo"
                        id="departure_heat_ulsfo"
                        value={
                          formdata.report_content.bunker.departure_heat_ulsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_heat_lsmgo"
                        id="departure_heat_lsmgo"
                        value={
                          formdata.report_content.bunker.departure_heat_lsmgo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_heat_mgo"
                        id="departure_heat_mgo"
                        value={
                          formdata.report_content.bunker.departure_heat_mgo
                        }
                        className="form-control"

                      />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Fuel Received:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_receoved_info"
                        id="departure_fuel_receoved_info"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_receoved_info
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_receoved_vlsfo"
                        id="departure_fuel_receoved_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_receoved_vlsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_receoved_ulsfo"
                        id="departure_fuel_receoved_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_receoved_ulsfo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_receoved_lsmgo"
                        id="departure_fuel_receoved_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_receoved_lsmgo
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_receoved_mgo"
                        id="departure_fuel_receoved_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_receoved_mgo
                        }
                        className="form-control"

                      />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Fuel Debunker:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_debunker_info"
                        id="departure_fuel_debunker_info"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_debunker_info
                        }
                        className="form-control"

                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_debunker_vlsfo"
                        id="departure_fuel_debunker_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_debunker_vlsfo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_debunker_ulsfo"
                        id="departure_fuel_debunker_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_debunker_ulsfo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_debunker_lsmgo"
                        id="departure_fuel_debunker_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_debunker_lsmgo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_fuel_debunker_mgo"
                        id="departure_fuel_debunker_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_fuel_debunker_mgo
                        }
                        className="form-control"
                      />
                    </td>
                  </tr>

                  <tr className="tableRow">
                    <td>Aux Exngine:</td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_aux_exngine_info"
                        id="departure_aux_exngine_info"
                        value={
                          formdata.report_content.bunker
                            .departure_aux_exngine_info
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_aux_exngine_vlsfo"
                        id="departure_aux_exngine_vlsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_aux_exngine_vlsfo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_aux_engine_ulsfo"
                        id="departure_aux_engine_ulsfo"
                        value={
                          formdata.report_content.bunker
                            .departure_aux_engine_ulsfo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_aux_exngine_lsmgo"
                        id="departure_aux_exngine_lsmgo"
                        value={
                          formdata.report_content.bunker
                            .departure_aux_exngine_lsmgo
                        }
                        className="form-control"
                      />
                    </td>
                    <td>
                      <Input
                        onChange={(e) => handleChange(e, "bunker")}
                        type="text"
                        name="departure_aux_exngine_mgo"
                        id="departure_aux_exngine_mgo"
                        value={
                          formdata.report_content.bunker
                            .departure_aux_exngine_mgo
                        }
                        className="form-control"
                      />
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="rpbox" style={{ gap: "2rem" }}>
        <Row gutter={[24, 24]}>
          <Col span={2}><label >Remark:</label></Col>
          <Col span={22}>
          <Input
            onChange={(e) => handleChange(e, "remark")}
            name="departure_remark"
            id="departure_remark"
            value={formdata.report_content?.remark?.departure_remark}
          />
          </Col>
        </Row>
        <Row gutter={[24, 24]}>
          <Col span={12}>
            <Row gutter={8}>
              <Col span={4}><label>Captain:</label></Col>
              <Col span={20}>
              <Input
                  onChange={(e) => handleChange(e, "remark")}
                  type="text"
                  name="departure_captain"
                  id="departure_captain"
                  value={formdata.report_content?.remark?.departure_captain}
                />
              </Col>
            </Row>
          </Col>

          <Col span={12}>
            <Row gutter={8}>

              <Col span={4}><label>C/E:</label></Col>

              <Col span={20}>
              <Input
                  onChange={(e) => handleChange(e, "remark")}
                  type="text"
                  name="departure_ce"
                  id="departure_ce"
                  value={formdata.report_content?.remark?.departure_captain}
             
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DepartureReport;
