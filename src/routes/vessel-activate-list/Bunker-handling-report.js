import React, { useEffect, useState } from "react";
import "./noonReport.css";
import { SaveOutlined } from "@ant-design/icons";
import { Button, Flex, Input, Row, Col, DatePicker, Space, Select, Modal } from "antd";
import URL_WITH_VERSION, {
  URL_WITHOUT_VERSION,
  getAPICall,
  postAPICall,
} from "../../shared";
import { useLocation, useParams } from "react-router-dom";
import dayjs from "dayjs";
import PortSelection from "../port-selection/PortSelection";

const { Option } = Select;
const BunkerHandlingReport = (props) => {
  // console.log("bunker handle props", props);
  const [isPortModal, setPortModal] = useState(false);
  const [formData, setFormData] = useState({
    report_content: {
      bunkerHandling: {
        bunker_vessel: "",
        bunker_imo: "",
        bunker_call_sign: "",
        bunker_time_zone: "",
        bunker_port: "",
        bunker_port_etd: "",
        bunker_voyage_no: "",
        bunker_supplier: "",
        bunker_bunkering_start: "",
        bunker_hose_con: "",
        bunker_hose_dis: "",
        bunker_bunkering_Co: "",
        bunker_report_date: "",
      },
      fuelType: {
        bunker_ifo: "",
        bunker_vlsfo: "",
        bunker_ulsfo: "",
        bunker_mgo: "",
        bunker_lsmgo: "",
      },
      remark: {
        bunker_remark: "",
        bunker_captain: "",
        bunker_ce: "",
      },
    },
  });

  const modalCloseEvent = (data) => {
    console.log(data);
    setFormData((prev) => ({
      ...prev,
      report_content: {
        ...prev.report_content,
        bunkerHandling: {
          ...prev.report_content.bunkerHandling,
          bunker_port: data.port.port_name,
        }
      }
    }))

    setPortModal(false)

  };
  const handleChange = (e, group) => {
    const { name, value } = e.target;

    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [name]: value,
        },
      },
    }));
  };

  const handleSelect = (value, group, field) => {
    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: value,
        },
      },
    }));
  };

  const handleSelectDate = (date, dateString, group, field) => {
    setFormData((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: dayjs(date, "YYYY-MM-DD HH:mm:ss"),
        },
      },
    }));
  };

  const { vessel_name, voyage_number } = useParams();
  const location = useLocation();

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const urlVesselName = params.get("vessel_name");
    const urlVoyageNumber = params.get("voyage_number");
    //  console.log({ 'aaaaa',urlVesselName,urlVoyageNumber})

    const getFormInfo = async () => {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/voyage-data?ae=${urlVoyageNumber}`
      );
      const result = await response.data;

      let obj = result[0];
      const { imo_no } = result[0];

      setFormData((prevState) => ({
        ...prevState,
        report_content: {
          ...prevState.report_content,
          bunkerHandling: {
            ...prevState.report_content.bunkerHandling,
            bunker_vessel: obj.vessel_name,
            bunker_imo: obj.imo_no,
            bunker_voyage_no: obj.estimate_id,
          },
        },
      }));
    };

    if (props.byEdit == true && props.reportNo) {
      getEditData();
    } else {
      getFormInfo();
    }
  }, []);

  const getEditData = async (id) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/vessel/edit-bunker-report?ae=${props.reportNo}`
    );
    let data = await response.data;

    setFormData((prevState) => ({
      ...prevState,
      // report_content:Object.assign({}, data.report_content.report_content)
      report_content: {
        ...data.report_content,
      },
      status: data.status,
    }));

    //setFormData(data)
  };
  const saveFormData = async (postData) => {
    let _url = "save";
    let _method = "post";

    if (props.byEdit == true) {
      _url = "update";
      _method = "put";
    }

    if (_url == "update") {
      postData.id = props.reportNo;
    }

    let suURL = `${URL_WITH_VERSION}/voyage-manager/vessel/${_url}-bunker-report`;

    // let suMethod = "POST";
    postAPICall(suURL, postData, _method, (data) => {
      if (data && data.data) {
        // openNotificationWithIcon("success", data.message);
        // if (vessel_form_id) {
        //   this.setState({ loadForm: false });
        //   this.props.onClose();
        // }
        //  getEditData(data.row.rid);
      } else {
        // openNotificationWithIcon("error", data.message);
      }
    });
  };

  return (
    <div className="parentBox">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <img
          src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
          width={150}
          alt="logo"
          style={{ marginLeft: "1rem" }}
        ></img>

        <div style={{ display: "flex", gap: "2rem" }}>
          <Button
            onClick={() => saveFormData(formData)}
            type="primary"
            style={{ padding: "4px 24px" }}
          >
            {/* <SaveOutlined style={{}} /> */}
            Send Report
          </Button>



        </div>

        {/* <h1
            style={{
              textAlign: "center",
              fontSize: "1.72rem",
              marginLeft: "30%",
            }}
          >
            Maritime is Our kingdom.
          </h1> */}
      </div>



      <div className="rpbox">

        <Row>
          <h4 className="mainHeading" >Bunker Handling</h4>
        </Row>
        <div

          style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
        >
          <Row gutter={[80]}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={2}>
                <Col span={8}>
                  <label className="required">Vessel:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Vessel"
                    name="bunker_vessel"
                    value={formData?.report_content?.bunkerHandling?.bunker_vessel}
                    onChange={(e) => handleChange(e, "bunkerHandling")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label className="required">IMO:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Basic usage"
                    name="bunker_imo"
                    value={formData?.report_content?.bunkerHandling?.bunker_imo}
                    onChange={(e) => handleChange(e, "bunkerHandling")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Call Sign:</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Basic usage"
                    name="bunker_call_sign"
                    value={
                      formData?.report_content?.bunkerHandling?.bunker_call_sign
                    }
                    onChange={(e) => handleChange(e, "bunkerHandling")}
                  />
                </Col>
              </Row>
            </Col>
          </Row>


          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Time Zone:</label>
                </Col>
                <Col span={16}>
                  <Row gutter={1}>
                    <Col span={8}>
                      <Select

                        defaultValue="GMT"
                        onChange={(value) => {
                          handleSelect(value, "bunkerHandling", "bunker_time_zone");
                        }}
                      >
                        <Option value="GMT">GMT</Option>
                        <Option value="Local">Local</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Select

                        defaultValue="+"
                        onChange={(value) => {
                          handleSelect(value, "bunkerHandling", "bunker_time_zone");
                        }}
                      >
                        <Option value="+">+</Option>
                        <Option value="-">-</Option>
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Input
                        style={{ height: "100%" }}
                        placeholder="Basic usage"
                        name="bunker_time_zone"
                        onChange={(e) => handleChange(e, "bunkerHandling")}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Port:</label>
                </Col>
                <Col span={16} >
                  <Input
                    onClick={() => {
                      setPortModal(true)
                    }}
                    type="text"
                    name="bunker_port"
                    id="bunker_port"

                    value={formData.report_content.bunkerHandling.bunker_port}
                    // onChange={(value) => {
                    //   handleChange(value, "portInformation");
                    // }}
                    style={{ height: "100%" }}
                    placeholder="Port"
                  />
                  {
                    isPortModal && (
                      <Modal
                        title="Select Port"
                        open={isPortModal}
                        width="80%"
                        onCancel={() => {
                          setPortModal(false);
                        }}
                        footer={null}
                      >
                        <PortSelection
                          fromPortID={null}
                          modalCloseEvent={(data) => {
                            modalCloseEvent(data);
                          }}
                        />
                      </Modal>
                    )
                  }
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Port ETD:</label>
                </Col>
                <Col span={16}>

                  <Space direction="vertical" style={{ width: '100%' }}>
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      name="bunker_port_etd"
                      value={
                        formData.report_content.bunkerHandling?.bunker_port_etd
                          ? dayjs(
                            formData.report_content.bunkerHandling
                              ?.bunker_port_etd,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }

                      style={{ width: '100%' }}
                      onChange={(date, dateString) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "bunkerHandling",
                          "bunker_port_etd"
                        )
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Voyage No. :</label>
                </Col>
                <Col span={16}>
                  <Input
                    placeholder="Basic usage"
                    name="bunker_voyage_no"
                    value={
                      formData.report_content?.bunkerHandling?.bunker_voyage_no
                    }
                    onChange={(e) => handleChange(e, "bunkerHandling")}
                    disabled
                  />
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Supplier:</label>
                </Col>
                <Col span={16} >
                  <select
                    className="form-control"
                    onChange={(e) => handleChange(e, "bunkerHandling")}

                  >
                    <option>Select</option>
                    <option>Option 1</option>
                    <option>Option2</option>
                  </select>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Bunkering Start:</label>
                </Col>
                <Col span={16}>
                  <Space direction="vertical" style={{ width: '100%' }}>
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      name="bunker_bunkering_start"
                      value={
                        formData.report_content.bunkerHandling
                          ?.bunker_bunkering_start
                          ? dayjs(
                            formData.report_content.bunkerHandling
                              ?.bunker_bunkering_start,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                      style={{ width: "100%" }}
                      onChange={(date, dateString) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "bunkerHandling",
                          "bunker_bunkering_start"
                        )
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </Col>
          </Row>


          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label>Hose con. :</label>
                </Col>
                <Col span={16}>
                  <Space direction="vertical" style={{ width: '100%' }}>
                    <DatePicker

                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      name="bunker_hose_con"
                      value={
                        formData.report_content.bunkerHandling?.bunker_hose_con
                          ? dayjs(
                            formData.report_content.bunkerHandling
                              ?.bunker_hose_con,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                      style={{ width: "100%" }}
                      onChange={(date, dateString) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "bunkerHandling",
                          "bunker_hose_con"
                        )
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={8}>
                <Col span={8}>
                  <label>Hose dis.:</label>
                </Col>
                <Col span={16} >
                  <Space direction="vertical" style={{ width: '100%' }} >
                    <DatePicker

                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      name="bunker_hose_dis"
                      value={
                        formData.report_content.bunkerHandling?.bunker_hose_dis
                          ? dayjs(
                            formData.report_content.bunkerHandling
                              ?.bunker_hose_dis,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                      style={{ width: "100%" }}
                      onChange={(date, dateString) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "bunkerHandling",
                          "bunker_hose_dis"
                        )
                      }
                    />
                  </Space>
                </Col>
              </Row>
            </Col>

            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row>
                <Col span={8}>
                  <label>Bunkering Com. :</label>
                </Col>
                <Col span={16}>
                  <Space direction="vertical" style={{ width: '100%' }}>
                    <Space direction="vertical" style={{ width: '100%' }}>
                      {/* <DatePicker onChange={(e) =>handleChange(e, 'bunkerHandling')}/> */}
                      <DatePicker
                        showTime
                        format="YYYY-MM-DD HH:mm:ss"
                        name="bunker_bunkering_Co"
                        value={
                          formData.report_content.bunkerHandling?.bunker_bunkering_Co
                            ? dayjs(
                              formData.report_content.bunkerHandling
                                ?.bunker_bunkering_Co,
                              "YYYY-MM-DD HH:mm:ss"
                            )
                            : undefined
                        }
                        style={{ width: "100%" }}
                        onChange={(date, dateString) =>
                          handleSelectDate(
                            date,
                            dateString,
                            "bunkerHandling",
                            "bunker_bunkering_Co"
                          )
                        }
                      />
                    </Space>
                  </Space>
                </Col>
              </Row>
            </Col>
          </Row>


          <Row gutter={80}>
            <Col span={8} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={16}>
                <Col span={8}>
                  <label className="required">Date</label>
                </Col>
                <Col span={16}>
                  <DatePicker
                    showTime
                    format="YYYY-MM-DD HH:mm:ss"
                    name="bunker_report_date"
                    value={
                      formData.report_content.bunkerHandling?.bunker_report_date
                        ? dayjs(
                          formData.report_content.bunkerHandling
                            ?.bunker_report_date,
                          "YYYY-MM-DD HH:mm:ss"
                        )
                        : undefined
                    }
                    id="bunker_report_date"
                    style={{ width: "100%" }}
                    onChange={(date, dateString) => {
                      // handleSelectDate(
                      //   date,
                      //   dateString,
                      //   "bunkerHandling",
                      //   "bunker_report_date"
                      // );
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
      <div className="rpbox">
        <div className="row">
          <div className="col-12">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="trrow">
                    <th>Fuel Type</th>
                    <th>Quantity Lifted</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="tableRow">
                    <td >
                      {/* <Select
                        onChange={(value) =>
                          handleSelectChange(value, "bunker_ifo", "fuelType")
                        }
                        defaultValue="IFO"
                        style={{ width: "70%" }}
                      >
                        <Option value="IFO">IFO</Option>
                        <Option value="Option1">Option 1</Option>
                        <Option value="Option2">Option 2</Option>
                      </Select> */}

                      <label>IFO:</label>
                    </td>
                    <td>

                      <Input
                        type="number"
                        placeholder="0"
                        name="bunker_ifo"
                        value={formData.report_content.fuelType.bunker_ifo}
                        onChange={(e) =>
                          handleChange(
                            e,

                            "fuelType"
                          )
                        }
                      />

                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>
                      <label>VLSFO:</label>
                    </td>
                    <td>
                      <Input
                        type="number"
                        placeholder="0"
                        name="bunker_vlsfo"
                        value={formData.report_content.fuelType.bunker_vlsfo}
                        onChange={(e) =>
                          handleChange(
                            e,

                            "fuelType"
                          )
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    <td>
                      {/* <Select
                        onChange={(value) =>
                          handleSelectChange(value, "bunker_ulsfo", "fuelType")
                        }
                        defaultValue="ULSFO"
                        style={{ width: "70%" }}
                      >
                        <Option value="ULSFO">ULSFO</Option>
                        <Option value="Option1">Option 1</Option>
                        <Option value="Option2">Option 2</Option>
                      </Select> */}
                      <label>ULSFO:</label>
                    </td>
                    <td>
                      <Input
                        type="number"
                        placeholder="0"
                        name="bunker_ulsfo"
                        value={formData.report_content.fuelType.bunker_ulsfo}
                        onChange={(e) =>
                          handleChange(
                            e,

                            "fuelType"
                          )
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    {/* <td>
                      <Select
                        onChange={(value) =>
                          handleSelectChange(value, "bunker_mgo", "fuelType")
                        }
                        defaultValue="MGO"
                        style={{ width: "70%" }}
                      >
                        <Option value="MGO">MGO</Option>
                        <label>Hose dis.:</label>
                        <Option value="Option1">Option 1</Option>
                        <Option value="Option2">Option 2</Option>
                      </Select>
                    </td> */}
                    <td><label>MGO</label></td>
                    <td>
                      <Input
                        type="number"
                        placeholder="0"
                        name="bunker_mgo"
                        value={formData.report_content.fuelType.bunker_mgo}
                        onChange={(e) =>
                          handleChange(
                            e,

                            "fuelType"
                          )
                        }
                      />
                    </td>
                  </tr>
                  <tr className="tableRow">
                    {/* <td>
                      <Select
                        onChange={(value) =>
                          handleSelectChange(value, "bunker_lsmgo", "fuelType")
                        }
                        defaultValue="LSMGO"
                        style={{ width: "70%" }}
                      >
                        <Option value="LSMGO">LSMGO</Option>
                        <Option value="Option1">Option 1</Option>
                        <Option value="Option2">Option 2</Option>
                      </Select>
                    </td> */}
                    <td><label>LSMGO</label></td>
                    <td>
                      <Input
                        type="number"
                        name="bunker_lsmgo"
                        value={formData.report_content.fuelType.bunker_lsmgo}
                        placeholder="0"
                        onChange={(e) =>
                          handleChange(
                            e,

                            "fuelType"
                          )
                        }
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="rpbox" style={{ gap: "2rem" }}>
        <Row gutter={[24, 24]}>
          <Col span={2}><label >Remark:</label></Col>
          <Col span={22}>
            <Input
              placeholder="Remark"
              name="bunker_remark"
              value={formData.report_content.remark.bunker_remark}
              onChange={(e) => handleChange(e, "remark")}
            />
          </Col>
        </Row>
        <Row gutter={[24, 24]}>
          <Col span={12}>
            <Row gutter={8}>
              <Col span={4}><label>Captain:</label></Col>
              <Col span={20}>
                <Input
                  placeholder="Captain"
                  name="bunker_captain"
                  value={formData.report_content.remark.bunker_captain}
                  onChange={(e) => handleChange(e, "remark")}
                />
              </Col>
            </Row>
          </Col>

          <Col span={12}>
            <Row gutter={8}>

              <Col span={4}><label>C/E:</label></Col>

              <Col span={20}>
                <Input
                  placeholder="C/E"
                  name="bunker_ce"
                  value={formData.report_content.remark.bunker_ce}
                  onChange={(e) => handleChange(e, "remark")}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default BunkerHandlingReport;
