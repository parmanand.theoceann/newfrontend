import React, { useEffect, useState } from "react";
import "./noonReport.css";
import { Input, Row, Col, Select, DatePicker, Button, Modal } from "antd";
import { useParams } from "react-router-dom";
import { useLocation } from "react-router-dom";
import URL_WITH_VERSION, { getAPICall, postAPICall } from "../../shared";
import moment from "moment";
import dayjs from "dayjs";
import PortSelection from "../port-selection/PortSelection";
const { Option } = Select;

const ArrivalReport = (props) => {
  const [ports, setPorts] = useState(null);
  const { vessel_name, voyage_number } = useParams();
  const location = useLocation();
  const [formdata, setFormdata] = useState({
    report_content: {
      mainInformation: {
        arrival_vessel: "",
        arrival_imo: "",
        arrival_call_sign: "",
        arrival_time_zone: "",
        arrival_time_zone_plus: "",
        arrival_9h: "",
        arrival_end_of_sea_passage: "",
        arrival_main_lat: "",
        arrival_lat_dir: "",
        arrival_main_long: "",
        arrival_main_long_dir: "",
        arrival_voyage_no: "",
        arrivl_report_date: "",
      },
      portInformation: {
        arrival_port: "",
        arrival_port_id: "",
        eosp_date_time: "",
        arrival_port_lat: "",
        arrival_port_lat_dir: "",
        arrival_port_long: "",
        arrival_port_long_dir: "",
        arrival_next_port: "",
        arrival_next_port_id: "",
        anchor_dropped_date_time: "",
      },
      cargoInformation: {
        arrival_ttl_cargo_onboard: "",
        arrival_hold_cargo_mt: "",
        arrival_slop_rob: "",
        arrival_displacement_mt: "",
        arrival_fw_consumed: "",
        arrival_slop_produced: "",
        arrival_deck_cargo_mt: "",
        arrival_fw_rob: "",
        arrival_ballast_water: "",
        arrival_cleaning_chemical_rob: "",
        arrival_cargo_ops_start: "",
        arrival_cargo_ops_end: "",
        arrival_type_of_operation: "",
        departure_cargo_loaded: "",
        departure_cargo_discharg: "",
        departure_distill_water_rob: "",
      },
      vesselDistance: {
        arrival_Passage: "",
        arrival_ordered_spd: "",
        arrival_reported_spd: "",
        arrival_observe_dist: "",
        arrival_engine_dist: "",
        arrival_main_eng_rev: "",
        arrival_slip_perc: "",
        arrival_salinity: "",
        arrival_density: "",
        arrival_aver_rpm: "",
        arrival_ave_bhp: "",

        arrival_fwd_dft: "",
        arrival_aft_drft: "",
        arrival_mid_draft: "",
        arrival_total_steaming_hr: "",
        arrival_air_press: "",
        arrival_air_tempt: "",
        arrival_gen_one_hr: "",
        arrival_gen_two_hr: "",
        arrival_gen_three_hr: "",
        arrival_gen_four_hr: "",
        arrival_boiler_running_hrs: "",

        arrival_me_hr: "",
        arrival_gen_one_kw_hr: "",
        arrival_gen_two_kw_hr: "",
        arrival_gen_three_kw_hr: "",
        arrival_gen_four_kw_hr: "",
        arrival_me_kw_hr: "",
        arrival_bad_wether_hrs: "",
        arrival_bad_wether_dist: "",
        arrival_sea_state: "",
        arrival_sea_dir: "",
        arrival_sea_height: "",
        arrival_swell: "",
        arrival_swell_dir: "",
        arrival_swell_height: "",
        arrival_wind_for: "",
        arrival_wind_dir: "",
        arrial_aver_speed: "",
        arrival_sea_surface_temp: "",
        arrival_total_miles_covered: "",
        arrival_consum_fd_main: "",
        arrival_consum_fd_aux: "",
        arrival_consum_boiler: "",
      },
      bunker: {
        arrival_brob_info: "",
        arrival_brob_vlsfo: "",
        arrival_brob_ulsfo: "",
        arrival_brob_lsmgo: "",
        arrival_brob_mgo: "",
        arrival_me_propulsion_info: "",
        arrival_me_propulsion_vlsfo: "",
        arrival_me_propulsion_ulsfo: "",
        arrival_me_propulsion_lsmgo: "",
        arrival_me_propulsion_mgo: "",
        arrival_boiler_info: "",
        arrival_boiler_vlsfo: "",
        arrival_boiler_ulsfo: "",
        arrival_boiler_lsmgo: "",
        arrival_boiler_mgo: "",
        arrival_generator_info: "",
        arrival_generator_vlsfo: "",
        arrival_generator_ulsfo: "",
        arrival_generator_lsmgo: "",
        arrival_generator_mgo: "",
        arrival_fuelReceoved_info: "",
        arrival_fuel_receoved_vlsfo: "",
        arrival_fuel_receoved_ulsfo: "",
        arrival_fuel_receoved_lsmgo: "",
        arrival_fuel_receoved_mgo: "",
        arrival_fuel_debunker_info: "",
        arrival_fuel_debunker_vlsfo: "",
        arrival_fuel_debunker_ulsfo: "",
        arrival_fuel_debunker_lsmgo: "",
        arrival_fuel_debunker_mgo: "",
        arrival_aux_exngine_info: "",
        arrival_aux_exngine_vlsfo: "",
        arrival_aux_exngine_ulsfo: "",
        arrival_aux_exngine_lsmgo: "",
        arrival_aux_exngine_mgo: "",
      },
      remark: {
        arrival_remark: "",
        arrival_captain: "",
        arrival_ce: "",
      },
    },
  });
  const [isPortModal, setPortModal] = useState(false);
  const [key, setKey] = useState("");

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const urlVesselName = params.get("vessel_name");
    const urlVoyageNumber = params.get("voyage_number");
    const getFormInfo = async () => {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/voyage-data?ae=${urlVoyageNumber}`
      );

      const data = await response.data;
      let obj = data[0];

      setFormdata((prevState) => ({
        ...prevState,
        report_content: {
          ...prevState.report_content,
          mainInformation: {
            ...prevState.report_content.mainInformation,
            arrival_vessel: obj.vessel_name,
            arrival_imo: obj.imo_no,
            arrival_voyage_no: obj.estimate_id,
          },
        },
      }));

      const portOptions = ports?.map(({ port, port_id }) => ({
        label: port,
        value: port,
        key: port_id,
      }));
      setPorts(portOptions);
    };

    if (props.byEdit == true) {
      getEditData();
    } else {
      getFormInfo();
    }
  }, []);

  const modalCloseEvent = (data) => {
    console.log(data);
    if (key == "arrival_next_port") {
      setFormdata((prev) => ({
        ...prev,
        report_content: {
          ...prev.report_content,
          portInformation: {
            ...prev.report_content.portInformation,
            arrival_next_port: data.port.port_name,
            arrival_next_port_id: data.port.PortID,
          },
        },
      }));
    }
    if (key === "arrival_port") {
      setFormdata((prev) => ({
        ...prev,
        report_content: {
          ...prev.report_content,
          portInformation: {
            ...prev.report_content.portInformation,
            arrival_port: data.port.port_name,
            arrival_port_id: data.port.PortID,
          },
        },
      }));
    }

    setPortModal(false);
  };

  const handleChange = (e, group) => {
    console.log("group", group);
    const { name, value } = e.target;
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [name]: value,
        },
      },
    }));
  };

  const handleSelect = (value, group, field) => {
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: value,
        },
      },
    }));
  };

  const handleSelectDate = (date, dateString, group, field) => {
    setFormdata((prevState) => ({
      ...prevState,
      report_content: {
        ...prevState.report_content,
        [group]: {
          ...prevState.report_content[group],
          [field]: dayjs(date, "YYYY-MM-DD HH:mm:ss"),
        },
      },
    }));
  };

  const getEditData = async (id) => {
    let response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/vessel/edit-arrival-report?ae=${props.reportNo}`
    );
    let data = await response.data;

    setFormdata((prevState) => ({
      ...prevState,

      report_content: {
        ...data.report_content,
      },
      status: data.status,
    }));
  };

  const saveFormData = async (postData) => {
    // console.log('postData 1 :', postData);

    let _url = "save";
    let _method = "post";

    if (props.byEdit == true) {
      _url = "update";
      _method = "put";
    }

    if (_url == "update") {
      postData.id = props.reportNo;
    }

    let suURL = `${URL_WITH_VERSION}/voyage-manager/vessel/${_url}-arrival-report`;
    let suMethod = "POST";
    postAPICall(suURL, postData, suMethod, (data) => {
      // console.log('postData 2 :', postData);
      // console.log('data :', data);
      if (data && data.data) {
      } else {
        // openNotificationWithIcon("error", data.message);
      }
    });
  };

  const portSelectionHandler = (value, group, field, currOption) => {
    if (field === "arrival_port") {
      setFormdata((prev) => ({
        ...prev,
        portInformation: {
          ...prev.portInformation,
          [field]: value,
          arrival_port_id: currOption.key,
        },
      }));
    } else {
      setFormdata((prev) => ({
        ...prev,
        portInformation: {
          ...prev.portInformation,
          [field]: value,
          arrival_next_port_id: currOption.key,
        },
      }));
    }
  };

  const portModal = () => {
    console.log("clicked port");
    setPortModal(true);
  };

  return (
    <div className="parentBox">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <img
          src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
          width={150}
          alt="logo"
          style={{ marginLeft: "1rem" }}
        ></img>
        <Button type="primary" onClick={() => saveFormData(formdata)}>
          Send Report
        </Button>
      </div>

        {/* <div className="rpox"> */}

        <div className="rpbox">
          <Row>
            <h4 className="mainHeading">Main Information</h4>
          </Row>

          <div
            style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
          >
            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label className="required">Vessel:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      value={
                        formdata.report_content.mainInformation.arrival_vessel
                      }
                      name="arrival_vessel"
                      id="arrival_vessel"
                      required
                      onChange={(e) => {
                        handleChange(e, "mainInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label className="required">IMO:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_imo"
                      value={
                        formdata.report_content.mainInformation.arrival_imo
                      }
                      id="arrival_imo"
                      required
                      onChange={(e) => {
                        handleChange(e, "mainInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Call Sign:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_call_sign"
                      id="arrival_call_sign"
                      onChange={(e) => {
                        handleChange(e, "mainInformation");
                      }}
                      value={
                        formdata.report_content.mainInformation
                          .arrival_call_sign
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Time Zone:</label>
                  </Col>
                  <Col span={14}>
                    <Row gutter={2}>
                      <Col span={8}>
                        <Select
                          defaultValue="GMT"
                          name="arrival_time_zone"
                          id="arrival_time_zone"
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "mainInformation",
                              "arrival_time_zone"
                            );
                          }}
                          value={
                            formdata.report_content.mainInformation
                              .arrival_time_zone
                          }
                        >
                          <Select.Option value="GMT">GMT</Select.Option>
                          <Select.Option value="Local">Local</Select.Option>
                        </Select>
                      </Col>
                      <Col span={8}>
                        <Select
                          name="arrival_time_zone_plus"
                          id="arrival_time_zone_plus"
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "mainInformation",
                              "arrival_time_zone_plus"
                            );
                          }}
                          value={
                            formdata.report_content.mainInformation
                              .arrival_time_zone_plus
                          }
                        >
                          <Select.Option value="+">+</Select.Option>
                          <Select.Option value="-">-</Select.Option>
                        </Select>
                      </Col>
                      <Col span={8}>
                        <Input
                          type="text"
                          name="arrival_9h"
                          id="arrival_9h"
                          style={{ height: "100%" }}
                          onChange={(e) => handleChange(e, "mainInformation")}
                          value={
                            formdata.report_content.mainInformation.arrival_9h
                          }
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>End of sea Passage:</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                      style={{ width: "100%" }}
                      onChange={(date, dateString) => {
                        handleSelectDate(
                          date,
                          dateString,
                          "mainInformation",
                          "arrival_end_of_sea_passage"
                        );
                      }}
                      // format="YYYY-MM-DD HH:mm:ss"
                      name="arrival_end_of_sea_passage"
                      id="arrival_end_of_sea_passage"
                      showTime="true"
                      value={
                        formdata.report_content.mainInformation
                          .arrival_end_of_sea_passage
                          ? dayjs(
                            formdata.report_content.mainInformation
                              .arrival_end_of_sea_passage,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    // value={moment(formatted_endofseapassage).format('YYYY-MM-DD HH:mm')}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Lat. :</label>
                  </Col>
                  <Col span={14}>
                    <Row gutter={8}>
                      <Col span={12}>
                        <Input
                          type="text"
                          name="arrival_main_lat"
                          id="arrival_main_lat"
                          style={{ height: "100%" }}
                          placeholder="DDMMSS"
                          onChange={(value) =>
                            handleChange(value, "mainInformation")
                          }
                          value={
                            formdata.report_content.mainInformation
                              .arrival_main_lat
                          }
                        />
                      </Col>
                      <Col span={12}>
                        <Select
                          defaultValue="N"
                          name="arrival_lat_dir"
                          id="arrival_lat_dir"
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "mainInformation",
                              "arrival_lat_dir"
                            );
                          }}
                          value={
                            formdata.report_content.mainInformation
                              .arrival_lat_dir
                          }
                        >
                          <Select.Option value="N">N</Select.Option>
                          <Select.Option value="S">S</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Long. :</label>
                  </Col>
                  <Col span={14}>
                    <Row gutter={2}>
                      <Col span={12}>
                        <Input
                          type="text"
                          name="arrival_main_long"
                          id="arrival_main_long"
                          style={{ height: "100%" }}
                          placeholder="DDMMSS"
                          onChange={(e) => {
                            handleChange(e, "mainInformation");
                          }}
                          value={
                            formdata.report_content.mainInformation.arrival_main_long
                          }
                        />
                      </Col>
                      <Col span={12}>
                        <Select
                          defaultValue="E"
                          name="arrival_main_long_dir"
                          id="arrival_main_long_dir"
                          style={{ flexBasis: "40%" }}
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "mainInformation",
                              "arrival_main_long_dir"
                            );
                          }}
                          value={
                            formdata.report_content.mainInformation
                              .arrival_main_long_dir
                          }
                        >
                          <Select.Option value="E">E</Select.Option>
                          <Select.Option value="W">W</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label className="required">Voyage No. :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_voyage_no"
                      id="arrival_voyage_no"
                      className="form-control"
                      required
                      value={
                        formdata.report_content.mainInformation
                          .arrival_voyage_no
                      }
                      onChange={(e) => {
                        handleChange(e, "mainInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label className="required">Date</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      name="arrivl_report_date"
                      id="arrivl_report_date"
                      style={{ width: "100%" }}
                      onChange={(date, dateString) => {
                        handleSelectDate(
                          date,
                          dateString,
                          "mainInformation",
                          "arrivl_report_date"
                        );
                      }}
                      value={
                        formdata.report_content.mainInformation
                          .arrivl_report_date
                          ? dayjs(
                            formdata.report_content.mainInformation
                              .arrivl_report_date,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>

        <div className="rpbox">
          <Row>
            <h4 className="mainHeading">Port Information</h4>
          </Row>

          <div
            style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
          >
            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Port:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      onClick={() => {
                        setKey("arrival_port");
                        setPortModal(true);
                      }}
                      type="text"
                      name="arrival_port"
                      id="arrival_port"
                      value={
                        formdata.report_content.portInformation.arrival_port
                      }
                      onChange={(value) => {
                        handleChange(value, "portInformation");
                      }}
                      required
                      style={{ height: "100%" }}
                      placeholder="Port"
                    />
                    {isPortModal && (
                      <Modal
                        title="Select Port"
                        open={isPortModal}
                        width="80%"
                        onOk={() => {
                          setPortModal(false);
                        }}
                        onCancel={() => {
                          setPortModal(false);
                        }}
                        footer={null}
                      >
                        <PortSelection
                          fromPortID={null}
                          modalCloseEvent={(data) => {
                            modalCloseEvent(data);
                          }}
                        />
                      </Modal>
                    )}
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>EOSP Date:</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      onChange={(date, dateString) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "portInformation",
                          "eosp_date_time"
                        )
                      }
                      className="form-control"
                      name="eosp_date_time"
                      id="eosp_date_time"
                      value={
                        formdata.report_content.portInformation.eosp_date_time
                          ? dayjs(
                            formdata.report_content.portInformation
                              .eosp_date_time,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Latitude:</label>
                  </Col>
                  <Col span={14}>
                    <Row gutter={8}>
                      <Col span={12}>
                        <Input
                          type="text"
                          name="arrival_port_lat"
                          id="arrival_port_lat"
                          style={{ height: "100%" }}
                          placeholder="DDMMSS"
                          onChange={(value) => {
                            handleChange(value, "portInformation");
                          }}
                          value={
                            formdata.report_content.portInformation
                              .arrival_port_lat
                          }
                        />
                      </Col>

                      <Col span={12}>
                        <Select
                          defaultValue="N"
                          name="arrival_port_lat_dir"
                          id="arrival_port_lat_dir"
                          style={{ flexBasis: "30%" }}
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "portInformation",
                              "arrival_port_lat_dir"
                            );
                          }}
                          value={
                            formdata.report_content.portInformation
                              .arrival_port_lat_dir
                          }
                        >
                          <Select.Option value="N">N</Select.Option>
                          <Select.Option value="S">S</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Longitude:</label>
                  </Col>
                  <Col span={14}>
                    <Row gutter={8}>
                      <Col span={12}>
                        <Input
                          type="text"
                          name="arrival_port_long"
                          id="arrival_port_long"
                          style={{ height: "100%" }}
                          placeholder="DDMMSS"
                          onChange={(value) => {
                            handleChange(value, "portInformation");
                          }}
                          value={
                            formdata.report_content.portInformation
                              .arrival_port_long
                          }
                        />
                      </Col>

                      <Col span={12}>
                        <Select
                          defaultValue="E"
                          name="arrival_port_long_dir"
                          id="arrival_port_long_dir"
                          style={{ flexBasis: "30%" }}
                          onChange={(value) => {
                            handleSelect(
                              value,
                              "portInformation",
                              "arrival_port_long_dir"
                            );
                          }}
                          value={
                            formdata.report_content.portInformation
                              .arrival_port_long_dir
                          }
                        >
                          <Select.Option value="E">E</Select.Option>
                          <Select.Option value="W">W</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Next Port:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      onClick={() => {
                        setKey("arrival_next_port");
                        setPortModal(true);
                      }}
                      type="text"
                      name="arrival_next_port"
                      id="arrival_next_port"
                      className="form-control"
                      value={
                        formdata.report_content.portInformation
                          .arrival_next_port
                      }
                      onChange={(value) => {
                        handleChange(value, "portInformation");
                      }}
                      required
                      style={{ width: "100%" }}
                      placeholder="Port"
                    />
                    {isPortModal && (
                      <Modal
                        title="Select Port"
                        open={isPortModal}
                        width="80%"
                        onOk={() => {
                          setPortModal(false);
                        }}
                        onCancel={() => {
                          setPortModal(false);
                        }}
                        footer={null}
                      >
                        <PortSelection
                          fromPortID={null}
                          modalCloseEvent={(data) => {
                            modalCloseEvent(data);
                          }}
                        />
                      </Modal>
                    )}
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Port ID:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      value={
                        formdata.report_content.portInformation
                          .arrival_next_port_id
                      }
                      name="arrival_next_port_id"
                      id="arrival_next_port_id"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "portInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Anchor Dropped Date Time</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      onChange={(date, dateString) => {
                        handleSelectDate(
                          date,
                          dateString,
                          "portInformation",
                          "anchor_dropped_date_time"
                        );
                      }}
                      className="form-control"
                      name="anchor_dropped_date_time"
                      id="anchor_dropped_date_time"
                      value={
                        formdata.report_content.portInformation
                          .anchor_dropped_date_time
                          ? dayjs(
                            formdata.report_content.portInformation
                              .anchor_dropped_date_time,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>

        <div className="rpbox">
          <Row>
            <h4 className="mainHeading">Cargo Information</h4>
          </Row>

          <div
            style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
          >
            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Ttl Cargo Onboard:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_ttl_cargo_onboard"
                      id="arrival_ttl_cargo_onboard"
                      defaultValue={22.22}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_ttl_cargo_onboard
                      }
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Displacemnt Mt:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_displacement_mt"
                      id="arrival_displacement_mt"
                      style={{ height: "100%" }}
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_displacement_mt
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Deck Cargo Mt:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_deck_cargo_mt"
                      id="arrival_deck_cargo_mt"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_deck_cargo_mt
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Hold cargo Mt:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_hold_cargo_mt"
                      id="arrival_hold_cargo_mt"
                      defaultValue={33}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_hold_cargo_mt
                      }
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>FW consumed:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_fw_consumed"
                      id="arrival_fw_consumed"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_fw_consumed
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>FW ROB:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_fw_rob"
                      id="arrival_fw_rob"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation.arrival_fw_rob
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>SLOP ROB:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_slop_rob"
                      id="arrival_slop_rob"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_slop_rob
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>SLOP Produced:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_slop_produced"
                      id="arrival_slop_produced"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_slop_produced
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Ballast Water:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_ballast_water"
                      id="arrival_ballast_water"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_ballast_water
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Type of Operation</label>
                  </Col>
                  <Col span={14}>
                    <Select
                      onChange={(value) =>
                        handleSelect(
                          value,
                          "cargoInformation",
                          "arrival_type_of_operation"
                        )
                      }
                      name="arrival_type_of_operation"
                      id="arrival_type_of_operation"
                      style={{ width: "100%" }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_type_of_operation
                      }
                    >
                      <Option value="Load">Load</Option>
                      <Option value="discharge">Discharge</Option>
                    </Select>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Cargo Ops Start</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                    style={{width:"100%"}}
                      onChange={(date, dateString, name, event) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "cargoInformation",
                          "arrival_cargo_ops_start"
                        )
                      }
                      format="YYYY-MM-DD"
                      name="arrival_cargo_ops_start"
                      id="arrival_cargo_ops_start"
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_cargo_ops_start
                          ? dayjs(
                            formdata.report_content.cargoInformation
                              .arrival_cargo_ops_start,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Cargo Ops End:</label>
                  </Col>
                  <Col span={14}>
                    <DatePicker
                           style={{width:"100%"}}
                      onChange={(date, dateString, name, event) =>
                        handleSelectDate(
                          date,
                          dateString,
                          "cargoInformation",
                          "arrival_cargo_ops_end"
                        )
                      }
                      format="YYYY-MM-DD"
                      name="arrival_cargo_ops_end"
                      id="arrival_cargo_ops_end"
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_cargo_ops_end
                          ? dayjs(
                            formdata.report_content.cargoInformation
                              .arrival_cargo_ops_end,
                            "YYYY-MM-DD HH:mm:ss"
                          )
                          : undefined
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Cargo Loaded:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      onChange={(e) => {
                        handleChange(e, "cargoInformation");
                      }}
                      type="number"
                      name="departure_cargo_loaded"
                      id="departure_cargo_loaded"
                      value={
                        formdata.report_content.cargoInformation
                          .departure_cargo_loaded
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Cleaning Chemical:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="arrival_cleaning_chemical_rob"
                      id="arrival_cleaning_chemical_rob"
                      className="form-control"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .arrival_cleaning_chemical_rob
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Distill Water Rob:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="number"
                      name="departure_distill_water_rob"
                      id="departure_distill_water_rob"
                      onChange={(value) => {
                        handleChange(value, "cargoInformation");
                      }}
                      value={
                        formdata.report_content.cargoInformation
                          .departure_distill_water_rob
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Cargo Discharged:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      onChange={(e) => handleChange(e, "cargoInformation")}
                      type="number"
                      name="departure_cargo_discharg"
                      id="departure_cargo_discharg"
                      className="form-control"
                      value={
                        formdata.report_content.cargoInformation
                          .departure_cargo_discharg
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>

        <div className="rpbox">
          <Row>
            <h4 className="mainHeading">Vessel & Distance</h4>
          </Row>

          <div
            style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
          >
            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Passage:</label>
                  </Col>
                  <Col span={14}>
                    <Select
                      style={{
                        borderBottom: "1px solid #6D96B4"
                      }}
                      name="arrival_Passage"
                      id="arrival_Passage"
                      onChange={(value) => {
                        handleSelect(
                          value,
                          "vesselDistance",
                          "arrival_Passage"
                        );
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_Passage
                      }
                    >
                      <Option value="Laden">Laden</Option>
                      <Option value="Ballast">Ballast</Option>
                    </Select>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Aver RPM:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_aver_rpm"
                      id="arrival_aver_rpm"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_aver_rpm
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 1 Hr :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_one_hr"
                      id="arrival_gen_one_hr"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_one_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Bad Weathr hrs:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_bad_wether_hrs"
                      id="arrival_bad_wether_hrs"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_bad_wether_hrs
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Ordered SPd:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_ordered_spd"
                      id="arrival_ordered_spd"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_ordered_spd
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Ave BHP:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_ave_bhp"
                      id="arrival_ave_bhp"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_ave_bhp
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 2 Hr :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_two_hr"
                      id="arrival_gen-two-hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_two_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Bad weathr Dist:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_bad_wether_dist"
                      id="arrival_badWether_dist"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_bad_wether_dist
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Reported spd:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_reported_spd"
                      id="arrival_reported_spd"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_reported_spd
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>FWD dft:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_fwd_dft"
                      id="arrival_fwd_dft"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_fwd_dft
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 3 Hr:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_three_hr"
                      id="arrival_gen_three_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_three_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Sea state:</label>
                  </Col>
                  <Col span={14}>
                    <Select
                      style={{
                        borderBottom: "1px solid #6D96B4"
                      }}
                      name="arrival_sea_state"
                      id="arrival_sea_state"
                      // Adjust width as needed
                      onChange={(value) =>
                        handleSelect(
                          value,
                          "vesselDistance",
                          "arrival_sea_state"
                        )
                      }
                      value={
                        formdata.report_content.vesselDistance.arrival_sea_state
                      }
                    >
                      {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((optionValue) => (
                        <Option
                          key={optionValue}
                          value={optionValue.toString()}
                        >
                          {optionValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              {/* <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }}>
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Observe distance:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_observe_dist"
                      id="arrival_observe_dist"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={formdata.report_content.vesselDistance.arrival_observe_dist}
                    />
                  </Col>
                </Row>
              </Col> */}
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Average Speed</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrial_aver_speed"
                      id="arrial_aver_speed"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrial_aver_speed
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>AFT drft:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_aft_drft"
                      id="arrival_aft_drft"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_aft_drft
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 4 Hr:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_four_hr"
                      id="arrival_gen_four_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_four_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Sea Direction:</label>
                  </Col>
                  <Col span={14}>
                    <Select
                      style={{
                        borderBottom: "1px solid #6D96B4"
                      }}
                      name="arrival_sea_dir"
                      id="arrival_sea_dir"
                      // Adjust width as needed
                      onChange={(value) => {
                        handleSelect(
                          value,
                          "vesselDistance",
                          "arrival_sea_dir"
                        );
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_sea_dir
                      }
                    >
                      {[
                        "North",
                        "South",
                        "East",
                        "West",
                        "North-East",
                        "East-West",
                        "North-West",
                        "South-East",
                        "South-West",
                      ].map((optionValue) => (
                        <Option key={optionValue} value={optionValue}>
                          {optionValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Observe distance:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_observe_dist"
                      id="arrival_observe_dist"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_observe_dist
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Mid draft:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_mid_draft"
                      id="arrival_mid_draft"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_mid_draft
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>M/E KWHR:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_me_kw_hr"
                      id="arrival_me_kw_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_me_kw_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Sea Height:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_sea_height"
                      id="arrival_sea_height"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_sea_height
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Engine dist. :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_engine_dist"
                      id="arrival_engine_dist"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_engine_dist
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Steaming Hr:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_total_steaming_hr"
                      id="arrival_total_steaming_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_total_steaming_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 1 KWHR:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_one_kw_hr"
                      id="arrival_gen_one_kw_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_one_kw_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Swell: </label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_swell"
                      id="arrival_swell"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_swell
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Main Eng Rev.:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_main_eng_rev"
                      id="arrival_main_eng_rev"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_main_eng_rev
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Air pressure:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_air_press"
                      id="arrival_air_press"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_air_press
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 2 KWHR:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_two_kw_hr"
                      id="arrival_gen_two_kw_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_two_kw_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Swell Direction:</label>
                  </Col>
                  <Col span={14}>
                    <Select
                      style={{
                        borderBottom: "1px solid #6D96B4"
                      }}
                      name="arrival_swell_dir"
                      id="arrival_swell_dir"
                      onChange={(value) => {
                        handleSelect(
                          value,
                          "vesselDistance",
                          "arrival_swell_dir"
                        );
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_swell_dir
                      }
                    >
                      {[
                        "North",
                        "South",
                        "East",
                        "West",
                        "North-East",
                        "East-West",
                        "North-West",
                        "South-East",
                        "South-West",
                      ].map((optionValue) => (
                        <Option key={optionValue} value={optionValue}>
                          {optionValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Slip% :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_slip_perc"
                      id="arrival_slip_perc"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_slip_perc
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Sea Surface Temperature</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_air_press"
                      id="arrival_air_press"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_air_press
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 3 KWHR:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_three_kw_hr"
                      id="arrival_gen_three_kw_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_three_kw_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Swell Height:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_swell_height"
                      id="arrival_swell_height"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_swell_height
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Salinity:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_salinity"
                      id="arrival_salinity"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_salinity
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Air Tempt. :</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_air_tempt"
                      id="arrival_air_tempt"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_air_tempt
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Gen 4 KWHR:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_gen_four_kw_hr"
                      id="arrival_gen_four_kw_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_gen_four_kw_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Wind For: </label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_wind_for"
                      id="arrival_wind_for"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_wind_for
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Density:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_density"
                      id="arrival_density"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_density
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Total Miles Covered</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_total_miles_covered"
                      id="arrival_total_miles_covered"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_total_miles_covered
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>M/E hr</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_me_hr"
                      id="arrival_me_hr"
                      className="form_control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance.arrival_me_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Wind Dire:</label>
                  </Col>
                  <Col span={14}>
                    <Select

                      style={{
                        borderBottom: "1px solid #6D96B4"
                      }}
                     
                      name="arrival_wind_dir"
                      id="arrival_wind_dir"

                      onChange={(value) =>
                        handleSelect(
                          value,
                          "vesselDistance",
                          "arrival_wind_dir"
                        )
                      }
                      value={
                        formdata.report_content.vesselDistance.arrival_wind_dir
                      }
                    >
                      {[
                        "North",
                        "South",
                        "East",
                        "West",
                        "North-East",
                        "East-West",
                        "North-West",
                        "South-East",
                        "South-West",
                      ].map((optionValue) => (
                        <Option key={optionValue} value={optionValue}>
                          {optionValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Total Steaming Hr:</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_total_steaming_hr"
                      id="arrival_total_steaming_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_total_steaming_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Consumption On F.D Main Engine</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_consum_fd_main"
                      id="arrival_consum_fd_main"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_consum_fd_main
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Consumption On F.D Aux Engine</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_consum_fd_aux"
                      id="arrival_consum_fd_aux"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_consum_fd_aux
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Average Daily D.O ()</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_total_steaming_hr"
                      id="arrival_total_steaming_hr"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_total_steaming_hr
                      }
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Consumption On F.D Main Engine</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_consum_fd_main"
                      id="arrival_consum_fd_main"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_consum_fd_main
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={[80]}>
              <Col
                span={8}
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 8 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
              >
                <Row gutter={16}>
                  <Col span={10}>
                    <label>Consumption Boiler</label>
                  </Col>
                  <Col span={14}>
                    <Input
                      type="text"
                      name="arrival_consum_boiler"
                      id="arrival_consum_boiler"
                      className="form-control"
                      onChange={(e) => {
                        handleChange(e, "vesselDistance");
                      }}
                      value={
                        formdata.report_content.vesselDistance
                          .arrival_consum_boiler
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>

       

        <div className="rpbox">
          <h4 className="mainHeading">Bunker</h4>
          <div className="row">
            <div className="col-12">
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Type (MT)</th>
                      <th>IFO</th>
                      <th>VLSFO</th>
                      <th>ULSFO</th>
                      <th>LSMGO</th>
                      <th>MGO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="tableRow">
                      <td className="required">BROB:</td>
                      <td>
                        <Input
                          type="text"
                          name="arrival_brob_info"
                          id="arrival_brob_info"
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          className="form-control"
                          required
                          value={
                            formdata.report_content.bunker.arrival_brob_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_brob_vlsfo"
                          id="arrival_brob_vlsfo"
                          className="form-control"
                          //   style="width: 80%;"

                          value={
                            formdata.report_content.bunker.arrival_brob_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_brob_ulsfo"
                          id="arrival_brob_ulsfo"
                          className="form-control"
                          //   style="width: 80%;"

                          value={
                            formdata.report_content.bunker.arrival_brob_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_brob_lsmgo"
                          id="arrival_brob_lsmgo"
                          className="form-control"
                          //   style="width: 80%;"
                          value={
                            formdata.report_content.bunker.arrival_brob_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_brob_mgo"
                          id="arrival_brob_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_brob_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td className="required">M/E Propulsion:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_me_propulsion_info"
                          id="arrival_me_propulsion_info"
                          className="form-control"
                          required
                          value={
                            formdata.report_content.bunker
                              .arrival_me_propulsion_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_me_propulsion_vlsfo"
                          id="arrival_me_propulsion_vlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_me_propulsion_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_me_propulsion_ulsfo"
                          id="arrival_me_propulsion_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_me_propulsion_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_me_propulsion_lsmgo"
                          id="arrival_me_propulsion_lsmgo"
                          value={
                            formdata.report_content.bunker
                              .arrival_me_propulsion_lsmgo
                          }
                          className="form-control"

                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_me_propulsion_mgo"
                          id="arrival_me_propulsion_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_me_propulsion_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td>Boiler:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_boiler_info"
                          id="arrival_"
                          value={
                            formdata.report_content.bunker.arrival_boiler_info
                          }
                          className="form-control"
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_boiler_vlsfo"
                          id="arrival_boiler_vlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_boiler_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_boiler_ulsfo"
                          id="arrival_boiler_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_boiler_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_boiler_lsmgo"
                          id="arrival_boiler_lsmgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_boiler_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_boiler_mgo"
                          id="arrival_boiler_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_boiler_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td>Generator:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_generator_info"
                          id="arrival_generator_info"
                          className="form-control"
                          value={
                            formdata.report_content.bunker
                              .arrival_generator_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_generator_vlsfo"
                          id="arrival_generator_vlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_generator_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_generator_ulsfo"
                          id="arrival_generator_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_generator_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_generator_lsmgo"
                          id="arrival_generator_lsmgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_generator_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_generator_mgo"
                          id="arrival_generator_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker.arrival_generator_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td>Fuel Receoved:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuelReceoved_info"
                          id="arrival_fuelReceoved_info"
                          className="form-control"
                          value={
                            formdata.report_content.bunker
                              .arrival_fuelReceoved_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_receoved_vlsfo"
                          id="arrival_fuel_receoved_vlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_receoved_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_receoved_ulsfo"
                          id="arrival_fuel_receoved_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_receoved_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_receoved_lsmgo"
                          id="arrival_fuel_receoved_lsmgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_receoved_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_receoved_mgo"
                          id="arrival_fuel_receoved_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_receoved_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td>Fuel Debunker:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_debunker_info"
                          id="arrival_fuel_debunker_info"
                          className="form-control"
                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_debunker_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_debunker_vlsfo"
                          id="arrival_fuel_debunkerVlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_debunker_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_debunker_ulsfo"
                          id="arrival_fuel_debunker_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_debunker_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_debunker_lsmgo"
                          id="arrival_fuel_debunker_lsmgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_debunker_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_fuel_debunker_mgo"
                          id="arrival_fuel_debunker_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_fuel_debunker_mgo
                          }
                        />
                      </td>
                    </tr>
                    <tr className="tableRow">
                      <td>Aux Exngine:</td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_aux_exngine_info"
                          id="arrival_aux_exngine_info"
                          className="form-control"
                          value={
                            formdata.report_content.bunker
                              .arrival_aux_exngine_info
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_aux_exngine_vlsfo"
                          id="arrival_aux_exngine_vlsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_aux_exngine_vlsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_aux_exngine_ulsfo"
                          id="arrival_aux_exngine_ulsfo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_aux_exngine_ulsfo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_aux_exngine_lsmgo"
                          id="arrival_aux_exngine_lsmgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_aux_exngine_lsmgo
                          }
                        />
                      </td>
                      <td>
                        <Input
                          onChange={(e) => {
                            handleChange(e, "bunker");
                          }}
                          type="text"
                          name="arrival_aux_exngine_mgo"
                          id="arrival_aux_exngine_mgo"
                          className="form-control"

                          value={
                            formdata.report_content.bunker
                              .arrival_aux_exngine_mgo
                          }
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div className="rpbox" style={{ gap: "2rem" }}>
          <Row gutter={[24, 24]}>
            <Col span={2}>
              <label style={{ flexBasis: "10%" }}>Remark:</label>
            </Col>
            <Col span={22}>
              <Input
                className="form-control"
                //   style="flex-basis:83%;"
                style={{ flexBasis: "83%" }}
                name="arrival_remark"
                id="arrival_remark"
                onChange={(e) => handleChange(e, "remark")}
                value={formdata.report_content.remark.arrival_remark}
              />
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={12}>
              <Row gutter={8}>
                <Col span={4}>
                  <label>Captain:</label>
                </Col>
                <Col span={20}>
                  <Input
                    type="text"
                    name="arrival_captain"
                    id="arrival_captain"
                    className="form-control"
                    //   style="flex-basis: 100%;"
                    style={{ flexBasis: "100%" }}
                    onChange={(e) => handleChange(e, "remark")}
                    value={formdata.report_content.remark.arrival_captain}
                  />
                </Col>
              </Row>
            </Col>

            <Col span={12}>
              <Row gutter={8}>
                <Col span={4}>
                  <label>C/E:</label>
                </Col>

                <Col span={20}>
                  <Input
                    type="text"
                    name="arrival_ce"
                    id="arrival_ce"
                    className="form-control"
                    //   style="flex-basis:100%;"
                    style={{ flexBasis: "100%" }}
                    onChange={(e) => handleChange(e, "remark")}
                    value={formdata.report_content.remark.arrival_ce}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>


    </div>
  );
};

export default ArrivalReport;
