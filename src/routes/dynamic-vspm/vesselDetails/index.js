import React, { useEffect, useState } from "react";
import { Form, Input, Select, Button, Modal } from "antd";
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon } from "../../../shared";
import {MapComponent} from "../Newmap";
import { number } from "yup";

const VesselDetails = () => {
  const [state, setState] = useState({
    voyageNoData: [],
    pdfData: [],
    keyNames: [],
    sumData: [],
    voyNo: "",
    data: [],
    isDisabled: true,
    noon_port_longLat:[],
    isOpen: true,
    isMapVisible: true,
    coordinates : null,
    noonDates: null,
  });

  const FormItem = Form.Item;
  const Option = Select.Option;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 14 },
    },
  };

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try{
      let _url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyage-historical-list?p=1&l=0`;
      const response = await getAPICall(_url);
      const data = await response // Parse JSON response

      setState(prevState => ({
        ...prevState,
        data: data.data,
      }));
    }
    catch (error) {
      console.log("error", error);
    }
  };

  const getVoyageno = async (value) => {
    try {
      setState((prev) => ({
        ...prev,
        keyNames: [],
        voyNo:""
      }))
      let mt_url = `${process.env.REACT_APP_MT_URL}marine/get-vessels-name/${value}`;
      const mt_resp = await getAPICall(mt_url);
      const mt_data = await mt_resp;
      console.log("mt_data ==>", mt_data);

      let vessel_mt = `${process.env.REACT_APP_MT_URL}marine/vessel-position-imo/${mt_data[0].IMO}`;
      console.log("vessel_mt", vessel_mt)
      const vessel_mt_resp = await getAPICall(vessel_mt);
      console.log("vessel_mt_data ==>", vessel_mt_resp);

      console.log("mt_data ==>", mt_data);
      let url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyage--historical-list-filter?name=${value}`;
      const respData = await getAPICall(url);
      const data = await respData
      setState(prevState => ({
        ...prevState,
        pdfData: data,
        keyNames: Object.keys(data.data),
        isDisabled: false,
      }));
    } catch (error) {
      console.log("error", error);
    }
  };

  const handleSelect = (value) => {
    const { pdfData } = state;

    setState(prevState => ({
      ...prevState,
      voyageNoData: [pdfData.data[value]],
      voyNo: value
    }));
  };

  const filterData = () => {

    if(state.voyNo !== ""){
      const filterNoonData = state.voyageNoData[0]?.allReport?.filter(
        (obj) => obj.report_type === 5
      );
  
      const noonDates = filterNoonData.map((e) => e.created_date)
      const noonLat = filterNoonData?.map((e) =>  Number.parseFloat(e.report_content?.portInformation?.noon_lat))
      const noonLong = filterNoonData?.map((e) => Number.parseFloat(e.report_content?.portInformation?.noon_main_long))
  
      const mergredNoonArray = noonLong.map((value, index) => [value, noonLat[index]]);
      const filterDepartureData = state.voyageNoData[0]?.allReport?.filter(
        (obj) => obj.report_type === 4
      );
  
      const firstDeparture = filterDepartureData[0];
      const departureLat = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_lat) || '';
      const departureLong = Number.parseFloat(firstDeparture?.report_content?.mainInformation?.departure_main_long) || '';
  
      const departureLongLat = [departureLong, departureLat]
  
      const filterArrivalData = state.voyageNoData[0]?.allReport?.filter(
        (obj) => obj.report_type === 1
      );
  
      const lastArrivaldata = filterArrivalData[filterArrivalData.length - 1];
      const arrivalLat = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_lat) || '';
      const arrivalLong = Number.parseFloat(lastArrivaldata?.report_content?.mainInformation?.arrival_main_long) || '';
  
      const arrivalLongLat = [arrivalLong, arrivalLat]
  
      const startDestination = [departureLongLat, ...mergredNoonArray, arrivalLongLat]
  
      setState((prev) => ({
        ...prev,
        noonDates: noonDates,
        coordinates: startDestination
      }))
    }else{
      openNotificationWithIcon("info", "Please select any voyage number")
    }
  };

  const {
    voyageNoData,
    keyNames,
    data,
    isDisabled,
    coordinates
  } = state;

  return (
    <div className="body-wrapper">
      <article className="article track-noon-form">
          <div>
            <Form>
              <div className="row">
                <div className="col-md-4">
                  <FormItem {...formItemLayout} label="Vessel Name">
                    <Select
                      showSearch
                      placeholder="Select Vessel"
                      onSelect={(value) => getVoyageno(value)}
                    >
                      {data
                        .filter((element, index, array) => {
                          // Filter out only the first occurrence of each vesselName
                          return (
                            array.findIndex(
                              (item) => item.vesselName === element.vesselName
                            ) === index
                          );
                        })
                        .map((e) => (
                          <Option key={e.vesselName} value={e.vesselName}>
                            {e.vesselName}
                          </Option>
                        ))}
                    </Select>
                  </FormItem>
                </div>

                <div className="col-md-4">
                  <FormItem {...formItemLayout} label="Voyage No">
                    <Select
                      onChange={(value) => handleSelect(value)}
                      disabled={isDisabled}
                      value={keyNames.length === 0 ? "Choose Vessel First" : undefined}
                      showSearch
                      placeholder={
                        keyNames?.length === 0
                          ? "Choose Vessel First"
                          : "Select Voyage No."
                      }
                    >
                      {keyNames?.map((e) => (
                        <Option value={e}>{e}</Option>
                      ))}
                    </Select>
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <div>
                    <Button
                      disabled={voyageNoData?.length === 0 ? true : false}
                      className="btn ant-btn-primary btn-md"
                      onClick={() => filterData()}
                    >
                      Search
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          </div>
      </article>
      {
        state.isMapVisible && (
          <div style={{position: 'relative', minHeight: '400px', overflow: 'hidden'}}>
            <MapComponent
              type="trackByNoon"
              coordinates={coordinates}
              dates={state.noonDates}
            />
          </div>

        )
      }
    </div>
  );
};

export default VesselDetails;
