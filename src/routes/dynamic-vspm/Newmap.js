import React, { useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import moment from 'moment';

// Replace 'YOUR_MAPBOX_ACCESS_TOKEN' with your actual Mapbox access token
mapboxgl.accessToken = `${process.env.REACT_APP_MAPBOX_TOKEN}`;

export const MapComponent = (props) => {
    // console.log("props", props);
    const [map, setMap] = useState(null);
    const [error, setError] = useState("");

    const [geojson, setGeojson] = useState({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'properties': {},
                    'coordinates': props.coordinates || []
                }
            },
            {

            }
        ]
    });

    useEffect(() => {
        checkerFunc();
        if (props.coordinates && props.coordinates.length >= 2) {
            const newGeojson = {
                'type': 'FeatureCollection',
                'features': [
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'LineString',
                            'properties': {},
                            'coordinates': props.coordinates ? props.coordinates : []
                        }
                    }
                ]
            };
            setGeojson(newGeojson);
        }
    }, [props.coordinates]);
    

    useEffect(() => {
        if (!geojson) return;

        // Remove existing map instance if it exists
        if (mapboxgl.getRTLTextPluginStatus() === 'loaded') {
            mapboxgl.getMaps().forEach((map) => {
                map.remove();
            });
        }

        const mapInstance = new mapboxgl.Map({
            container: 'map',
            style: "mapbox://styles/techtheocean/cl6yw3vjx000h14s0yrxn5cf6",
            center: { lat: 20.5937, lng: 78.9629 },
            minZoom: 1,
            zoom: 1,
        });

        setMap(mapInstance);

        mapInstance.on('load', () => {

            mapInstance.addSource('LineString', {
                'type': 'geojson',
                'data': geojson
            });

            mapInstance.addLayer({
                'id': 'LineString',
                'type': 'line',
                'source': 'LineString',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#003e78',
                    'line-width': 3,
                    'line-dasharray': [1, 3]
                }
            });

            if (props.coordinates && props.coordinates.length > 0) {
                const hasEmptyCoordinates = props.coordinates.some(coord => {
                    return coord[0] === "" || coord[1] === "";
                });
            
                if (!hasEmptyCoordinates) {
                    const firstCoordinate = props.coordinates[0];
                    const lastCoordinate = props.coordinates[props.coordinates.length - 1];
                    
            
                    // Ensure latitude values are within valid range
                    const firstLat = Math.max(Math.min(firstCoordinate[1], 90), -90);
                    const lastLat = Math.max(Math.min(lastCoordinate[1], 90), -90);
                    const customMarker1 = document.createElement('div');
                            customMarker1.innerHTML = 
                            `<div class="noon-box">
                                <img 
                                width="17"
                                src="./anchor.png"
                                alt="Noon Report"
                                title="Departure Port" 
                                />
                            </div>`;
                    const customMarker2 = document.createElement('div');
                            customMarker2.innerHTML = 
                            `<div class="noon-box">
                                <img 
                                width="17"
                                src="./anchor.png"
                                alt="Noon Report"
                                title="Arrival Port" 
                                />
                            </div>`;

                    new mapboxgl.Marker(customMarker1).setLngLat([firstCoordinate[0], firstLat]).addTo(mapInstance);
                    new mapboxgl.Marker(customMarker2).setLngLat([lastCoordinate[0], lastLat]).addTo(mapInstance);
                }

                if (props.type === "trackByNoon") {
                    const hasInvalidCoordinates = props.coordinates.some(coord => {
                        return isNaN(coord[0]) || isNaN(coord[1]) || coord[0] === "" || coord[1] === "";
                    });
                
                    if (!hasEmptyCoordinates) {
                        const noonCords = props.coordinates.slice(1, -1);
                        const noonDates = props.dates ? props.dates : [];
                
                        noonCords.forEach((coordinate, index) => {
                            let [latitude ,longitude] = coordinate; // Destructure the coordinate pair
                            latitude = isNaN(latitude) ? '' : latitude.toString(); // Replace NaN with empty string
                            longitude = isNaN(longitude) ? '' : longitude.toString(); // Replace NaN with empty string
                            const date = new Date(noonDates[index]);
                            // Create a custom marker element
                            const customMarker = document.createElement('div');
                            customMarker.className = 'custom-marker'; 
                            customMarker.innerHTML = 
                            `<div class="noon-box">
                                <div class="noon-info-box">
                                    <p>Noon Report</p>
                                    <p><span>Time :</span><span>${moment.utc(date).format("DD/MM/YYYY HH:mm")}</span></p>
                                    <p><span>Latitude :</span><span>${latitude}</span></p>
                                    <p><span>Longitude :</span><span>${longitude}</span></p>
                                </div>
                                <img 
                                class="noon-marker"
                                width="10"
                                src="./noonbtn.png"
                                alt="Noon Report" 
                                />
                            </div>`;
                
                            // Add marker to the map
                            new mapboxgl.Marker(customMarker)
                            .setLngLat([latitude ,longitude])
                            .addTo(mapInstance);
                        });
                    }
                }
                
            }
            
        });
        // console.log("mapInstance", mapInstance);
        return () => mapInstance.remove();
    }, [geojson, props.coordinates]);

    const checkerFunc = () => {
        if (!props.coordinates || !Array.isArray(props.coordinates) || props.coordinates.length < 2) {
            setError('Invalid coordinates provided');
            return;
        }

        const isValidCoordinates = props.coordinates.every(coord => {
            return (
                Array.isArray(coord) &&
                coord.length === 2 &&
                typeof coord[0] === 'number' &&
                typeof coord[1] === 'number'
            );
        });

        if (!isValidCoordinates) {
            setError('Invalid coordinates provided');
            return;
        }

        setGeojson({
            'type': 'FeatureCollection',
            'features': [
                {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'LineString',
                        'properties': {},
                        'coordinates': props.coordinates ? props.coordinates : []
                    }
                }
            ]
        });
    }
    

    return (
        <div style={{height: '100%'}}>
             {/* {error && <div>{error}</div>} */}
            <div id="map" style={{ position: 'absolute', top: 0, bottom: 0, width: '100%' }} />
            {/* <button className="btn-control btn-primary" onClick={zoomToBounds}>Zoom to bounds</button> */}
        </div>
    );
};



export default MapComponent;
