import React from 'react';
import { Table,  Popconfirm, Modal } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import VoyagePerformanceReport from '../operation-reports/VoyagePerformanceReport';
import EmissionReport from '../operation-reports/EmissionReport';
import ConsolidateReport from '../operation-reports/ConsolidateReport';
import Tde from '../tde/Tde';
import { EyeOutlined } from '@ant-design/icons';
import { useEffect } from 'react';
const revenueStatus = { "revenue": 173, "expense": 174 }

const  ReportList = () => {

  const [state, setState] = useStateCallback({
      loading: false,
      columns: [],
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      typesearch:{},
      donloadArray: [],
      reportdata: {},
      dumpData: [],
      voyNo:"",
      reportType:"",
  })
  

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      // width: 100,
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => redirectToAdd(e, record)}>
             <EyeOutlined/>
            </span>
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['report-list'] ? FIELDS['report-list']['tableheads'] : []
    );
   
    tableHeaders.push(tableAction);

    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  },[])

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
    
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
     
      }
    
      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }
    
      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }
    

    setState(prev => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/vessel/voyagereportlist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    let donloadArr = [];
    if (dataArr.length > 0) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      _state['responseData'] = dataArr;
    }
    setState(prev => ({
      ...prev,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };


  const redirectToAdd = async (e, record = null) => {
    try{
      const url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyage--historical-list-filter?name=${record.vessel_name}`;
      const _url = `${process.env.REACT_APP_URL}v1/voyage-manager/vessel/voyage-historical-list?p=1&l=0`;
      // To Make both API calls simultaneously
      const [data, _data] = await Promise.all([getAPICall(url), getAPICall(_url)]);
      if(record.voy_no){
        setState(
          (prevState) => ({
            ...prevState,
            isVisible: true,
            dumpData: _data,
            reportdata: data?.data[record.voy_no],
            voyNo: record.voy_no,
            reportType: record.report_type
        }));
      }else{
        openNotificationWithIcon("info", "No report Available")
      }
    } catch (error){
      console.error(error);
    }
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
  
      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);
  
      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
  
      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];
  
      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }
  
      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
    };

  const handleResize = (index) => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const onCancel = () => {
    setState({ ...state, isAdd: true, isVisible: false });
  }

  const modalCloseEvent = () => {
    setState({ ...state, isAdd: true, isVisible: false }, ()=>getTableData());
  }

  const onActionDonwload = (downType, pageType) => {
      let params = `t=${pageType}`, cols = [];
      const { columns, pageOptions, donloadArray } = state; 
    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }

  const {
      columns,
      loading,
      responseData,
      pageOptions,
      search,
      isVisible,
      sidebarVisible,
      formDataValues,
      reportdata,
      dumpData,
      voyNo,
      reportType
    } = state;

    const tableCol = columns
      .filter((col) => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

      
    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-wrapper">
                  <div className="form-heading">
                    <h4 className="title">
                      <span>All Report List</span>
                    </h4>
                  </div>
                </div>
                <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                  {
                    loading === false ?
                      <ToolbarUI 
                        routeUrl={'report-list'}
                        optionValue={{ 'pageOptions': pageOptions, 
                        'columns': columns, 
                        'search': search }} 
                        callback={(e) => callOptions(e)}
                        dowloadOptions={[
                          { title: 'CSV', event: () => onActionDonwload('csv', 'voyage-history') },
                          { title: 'PDF', event: () => onActionDonwload('pdf', 'voyage-history') },
                          { title: 'XLS', event: () => onActionDonwload('xlsx', 'voyage-history') },
                        ]}
                      />
                      : undefined
                  }
                </div>
                <div>
                  <Table
                    // rowKey={record => record.id}
                    className="inlineTable resizeableTable"
                    bordered
                    columns={tableCol}
                    components={components}
                    size="small"
                    scroll={{ x: 'max-content' }}
                    dataSource={responseData}
                    loading={loading}
                    pagination={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                  />
                </div>
              </div>
            </div>
          </article>
        {
          isVisible === true && reportType !== "" && voyNo !== "" ?
            <Modal
              style={{ top: '2%' }}
              title="Report"
              open={isVisible}
              onCancel={onCancel}
              width="95%"
              footer={null}
            >
              {
                reportType === "VoyagePerformanceReport" ? <VoyagePerformanceReport type="Voyage Performance Report" voyNo={voyNo} sumData={dumpData.data} data={reportdata} /> : reportType === "ConsolidateReport" ? 
                <ConsolidateReport type="Consolidate Voyage Report" voyNo={voyNo} sumData={dumpData.data} data={reportdata} /> : reportType === "EmissionReport" ? <EmissionReport voyNo={voyNo} sumData={dumpData.data} data={reportdata} /> : null
              }
            </Modal>
          : undefined}

          {/* column filtering show/hide */}
          {sidebarVisible ? (
            <SidebarColumnFilter
              columns={columns}
              sidebarVisible={sidebarVisible}
              callback={(e) => callOptions(e)}
            />
          ) : null}
        </div>
      </>
    );
  }

export default ReportList;
