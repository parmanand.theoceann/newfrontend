import { Col, Modal, Row, Button, Input, Select, InputNumber } from "antd";
import React, { useEffect, useState } from "react";
import { CII_REF_METHODOLOGY_VAL, getNumberValue, VESSEL_CATEGORY, YEARS } from "./constants";


const InputNumberWrapper = ({
  onChange,
  name,
  value,
  disabled
}) => {

  const handleChangeNumber = (val) => {
    if (onChange) {
      onChange(val, name);
    }
  }

  return (
    <InputNumber
      name={name}
      onChange={handleChangeNumber}
      value={value}
      disabled={disabled}
    />
  )
}

const getCategoryViseData = (categotry, deadWeight = 0) => {
  if (!categotry || !CII_REF_METHODOLOGY_VAL[categotry]) {
    return {}
  }

  let data = CII_REF_METHODOLOGY_VAL[categotry];

  if (categotry === "BULK_CARRIER") {
    data = deadWeight >= 279000 ? data.dwtMoreThen279k : data.dwtLessThen279k;
  } else if (categotry === "GENERAL_CARGO_SHIP") {
    data = deadWeight >= 20000 ? data.dwtMoreThen20k : data.dwtLessThen20k;
  }


  if (data.capacity === "DEAD_WEIGHT") {
    data.capacity = deadWeight;
  }

  return data;
}


const AddCiiModal = ({
  onSubmit
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [formValues, setFormValues] = useState({});
  const [errors, setErrors] = useState([]);
  const [ciiRefer, setCiiRefer] = useState("");
  const categoryData = getCategoryViseData(formValues.category || "", formValues.deadWeight)

  useEffect(() => {
    if (isOpen) {
      setFormValues({});
      setErrors([]);
    }
  }, [isOpen]);

  useEffect(() => {
    getCiiRefer();
  }, [formValues]);

  const checkError = () => {
    const errArray = [];
    if (!formValues.category) {
      errArray.push("Select Vessel category");
    }

    setErrors(errArray);
    return errArray;
  }

  const handleSubmit = () => {
    setErrors([]);
    const hasError = checkError().length;
    if (hasError) {
      return;
    }

    const vesselitem = VESSEL_CATEGORY.find(i => i.value === formValues.category) || {}

    onSubmit({
      ...formValues,
      category: vesselitem.label,
      categoryVal: formValues.category,
      refCii: ciiRefer
    });
    handleCancel();
  }

  const openModal = () => {
    setIsOpen(true)
  }

  const handleCancel = () => {
    setFormValues({});
    setErrors([]);
    setIsOpen(false)
  }

  const getCiiRefer = () => {
    const ciiReferVal = categoryData.aFector * (Math.pow(formValues.deadWeight, -0.622));
    const val = !Number.isNaN(ciiReferVal) && Number.isFinite(ciiReferVal) ? getNumberValue(ciiReferVal) : "";
    setCiiRefer(val);
  }

  const handleChangeText = (event) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value
    });
  }

  const handleChangeNumber = (value, name) => {
    setFormValues({
      ...formValues,
      [name]: value
    });
  }

  const handleChangeSelect = (value, name) => {
    setFormValues({
      ...formValues,
      [name]: value
    });
  }

  return (
    <div>
      <div className="port-search-button" tour-id='voy-port-search'>
        <Button
          id="port-search-button" data-hide-on-download="true"
          type="primary" onClick={openModal}>Add Vessel</Button>
      </div>
      <Modal
        title="Add Vessel"
       open={isOpen}
        onCancel={handleCancel}
        width={600}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Close
          </Button>,
          <Button
            data-hide-on-download="true"
            type="primary"
            className='add-btn'
            style={{ display: "inline-flex", marginLeft: "20px" }}
            onClick={handleSubmit}>Submit
          </Button>]}
      >
        <Row>
          <Col span={24}>
            <Row>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Vessel Name </label>
                  <div>
                    <Input
                      name="vesselName"
                      onChange={handleChangeText}
                      value={formValues.vesselName}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Type </label>
                  <div>
                    <Input
                      name="type"
                      onChange={handleChangeText}
                      value={formValues.type}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Year </label>
                  <div>
                    <Select
                      name="year"
                      onChange={(item) => handleChangeSelect(item, "year")}
                      value={formValues.year}
                      options={YEARS}
                    >
                      {
                        YEARS.map(i =>
                          <Select.Option value={i.value}>{i.label}</Select.Option>
                        )
                      }
                    </Select>
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Deadweight </label>
                  <div>
                    <InputNumberWrapper
                      name="deadWeight"
                      onChange={handleChangeNumber}
                      value={formValues.deadWeight}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Displacement </label>
                  <div>
                    <InputNumberWrapper
                      name="displacement"
                      onChange={handleChangeNumber}
                      value={formValues.displacement}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Vessel category </label>
                  <div>
                    <Select
                      name="category"
                      onChange={(item) => handleChangeSelect(item, "category")}
                      value={formValues.category}
                      options={VESSEL_CATEGORY}
                    >
                      {
                        VESSEL_CATEGORY.map(i =>
                          <Select.Option value={i.value}>{i.label}</Select.Option>
                        )
                      }
                    </Select>
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>IMO No </label>
                  <div>
                    <InputNumberWrapper
                      name="imoNo"
                      onChange={handleChangeNumber}
                      value={formValues.imoNo}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>EEDI (g.co2/TonMiles) </label>
                  <div>
                    <InputNumberWrapper
                      name="eedi"
                      onChange={handleChangeNumber}
                      value={formValues.eedi}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Main Propulsion (kw) </label>
                  <div>
                    <InputNumberWrapper
                      name="propulsion"
                      onChange={handleChangeNumber}
                      value={formValues.propulsion}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>A/E engine (kw) </label>
                  <div>
                    <InputNumberWrapper
                      name="aeEngine"
                      onChange={handleChangeNumber}
                      value={formValues.aeEngine}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>LSMGO CO2 factor (T/ t fuel) </label>
                  <div>
                    <InputNumberWrapper
                      name="lsmgoCo2Fector"
                      onChange={handleChangeNumber}
                      value={formValues.lsmgo}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>ULSFO Co2 Factor (T/t Fuel) </label>
                  <div>
                    <InputNumberWrapper
                      name="ulsfoCo2Fector"
                      onChange={handleChangeNumber}
                      value={formValues.ulsfo}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>VLSFO CO2 Factor (T/t fuel) </label>
                  <div>
                    <InputNumberWrapper
                      name="vlsfoCo2Fector"
                      onChange={handleChangeNumber}
                      value={formValues.vlsfo}
                    />
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={11}>
                <div className="form-field-set">
                  <label>Gross Tonnage </label>
                  <div>
                    <InputNumberWrapper
                      name="grossTonnage"
                      onChange={handleChangeNumber}
                      value={formValues.grossTonnage}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>Net tonnage </label>
                  <div>
                    <InputNumberWrapper
                      name="netTonnage"
                      onChange={handleChangeNumber}
                      value={formValues.netTonnage}
                    />
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={11}>
                <div className="form-field-set">
                  <label>Refer year for CII </label>
                  <div>
                    <InputNumberWrapper
                      name="referYear"
                      onChange={handleChangeNumber}
                      value={formValues.referYear}
                    />
                  </div>
                </div>
              </Col>

              <Col span={11}>
                <div className="form-field-set">
                  <label>CII Reference </label>
                  <div>
                    <InputNumberWrapper
                      name="ciiRefer"
                      value={ciiRefer}
                      disabled={true}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            {
              errors.map((msg, index) =>
                <div className="error-block" key={index}>{msg}</div>
              )
            }
          </Col>
        </Row>
      </Modal>
    </div>
  )
}

export default AddCiiModal;
