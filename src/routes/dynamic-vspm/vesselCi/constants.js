
export const VESSEL_CATEGORY = [
  { label: "Bulk carrier", value: "BULK_CARRIER" },
  { label: "Tanker", value: "TANKER" },
  { label: "Container", value: "CONTAINER" },
  { label: "General cargo ship", value: "GENERAL_CARGO_SHIP" },
];

// const currentYear = new Date().getFullYear();
export const YEARS = [
  { label: "None" },
  { label: "2023", value: 2023 },
  { label: "2024", value: 2024 },
  { label: "2025", value: 2025 },
  { label: "2026", value: 2026 },
];

export const TO_FIXED_2_NUM = 2;
export const getNumberValue = (value) => {
  value = Number(value).toFixed(TO_FIXED_2_NUM);

  return value;
}

export const CII_REF_METHODOLOGY_VAL = {
  BULK_CARRIER: {
    dwtMoreThen279k: {
      capacity: 279000,
      aFector: 4745,
      cFector: 0.622,
      d1: 0.86,
      d2: 0.94,
      d3: 1.06,
      d4: 1.18,
    },
    dwtLessThen279k: {
      capacity: "DEAD_WEIGHT",
      aFector: 4745,
      cFector: 0.622,
      d1: 0.86,
      d2: 0.94,
      d3: 1.06,
      d4: 0,
    }
  },
  TANKER: {
    capacity: "DEAD_WEIGHT",
    aFector: 5247,
    cFector: 0.61,
    d1: 0.82,
    d2: 0.93,
    d3: 1.08,
    d4: 1.28,
  },
  CONTAINER: {
    capacity: "DEAD_WEIGHT",
    aFector: 1984,
    cFector: 0.489,
    d1: 0.83,
    d2: 0.94,
    d3: 1.07,
    d4: 1.19,
  },
  GENERAL_CARGO_SHIP: {
    dwtMoreThen20k: {
      capacity: "DEAD_WEIGHT",
      aFector: 31948,
      cFector: 0.792,
      d1: 0.83,
      d2: 0.94,
      d3: 1.06,
      d4: 1.19,
    },
    dwtLessThen20k: {
      capacity: "DEAD_WEIGHT",
      aFector: 588,
      cFector: 0.3885,
      d1: 0.83,
      d2: 0.94,
      d3: 1.06,
      d4: 0,
    }
  },
}

