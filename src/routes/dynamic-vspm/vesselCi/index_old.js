import React, { useEffect, useState } from "react";
import { Row, Col, Table, Button, Modal, InputNumber } from "antd";
import data from "./data";
import AddCiiModal from './addCiiModal';
import PaiChart from "./paiChart";
import { getNumberValue } from "./constants";


const tableColumns = [
  { title: "Vessel/Voyage", dataIndex: "vesselName", },
  { title: "Category", dataIndex: "category", },
  { title: "DWT", dataIndex: "deadWeight", },
  // { title: "VLSFO", dataIndex: "vlsfo", isLock: true },
  // { title: "ULSFO", dataIndex: "ulsfo", isLock: true },
  // { title: "LSMGO", dataIndex: "lsmgo", isLock: true },
  // { title: "Distance", dataIndex: "distance", isLock: true },
  { title: "VLSFO", dataIndex: "vlsfo", isEditable: true, type: "numberInput" },
  { title: "ULSFO", dataIndex: "ulsfo", isEditable: true, type: "numberInput" },
  { title: "LSMGO", dataIndex: "lsmgo", isEditable: true, type: "numberInput" },
  { title: "Distance", dataIndex: "distance", isEditable: true, type: "numberInput" },
  { title: "Co2 Estimate", dataIndex: "co2Estimate", isLock: false },
  { title: "Attained CII YTD", dataIndex: "attainedCiiYtd", },
  { title: "Ref CII", dataIndex: "refCii", },
  { title: "CII 2023 Proj.", dataIndex: "cii2023", isRatingColor: true },
  { title: "CII 2024 Proj.", dataIndex: "cii2024", isRatingColor: true },
  { title: "CII 2025 Proj.", dataIndex: "cii2025", isRatingColor: true }
];

const getCo2Estimate = (record) => {
  const total = Number(record.vlsfo) * Number(record.vlsfoCo2Fector) + Number(record.ulsfo) * Number(record.ulsfoCo2Fector) + Number(record.lsmgo) * Number(record.lsmgoCo2Fector)

  return Number.isFinite(total) && !Number.isNaN(total) ? getNumberValue(total) : "";
}

const getAttainedCiiYtd = (record) => {
  const total = (Number(record.co2Estimate) / (Number(record.distance) * Number(record.deadWeight))) * Math.pow(10, 6);

  return Number.isFinite(total) && !Number.isNaN(total) ? getNumberValue(total) : "";
}

const VesselCiList = ({
  isOpen,
  onClose
}) => {
  const [tableData, setTableData] = useState(data);

  useEffect(() => {
    updateTableDataWithFooter(data);
  }, []);

  const updateTableDataWithFooter = (updatedData = []) => {
    setTableData(updatedData);
  }

  const showModal = () => {
    // CommonModals({
    //   type: "warning",
    //   title: "Warning!",
    //   content: "Please buy Ocean plan!",
    //   closeok: () => { }
    // });
  }

  const getRowCalculations = (record) => {
    const co2Estimate = getCo2Estimate(record);
    let updatedRecord = { ...record, co2Estimate };
    updatedRecord.attainedCiiYtd = getAttainedCiiYtd(updatedRecord);

    return { ...updatedRecord };
  }


  const handleChangeValue = (value, record, name) => {
    record[name] = value;
    const updatedRecord = getRowCalculations(record);

    let updatedData = tableData.map(item => {
      if (item.id === record.id) {
        return {
          ...item,
          ...updatedRecord,
        }
      }

      return item;
    });

    updateTableDataWithFooter(updatedData);

  }

  const editableInputNumber = (text, record, name, { isPercentage } = {}) => {
    if (record.isFooterCol) {
      return " ";
    }

    const otherProp = {};
    if (isPercentage) {
      otherProp.min = 0;
      otherProp.max = 100;
    }

    return (
      <InputNumber
        {...otherProp}
        style={{ width: '100px' }}
        value={Number(text)}
        onChange={(val) => {
          handleChangeValue(val, record, name);
        }}
        min={0}
      />
    );
  }

  const columnConfiguration = (columns) => {

    return columns.map((item) => {
      if (item.isRatingColor) {
        return {
          ...item,
          render: (text) => {
            return (
              <div className={`cii-rating-cell rating-col rating-color-${text && text.toLowerCase()}`}>
                {text}
              </div>
            )
          }
        }
      }

      if (item.isLock) {
        return {
          ...item,
          render: () => {
            return <i className="fas fa-lock yellow-icon-color" onClick={showModal} />
          }
        }
      }

      if (item.isEditable && item.type === 'numberInput') {
        return {
          ...item,
          render: (text, record) => record.isFooterCol ? text : editableInputNumber(text, record, item.dataIndex, item)
        }
      }

      return {
        ...item,
        render: (text) => text
      }
    })
  }

  const handleCancel = () => {
    onClose()
  }

  const handleAddVessel = (data) => {
    setTableData([
      ...tableData,
      getRowCalculations(data)
    ]);
  }

  return (
    <Modal
      className="ci-table-block"
      title="CII"
      open={isOpen}
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={handleCancel}>
          Close
        </Button>,
      ]}
      width={1024}
    >
      <PaiChart />
      <Row>
        <Col span={24}>
          {/* <AddCiiModal onSubmit={handleAddVessel} /> */}
          <div className="cii-table">
            <Table
              columns={columnConfiguration(tableColumns)}
              dataSource={tableData}
              rowKey="id"
              pagination={false}
              align={"left"}
            />
          </div>
        </Col>
      </Row>
    </Modal>
  )
}

export default VesselCiList;
