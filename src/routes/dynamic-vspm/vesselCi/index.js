import { Suspense, lazy, useEffect, useLayoutEffect, useState } from "react";
import NormalFormIndex from "../../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  useStateCallback,
} from "../../../shared";
import { Alert, Button, DatePicker, Input, Select, Spin } from "antd";
import DoughNutChart from "../../dashboard/charts/DoughNutChart";
import ClusterColumnChart from "../../dashboard/charts/ClusterColumnChart";
import moment from "moment";
const Option = Select.Option;
const VesselCiiBoard = () => {
  const [state, setState] = useState({
    frmName: "cii_details_form",
    formdata: {
      ciidetails: ""
    },
    frmVisible: false,
    vessel_name: [],
    voyage_number: [],
    filterData: {
      commenceDate: "",
      completedDate: "",
      vesselName: "",
      voyageNumber: ""
    },
    EmissionchartDataRef:null,
    ciibandchartDataRef:null,
    topCO2Emissions:null
  });
  useLayoutEffect(() => {
    getVesselConsumption()
  }, [])
  useEffect(() => {
    setState({
      ...state,
      chartdata: { ...state.chartdata, },
    });

  }, []);

  const getVesselConsumption = async (payload) => {
    
    setState((pre) => ({
      ...pre,
      formdata: {
        ciidetails: ""
      },
      frmVisible: false,
    }))
    try {
      let suURL = `${URL_WITH_VERSION}/voyage-manager/voyage-consp`;
      let suMethod = "POST";
      await postAPICall(suURL, payload ?? null, suMethod, (data) => {
        const respdata = data.data;
        setState((pre) => ({
          ...pre,
          formdata: {
            ciidetails: respdata
          },
          frmVisible: true
        }))
      })
    } catch (error) {
      console.error('Error fetching data:', error);
    }

  };

 
  useEffect(() => {
    if (state.frmVisible && state.formdata.ciidetails.length > 0) {
      EmissionGraphData();
      ciibandData();
            TotalTopCO2Emissions()
      ListApiCall()
    }
  }, [state.frmVisible, state.formdata]);

  const ListApiCall = async () => {
    let qParams = { p: 1, l: 0 };
    let _url = `${URL_WITH_VERSION}/voyage-manager/list?${new URLSearchParams(qParams)}`;
    const response = await getAPICall(_url);
    const respData = await response

    const voyageNumbers = [];
    const uniqueVesselNames = new Set(); // Using a Set to collect unique vessel names

    respData?.data?.forEach(item => {
      voyageNumbers.push(item.voyage_number);
      uniqueVesselNames.add(item.vessel_name); // Adding vessel names to the Set
    });

    const vesselNames = [...uniqueVesselNames]; // Converting Set back to array

    setState(prevState => ({
      ...prevState,
      vessel_name: vesselNames,
      voyage_number: voyageNumbers
    }));
  };

  const ciibandData = () => {
    let acii_band = 0;
    let bcii_band = 0;
    let ccii_band = 0;
    let dcii_band = 0;
    let ecii_band = 0;
    state?.formdata?.ciidetails?.forEach(item => {
      if (item.cii_band === "A") {
        acii_band++;
      } else if (item.cii_band === "B") {
        bcii_band++;
      } else if (item.cii_band === "C") {
        ccii_band++;
      } else if (item.cii_band === "D") {
        dcii_band++;
      } else if (item.cii_band === "E") {
        ecii_band++;
      }
    });

    const chartDataciibandData = [
      { value: acii_band, name: "A" },
      { value: bcii_band, name: "B" },
      { value: ccii_band, name: "C" },
      { value: dcii_band, name: "D" },
      { value: ecii_band, name: "E" }
    ];
    
    setState((pre)=>({...pre,ciibandchartDataRef:chartDataciibandData}))

  }



  const EmissionGraphData = () => {
    let totalCo2EmissionVu = 0;
    let totalCo2EmissionLm = 0;
    let totalCo2EmissionIfo = 0;

    state?.formdata?.ciidetails?.forEach(item => {
      totalCo2EmissionVu += item.co2_emission_vu || 0;
      totalCo2EmissionLm += item.co2_emission_lm || 0;
      totalCo2EmissionIfo += item.co2_emission_ifo || 0;
    });

    const chartDataEmissionGraph = [
      { value: totalCo2EmissionLm.toFixed(2), name: "Total CO2 Emission For LSMGO & MGO" },
      { value: totalCo2EmissionVu.toFixed(2), name: "Total CO2 Emission For VLSFO & ULSFO" },
      { value: totalCo2EmissionIfo.toFixed(2), name: "Total Co2 Emission IFO " },
    ];
    setState((pre)=>({...pre,EmissionchartDataRef:chartDataEmissionGraph}))
    
  }

  const TotalTopCO2Emissions = () => {
    const sortedData = state?.formdata?.ciidetails?.sort((a, b) => b.co2_emission_total - a.co2_emission_total);
    const topFive = sortedData?.slice(0, 10);
    setState((pre)=>({...pre,topCO2Emissions:topFive}))
    
  }
  const xAxisData = state.topCO2Emissions?.map(item => item.vessel);
  const seriesData = state.topCO2Emissions?.map(item => item.co2_emission_total);

  const handleChange = (field, value) => {
    setState(prevState => ({
      ...prevState,
      filterData: {
        ...prevState.filterData,
        [field]: value
      }
    }));
  };
  const handleReset = () => {
    setState(prevState => ({
      ...prevState,
      filterData: {
        commenceDate: "",
        completedDate: "",
        vesselName: '',
        voyageNumber: ''
      }
    }));
    getVesselConsumption()
  }
  const handleSubmit = async () => {

    getVesselConsumption(state?.filterData);

  };
  return (
    <>
      {state?.formdata.ciidetails.length > 0 && state.ciibandchartDataRef?.length > 0 ?
        <div>
          <div className="d-flex gap-1">
            <div className="d-flex mb-2 ">
              <div className="d-flex align-items-center ">
                <p className="text-center mb-0">Commence date:</p>
                <DatePicker
                  className="form-control"
                  style={{ width: "150px", padding: "3px" }}
                  picker="date"
                  format="YYYY-MM-DD HH:mm:ss"
                  onChange={(date, dateString) => {
                    
                    handleChange("commenceDate", dateString);
                  }}
                  value={state?.filterData?.commenceDate ? moment(state?.filterData?.commenceDate, "YYYY-MM-DD HH:mm:ss") : ""}
                />
              </div>
              <div className="d-flex align-items-center ">
                <p className="text-center mb-0">Completed date:</p>

                <DatePicker
                  className="form-control"
                  style={{ width: "150px", padding: "3px" }}
                  picker="date"
                  format="YYYY-MM-DD HH:mm:ss"
                  onChange={(date, dateString) => {
                    handleChange("completedDate", dateString);
                  }}
                  value={state?.filterData?.completedDate ? moment(state?.filterData?.completedDate, "YYYY-MM-DD HH:mm:ss") : ""}
                />
              </div>
              <div className="d-flex align-items-center ">
                <p className="text-center mb-0">Vessel name:</p>
                <Select
                  className="form-control rounded"
                  style={{ width: "150px", padding: "3px" }}
                  value={state?.filterData?.vesselName || undefined}
                  onChange={(value) => handleChange('vesselName', value)}
                  placeholder="Select value"
                  bordered={false}
                >
                  {state?.vessel_name?.map((vesselName, index) => (
                    <Option key={index} value={vesselName}>{vesselName}</Option>
                  ))}
                </Select>
              </div>

              <div className="d-flex align-items-center ">
                <p className="text-center mb-0">Voyage No:</p>
                <Select
                  className="form-control rounded"
                  style={{ width: "150px" }}
                  value={state?.filterData?.voyageNumber || undefined}
                  onChange={(value) => handleChange('voyageNumber', value)}
                  placeholder="Select value"
                  bordered={false}
                >
                  {state.voyage_number.map((voyageNumber, index) => (
                    <Select.Option key={index} value={voyageNumber}>{voyageNumber}</Select.Option>
                  ))}
                </Select>
              </div>

            </div>
            <div className="d-flex">
              <Button className="mb-2" onClick={handleSubmit}>Submit</Button>
              <Button className="mb-2" onClick={handleReset}>Reset All</Button>
            </div>
          </div>
          <article className="article toolbaruiWrapper">
            <div className="box box-default">
              <div className="box-body">
                <div className="row">
                  <div className="col-md-4" >

                    <DoughNutChart DoughNutChartSeriesData={state.ciibandchartDataRef}
                      DoughNutChartSeriesRadius={["40%", "70%"]}
                      Heading={"YTD Fleet Ratings"}
                      DoughNutChartSeriesName={"Fleet Ratings"}
                    />
                  </div>
                  <div className="col-md-8">

                    <Suspense fallback="Loading..">
                      <DoughNutChart
                        DoughNutChartSeriesData={state.EmissionchartDataRef}
                        DoughNutChartSeriesRadius={["40%", "70%"]}
                        Heading={"Emission Graph"}
                        DoughNutChartSeriesName={"Emission Graph"}
                        legendhorizontal={"horizontal"}
                      />
                    </Suspense>
                  </div>
                </div>
              </div>
            </div>
          </article>

          <article className="article toolbaruiWrapper mt-2">
            <div className="box box-default">
              <div className="box-body">
                <div className="row">
                  <div className="col-md-12">
                    <Suspense fallback="Loading..">
                      <ClusterColumnChart
                        Heading={"Total Co2 Emissions"}
                        ClusterDataxAxis={xAxisData}
                        ClusterDataSeries={[
                          {
                            name: "Total Co2 Emissions",
                            type: "bar",
                            data: seriesData,
                          }
                        ]}
                        minValueyAxis={"1000"}
                        maxValueyAxis={"100000"}
                      />
                    </Suspense>
                  </div>
                  <div className="col-md-6">
                  </div>
                </div>
              </div>
            </div>
          </article>

          <div className="body-wrapper">
            <article className="article">
              <div className=" box-default">
                <div className="box-body">
                  {state.frmVisible && (
                    <NormalFormIndex
                      key={"key_" + state.frmName + "_0"}
                      formClass="label-min-height"
                      formData={state?.formdata}
                      showForm={true}
                      frmCode={state.frmName}
                      addForm={true}
                      inlineLayout={true}
                      isShowFixedColumn={["CII Details"]}
                    />
                  )}
                </div>

              </div>
            </article>
          </div>
        </div>

        : <div className="col col-lg-12">
        <Spin tip="Loading...">
          <Alert
            message=" "
            description="Please wait..."
            type="info"
          />
        </Spin>
      </div>

      }
    </>
  );
};

export default VesselCiiBoard;
