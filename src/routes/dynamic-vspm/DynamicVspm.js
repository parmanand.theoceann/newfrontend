import React, { Component } from "react";
import { Modal, Icon, Menu, Dropdown, Tooltip, Spin } from "antd";
import ShipDetail from "./ShipDetail";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import DynamicMap from "./DynamicMap";
import URL_WITH_VERSION, {
  getAPICall,
  IMAGE_PATH,
  openNotificationWithIcon,
} from "../../shared";
// import LiveVesselSearch from './liveVesselSearch';
import VesselCiiBoard from "./vesselCi";
import VesselDetails from "./vesselDetails";
import {
  ProfileOutlined,
  FileProtectOutlined,
  RiseOutlined,
  HistoryOutlined,
  AppstoreOutlined,
  MonitorOutlined,
  FileExcelOutlined,
  ContainerOutlined,
} from "@ant-design/icons";

const voyagehistory = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/noon-verification">
        Noon verification
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/voyage-history-list">
        Voyage Historical Data
      </a>
    </Menu.Item>
  </Menu>
);

const spdconsdynamic = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/overall-performance-analysis">
        Overall Performance Analysis
      </a>
    </Menu.Item>

    {/* <Menu.Item>
      <a rel="noopener noreferrer" href="#/fuel-cons-analysis-vessel">
        Fuel Cons. Analysis Vessel
      </a>
    </Menu.Item> */}
  </Menu>
);

const activatevspm = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="#/vessel-activate-link">
        Vessel activate link list
      </a>
    </Menu.Item>

    <Menu.Item>
      <a rel="noopener noreferrer" href="#/reporting-form">
        Reporting form
      </a>
    </Menu.Item>
  </Menu>
);

const AnyReactComponent = ({ text }) => (
  <div
    style={{
      color: "white",
      padding: "15px 10px",
      display: "inline-flex",
      textAlign: "center",
      alignItems: "center",
      justifyContent: "center",
      transform: "translate(-50%, -50%)",
    }}
  >
    <img
      alt="Ship"
      src={IMAGE_PATH + "icons/ship"}
      height="30"
      className="map-ship2"
      onClick={() => text(true, "ShipDetails1")}
      // onMouseOver={()=>console.log("mouse over")}
      // onMouseLeave={()=>console.log("leave")}
    />
  </div>
);

class DynamicVspm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modals: {
        ShipDetails1: false,
      },
      title: `vessel Name:VSL1412211\nlat:18.5204\nlng:73.8567`,
      mapSettings: {
        center: { lat: 20.5937, lng: 78.9629 },
        zoom: 4,
      },
      latlng: [],
      shipData: null,
      vessel_name: "Spring",
      loading: false,
      // isShowVesselModal: false,
      isShowVesselCiiModal: false,
      isShowVesselDetailsModal: false,
    };
  }
  componentDidMount = async () => {
    this.fetchData();
  };

  fetchData = async () => {
    let array = [];
    try {
      let headers = { order_by: { id: "desc" } };
      let _url = `${URL_WITH_VERSION}/noon-verification/list?p=1&l=20`;
      const request = await getAPICall(_url, headers);
      const response = request["data"];

      if (response) {
        response &&
          response.length > 0 &&
          response.map((data) => {
            if (data)
              array.push({
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: [
                    data.position.split(",")[0],
                    data.position.split(",")[1].split(" ")[1],
                  ],
                },
                properties: {
                  title: data.vessel_name,
                  rotation: 0,
                },
              });
          });
      }
      this.setState({
        ...this.state,
        latlng: array,
        loading: true,
      });
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
      this.setState({
        ...this.state,
        latlng: array,
        loading: false,
      });
    }
  };

  showHideModal = (visible, modal, id) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
      shipData: id,
    });
  };

  // openVesselModal = (event) => {
  //   event && event.preventDefault();
  //   this.setState({ isShowVesselModal: true });
  // }

  // closeVesselModal = () => {
  //   this.setState({ isShowVesselModal: false });
  // }

  openVesselCiiModal = (key) => {
    this.setState({ ...this.state, isShowVesselCiiModal: key });
  };

  openVesselDetailsModal = (event) => {
    event && event.preventDefault();
    this.setState({ isShowVesselDetailsModal: true });
  };

  closeVesselDetailsModal = () => {
    this.setState({ isShowVesselDetailsModal: false });
  };

  setSelectedVesselData = (selectedVessel) => {};

  render() {
    const { shipData, mapSettings, loading } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-md-1" >
                  <div className="p-2 dynamic-vspm-left-menu">
                    <ul
                      className="p-0"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "12px",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <li
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <a
                          href="#/vessel-activate-link"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <img
                              style={{ width: "16px" }}
                              src={IMAGE_PATH + "svgimg/addvessel.svg"}
                              alt=""
                            />
                          </div>
                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            Activate VSD
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          href="#/noon-verification"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <FileProtectOutlined style={{ fontSize: "16px" }} />
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            Noon Verfication List
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          href="#/voyage-history-list"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <HistoryOutlined style={{ fontSize: "16px" }} />
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            Voyage Historical List
                          </div>
                        </a>
                      </li>
                      <li>
                        <a
                          href="#/voyage-rejected-list"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <FileExcelOutlined
                              style={{ fontSize: "16px" }}
                            />{" "}
                          </div>
                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            Voyage Rejection List{" "}
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          href="#/track-by-noon"
                          // onClick={this.openVesselDetailsModal}
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <ProfileOutlined style={{ fontSize: "16px" }} />
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            Track by Noon Report{" "}
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          href="#/overall-performance-analysis"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <MonitorOutlined
                              style={{ fontSize: "16px" }}
                            />{" "}
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            Voyage Performance Analysis
                          </div>
                        </a>
                      </li>

                      <li>
                        {/* <a href="#/">Voyage optimisation</a> */}
                        <a
                          href="- #/voyage-optimization"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <RiseOutlined style={{ fontSize: "16px" }} />{" "}
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            Voyage Optimization{" "}
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          onClick={() => this.openVesselCiiModal(true)}
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          {/* <AppstoreOutlined style={{fontSize:"24px"}}/> */}
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <img
                              style={{ width: "20px" }}
                              src={IMAGE_PATH + "svgimg/cii.svg"}
                              alt="cii"
                            />{" "}
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            CII Dashboard{" "}
                          </div>
                        </a>
                      </li>

                      <li>
                        <a
                          href="#/reporting-form"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <ContainerOutlined
                              style={{ fontSize: "16px" }}
                            />{" "}
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            Report{" "}
                          </div>
                        </a>
                      </li>
                      <li>
                        <a
                          href="#/report-list"
                          style={{
                            textDecoration: "none",
                            color: "white",
                            display: "grid",
                            gridTemplateRows: "24px 1fr",
                            gap: "1px",
                            justifyContent: "center",
                            alignItems: "center",
                            lineHeight: "normal",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <ContainerOutlined
                              style={{ fontSize: "16px" }}
                            />{" "}
                          </div>

                          <div
                            style={{ textAlign: "center", fontSize: "11px" }}
                          >
                            {" "}
                            All Report{" "}
                          </div>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-11" >
                  <DynamicMap data={this.state.latlng} />
                </div>
              </div>
            </div>
          </div>
        </article>
        {this.state.modals["ShipDetails1"] ? (
          <Modal
            className="ship-detail-modal"
            style={{ top: "2%" }}
            title={this.state.vessel_name}
            open={this.state.modals["ShipDetails1"]}
            onCancel={() => this.showHideModal(false, "ShipDetails1")}
            width="40%"
            footer={null}
            header={null}
          >
            <ShipDetail
              data={shipData}
              changeTitle={(data) => this.setState({ vessel_name: data })}
            />
          </Modal>
        ) : undefined}

        {this.state.isShowVesselCiiModal ? (
          <Modal
            className="ship-detail-modal"
            style={{ top: "2%" }}
            open={this.state.isShowVesselCiiModal}
            title={"CII Dashboard"}
            onCancel={() => this.openVesselCiiModal(false)}
            width="90%"
            footer={null}
            header={null}
          >
            <VesselCiiBoard />
          </Modal>
        ) : undefined}

        <Modal
          title="Select Vessel and Voyage"
          className="noon-portal-modal"
          onCancel={this.closeVesselDetailsModal}
          footer={null}
          width="60%"
          open={this.state.isShowVesselDetailsModal}
        >
        <VesselDetails />
        </Modal>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyCIZJxl4b3B520rAjPUqIu_YD5FHfiFQ6M",
})(DynamicVspm);
