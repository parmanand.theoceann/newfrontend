// import { URL_WITH_VERSION, apiPostCall } from './apiActions';
import URL_WITH_VERSION from "../../shared";
import axios from "axios";

export const apiPostCall = async (url, data, callback = null) => {
	return await axios({
		method: "POST",
		url: url,
		data: data,
	}).then(response => {
		if (typeof callback === 'function') {
			return callback(response.data);
		} else {
			return response.data;
		}
	});
}

const postVesselData = async (dataset = {}) => {
  if (!dataset.imo_number) {
    return;
  }

  const postData = {
    imo_no: `${dataset.imo_number}`,
    vessel_name: dataset.name,
    speed: dataset.position.speed,
    last_pos_lat: dataset.position.latitude || null,
    last_pos_lon: dataset.position.longitude || null,
    status: dataset.position.nav_status || null,
    current_port_name: dataset.position.location_str || null,
    vessel_last_pos: dataset.vessel_last_pos || null,
    mmsi_number: dataset.mmsi_number || null,
    view: 1,
    my_fleet: 0,
    degree: 0,
    toc_vessel_details: 0,
    deadWeight: null,
    new_name: dataset.name,
    vessel_type: dataset.vt_verbose || null,
    ata: dataset.last_port.ata,
    atd: dataset.last_port.atd,
    last_port: dataset.last_port.name,
    next_port: dataset.next_port.name,
    eta_calc: dataset.next_port.eta_calc,
    destination: dataset.voyage.destination,
    eta: dataset.voyage.eta,
    received: dataset.voyage.received,
  }

  const url = `${URL_WITH_VERSION}/vessel/livedetails/save`;
  await apiPostCall(url, postData, (res) => res);
}

export const getLiveVesselDetail = async (dataset, isNotShowInMap) => {
  const { imo_number, vessel_id, vt_verbose } = dataset

  // const tempSelectedData = { "imo_number": 0, "last_port": { "ata": "2022-12-01T10:24:33Z", "atd": "2022-12-01T10:24:33Z", "locode": "string", "name": "string" }, "mmsi_number": 0, "name": "string", "next_port": { "eta_calc": "2022-12-01T10:24:33Z", "locode": "string", "name": "string", "travel_distance_nm": 0, "travel_time_h": 0 }, "position": { "course_over_ground": 0, "latitude": 0, "location_str": "string", "longitude": 0, "nav_status": "string", "received": "2022-12-01T10:24:33Z", "speed": 0, "true_heading": 0 }, "request_limit_info": { "left_requests": 0, "max_requests": 0, "used_requests": 0 }, "vessel_id": 0, "voyage": { "destination": "string", "draught": 0, "eta": "2022-12-01T10:24:33Z", "received": "2022-12-01T10:24:33Z" } };

  try {
    let url = `${process.env.REACT_APP_VESSEL_DETAILS}/${vessel_id}?apikey=${process.env.REACT_APP_VESSEL_API_KEY}`;

    return await fetch(url, {
      method: "GET",
      headers: {
        "access-control-allow-origin": "*",
        "Accept": "application/json",
        'Content-Type': 'application/json'
      }
    })
      .then((res) => res.json())
      .then((data) => {
        // Save data into DB
        postVesselData({ ...data, vt_verbose });
        // Add to map and update the map
        let mapDataItem = null;
        if (!isNotShowInMap && data.imo_number && data.name) {
          mapDataItem = {
            imo_no: `${data.imo_number}`,
            vessel_name: data.name,
            speed: data.position.speed,
            vessel_lat: data.position.latitude || null,
            vessel_lon: data.position.longitude || null,
            vessel_status: data.position.nav_status || null,
            current_port_name: data.current_port_name || null,
            last_pos_lon: data.position.longitude,
            last_pos_lat: data.position.latitude
          }
        }

        return { data, mapDataItem };
      });

  } catch (error) {
    console.log(error);
    return {};
  }
}

export const getVesselParticulars = async (vesselId) => {
  const url = `${process.env.REACT_APP_VESSEL_NONAIS}/${vesselId}?apikey=${process.env.REACT_APP_VESSEL_API_KEY}`;

  return await fetch(url, {
    method: "GET",
    headers: {
      "access-control-allow-origin": "*",
      "Accept": "application/json",
      'Content-Type': 'application/json'
    }
  }).then((res) => res.json())
}

/* get selected data from api */
// export const getVesselDetail = async (dataset) => {
//   const { vessel_name, current_port_name, imo_no, last_pos_lat, last_pos_lon, speed } = dataset;

//   let tempSelectedData = {
//     "imo_number": imo_no,
//     "last_port": {
//       "ata": "NA",
//       "atd": "NA",
//       "locode": "NA",
//       "name": "NA"
//     },
//     "mmsi_number": "NA",
//     "name": vessel_name,
//     "next_port": null,
//     "position": {
//       "course_over_ground": 188.3,
//       "latitude": last_pos_lat,
//       "location_str": current_port_name,
//       "longitude": last_pos_lon,
//       "nav_status": "NA",
//       "received": "NA",
//       "speed": speed,
//       "true_heading": 10
//     },
//     "vessel_id": "NA",
//     "voyage": {
//       "destination": "NA",
//       "draught": "NA",
//       "eta": "NA",
//       "received": "NA"
//     }
//   };
//   try {
//     await fetch(`${process.env.REACT_APP_URL_NEW}/VesselDetail/find/${imo_no}`)
//       .then((res) => res.json())
//       .then((data) => {
//         tempSelectedData = {
//           "imo_number": imo_no,
//           "last_port": {
//             "ata": "NA",
//             "atd": "NA",
//             "locode": "NA",
//             "name": "NA"
//           },
//           "mmsi_number": data[0].maritimemobileserviceidentitymmsinumber,
//           "name": vessel_name,
//           "next_port": null,
//           "position": {
//             "course_over_ground": 188.3,
//             "latitude": last_pos_lat,
//             "location_str": current_port_name,
//             "longitude": last_pos_lon,
//             "nav_status": data[0].shipstatuss,
//             "received": "NA",
//             "speed": speed,
//             "true_heading": 10
//           },
//           "vessel_id": "NA",
//           "voyage": {
//             "destination": "NA",
//             "draught": data[0].draught,
//             "eta": "NA",
//             "received": "NA"
//           }
//         };

//         return;
//       });
//   } catch (error) {
//     console.log(error);
//   }
// }
