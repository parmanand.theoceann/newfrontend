import React, { useContext, useRef, useState } from "react";
// import CloseOutlined from "@ant-design/icons/CloseOutlined";
import { Modal, Spin, Input } from "antd";
import { getLiveVesselDetail } from './actions';
import { IMAGE_PATH } from "../../shared";

const searchLabel = "Search Vessel by it's Name or IMO Number";
const LiveVesselSearch = ({
  onCloseModal,
  isOpenModal,
  setSelectedVesselData
}) => {
  const [liveSearchList, setLiveSearchList] = useState([]);
  const [searchStatus, setSearchStatus] = useState('');
  const [searchText, setSearchText] = useState('');
  const [loader, setLoader] = useState(false);
  const searchTimer = useRef(null);

  const handleLiveSearchInput = (e) => {
    const val = e.target.value;
    setSearchText(val);
    clearTimeout(searchTimer.current);
    searchTimer.current = setTimeout(() => {
      onLiveSearch(val);
    }, 300);
  }

  const onLiveSearch = async (searchTextVal) => {
    setSearchStatus("Searching your request");
    let url = "";
    if (isNaN(searchTextVal)) {
      if (searchTextVal.length > 3) {
        url = `${process.env.REACT_APP_VESSEL_SEARCH}?apikey=${process.env.REACT_APP_VESSEL_API_KEY}&name=${searchTextVal}`;
      }
    } else {
      url = `${process.env.REACT_APP_VESSEL_SEARCH}?apikey=${process.env.REACT_APP_VESSEL_API_KEY}&imo_number=${searchTextVal}`;
    }

    if (!url) {
      return;
    }

    try {
      setLoader(true);
      await fetch(url, {
        method: "GET",
        headers: {
          "access-control-allow-origin": "*",
          "Accept": "application/json",
          'Content-Type': 'application/json'
        }
      }).then((res) => res.json())
        .then((data) => {
          if (data.status !== "error") {
            if (data.vessels.length > 0) {
              setLiveSearchList(data.vessels)
            } else {
              setSearchStatus("No data found");
            }
          }
          setLoader(false);
          return;
        });
    } catch (error) {
      console.log(error);
      setLiveSearchList([]);
      setSearchStatus(searchLabel);
      setLoader(false);
    }
    setLoader(false);
  }

  const handleClickVesselDetail = async (vesselDetail) => {
    setLoader(true);
    const result = await getLiveVesselDetail(vesselDetail);
    if (result && result.data) {
      setSelectedVesselData(result.data);
    }

    if (result && result.mapDataItem) {
      // dispatch({
      //   type: types.SET_APP_PAYLOAD,
      //   payload: { newVesselMapItem: { ...result.data, ...result.mapDataItem } }
      // });
    }
    setLoader(false);
  }

  return (
    <Modal open={isOpenModal} onCancel={onCloseModal} title="Vessel Search">
      <div className="vessel-search-list">
        <div className="search-input">
          <Input type="text" value={searchText} onChange={handleLiveSearchInput} placeholder="Enter minimum 3 letter vessel name or IMO number" />
        </div>
        <Spin tip="Loading" spinning={loader}>
          <div className="list-data live-search-data-list">
            {
              liveSearchList.length > 0 ? liveSearchList.map((v, index) =>
                <div key={index} className="list-item" onClick={() => handleClickVesselDetail(v)}>
                  <img loading="lazy" src={IMAGE_PATH+"icons/ship"} alt="" />
                  <div className="middle-wrapper">
                    <span className="vessel-name">{v.name}</span>
                    <div className="middle-wrapper-discription">
                      <span>
                        <b>MMSI:</b> {v.mmsi_number}
                      </span>
                      <span>
                        <b>IMO no.:</b> {v.imo_number ? v.imo_number : "NA"}
                      </span>
                    </div>
                  </div>
                  <div className="last-wrapper">
                    <p>live</p>
                  </div>
                </div>
              ) : <div className="data-message">{searchStatus}</div>
            }
          </div>
        </Spin>
      </div>
    </Modal>
  )
}

export default LiveVesselSearch;
