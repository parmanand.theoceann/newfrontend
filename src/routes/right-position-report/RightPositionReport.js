import React, { Component } from 'react';
import { Table, Form, Select  } from 'antd';

const { Column, ColumnGroup } = Table;


const data = [
  {
    key: '1',
    vname: 'Vessel Name',
    vtype: 'Vessel Type',
    rtype: 'Report Type',
    rdatetime: 'Report Date/Time',
    platlong: 'Position (Lat/Long)',
    ospd: 'Ordered SPD',
    rspd: 'Reported SPD',
    change: '% Change',
    odist: 'Observed Dist.',
    edist: 'Eng. Dist.',
    slip: 'Slip %',
    arpm: 'Ave. RPM',
    abhp: 'Ave. BHP',
    shr: 'Steaming Hr',
    displacement: 'Displacement',
    sstate: 'Sea State',
    sheight: 'Swell Height',
    wforce: 'Wind Force',
    current: 'Current',
    bwhr: 'Bad Weather Hr',
    bwdist: 'Bad Weather Dist.',
    mepropulsion: 'M/E Propulsion',
    ocons: 'Over Cons.',
    mane: 'Maneuver',
    ld: 'L/D',
    aengine: 'Aux Engine',
    generator: 'Generator',
    dtg: 'DTG',
    nextport: 'Next Port',
    eta: 'ETA',
    status: 'status',
    action: 'Action',
  },

  {
    key: '2',
    vname: 'Vessel Name',
    vtype: 'Vessel Type',
    rtype: 'Report Type',
    rdatetime: 'Report Date/Time',
    platlong: 'Position (Lat/Long)',
    ospd: 'Ordered SPD',
    rspd: 'Reported SPD',
    change: '% Change',
    odist: 'Observed Dist.',
    edist: 'Eng. Dist.',
    slip: 'Slip %',
    arpm: 'Ave. RPM',
    abhp: 'Ave. BHP',
    shr: 'Steaming Hr',
    displacement: 'Displacement',
    sstate: 'Sea State',
    sheight: 'Swell Height',
    wforce: 'Wind Force',
    current: 'Current',
    bwhr: 'Bad Weather Hr',
    bwdist: 'Bad Weather Dist.',
    mepropulsion: 'M/E Propulsion',
    ocons: 'Over Cons.',
    mane: 'Maneuver',
    ld: 'L/D',
    aengine: 'Aux Engine',
    generator: 'Generator',
    dtg: 'DTG',
    nextport: 'Next Port',
    eta: 'ETA',
    status: 'status',
    action: 'Action',
  },

  {
    key: '3',
    vname: 'Vessel Name',
    vtype: 'Vessel Type',
    rtype: 'Report Type',
    rdatetime: 'Report Date/Time',
    platlong: 'Position (Lat/Long)',
    ospd: 'Ordered SPD',
    rspd: 'Reported SPD',
    change: '% Change',
    odist: 'Observed Dist.',
    edist: 'Eng. Dist.',
    slip: 'Slip %',
    arpm: 'Ave. RPM',
    abhp: 'Ave. BHP',
    shr: 'Steaming Hr',
    displacement: 'Displacement',
    sstate: 'Sea State',
    sheight: 'Swell Height',
    wforce: 'Wind Force',
    current: 'Current',
    bwhr: 'Bad Weather Hr',
    bwdist: 'Bad Weather Dist.',
    mepropulsion: 'M/E Propulsion',
    ocons: 'Over Cons.',
    mane: 'Maneuver',
    ld: 'L/D',
    aengine: 'Aux Engine',
    generator: 'Generator',
    dtg: 'DTG',
    nextport: 'Next Port',
    eta: 'ETA',
    status: 'status',
    action: 'Action',
  },

  {
    key: '4',
    vname: 'Vessel Name',
    vtype: 'Vessel Type',
    rtype: 'Report Type',
    rdatetime: 'Report Date/Time',
    platlong: 'Position (Lat/Long)',
    ospd: 'Ordered SPD',
    rspd: 'Reported SPD',
    change: '% Change',
    odist: 'Observed Dist.',
    edist: 'Eng. Dist.',
    slip: 'Slip %',
    arpm: 'Ave. RPM',
    abhp: 'Ave. BHP',
    shr: 'Steaming Hr',
    displacement: 'Displacement',
    sstate: 'Sea State',
    sheight: 'Swell Height',
    wforce: 'Wind Force',
    current: 'Current',
    bwhr: 'Bad Weather Hr',
    bwdist: 'Bad Weather Dist.',
    mepropulsion: 'M/E Propulsion',
    ocons: 'Over Cons.',
    mane: 'Maneuver',
    ld: 'L/D',
    aengine: 'Aux Engine',
    generator: 'Generator',
    dtg: 'DTG',
    nextport: 'Next Port',
    eta: 'ETA',
    status: 'status',
    action: 'Action',
  },

  {
    key: '5',
    vname: 'Vessel Name',
    vtype: 'Vessel Type',
    rtype: 'Report Type',
    rdatetime: 'Report Date/Time',
    platlong: 'Position (Lat/Long)',
    ospd: 'Ordered SPD',
    rspd: 'Reported SPD',
    change: '% Change',
    odist: 'Observed Dist.',
    edist: 'Eng. Dist.',
    slip: 'Slip %',
    arpm: 'Ave. RPM',
    abhp: 'Ave. BHP',
    shr: 'Steaming Hr',
    displacement: 'Displacement',
    sstate: 'Sea State',
    sheight: 'Swell Height',
    wforce: 'Wind Force',
    current: 'Current',
    bwhr: 'Bad Weather Hr',
    bwdist: 'Bad Weather Dist.',
    mepropulsion: 'M/E Propulsion',
    ocons: 'Over Cons.',
    mane: 'Maneuver',
    ld: 'L/D',
    aengine: 'Aux Engine',
    generator: 'Generator',
    dtg: 'DTG',
    nextport: 'Next Port',
    eta: 'ETA',
    status: 'status',
    action: 'Action',
  },
 
];

class NoonVerification extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        
              
              <div className="row p10">
                <div className="col-md-12">
                  <div className="noonverification-table">
                  <Table
                      bordered
                      dataSource={data}
                      pagination={false}
                      footer={false}
                      scroll={{x: 'max-content' }}
                      rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                    >
                      <Column title="Vessel Name" dataIndex="vname" />
                      <Column title="Vsl Type" dataIndex="vtype" />
                      <Column title="Report Type" dataIndex="rtype" />
                      <Column title="Report Date/Time" dataIndex="rdatetime" />

                      <ColumnGroup title="DIstance & Vessel">
                        <Column title="Position (Lat/Long)" dataIndex="platlong" />
                        <Column title="Ordered SPD" dataIndex="ospd" />
                        <Column title="Reported SPD" dataIndex="rspd" />
                        <Column title="% Change" dataIndex="change" />
                        <Column title="Observed Dist." dataIndex="odist" />
                        <Column title="Eng. dist. " dataIndex="edist" />
                        <Column title="Slip %" dataIndex="slip" />
                        <Column title="Ave. RPM" dataIndex="arpm" />
                        <Column title="Ave. BHP" dataIndex="abhp" />
                        <Column title="Steaming Hr" dataIndex="shr" />
                        <Column title="Displacement" dataIndex="displacement" />
                      </ColumnGroup>

                      <ColumnGroup title="Weather">
                        <Column title="Sea State" dataIndex="sstate" />
                        <Column title="Swell Height" dataIndex="sheight" />
                        <Column title="Wind Force" dataIndex="wforce" />
                        <Column title="Current" dataIndex="current" />
                        <Column title="Bad Weather Hr" dataIndex="bwhr" />
                        <Column title="Bad Weather Dist." dataIndex="bwdist" />
                      </ColumnGroup>

                      <ColumnGroup title="Bunker cons.">
                        <Column title="M/E Propulsion" dataIndex="mepropulsion" />
                        <Column title="Over Cons." dataIndex="ocons" />
                        <Column title="Maneuver" dataIndex="mane" />
                        <Column title="L/D" dataIndex="ld" />
                        <Column title="Aux Engine" dataIndex="aengine" />
                        <Column title="Generator" dataIndex="generator" />
                        <Column title="DTG." dataIndex="dtg" />
                        <Column title="Next Port " dataIndex="nextport" />
                        <Column title="ETA" dataIndex="eta" />
                        <Column title="Status" dataIndex="status" />
                        <Column title="Action" dataIndex="action" />
                      </ColumnGroup>
                    </Table>
                  </div>
                </div>
              </div>
           
      </div>
    );
  }
}

export default NoonVerification;

