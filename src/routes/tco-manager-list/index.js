import React, { useEffect } from 'react';
import { Button, Table,   Modal } from 'antd';
import URL_WITH_VERSION, { postAPICall, getAPICall, objectToQueryStringFunc,  openNotificationWithIcon, ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import TCO from '../../routes/chartering/routes/tco/index'
import { EditOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';


const TCOManagerList = ()=> {
  const [state, setState] = useStateCallback(
    {
      frmName: "tco_form",
      loading: false,
      columns: [],
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      reportformDataValue:{},
      typesearch:{},
      donloadArray: []
    }
  );
  
  //const navigate = useNavigate();

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record.id)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["tco-manager-list"]
        ? FIELDS["tco-manager-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
    
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
     
      }
    
      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }
    
      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }
    

    setState(prev => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/tco/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state['responseData'] = dataArr;
    }
    setState(prev => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };


  const redirectToAdd = async (e, id = null) => {
    let qParams = { "ae": id };
    let qParamString = objectToQueryStringFunc(qParams);
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tco/edit?${qParamString}`);
      const respData = await response['data'];
      setState(prevState =>({...prevState, isAdd: false, formDataValues: respData, isVisible: true}))
    } else {
      setState(prevState =>({...prevState, isAdd: true, isVisible: true}))
    }
  }

  const onCancel = () => {
    setState({ ...state, isAdd: false, isVisible: false }, ()=>getTableData());
  }

  // const _onDelete = (postData) => {
  //   let _url = `${URL_WITH_VERSION}/tco/delete`;
  //   apiDeleteCall(_url, { id: postData.id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon('success', response.message);
  //       setState({ ...state, frmVisible: false }, () => {
  //         setState({
  //           ...state,
  //           formData: formDataValue,
  //           showSideListBar: true,
  //           frmVisible: true,
  //         });
  //       });
  //     } else {
  //       openNotificationWithIcon('error', response.message);
  //     }
  //   });
  // };

/*
  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/tco/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })
  }
*/

const callOptions = (evt) => {
  let _search = {
    searchOptions: evt["searchOptions"],
    searchValue: evt["searchValue"],
  };
  if (
    evt.hasOwnProperty("searchOptions") &&
    evt.hasOwnProperty("searchValue")
  ) {
    let pageOptions = state.pageOptions;

    pageOptions["pageIndex"] = 1;
    setState(
      (prevState) => ({
        ...prevState,
        search: _search,
        pageOptions: pageOptions,
      }),
      () => {
        getTableData(_search);
      }
    );
  } else if (
    evt &&
    evt.hasOwnProperty("actionName") &&
    evt["actionName"] === "reset-serach"
  ) {
    let pageOptions = state.pageOptions;
    pageOptions["pageIndex"] = 1;

    setState(
      (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
      () => {
        getTableData();
      }
    );
  } else if (
    evt &&
    evt.hasOwnProperty("actionName") &&
    evt["actionName"] === "column-filter"
  ) {
    // column filtering show/hide
    let responseData = state.responseData;
    let columns = Object.assign([], state.columns);

    if (responseData.length > 0) {
      for (var k in responseData[0]) {
        let index = columns.some(
          (item) =>
            (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
            (item.hasOwnProperty("key") && item.key === k)
        );
        if (!index) {
          let title = k
            .split("_")
            .map((snip) => {
              return snip[0].toUpperCase() + snip.substring(1);
            })
            .join(" ");
          let col = Object.assign(
            {},
            {
              title: title,
              dataIndex: k,
              key: k,
              invisible: "true",
              isReset: true,
            }
          );
          columns.splice(columns.length - 1, 0, col);
        }
      }
    }

    setState((prevState) => ({
      ...prevState,
      sidebarVisible: evt.hasOwnProperty("sidebarVisible")
        ? evt.sidebarVisible
        : !prevState.sidebarVisible,
      columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
    }));
  } else {
    let pageOptions = state.pageOptions;
    pageOptions[evt["actionName"]] = evt["actionVal"];

    if (evt["actionName"] === "pageLimit") {
      pageOptions["pageIndex"] = 1;
    }

    setState(
      (prevState) => ({ ...prevState, pageOptions: pageOptions }),
      () => {
        getTableData();
      }
    );
  }
};

const onSaveFormData = async (data) => {
    // let qParams = { "frm": "port_information" };
    // let qParamString = objectToQueryStringFunc(qParams);
    let { isAdd } = state;

    if (data && isAdd) {
      let _url = `${URL_WITH_VERSION}/tco/save?frm=tco_form`;
      await postAPICall(`${_url}`, data, 'post', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });

      setState({ ...state, isAdd: false, formDataValues: {} }, () => setState({ ...state, isVisible: false }));
    } else {
      setState({ ...state, isAdd: true, isVisible: true });
    }
  }

  const onEditFormData = async (data) => {
    let { isAdd } = state;

    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/tco/update?frm=tco_form`;
      await postAPICall(`${_url}`, data, 'patch', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });
      setState({ ...state, isAdd: false, formDataValues: {} }, () => setState({ ...state, isVisible: false }));

    } else {
      setState({ ...state, isAdd: true, isVisible: true });

    }
  }

  //resizing function
  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      }); 
    };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onActionDonwload = (downType, pageType) => {
      let params = `t=${pageType}`, cols = [];
      const { columns, pageOptions, donloadArray } = state;  
    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }

    const tableColumns = state.columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

    const match= {isExact: true,params: {},path: "/add-tc-out",url: "/add-tc-out"}

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span>TC OUT List</span></h4>
                </div>
                <div className="action-btn">
                  <Button type="primary" onClick={(e) => redirectToAdd(e)}>Add TCO Manager</Button>
                </div>
              </div>
              <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                {
                  state.loading === false ?
                    <ToolbarUI routeUrl={'tco-manager-list-toolbar'} optionValue={{ 'pageOptions': state.pageOptions, 'columns': state.columns, 'search': state.search }} callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {title:'CSV', event: () => onActionDonwload('csv', 'tco')},
                      {title:'PDF', event: () => onActionDonwload('pdf', 'tco')},
                      {title:'XLS', event: () => onActionDonwload('xlsx', 'tco')}
                    ]}
                    />
                    : undefined
                }
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  components={components}
                  // scroll={{ x: 1200, y: 370 }}
                  scroll={{ x: 'max-content' }}
                  columns={tableColumns}
                  //size="small"
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                />
              </div>
            </div>
          </div>
        </article>
        
        {
          state.isVisible === true ?
            <Modal
              title={(state.isAdd === false ? "Edit" : "Add") + " TCO Form"}
              open={state.isVisible}
              width='95%'
              onCancel={onCancel}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
              maskClosable={false}
            >
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default" style={{ padding: '15px' }}>
                  {
                
                  state.isAdd === false ?
                    <TCO formData={state.formDataValues} reportFormData={state.reportformDataValue}  match={match}  />
                    :
                    <TCO formData={{}}  match={match} />
                  }
                    {/* {
                      isAdd === true ?
                        <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height"
                          showForm={true} frmCode={frmName} addForm={true} showButtons={[
                            { "id": "cancel", "title": "Reset", "type": "danger" },
                            { "id": "save", "title": "Save", "type": "primary", "event": (data) => { onSaveFormData(data) } }
                          ]} inlineLayout={true}
                        />
                        :
                        <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formDataValues}
                          showForm={true} frmCode={frmName} addForm={true} showButtons={[
                            { "id": "cancel", "title": "Reset", "type": "danger" },
                            { "id": "save", "title": "Update", "type": "primary", "event": (data) => { onEditFormData(data) } }
                          ]} inlineLayout={true}
                        />
                    } */}
                  </div>
                </article>
              </div>
            </Modal>
            : undefined
        }
        {/* column filtering show/hide */}
        {
          state.sidebarVisible ? <SidebarColumnFilter columns={state.columns} sidebarVisible={state.sidebarVisible} callback={(e) => callOptions(e)} /> : null
        }
      </div>
    )
}

export default TCOManagerList;