// import React from 'react';

// export default class Popup extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {}
//   }

//   render() {
//     return (
//       <div className="body-wrapper">
//         <article className="article">
//           <div className="box box-default">
//             <div className="box-body">
//               <div className="col-lg-8 row">
//                 <div className="col-lg-4 text-right">
//                   <a onClick={(e) => this.onVesselClick(e)}><label>Vessel Name</label></a>
//                 </div>
//                 <div className="col-lg-8">
//                   <Input placeholder="Basic usage" value={vesselName} />
//                 </div>
//               </div>
//             </div>
//           </div>
//         </article>
//       </div>
//     )
//   }
// }

import React from 'react';
import Autosuggest from '../../shared/Autosuggest';

export default class Complete extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      selectedField: "vessel_name"
    }
  }
  getCallback = (e) => {
    // console.log(e);
  }

  render() {
    return (
      <Autosuggest apiLink='vessel/list?l=0' orderBy="vessel_id" searchBy="vessel_name" onRowSelect={(e) => this.getCallback(e)} />
    );
  }
}