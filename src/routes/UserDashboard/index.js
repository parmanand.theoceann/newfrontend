import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Collapse } from "antd";
import {
  UserOutlined,
  UsergroupAddOutlined,
  AlertOutlined,
  BankOutlined,
} from "@ant-design/icons";
const { Panel } = Collapse;
//const { Option } = Select;

function callback(key) {
  // console.log(key);
}

const genExtra = () => (
  <UserOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra2 = () => (
  <UsergroupAddOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra5 = () => (
  <AlertOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra6 = () => (
  <AlertOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra8 = () => (
  <UserOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra11 = () => (
  <BankOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);
const genExtra1 = () => (
  <h3>
    User <br />
    <span className="span-text">Create, Update, Delete User</span>
  </h3>
);
const genExtra3 = () => (
  <h3>
    Group <br />
    <span className="span-text"> Create User’s Group</span>
  </h3>
);
const genExtra4 = () => (
  <h3>
    Subscription <br />
    <span className="span-text">Maintain & purchase License</span>
  </h3>
);
const genExtra7 = () => (
  <h3>
    Access Right <br />
    <span className="span-text">Assign access rights to user</span>
  </h3>
);
const genExtra9 = () => (
  <h3>
    Admin Role <br />
    <span className="span-text">Admin Details</span>
  </h3>
);
const genExtra10 = () => (
  <h3>
    Main Organization <br />
    <span className="span-text"> Register Own Company</span>
  </h3>
);

const UserDashboard = () => {
  const [expandIconPosition, setExpandIconPosition] = useState("right");

  const onPositionChange = (expandIconPosition) => {
    setExpandIconPosition(expandIconPosition);
  };

  return (
    <>
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <section className="main-user-section">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-4">
                      <Collapse
                        defaultActiveKey={["0"]}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >

                        <Panel header={genExtra1()} key="1" extra={genExtra()}>
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <Link to="/user-list">Add User</Link>
                              </li>
                              <li>
                                <Link to="/user-list">User List</Link>
                              </li>
                              <li>
                                <Link to="/user-list">Delete User</Link>
                              </li>
                              <li>
                                <Link to="/user-list">Update User</Link>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div>

                    <div className="col-4">
                      <Collapse
                        defaultActiveKey={["0"]}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >
                        <Panel header={genExtra3()} key="1" extra={genExtra2()}>
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <Link to="/group-list">Create Group</Link>
                              </li>
                              <li>
                                <Link to="/group-list">Group List</Link>
                              </li>
                              <li>
                                <Link to="/group-list">Update Group</Link>
                              </li>
                              <li>
                                <Link to="/group-list">Delete Group</Link>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div>
                    {/* <div className="col-4">
                      <Collapse
                        defaultActiveKey={['0']}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >
                        <Panel header={genExtra4()} key="1" extra={genExtra5()}>
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <a href="#/company-subscription">User license (10)</a>
                              </li>
                              <li>
                                <a href="#/company-subscription">Upgrade</a>
                              </li>
                              <li>
                                <a href="#/company-subscription">Manage</a>
                              </li>
                              <li>
                                <a href="#/company-subscription">Payment</a>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div> */}

                    {/* <div className="col-4">
                      <Collapse
                        defaultActiveKey={['0']}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >
                        <Panel header={genExtra7()} key="1" extra={genExtra6()}>
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <a href="#/access-right">Module Right</a>
                              </li>
                              <li>
                                <a href="#/access-right">Object Right</a>
                              </li>
                              <li>
                                <a href="#/access-right">Items Rights</a>
                              </li>
                              <li>
                                <a href="#/access-right">Page Rights</a>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div> */}

                    {/* <div className="col-4">
                      <Collapse
                        defaultActiveKey={['0']}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >
                        <Panel header={genExtra9()} key="1" extra={genExtra8()}>
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <a href="#/admin-role">Security Measure</a>
                              </li>
                              <li>
                                <a href="#/admin-role">Create User</a>
                              </li>
                              <li>
                                <a href="#/admin-role">Manage User</a>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div> */}

                    <div className="col-4">
                      <Collapse
                        defaultActiveKey={["0"]}
                        onChange={callback}
                        expandIconPosition={expandIconPosition}
                        className="user-block firstrow-block"
                      >
                        <Panel
                          header={genExtra10()}
                          key="1"
                          extra={genExtra11()}
                        >
                          <div className="user-panel-list">
                            <ul className="unstyled">
                              <li>
                                <Link to="/organization">
                                  Register Main company
                                </Link>
                              </li>
                              <li>
                                <Link to="/organization">Update Comapny</Link>
                              </li>
                              <li>
                                <Link to="/organization">Update Logo</Link>
                              </li>
                              <li>
                                <Link to="/organization">
                                  Update Header/Footer
                                </Link>
                              </li>
                              <li>
                                <Link to="/organization">Bank Details</Link>
                              </li>
                              <li>
                                <Link to="/organization">Show Address List</Link>
                              </li>
                            </ul>
                          </div>
                        </Panel>
                      </Collapse>
                    </div>

                    {/* <div className="col-4">
                      <div className="user-block">
                        <div className="row">
                          <div className="col-md-2 icon_col">
                            <div className="icon colorsBackground">
                              <i className="fas fa-user-tie"></i>
                            </div>
                          </div>
                          <div className="col-md-10 name_col">
                            <div className="name_col_inner">
                              <div className="nameBlock">Agent setup</div>
                              <p className="designation_name">Activate Agent on Port/Ports</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* <div className="col-4">
                      <div className="user-block">
                        <div className="row">
                          <div className="col-md-2 icon_col">
                            <div className="icon colorsBackground">
                              <i className="fas fa-shield-alt"></i>
                            </div>
                          </div>
                          <div className="col-md-10 name_col">
                            <div className="name_col_inner">
                              <div className="nameBlock">Security</div>
                              <p className="designation_name">System Security Access</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                    <div className="col-4">
                      <a href="#/notification">
                        <div className="user-block">
                          <div className="row">
                            <div className="col-md-2 icon_col">
                              <div className="icon colorsBackground">
                                <i className="fas fa-bell"></i>
                              </div>
                            </div>
                            <div className="col-md-10 name_col">
                              <div className="name_col_inner">
                                <div className="nameBlock">Notification</div>
                                <p className="designation_name">
                                  Set Up Email Notification
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    {/* <div className="col-4">
                      <div className="user-block">
                        <div className="row">
                          <div className="col-md-2 icon_col">
                            <div className="icon colorsBackground">
                              <i className="fas fa-file-alt"></i>
                            </div>
                          </div>
                          <div className="col-md-10 name_col">
                            <div className="name_col_inner">
                              <div className="nameBlock">Reports</div>
                              <p className="designation_name">Analysis of Users</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="user-block">
                        <div className="row">
                          <div className="col-md-2 icon_col">
                            <div className="icon colorsBackground">
                              <i className="fas fa-file-alt"></i>
                            </div>
                          </div>
                          <div className="col-md-10 name_col">
                            <div className="name_col_inner">
                              <div className="nameBlock">Support</div>
                              <p className="designation_name">Support</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
              </section>
            </div>
          </div>
        </article>
      </div>
    </>
  );
};
export default UserDashboard;
