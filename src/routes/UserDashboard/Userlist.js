import React, { useEffect } from 'react';
import { Table, Modal, Button } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  openNotificationWithIcon,
  apiDeleteCall,
  objectToQueryStringFunc,
  ResizeableTitle,
  useStateCallback,
} from '../../shared';
import {DeleteOutlined, EditOutlined,LeftCircleOutlined, PlusOutlined, SaveOutlined} from "@ant-design/icons";
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import { FIELDS } from '../../shared/tableFields';
import AddNewUserReport from './AddNewUserReport';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter'

const  UserList = (props) => {

  const [state, setState] = useStateCallback({
    loading: false,
    visible: false,
    frmName: 'user_form',
    formData: {},
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    isAdd: true,
    isShowAddNewUserReport: false,
    sidebarVisible: false,
    typesearch:{},
    donloadArray: []
  })

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={e => redirectToAdd(e, record.id)}>
            <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['user-list'] ? FIELDS['user-list']['tableheads'] : []
    );
    tableHeaders.push(tableAction);

    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
  });
  },[])


  const openAddNewUserReport = showAddNewUserReport =>
    setState(prevState => ({ ...prevState, isShowAddNewUserReport: showAddNewUserReport }));

  const redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/user/edit?e=${id}`);
      const respData = await response['data'];
      setState(prevState => ({ ...prevState, isAdd: false, formData: respData, visible: true }));
    } else {
      setState(prevState => ({ ...prevState, isAdd: true, visible: true, formData: {} }));
    }
  };

  const _onDeleteFormData = (data) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => onRowDeletedClick(data),
    });
  };

  const onRowDeletedClick = data => {
    if(data.id) {
      let _url = `${URL_WITH_VERSION}/user/delete`;
      apiDeleteCall(_url, { id: data.id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
        handleCancel();
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
    }
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
      setState(prevState => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
  
      setState(prevState => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);
      let resdata; 
      if (responseData.length > 0) {
        for (resdata in responseData[0]) { 
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === resdata) ||
              (item.hasOwnProperty("key") && item.key === resdata)
          );
          if (!index) {
            let title =resdata
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: resdata,
                key: resdata,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
  
      setState(prevState => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];
  
      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }
  
      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
    };

  const _onCreateFormData = () => {
    setState(prevState => ({...prevState, visible: false}), () => {
      setState(prevState => ({
        ...prevState,
        formData: {
          id: 0,
          'portcostp.tableperday': [
            { editable: true, index: 0, con_type: 2, con_g: 2, id: -9e6, con_unit: 2 },
            { editable: true, index: 1, con_g: 3, con_type: 5, id: -9e6 + 1, con_unit: 2 },
            { editable: true, index: 2, con_g: 3, con_type: 7, id: -9e6 + 2, con_unit: 2 },
            { editable: true, 'ind ex': 3, con_type: 1, con_g: 1, id: -9e6 + 3, con_unit: 2 },
            { editable: true, index: 4, con_type: 10, con_g: 4, id: -9e6 + 4, con_unit: 2 },
          ],
        },
        visible: true,
      }))
    })
  };

  const handleCancel = () => {
    setState(prevState => ({ ...prevState, visible: false }));
  };

  // const passwordValidation = (user_password) =>{
  //   const validPassword = new RegExp('^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$');
  //   if (!validPassword.test(user_password)) {
  //     return false;
  //   }
  //   return true;
  // }

  const emailValidation = (user_email) => {
    const validEmail = new RegExp('^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$');
    if (!validEmail.test(user_email)) {
      return false;
    }
    return true;
  }

 

  const getTableData = async (searchtype = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };
    let search=searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue')?searchtype:state.typesearch;

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim()

      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
      state.typesearch={'searchOptions':search.searchOptions,'searchValue':search.searchValue}

    }

    setState((prevState) => ({
      ...prevState,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);
    let _state = {}, dataArr = [], totalRows = 0;
    let _url = `${URL_WITH_VERSION}/user/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    totalRows = data && data.total_rows ? data.total_rows : 0;
    dataArr = data && data.data ? data.data : [];
    let donloadArr = []
    if (dataArr.length > 0 && totalRows > 0) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      _state['responseData'] = dataArr;
    }
    setState((prevState) => ({
      ...prevState,
      ..._state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const validateEvent = (data, cb) => {
    let isFaluty = false,
      message = undefined;

    if (emailValidation(data['user_email']) === false) {
      isFaluty = true;
      message = 'Email Field is Invalid';
    }
    if (emailValidation(data['user_email']) === false) {
      isFaluty = true;
      message = 'Email Field is Invalid';
    }
    return cb(isFaluty, message);
  };

  const saveFormData = (postData, innerCB) => {
    const { frmName } = state;
    let _url = 'save';
    let _method = 'post';
    if (postData.hasOwnProperty('id')) {
      _url = 'update';
      _method = 'put';
    }
    validateEvent(postData, (isFail, message) => {
      if (isFail === true) {
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: message }} />, 5);
      } else {
        postAPICall(`${URL_WITH_VERSION}/user/${_url}?frm=${frmName}`, postData, _method, data => {
          if (data.data) {
            openNotificationWithIcon('success', data.message);
            getTableData();
            handleCancel();
          } else {
            let dataMessage = data.message;
            let msg = "<div className='row'>";

            if (typeof dataMessage !== 'string') {
              Object.keys(dataMessage).map(
                i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
              );
            } else {
              msg += dataMessage;
            }

            msg += '</div>';
            openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
          }
        });
      }
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = state;  
 
     let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
     columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
     // if (cols && cols.length > 0) {
     //   params = params + '&c=' + cols.join(',')
     // }
     const filter = donloadArray.join()
     window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }

    const {
      frmName,
      formData,
      columns,
      responseData,
      loading,
      pageOptions,
      search,
      visible,
      isAdd,
      isShowAddNewUserReport,
      sidebarVisible
    } = state;
    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Users | Showing Me All User</span>
                  </h4>
                </div>
                <div className="action-btn">
                  <a href="#/user-dashboard" className="btn ant-btn-primary pt-1 pb-1 mr-2">
                   <LeftCircleOutlined />
                  </a>
                  <Button type="primary" onClick={e => redirectToAdd()}>
                    Add User
                  </Button>
                </div>
              </div>

              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={'user-list-toolbar'}
                    optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                    callback={e => callOptions(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => onActionDonwload('csv', 'user') },
                      { title: 'PDF', event: () => onActionDonwload('pdf', 'user') },
                      { title: 'XLS', event: () => onActionDonwload('xlsx', 'user') },
                    ]}
                  />
                ) : (
                  undefined
                )}
              </div>
              <section className="main-user-section mt-5">
                <div className="container-fluid p-0">
                  <Table
                    rowKey={record => record.id}
                    className="inlineTable editableFixedHeader resizeableTable"
                    bordered
                    scroll={{ x: 'max-content' }}
                    // scroll={{ y: 370 }}
                    components={components}
                    columns={tableColumns}
                    size="small"
                    dataSource={responseData}
                    loading={loading}
                    pagination={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                  />
                </div>
              </section>
            </div>
          </div>
        </article>
        {/* <!-- Modal --> */}
        {visible && (
          <Modal
            style={{ top: '2%' }}
            title={(isAdd === false ? 'Edit' : 'Add') + ' New User'}
            open={visible}
            onCancel={handleCancel}
            width="90%"
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    <NormalFormIndex
                      key={'key_' + frmName + '_0'}
                      formClass="label-min-height"
                      formData={formData}
                      showForm={true}
                      frmCode={frmName}
                      addForm={true}
                      showToolbar={[
                        {
                          isLeftBtn: [
                            {
                              key: 's2',
                              isSets: [
                                {
                                  id: '1',
                                  key: 'add',
                                  type: <PlusOutlined />,
                                  withText: '',
                                  event: (key, data) => _onCreateFormData(),
                                },
                                {
                                  id: '3',
                                  key: 'save',
                                  type: <SaveOutlined />,
                                  withText: '',
                                  event: (key, data, innerCB) => saveFormData(data, innerCB),
                                },
                                formData && formData['id'] &&{
                                  id: '6',
                                  key: 'delete',
                                  type: <DeleteOutlined />,
                                  withText: 'Delete',
                                  showToolTip: true,
                                  event: (key, data) => _onDeleteFormData(data),
                                },
                              ],
                            },
                          ],
                          isRightBtn: [
                            {
                              key: 's1',
                              isSets: [
                                {
                                  id: '3',
                                  key: 'report',
                                  type: '',
                                  isDropdown: 0,
                                  withText: 'Report',
                                  event: key => openAddNewUserReport(true),
                                },
                              ],
                            },
                          ],
                          isResetOption: false,
                        },
                      ]}
                      inlineLayout={true}
                    />
                  </div>
                </div>
              </article>
            </div>
          </Modal>
        )}

        {isShowAddNewUserReport ? (
          <Modal
            style={{ top: '2%' }}
            title="Report"
            open={isShowAddNewUserReport}
            onCancel={() => openAddNewUserReport(false)}
            width="95%"
            footer={null}
          >
            <AddNewUserReport />
          </Modal>
        ) : (
          undefined
        )}
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => callOptions(e)} /> : null
        }
      </div>
    );
  }
export default UserList;
