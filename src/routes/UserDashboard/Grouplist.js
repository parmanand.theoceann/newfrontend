import React, { Component } from 'react';
import { Table} from 'antd';
import {LeftCircleOutlined } from '@ant-design/icons';
import { FIELDS } from '../../shared/tableFields';
import URL_WITH_VERSION, { getAPICall, ResizeableTitle, useStateCallback } from '../../shared';
import { useEffect } from 'react';

let tableHeaders = Object.assign([], FIELDS && FIELDS['group-user-list'] ? FIELDS['group-user-list']['tableheads'] : []);

const  UserDashboard = (props) =>  {

  const [state, setState] = useStateCallback({
    visible: false,
    loading: true,
    columns: tableHeaders,
    responseData: [],
  })

  useEffect(() => {
    const getFormData = async() => {
      let _url = `${URL_WITH_VERSION}/user/groups/list`;
      const response = await getAPICall(_url);
      const data = await response['data'];
      setState({ ...state, responseData: data, loading: false });
    }
    getFormData()
  }, [])

  //resizing function
  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

    const { columns, loading, responseData } = state;

    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));
    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                {/* <ToolbarUI routeUrl="group_list_top_menu" /> */}
                <div className="form-heading">
                  <h4 className="title">
                    <span>Group | Showing All Group</span>
                    <span className="float-right">
                      <a href="#/user-dashboard" className="btn ant-btn-primary pl-2 pr-2 pt-1 pb-1">
                      <LeftCircleOutlined />
                      </a>
                    </span>
                  </h4>
                </div>
                {/* <div className="col-md-12 p-1">
                  <a href="#/user-dashboard">Dashboard</a>
                </div> */}

                <section className="main-user-section mt-5">
                  <div className="container-fluid p-0">
                    <Table
                      rowKey={record => record.id}
                      className="inlineTable editableFixedHeader resizeableTable"
                      bordered
                      scroll={{ x: 'max-content' }}
                      components={components}
                      columns={tableColumns}
                      size="small"
                      dataSource={responseData}
                      loading={loading}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                    />
                  </div>
                </section>
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }

export default UserDashboard;
