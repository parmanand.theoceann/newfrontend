import React, { Component } from "react";
import {
  Select,
  Form,
  Input,
  Checkbox,
  Popconfirm,
  Button,
  Tabs,
  Table,
  Modal,
} from "antd";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import URL_WITH_VERSION, {
  getAPICall,
  postAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
} from "../../shared";
import { EditOutlined,DeleteOutlined, } from '@ant-design/icons';
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

class AccessRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabKey: "UserRoll",
      visible1: false,
      visible: false,
      loadingComp: false,
      loadingComp1: false,
      loadingCompAcl: false,
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      pageOptions1: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      pageOptionsAcl: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      data: [],
      data2: [],
      columns: [
        {
          title: "Roll Name",
          dataIndex: "name",
        },

        {
          title: "Action",
          dataIndex: "action",
          width: 150,
          render: (text, record) => {
            return (
              <div className="editable-row-operations">
                {
                  <span className="iconWrapper">
                    <EditOutlined
                      onClick={() => this.redirectToAdd(record.id)}
                    />
                  </span>
                }
                {
                  <span className="iconWrapper cancel">
                    <Popconfirm
                      title="Are you sure, you want to delete it?"
                      onConfirm={() => this.onRowDeletedClick(record.id)}
                    >
                      <DeleteOutlined />
                    </Popconfirm>
                  </span>
                }
              </div>
            );
          },
        },
      ],
      columns2: [
        {
          title: "User",
          dataIndex: "full_name",
        },

        {
          title: "User Roles",
          dataIndex: "user_role",
        },

        {
          title: "Action",
          dataIndex: "action",
          width: 100,
          render: (text, record) => {
            return (
              <div className="editable-row-operations">
                {
                  <span className="iconWrapper">
                    <EditOutlined
                      onClick={() => this.redirectToAddRole(record)}
                    />
                  </span>
                }

                {
                  <span className="iconWrapper cancel">
                    <DeleteOutlined />
                  </span>
                }
              </div>
            );
          },
        },
      ],
      columns3: [
        {
          title: "Name",
          dataIndex: "name",
          render: (data, record) => {
            if (record && record.page_name) {
              return <span>{record.page_name}</span>;
            } else {
              if (record && record.permission_type) {
                return <span>{record.permission_type}</span>;
              }
            }
          },
        },
        {
          title: "Admin",
          dataIndex: "admin",
          render: (data, record) => {
            if (record && record.hasOwnProperty("children")) {
              return (
                <Checkbox
                  onChange={(e) => this.changeCheck(e, record)}
                  checked={false}
                />
              );
            } else {
              if (record && record.hasOwnProperty("Admin")) {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={true}
                  />
                );
              } else {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={false}
                  />
                );
              }
            }
          },
        },
        {
          title: "Charterers",
          dataIndex: "charterers",
          render: (data, record) => {
            if (record && record.hasOwnProperty("children")) {
              return (
                <Checkbox
                  onChange={(e) => this.changeCheck(e, record)}
                  // checked={false}
                />
              );
            } else {
              if (record && record.hasOwnProperty("Charterers")) {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={true}
                  />
                );
              } else {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    // checked={false}
                  />
                );
              }
            }
          },
        },

        {
          title: "Operations",
          dataIndex: "operations",
          render: (data, record) => {
            if (record && record.hasOwnProperty("children")) {
              return (
                <Checkbox
                  onChange={(e) => this.changeCheck(e, record)}
                  // checked={false}
                />
              );
            } else {
              if (record && record.hasOwnProperty("Operations")) {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={true}
                  />
                );
              } else {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    // checked={false}
                  />
                );
              }
            }
          },
        },

        {
          title: "Account",
          dataIndex: "account",
          render: (data, record) => {
            if (record && record.hasOwnProperty("children")) {
              return (
                <Checkbox
                  onChange={(e) => this.changeCheck(e, record)}
                  // checked={false}
                />
              );
            } else {
              if (record && record.hasOwnProperty("Account")) {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={true}
                  />
                );
              } else {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    // checked={false}
                  />
                );
              }
            }
          },
        },

        {
          title: "Manager",
          dataIndex: "manager",
          render: (data, record) => {
            if (record && record.hasOwnProperty("children")) {
              return (
                <Checkbox
                  onChange={(e) => this.changeCheck(e, record)}
                  // checked={false}
                />
              );
            } else {
              if (record && record.hasOwnProperty("Manager")) {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    checked={true}
                  />
                );
              } else {
                return (
                  <Checkbox
                    onChange={(e) => this.changeCheck(e, record)}
                    // checked={false}
                  />
                );
              }
            }
          },
        },
      ],
      aclData: [],
      frmName: "role",
      formData: {},
      name: "",
      description: "",
      isAdd: "false",
      id: "",
      user_role: "",
      checkData: [],
    };
  }

  componentDidMount() {
    this.fetchData();
    this.fetchData1();
    this.fetchAcl();
  }

  // let changedCheckbox = ''
  //     changedCheckbox = this.state.checkData.find((cb) => cb.page_name === record.name);
  //     changedCheckbox.delete_rec = 0;
  //     changedCheckbox.get_rec = 0;
  //     changedCheckbox.save = 0;
  //     changedCheckbox.update_rec = 0;
  //     changedCheckbox.view = 0;
  //     changedCheckbox.full_access = 0
  //     arr = Object.assign([], this.state.checkData, changedCheckbox)
  changeCheck = (event, record) => {
    // console.log("event,record",event,record,this.state.aclData)
  };
  fetchAcl = async (search = {}) => {
    const { pageOptionsAcl } = this.state;
    let qParams = { p: pageOptionsAcl.pageIndex, l: pageOptionsAcl.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { OR: {} };
        wc["OR"][search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
    }

    this.setState(
      {
        aclData: [],
      },
      () => {
        this.setState({
          loadingCompAcl: true,
        });
      }
    );

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/user/acllist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const aclData = await response;
    const totalRows = aclData && aclData.total_rows ? aclData.total_rows : 0;
    let state = {
      loadingCompAcl: false,
    };
    if (aclData["data"].length > 0 && totalRows > this.state.aclData.length) {
      state["aclData"] = aclData["data"];
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptionsAcl: {
        pageIndex: pageOptionsAcl.pageIndex,
        pageLimit: pageOptionsAcl.pageLimit,
        totalRows: totalRows,
      },
      loadingCompAcl: false,
    });
  };

  fetchData = async (search = {}) => {
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { OR: {} };
        wc["OR"][search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
    }

    this.setState(
      {
        data: [],
      },
      () => {
        this.setState({
          loadingComp: true,
        });
      }
    );

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/user/role/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let state = {
      loadingComp: false,
    };
    if (data["data"].length > 0 && totalRows > this.state.data.length) {
      state["data"] = data["data"];
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loadingComp: false,
    });
    // this.setState({
    //   data:arr
    // })
  };

  fetchData1 = async (search = {}) => {
    const { pageOptions1 } = this.state;
    let qParams = { p: pageOptions1.pageIndex, l: pageOptions1.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { OR: {} };
        wc["OR"][search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
    }

    this.setState(
      {
        data1: [],
      },
      () => {
        this.setState({
          loadingComp1: true,
        });
      }
    );

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/user/role/urlist?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let state = {
      loadingComp1: false,
    };
    if (data["data"].length > 0 && totalRows > this.state.data2.length) {
      state["data2"] = data["data"];
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions1: {
        pageIndex: pageOptions1.pageIndex,
        pageLimit: pageOptions1.pageLimit,
        totalRows: totalRows,
      },
      loadingComp1: false,
    });
    // this.setState({
    //   data:arr
    // })
  };

  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/user/role/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        this.fetchData();
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  callOptionsComp = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.fetchData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions },
        () => {
          this.fetchData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.fetchData();
      });
    }
  };

  callOptionsComp1 = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions1 = this.state.pageOptions1;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions1["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions1: pageOptions1 },
        () => {
          this.fetchData1(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions1 = this.state.pageOptions1;
      pageOptions1["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions1: pageOptions1 },
        () => {
          this.fetchData1();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions1 = this.state.pageOptions1;
      pageOptions1[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions1["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions1: pageOptions1 }, () => {
        this.fetchData1();
      });
    }
  };
  handleCancel = () => {
    this.setState({
      visible1: false,
      name: "",
      description: "",
    });
  };
  handleCancel1 = () => {
    this.setState({
      visible: false,
      id: "",
      user_role: "",
    });
  };

  redirectToAddRole = async (id) => {
    if (id) {
      this.setState(
        {
          ...this.state,
          isAdd: false,
          formData: id,
          id: id.id,
          user_role: id.user_role.split(","),
        },
        () => this.setState({ ...this.state, visible: true })
      );
    } else {
      this.setState({
        ...this.state,
        isAdd: true,
        visible: true,
        formData: {},
      });
    }
  };

  redirectToAdd = async (id) => {
    if (id) {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/user/role/edit?e=${id}`
      );
      const respData = await response["data"];
      this.setState(
        {
          ...this.state,
          isAdd: false,
          formData: respData,
          name: respData.name,
          description: respData.description,
        },
        () => this.setState({ ...this.state, visible1: true })
      );
    } else {
      this.setState({
        ...this.state,
        isAdd: true,
        visible1: true,
        formData: {},
      });
    }
  };

  saveUpdateAcl = () => {
    const { checkData } = this.state;
    checkData.map((item) => {
      item && item.hasOwnProperty("role_name") && delete item.role_name;
      return item;
    });
    let _url = "update";
    let _method = "put";
    postAPICall(
      `${URL_WITH_VERSION}/user/acl${_url}`,
      checkData,
      _method,
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
        } else {
          openNotificationWithIcon("error", data.message);
        }
      }
    );
  };

  handleSubmit = () => {
    const { name, description, formData } = this.state;
    let obj1 = {
      name: name,
      description: description,
    };
    if (name == "") {
      openNotificationWithIcon("error", "Please Enter Name");
      return;
    }
    let _url = "save";
    let _method = "post";
    if (this.state.isAdd == false) {
      _url = "update";
      _method = "put";
      obj1.id = formData.id;
    }

    postAPICall(
      `${URL_WITH_VERSION}/user/role/${_url}`,
      obj1,
      _method,
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          this.setState(
            {
              visible1: false,
              name: "",
              description: "",
            },
            () => {
              this.fetchData();
            }
          );
          // window.location.reload(false)
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />
          );
        }
      }
    );
  };

  handleSubmitRole = () => {
    const { id, user_role } = this.state;

    let obj1 = {
      id: id,
      user_role: user_role.toString(),
    };

    if (id == "" || user_role == "") {
      openNotificationWithIcon("error", "Please enter all mandetory fields !");
      return;
    }

    let _url = "urupdate";
    let _method = "put";
    if (this.state.isAdd == false) {
      _url = "urupdate";
      _method = "put";
      // obj1.id=formData.id
    }
    postAPICall(
      `${URL_WITH_VERSION}/user/role/${_url}`,
      obj1,
      _method,
      (data) => {
        if (data.data) {
          openNotificationWithIcon("success", data.message);
          this.setState(
            {
              visible: false,
              id: "",
              user_id: "",
            },
            () => {
              this.fetchData1();
            }
          );
          // window.location.reload(false)
        } else {
          let dataMessage = data.message;
          let msg = "<div className='row'>";

          if (typeof dataMessage !== "string") {
            Object.keys(dataMessage).map(
              (i) =>
                (msg +=
                  "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
            );
          } else {
            msg += dataMessage;
          }

          msg += "</div>";
          openNotificationWithIcon(
            "error",
            <div dangerouslySetInnerHTML={{ __html: msg }} />
          );
        }
      }
    );
  };

  callback = (key) => {
    this.setState({
      tabKey: key,
    });
  };

  onChange = (value) => {
    this.setState({
      id: value,
    });
    // console.log(`selected ${value}`);
  };

  onChange1 = (value) => {
    this.setState({
      user_role: value,
    });
    // console.log(`selected ${value}`);
  };
  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`,
      "_blank"
    );
  };
  render() {
    const {
      visible,
      pageOptionsAcl,
      loadingCompAcl,
      aclData,
      isAdd,
      columns3,
      visible1,
      columns,
      data,
      pageOptions,
      search,
      loadingComp,
      data2,
      columns2,
      pageOptions1,
      loadingComp1,
    } = this.state;
    // console.log("checkData",this.state.aclData)
    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Module Access Right</span>

                    {/* <span className="float-right">
                      {this.state.tabKey == "UserRoll"
                        ?
                        <Button type="primary" onClick={e => this.redirectToAdd(null)}>
                          Add New Role
                        </Button>
                        :
                        this.state.tabKey == "RollAssignment" ?

                          <Button type="primary" onClick={e => this.redirectToAddRole(null)}>
                            Add Role
                          </Button>
                          :
                          this.state.tabKey == "AclPermissions" ?
                            <Button type="primary" onClick={e => this.saveUpdateAcl(null)}>
                              save/update
                            </Button>
                            :
                            undefined
                      }

                    </span> */}
                  </h4>
                </div>

                {/* <Tabs defaultActiveKey="UserRoll" size="small" onChange={this.callback}>
                  <TabPane className="pt-3" tab="User Roles" key="UserRoll">
                    {
                      loadingComp === false ?
                        <ToolbarUI
                          routeUrl={'user-roles-access-right'}
                          optionValue={{ pageOptions: pageOptions, columns: columns, search: search }}
                          callback={e => this.callOptionsComp(e)}
                          dowloadOptions={[
                            { title: 'CSV', event: () => this.onActionDonwload('csv', 'archieve') },
                            { title: 'PDF', event: () => this.onActionDonwload('csv', 'archieve') },
                            { title: 'XLS', event: () => this.onActionDonwload('csv', 'archieve') },
                          ]}
                        />
                        :
                        undefined
                    }

                    <Table
                      className="mt-3"
                      bordered
                      columns={columns}
                      dataSource={data}
                      pagination={false}
                      // footer={() => (
                      //   <div className="text-center">
                      //     <Button type="link">Add New</Button>
                      //   </div>
                      // )}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                    />
                  </TabPane>
                  <TabPane className="pt-3" tab="User Role" key="RollAssignment">
                    {
                      loadingComp1 === false ?
                        <ToolbarUI
                          routeUrl={'user-roles-assignment-access-right'}
                          optionValue={{ pageOptions: pageOptions1, columns: columns2, search: search }}
                          callback={e => this.callOptionsComp1(e)}
                          dowloadOptions={[
                            { title: 'CSV', event: () => this.onActionDonwload('csv') },
                            { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                            { title: 'XLS', event: () => this.onActionDonwload('xls') },
                          ]}
                        />
                        :
                        undefined
                    }

                    <Table
                      className="mt-3"
                      bordered
                      columns={columns2}
                      dataSource={data2}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                    />
                  </TabPane>

                  <TabPane className="pt-3" tab="ACL Permissions" key="AclPermissions">
                    {
                      loadingCompAcl === false ?
                        <ToolbarUI
                          routeUrl={'user-roles-acl-permissions'}
                          optionValue={{ pageOptions: pageOptionsAcl, columns: columns3, search: search }}
                          callback={e => this.callOptionsComp(e)}
                          dowloadOptions={[
                            { title: 'CSV', event: () => this.onActionDonwload('csv') },
                            { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                            { title: 'XLS', event: () => this.onActionDonwload('xls') },
                          ]}
                        />
                        : undefined
                    }
                    <Table
                      className="mt-3"
                      bordered
                      columns={columns3}
                      dataSource={aclData}
                      pagination={false}
                      footer={false}
                      rowKey={(record) => record.key}
                      rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                      }
                    />
                  </TabPane>
                </Tabs> */}
                {visible1 && (
                  <Modal
                    style={{ top: "2%" }}
                    title={(isAdd ? "Edit" : "Add") + " User Role"}
                   open={visible1}
                    onCancel={this.handleCancel}
                    width="90%"
                    footer={null}
                  >
                    <div className="body-wrapper">
                      <article className="article">
                        <div className="box box-default">
                          <div className="box-body">
                            <Form>
                              <div className="row">
                                <div className="col-md-6">
                                  <FormItem
                                    {...formItemLayout}
                                    label="Role Name"
                                  >
                                    <Input
                                      value={this.state.name}
                                      onChange={(event) => {
                                        this.setState({
                                          name: event.target.value,
                                        });
                                      }}
                                      size="default"
                                      placeholder="Role Name"
                                    />
                                  </FormItem>
                                </div>

                                <div className="col-md-6">
                                  <FormItem
                                    {...formItemLayout}
                                    label="Description"
                                  >
                                    <Input
                                      value={this.state.description}
                                      onChange={(event) => {
                                        this.setState({
                                          description: event.target.value,
                                        });
                                      }}
                                      size="default"
                                      placeholder="Description"
                                    />
                                  </FormItem>
                                </div>
                              </div>
                              <span className="float-right">
                                <Button
                                  onClick={() => {
                                    this.handleSubmit();
                                  }}
                                  className="btn ant-btn-primary btn-sm"
                                >
                                  Save
                                </Button>
                              </span>
                            </Form>
                          </div>
                        </div>
                      </article>
                    </div>
                  </Modal>
                )}

                {visible && (
                  <Modal
                    style={{ top: "2%" }}
                    title={(isAdd ? "Edit" : "Add") + " User Assignment"}
                   open={visible}
                    onCancel={this.handleCancel1}
                    width="90%"
                    footer={null}
                  >
                    <div className="body-wrapper">
                      <article className="article">
                        <div className="box box-default">
                          <div className="box-body">
                            <Form>
                              <div className="row">
                                <div className="col-md-6">
                                  <FormItem
                                    {...formItemLayout}
                                    label="Full Name"
                                  >
                                    <Select
                                      placeholder="Full Name"
                                      onChange={this.onChange}
                                      value={this.state.id}
                                    >
                                      {data2.length > 0 &&
                                        data2.map((val, ind) => {
                                          return (
                                            <Option value={val.id} key={val.id}>
                                              {val.full_name}
                                            </Option>
                                          );
                                        })}
                                    </Select>
                                  </FormItem>
                                </div>

                                <div className="col-md-6">
                                  <FormItem
                                    {...formItemLayout}
                                    label="User Role"
                                  >
                                    <Select
                                      mode="multiple"
                                      placeholder="User Role"
                                      onChange={this.onChange1}
                                      value={this.state.user_role}
                                    >
                                      {data.length > 0 &&
                                        data.map((val, ind) => {
                                          return (
                                            <Option value={val.id} key={val.id}>
                                              {val.name}
                                            </Option>
                                          );
                                        })}
                                    </Select>
                                  </FormItem>
                                </div>
                              </div>
                              <span className="float-right">
                                <Button
                                  onClick={() => {
                                    this.handleSubmitRole();
                                  }}
                                  className="btn ant-btn-primary btn-sm"
                                >
                                  Save
                                </Button>
                              </span>
                            </Form>
                          </div>
                        </div>
                      </article>
                    </div>
                  </Modal>
                )}
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }
}
export default AccessRight;
