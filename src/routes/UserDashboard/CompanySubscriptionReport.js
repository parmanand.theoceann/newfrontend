import React, { Component } from 'react';
import {PrinterOutlined } from '@ant-design/icons';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td>Company Name :</td>
                  <td>Value</td>

                  <td>Phone Number :</td>
                  <td>Value</td>

                  <td>Company ID :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Contact Number :</td>
                  <td>Value</td>

                  <td>Country :</td>
                  <td>Value</td>

                  <td>Email ID :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Country Code :</td>
                  <td>Value</td>

                  <td>Tax :</td>
                  <td>Value</td>

                  <td>Licence Plan :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Subscription Price :</td>
                  <td>Value</td>

                  <td>Pricing Per User :</td>
                  <td>Value</td>

                  <td>Discount :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Total Users :</td>
                  <td>Value</td>

                  <td>Total Price :</td>
                  <td>Value</td>

                  <td>Discounted Price :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Extra Adjustment :</td>
                  <td>Value</td>

                  <td>Payble Amount :</td>
                  <td>Value</td>

                  <td>Subscriber Status :</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td>Activation Date :</td>
                  <td>Value</td>

                  <td>Deactivation Date :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Total Recieved :</td>
                  <td>Value</td>

                  <td>Payble Amount :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Total Amount Due :</td>
                  <td>Value</td>

                  <td>Next Due Date :</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  }
}

class CompanySubscriptionReport extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Printer',
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                             <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)} />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default CompanySubscriptionReport;
