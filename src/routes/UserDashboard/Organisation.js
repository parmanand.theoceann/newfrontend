import React, { useEffect } from "react";
import { Table } from "antd";
import AddressList from "../address-list/index";
import { FIELDS } from "../../shared/tableFields";
import URL_WITH_VERSION, { getAPICall, ResizeableTitle, useStateCallback } from "../../shared";
import {LeftCircleOutlined } from '@ant-design/icons';

let tableHeaders_head = Object.assign(
    [],
    FIELDS && FIELDS["orgnization-user-head-office-list"]
      ? FIELDS["orgnization-user-head-office-list"]["tableheads"]
      : []
  );
let tableHeaders_head2 = Object.assign(
    [],
    FIELDS && FIELDS["orgnization-user-head-office-details-list"]
      ? FIELDS["orgnization-user-head-office-details-list"]["tableheads"]
      : []
  );  

const Organisation = (props) => {

  const [state, setState] = useStateCallback({
    visible: false,
    loading: true,
    columns1: tableHeaders_head,
    columns2: tableHeaders_head2,
    responseData: [],
    sub_comp_details: [],
  })

useEffect(()=> {
  const getFormData = async () => {
    let _url = `${URL_WITH_VERSION}/address/organisation/list`;
    const response = await getAPICall(_url);
    const data = await response["data"];
    if (data && data.length > 0) {
      let data1 = [];
      {
        data &&
          data.map((item) => {
            if (item.sub_comp_details && item.sub_comp_details.length > 0) {
              item.sub_comp_details.map((val) => {
                data1.push(val);
              });
            }
          });
      }
      // putting setState inside map function causing multiple re-rendering.so i kept it outside of the map function.
      setState(prevState => ({
        ...prevState,
        sub_comp_details: [...data1],
      }));
    }
    setState(prevState => ({ ...prevState, responseData: data, loading: false }));
  };
  getFormData()
}, [])

  //resizing function
  const handleResize = (index) => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

    const {
      columns1,
      columns2,
      loading,
      responseData,
      sub_comp_details,
    } = state;
    return (
      <>
        <div className="body-wrapper organisation-list">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Organization</span>
                    <span className="float-right">
                      <a
                        href="#/user-dashboard"
                        className="btn btn-primary pl-2 pr-2 pt-1 pb-1"
                      >
                        <LeftCircleOutlined />
                      </a>
                    </span>
                  </h4>
                </div>

                <section className="main-user-section mt-5">
                  <div className="container-fluid p-0">
                    <div className="row">
                      <div className="col-md-5">
                        <Table
                          bordered
                         // rowKey={(record) => record.id}
                          columns={columns1}
                          dataSource={responseData}
                          pagination={false}
                          components={components}
                          loading={loading}
                          size="small"
                          footer={() => <div className="text-center" />}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                          scroll={{ x: "max-content" }}
                          title={() => (
                            <div className="bold-title-wrapper">
                              <span>Head Office</span>
                            </div>
                          )}
                        />
                      </div>

                      <div className="col-md-7">
                        <Table
                          bordered
                        //  rowKey={(record) => record.id}
                          columns={columns2}
                          dataSource={responseData}
                          components={components}
                          pagination={false}
                          loading={loading}
                          size="small"
                          footer={() => <div className="text-center" />}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                          scroll={{ x: "max-content" }}
                          title={() => (
                            <div className="bold-title-wrapper">
                              <span>Head Office (Bank Details)</span>
                            </div>
                          )}
                        />
                      </div>
                    </div>



                     <div className="row p10">
                      <div className="col-md-5">
                        <Table
                          bordered
                       //   rowKey={(record) => record.id}
                          columns={columns1}
                          dataSource={sub_comp_details}
                          pagination={false}
                          components={components}
                          loading={loading}
                          size="small"
                          footer={() => <div className="text-center" />}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                          scroll={{ x: "max-content" }}
                          title={() => (
                            <div className="bold-title-wrapper">
                              <span>Branches</span>
                            </div>
                          )}
                        />
                      </div>

                      <div className="col-md-7">
                        <Table
                          bordered
                        //  rowKey={(record) => record.id}
                          columns={columns2}
                          dataSource={sub_comp_details}
                          pagination={false}
                          components={components}
                          loading={loading}
                          size="small"
                          footer={() => <div className="text-center" />}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                          scroll={{ x: "max-content" }}
                          title={() => (
                            <div className="bold-title-wrapper">
                              <span>Branches (Bank Details)</span>
                            </div>
                          )}
                        />
                      </div>
                    </div> 
                    <AddressList childPage="orgnization" />
                  </div>
                </section>
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }


export default Organisation;
