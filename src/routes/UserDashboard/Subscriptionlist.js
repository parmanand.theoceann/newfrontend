import React, { Component } from 'react';
import { Select, Form, Input, Button, Switch, Modal } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
} from '../../shared';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import CompanySubscriptionReport from './CompanySubscriptionReport'


class Subscriptionlist extends Component {
  constructor(props) {
    super(props);
  this.state = { 
    visible: false,
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    frmName: 'my_subscriber_form',
    formData:{},
    loading:true,
    isShowCompanySubscriptionReport: false,
  }
  }

  CompanySubscriptionReport = showCompanySubscriptionReport => this.setState({ ...this.state, isShowCompanySubscriptionReport: showCompanySubscriptionReport });
  

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    // console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    // console.log(e);
    this.setState({
      visible: false,
    });
  };

  componentDidMount = () => {
    this.getTableData();
  };

  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };


    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/subscription/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response['data'];
    if(data.length>0){
      let id=data[0].subscription_id
      // console.log("id..",id)  
      const response = await getAPICall(`${URL_WITH_VERSION}/subscription/edit?e=${id}`);
      const respData = await response;
      respData['data']['disablefield'] = ["company_name","company_id","country_code","country","contact_no","phone_number","email_id","tax","licence_plan","subscriber_status","payble_amount","extra_adjustment","dis_amount","total_price","total_users","pricing_per_user","subscription_price","discounted_price","discount"]
     respData['data']['-']['disablefield']=["activation_date","deactivation_date","total_recieved","payble_amount","total_amount_due","next_due_date"]
      // console.log("resp..",respData['data'])
      delete respData['data'].paymenthistory
      this.setState({ 
        ...this.state,
        loading: false,
        formData:respData['data']
      });
    }
   

  };

  render() {
  const  {frmName,formData,loading, isShowCompanySubscriptionReport}=this.state
    return (
      <>
       <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            {!loading && 
            <NormalFormIndex
              key={'key_' + frmName + '_0'}
              formClass="label-min-height"
              formData={formData}
              showForm={true}
              frmCode={frmName}
              addForm={true}
              hideGroup={["Payment History"]}
              showToolbar={[
                {
                  isLeftBtn: [],
                  isRightBtn: [
                    {
                      key: 's1',
                      isSets: [
                       
                        {
                          id: '3',
                          key: 'report',
                          type: '',
                          isDropdown: 0,
                          withText: 'Report',
                          event: key => this.CompanySubscriptionReport(true),
                        },
                      ],
                    },
                  ],
                  isResetOption: false,
                },
              ]}
              inlineLayout={true}
            />
  }
          </div>
        </div>
      </article>
      {isShowCompanySubscriptionReport ? (
            <Modal
              style={{ top: '2%' }}
              title="Report"
             open={isShowCompanySubscriptionReport}
              onOk={this.handleOk}
              onCancel={() => this.CompanySubscriptionReport(false)}
              width="95%"
              footer={null}
            >
              <CompanySubscriptionReport />
            </Modal>
          ) : (
            undefined
          )}
    </div>
      </>
    );
  }
}
export default Subscriptionlist;
