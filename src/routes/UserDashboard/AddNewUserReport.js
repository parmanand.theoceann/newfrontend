import React, { useRef, useState, forwardRef } from 'react';
import {PrinterOutlined } from '@ant-design/icons';

import ReactToPrint from 'react-to-print';

const  ComponentToPrint = forwardRef((props, ref) =>  {

    return (
      <article className="article toolbaruiWrapper" ref={ref}>
        <div className="box box-default">
          <div className="box-body">
            <div className="invoice-inner-download mt-3">
              <div className="row">
                <div className="col-12 text-center">
                  <span className="title">ABC</span>
                  <p className="sub-title m-0">Meritime Company</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <div className="text-center invoice-top-address">
                  <p>Abc Maritime pvt ltd, sector-63, noida, up-201301</p>
                </div>
              </div>
            </div>

            <table className="table table-bordered table-invoice-report-colum">
              <tbody>
                <tr>
                  <td>Subscription Company :</td>
                  <td>Value</td>

                  <td>First Name :</td>
                  <td>Value</td>

                  <td>Last Name :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Initials :</td>
                  <td>Value</td>

                  <td>User Name :</td>
                  <td>Value</td>

                  <td>User Email :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>User Password :</td>
                  <td>Value</td>

                  <td>Phone Number :</td>
                  <td>Value</td>

                  <td>Country :</td>
                  <td>Value</td>
                </tr>

                <tr>
                  <td>Organization :</td>
                  <td>Value</td>

                  <td>Designation :</td>
                  <td>Value</td>

                  <td>User Status :</td>
                  <td>Value</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </article>
    );
  })

const  AddNewUserReport = () =>  {
  const [state, setState] = useState({
    name: 'Printer',
  })

  const componentRef = useRef()

  const printReceipt = () => {
    window.print();
  }

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection"></div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt"> Download</span>
                      </li>
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                             <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => componentRef.current}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={componentRef} />
            </div>
          </div>
        </article>
      </div>
    );
  }

export default AddNewUserReport;
