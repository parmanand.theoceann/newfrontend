import { useEffect, useState } from "react";
import URL_WITH_VERSION, {
  ResizeableTitle,
  getAPICall,
  objectToQueryStringFunc,
  useStateCallback,
} from "../../shared";
import { EditOutlined } from "@ant-design/icons";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { Modal, Table, Row, Col, Card, Select, Button, Tag, Space } from "antd";
import OtherExpenseModal from "../operation/revenue-expenses/components/OtherExpenseModal";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import PieChart from "../performancedashboard/PieChart";
import FunnelChart from "../dashboard/charts/FunnelChart";

const CargoDashboard = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
    isGraphModal: false,
  });
  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
                <Popconfirm
                  title="Are you sure, you want to delete it?"
                  onConfirm={() => onRowDeletedClick(record.id)}
                >
                 <DeleteOutlined /> />
                </Popconfirm>
              </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["cargo-list"] ? FIELDS["cargo-list"]["tableheads"] : []
    );

    console.log(FIELDS["voyage-efficiency-list"]);

    console.log(tableAction);
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/invoice/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const redirectToAdd = async (e, record = null) => {
    _onLeftSideListClick(record.id);
  };
  const onChangeStatus = (status = false) => {
    setState({ ...state, isVisible: status }, () => getTableData());
  };

  const _onLeftSideListClick = async (delayId) => {
    setState((prevState) => ({ ...prevState, isVisible: false }));
    // setState({ ...state, isVisible: false });
    let _url = "edit?e=";
    const response = await getAPICall(
      `${URL_WITH_VERSION}/voyage-manager/invoice/${_url + delayId}`
    );
    const data = await response["data"];
    setState((prevState) => ({
      ...prevState,
      formDataValues: data,
      isVisible: true,
    }));
    // setState({ ...state, isVisible: false, formDataValues }, () => {
    //   setState({ ...state, isVisible: true });
    // });
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };
  const {
    columns,
    loading,
    responseData,
    pageOptions,
    search,
    isVisible,
    sidebarVisible,
    formDataValues,
  } = state;
  const tableCol = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  const ClusterDataxAxis = [
    "TCE02-24-01592",
    "TCE11-23-01243",
    "TCE11-23-01235",
    "TCE10-23-01219",
    "TCE01-24-01572",
  ];
  const ClusterDataSeries = [
    {
      name: "Net Amount",
      type: "bar",
      barGap: 0,

      data: [450, 500, 789, 870, 290],
    },
  ];

  const [clusterChartOption, setClusterChartOption] = useState({
    legend: {},
    tooltip: {},
    dataset: {
      source: [
        ["product", "Extra Revenue", "Net Revenue"],
        ["COACARGO-00308", 43.3, 85.8],
        ["COACARGO-00300", 83.1, 73.4],
        ["COACARGO-00297", 86.4, 65.2],
        ["COACARGO-00294", 72.4, 53.9],
      ],
    },
    xAxis: {
      type: "category",
      axisLabel: {
        rotate: 45, 
        fontSize: 9// Rotate labels by 45 degrees
      },
    },
    yAxis: {
      type: "value",
      // min: 50 ?? 0,
      // max: 50 ?? 1000,
      axisLabel: {
        formatter: "{value}k",
      },
    },
    // Declare several bar series, each will be mapped
    // to a column of dataset.source by default.
    series: [{ type: "bar" }, { type: "bar" }],
  });

  const [pieChartOptions, setPieChartOptions] = useState({
    bar1: {
      option: {
        // title: {
        //   text: "Net Amount Per Vessel",
        //   left: "center",
        // },
        tooltip: {
          trigger: "item",
        },
        series: [
          {
            name: "Access From",
            type: "pie",
            radius: "50%",
            data: [
              { value: 1048, name: "Petrolium 3" },
              { value: 735, name: "Crude Oil 121.4" },
              { value: 580, name: "Parafine 332.1" },
              { value: 484, name: "Coal 454.33" },
              { value: 300, name: "Toluene 321.22" },
            ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
          },
        ],
      },
      // labelLine: {
      //   show: true,
      //   length: 20,
      //   length2: 20,
      // },
    },
  });

  return (
    <div>
      <div>
        <Row
          gutter={[16, 16]}
          style={{ justifyContent: "space-between", margin: "1rem 1rem" }}
        >
          <Col span={2.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Count of Vessel
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                210
              </p>
            </Card>
          </Col>
          <Col span={2.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Count Of Cargo
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                150
              </p>
            </Card>
          </Col>
          <Col span={2.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Count Of Voyage
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                120
              </p>
            </Card>
          </Col>
          <Col span={4}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Total Comm. Amount
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                540 $ 
              </p>
            </Card>
          </Col>
          <Col span={2.5}>
            <Card
              title={
                <div
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: "0.8rem",
                  }}
                >
                  Total Amount
                </div>
              }
              bordered={false}
              style={{
                padding: 0,
                border: 0,
                borderRadius: 15,
                backgroundColor: "#1D406A",
              }}
              bodyStyle={{
                padding: 6,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <p
                style={{
                  textAlign: "center",
                  margin: 0,
                  color: "white",
                  fontSize: "1rem",
                }}
              >
                300 $ 
              </p>
            </Card>
          </Col>
          <Col span={8}>
            <Row gutter={16}>
              <Col span={8}>Voyage No.</Col>
              <Col span={12}>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "TCE02-24-01592",
                      label: "TCE02-24-01592",
                    },
                    {
                      value: "TCE01-24-01582",
                      label: "TCE01-24-01582",
                    },
                    {
                      value: "TCE01-24-01573",
                      label: "TCE01-24-01573",
                    },
                  ]}
                />
              </Col>
              <Col span={4}><Button>Reset</Button></Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>Vessel</Col>
              <Col span={16}>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "PACIFIC EXPLORER",
                      label: "OCEANIC MAJESTY",
                    },
                    {
                      value: "OCEANIC MAJESTY",
                      label: "OCEANIC MAJESTY",
                    },
                    {
                      value: "CS HANA",
                      label: "CS HANA",
                    },
                  ]}
                />
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>Charterer</Col>
              <Col span={16}>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "ECHPIN2306",
                      label: "ECHPIN2306",
                    },
                    {
                      value: "LAYPAN2306",
                      label: "LAYPAN2306",
                    },
                    {
                      value: "BLUPIN2306",
                      label: "BLUPIN2306",
                    },
                  ]}
                />
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>Cargo Name</Col>
              <Col span={16}>
                <Select
                  placeholder="All"
                  optionFilterProp="children"
                  options={[
                    {
                      value: "Petrol",
                      label: "petrol",
                    },
                    {
                      value: "Parafin",
                      label: "Parafin",
                    },
                    {
                      value: "Crude Oil",
                      label: "Crude Oil",
                    },
                  ]}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Net Amount Per Voyage No."}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"800"}
            />
          </Col>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Extra Revenue & Net Revenue Per Cargo ID"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              // maxValueyAxis={"50"}
              clusterChartOption={clusterChartOption}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <PieChart
              pieChartOptions={pieChartOptions}
              //   PieChartData={PieChartData}
              Heading={"Commission Amount Per Cargo"}
            />
          </Col>
          <Col span={12}>
            <FunnelChart Heading={"Cargo Name & Net Amount"} />
          </Col>
        </Row>
        <Table
          bordered
          columns={[
            {
              title: "Cargo Name",
              dataIndex: "cargo_name",
              key: "cargo_name",
              render: (text) => <a>{text}</a>,
              width: 150,
            },
            {
              title: "Ballast Port Name",
              dataIndex: "ballast_port_name",
              key: "ballast_port_name",
              width: 150,
            },
            {
              title: "Repos. Port Name",
              dataIndex: "repos_port_name",
              key: "repos_port_name",
              width: 150,
            },
          ]}
          dataSource={[
            {
              key: 1,
              cargo_name: "Crude Oil",
              ballast_port_name: "Chennai",
              repos_port_name: "Mumbai",
            },
            {
              key: 2,
              cargo_name: "Petrolium",
              ballast_port_name: "s-Gravenzande",
              repos_port_name: "1/2-1 PE (30/3)",
            },
            {
              key: 3,
              cargo_name: "Perafine",
              ballast_port_name: "Portoscuso",
              repos_port_name: "(Arctic) North Sea Route",
            },
            {
              key: 4,
              cargo_name: "Toluen",
              ballast_port_name: "Chennai",
              repos_port_name: "1/2-1 PE (30/3)",
            },
            {
              key: 1,
              cargo_name: "Coal",
              ballast_port_name: "Chennai",
              repos_port_name: "Chennai (Madras)",
            },
          ]}
          size="small"
          //   scroll={{ x: "max-content" }}
          style={{ width: "45%", marginLeft: "1rem" }}
          pagination={false}
        />
      </div>
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div className="form-wrapper">
              <div className="form-heading">
                <h4 className="title">
                  <span>Other Expense List</span>
                </h4>
              </div>
            </div>
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {loading === false ? (
                <ToolbarUI
                  routeUrl={"other-expense-toolbar"}
                  optionValue={{
                    pageOptions: pageOptions,
                    columns: columns,
                    search: search,
                  }}
                  //   showGraph={showGraphs}
                  callback={(e) => callOptions(e)}
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "otherrevenue"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "otherrevenue"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "otherrevenue"),
                    },
                  ]}
                />
              ) : undefined}
            </div>
            <div>
              <Table
                className="inlineTable resizeableTable"
                bordered
                columns={tableCol}
                components={components}
                size="small"
                scroll={{ x: "max-content" }}
                dataSource={responseData}
                loading={loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? "table-striped-listing"
                    : "dull-color table-striped-listing"
                }
              />
            </div>
          </div>
        </div>
      </article>
      {isVisible ? (
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Edit Invoice"
          open={isVisible}
          onCancel={() => onChangeStatus()}
          width="95%"
          footer={null}
        >
          <OtherExpenseModal
            formData={formDataValues}
            isEdit={true}
            modalCloseEvent={() => onChangeStatus()}
          />
        </Modal>
      ) : undefined}
      {/* column filtering show/hide */}
      {sidebarVisible ? (
        <SidebarColumnFilter
          columns={columns}
          sidebarVisible={sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
    </div>
  );
};

export default CargoDashboard;
