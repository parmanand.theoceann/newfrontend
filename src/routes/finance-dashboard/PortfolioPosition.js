import React, { Component } from 'react';
import { Table} from 'antd';

const columns1 = [
  {
    title: 'Voyage No',
    dataIndex: 'voyage_no',
  },

  {
    title: 'Voyage ops type',
    dataIndex: 'voyage_ops',
  },

  {
    title: 'Vessel',
    dataIndex: 'vessel',
  },

  {
    title: 'Cargo Id',
    dataIndex: 'cargo_id',
  },
  {
    title: 'Vessel Type',
    dataIndex: 'vessel_type',
  },

  {
    title: 'Cargo Type',
    dataIndex: 'cargo_type',
  },

  {
    title: 'Profit/Loss',
    dataIndex: 'profit_loss',
  },

  {
    title: 'Revenue',
    dataIndex: 'revenue',
  },

  {
    title: 'Demurrage',
    dataIndex: 'demurrage',
  },

  {
    title: 'Despacth',
    dataIndex: 'despacth',
  },

  {
    title: 'Add Com.',
    dataIndex: 'add_com',
  },

  {
    title: 'Broker Com.',
    dataIndex: 'broker_com',
  },

  {
    title: 'Miss Revenue',
    dataIndex: 'miss_revenue',
  },

  {
    title: 'Net Revenue',
    dataIndex: 'net_revenue',
  },

  {
    title: 'Vessel Expense',
    dataIndex: 'vessel_expense',
  },

  {
    title: 'Port Expense',
    dataIndex: 'port_expense',
  },

  {
    title: 'Other Expense',
    dataIndex: 'other_expense',
  },

  {
    title: 'Other Ledger',
    dataIndex: 'other_ledger',
  },

  {
    title: 'Profit Share',
    dataIndex: 'profit_share',
  },

  {
    title: 'Pool Point',
    dataIndex: 'pool_point',
  },

  {
    title: 'Pool Period',
    dataIndex: 'pool_period',
  },

  {
    title: 'Paid',
    dataIndex: 'paid',
  },

  {
    title: 'To be Paid',
    dataIndex: 'tobe_paid',
  },

  {
    title: 'Voyage Days',
    dataIndex: 'voyage_days',
  },

  {
    title: 'Period From',
    dataIndex: 'period_from',
  },

  {
    title: 'Period To',
    dataIndex: 'period_to',
  },
];
const data1 = [
  {
    key: '1',
    voyage_no: 'Value',
    voyage_ops: 'Value',
    vessel: 'Value',
    cargo_id: 'Value',
    vessel_type: 'Value',
    cargo_type: 'Value',
    profit_loss: 'Value',
    revenue: 'Value',
    demurrage: 'Value',
    despacth: 'Value',
    add_com: 'Value',
    broker_com: 'Value',
    miss_revenue: 'Value',
    net_revenue: 'Value',
    vessel_expense: 'Value',
    port_expense: 'Value',
    other_expense: 'Value',
    other_ledger: 'Value',
    profit_share: 'Value',
    pool_point: 'Value',
    pool_period: 'Value',
    paid: 'Value',
    tobe_paid: 'Value',
    voyage_days: 'Value',
    period_from: 'Value',
    period_to: 'Value',
  },

  {
    key: '2',
    voyage_no: 'Value',
    voyage_ops: 'Value',
    vessel: 'Value',
    cargo_id: 'Value',
    vessel_type: 'Value',
    cargo_type: 'Value',
    profit_loss: 'Value',
    revenue: 'Value',
    demurrage: 'Value',
    despacth: 'Value',
    add_com: 'Value',
    broker_com: 'Value',
    miss_revenue: 'Value',
    net_revenue: 'Value',
    vessel_expense: 'Value',
    port_expense: 'Value',
    other_expense: 'Value',
    other_ledger: 'Value',
    profit_share: 'Value',
    pool_point: 'Value',
    pool_period: 'Value',
    paid: 'Value',
    tobe_paid: 'Value',
    voyage_days: 'Value',
    period_from: 'Value',
    period_to: 'Value',
  },

  {
    key: '3',
    voyage_no: 'Value',
    voyage_ops: 'Value',
    vessel: 'Value',
    cargo_id: 'Value',
    vessel_type: 'Value',
    cargo_type: 'Value',
    profit_loss: 'Value',
    revenue: 'Value',
    demurrage: 'Value',
    despacth: 'Value',
    add_com: 'Value',
    broker_com: 'Value',
    miss_revenue: 'Value',
    net_revenue: 'Value',
    vessel_expense: 'Value',
    port_expense: 'Value',
    other_expense: 'Value',
    other_ledger: 'Value',
    profit_share: 'Value',
    pool_point: 'Value',
    pool_period: 'Value',
    paid: 'Value',
    tobe_paid: 'Value',
    voyage_days: 'Value',
    period_from: 'Value',
    period_to: 'Value',
  },

  {
    key: '4',
    voyage_no: 'Value',
    voyage_ops: 'Value',
    vessel: 'Value',
    cargo_id: 'Value',
    vessel_type: 'Value',
    cargo_type: 'Value',
    profit_loss: 'Value',
    revenue: 'Value',
    demurrage: 'Value',
    despacth: 'Value',
    add_com: 'Value',
    broker_com: 'Value',
    miss_revenue: 'Value',
    net_revenue: 'Value',
    vessel_expense: 'Value',
    port_expense: 'Value',
    other_expense: 'Value',
    other_ledger: 'Value',
    profit_share: 'Value',
    pool_point: 'Value',
    pool_period: 'Value',
    paid: 'Value',
    tobe_paid: 'Value',
    voyage_days: 'Value',
    period_from: 'Value',
    period_to: 'Value',
  },

  {
    key: '5',
    voyage_no: 'Value',
    voyage_ops: 'Value',
    vessel: 'Value',
    cargo_id: 'Value',
    vessel_type: 'Value',
    cargo_type: 'Value',
    profit_loss: 'Value',
    revenue: 'Value',
    demurrage: 'Value',
    despacth: 'Value',
    add_com: 'Value',
    broker_com: 'Value',
    miss_revenue: 'Value',
    net_revenue: 'Value',
    vessel_expense: 'Value',
    port_expense: 'Value',
    other_expense: 'Value',
    other_ledger: 'Value',
    profit_share: 'Value',
    pool_point: 'Value',
    pool_period: 'Value',
    paid: 'Value',
    tobe_paid: 'Value',
    voyage_days: 'Value',
    period_from: 'Value',
    period_to: 'Value',
  },
];
class PortfolioPosition extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <h4 className="mb-3">Portfolio Position</h4>
              <Table
                bordered
                size="small"
                pagination={false}
                dataSource={data1}
                scroll={{ x: 'max-content' }}
                columns={columns1}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortfolioPosition;
