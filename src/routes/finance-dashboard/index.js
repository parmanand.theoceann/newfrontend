import React, { useEffect } from "react";
import { Table, Popconfirm, Row, Col, Select, Card, Button } from "antd";
import { EditOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import { useNavigate } from "react-router-dom";
import BarChart from "../performancedashboard/RoundBarChart";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";
import PieChart from "../dashboard/charts/PieChart";
import DoughNutChart from "../dashboard/charts/DoughNutChart";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import StackGoupColumnChart from "../dashboard/charts/StackNormalizationChart";
import FunnelChart from "../dashboard/charts/FunnelChart";

const FinanceDashboardList = () => {
  const totalDashboarddat = [
    {
      title: "Count of total Voyage",
      value: "abcd",
    },
    {
      title: " Total Hire Receivable",
      value: "abcd",
    },
    {
      title: "Total Hire Payable ",
      value: "abcd",
    },
    {
      title: "Total Frt Amount",
      value: "abcd",
    },
    {
      title: "Total Claim Amount",
      value: "abcd",
    },
    {
      title: "Total OffHire Amount",
      value: "abcd",
    },
    {
      title: "Total Bunker Amount",
      value: "abcd",
    },
    {
      title: " Total Port Expense",
      value: "abcd",
    },
    {
      title: "Net Revenue",
      value: "abcd",
    },
    {
      title: "Net Expense",
      value: "abcd",
    },
    {
      title: "Total Profit/Loss",
      value: "abcd",
    },
  ];
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
  });

  const navigate = useNavigate();

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(e, record)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
                    <Popconfirm
                      title="Are you sure, you want to delete it?"
                      onConfirm={() => onRowDeletedClick(record)}
                    >
                     <DeleteOutlined /> />
                    </Popconfirm>
                  </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["chat-dash-list"]
        ? FIELDS["finance-dash-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/tcov/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };
  const redirectToAdd = (e, record = null) => {
    navigate(`/edit-voyage-estimate/${record.estimate_id}`);
  };

  const onCancel = () => {
    // getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  };

  /*
      onRowDeletedClick = (record) => {
        if ( record && record['tcov_status'] && record['tcov_status'].toUpperCase() === 'PENDING' ) {
          let _url = `${URL_WITH_VERSION}/tcov/delete`;
          apiDeleteCall(_url, { id: record.id }, (response) => {
            if (response && response.data) {
              openNotificationWithIcon('success', response.message);
              getTableData(1);
            } else {
              openNotificationWithIcon('error', response.message);
            }
          });
        } else {
          openNotificationWithIcon('error', "You can't delete TCOV Record as it is fixed ( OR Voyage Is Generated )");
        }
      };
    
    */

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${URL_WITH_VERSION}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    sidebarVisible,
    search,
    formData,
    isShowClaimInvoice,
    laytimeID,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));
  const ClusterDataxAxis = [
    "TCOVFULL304",
    "TCOVFULL305",
    "TCOVFULL306",
    "TCOVFULL307",
    "TCOVFULL308",
  ];
  const StackHorizontalChartyAxis = [
    "Western Europe",
    "Central America",
    "Oceania",
    "Southeastern Asia",
  ];
  const StackHorizontalChartSeries = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
    {
      name: "Total Port Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 777],
    },
  ];
  const StackHorizontalChartyAxis2 = [
    "FullName1",
    "FullName2",
    "FullName3",
    "FullName4",
  ];
  const StackHorizontalChartSeries2 = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      // stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599],
    },
  ];
  const ClusterDataSeries = [
    {
      name: "Total Revenue",
      type: "bar",
      barGap: 0,

      data: [320, 332, 301, 334, 390],
    },
    {
      name: " Total Expense,",
      type: "bar",

      data: [220, 182, 191, 234, 290],
    },
  ];
  const PieChartData = [
    { value: 40, name: "BALTIC VOYAGER" },
    { value: 30, name: "CARIBBEN PRINCESS" },
    { value: 13, name: "PACIFIC EXPLORER"},
    { value: 10, name: "GOODWYN ISLAND" },
    { value: 7, name: "OCEANIC MAJESTY" },
  ];
  const DoughNutChartSeriesData = [
    { value: 50, name: "The Atlantic" },
    { value: 13, name: "OCEANIC MAJESTY" },
    { value: 20, name: "CARGO SHIP" },
    { value: 10, name: "PACIFIC EXPLORER" },
    { value: 7, name: "GOODWYN ISLAND" },
  ];
  const StackNormalizationSeriesData = [
    {
      name: "Total Revenue",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [32, 33.2, 30.1, 33.4, 39], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Total Expense",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [22, 18.2, 19.1, 23.4, 29], // Normalize data by dividing by max and multiplying by 100
    },
    {
      name: "Net Revenue",
      type: "bar",
      stack: "stack1",
      label: {
        show: true,
        position: "insideTop", // Display labels inside bars
        formatter: "{c}%", // Show percentage values
      },
      data: [15, 23.2, 20.1, 15.4, 19], // Normalize data by dividing by max and multiplying by 100
    },
  ];
  return (
    <>
      <Row
        gutter={[16, 16]}
        style={{ justifyContent: "space-between", margin: "1rem 1rem" }}
      >
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Count of total Voyage
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              210
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Hire Receivable
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              150 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Hire Payable
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              120 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Frt Amount
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              540 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Claim Amount
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              300 $
            </p>
          </Card>
        </Col>
      </Row>
      <Row
        gutter={[16, 16]}
        style={{ justifyContent: "space-between", margin: "1rem 1rem" }}
      >
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total OffHire Amount
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              210 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Bunker Amount
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              150 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Total Port Expense
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              120 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Net Revenue
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              540 $
            </p>
          </Card>
        </Col>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                Net Expense
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              300 $
            </p>
          </Card>
        </Col>
      </Row>
      <Row gutter={[16]} style={{ margin: "1rem 1rem", gap: "6px" }}>
        <Col span={4}>
          <Card
            title={
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: "0.8rem",
                }}
              >
                 Total Profit/Loss
              </div>
            }
            bordered={false}
            style={{
              padding: 0,
              border: 0,
              borderRadius: 15,
              backgroundColor: "#1D406A",
            }}
            bodyStyle={{
              padding: 6,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                textAlign: "center",
                margin: 0,
                color: "white",
                fontSize: "1rem",
              }}
            >
              300 $
            </p>
          </Card>
        </Col>
        <Col span={16}>
          <Row
            gutter={[16, 16]}
            style={{ justifyContent: "space-between", marginTop: "1rem" }}
          >
            <Col span={4}>Vessel</Col>
            <Col span={4}>Voyage no.</Col>
            <Col span={4}>Voyage Type</Col>
            <Col span={4}>Commence date</Col>
            <Col span={4}>completion date</Col>
            <Col span={4}>Status</Col>
          </Row>
          <Row
            gutter={[16, 16]}
            style={{ justifyContent: "space-between", marginTop: "1rem" }}
          >
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "SUPRAMAX",
                    label: "SUPRAMAX",
                  },
                  {
                    value: "PANAMAX",
                    label: "PANAMAX",
                  },
                  {
                    value: "KAMSARMAX",
                    label: "KAMSARMAX",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-25",
                    label: "2024-01-25",
                  },
                  {
                    value: "2024-01-24",
                    label: "2024-01-24",
                  },
                  {
                    value: "2024-01-15",
                    label: "2024-01-15",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-08-13",
                    label: "2024-08-13",
                  },
                  {
                    value: "2024-01-24",
                    label: "2024-01-24",
                  },
                  {
                    value: "2024-01-15",
                    label: "2024-01-15",
                  },
                ]}
              />
            </Col>
            <Col span={4}>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PENDING",
                    label: "PENDING",
                  },
                  {
                    value: "FIX",
                    label: "FIX",
                  },
                  {
                    value: "SCHEDUED",
                    label: "SCHEDUED",
                  },
                ]}
              />
            </Col>
          </Row>
        </Col>
        <Col span={2}>
          <Button>Reset</Button>
        </Col>
      </Row>

      <div>
        <div className="row">
          {/* <div className="col-md-6">
            <ClusterColumnChart
              Heading={"Net Revenue Vs Net Expense"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </div> */}
          {/* <div className="col-md-6">
            <StackGoupColumnChart
              Heading={"Invoice Amount Per Voyage No."}
              StackNormalizationSeriesData={StackNormalizationSeriesData}
            />
          </div> */}
        </div>
        {/* <div className="row">
          <div className="col-md-6">
            <PieChart
              selectedMode={"multiple"}
              PieChartData={PieChartData}
              Heading={"Net Revenue Per Voyage"}
            />
          </div>
          <div className="col-md-6">
            <DoughNutChart
              Heading={"Net Expenses Per Voyage"}
              DoughNutChartSeriesData={DoughNutChartSeriesData}
              DoughNutChartSeriesRadius={["20%", "70%"]}
            />
          </div>
        </div> */}
        <div className="row">
          {/* <div className="col-md-6">
            <StackHorizontalChart
              Heading={"Hire Receivable Amount Vs Hire Payable Amount"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              StackHorizontalChartSeries={StackHorizontalChartSeries}
            />
          </div> */}
          {/* <div className="col-md-6">
            <StackHorizontalChart
              Heading={"Dashboard BarChart"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis2}
              StackHorizontalChartSeries={StackHorizontalChartSeries2}
            />
          </div> */}
        </div>
        <div className="row">
          {/* <div className="col-md-6">
            <FunnelChart
              Heading={"Dashboard BarChart"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              StackHorizontalChartSeries={StackHorizontalChartSeries}
            />
          </div>
          <div className="col-md-6"></div> */}
        </div>
        <Row gutter={16}>
          <Col span={12}>
            <ClusterColumnChart
              Heading={"Net Revenue Vs Net Expense"}
              ClusterDataxAxis={ClusterDataxAxis}
              ClusterDataSeries={ClusterDataSeries}
              maxValueyAxis={"500"}
            />
          </Col>
          <Col span={12}>
            <StackGoupColumnChart
              Heading={"Invoice Amount Per Voyage No."}
              StackNormalizationSeriesData={StackNormalizationSeriesData}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <PieChart
              selectedMode={"multiple"}
              PieChartData={PieChartData}
              Heading={"Net Revenue Per Voyage"}
            />
          </Col>
          <Col span={12}>
            <DoughNutChart
              Heading={"Net Expenses Per Voyage"}
              DoughNutChartSeriesData={DoughNutChartSeriesData}
              DoughNutChartSeriesRadius={["20%", "70%"]}
              style={{height:"330px"}}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <StackHorizontalChart
              Heading={"Hire Receivable Amount Vs Hire Payable Amount"}
              StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              StackHorizontalChartSeries={StackHorizontalChartSeries}
            />
          </Col>
          <Col span={12}>
            <FunnelChart
              Heading={"Profit/Loss Per Voyage"}
              // StackHorizontalChartyAxis={StackHorizontalChartyAxis}
              // StackHorizontalChartSeries={StackHorizontalChartSeries}
            />
          </Col>
        </Row>
      </div>

      {/* <ChatringDashList /> */}

      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span> Finance Dashboard List</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: "100%",
                  marginBottom: "10px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
              >
                {state.loading === false ? (
                  <ToolbarUI
                    routeUrl={"Finance-Dashboard-list"}
                    optionValue={{
                      pageOptions: state.pageOptions,
                      columns: state.columns,
                      search: state.search,
                    }}
                    callback={(e) => callOptions(e)}
                    // filter={filter}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", "tcov"),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", "tcov"),
                      },
                      {
                        title: "XLS",
                        event: () => onActionDonwload("xlsx", "tcov"),
                      },
                    ]}
                  />
                ) : undefined}
              </div>
              <div>
                <Table
                  // rowKey={record => record.id}
                  className="inlineTable resizeableTable"
                  bordered
                  columns={tableColumns}
                  components={components}
                  size="small"
                  scroll={{ x: "max-content" }}
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
              </div>
            </div>
          </div>
        </article>

        {/* column filtering show/hide */}
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={tableColumns}
            sidebarVisible={sidebarVisible}
            callback={(e) => callOptions(e)}
          />
        ) : null}
      </div>
    </>
  );
};

export default FinanceDashboardList;
