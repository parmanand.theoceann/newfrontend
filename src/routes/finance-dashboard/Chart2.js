import React, { useState } from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import { Row, Col, Form, DatePicker, Input } from 'antd';
import { useEffect } from 'react';

const FormItem = Form.Item;

const Chart = (props) => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [openingBalance, setOpeningBalance] = useState(props.openingBalance || 0);
  const [closingBalance, setClosingBalance] = useState(props.closingBalance || 0);

  let pie1 = {};

  pie1.option = {
    title: {
      text: `Opening Balance ($) ${openingBalance} / Closing Balance ${closingBalance}`,
      x: 'center'
    },
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    //   legend: {
    //     orient: 'vertical',
    //     x: 'left',
    //     data: ['Balance $'],
    //     textStyle: {
    //       color: CHARTCONFIG.color.text
    //     }
    //   },
    //   toolbox: {
    //     show: true,
    //     feature: {
    //       saveAsImage: {show: true, title: 'save'}
    //     }
    //   },
    calculable: true,
    series: [
      {
        name: 'Opening Balance Closing Balance',
        type: 'pie',
        radius: '55%',
        center: ['50%', '60%'],
        data: [
          { value: openingBalance, name: 'Opening Balance $' },
          { value: closingBalance, name: 'Closing Balance $' }
        ],
        color:['#7FE817','#FF6700'],
      }
    ]
  };

  useEffect(() => {
    if (props.openingBalance && props.openingBalance !== openingBalance)
      setOpeningBalance(props.openingBalance);

    if (props.closingBalance && props.closingBalance !== closingBalance)
      setClosingBalance(props.closingBalance);
  }, [props]);

  const onStartDateChange = (date) => {
    setStartDate(date)
    props.onSelectionCriteriaChanged(date, endDate);
  };

  const onEndDateChange = (date) => {
    setEndDate(date)
    props.onSelectionCriteriaChanged(startDate, date);
  };

  return (
    <div className="box box-default mb-4">
      <div className="box-header"><h4 className='Char_dashboard'>Opening Balance ($) / Closing Balance($)</h4></div>
      <div className="box-body">
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="mb-4">
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <FormItem label="Start Date">
              <DatePicker value={startDate} onChange={onStartDateChange} />
            </FormItem>
          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <FormItem label="End Date">
              <DatePicker value={endDate} onChange={onEndDateChange} />
            </FormItem>
          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <FormItem label="Opening Balance">
              <Input name="openingBalance" value={openingBalance} placeholder="Amount in $" onChange={(e) => setOpeningBalance(e.target.value)} disabled />
            </FormItem>
          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <FormItem label="Closing Balance">
              <Input name="closingBalance" value={closingBalance} placeholder="Amount in $" onChange={(e) => setClosingBalance(e.target.value)} disabled />
            </FormItem>
          </Col>
        </Row>
        <ReactEcharts option={pie1.option} theme={"macarons"} />
      </div>
    </div>
  )
}

export default Chart;