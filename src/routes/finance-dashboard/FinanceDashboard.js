import React, { Component } from 'react';
import { Drawer, Layout, Table, Modal, Popconfirm } from 'antd';
import { EditOutlined } from '@ant-design/icons'; 
import Chart from './Chart';
import Chart2 from './Chart2';
import PortfolioPosition from './PortfolioPosition';
import RightBarUI from '../../components/RightBarUI';
import moment from 'moment';

import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
} from "../../shared";
import { FIELDS } from "../../shared/tableFields";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import Tde from "../tde/Tde";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";

const { Content } = Layout;

class FinanceDashboard extends Component {
  constructor(props) {
    super(props);
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["transaction-summary-list"]
        ? FIELDS["transaction-summary-list"]["tableheads"]
        : []
    );
    tableHeaders.push({
      title: 'Action',
      key: 'action',
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => this.redirectToAdd(e, record.id)}
            >
         <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    });

    this.state = {
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
      width: 1200,
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      openingBalance: 10,
      closingBalance: 20,
      openingBalanceChart2: 10,
      closingBalanceChart2: 20,
      typesearch:{},
      donloadArray: []
    };
  }

  componentDidMount = () => {
    this.getTableData();
  };

  getTableData = async (searchtype = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: "desc" } };
    let search=searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue')?searchtype:this.state.typesearch;

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim()

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
      this.state.typesearch={'searchOptions':search.searchOptions,'searchValue':search.searchValue}

    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/tde/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    // console.log('@@@', { data });

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    let donloadArr = []
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      state["responseData"] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };

  onCloseDrawer = () =>
    this.setState({
      ...this.state,
      visibleDrawer: false,
      title: undefined,
      loadComponent: undefined,
    });

  onClickRightMenu = (key, options) => {
    this.onCloseDrawer();
    let loadComponent = undefined;
    switch (key) {
      case "portfolio-position":
        loadComponent = <PortfolioPosition />;
        break;
    }

    this.setState({
      ...this.state,
      visibleDrawer: true,
      title: options.title,
      loadComponent: loadComponent,
      width: options.width && options.width > 0 ? options.width : 1200,
    });
  };

  redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/edit?e=${id}`);
      const respData = await response["data"];
      this.setState(
        { ...this.state, isAdd: false, formDataValues: respData },
        () => this.setState({ ...this.state, isVisible: true })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  // onRowDeletedClick = (id) => {
  //   let _url = `${URL_WITH_VERSION}/tde/delete`;
  //   apiDeleteCall(_url, { id: id }, (response) => {
  //     if (response && response.data) {
  //       openNotificationWithIcon("success", response.message);
  //       this.getTableData(1);
  //     } else {
  //       openNotificationWithIcon("error", response.message);
  //     }
  //   });
  // };

  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(evt);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions,typesearch:{} },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  handleUpdateClose=()=>{
    this.setState({ ...this.state,isVisible: false},()=> this.getTableData());
   
  }
  //resizing function
  handleResize = (index) => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns, pageOptions, donloadArray } = this.state;  
    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
  // if (cols && cols.length > 0) {
  //   params = params + '&c=' + cols.join(',')
  // }
    const filter = donloadArray.join()
  // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
}

  selectionCriteriaChanged(startDate, endDate) {
    if (!endDate) endDate = moment();
    if (!startDate) startDate = moment("1970-01-01");

    const records = this.state.responseData.filter((item) => {
      return (
        moment(item.accounting_date).isAfter(
          moment(startDate).add(-1, "days"),
          "day"
        ) &&
        moment(item.accounting_date).isBefore(
          moment(endDate).add(1, "days"),
          "day"
        )
      );
    });

    if (Array.isArray(records) && records.length > 0) {
      this.setState({ ...this.state, openingBalance: 9999 });
    }
  }

  selectionCriteriaChart2Changed(startDate, endDate) {
    if (!endDate) endDate = moment();
    if (!startDate) startDate = moment("1970-01-01");

    const records = this.state.responseData.filter((item) => {
      return (
        moment(item.accounting_date).isAfter(
          moment(startDate).add(-1, "days"),
          "day"
        ) &&
        moment(item.accounting_date).isBefore(
          moment(endDate).add(1, "days"),
          "day"
        )
      );
    });

    // console.log('@@@@');
    if (Array.isArray(records) && records.length > 0) {
      const openingBalanceChart2 = 12000;
      let closingBalanceChart2 = 0;
      if (records.length > 1) {
        closingBalanceChart2 = 16500;
      }
      this.setState({
        ...this.state,
        openingBalanceChart2,
        closingBalanceChart2,
      });
    }
  }

  render() {
    const {
      columns,
      visibleDrawer,
      loadComponent,
      title,
      loading,
      responseData,
      pageOptions,
      search,
      isAdd,
      isVisible,
      formDataValues,
      sidebarVisible,
    } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));

    return (
      <div className="wrap-rightbar full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="fieldscroll-wrap">
                <div className="body-wrapper modalWrapper">
                  <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                      <div className="box-body">
                        <div className="row">
                          {/* <div className="col-md-6">
                            <Chart onSelectionCriteriaChanged={(startDate, endDate) => this.selectionCriteriaChanged(startDate, endDate)}
                              openingBalance={this.state.openingBalance} />
                          </div>
                          <div className="col-md-6">
                            <Chart2
                              onSelectionCriteriaChanged={(startDate, endDate) => this.selectionCriteriaChart2Changed(startDate, endDate)}
                              openingBalance={this.state.openingBalanceChart2}
                              closingBalance={this.state.closingBalanceChart2} />
                          </div> */}

                          <div>
                            <iframe
                              title="Report Section"
                              style={{ width: "100%", height: "110vh" }}
                              src="https://app.powerbi.com/view?r=eyJrIjoiNzViZGYyYmMtZGFjMy00OGQwLWFlNzQtMGUwZDJkMGMyN2EyIiwidCI6IjA0YWVkYTA0LWExYTctNDZiOC1hNTgyLTJhOTVjY2Q4ZmUzMiJ9"
                              frameborder="0"
                              allowFullScreen={true}
                            />

                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>

                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body">
                        <div className="form-wrapper">
                          <div className="form-heading">
                            <h4 className="title">
                              <span>Transaction Summary List</span>
                            </h4>
                          </div>
                        </div>
                        <div
                          className="section"
                          style={{
                            width: "100%",
                            marginBottom: "10px",
                            paddingLeft: "15px",
                            paddingRight: "15px",
                          }}
                        >
                          {loading === false ? (
                            <ToolbarUI
                              routeUrl={"transaction-summary-toolbar"}
                              optionValue={{
                                pageOptions: pageOptions,
                                columns: columns,
                                search: search,
                              }}
                              callback={(e) => this.callOptions(e)}
                              dowloadOptions={[
                                {
                                  title: "CSV",
                                  event: () =>
                                    this.onActionDonwload("csv", "transaca"),
                                },
                                {
                                  title: "PDF",
                                  event: () =>
                                    this.onActionDonwload("pdf", "transaca"),
                                },
                                {
                                  title: "XLS",
                                  event: () =>
                                    this.onActionDonwload("xlsx", "transaca"),
                                },
                              ]}
                            />
                          ) : (
                            undefined
                          )}
                        </div>
                        <div>
                          <Table
                            rowKey={(record) => record.id}
                            className="inlineTable editableFixedHeader resizeableTable"
                            bordered
                            scroll={{ x: "max-content" }}
                            // scroll={{ y: 370 }}
                            components={this.components}
                            columns={tableColumns}
                            size="small"
                            dataSource={responseData}
                            loading={loading}
                            pagination={false}
                            rowClassName={(r, i) =>
                              i % 2 === 0
                                ? "table-striped-listing"
                                : "dull-color table-striped-listing"
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Content>
          </Layout>

          {isVisible === true ? (
            <Modal
              className="page-container"
              style={{ top: "2%" }}
              title="TDE"
              open={isVisible}
              onCancel={this.onCancel}
              width="95%"
              footer={null}
            >
              <Tde
                receivablePayableType="78"
                invoiceType="TDE"
                formData={formDataValues}
                isEdit={true}
                saveUpdateClose={this.handleUpdateClose}
              />
            </Modal>
          ) : (
            undefined
          )}

          <RightBarUI
            pageTitle="finance-dashboard-righttoolbar"
            callback={(data, options) => this.onClickRightMenu(data, options)}
          />
          {loadComponent !== undefined &&
          title !== undefined &&
          visibleDrawer === true ? (
            <Drawer
              title={this.state.title}
              placement="right"
              closable={true}
              onClose={this.onCloseDrawer}
             open={this.state.visibleDrawer}
              getContainer={false}
              style={{ position: "absolute" }}
              width={this.state.width}
              maskClosable={false}
              className="drawer-wrapper-container"
            >
              <div className="tcov-wrapper">
                <div className="layout-wrapper scrollHeight">
                  <div className="content-wrapper noHeight">
                    {this.state.loadComponent}
                  </div>
                </div>
              </div>
            </Drawer>
          ) : (
            undefined
          )}
        </Layout>
      </div>
    );
  }
}
export default FinanceDashboard;
