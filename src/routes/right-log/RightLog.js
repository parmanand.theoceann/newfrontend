import React, { Component } from 'react';

class RightLog extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper m-0">
        <article className="article toolbaruiWrapper">
          <div className="box box-default mb-2">
            <div className="box-body">
                <p className="m-0">Change Cargo, Bill Via</p>
                <p className="m-0">01/15/2021 <span className="ml-3">19:17</span></p>
                <p className="m-0">By: <small>Admin</small></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <p className="m-0">Change Cargo, Bill Via</p>
                <p className="m-0">01/15/2021 <span className="ml-3">19:17</span></p>
                <p className="m-0">By: <small>Admin</small></p>
            </div>
          </div>

          <div className="box box-default mb-2">
            <div className="box-body">
                <p className="m-0">Change Cargo, Bill Via</p>
                <p className="m-0">01/15/2021 <span className="ml-3">19:17</span></p>
                <p className="m-0">By: <small>Admin</small></p>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default RightLog;

