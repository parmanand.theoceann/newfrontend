import React, { Component } from 'react';
import {} from 'antd';

class EmailTcov extends Component {
  render() {
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <p className="m-0">Dear Sir</p>
              <p>Date: xx</p>
              <p>
                Vessel Has been fixed by User for Time Charter Voyage Out Basis with commencement
                Date ddmmyy.
              </p>
              <p className="m-0">Thanks and Regards</p>
              <p>Theoceann.com</p>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default EmailTcov;
