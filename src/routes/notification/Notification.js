import React, { Component } from 'react';
import { Table,  Checkbox, Modal } from 'antd';
import {LeftCircleOutlined} from '@ant-design/icons';
import EmailTcov from './EmailTcov';

const data = [
  {
    key: 1,
    module: 'Charteing',

    children: [
      {
        key: 11,
        page_name: 'TCOV',
        event: 'Fixed and Schedule',
        subject: 'Estimate ID/ Vessel/Cargo Name: Fixed By user',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User email',
        cc_email: 'My company email',
        cc_email_two: 'EMial 2',
      },

      {
        key: 11,
        page_name: 'Voy Relet',
        event: 'Fixed and schedule',
        subject: ' Estimate ID/ Vessel/Cargo Name: Fixed By user',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User email',
        cc_email: 'My company email',
        cc_email_two: 'EMial 2',
      },

      {
        key: 11,
        page_name: 'TCTO',
        event: 'Fixed and schedule',
        subject: ' Estimate ID/ Vessel/Cargo Name: Fixed By user',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User email',
        cc_email: 'My company email',
        cc_email_two: 'EMial 2',
      },
    ],
  },

  {
    key: 2,
    module: 'Operation',

    children: [
      {
        key: 12,
        page_name: 'Create Agency Appointment',
        event: 'Save and Send',
        subject: 'PortCall ID/ Vessel Name/ Port/ Activity/ Agency appointment Sent',
        from_email: 'Notification@Theoceann.com',
        to_email: 'Agent Email',
        cc_email: 'user email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'PDA expenses (agent Panel)',
        event: 'PDA Submitted by agent',
        subject: 'PortCall ID/ Vessel Name/ Port/ Activity/ PDA Submitted by agent',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'Agent email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'PDA expenses (User Panel)',
        event: 'PDA Approved By user',
        subject: 'PortCall ID/ Vessel Name/ Port/ Activity/ PDA Approved By User',
        from_email: 'Notification@Theoceann.com',
        to_email: 'Agent Email',
        cc_email: 'user email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'Port Expenses ( User Panel)',
        event: 'PDA resubmit',
        subject: 'PortCall ID/ Vessel Name/ Port/ Activity/ PDA resubmit',
        from_email: 'Notification@Theoceann.com',
        to_email: 'Agent Email',
        cc_email: 'user email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'Port Expenses ( User Panel)',
        event: 'FDA Approved By user',
        subject: 'PortCall ID/ Vessel Name/ Port/ Activity/ FDA Approved By user',
        from_email: 'Notification@Theoceann.com',
        to_email: 'Agent Email',
        cc_email: 'user email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'Port Activity( agent Panel)',
        event: 'Port activity submitted by agent',
        subject: 'PortCall ID/ Vessel Name/ Port/ Port Activity has been submitted',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'Agent email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'Port Information( agent Panel)',
        event: 'Port information has been updated',
        subject: 'PortName / Port information has been updated',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'Agent email',
        cc_email_two: 'My company email',
      },

      {
        key: 12,
        page_name: 'Voyage Manager',
        event: 'Status Changed to “Commenced”',
        subject: 'Voyage no/ Vessel Name/ Ops Type  Status changed to Commenced ',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'email',
        cc_email_two: 'email',
      },

      {
        key: 12,
        page_name: 'Bunker requirement Page',
        event: 'Send Email',
        subject: 'Voyage No./ Ops type/ Vessel Name/ Bunker Enquiry raised',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'User email',
        cc_email_two: 'My company',
      },

      {
        key: 12,
        page_name: 'Make Payment On TCI Page',
        event: 'Make payment Invoice created',
        subject: 'Vessel / TCI Code/ Invoice No./ Raised',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'email',
        cc_email_two: 'Emial',
      },

      {
        key: 12,
        page_name: 'TCI Page / Notice  Alerts',
        event: 'Redelivery/Delivery Date- Notice days',
        subject: 'Vessel Name/ Notice type ',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'Email',
        cc_email_two: 'Email',
      },

      {
        key: 12,
        page_name: 'Report Type ( Noon report/ arrival- departure report)',
        event: 'as and when any report hits data base',
        subject: 'Vessel Name/ report type Submitted / report date.',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User Email',
        cc_email: 'Email',
        cc_email_two: 'Email',
      },

      {
        key: 12,
        page_name: 'Noon report',
        event: 'SPD variation %  when red',
        subject: 'Vessel / Daily speed alert ',
        from_email: 'Notification@Theoceann.com',
        to_email: 'User',
        cc_email: 'Email',
        cc_email_two: 'Email',
      },
    ],
  },

];

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: 'Modules',
          dataIndex: 'module',
        },
        {
          title: 'Page Name',
          dataIndex: 'page_name',
        },
        {
          title: 'Event Name',
          dataIndex: 'event',
        },

        {
          title: 'Subject',
          dataIndex: 'subject',
        },
        {
          title: 'Email Content',
          dataIndex: 'email_content',
          render: text => {
            return (
              <button
                className="btn ant-btn-primary btn-sm"
                onClick={() => this.showHideModal(true, 'Showemail')}
              >
                View
              </button>
            );
          },
        },
        {
          title: 'From Email',
          dataIndex: 'from_email',
        },
        {
          title: 'To Email',
          dataIndex: 'to_email',
        },
        {
          title: 'CC Email',
          dataIndex: 'cc_email',
        },
        {
          title: 'CC Email 2',
          dataIndex: 'cc_email_two',
        },
        {
          title: 'Activate/Deactivate',
          dataIndex: 'activate_deactivate',
          render(text) {
            return <Checkbox />;
          },
        },
      ],
      modals: {
        Showemail: false,
      },
    };
  }

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  render() {
    const { columns } = this.state;

    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-heading mb-3">
                <h4 className="title">
                  <span>Notification</span>
                  <span className="float-right">
                    <a href="#/user-dashboard" className="btn ant-btn-primary pl-2 pr-2 pt-1 pb-1">
                    <LeftCircleOutlined />
                    </a>
                  </span>
                </h4>
              </div>
              <Table
                className="pl-summary-list-view"
                columns={columns}
                dataSource={data}
                pagination={false}
                bordered
                scroll={{ x: 'max-content' }}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
              />
            </div>
          </div>
        </article>

        <Modal
          style={{ top: '2%' }}
          title="Email Tcov"
         open={this.state.modals['Showemail']}
          onCancel={() => this.showHideModal(false, 'Showemail')}
          width="60%"
          footer={null}
        >
          <EmailTcov />
        </Modal>
      </div>
    );
  }
}

export default Notification;
