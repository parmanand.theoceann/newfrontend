import React from 'react';
import { Route } from 'react-router-dom';

import PortActivityList from './port-activity-list';
import PortFunctions from './port-functions';


const Port = ({ match }) => (
    <div>
        <Route path={`${match.url}/port-activity-list`} component={PortActivityList} />
        <Route path={`${match.url}/port-functions`} component={PortFunctions} />
      
    </div>
)

export default Port;