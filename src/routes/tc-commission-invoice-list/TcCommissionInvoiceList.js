import React, { Component } from 'react';
import { Table, Popconfirm,Modal } from 'antd';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import { FIELDS } from '../../shared/tableFields';
import URL_WITH_VERSION, { getAPICall, objectToQueryStringFunc, useStateCallback} from '../../shared';
import { apiDeleteCall } from '../../shared';
import { openNotificationWithIcon } from '../../shared';
import CommissionPayment from '../../shared/components/CommissionPayment';
import { EditOutlined } from '@ant-design/icons';
import { useEffect } from 'react';


const  TcCommissionInvoiceList  = (props) => {

  const [state, setState] = useStateCallback({
      responseData: { frm: [], tabs: [], active_tab: {} },
      frmKey: "frm_key_" + props.frmCode,
      isVisiblePayment: false,
      frmName: "tc_commission_entry",
      formData: props.formData || {},
      commissionData: props.commissionData || {},
      frmOptions: props.frmOptions || [],
      loading: false,
      columns: [],
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      typesearch:{},
      donloadArray: []
  })
  

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => {
                editCommision(e, record);
              }}
            >
              <EditOutlined/>
            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["tc-commision-invoice-list"]
        ? FIELDS["tc-commision-invoice-list"]["tableheads"]
        : []
    );
    tableHeaders.push(tableAction);
      
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  },[])



  const onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/commission/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
    
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
     
      }
    
      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }
    
      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }
    

    setState(prev => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/commission/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state['responseData'] = dataArr;
    }
    setState(prev => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };


 
  const editCommisionData = async (invoice_no) => {
    try {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/commission/edit?e=${invoice_no}`
      );
      const respData = await response["data"];

      const response1 = await getAPICall(
        `${URL_WITH_VERSION}/accounts/borker/commission?t=tci&e=${
          respData.tci_id
        }`
      );
      const respSData = await response1["data"];
  
      respData["mycompny_id"] = respSData["mycompny_id"];

      respData["."] = {
        total: respData["inv_total"],
      };
      const frmOptions = [
        { key: "broker", data: respSData["brokers"] },
        { key: "remmitance_bank", data: respSData["banks"] },
      ];
      // let data1 = {
      //   'broker': respData.inv_type,
      //   'tci_id': respData.tci_id
      //  }
      //  let suURL = `${URL_WITH_VERSION}/accounts/borker/commission`;
      //  let suMethod = 'POST';
      //  postAPICall(suURL, data1, suMethod, data => {
      //   if (data && data.data) {
      //     Object.assign(respData, data.data)
      //   }});
      //   setTimeout(() => {
      //     setState({ isEdit: true, editData: respData, isVisiblePayment:true ,frmOption : frmOptions})
      //   }, 2000, this);
      Object.assign(respData, { disablefield: ["broker"] });
      setState(prevState => ({...prevState, isEdit: true, editData: respData, isVisiblePayment: true, frmOption: frmOptions,}))

    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong.", 5);
    }
  };

  const editCommision = async (action, data) => {
    if (data && data.invoice_no) {
      editCommisionData(data.invoice_no);
    }
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;
  
      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;
  
      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);
  
      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
  
      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];
  
      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }
  
      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
    };

  const onCancelPayment = (e) => {
    setState({ ...state, isVisiblePayment: false },()=>{
      // getTableData()
    });
  };

  const handleResize = index => (e, { size }) => {
    setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };


  const onActionDonwload = (downType, pageType) => {
      let params = `t=${pageType}`, cols = [];
      const { columns, pageOptions, donloadArray } = state;  
    let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };
    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join()
    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`, '_blank');
  }

  const closeModal = () => {
    setState({ ...state, isVisiblePayment: false }, ()=> getTableData())
  }



    const {
      responseData,
      columns,
      pageOptions,
      search,
      loading,
      sidebarVisible,
      isEdit,
      isVisiblePayment,
      oldFromData,
      frmOption,
      commissionData,
      frmName,
      editData,
    } = state;



    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));


    return (
      <>
        <div className="body-wrapper">
          <article className="article">
            <div className="box box-default">
              <div className="box-body">
                {loading === false ? (
                  <ToolbarUI
                    routeUrl={"tc-commission-invoice-list"}
                    optionValue={{
                      pageOptions: pageOptions,
                      columns: columns,
                      search: search,
                    }}
                    callback={(e) => callOptions(e)}
                    dowloadOptions={[
                      {
                        title: "CSV",
                        event: () => onActionDonwload("csv", 'commission-invoice'),
                      },
                      {
                        title: "PDF",
                        event: () => onActionDonwload("pdf", 'commission-invoice'),
                      },
                      {
                        title: "XLSX",
                        event: () => onActionDonwload("xlsx", 'commission-invoice'),
                      },
                    ]}
                  />
                ) : (
                  undefined
                )}
                <Table
                  className="mt-3"
                  bordered
                  size="small"
                  scroll={{ x: "max-content" }}
                  columns={tableColumns}
                  loading={loading}
                  dataSource={responseData}
                  pagination={false}
                  footer={false}
                  rowClassName={(r, i) =>
                    i % 2 === 0
                      ? "table-striped-listing"
                      : "dull-color table-striped-listing"
                  }
                />
                {sidebarVisible ? (
                  <SidebarColumnFilter
                    columns={columns}
                    sidebarVisible={sidebarVisible}
                    callback={(e) => callOptions(e)}
                  />
                ) : null}
                {isVisiblePayment && (
                  <Modal
                    title="Time Charter Commission Payment"
                   open={isVisiblePayment}
                    width="80%"
                    style={{ top: "2%" }}
                    onCancel={onCancelPayment}
                    footer={null}
                  >
                    {frmName ? (
                      <CommissionPayment
                        isEdit={isEdit}
                        oldFromData={oldFromData}
                        frmOptions={frmOption}
                        commissionData={
                          isEdit === true ? editData : commissionData
                        }
                        closeModal={() =>closeModal()}
                      />
                    ) : (
                      undefined
                    )}
                  </Modal>
                )}
              </div>
            </div>
          </article>
        </div>
      </>
    );
  }
export default TcCommissionInvoiceList;
