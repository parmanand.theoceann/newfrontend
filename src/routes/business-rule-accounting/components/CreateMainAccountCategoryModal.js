import React, { Component } from 'react';
import { Table, Button } from 'antd';

const columns = [
  {
    title: 'Main Category Short Code',
    dataIndex: 'main_sc',
  },

  {
    title: 'Main Category Description',
    dataIndex: 'main_des',
  },

];
const data = [
  {
    key: '1',
    main_sc: 'PORTX',
    main_des: 'Port expenses',
  },
  {
    key: '2',
    main_sc: 'FINV',
    main_des: 'Freight Invoice',
  },

];

class CreateMainAccountCategoryModal extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    bordered
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    footer={() => (
                      <div className="text-center">
                        <Button type="link">Add New</Button>
                      </div>
                    )}
                    rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default CreateMainAccountCategoryModal;
