import React, { Component } from 'react';
import { Table, Modal } from 'antd';
import CreateMainAccountCategoryModal from './CreateMainAccountCategoryModal';

const columns = [
  {
    title: 'Main Category SC',
    dataIndex: 'main_sc',
  },

  {
    title: 'Main Category Descpt',
    dataIndex: 'main_des',
  },
  {
    title: 'Sub category SC',
    dataIndex: 'sub_sc',
  },
  {
    title: 'Sub category Descrpition',
    dataIndex: 'sub_des',
  },
  {
    title: 'Accounting Code',
    dataIndex: 'accounting_code',
  },
  {
    title: ' Financial Transaction Type',
    dataIndex: 'financial_type',
  },
  {
    title: 'Finannce Category',
    dataIndex: 'finannce_cat',
  },
];
const data = [
  {
    key: '1',
    main_sc: 'PORTX',
    main_des: 'Port expenses',
    sub_sc: 'PDA',
    sub_des: 'Agency Communication',
    accounting_code: '210001',
    financial_type: 'Receivable',
    finannce_cat: 'Revenue',
  },

  {
    key: '2',
    main_sc: 'FINV',
    main_des: 'Freight Invoice',
    sub_sc: 'FTCOV',
    sub_des: 'Freight Invoice TCOV',
    accounting_code: '220001',
    financial_type: 'Payable    ',
    finannce_cat: 'Expense',
  },
];

class BusinessRuleAccounting extends Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div class="form-wrapper">
                <div class="form-heading">
                  <h4 class="title">
                    <span>Business Rule of Accounting</span>
                  </h4>
                </div>
                <div class="action-btn">
                  <button type="button" class="ant-btn ant-btn-primary" onClick={this.showModal}>
                    <span>Create Main account category</span>
                  </button>
                </div>
              </div>

              <div className="row p10">
                <div className="col-md-12">
                  <Table bordered columns={columns} dataSource={data} pagination={false}
                  scroll={{x: 'max-content' }}
                  rowClassName={(r,i) => ((i%2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                  />
                </div>
              </div>
            </div>
          </div>
        </article>

        <Modal
          style={{ top: '2%' }}
          title="Create Main account category"
          open={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width="50%"
          footer={null}
        >
          <CreateMainAccountCategoryModal />
        </Modal>
      </div>
    );
  }
}

export default BusinessRuleAccounting;
