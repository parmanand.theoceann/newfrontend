import React, { Component, useEffect } from 'react';
import { useNavigate } from 'react-router-dom'
import { Table,  Popconfirm } from 'antd';
import URL_WITH_VERSION, { URL_WITHOUT_VERSION, getAPICall, postAPICall, objectToQueryStringFunc, apiDeleteCall, openNotificationWithIcon, ResizeableTitle, useStateCallback } from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import { EditOutlined } from '@ant-design/icons';


const TCTOList = () => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    typesearch: {},
    donloadArray: [],
  });
  const navigate = useNavigate();


    useEffect(() => {
      const tableAction = {
        title: 'Action',
        key: 'action',
        fixed: 'right',
        width: 70,
        render: (text, record) => {
          return (
            <div className="editable-row-operations">
              <span className="iconWrapper" onClick={(e) => redirectToAdd(e, record)}>
               <EditOutlined/>
              </span>
              {/* <span className="iconWrapper cancel">
                <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(record.id)}>
                 <DeleteOutlined /> />
                </Popconfirm>
              </span> */}
            </div>
          )
        }
      };
      let tableHeaders = Object.assign([], FIELDS && FIELDS['tcto-list'] ? FIELDS['tcto-list']["tableheads"] : [])

      tableHeaders.push(tableAction)
      setState({ ...state, columns: tableHeaders }, () => {
        getTableData();
      });
    }, []);

    const getTableData = async (search = {}) => {
      const { pageOptions } = state;
  
      let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
      let headers = { order_by: { id: 'desc' } };
  
      if (
        search &&
        search.hasOwnProperty("searchValue") &&
        search.hasOwnProperty("searchOptions") &&
        search["searchOptions"] !== "" &&
        search["searchValue"] !== ""
      ) {
        let wc = {};
        search["searchValue"] = search["searchValue"].trim();
      
        if (search["searchOptions"].indexOf(";") > 0) {
          let so = search["searchOptions"].split(";");
          wc = { OR: {} };
          so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
        } else {
          wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };
       
        }
      
        if (headers.hasOwnProperty("where")) {
          // If "where" property already exists, merge the conditions
          headers["where"] = { ...headers["where"], ...wc };
        } else {
          // If "where" property doesn't exist, set it to the new condition
          headers["where"] = wc;
        }
      
        state.typesearch = {
          searchOptions: search.searchOptions,
          searchValue: search.searchValue,
        };
      }
      
  
      setState(prev => ({
        ...prev,
        loading: true,
        responseData: [],
      }));
  
      let qParamString = objectToQueryStringFunc(qParams);
  
      let _url =`${URL_WITH_VERSION}/tcto/list?${qParamString}`;
      const response = await getAPICall(_url, headers);
      const data = await response;
  
      const totalRows = data && data.total_rows ? data.total_rows : 0;
      let dataArr = data && data.data ? data.data : [];
      let _state = { loading: false };
      if (dataArr.length > 0) {
        _state['responseData'] = dataArr;
      }
      setState(prev => ({
        ...prev,
        ..._state,
        pageOptions: {
          pageIndex: pageOptions.pageIndex,
          pageLimit: pageOptions.pageLimit,
          totalRows: totalRows,
        },
        loading: false,
      }));
    };




  const redirectToAdd =  (e, id = null) => {
    navigate(`/edit-tc-est-fullestimate/${id.estimate_id}`);
    // return <Redirect to={`/add-tcto/${id.id}`}/>
  }

  const onCancel = () => {
    // getTableData();
    setState({ ...state, isAdd: true, isVisible: false });
  }


/*

  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/tcto/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })
  }

*/


const callOptions = (evt) => {
  let _search = {
    searchOptions: evt["searchOptions"],
    searchValue: evt["searchValue"],
  };
  if (
    evt.hasOwnProperty("searchOptions") &&
    evt.hasOwnProperty("searchValue")
  ) {
    let pageOptions = state.pageOptions;

    pageOptions["pageIndex"] = 1;
    setState(
      (prevState) => ({
        ...prevState,
        search: _search,
        pageOptions: pageOptions,
      }),
      () => {
        getTableData(_search);
      }
    );
  } else if (
    evt &&
    evt.hasOwnProperty("actionName") &&
    evt["actionName"] === "reset-serach"
  ) {
    let pageOptions = state.pageOptions;
    pageOptions["pageIndex"] = 1;

    setState(
      (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
      () => {
        getTableData();
      }
    );
  } else if (
    evt &&
    evt.hasOwnProperty("actionName") &&
    evt["actionName"] === "column-filter"
  ) {
    // column filtering show/hide
    let responseData = state.responseData;
    let columns = Object.assign([], state.columns);

    if (responseData.length > 0) {
      for (var k in responseData[0]) {
        let index = columns.some(
          (item) =>
            (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
            (item.hasOwnProperty("key") && item.key === k)
        );
        if (!index) {
          let title = k
            .split("_")
            .map((snip) => {
              return snip[0].toUpperCase() + snip.substring(1);
            })
            .join(" ");
          let col = Object.assign(
            {},
            {
              title: title,
              dataIndex: k,
              key: k,
              invisible: "true",
              isReset: true,
            }
          );
          columns.splice(columns.length - 1, 0, col);
        }
      }
    }

    setState((prevState) => ({
      ...prevState,
      sidebarVisible: evt.hasOwnProperty("sidebarVisible")
        ? evt.sidebarVisible
        : !prevState.sidebarVisible,
      columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
    }));
  } else {
    let pageOptions = state.pageOptions;
    pageOptions[evt["actionName"]] = evt["actionVal"];

    if (evt["actionName"] === "pageLimit") {
      pageOptions["pageIndex"] = 1;
    }

    setState(
      (prevState) => ({ ...prevState, pageOptions: pageOptions }),
      () => {
        getTableData();
      }
    );
  }
};

  //resizing function
  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onSaveFormData = async (data) => {
    // let qParams = { "frm": "port_information" };
    // let qParamString = objectToQueryStringFunc(qParams);
    let { isAdd } = state;

    if (data && isAdd) {
      let _url = `${URL_WITH_VERSION}/port/save?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'post', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });



      setState({ ...state, isAdd: false, formDataValues: {} }, () => setState({ ...state, isVisible: false }));
    } else {
      setState({ ...state, isAdd: true, isVisible: true });
    }
  }

  const onEditFormData = async (data) => {
    let { isAdd } = state;

    if (data && !isAdd) {
      let _url = `${URL_WITH_VERSION}/tcto/update?frm=tcto_form`;
      await postAPICall(`${_url}`, data, 'put', (response) => {
        if (response && response.data) {
          openNotificationWithIcon('success', response.message);
          getTableData(1);
        } else {
          openNotificationWithIcon('error', response.message)
        }
      });
      setState({ ...state, isAdd: false, formDataValues: {} }, () => setState({ ...state, isVisible: false }));

    } else {
      setState({ ...state, isAdd: true, isVisible: true });

    }
  }

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };
  
 
  const tableColumns = state.columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: handleResize(index),
        }),
      }));

    
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title"><span>TC Estimate List</span></h4>
                </div>
              </div>
              <div className="section" style={{ width: '100%', marginBottom: '10px', paddingLeft: '15px', paddingRight: '15px' }}>
                {
                  state.loading === false ?
                    <ToolbarUI
                      routeUrl={'tcto-list-toolbar'}
                      optionValue={{ 'pageOptions': state.pageOptions, 'columns': state.columns, 'search': state.search }}
                      callback={(e) => callOptions(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => onActionDonwload('csv', 'tcto') },
                        { title: 'PDF', event: () => onActionDonwload('pdf', 'tcto') },
                        { title: 'XLS', event: () => onActionDonwload('xlsx', 'tcto') }
                      ]}
                    />
                    : undefined
                }


              </div>
              <div>
                <Table
                  rowKey={record => record.id}
                  className="inlineTable editableFixedHeader resizeableTable"
                  bordered
                  scroll={{ x: 'max-content' }}
                  // scroll={{ x: 1200, y: 370 }}
                  columns={tableColumns}
                  // size="small"
                  components={components}
                  dataSource={state.responseData}
                  loading={state.loading}
                  pagination={false}
                  rowClassName={(r, i) => ((i % 2) === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing')}
                />
              </div>
            </div>
          </div>
        </article>
        {/* column filtering show/hide */}
        {
          state.sidebarVisible ? <SidebarColumnFilter columns={state.columns} sidebarVisible={state.sidebarVisible} callback={(e) => callOptions(e)} /> : null
        }
      </div>
    )
  }


export default TCTOList;
