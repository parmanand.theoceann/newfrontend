import React, { Component } from "react";
import { Icon, Modal } from "antd";
import Tde from "../tde/Tde";
import CreateInvoice from "../create-invoice/CreateInvoice";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import URL_WITH_VERSION, {
  postAPICall,
  getAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
} from "../../shared";
import _ from "lodash";
import InvoicePopup from "../create-invoice/InvoicePopup";
import { DeleteOutlined, SaveOutlined,EditOutlined } from "@ant-design/icons";
import Remarks from "../../shared/components/Remarks";
import Attachment from "../../shared/components/Attachment";
import {
  uploadAttachment,
  deleteAttachment,
  getAttachments,
} from "../../shared/attachments";

class ClaimInvoice extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      modals:{
        InvoiceModal: false,
        TdeModal: false,
        InvoicePopup: false,
      },
      frmName: "claim_invoice_form",
      //formData: this.props.formDataValues || {},
      formData: this.props.formDataValues
        ? { ...this.props.formDataValues }
        : {},
      loadForm: false,
      oldFormData: this.props.frmData || {},
      isUpdate: false,
      TdeList: null,
      msg: "",
      modelVisible: false,
      isRemarkModel: false,
      invoiceReport: null,
      isSaved: false,
      popupdata: null,
      laytimeID: this.props.laytimeID || null,
      claimInvoiceNo: "",
    };
    this.claimdataref = React.createRef();
  }

  componentDidMount = () => {
    let tempForm = this.props.frmData;
    let voyage_num = tempForm && tempForm.voyage_number;
    let voyage_no = tempForm && tempForm.voyage_number;
    let voyage_number = tempForm && tempForm.voyage_number;
    let invoice_no =
      this.props.formDataValues.invoice_number || this.props.frmData.invoice_no;

    tempForm.voyage_no = voyage_no;
    tempForm.voyage_number = voyage_number;
    tempForm.voyage_num = voyage_num;
    tempForm.invoice_no = invoice_no;

    this.setState({
      formData: tempForm,
      claimInvoiceNo: invoice_no,
    });

    const { formData, laytimeID } = this.state;
    let laytime_id = laytimeID ? laytimeID : formData.laytime_id;

    this.editFormData(laytime_id);
  };

  deleteTdeData = () => {
    const { formData } = this.state;
    let laytime_id = formData.laytime_id;
    this.setState({ loadForm: false });
    this.editFormData(laytime_id, "delete");
  };

  editFormData = async (laytime_id, trans_no) => {
    const { oldFormData, formData } = this.state;

    let formValues = formData;
    let frmData = {};

    let _frmdata = {
      total_amount: formValues["..."] && formValues["..."]["demurrage_amount"],
      other_extra_time:
        formValues["..."] && formValues["..."]["other_extra_time"],
      amount_paid_to_owner:
        formValues["..."] && formValues["..."]["amount_paid_to_owner"],
      final_total_claim:
        formValues["..."] && formValues["..."]["final_total_claim"],
      origional_claim_amount:
        formValues["..."] && formValues["..."]["origional_claim_amount"],
      demurrage_dispatch:
        formValues["..."] && formValues["..."]["demurrage_dispatch"],
      final_amt_us:
        formValues["..."] && formValues["..."]["final_total_claim"]
          ? formValues["..."]["final_total_claim"]
          : 0,
      final_amt_loc:
        formValues["..."] && formValues["..."]["final_total_claim"]
          ? formValues["..."]["final_total_claim"] * formValues["exch_rate"]
          : 0,
      currency: formValues["currency"],
      exchange: formValues["exch_rate"],
      laytime_id: laytime_id,
    };

    if (laytime_id) {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/voyage-manager/claim-inv/edit?e=${laytime_id}`
      );
      const respData = await response["data"];
     
      if (respData) {
        // respData["invoice_no"] =
        //   this.props.formDataValues.invoice_number ||
        //   this.props.frmData.invoice_no;
        if (trans_no == "delete") {
          respData["trans_no"] = "";
        }
        if (trans_no && trans_no != "delete") {
          respData["trans_no"] = trans_no;
        }

        this.claimdataref.current = { ...respData };
        this.setState({
          formData: Object.assign({}, _frmdata, respData),
          isUpdate: true,
          loadForm: true,
        });
      } else {
        frmData = {
          vessel_id: formValues.vessel_id,
          //'invoice_no':this.props.formDataValues.invoice_number,
          voyage_no: oldFormData.voyage_number,
          counter_Party: formValues.charterer_id,
          company: oldFormData && oldFormData.my_company_lob,
          ap_ar: formValues["..."]
            ? formValues["..."]["demurrage_amount"] > 0
              ? 77
              : 78
            : 77,
          invoice_type: 93,
          //'due_date':'',
          //'trans_no' :'',
          //'bi_code':'','cargo_bi_date':'','p_i_club':'',
          claim_status: formValues.l_status,
          //'claim_date':'', 'settle_date':'',
          laytime_id: formValues["laytime_id"],
          //'last_update':''
          rebill: "No",
          //'settle_amount' :'',
          // 'actual_claim_amount':formValues &&   formValues['...'] ? formValues['...']['origional_claim_amount'] : 0.00,
          //'demurrage_dispatch' : formValues['...'] && formValues['...']['demurrage_dispatch'],
          //  'agreed_amount':formValues &&  formValues['...'] && formValues['...']['amount_paid_to_owner'] > 0 ? formValues['...']['amount_paid_to_owner'] :0.00,
          // 'total_amount':formValues &&  formValues['...'] && formValues['...']['demurrage_amount'] ? formValues['...']['demurrage_amount'] : 0.00
          //---------------------------------------------------------------------------
        };
        this.claimdataref.current = { ...frmData, ..._frmdata };
        this.setState({
          formData: Object.assign({}, frmData, _frmdata),
          loadForm: true,
          TdeList: null,
        });
      }
    }
  };

  saveFormData = (data) => {
    let layTime_id = data.laytime_id;
    const { frmName } = this.state;
    let suURL = `${URL_WITH_VERSION}/voyage-manager/claim-inv/save?frm=${frmName}`;
    let suMethod = "POST";
    this.claimdataref.current = { ...data };
    let newdata = _.omit(data, [
      "total_amount",
      "other_extra_time",
      "amount_paid_to_owner",
      "final_total_claim",
      "origional_claim_amount",
    ]);
    data = { ...newdata };
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        if (layTime_id) {
          this.setState({ ...this.state, loadForm: false });
          this.editFormData(layTime_id);
        }
      } else {
        openNotificationWithIcon("error", data.message);
      }
    });
  };

  updateFormData = (data) => {
    // data['settle_amount']=data['netamount&commision'].settle_amount
    data["netamount&commision"] = "";
    let layTime_id = data.laytime_id;
    const { frmName } = this.state;
    let suURL = `${URL_WITH_VERSION}/voyage-manager/claim-inv/update?frm=${frmName}`;
    let suMethod = "PUT";
    this.claimdataref.current = { ...data };
    let newdata = _.omit(data, [
      "total_amount",
      "other_extra_time",
      "amount_paid_to_owner",
      "final_total_claim",
      "origional_claim_amount",
    ]);
    data = { ...newdata };
    postAPICall(suURL, data, suMethod, (data) => {
      if (data && data.data) {
        openNotificationWithIcon("success", data.message);
        if (layTime_id) {
          this.setState({ loadForm: false });
          this.editFormData(layTime_id);
        }
      } else {
        openNotificationWithIcon("error", data.message);
      }
    });
  };



  _onDeleteFormData = (postData) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure, you want to delete it?",
      onOk: () => this.DeleteFormData(postData),
    });
  };

  DeleteFormData = async (data) => {
    let delete_data = {
      id: data.id,
    };
    postAPICall(
      `${URL_WITH_VERSION}/voyage-manager/claim-inv/delete`,
      delete_data,
      "delete",
      (response) => {
        if (response && response.data) {
          openNotificationWithIcon("success", response.message);
          this.props.closeModal(false);
        } else {
          openNotificationWithIcon("error", response.message);
        }
      }
    );
  };

  tdeModalOpen = async (invoice_no, modal) => {
    const { oldFormData, formData, modelVisible } = this.state;
    let tde_id = 0;
    if (invoice_no) {
      let _modal = {};
      _modal[modal] = true;
      const response = await getAPICall(`${URL_WITH_VERSION}/tde/list`);
      const tdeList = response["data"];
      let TdeList = tdeList&&tdeList.length>0?tdeList.filter((el) => invoice_no == el.invoice_no):[];
      if (TdeList && TdeList.length > 0) {
        tde_id = TdeList[0]["id"];
      }

      const responseData = await getAPICall(
        `${URL_WITH_VERSION}/address/edit?ae=${oldFormData.my_company_lob}`
      );
      const responseAddressData = responseData["data"];
      let account_no =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["account_no"]
          : "";

      let swift_code =
        responseAddressData &&
        responseAddressData["bank&accountdetails"] &&
        responseAddressData["bank&accountdetails"].length > 0
          ? responseAddressData["bank&accountdetails"][0] &&
            responseAddressData["bank&accountdetails"][0]["cb_swift_code"]
          : "";

      if (tde_id != 0) {
        const editData = await getAPICall(
          `${URL_WITH_VERSION}/tde/edit?e=${tde_id}`
        );
        const tdeEditData = await editData["data"];
        let rem_data = tdeEditData["----"][0];
        tdeEditData["----"] = {
          total_due: rem_data["total_due"],
          total: rem_data["total"],
          remittance_bank: rem_data["remittance_bank"],
        };
        tdeEditData["accounting"] = [
          {
            vessel_name: tdeEditData["vessel"],
            voyage: oldFormData["voyage_number"],
            ap_ar_acct: tdeEditData["ar_pr_account_no"],
            vessel_code: oldFormData["vessel_code"],
            company: tdeEditData["bill_via"],
            amount: tdeEditData["invoice_amount"],
            description: `claim-invoive @ ${tdeEditData["invoice_amount"]}`,
            lob: oldFormData.company_lob,
            id: -9e6,
            account: swift_code,
          },
        ];

        if (!formData["trans_no"]) {
          this.setState({ loadForm: false });
          this.editFormData(formData["laytime_id"], tdeEditData["trans_no"]);
        }
        if (formData["trans_no"] != tdeEditData["trans_no"]) {
          this.setState({ loadForm: false });
          this.editFormData(formData["laytime_id"], tdeEditData["trans_no"]);
        }
        this.setState({
          ...this.state,
          TdeList: Object.assign({}, tdeEditData),
          modals: _modal,
        });
      } else {
        let frmData = {};
        frmData = {
          bill_via: oldFormData.my_company_lob,
          invoice: formData["ap_ar"],
          invoice_no: formData["invoice_no"],
          invoice_date: formData["due_date"],
          invoice_type: formData["invoice_type"],
          received_date: formData["claim_date"],
          vessel: formData["vessel_id"],
          vendor: formData["counter_Party"],
          voyage_manager_id: oldFormData["id"],
          inv_status: formData["invoice_status"],
          invoice_amount: formData["final_amt_us"],
          account_base: formData["final_amt_us"],
          ar_pr_account_no: account_no,
          //  responseAddressData["bank&accountdetails"][0]["account_no"],
          voy_no: formData["voyage_no"],
          accounting: [],
        };
        frmData["accounting"] = [
          {
            vessel_name: formData["vessel_id"],
            voyage: oldFormData["voyage_number"],
            ap_ar_acct: account_no,
            // responseAddressData["bank&accountdetails"][0]["account_no"],
            vessel_code: oldFormData["vessel_code"],
            company: oldFormData.my_company_lob,
            amount: formData["final_amt_us"],
            description: `claim-invoive @ ${formData["final_amt_us"]}`,
            lob: oldFormData.company_lob,
            account: swift_code,
            // responseAddressData["bank&accountdetails"][0]["cb_swift_code"],
            port: oldFormData["b_port_name"],
            id: -9e6,
          },
        ];
        this.setState({ ...this.state, modals: _modal, TdeList: frmData });
      }
    } else {
      this.setState({
        msg: `Without "Invoice No" you cant't access the TDE Form`,
        modelVisible: !modelVisible,
      });
    }
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    // if(modal == 'TdeModal'){
    //   this.setState({TdeList : null })
    // }
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };
  

  invoiceModal = async (data = {}) => {
    let { formData, invoiceReport, claimInvoiceNo } = this.state;
    try {
      if (Object.keys(data).length == 0) {
        const response = await getAPICall(
          `${URL_WITH_VERSION}/voyage-manager/claim-inv/report?e=${
            formData.laytime_id
          }`
        );
        const respData = await response["data"];
        if (respData) {
          this.setState({
            ...this.state,
            invoiceReport: { ...respData, invoice_no: claimInvoiceNo },
            popupdata: respData,
          });
          this.showHideModal(true, "InvoicePopup");
        } else {
          openNotificationWithIcon("error", "Sorry, Unable to show invoice", 3);
        }
      } else {
        this.setState({
          ...this.state,
          invoiceReport: {
            ...invoiceReport,
            ...data,
            // invoice_no: claimInvoiceNo,
          },
        });
      }
    } catch (err) {
      openNotificationWithIcon("error", "Something went wrong", 3);
    }
  };

  handleok = () => {
    const { invoiceReport } = this.state;

    if (invoiceReport["isSaved"]) {
      //  this.setState({ ...this.state, invoiceReport: invoiceReport });
      this.showHideModal(false, "InvoicePopup");
      setTimeout(() => {
        this.showHideModal(true, "InvoiceModal");
      }, 2000);
    } else {
      openNotificationWithIcon(
        "info",
        "Please click on Save to generate invoice.",
        3
      );
    }
  };

  

  render() {
    const {
      frmName,
      formData,
      loadForm,
      isUpdate,
      modelVisible,
      isRemarkModel,
      msg,
      TdeList,
      invoiceReport,
      popupdata,
    } = this.state;

    const ShowAttachment = async (isShowAttachment) => {
      let loadComponent = undefined;
      const { id } = this.state.formData;
      if (id && isShowAttachment) {
        const attachments = await getAttachments(id, "EST");
        const callback = (fileArr) =>
          uploadAttachment(fileArr, id, "EST", "port-expense");
        loadComponent = (
          <Attachment
            uploadType="Estimates"
            attachments={attachments}
            onCloseUploadFileArray={callback}
            deleteAttachment={(file) =>
              deleteAttachment(file.url, file.name, "EST", "port-expense")
            }
            tableId={0}
          />
        );
        this.setState((prevState) => ({
          ...prevState,
          isShowAttachment: isShowAttachment,
          loadComponent: loadComponent,
        }));
      } else {
        this.setState((prevState) => ({
          ...prevState,
          isShowAttachment: isShowAttachment,
          loadComponent: undefined,
        }));
      }
    };

    const handleRemark = () => {
      this.setState({
        isRemarkModel: !isRemarkModel,
      });
   
    }
  

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="body-wrapper">
                <article className="article">
                  <div className="box box-default">
                    <div className="box-body common-fields-wrapper">
                      {loadForm && (
                        <NormalFormIndex
                          key={"key_" + frmName + "_0"}
                          formClass="label-min-height"
                          formData={formData}
                          //  formData={Object.assign({},this.claimdataref.current)}
                          frmCode={frmName}
                          showForm={true}
                          addForm={true}
                          showToolbar={[
                            {
                              isLeftBtn: [
                                {
                                  key: "s1",
                                  isSets: [
                                    {
                                      id: "1",
                                      key:
                                        isUpdate === true ? "update" : "save",
                                      type: <SaveOutlined />,
                                      withText: "Save",
                                      showToolTip: true,
                                      event: (key, data) =>
                                        isUpdate === true
                                          ? this.updateFormData(data)
                                          : this.saveFormData(data),
                                    },
                                    {
                                      id: "1",
                                      key: "delete",
                                      type: <DeleteOutlined />,
                                      withText: "Delete",
                                      showToolTip: true,
                                      event: (key, data) =>
                                        this._onDeleteFormData(data),
                                    },
                                      {
                              id: "3",
                              key: "edit",
                              type: <EditOutlined/>,
                              withText: "Remark",
                              showToolTip: true,
                              event: (key, data) => handleRemark(),
                            },
                                  ],
                                },
                              ],
                              isRightBtn: [
                                {
                                  key: "s2",
                                  isSets: [
                                    {
                                      key: "invoice",
                                      isDropdown: 0,
                                      withText: "Create Invoice",
                                      type: "",
                                      menus: null,
                                      event: (key) => {
                                        this.invoiceModal();
                                      },
                                    },
                                    {
                                      key: "tde",
                                      isDropdown: 0,
                                      withText: "TDE",
                                      type: "",
                                      menus: null,
                                      event: (key) =>
                                        this.tdeModalOpen(
                                          formData["invoice_no"],
                                          "TdeModal"
                                        ),
                                    },
                                    {
                                      key: "attachment",
                                      isDropdown: 0,
                                      withText: "Attachment",
                                      type: "",
                                      menus: null,
                                      event: (key, data) => {
                                        data &&
                                        data.hasOwnProperty("id") &&
                                        data["id"] > 0
                                          ? ShowAttachment(true)
                                          : openNotificationWithIcon(
                                              "info",
                                              "Please save the Invoice First.",
                                              3
                                            );
                                      },
                                    },                                  ],
                                },
                              ],
                              isResetOption: false,
                            },
                          ]}
                          inlineLayout={true}
                        />
                      )}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </article>
        {this.state.modals["InvoiceModal"] ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Download Invoice"
            open={this.state.modals["InvoiceModal"]}
            onCancel={() => this.showHideModal(false, "InvoiceModal")}
            width="95%"
            footer={null}
          >
            <CreateInvoice type="claimInvoice" claimInvoice={invoiceReport} />
          </Modal>
        ) : (
          undefined
        )}

        {this.state.modals["InvoicePopup"] ? (
          <Modal
            style={{ top: "2%" }}
            title="Invoice"
            open={this.state.modals["InvoicePopup"]}
            onCancel={() => this.showHideModal(false, "InvoicePopup")}
            width="95%"
            okText="Create PDF"
            onOk={this.handleok}
          >
            <InvoicePopup
              data={popupdata}
              updatepopup={(data) => this.invoiceModal(data)}
            />
          </Modal>
        ) : (
          undefined
        )}
        {this.state.isShowAttachment ? (
          <Modal
            style={{ top: "2%" }}
            title="Upload Attachment"
            open={this.state.isShowAttachment}
            onCancel={() => ShowAttachment(false)}
            width="50%"
            footer={null}
          >
            {this.state.loadComponent}
          </Modal>
        ) : undefined}

        {this.state.modals["TdeModal"] && (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="TDE"
            open={this.state.modals["TdeModal"]}
            onCancel={() => this.showHideModal(false, "TdeModal")}
            width="95%"
            footer={null}
          >
            <Tde
              invoiceType="claim-invoice"
              isEdit={
                TdeList != null && TdeList.id && TdeList.id > 0 ? true : false
              }
              formData={TdeList}
              deleteTde={() => this.deleteTdeData()}
              saveUpdateClose={() => this.showHideModal(false, "TdeModal")}
            />
          </Modal>
        )}
        <Modal
          title="Alert"
          open={modelVisible}
          onOk={() => this.setState({ modelVisible: !modelVisible })}
          onCancel={() => this.setState({ modelVisible: !modelVisible })}
        >
          <p>{msg}</p>
        </Modal>

        {isRemarkModel && (
        <Modal
          width={600}
          title="Remark"
          open={isRemarkModel}
          onOk={() => {
            this.setState({ isRemarkModel: true });
          }}
          onCancel={() => this.setState({  isRemarkModel: false })}
          footer={false}
        >
          <Remarks
            remarksID={formData.invoice_no}
            remarkType="voyage-manager"
          // idType="Bunker_no"
          />


        </Modal>
      )}
      </div>
    );
  }
}

export default ClaimInvoice;
