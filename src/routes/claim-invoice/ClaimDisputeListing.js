import React, { Component } from 'react';
import { Table } from 'antd';
import ClaimListing from './ClaimListing';

class ClaimDisputeListing extends Component {
  render() {
    return (
      <ClaimListing type='disputed' />
    );
  }
}

export default ClaimDisputeListing;
