import React, { Component, useState } from "react";
import { Button, Table, Popconfirm, Modal, Col, Select, Row } from "antd";
import { EditOutlined } from "@ant-design/icons";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import { FIELDS } from "../../shared/tableFields";
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  openNotificationWithIcon,
  ResizeableTitle,
  apiDeleteCall,
  useStateCallback,
} from "../../shared";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import ClaimInvoice from "./ClaimInvoice";
import { useEffect } from "react";
import ClusterColumnChart from "../dashboard/charts/ClusterColumnChart";
import LineChart from "../dashboard/charts/LineChart";
import PieChart from "../dashboard/charts/PieChart";
import StackHorizontalChart from "../dashboard/charts/StackHorizontalChart";
import FunnelChart from "../dashboard/charts/FunnelChart";

const ClaimListing = (props) => {
  const [state, setState] = useStateCallback({
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
    claim_status: props.type,
    sidebarVisible: false,
    isShowClaimInvoice: false,
    formData: {},
    typesearch: {},
    donloadArray: [],
  });

  const ClusterDataxAxis = [
    "GOLIATH",
    "ATHERINA",
    "Ocean Cobra",
    "MSC LORETO",
    "Destiny",
  ];
  const ClusterDataSeries = [
    {
      name: "Total Amount",
      type: "bar",
      barGap: 0,

      data: [400, 322, 300, 267, 201],
    },
  ];
  const LineCharSeriesData = [200, 400, 1200, 100, 900];
  const LineCharxAxisData = [
    "TCOV-FULL-00998",
    "FTCOV-FULL-00997",
    "TCOV-FULL-00996",
    "TCOV-FULL-00995",
    "TCOV-FULL-00993",
  ];
  const PieChartData = [
    { value: 40, name: "CLM-INV-00052-32k" },
    { value: 30, name: "CLM-INV-00051-25k" },
    { value: 13, name: "CLM-INV-00050-9k" },
    { value: 10, name: "CLM-INV-00049-5k" },
  ];

  const StackHorizontalChartSeries = [
    {
      name: "Total Sea Cons.",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [320, 302, 301, 599, 555],
    },
    {
      name: "Total Port Cons.",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [120, 132, 101, 777, 555],
    },
    {
      name: "Total Bunkar Cons.",
      type: "bar",
      stack: "total",
      label: {
        show: true,
      },
      emphasis: {
        focus: "series",
      },
      data: [220, 182, 191, 888, 555],
    },
  ];

  const StackHorizontalChartyAxis = [
    "GOLI-00009",
    "SOUT-00016",
    "EMMA-00006	",
    "CAPF-00001",
    "SOLD-00005",
  ];

  const [option, setOption] = useState({
    bar1: {
      option: {
        tooltip: {
          trigger: "axis",
          axisPointer: {
            // Use axis to trigger tooltip
            type: "shadow", // 'shadow' as default; can also be 'line' or 'shadow'
          },
        },
        // legend: {},
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 5,
          containLabel: true,
        },
        xAxis: {
          type: "value",
          data: StackHorizontalChartyAxis ?? null,
          axisLabel: {
            formatter: "{value}k", // Add 'k' after each number on x-axis
          },
        },
        yAxis: {
          type: "category",
          // data: ["Western Europe", "Central America", "Oceania", "Southeastern Asia",],
          data: StackHorizontalChartyAxis ?? null,
        },
        series: StackHorizontalChartSeries,
        // series: [
        //   {
        //     name: "Total Sea Cons.",
        //     type: "bar",
        //     // stack: "total",
        //     label: {
        //       show: true,
        //     },
        //     emphasis: {
        //       focus: "series",
        //     },
        //     data: [320, 302, 301,599],
        //   },
        //   {
        //     name: "Total Port Cons.",
        //     type: "bar",
        //     // stack: "total",
        //     label: {
        //       show: true,
        //     },
        //     emphasis: {
        //       focus: "series",
        //     },
        //     data: [120, 132, 101,777],
        //   },
        //   {
        //     name: "Total Bunkar Cons.",
        //     type: "bar",
        //     // stack: "total",
        //     label: {
        //       show: true,
        //     },
        //     emphasis: {
        //       focus: "series",
        //     },
        //     data: [220, 182, 191,888],
        //   },

        // ],
      },
    },
  });

  const [pieChartOptions, setPieChartOptions] = useState({
    bar1: {
      option: {
        tooltip: {
          trigger: "item",
          formatter: "{a} <br/>{b}: {c}%",
        },
        grid: {
          left: 30,
          right: 30,
          bottom: 3,
          top: 5,
          containLabel: true,
        },
        // legend: {
        //   orient: "vertical",
        //   left: "left",
        // },
        series: [
          {
            type: "pie",
            radius: "65%",
            // selectedMode:selectedMode,
            name: "Total Voyage days",
            data: PieChartData,
            // [
            //   { value: 40, name: 'Vessel 1' },
            //   { value: 30, name: 'Vessel 2' },
            //   { value: 13, name: 'Vessel 3' },
            //   { value: 10, name: 'Vessel 4' },
            //   { value: 7, name: 'Vessel 5' },
            // ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: "rgba(0, 0, 0, 0.5)",
              },
            },
            // label: { // Add label configuration here
            //   show: true,
            //   formatter: '{c}', // {b} refers to the name of each slice
            //   position: 'inside', // Change this according to your preference
            // },
          },
        ],
      },
      legend: {
        orient: "horizontal",
        left: "left",
      },
      // labelLine: {
      //   show: true,
      //   length: 20, // Length of the label line
      //   length2: 20, // Length of the second part of the label line
      // },
    },
  });

  const [isGraphModal, setIsGraphModalOpen] = useState(false);
  const showGraphs = () => {
    setIsGraphModalOpen(true);
  };

  const handleCancel = () => {
    setIsGraphModalOpen(false);
  };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  useEffect(() => {
    const tableAction = {
      title: "Action",
      key: "action",
      fixed: "right",
      // width: 100,
      width: 70,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span
              className="iconWrapper"
              onClick={(e) => redirectToAdd(record, record.laytime_id)}
            >
              <EditOutlined />
            </span>
            {/* <span className="iconWrapper cancel">
            commented As per discussion with Pallav sir
              <Popconfirm
                title="Are you sure, you want to delete it?"
                onConfirm={() => onRowDeletedClick(record,record.id)}
              >
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        );
      },
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS["claim-list"] ? FIELDS["claim-list"]["tableheads"] : []
    );
    tableHeaders.push(tableAction);
    setState({ ...state, columns: tableHeaders }, () => {
      getTableData();
    });
  }, []);

  const redirectToAdd = async (record, id) => {
    setState((prevState) => ({
      ...prevState,
      isShowClaimInvoice: true,
      laytimeID: id,
      formData: record,
    }));
  };

  const onRowDeletedClick = (record, id) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/claim-inv/delete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon("success", response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon("error", response.message);
      }
    });
  };

  const getTableData = async (search = {}) => {
    const { pageOptions } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    let headers = { order_by: { id: "desc" } };

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();

      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc = {
          OR: { [search["searchOptions"]]: { l: search["searchValue"] } },
        };
      }

      if (headers.hasOwnProperty("where")) {
        // If "where" property already exists, merge the conditions
        headers["where"] = { ...headers["where"], ...wc };
      } else {
        // If "where" property doesn't exist, set it to the new condition
        headers["where"] = wc;
      }

      state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }

    setState((prev) => ({
      ...prev,
      loading: true,
      responseData: [],
    }));

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/claim-inv/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;

    let dataArr = data && data.data ? data.data : [];
    let _state = { loading: false };
    if (dataArr.length > 0) {
      _state["responseData"] = dataArr;
    }
    setState((prev) => ({
      ...prev,
      ..._state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };

  const closeModal = () => {
    setState({ ...state, isShowClaimInvoice: false }, () => getTableData());
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };

  const handleResize =
    (index) =>
    (e, { size }) => {
      setState(({ columns }) => {
        const nextColumns = [...columns];
        nextColumns[index] = {
          ...nextColumns[index],
          width: size.width,
        };
        return { columns: nextColumns };
      });
    };

  const {
    columns,
    loading,
    responseData,
    pageOptions,
    sidebarVisible,
    search,
    formData,
    isShowClaimInvoice,
    laytimeID,
  } = state;

  const tableColumns = columns
    .filter((col) => (col && col.invisible !== "true" ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <div className="body-wrapper modalWrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div
              className="section"
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "15px",
                paddingRight: "15px",
              }}
            >
              {loading === false && (
                <ToolbarUI
                  routeUrl={"claim_list_toolbar"}
                  optionValue={{
                    pageOptions: pageOptions,
                    columns: columns,
                    search: search,
                  }}
                  callback={(e) => callOptions(e)}
                  showGraph={ props.voyID ||showGraphs}
                  dowloadOptions={[
                    {
                      title: "CSV",
                      event: () => onActionDonwload("csv", "claim-invoice"),
                    },
                    {
                      title: "PDF",
                      event: () => onActionDonwload("pdf", "claim-invoice"),
                    },
                    {
                      title: "XLS",
                      event: () => onActionDonwload("xlsx", "claim-invoice"),
                    },
                  ]}
                />
              )}
            </div>
            <Table
              bordered
              columns={tableColumns}
              size="small"
              scroll={{ x: "max-content" }}
              dataSource={responseData}
              pagination={false}
              footer={false}
              loading={loading}
              rowClassName={(r, i) =>
                i % 2 === 0
                  ? "table-striped-listing"
                  : "dull-color table-striped-listing"
              }
            />
          </div>
        </div>
      </article>
      {isShowClaimInvoice ? (
        // after api this page will open.
        <Modal
          className="page-container"
          style={{ top: "2%" }}
          title="Create Invoice"
          open={isShowClaimInvoice}
          // onOk={handleOk}
          onCancel={closeModal}
          width="90%"
          footer={null}
        >
          <ClaimInvoice
            formDataValues={formData}
            laytimeID={laytimeID}
            frmData={formData}
            closeModal={closeModal}
          />
        </Modal>
      ) : undefined}
      {sidebarVisible ? (
        <SidebarColumnFilter
          columns={tableColumns}
          sidebarVisible={sidebarVisible}
          callback={(e) => callOptions(e)}
        />
      ) : null}
      {
        <Modal
          title="Claim Dashboard"
          open={isGraphModal}
          //  onOk={handleOk}
          width="90%"
          onCancel={handleCancel}
          footer={null}
        >
          <Row gutter={[16, 0]} style={{ textAlign: "center" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Vessels</p>
                <p>300</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Amount</p>
                <p>300 $</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ borderRadius: "15px", padding: "8px" }}
            >
              <div
                style={{
                  background: "#1D406A",
                  color: "white",
                  borderRadius: "15px",
                }}
              >
                <p>Total Invoice</p>
                <p>240</p>
              </div>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={3}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Vessel Name</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "PACIFIC EXPLORER",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "OCEANIC MAJESTY",
                    label: "OCEANIC MAJESTY",
                  },
                  {
                    value: "CS HANA",
                    label: "CS HANA",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Invoice No</p>
              <Select
                placeholder="All"
                optionFilterProp="children"
                options={[
                  {
                    value: "TCE02-24-01592",
                    label: "TCE02-24-01592",
                  },
                  {
                    value: "TCE01-24-01582",
                    label: "TCE01-24-01582",
                  },
                  {
                    value: "TCE01-24-01573",
                    label: "TCE01-24-01573",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Account Type</p>
              <Select
                placeholder="Payable"
                optionFilterProp="children"
                options={[
                  {
                    value: "Payable",
                    label: "Payable",
                  },
                  {
                    value: "Receivable",
                    label: "Receivable",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Status</p>
              <Select
                placeholder="PREPARED"
                optionFilterProp="children"
                options={[
                  {
                    value: "PREPARED",
                    label: "PREPARED",
                  },
                  {
                    value: "APPROVED",
                    label: "APPROVED",
                  },
                  {
                    value: "POSTED",
                    label: "POSTED",
                  },
                  {
                    value: "VERIFIED",
                    label: "VERIFIED",
                  },
                ]}
              ></Select>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={6}
              lg={3}
              xl={3}
              style={{ textAlign: "start", padding: "8px" }}
            >
              <p style={{ margin: "0" }}>Date To</p>
              <Select
                placeholder="2024-02-18"
                optionFilterProp="children"
                options={[
                  {
                    value: "2024-01-26",
                    label: "2024-01-26",
                  },
                  {
                    value: "2023-11-16",
                    label: "2023-11-16",
                  },
                  {
                    value: "2023-11-17",
                    label: "2023-11-17",
                  },
                ]}
              ></Select>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <ClusterColumnChart
                Heading={"Total Amount Per Vessel"}
                ClusterDataxAxis={ClusterDataxAxis}
                ClusterDataSeries={ClusterDataSeries}
                maxValueyAxis={"500"}
              />
            </Col>
            <Col span={12}>
              <FunnelChart Heading={"Total Amount Per Voyage Number"} />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <PieChart
                PieChartData={PieChartData}
                Heading={" Total Inv Amount Per Invoice no "}
                pieChartOptions={pieChartOptions}
              />
            </Col>
          </Row>
        </Modal>
      }
    </div>
  );
};

export default ClaimListing;
