import React from "react";
import {CloseOutlined} from '@ant-design/icons';
import URL_WITH_VERSION, { IMAGE_PATH } from "../../shared";
const LiveVesselSearch = ({
  onCloseFilter,
  onChangeLiveSearchInput,
  onLiveSearchDataClick,
  listData = [],
  isLiveSearchLoading,
  liveSearchResult
}) => {
  

  return (
    <div className="filter-search-list">
      <div className="close-icon-wrapper" onClick={onCloseFilter}>
      <CloseOutlined/>
      </div>
      <div className="close-icon">
        <input type="text" onChange={onChangeLiveSearchInput} placeholder="Enter minimum 3 letter vessel name or IMO number" />
      </div>
      <div className="list-data">
        {
          
          Array.isArray(listData) &&listData.length >= 0 ? listData.map((v, index) =>
          
            <div key={index} className="list-item" onClick={() => onLiveSearchDataClick(v)}>
              <img loading="lazy" src={IMAGE_PATH+"icons/ship.png"} alt="" />
              <div className="middle-wrapper">
                <span className="vessel-name">{v.SHIPNAME}</span>
                <div className="middle-wrapper-discription">
                  <span>
                    MMSI: {v.MMSI}
                  </span>
                  <span>
                    IMO no. : {v.IMO ? v.IMO : "NA"}
                  </span>
                </div>
              </div>
              <div className="last-wrapper">
                <p>live</p>
              </div>
            </div>
          ) : <div className="data-message">{liveSearchResult}</div>
        }
      </div>
    </div>
  )
}

export default LiveVesselSearch;
