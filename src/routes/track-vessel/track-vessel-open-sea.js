import React from "react";
import { Layout } from "antd";

const TrackVesselOpenSeaMap = () => (
  <div className="wrap-rightbar full-wraps" style={{ marginTop: "24px" }}>
    <Layout className="layout-wrapper" style={{ overflow: "hidden" }}>
      <Layout>
        <Layout.Content className="content-wrapper">
          <section className="map-wrapper-container">
            <iframe
              style={{ width: "100%", height: "600px" }}
              src="https://map.openseamap.org/"
            />
          </section>
        </Layout.Content>
      </Layout>
    </Layout>
  </div>
);

export default TrackVesselOpenSeaMap;
