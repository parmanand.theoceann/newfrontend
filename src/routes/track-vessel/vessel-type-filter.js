import React from 'react';
import {CloseOutlined} from '@ant-design/icons';

const VesselTypeFilter = ({
  onCloseFilter
}) => {

  return (

    <div className="filter-search-list checkbox-list-wrapper">
      <div className="close-icon-wrapper" onClick={onCloseFilter}>
      <CloseOutlined />
      </div>
      <div className="filter-boxes">
        <input type="checkbox" id="box-1" defaultChecked />
        <label htmlFor="box-1">All</label>

        <input type="checkbox" id="box-2" />
        <label htmlFor="box-2">Cargo ship </label>

        <input type="checkbox" id="box-3" />
        <label htmlFor="box-3">Tanker</label>

        <input type="checkbox" id="box-3" />
        <label htmlFor="box-3">Passenger ship</label>

        <input type="checkbox" id="box-3" />
        <label htmlFor="box-3">Fishing vessel</label>

        <input type="checkbox" id="box-3" />
        <label htmlFor="box-3">High speed craft</label>
      </div>
    </div>
  )
}

export default VesselTypeFilter;
