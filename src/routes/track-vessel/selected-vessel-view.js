import React from "react";
import { Spin, Tooltip, Collapse } from "antd";
import {CloseOutlined, DownOutlined, StarOutlined} from '@ant-design/icons';
import moment from "moment";
import URL_WITH_VERSION, { IMAGE_PATH } from "../../shared";
const { Panel } = Collapse;
const SelectedVesselView = ({
  isLiveDetailLoading,
  vesselDetails = {},
  onClose,
  addmyfleet,
}) => {
    return (
    <div>
      {vesselDetails && (
        <div
          className="filter-search-list selected-vessel"
          style={{ width: "520px" }}
        >
          {isLiveDetailLoading && (
            <div className="live-data-loader">
              <Spin />
            </div>
          )}
          {typeof onClose == "function" ? (
            <div className="close-icon-wrapper" onClick={onClose}>
            <CloseOutlined />
            </div>
          ) : (
            undefined
          )}

          <div className="list-data">
            {typeof addmyfleet == "function" ? (
              <div className="close-icon-wrapper1">
                <Tooltip title="Add this vessel into my fleet">
                <StarOutlined
                    onClick={() => addmyfleet(vesselDetails.IMO)}
                  />
                </Tooltip>
              </div>
            ) : (
              undefined
            )}

            <div className="heading-name-container">
              <img
               src={IMAGE_PATH+"icons/vessel-yacht.svg"}
                className="heading-image"
                alt="ship image"
              />
              <div className="heading-icon-list">
                <div className="service-logo-container">
                  <img src={IMAGE_PATH+"icons/speedometer.png"} alt="" />
                  <span className="icon-data">
                    {vesselDetails.position
                      ? vesselDetails.position.SPEED
                      : "NA"}
                  </span>
                </div>
                <div className="service-logo-container">
                  <img src={IMAGE_PATH+"icons/waves.png"} alt="" />
                  <span className="icon-data">
                    {vesselDetails.position
                      ? vesselDetails.voyage.DRAUGHT
                      : "NA"}
                  </span>
                </div>
                <div className="service-logo-container">
                  <img src={IMAGE_PATH+"icons/explore.png"} alt="" />
                  <span className="icon-data">
                    {vesselDetails.position
                      ? vesselDetails.position.HEADING
                      : "NA"}
                  </span>
                </div>
              </div>
            </div>
            <div className="list-items">
              <div className="selected-data-wrapper">
                <div className="first-div">
                  <div className="heading-logo-container">
                    <img src={IMAGE_PATH+"icons/anchor_green.svg"}  alt="location" />
                    <div className="selected-data-name selected-vessel-name">
                      {vesselDetails.SHIPNAME}
                    </div>
                  </div>
                  <div className="heading-logo-container">
                    <img src={IMAGE_PATH+"icons/anchor_red.svg"} alt="location" />
                    <div className="selected-data-name selected-vessel-id selected-next-port-name">
                      {vesselDetails.NEXT_PORT_NAME
                        ? vesselDetails.NEXT_PORT_NAME
                        : "NA"}
                    </div>
                  </div>
                </div>
                <div className="second-div">
                  <div className="second-div">
                    <span className="selected-data-name">IMO:</span>
                    &nbsp;
                    <span className="selected-data-name1">
                      {vesselDetails.IMO
                        ? vesselDetails.IMO
                        : "NA"}
                    </span>
                  </div>
                  <div className="second-div">
                    <span className="selected-data-name">MMSI:</span>
                    &nbsp;
                    <span className="selected-data-name1">
                      {vesselDetails.IMO
                        ? vesselDetails.MMSI
                        : "NA"}
                    </span>
                  </div>
                </div>
                <ul className="ul-lists-services">
                  {/* <li className="list-items-services">
                    <input type="checkbox" defaultChecked />
                    <i />
                    <div className="service-logo-container">
                      <img src="assets/location-icon.png" alt="location" />
                      <h2 className="service-name">Current Location</h2>
                    </div>
                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">location:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.position
                            ? vesselDetails.position.location_str
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">Destination:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {" "}
                          {vesselDetails.voyage
                            ? vesselDetails.voyage.destination
                            : "NA"}
                        </span>
                      </div>
                    </p>
                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">latitude:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.position
                            ? vesselDetails.position.latitude
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">Longitude:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {" "}
                          {vesselDetails.position
                            ? vesselDetails.position.longitude
                            : "NA"}
                        </span>
                      </div>
                    </p>
                  </li>

                  <li>
                    <input type="checkbox" />
                    <i />
                    <div className="service-logo-container">
                      <img src="assets/last-port-ship.png" alt="" />
                      <h2 className="service-name">Last Portcall</h2>
                    </div>

                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">Name:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {" "}
                          {vesselDetails.last_port
                            ? vesselDetails.last_port.name
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">
                          Location-code:
                        </span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.last_port
                            ? vesselDetails.last_port.locode
                            : "NA"}
                        </span>
                      </div>
                    </p>
                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">ATA:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.last_port &&
                          vesselDetails.last_port.ata !== "NA"
                            ? moment(vesselDetails.last_port.ata).format(
                                "YYYY-MM-DD HH:mm"
                              )
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">ATD:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.last_port &&
                          vesselDetails.last_port.atd != "NA"
                            ? moment(vesselDetails.last_port.atd).format(
                                "YYYY-MM-DD HH:mm"
                              )
                            : "NA"}
                        </span>
                      </div>
                    </p>
                  </li>

                  <li>
                    <input type="checkbox" defaultChecked />
                    <i />
                    <div className="service-logo-container">
                      <img src="assets/port.png" alt="" />
                      <h2 className="service-name">Next Portcall</h2>
                    </div>

                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">Name:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.next_port
                            ? vesselDetails.next_port.name
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">
                          location-code:
                        </span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {vesselDetails.next_port
                            ? vesselDetails.next_port.name
                            : "NA"}
                        </span>
                      </div>
                    </p>
                    <p className="second-div">
                      <div className="second-div">
                        <span className="selected-data-name">
                          Travel Distance:
                        </span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {" "}
                          {vesselDetails.next_port
                            ? vesselDetails.next_port.travel_distance_nm
                            : "NA"}
                        </span>
                      </div>

                      <div className="second-div">
                        <span className="selected-data-name">Travel Time:</span>
                        &nbsp;
                        <span className="selected-data-name1">
                          {" "}
                          {vesselDetails.next_port
                            ? vesselDetails.next_port.travel_time_h
                            : "NA"}
                        </span>
                      </div>
                    </p>
                  </li>

                  <li>
                    <input type="checkbox" defaultChecked />
                    <div className="service-logo-container">
                      <img src="assets/anchor.png" alt="" />
                      <h2 className="service-name">Vessel Particulers</h2>
                    </div>

                    <div className="">
                      <span className="selected-data-name">
                        Position Received:
                      </span>
                      &nbsp;
                      <span className="selected-data-name1">
                        {vesselDetails.position &&
                        vesselDetails.position.received != "NA"
                          ? moment(vesselDetails.position.received).format(
                              "YYYY-MM-DD HH:mm"
                            )
                          : "NA"}
                      </span>
                    </div>

                    <div className="">
                      <span className="selected-data-name">Nav Status:</span>
                      &nbsp;
                      <span className="selected-data-name1">
                        {vesselDetails.position
                          ? vesselDetails.position.nav_status
                          : "NA"}
                      </span>
                    </div>
                  </li>  */}

                  <li className="list-items-services roushan">
                    <Collapse
                      defaultActiveKey={["1", "2", "3", "4"]}
                      expandIcon={({ isActive }) => (
                        <DownOutlined rotate={isActive ? 180 : 0} />
                      )}
                      expandIconPosition="right"
                    >
                      <Panel
                        header={
                          <div className="track" style={{color : 'black', display : 'flex', alignItems : 'center'}}>
                            <img
                              src={IMAGE_PATH+"icons/location-icon.png"}
                              alt="location"
                            />
                            <h2 className="service-name" style={{margin : '0 0 0 4px'}}>Current Location</h2>
                          </div>
                        }
                        key="1"
                      >
                        <>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">
                                location:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.position
                                  ? vesselDetails.position.location_str
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">
                                Destination:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {" "}
                                {vesselDetails.voyage
                                  ? vesselDetails.voyage.DESTINATION
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">
                                latitude:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.position
                                  ? vesselDetails.position.LAT
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">
                                Longitude:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {" "}
                                {vesselDetails.position
                                  ? vesselDetails.position.LON
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                        </>
                      </Panel>

                      <Panel
                        header={
                          <div className="track" style={{color : 'black', display : 'flex', alignItems : 'center'}}>
                            <img src={IMAGE_PATH+"icons/last-port-ship.png"} alt="" />
                            <h2 className="service-name" style={{margin : '0 0 0 4px'}}>Last Portcall</h2>
                          </div>
                        }
                        key="2"
                      >
                        <>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">Name:</span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {" "}
                                {vesselDetails.last_port
                                  ? vesselDetails.last_port.LAST_PORT_NAME
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">
                                Location-code:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.last_port
                                  ? vesselDetails.last_port.locode
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">ATA:</span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.last_port &&
                                vesselDetails.last_port.ata !== "NA"
                                  ? moment(vesselDetails.last_port.ata).format(
                                      "YYYY-MM-DD HH:mm"
                                    )
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">ATD:</span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.last_port &&
                                vesselDetails.last_port.atd != "NA"
                                  ? moment(vesselDetails.last_port.atd).format(
                                      "YYYY-MM-DD HH:mm"
                                    )
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                        </>
                      </Panel>

                      <Panel
                        header={
                          <div className="track" style={{color : 'black', display : 'flex', alignItems : 'center'}}>
                            <img src={IMAGE_PATH+"icons/port.png"} alt="" />
                            <h2 className="service-name" style={{margin : '0 0 0 4px'}}>Next Portcall</h2>
                          </div>
                        }
                        key="3"
                      >
                        <>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">Name:</span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.next_port
                                  ? vesselDetails.next_port.NEXT_PORT_NAME
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">
                                location-code:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {vesselDetails.next_port
                                  ? vesselDetails.next_port.NEXT_PORT_NAME
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                          <p className="second-div">
                            <div className="second-div">
                              <span className="selected-data-name">
                                Travel Distance:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {" "}
                                {vesselDetails.next_port
                                  ? vesselDetails.next_port.DISTANCE_TRAVELLED
                                  : "NA"}
                              </span>
                            </div>

                            <div className="second-div">
                              <span className="selected-data-name">
                                Travel Time:
                              </span>
                              &nbsp;
                              <span className="selected-data-name1">
                                {" "}
                                {vesselDetails.next_port
                                  ? vesselDetails.next_port.travel_time_h
                                  : "NA"}
                              </span>
                            </div>
                          </p>
                        </>
                      </Panel>

                      <Panel
                        header={
                          <div className="track" style={{color : 'black', display : 'flex', alignItems : 'center'}}>
                            <img src={IMAGE_PATH+"icons/anchor.png"} alt="" />
                            <h2 className="service-name" style={{margin : '0 0 0 4px'}}>Vessel Particulers</h2>
                          </div>
                        }
                        key="4"
                      >
                        <>
                          <div className="">
                            <span className="selected-data-name">
                              Position Received:
                            </span>
                            &nbsp;
                            <span className="selected-data-name1">
                              {vesselDetails.position &&
                              vesselDetails.position.received != "NA"
                                ? moment(
                                    vesselDetails.position.received
                                  ).format("YYYY-MM-DD HH:mm")
                                : "NA"}
                            </span>
                          </div>

                          <div className="">
                            <span className="selected-data-name">
                              Nav Status:
                            </span>
                            &nbsp;
                            <span className="selected-data-name1">
                              {vesselDetails.position
                                ? vesselDetails.position.nav_status
                                : "NA"}
                            </span>
                          </div>
                        </>
                      </Panel>
                    
                    </Collapse>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SelectedVesselView;
