import { Layout, Table, Typography, Input, Row, Col, Divider } from "antd";
import React, { Component } from "react";
import URL_WITH_VERSION, {
  objectToQueryStringFunc,
  getAPICall,
} from "../../shared";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
const { Title } = Typography;
const { Content } = Layout;

class VesselDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount = async () => {
    const { formDetailsValues } = this.props;
    this.setState({
      data: formDetailsValues,
    });
  };

  render() {
    const { data } = this.state;
    return (
      <>
        <div className="tcov-wrapper full-wraps">
          <Layout className="layout-wrapper">
            <Layout>
              <Content className="content-wrapper">
                <div className="fieldscroll-wrap">
                  <div className="body-wrapper">
                    <article className="article">
                      <div className="box box-default">
                        <div className="box-body common-fields-wrapper">
                          <Divider>
                            <Title level={4}>General Information</Title>
                          </Divider>

                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Name
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipname &&
                                        data.shipname != "null"
                                        ? data.shipname
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  LRIMOShipNo
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.lrimoshipno &&
                                        data.lrimoshipno != "null"
                                        ? data.lrimoshipno
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship type Level5
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shiptypelevel5 &&
                                        data.shiptypelevel5 != "null"
                                        ? data.shiptypelevel5
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Date Of Build
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.dateofbuild &&
                                        data.dateofbuild != "null"
                                        ? data.dateofbuild
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Consumption Speed1
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.consumptionspeed1 &&
                                        data.consumptionspeed1 != "null"
                                        ? data.consumptionspeed1
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Consumption Value1
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.consumptionvalue1 &&
                                        data.consumptionvalue1 != "null"
                                        ? data.consumptionvalue1
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  ExName
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.exname &&
                                        data.exname != "null"
                                        ? data.exname
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Flag Name
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.flagname &&
                                        data.flagname != "null"
                                        ? data.flagname
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Port of Registry
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.portofregistry &&
                                        data.portofregistry != "null"
                                        ? data.portofregistry
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Country Of Build
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.countryofbuild &&
                                        data.countryofbuild != "null"
                                        ? data.countryofbuild
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Consumption Speed2
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.consumptionspeed2 &&
                                        data.consumptionspeed2 != "null"
                                        ? data.consumptionspeed2
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Consumption Value2
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.consumptionvalue2 &&
                                        data.consumptionvalue2 != "null"
                                        ? data.consumptionvalue2
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Status
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipstatus &&
                                        data.shipstatus != "null"
                                        ? data.shipstatus
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Gross Tonnage
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.grosstonnage &&
                                        data.grosstonnage != "null"
                                        ? data.grosstonnage
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Deadweight
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.deadweight &&
                                        data.deadweight != "null"
                                        ? data.deadweight
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Call Sign
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.callsign &&
                                        data.callsign != "null"
                                        ? data.callsign
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  PandI Club
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.pandiclub &&
                                        data.pandiclub != "null"
                                        ? data.pandiclub
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>

                          <Divider>
                            <Title level={4}>General details</Title>
                          </Divider>

                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Draught
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.draught &&
                                        data.draught != "null"
                                        ? data.draught
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Formula DWT
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.formuladwt &&
                                        data.formuladwt != "null"
                                        ? data.formuladwt
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Fuel Consumption Main Engines only
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.fuelconsumptionmainenginesonly &&
                                        data.fuelconsumptionmainenginesonly !=
                                        "null"
                                        ? data.fuelconsumptionmainenginesonly
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Fuel Consumption Total
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.fuelconsumptiontotal &&
                                        data.fuelconsumptiontotal != "null"
                                        ? data.fuelconsumptiontotal
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Gear No Largest
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.gearnolargest &&
                                        data.gearnolargest != "null"
                                        ? data.gearnolargest
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Gear SWL Largest
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.gearswllargest &&
                                        data.gearswllargest != "null"
                                        ? data.gearswllargest
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Gear Type Largest
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.geartypelargest &&
                                        data.geartypelargest != "null"
                                        ? data.geartypelargest
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Gearless
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.gearless &&
                                        data.gearless != "null"
                                        ? data.gearless
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Grain Capacity
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.graincapacity &&
                                        data.graincapacity != "null"
                                        ? data.graincapacity
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Breadth Extreme
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.breadthextreme &&
                                        data.breadthextreme != "null"
                                        ? data.breadthextreme
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Breadth Moulded
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.breadthmoulded &&
                                        data.breadthmoulded != "null"
                                        ? data.breadthmoulded
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Crane SWL
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.craneswl &&
                                        data.craneswl != "null"
                                        ? data.craneswl
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Depth
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.depth &&
                                        data.depth != "null"
                                        ? data.depth
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Displacement
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.displacement &&
                                        data.displacement != "null"
                                        ? data.displacement
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Length Overall LOA
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.lengthoverallloa &&
                                        data.lengthoverallloa != "null"
                                        ? data.lengthoverallloa
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Light Displacement Tonnage
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.lightdisplacementtonnage &&
                                        data.lightdisplacementtonnage != "null"
                                        ? data.lightdisplacementtonnage
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  TEU
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.teu &&
                                        data.teu != "null"
                                        ? data.teu
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Tonnage Type
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.tonnagetype &&
                                        data.tonnagetype != "null"
                                        ? data.tonnagetype
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Year Of Build
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.yearofbuild &&
                                        data.yearofbuild != "null"
                                        ? data.yearofbuild
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Classification Society
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.classificationsociety &&
                                        data.classificationsociety != "null"
                                        ? data.classificationsociety
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Shipbuilder
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipbuilder &&
                                        data.shipbuilder != "null"
                                        ? data.shipbuilder
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>Main Engine Builder</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine Designer
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginedesigner &&
                                        data.mainenginedesigner != "null"
                                        ? data.mainenginedesigner
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine Model
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginemodel &&
                                        data.mainenginemodel != "null"
                                        ? data.mainenginemodel
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine Number of Cylinders
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginenumberofcylinders &&
                                        data.mainenginenumberofcylinders != "null"
                                        ? data.mainenginenumberofcylinders
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine RPM
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginerpm &&
                                        data.mainenginerpm != "null"
                                        ? data.mainenginerpm
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine Stroke Type
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginestroketype &&
                                        data.mainenginestroketype != "null"
                                        ? data.mainenginestroketype
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>

                              <Row>
                                <Col className="bold" span={12}>
                                  Main Engine Type
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.mainenginetype &&
                                        data.mainenginetype != "null"
                                        ? data.mainenginetype
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Maritime Mobile Service Identity MMSI Number
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.maritimemobileserviceidentitymmsinumber &&
                                        data.maritimemobileserviceidentitymmsinumber !=
                                        "null"
                                        ? data.maritimemobileserviceidentitymmsinumber
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>

                              <Row>
                                <Col className="bold" span={12}>
                                  Number of Hatches
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.numberofhatches &&
                                        data.numberofhatches != "null"
                                        ? data.numberofhatches
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>

                              <Row>
                                <Col className="bold" span={12}>
                                  Number of Holds
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.numberofholds &&
                                        data.numberofholds != "null"
                                        ? data.numberofholds
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>
                              Registered Owner & Manager details
                            </Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredowner &&
                                        data.registeredowner != "null"
                                        ? data.registeredowner
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner Code
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredownercode &&
                                        data.registeredownercode != "null"
                                        ? data.registeredownercode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner Country Of Control
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredownercountryofcontrol &&
                                        data.registeredownercountryofcontrol !=
                                        "null"
                                        ? data.registeredownercountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner Country of Domicile
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredownercountryofdomicile &&
                                        data.registeredownercountryofdomicile !=
                                        "null"
                                        ? data.registeredownercountryofdomicile
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner Country of Domicile Code
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredownercountryofdomicilecode &&
                                        data.registeredownercountryofdomicilecode !=
                                        "null"
                                        ? data.registeredownercountryofdomicilecode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Registered Owner Country Of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.registeredownercountryofregistration &&
                                        data.registeredownercountryofregistration !=
                                        "null"
                                        ? data.registeredownercountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>Technical Manager</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Technical Manager Code
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.technicalmanagercode &&
                                        data.technicalmanagercode != "null"
                                        ? data.technicalmanagercode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Technical Manager Country Of Control
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.technicalmanagercountryofcontrol &&
                                        data.technicalmanagercountryofcontrol !=
                                        "null"
                                        ? data.technicalmanagercountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Technical Manager Country Of Domicile
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.technicalmanagercountryofdomicile &&
                                        data.technicalmanagercountryofdomicile !=
                                        "null"
                                        ? data.technicalmanagercountryofdomicile
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={12}>
                                  Technical Manager Country Of Domicile Code
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.technicalmanagercountryofdomicilecode &&
                                        data.technicalmanagercountryofdomicilecode !=
                                        "null"
                                        ? data.technicalmanagercountryofdomicilecode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={12}>
                                  Technical Manager Country Of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={12}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.technicalmanagercountryofregistration &&
                                        data.technicalmanagercountryofregistration !=
                                        "null"
                                        ? data.technicalmanagercountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>Ship Manager</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Company
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanager &&
                                        data.shipmanager != "null"
                                        ? data.shipmanager
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Company Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanagercompanycode &&
                                        data.shipmanagercompanycode != "null"
                                        ? data.shipmanagercompanycode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Country Of Control
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanagercountryofcontrol &&
                                        data.shipmanagercountryofcontrol != "null"
                                        ? data.shipmanagercountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Country of Domicile Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanagercountryofdomicilecode &&
                                        data.shipmanagercountryofdomicilecode !=
                                        "null"
                                        ? data.shipmanagercountryofdomicilecode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Country of Domicile Name
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanagercountryofdomicilename &&
                                        data.shipmanagercountryofdomicilename !=
                                        "null"
                                        ? data.shipmanagercountryofdomicilename
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Ship Manager Country Of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.shipmanagercountryofregistration &&
                                        data.shipmanagercountryofregistration !=
                                        "null"
                                        ? data.shipmanagercountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>Group Beneficial Owner</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Group Beneficial Owner <br /> Company Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.groupbeneficialownercompanycode &&
                                        data.groupbeneficialownercompanycode !=
                                        "null"
                                        ? data.groupbeneficialownercompanycode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Group Beneficial Owner Country Of Control
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.groupbeneficialownercountryofcontrol &&
                                        data.groupbeneficialownercountryofcontrol !=
                                        "null"
                                        ? data.groupbeneficialownercountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Group Beneficial Owner Country of Domicile
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.groupbeneficialownercountryofdomicile &&
                                        data.groupbeneficialownercountryofdomicile !=
                                        "null"
                                        ? data.groupbeneficialownercountryofdomicile
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Group Beneficial Owner Country of Domicile
                                  Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.groupbeneficialownercountryofdomicilecode &&
                                        data.groupbeneficialownercountryofdomicilecode !=
                                        "null"
                                        ? data.groupbeneficialownercountryofdomicilecode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Group Beneficial Owner Country Of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.groupbeneficialownercountryofregistration &&
                                        data.groupbeneficialownercountryofregistration !=
                                        "null"
                                        ? data.groupbeneficialownercountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>Operator</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Operator Company Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.operatorcompanycode &&
                                        data.operatorcompanycode != "null"
                                        ? data.operatorcompanycode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Operator Country Of Control
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.operatorcountryofcontrol &&
                                        data.operatorcountryofcontrol != "null"
                                        ? data.operatorcountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Operator Country of Domicile Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.operatorcountryofdomicilecode &&
                                        data.operatorcountryofdomicilecode !=
                                        "null"
                                        ? data.operatorcountryofdomicilecode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Operator Country of Domicile Name
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.operatorcountryofdomicilename &&
                                        data.operatorcountryofdomicilename !=
                                        "null"
                                        ? data.operatorcountryofdomicilename
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Operator Country Of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.operatorcountryofregistration &&
                                        data.operatorcountryofregistration !=
                                        "null"
                                        ? data.operatorcountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Divider>
                            <Title level={4}>BareBoat details</Title>
                          </Divider>
                          <Row>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Bareboat Charter Company
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.bareboatchartercompany &&
                                        data.bareboatchartercompany != "null"
                                        ? data.bareboatchartercompany
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Bareboat Charter Company Code
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.bareboatchartercompanycode &&
                                        data.bareboatchartercompanycode != "null"
                                        ? data.bareboatchartercompanycode
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Bareboat Charter Country of Control
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.bareboatchartercountryofcontrol &&
                                        data.bareboatchartercountryofcontrol !=
                                        "null"
                                        ? data.bareboatchartercountryofcontrol
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col className="bold" span={10}>
                                  Bareboat Charter Country of Domicile
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.bareboatchartercountryofdomicile &&
                                        data.bareboatchartercountryofdomicile !=
                                        "null"
                                        ? data.bareboatchartercountryofdomicile
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Col className="gutter-row" span={8}>
                              <Row>
                                <Col className="bold" span={10}>
                                  Bareboat Charter Country of Registration
                                </Col>
                                <Col
                                  className=""
                                  span={14}
                                  style={{ paddingRight: 10 }}
                                >
                                  <Input
                                    type="text"
                                    value={
                                      data != null &&
                                        data.bareboatchartercountryofregistration &&
                                        data.bareboatchartercountryofregistration !=
                                        "null"
                                        ? data.bareboatchartercountryofregistration
                                        : ""
                                    }
                                  />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </Content>
            </Layout>
          </Layout>
        </div>
      </>
    );
  }
}

export default VesselDetails;
