import {
  Layout,
  Table,
  Typography,
  Input,
  Row,
  
  Col,
  Divider,
  Modal,
  message,
  Spin,
  Alert,
} from "antd";
import React, { Component } from "react";
import URL_WITH_VERSION, {
  objectToQueryStringFunc,
  postAPICall,
  getAPICall,
  openNotificationWithIcon,
} from "../../shared";
import MapView from "./map";
import TrackVesselMap from "./track-vessel-map";
import TrackVesselOpenSea from "./track-vessel-open-sea";
import ToolbarUI from "../../components/CommonToolbarUI/toolbar_index";
import VesselSchedule from "../../components/vessel-form/index";
import VesselDetails from "./vesselDetails";
import SidebarColumnFilter from "../../shared/SidebarColumnFilter";
import moment from "moment";
import SelectedVesselView from "./selected-vessel-view";
import { EnvironmentOutlined } from "@ant-design/icons";
const { Title } = Typography;
const { Content } = Layout;

class TrackVessel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      mapDataArr: [],
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      columns: [
        {
          title: "Vessel Name",
          dataIndex: "vessel_name",
          key: "vessel_name",
          ellipsis: "true",
          width: 200,
        },

        {
          title: "IMO No",
          dataIndex: "imo_no",
          key: "imo_no",

          width: 100,
        },
        {
          title: "MMSI",
          dataIndex: "mmsi_number",
          key: "mmsi_number",

          width: 200,
        },
        {
          title: "Deadweight",
          dataIndex: "deadWeight",
          key: "deadWeight",

          width: 200,
        },
        {
          title: "Last port",
          dataIndex: "last_port",
          key: "last_port",
          // ellipsis: "true",
          width: 200,
        },
        {
          title: "ATA",
          dataIndex: "ata",
          key: "ata",
          render: (date) => date && moment(date).format("YYYY-MM-DD HH:mm"),
          //  ellipsis: "true",
          width: "200px",
        },
        {
          title: "ATD",
          dataIndex: "atd",
          key: "atd",
          render: (date) => date && moment(date).format("YYYY-MM-DD HH:mm"),
          // ellipsis: "true",
          width: "200px",
        },
        {
          title: "Next port",
          dataIndex: "next_port",
          key: "next_port",
          //ellipsis: "true",
          width: "200px",
        },
        {
          title: "ETA Calc",
          dataIndex: "eta_calc",
          key: "eta_calc",
          render: (date) => date && moment(date).format("YYYY-MM-DD HH:mm"),
          // ellipsis: "true",
          width: "200px",
        },
        {
          title: "Status",
          dataIndex: "status",
          key: "status",
          ellipsis: "true",
          width: 200,
          // render: (text, record) => (
          //   <div style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
          //     {text}
          //   </div>
          // ),
        },
        {
          title: "Destination",
          dataIndex: "destination",
          key: "destination",
          // ellipsis: "true",
          width: "200px",
        },
        {
          title: "ETA",
          dataIndex: "eta",
          key: "eta",
          render: (date) => date && moment(date).format("YYYY-MM-DD HH:mm"),
          // ellipsis: "true",
          width: "200px",
        },
        {
          title: "Current Port Name",
          dataIndex: "current_port_name",
          key: "current_port_name",
          // ellipsis: "true",
          width: "200px",
        },

        {
          title: "Last pos. Latitude",
          dataIndex: "last_pos_lat",
          key: "last_pos_lat",
          // ellipsis: "true",
          width: "200px",
        },
        {
          title: "Last Pos Longitude",
          dataIndex: "last_pos_lon",
          key: "last_pos_lon",
          //  ellipsis: "true",
          width: "200px",
        },
        {
          title: "Degree",
          dataIndex: "degree",
          key: "degree",
          ellipsis: "true",
          width: "200px",
        },
        {
          title: "Speed",
          dataIndex: "speed",
          key: "speed",
          ellipsis: "true",
          width: "200px",
        },
        {
          title: "Live On Map",
          width: "100px",
          // dataIndex: 'address',
          // key: "address",
          align: "center",
          fixed: "right",
          render: (data) => {
            let icon = (
              <EnvironmentOutlined
                onClick={() => this.trackVessel(data)}
                style={{ fontSize: "25px", color: "red", alignSelf: "center" }}
              />
            );
            return data.last_pos_lat != "None" && data.last_pos_lat != "None"
              ? icon
              : null;
          },
        },
        // {
        //   title: "Tools",
        //   // key: "action",
        //   render: (data) => {
        //     return (
        //       <div className="editable-row-operations">
        //         <span
        //           className="iconWrapper"
        //           onClick={(e) => this.redirectToDetailes(e, data.imo_no)}
        //         >
        //       <EyeOutlined />
        //         </span>
        //         {data.toc_vessel_details == 1 ? (
        //           <span className="iconWrapper">
        //             <Icon
        //               type="eye"
        //               onClick={(e) => this.redirectToAdd(e, data.imo_no)}
        //             />
        //           </span>
        //         ) : null}
        //       </div>
        //     );
        //   },
        // },
        {
          title: "Add my fleet",
          ellipsis: "true",
          width: "100px",
          // dataIndex: "my_fleet",
          // key: "address",
          align: "center",
          fixed: "right",
          render: (data) => {
            let ischecked;
            data.my_fleet === 1 ? (ischecked = true) : (ischecked = false);
            let checkbox =
              data.my_fleet === 1 ? (
                <input type="checkbox" name="" id="" checked={true} disabled />
              ) : (
                <input
                  type="checkbox"
                  name=""
                  id=""
                  checked={false}
                  onChange={() => this.addtofleet(data.imo_no, true)}
                />
              );
            return <div className="editable-row-operations">{checkbox}</div>;
          },
        },
      ],

      // columnsF: [
      //   {
      //     title: "S/N",
      //     dataIndex: "sn",
      //     key: "sn",
      //   },
      //   {
      //     title: "Cargo C/P Qty",
      //     dataIndex: "cargo-cp-qty",
      //     key: "cargo-cp-qty",
      //   },
      //   {
      //     title: "Unit",
      //     dataIndex: "unit",
      //     key: "unit",
      //   },
      //   {
      //     title: "Load Port",
      //     dataIndex: "load-port",
      //     key: "load-port",
      //   },
      //   {
      //     title: "Dis Port",
      //     dataIndex: "disch-port",
      //     key: "disch-port",
      //   },
      //   {
      //     title: "Freight $",
      //     dataIndex: "freight",
      //     key: "freight",
      //   },
      //   {
      //     title: "Laycan",
      //     dataIndex: "laycan",
      //     key: "laycan",
      //   },
      // ],

      // columnsTonnage: [
      //   {
      //     title: "S/N",
      //     dataIndex: "sn",
      //     key: "sn",
      //   },
      //   {
      //     title: "Vessel Name",
      //     dataIndex: "vessel-name",
      //     key: "vessel-name",
      //   },
      //   {
      //     title: "Type",
      //     dataIndex: "type",
      //     key: "type",
      //   },
      //   {
      //     title: "DWT",
      //     dataIndex: "dwt",
      //     key: "dwt",
      //   },
      //   {
      //     title: "Open Port",
      //     dataIndex: "open-port",
      //     key: "open-port",
      //   },
      //   {
      //     title: "Open Date",
      //     dataIndex: "open-date",
      //     key: "open-date",
      //   },
      //   {
      //     title: "Last Port",
      //     dataIndex: "last-port",
      //     key: "last-port",
      //   },
      //   {
      //     title: "ATA",
      //     dataIndex: "ata",
      //     key: "ata",
      //   },
      //   {
      //     title: "Hire Rate",
      //     dataIndex: "hire-rate",
      //     key: "hire-rate",
      //   },
      // ],

      mapData: null,
      loading: false,
      filterVessels: [],
      isAdd: true,
      isVisible: false,
      formDataValues: {},
      isAddDetails: true,
      isVisibleDetails: false,
      formDetailsValues: {},
      sidebarVisible: false,
      visibleLiveVessel: false,
      isLiveDetailLoading: false,
      isMapBox: true,
      typesearch: {},
      donloadArray: [],
      isShowLocationFromtable: false,
      showvesseldetailfromtable: {},
    };
  }

  componentDidMount = async () => {
    this.setState({
      loading: true,
    });
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      `${process.env.REACT_APP_URL}v1/` + "vessel/live/data?p=" + 0,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          mapDataArr: result.data,
        });
      })
      .catch((error) => undefined);

    this.getTableData();
  };

  trackVessel = async (dataset) => {
    this.setState({
      ...this.state,
      showvesseldetailfromtable: dataset,
      isShowLocationFromtable: true,
    });
  };


  // _searchVessel = (q) => {
  //   if (q.length > 3) {
  //     let fArr = this.state.mapDataArr
  //       .filter((item) => {
  //         if (
  //           item["vessel_name"].toLowerCase().includes(q.toLowerCase()) ||
  //           item["imo_no"].includes(q.toLowerCase())
  //         ) {
  //           if (
  //             item["last_pos_lat"].toLowerCase() !== "none" &&
  //             item["last_pos_lon"].toLowerCase() !== "none"
  //           ) {
  //             return item;
  //           }
  //         }
  //       })
  //       .map((filterItem) => filterItem);
  //     this.setState({
  //       filterVessels: fArr,
  //     });
  //   }
  // };

  getTableData = async (searchtype = {}) => {
    const { pageOptions } = this.state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = {};
    let search =
      searchtype &&
      searchtype.hasOwnProperty("searchOptions") &&
      searchtype.hasOwnProperty("searchValue")
        ? searchtype
        : this.state.typesearch;

    if (
      search &&
      search.hasOwnProperty("searchValue") &&
      search.hasOwnProperty("searchOptions") &&
      search["searchOptions"] !== "" &&
      search["searchValue"] !== ""
    ) {
      let wc = {};
      search["searchValue"] = search["searchValue"].trim();
      if (search["searchOptions"].indexOf(";") > 0) {
        let so = search["searchOptions"].split(";");
        wc = { OR: {} };
        so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
      } else {
        wc[search["searchOptions"]] = { l: search["searchValue"] };
      }

      headers["where"] = wc;
      this.state.typesearch = {
        searchOptions: search.searchOptions,
        searchValue: search.searchValue,
      };
    }
    // console.log(search);
    this.setState({
      ...this.state,
      loading: true,
      data: [],
    });
    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${
      process.env.REACT_APP_URL
    }v1/vessel/live/data?${qParamString}`;

    const response = await getAPICall(_url, headers);
    const data = await response;

    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    let donloadArr = [];
    if (dataArr.length > 0 && totalRows > this.state.data.length) {
      dataArr.forEach((d) => donloadArr.push(d["id"]));
      state["data"] = dataArr;
    }

    // let state = { loading: false };
    // let donloadArr = []
    // state["data"] = data && data.data ? data.data : [];
    // // console.log("**** data.total_rows;", data.total_rows);
    // let totalRows = data.total_rows;
    this.setState({
      ...this.state,
      ...state,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });
  };
  callOptions = (evt) => {
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = this.state.pageOptions;
      let search = {
        searchOptions: evt["searchOptions"],
        searchValue: evt["searchValue"],
      };
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: search, pageOptions: pageOptions },
        () => {
          this.getTableData(search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = this.state.pageOptions;
      pageOptions["pageIndex"] = 1;
      this.setState(
        { ...this.state, search: {}, pageOptions: pageOptions,typesearch:{} },
        () => {
          this.getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let data = this.state.data;
      let columns = Object.assign([], this.state.columns);

      if (data.length > 0) {
        for (var k in data[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !this.state.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  redirectToAdd = async (e, imo = null) => {
    if (imo) {
      const response = await getAPICall(
        `${URL_WITH_VERSION}/vessel/list/imo/${imo}`
      );
      const respData = await response["data"];
      this.setState(
        { ...this.state, isAdd: false, formDataValues: respData },
        () => this.setState({ ...this.state, isVisible: true })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, isVisible: true });
    }
  };

  redirectToDetailes = async (e, imo = null) => {
    if (imo) {
      const response = await getAPICall(
        `${process.env.REACT_APP_URL_NEW}/VesselDetail/find/${imo}`
      );
      const respData = await response[0];

      this.setState(
        { ...this.state, isAddDetails: false, formDetailsValues: respData },
        () => this.setState({ ...this.state, isVisibleDetails: true })
      );
    } else {
      this.setState({
        ...this.state,
        isAddDetails: true,
        isVisibleDetails: true,
      });
    }
  };

  onCancel = () => {
    this.getTableData();
    this.setState({ ...this.state, isAdd: true, isVisible: false });
  };

  onCancelDetails = () => {
    this.getTableData();
    this.setState({
      ...this.state,
      isAddDetails: true,
      isVisibleDetails: false,
    });
  };

  addtofleet = async (imo, checked) => {
    if (checked === true) {
      const response = await postAPICall(
        `${URL_WITH_VERSION}/vessel/add-my-fleet`,
        { imo: imo, fleet_value: 1 },
        "post"
      );
      //   console.log("***** response", response);
      this.getTableData();
    }
  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };

    columns.map(
      (e) =>
        e.invisible === "false" && e.key !== "action"
          ? cols.push(e.dataIndex)
          : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${
        qParams.l
      }&ids=${filter}`,
      "_blank"
    );
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  };

  //resizing function
  handleResize = (index) => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  showVisibleliveVessel = (show) =>
    this.setState({ ...this.state, visibleLiveVessel: show }, () =>
      this.getTableData()
    );

  render() {
    const {
      columns,
      loading,
      data,
      pageOptions,
      filter,
      search,
      isAdd,
      isVisible,
      formDataValues,
      filterVessels,
      isAddDetails,
      isVisibleDetails,
      formDetailsValues,
      sidebarVisible,
      showSelectedVessel,
      isLiveDetailLoading,
      selectedData,
      visibleLiveVessel,
      isShowLocationFromtable,
      showvesseldetailfromtable,
    } = this.state;

    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true" ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: (column) => ({
          width: column.width,
          // onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="tcov-wrapper full-wraps">
        <Layout className="layout-wrapper">
          <Layout>
            <Content className="content-wrapper">
              <div className="fieldscroll-wrap">
                <div className="body-wrapper" style={{ marginRight: "65px" }}>
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body common-fields-wrapper">
                        <table style={{ width: "100%" }}>
                          <tbody>
                            <tr>
                              <td>
                                <Title level={2}>Database - Live Vessel</Title>
                              </td>

                              {/* <td valign="right" style={{ textAlign: "right" }}>
                                <Input
                                  onChange={(e) =>
                                    this._searchVessel(e.target.value)
                                  }
                                  placeholder="Search Vessel (Vessel Name, IMO)"
                                  style={{ marginTop: 10, marginBottom: 10 }}
                                />
                              </td> */}
                              <td>
                                <button
                                  className="btn btn-info float-right"
                                  onClick={() => {
                                    this.setState({
                                      isMapBox: !this.state.isMapBox,
                                    });
                                  }}
                                >
                                  {this.state.isMapBox ? "OpenSea" : "MapBox"}
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <Divider />

      

                        {data && data.length > 0 && !isShowLocationFromtable ? (
                          <>
                            <div
                              style={{
                                display: this.state.isMapBox ? "block" : "none",
                              }}
                            >
                              <TrackVesselMap
                                mapData={data}
                                searchedItem={
                                  filterVessels.length > 0
                                    ? filterVessels[0]
                                    : {}
                                }
                                loadpage={() => {
                                  this.getTableData();
                                }}
                              />
                            </div>

                            <div
                              style={{
                                display: this.state.isMapBox ? "none" : "block",
                              }}
                            >
                              <TrackVesselOpenSea />
                            </div>
                          </>
                        ) : null}

                        {data && data.length > 0 && isShowLocationFromtable ? (
                          <>
                            <div
                              style={{
                                display: this.state.isMapBox ? "block" : "none",
                              }}
                            >
                              <TrackVesselMap
                                mapData={data.filter(
                                  (el) =>
                                    el.imo_no !==
                                    showvesseldetailfromtable.imo_no
                                )}
                                searchedItem={showvesseldetailfromtable}
                                loadpage={() => {
                                  this.getTableData();
                                }}
                                isShowLocationFromtable={
                                  isShowLocationFromtable
                                }
                                modalCloseEvent={() =>
                                  this.setState({
                                    ...this.state,
                                    isShowLocationFromtable: false,
                                  })
                                }
                              />
                            </div>
                            <div
                              style={{
                                display: this.state.isMapBox ? "none" : "block",
                              }}
                            >
                              <TrackVesselOpenSea />
                            </div>
                          </>
                        ) : null}

                        {loading === false ? (
                          <ToolbarUI
                            routeUrl={"track-my-fleet"}
                            optionValue={{
                              pageOptions: pageOptions,
                              columns: columns,
                              search: search,
                            }}
                            callback={(e) => this.callOptions(e)}
                            filter={filter}
                            dowloadOptions={[
                              {
                                title: "CSV",
                                event: () =>
                                  this.onActionDonwload("csv", "live-vessel"),
                              },
                              {
                                title: "PDF",
                                event: () =>
                                  this.onActionDonwload("pdf", "live-vessel"),
                              },
                              {
                                title: "XLS",
                                event: () =>
                                  this.onActionDonwload("xlsx", "live-vessel"),
                              },
                            ]}
                          />
                        ) : (
                          undefined
                        )}
                        <Table
                          bordered
                          columns={tableColumns}
                          dataSource={this.state.data}
                          // scroll={{
                          //   x:'max-content',
                          //   y:400
                          // }}
                          scroll={{ x: "max-content" }}
                          loading={this.state.loading}
                          className="inlineTable resizeableTable"
                          size="small"
                          pagination={false}
                          rowClassName={(r, i) =>
                            i % 2 === 0
                              ? "table-striped-listing"
                              : "dull-color table-striped-listing"
                          }
                        />

                        {/* 
                        <Row>
                          <Col span={11}>
                            <br />
                            <table style={{ width: "100%" }}>
                              <tbody>
                                <tr>
                                  <td>
                                    <Title level={4}>Cargo Trade Schedule</Title>
                                  </td>
                                  <td
                                    valign="right"
                                    style={{ textAlign: "right" }}
                                  >
                                    <Input placeholder="Search" />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <Divider />
                            <Table
                              columns={this.state.columnsF}
                              scroll={{
                                x: 800,
                              }}
                            />
                            ;
                          </Col>
                          <Col span={1}></Col>
                          <Col span={11}>
                            <br />
                            <table style={{ width: "100%" }}>
                              <tbody>
                                <tr>
                                  <td>
                                    <Title level={4}>
                                      Tonnage Trade Schedule
                                    </Title>
                                  </td>
                                  <td
                                    valign="right"
                                    style={{ textAlign: "right" }}
                                  >
                                    <Input placeholder="Search" />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <Divider />
                            <Table
                              columns={this.state.columnsTonnage}
                              scroll={{
                                x: 800,
                              }}
                            />
                            ;
                          </Col>
                        </Row> */}

                        

                        {isVisible === true ? (
                          <Modal
                            title={
                              (isAdd === false ? "Edit" : "Add") +
                              " Vessel Form"
                            }
                           open={isVisible}
                            width="95%"
                            onCancel={this.onCancel}
                            style={{ top: "10px" }}
                            bodyStyle={{
                              height: 790,
                              overflowY: "auto",
                              padding: "0.5rem",
                            }}
                            footer={null}
                          >
                            <VesselSchedule
                              formData={formDataValues}
                              modalCloseEvent={this.onCancel}
                              onEdit={true}
                              showSideListBar={false}
                            />
                            {/* {isAdd === false ? (
                              <VesselSchedule
                                formData={formDataValues}
                                modalCloseEvent={this.onCancel}
                                onEdit={true}
                                showSideListBar={false}
                              />
                            ) : (
                              <VesselSchedule
                                formData={{}}
                                modalCloseEvent={this.onCancel}
                              />
                            )} */}
                          </Modal>
                        ) : (
                          undefined
                        )}

                        {isVisibleDetails === true ? (
                          <Modal
                            style={{ top: "2%" }}
                            title={"Vessel Details"}
                           open={isVisibleDetails}
                            onCancel={this.onCancelDetails}
                            width="95%"
                            footer={null}
                          >
                            <VesselDetails
                              formDetailsValues={formDetailsValues}
                            />
                          </Modal>
                        ) : (
                          undefined
                        )}
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </Content>
          </Layout>
        </Layout>
        {sidebarVisible ? (
          <SidebarColumnFilter
            columns={columns}
            sidebarVisible={sidebarVisible}
            callback={(e) => this.callOptions(e)}
          />
        ) : null}
      </div>

      //-------------------------
    );
  }
}
export default TrackVessel;
