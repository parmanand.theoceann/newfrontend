import React, { useState } from "react";
import NormalFormIndex from "../../shared/NormalForm/normal_from_index";
import { ImportOutlined, SaveOutlined } from "@ant-design/icons";
import URL_WITH_VERSION, { postAPICall } from "../../shared";

const LifoBunkerPlan = ({ initqty, seacons, portcons, recqty, endqty,modalCloseEvent }) => {
  const [state, setState] = useState({
    frmName: "lifo_bunker_plan_form",
    formData: {
      lifobunkerplan: [
        {
          type: 3, // IFO
          init_qty: initqty?.init_qty_ifo
            ? parseFloat(initqty.init_qty_ifo).toFixed(2)
            : 0,
          init_prc: 0,
          sea_cons: seacons?.sea_cons_ifo
            ? parseFloat(seacons.sea_cons_ifo).toFixed(2)
            : 0,
          port_cons: portcons?.port_cons_ifo
            ? parseFloat(portcons.port_cons_ifo).toFixed(2)
            : 0,
          rec_qty: recqty?.rec_qty_ifo
            ? parseFloat(recqty.rec_qty_ifo).toFixed(2)
            : 0,
          last_prc: 0,
          end_qty: endqty?.end_qty_ifo
            ? parseFloat(endqty?.end_qty_ifo).toFixed(2)
            : 0,
          end_prc: 0,
          bunker_exp: 0,
        },

        {
          type: 4, // MGo
          init_qty: initqty?.init_qty_mgo
            ? parseFloat(initqty.init_qty_mgo).toFixed(2)
            : 0,
          init_prc: 0,
          sea_cons: seacons?.sea_cons_mgo
            ? parseFloat(seacons.sea_cons_mgo).toFixed(2)
            : 0,
          port_cons: portcons?.port_cons_mgo
            ? parseFloat(portcons.port_cons_mgo).toFixed(2)
            : 0,
          rec_qty: recqty?.rec_qty_mgo
            ? parseFloat(recqty.rec_qty_mgo).toFixed(2)
            : 0,
          last_prc: 0,
          end_qty: endqty?.end_qty_mgo
            ? parseFloat(endqty?.end_qty_mgo).toFixed(2)
            : 0,
          end_prc: 0,
          bunker_exp: 0,
        },

        {
          type: 5, // VLSFO
          init_qty: initqty?.init_qty_vlsfo
            ? parseFloat(initqty.init_qty_vlsfo).toFixed(2)
            : 0,
          init_prc: 0,
          sea_cons: seacons?.sea_cons_vlsfo
            ? parseFloat(seacons.sea_cons_vlsfo).toFixed(2)
            : 0,
          port_cons: portcons?.port_cons_vlsfo
            ? parseFloat(portcons.port_cons_vlsfo).toFixed(2)
            : 0,
          rec_qty: recqty?.rec_qty_vlsfo
            ? parseFloat(recqty.rec_qty_vlsfo).toFixed(2)
            : 0,
          last_prc: 0,
          end_qty: endqty?.end_qty_vlsfo
            ? parseFloat(endqty?.end_qty_vlsfo).toFixed(2)
            : 0,
          end_prc: 0,
          bunker_exp: 0,
        },

        {
          type: 7, // LSMGO
          init_qty: initqty?.init_qty_lsmgo
            ? parseFloat(initqty.init_qty_lsmgo).toFixed(2)
            : 0,
          init_prc: 0,
          sea_cons: seacons?.sea_cons_lsmgo
            ? parseFloat(seacons.sea_cons_lsmgo).toFixed(2)
            : 0,
          port_cons: portcons?.port_cons_lsmgo
            ? parseFloat(portcons.port_cons_lsmgo).toFixed(2)
            : 0,
          rec_qty: recqty?.rec_qty_lsmgo
            ? parseFloat(recqty.rec_qty_lsmgo).toFixed(2)
            : 0,
          last_prc: 0,
          end_qty: endqty?.end_qty_lsmgo
            ? parseFloat(endqty?.end_qty_lsmgo).toFixed(2)
            : 0,
          end_prc: 0,
          bunker_exp: 0,
        },

        {
          type: 10, // ULSFO
          init_qty: initqty?.init_qty_ulsfo
            ? parseFloat(initqty.init_qty_ulsfo).toFixed(2)
            : 0,
          init_prc: 0,
          sea_cons: seacons?.sea_cons_ulsfo
            ? parseFloat(seacons.sea_cons_ulsfo).toFixed(2)
            : 0,
          port_cons: portcons?.port_cons_ulsfo
            ? parseFloat(portcons.port_cons_ulsfo).toFixed(2)
            : 0,
          rec_qty: recqty?.rec_qty_ulsfo
            ? parseFloat(recqty.rec_qty_ulsfo).toFixed(2)
            : 0,
          last_prc: 0,
          end_qty: endqty?.end_qty_ulsfo
            ? parseFloat(endqty?.end_qty_ulsfo).toFixed(2)
            : 0,
          end_prc: 0,
          bunker_exp: 0,
        },
      ],
    },
  });


  const importFormData = (data) => {
    modalCloseEvent(data.lifobunkerplan);
  };

  return (
    <div className="body-wrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <NormalFormIndex
                key={"key_" + state.frmName + "_0"}
                formClass="label-min-height"
                formData={state.formData}
                showForm={true}
                frmCode={state.frmName}
                addForm={true}
                isShowFixedColumn={["LIFO Bunker Plan"]}
                inlineLayout={true}
                showToolbar={[
                  {
                    leftWidth: 8,
                    rightWidth: 16,
                    isLeftBtn: [
                      {
                        key: "s1",
                        isSets: [
                          {
                            id: "3",
                            key: "import",
                            type: <ImportOutlined />,
                            withText: "Import",
                            showToolTip: true,
                            event: (key, data) => importFormData(data),
                          },
                        ],
                      },
                    ],
                    isRightBtn: [],
                    isResetOption: false,
                  },
                ]}
              />
            </div>
          </div>
        </div>
      </article>
    </div>
  );
};

export default LifoBunkerPlan;
