import { Table, Popconfirm, Input, Checkbox, Icon, Button } from 'antd';
import { useStateCallback } from '../../shared';
import { useEffect } from 'react';



const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);


const  MGOList = (props) => {

    const [state, setState] = useStateCallback({
        columns: [{
            title: 'Port Name',
            dataIndex: 'port',
            width: 140,
            render: (text, record) => renderColumns(text, record, 'port'),
        },
        {
            title: 'Sea Cons',
            dataIndex: 'mgo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'mgo'),
        },
        {
            title: 'Avg Sea Cons',
            dataIndex: 'avgseacons',
            width: 100,
            render: (text, record) => {
                let num = Number(record.avgVal) > 0 ? Number(record.avgVal) : 1
                return (
                    <span>{((Number(record.mgo)) / num).toFixed(2)}</span>
                )
            }
        },
        {
            title: 'ROB Arr',
            dataIndex: 'arob_mgo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'arob_mgo'),
        },
        {
            title: 'Port Cons',
            dataIndex: 'pc_mgo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'pc_mgo'),
        },
        {
            title: 'Avg Port Cons',
            dataIndex: 'avgportcons',
            width: 100,
          
            render: (text, record) => {
                let num = Number(record.tpd) > 0 ? Number(record.tpd) : 1

                return (
                    <span>{((Number(record.pc_mgo)) / num).toFixed(2)}</span>
                )
            }

        },
        // {
        //     title: 'Planed Qty',
        //     dataIndex: 'reqqty',
        //     width: 80,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} disabled />
        //         )
        //     },
        // },
        {
            title: 'Receive Qty',
            dataIndex: 'r_mgo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'r_mgo'),
        },
        // {
        //     title: 'Price',
        //     dataIndex: 80,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} disabled />
        //         )
        //     },
        // },
        {
            title: 'ROB Dpt',
            dataIndex: 'dr_mgo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'dr_mgo'),
        },
            // {
            //     title: 'Select',
            //     width: 100,
            //     render: (text, record) => {
            //         const { editable } = record;
            //         return (
            //             <div className="editable-row-operations">
            //                 {
            //                     <Checkbox onChange={(evt) => onChangeEvent(evt)}></Checkbox>
            //                 }
            //             </div>
            //         )
            //     }
            // },
            // {
            //     title: 'Action',
            //     dataIndex: 'action',
            //     width: 100,
            //     // fixed: 'right',
            //     render: (text, record) => {
            //         const { editable } = record;
            //         return (
            //             <div className="editable-row-operations">
            //                 {
            //                     editable ?
            //                         <span>
            //                             <span className="iconWrapper save" onClick={() => save(record.key)}><SaveOutlined /></span>
            //                             <span className="iconWrapper cancel">
            //                                 <Popconfirm title="Sure to cancel?" onConfirm={() => cancel(record.key)}>
            //                                    <DeleteOutlined /> />
            //                                 </Popconfirm>
            //                             </span>
            //                         </span>
            //                         : <span className="iconWrapper edit" onClick={() => edit(record.key)}><EditOutlined /></span>
            //                 }
            //             </div>
            //         );
            //     },
            // }
        ],
        data: []
        //  props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails : []
    })

    let cacheData = props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails.map(item => ({ ...item })) : []

    useEffect(() =>{
        getFormData()
    },[])

    const getFormData = () => {
        let data = props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails : []
        let data1 = props && props.data && props.data.portitinerary ? props.data.portitinerary : []

        let arr = [];
        // let old_port = []
        data.length > 0 && data.map((val, ind) => {
            // if (old_port.includes(val.port) == false) {
            //     old_port.push(val.port)
                let obj = { ...val }
                if (typeof data1[ind] === "undefined") { } else {
                obj['avgVal'] = data1.length > 0 && (data1[ind] ? data1[ind].tsd : 0)
                obj['tpd'] = data1.length > 0 && data1[ind].hasOwnProperty('t_port_days')&&parseFloat(data1[ind].t_port_days).toFixed(2)
                }
                arr.push(obj)
            // }
        })
        // props.handleupdate(arr)
        setState(prevState => ({...prevState, data: arr}))
    }

    // Start table section
    const renderColumns = (text, record, column) =>{
        // console.log(record)
        record.mgo = parseFloat(record.mgo ? record.mgo : 0.00).toFixed(2)
        record.avgseacons = parseFloat(record.avgseacons ? record.avgseacons : 0.00).toFixed(2)
        record.arob_mgo = parseFloat(record.arob_mgo ? record.arob_mgo : 0.00).toFixed(2)
        record.pc_mgo = parseFloat(record.pc_mgo ? record.pc_mgo : 0.00).toFixed(2)
        record.avgportcons = parseFloat(record.avgportcons ? record.avgportcons : 0.00).toFixed(2)
        record.reqqty = parseFloat(record.reqqty ? record.reqqty : 0.00).toFixed(2)
        record.r_mgo = parseFloat(record.r_mgo ? record.r_mgo : 0.00).toFixed(2)
        record.price = parseFloat(record.price ? record.price : 0.00).toFixed(2)
        record.dr_mgo = parseFloat(record.dr_mgo ? record.dr_mgo : 0.00).toFixed(2)
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => handleChange(value, record.key, column)}
            />
        );
    }

    const handleChange = (value, key, column) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    const edit = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    const save = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
            cacheData = newData.map(item => ({ ...item }));
        }
    }

    const cancel = (key) => {
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }
    const onChangeEvent = (event) => {
    }

        return (
            <>
                <Table
                    bordered
                    dataSource={state.data}
                    columns={state.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                />

            </>
        )
    }

export default MGOList;