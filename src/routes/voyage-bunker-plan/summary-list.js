import React, { Component } from 'react';
import { Table, Popconfirm, Input, Icon, Button } from 'antd';
import { useStateCallback } from '../../shared';
import { useEffect } from 'react';


const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

const  SummaryList = (props) =>  {

    const [state, setState] = useStateCallback({
        data: [],
        columns: [{
            title: 'Type',
            dataIndex: 'type',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'type'),
        },
        {
            title: 'Init Qty',
            dataIndex: 'initQty',
            width: 150,
            render: (text, record) => renderColumns(text, record, 'initQty'),
        },
        {
            title: 'Init Prc',
            dataIndex: 'initPrice',
            width: 150,
            // render: (text, record) => {
            //     return (
            //         <Input defaultValue={0.00} />
            //     )
            // }
        },
        {
            title: 'Sea Cons',
            dataIndex: 'ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'ifo'),
        },
        {
            title: 'Avg Sea Cons',
            dataIndex: 'avgseacons',
            width: 100,
            render: (text, record) => {
                return (
                    <span>{((record.ifo) / props.days).toFixed(2)}</span>
                )
            }
            // renderColumns(text, record, 'avgseacons'),
        },
        {
            title: 'Port Cons',
            dataIndex: 'pc_ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'pc_ifo'),
        },
        {
            title: 'Rec Qty',
            dataIndex: 'recQty',
            width: 150,
            render: (text, record) => renderColumns(text, record, 'recQty'),
        },
        // {
        //     title: 'Opr Qty',
        //     dataIndex: 'oprqty',
        //     width: 150,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} />
        //         )
        //     },
        // },
        // {
        //     title: 'Last Prc',
        //     dataIndex: 'lastprc',
        //     width: 150,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} />
        //         )
        //     }
        // },
        {
            title: 'End Qty',
            dataIndex: 'endQty',
            width: 150,
            render: (text, record) => renderColumns(text, record, 'endQty'),
        },
        // {
        //     title: 'End Prc',
        //     dataIndex: 'endPrice',
        //     width: 150,
        //     // render: (text, record) => {
        //     //     return (
        //     //         <Input defaultValue={0.00} />
        //     //     )
        //     // }
        //     // renderColumns(text, record, 'endprc'),
        // },
            // {
            //     title: 'Action',
            //     dataIndex: 'action',
            //     width: 100,
            //     fixed: 'right',
            //     render: (text, record) => {
            //         const { editable } = record;
            //         return (
            //             <div className="editable-row-operations">
            //                 {
            //                     editable ?
            //                         <span>
            //                             <span className="iconWrapper save" onClick={() => save(record.key)}><SaveOutlined /></span>
            //                             <span className="iconWrapper cancel">
            //                                 <Popconfirm title="Sure to cancel?" onConfirm={() => cancel(record.key)}>
            //                                    <DeleteOutlined /> />
            //                                 </Popconfirm>
            //                             </span>
            //                         </span>
            //                         : <span className="iconWrapper edit" onClick={() => edit(record.key)}><EditOutlined /></span>
            //                 }
            //             </div>
            //         );
            //     },
            // }
        ]
    })

    let cacheData = [];

    useEffect(() => {
        getFormData()
    },[])

    const getFormData = () => {
        let data = props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails : []
        
        if (data.length > 0) {
            let arr = [], 
                ifo = 0.00, pc_ifo = 0.00,
                mgo = 0.00, pc_mgo = 0.00,
                lsmgo = 0.00, pc_lsmgo = 0.00,
                vlsfo = 0.00, pc_vlsfo = 0.00,
                ulsfo = 0.00, pc_ulsfo = 0.00,
                obj = {}, obj1 = {}, obj2 = {}, obj3 = {}, obj4 = {};
            data.map((val, ind) => {
                ifo += Number(val.ifo)
                pc_ifo += Number(val.pc_ifo)
             
                pc_mgo += Number(val.pc_mgo)
                mgo += Number(val.mgo)
                lsmgo += Number(val.lsmgo)
                pc_lsmgo += Number(val.pc_lsmgo)
                vlsfo += Number(val.vlsfo)
                pc_vlsfo += Number(val.pc_vlsfo)
                ulsfo += Number(val.ulsfo)
                pc_ulsfo += Number(val.pc_ulsfo)
                if (data.length - 1 == ind) {
                    obj['endQty'] = val.dr_ifo
                    obj1['endQty'] = val.dr_mgo
                    obj2['endQty'] = val.dr_lsmgo
                    obj3['endQty'] = val.dr_vlsfo
                    obj4['endQty'] = val.dr_ulsfo

                }
                if (ind == 0) {
                    obj = {
                        type: 'IFO',
                        initQty: val.arob_ifo,
                        ifo: ifo,
                       
                        pc_ifo: pc_ifo,
                        recQty: val.r_ifo,
                        initPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBAD').f_IFO_p : '',
                        endPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBOR').f_IFO_p : ''

                    }
                    obj1 = {
                        type: 'MGO',
                        initQty: val.arob_mgo,
                        ifo: mgo,
                      
                        pc_ifo: pc_mgo,
                        recQty: val.r_mgo,
                        initPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBAD').f_MGO_p : '',
                        endPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBOR').f_MGO_p : ''
                    }
                    obj2 = {
                        type: 'LSMGO',
                        initQty: val.arob_lsmgo,
                        ifo: lsmgo,
                        pc_ifo: pc_lsmgo,
                        recQty: val.r_lsmgo,
                        initPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBAD').f_LSMGO_p : '',
                        endPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBOR').f_LSMGO_p : ''
                    }
                    obj3 = {
                        type: 'VLSFO',
                        initQty: val.arob_vlsfo,
                        ifo: vlsfo,
                        pc_ifo: pc_vlsfo,
                        recQty: val.r_vlsfo,
                        initPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBAD').f_VLSFO_p : '',
                        endPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBOR').f_VLSFO_p : ''
                    }
                    obj4 = {
                        type: 'ULSFO',
                        initQty: val.arob_ulsfo,
                        ifo: ulsfo,
                        pc_ifo: pc_ulsfo,
                        recQty: val.r_ulsfo,
                        initPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBAD').f_ULSFO_p : '',
                        endPrice: props.initialPrice && props.initialPrice.length > 0 ? props.initialPrice.find(({ short_code }) => short_code === 'TCIBOR').f_ULSFO_p : ''
                    }
                }

            })
            obj['ifo'] = ifo
            obj['pc_ifo'] = pc_ifo
            obj['avg_seaCons']=(ifo/props.days).toFixed(2)

            obj1['ifo'] = mgo
            obj1['pc_ifo'] = pc_mgo
            obj1['avg_seaCons']=(mgo/props.days).toFixed(2)

            obj2['ifo'] = lsmgo
            obj2['pc_ifo'] = pc_lsmgo
            obj2['avg_seaCons']=(lsmgo/props.days).toFixed(2)


            obj3['ifo'] = vlsfo
            obj3['pc_ifo'] = pc_vlsfo
            obj3['avg_seaCons']=(vlsfo/props.days).toFixed(2)

            obj4['ifo'] = ulsfo
            obj4['pc_ifo'] = pc_ulsfo
            obj4['avg_seaCons']=(ulsfo/props.days).toFixed(2)
            arr.push(obj, obj1, obj2, obj3, obj4)
            cacheData = arr.map(item => ({ ...item }))
          
            props.handleupdate(arr)

            setState(prevState => ({...prevState, data: arr}))
        }
       
        

    }

    // Start table section
    const renderColumns = (text, record, column) =>{
        // Added check for missing and empty data
        record.initQty = parseFloat(record.initQty ? record.initQty : 0.00).toFixed(2) 
        record.initPrice = parseFloat(record.initPrice ? record.initPrice : 0.00).toFixed(2) 
        record.ifo = parseFloat(record.ifo ? record.ifo : 0.00).toFixed(2) 
        record.pc_ifo = parseFloat(record.pc_ifo ? record.pc_ifo : 0.00).toFixed(2) 
        record.recQty = parseFloat(record.recQty ? record.recQty : 0.00).toFixed(2) 
        record.endQty = parseFloat(record.endQty ? record.endQty : 0.00).toFixed(2) 
        record.endPrice = parseFloat(record.endPrice ? record.endPrice : 0.00).toFixed(2) 
        
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => handleChange(value, record.key, column)}
            />
        );
    }

    const handleChange = (value, key, column) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    const edit = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    const save = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
            cacheData = newData.map(item => ({ ...item }));
        }
    }

    const cancel = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    // End table section

    return (
        <>
            <Table
                bordered
                dataSource={state.data}
                columns={state.columns}
                scroll={{ y: 300 }}
                size="small"
                pagination={false}
                rowClassName={(r, i) =>
                    i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
            />
        </>
        )
    }

export default SummaryList;