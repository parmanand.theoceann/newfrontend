import React, { useEffect, useState } from "react";
import { Form, Input, Switch, Tabs, Row, Col, Modal } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import SummaryList from "./summary-list";
import BunkerReqSummaryList from "./bunker-req-summary-list";
import IFOList from "./ifo";
import MGOList from "./mgo";
import ULSFOList from "./ulsfo";
import LSMGOList from "./lsmgo";
import VLSFOList from "./vlsfo";
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  useStateCallback,
} from "../../shared";

import VoyageBunkerPlanReport from "../operation-reports/VoyageBunkerPlan";

const FormItem = Form.Item;

const TabPane = Tabs.TabPane;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const VoygenBunkerPlan = (props) => {
 
  const [state, setState] = useState({
    modals: {
      VoyageBunkerPlan: false,
    },
    id: props?.data?.id,
  });

  const EstimateReport = async (showEstimateReport) => {
    // for report Api
    // const responseReport = await getAPICall(`${URL_WITH_VERSION}/voyage-manager/voyage_bunker/report?e=${state.id}`);
    // const respDataReport = await responseReport['data'];
    let responseReport = props.data;
    if (responseReport) {
      setState((prevState) => ({
        ...prevState,
        reportFormData: responseReport,
        isShowEstimateReport: showEstimateReport,
      }));
    } else {
      openNotificationWithIcon("error", "Unable to show report", 5);
    }
  };

  const showHideModal = (visible, modal) => {
    const { modals } = state;
    let _modal = {};
    _modal[modal] = visible;
    setState((prevState) => ({
      ...prevState,
      modals: Object.assign(modals, _modal),
    }));
  };

  const handleupdate = (data) => {
    setState((prevState) => ({ ...prevState, summary: data }));
  };

  const { isShowEstimateReport, reportFormData, summary, bunkerplandata } =
    state;

  return (
    <div className="body-wrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection">
                <span key="first" className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    {/* <li>
                        <span className="text-bt">
                          <SaveOutlined /> Save
                        </span>
                      </li> */}
                  </ul>
                </span>
              </div>
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li>
                      <span
                        className="text-bt"
                        onClick={() => EstimateReport(true)}
                      >
                        Report
                      </span>
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <Tabs defaultActiveKey="initialbunderdata" size="small">
              <TabPane
                className="pt-3"
                tab="Initial Bunker Data"
                key="initialbunderdata"
              >
                <Tabs defaultActiveKey="summary" size="small">
                  <TabPane className="pt-3" tab="Summary" key="summary">
                    <>
                      <SummaryList
                        data={props.data}
                        days={
                          props && props.days && props.days.tsd
                            ? props.days.tsd
                            : 1
                        }
                        initialPrice={props && props.initialPrice}
                        handleupdate={(data) => handleupdate(data)}
                      />
                      <hr />
                      {/* <BunkerReqSummaryList /> */}

                      <Row gutter={16} className="bunk-wrap-form">
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                          <FormItem
                            {...formItemLayout}
                            label="Starting Lube (L)"
                          >
                            <Input size="default" placeholder="0.00" />
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                          <FormItem {...formItemLayout} label="Received">
                            <Input size="default" placeholder="0.00" />
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                          <FormItem {...formItemLayout} label="Ending Lube">
                            <Input size="default" placeholder="0.00" />
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                          <FormItem {...formItemLayout} label="Fuel Zone Set">
                            <Input size="default" placeholder="Fuel Zone 1" />
                          </FormItem>
                        </Col>

                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                          <FormItem {...formItemLayout} label="Scrubber">
                            <Switch
                              checkedChildren={<CheckOutlined />}
                              unCheckedChildren={<CloseOutlined />}
                              defaultChecked
                            />
                          </FormItem>
                        </Col>
                      </Row>
                    </>
                  </TabPane>
                  <TabPane className="pt-3" tab="IFO" key="ifo">
                    <IFOList
                      data={props.data}
                      // handleupdate={(data)=>handleupdate(data,'ifo')}
                    />
                  </TabPane>
                  <TabPane className="pt-3" tab="MGO" key="mgo">
                    <MGOList
                      data={props.data}
                      //  handleupdate={(data)=>handleupdate(data,'mgo')}
                    />
                  </TabPane>
                  <TabPane className="pt-3" tab="LSMGO" key="lsmgo">
                    <LSMGOList
                      data={props.data}
                      // handleupdate={(data)=>handleupdate(data,'lsmgo')}
                    />
                  </TabPane>
                  <TabPane className="pt-3" tab="ULSFO" key="ulsfo">
                    <ULSFOList
                      data={props.data}
                      // handleupdate={(data)=>handleupdate(data,'ulsfo')}
                    />
                  </TabPane>
                  <TabPane className="pt-3" tab="VLSFO" key="vlsfo">
                    <VLSFOList
                      data={props.data}
                      // handleupdate={(data)=>handleupdate(data,'vlsfo')}
                    />
                  </TabPane>

                  {/* <TabPane className="pt-3" tab="Surveys" key="surveys">
                      Surveys
                    </TabPane>
                    <TabPane className="pt-3" tab="Swaps" key="swaps">
                      Swaps
                    </TabPane> */}
                </Tabs>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </article>
      {isShowEstimateReport ? (
        <Modal
          style={{ top: "2%" }}
          title="Reports"
          open={isShowEstimateReport}
          // onOk={handleOk}
          onCancel={() => EstimateReport(false)}
          width="95%"
          footer={null}
        >
          <VoyageBunkerPlanReport data={reportFormData} summary={summary} />
        </Modal>
      ) : undefined}
    </div>
  );
};

export default VoygenBunkerPlan;
