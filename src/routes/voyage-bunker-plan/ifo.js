import React, { useEffect } from 'react';
import { Table, Popconfirm, Input, Checkbox, Icon, Button } from 'antd';
import { useStateCallback } from '../../shared';


const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

// const dataSource2 = [
//     {
//         key: '1',
//         t1: "Total",
//         t2: "0.00",
//         t3: "0.00",
//         t4: "0.00",
//         t5: "0.00",
//         t6: "0.00",
//         t7: "0.00",
//         t8: "0.00",
//         t9: "0.00",
//         t10: "0.00",
//         t11: "0.00",
//         t12: "",
//     },
// ];

// const columns11 = [
//     {
//         title: 'Total',
//         dataIndex: 't1',
//         width: 100,
//         key: 't1',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't2',
//         width: 100,
//         key: 't2',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't3',
//         width: 100,
//         key: 't3',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't4',
//         width: 100,
//         key: 't4',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't5',
//         width: 100,
//         key: 't5',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't6',
//         width: 100,
//         key: 't6',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't7',
//         width: 100,
//         key: 't7',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't8',
//         width: 100,
//         key: 't8',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't9',
//         width: 100,
//         key: 't9',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't10',
//         width: 100,
//         key: 't10',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't11',
//         width: 100,
//         key: 't11',
//     },
//     {
//         title: '0.00',
//         dataIndex: 't12',
//         width: 100,
//         key: 't12',
//     },
// ];

const  IFOList = (props) => {

    const [state, setState] = useStateCallback({
        columns: [{
            title: 'Port Name',
            dataIndex: 'port',
            width: 140,
            render: (text, record) => renderColumns(text, record, 'port'),
        },
        {
            title: 'Sea Cons',
            dataIndex: 'ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'ifo'),
        },
        {
            title: 'Avg Sea Cons',
            dataIndex: 'avgseacons',
            width: 100,

            render: (text, record) => {
           
                let num = Number(record.avgVal) > 0 ? Number(record.avgVal) : 1
                return (
                    <span>{((Number(record.ifo)) / num).toFixed(2)}</span>
                )

            }

        },
        {
            title: 'ROB Arr',
            dataIndex: 'arob_ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'arob_ifo'),
        },
        {
            title: 'Port Cons',
            dataIndex: 'pc_ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'pc_ifo'),
        },
        {
            title: 'Avg Port Cons',
            dataIndex: 'avgportcons',
            width: 100,
           
            render: (text, record) => {
                let num = Number(record.tpd) > 0 ? Number(record.tpd) : 1

                return (
                    <span>{((Number(record.pc_ifo)) / num).toFixed(2)}</span>
                )
            }
        },
        // {
        //     title: 'Planed Qty',
        //     dataIndex: 'reqqty',
        //     width: 80,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} disabled />
        //         )
        //     },
        // },
        {
            title: 'Receive Qty',
            dataIndex: 'r_ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'r_ifo'),
        },
        // {
        //     title: 'Price',
        //     dataIndex: 'price',
        //     width: 80,
        //     render: (text, record) => {
        //         return (
        //             <Input defaultValue={0.00} disabled />
        //         )
        //     },
        // },
        {
            title: 'ROB Dpt',
            dataIndex: 'dr_ifo',
            width: 100,
            render: (text, record) => renderColumns(text, record, 'dr_ifo'),
        },
        // {
        //     title: 'Select',
        //     width: 100,
        //     render: (text, record) => {
        //         const { editable } = record;
        //         return (
        //             <div className="editable-row-operations">
        //                 {
        //                     <Checkbox onChange={(evt) => onChangeEvent(evt)}></Checkbox>
        //                 }
        //             </div>
        //         )
        //     }
        // },
            // {
            //     title: 'Action',
            //     dataIndex: 'action',
            //     width: 100,
            //     // fixed: 'right',
            //     render: (text, record) => {
            //         const { editable } = record;
            //         return (
            //             <div className="editable-row-operations">
            //                 {
            //                     editable ?
            //                         <span>
            //                             <span className="iconWrapper save" onClick={() => save(record.key)}><SaveOutlined /></span>
            //                             <span className="iconWrapper cancel">
            //                                 <Popconfirm title="Sure to cancel?" onConfirm={() => cancel(record.key)}>
            //                                    <DeleteOutlined /> />
            //                                 </Popconfirm>
            //                             </span>
            //                         </span>
            //                         : <span className="iconWrapper edit" onClick={() => edit(record.key)}><EditOutlined /></span>
            //                 }
            //             </div>
            //         );
            //     },
            // }
        ],
        data: []
        //  props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails : []
    })

    let cacheData = props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails.map(item => ({ ...item })) : []

    useEffect(() =>{
        getFormata()
    }, [])


    const getFormata = () => {
        let data = props && props.data && props.data.bunkerdetails ? props.data.bunkerdetails : []
        let data1 = props && props.data && props.data.portitinerary ? props.data.portitinerary : []

        let arr = [];
        // let old_port=[]
        data.length > 0 && data.map((val, ind) => {
        //   if(old_port.includes(val.port)==false) {
        //     old_port.push(val.port)
            let obj = { ...val }
            if(typeof data1[ind] === "undefined") {} else {
                obj['avgVal'] = data1.length > 0 && data1[ind].tsd&&parseFloat(data1[ind].tsd).toFixed(2)
                obj['tpd'] = data1.length > 0 &&data1[ind].hasOwnProperty('t_port_days')&& parseFloat(data1[ind].t_port_days).toFixed(2)
            }
            arr.push(obj)  
        // }
        
        })
       
        setState(prevState => ({...prevState, data: arr}))
    }

    // Start table section
    const renderColumns = (text, record, column)=> {
      
        record.avgportcons = parseFloat(record.avgportcons ? record.avgportcons : 0).toFixed(2)
        record.ifo = parseFloat(record.ifo ? record.ifo : 0.00).toFixed(2) 
        record.avgseacons = parseFloat(record.avgseacons ? record.avgseacons : 0.00).toFixed(2) 
        record.arob_ifo = parseFloat(record.arob_ifo ? record.arob_ifo : 0.00).toFixed(2) 
        record.pc_ifo = parseFloat(record.pc_ifo ? record.pc_ifo : 0.00).toFixed(2) 
        record.reqqty = parseFloat(record.reqqty ? record.reqqty : 0.00).toFixed(2) 
        record.r_ifo = parseFloat(record.r_ifo ? record.r_ifo : 0.00).toFixed(2) 
        record.price = parseFloat(record.price ? record.price : 0.00).toFixed(2) 
        record.dr_ifo = parseFloat(record.dr_ifo ? record.dr_ifo : 0.00).toFixed(2) 

        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => handleChange(value, record.key, column)}
            />
        );
    }

    // select = () => {

    // }

    const handleChange = (value, key, column) => {
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            setState(prevState => ({...prevState, data: newData}));
        }
    }

    const edit = (key) =>{
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }

    const save = (key) => {
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
            cacheData = newData.map(item => ({ ...item }));
        }
    }

    const cancel = (key) => {
        const newData = [...state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            setState(prevState => ({ ...prevState, data: newData }));
        }
    }
    const onChangeEvent = (event) => {
        
    }

        return (
            <>
                <Table
                    bordered
                    dataSource={state.data}
                    columns={state.columns}
                    scroll={{ y: 300 }}
                    size="small"
                    pagination={false}
                    rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                />

                {/* <div className="table-info-wrapper hide-table-header">
                    <Table dataSource={dataSource2} columns={columns11} bordered pagination={false} />
                </div> */}

            </>
        )
    }

export default IFOList;