import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom'
import { Table, Popconfirm, Modal } from 'antd';
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  ResizeableTitle,
  useStateCallback
} from '../../shared';
import { EditOutlined } from '@ant-design/icons';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import BunkerRequirements from '../operation/bunkers/bunker-requirements';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';


const BunkerReqSummaryList = (props) => {

  const [state, setState] = useStateCallback({
    frmName: 'bunker_requirement_form',
    voyID: props.voyID || 0,
    formData: [],
    loading: false,
    columns: [],
    responseData: [],
    pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
    isAdd: true,
    isVisible: false,
    sidebarVisible: false,
    formDataValues: {},
    showModal: false,
    voyageData: props.voyageData || {},
    listType: props.listType || 'selected',
    typesearch: {},
    donloadArray: [],



  });


  const navigate = useNavigate();

  useEffect(() => {
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, id) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => onRowClick(id)}>
              <EditOutlined />

            </span>
            {/* <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => onRowDeletedClick(id.id)}>
               <DeleteOutlined /> />
              </Popconfirm>
            </span> */}
          </div>
        )
      }
    };
    let tableHeaders = Object.assign([], FIELDS && FIELDS['bunker-requirement-list'] ? FIELDS['bunker-requirement-list']["tableheads"] : [])
    tableHeaders.push(tableAction)

    setState(prevState => ({ ...prevState, columns: tableHeaders }), () => {
      getTableData();
    })

  }, []);


  // const getTableData = async (search = {}) => {
  //   const { pageOptions,voyageData } = state;

  //   let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
  //   let headers = { order_by: { id: 'desc' } };

  //   if (
  //     search &&
  //     search.hasOwnProperty("searchValue") &&
  //     search.hasOwnProperty("searchOptions") &&
  //     search["searchOptions"] !== "" &&
  //     search["searchValue"] !== ""
  //   ) {
  //     let wc = {};
  //     search["searchValue"] = search["searchValue"].trim();

  //     if (search["searchOptions"].indexOf(";") > 0) {
  //       let so = search["searchOptions"].split(";");
  //       wc = { OR: {} };
  //       so.map((e) => (wc["OR"][e] = { l: search["searchValue"] }));
  //     } else {
  //       wc = { "OR": { [search['searchOptions']]: { "l": search['searchValue'] } } };

  //     }

  //     if (headers.hasOwnProperty("where")) {
  //       // If "where" property already exists, merge the conditions
  //       headers["where"] = { ...headers["where"], ...wc };
  //     } else {
  //       // If "where" property doesn't exist, set it to the new condition
  //       headers["where"] = wc;
  //     }

  //     state.typesearch = {
  //       searchOptions: search.searchOptions,
  //       searchValue: search.searchValue,
  //     };
  //   }


  //   setState(prev => ({
  //     ...prev,
  //     loading: true,
  //     responseData: [],
  //   }));

  //   let qParamString = objectToQueryStringFunc(qParams);

  //   let _url =`${URL_WITH_VERSION}/voyage-manager/bunker-req/list?${qParamString}`;

  //   const response = await getAPICall(_url, headers);
  //   const data = await response;

  //   const totalRows = data && data.total_rows ? data.total_rows : 0;
  //   let dataArr = data && data.data ? data.data : [];  
  //   let new_array = []

  //   if(dataArr && dataArr.length > 0){
  //     dataArr.map(e=>{
  //       if(e.voyage_manager_id == voyageData.id){
  //         new_array.push(e)
  //       }
  //     })
  //   }
  //   console.log("dataaa",dataArr);  
  //   let _state = { loading: false };

  //   dataArr.map((ele)=>{
  //     ele["recieved_qty"]=  parseFloat(ele['....'][0]?.recieved_qty).toFixed(2);
  //     ele["required_qty"]= parseFloat(ele['....'][0]?.required_qty).toFixed(2);
  //   })

  //   if (dataArr.length > 0) {
  //     _state['responseData'] = dataArr;

  //   }




  //   console.log("_state",_state);


  //   // dataArr.forEach(item => {
  //   //   item['....'].forEach(entry => {
  //   //     const receivedQty = entry.recieved_qty;
  //   //     const requiredQty = entry.required_qty;

  //   //     

  //   //   });

  //   // });



  //   console.log("state before", state);
  //   setState(prev => ({
  //     ...prev,
  //     ..._state,


  //     pageOptions: {
  //       pageIndex: pageOptions.pageIndex,
  //       pageLimit: pageOptions.pageLimit,
  //       totalRows: totalRows,
  //     },
  //     loading: false,

  //   }));
  //   console.log("state after",state);
  // };



  const getTableData = async (searchtype = {}) => {
    const { pageOptions, voyageData, listType, typesearch } = state;
    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };
    let search = searchtype && searchtype.hasOwnProperty('searchOptions') && searchtype.hasOwnProperty('searchValue') ? searchtype : typesearch;

    if (search && search.hasOwnProperty('searchValue') && search.hasOwnProperty('searchOptions') && search['searchOptions'] !== '' && search['searchValue'] !== '') {
      let wc = {};
      search['searchValue'] = search['searchValue'].trim();

      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
      setState(prevState => ({ ...prevState, typesearch: { 'searchOptions': search.searchOptions, 'searchValue': search.searchValue } }));
    }

    setState(prev => ({
      ...prev,
      loading: true,
      responseData: [],
    }));


    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;

    let totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr1 = data && data.data ? data.data : [];
    let new_array = []
    if (dataArr1 && dataArr1.length > 0) {
      dataArr1.map(e => {
        if (e.voyage_manager_id == voyageData.id) {
          new_array.push(e)
        }
      })
    }
    let dataArr = listType == 'all' ? dataArr1 : new_array;
    if (dataArr && dataArr.length > 0) {
      // dataArr.map(e => {
      //   let received_qty = 0;
      //   let required_qty = 0;
      //   e['....'] && e['....'].length > 0 && e['....'].map(val => {
      //     received_qty += ele["recieved_qty"]=  parseFloat(ele['....'][0]?.recieved_qty).toFixed(2);
      //     required_qty += parseFloat(ele['....'][0]?.required_qty).toFixed(2);
      //   })
      //   e['required_qty'] = required_qty;
      //   e['received_qty'] = received_qty;
      // })


      dataArr.map((ele) => {
        ele["recieved_qty"] = parseFloat(ele['....'][0]?.recieved_qty).toFixed(2);
        ele["required_qty"] = parseFloat(ele['....'][0]?.required_qty).toFixed(2);
      })


    }
    let newState = { loading: false };
    let donloadArr = []
    if (dataArr.length > 0 && totalRows > state.responseData.length) {
      dataArr.forEach(d => donloadArr.push(d["id"]))
      newState['responseData'] = dataArr;
    }
    setState(prevState => ({
      ...prevState,
      ...newState,
      donloadArray: donloadArr,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    }));
  };






  const onCancel = () => {
    setState(
      (prevState) => ({
        ...prevState,
        isAdd: true,
        isVisible: false,
      }),
      () => {
        getTableData();
      }
    );
  };



  const onRowClick = async record => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/edit?e=${record['id']}`;
    const response = await getAPICall(_url);
    const respData = await response['data'];
    setState(prevState => ({ ...prevState, formDataValues: respData, showModal: true }))

  };

  /*
  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/voyage-manager/bunker-req/delete`;
    apiDeleteCall(_url, { "id": id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message)
      }
    })
  }

  */

  const callOptions = (evt) => {
    let _search = {
      searchOptions: evt["searchOptions"],
      searchValue: evt["searchValue"],
    };
    if (
      evt.hasOwnProperty("searchOptions") &&
      evt.hasOwnProperty("searchValue")
    ) {
      let pageOptions = state.pageOptions;

      pageOptions["pageIndex"] = 1;
      setState(
        (prevState) => ({
          ...prevState,
          search: _search,
          pageOptions: pageOptions,
        }),
        () => {
          getTableData(_search);
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "reset-serach"
    ) {
      let pageOptions = state.pageOptions;
      pageOptions["pageIndex"] = 1;

      setState(
        (prevState) => ({ ...prevState, search: {}, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    } else if (
      evt &&
      evt.hasOwnProperty("actionName") &&
      evt["actionName"] === "column-filter"
    ) {
      // column filtering show/hide
      let responseData = state.responseData;
      let columns = Object.assign([], state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            (item) =>
              (item.hasOwnProperty("dataIndex") && item.dataIndex === k) ||
              (item.hasOwnProperty("key") && item.key === k)
          );
          if (!index) {
            let title = k
              .split("_")
              .map((snip) => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(" ");
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: "true",
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }

      setState((prevState) => ({
        ...prevState,
        sidebarVisible: evt.hasOwnProperty("sidebarVisible")
          ? evt.sidebarVisible
          : !prevState.sidebarVisible,
        columns: evt.hasOwnProperty("columns") ? evt.columns : columns,
      }));
    } else {
      let pageOptions = state.pageOptions;
      pageOptions[evt["actionName"]] = evt["actionVal"];

      if (evt["actionName"] === "pageLimit") {
        pageOptions["pageIndex"] = 1;
      }

      setState(
        (prevState) => ({ ...prevState, pageOptions: pageOptions }),
        () => {
          getTableData();
        }
      );
    }
  };


  //const hideShowModal = visible => setState({ ...state, showModal: visible }, ()=> getTableData());

  const hideShowModal = visible => setState(prevState => ({ ...prevState, showModal: visible }), () => {
    getTableData();
  })

  //resizing function
  const handleResize =
    (index) =>
      (e, { size }) => {
        setState(({ columns }) => {
          const nextColumns = [...columns];
          nextColumns[index] = {
            ...nextColumns[index],
            width: size.width,
          };
          return { columns: nextColumns };
        });
      };

  const components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  const onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`,
      cols = [];
    const { columns, pageOptions, donloadArray } = state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    columns.map((e) =>
      e.invisible === "false" && e.key !== "action"
        ? cols.push(e.dataIndex)
        : false
    );
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }
    const filter = donloadArray.join();
    // window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}&ids=${filter}`, '_blank');
    window.open(
      `${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}&l=${qParams.l}&ids=${filter}`,
      "_blank"
    );
  };


  const tableColumns = state.columns
    .filter(col => (col && col.invisible !== 'true' ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }));

  return (
    <div className="body-wrapper">
      <article className="article">
        <div className="box box-default">
          <div className="box-body">
            <div
              className="section"
              style={{
                width: '100%',
                marginBottom: '10px',
                paddingLeft: '15px',
                paddingRight: '15px',
              }}
            >
              {state.loading === false ? (
                <ToolbarUI
                  routeUrl={'bunker-req-summery-list-toolbar'}
                  optionValue={{ pageOptions: state.pageOptions, columns: state.columns, search: state.search }}
                  callback={e => callOptions(e)}
                  dowloadOptions={[
                    { title: 'CSV', event: () => onActionDonwload('csv', 'bunreq') },
                    { title: 'PDF', event: () => onActionDonwload('pdf', 'bunreq') },
                    { title: 'XLS', event: () => onActionDonwload('xlsx', 'bunreq') }
                  ]}
                />
              ) : (
                undefined
              )}
            </div>
            <div>
              <Table
                className="inlineTable editableFixedHeader resizeableTable"
                bordered
                components={components}
                scroll={{ x: 'max-content' }}
                columns={tableColumns}
                // size="small"
                dataSource={state.responseData}
                loading={state.loading}
                pagination={false}
                rowClassName={(r, i) =>
                  i % 2 === 0
                    ? 'table-striped-listing'
                    : 'dull-color table-striped-listing'
                }
              />
            </div>
          </div>
        </div>
      </article>
      {state.showModal ? (
        <Modal
          className="page-container"
          style={{ top: '2%' }}
          title={`${state.formDataValues.id ? 'Edit' : 'Create'} - Bunker Requirement `}
          open={state.showModal}
          onCancel={() => hideShowModal(false)}
          width="90%"
          footer={null}
        >
          <BunkerRequirements formData={state.formDataValues} portData={props.formData} />
        </Modal>
      ) : (
        undefined
      )}

      {
        state.sidebarVisible ? <SidebarColumnFilter columns={state.columns} sidebarVisible={state.sidebarVisible} callback={(e) => callOptions(e)} /> : null
      }
    </div>
  );
}
export default BunkerReqSummaryList;