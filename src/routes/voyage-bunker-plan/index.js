import { Button, Dropdown, Select } from "antd";
import React, { useEffect, useState } from "react";
import AverageBunkerPlan from "./AverageBunkerPlan";
import LifoBunkerPlan from "./LifoBunkerPlan";
import FifoBunkerPlan from "./FifoBunkerPlan";

const VoygenBunkerPlan = (props) => {
  const items = [
    {
      value: "1",
      label: "Average Bunker Plan",
    },
    {
      value: "2",
      label: "LIFO Bunker Plan",
    },
    {
      value: "3",
      label: "FIFO Bunker Plan",
    },
  ];

  const [showplan, setShowPlan] = useState({
    isShowAveragePlan: false,
    isShowLifoPlan: false,
    isShowFifoPlan: false,
  });

  const [initQty, setInitQty] = useState({
    init_qty_ifo: 0,
    init_qty_mgo: 0,
    init_qty_lsmgo: 0,
    init_qty_vlsfo: 0,
    init_qty_ulsfo: 0,
  });

  const [endQty, setEndQty] = useState({
    end_qty_ifo: 0,
    end_qty_mgo: 0,
    end_qty_lsmgo: 0,
    end_qty_vlsfo: 0,
    end_qty_ulsfo: 0,
  });

  const [seaCons, setSeaCons] = useState({
    sea_cons_ifo: 0,
    sea_cons_mgo: 0,
    sea_cons_lsmgo: 0,
    sea_cons_vlsfo: 0,
    sea_cons_ulsfo: 0,
  });

  const [portCons, setPortCons] = useState({
    port_cons_ifo: 0,
    port_cons_mgo: 0,
    port_cons_lsmgo: 0,
    port_cons_vlsfo: 0,
    port_cons_ulsfo: 0,
  });

  const [recQty, setRecQty] = useState({
    rec_qty_ifo: 0,
    rec_qty_mgo: 0,
    rec_qty_lsmgo: 0,
    rec_qty_vlsfo: 0,
    rec_qty_ulsfo: 0,
  });

  const handleChange = (value) => {
    if (value === "1") {
      setShowPlan({
        isShowAveragePlan: true,
        isShowLifoPlan: false,
        isShowFifoPlan: false,
      });
    } else if (value === "2") {
      setShowPlan({
        isShowAveragePlan: false,
        isShowLifoPlan: true,
        isShowFifoPlan: false,
      });
    } else if (value === "3") {
      setShowPlan({
        isShowAveragePlan: false,
        isShowLifoPlan: false,
        isShowFifoPlan: true,
      });
    }
  };

  const CalculateBunkerSum = (bunkerarr = []) => {
    let _ifoarob = 0,
      _lsmgoarob = 0,
      _vlsfoarob = 0,
      _ulsfoarob = 0,
      _mgoarob = 0;
    let _ifosea = 0,
      _lsmgosea = 0,
      _vlsfosea = 0,
      _ulsfosea = 0,
      _mgosea = 0;
    let _ifoport = 0,
      _lsmgoport = 0,
      _vlsfoport = 0,
      _ulsfoport = 0,
      _mgoport = 0;
    let _iforec = 0,
      _lsmgorec = 0,
      _vlsforec = 0,
      _ulsforec = 0,
      _mgorec = 0;

    let _endqtyifo = 0,
      _endqtylsmgo = 0,
      _endqtymgo = 0,
      _endqtyvlsfo = 0,
      _endqtyulsfo = 0;

    bunkerarr.length > 0 &&
      bunkerarr?.map((el, index) => {
        
        _ifoarob += Number(el?.arob_ifo) ? parseFloat(el.arob_ifo) : 0;
        _lsmgoarob += Number(el?.arob_lsmgo) ? parseFloat(el.arob_lsmgo) : 0;
        _vlsfoarob += Number(el?.arob_vlsfo) ? parseFloat(el.arob_vlsfo) : 0;
        _ulsfoarob += Number(el?.arob_ulsfo) ? parseFloat(el.arob_ulsfo) : 0;
        _mgoarob += Number(el?.arob_mgo) ? parseFloat(el.arob_mgo) : 0;

        // sea consumption
        _ifosea += Number(el.ifo) ? parseFloat(el.ifo) : 0;
        _lsmgosea += Number(el.lsmgo) ? parseFloat(el.lsmgo) : 0;
        _vlsfosea += Number(el.vlsfo) ? parseFloat(el.vlsfo) : 0;
        _ulsfosea += Number(el.ulsfo) ? parseFloat(el.ulsfo) : 0;
        _mgosea += Number(el.mgo) ? parseFloat(el.mgo) : 0;

        // port Consumption
        _ifoport += Number(el.pc_ifo) ? parseFloat(el.pc_ifo) : 0;
        _lsmgoport += Number(el.pc_lsmgo) ? parseFloat(el.pc_lsmgo) : 0;
        _vlsfoport += Number(el.pc_vlsfo) ? parseFloat(el.pc_vlsfo) : 0;
        _ulsfoport += Number(el.pc_ulsfo) ? parseFloat(el.pc_ulsfo) : 0;
        _mgoport += Number(el.pc_mgo) ? parseFloat(el.pc_mgo) : 0;

        // rec qty

        _iforec += Number(el.r_ifo) ? parseFloat(el.r_ifo) : 0;  //50
        _lsmgorec += Number(el.r_lsmgo) ? parseFloat(el.r_lsmgo) : 0;
        _vlsforec += Number(el.r_vlsfo) ? parseFloat(el.r_vlsfo) : 0;
        _ulsforec += Number(el.r_ulsfo) ? parseFloat(el.r_ulsfo) : 0;
        _mgorec += Number(el.r_mgo) ? parseFloat(el.r_mgo) : 0;

        // end qty

        _endqtyifo +=
          parseFloat(el.arob_ifo) +
          parseFloat(el.r_ifo) -
          (parseFloat(el.pc_ifo) + parseFloat(el.ifo));

        _endqtylsmgo +=
          parseFloat(el.arob_lsmgo) +
          parseFloat(el.r_lsmgo) -
          (parseFloat(el.pc_lsmgo) + parseFloat(el.lsmgo));

        _endqtymgo +=
          parseFloat(el.arob_mgo) +
          parseFloat(el.r_mgo) -
          (parseFloat(el.pc_mgo) + parseFloat(el.mgo));

        _endqtyvlsfo +=
          parseFloat(el.arob_vlsfo) +
          parseFloat(el.r_vlsfo) -
          (parseFloat(el.pc_vlsfo) + parseFloat(el.vlsfo));

        _endqtyulsfo +=
          parseFloat(el.arob_ulsfo) +
          parseFloat(el.r_ulsfo) -
          (parseFloat(el.pc_ulsfo) + parseFloat(el.ulsfo));
      });

    return {
      _ifoarob,
      _lsmgoarob,
      _vlsfoarob,
      _ulsfoarob,
      _mgoarob,
      _ifosea,
      _lsmgosea,
      _vlsfosea,
      _ulsfosea,
      _mgosea,
      _ifoport,
      _lsmgoport,
      _vlsfoport,
      _ulsfoport,
      _mgoport,
      _iforec,
      _lsmgorec,
      _vlsforec,
      _ulsforec,
      _mgorec,

      _endqtyifo,
      _endqtylsmgo,
      _endqtymgo,
      _endqtyvlsfo,
      _endqtyulsfo,
    };
  };

  useEffect(() => {
    setShowPlan({ ...showplan, isShowAveragePlan: true });
  }, []);

  useEffect(() => {
    const sumvalues = CalculateBunkerSum(props.data);

    setInitQty({
      init_qty_ifo: sumvalues._ifoarob,
      init_qty_mgo: sumvalues._mgoarob,
      init_qty_lsmgo: sumvalues._lsmgoarob,
      init_qty_vlsfo: sumvalues._vlsfoarob,
      init_qty_ulsfo: sumvalues._ulsfoarob,
    });

    setEndQty({
      end_qty_ifo: sumvalues._endqtyifo,
      end_qty_mgo: sumvalues._endqtymgo,
      end_qty_lsmgo: sumvalues._endqtylsmgo,
      end_qty_vlsfo: sumvalues._endqtyvlsfo,
      end_qty_ulsfo: sumvalues._endqtyulsfo,
    });

    setSeaCons({
      sea_cons_ifo: sumvalues._ifosea,
      sea_cons_mgo: sumvalues._mgosea,
      sea_cons_lsmgo: sumvalues._lsmgosea,
      sea_cons_vlsfo: sumvalues._vlsfosea,
      sea_cons_ulsfo: sumvalues._ulsfosea,
    });

    setPortCons({
      port_cons_ifo: sumvalues._ifoport,
      port_cons_mgo: sumvalues._mgoport,
      port_cons_lsmgo: sumvalues._lsmgoport,
      port_cons_vlsfo: sumvalues._vlsfoport,
      port_cons_ulsfo: sumvalues._ulsfoport,
    });

    setRecQty({
      rec_qty_ifo: sumvalues._iforec,
      rec_qty_mgo: sumvalues._mgorec,
      rec_qty_lsmgo: sumvalues._lsmgorec,
      rec_qty_vlsfo: sumvalues._vlsforec,
      rec_qty_ulsfo: sumvalues._ulsforec,
    });
  }, []);

  const handleImport = (data, key) => {
    if (key === "1") {
      setShowPlan({ ...showplan, isShowAveragePlan: false });
      props.updatecp(data);
    } else if (key === "2") {
      setShowPlan({ ...showplan, isShowLifoPlan: false });
      props.updatecp(data);
    } else if (key === "3") {
      setShowPlan({ ...showplan, isShowFifoPlan: false });
      props.updatecp(data);
    }
  };

  return (
    <div className="body-wrapper">
      <article className="article toolbaruiWrapper">
        <div className="box box-default">
          <div className="box-body">
            <div className="toolbar-ui-wrapper">
              <div className="leftsection">
                <span key="first" className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    {/* <li>
                      <span className="text-bt">
                        <SaveOutlined /> Save
                      </span>
                    </li> */}
                  </ul>
                </span>
              </div>
              <div className="rightsection">
                <span className="wrap-bar-menu">
                  <ul className="wrap-bar-ul">
                    <li>
                      <Select
                        options={items}
                        defaultValue="Average Bunker Plan"
                        onChange={handleChange}
                      />
                    </li>
                    <li>
                      {/* <span
                        className="text-bt"
                        // onClick={() => EstimateReport(true)}
                      >
                        Report
                      </span> */}
                    </li>
                  </ul>
                </span>
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className="article">
        {/* <div className="box box-default">
          <div className="box-body"> */}
        <div>
          <div>
            {showplan.isShowAveragePlan && (
              <AverageBunkerPlan
                initqty={initQty}
                seacons={seaCons}
                portcons={portCons}
                recqty={recQty}
                endqty={endQty}
                modalCloseEvent={(data) => handleImport(data, "1")}
              />
            )}
            {showplan.isShowFifoPlan && (
              <FifoBunkerPlan
                initqty={initQty}
                seacons={seaCons}
                portcons={portCons}
                recqty={recQty}
                endqty={endQty}
                modalCloseEvent={(data) => handleImport(data, "2")}
              />
            )}
            {showplan.isShowLifoPlan && (
              <LifoBunkerPlan
                initqty={initQty}
                seacons={seaCons}
                portcons={portCons}
                recqty={recQty}
                endqty={endQty}
                modalCloseEvent={(data) => handleImport(data, "3")}
              />
            )}
          </div>
        </div>
      </article>
    </div>
  );
};

export default VoygenBunkerPlan;
