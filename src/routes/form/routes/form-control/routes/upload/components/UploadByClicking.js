import React from 'react';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

const Box = () => {
  return(
    <div className="box box-default">
      <div className="box-header">Upload by clicking</div>
      <div className="box-body">
        <Upload {...props}>
          <Button>
        <UploadOutlined /> Click to Upload
          </Button>
        </Upload>
      </div>
    </div>
  )
}

export default Box;