import React from 'react';
import { Steps} from 'antd';
import { UserOutlined,SolutionOutlined,CreditCardOutlined,SmileOutlined } from '@ant-design/icons';
const Step = Steps.Step;

const Box = () => {
  return(
    <div className="box box-default">
      <div className="box-header">With icon</div>
      <div className="box-body">
        <Steps>
          <Step status="finish" title="Login" icon={<UserOutlined />} />
          <Step status="finish" title="Verification" icon={<SolutionOutlined />} />
          <Step status="process" title="Pay" icon={<CreditCardOutlined />} />
          <Step status="wait" title="Done" icon={<SmileOutlined />} />
        </Steps>
      </div>
    </div>
  )
}

export default Box;