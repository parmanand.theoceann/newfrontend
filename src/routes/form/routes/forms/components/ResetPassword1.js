import React from 'react';
import { Form,  Input, Button, Spin , Alert} from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import { useLocation,useNavigate,useParams } from 'react-router-dom';
import URL_WITH_VERSION, { postAPICall, openNotificationWithIcon,IMAGE_PATH} from '../../../../../shared';
const FormItem = Form.Item;

class NormalForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      tokenVisible : false,
      passwordVisible : false,
      buttonTxt : 'Send Reset Instructions',
      active: true
    }
  }

  errorMessage = (dataMessage) => {
    this.setState({ active: true })
    let msg = "<div className='row'>";
    if (typeof dataMessage !== "string") {
        Object.keys(dataMessage).map(i => msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>");
      } else {
          msg += dataMessage
      }

    msg += "</div>"
    openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
  }

  handleSubmit = e => {
    const { tokenVisible, passwordVisible } = this.state;
    let _method = "post";
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ active : false })
        if(tokenVisible  && passwordVisible) {
          let postData = { user_email : values['resetpassword1-email'], auth_token : values['token'], newpassword : values['password'] }
          postAPICall(`${URL_WITH_VERSION}/user/reset`, postData, _method, (data) => {
            if (data.data) {
                openNotificationWithIcon('success', data.message);
             this.props.history.push("/user/login");
              } else {
                this.errorMessage(data.message)
              }
          });
        } else {
          let postData = { "user_email" : values['resetpassword1-email'] }
          postAPICall(`${URL_WITH_VERSION}/user/reset`, postData, _method, (data) => {
             if (data.data) {
                openNotificationWithIcon('success', data.message);
                this.setState({ buttonTxt :'Update Password', tokenVisible : true, passwordVisible : true})
               } else {
                this.errorMessage(data.message)
              }
          });
        }
        this.setState({active:true})
      }
    });
  };

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { tokenVisible, passwordVisible, buttonTxt, active } = this.state;
    return (
      <section className="form-v1-container">
        <div className="signin-logo text-center">
          <div className="middel-text">
            {/* <img src="assets/logo2.png" alt="logo" height="50" /> */}
            <img src={IMAGE_PATH+"theoceann.png"} height="50" />
            <span>Theoceann</span>
          </div>
        </div>
        <h2>Forgot Password?</h2>
        <p className="additional-info col-lg-10 mx-lg-auto mb-3">
          Enter the email address you used when you joined and we’ll send you instructions to reset
          your password.
        </p>
        <Form onSubmit={this.handleSubmit} className="form-v1">
          <FormItem className="mb-3">
            {getFieldDecorator('resetpassword1-email', {
              rules: [
                { type: 'email', message: 'The input is not valid E-mail!' },
                { required: true, message: 'Please input your email!' },
              ],
            })(
              <Input
                size="large"
                prefix={<MailOutlined style={{ fontSize: 13 }} />}
                placeholder="Email"
              />
            )}
          </FormItem>
          {tokenVisible && 
          <FormItem className="mb-3">
            {getFieldDecorator('token', {
              rules: [{ required: true, message: 'Please input your token!' }],
            })(
              <Input
                size="large"
                prefix={<LockOutlined  style={{ fontSize: 13 }} />}
                placeholder="Token"
              />
            )}
          </FormItem>
        }
          {passwordVisible &&
          <FormItem className="mb-3" >
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: 'Please input your password!',
              }, {
                validator: this.checkConfirm,
              }],
            })(
              <Input
                size="large"
                type="password"
                prefix={<LockOutlined  style={{ fontSize: 13 }} />}
                placeholder="password"
              />
            )}
          </FormItem>
          }
          {passwordVisible &&
          <FormItem className="mb-3"  >
            {getFieldDecorator('confirm', {
              rules: [{
                required: true, message: 'Please confirm your password!',
              }, {
                validator: this.checkPassword,
              }],
            })(
              <Input 
                 type="password" 
                 onBlur={this.handleConfirmBlur} 
                 prefix={<LockOutlined  style={{ fontSize: 13 }} />}
                 placeholder="confirm password"
                 />
            )}
          </FormItem>
         }
          <FormItem>
            {active ?
                <Button type="primary" htmlType="submit" className="btn-cta btn-block">
                  {buttonTxt}
                </Button>
              :
              <Spin tip="Loading...">
                <Alert
                  message=" "
                  description="Please wait..."
                  type="info"
                />
            </Spin>
             
            }
          </FormItem>
        </Form>
      </section>
    );
  }
}


const withLocation = (Component) => (props) => {
  let location = useLocation();
  let navigate = useNavigate();
  let params = useParams();

  return <Component {...props} router={{ location, navigate, params }} />;
};


// const WrappedNormalForm = Form.create()(withLocation(NormalForm));
const WrappedNormalForm = NormalForm
export default WrappedNormalForm;
