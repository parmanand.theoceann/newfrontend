// import React, { useState, useEffect } from "react";
// import { Form, Input, Button, Checkbox, Modal } from "antd";
// import { LockOutlined, UserOutlined } from "@ant-design/icons";
// import { useNavigate } from "react-router-dom";
// import APPCONFIG from "../../../../../constants/appConfig";
// import DEMO from "../../../../../constants/demoData";
// import URL_WITH_VERSION, {
//   postAPICall,
//   openNotificationWithIcon,
// } from "../../../../../shared";
// import Cookies from "universal-cookie";
// import { encrypt, decrypt } from "./crpyto";

// const cookies = new Cookies();

// const NormalLoginForm = () => {
//   const [visibleviewmodal, setVisibleViewModal] = useState(false);
//   const [isloggedin, setIsloggedIn] = useState(false);
//   const [form] = Form.useForm();
//   const navigate = useNavigate();

//   useEffect(() => {
//     if (isloggedin) {
//       setVisibleViewModal(false);
//       navigate("/chartering-dashboard", {replace:true});
//     }
//   }, [isloggedin]);

  // const handleSubmit = () => {
  //   form
  //     .validateFields()
  //     .then((values) => {
  //       let _method = "post";

  //       let postData = {
  //         username: values.username,
  //         encpass: btoa(values.password),
  //       };

  //       postAPICall(
  //         `${URL_WITH_VERSION}/user/login`,
  //         postData,
  //         _method,
  //         (data) => {
  //           if (data.data.status === 1) {
  //             if (values["login2-remember"] === true) {
  //               let username = values["username"];
  //               let d = new Date();
  //               d.setTime(d.getTime() + 3600 * 10 * 1000);
  //               cookies.set("username", `${username}`, {
  //                 path: "/",
  //                 expires: d,
  //               });
  //               cookies.set("user_type", data.data.user_type, {
  //                 path: "/",
  //                 expires: d,
  //               });
  //               cookies.set("user_id", data.data.user_id, {
  //                 path: "/",
  //                 expires: d,
  //               });
  //               cookies.set("encpass", `${postData.encpass}`, {
  //                 path: "/",
  //                 expires: d,
  //               });
  //               setVisibleViewModal(true);
  //               setIsloggedIn(true);
  //             } else {
  //               cookies.remove("username");
  //               cookies.remove("user_type");
  //               cookies.remove("is_broker");
  //               cookies.remove("encpass");
  //             }
  //             openNotificationWithIcon("success", data.message);

  //           } else {
  //             let dataMessage = data.message;
  //             let msg = "<div className='row'>";

  //             if (typeof dataMessage !== "string") {
  //               Object.keys(dataMessage).map(
  //                 (i) =>
  //                   (msg +=
  //                     "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
  //               );
  //             } else {
  //               msg += dataMessage;
  //             }

  //             msg += "</div>";
  //             openNotificationWithIcon(
  //               "error",
  //               <div dangerouslySetInnerHTML={{ __html: msg }} />
  //             );
  //           }
  //         }
  //       );
  //     })
  //     .catch((errorInfo) => {
  //       console.log("Failed:", errorInfo);
  //     });
  // };

//   return (
//     <section className="form-v1-container">
//       <div className="signin-logo text-center">
//         <div className="middel-text">
//           <img
//             // src="https://theoceann.com/image/logo-toc-2.png"
//             src="https://theoceann.com/static/media/oceann_logo.2cd67ad80e15d78fedd44f3ce40143d4.svg"
//             height="100"
//             alt="TheOceann"
//           />
//           {/* <span>TheOceann</span> */}
//         </div>
//       </div>
//       <p className="lead">
//         Welcome back, sign in with your {APPCONFIG.brand} account
//       </p>

//       <Form form={form} onFinish={handleSubmit} className="form-v1">
//         <Form.Item
//           name="username"
//           rules={[{ required: true, message: "Please input your username!" }]}
//         >
//           <Input
//             size="large"
//             prefix={<UserOutlined style={{ fontSize: 13 }} />}
//             placeholder="Username"
//           />
//         </Form.Item>
//         <Form.Item
//           name="password"
//           rules={[{ required: true, message: "Please input your Password!" }]}
//         >
//           <Input.Password
//             size="large"
//             prefix={<LockOutlined style={{ fontSize: 13 }} />}
//             type="password"
//             placeholder="Password"
//             autoComplete="off"
//           />
//         </Form.Item>
//         <Form.Item
//           className="form-v1__remember"
//           name="login2-remember"
//           valuePropName="checked"
//           initialValue={true}
//         >
//           <Checkbox disabled>Remember me</Checkbox>
//         </Form.Item>
//         <Form.Item>
//           <Button
//             type="primary"
//             htmlType="submit"
//             className="btn-cta btn-block"
//           >
//             Log in
//           </Button>
//         </Form.Item>
//       </Form>

//       <p className="additional-info">
//         Forgot your username or password?{" "}
//         <a href={DEMO.forgotPassword}>Reset password</a>
//       </p>

//       {visibleviewmodal && (
//         <Modal
//           style={{ top: "30%" }}
//           title={"Help Guide"}
//           open={true}
//           onCancel={() => setVisibleViewModal(false)}
//           width="50%"
//           footer={null}
//         >
//           <div className="front">
//             <div className="front_head">
//               Dear User,
//               <p>
//                 Pls follow below help link to understand chartering operation
//                 and finance workflow .
//               </p>
//             </div>
//             <div className="sub_front">
//               Help Link &nbsp; <i className="fas fa-arrow-right"></i> &nbsp;
//               <a href="https://www.youtube.com/@theoceann21">
//                 {" "}
//                 Help Guide Link
//               </a>
//             </div>
//             <div className="sub_front">
//               Chartering &nbsp;<i className="fas fa-arrow-right"></i> &nbsp;{" "}
//               <a href="https://www.youtube.com/playlist?list=PLEVqABmgiEh9nKM0VLJCSnVPNe0keTGDf">
//                 {" "}
//                 Chartering{" "}
//               </a>
//             </div>
//             <div className="sub_front">
//               Operation &nbsp;<i className="fas fa-arrow-right"></i> &nbsp;
//               <a href="https://www.youtube.com/playlist?list=PLEVqABmgiEh-sVj-p7qbM74rS4iS3bhya">
//                 {" "}
//                 Operation{" "}
//               </a>
//             </div>
//             <div className="sub_front">
//               Intelligence Tools &nbsp;<i className="fas fa-arrow-right"></i>{" "}
//               &nbsp;{" "}
//               <a href="https://www.youtube.com/playlist?list=PLEVqABmgiEh_y3KaJ44GaypCHoe8E1ISG">
//                 {" "}
//                 Intelligence tools
//               </a>
//             </div>
//             <div>&nbsp;</div>
//           </div>
//         </Modal>
//       )}
//     </section>
//   );
// };

// export default NormalLoginForm;

import React, { useState, useEffect } from "react";
import { Form, Input, Button, Checkbox, notification } from "antd";
import { useNavigate, Link } from "react-router-dom";
import APPCONFIG from "../../../../../constants/appConfig";
import DEMO from "../../../../../constants/demoData";
import URL_WITH_VERSION, { openNotificationWithIcon, postAPICall, postAPICallService } from "../../../../../shared";
import Cookies from "universal-cookie";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";

const cookies = new Cookies();
const { Item } = Form;


const NormalLoginForm = () => {
  const [isloggedin, setIsloggedIn] = useState(false);
  const [form] = Form.useForm(); // Using the newer hook approach
  const navigate = useNavigate();

  useEffect(() => {
    try{
    const url = window.location.href;
    if (url.includes("token")) {
      const token = url.split("?")[1].split("=")[1];
      localStorage.setItem("oceanToken", token);
      let userData =JSON.parse(atob(token.split(".")[1]))  ;
      if(userData.role!=='admin'){
        fetchPermissionList()
      }
      else{
        navigate("/chartering-dashboard", { replace: true });
      }
    }
  }
  catch(error){
    openNotificationWithIcon('error',error)
  }

  }, []);

  const handleSubmit = (values) => {
    let _method = "post";

    let postData = {
      email: values.username,
      password: values.password,
    };

    // const url = 'https://apiservices.theoceann.com/client/login'
    const url=process.env.REACT_APP_Login
    // console.log(URL_WITH_VERSION,"---");
    // const url=`${URL_WITH_VERSION}/user/login`
    postAPICall(url, postData, _method, (data) => {
      if (data.token) {
        localStorage.setItem("oceanToken", data.token);
        let userData =JSON.parse(atob(data.token.split(".")[1]))  ;

        // console.log(userData,"dsfdsfdsfds");
        if(userData.role!=='admin'){
          fetchPermissionList()
        }
        else{
          openNotificationWithIcon("success", data.msg);
          navigate("/chartering-dashboard", { replace: true });
        }
      }
      else{
        openNotificationWithIcon('error',data.msg)
      }
      if (data.data.status === 1) {
        if (values["login2-remember"] === true) {
          let username = values["username"];
          let d = new Date();
          d.setTime(d.getTime() + 3600 * 10 * 1000);
          cookies.set("username", `${username}`, {
            path: "/",
            expires: d,
          });
          cookies.set("user_type", data.data.user_type, {
            path: "/",
            expires: d,
          });
          cookies.set("user_id", data.data.user_id, {
            path: "/",
            expires: d,
          });
          cookies.set("encpass", `${postData.encpass}`, {
            path: "/",
            expires: d,
          });
          setIsloggedIn(true);
        } else {
          cookies.remove("username");
          cookies.remove("user_type");
          cookies.remove("is_broker");
          cookies.remove("encpass");
        }
        openNotificationWithIcon("success", data.message);
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== "string") {
          Object.keys(dataMessage).map(
            (i) =>
              (msg += "<div className='col-sm-12'>" + dataMessage[i] + "</div>")
          );
        } else {
          msg += dataMessage;
        }

        msg += "</div>";
        openNotificationWithIcon(
          "error",
          <div dangerouslySetInnerHTML={{ __html: msg }} />
        );
      }
    });
  };

  const fetchPermissionList=async ()=>{
    const url='https://apiservices.theoceann.com/client/access-controll'
    const response=await postAPICallService({url,payload:{}})
    // console.log(response,"kkk");
    localStorage.setItem('permissionList',JSON.stringify(response.accessControl))
    openNotificationWithIcon("success", response.msg);
    navigate("/chartering-dashboard", { replace: true });
  }

  // const openNotificationWithIcon = (type, message) => {
  //   notification[type]({
  //     message: "Success",
  //     description: message,
  //   });
  // };

  const loginFunc = async (e) => {

    console.log("user Creds Arw",e)
    let obj = e
    let cred = e.credential
    let data  = cred.split(".")
    let userData  = data[1]


    let decode_cred = atob(userData);
    decode_cred = JSON.parse(decode_cred)
    console.log(decode_cred);


    obj.email = decode_cred.email
    obj.metaData  =  decode_cred
    console.log("Authrized user Data IS ",obj)

    const resp = await fetch('https://apiservices.theoceann.com/client/googleLogin',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(obj)
    })
    let res  = await resp.json()
    console.log(res)
    if ("token" in res ){
      res = res.token

    }
    else{
      openNotificationWithIcon('error',res.error)
      return
    }


    if (res.token) {
      localStorage.setItem("oceanToken", res.token);
      let userData =JSON.parse(atob(res.token.split(".")[1]))  ;

      // console.log(userData,"dsfdsfdsfds");
      if(userData.role!=='admin'){
        fetchPermissionList()
      }
      else{
        openNotificationWithIcon("success", res.msg);
        navigate("/chartering-dashboard", { replace: true });
      }
    }
   
  }

  return (
    <section className="form-v1-container">
      <div>
        <div className="middel-text">
          <img
            src={`${process.env.REACT_APP_IMAGE_PATH}theoceannlogo.svg`}
            width={180}
            alt="img"
          />
        </div>
      </div>

      <div>
        <p className="lead">Welcome back</p>
        <p className="lead-text">Sign in with your {APPCONFIG.brand} account</p>
      </div>

      <Form form={form} onFinish={handleSubmit} className="form-v1">
        <Item
          name="username"
          label={
            <span style={{ color: "white", fontWeight: "bold"}}>Username</span>
          }
          rules={[{ required: true, message: "Please enter your username!" }]}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Input
            size="large"
            prefix={
              <UserOutlined
                style={{ fontSize: 13 }}
                className="anticon anticon-user "
              />
            }
            placeholder="Enter Your Username"
            className="sign-form-input"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          />
        </Item>

        <Item
          name="password"
          label={
            <span style={{ color: "white", fontWeight: "bold" }}>Password</span>
          }
          rules={[{ required: true, message: "Please enter your Password!" }]}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Input.Password
            size="large"
            prefix={
              <LockOutlined
                style={{ fontSize: 13 }}
                className="anticon anticon-lock"
              />
            }
            placeholder="Enter Password"
            autoComplete="off"
          />
        </Item>

        {/* <Item
          name="login2-remember"
          valuePropName="checked"
          initialValue={true}
          className="form-v1__remember"
        >
          <Checkbox
            style={{ color: "white" }}
            disabled
            className="sign-in-checkbox"
          >
            <span className="lead-text">Remember me</span>
          </Checkbox>
        </Item> */}

        <Item>
          <Button
            htmlType="submit"
            style={{ backgroundColor: "#003e78", marginBlock: '7px' }}
            className="btn-cta btn-block"
          >
            Sign in
          </Button>
        </Item>
      </Form>
      <span style={{color: 'white', fontSize: '12px'}}>Login with Google</span>
      <div className="googleLogin-Box">
        <GoogleLogin 
        theme="filled_blue"
        size="large"
        shape="circle"
        width="280px"
        onSuccess={loginFunc}
        onFailure={(e) => console.log("Failed to log in", e)}
        />
      </div>


      {/* <p className="additional-info info-text">
        Forgot your username or password?{" "}
        DEMO.forgotPassword replaced with # as there is no page for forget Password
        <Link href={'#'}> 
          <span className="additional-info lead-text">Reset password</span>
        </Link>
      </p> */}
    </section>
  );
};

export default NormalLoginForm;
