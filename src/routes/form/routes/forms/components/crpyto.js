import CryptoJS from "crypto-js";

const secretKey = "vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3";

const encrypt = (text) => {
  // Generate a random IV (Initialization Vector)
  const iv = CryptoJS.lib.WordArray.random(16);

  // Encrypt the text using AES encryption
  const cipherText = CryptoJS.AES.encrypt(text, secretKey, {
    iv: iv,
    mode: CryptoJS.mode.CTR,
  });

  // Return the encrypted text and IV
  return {
    iv: iv.toString(CryptoJS.enc.Hex),
    content: cipherText.toString(),
  };
};

const decrypt = (hash) => {
  // Parse the IV from the input
  const iv = CryptoJS.enc.Hex.parse(hash.iv);

  // Decrypt the text using AES encryption
  const decryptedText = CryptoJS.AES.decrypt(hash.content, secretKey, {
    iv: iv,
    mode: CryptoJS.mode.CTR,
  });

  // Return the decrypted text
  return decryptedText.toString(CryptoJS.enc.Utf8);
};

export { decrypt, encrypt };
