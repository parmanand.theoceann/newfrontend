import React, { Component } from 'react';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  getAPICall,
  openNotificationWithIcon,
  postAPICall,
  apiDeleteCall,
} from '../../shared';
import moment from 'moment';

 
class PortInformation extends Component {
  constructor(props) {
    super(props);
    // Start table section
      this.state = {
      frmName: "port_call_port_information_form",
      formData: {},
      loading: true,
      responseData : this.props.responseData || false,
      modals: {
        PortActivityReport: false,
      },
      editportexpData: this.props.editportexpData,
      port_call_id : this.props.editportexpData && this.props.editportexpData['port_call'] && this.props.editportexpData['port_call']['id'],
      isUserView : this.props.isUserView,
      isResubmitButton : false,
      isAgentView : this.props.isAgentView === true ? true : false
    };
     // End table section
  }

  componentDidMount = async () => {
    const { editportexpData } = this.state;
    this.updateFormData(editportexpData)
  //  this.showPortDetails()
  }

  // showPortDetails = () => {
  //   const { isUserView, editportexpData } = this.state;
  // //  let port_call_ref_id = editportexpData['port_call']['port_call_ref_id']
  //   if(isUserView.length > 0){
  //     isUserView.map(val=>{
  //       // if(val.port_call_ref_id == port_call_ref_id){
  //       //   this.setState({ isResubmitButton : true})
  //       // }
  //     })
  //   }
  // }

  updateFormData = (editportexpData) => {
    const { isAgentView } = this.state;
    let frmData = {};  
    if(editportexpData && editportexpData['port_info'] !== -1){
      if(isAgentView === false){
        editportexpData['port_info'] = Object.assign({}, editportexpData['port_info'] ,{'disablefield' : ['attachment', 'crane_avaiblity', 'discharge_rate', 'draft_restriction', 'fw_draft_limit', 'load_rate', 'no_of_birth', 'port_name', 'remark', 'sw_draft_limit', 'type_of_cargo_load_discharge']})
     }
       editportexpData['port_name'] = editportexpData['port_call']['portcalldetails']['port']
       this.setState({ ...this.state, loading: false, formData :editportexpData['port_info'] });
    }else{
      if(isAgentView === false){
         frmData = Object.assign({}, frmData ,{'disablefield' : ['attachment', 'crane_avaiblity', 'discharge_rate', 'draft_restriction', 'fw_draft_limit', 'load_rate', 'no_of_birth', 'port_name', 'remark', 'sw_draft_limit', 'type_of_cargo_load_discharge']})
      }
    //  frmData['port_name'] =  editportexpData['port_call']['portcalldetails'] && editportexpData['port_call']['portcalldetails']['port']
      this.setState({ ...this.state, formData :frmData, loading: false });
    }
  
  }

  editPortInformation = async(portexpID) => {
    const Editreponse = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${portexpID}`)
    const editData = await Editreponse['data'];
    this.state.editportexpData = editData;
    this.state.formData = {};
    this.setState({ ...this.state, loading: true, editportexpData: editData, formData: {} })
    this.updateFormData(editData)
  }

  saveFormData = (data, innerCB) => {
    const { frmName, port_call_id } = this.state;
    data['last_update_dt'] = moment();
    if(port_call_id){
      data['port_call_id'] = port_call_id
    }
    if(data['-']) {
      delete data['-']
    }
    if(data['.']) {
      delete data['.']
    }
    if(data['..']){
      delete data['..']
    }
    if(data['...']){
      delete data['...']
    }
    if(data['id']){
      postAPICall( `${URL_WITH_VERSION}/port-call/pi/update?frm=${frmName}`, data, 'PUT', response => {
          if (response && response.data) {
            openNotificationWithIcon('success', response.message);
            this.editPortInformation(port_call_id)
            } else {
              openNotificationWithIcon('error', response.message);
          }
      });
    }else{
      postAPICall( `${URL_WITH_VERSION}/port-call/pi/save?frm=${frmName}`, data, 'POST', response => {
        if (response && response.data) {
          if(response.row){
            this.editPortInformation(port_call_id)
          }
          openNotificationWithIcon('success', response.message);
        } else {
          openNotificationWithIcon('error', response.message);
        }
      });
    }
  }

  onDeleteFormData = (data, innerCB) => {
    //const { saveIID } = this.state
    apiDeleteCall(`${URL_WITH_VERSION}/port-call/pi/delete`, { "id": data.id }, respon2 => {
      if (respon2.data === true) {
        openNotificationWithIcon('success', respon2.message);
      } else {
        openNotificationWithIcon('error', respon2.message);
      }
    });
  };

  notFillAllData = (port_call_id, innerCB) => {
    const { editportexpData } = this.state;
    if( editportexpData['port_info'] !== -1 &&  editportexpData['port_activity'] != -1 && editportexpData['port_expense'] !== -1){
      if(editportexpData['port_expense']['pda_submit'] == 1 && editportexpData['port_expense']['fda_submit'] == 1){
        this.props.submitForm(port_call_id, innerCB)
      }else{
        openNotificationWithIcon('error', 'Please submit PDA/FdA data');
      }
    }else{
      openNotificationWithIcon('error', 'Please fill all tabs informations');
    }
  }

  render() {
    const { frmName, formData, loading, port_call_id, isResubmitButton, isAgentView, editportexpData } = this.state;
  // let update = editportexpData['port_info'] != -1 ? true : false;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
          
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>General port information</span>
                  </h4>
                </div>
              </div>
              {loading === false && 
              <NormalFormIndex key={'key_' + frmName + '_0'} formClass="label-min-height" formData={formData} showForm={true} frmCode={frmName} addForm={true} 
                    // showButtons={
                    //   isAgentView === true && 
                    //   [
                    //   { "id": `${update === true ? "update" : "save" }`, "title": `${update === true ? "Update" : "Save" }`, "type": "primary", "event": (data, innerCB) => { this.saveFormData(data, innerCB) }  },
                    //  ]
                 
                 // }
                  //   showToolbar={
                  //     [
                  //     {
                  //       isLeftBtn: [
                  //         {
                  //           isSets: [
                  //             { id: '3', key: 'save', type: <SaveOutlined />, withText: '', "event": (data, innerCB) => { this.notFillAllData(port_call_id, innerCB) } },
                  //           ],
                  //         },
                  //       ],
                  //       isRightBtn: []
                  //     },
                  //   ] 
                  // }
                    inlineLayout={true}
                  />
               }

              {/* <div className="row p10">
                <div className="col-md-12">
                  <article className="article">
                    <div className="box box-default">
                      <div className="box-body">
                      <BirthInformation />
                      </div>
                    </div>
                  </article>
                </div>
            </div> */}
              {/* <Form>
                <div className="row p10">
                  <div className="col-md-6">
                    <FormItem label="Remark">
                      <TextArea rows={4} />
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem label="Attchment">
                      <Upload>
                        <Button>
                      <UploadOutlined /> Click to Upload
                        </Button>
                      </Upload>
                    </FormItem>

                    <FormItem label="Last Update Time">
                      <TimePicker />
                    </FormItem>
                  </div>
                </div>
              </Form> */}
              {/* <div className="row p10">
                <div className="col-md-12">
                  <div className="action-btn text-right">
                    <Button className="ant-btn ant-btn-primary">Save</Button>
                    <Button className="ant-btn ant-btn-danger ml-2">Cancel</Button>
                  </div>
                </div>
              </div>  */}
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortInformation;
