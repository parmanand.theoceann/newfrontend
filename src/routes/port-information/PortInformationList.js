import React, { Component } from 'react';
import { Table,  Modal } from 'antd';
import {CheckCircleOutlined} from '@ant-design/icons';
import BirthInformation from './BirthInformation';
import URL_WITH_VERSION, { getAPICall, ResizeableTitle} from '../../shared';
import { FIELDS } from '../../shared/tableFields';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';

class PortInformationList extends Component {
  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Birth',
          dataIndex: 'birth',
          render: (text, record) => {
            const { editable } = record;
            return (
              <div className="editable-row-operations">
                {
                  <span
                    className="iconWrapper"
                    onClick={() => this.showHideModal(true, 'showBirthInformation')}
                  >
             <CheckCircleOutlined />
                  </span>
                }
              </div>
            );
          },
    }
    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['port-info-list'] ? FIELDS['port-info-list']['tableheads'] : []
    );
    tableHeaders.push(tableAction);
    this.state = {
      sidebarVisible: false,
      columns: tableHeaders,
      modals: { showBirthInformation: false, },
      responseData : [],
      pageOptions: { pageIndex: 1, pageLimit: 20, totalRows: 0 },
      loading:false,
    };
  }

  componentDidMount = async() => {
      this.getTableData()
  }

  getTableData = async() => {
    this.setState({
      loading:true
    })
    const voy_id = this.props.location && this.props.location.state ? this.props.location.state : null
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/list?us=1`);
    const responseData = response['data']
     if(responseData){
      responseData.map(e=>{ 
        if(voy_id == e.voyage_manager_id){
           this.portInfoAPIcall(e)
        }
        if(voy_id == -1 || !voy_id){
          this.portInfoAPIcall(e)
        }
      })
    }
  }

  portInfoAPIcall = async(value) => {
    let ID = value.id;
    let agent_by = value.agent_name
    let newArray = this.state.responseData;
    const response = await getAPICall(`${URL_WITH_VERSION}/port-call/editall?e=${ID}`);
        const responseData = response['data']
        if(responseData.port_info != -1){
          let data = {
            attachment:responseData.port_info.attachment,
            crane_avaiblity:responseData.port_info.crane_avaiblity,
            discharge_rate:responseData.port_info.discharge_rate,
            draft_restriction:responseData.port_info.draft_restriction,
            fw_draft_limit:responseData.port_info.fw_draft_limit,
            load_rate:responseData.port_info.load_rate,
            no_of_birth:responseData.port_info.no_of_birth,
            port_name:responseData.port_info.port_name,
            attaremarkchment:responseData.port_info.remark,
            sw_draft_limit:responseData.port_info.sw_draft_limit,
            type_of_cargo_load_discharge:responseData.port_info.type_of_cargo_load_discharge,
            agent_by:agent_by
          }
          newArray.push(data)
          this.setState({...this.state, responseData:newArray,loading:false})
        }
     
  }

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns = Object.assign([], this.state.columns);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns.splice(columns.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        columns: evt.hasOwnProperty('columns') ? evt.columns : columns,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible),
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };
  onActionDonwload = (downType, pageType) => {
  //     let params = `t=${pageType}`, cols = [];
  //  const { columns, pageOptions } = this.state;  



  //   let qParams = { "p": pageOptions.pageIndex, "l": pageOptions.pageLimit };

  //   columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
  //   // if (cols && cols.length > 0) {
  //   //   params = params + '&c=' + cols.join(',')
  //   // }
  //   window.open(`${URL_WITH_VERSION}/download/file/${downType}?${params}&p=${qParams.p}&l=${qParams.l}`, '_blank');
  }
  render() {
    const { columns, responseData, pageOptions,sidebarVisible, search,loading } = this.state;
    const tableColumns = columns
    .filter(col => (col && col.invisible !== 'true' ? true : false))
    .map((col, index) => ({
      ...col,
      onHeaderCell: column => ({
        width: column.width,
        onResize: this.handleResize(index),
      }),
    }));

    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="form-wrapper">
                <div className="form-heading">
                  <h4 className="title">
                    <span>Port Information List</span>
                  </h4>
                </div>
              </div>
              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loading === false ?
                    <ToolbarUI
                      routeUrl={'subscriber-list-toolbar'}
                      optionValue={{ "pageOptions": pageOptions, "columns": columns, "search": search }}
                      callback={e => this.callOptions(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => this.onActionDonwload('csv') },
                        { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                        { title: 'XLS', event: () => this.onActionDonwload('xls') },
                      ]}
                    />
                    :
                    undefined
                }
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Table
                    bordered
                    size="small"
                    scroll={{ x: 'max-content' }}
                    columns={tableColumns}
                    dataSource={responseData}
                    pagination={false}
                    footer={false}
                    rowClassName={(r, i) =>
                      i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
        <Modal
          style={{ top: '2%' }}
          title="Birth Information"
         open={this.state.modals['showBirthInformation']}
          onCancel={() => this.showHideModal(false, 'showBirthInformation')}
          width="75%"
          footer={null}
        >
          <article className="article toolbaruiWrapper">
            <div className="box box-default">
              <div className="box-body">
                <BirthInformation />
              </div>
            </div>
          </article>
        </Modal>
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptions(e)} /> : null
        }
      </div>
    );
  }
}

export default PortInformationList;
