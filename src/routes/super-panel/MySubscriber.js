import React, { Component } from 'react';
import { Modal } from 'antd';
import { EyeOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import URL_WITH_VERSION, { postAPICall, openNotificationWithIcon } from '../../shared';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import MySubscriptionReport from './MySubscriptionReport'

class MySubscriber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      frmName: 'my_subscriber_form',
      formData: this.props && this.props.location && this.props.location.state && this.props.location.state.detail ? this.props.location.state.detail : {},
      editOrSave: this.props && this.props.location && this.props.location.state && this.props.location.state.detail ? "edit" : "save",
      columns: [
        {
          title: 'Sr No.',
          dataIndex: 'sr_no',
        },

        {
          title: 'Transaction ID',
          dataIndex: 'transaction_id',
        },

        {
          title: 'Received Amount',
          dataIndex: 'received_amount',
        },
        {
          title: 'Pay Type',
          dataIndex: 'pay_type',
        },
        {
          title: 'Transaction Date',
          dataIndex: 'transaction_date',
        },

        {
          title: 'Pending Amount',
          dataIndex: 'pending_amount',
        },

        {
          title: 'Next Due Date',
          dataIndex: 'next_due_date',
        },

        {
          title: 'Action',
          dataIndex: 'action',
          render: (text, record) => {
            // const { editable } = record;
            return (
              <div className="editable-row-operations">
                {
                  <span
                    className="iconWrapper"
                    onClick={() => this.showHideModal(true, 'showPaymentHistoryDetail')}
                  >
                <EyeOutlined />
                  </span>
                }
              </div>
            );
          },
        },
      ],
      modals: {
        showPaymentHistoryDetail: false,
      },
      isShowMySubscriptionReport: false,
    };
  }
  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };

  MySubscriptionReport = showMySubscriptionReport => this.setState({ ...this.state, isShowMySubscriptionReport: showMySubscriptionReport });


  emailValidation(user_email) {
    const validEmail = new RegExp('^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$');
    if (!validEmail.test(user_email)) {
      return false;
    }
    return true;
  }

  validateEvent = (data, cb) => {
    let isFaluty = false, message = undefined;
    if (this.emailValidation(data['email_id']) === false) {
      isFaluty = true;
      message = 'Email Field is Invalid';
    }

    return cb(isFaluty, message);
  };

  saveFormData = (postData, innerCB) => {
    const { frmName } = this.state;
    // console.log("data...",postData)
    let _url = 'save';
    let _method = 'post';
    if (postData.hasOwnProperty('id')) {
      _url = 'update';
      _method = 'put';
    }
    // console.log("_url",_url)
    this.validateEvent(postData, (isFail, message) => {
      if (isFail === true) {
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: message }} />, 5);
      } else {
        postAPICall(`${URL_WITH_VERSION}/subscription/${_url}?frm=${frmName}`, postData, _method, data => {
          if (data.data) {
            openNotificationWithIcon('success', data.message);
          } else {
            let dataMessage = data.message;
            let msg = "<div className='row'>";

            if (typeof dataMessage !== 'string') {
              Object.keys(dataMessage).map(
                i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
              );
            } else {
              msg += dataMessage;
            }

            msg += '</div>';
            openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
          }
        });
      }
    });
  };

  render() {
    const {
      frmName,
      formData,
      isShowMySubscriptionReport
    } = this.state;
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <NormalFormIndex
                key={'key_' + frmName + '_0'}
                formClass="label-min-height"
                formData={formData}
                showForm={true}
                frmCode={frmName}
                addForm={true}
                showToolbar={[
                  {
                    isLeftBtn: [
                      {
                        key: 's2',
                        isSets: [
                          {
                            id: '1',
                            key: 'add',
                            type: <PlusOutlined />,
                            withText: '',
                            event: (key, data) => this._onCreateFormData(),
                          },
                          {
                            id: '3',
                            key: 'save',
                            type: <SaveOutlined />,
                            withText: '',
                            event: (key, data, innerCB) => this.saveFormData(data, innerCB),
                          },
                        ],
                      },
                    ],
                    isRightBtn: [
                      {
                        key: 's1',
                        isSets: [

                          {
                            id: '3',
                            key: 'report',
                            type: '',
                            isDropdown: 0,
                            withText: 'Report',
                            event: key => this.MySubscriptionReport(true),
                          },
                        ],
                      },
                    ],
                    isResetOption: false,
                  },
                ]}
                inlineLayout={true}
              />
            </div>
          </div>
        </article>

        {isShowMySubscriptionReport ? (
          <Modal
            style={{ top: '2%' }}
            title="Report"
           open={isShowMySubscriptionReport}
            onOk={this.handleOk}
            onCancel={() => this.MySubscriptionReport(false)}
            width="95%"
            footer={null}
          >
            <MySubscriptionReport />
          </Modal>
        ) : (
          undefined
        )}
      </div>



    );
  }
}

export default MySubscriber;
