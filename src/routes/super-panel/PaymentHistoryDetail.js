import React, { Component } from 'react';

class PaymentHistoryDetail extends Component {
  render() {
    let data=this.props.data && this.props.data !== undefined ? this.props.data :null
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Sr No. :</label> {data!=null && data.sr_no}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Transaction ID :</label> {data!=null && data.transaction_id ? data.transaction_id:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Received Amount :</label> {data!=null && data.recieved_amount ? data.recieved_amount:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Pay Type :</label> {data!=null && data.pay_type ? data.pay_type:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Transaction Date :</label> {data!=null && data.transaction_date ? data.transaction_date:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Pending Amount :</label> {data!=null && data.pending_amount ? data.pending_amount:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Next Due Date :</label> {data!=null && data.next_due_date ? data.next_due_date:'--'}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PaymentHistoryDetail;
