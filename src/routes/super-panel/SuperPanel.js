import React, { Component } from 'react';
import { Table, Modal,  Popconfirm, Button, Form, Input, Select } from 'antd';
import ToolbarUI from '../../components/CommonToolbarUI/toolbar_index';
import CompanyUser from './CompanyUser';
import ServiceProvider from './ServiceProvider'
import SubscriberDetail from './SubscriberDetail';
import PayNow from './PayNow'
import URL_WITH_VERSION, {
  getAPICall,
  objectToQueryStringFunc,
  apiDeleteCall,
  openNotificationWithIcon,
  postAPICall,
} from '../../shared';
import NormalFormIndex from '../../shared/NormalForm/normal_from_index';
import AddAddressBook from '../../components/AddAddressBook';
import SidebarColumnFilter from '../../shared/SidebarColumnFilter';
import { DeleteOutlined, EditOutlined, EyeOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';

const FormItem = Form.Item;
const { Option } = Select;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
class SuperPanel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      user_role: '',
      name: '',
      roleData: '',
      roleAssignmentData: {},
      responseData: [],
      ServiceProviderList: [],
      frmName: 'user_form',
      viewData: {},
      addvisible: false,
      pageOptions: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      pageOptionsComp: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      pageOptionsSer: { pageIndex: 1, pageLimit: 10, totalRows: 0 },
      responseDataComp: [],
      loading: true,
      loadingComp: false,
      loadingSer: false,
      sidebarVisible: false,
      sidebarVisible2: false,
      sidebarVisible3: false,
      roleVisible: false,
      columns: [
        {
          title: 'Subscription Company',
          dataIndex: 'subscription_company',
          width: 160,
          render(text) {
            return {
              props: {
                style: { color: '#3bb812' },
              },
              children: <div>{text}</div>,
            };
          },
        },


        {
          title: 'Country',
          dataIndex: 'country_name',
          width: 100,
        },
        {
          title: 'Name',
          dataIndex: 'first_name',
          width: 100,
          render(text) {
            return {
              props: {
                style: { color: '#1d565c' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'Email id',
          dataIndex: 'user_email',
          width: 120,
        },
        {
          title: 'Role',
          dataIndex: 'role_type',
          width: 100,
        },
        {
          title: 'Status',
          dataIndex: 'status',
          width: 100,
          render: (text, record) => {
            let user_status_name = record.user_status_name
            return (
              <div className="editable-row-operations">
                {user_status_name == "ACTIVE" ?
                  <span className="status-btn active-status">Active</span>
                  :
                  <span className="status-btn deactive-status">Inactive</span>
                }
              </div>
            );
          },
        },
        {
          title: 'Action',
          dataIndex: 'action',
          width: 100,
          render: (text, record) => {
            const { editable } = record;
            return (
              <div className="editable-row-operations">
                {
                  <span className="iconWrapper" onClick={() => this.showHideModal(true, 'showCompanyUserDetail', record)}>
                   <EyeOutlined/>
                  </span>
                }
                {
                  <span className="iconWrapper" onClick={e => this.redirectToAdd(e, record.id)}>
                   <EditOutlined/>
                  </span>
                }
                {
                  <span className="iconWrapper cancel">
                    <Popconfirm
                      title="Are you sure, you want to delete it?"
                      onConfirm={() => this.onRowDeletedClick1(record.id)}
                    >
                      <DeleteOutlined/>
                    </Popconfirm>
                  </span>
                }
                {
                  <Button type="primary" onClick={e => this.redirectToAddRole(record)}>
                    Role Assignment
                  </Button>
                }
              </div>
            );
          },
        },
      ],
      columns2: [
        {
          title: 'Suscriber company',
          dataIndex: 'subscription_company',
          width: 160,
          render(text) {
            return {
              props: {
                style: { color: '#3bb812' },
              },
              children: <div>{text}</div>,
            };
          },
        },
        {
          title: 'Company Name',
          dataIndex: 'full_name',
          width: 100,
          render(text) {
            return {
              props: {
                style: { color: '#3bb812' },
              },
              children: <div>{text}</div>,
            };
          },

        },

        {
          title: 'Short Name',
          dataIndex: 'short_name',
          width: 100,
        },
        {
          title: 'Country',
          dataIndex: 'country',
          width: 100,
        },

        {
          title: 'Email id',
          dataIndex: 'email',
          width: 120,
        },


        {
          title: 'Mail Alert',
          dataIndex: 'mail_alert',
          width: 100,
        },

        {
          title: 'Serviced port',
          dataIndex: 'port_names',
          width: 120,
          render: (text, record) => {
            const { editable } = record;
            return (
              <p>{record && record.port_names && record.port_names.join(', ')}</p>
            );
          },
        },
        {
          title: 'Service country',
          width: 120,
          dataIndex: 'country',
        },


        {
          title: 'Action',
          dataIndex: 'action',
          width: 80,
          render: (text, record) => {
            const { editable } = record;
            return (
              <div className="editable-row-operations">
                {
                  <span className="iconWrapper" onClick={() => this.showHideModal(true, 'showServiceProviderDetail', record)}>
                   <EyeOutlined/>
                  </span>
                }
                {
                  <span onClick={() => { this.editTableData(record.id) }} className="iconWrapper">
                   <EditOutlined/>
                  </span>
                }
              </div>
            );
          },
        },
      ],
      columns3: [
        {
          title: 'Company Name',
          dataIndex: 'company_name',
          width: 150,
          render(text) {
            return {
              props: {
                style: { color: '#3bb812' },
              },
              children: <div>{text}</div>,
            };
          },
        },

        // {
        //   title: 'Company ID',
        //   dataIndex: 'company_id',
        // },
        {
          title: 'Country',
          dataIndex: 'country_name',
          width: 100,
        },
        {
          title: 'Email',
          dataIndex: 'email_id',
          width: 120,
        },

        {
          title: 'License Plan',
          dataIndex: 'licence_plan_name',
          width: 100,
        },


        {
          title: 'Active Date',
          width: 120,
          dataIndex: 'activation_date',
          render: (text, record) => {
            let paymenthistory = record['-']
            return {
              rops: {
                style: { color: '#0726ff' },
              },
              children: <div className="activedate">
                {paymenthistory.activation_date}
              </div>,
            }
          }
        },

        {
          title: 'Next Due Date',
          width: 120,
          dataIndex: 'next_due',
          render: (text, record) => {
            let paymenthistory = record['-']
            return {
              props: {
                style: { color: '#ff0000' },
              },
              children: <div>{paymenthistory.next_due_date}</div>,
            }
          }
        },

        {
          title: 'Status',
          width: 100,
          dataIndex: 'status',
          render: (text, record) => {
            let user_status_name = record.subscriber_status_name
            return (
              <div className="editable-row-operations">
                {user_status_name == "ACTIVE" ?
                  <span className="status-btn active-status">Active</span>
                  :
                  <span className="status-btn deactive-status">Inactive</span>
                }
              </div>
            );
          },
        },

        {
          title: 'Action',
          width: 110,
          dataIndex: 'action',
          render: (text, record) => {
            const { editable } = record;
            // console.log("record..",record)
            return (
              <div className="editable-row-operations">
                {
                  <span className="iconWrapper" onClick={() => this.showHideModal(true, 'showSubscriberDetail', record)}>
                    <EyeOutlined/>
                  </span>
                }

                {
                  <span onClick={() => { this.redirectToEdit(record.subscription_id) }} className="iconWrapper">
                     <EditOutlined/>
                  </span>
                }



                {
                  <span className="status-btn active-status pointer" onClick={() => this.showHideModal(true, 'showPayNow', record)}>
                    Pay
                  </span>
                }
              </div>
            );
          },
        },
      ],
      modals: {
        showServiceProviderDetail: false,
        showCompanyUserDetail: false,
        showSubscriberDetail: false,
        showPayNow: false,
      },

    }
  }

  componentDidMount = async () => {
    this.getTableData();
    this.getCompData();
    this.getSerData();
    let _url = `${URL_WITH_VERSION}/user/role/list`;
    const response = await getAPICall(_url);
    const data = await response;
    this.setState({
      roleData: data.data
    })
  };

  redirectToAddRole = (data, innerCB) => {
    // console.log("role", data)
    this.setState({
      roleVisible: true,
      roleAssignmentData: data,
      name: data.first_name
    })
  }

  onRowDeletedClick1 = id => {
    let _url = `${URL_WITH_VERSION}/user/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getCompData();
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };
  editTableData = async (id) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/address/edit?ae=${id}`);
      const respData = await response;
      this.setState({ ...this.state, formDataValuesAdd: respData['data'], isAdd1: false }, () => {
        this.setState({ ...this.state, addvisible: true })
      });
    }
    else {
      // console.log("else")
      this.setState({ ...this.state, isAdd1: true, addvisible: true, formDataValuesAdd: {} });
    }
  }

  redirectToEdit = async (id) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/subscription/edit?e=${id}`);
      const respData = await response;
      respData['data']['disablefield'] = ["company_name", "company_id", "country_code", "country"]
      this.props.history.push({
        pathname: `/edit-subscriber/${id}`,
        state: { detail: respData['data'] }
      })
    }
  };

  onRowDeletedClick = id => {
    let _url = `${URL_WITH_VERSION}/subscription/delete`;
    apiDeleteCall(_url, { id: id }, response => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        this.getTableData();
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  callOptions = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptions;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptions: pageOptions }, () => {
        this.getTableData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptions;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.responseData;
      let columns3 = Object.assign([], this.state.columns3);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns3.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns3.splice(columns3.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible3: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible3),
        columns3: evt.hasOwnProperty('columns') ? evt.columns : columns3,
      });
    } else {
      let pageOptions = this.state.pageOptions;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptions: pageOptions }, () => {
        this.getTableData();
      });
    }
  };

  callOptionsSer = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptionsSer = this.state.pageOptionsSer;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptionsSer['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptionsSer: pageOptionsSer }, () => {
        this.getSerData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptionsSer = this.state.pageOptionsSer;
      pageOptionsSer['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptionsSer: pageOptionsSer }, () => {
        this.getSerData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseData = this.state.ServiceProviderList;
      let columns2 = Object.assign([], this.state.columns2);

      if (responseData.length > 0) {
        for (var k in responseData[0]) {
          let index = columns2.some(
            item =>
              (item.hasOwnProperty('dataIndex') && item.dataIndex === k) ||
              (item.hasOwnProperty('key') && item.key === k)
          );
          if (!index) {
            let title = k
              .split('_')
              .map(snip => {
                return snip[0].toUpperCase() + snip.substring(1);
              })
              .join(' ');
            let col = Object.assign(
              {},
              {
                title: title,
                dataIndex: k,
                key: k,
                invisible: 'true',
                isReset: true,
              }
            );
            columns2.splice(columns2.length - 1, 0, col);
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible2: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible2),
        columns2: evt.hasOwnProperty('columns') ? evt.columns : columns2,
      });
    } else {
      let pageOptionsSer = this.state.pageOptionsSer;
      pageOptionsSer[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptionsSer['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptionsSer: pageOptionsSer }, () => {
        this.getSerData();
      });
    }
  };


  callOptionsComp = evt => {
    if (evt.hasOwnProperty('searchOptions') && evt.hasOwnProperty('searchValue')) {
      let pageOptions = this.state.pageOptionsComp;
      let search = { searchOptions: evt['searchOptions'], searchValue: evt['searchValue'] };
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: search, pageOptionsComp: pageOptions }, () => {
        this.getCompData(evt);
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'reset-serach') {
      let pageOptions = this.state.pageOptionsComp;
      pageOptions['pageIndex'] = 1;
      this.setState({ ...this.state, search: {}, pageOptionsComp: pageOptions }, () => {
        this.getCompData();
      });
    } else if (evt && evt.hasOwnProperty('actionName') && evt['actionName'] === 'column-filter') {
      // column filtering show/hide
      let responseDataComp = this.state.responseDataComp;
      let columns = Object.assign([], this.state.columns);;

      if (responseDataComp.length > 0) {
        for (var k in responseDataComp[0]) {
          let index = columns.some(item => (item.hasOwnProperty('dataIndex') && item.dataIndex === k) || (item.hasOwnProperty('key') && item.key === k));
          if (!index) {
            let title = k.split("_").map(snip => {
              return snip[0].toUpperCase() + snip.substring(1);
            }).join(" ");
            let col = Object.assign({}, {
              "title": title,
              "dataIndex": k,
              "key": k,
              "invisible": "true",
              "isReset": true
            })
            columns.splice(columns.length - 1, 0, col)
          }
        }
      }
      this.setState({
        ...this.state,
        sidebarVisible: (evt.hasOwnProperty("sidebarVisible") ? evt.sidebarVisible : !this.state.sidebarVisible),
        columns: (evt.hasOwnProperty("columns") ? evt.columns : columns)
      })
    } else {
      let pageOptions = this.state.pageOptionsComp;
      pageOptions[evt['actionName']] = evt['actionVal'];

      if (evt['actionName'] === 'pageLimit') {
        pageOptions['pageIndex'] = 1;
      }

      this.setState({ ...this.state, pageOptionsComp: pageOptions }, () => {
        this.getCompData();
      });
    }
  };




  getCompData = async (search = {}) => {
    const { pageOptionsComp } = this.state;

    let qParams = { p: pageOptionsComp.pageIndex, l: pageOptionsComp.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      responseDataComp: [],
    }, () => {
      this.setState({
        loadingComp: true,
      })
    });

    let qParamString = objectToQueryStringFunc(qParams);
    let _url = `${URL_WITH_VERSION}/user/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = {
      loadingComp: false,
    };
    if (dataArr.length > 0 && totalRows > this.state.responseDataComp.length) {
      state['responseDataComp'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptionsComp: {
        pageIndex: pageOptionsComp.pageIndex,
        pageLimit: pageOptionsComp.pageLimit,
        totalRows: totalRows,
      },
      loadingComp: false,
    });

  };

  getTableData = async (search = {}) => {
    const { pageOptions } = this.state;

    let qParams = { p: pageOptions.pageIndex, l: pageOptions.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loading: true,
      responseData: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);

    let _url = `${URL_WITH_VERSION}/subscription/list?${qParamString}`;
    const response = await getAPICall(_url, headers);
    const data = await response;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loading: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state['responseData'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptions: {
        pageIndex: pageOptions.pageIndex,
        pageLimit: pageOptions.pageLimit,
        totalRows: totalRows,
      },
      loading: false,
    });

  };

  getSerData = async (search = {}) => {
    const { pageOptionsSer } = this.state;

    let qParams = { p: pageOptionsSer.pageIndex, l: pageOptionsSer.pageLimit };
    let headers = { order_by: { id: 'desc' } };

    if (
      search &&
      search.hasOwnProperty('searchValue') &&
      search.hasOwnProperty('searchOptions') &&
      search['searchOptions'] !== '' &&
      search['searchValue'] !== ''
    ) {
      let wc = {};
      if (search['searchOptions'].indexOf(';') > 0) {
        let so = search['searchOptions'].split(';');
        wc = { OR: {} };
        so.map(e => (wc['OR'][e] = { l: search['searchValue'] }));
      } else {
        wc[search['searchOptions']] = { l: search['searchValue'] };
      }

      headers['where'] = wc;
    }

    this.setState({
      ...this.state,
      loadingSer: true,
      ServiceProviderList: [],
    });

    let qParamString = objectToQueryStringFunc(qParams);
    let _url1 = `${URL_WITH_VERSION}/address/pdmslist?${qParamString}`;
    const response1 = await getAPICall(_url1, headers);
    const data = await response1;
    const totalRows = data && data.total_rows ? data.total_rows : 0;
    let dataArr = data && data.data ? data.data : [];
    let state = { loadingSer: false };
    if (dataArr.length > 0 && totalRows > this.state.responseData.length) {
      state['ServiceProviderList'] = dataArr;
    }
    this.setState({
      ...this.state,
      ...state,
      pageOptionsSer: {
        pageIndex: pageOptionsSer.pageIndex,
        pageLimit: pageOptionsSer.pageLimit,
        totalRows: totalRows,
      },
      loadingSer: false,
    });

  };

  // showServiceProviderDetail =()=> {}
  showHideModal = async (visible, modal, record) => {
    // console.log("rec..",record)
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
      viewData: record
    }, () => { console.log(".....") });

  };

  onActionDonwload = (downType, pageType) => {
    let params = `t=${pageType}`, cols = [];
    const { columns } = this.state;

    columns.map(e => (e.invisible === "false" && e.key !== 'action' ? cols.push(e.dataIndex) : false));
    // if (cols && cols.length > 0) {
    //   params = params + '&c=' + cols.join(',')
    // }

    window.open(`${process.env.REACT_APP_ATTACHMENT}/download/file/${downType}?${params}`, '_blank');
  }
  redirectToAdd = async (e, id = null) => {
    if (id) {
      const response = await getAPICall(`${URL_WITH_VERSION}/user/edit?e=${id}`);
      const respData = await response['data'];
      respData['id'] = respData['id'];
      this.setState({ ...this.state, isAdd: false, formData: respData }, () =>
        this.setState({ ...this.state, visible1: true })
      );
    } else {
      this.setState({ ...this.state, isAdd: true, visible1: true, formData: {} });
    }
  };

  handleCancel = () => {
    this.setState({ visible1: false });
  };

  onCancel = (e) => {
    this.getSerData(1, { "order_by": { "id": "DESC" } });
    this.setState({ ...this.state, isAdd1: false }, () => this.setState({ ...this.state, addvisible: false }));
  }

  validateEvent = (data, cb) => {
    let isFaluty = false,
      message = undefined;
    if (this.passwordValidation(data['user_password']) === false) {
      isFaluty = true;
      message = 'Your password is invalid (ex.: Admin@123)';
    }

    if (this.emailValidation(data['user_email']) === false) {
      isFaluty = true;
      message = 'Email Field is Invalid';
    }


    return cb(isFaluty, message);
  };

  _onCreateFormData = () => {
    this.setState({ ...this.state, frmVisible: false }, () => {
      this.setState({
        ...this.state,
        formData: {
          id: 0,
          'portcostp.tableperday': [
            { editable: true, index: 0, con_type: 2, con_g: 2, id: -9e6, con_unit: 2 },
            { editable: true, index: 1, con_g: 3, con_type: 5, id: -9e6 + 1, con_unit: 2 },
            { editable: true, index: 2, con_g: 3, con_type: 7, id: -9e6 + 2, con_unit: 2 },
            { editable: true, 'ind ex': 3, con_type: 1, con_g: 1, id: -9e6 + 3, con_unit: 2 },
            { editable: true, index: 4, con_type: 10, con_g: 4, id: -9e6 + 4, con_unit: 2 },
          ],
        },
        frmVisible: true,
      });
    });
  };

  saveFormData = (postData, innerCB) => {
    const { frmName } = this.state;
    let _url = 'save';
    let _method = 'post';
    if (postData.hasOwnProperty('id')) {
      _url = 'update';
      _method = 'put';
      postData['id'] = postData['id'];
    }
    this.validateEvent(postData, (isFail, message) => {
      if (isFail === true) {
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: message }} />, 5);
      } else {
        postAPICall(`${URL_WITH_VERSION}/user/${_url}?frm=${frmName}`, postData, _method, data => {
          if (data.data) {
            openNotificationWithIcon('success', data.message);
            //if(innerCB) {innerCB()};
            this.getCompData();
            this.handleCancel();
          } else {
            let dataMessage = data.message;
            let msg = "<div className='row'>";

            if (typeof dataMessage !== 'string') {
              Object.keys(dataMessage).map(
                i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
              );
            } else {
              msg += dataMessage;
            }

            msg += '</div>';
            openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
          }
        });
      }
    });
  };

  passwordValidation(user_password) {
    const validPassword = new RegExp('^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$');
    if (!validPassword.test(user_password)) {
      return false;
    }
    return true;
  }

  emailValidation(user_email) {
    const validEmail = new RegExp('^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$');
    if (!validEmail.test(user_email)) {
      return false;
    }
    return true;
  }

  onChange = (value) => {
    this.setState({
      id: value
    })
  }

  onChange1 = (value) => {
    // console.log("val", value)
    this.setState({
      user_role: value
    })
  }


  handleSubmitRole = () => {
    const { roleAssignmentData, name, user_role } = this.state
    let obj1 = {
      "id": roleAssignmentData.id,
      "user_role": user_role.toString(),
    }
    let _url = 'urupdate';
    let _method = 'put';
    postAPICall(`${URL_WITH_VERSION}/user/role/${_url}`, obj1, _method, data => {
      if (data.data) {
        openNotificationWithIcon('success', data.message);
        this.setState({
          roleVisible: false,
          roleAssignmentData: {},
          name: ''
        }, () => {
        })
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
      }
    });
  }

  handleCancel1 = () => {
    this.setState({
      roleVisible: false,
      id: '',
      user_role: '',
      roleAssignmentData: {}
    });
  };

  render() {
    const { visible1, roleData, roleVisible, loadingComp, sidebarVisible, sidebarVisible3, sidebarVisible2, loadingSer, pageOptionsSer, ServiceProviderList, frmName, formData, isAdd, isAdd1, formDataValuesAdd, loading, columns, columns2, columns3, pageOptions, search, responseDataComp, pageOptionsComp } = this.state;
    const tableColumns = columns
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
      }));
    const tableColumns2 = columns2
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
      }));
    const tableColumns3 = columns3
      .filter((col) => (col && col.invisible !== "true") ? true : false)
      .map((col, index) => ({
        ...col,
      }));
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <h4 className="mt-3">
                Company User List
                <span className="float-right">
                  <Button type="primary" onClick={e => this.redirectToAdd()}>
                    Add User
                  </Button>
                </span>
              </h4>
              <hr />

              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loadingComp === false ?
                    <ToolbarUI
                      routeUrl={'company-user-list-toolbar'}
                      optionValue={{ pageOptions: pageOptionsComp, columns: this.state.columns, search: search }}
                      callback={e => this.callOptionsComp(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => this.onActionDonwload('csv') },
                        { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                        { title: 'XLS', event: () => this.onActionDonwload('xls') },
                      ]}
                    />
                    :
                    undefined
                }
              </div>
              <Table
                className="mt-3"
                columns={tableColumns}
                dataSource={responseDataComp}
                pagination={false}
                loading={loadingComp}
                size="small"
                bordered
                scroll={{ x: 'max-content' }}

                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />
              <h4 className="mt-3">
                Service Provider User List
                <span className="float-right">
                  <Button type="primary" onClick={e => this.editTableData(null)}>
                    Add New Service Provider
                  </Button>
                </span>
              </h4>

              <hr />

              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {loadingSer === false ?
                  <ToolbarUI
                    routeUrl={'service-provider-list-toolbar'}
                    optionValue={{ "pageOptions": pageOptionsSer, "columns": columns2, "search": search }}
                    callback={e => this.callOptionsSer(e)}
                    dowloadOptions={[
                      { title: 'CSV', event: () => this.onActionDonwload('csv') },
                      { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                      { title: 'XLS', event: () => this.onActionDonwload('xls') },
                    ]}
                  />
                  :
                  undefined
                }

              </div>
              <Table
                className="mt-3"
                columns={tableColumns2}
                dataSource={ServiceProviderList}
                pagination={false}
                loading={loadingSer}
                size="small"
                bordered
                scroll={{ x: 'max-content' }}

                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />

              <h4 className="mt-3">
                My Subscriber List{' '}
                <span className="float-right">
                  <a href="#/my-subscriber" className="ant-btn ant-btn-primary">
                    Add Subscriber
                  </a>
                </span>
              </h4>
              <hr />

              <div
                className="section"
                style={{
                  width: '100%',
                  marginBottom: '10px',
                  paddingLeft: '15px',
                  paddingRight: '15px',
                }}
              >
                {
                  loading === false ?
                    <ToolbarUI
                      routeUrl={'subscriber-list-toolbar'}
                      optionValue={{ "pageOptions": pageOptions, "columns": columns3, "search": search }}
                      callback={e => this.callOptions(e)}
                      dowloadOptions={[
                        { title: 'CSV', event: () => this.onActionDonwload('csv') },
                        { title: 'PDF', event: () => this.onActionDonwload('pdf') },
                        { title: 'XLS', event: () => this.onActionDonwload('xls') },
                      ]}
                    />
                    :
                    undefined
                }
              </div>
              <Table
                className="mt-3"
                columns={tableColumns3}
                dataSource={this.state.responseData}
                pagination={false}
                loading={loading}
                size="small"
                bordered
                scroll={{ x: 'max-content' }}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />
            </div>
          </div>
        </article>


        <Modal
          style={{ top: '2%' }}
          title="Company User  Detail"
         open={this.state.modals['showCompanyUserDetail']}
          onCancel={() => this.showHideModal(false, 'showCompanyUserDetail')}
          width="75%"
          footer={null}
        >
          <CompanyUser data={this.state.viewData} />
        </Modal>

        <Modal
          style={{ top: '2%' }}
          title="Service Provider User Detail"
         open={this.state.modals['showServiceProviderDetail']}
          onCancel={() => this.showHideModal(false, 'showServiceProviderDetail')}
          width="75%"
          footer={null}
        >
          <ServiceProvider data={this.state.viewData} />
        </Modal>

        <Modal
          style={{ top: '2%' }}
          title="My Subscriber Detail"
         open={this.state.modals['showSubscriberDetail']}
          onCancel={() => this.showHideModal(false, 'showSubscriberDetail')}
          width="75%"
          footer={null}
        >
          <SubscriberDetail data={this.state.viewData} />
        </Modal>


        <Modal
          style={{ top: '2%' }}
          title="Pay Now"
         open={this.state.modals['showPayNow']}
          onCancel={() => this.showHideModal(false, 'showPayNow')}
          width="50%"
          footer={null}
        >
          <PayNow data={this.state.viewData} fun={() => { this.showHideModal(false, 'showPayNow') && this.getTableData() }} />
        </Modal>

        {
          this.state.addvisible === true ?
            <Modal
              title={isAdd1 ? `Add Address List` : `Edit Address List`}
             open={this.state.addvisible}
              width={'90%'}
              onCancel={this.onCancel}
              style={{ top: '10px' }}
              bodyStyle={{ height: 790, overflowY: 'auto', padding: '0.5rem' }}
              footer={null}
            >
              {
                isAdd1 === true ?
                  <AddAddressBook formDataValues={{}} modalCloseEvent={this.onCancel} />
                  :
                  <AddAddressBook formDataValues={formDataValuesAdd} modalCloseEvent={this.onCancel} />
              }
            </Modal>
            : undefined
        }

        {roleVisible && (
          <Modal
            style={{ top: '2%' }}
            title={(isAdd === false ? 'Edit' : 'Add') + ' User Assignment'}
           open={roleVisible}
            onCancel={this.handleCancel1}
            width="90%"
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    <Form>
                      <div className="row">
                        <div className="col-md-6">
                          <FormItem {...formItemLayout} label="Full Name">
                            <Input value={this.state.name} onChange={(event) => { this.setState({ name: event.target.value }) }} size="default" placeholder="Role Name" />
                          </FormItem>
                        </div>

                        <div className="col-md-6">
                          <FormItem {...formItemLayout} label="User Role">
                            <Select
                              mode="multiple"
                              placeholder="User Role"
                              onChange={this.onChange1}
                            >
                              {roleData.length > 0 && roleData.map((val, ind) => {
                                return (
                                  <Option value={val.id} key={val.id}>{val.name}</Option>
                                )
                              })}

                            </Select>
                          </FormItem>
                        </div>
                      </div>
                      <span className="float-right">

                        <Button onClick={() => { this.handleSubmitRole() }} className="btn ant-btn-primary btn-sm">Save</Button>
                      </span>
                    </Form>
                  </div>
                </div>
              </article>
            </div>
          </Modal>

        )}


        {visible1 && (
          <Modal
            style={{ top: '2%' }}
            title={(isAdd === false ? 'Edit' : 'Add') + ' New User'}
           open={visible1}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            width="90%"
            footer={null}
          >
            <div className="body-wrapper">
              <article className="article">
                <div className="box box-default">
                  <div className="box-body">
                    <NormalFormIndex
                      key={'key_' + frmName + '_0'}
                      formClass="label-min-height"
                      formData={formData}
                      showForm={true}
                      frmCode={frmName}
                      addForm={true}
                      showToolbar={[
                        {
                          isLeftBtn: [
                            {
                              key: 's2',
                              isSets: [
                                {
                                  id: '1',
                                  key: 'add',
                                  type: <PlusOutlined />,
                                  withText: '',
                                  event: (key, data) => this._onCreateFormData(),
                                },
                                {
                                  id: '3',
                                  key: 'save',
                                  type: <SaveOutlined />,
                                  withText: '',
                                  event: (key, data, innerCB) => this.saveFormData(data, innerCB),
                                },
                                {
                                  id: '3',
                                  key: 'role',
                                  type: 'plus-circle',
                                  withText: '',
                                  event: (key, data, innerCB) => this.redirectToAddRole(data, innerCB),
                                },
                              ],
                            },
                          ],
                          isRightBtn: [],
                          isResetOption: false,
                        },
                      ]}
                      inlineLayout={true}
                    />
                  </div>
                </div>
              </article>
            </div>
          </Modal>

        )}
        {
          sidebarVisible ? <SidebarColumnFilter columns={columns} sidebarVisible={sidebarVisible} callback={(e) => this.callOptionsComp(e)} /> : null
        }
        {
          sidebarVisible2 ? <SidebarColumnFilter columns={columns2} sidebarVisible={sidebarVisible2} callback={(e) => this.callOptionsSer(e)} /> : null
        }
        {
          sidebarVisible3 ? <SidebarColumnFilter columns={columns3} sidebarVisible={sidebarVisible3} callback={(e) => this.callOptions(e)} /> : null
        }
      </div>
    );
  }
}

export default SuperPanel;
