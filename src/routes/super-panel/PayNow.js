import React, { Component } from 'react';
import { Form, Input, Select, Button } from 'antd';
import {CheckOutlined} from '@ant-design/icons';
import URL_WITH_VERSION, { postAPICall, openNotificationWithIcon } from '../../shared';

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const min = 1;
const max = 100;
class SubscriberDetail extends Component {


  constructor(props) {
    super(props)
    this.state = {
      data: this.props.data && this.props.data != undefined ? this.props.data : null,
      pendingAmount: '',
      receivedAmount: '',
      transaction_Id: min + (Math.random() * (max - min)),
      pay_type: 'cash',
      frmName: 'my_subscriber_form',
    }

  }

  getrecAmnt = (event) => {
    if (event.target.value) {
      let amount = this.state.data['-'].total_amount_due - event.target.value
      this.setState({
        pendingAmount: amount,
        receivedAmount: Number(event.target.value)
      })
    }
  }

  handleSubmit = () => {
    const { pendingAmount, receivedAmount, date, pay_type, transaction_Id, frmName } = this.state
    let obj = { ...this.state.data }
    obj['-'].total_amount_due = pendingAmount;
    let obj1 = {
      "pay_type": pay_type,
      "pending_amount": pendingAmount,
      "recieved_amount": receivedAmount,
      "sr_no": obj.paymenthistory.length + 1,
      "transaction_id": transaction_Id,
      "transaction_date": date,
      "index": 0,
      "key": "table_row_0",
      "editable": true
    }
    obj.paymenthistory.push(obj1)
    // console.log("obj...",obj)
    let _url = 'update';
    let _method = 'put';
    postAPICall(`${URL_WITH_VERSION}/subscription/${_url}?frm=${frmName}`, obj, _method, data => {
      if (data.data) {
        // console.log("data",data)
        openNotificationWithIcon('success', data.message);
        this.props.fun()
        // window.location.reload(false)
      } else {
        let dataMessage = data.message;
        let msg = "<div className='row'>";

        if (typeof dataMessage !== 'string') {
          Object.keys(dataMessage).map(
            i => (msg += "<div className='col-sm-12'>" + dataMessage[i] + '</div>')
          );
        } else {
          msg += dataMessage;
        }

        msg += '</div>';
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />);
      }
    });
  }

  render() {
    // console.log("this...",this.state.data)
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row">
                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Transaction ID">
                      <Input value={this.state.transaction_Id} disabled={true} size="default" placeholder="" />
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Received Amount">
                      <Input type="number" onChange={(event) => { this.getrecAmnt(event) }} size="default" placeholder="" />
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Pay Type">
                      <Select onSelect={(event) => { this.setState({ pay_type: event }) }} size="default" defaultValue="cash">
                        <Option value="cash">Cash</Option>
                        <Option value="cheque">Cheque</Option>
                        <Option value="online">Online Transaction</Option>
                      </Select>
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Transaction Date">
                      <Input onChange={(event) => { this.setState({ date: event.target.value }) }} type="date" size="default" placeholder="" />
                    </FormItem>
                  </div>

                  <div className="col-md-6">
                    <FormItem {...formItemLayout} label="Pending Amount">
                      <Input value={this.state.pendingAmount} disabled={true} size="default" placeholder="" />
                    </FormItem>
                  </div>
                </div>
                <Button onClick={() => { this.handleSubmit() }} className="btn ant-btn-primary btn-sm"><CheckOutlined  className="mr-1" />Pay Now</Button>
              </Form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default SubscriberDetail;
