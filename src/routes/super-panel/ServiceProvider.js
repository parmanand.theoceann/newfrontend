import React, { Component } from 'react';



class ServiceProvider extends Component {
  render() {
    let data=this.props.data && this.props.data !== undefined ? this.props.data :null
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="row">
              <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Suscriber Company :</label> {data!=null && data.subscription_company ? data.subscription_company:'--'}
                    </p>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company Name :</label> {data!=null && data.full_name ? data.full_name:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company Type :</label> {data!=null && data.subscription_company ? data.subscription_company:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Short Name :</label> {data!=null && data.short_name ? data.short_name:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Country :</label> {data!=null && data.country ? data.country:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Email ID :</label> {data!=null && data.email ? data.email:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>User ID :</label> {data!=null && data.subscription_company ? data.subscription_company:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Mail Alert :</label> {data!=null && data.alerts ? data.alerts:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Registered :</label> {data!=null && data.subscription_company ? data.subscription_company:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Agent Name :</label> {data!=null && data.agent_name ? data.agent_name:'--'}
                    </p>
                  </div>
                </div>

                {/* <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Status :</label> <span className="deactive-status">Inactive</span>
                    </p>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default ServiceProvider;
