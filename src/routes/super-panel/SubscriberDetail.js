import React, { Component } from 'react';
import {Modal, Table } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import PaymentHistoryDetail from './PaymentHistoryDetail';

class SubscriberDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      paymenthistory:{},
      columns: [
        {
          title: 'Sr No.',
          dataIndex: 'sr_no',
        },

        {
          title: 'Transaction ID',
          dataIndex: 'transaction_id',
        },

        {
          title: 'Received Amount',
          dataIndex: 'recieved_amount',
        },
        {
          title: 'Pay Type',
          dataIndex: 'pay_type',
        },
        {
          title: 'Transaction Date',
          dataIndex: 'transaction_date',
        },

        {
          title: 'Pending Amount',
          dataIndex: 'pending_amount',
        },

        {
          title: 'Next Due Date',
          dataIndex: 'next_due_date',
        },

        {
          title: 'Action',
          dataIndex: 'action',
          render: (text, record) => {
            //const { editable } = record;
            return (
              <div className="editable-row-operations">
                {
                  <span
                    className="iconWrapper"
                    onClick={() => this.showHideModal(true, 'showPaymentHistoryDetail',record)}
                  >
                <EyeOutlined />
                  </span>
                }
              </div>
            );
          },
        },
      ],
      modals: {
        showPaymentHistoryDetail: false,
      },
    };
  }
  showHideModal = (visible, modal,record) => {
    // console.log("rec..",record)
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
      paymenthistory:record
    });
  };
  render() {
    const { columns } = this.state;
    let data=this.props.data && this.props.data !== undefined ? this.props.data :null
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              
              <div className="row">
                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company Name :</label> {data!=null && data.company_name}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company ID :</label> {data!=null && data.company_id}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Country :</label> {data!=null && data.country_name}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Email :</label> {data!=null && data.email_id}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Contact No. :</label> {data!=null && data.contact_no}
                    </p>
                  </div>
                </div>

                

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>License Plan :</label> {data!=null && data.licence_plan_name}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Total User :</label> {data!=null && data.total_users}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Total Payment :</label> {data!=null && data.total_price}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Payment Due :</label> {data!=null && data['-'] && data['-'].total_amount_due}
                    </p>
                  </div>
                </div>

               

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Active Date :</label> {data!=null && data['-'] && data['-'].activation_date}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Next Due Date :</label> {data!=null && data['-'] && data['-'].next_due_date}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Status :</label> <span className="active-status">Active</span>
                    </p>
                  </div>
                </div>
              </div>
              <Table
                className="mt-3"
                columns={columns}
                dataSource={data!=null && data.paymenthistory.length>0 ? data.paymenthistory :[]}
                pagination={false}
                size="small"
                bordered
                scroll={{ x: 'max-content' }}
                title={() => (
                  <div className="bold-title-wrapper">
                    <span>Payment History</span>
                  </div>
                )}
                rowClassName={(r, i) =>
                  i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                }
                footer={false}
              />
            </div>
          </div>
        </article>
        <Modal
          style={{ top: '2%' }}
          title="Payment History Detail"
         open={this.state.modals['showPaymentHistoryDetail']}
          onCancel={() => this.showHideModal(false, 'showPaymentHistoryDetail')}
          width="75%"
          footer={null}
        >
          <PaymentHistoryDetail data={this.state.paymenthistory}/>
        </Modal>
      </div>
    );
  }
}

export default SubscriberDetail;
