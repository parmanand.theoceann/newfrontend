import React, { Component } from 'react';

class CompanyUser extends Component {
  render() {
    let data=this.props.data && this.props.data !== undefined ? this.props.data :null
  //  console.log("data..",data)
    return (
      <div className="body-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
             
              <div className="row">
                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company Name :</label> {data!=null && data.subscription_company ? data.subscription_company:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Company ID :</label> {data!=null && data.id ? data.id:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Country :</label> {data!=null && data.country_name ? data.country_name:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Name :</label> {data!=null && (data.first_name || data.last_name) ? (data.first_name + ' '+data.last_name):'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>User ID :</label> {data!=null && data.user_id ? data.user_id:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Email ID :</label> {data!=null && data.user_email ? data.user_email:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Role :</label> {data!=null && data.transaction_id ? data.transaction_id:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Initials :</label> {data!=null && data.initials ? data.initials:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Phone Number :</label> {data!=null && data.phone_number ? data.phone_number:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Organization :</label> {data!=null && data.organization ? data.organization:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Designation :</label> {data!=null && data.designation_name ? data.designation_name:'--'}
                    </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="user-details-list">
                    <p>
                      <label>Status :</label>
                      {data !== null && data.user_status_name && data.user_status_name == "ACTIVE"
                        ?
                        <span className="active-status">Active</span>
                        :
                        <span className="deactive-status">Inactive</span>
                      }
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default CompanyUser;
