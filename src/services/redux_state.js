class ReduxState {
  constructor() {
    this.formName = null;
    this.reduxStore = {};
  }

  setForm = (formName) => {
    this.formName = formName;
    this.reduxStore[this.formName] = {};
  };

  getForm = () => this.formName;

  addGroupProperty = (groupName, value, index) => {
    let oi = {};

    if (!this.reduxStore.hasOwnProperty(this.formName)) {
      this.reduxStore[this.formName] = {};
    }

    if (index >= 0) {
      if (!this.reduxStore[this.formName].hasOwnProperty(groupName)) {
        this.reduxStore[this.formName][groupName] = [];
      }

      let oldItems = this.reduxStore[this.formName][groupName];
      if (oldItems && oldItems[index]) {
        oi = Object.assign(oi, oldItems[index], value);
      }
      oldItems[index] = oi;
      this.reduxStore[this.formName][groupName] = oldItems;
    } else if (!index && value.hasOwnProperty(groupName)) {
      if (!this.reduxStore[this.formName].hasOwnProperty(groupName)) {
        this.reduxStore[this.formName][groupName] = [];
      }
      this.reduxStore[this.formName][groupName] = value[groupName];
    } else if (!index) {
      if (!this.reduxStore[this.formName].hasOwnProperty(groupName)) {
        this.reduxStore[this.formName][groupName] = {};
      }

      oi = Object.assign({}, this.reduxStore[this.formName][groupName], value);
      this.reduxStore[this.formName][groupName] = oi;
    }
  };

  createReducer = (store = {}, action) => {
    if (
      action &&
      action.hasOwnProperty("formName") &&
      action["formName"] !== "" &&
      this.formName !== action["formName"]
    ) {
      this.formName = action["formName"];
    }
    if (action && action.type === "add") {
      if (action.hasOwnProperty("groupName") && action.groupName) {
        this.addGroupProperty(action.groupName, action.value, action.index);
      } else if (action.hasOwnProperty("groupName") && !action.groupName) {
        let oldItems = Object.assign(this.reduxStore[this.formName], {});
        this.reduxStore[this.formName] = Object.assign(oldItems, action.value);
      }
    } else if (action && action.type === "delete") {
      
      if (
        action.hasOwnProperty("groupName") &&
        action.groupName &&
        this.reduxStore[this.formName][action.groupName]
      ) {
        let elements = this.reduxStore[this.formName][action.groupName].filter(
          (e, idx) => e.editable !== undefined
        );

        this.reduxStore[this.formName][action.groupName] = [];
        this.reduxStore[this.formName][action.groupName] = [...elements];
        // this.reduxStore[this.formName][action.groupName].map((e,idx) => {
        //   if (idx !== action.index) elements.push(e)
        //   return true;
        // });

        // this.reduxStore[this.formName][action.groupName] = elements;
      }
    } else if (action && action.type === "reset") {
      this.reduxStore[this.formName] = {};
    } else if (action && action.type === "edit_info") {
      this.reduxStore[this.formName] = action.editFormData;
    }

    return this.reduxStore;
  };
}

export default new ReduxState();
