export const calculateAverageBunkerPlan = (rowValue) => {
  let endPrice = 0,
    bunkerExpense = 0,
    totalcons = 0,
    totalqty = 0,
    totalprice = 0;
  let seaCons = rowValue?.["sea_cons"] ?? 0;
  let portCons = rowValue?.["port_cons"] ?? 0;
  let initalPrice = rowValue?.["init_prc"] ?? 0;
  let lastPrice = rowValue?.["last_prc"] ?? 0;
  totalcons = seaCons * 1 + portCons * 1;
  let initqty = rowValue?.["init_qty"] ?? 0;
  let lastqty = rowValue?.["rec_qty"] ?? 0;
  totalqty = initqty * 1 + lastqty * 1;
  totalprice = initqty * initalPrice + lastqty * lastPrice;
  endPrice = (totalprice / totalqty).toFixed(2);
  bunkerExpense = (endPrice * totalcons).toFixed(2);
  endPrice = isNaN(endPrice) ? 0 : parseFloat(endPrice).toFixed(2);
  bunkerExpense = isNaN(bunkerExpense)
    ? 0
    : parseFloat(bunkerExpense).toFixed(2);
  return { endPrice, bunkerExpense };
};

export const calculateLifoBunkerPlan = (rowValue) => {
  let endPrice = 0,
    bunkerExpense = 0,
    totalcons = 0,
    totalqty = 0,
    totalprice = 0;
  let lastinprice = 0;

  let seaCons = rowValue?.["sea_cons"] ?? 0;
  let portCons = rowValue?.["port_cons"] ?? 0;
  let initalPrice = rowValue?.["init_prc"] ?? 0;
  let lastPrice = rowValue?.["last_prc"] ?? 0;
  totalcons = seaCons * 1 + portCons * 1;
  let initqty = rowValue?.["init_qty"] ?? 0;
  let lastqty = rowValue?.["rec_qty"] ?? 0;
  let totalseaportcons = totalcons;
  if (totalcons > lastqty) {
    lastinprice = lastqty * lastPrice;
    totalcons = totalcons - lastqty;
    lastinprice += totalcons * initalPrice;
  } else if (totalcons < lastqty) {
    lastinprice = totalcons * lastPrice;
  }
  lastinprice = isNaN(lastinprice) ? 0 : parseFloat(lastinprice).toFixed(2);
  endPrice = (lastinprice / totalseaportcons).toFixed(2);
  return { lastinprice, endPrice };
};

export const calculateFifoBunkerPlan = (rowValue) => {
  let endPrice = 0,
    bunkerExpense = 0,
    totalcons = 0,
    totalqty = 0,
    totalprice = 0;
  let firstinprice = 0;
  let seaCons = rowValue?.["sea_cons"] ?? 0;
  let portCons = rowValue?.["port_cons"] ?? 0;
  let initalPrice = rowValue?.["init_prc"] ?? 0;
  let lastPrice = rowValue?.["last_prc"] ?? 0;
  totalcons = seaCons * 1 + portCons * 1;
  let initqty = rowValue?.["init_qty"] ?? 0;
  let lastqty = rowValue?.["rec_qty"] ?? 0;
  let totalseaportcons = totalcons;
  if (totalcons > initqty) {
    firstinprice = initqty * initalPrice;
    totalcons = totalcons - initqty;
    firstinprice += totalcons * lastPrice;
  } else if (totalcons < initqty) {
    firstinprice = totalcons * initalPrice;
  }
  firstinprice = isNaN(firstinprice) ? 0 : parseFloat(firstinprice).toFixed(2);
  endPrice = (firstinprice / totalseaportcons).toFixed(2);
  return { firstinprice, endPrice };
};
