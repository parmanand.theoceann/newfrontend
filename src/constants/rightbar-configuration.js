import {
  HomeOutlined,
  SearchOutlined,
  FileTextOutlined,
  CloudUploadOutlined,
  LineChartOutlined,
  FilePdfOutlined,
  ContactsOutlined,
  AlertOutlined,
  EditOutlined,
  PieChartOutlined,
  HistoryOutlined,
  FilterOutlined,
  DiffOutlined,
  CaretUpOutlined,
  SettingOutlined,
  UnorderedListOutlined,
  CodeSandboxOutlined,
  FileDoneOutlined,
  VerticalAlignTopOutlined,
  BuildOutlined,
  MailOutlined,
  HeatMapOutlined,
  } from "@ant-design/icons";
import URL_WITH_VERSION, { IMAGE_PATH } from "../shared";
import Icon from "@ant-design/icons/lib/components/Icon";

const RightBARCONFIGURATION = {
  "anlytical-dashboard-right-panel": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "search", title: "Search", icon: <SearchOutlined />, width: 1100 },
      {
        key: "chartring",
        title: "Chartring Dashboard",
        icon: <BuildOutlined />,
        width: 400,
      },
      {
        key: "accounting",
        title: "Accounting Dashboard",
        icon: <BuildOutlined />,
        width: 400,
      },
      {
        key: "summary",
        title: "summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      { key: "reports", title: "Reports", icon: <CloudUploadOutlined /> },
      { key: "vspm", title: "VSPM Dashboard", icon: <CloudUploadOutlined /> },
    ],
  },
  "vessel-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
      {
        key: "vm",
        title: "Voyage Manager Info",
        icon:<Icon component={() => <img
          style={{ width: "19px", marginLeft: "0px" }}
          src={IMAGE_PATH + "svgimg/vm.svg"}
          alt="vm"
        />}/>
      },

    ],
  },
  "tci-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      // { key: 'pl-summary', title: 'P&L', icon: <LineChartOutlined />, width: 1100 },
      {
        key: "summary",
        title: "summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      // { key: 'properties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
    ],
  },

  "voyage-manager-righttoolbar": {
    menuoption: [
      { key: "voyage-home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "pl-summary",
        title: "PL Summary",
        icon: <LineChartOutlined />,
        width: 1100,
      },
      // // { key: 'estimates-summary', title: 'Summary Tree', icon: <FileDoneOutlined />, width: 400 },
      {
        key: "vmpnl",
        title: "PNL Saved History",
        icon:<Icon component={() => <img
          style={{ width: "25px", marginLeft: "-3px" }}
          src={IMAGE_PATH + "icons/pnl-save-history.svg"}
          alt="savepnlhistory"
        />}/>
      },
      { key: "log", title: "Log", icon: <HistoryOutlined />, width: 400 },
      {
        key: "invoice",
        title: "Voyage Invoice List",
        icon: <FilePdfOutlined />,
        width: 400,
      },
      {
        key: "contact",
        title: "Voyage Contact List",
        icon: <ContactsOutlined />,
        width: 400,
      },
      //{ key: 'proprties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      //{ key: 'position-report', title: 'Position Report', icon: 'pie-chart', width: 1100 },
      {
        key: "task-alert",
        title: "Task & alert",
        icon: <AlertOutlined />,
        width: 1150,
      },
      //{ key: 'analytics', title: 'Analytics', icon: 'code-sandbox' },
      //{ key: 'port_distance', title: 'Port Distance', icon: 'environment', width: 1100 },
      // { key: 'port_route_details', title: 'Port Route Details', icon: 'share-alt', width: 1100 },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
        width: 1100,
      },
      { key: "remark", title: "Remarks", icon: <EditOutlined />, width: 1100 },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
      {
        key: "map",
        title: "Map",
        icon: (
          <Icon component={() => <img
            style={{ width: "25px", marginLeft: "-6px" }}
            src={IMAGE_PATH + "svgimg/map.svg"}
            alt="map"
          />}/>
        ),  
        width: 1100,
      },
      {
        key: "bunker",
        title: "Bunker",
        icon: (
          <Icon component={() => (<img
            style={{ width: "20px", marginLeft: "-2px" }}
            src={IMAGE_PATH + "svgimg/bunker.svg"}
            alt="Bunker"
          />)}/>
        ),
        width: 1100,
      },
      {
        key: "weather",
        title: "Weather",
        icon: (
          <Icon component={() => <img
            style={{ width: "25px", marginLeft: "-6px" ,filter: "invert(100%)"}}
           
            src={IMAGE_PATH + "svgimg/weather.svg"}
            alt="map"
          />}/>
        ),  
        width: 1100,
      },

      {
        key: "Noon",
        title: "Noon",
        icon: (
          <a href="#/dynamic-vspm" target="_blank" rel="noopener noreferrer">
            <Icon component={() => (
              <img
                style={{ width: "25px",paddingRight:"6px",}}
                src={IMAGE_PATH + "svgimg/cargo-vc.svg"}
                alt="map"
              />
            )} />
          </a>
        ),
         width: 1100
      },
      



      

      { key: "sideMap", title: "Vessel Map", icon: (
        <Icon component={() => <img
          style={{ width: "25px", marginLeft: "-6px" ,filter: "invert(100%)"}}
         
          src= {'https://d2olrrfwjazc7n.cloudfront.net/website/assets/map-svgrepo-com.svg'}
          alt="sideMap"
        />}/>
      ), width: 700 },






    ],
  },
  "tco-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      // { key: 'pl-summary', title: 'P&L', icon: <LineChartOutlined />, width: 1100 },
      {
        key: "summary",
        title: "summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      // { key: 'properties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
    ],
  },
  "tcov-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "pl", title: "P & L", icon: <LineChartOutlined />, width: 1100 },
      // { key: 'summary', title: 'summary', icon: <FileTextOutlined />, width: 400 },
      // { key: 'portinformation', title: 'Port Information', icon: <SettingOutlined />, width: 1100 },
      // { key: 'search', title: 'Search', icon: <SearchOutlined />, width: 400 },
      // { key: 'properties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      //  { key: 'port_distance', title: 'Port Distance', icon: 'environment', width: 1100 },
      // { key: 'port_route_details', title: 'Port Route Details', icon: 'share-alt', width: 1100 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
      { key: "remark", title: "Remarks", icon: <EditOutlined />, width: 1100 },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
      {
        key: "map",
        title: "Map",
        icon: (
          <Icon component={() => <img
            style={{ width: "25px", marginLeft: "-6px" }}
            src={IMAGE_PATH + "svgimg/map.svg"}
            alt="map"
          />}/>
        ),  
        width: 1100,
      },
      {
        key: "bunker",
        title: "Bunker",
        icon: (
          <Icon component={() => (<img
            style={{ width: "20px", marginLeft: "-2px" }}
            src={IMAGE_PATH + "svgimg/bunker.svg"}
            alt="Bunker"
          />)}/>
        ),
        width: 1100,
      },
      {
        key: "weather",
        title: "Weather",
        icon: (
          <Icon component={() => <img
            style={{ width: "25px", marginLeft: "-6px" ,filter: "invert(100%)"}}
           
            src={IMAGE_PATH + "svgimg/weather.svg"}
            alt="map"
          />}/>
        ),  
        width: 1100,
      },

      
    ],
  },

  "tcov-quick-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "search", title: "Search", icon: <SearchOutlined />, width: 400 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
    ],
  },

  "voy-relet-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "pl", title: "P & L", icon: <LineChartOutlined />, width: 1100 },
      // { key: 'portinformation', title: 'Port Information', icon: <SettingOutlined />, width: 1100 },
      // { key: 'properties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      //  { key: 'port_distance', title: 'Port Distance', icon: 'environment', width: 1100 },
      //  { key: 'port_route_details', title: 'Port Route Details', icon: 'share-alt', width: 1100 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
    ],
  },

  "voy-relet-quick-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "search", title: "Search", icon: <SearchOutlined />, width: 400 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
    ],
  },

  "tcto-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "pl-summary",
        title: "PL Summary",
        icon: <LineChartOutlined />,
        width: 1100,
      },
      // { key: 'estimates-summary', title: 'Summary Tree', icon: <FileDoneOutlined />, width: 400 },
      // { key: 'portinformation', title: 'Port Information', icon: <SettingOutlined />, width: 1100 },
      //  { key: 'port_distance', title: 'Port Distance', icon: 'environment', width: 1100 },
      //  { key: 'port_route_details', title: 'Port Route Details', icon: 'share-alt', width: 1100 },
      // { key: 'search', title: 'Search', icon: <SearchOutlined />, width: 400 },
      // { key: 'properties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
      { key: "remark", title: "Remarks", icon: <EditOutlined />, width: 1100 },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
      {
        key: "map",
        title: "Map",
        icon: (
          <Icon component={() => <img
            style={{ width: "25px", marginLeft: "-6px" }}
            src={IMAGE_PATH + "svgimg/map.svg"}
            alt="map"
          />}/>
        ),  
        width: 1100,
      },
      {
        key: "bunker",
        title: "Bunker",
        icon: (
          <Icon component={() => (<img
            style={{ width: "20px", marginLeft: "-2px" }}
            src={IMAGE_PATH + "svgimg/bunker.svg"}
            alt="Bunker"
          />)}/>
        ),
        width: 1100,
      },
    ],
  },
  "tcto-quick-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "search", title: "Search", icon: <SearchOutlined />, width: 400 },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
    ],
  },
  "port-expenses": {
    menuoption: [
      { key: "voyage-home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "pl-summary",
        title: "PL Summary",
        icon: <LineChartOutlined />,
        width: 1100,
      },
      {
        key: "estimates-summary",
        title: "Summary Tree",
        icon: <FileDoneOutlined />,
        width: 400,
      },
      { key: "log", title: "Log", icon: <HistoryOutlined />, width: 400 },
      {
        key: "invoice",
        title: "Voyage Invoice List",
        icon: <FilePdfOutlined />,
        width: 400,
      },
      {
        key: "contact",
        title: "Voyage Contact List",
        icon: <ContactsOutlined />,
        width: 400,
      },
      {
        key: "proprties",
        title: "Properties",
        icon: <BuildOutlined />,
        width: 400,
      },
      {
        key: "position-report",
        title: "Position Report",
        icon: <PieChartOutlined />,
      },
      {
        key: "task-alert",
        title: "Task & alert",
        icon: <AlertOutlined />,
        width: 1150,
      },
      { key: "analytics", title: "Analytics", icon: <CodeSandboxOutlined /> },
    ],
  },
  "quick-estimate": {
    menuoption: [
      {
        key: "quick-estimate",
        title: "Quick-Estimate",
        icon: <MailOutlined />,
      },
    ],
  },

  "voyage-fixture-righttoolbar": {
    menuoption: [
      { key: "voyage-home", title: "Home", icon: <HomeOutlined /> },
      // { key: 'pl-summary', title: 'PL Summary', icon: <LineChartOutlined />, width: 1100 },
      // { key: 'summary', title: 'Summary Tree', icon: <FileDoneOutlined />, width: 400 },
      // { key: 'log', title: 'Log', icon: <HistoryOutlined />, width: 400 },
      // { key: 'invoice', title: 'Voyage Invoice List', icon: <FilePdfOutlined />, width: 400 },
      // { key: 'contact', title: 'Voyage Contact List', icon: <ContactsOutlined />, width: 400 },
      // { key: 'proprties', title: 'Properties', icon: <BuildOutlined />, width: 400 },
      // { key: 'position-report', title: 'Position Report', icon: 'pie-chart' },
      // { key: 'task-alert', title: 'Task & alert', icon: <AlertOutlined />, width: 1150 },
      // { key: 'analytics', title: 'Analytics', icon: 'code-sandbox' },
    ],
  },

  "voyage-cargo-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "summary",
        title: "Summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
    ],
  },

  "coa-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "summary",
        title: "Summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
        width: 400,
      },
    ],
  },

  "voyage-cargo-list-righttoolbar": {
    menuoption: [
      { key: "caret-up", title: "", icon: <CaretUpOutlined /> },
      { key: "filter", title: "", icon: <FilterOutlined /> },
    ],
  },

  "address-form-righttoolbar": {
    menuoption: [
      { key: "properties", title: "Properties", icon: <SettingOutlined /> },
    ],
  },

  "add-contractform-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "summary",
        title: "summary",
        icon: <FileTextOutlined />,
        width: 400,
      },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
      { key: "mail", title: "Mail", icon: <MailOutlined />, width: 1100 },
    ],
  },

  "open-vessel-schedule-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
    ],
  },

  "hire-schedule-righttoolbar": {
    menuoption: [
      { key: "filter", title: "Filter", icon: <FilterOutlined />, width: 600 },
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      {
        key: "hire-payment-list",
        title: "Hire Payment List",
        icon: <DiffOutlined />,
        width: 1100,
      },
      {
        key: "hire-issue-bill-list",
        title: "Hire Issue Bill List",
        icon: <UnorderedListOutlined />,
        width: 1100,
      },
      { key: "attachment", title: "Attachment", icon: <CloudUploadOutlined /> },
    ],
  },

  "finance-dashboard-righttoolbar": {
    menuoption: [
      { key: "home", title: "Cashflow", icon: <HomeOutlined />, width: 600 },
      {
        key: "portfolio-position",
        title: "Portfolio Position",
        icon: <DiffOutlined />,
        width: 1200,
      },
      {
        key: "tanker-pool-point",
        title: "Tanker Pool Point",
        icon: <VerticalAlignTopOutlined />,
      },
    ],
  },

  "report-analytics-righttoolbar": {
    menuoption: [
      { key: "home", title: "ome", icon: <HomeOutlined /> },
      { key: "filter", title: "Filter", icon: <FilterOutlined /> },
      { key: "setting", title: "Setting", icon: <SettingOutlined /> },
    ],
  },

  "vessel-file-righttoolbar": {
    menuoption: [
      { key: "home", title: "Home", icon: <HomeOutlined /> },
      // { key: 'summary', title: 'Summary', icon: <FileTextOutlined />, width: 400 },
      {
        key: "attachment",
        title: "Attachments",
        icon: <CloudUploadOutlined />,
      },
    ],
  },
};

export default RightBARCONFIGURATION;
