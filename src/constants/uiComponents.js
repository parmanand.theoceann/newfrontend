// optional `menuName` overrides default name for menu item if it's defined
// hideInMenu hides item in menu

const IMCS = [
  {
    name: 'IMCS - RegisterCompany',
    menuName: 'Register Own Company',
    desc: 'IMCS - Register Own Company',
    path: '/imcs/register-own-company'
  },
  {
    name: 'IMCS - RegisterBranchCompany',
    menuName: 'Register Branch Company',
    desc: 'IMCS - Register Branch Company',
    path: '/company/register-branch-company'
  },
  {
    name: 'IMCS - RegisterOwnBankDetails',
    menuName: 'Register Own Bank Details',
    desc: 'IMCS - Register Own Bank Details',
    path: '/vendor/register-bank'
  },
  {
    name: 'IMCS - RegisterVendorDetails',
    menuName: 'Register Vendor Details',
    desc: 'IMCS - Register Vendor Details',
    path: '/vendor/register-vendor'
  },
]


const USER_LIST = [
  {
    name: 'User List - Register  Department',
    menuName: 'Register  Department',
    desc: 'User List - Register  Department',
    path: '/users/register-department'
  }, {
    name: 'User List - Register Team-Group',
    menuName: 'Register Team-Group',
    desc: 'User List - Register Team-Group',
    path: '/users/register-team-group'
  }, {
    name: 'User List - Register User & Role',
    menuName: 'Register User & Role',
    desc: 'User List - Register User & Role',
    path: '/users/register-user-role'
  }
]


const EMAIL_ALERT_SETUP = [
  {
    name: 'Email alert list - Chartering Email Setup',
    menuName: 'Chartering Email Setup',
    desc: 'Email alert list - Chartering Email Setup',
    path: '/email-alert/chartering-email-setup'
  }, {
    name: 'Email alert list - Accounting Email Setup',
    menuName: 'Accounting Email Setup',
    desc: 'Email alert list - Accounting Email Setup',
    path: '/email-alert/accounting-email-setup'
  }, {
    name: 'Email alert list - Operational Email Setup',
    menuName: 'Operational Email Setup',
    desc: 'Email alert list - Operational Email Setup',
    path: '/email-alert/operational-email-setup'
  }
]

const EMAIL_ALERT = [
  {
    name: 'Email Alert Setup',
    menuName: 'Email Alert Setup',
    desc: 'Email Alert List - Email Alert Setup',
    path: '/email-alert/email-setup',
    children: EMAIL_ALERT_SETUP
  }
]


// Menu for Operation sub option
const POSITIONREPORTS = [
  {
    name: 'Departure Report',
    menuName: 'Departure Report',
    desc: 'Position Report - Report Type',
    path: '/operation/position-report/departure-report',
  },
  {
    name: 'COSP',
    menuName: 'COSP',
    desc: 'Report Type - COSP',
    path: '/operation/position-report/cosp',
  },
  {
    name: 'Noon Report',
    menuName: 'Noon Report',
    desc: 'Report Type - Noon Report',
    path: '/operation/position-report/noon-report',
  },
  {
    name: 'Stop at Sea',
    menuName: 'Stop at Sea',
    desc: 'Report Type - Stop at Sea',
    path: '/operation/position-report/stop-at-sea',
  },
  {
    name: 'Fuel change',
    menuName: 'Fuel change',
    desc: 'Report Type - Fuel change',
    path: '/operation/position-report/fuel-change',
  },
  {
    name: 'EOSP',
    menuName: 'EOSP',
    desc: 'Report Type - EOSP',
    path: '/operation/position-report/eosp',
  },
  {
    name: 'Anchor',
    menuName: 'Anchor',
    desc: 'Report Type - Anchor',
    path: '/operation/position-report/anchor',
  },
  {
    name: 'Shifting',
    menuName: 'Shifting',
    desc: 'Report Type - Shifting',
    path: '/operation/position-report/shifting',
  },
  {
    name: 'Drifting',
    menuName: 'Drifting',
    desc: 'Report Type - Drifting',
    path: '/operation/position-report/drifting',
  },
  {
    name: 'Arrival',
    menuName: 'Arrival',
    desc: 'Report Type - Arrival',
    path: '/operation/position-report/arrival',
  },
  {
    name: 'In port report',
    menuName: 'In port report',
    desc: 'Report Type - In port report',
    path: '/operation/position-report/in-port',
  },
];


const MYPORTCALLS_NEW = [
  {
    name: 'Operation - My Portcalls - Create Agency Appointment',
    menuName: 'Create Agency Appointment',
    desc: 'Operation - My Portcalls - Create Agency Appointment',
    path: '/operation/my-portcalls/agency-appointment'
  },
  {
    name: 'Operation - My Portcalls - Myportcall Dashboard',
    menuName: 'Myportcall Dashboard',
    desc: 'Operation - My Portcalls - Myportcall Dashboard',
    path: '/operation/my-portcalls/myportcall-dashboard'
  },
  {
    name: 'Operation - My Portcalls - Advance/Performa DA',
    menuName: 'Advance/Performa DA',
    desc: 'Operation - My Portcalls - Advance/Performa DA',
    path: '/operation/my-portcalls/performa-da'
  },
  {
    name: 'Operation - My Portcalls - Final Disbursement Amount',
    menuName: 'Final Disbursement Amount',
    desc: 'Operation - My Portcalls - Final Disbursement Amount',
    path: '/operation/my-portcalls/final-disbursement-amount'
  },
  {
    name: 'Operation - My Portcalls - Other Vendors Payment',
    menuName: 'Other Vendors Payment',
    desc: 'Operation - My Portcalls - Other Vendors Payment',
    path: '/operation/my-portcalls/other-vendors-payment'
  },
  {
    name: 'Operation - My Portcalls - Port Activity',
    menuName: 'Port Activity',
    desc: 'Operation - My Portcalls - Port Activity',
    path: '/hire-payment/my-portcall/port-activity'
  },
  {
    name: 'Operation - My Portcalls - Port Performance',
    menuName: 'Port Performance',
    desc: 'Operation - My Portcalls - Port Performance',
    path: '/hire-payment/my-portcalls/port-performance'
  },
  {
    name: 'Operation - My Portcalls - New Appointment',
    menuName: 'New Appointment',
    desc: 'Operation - My Portcalls - New Appointment',
    path: '/hire-payment/my-portcalls/new-appointment'
  },
  {
    name: 'Operation - My Portcalls - Portcall List',
    menuName: 'Portcall List',
    desc: 'Operation - My Portcalls - Portcall List',
    path: '/hire-payment/my-portcalls/portcall-list'
  },
  {
    name: 'Operation - My Portcalls - PDA List',
    menuName: 'PDA List',
    desc: 'Operation - My Portcalls - PDA List',
    path: '/hire-payment/my-portcalls/pda-list'
  },
  {
    name: 'Operation - My Portcalls - FDA List',
    menuName: 'FDA List',
    desc: 'Operation - My Portcalls - FDA List',
    path: '/hire-payment/my-portcalls/fda-list'
  },
  {
    name: 'Operation - My Portcalls - Status List',
    menuName: 'Status List',
    desc: 'Operation - My Portcalls - Status List',
    path: '/hire-payment/my-portcalls/status-list'
  },
]

const VSOP_NEW = [
  {
    name: 'Operation - VSOP - Sealink Reporting Form',
    menuName: 'Sealink Reporting Form',
    desc: 'Operation - VSOP - Sealink Reporting Form',
    path: '/hire-payment/vsop/sealink-reporting-form'
  },
  {
    name: 'Operation - VSOP - Vessel Mapping List',
    menuName: 'Vessel Mapping List',
    desc: 'Operation - VSOP - Vessel Mapping List',
    path: '/hire-payment/vsop/vessel-mapping-list'
  },
  {
    name: 'Operation - VSOP - Vessel ECO Performance Matrix Reports & Analytics',
    menuName: 'Vessel ECO Performance Matrix Reports & Analytics',
    desc: 'Operation - VSOP - Vessel ECO Performance Matrix Reports & Analytics',
    path: '/hire-payment/vsop/vessel-eco-performance-matrix-reports-analytics'
  },
]

const PERFORMANCEREPORT_NEW = [
  {
    name: 'Operation - Performance Report - Voyage Performance Report',
    menuName: 'Voyage Performance Report',
    desc: 'Operation - Performance Report - Voyage Performance Report',
    path: '/hire-payment/performance-report/voyage-performance-report'
  },
  {
    name: 'Operation - Performance Report - Voyage P & L Report',
    menuName: 'Voyage P & L Report',
    desc: 'Operation - Performance Report - Voyage P & L Report',
    path: '/hire-payment/performance-report/voyage-pl-report'
  },
  {
    name: 'Operation - Performance Report - Bunker Summary Report',
    menuName: 'Bunker Summary Report',
    desc: 'Operation - Performance Report - Bunker Summary Report',
    path: '/hire-payment/performance-report/bunker-summary-report'
  },
  {
    name: 'Operation - Performance Report - Demurage Report',
    menuName: 'Demurage Report',
    desc: 'Operation - Performance Report - Demurage Report',
    path: '/hire-payment/performance-report/demurrage-report'
  },
  {
    name: 'Operation - Performance Report - Port Expenses Summary',
    menuName: 'Port Expenses Summary',
    desc: 'Operation - Performance Report - Port Expenses Summary',
    path: '/hire-payment/performance-report/port-expenses-summary'
  },
  {
    name: 'Operation - Performance Report - Port Activity Report',
    menuName: 'Port Activity Report',
    desc: 'Operation - Performance Report - Port Activity Report',
    path: '/hire-payment/performance-report/port-activity-report'
  },
  {
    name: 'Operation - Performance Report - Delay Reports',
    menuName: 'Delay Reports',
    desc: 'Operation - Performance Report - Delay Reports',
    path: '/hire-payment/performance-report/delay-reports'
  },
];

const OPERATIONS_New = [
  {
    name: 'Position Report',
    menuName: 'Position Report',
    desc: 'Operations - Position Report',
    path: '/operation/position-report',
    children: POSITIONREPORTS
  },

  {
    name: 'Port Management',
    menuName: 'Port Management',
    desc: 'Operations - Port Management',
    path: '/operation/port-management',
    children: MYPORTCALLS_NEW
  }

]
// End: Menu for operation sub option

// Bunker sub menus start

const BUNKERS_menu = [

  {
    name: 'Bunker Purchase List ',
    menuName: 'Bunker Purchase List ',
    path: '/bunker-purchase-list',
  },

  {
    name: 'Bunker Requirement List ',
    menuName: 'Bunker Requirement List ',
    path: '/bunker-requirement-list',
  },

  {
    name: 'Bunker Invoice List',
    menuName: 'Invoice List',
    path: '/bunker-invoice-list',
  },

]

// Bunker sub menus End


// Menu for Chartring sub option 

const CHARTERINGS = [
  {
    name: 'TCOV-Est',
    menuName: 'TCOV-Est',
    desc: 'Chartering - TCOV estimate',
    path: '/chartering/tcov',
    // children: TCOV_EST
  },
  {
    name: 'TCTO-Est.',
    menuName: 'TCTO-Est.',
    desc: 'Chartering - TCTO-Est.',
    path: '/chartering/tcto',
    // children: TCTO_EST
  },
  {
    name: 'VCI-VCO Est.',
    menuName: 'VCI-VCO Est.',
    desc: 'Chartering - VCI-VCO Est.',
    // path: '/estimates/vci-vco-est',
    path: '/chartering/vci-vco-est',
  },
  {
    name: 'TCI-Est.',
    menuName: 'TCI-Est.',
    desc: 'Chartering - TCI-Est.',
    path: '/chartering/tci',
  },
  {
    name: 'Cargo Contract',
    menuName: 'Cargo Contract',
    desc: 'Chartering - Cargo Contract',
    // path: '/cargo-estimate/cargo-contract',
    path: '/add-cargo-contract',
  },
  {
    name: 'COA Contract',
    menuName: 'COA Contract',
    desc: 'Chartering - COA Contract',
    // path: '/coas-list/coa-contract',
    path: '/coa-contract',
  },
  {
    name: 'VC IN',
    menuName: 'Voyage cargo (IN) ',
    desc: 'Chartering - VC IN',
    // path: '/tc-in-list/vc-in',
    path: '/chartering/vci',
  },
  {
    name: 'TCO-Manager',
    menuName: 'TCO-Manager',
    desc: 'Chartering - TCO-Manager',
    path: '/chartering/tco',
  },
  {
    name: 'Voyage Fixture Form',
    desc: 'Voyage Fixture Form',
    path: '/chartering/voyage-fixture-form'
  },

  {
    name: 'COA (VCI) form',
    desc: 'COA (VCI) form',
    path: '/chartering/coa-vci'
  },

  {
    name: 'Paper Trade',
    menuName: 'Paper Trade',
    desc: 'Chartering - Paper Trade',
    // path: '/vci-VCO/paper-trade',
    path: '/chartering/paper-trade',
  },
  {
    name: 'Trade Data',
    menuName: 'Trade Data',
    desc: 'Chartering - Trade Data',
    path: '/chartering/trade-data',
  },
  {
    name: 'Market Bunker data',
    menuName: 'Market Bunker data',
    desc: 'Chartering - Market Bunker data',
    path: '/chartering/market-bunker-data',
  },
  {
    name: 'P&L Estimate Page',
    desc: 'P&L Eestimate Page',
    // path: '/chartering/tcov-est/pl-estimate-page'
    path: '/chartering/pl-estimate-page'
  },

];



const ACCOUNT_RULE = [
  {
    name: 'Account Rule - Charts of Accounts List',
    menuName: 'Charts of Accounts List',
    desc: 'Account Rule - Charts of Accounts List',
    path: '/account-rule/chart-account-list'
  }, {
    name: 'Account Rule - Account Period',
    menuName: 'Account Period',
    desc: 'Account Rule - Account Period',
    path: '/account-rule/account-period'
  }, {
    name: 'Account Rule - Line of Business (rules) List.',
    menuName: 'Line of Business (rules) List.',
    desc: 'Account Rule - Line of Business (rules) List.',
    path: '/account-rule/business-list'
  }
]

const INVOICE_LIST = [
  {
    name: 'Invoice List - Create Invoices',
    menuName: 'Create Invoices',
    desc: 'Invoice List - Create Invoices',
    path: '/invoice-list/create-invoices'
  }, {
    name: 'Invoice List - Edit Invoices',
    menuName: 'Edit Invoices',
    desc: 'Invoice List - Edit Invoices',
    path: '/invoice-list/edit-invoices'
  }, {
    name: 'Invoice List - Approve or Reject Invoices',
    menuName: 'Approve or Reject Invoices',
    desc: 'Invoice List - Approve or Reject Invoices',
    path: '/invoice-list/approve-reject-invoices'
  }, {
    name: 'Invoice List - Post Invoices',
    menuName: 'Post Invoices',
    desc: 'Invoice List - Post Invoices',
    path: '/invoice-list/post-invoices'
  }, {
    name: 'Invoice List - Transaction Data Entry',
    menuName: 'Transaction Data Entry',
    desc: 'Invoice List - Transaction Data Entry',
    path: '/invoice-list/transaction-entry'
  }, {
    name: 'Invoice List - Payments and Receipts',
    menuName: 'Payments and Receipts',
    desc: 'Invoice List - Payments and Receipts',
    path: '/invoice-list/payment-receipt'
  }, {
    name: 'Invoice List - Journal Entries',
    menuName: 'Journal Entries',
    desc: 'Invoice List - Journal Entries',
    path: '/invoice-list/journal-entries'
  }, {
    name: 'Invoice List - Monthly Accruals',
    menuName: 'Monthly Accruals',
    desc: 'Invoice List - Monthly Accruals',
    path: '/invoice-list/monthly-accruals'
  }
]

const HIRE_INVOICE = [
  {
    name: 'Hire Invoice - Hire Payable Invoice Summary',
    menuName: 'Hire Payable Invoice Summary',
    desc: 'Hire Invoice - Hire Payable Invoice Summary',
    path: '/hire-invoice/hire-payable-summary'
  }, {
    name: 'Hire Invoice - Hire Receivable Invoice Summary',
    menuName: 'Hire Receivable Invoice Summary',
    desc: 'Hire Invoice - Hire Receivable Invoice Summary',
    path: '/hire-invoice/hire-receivable-summary'
  }
]

const BUNKER_VENDOR = [
  {
    name: 'Bunker Vendor - Bunker Invoice Created List',
    menuName: 'Bunker Invoice Created List',
    desc: 'Bunker Vendor - Bunker Invoice Created List',
    path: '/bunker-vendors/bunker-invoice-created'
  }, {
    name: 'Bunker Vendor - Bunker Invoice Posted List',
    menuName: 'Bunker Invoice Posted List',
    desc: 'Bunker Vendor - Bunker Invoice Posted List',
    path: '/bunker-vendors/bunker-invoice-posted'
  }, {
    name: 'Bunker Vendor - Bunker Invoice Transaction Summary',
    menuName: 'Bunker Invoice Transaction Summary',
    desc: 'Bunker Vendor - Bunker Invoice Transaction Summary',
    path: '/bunker-vendors/bunker-invoice-transaction'
  }
]

const VENDOR_TRANSACTION = [
  {
    name: 'Vendor Transaction - PDA/Advance Invoice Created List',
    menuName: 'PDA/Advance Invoice Created List',
    desc: 'Vendor Transaction - PDA/Advance Invoice Created List',
    path: '/vendor-transaction/pda-advance-invoice-created'
  }, {
    name: 'Vendor Transaction - FDA Invoice Created List',
    menuName: 'FDA Invoice Created List',
    desc: 'Vendor Transaction - FDA Invoice Created List',
    path: '/vendor-transaction/fda-invoice-created'
  }, {
    name: 'Vendor Transaction - PDA/Advance Invoice Approved List',
    menuName: 'PDA/Advance Invoice Approved List',
    desc: 'Vendor Transaction - PDA/Advance Invoice Approved List',
    path: '/vendor-transaction/pda-advance-approved'
  }, {
    name: 'Vendor Transaction - FDA Invoice Approved List',
    menuName: 'FDA Invoice Approved List',
    desc: 'Vendor Transaction - FDA Invoice Approved List',
    path: '/vendor-transaction/fda-invoice-approved'
  }, {
    name: 'Vendor Transaction - DA Receivable List',
    menuName: 'DA Receivable List',
    desc: 'Vendor Transaction - DA Receivable List',
    path: '/vendor-transaction/da-receivable'
  }
]

const TC_IN = [
  {
    name: 'TC-Voyage List',
    desc: 'TC-Voyage List',
    path: '/hire-payment/tc-in/voyager'
  }, {
    name: 'TCOV-VM list',
    desc: 'TCOV-VM list',
    path: '/hire-payment/tc-in/vm'
  }, {
    name: 'TC Hire Payment Schedule',
    desc: 'TC Hire Payment Schedule',
    path: '/hire-payment/tc-in/payment-schedule'
  }, {
    name: 'TC Hire Paid List',
    desc: 'TC Hire Paid List',
    path: '/hire-payment/tc-in/hire-paid'
  }, {
    name: 'Commission List',
    desc: 'Commission List',
    path: '/hire-payment/tc-in/commission'
  }, {
    name: 'Commission Invoice',
    desc: 'Commission Invoice',
    path: '/hire-payment/tc-in/commission-invoice-tc'
  }
];

const TC_OUT = [
  {
    name: 'TC Out List',
    desc: 'TC Out List',
    path: '/hire-payment/tc-out/list'
  }, {
    name: 'TC Billing List',
    desc: 'TC Billing List',
    path: '/hire-payment/tc-out/billing'
  }, {
    name: 'Statement',
    desc: 'Statement',
    path: '/hire-payment/tc-out/statement'
  }, {
    name: 'Commission List',
    desc: 'Commission List',
    path: '/hire-payment/tc-out/commission'
  }
];

const BUNKERS_NEW = [
  {
    name: 'Operation - Bunkers - Bunker Planning List',
    menuName: 'Bunker Planning List',
    desc: 'Operation - Bunkers - Bunker Planning List',
    path: '/hire-payment/bunker/bunker-planning-list'
  },
  {
    name: 'Operation - Bunkers - Bunker Purchased List',
    menuName: 'Bunker Purchased List',
    desc: 'Operation - Bunkers - Bunker Purchased List',
    path: '/hire-payment/bunker/bunker-purchased-list'
  },
  {
    name: 'Operation - Bunkers - Bunker Invoice',
    menuName: 'Bunker Invoice',
    desc: 'Operation - Bunkers - Bunker Invoice',
    path: '/hire-payment/bunker/bunker-invoice'
  },
  {
    name: 'Operation - Bunkers - Bunker Status List',
    menuName: 'Bunker Status List',
    desc: 'Operation - Bunkers - Bunker Status List',
    path: '/hire-payment/bunker/bunker-status-list'
  },
  {
    name: 'Operation - Bunkers - Vendors Performance Analytics',
    menuName: 'Vendors Performance Analytics',
    desc: 'Operation - Bunkers - Vendors Performance Analytics',
    path: '/hire-payment/bunker/vendors-performance-analytics'
  },
  {
    name: 'Operation - Bunkers - Market Data',
    menuName: 'Market Data',
    desc: 'Operation - Bunkers - Market Data',
    path: '/hire-payment/bunker/market-data'
  },
  {
    name: 'Operation - Bunkers - Bunker Planning',
    menuName: 'Bunker Planning',
    desc: 'Operation - Bunkers - Bunker Planning',
    path: '/hire-payment/bunker/bunker-planning'
  },
  {
    name: 'Operation - Bunkers - Bunker Requirements',
    menuName: 'Bunker Requirements',
    desc: 'Operation - Bunkers - Bunker Requirements',
    path: '/hire-payment/bunker/bunker-requirements'
  },
]



const HIRE_SETUP = [
  {
    name: 'Vendor Transaction - Operational Task set up',
    menuName: 'Operational Task set up',
    desc: 'Vendor Transaction - Operational Task set up',
    path: '/hire-payment/set-up/task-setup'
  }, {
    name: 'Vendor Transaction - Operational Email set up',
    menuName: 'Operational Email set up',
    desc: 'Vendor Transaction - Operational Email set up',
    path: '/hire-payment/set-up/email-setup'
  }
]

const HIRE_PAYMENT_LIST = [
  {
    name: 'TC In',
    menuName: 'TC In',
    desc: 'Hire Payment List - TC In',
    path: '/hire-payment/tc-in',
    children: TC_IN
  }, {
    name: 'TC Out',
    menuName: 'TC Out',
    desc: 'Hire Payment List - TC Out',
    path: '/hire-payment/tc-out',
    children: TC_OUT
  }, {
    name: 'Bunker',
    menuName: 'Bunker',
    desc: 'Hire Payment List - Bunker',
    path: '/hire-payment/bunker',
    children: BUNKERS_NEW
  }, {
    name: 'My portcalls',
    menuName: 'My portcalls',
    desc: 'Hire Payment List - My portcalls',
    path: '/hire-payment/my-portcalls',
    children: MYPORTCALLS_NEW
  }, {
    name: 'VSOP',
    menuName: 'VSOP',
    desc: 'Hire Payment List - VSOP',
    path: '/hire-payment/vsop',
    children: VSOP_NEW
  }, {
    name: 'Performance report',
    menuName: 'Performance report',
    desc: 'Hire Payment List - Performance report',
    path: '/hire-payment/performance-report',
    children: PERFORMANCEREPORT_NEW
  }, {
    name: 'Set Up',
    menuName: 'Set Up',
    desc: 'Hire Payment List - Set Up',
    path: '/hire-payment/set-up',
    children: HIRE_SETUP
  }
]

const DEMURRAGE_NEW = [
  {
    name: 'Track Fleet - Demurrage - Laytime Form',
    menuName: 'Laytime Form',
    desc: 'Track Fleet - Demurrage - Laytime Form',
    path: '/track-fleet/demurrage/laytime-form'
  },
  {
    name: 'Track Fleet - Demurrage - Claim List',
    menuName: 'Claim List',
    desc: 'Track Fleet - Demurrage - Claim List',
    path: '/track-fleet/demurrage/claim'
  },
  {
    name: 'Track Fleet - Demurrage - Claim Dashboard',
    menuName: 'Claim Dashboard',
    desc: 'Track Fleet - Demurrage - Claim Dashboard',
    path: '/track-fleet/demurrage/claim-dashboard'
  },
  {
    name: 'Track Fleet - Demurrage - Laytime Invoice',
    menuName: 'Laytime Invoice',
    desc: 'Track Fleet - Demurrage - Laytime Invoice',
    path: '/track-fleet/demurrage/laytime-desk'
  },
]

const TRACK_FLEET = [
  {
    name: 'Demurrage',
    menuName: 'Demurrage',
    desc: 'Track Fleet - Demurrage',
    path: '/track-fleet/demurrage',
    children: DEMURRAGE_NEW
  }
]



export const CHARTERING = [
  {
    name: 'Chartering - TCOV',
    menuName: 'TCOV',
    desc: 'Chartering - TCOV',
    path: '/chartering/tcov'
  },
  {
    name: 'Chartering - TCI',
    menuName: 'TCI',
    desc: 'Chartering - TCI',
    path: '/chartering/tci'
  },
  {
    name: 'Chartering - TCO',
    menuName: 'TCO',
    desc: 'Chartering - TCO',
    path: '/chartering/tco'
  },
  {
    name: 'Chartering - Bunker',
    menuName: 'Bunker',
    desc: 'Chartering - Bunker',
    path: '/chartering/bunker'
  },
  {
    name: 'Chartering - Cargo',
    menuName: 'Cargo',
    desc: 'Chartering - Cargo',
    path: '/chartering/cargo'
  },
  {
    name: 'Chartering - Estimate',
    menuName: 'Estimate',
    desc: 'Chartering - Estimate',
    path: '/chartering/estimate'
  },
  {
    name: 'Chartering - VCI',
    menuName: 'VCI',
    desc: 'Chartering - VCI',
    path: '/chartering/vci'
  },
  {
    name: 'Chartering - TCTO',
    menuName: 'TCTO',
    desc: 'Chartering - TCTO',
    path: '/chartering/tcto'
  },
]

export const FREIGHT = [
  {
    name: 'Operation - Freight - Initial Freight Invoice',
    menuName: 'Initial Freight Invoice',
    desc: 'Operation - Freight - Initial Freight Invoice',
    path: '/operation/freight/initial-freight-invoice'
  },
  {
    name: 'Operation - Freight - Final Freight Invoice',
    menuName: 'Final Freight Invoice',
    desc: 'Operation - Freight - Final Freight Invoice',
    path: '/operation/freight/final-freight-invoice'
  },
  {
    name: 'Operation - Freight - Commission Invoice',
    menuName: 'Commission Invoice',
    desc: 'Operation - Freight - Commission Invoice',
    path: '/operation/freight/commission-invoice'
  },
  {
    name: 'Operation - Freight - Freight Receivable List',
    menuName: 'Freight Receivable List',
    desc: 'Operation - Freight - Freight Receivable List',
    path: '/operation/freight/freight-receivable-list'
  },
]

export const DEMURRAGE = [
  {
    name: 'Operation - Demurrage - Laytime Calculator',
    menuName: 'Laytime Calculator',
    desc: 'Operation - Demurrage - Laytime Calculator',
    path: '/operation/demurrage/laytime-calculator'
  },
  {
    name: 'Operation - Demurrage - Claim Dashboard',
    menuName: 'Claim Dashboard',
    desc: 'Operation - Demurrage - Claim Dashboard',
    path: '/operation/demurrage/claim-dashboard'
  },
  {
    name: 'Operation - Demurrage - Claim Payable Claim Status',
    menuName: 'Claim Payable Claim Status',
    desc: 'Operation - Demurrage - Claim Payable Claim Status',
    path: '/operation/demurrage/claim-payable-claim-status'
  },
  {
    name: 'Operation - Demurrage - Laytime Desk',
    menuName: 'Laytime Desk',
    desc: 'Operation - Demurrage - Laytime Desk',
    path: '/operation/demurrage/laytime-desk'
  },
]

export const TIMECHARTERERIN = [
  {
    name: 'Operation - Time Charterer In - TC List',
    menuName: 'TC List',
    desc: 'Operation - Time Charterer In - TC List',
    path: '/operation/time-charterer-in/tc-list'
  },
  {
    name: 'Operation - Time Charterer In - TC Hire Payment Schedule List',
    menuName: 'TC Hire Payment Schedule List',
    desc: 'Operation - Time Charterer In - TC Hire Payment Schedule List',
    path: '/operation/time-charterer-in/tc-hire-payment-schedule-list'
  },
  {
    name: 'Operation - Time Charterer In - TC Payment',
    menuName: 'TC Payment',
    desc: 'Operation - Time Charterer In - TC Payment',
    path: '/operation/time-charterer-in/tc-payment'
  },
  {
    name: 'Operation - Time Charterer In - PHS/FHS Statement',
    menuName: 'PHS/FHS Statement',
    desc: 'Operation - Time Charterer In - PHS/FHS Statement',
    path: '/operation/time-charterer-in/phs-fhs-statement'
  },
  {
    name: 'Operation - Time Charterer In - Commission',
    menuName: 'Commission',
    desc: 'Operation - Time Charterer In - Commission',
    path: '/operation/time-charterer-in/commission'
  },
  {
    name: 'Operation - Time Charterer In - Owner Commission List',
    menuName: 'Owner Commission List',
    desc: 'Operation - Time Charterer In - Owner Commission List',
    path: '/operation/time-charterer-in/owner-commission-list'
  },
]

export const TIMECHARTEREROUT = [
  {
    name: 'Operation - Time Charterer Out - Time Charter Out List',
    menuName: 'Time Charter Out List',
    desc: 'Operation - Time Charterer Out - Time Charter Out List',
    path: '/operation/time-charterer-out/time-charter-out-list'
  },
  {
    name: 'Operation - Time Charterer Out - TC Billing List',
    menuName: 'TC Billing List',
    desc: 'Operation - Time Charterer Out - TC Billing List',
    path: '/operation/time-charterer-out/tc-billing-list'
  },
  {
    name: 'Operation - Time Charterer Out - Commission',
    menuName: 'Commission',
    desc: 'Operation - Time Charterer Out - Commission',
    path: '/operation/time-charterer-out/commission'
  },
  {
    name: 'Operation - Time Charterer Out - Hire Statement',
    menuName: 'Hire Statement',
    desc: 'Operation - Time Charterer Out - Hire Statement',
    path: '/operation/time-charterer-out/hire-statement'
  },
]

export const BUNKERS = [
  {
    name: 'Operation - Bunkers - Bunker Planning List',
    menuName: 'Bunker Planning List',
    desc: 'Operation - Bunkers - Bunker Planning List',
    path: '/operation/bunkers/bunker-planning-list'
  },
  {
    name: 'Operation - Bunkers - Bunker Purchased List',
    menuName: 'Bunker Purchased List',
    desc: 'Operation - Bunkers - Bunker Purchased List',
    path: '/operation/bunkers/bunker-purchased-list'
  },
  {
    name: 'Operation - Bunkers - Bunker Invoice',
    menuName: 'Bunker Invoice',
    desc: 'Operation - Bunkers - Bunker Invoice',
    path: '/operation/bunkers/bunker-invoice'
  },
  {
    name: 'Operation - Bunkers - Bunker Status List',
    menuName: 'Bunker Status List',
    desc: 'Operation - Bunkers - Bunker Status List',
    path: '/operation/bunkers/bunker-status-list'
  },
  {
    name: 'Operation - Bunkers - Vendors Performance Analytics',
    menuName: 'Vendors Performance Analytics',
    desc: 'Operation - Bunkers - Vendors Performance Analytics',
    path: '/operation/bunkers/vendors-performance-analytics'
  },
  {
    name: 'Operation - Bunkers - Market Data',
    menuName: 'Market Data',
    desc: 'Operation - Bunkers - Market Data',
    path: '/operation/bunkers/market-data'
  },
  {
    name: 'Operation - Bunkers - Bunker Planning',
    menuName: 'Bunker Planning',
    desc: 'Operation - Bunkers - Bunker Planning',
    path: '/operation/bunkers/bunker-planning'
  },
  {
    name: 'Operation - Bunkers - Bunker Requirements',
    menuName: 'Bunker Requirements',
    desc: 'Operation - Bunkers - Bunker Requirements',
    path: '/operation/bunkers/bunker-requirements'
  },
]

export const MYPORTCALLS = [
  {
    name: 'Operation - My Portcalls - Create Agency Appointment',
    menuName: 'Create Agency Appointment',
    desc: 'Operation - My Portcalls - Create Agency Appointment',
    path: '/operation/my-portcalls/agency-appointment'
  },
  {
    name: 'Operation - My Portcalls - Myportcall Dashboard',
    menuName: 'Myportcall Dashboard',
    desc: 'Operation - My Portcalls - Myportcall Dashboard',
    path: '/operation/my-portcalls/agency-appointment'
  },
  {
    name: 'Operation - My Portcalls - Advance/Performa DA',
    menuName: 'Advance/Performa DA',
    desc: 'Operation - My Portcalls - Advance/Performa DA',
    path: '/operation/my-portcalls/performa-da'
  },
  {
    name: 'Operation - My Portcalls - Final Disbursement Amount',
    menuName: 'Final Disbursement Amount',
    desc: 'Operation - My Portcalls - Final Disbursement Amount',
    path: '/operation/my-portcalls/final-disbursement-amount'
  },
  {
    name: 'Operation - My Portcalls - Other Vendors Payment',
    menuName: 'Other Vendors Payment',
    desc: 'Operation - My Portcalls - Other Vendors Payment',
    path: '/operation/my-portcalls/other-vendors-payment'
  },
  {
    name: 'Operation - My Portcalls - Port Activity',
    menuName: 'Port Activity',
    desc: 'Operation - My Portcalls - Port Activity',
    path: '/operation/my-portcalls/port-activity'
  },
  {
    name: 'Operation - My Portcalls - Port Performance',
    menuName: 'Port Performance',
    desc: 'Operation - My Portcalls - Port Performance',
    path: '/operation/my-portcalls/port-performance'
  },
  {
    name: 'Operation - My Portcalls - New Appointment',
    menuName: 'New Appointment',
    desc: 'Operation - My Portcalls - New Appointment',
    path: '/operation/my-portcalls/new-appointment'
  },
  {
    name: 'Operation - My Portcalls - Portcall List',
    menuName: 'Portcall List',
    desc: 'Operation - My Portcalls - Portcall List',
    path: '/operation/my-portcalls/portcall-list'
  },
  // {
  //   name: 'Operation - My Portcalls - Bunker Invoice',
  //   menuName: 'Bunker Invoice',
  //   desc: 'Operation - My Portcalls - Bunker Invoice',
  //   path: '/operation/my-portcalls/bunker-invoice'
  // },
  {
    name: 'Operation - My Portcalls - PDA List',
    menuName: 'PDA List',
    desc: 'Operation - My Portcalls - PDA List',
    path: '/operation/my-portcalls/pda-list'
  },
  {
    name: 'Operation - My Portcalls - FDA List',
    menuName: 'FDA List',
    desc: 'Operation - My Portcalls - FDA List',
    path: '/operation/my-portcalls/fda-list'
  },
  {
    name: 'Operation - My Portcalls - Status List',
    menuName: 'Status List',
    desc: 'Operation - My Portcalls - Status List',
    path: '/operation/my-portcalls/status-list'
  },
]

export const VSOP = [
  {
    name: 'Operation - VSOP - Sealink Reporting Form',
    menuName: 'Sealink Reporting Form',
    desc: 'Operation - VSOP - Sealink Reporting Form',
    path: '/operation/vsop/sealink-reporting-form'
  },
  {
    name: 'Operation - VSOP - Vessel Mapping List',
    menuName: 'Vessel Mapping List',
    desc: 'Operation - VSOP - Vessel Mapping List',
    path: '/operation/vsop/vessel-mapping-list'
  },
  {
    name: 'Operation - VSOP - Vessel ECO Performance Matrix Reports & Analytics',
    menuName: 'Vessel ECO Performance Matrix Reports & Analytics',
    desc: 'Operation - VSOP - Vessel ECO Performance Matrix Reports & Analytics',
    path: '/operation/vsop/vessel-eco-performance-matrix-reports-analytics'
  },
]

export const PERFORMANCEREPORT = [
  {
    name: 'Operation - Performance Report - Voyage Performance Report',
    menuName: 'Voyage Performance Report',
    desc: 'Operation - Performance Report - Voyage Performance Report',
    path: '/operation/performance-report/voyage-performance-report'
  },
  {
    name: 'Operation - Performance Report - Voyage P & L Report',
    menuName: 'Voyage P & L Report',
    desc: 'Operation - Performance Report - Voyage P & L Report',
    path: '/operation/performance-report/voyage-pl-report'
  },
  {
    name: 'Operation - Performance Report - Bunker Summary Report',
    menuName: 'Bunker Summary Report',
    desc: 'Operation - Performance Report - Bunker Summary Report',
    path: '/operation/performance-report/bunker-summary-report'
  },
  {
    name: 'Operation - Performance Report - Demurage Report',
    menuName: 'Demurage Report',
    desc: 'Operation - Performance Report - Demurage Report',
    path: '/operation/performance-report/demurrage-report'
  },
  {
    name: 'Operation - Performance Report - Port Expenses Summary',
    menuName: 'Port Expenses Summary',
    desc: 'Operation - Performance Report - Port Expenses Summary',
    path: '/operation/performance-report/port-expenses-summary'
  },
  {
    name: 'Operation - Performance Report - Port Activity Report',
    menuName: 'Port Activity Report',
    desc: 'Operation - Performance Report - Port Activity Report',
    path: '/operation/performance-report/port-activity-report'
  },
  {
    name: 'Operation - Performance Report - Delay Reports',
    menuName: 'Delay Reports',
    desc: 'Operation - Performance Report - Delay Reports',
    path: '/operation/performance-report/delay-reports'
  },
]

export const SETUP = [
  {
    name: 'Operation - Setup - Agency Appointment',
    menuName: 'Agency Appointment',
    desc: 'Operation - Setup - Agency Appointment',
    path: '/operation/setup/agency-appointment'
  },
  {
    name: 'Operation - Setup - Alert Setup',
    menuName: 'Alert Setup',
    desc: 'Operation - Setup - Alert Setup',
    path: '/operation/setup/alert-setup'
  },
  {
    name: 'Operation - Setup - Operation Task Setup',
    menuName: 'Operation Task Setup',
    desc: 'Operation - Setup - Operation Task Setup',
    path: '/operation/setup/operation-task-setup'
  },
  {
    name: 'Operation - Setup - Delivery-Redelivery Setup',
    menuName: 'Delivery-Redelivery Setup',
    desc: 'Operation - Setup - Delivery-Redelivery Setup',
    path: '/operation/setup/delivery-redelivery-setup'
  },
  {
    name: 'Operation - Setup - Payment Setup',
    menuName: 'Payment Setup',
    desc: 'Operation - Setup - Payment Setup',
    path: '/operation/setup/payment-setup'
  },
  {
    name: 'Operation - Setup - Email Alert Setup',
    menuName: 'Email Alert Setup',
    desc: 'Operation - Setup - Email Alert Setup',
    path: '/operation/setup/email-alert-setup'
  },
]

export const CARDS = [
  {
    name: 'Card - Image Cards',
    menuName: 'Image Cards',
    desc: 'Card component for displaying image and related content',
    path: '/card/image-cards'
  },
  {
    name: 'Card - Form Cards',
    menuName: 'Form Cards',
    desc: 'Card component for displaying form content',
    path: '/card/form-cards'
  },
  {
    name: 'Card - Blog Cards (Grid)',
    menuName: 'Blog Cards (Grid)',
    desc: 'Card component for displaying blog content',
    path: '/card/blog-cards-grid'
  },
  {
    name: 'Card - Blog Cards (List)',
    menuName: 'Blog Cards (List)',
    desc: 'Card component for displaying blog content',
    path: '/card/blog-cards-list'
  },
  {
    name: 'Card - Number Cards',
    menuName: 'Number Cards',
    desc: 'Card component for displaying number and related content',
    path: '/card/number-cards'
  },
  {
    name: 'Card - Profile Cards',
    menuName: 'Profile Cards',
    desc: 'Card component for displaying profile content',
    path: '/card/profile-cards'
  },
  {
    name: 'Card - Simple Card',
    menuName: 'Simple Cards',
    desc: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and sizes.',
    path: '/card/cards'
  },
  {
    name: 'Card - Icon Cards',
    menuName: 'Icon Cards',
    desc: 'Card component for displaying Icon and related content',
    path: '/card/icon-cards'
  },
  {
    name: 'Card - Product Cards (Grid)',
    menuName: 'Product Cards (Grid)',
    desc: 'Card component for displaying products',
    path: '/card/product-cards-grid'
  },
  {
    name: 'Card - Product Cards (List)',
    menuName: 'Product Cards (List)',
    desc: 'Card component for displaying products',
    path: '/card/product-cards-list'
  },
  {
    name: 'Card - Portfolio Cards',
    menuName: 'Portfolio Cards',
    desc: 'Card component for displaying portfolio',
    path: '/card/portfolio-cards'
  },
]

export const LAYOUTS = [
  {
    name: 'Layout - Header',
    menuName: 'Header',
    desc: 'The header section of App layout',
    path: '/layout/header'
  },
  {
    name: 'Layout - Footer',
    menuName: 'Footer',
    desc: 'The footer section of App layout',
    path: '/layout/footer'
  },
  {
    name: 'Layout - Sidenav',
    menuName: 'Sidenav',
    desc: 'The sidenav section of App layout',
    path: '/layout/sidenav'
  },
  {
    name: 'Layout - Page / Content',
    menuName: 'Page / Content',
    desc: 'The content / page section of App layout',
    path: '/layout/page'
  },
  {
    name: 'Layout - Page (Fullscreen)',
    menuName: 'Page (Fullscreen)',
    desc: 'A fullscreen page, without App header, footer or sidenav',
    path: '/layout/page-fullscreen'
  },
  {
    name: 'Layout - Page with Tabs',
    menuName: 'Page (with Tabs)',
    desc: 'A standard page with tabs for different views',
    path: '/layout/page-with-tabs'
  },
  {
    name: 'Layout - Page with Breadcrumb',
    menuName: 'Page (with Breadcrumb)',
    desc: 'A standard page with breadcrumb',
    path: '/layout/page-with-breadcrumb'
  },
  {
    name: 'Layout - Grid System',
    menuName: 'Grid System',
    desc: "Bootstrap's powerful mobile-first flexbox grid to build layouts of all shapes and sizes",
    path: '/layout/grid'
  },
  {
    name: 'List / List Group',
    menuName: 'List',
    desc: 'A list can be used to display content related to a single subject. The content can consist of multiple elements of varying type and size.',
    path: '/layout/list'
  },
]

const UIHOVER = [
  {
    name: 'Hover',
    menuName: 'Basic',
    desc: 'A mouse hover, also called just hover, triggers an event when a user places a mouse over a designated area',
    path: '/ui/hover/hover'
  }, {
    name: 'Hover - Link Hover',
    menuName: 'Link Hover',
    desc: 'Link hover effect is triggered when a user places a mouse over a link',
    path: '/ui/hover/link-hover'
  }, {
    name: 'Hover - With Overlay',
    menuName: 'With Overlay',
    desc: 'Overlay content is displayed when a user places a mouse over a designated area',
    path: '/ui/hover/with-overlay'
  },
]

const UIICON = [
  {
    name: 'Icon - Ant Icons',
    menuName: 'Ant Icons',
    desc: 'Antd icon assets',
    path: '/ui/icon/ant-icons'
  },
  {
    name: 'Icon - Social Icons',
    menuName: 'Social Icons',
    desc: 'Social icon assets',
    path: '/ui/icon/social-icons'
  }
]

const UIMORE = [
  {
    name: 'Avatar',
    desc: 'Avatars can be used to represent people or objects. It supports images, Icons, or letters.',
    path: '/ui/more/avatar'
  },
  {
    name: 'BackTop',
    desc: 'BackTop makes it easy to go back to the top of the page.',
    path: '/ui/more/back-top',
    hideInMenu: true
  },
  {
    name: 'Badge / Label',
    desc: 'Badge normally appears in proximity to notifications or user avatars with eye-catching appeal, typically displaying unread messages count.',
    path: '/ui/more/badge'
  },
  {
    name: 'Call to Action',
    desc: 'A call to action (CTA) is an instruction to the audience designed to provoke an immediate response.',
    path: '/ui/more/call-to-action'
  },
  {
    name: 'Callout',
    desc: 'A callout is often a short piece of text set in larger type or with colorful background and intended to attract attention.',
    path: '/ui/more/callout'
  },
  {
    name: 'Carousel',
    desc: 'A carousel component. Scales with its container.',
    path: '/ui/more/carousel'
  },
  {
    name: 'Popover',
    desc: 'The floating card popped by clicking or hovering.',
    path: '/ui/more/popover'
  },
  {
    name: 'Ribbon',
    desc: 'A ribbon is used primarily as decorative binding for highlighting a piece of information.',
    path: '/ui/more/ribbon'
  },
  {
    name: 'Tag / Chip',
    desc: 'Tag for categorizing or markup.',
    path: '/ui/more/tag'
  },
  {
    name: 'Tooltip',
    desc: 'A simple text popup tip.',
    path: '/ui/more/tooltip'
  },
  {
    name: 'Tree',
    desc: 'Almost anything can be represented in a tree structure. Examples include directories, organization hierarchies, biological classifications, countries, etc.',
    path: '/ui/more/tree'
  },
]

const UINAVIGATION = [
  {
    name: 'Breadcrumb',
    desc: 'A breadcrumb displays the current location within a hierarchy. It allows going back to states higher up in the hierarchy.',
    path: '/ui/navigation/breadcrumb'
  },
  {
    name: 'Dropdown',
    desc: 'A dropdown list. If there are too many operations to display, you can wrap them in a Dropdown.',
    path: '/ui/navigation/dropdown'
  },
  {
    name: 'Pagination',
    desc: 'When it will take a long time to load/render all items. Or if you want to browse the data by navigating through pages.',
    path: '/ui/navigation/pagination'
  },
]

export const UITIMELINE = [
  {
    name: 'Timeline / Streamline',
    desc: 'Vertical display timeline.',
    path: '/ui/timeline/timeline'
  },
  {
    name: 'Timeline (Large)',
    desc: 'Large vertical display timeline.',
    path: '/ui/timeline/timeline-lg'
  },
]

const UITYPOGRAPHY = [
  {
    name: 'Blockquote',
    desc: 'Blockquote specifies a section that is quoted from another source.',
    path: '/ui/typography/blockquote'
  },
  {
    name: 'Typography',
    desc: 'Typography is one of the most basic foundational part of a interface design system.',
    path: '/ui/typography/typography'
  },
]

const UIUTILITY = [
  {
    name: 'Overlay',
    desc: "Overlays are often used when you want to make the content on top of an image more readable. It's used on components like Image Cards, Covers, Hero etc.",
    path: '/ui/utility/overlay'
  },
  {
    name: 'Background Color',
    desc: 'Convey meaning through color with a handful of color utility classes.',
    path: '/ui/utility/color'
  },
  {
    name: 'Spacing',
    desc: 'A wide range of shorthand responsive margin and padding utility classes to modify an element’s appearance.',
    path: '/ui/utility/spacing'
  },
  {
    name: 'Gradient Backgrounds',
    desc: 'Convey meaning through color with a handful of color utility classes.',
    path: '/ui/utility/gradient-background'
  },
  {
    name: 'Divider',
    desc: 'A divider line separates different content.',
    path: '/ui/utility/divider'
  },
]

export const UIKIT = [
  {
    name: 'Button / Button Group',
    menuName: 'Button',
    desc: 'A button means an operation (or a series of operations). Clicking a button will trigger corresponding business logic.',
    path: '/ui/button'
  },
  {
    name: 'Box',
    desc: 'A box is often used as a container to display content, it works like a Card.',
    path: '/ui/box'
  },
  {
    name: 'Color Palette',
    desc: 'Color palette of Ant Design',
    path: '/ui/color-palette'
  },
  {
    name: 'Collapse / Accordion',
    desc: 'A content area which can be collapsed and expanded.',
    path: '/ui/collapse'
  },
  {
    name: 'Cover',
    desc: 'A lightweight, flexible component that can optionally extend the entire viewport to showcase key marketing messages on your site.',
    path: '/ui/cover'
  },
  {
    name: 'Feature Callout',
    desc: 'A callout for feature',
    path: '/ui/feature-callout'
  },
  {
    name: 'Hover',
    path: '/ui/hover',
    children: UIHOVER
  },
  {
    name: 'Icon',
    path: '/ui/icon',
    children: UIICON
  },
  {
    name: 'Jumbotron / Hero',
    desc: 'A lightweight, flexible component that can optionally extend the entire viewport to showcase key marketing messages on your site.',
    path: '/ui/jumbotron'
  },
  {
    name: 'More Components',
    path: '/ui/more',
    children: UIMORE
  },
  {
    name: 'Navigation',
    path: '/ui/navigation',
    children: UINAVIGATION
  },
  {
    name: 'Pricing Tables',
    desc: 'A table shows the pricing and corresponding features',
    path: '/ui/pricing-table'
  }, {
    name: 'Sash',
    desc: 'A sash is a large and usually colorful ribbon or band of material positioned around the content body',
    path: '/ui/sash'
  }, {
    name: 'Tabs',
    desc: 'Tabs make it easy to switch between different views.',
    path: '/ui/tabs'
  }, {
    name: 'Testimonial',
    desc: "A testimonial consists of a person's written or spoken statement extolling the virtue of a product.",
    path: '/ui/testimonials'
  },
  {
    name: 'Timeline',
    path: '/ui/timeline',
    children: UITIMELINE
  },
  {
    name: 'Typography',
    path: '/ui/typography',
    children: UITYPOGRAPHY
  },
  {
    name: 'Utility',
    path: '/ui/utility',
    children: UIUTILITY
  },
];

const FORMCONTROLS = [
  {
    name: 'Rate',
    desc: 'Rate component. Usage: Show evaluation. Or a quick rating operation on something.',
    path: '/form/form-control/rate'
  },
  {
    name: 'Slider',
    desc: 'A Slider component for displaying current value and intervals in range.',
    path: '/form/form-control/slider'
  },
  {
    name: 'Switch',
    desc: 'Switching Selector. Usage: If you need to represent the switching between two states or on-off state.',
    path: '/form/form-control/switch'
  },
  {
    name: 'TimePicker',
    desc: 'By clicking the input box, you can select a time from a popup panel.',
    path: '/form/form-control/timepicker'
  },
  {
    name: 'Transfer',
    desc: 'Double column transfer choice box.',
    path: '/form/form-control/transfer'
  },
  {
    name: 'Tree Select',
    desc: 'Tree Select is similar to Select, but the values are provided in a tree like structure.',
    path: '/form/form-control/tree-select'
  },
  {
    name: 'Upload',
    desc: 'Upload file by selecting or dragging.',
    path: '/form/form-control/upload'
  },
  {
    name: 'Select & Tags',
    desc: 'Select component to select value from options.',
    path: '/form/form-control/select'
  },
  {
    name: 'Radio',
    desc: 'Radio. Usage: Used to select a single state in multiple options.',
    path: '/form/form-control/radio'
  },
  {
    name: 'Mention',
    desc: 'Mention component. Usage: When need to mention someone or something.',
    path: '/form/form-control/mention'
  },
  {
    name: 'Input Number',
    desc: 'Enter a number within certain range with the mouse or keyboard.',
    path: '/form/form-control/input-number'
  },
  {
    name: 'Input / Form',
    desc: 'A basic widget for getting the user input is a text field. Keyboard and mouse can be used for providing or changing data.',
    path: '/form/form-control/input'
  },
  {
    name: 'DatePicker',
    desc: 'By clicking the input box, you can select a date from a popup calendar.',
    path: '/form/form-control/datepicker'
  },
  {
    name: 'AutoComplete',
    desc: 'Autocomplete function of input field. Usage: When there is a need for autocomplete functionality.',
    path: '/form/form-control/autocomplete'
  },
  {
    name: 'Cascader',
    desc: 'Cascade selection box.',
    path: '/form/form-control/cascader'
  },
  {
    name: 'Checkbox',
    desc: 'Checkbox. Used for selecting multiple values from several options.',
    path: '/form/form-control/checkbox'
  },
]

export const FORMS = [
  {
    name: 'Form Examples',
    desc: 'Form is used to collect, validate, and submit the user input, usually contains various form items including checkbox, radio, input, select, and etc.',
    path: '/form/forms'
  },
  {
    name: 'Form Layout',
    desc: 'Different layout type for forms',
    path: '/form/layout'
  },
  {
    name: 'Form Control',
    path: '/form/form-control',
    badge: 'badge-status-dot badge-info',
    children: FORMCONTROLS
  },
  {
    name: 'Form Validation',
    desc: "Validate status and/or message will be displayed when user's input violate specified validation rules",
    path: '/form/validation'
  },
  {
    name: 'Steps / Wizard',
    desc: 'Steps is a navigation bar that guides users through the steps of a task.',
    path: '/form/steps'
  }
]

const CHARTERPARTYTERMS = [
  {
    name: 'Charter Party Forms',
    desc: 'Charter Party Forms menu',
    path: '/data-center/charter-party-terms/charter-party-forms'
  },
  {
    name: 'Freight Codes',
    desc: 'Freight Codes menu',
    path: '/data-center/charter-party-terms/freight-codes'
  },
  {
    name: 'Laytime Types',
    desc: 'Laytime Types menu',
    path: '/data-center/charter-party-terms/laytime-types'
  },
  {
    name: 'Loading Costs',
    desc: 'Loading Costs menu',
    path: '/data-center/charter-party-terms/loading-costs'
  },
  {
    name: 'Laytime to Commence',
    desc: 'Laytime to Commence menu',
    path: '/data-center/charter-party-terms/laytime-commence'
  },
  {
    name: 'NOR to Tender',
    desc: 'NOR to Tender menu',
    path: '/data-center/charter-party-terms/nor-tender'
  },
  {
    name: 'Normal Off/Hrs',
    desc: 'Normal Off/Hrs menu',
    path: '/data-center/charter-party-terms/normal-off'
  },
  {
    name: 'Other Loading Terms',
    desc: 'Other Loading Terms menu',
    path: '/data-center/charter-party-terms/other-loading-terms'
  },
  {
    name: 'Shifting Terms',
    desc: 'Shifting Terms menu',
    path: '/data-center/charter-party-terms/shifting-terms'
  },
  {
    name: 'SHINC/SHEX Terms',
    desc: 'SHINC/SHEX Terms menu',
    path: '/data-center/charter-party-terms/shinc-terms'
  },
  {
    name: 'Time to Tender',
    desc: 'Time to Tender menu',
    path: '/data-center/charter-party-terms/time-tender'
  },
  {
    name: 'Time Used',
    desc: 'Time Used menu',
    path: '/data-center/charter-party-terms/time-used'
  },
  {
    name: 'Working Days',
    desc: 'Working Days menu',
    path: '/data-center/charter-party-terms/working-days'
  },
]

const CURRENCIES = [
  {
    name: 'Currency Types',
    desc: 'Currency Types menu',
    path: '/data-center/currencies/currency-types'
  },
  {
    name: 'Exchange Rates',
    desc: 'Exchange Rates menu',
    path: '/data-center/currencies/exchange-rates'
  },
]

const DELAYSANDWEATHER = [
  {
    name: 'Beaufort Scale',
    desc: 'Beaufort Scale menu',
    path: '/data-center/delay-weather/beaufort-scale'
  },
  {
    name: 'Sea States',
    desc: 'Sea States menu',
    path: '/data-center/delay-weather/sea-states'
  },
  {
    name: 'Swell States',
    desc: 'Swell States menu',
    path: '/data-center/delay-weather/swell-states'
  },
  {
    name: 'Delay Reasons',
    desc: 'Delay Reasons menu',
    path: '/data-center/delay-weather/delay-reasons'
  },
  {
    name: 'Delay Types',
    desc: 'Delay Types menu',
    path: '/data-center/delay-weather/delay-types'
  },
]

const OTHER = [
  {
    name: 'Departments/Teams',
    desc: 'Departments/Teams menu',
    path: '/data-center/other/departments-teams'
  },
  {
    name: 'Trade Areas',
    desc: 'Trade Areas menu',
    path: '/data-center/other/trade-areas'
  },
  {
    name: 'Operations Ledger',
    desc: 'Operations Ledger menu',
    path: '/data-center/other/operations-ledger'
  },
  {
    name: 'Units of Measure',
    desc: 'Units of Measure menu',
    path: '/data-center/other/units-measure'
  },
  {
    name: 'Port Activities Term',
    desc: 'Port Activities List',
    path: '/data-center/other/port-activities-term'
  },
  {
    name: 'Port Functions',
    desc: 'Port Functions List',
    path: '/data-center/other/port-functions'
  },

  // {
  //   name: 'Vessel Fleets',
  //   desc: 'Vessel Fleets menu',
  //   path: '/data-center/other/vessel-fleets'
  // },
  // {
  //   name: 'Class Societies',
  //   desc: 'Class Societies menu',
  //   path: '/data-center/other/class-societies'
  // },
  // {
  //   name: 'Fuel/Lube Types',
  //   desc: 'Fuel/Lube Types menu',
  //   path: '/data-center/other/fuel-lube-types'
  // },
  {
    name: 'OPA Rates',
    desc: 'OPA Rates menu',
    path: '/data-center/other/opa-rates'
  },
  {
    name: 'Cargo Groups',
    desc: 'Cargo Groups menu',
    path: '/data-center/other/cargo-groups'
  },
  {
    name: 'Fuel Zone',
    desc: 'Fuel Zone menu',
    path: '/data-center/other/fuel-zone'
  },


]

const VESSELS = [
  {
    name: 'Fuel Consumption Categories',
    desc: 'Fuel Consumption Categories menu',
    path: '/data-center/vessels/fuel-consumption-categories'
  },
  {
    name: 'Fuel Grades',
    desc: 'Fuel Grades menu',
    path: '/data-center/vessels/fuel-grades'
  },
  {
    name: 'Fuel Types',
    desc: 'Fuel Types menu',
    path: '/data-center/vessels/fuel-types',
    pageTitle: 'All Fuel-Lube Type'
  }
]

const DATACENTER_NEW = [
  {
    name: 'Charter Party Terms',
    path: '/data-center/charter-party-terms',
    badge: 'badge-status-dot badge-info',
    children: CHARTERPARTYTERMS
  },
  {
    name: 'Currencies',
    path: '/data-center/currencies',
    badge: 'badge-status-dot badge-info',
    children: CURRENCIES
  },
  {
    name: 'Delay And Weather',
    path: '/data-center/delay-weather',
    badge: 'badge-status-dot badge-info',
    children: DELAYSANDWEATHER
  },
  {
    name: 'Other',
    path: '/data-center/other',
    badge: 'badge-status-dot badge-info',
    children: OTHER
  },
  {
    name: 'Vessels',
    path: '/data-center/vessels',
    badge: 'badge-status-dot badge-info',
    children: VESSELS
  },
]

export let DATACENTER = DATACENTER_NEW;

export const FEEDBACKS = [
  {
    name: 'Modal / Dialog',
    desc: "Modal dialogs. Usage: When requiring users to interact with the application, but without jumping to a new page and interrupting the user's workflow",
    path: '/feedback/modal'
  },
  {
    name: 'Notification',
    desc: 'Display a notification message globally.',
    path: '/feedback/notification'
  },
  {
    name: 'Alert',
    desc: 'Alert component for feedback. Usage: When you need to show alert messages to users.',
    path: '/feedback/alert'
  },
  {
    name: 'Message / Snackbar',
    desc: 'Display global messages as feedback in response to user operations.',
    path: '/feedback/message'
  },
  {
    name: 'Popconfirm',
    desc: 'A simple and compact confirmation dialog of an action.',
    path: '/feedback/popconfirm'
  },
  {
    name: 'Progress',
    desc: 'Display the current progress of an operation flow.',
    path: '/feedback/progress'
  },
  {
    name: 'Spin',
    desc: 'A spinner for displaying loading state of a page or a section.',
    path: '/feedback/spin'
  },
  {
    name: 'Loader',
    desc: 'A loader for displaying loading state of a page or a section.',
    path: '/feedback/loader'
  },
]

export const TABELS = [
  {
    name: 'Table - Tables',
    menuName: 'Table Examples',
    desc: 'A table displays rows of data.',
    path: '/table/tables'
  }, {
    name: 'Table - Data Table',
    menuName: 'Data Table',
    desc: 'Usage: to display a collection of structured data and to sort, search, paginate, filter data.',
    path: '/table/data-table'
  },
]

export const CHARTS = [
  {
    name: 'Chart - Line & Area',
    menuName: 'Line & Area',
    desc: 'Line & Area chart',
    path: '/chart/line'
  },
  {
    name: 'Chart - Bar',
    menuName: 'Bar',
    desc: 'Bar chart',
    path: '/chart/bar'
  },
  {
    name: 'Chart - Pie',
    menuName: 'Pie',
    desc: 'Pie chart',
    path: '/chart/pie'
  },
  {
    name: 'Chart - Scatter',
    menuName: 'Scatter',
    desc: 'Scatter chart',
    path: '/chart/scatter'
  },
  {
    name: 'Chart - Radar',
    menuName: 'Radar',
    desc: 'Radar chart',
    path: '/chart/radar'
  },
  {
    name: 'Chart - Funnel',
    menuName: 'Funnel',
    desc: 'Funnel chart',
    path: '/chart/funnel'
  },
  {
    name: 'Chart - Gauge',
    menuName: 'Gauge',
    desc: 'Gauge chart',
    path: '/chart/gauge'
  },
  {
    name: 'Chart - Candlestick',
    menuName: 'Candlestick',
    desc: 'Candlestick chart',
    path: '/chart/candlestick'
  },
  {
    name: 'Chart - Heatmap',
    menuName: 'Heatmap',
    desc: 'Heatmap chart',
    path: '/chart/heatmap'
  },
  {
    name: 'Chart - PictorialBar',
    menuName: 'PictorialBar',
    desc: 'PictorialBar chart',
    path: '/chart/pictorial-bar'
  },
  {
    name: 'Chart - Sunburst',
    menuName: 'Sunburst',
    desc: 'Sunburst chart',
    path: '/chart/sunburst'
  },
  {
    name: 'Chart - ThemeRiver',
    menuName: 'ThemeRiver',
    desc: 'ThemeRiver chart',
    path: '/chart/theme-river'
  },
]

const CALENDAR = [
  {
    name: 'Calendar',
    desc: 'When data is in the form of dates, such as schedules, timetables, prices calendar, lunar calendar. This component also supports Year/Month switch.',
    path: '/calendar'
  }
]

export const PAGES = [
  {
    name: 'About',
    path: '/page/about'
  }, {
    name: 'About History',
    path: '/page/about-history'
  }, {
    name: 'Blog',
    path: '/page/blog'
  }, {
    name: 'Services',
    path: '/page/services'
  }, {
    name: 'Services v2',
    path: '/page/services-v2'
  }, {
    name: 'Careers',
    path: '/page/careers'
  }, {
    name: 'Contact',
    path: '/page/contact'
  }, {
    name: 'FAQs',
    path: '/page/faqs'
  }, {
    name: 'Terms of Services',
    path: '/page/terms'
  },
]

export const ECOMMERCE = [
  {
    name: 'Products',
    path: '/ecommerce/products'
  }, {
    name: 'Products v2',
    path: '/ecommerce/products-v2'
  }, {
    name: 'Invoice',
    path: '/ecommerce/invoice'
  }
]

export const USER = [
  {
    name: 'Login',
    path: '/user/login'
  }, {
    name: 'Login v2',
    path: '/user/login-v2'
  }, {
    name: 'Sign Up',
    path: '/user/sign-up'
  }, {
    name: 'Sign Up v2',
    path: '/user/sign-up-v2'
  }, {
    name: 'Forgot Password',
    path: '/user/forgot-password'
  }, {
    name: 'Forgot Password v2',
    path: '/user/forgot-password-v2'
  }
]

export const EXCEPTION = [
  {
    name: '403 Error',
    path: '/exception/403'
  }, {
    name: '403 Error (Fullscreen)',
    path: '/exception/403'
  }, {
    name: '404 Error',
    path: '/exception/404'
  }, {
    name: '404 Error (Fullscreen)',
    path: '/exception/404'
  }, {
    name: '500 Error',
    path: '/exception/500'
  }, {
    name: '500 Error (Fullscreen)',
    path: '/exception/500'
  },
]

export const ROUTES = [
  {
    key: "/imcs",
    link: "#/imcs",
    title: "Set Up IMCS",
    _children: IMCS,
    isHeading: true
  }, {
    key: "/add-company",
    link: ["#/add-company", "#/company"],
    title: ['Company', "List"],
    isTriggerDoubleClick: true,
    // _children: COMPANY_LIST
  }, {
    key: "/vendor",
    link: ["#/add-vendor", "#/vendor"],
    title: ['Vendor', 'List'],
    isTriggerDoubleClick: true,
    // _children: VENDOR_LIST
  }, {
    key: "/users",
    link: ["#/add-user", "#/users"],
    title: ['User', 'List'],
    isTriggerDoubleClick: true,
    _children: USER_LIST
  }, {
    key: "/email-alert",
    link: ["#/add-email-alert", "#/email-alert"],
    title: ['Email Alert', 'List'],
    isTriggerDoubleClick: true,
    _children: EMAIL_ALERT
  }, {
    key: "/chartering",
    link: "#/chartering",
    title: "Chartering",
    _children: CHARTERINGS,
    isHeading: true
  },

  {
    key: "/chartering-dashboard",
    link: "#/chartering-dashboard",
    title: "Chartering",
    isHeading: false
  },

  {
    key: "/tcov-list",
    link: ["#/add-tcov", "#/tcov-list"],
    title: ['TCOV', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCOV_LIST
  },
  {
    key: "/tcto-list-1",
    link: ["#/add-tcto", "#/tcto-list"],
    title: ['TCTO', 'List'],
    isTriggerDoubleClick: true,
    // _children: ESTIMATE_LIST
  },

  {
    key: "/vci-vco",
    link: ["#/add-vci-vco", "#/vci-vco"],
    title: ['VCI-VCO', 'List'],
    isTriggerDoubleClick: true,
    // _children: VCI_VCO_LIST
  },
  {
    key: "/tc-in-list",
    link: ["#/add-tci", "#/tc-in-list"],
    title: ['TCI', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCI_LIST
  },
  {
    key: "/cargo-list",
    link: ["#/add-cargo-contract", "#/cargo-list"],
    title: ['Cargo Contract', 'List'],
    isTriggerDoubleClick: true,
    // _children: CARGO_ESTIMATE
  }, {
    key: "/coa-list",
    link: ["#/add-coa-contract", "#/coa-list"],
    title: ['COA (Cargo) Contract', 'List'],
    isTriggerDoubleClick: true,
    // _children: COA_LIST
  },
  {
    key: "/voyage-cargo-list",
    link: ["#/add-voyage-cargo", "#/voyage-cargo-list"],
    title: ['Voyage Cargo (IN)', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCI_LIST
  },

  {
    key: "/",
    link: ["#/add-tco", "#/"],
    title: ['TCO-Manager', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCI_LIST
  },

  {
    key: "/voyage-fix-list",
    link: ["#/voyage-fixture", "#/voyage-fix-list"],
    title: ['Voyage Fixture', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCI_LIST
  },

  {
    key: "/coa-vci-list",
    link: ["#/add-coa-vci", "#/list-coa-vci"],
    title: ['COA(VCI)', 'List'],
    isTriggerDoubleClick: true,
    // _children: TCI_LIST
  },
  {
    key: "/operation",
    link: "#/operation",
    title: "Operation",
    _children: OPERATIONS_New,
    isHeading: true
  },

  {
    key: "/vessel-schedule",
    link: "#/vessel-schedule",
    title: "Open Vessel Schedule",
    // _children: VESSEL_SCHEDULE
  },

  // {
  //   key: "/voyagers",
  //   link: ["#/add-voyage", "#/voyages"],
  //   title: ['Voyage Manager', 'List'],
  //   isTriggerDoubleClick: true,
  //   _children: VOYAGER_MANAGER
  // },
  {
    key: "/voyagers",
    // link: ["#/voyage-manager", "#/voyage-list"],
    link: ["#/voyage-manager", "#/voyage-manager-list"],
    title: ['Voyage Manager', 'List'],
    isTriggerDoubleClick: true,
    // _children: VOYAGER_MANAGER
  },
  {
    key: "/port-schedule",
    link: "#/port-schedule",
    title: "Port Schedule"
  },
  {
    key: "/portcalls-list",
    link: ["#/add-portcalls", "#/portcalls-list"],
    title: ['Portcalls', 'List'],
    isTriggerDoubleClick: true,
  },
  {
    key: "/claim-list",
    link: "#/claim-list",
    title: 'Claim List',
  },
  // {
  //   key: "/bunker-enquiry-list",
  //   link: ["#/add-bunker-enquiry", "#/bunker-enquiry-list"],
  //   title: ['Bunker Enquiry', 'List'],
  //   isTriggerDoubleClick: true,
  // },

  {
    key: "/Bunker",
    link: "#/bunker",
    title: "Bunker",
    _children: BUNKERS_menu,
    isHeading: true
  },

  {
    key: "/track-fleet",
    link: "#/track-fleet",
    title: "Track Fleet",
    _children: TRACK_FLEET
  }, {
    key: "/dynamic-vsop",
    link: "#/dynamic-vspm",
    title: "Dynamic VSPM"
  }, {
    key: "/bi-reports",
    link: "#/bi-reports",
    title: "BI & Reports"
  }, {
    key: "/tasks",
    link: ["#/add-task", "#/tasks"],
    title: ['Task', 'List'],
    isTriggerDoubleClick: true,
  }, {
    key: "/hire-payment",
    link: ["#/add-hire-payment", "#/hire-payment"],
    title: ['Hire Payment', 'List'],
    isTriggerDoubleClick: true,
    _children: HIRE_PAYMENT_LIST
  }, {
    key: "/voy-voy",
    link: ["#/add-voy-voy", "#/voy-voy"],
    title: ['Voy-Voy', 'List'],
    isTriggerDoubleClick: true
  }, {
    key: "/finance",
    link: "#/finance",
    title: "Finance",
    isHeading: true
  }, {
    key: "/vendor-transaction",
    link: "#/vendor-transaction",
    title: "Vendor(agents) Transaction Summary",
    _children: VENDOR_TRANSACTION
  }, {
    key: "/bunker-vendors",
    link: "#/bunker-vendors",
    title: "Bunker Vendors Transaction Summary",
    _children: BUNKER_VENDOR
  }, {
    key: "/hire-invoice",
    link: "#/hire-invoice",
    title: "Hire Invoice Summary",
    _children: HIRE_INVOICE
  }, {
    key: "/vendors-invoice-summary",
    link: "#/vendors-invoice-summary",
    title: "Other Vendors Invoice Summary"
  }, {
    key: "/invoice-list",
    link: ["#/add-invoice", "#/invoice-list"],
    title: ['Invoice', 'List'],
    isTriggerDoubleClick: true,
    _children: INVOICE_LIST
  }, {
    key: "/account-rule",
    link: "#/account-rule",
    title: "Account Rule",
    _children: ACCOUNT_RULE
  }, {
    key: "/data-center",
    link: "#/data-center",
    title: "Data Center",
    isHeading: true,
    _children: DATACENTER_NEW
  }, {
    key: "/address-list",
    link: ["#/add-address-form", "#/address-list"],
    title: ['New Address Form', 'List'],
    isTriggerDoubleClick: true,
  },

  {
    key: "/port-information",
    link: "#/port-information",
    title: "Port Information",
  },

  {
    key: "/vessel-form",
    link: "#/vessel-form",
    title: "New Vessel form"
  }, {
    key: "/ports-list",
    link: ["#/port-form", "#/ports-list"],
    title: ['Port', 'List'],
    isTriggerDoubleClick: true,
  }, {
    key: "/country-list",
    link: "#/country-list",
    title: "Country List",
    isTriggerDoubleClick: true,
  }, {
    key: "/vessel-list",
    link: "#/vessel-list",
    title: "Master List (All Vessel)"
  }, {
    key: "/cargos",
    link: ["#/add-cargo", "#/cargos"],
    title: ["New cargo form", "Cargo list"],
    isTriggerDoubleClick: true
  }, {
    key: "/port-distances",
    link: "#/port-distances",
    title: "Port To Port Distance"
  },


  {
    key: "/Business-Rule-of-Accounting",
    link: "#/business-rule-accounting",
    title: "Business Rule of Accounting"
  },
  {
    name: 'Bank Names',
    desc: 'Bank Names menu',
    path: '/data-center/other/bank-names'
  },




]

// for UI Overview page
const COMPONENTS = [
  ...CARDS,
  ...LAYOUTS,
  ...UIKIT, ...UIHOVER, ...UIICON, ...UIMORE, ...UINAVIGATION, ...UITIMELINE, ...UITYPOGRAPHY, ...UIUTILITY,
  ...FORMS, ...FORMCONTROLS,
  ...FEEDBACKS,
  ...TABELS,
  ...CHARTS,
  ...CALENDAR,
  ...CHARTERING,
  ...FREIGHT,
  ...DEMURRAGE,
  ...TIMECHARTERERIN,
  ...TIMECHARTEREROUT,
  ...MYPORTCALLS,
  ...VSOP,
  ...DATACENTER, ...CHARTERPARTYTERMS, ...CURRENCIES, ...DELAYSANDWEATHER, ...OTHER, ...VESSELS,
  ...PERFORMANCEREPORT,
  ...SETUP
];

export default COMPONENTS;

