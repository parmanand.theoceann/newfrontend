export const breadcrumb = {
    "register-own-company": ["Set Up IMCS", "Register Own Company"],
    "register-branch-company": ["Set Up IMCS", "Register Branch Company"],
    "register-bank": ["Set Up IMCS", "Register Own Bank Details"],
    "register-vendor": ["Set Up IMCS", "Register Vendor Details"],
    "register-department": ["User - List", "Register Department"],
    "register-team-group": ["User - List", "Register Team-Group"],
    "register-user-role": ["User - List", "Register User & Role"],
    "company": ["Company - List"],
    "vendor": ["Vendor - List"],
    "users": ["User - List"],
    "intruction-set": ["PDMS Setup", "Intruction Set for Agent"],
    "user-role": ["PDMS Setup", "User Role"],
    "port-agent": ["PDMS Setup", "Port Agent Panel - Service Setup"],
    "portexpensecategory": ["PDMS Setup", "Port Expenses Category - Service Setup"],
    "active-agent": ["Active Agent List"],
    "add-agent": ["Add Agent List"],
    "chartering-email-setup": ["Email alert - list", "Email Alert Setup", "Chartering Email Setup"],
    "accounting-email-setup": ["Email alert - list", "Email Alert Setup", "Accounting Email Setup"],
    "operational-email-setup": ["Email alert - list", "Email Alert Setup", "Operational Email Setup"],
    "tcov": ["Chartering", "TCOV - Est."],
    "add-tcov": ["TC Estimate"],
    "tcto": ["Chartering", "TCTO - Est."],
    "vci-vco-est": ["Chartering", "VCI - VCO Est."],
    "tci": ["Chartering", "TCI - Est."],
    "coa-contract": ["Chartering", "COA Contract"],
    "vci": ["Chartering", "Voyage cargo (IN)"],
    "tco": ["Chartering", "TCO - Manager"],
    "coa-vci-form": ["Chartering", "COA (VCI) form"],
    "pl-estimate-page": ["Chartering", "P&L Estimate Page"],
    "tcov-list": ["TCOV - List"],
    "add-tcto": ["TCTO"],
    "add-vci-vco": ["VCI-VCO"],
    "add-tci": ["TCI"],
    "add-coa-contract": ["COA (Cargo) Contract"],
    "edit-coa-contract": ["Edit COA (Cargo) Contract"],
    "tc-in-list": ["TC-IN - List"],
    "add-cargo": ["Cargo Name"],
    "vci-vco": ["VCI-VCO - List"],
    "add-voyage-cargo": ["VC (Purchase)"],
    "edit-voyage-cargo": ["Edit VC (Purchase)"],
    "add-coa-vci": ["COA (VC-Purchase)"],
    "edit-coa-vci": ["Edit COA (VC-Purchase)"],
    "coa-vci-list": ["COA (VCI) - List"],
    "tcto-list": ["TCTO - List"],
    "coa-list": ["COA List"],
    "add-tco": ["TCO - Manager"],
    "voyage-fix-list": ["Voyage Fixture List"],
    "vessel-schedule": ["Open Vessel Schedule"],
    "initial-freight-invoice": ["Voyage Manager list", "Freight", "Initial Freight Invoice"],
    "final-freight-invoice": ["Voyage Manager list", "Freight", "Final Freight Invoice"],
    "commission-invoice-tc": ["Hire Payment", "Tc In", "Commission Invoice"],
    "commission-invoice": ["Voyage Manager list", "Freight", "Commission Invoice"],
    "freight-receivable-list": ["Voyage Manager list", "Freight", "Freight Invoice List"],
    "shifting-terms": ["Data Center", "Charter Party Terms", "Shifting Terms"],
    "tco-manager-list": ["TCO Manager List"]
}