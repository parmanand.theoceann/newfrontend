import React, { Component } from 'react';
import { Form, Input, Select, Spin, Alert, InputNumber, Switch } from 'antd';
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon, URL_WITHOUT_VERSION } from '../../shared';
import TextArea from 'antd/lib/input/TextArea';

const FormItem = Form.Item;
const Option = Select.Option;

class NormalForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      "responseData": { "frm": [], "tabs": [], "active_tab": {} },
      "ddlData": {},
      "conditionalFields": null
    }
    this.vessel_name = {
      name: '',
      typing: false,
      typingTimeout: 0
    }
  }

  componentDidMount = async () => {
    if (this.props.frmCode) {
      const response = await getAPICall(`${URL_WITHOUT_VERSION}get/${this.props.frmCode}`);
      const data = await response['data'];
      this.setState({ ...this.state, "responseData": data });
    } else if (this.props.frm) {
      this.setState({ ...this.state, "responseData": this.props.frm });
      this.callForDdlBinding(this.props.frm);
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.frm && state.responseData.frm.length === 0) {
      return {
        ...state,
        "conditionalFields": props.conditionalFields
      }
    }
    return null
  }

  callForDdlBinding = async (data) => {
    if (data.frm.length > 0) {
      data.frm.map((e, i) => {
        if (e.f_name === "year_built") {
          let currentYear = new Date().getFullYear();
          let years = [], ddl = {};
          for (let i = currentYear; i > (currentYear - 30); i--) {
            years.push({ label: i, value: i })
          }
          ddl["options"] = years;
          data.frm[i]["f_dyc_extras"] = ddl;
        }
      });
    }
    this.setState({ ...this.state, "responseData": data });
  }

  showTypeFields = (e) => {
    let f_value = this.props.frmProp.form.getFieldValue(e.f_name);
    switch (e.f_type) {
      case "dropdown":
        return (<Select
          placeholder={"Select " + e.name}
          key={e.f_name}
          showSearch
          // placeholder={"Select a " + e.name}
          onChange={e.f_name === "group" ? (evt) => this.changeCondition(e, evt) : null}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {
            e.f_dyc_extras && e.f_dyc_extras.options && e.f_dyc_extras.options.length > 0 ? (
              e.f_dyc_extras.options.map(e => {
                return <Option value={e.value.toString()} key={e.value}>{e.label}</Option>
              })
            ) : null
          }
        </Select>)
      case "number": return (
        <span className="ant-input-group-wrapper">
          <span className="ant-input-wrapper ant-input-group">
            {e.f_prefix ? <span className="ant-input-group-addon">{e.f_prefix}</span> : null}
            <InputNumber
              key={e.f_name}
              // min={e.f_min_len}
              addonBefore={e.f_prefix ? e.f_prefix : null}
              addonAfter={e.f_postfix ? e.f_postfix : null}
              type={e.f_type ? e.f_type : "text"}
              name={e.f_name}
              value={f_value ? parseFloat(f_value) : undefined}
              readOnly={e.f_readonly === 1 ? true : false}
              placeholder={"Enters" + e.name}
            />
            {e.f_postfix ? <span className="ant-input-group-addon">{e.f_postfix}</span> : null}
          </span>
        </span>
      )
      case "boolean": return (
        <Switch
          checked={((f_value === true || f_value === 1) ? true : false)}
          name={e.f_name}
          disabled={e.f_readonly === 1 ? true : false}
          onChange={(evt) => this.changeCondition(e, evt)}
        />
      )
      default: return (
        (e.f_max_len >= 10000)
          ?
          <TextArea
            key={e.f_name}
            name={e.f_name}
            maxLength={e.f_max_len}
            placeholder={"Enter " + e.name}
            readOnly={e.f_readonly === 1 ? true : false}
            autoSize={{ minRows: 2, maxRows: 2 }}
          />
          :
          <Input
            key={e.f_name}
            addonBefore={e.f_prefix ? e.f_prefix : null}
            addonAfter={e.f_postfix ? e.f_postfix : null}
            type={e.f_type ? e.f_type : "text"}
            name={e.f_name}
            onChange={e.f_name === "vessel_name" ? (evt) => this.generateVesselCode(evt.target.value) : null}
            readOnly={e.f_readonly === 1 ? true : false}
            placeholder={"Enter " + e.name}
          />
      )
    }
  }

  generateVesselCode = (e) => {
    if (this.vessel_name.typingTimeout) {
      clearTimeout(this.vessel_name.typingTimeout);
    }
    this.vessel_name = {
      name: e,
      typing: false,
      typingTimeout: setTimeout(() => {
        this.triggerVesselCodeApi(e)
      }, 2000)
    };
  }

  triggerVesselCodeApi = async (e) => {
    if (2 < e.length && e.length < 10) {
      const response = await getAPICall(`${URL_WITH_VERSION}/vessel/generate/code?vessel_name=${e}`);
      const responseData = await response;
      if (responseData && typeof responseData.data === "boolean") {
        openNotificationWithIcon("error", responseData.message)
        return;
      }
      if (responseData && typeof responseData.data === "object") {
        this.props.frmProp.form.setFieldsValue({
          vessel_code: (response.data && response.data.vessel_code ? response.data.vessel_code : null)
        });
        return;
      }
    } else {
      this.props.frmProp.form.setFieldsValue({
        vessel_code: null
      });
    }
  }

  changeCondition = (e, value) => {
    let { conditionalFields } = this.state;
    switch (e.f_name) {
      case "group": {
        if (conditionalFields) {
          let checkFields = conditionalFields.checkFields
          if (checkFields && checkFields.length > 0) {
            checkFields.map(name => {
              this.props.frmProp.form.setFieldsValue({
                [name]: undefined
              });
            })
          }
        }
        let options = (e.f_dyc_extras && e.f_dyc_extras.options ? e.f_dyc_extras.options : null);
        if (options) {
          let index = options.findIndex(i => i.value === value);
          let classGroup = "";
          if (index > -1) {
            classGroup = options[index]["class"];
          }
          this.props.frmProp.form.setFieldsValue({
            cargo_type: classGroup
          });
        }
        return true;
      }
      default: return true;
    }
  }

  render() {
    const { responseData, conditionalFields } = this.state
    const { getFieldDecorator, getFieldError, isFieldTouched, getFieldValue } = this.props.frmProp.form;
    return (
      <>
        {
          responseData.frm.length > 0 ? (
            responseData.frm.map((e, i) => {
              return (
                (
                  conditionalFields === null ||
                  (
                    conditionalFields !== null &&
                    getFieldValue(conditionalFields["key"]) === undefined &&
                    !conditionalFields["checkFields"].includes(e.f_name)
                  ) ||
                  (
                    conditionalFields !== null &&
                    getFieldValue(conditionalFields["key"]) !== undefined &&
                    !conditionalFields["checkFields"].includes(e.f_name)
                  ) ||
                  (
                    conditionalFields !== null &&
                    getFieldValue(conditionalFields["key"]) !== undefined &&
                    conditionalFields["checkFields"].includes(e.f_name) &&
                    conditionalFields["showFields"][getFieldValue(conditionalFields["key"])] &&
                    conditionalFields["showFields"][getFieldValue(conditionalFields["key"])].includes(e.f_name)
                  )
                )
                  ? (
                    <div className="col-md-4" key={e.f_name + "-" + i}>
                      <FormItem
                        label={e.name}
                        validateStatus={isFieldTouched(e.f_name) && getFieldError(e.f_name) ? 'error' : ''}
                        help={isFieldTouched(e.f_name) && getFieldError(e.f_name) || ''}
                      >
                        {
                          (
                            getFieldDecorator(e.f_name, {
                              rules: [{ required: (e.f_req === 1 ? true : false), message: `Please ${e.f_type === "dropdown" ? "Select " : "Input "} ${e.name}!` }],
                            })(
                              this.showTypeFields(e)
                            )
                          )
                        }
                      </FormItem>
                    </div>
                  ) : null)
            })) : (
            <div className="col col-lg-12">
              <Spin tip="Loading...">
                <Alert
                  message=" "
                  description="Please wait..."
                  type="info"
                />
              </Spin>
            </div>
          )
        }
      </>
    )
  }
}

export default NormalForm;