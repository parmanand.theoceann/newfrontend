export const BunkerDetailsCalculation = (formData, index) => {
  let ecafuelGrade = formData.eca_fuel_grade;
  let bunkerobj = {};
  let consarr = formData?.["."] ?? [];

  formData.portitinerary.map((el, ind) => {
    if (ind == index) {
      let { tsd, eca_days, passage, funct, days, xpd } = el;
      let nonEcadays = tsd;
      bunkerobj["ifo"] = SeafuelCons("3", passage, consarr, tsd);
      bunkerobj["vlsfo"] = SeafuelCons("5", passage, consarr, tsd);
      bunkerobj["lsmgo"] = SeafuelCons("7", passage, consarr, tsd);
      bunkerobj["mgo"] = SeafuelCons("4", passage, consarr, tsd);
      bunkerobj["ulsfo"] = SeafuelCons("10", passage, consarr, tsd);

      switch (ecafuelGrade) {
        case "3":
          nonEcadays = tsd - eca_days;
          bunkerobj["ifo"] = SeafuelCons("3", passage, consarr, nonEcadays);
          break;
        case "5":
          nonEcadays = tsd - eca_days;
          bunkerobj["vlsfo"] = SeafuelCons("5", passage, consarr, nonEcadays);
          break;
        case "7":
          nonEcadays = tsd - eca_days;
          bunkerobj["lsmgo"] = SeafuelCons("7", passage, consarr, nonEcadays);
          break;
        case "4":
          nonEcadays = tsd - eca_days;
          bunkerobj["mgo"] = SeafuelCons("4", passage, consarr, nonEcadays);
          break;
        case "10":
          nonEcadays = tsd - eca_days;
          bunkerobj["ulsfo"] = SeafuelCons("10", passage, consarr, nonEcadays);
          break;
        default:
          return;
      }
      bunkerobj["eca_consp"] = SeafuelCons(
        ecafuelGrade,
        passage,
        consarr,
        eca_days
      );
      bunkerobj["pc_ifo"] = portfuelCons("3", consarr, funct, days, xpd);
      bunkerobj["pc_vlsfo"] = portfuelCons("5", consarr, funct, days, xpd);
      bunkerobj["pc_lsmgo"] = portfuelCons("7", consarr, funct, days, xpd);
      bunkerobj["pc_mgo"] = portfuelCons("4", consarr, funct, days, xpd);
      bunkerobj["pc_ulsfo"] = portfuelCons("10", consarr, funct, days, xpd);
     
    }
  });
  return bunkerobj;
};

export const findCpPassage = (fuelType, passageType, consArr) => {
  let fuel_cons = 0;
  consArr?.map((el) => {
    const { fuel_type, fuel_code } = el;
    if (fuel_type == fuelType) {
      if (passageType === "1") {
        fuel_cons = isNaN(el?.ballast_value) ? 0 : parseFloat(el.ballast_value);
      } else if (passageType === "2") {
        fuel_cons = isNaN(el?.laden_value) ? 0 : parseFloat(el.laden_value);
      }
    }
  });

  fuel_cons = isNaN(fuel_cons) ? 0 : parseFloat(fuel_cons);
  return fuel_cons;
};

export const findPortFunc = (fuelType, consArr, portFunc) => {
  let port_cons = 0;
  let idlePort_cons = 0;
  consArr?.map((el) => {
    const { fuel_type, fuel_code } = el;
  
    if (fuel_type == fuelType) {
      idlePort_cons = el.con_ideal_on;
      switch (portFunc) {
        case "2":// loading
          port_cons = el.con_loading;
          break;
        case "9": // Dischagring
          port_cons = el.con_disch;
          break;
        default: // ideal
          port_cons = el.con_ideal_on;
      }
    }
  });

  port_cons = isNaN(port_cons) ? 0 : parseFloat(port_cons);
  idlePort_cons = isNaN(idlePort_cons) ? 0 : parseFloat(idlePort_cons);
  return { port_cons, idlePort_cons };
};

export const SeafuelCons = (fuelType, passageType, consArr, days) => {
  let fuel_cons = findCpPassage(fuelType, passageType, consArr);
  let _days = isNaN(days) ? 0 : parseFloat(days);
  return parseFloat(fuel_cons * _days).toFixed(2);
};

export const portfuelCons = (fuelType, consArr, portFunc, pdays, xpd) => {
  let { port_cons, idlePort_cons } = findPortFunc(fuelType, consArr, portFunc);
  let _pdays = isNaN(pdays) ? 0 : parseFloat(pdays);
  let _xpd = isNaN(xpd) ? 0 : parseFloat(xpd);
  let totalportcons = port_cons * _pdays + idlePort_cons * _xpd;
  return parseFloat(totalportcons).toFixed(2);
};
