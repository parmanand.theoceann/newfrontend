import React, { Component } from "react";
import {
  Form,
  Spin,
  Alert,
  Col,
  Tabs,
  Button,
  Row,
  Menu,
  Dropdown,
  Input,
  Tooltip,
  Collapse,
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  URL_WITHOUT_VERSION,
  openNotificationWithIcon,
  IMAGE_PATH,
} from "../../shared";
import IndexTableForm from "../../shared/TableForm/index_table";
import DynamicFormFields from "../../services/dynamic_form_fields";
import ReduxState from "../../services/redux_state";
import ModalAlertBox from "../ModalAlertBox";
import PopupSelection from "../PopupSelection";
import { createStore } from "redux";
import SideList from "../components/side-list/SideList";
// import * as resp1 from '../../mock_data.json';
import * as moment from "moment";
import {
  VoyageCIIDynamicsCalculation,
  VoyageEuetsCalculation,
} from "../../services/ciiCalculation";
import Cookies from "universal-cookie";
import { BunkerDetailsCalculation } from "./normalhelper";
import { ConsoleSqlOutlined } from "@ant-design/icons";
// import * as momenttz from 'moment-timezone';
// momenttz.tz.setDefault('UTC');
let divshow = "";
const { Panel } = Collapse;
const FormItem = Form.Item;
const GroupEle = Input.Group;
const TabPane = Tabs.TabPane;
const INITIAL_MODAL = {
  modalHeader: null,
  modalStatus: false,
  modalBody: null,
  modalFooter: null,
};
const cookies = new Cookies();
class NormalFormIndex extends Component {
  constructor(props) {
    super(props);

    this.resetFormFlag = false;
    this.elements = [];
    this.state = {
      showSideListBar:
        this.props.showSideListBar === false
          ? this.props.showSideListBar
          : true,
      resetFormFlag: false,
      responseData: { frm: [], tabs: [], active_tab: {} },
      forms: {},
      resp: this.props.formData || {},
      groupAction: this.props.hasOwnProperty("showGroupTableAction")
        ? this.props.showGroupTableAction === false
        : false,
      isShowFixedColumn: this.props.hasOwnProperty("isShowFixedColumn")
        ? this.props.isShowFixedColumn
        : "",
      extraFormFields: this.props.extraFormFields || undefined,
      extraTableButton: this.props.extraTableButton || undefined,
      labelModal: { ...INITIAL_MODAL },
      formDevideInCols: this.props.formDevideInCols || 3,
      tabEvents: this.props.tabEvents || [],
      formExtraEvents: this.props.formExtraEvents || {},
      formGroupDevideInCols: this.props.formGroupDevideInCols || {},
      dyncTabs: this.props.dyncTabs || {},
      sideList: this.props.sideList || {},
      frmOptions: this.props.frmOptions || [],
      sendBackData: this.props.sendBackData || false,
      hideGroup: this.props.hideGroup || [],
      showButtons: this.props.showButtons || [],
      disableTabs: this.props.disableTabs || [],
      tableRowDeleteAction: this.props.tableRowDeleteAction || (() => {}),
      editMode: this.props.hasOwnProperty("editMode")
        ? this.props.editMode
        : true,
      parentformName: this.props.parentformName || undefined,
      // initialAmount: 0
    };

    if (!this.props.dynamicForm) {
      ReduxState.setForm(this.props.frmCode);
      this.createStore = createStore(ReduxState.createReducer);
      this.dynamicForm = new DynamicFormFields(
        this.createStore,
        this.props.frmCode,
        this.props.ogState,
        (resp) => {
          this.setState({ ...this.state, resp }, () => {
            this.dynamicForm.setStateObject(this.state["resp"]);
          });
        },
        this.onEventResponse
      );
    } else {
      this.dynamicForm = this.props.dynamicForm;
      this.createStore = this.props.createStore;
    }
  }

  componentDidMount = async () => {
    if (this.props.formData && this.props.formData.hasOwnProperty("id")) {
      if (this.props.formData["id"] === 0) {
        delete this.props.formData["id"];
      }

      this.dynamicForm.setStateObject(this.props.formData);

      this.createStore.dispatch({
        type: "edit_info",
        editFormData: this.props.formData,
        setOriginalData: true,
      });
    }

    if (this.props.frmCode) {
      try {
        const response = await getAPICall(
          `${URL_WITHOUT_VERSION}get/${this.props.frmCode}`
        );
        const data = await response["data"];

        if (
          this.props.formData &&
          this.props.dyncTabs &&
          this.props.dyncTabs.hasOwnProperty("copyTab") &&
          this.props.dyncTabs.hasOwnProperty("tabs") &&
          data &&
          data.hasOwnProperty("tabs")
        ) {
          let tabs = data["tabs"].filter(
            (e) => e.frm_code === this.props.dyncTabs["copyTab"]
          );
          tabs = tabs && tabs.length === 1 ? tabs[0] : {};
          data["tabs"] = [];
          this.props.dyncTabs["tabs"].map((e) => {
            let tab = Object.assign({}, tabs, {
              tab_name: e.tabName,
              groupName: e.tabGroup,
            });
            data["tabs"].push(tab);
          });
        }

        if (this.props.frmOptions && this.props.frmOptions.length > 0) {
          this.props.frmOptions.map((e) => {
            data["frm"].map((fe, fi) => {
              if (fe["f_name"] === e["key"]) {
                data["frm"][fi]["f_dyc_extras"] = {
                  multiSelected: undefined,
                  options: e.data != 0 ? e.data : e.f_default,
                };
              }
            });
          });
        }
        this.setDefaultData(this.props.formData, data, () => {
          setTimeout(() => {
            this.setState({ ...this.state, responseData: data });
          }, 5);
        });

        this.dynamicForm.originalState.setFormFields(data["frm"]);
      } catch (err) {
       
        openNotificationWithIcon("error", "Something Went wrong.", 3);
      }
    } else if (this.props.frm) {
      this.setState({ ...this.state, responseData: this.props.frm });
      this.callForDdlBinding(this.props.frm);
    }
  };

  // componentDidUpdate = () => {
  //   if (
  //     this.props.hasOwnProperty('sendBackData') && this.props['sendBackData'].hasOwnProperty('show') &&
  //     this.props['sendBackData']['show'] === true && this.props.hasOwnProperty('triggerEvent') &&
  //     typeof this.props['triggerEvent'] === 'function'
  //   ) {
  //     this.props.triggerEvent(this.state.resp);
  //   }
  // }

  foldMenu() {
    const { showSideListBar } = this.state;
    this.setState({ ...this.state, showSideListBar: !showSideListBar });
  }

  setDefaultData = (formData, formProps, cb) => {
    let data = {};
    if (
      formProps &&
      formProps.hasOwnProperty("frm") &&
      formProps["frm"].length > 0
    ) {
      formProps.frm.map((e) => {
        if (e && e.f_default && e.f_default !== "" && !e.group_name) {
          let v = e.f_default;
          if (e.f_type === "boolean") {
            v =
              e.f_default === true
                ? e.f_default
                : e.f_default === "1"
                ? true
                : false;
          } else if (
            e.f_default === "now" &&
            (e.f_type === "date" ||
              e.f_type === "time" ||
              e.f_type === "datetime")
          ) {
            v = moment(new Date()).format("YYYY-MM-DDTHH:mm");
          }

          data[e.f_name] = v;
        } else if (
          e &&
          e.f_default &&
          e.f_default !== "" &&
          e.group_name &&
          e.group_name !== "" &&
          !e.f_g_frm_type
        ) {
          let v = e.f_default,
            gKey = this.dynamicForm.getGroupKey(e.group_name);
          if (!data.hasOwnProperty(gKey)) data[gKey] = {};
          if (e.f_type === "boolean") {
            v =
              e.f_default === true
                ? e.f_default
                : e.f_default === "1"
                ? true
                : false;
          } else if (
            e.f_default === "now" &&
            (e.f_type === "date" ||
              e.f_type === "time" ||
              e.f_type === "datetime")
          ) {
            v = moment(new Date()).format("YYYY-MM-DDTHH:mm");
          }

          data[gKey][e.f_name] = v;
        } else if (
          e &&
          e.f_default &&
          e.f_default !== "" &&
          e.group_name &&
          e.group_name !== "" &&
          e.f_g_frm_type
        ) {
          let v = e.f_default,
            gKey = this.dynamicForm.getGroupKey(e.group_name);
          let ffg = formProps.groups.filter((e) => e.group_key === gKey);

          if (ffg && ffg.length > 0) {
            if (ffg[0]["group_type"] === "Table") {
              if (!data.hasOwnProperty(gKey)) data[gKey] = [];
              if (e.f_type === "boolean") {
                v =
                  e.f_default === true
                    ? e.f_default
                    : e.f_default === "1"
                    ? true
                    : false;
              } else if (
                e.f_default === "now" &&
                (e.f_type === "date" ||
                  e.f_type === "time" ||
                  e.f_type === "datetime")
              ) {
                v = moment(new Date()).format("YYYY-MM-DDTHH:mm");
              }

              if (data[gKey].length === 0) {
                let ele = {};
                ele[e.f_name] = v;
                data[gKey].push(ele);
              } else {
                data[gKey][0][e.f_name] = v;
              }
            } else if (ffg[0]["group_type"] === "Normal") {
              if (!data.hasOwnProperty(gKey)) data[gKey] = {};
              if (e.f_type === "boolean") {
                v =
                  e.f_default === true
                    ? e.f_default
                    : e.f_default === "1"
                    ? true
                    : false;
              } else if (
                e.f_default === "now" &&
                (e.f_type === "date" ||
                  e.f_type === "time" ||
                  e.f_type === "datetime")
              ) {
                v = moment(new Date()).format("YYYY-MM-DDTHH:mm");
              }
              data[gKey][e.f_name] = v;
            }
          }
        }
      });

      data = Object.assign(data, formData);
      this.dynamicForm.setStateObject(data);
      this.createStore.dispatch({ type: "edit_info", editFormData: data });
      cb();
    }
  };

  onEventResponse = (event, data) => {
    const { responseData } = this.state;
    let formFields = Object.assign({}, responseData);
    let fieldValue = {};
    formFields.frm.map((e, i) => {
      if (e.f_name === event.addOptionsIn) {
        formFields.frm[i]["f_dyc_extras"] = {
          options: data,
          //multiSelected: formFields.frm[i]["f_dyc_extras"]["multiSelected"],
        };
      }
    });
    fieldValue[event.addOptionsIn] =
      data && data.length > 0 ? data[0]["id"] : "";
    this.setState({ ...this.state, responseData: formFields }, () => {
      if (
        (event.isUpdateValue && event.isUpdateValue === true) ||
        typeof event.isUpdateValue === "undefined"
      ) {
        this.createStore.dispatch({
          type: "add",
          groupName: undefined,
          index: -1,
          value: fieldValue,
        });
      }
    });
  };

  callForDdlBinding = async (data) => {
    if (data.frm.length > 0) {
      data.frm.map((e, i) => {
        if (e.f_name === "year_built") {
          let currentYear = new Date().getFullYear();
          let years = [],
            ddl = {};
          for (let i = currentYear; i > currentYear - 30; i--) {
            years.push({ label: i, value: i });
          }
          ddl["options"] = years;
          data.frm[i]["f_dyc_extras"] = ddl;
        }
      });
    }
    this.setState({ ...this.state, responseData: data });
  };

  generateMenus = (menus) => {
    return (
      <Menu>
        {menus
          ? menus.map((menu, i) => {
              if (menu.link != null) {
                let linkhrf = "href=" + menu.href ? menu.href : "#";
              } else {
                let linkhrf = "";
              }

              if (menu.label.length > 0) {
                return (
                  <Menu.Item key={i}>
                    <a
                      rel="noopener noreferrer"
                      linkhrf="true"
                      onClick={() => {
                        if (
                          menu.hasOwnProperty("event") &&
                          typeof menu.event === "function"
                        ) {
                          menu.event(menu.modalKey, this.state.resp);
                        }
                      }}
                    >
                      <>
                        {menu.icon ? menu.icon : undefined}
                        {menu.label ? menu.label : null}
                      </>
                    </a>
                  </Menu.Item>
                );
              }
            })
          : " "}
      </Menu>
    );
  };

  onCloseModal = async (showData) => {
    if (
      showData &&
      showData.hasOwnProperty("data") &&
      showData["data"] &&
      showData["data"].hasOwnProperty("rid")
    ) {
      this.dynamicForm.onChangeEvent(
        showData["data"]["rid"],
        showData["element"],
        undefined
      );
    }

    if (
      showData &&
      showData.hasOwnProperty("element") &&
      showData["element"] &&
      showData["element"].hasOwnProperty("f_label_display") &&
      showData["element"]["f_label_display"]
    ) {
      let dEle = JSON.parse(showData["element"]["f_label_display"]);
      if (
        dEle &&
        dEle.hasOwnProperty("listEvent") &&
        dEle["listEvent"] &&
        dEle["listEvent"].hasOwnProperty("apiLink")
      ) {
        let response = await getAPICall(
          `${URL_WITH_VERSION}${dEle["listEvent"]["apiLink"]}`
        );
        let respData = await response;
        if (
          respData &&
          respData.hasOwnProperty("data") &&
          respData["data"] === false
        ) {
          openNotificationWithIcon(
            "error",
            <div
              className="notify-error"
              dangerouslySetInnerHTML={{ __html: respData["message"] }}
            />,
            5
          );
        } else {
          this.onEventResponse(dEle["listEvent"], respData.data);
        }
      }

      if (dEle && dEle.hasOwnProperty("updateFields") && dEle["updateFields"]) {
        let jEle = {},
          oEle = dEle["updateFields"];
        if (showData && showData.hasOwnProperty("data") && showData["data"]) {
          Object.keys(oEle).map((e) => {
            jEle[e] = showData["data"][oEle[e]];
          });
          this.createStore.dispatch({
            type: "add",
            groupName: undefined,
            index: -1,
            value: jEle,
            formName: this.props.frmCode,
          });
        }
      }
    }

    this.setState({ ...this.state, labelModal: { status: false } }, () =>
      this.setState({ ...this.state, labelModal: { ...INITIAL_MODAL } })
    );
  };

  onPopupRowSelect = async (data, fieldSetting, field) => {
    let fjson = field["f_label_display"];
    if (typeof fjson === "string") {
      fjson = JSON.parse(fjson);
    }

    if (fjson && fjson.hasOwnProperty("putValues")) {
      let fCounter = [];
      fieldSetting.forEach((e) => {
        let fields = {};
        fields[e["toField"]] = data[e["fromField"]];
        this.createStore.dispatch({
          type: "add",
          groupName: field.group_name,
          value: fields,
        });
        fCounter.push(e);

        if (fCounter.length === fieldSetting.length) {
          setTimeout(() => this.onCloseModal(), 10);
        }
      });
    } else if (
      fjson &&
      fjson.hasOwnProperty("showAllValues") &&
      fjson["showAllValues"] === true &&
      data[fjson.orderBy] !== undefined
    ) {
      this.setState({ ...this.state, resetFormFlag: true });
      const resp = await getAPICall(
        `${URL_WITH_VERSION}${fjson.editApiLink}${data[fjson.orderBy]}`
      );
      const respData = await resp["data"];
      this.dynamicForm.setStateObject(Object.assign(respData, this.props.resp));
      this.setState(
        { ...this.state, resp: respData, resetFormFlag: false },
        () => this.onCloseModal()
      );
    }
  };

  updateMainForm = () => {
    if (typeof this.props.updateMainForm === "function") {
      this.props.updateMainForm(this.state.resp);
    }
  };

  showLinkPopup = (
    pElements,
    popupElements,
    filter = undefined,
    index = -1,
    cFormData = undefined,
    handelPopupReturn = undefined
  ) => {
    const { formExtraEvents } = this.state;
    let popupForms = {},
      editLink = undefined;
    if (
      pElements.hasOwnProperty("type") &&
      pElements.type !== "" &&
      pElements.type.toUpperCase() === "FORM"
    ) {
      let formData = {};
      if (
        pElements.hasOwnProperty("passData") &&
        pElements.passData !== "" &&
        pElements.passData.hasOwnProperty("length") &&
        pElements.passData.length > 0
      ) {
        const { resp } = this.state;
        pElements.passData.map((e) => {
          formData[e] = resp[e];
        });
      }

      if (
        formExtraEvents.hasOwnProperty("popups") &&
        formExtraEvents["popups"].hasOwnProperty(popupElements.f_name)
      ) {
        popupForms = formExtraEvents["popups"][popupElements.f_name];
      }

      if (
        pElements.hasOwnProperty("condition") &&
        pElements.hasOwnProperty("conditionValue") &&
        pElements["conditionValue"] !=
          cFormData[pElements["groupKey"]][index][pElements["condition"]]
      ) {
        return;
      }

      if (
        pElements.hasOwnProperty("editAPILink") &&
        pElements["editAPILink"].hasOwnProperty("fieldName") &&
        pElements["editAPILink"]["fieldName"] !== "" &&
        formData[pElements["editAPILink"]["fieldName"]] !== ""
      ) {
        editLink =
          pElements.editAPILink["apiLink"] +
          formData[pElements["editAPILink"]["fieldName"]];
      } else if (
        pElements.hasOwnProperty("editLink") &&
        pElements.editLink !== "" &&
        pElements.hasOwnProperty("condition")
      ) {
        editLink =
          pElements.editLink + filter[pElements["condition"]["fieldName"]];
      } else if (
        pElements.hasOwnProperty("editLink") &&
        pElements.editLink !== "" &&
        !pElements.hasOwnProperty("condition")
      ) {
        editLink = pElements.editLink;
      }

      this.setState({
        ...this.state,
        labelModal: {
          modalHeader: pElements.headerTitle,
          modalStatus: true,
          modalBody: () => (
            <PopupSelection
              apiLink={editLink}
              elementIndex={index}
              completeDataObject={{ fd: cFormData, pe: pElements }}
              formData={formData}
              frmCode={pElements.frmCode}
              isShowInPopup={pElements.isShowInPopup || false}
              isShowImport={pElements.isShowImport || false}
              isShowAddButton={
                pElements.hasOwnProperty("isShowAddButton")
                  ? pElements.isShowAddButton
                  : true
              }
              onCloseModal={
                pElements.hasOwnProperty("autoClose") &&
                pElements["autoClose"] === false
                  ? undefined
                  : this.onCloseModal
              }
              referenceElement={popupElements}
              popupFroms={popupForms}
              handelPopupReturn={handelPopupReturn}
              updateMainForm={() => {
                if (pElements.isUpdateMainForm === true) this.updateMainForm();
              }}
            />
          ),
          modalFooter: null,
          modalWidth: pElements.width || "90%",
        },
      });
    } else {
      editLink = pElements.apiLink;
      if (
        pElements.hasOwnProperty("valueFrom") &&
        pElements["valueFrom"] &&
        pElements["valueFrom"] !== ""
      ) {
        editLink =
          editLink + this.dynamicForm.getStateObject(pElements["valueFrom"]);
      }
      this.setState({
        ...this.state,
        labelModal: {
          modalHeader: pElements.headerTitle,
          modalStatus: true,
          modalBody: () => (
            <PopupSelection
              apiLink={editLink}
              identifyName={pElements.identifyName}
              orderBy={pElements.orderBy}
              filterBy={filter}
              onRowSelect={(e) =>
                this.onPopupRowSelect(e, pElements.putValues, popupElements)
              }
            />
          ),
          modalFooter: null,
          modalWidth: pElements.width || "90%",
        },
      });
    }
  };

  loadPopup = (
    popupElements,
    index = 0,
    formData = undefined,
    handelPopupReturn = undefined
  ) => {
    const { resp } = this.state;

    //let pElements = JSON.parse(popupElements["f_label_display"]);
    let pElements = JSON.parse(popupElements["f_label_display"] ?? "{}");
    if (
      pElements &&
      pElements.hasOwnProperty("condition") &&
      pElements["condition"].hasOwnProperty("fieldName")
    ) {
      if (
        pElements["condition"]["fieldName"] == "vessel_id" &&
        (pElements["condition"]["section"] == "tcov_quick" ||
          pElements["condition"]["section"] == "voy-relet") &&
        resp.hasOwnProperty("vesseldetails")
      ) {
        resp[pElements["condition"]["fieldName"]] =
          resp["vesseldetails"]["vessel_id"];
      }
      if (
        pElements["condition"]["fieldName"] == "vessel_id" &&
        pElements["condition"]["section"] == "tcto_quick" &&
        resp.hasOwnProperty("tcidetails")
      ) {
        resp[pElements["condition"]["fieldName"]] =
          resp["tcidetails"]["vessel_id"];
      }

      let filter = {};
      if (
        pElements["condition"].hasOwnProperty("gt") &&
        popupElements.hasOwnProperty("group_name") &&
        popupElements["group_name"] &&
        popupElements["group_name"] !== "" &&
        resp.hasOwnProperty(popupElements["group_name"]) &&
        resp[popupElements["group_name"]].hasOwnProperty(
          pElements["condition"]["fieldName"]
        ) &&
        resp[popupElements["group_name"]][pElements["condition"]["fieldName"]] *
          1 >
          0
      ) {
        filter[pElements["condition"]["fieldName"]] =
          resp[popupElements["group_name"]][
            pElements["condition"]["fieldName"]
          ];
        this.showLinkPopup(
          pElements,
          popupElements,
          filter,
          index,
          formData,
          handelPopupReturn
        );
      } else if (
        pElements["condition"].hasOwnProperty("gt") &&
        resp.hasOwnProperty(pElements["condition"]["fieldName"]) &&
        resp[pElements["condition"]["fieldName"]] * 1 > 0
      ) {
        filter[pElements["condition"]["fieldName"]] =
          resp[pElements["condition"]["fieldName"]];
        this.showLinkPopup(
          pElements,
          popupElements,
          filter,
          index,
          formData,
          handelPopupReturn
        );
      } else {
        let msg = "Please select " + popupElements.name + " and then click.";
        if (
          pElements["condition"].hasOwnProperty("gt") &&
          pElements["condition"].hasOwnProperty("message")
        ) {
          msg = pElements["condition"]["message"];
        }
        openNotificationWithIcon(
          "error",
          <div
            className="notify-error"
            dangerouslySetInnerHTML={{ __html: msg }}
          />,
          15
        );
      }
    } else {
      this.showLinkPopup(
        pElements,
        popupElements,
        undefined,
        index,
        formData,
        handelPopupReturn
      );
    }
  };

  displayColon = (field) => {
    let notNames = [".", "-"];
    let evaluate =
      field.name &&
      field.name !== "" &&
      field.name.trim() !== "" &&
      notNames.indexOf(field.name) === -1;
    return evaluate !== "" ? evaluate : false;
  };

  hasBooleanCheckbox = (field) => {
    let rv = false;
    if (
      field.f_type === "boolean" &&
      field.hasOwnProperty("f_dyc_extras") &&
      field.f_dyc_extras !== null &&
      field.f_dyc_extras !== undefined &&
      field.f_dyc_extras !== ""
    ) {
      let fProperty = field["f_dyc_extras"];
      if (typeof fProperty === "string") {
        fProperty = JSON.parse(fProperty);
      }
      rv =
        fProperty.hasOwnProperty("isCheckbox") &&
        fProperty["isCheckbox"] === true;
    }

    return rv;
  };

  displayLabel = (field) => {
    let notNames = [".", "-"],
      lableName = undefined;

    if (
      field.name &&
      field.name !== "" &&
      notNames.indexOf(field.name) === -1
    ) {
      lableName = field.name;

      if (
        field["f_label_display"] &&
        field["f_label_display"] !== "" &&
        field["f_type"] !== "link-field"
      ) {
        lableName = <a onClick={() => this.loadPopup(field)}>{field.name}</a>;
      }
    }

    return this.hasBooleanCheckbox(field) === true ? undefined : lableName;
  };

  collapseGroupItems = (e, i, formRows) => {
    let frRows = formRows.filter((fr) => e.group_name === fr.group_name);
    let _items = [];
    frRows.map((_fr, _fri) => {
      let __fr = Object.assign({}, _fr);
      __fr["collapse_group"] = 0;
      _items.push(this.renderItems(__fr, i + _fri, formRows, true)[0]);
    });

    let items = (
      <div className="col-md-12 quick-estimate-collapse">
        {" "}
        <Collapse>
          <Panel header={e.group_name}>{_items}</Panel>
        </Collapse>{" "}
      </div>
    );

    return [items, i + frRows.length];
  };

  renderItems = (e, i, formRows, noGroupTitle = false) => {
    let gKey = this.dynamicForm.getGroupKey(e.group_name);
    let items = undefined;

    if (e.collapse_group === 1 && e.group_name && e.group_name !== "") {
      let ipLen = this.collapseGroupItems(e, i, formRows);
      i = ipLen[1];
      items = ipLen[0];
    } else if (this.elements.indexOf(e.f_id) === -1) {
      if (
        (e.f_prefix && e.f_prefix !== "" && e.f_prefix * 1 > 0) ||
        (e.f_postfix && e.p_postfix !== "" && e.f_postfix * 1 > 0)
      ) {
        let frElement = formRows.filter(
          (fr) => fr.f_id === (e.f_prefix || e.f_postfix) * 1
        );
        if (frElement && frElement.length === 1) {
          items = (
            <div
              className={
                this.state.formDevideInCols > 1
                  ? "col-md-" + 12 / this.state.formDevideInCols
                  : "col-md-12"
              }
              key={e.f_name + "-" + i}
            >
              <FormItem
                label={this.displayLabel(e)}
                colon={this.displayColon(e)}
                labelCol={{ span: this.props.inlineLayout ? 12 : 24 }}
                wrapperCol={{ span: this.props.inlineLayout ? 12 : 24 }}
                // validateStatus={isFieldTouched(e.f_name) && getFieldError(e.f_name) ? 'error' : ''}
                // help={isFieldTouched(e.f_name) && getFieldError(e.f_name) || ''}
                className={e.f_req === 1 ? "astrick-on" : ""}
                // className = {(e.name === null || e.name === '') ? "no-label-display" : ""}
              >
                <GroupEle compact>
                  {this.dynamicForm.emptyCell(
                    e,
                    undefined,
                    { editable: true },
                    this.elements.indexOf(e.f_id) === -1,
                    false,
                    this.loadPopup
                  )}
                  {this.dynamicForm.emptyCell(
                    frElement[0],
                    undefined,
                    { editable: true },
                    undefined,
                    false,
                    this.loadPopup
                  )}
                </GroupEle>
              </FormItem>
            </div>
          );
        }
        this.elements.push((e.f_prefix || e.f_postfix) * 1);
      } else {
        items = (
          <div
            className={
              this.state.formDevideInCols > 1
                ? "col-md-" + 12 / this.state.formDevideInCols
                : "col-md-12"
            }
            key={e.f_name + "-" + i}
          >
            {this.hasBooleanCheckbox(e) === true ? (
              this.dynamicForm.emptyCell(e, undefined, { editable: true })
            ) : (
              <FormItem
                label={
                  this.displayLabel(e) === "" ? " " : this.displayLabel(e)
                }
                colon={this.displayColon(e)}
                labelCol={{ span: this.props.inlineLayout ? 12 : 24 }}
                wrapperCol={{ span: this.props.inlineLayout ? 12 : 24 }}
                className={e.f_req === 1 ? "astrick-on" : ""}
              >
                {this.dynamicForm.emptyCell(
                  e,
                  undefined,
                  { editable: true },
                  undefined,
                  true,
                  this.loadPopup
                )}
              </FormItem>
            )}
          </div>
        );
      }

      // if (!this.dynamicForm.hasGroupKey(gKey) && noGroupTitle === false) {
      //   const regex = /^(\-+)|(\.+)$/g;
      //   items = (
      //     <>
      //       {e.group_name && !e.group_name.match(regex) ? (
      //         <div key={"parent-" + gKey + "-" + i} className="col-md-12">
      //           <div className="wrap-group-heading" key={gKey + "-" + i}>
      //             <h3 className="normal-heading">{e.group_name}</h3>
      //           </div>
      //         </div>
      //       ) : (
      //         ""
      //       )}
      //       {items}
      //     </>
      //   );
      // }
    }

    return [items, i];
  };

  // setReRenderStatus = (status) => {
  //   this.setState({ ...this.state, renderItems: { reRender: status } });
  // };

  calculatedData = (copyData, cp1, cp2, ft, spdtype, passage, cb) => {
    let defaultRow = {};
    ft.map((t) => (defaultRow[t] = 0));

    if (
      spdtype &&
      typeof spdtype !== "string" &&
      spdtype.hasOwnProperty("length") &&
      cp1 &&
      cp2
    ) {
      cp2.map((e, i) => {
        let tsd = e.tsd;
        let std = cp1[spdtype[0] + "_data"];
        let ptd = passage[0] + "_value";
        if (e.spd_type === "2") std = cp1[spdtype[1] + "_data"];
        if (e.passage === "2") ptd = passage[1] + "_value";

        if (i > 0) {
          ft.map((t) => {
            let v = std
              ? std.filter((s) => s.fuel_code.toLowerCase() === t)
              : [];
            if (v && v.length === 1) {
              let value = v[0][ptd] * tsd;
              copyData[i][t] = isNaN(value) ? 0 : value;
            }
          });
        } else {
          copyData[i] = Object.assign(copyData[i], defaultRow);
        }
      });

      if (cb && typeof cb === "function") {
        return cb(copyData);
      }
    }

    return copyData;
  };

  calculateSeaCons = (item, FUEL_HEAD, type) => {
    let ecaconst;
    let laden_value = isNaN(FUEL_HEAD.laden_value)
      ? 0.0
      : parseFloat(FUEL_HEAD.laden_value);
    let ballast_value = isNaN(FUEL_HEAD.ballast_value)
      ? 0.0
      : parseFloat(FUEL_HEAD.ballast_value);
    let retVal = 0.0;

    if (type == "ECA_CONS") {
      let eca_days =
        item && item.eca_days && !isNaN(item.eca_days)
          ? parseFloat(item.eca_days)
          : 0;
      const consValOfType = item.passage === "2" ? laden_value : ballast_value;
      const calcVal = eca_days * consValOfType;
      retVal = calcVal;
    }

    if (type == "CO2_CONS") {
      let ECOSCO2 = this.props.formData["."];
      let IFO = 0,
        VLSFO = 0,
        LSMGO = 0,
        MGO = 0,
        ULSFO = 0; // Default values

      let tsd = item && item.tsd && !isNaN(item.tsd) ? parseFloat(item.tsd) : 0;
      //let xsd = (item && item.xsd && !isNaN(item.xsd)) ? parseFloat(item.xsd) : 0;

      ECOSCO2 &&
        ECOSCO2.map((items) => {
          // Assigning Values
          switch (items.fuel_code) {
            case "IFO":
              IFO = items;
              break;
            case "VLSFO":
              VLSFO = items;
              break;
            case "LSMGO":
              LSMGO = items;
              break;
            
            case "ULSFO":
              ULSFO = items;
              break;
          }
        });
      let laden_valueifo = isNaN(IFO.laden_value)
        ? 0.0
        : parseFloat(IFO.laden_value);
      let laden_valuevlsfo = isNaN(VLSFO.laden_value)
        ? 0.0
        : parseFloat(VLSFO.laden_value);
      let laden_valuevlsmgo = isNaN(LSMGO.laden_value)
        ? 0.0
        : parseFloat(LSMGO.laden_value);
      let laden_valuevulsfo = isNaN(ULSFO.laden_value)
        ? 0.0
        : parseFloat(ULSFO.laden_value);

      let ladenco2 =
        laden_valueifo * 3.1 * tsd +
        laden_valuevlsfo * 3.01 * tsd +
        laden_valuevlsmgo * 3.01 * tsd +
        laden_valuevulsfo * 3.01 * tsd;

      let ballast_valueifo = isNaN(IFO.ballast_value)
        ? 0.0
        : parseFloat(IFO.ballast_value);
      let ballast_valuevlsfo = isNaN(VLSFO.ballast_value)
        ? 0.0
        : parseFloat(VLSFO.ballast_value);
      let ballast_valuelsmgo = isNaN(LSMGO.ballast_value)
        ? 0.0
        : parseFloat(LSMGO.ballast_value);
      let ballast_valueulsfo = isNaN(ULSFO.ballast_value)
        ? 0.0
        : parseFloat(ULSFO.ballast_value);

      let ballastco2 =
        ballast_valueifo * 3.1 * tsd +
        ballast_valuevlsfo * 3.01 * tsd +
        ballast_valuelsmgo * 3.01 * tsd +
        ballast_valueulsfo * 3.01 * tsd;

      const consValOfType = item.passage === "2" ? ladenco2 : ballastco2;
      retVal = consValOfType;
    }

    if (type == "SEA_CONS") {
      let tsd = item && !isNaN(item.tsd) ? parseFloat(item.tsd) : 0;
      let eca_days =
        item && item.eca_days && !isNaN(item.eca_days)
          ? parseFloat(item.eca_days)
          : 0;

      //let xsd = (item && !isNaN(item.xsd)) ? parseFloat(item.xsd) : 0;
      const consValOfType = item.passage === "2" ? laden_value : ballast_value;

      let calcVal = tsd * consValOfType;

      if (FUEL_HEAD.fuel_code == "IFO" || FUEL_HEAD.fuel_code == "VLSFO") {
        let ecaconst = eca_days * consValOfType;
        calcVal = parseFloat(calcVal - ecaconst);
      }
      if (FUEL_HEAD.fuel_code == "LSMGO") {
        let ecaconst = eca_days * consValOfType;
        calcVal = parseFloat(calcVal + ecaconst);
      }

      retVal = calcVal;
    } else if (type == "PORT_CONS") {
      // for portconsumptions we need to understand Function [funct]
      // funct 2 => L
      // funct 3 => F
      // funct 9 => D
      let portConsOfType = 0;

      switch (item.funct) {
        case "2":
          portConsOfType = isNaN(FUEL_HEAD.con_loading)
            ? 0.0
            : parseFloat(FUEL_HEAD.con_loading);
          break; //con_loading
        case "9":
          portConsOfType = isNaN(FUEL_HEAD.con_disch)
            ? 0.0
            : parseFloat(FUEL_HEAD.con_disch);
          break; //con_disch
        default:
          portConsOfType = isNaN(FUEL_HEAD.con_ideal_on)
            ? 0.0
            : parseFloat(FUEL_HEAD.con_ideal_on);
          break; //con_ideal_on
      }
      let idleCons = isNaN(FUEL_HEAD.con_ideal_on)
        ? 0.0
        : parseFloat(FUEL_HEAD.con_ideal_on);
      retVal =
        eval(parseFloat(item.days) * portConsOfType) +
        eval(parseFloat(item.xpd == "" ? 0 : item.xpd) * idleCons);
    }
    return isNaN(retVal) ? 0.0 : parseFloat(retVal).toFixed(2);
  };

  calculateSeaConss = (item, FUEL_HEAD, LSMGOCONS, type) => {
    let laden_value = isNaN(FUEL_HEAD.laden_value)
      ? 0.0
      : parseFloat(FUEL_HEAD.laden_value);
    let ballast_value = isNaN(FUEL_HEAD.ballast_value)
      ? 0.0
      : parseFloat(FUEL_HEAD.ballast_value);
    let retVal = 0.0;

    if (type == "SEA_CONSS") {
      let tsd = item && !isNaN(item.tsd) ? parseFloat(item.tsd) : 0;
      const consValOfType = item.passage === "2" ? laden_value : ballast_value;
      let calcVal = tsd * consValOfType;
      calcVal = calcVal + parseFloat(LSMGOCONS);
      retVal = calcVal;
    }
    return isNaN(retVal) ? 0.0 : parseFloat(retVal).toFixed(2);
  };

  findCpPassage = (fuelType, passageType, consArr) => {
    let cp_price = 0;
    let fuel_cons = 0;
    consArr?.map((el) => {
      const { fuel_type, fuel_code } = el;
      if (fuel_type == fuelType) {
        cp_price = parseFloat(el.cp_price);
        if (passageType === "1") {
          fuel_cons = isNaN(el?.ballast_value)
            ? 0
            : parseFloat(el.ballast_value);
        } else if (passageType === "2") {
          fuel_cons = isNaN(el?.laden_value) ? 0 : parseFloat(el.laden_value);
        }
      }
    });
    cp_price = isNaN(cp_price) ? 0 : parseFloat(cp_price);
    fuel_cons = isNaN(fuel_cons) ? 0 : parseFloat(fuel_cons);
    return { cp_price, fuel_cons };
  };

  EcaSeaconsCalculation = (ele, fuelType, consarr) => {
    let ecafuelConsPrice = 0;
    const { eca_days, passage } = ele;
    const { cp_price, fuel_cons } = this.findCpPassage(
      fuelType,
      passage,
      consarr
    );
    ecafuelConsPrice = cp_price * eca_days * fuel_cons;
    return ecafuelConsPrice.toFixed(2);
  };

  /*
  tabChangeEvent = (e) => {
    const { tabEvents, showButtons } = this.state;

    const { formData } = this.props;
    let copyData2 = [];
    let copy = {};

    let ttlecaDist = 0,
      ttlecaday = 0,
      ttlmiles = 0,
      ttlspeed = 0,
      eculsfo = 0,
      eclsmgo = 0,
      ttlifo = 0,
      ttlvlsfo = 0,
      ttllsmgo = 0,
      ttlmgo = 0,
      ttlulsfo = 0,
      ttlPcifo = 0,
      ttlPcvlsfo = 0,
      ttlPclsmgo = 0,
      ttlPcmgo = 0,
      ttlPculsfo = 0;
    let bunkerarrlength =
      this.props.formData &&
      this.props.formData.bunkerdetails &&
      this.props.formData.bunkerdetails.length
        ? this.props.formData.bunkerdetails.length
        : 1;
    let { resp } = this.state;

    tabEvents.map((te) => {
      if (
        te.tabName === e &&
        te.event &&
        te.event.type === "copy" &&
        !te.event.hasOwnProperty("condition") &&
        te.event.hasOwnProperty("showSingleIndex") &&
        !te.event.showSingleIndex
      ) {
        let keyName = this.dynamicForm.getGroupKey(te.event.from);
        let copyName = this.dynamicForm.getGroupKey(te.tabName);
        let stObject = this.dynamicForm.getStateObject();

        if (
          resp &&
          resp.hasOwnProperty(keyName) &&
          resp[keyName] &&
          copyName != "portitinerary"
        ) {
          //skip if returning to port itinerary
          let fields = Object.assign({}, te.event.fields);
          let fieldKeys = Object.keys(fields);
          let copyData = [],
            groupData = [];
          let matchPortId = false;

          resp[keyName].map((rkn, rki) => {
            let crkn = { editable: true, index: rki };
            if (fieldKeys) {
              fieldKeys.map((fk) => {
                if (
                  // this.props.formData.bunkerdetails &&
                  te.tabName === "Bunker Details" &&
                  te.event.from === "Port Itinerary" &&
                  this.props.formData.bunkerdetails &&
                  this.props.formData.bunkerdetails[rki] &&
                  rkn["port_id"] ==
                    this.props.formData.bunkerdetails[rki].port_id
                ) {
                  matchPortId = true;

                  crkn[fields[fk]] =
                    fk == "port"
                      ? this.props.formData.bunkerdetails[rki][fk]
                      : rkn[fk] || this.props.formData.portitinerary[rki][fk]; // new row will not be there in props
                } else {
                  if (rkn.hasOwnProperty(fk)) {
                    crkn[fields[fk]] = rkn[fk];
                  }
                }
              });
            }

            let ECOS = stObject["."];

            let IFO = 0,
              VLSFO = 0,
              LSMGO = 0,
              MGO = 0,
              ULSFO = 0; // Default values
            ECOS &&
              ECOS.map((item) => {
                // Assigning Values
                switch (item.fuel_code) {
                  case "IFO":
                    IFO = item;
                    break;
                  case "VLSFO":
                    VLSFO = item;
                    break;
                  case "LSMGO":
                    LSMGO = item;
                    break;
                  case "MGO":
                    MGO = item;
                    break;
                  case "ULSFO":
                    ULSFO = item;
                    break;
                }
              });

            // if (this.props.formData.bunkerdetails) {
            //   this.props.formData.bunkerdetails[rki] = crkn;
            // }

            // FOR Bunker Details tab .. Sea Cons. in MT

            if (keyName == "portitinerary") {
              if (te.tabName === "Bunker Details") {
                resp[keyName].map((item, index) => {
                  if (
                    crkn["port_id"] === item.port_id &&
                    crkn["index"] == index
                  ) {
                    crkn["ec_ulsfo"] = this.calculateSeaCons(
                      item,
                      ULSFO,
                      "ECA_CONS"
                    );

                    if (
                      (VLSFO.ballast_value || VLSFO.laden_value) &&
                      (IFO.ballast_value || IFO.laden_value)
                    ) {
                      crkn["ec_lsmgo"] = this.calculateSeaCons(
                        item,
                        VLSFO,
                        "ECA_CONS"
                      );
                    } else if (VLSFO.ballast_value || VLSFO.laden_value) {
                      crkn["ec_lsmgo"] = this.calculateSeaCons(
                        item,
                        VLSFO,
                        "ECA_CONS"
                      );
                    } else if (!VLSFO.ballast_value && !VLSFO.laden_value) {
                      crkn["ec_lsmgo"] = this.calculateSeaCons(
                        item,
                        IFO,
                        "ECA_CONS"
                      );
                    } else {
                      crkn["ec_lsmgo"] = this.calculateSeaCons(
                        item,
                        VLSFO,
                        "ECA_CONS"
                      );
                    }

                    crkn["co_emmission"] = this.calculateSeaCons(
                      item,
                      IFO,
                      "CO2_CONS"
                    );

                    crkn["eca_consp"] =
                      formData?.eca_fuel_grade && formData?.["."].length > 0
                        ? this.EcaSeaconsCalculation(
                            item,
                            formData.eca_fuel_grade,
                            formData["."]
                          )
                        : this.EcaSeaconsCalculation(
                            item,
                            resp.eca_fuel_grade,
                            resp["."]
                          );
                    crkn["id"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? this.props.formData.bunkerdetails[rki]["id"]
                        : "";
                    if (te.event.page == "tcov" || te.event.page == "tcto") {
                      crkn["arrival_date_time"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki][
                          "arrival_date_time"
                        ] &&
                        this.props.formData.bunkerdetails[rki][
                          "arrival_date_time"
                        ] !== "0000:00:00 00:00"
                          ? moment(
                              this.props.formData.bunkerdetails[rki][
                                "arrival_date_time"
                              ]
                            ).format("YYYY-MM-DDTHH:mm:ss")
                          : "";
                      crkn["departure"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["departure"] &&
                        this.props.formData.bunkerdetails[rki]["departure"] !==
                          "0000:00:00 00:00"
                          ? moment(
                              this.props.formData.bunkerdetails[rki][
                                "departure"
                              ]
                            ).format("YYYY-MM-DDTHH:mm:ss")
                          : "";
                      crkn["ifo"] = this.calculateSeaCons(
                        item,
                        IFO,
                        "SEA_CONS"
                      );
                      crkn["vlsfo"] = this.calculateSeaCons(
                        item,
                        VLSFO,
                        "SEA_CONS"
                      );
                      crkn["lsmgo"] = this.calculateSeaConss(
                        item,
                        LSMGO,
                        crkn["ec_lsmgo"],
                        "SEA_CONSS"
                      );
                      crkn["mgo"] = this.calculateSeaCons(
                        item,
                        MGO,
                        "SEA_CONS"
                      );
                      crkn["ulsfo"] = this.calculateSeaCons(
                        item,
                        ULSFO,
                        "SEA_CONS"
                      );

                      crkn["pc_ifo"] = this.calculateSeaCons(
                        item,
                        IFO,
                        "PORT_CONS"
                      );
                      crkn["pc_vlsfo"] = this.calculateSeaCons(
                        item,
                        VLSFO,
                        "PORT_CONS"
                      );
                      crkn["pc_lsmgo"] = this.calculateSeaCons(
                        item,
                        LSMGO,
                        "PORT_CONS"
                      );
                      crkn["pc_mgo"] = this.calculateSeaCons(
                        item,
                        MGO,
                        "PORT_CONS"
                      );
                      crkn["pc_ulsfo"] = this.calculateSeaCons(
                        item,
                        ULSFO,
                        "PORT_CONS"
                      );
                    } else {
                      crkn["id"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki]
                          ? this.props.formData.bunkerdetails[rki]["id"]
                          : "";
                      crkn["arrival_date_time"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki][
                              "arrival_date_time"
                            ]
                          : this.props.formData.portitinerary[rki][
                              "arrival_date_time"
                            ];
                      crkn["departure"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["departure"]
                          : this.props.formData.portitinerary[rki]["departure"];
                      crkn["ifo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["ifo"]
                          : this.calculateSeaCons(item, IFO, "SEA_CONS");
                      crkn["vlsfo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["vlsfo"]
                          : this.calculateSeaCons(item, VLSFO, "SEA_CONS");
                      crkn["lsmgo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["lsmgo"]
                          : this.calculateSeaConss(
                              item,
                              LSMGO,
                              crkn["ec_lsmgo"],
                              "SEA_CONSS"
                            );
                      //crkn["lsmgo"] = this.calculateSeaCons(item, LSMGO, "SEA_CONS");
                      crkn["mgo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["mgo"]
                          : this.calculateSeaCons(item, MGO, "SEA_CONS");
                      crkn["ulsfo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["ulsfo"]
                          : this.calculateSeaCons(item, ULSFO, "SEA_CONS");

                      crkn["pc_ifo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["pc_ifo"]
                          : this.calculateSeaCons(item, IFO, "PORT_CONS");
                      crkn["pc_vlsfo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["pc_vlsfo"]
                          : this.calculateSeaCons(item, VLSFO, "PORT_CONS");
                      crkn["pc_lsmgo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["pc_lsmgo"]
                          : this.calculateSeaCons(item, LSMGO, "PORT_CONS");
                      crkn["pc_mgo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["pc_mgo"]
                          : this.calculateSeaCons(item, MGO, "PORT_CONS");
                      crkn["pc_ulsfo"] =
                        this.props.formData.bunkerdetails &&
                        this.props.formData.bunkerdetails[rki] &&
                        this.props.formData.bunkerdetails[rki]["lock_status"] ==
                          1
                          ? this.props.formData.bunkerdetails[rki]["pc_ulsfo"]
                          : this.calculateSeaCons(item, ULSFO, "PORT_CONS");
                    }
                    crkn["arob_ifo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["arob_ifo"]
                          ).toFixed(2)
                        : 0.0;
                    crkn["arob_vlsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["arob_vlsfo"]
                          ).toFixed(2)
                        : 0.0;
                    crkn["arob_lsmgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["arob_lsmgo"]
                          ).toFixed(2)
                        : 0.0;
                    crkn["arob_mgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["arob_mgo"]
                          ).toFixed(2)
                        : 0.0;
                    crkn["arob_ulsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["arob_ulsfo"]
                          ).toFixed(2)
                        : 0.0;

                    crkn["r_ifo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["r_ifo"]
                          ).toFixed(2)
                        : 0;
                    crkn["r_vlsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["r_vlsfo"]
                          ).toFixed(2)
                        : 0;
                    crkn["r_lsmgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["r_lsmgo"]
                          ).toFixed(2)
                        : 0;
                    crkn["r_mgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["r_mgo"]
                          ).toFixed(2)
                        : 0;
                    crkn["r_ulsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["r_ulsfo"]
                          ).toFixed(2)
                        : 0;

                    crkn["dr_ifo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["dr_ifo"]
                          ).toFixed(2)
                        : 0;
                    crkn["dr_vlsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["dr_vlsfo"]
                          ).toFixed(2)
                        : 0;
                    crkn["dr_lsmgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["dr_lsmgo"]
                          ).toFixed(2)
                        : 0;
                    crkn["dr_mgo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["dr_mgo"]
                          ).toFixed(2)
                        : 0;
                    crkn["dr_ulsfo"] =
                      this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["dr_ulsfo"]
                          ).toFixed(2)
                        : 0;

                    crkn["tcov_id"] = item["tcov_id"];
                    ttlecaDist =
                      ttlecaDist +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["seca_length"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki][
                              "seca_length"
                            ]
                          )
                        : 0);
                    ttlecaday =
                      ttlecaday +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["eca_days"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["eca_days"]
                          )
                        : 0);
                    ttlmiles =
                      ttlmiles +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["miles"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["miles"]
                          )
                        : 0);
                    ttlspeed =
                      ttlspeed +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["speed"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["speed"]
                          )
                        : 0);
                    eculsfo =
                      eculsfo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["ec_ulsfo"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["ec_ulsfo"]
                          )
                        : 0);
                    eclsmgo =
                      eclsmgo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki] &&
                      this.props.formData.bunkerdetails[rki]["ec_lsmgo"]
                        ? parseFloat(
                            this.props.formData.bunkerdetails[rki]["ec_lsmgo"]
                          )
                        : 0);

                    ttlifo =
                      ttlifo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["ifo"])
                        : 0);

                    ttlvlsfo =
                      ttlvlsfo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["vlsfo"])
                        : 0);
                    ttllsmgo =
                      ttllsmgo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["lsmgo"])
                        : 0);
                    ttlmgo =
                      ttlmgo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["mgo"])
                        : 0);
                    ttlulsfo =
                      ttlulsfo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["ulsfo"])
                        : 0);

                    ttlPcifo =
                      ttlPcifo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["pc_ifo"])
                        : 0);
                    ttlPcvlsfo =
                      ttlPcvlsfo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["pc_vlsfo"])
                        : 0);
                    ttlPclsmgo =
                      ttlPclsmgo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["pc_lsmgo"])
                        : 0);
                    ttlPcmgo =
                      ttlPcmgo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["pc_mgo"])
                        : 0);
                    ttlPculsfo =
                      ttlPculsfo +
                      (this.props.formData.bunkerdetails &&
                      this.props.formData.bunkerdetails[rki]
                        ? parseFloat(crkn["pc_ulsfo"])
                        : 0);

                    // this.props.formData.bunkerdetails[rki] = crkn;
                    // } else {
                    // there is no props could be a first time copy
                    //if copyData[rki]  None:

                    copyData[rki] = crkn;
                    // }
                  }
                });
              } else if (te.tabName === "Port Date Details") {
                let fd = this.props.formData;

                // crkn["arrival_date_time"] = (fd.hasOwnProperty("portdatedetails") && fd.portdatedetails != undefined && fd.portdatedetails && fd.portdatedetails[rki]) ?
                //     moment(fd.portdatedetails[rki]["arrival_date_time"]).format('YYYY-MM-DD HH:mm') :
                //     (fd.hasOwnProperty("portdatedetails") && fd.portdatedetails != undefined && fd.portdatedetails && fd.portdatedetails[rki] ?
                //       moment(fd.portdatedetails[rki]["arrival_date_time"]).format('YYYY-MM-DD HH:mm')
                //       : '');
                //  crkn["departure"] = fd.portdatedetails && fd.portdatedetails[rki] ? moment(fd.portdatedetails[rki]["departure"]).format('YYYY-MM-DD HH:mm') : (fd.portdatedetails && fd.portdatedetails[rki] ? moment(fd.portdatedetails[rki]["departure"]).format('YYYY-MM-DD HH:mm') : '');

                crkn["arrival_date_time"] =
                  fd && fd.portdatedetails && fd.portdatedetails[rki]
                    ? moment(
                        fd.portdatedetails[rki]["arrival_date_time"]
                      ).format("YYYY-MM-DDTHH:mm")
                    : "";
                crkn["departure"] =
                  fd && fd.portdatedetails && fd.portdatedetails[rki]
                    ? moment(fd.portdatedetails[rki]["departure"]).format(
                        "YYYY-MM-DDTHH:mm"
                      )
                    : "";

                // this.props.formData.portdatedetails[rki] = crkn;
                crkn["day"] =
                  fd.portdatedetails && fd.portdatedetails[rki]
                    ? moment(fd.portdatedetails[rki]["arrival_date_time"])
                        .format("dddd")
                        .substring(0, 3)
                    : "";

                let tpdays =
                  resp.portitinerary[rki]["days"] * 1 +
                  resp.portitinerary[rki]["xpd"] * 1;

                crkn["pdays"] = isNaN(tpdays) ? 0 : tpdays.toFixed(2);

                crkn["tsd"] =
                  resp.portitinerary && resp.portitinerary[rki]
                    ? parseFloat(resp.portitinerary[rki]["tsd"]).toFixed(2)
                    : "";
                crkn["xsd"] =
                  resp.portitinerary && resp.portitinerary[rki]
                    ? parseFloat(resp.portitinerary[rki]["xsd"]).toFixed(2)
                    : "";
                crkn["s_type"] = crkn["spd_type"] || crkn["s_type"];
                crkn["id"] =
                  fd.portdatedetails && fd.portdatedetails[rki]
                    ? fd.portdatedetails[rki]["id"]
                    : "";
                copyData[rki] = crkn;
              }
            }
          });

          if (
            te.event.hasOwnProperty("group") &&
            te.event.group &&
            resp[te.event.group.from]
          ) {
            groupData.push({
              groupName: te.event.group.to,
              value: resp[te.event.group.from],
            });
          }
          if (
            resp &&
            resp.hasOwnProperty(keyName) &&
            resp[keyName]
            // && !matchPortId
          ) {
            // logic is perfectly fine only issue is state update (re-render which is done only once)
            if (copyName === "bunkerdetails") {
              resp[copyName] = copyData;

              this.dynamicForm.setData(
                "bunkerdetails",
                copyData,
                groupData,
                undefined,
                "COPYST"
              );

              // let copyData2 = []
              // let copy={}

              copy["ttl_seca_length"] = ttlecaDist.toFixed(2);
              copy["ttl_eca_days"] = ttlecaday.toFixed(2);
              copy["ttl_miles"] = ttlmiles.toFixed(2);
              copy["ttl_speed"] = (ttlspeed / bunkerarrlength).toFixed(2);
              copy["ttl_ec_ulsfo"] = eculsfo.toFixed(2);
              copy["ttl_ec_lsmgo"] = eclsmgo.toFixed(2);

              copy["ttl_ifo"] = ttlifo.toFixed(2);
              copy["ttl_vlsfo"] = ttlvlsfo.toFixed(2);
              copy["ttl_lsmgo"] = ttllsmgo.toFixed(2);
              copy["ttl_mgo"] = ttlmgo.toFixed(2);
              copy["ttl_ulsfo"] = ttlulsfo.toFixed(2);

              copy["ttl_pc_ifo"] = ttlPcifo.toFixed(2);
              copy["ttl_pc_vlsfo"] = ttlPcvlsfo.toFixed(2);
              copy["ttl_pc_lsmgo"] = ttlPclsmgo.toFixed(2);
              copy["ttl_pc_mgo"] = ttlPcmgo.toFixed(2);
              copy["ttl_pc_ulsfo"] = ttlPculsfo.toFixed(2);
              copyData2.push(copy);
              resp["totalbunkerdetails"] = copyData2;
              this.dynamicForm.setData(
                "totalbunkerdetails",
                copyData2,
                "totalbunkerdetails",
                undefined,
                undefined
              );
            } else if (copyName === "portdatedetails") {
              // delete this.props.formData.portdatedetails;

              resp[copyName] = copyData;
              this.dynamicForm.setData(
                "portdatedetails",
                copyData,
                groupData,
                undefined,
                "COPYST"
              );
              // this.setState({ ...this.state }, () => { this.props.formData.portdatedetails = copyData; });
            } else if (copyName === "portcargo") {
              resp[copyName] = copyData;
              this.dynamicForm.setData(
                "portcargo",
                copyData,
                groupData,
                undefined,
                "COPYST"
              );
            } else {
              this.dynamicForm.setData(copyName, resp[keyName], groupData);
            }
          } else {
            this.dynamicForm.setData(copyName, copyData, groupData);
          }

          if (te.event.calculations && te.event.calculations.length > 0) {
            this.calculatedData(
              copyData,
              resp[te.event.calculations[0]],
              resp[keyName],
              te.event.calculations[1],
              te.event.calculations[2],
              te.event.calculations[3],
              () => {
                if (resp && !resp.hasOwnProperty(copyName)) {
                  this.dynamicForm.setData(copyName, copyData, groupData);
                } else if (
                  resp &&
                  resp.hasOwnProperty(copyName) &&
                  resp[copyName]
                ) {
                  this.dynamicForm.setData(
                    copyName,
                    Object.assign(resp[copyName], copyData),
                    groupData
                  );
                }
              }
            );
          } else {
            // if (resp && !resp.hasOwnProperty(copyName)) {
            //   var port = 0;

            //   if (copyName == 'bunkerdetails' || copyName == 'portdatedetails') {
            //     copyData.forEach(element => {
            //       port = element.port;
            //       element.port = element.port_id;
            //       element.port_id = port;
            //     });
            //     copyData.shift();
            //   }
            //   this.dynamicForm.setData(copyName, copyData, groupData);
            // } else if (resp && resp.hasOwnProperty(copyName) && resp[copyName]) {
            // var port = 0;
            // var indexData = 0;
            // copyData.shift();
            // if (copyName == 'bunkerdetails' || copyName == 'portdatedetails') {
            //   copyData.forEach(element => {
            //     port = element.port;
            //     element.port = element.port_id;
            //     element.port_id = port;
            //     element.index = indexData;
            //     element.key = "table_row_" + indexData
            //     indexData = indexData + 1;
            //   });
            // }
            if (resp[copyName] == undefined) {
              // first click initially may have no values in response
              resp[copyName] = [];
            }

            // this.dynamicForm.setData(
            //   copyName, this.props.formData.bunkerdetails,
            //   //Object.assign(resp[copyName], copyData),
            //   groupData
            // );
            // }
          }
        }

        if (resp && resp[keyName] && copyName == "ciidynamics") {
          let copyData3 = [];
          resp[keyName].map((el, _index) => {
            let obj = {
              ...this.props?.formData?.["ciidynamics"]?.[_index],
              editable: true,
              index: _index,
            };
            let days = resp?.["portitinerary"]?.[_index]?.["days"] ?? 0;
            let xpd = resp?.["portitinerary"]?.[_index]?.["xpd"] ?? 0;

            let tsd = el["tsd"] ? parseFloat(el["tsd"]).toFixed(2) : 0;
            let opsport = parseFloat(Number(days) + Number(xpd)).toFixed(2);
            let total = tsd * 1 + opsport * 1;

            let date = new Date();
            obj = {
              ...obj,
              ifo: el["ifo"],
              lsmgo: el["lsmgo"],
              mgo: el["mgo"],
              vlsfo: el["vlsfo"],
              ulsfo: el["ulsfo"],
              pc_ifo: el["pc_ifo"],
              pc_lsmgo: el["pc_lsmgo"],
              pc_mgo: el["pc_mgo"],
              pc_vlsfo: el["pc_vlsfo"],
              pc_ulsfo: el["pc_ulsfo"],
              year: date.getFullYear(),
              dst_sailed: el["miles"] ? el["miles"] : "0.00",
              sea: tsd,
              ops_port: parseFloat(opsport).toFixed(2),
              port: el["port"],
              funct: el["funct"],
              passage: resp["portitinerary"][_index]["passage"],
              total: isNaN(total) ? 0 : parseFloat(total).toFixed(2),
            };

            copyData3.push(obj);
          });

          this.dynamicForm.setData(
            "ciidynamics",
            copyData3,
            undefined,
            undefined,
            "COPYST"
          );

          setTimeout(() => {
            //  const { groupName, updateFields, field_name } = f_event;
            let cii_calculation_fields = {
              cii_ref: "",
              cii_req: "",
              cii_att: "",
              cii_ret: "",
              cii_band: "",
              cii_pred: "",
              cii_pred_ret: "",
              year: "",
              port: "",
              function: "",
              passage: "",
              sea: "",
              total: "",
              dst_sailed: "",
              ifo: "",
              vlsfo: "",
              lsmgo: "",
              mgo: "",
              pc_ifo: "",
              pc_vlsfo: "",
              pc_lsmgo: "",
              pc_mgo: "",
              pc_ulsfo: "",
              co2_emission_vu: "",
              co2_emission_ml: "",
              co2_emission_ifo: "",
              co2_emission_total: "",
            };

            const ciidynamicsarr = copyData3;
            let groupName = "ciidynamics";
            ciidynamicsarr &&
              ciidynamicsarr.length > 0 &&
              ciidynamicsarr.map((el, _index) => {
                let action = {
                  type: "add",
                  formName: this.props.frmCode,
                  groupName: this.dynamicForm.getGroupKey(groupName),
                  index: _index,
                  value: {},
                };
                let cii_field_values = resp[groupName][_index];
                let dwt = resp["dwt"];
                let cii_values = {
                  ...cii_calculation_fields,
                  ...cii_field_values,
                };
                let vesselType = cookies.get("typecode");
                let values = VoyageCIIDynamicsCalculation(
                  vesselType,
                  dwt,
                  cii_values
                ); // for default value of shipt type='bulk carrier'

                let updateFields = [
                  "co2_emission_vu",
                  "co2_emission_ml",
                  "co2_emission_ifo",
                  "co2_emission_total",
                  "cii_ref",
                  "cii_req",
                  "cii_att",
                  "cii_ret",
                  "cii_band",
                  "cii_pred",
                  "cii_pred_ret",
                ];
                updateFields?.map((el) => {
                  let ciiAction = Object.assign({}, action);
                  ciiAction["value"][el] = values[el];
                  this.createStore.dispatch(ciiAction);
                });
              });
          }, 1000);
        }

        if (resp && resp[keyName] && copyName == "euets") {
          let copyData3 = [];
          resp[keyName].map((el, _index) => {
            let obj = {
              ...this.props?.formData?.["euets"]?.[_index],
              editable: true,
              index: _index,
            };
            let date = new Date();
            obj = {
              ...obj,
              ifo: el["ifo"],
              lsmgo: el["lsmgo"],
              mgo: el["mgo"],
              vlsfo: el["vlsfo"],
              ulsfo: el["ulsfo"],
              pc_ifo: el["pc_ifo"],
              pc_lsmgo: el["pc_lsmgo"],
              pc_mgo: el["pc_mgo"],
              pc_vlsfo: el["pc_vlsfo"],
              pc_ulsfo: el["pc_ulsfo"],
              year: date.getFullYear(),
              dst_sailed: el["miles"] ? el["miles"] : "0.00",
              sea: el["tsd"],
              port: el["port"],
              funct: el["funct"],
              is_eur:
                resp?.["portitinerary"]?.[_index]?.["is_eur"] === "False"
                  ? 0
                  : 1,
            };

            copyData3.push(obj);
          });

          this.dynamicForm.setData(
            "euets",
            copyData3,
            undefined,
            undefined,
            "COPYST"
          );

          setTimeout(() => {
            let groupName = "euets";

            let euets_calculation_fields = {
              year: "",
              port: "",
              function: "",
              ifo: "",
              vlsfo: "",
              lsmgo: "",
              mgo: "",
              ulsfo: "",
              pc_ifo: "",
              pc_vlsfo: "",
              pc_ulsfo: "",
              pc_lsmgo: "",
              pc_mgo: "",
              ttl_ems: "",
              phase_pre: "",
              sea_pre_ets: "",
              sea_ets_ems: "",
              port_pre_ems: "",
              port_ets_ems: "",
              ttl_eu_ets: "",
              ttl_eu_ets_exp: "",
            };
            const euetsarr = copyData3;

            euetsarr &&
              euetsarr.length > 0 &&
              euetsarr.map((el, _index) => {
                let action = {
                  type: "add",
                  formName: this.props.frmCode,
                  groupName: this.dynamicForm.getGroupKey(groupName),
                  index: _index,
                  value: {},
                };
                let euets_field_values = resp[groupName][_index];
                let euets_values = {
                  ...euets_calculation_fields,
                  ...euets_field_values,
                };

                let prevPort =
                  _index - 1 >= 0 ? euetsarr[_index - 1]?.is_eur : "";
                let currentPort = euetsarr[_index]?.is_eur ? 1 : 0;
                let values = VoyageEuetsCalculation(
                  prevPort,
                  currentPort,
                  euets_values
                ); // for default value of shipt type='bulk carrier';

                let updateFields = [
                  "sea_ems",
                  "port_ems",
                  "ttl_ems",
                  "phase_pre",
                  "sea_pre_ets",
                  "sea_ets_ems",
                  "port_pre_ems",
                  "port_ets_ems",
                  "ttl_eu_ets",
                  "ttl_eu_ets_exp",
                ];
                updateFields?.map((el) => {
                  let ciiAction = Object.assign({}, action);
                  ciiAction["value"][el] = values[el];
                  this.createStore.dispatch(ciiAction);
                });
              });
          }, 1000);
        }
      } else if (
        te.tabName === e &&
        te.event &&
        te.event.type === "copy" &&
        te.event.hasOwnProperty("condition") &&
        te.event.hasOwnProperty("showSingleIndex")
      ) {
        let fromGKey = this.dynamicForm.getGroupKey(te.event.group.from);
        let toGKey = this.dynamicForm.getGroupKey(te.event.group.to);
        let stObject = this.dynamicForm.getStateObject();
        if (stObject.hasOwnProperty(fromGKey)) {
          let row = stObject[fromGKey].filter(
            (frow) =>
              frow[te.event.condition.columnName] ===
              te.event.condition.columnValue
          );
          if (
            row &&
            typeof row === "object" &&
            row.hasOwnProperty("length") &&
            row.length > 0
          ) {
            let copyData = {};
            row = row[0];
            Object.keys(te.event.fields).map(
              (field) => (copyData[field] = row[field])
            );
            this.dynamicForm.setData(toGKey, copyData, te.event.group.to);
          }
        }
      } else if (
        te.tabName === e &&
        te.event &&
        te.event.type === "rename-button" &&
        showButtons.hasOwnProperty("length")
      ) {
        showButtons.map((be, bidx) => {
          if (be.id === te.event.key)
            showButtons[bidx]["title"] = te.event.title;
        });
        this.setState({ ...this.state, showButtons: showButtons });
      }
    });
  };


*/

  tabChangeEvent = (e) => {
    const { tabEvents, showButtons } = this.state;
    const { formData } = this.props;
    let { resp } = this.state;

    tabEvents.map((te) => {
      if (
        te.tabName === e &&
        te?.event?.type === "copy" &&
        !te.event.condition &&
        !te.event.showSingleIndex
      ) {
        let fromkeyName = this.dynamicForm.getGroupKey(te.event.from);
        let copyName = this.dynamicForm.getGroupKey(te.tabName);
        let stObject = this.dynamicForm.getStateObject();
        let fromfields = Object.keys(te.event.fields);
        let tofields = Object.values(te.event.fields);
        let copyData = [];
        let groupData = [];
        resp[fromkeyName]?.map((el, index) => {
          let copyobj = {};

          if (
            formData?.[copyName]?.[index] &&
            formData?.[copyName]?.[index]["id"]
          ) {
            copyobj = formData?.[copyName]?.[index];
          } else if (
            resp?.[copyName]?.[index] &&
            resp?.[copyName]?.[index]["id"] < 0
          ) {
            copyobj = resp?.[copyName]?.[index];
          } else {
            copyobj = {
              id: -9e6 + index,
              index: index,
            };
          }

          for (let i = 0; i < tofields.length; i++) {
            copyobj = {
              ...copyobj,
              [tofields[i]]: resp[fromkeyName][index][fromfields[i]],
            };
          }
          if (copyName == "bunkerdetails") {
            const values = BunkerDetailsCalculation(resp, index);
            copyobj = { ...copyobj, ...values };
            copyData.push(copyobj);
          }

          if (copyName === "portdatedetails") {
            copyData.push(copyobj);
          }
          if (copyName === "portcargo") {
            copyData.push(copyobj);
          }

          if (copyName == "ciidynamics") {
            // const values = BunkerDetailsCalculation(resp, index);
            // copyobj = { ...copyobj, ...values };
            let days = resp?.["portitinerary"]?.[index]?.["days"] ?? 0;
            let xpd = resp?.["portitinerary"]?.[index]?.["xpd"] ?? 0;

            let tsd = el["tsd"] ? parseFloat(el["tsd"]).toFixed(2) : 0;
            let opsport = parseFloat(Number(days) + Number(xpd)).toFixed(2);
            let total = tsd * 1 + opsport * 1;
            let date = new Date();
            copyobj = {
              ...copyobj,
              year: date.getFullYear(),
              dst_sailed: el["miles"] ? el["miles"] : "0.00",
              sea: tsd,
              ops_port: parseFloat(opsport).toFixed(2),
              port: el["port"],
              funct: el["funct"],
              passage: resp["portitinerary"][index]["passage"],
              total: isNaN(total) ? 0 : parseFloat(total).toFixed(2),
            };

            copyData.push(copyobj);
          }

          if (copyName == "euets") {
            let date = new Date();
            copyobj = {
              ...copyobj,

              year: date.getFullYear(),
              dst_sailed: el["miles"] ? el["miles"] : "0.00",
              sea: el["tsd"],
              port: el["port"],
              funct: el["funct"],
              is_eur:
                resp?.["portitinerary"]?.[index]?.["is_eur"] === "False"
                  ? 0
                  : 1,
            };

            copyData.push(copyobj);
          }
          this.dynamicForm.setData(
            copyName,
            copyData,
            groupData,
            undefined,
            "COPYST"
          );
        });

        if (copyName == "ciidynamics") {
          setTimeout(() => {
            //  const { groupName, updateFields, field_name } = f_event;
            let cii_calculation_fields = {
              cii_ref: "",
              cii_req: "",
              cii_att: "",
              cii_ret: "",
              cii_band: "",
              cii_pred: "",
              cii_pred_ret: "",
              year: "",
              port: "",
              function: "",
              passage: "",
              sea: "",
              total: "",
              dst_sailed: "",
              ifo: "",
              vlsfo: "",
              lsmgo: "",
              mgo: "",
              pc_ifo: "",
              pc_vlsfo: "",
              pc_lsmgo: "",
              pc_mgo: "",
              pc_ulsfo: "",
              co2_emission_vu: "",
              co2_emission_ml: "",
              co2_emission_ifo: "",
              co2_emission_total: "",
            };

            const ciidynamicsarr = resp["ciidynamics"];
            let groupName = "ciidynamics";
            ciidynamicsarr &&
              ciidynamicsarr.length > 0 &&
              ciidynamicsarr?.map((el, _index) => {
                let action = {
                  type: "add",
                  formName: this.props.frmCode,
                  groupName: this.dynamicForm.getGroupKey(groupName),
                  index: _index,
                  value: {},
                };
                let cii_field_values = resp[groupName][_index];
                let dwt = resp["dwt"];
                let cii_values = {
                  ...cii_calculation_fields,
                  ...cii_field_values,
                };
                let vesselType = cookies.get("typecode");
                let values = VoyageCIIDynamicsCalculation(
                  vesselType,
                  dwt,
                  cii_values
                ); // for default value of shipt type='bulk carrier'

                let updateFields = [
                  "co2_emission_vu",
                  "co2_emission_ml",
                  "co2_emission_ifo",
                  "co2_emission_total",
                  "cii_ref",
                  "cii_req",
                  "cii_att",
                  "cii_ret",
                  "cii_band",
                  "cii_pred",
                  "cii_pred_ret",
                ];
                updateFields?.map((el) => {
                  let ciiAction = Object.assign({}, action);
                  ciiAction["value"][el] = values[el];
                  this.createStore.dispatch(ciiAction);
                });
              });
          }, 1000);
        }

        if (copyName === "euets") {
          setTimeout(() => {
            let groupName = "euets";

            let euets_calculation_fields = {
              year: "",
              port: "",
              function: "",
              ifo: "",
              vlsfo: "",
              lsmgo: "",
              mgo: "",
              ulsfo: "",
              pc_ifo: "",
              pc_vlsfo: "",
              pc_ulsfo: "",
              pc_lsmgo: "",
              pc_mgo: "",
              ttl_ems: "",
              phase_pre: "",
              sea_pre_ets: "",
              sea_ets_ems: "",
              port_pre_ems: "",
              port_ets_ems: "",
              ttl_eu_ets: "",
              ttl_eu_ets_exp: "",
            };
            const euetsarr = resp["euets"];

            euetsarr &&
              euetsarr.length > 0 &&
              euetsarr?.map((el, _index) => {
                let action = {
                  type: "add",
                  formName: this.props.frmCode,
                  groupName: this.dynamicForm.getGroupKey(groupName),
                  index: _index,
                  value: {},
                };
                let euets_field_values = resp[groupName][_index];
                let euets_values = {
                  ...euets_calculation_fields,
                  ...euets_field_values,
                };

                let prevPort =
                  _index - 1 >= 0 ? euetsarr[_index - 1]?.is_eur : "";
                let currentPort = euetsarr[_index]?.is_eur ? 1 : 0;
                let values = VoyageEuetsCalculation(
                  prevPort,
                  currentPort,
                  euets_values
                ); // for default value of shipt type='bulk carrier';

                let updateFields = [
                  "sea_ems",
                  "port_ems",
                  "ttl_ems",
                  "phase_pre",
                  "sea_pre_ets",
                  "sea_ets_ems",
                  "port_pre_ems",
                  "port_ets_ems",
                  "ttl_eu_ets",
                  "ttl_eu_ets_exp",
                ];
                updateFields?.map((el) => {
                  let ciiAction = Object.assign({}, action);
                  ciiAction["value"][el] = values[el];
                  this.createStore.dispatch(ciiAction);
                });
              });
          }, 1000);
        }

        // if (copyName === "portdatedetails") {
        //   setTimeout(() => {
        //     let commence =
        //       resp?.["portdatedetails"]?.[0]?.["arrival_date_time"];
        //     let completed =
        //       resp?.["portdatedetails"]?.[
        //         resp["portdatedetails"]?.length - 1
        //       ]?.["departure"];
        //     this.dynamicForm.setData("commence_date", commence, undefined);
        //     this.dynamicForm.setData("completing_date", completed, undefined);
        //   }, 1000);
        // }
      } else if (
        te.tabName === e &&
        te.event &&
        te.event.type === "copy" &&
        te.event.hasOwnProperty("condition") &&
        te.event.hasOwnProperty("showSingleIndex")
      ) {
        let fromGKey = this.dynamicForm.getGroupKey(te.event.group.from);
        let toGKey = this.dynamicForm.getGroupKey(te.event.group.to);
        let stObject = this.dynamicForm.getStateObject();
        if (stObject.hasOwnProperty(fromGKey)) {
          let row = stObject[fromGKey].filter(
            (frow) =>
              frow[te.event.condition.columnName] ===
              te.event.condition.columnValue
          );
          if (
            row &&
            typeof row === "object" &&
            row.hasOwnProperty("length") &&
            row.length > 0
          ) {
            let copyData = {};
            row = row[0];
            Object.keys(te.event.fields).map(
              (field) => (copyData[field] = row[field])
            );
            this.dynamicForm.setData(toGKey, copyData, te.event.group.to);
          }
        }
      } else if (
        te.tabName === e &&
        te.event &&
        te.event.type === "rename-button" &&
        showButtons.hasOwnProperty("length")
      ) {
        showButtons.map((be, bidx) => {
          if (be.id === te.event.key)
            showButtons[bidx]["title"] = te.event.title;
        });
        this.setState({ ...this.state, showButtons: showButtons });
      }
    });
  };

  formInnerItems = (responseData, showButtons, showToolbar) => {
    const {
      extraFormFields,
      resp,
      groupAction,
      isShowFixedColumn,
      extraTableButton,
      sideList,
      showSideListBar,
      hideGroup,
      disableTabs,
      tableRowDeleteAction,
      editMode,
    } = this.state;
    let groupItems = [],
      showDiv = false,
      toolbarElements = "",
      formTagElements = "",
      skipIndex = -1,
      findTab = [];

    toolbarElements = (
      <>
        {showToolbar && showToolbar.length > 0
          ? showToolbar.map((t, i) => {
              return (
                <div key={"lr_" + i} className="full-width">
                  <Row>
                    <Col
                      xs={24}
                      sm={5}
                      md={t.leftWidth || 3}
                      lg={t.leftWidth || 3}
                      xl={t.leftWidth || 3}
                    >
                      {t.isLeftBtn.map((el, i) => {
                        return (
                          <span
                            key={el.key + "_" + (i + 1).toString()}
                            className="wrap-bar-menu" 
                          >
                            <ul className="wrap-bar-ul">
                              {el.isSets.map((set, j) => {
                                if (set) {
                                  return (
                                    <li
                                      key={"opt-" + j}
                                      onClick={() => {
                                        if (
                                          set.hasOwnProperty("event") &&
                                          typeof set.event === "function"
                                        ) {
                                          set.event(
                                            set.key,
                                            this.state.resp,
                                            this.resetForm
                                          );
                                        } else if (
                                          set.hasOwnProperty("event") &&
                                          typeof set.event !== "function" &&
                                          set.event === "reset"
                                        ) {
                                          this.resetForm();
                                        } else if (
                                          set.hasOwnProperty("event") &&
                                          typeof set.event !== "function" &&
                                          set.event === "menu-fold"
                                        ) {
                                          this.foldMenu();
                                        }
                                      }}
                                    >
                                      {set.showToolTip === true ? (
                                        <Tooltip title={set.withText}>
                                          {set.type}
                                        </Tooltip>
                                      ) : (
                                        <>
                                          {set.type}
                                          <span className="text-bt" >
                                            {set.withText}
                                          </span>
                                        </>
                                      )}
                                    </li>
                                  );
                                }
                              })}
                            </ul>
                          </span>
                        );
                      })}
                    </Col>

                    <Col
                      xs={24}
                      sm={19}
                      md={t.rightWidth || 21}
                      lg={t.rightWidth || 21}
                      xl={t.rightWidth || 21}
                    >
                      <Row type="flex" justify="end">
                        {t.isRightBtn.map((el, i) => {
                          return (
                            <span
                              key={(i + 1).toString()}
                              className="wrap-bar-menu" 
                            >
                              <ul className="wrap-bar-ul">
                                {el.isSets.map((set, j) => {
                                  if (typeof set === "object" && set) {
                                    var myvar = (
                                      <li key={j}>
                                        {
                                          set.isDropdown === 0 ? (
                                            <span
                                              onClick={() => {
                                                if (
                                                  set.hasOwnProperty("event") &&
                                                  typeof set.event ===
                                                    "function"
                                                ) {
                                                  set.event(
                                                    set.key,
                                                    this.state.resp
                                                  );
                                                }
                                              }}
                                            >
                                              {set.key ==
                                              "port_route_details" ? (
                                                <Tooltip title={set.withText}>
                                                  <img
                                                    src={
                                                      IMAGE_PATH +
                                                      "icons/marker.png"
                                                    }
                                                    height="25"
                                                  />
                                                </Tooltip>
                                              ) : (
                                                set.withText
                                              )}
                                            </span>
                                          ) : (
                                            <Dropdown
                                              key={j}
                                              overlay={() =>
                                                this.generateMenus(set.menus)
                                              }
                                              trigger={["click"]}
                                            >
                                              <span className="text-bt" >
                                                {set.withText}
                                              </span>
                                            </Dropdown>
                                          )
                                          //TODO TECH LOCAL
                                          // <span className="text-bt">{set.withText}</span>
                                        }
                                      </li>
                                    );

                                    return myvar;
                                  }
                                })}
                              </ul>
                            </span>
                          );
                        })}
                        <div>
                          {t.isResetOption ? (
                            <span className="wrap-bar-menu more-icon" >
                              <Button
                                type="danger"
                                onClick={() => this.resetForm()}
                              >
                                Reset
                              </Button>
                            </span>
                          ) : null}
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </div>
              );
            })
          : undefined}
      </>
    );

    formTagElements = (
      <>
        {responseData && responseData.frm && responseData.frm.length > 0 ? (
          
          responseData.frm.map((e, i) => {
            if (e.group_name && i > skipIndex) {
              let it = responseData.groups.filter(
                (ge) => ge.group_name === e.group_name
              );
              if (it && it.length > 0 && groupItems.indexOf(e.group_name) < 0) {
                groupItems.push(e.group_name);
                let groupKey = this.dynamicForm.getGroupKey(e.group_name);
                if (it[0].g_width * 1 < 100 && showDiv === false) {
                  showDiv = true;
                } else {
                  showDiv = false;
                }
                if (it[0].group_type.toLowerCase() === "normal") {
                  divshow = "";
                  if (
                    groupKey == "totalitinerarysummary" ||
                    groupKey == "totalbunkersummary" ||
                    groupKey == "companytype"
                  ) {
                    divshow = <div className="normal_cls">{e.group_name}</div>;
                  }

                  if (
                    hideGroup.length == 0 ||
                    (hideGroup.length > 0 &&
                      hideGroup.indexOf(it[0]["group_name"]) === -1)
                  ) {
                    return (
                      <div
                        key={groupKey + "_div_" + i}
                        className={"nmp normal-" + groupKey}
                        style={{
                          marginTop: "10px",
                          paddingLeft: "10px",
                          paddingRight: "10px",
                          width:
                            it[0].g_width && it[0].g_width === "100"
                              ? "100%"
                              : it[0].g_width + "%",
                        }}
                      >
                        {divshow}
                        <div key={groupKey + "_div__" + i} className="row">
                          <NormalFormIndex
                            key={groupKey}
                            frm={{ frm: it[0].columns, groups: [] }}
                            frmProp={this.props}
                            parentformName={this.props.frmCode}
                            formData={resp}
                            inlineLayout={true}
                            editMode={editMode}
                            dynamicForm={this.dynamicForm}
                            createStore={this.createStore}
                            isShowFixedColumn={isShowFixedColumn}
                            formDevideInCols={
                              this.state.formGroupDevideInCols.hasOwnProperty(
                                groupKey
                              )
                                ? this.state.formGroupDevideInCols[groupKey]
                                : it[0].view_cols > 0
                                ? it[0].view_cols
                                : it[0].g_width && it[0].g_width !== "100"
                                ? "1"
                                : undefined
                            }
                            summary={this.props.summary || []}
                            disableTabs={disableTabs}
                            tableRowDeleteAction={tableRowDeleteAction}
                          />
                        </div>
                      </div>
                    );
                  }
                } else if (it[0].group_type.toLowerCase() === "table") {
                  findTab =
                    disableTabs && disableTabs.length > 0
                      ? disableTabs.find(
                          (de) => de["groupKey"] === it[0]["group_key"]
                        )
                      : undefined;
                  if (
                    hideGroup.length == 0 ||
                    (hideGroup.length > 0 &&
                      hideGroup.indexOf(it[0]["group_name"]) === -1)
                  ) {
                    return (
                      <IndexTableForm
                        key={groupKey}
                        className={"table-" + groupKey}
                        mainFormName={this.props.frmCode}
                        parentformName={this.state.parentformName}
                        frmConfig={{
                          frm: it[0].columns,
                          groups: [
                            Object.assign({}, it[0], {
                              g_width: "100",
                              actual_width: it[0]["g_width"],
                            }),
                          ],
                        }}
                        dynamicForm={this.dynamicForm}
                        createStore={this.createStore}
                        formData={resp}
                        displayOnly={groupAction}
                        editMode={editMode}
                        groupKey={groupKey}
                        fullWidth={it[0].g_width * 1 < 100 ? false : true}
                        showDiv={showDiv}
                        isShowFixedColumn={isShowFixedColumn}
                        extraTableButton={extraTableButton}
                        summary={this.props.summary || []}
                        modalEvent={this.loadPopup}
                        disableTab={
                          findTab && findTab.hasOwnProperty("tabName")
                            ? findTab
                            : undefined
                        }
                        tableRowDeleteAction={tableRowDeleteAction}
                      />
                    );
                  }
                }
              } else if (
                it &&
                it.length === 0 &&
                groupItems.indexOf(e.group_name) < 0
              ) {
                let _return = this.renderItems(e, i, responseData.frm);
                if (i === _return[1]) {
                  return _return[0];
                } else {
                  skipIndex = _return[1];
                  return _return[0];
                }
              }
            } else if (!e.group_name) {
              return this.renderItems(e, i, responseData.frm)[0];
            }
          })
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
        {responseData && responseData.tabs && responseData.tabs.length > 0 ? (
          <div
            className="tabbed-wrapper-cust"
            style={{ width: responseData.tabs[0]["tab_width"] + "%" }}
          >
            <Tabs
              size="small"
              style={{ width: "100%" }}
              onChange={this.tabChangeEvent}
            >
              {responseData.tabs.map((e, i) => {
                {
                  findTab =
                    disableTabs && disableTabs.length > 0
                      ? disableTabs.find(
                          (de) =>
                            de.tabName.toLowerCase() ===
                            e.tab_name.toLowerCase()
                        )
                      : undefined;
                }
                return (
                  <TabPane tab={e.tab_name} key={e.tab_name}>
                    {e.frm_type === "Table" ? (
                      this.props.staticTabs &&
                      this.props.staticTabs[e.tab_name] ? (
                        this.props.staticTabs[e.tab_name]()
                      ) : (
                        <IndexTableForm
                          key={e.frm_code}
                          frmCode={e.frm_code}
                          dynamicForm={this.dynamicForm}
                          createStore={this.createStore}
                          mainFormName={this.props.frmCode}
                          parentformName={this.state.parentformName}
                          formData={resp}
                          editMode={editMode}
                          originalForm={responseData.tabs}
                          isShowFixedColumn={isShowFixedColumn}
                          extraTableButton={extraTableButton}
                          fullWidth={true}
                          summary={this.props.summary || []}
                          dyncGroupName={e.groupName || undefined}
                          modalEvent={this.loadPopup}
                          disableTab={
                            findTab && findTab.hasOwnProperty("tabName")
                              ? findTab
                              : undefined
                          }
                          tableRowDeleteAction={tableRowDeleteAction}
                        />
                      )
                    ) : (
                      <div className="row">
                        {this.props.staticTabs &&
                        this.props.staticTabs[e.tab_name] ? (
                          this.props.staticTabs[e.tab_name]()
                        ) : (
                          <NormalFormIndex
                            key={e.frm_code}
                            frmCode={e.frm_code}
                            frmProp={this.props}
                            parentformName={this.props.frmCode}
                            formData={resp}
                            dynamicForm={this.dynamicForm}
                            createStore={this.createStore}
                            editMode={editMode}
                            isShowFixedColumn={isShowFixedColumn}
                            inlineLayout={true}
                            summary={this.props.summary || []}
                            extraTableButton={
                              this.props.extraTableButton || undefined
                            }
                            disableTabs={disableTabs}
                            tableRowDeleteAction={tableRowDeleteAction}
                          />
                        )}
                      </div>
                    )}
                  </TabPane>
                );
              })}
            </Tabs>
          </div>
        ) : undefined}
        {extraFormFields &&
        extraFormFields.hasOwnProperty("isShowInMainForm") &&
        extraFormFields.isShowInMainForm === true
          ? extraFormFields.content
          : undefined}
        <div
          style={{ width: "100%", paddingTop: "10px", paddingRight: "10px" }}
        >
          <Row align="middle" justify="end" type="flex">
            {showButtons && showButtons.length > 0
              ? showButtons.map((b, i) => {
                  if (b.id === "cancel") {
                    return (
                      <Button
                        key={b.id}
                        type={b.type}
                        style={{ marginRight: "10px" }}
                        onClick={() => {
                          this.resetForm();
                          if (b.event && typeof b.event === "function") {
                            b.event();
                          }
                        }}
                      >
                        {b.title}
                      </Button>
                    );
                  } else {
                    return (
                      <Button
                        key={b.id}
                        type={b.type}
                        onClick={() => b.event(this.state.resp, this.resetForm)}
                      >
                        {b.title}
                      </Button>
                    );
                  }
                })
              : undefined}
          </Row>
        </div>
        {/* {this.dynamicForm.resetGroupList()} */}
      </>
    );

    if (
      sideList &&
      sideList.hasOwnProperty("showList") &&
      sideList["showList"] === true
    ) {
      return (
        <>
          {toolbarElements}
          {showSideListBar === true ? (
            <div className="col-md-2 pr-0">
              <SideList
                selectedID={sideList.selectedID}
                statusList={sideList.statusList}
                toolbarKey={sideList.toolbarKey}
                title={sideList.title}
                columns={sideList.columns}
                uri={sideList.uri}
                searchString={sideList.searchString}
                rowClick={(event) => sideList.rowClickEvent(event)}
                icon={sideList.icon || false}
              />
            </div>
          ) : (
            ""
          )}
          <div className={showSideListBar === true ? "col-md-10" : "col-md-12"}>
            <div className="row">{formTagElements}</div>
          </div>
        </>
      );
    } else {
      return (
        <>
          {toolbarElements}
          {formTagElements}
        </>
      );
    }
  };

  resetForm = () => {
    this.setState({ ...this.state, resetFormFlag: true });
    setTimeout(() => {
      this.createStore.dispatch({ type: "reset" });
      this.setState({ ...this.state, resp: {}, resetFormFlag: false });
    }, 50);
  };

  renderFormTag = (showForm, showButtons, showToolbar, responseData) => {
    let formTag = this.formInnerItems(responseData, showButtons, showToolbar);
    if (showForm === true) {
      let formTagEle = (
        <Form>
          <div className="row">{formTag}</div>
        </Form>
      );
      if (this.props.formClass) {
        formTagEle = (
          <Form className={this.props.formClass}>
            <div className="row">{formTag}</div>
          </Form>
        );
      }
      formTag = formTagEle;
    }

    return formTag;
  };

  render() {
    const { responseData, resetFormFlag, showButtons, labelModal } = this.state;
    const { showForm, showToolbar, sendBackData, triggerEvent } = this.props;

    if (
      sendBackData &&
      sendBackData["show"] === true &&
      typeof triggerEvent === "function"
    ) {
      this.props.triggerEvent(this.state.resp);
    }

    return (
      <>
        {!resetFormFlag ? (
          this.renderFormTag(showForm, showButtons, showToolbar, responseData)
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
        {labelModal.modalStatus ? (
          <ModalAlertBox
            modalStatus={labelModal.modalStatus}
            modalHeader={labelModal.modalHeader}
            modalBody={labelModal.modalBody}
            modalFooter={labelModal.modalFooter}
            onCancelFunc={(data) => this.onCloseModal(data)}
            modalWidth={labelModal.modalWidth}
          />
        ) : undefined}
      </>
    );
  }
}

export default NormalFormIndex;
