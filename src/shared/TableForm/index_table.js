import React, { Component } from "react";
import {
  Table,
  Card,
  Popconfirm,
  Button,
  Row,
  Col,
  Menu,
  Dropdown,
} from "antd";
import URL_WITH_VERSION, {
  getAPICall,
  URL_WITHOUT_VERSION,
  openNotificationWithIcon,
} from "../../shared";
import { v4 as uuidv4 } from "uuid";
import {
  EditOutlined,
  DeleteOutlined,
  SettingOutlined,
} from "@ant-design/icons";

class IndexTableForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainFormName: this.props.mainFormName || undefined,
      responseData: { columns: [], objectKeys: [], dataArray: {}, group: {} },
      ids: -9e6,
      index: 0,
      isDisplayOnly: this.props.displayOnly === true,
      editMode: this.props.editMode,
      isShowFixedColumn: this.props.isShowFixedColumn,
      extraTableButton: this.props.extraTableButton || undefined,
      dataArray: this.props.formData || {},
      groupKey: this.props.groupKey || undefined,
      fullWidth: true,
      className: this.props.className || "",
      summary: this.props.summary || [],
      dyncGroupName: this.props.dyncGroupName || undefined,
      fieldModalEvent: this.props.modalEvent || undefined,
      disableTab: this.props.disableTab || undefined,
      tableRowDeleteAction: this.props.tableRowDeleteAction || (() => {}),
    };
    this.dynamicForm = this.props.dynamicForm;
    this.createStore = this.props.createStore;
  }

  

  componentDidMount = async () => {
    let data = {},
      headers = { extraCols: [] };
    if (this.props.frmCode) {
      this.props.originalForm.map((e) => {
        if (e.f_dyn_col && e.f_dyn_col !== "") {
          let item = JSON.parse(e.f_dyn_col);
          item["tableColumnValue"] = this.props.formData[item["keyColumn"]];
          headers["extraCols"].push(item);
          headers["formCode"] = e.frm_code;
        }
      });
      const response = await getAPICall(
        `${URL_WITHOUT_VERSION}get/${this.props.frmCode}`,
        headers
      );
      data = await response["data"];
    } else if (this.props.frmConfig) {
      data = this.props.frmConfig;
    }
    if (this.props.dyncGroupName && this.props.dyncGroupName !== "") {
      data["groups"].map((e, i) => {
        data["groups"][i] = Object.assign({}, e, {
          group_name: this.props.dyncGroupName,
          group_key: this.dynamicForm.getGroupKey(this.props.dyncGroupName),
        });
      });

      data["frm"].map((e, i) => {
        data["frm"][i] = Object.assign({}, e, {
          group_name: this.props.dyncGroupName,
        });
      });
    }

    this.dynamicForm.originalState.setFormFields(data["frm"]);
    this.setState({
      ...this.state,
      responseData: this.createColumns(data),
      dataArray: await this.getDataArray(this.props.dataURL, data),
      fullWidth: this.props.fullWidth ? true : false,
    });
  };

  getDataArray = async (codeURL, dataObject) => {
    // alert('fetcingdata')
    let dataPart = {};
    let objectKeys = [];
    dataObject.frm.map((e) => {
      if (e.group_name && objectKeys.indexOf(e.group_name) === -1) {
        objectKeys.push(e.group_name);
      } else if (!e.group_name && objectKeys.indexOf() === -1) {
        objectKeys.push("g");
      }
    });

    if (codeURL) {
      const response = await getAPICall(`${URL_WITH_VERSION}/${codeURL}`);
      const dataResp = await response["data"];

      Object.keys(dataResp).forEach((e) => {
        objectKeys.forEach((f) => {
          if (dataResp[e] && dataResp[e].hasOwnProperty(f)) {
            dataPart[f] = dataResp[e][f];
          }
        });
      });
    } else if (this.props.formData) {
      objectKeys.forEach((fo) => {
        let groupKey = this.dynamicForm.getGroupKey(fo);

        if (
          this.props.formData.hasOwnProperty(groupKey) &&
          this.props.formData[groupKey]
        ) {
          let data = this.props.formData[groupKey];
          data &&
            data.length > 0 &&
            data.forEach((d, i) => {
              data[i]["index"] = i;
              data[i]["key"] = groupKey + "_table_row_" + i;
            });
          dataPart[fo] = data;
        }
      });
    }

    objectKeys.map((e) => {
      if (dataPart && dataPart[e])
        dataPart[e].map((e) => (e["editable"] = true));
    });
    return dataPart;
  };

  extartButtons = (data, record, e) => {
    let rows = [];
    data.map((b) => {
      if (!b.hasOwnProperty("isMenu") && !b.hasOwnProperty("onClickAction")) {
        rows.push(
          <span
            key={uuidv4()}
            className={
              "iconWrapper " +
              b.icon +
              " " +
              (b.class && b.class !== "" ? b.class : undefined)
            }
            onClick={() =>
              this.onAddNewBelow({
                gKey: e,
                index: record.index,
                callType: data[0]["callType"],
              })
            }
          >
            {b.icon}
          </span>
        );
      } else if (
        !b.hasOwnProperty("isMenu") &&
        b.hasOwnProperty("onClickAction") &&
        typeof b.onClickAction === "function"
      ) {
        rows.push(
          <span
            key={uuidv4()}
            className={
              "iconWrapper " +
              b.icon +
              " " +
              (b.class && b.class !== "" ? b.class : undefined)
            }
            onClick={() =>
              b.onClickAction(
                { ID: record.id, gKey: e, index: record.index },
                this.dynamicForm.stateObject
              )
            }
          >
            {b.icon}
          </span>
        );
      } else if (b.hasOwnProperty("isMenu") && b["isMenu"] === true) {
        const menu = (
          <Menu>
            {b["options"].map((bo) => (
              <Menu.Item
                onClick={() =>
                  bo.onClickAction({
                    ID: record.id,
                    gKey: e,
                    index: record.index,
                    eventType: bo.key,
                    eventData: record,
                  })
                }
                key={bo.key}
              >
                {bo.text}
              </Menu.Item>
            ))}
          </Menu>
        );
        rows.push(
          <Dropdown overlay={menu}>
            <span key={uuidv4()} className={"iconWrapper " + b.icon}>
              <SettingOutlined />
            </span>
          </Dropdown>
        );
      }
    });

    return rows;
  };

  createColumns = (data) => {
    const {
      isDisplayOnly,
      isShowFixedColumn,
      extraTableButton,
      fieldModalEvent,
      editMode,
    } = this.state;
    let groups = {};
    let objectKeys = [];
    let groupWidth = {};
    let itemData = [];
    let subItems = [];
    let skipCols = [];
    let objWidth = {};
    let addItems = {};
    if (data && data.groups) {
      data.groups.map((e) => (groupWidth[e.group_name] = e.g_width * 1));
    }

    data.frm.map((e, i) => {
      let colSettings = "";
      if (
        objectKeys.indexOf(e.group_name) === -1 &&
        e.group_name &&
        !groups.hasOwnProperty(e.group_name)
      ) {
        groups[e.group_name] = [];
        objectKeys.push(e.group_name);
      } else if (
        objectKeys.indexOf("g") === -1 &&
        !e.group_name &&
        !groups.hasOwnProperty(e.group_name)
      ) {
        groups["g"] = [];
        objectKeys.push("g");
      }

      if (e.f_default && e.f_default !== "") {
        addItems[e.f_name] = e.f_default;
      }

      if (!objWidth[e.group_name || "g"]) {
        objWidth[e.group_name || "g"] = 0;
      }

      if (
        e.f_t_col_span &&
        e.f_t_col_span !== "" &&
        e.f_t_col_span !== JSON.stringify({})
      ) {
        colSettings = JSON.parse(e.f_t_col_span);

        if (
          typeof colSettings.skipCol === "object" &&
          colSettings.skipCol.hasOwnProperty("length")
        ) {
          colSettings.skipCol.map((e) => skipCols.push(e));
        } else {
          skipCols.push(colSettings.skipCol);
        }

        colSettings.colDetails.children.map((ec, ic) => {
          let ecID = ec.f_id * 1;
          let colValue = Object.assign({}, e);

          if (
            ic >= 1 &&
            (colSettings.skipCol === ecID ||
              colSettings.skipCol.indexOf(ecID) >= 0)
          ) {
            colValue = data.frm.find((e) => e.f_id * 1 === ecID);
          }
          colSettings.colDetails.children[ic] = {
            title: ec.title,
            dataIndex: ec.dataIndex,
            key: ec.key,
            width: ec.width,
            actualField: colValue,
            render: (text, record) =>
              this.dynamicForm.emptyCell(
                colValue,
                text,
                record,
                false,
                false,
                fieldModalEvent
              ),
          };
          objWidth[e.group_name || "g"] =
            objWidth[e.group_name || "g"] + ec.width * 1;
        });
        groups[e.group_name || "g"].push(colSettings.colDetails);
      } else if (skipCols.indexOf(e.f_id) < 0) {
        groups[e.group_name || "g"].push({
          title: e.name,
          dataIndex: e.f_name,
          key: e.f_name,
          actualField: e,
          width:
            e.f_width && e.f_width !== ""
              ? isNaN(e.f_width * 1)
                ? e.f_width
                : e.f_width * 1
              : undefined,
          render: (text, record) =>
            this.dynamicForm.emptyCell(
              e,
              text,
              record,
              false,
              false,
              fieldModalEvent
            ), //this.emptyCell()
        });
        objWidth[e.group_name || "g"] =
          objWidth[e.group_name || "g"] +
          (e.f_width && e.f_width !== "" ? e.f_width * 1 : 0);
      }
    });

    if (isDisplayOnly === false) {
      objectKeys.map((e) => {
        if (
          (data["groups"] &&
            data["groups"].length > 0 &&
            data["groups"][0].hasOwnProperty("isShowActionButtons") &&
            data["groups"][0]["isShowActionButtons"] === 1) ||
          (data["groups"] && data["groups"].length == 0)
        ) {
          objWidth[e] += 100;
          groups[e].push({
            title: "Actions",
            dataIndex: "actions",
            key: "actions",
            fixed:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? "right"
                : undefined,
            width:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? 100
                : 120,
            render: (text, record) => {
              
              const { editable } = record;
              let gpkey = this.dynamicForm.getGroupKey(e);
              return (
                <div className="editable-row-operations">
                  {editable ? (
                    <div>
                      {editMode ? (
                        <span className="iconWrapper cancel">
                          <Popconfirm
                            placement={
                              gpkey == "portitinerary" ? "left" : "top"
                            }
                            title={
                              gpkey == "portitinerary" ? (
                                <p>
                                  Deleting this port will require reselecting{" "}
                                  <br /> the below port again to match the
                                  distance.
                                </p>
                              ) : (
                                "Sure to delete?"
                              )
                            }
                            onConfirm={() =>
                              this.editTableData({
                                editable: false,
                                ID: record.id,
                                gKey: e,
                                ocd: record.ocd,
                                index: record.index,
                                action: "delete",
                              })
                            }
                          >
                            <DeleteOutlined />
                          </Popconfirm>
                        </span>
                      ) : (
                        ""
                      )}

                      {extraTableButton &&
                      extraTableButton.hasOwnProperty(e) &&
                      extraTableButton[e].length > 0
                        ? this.extartButtons(extraTableButton[e], record, e)
                        : undefined}
                    </div>
                  ) : (
                    <>
                      <span
                        className="iconWrapper edit"
                        onClick={(ev) =>
                          this.editTableData({
                            editable: true,
                            ID: record.id,
                            gKey: e,
                            index: record.index,
                          })
                        }
                      >
                        <EditOutlined />
                      </span>
                      {extraTableButton &&
                      extraTableButton.hasOwnProperty(e) &&
                      extraTableButton[e].length > 0
                        ? this.extartButtons(extraTableButton[e], record, e)
                        : undefined}
                    </>
                  )}
                </div>
              );
            },
          });
        } else if (
          (data["groups"] &&
            data["groups"].length > 0 &&
            data["groups"][0].hasOwnProperty("isShowActionButtons") &&
            data["groups"][0]["isShowActionButtons"] === 0 &&
            extraTableButton &&
            extraTableButton.hasOwnProperty(e) &&
            extraTableButton[e].length > 0) ||
          (data["groups"] && data["groups"].length == 0)
        ) {
          objWidth[e] += 100;
          groups[e].push({
            title: "Actions",
            dataIndex: "actions",
            key: "actions",
            fixed:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? "right"
                : undefined,
            width:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? 100
                : 120,
            render: (text, record) => {
              const { editable } = record;
              return (
                <div className="editable-row-operations">
                  {editable
                    ? this.extartButtons(extraTableButton[e], record, e)
                    : undefined}
                </div>
              );
            },
          });
        }
      });
    }

    objectKeys.map((e) => {
      if (groupWidth[e] < 100) {
        subItems.push(e);
      } else if (groupWidth[e] === 100 || !groupWidth[e]) {
        if (subItems.length > 0) {
          itemData.push(subItems);
          subItems = [];
        }
        itemData.push(e);
      }
    });

    if (subItems.length > 0) {
      itemData.push(subItems);
      subItems = [];
    }

    return {
      columns: groups,
      objectKeys: itemData,
      group: data["groups"],
      groupWidth: groupWidth,
      objectLength: objWidth,
      addItemValue: addItems,
    };
  };
  
  editTableData = (editInfo) => {
    const { summary, mainFormName } = this.state;
    let summaryTotal = {};
    let sKeys = [];
    let dArray = this.state.dataArray;
    let eArray = [];
    let _id = -9e6;

    let upObj = {};
    let gpKey = this.dynamicForm.getGroupKey(editInfo.gKey);
    dArray[editInfo.gKey].map((dag, index) => {
      if (dag.id === editInfo.ID) {
        dArray[editInfo.gKey][index] = {
          ...dag,
          editable: editInfo.editable === false ? undefined : true,
        };
      }
    });

    if (
      editInfo.ocd ||
      (editInfo.hasOwnProperty("action") && editInfo["action"] === "delete")
    ) {
      dArray[editInfo.gKey].map((dag, index) => {
        if (
          dag.id === editInfo.ID &&
          dag.ocd === editInfo.ocd &&
          dag.editable == undefined
        ) {




          const completeState=this.dynamicForm.getState();
          this.state.tableRowDeleteAction(
            editInfo,
            dArray[editInfo.gKey][index],
            completeState
          );
          //  delete dArray[editInfo.gKey][index];
          if (eArray.length === 0) {
            dArray[editInfo.gKey].map((p, pi) => {
              if (pi !== index) {
                upObj = {
                  id: _id + eArray.length,
                  index: eArray.length,
                  key: gpKey + "_table_row_" + eArray.length,
                };
                if (p["id"] > 0)
                  upObj = {
                    index: eArray.length,
                    key: gpKey + "_table_row_" + eArray.length,
                  };
                eArray.push(Object.assign(p, upObj));
              }
            });
            dArray[editInfo.gKey] = eArray;

            this.dynamicForm.deleteRow(editInfo);
          }
        }
      });

      if (
        this.props.mainFormName == "tc_make_payment" ||
        this.props.mainFormName == "tco_make_payment" ||
        this.props.mainFormName == "tc_commission_entry" ||
        this.props.mainFormName == "tco_commission_entry"
      ) {
        summary.map((s) => {
          let fObj = this.dynamicForm.getStateObject(
            this.dynamicForm.getGroupKey(s.gKey)
          );
          if (
            this.dynamicForm.getGroupKey(s.gKey) === gpKey &&
            fObj &&
            fObj.length > 0
          ) {
            s.showTotalFor.map((st) => {
              summaryTotal[st] = 0;
            });
            fObj.map((e) => {
              s.showTotalFor.map((st) => {
                if (e.hasOwnProperty(st) && s.dhm === true) {
                  summaryTotal[st] =
                    (summaryTotal[st] ? summaryTotal[st] : 0) +
                    this.dynamicForm.stringToDaysHoursMinutsObject(e[st], true);
                } else if (e.hasOwnProperty(st) && !s.dhm) {
                  summaryTotal[st] =
                    (summaryTotal[st] ? summaryTotal[st] : 0) + e[st] * 1;
                }
              });
              if (
                this.props.mainFormName == "tc_commission_entry" ||
                this.props.mainFormName == "tco_commission_entry"
              ) {
                // parameter for this.dynamicForm.setData  function =>(fieldName, fieldData, groupData, index = undefined, calledFrom = undefined)
                this.dynamicForm.setData(
                  "total",
                  summaryTotal["commission"],
                  ".",
                  undefined,
                  "COPYST"
                );
              } else {
                this.dynamicForm.setData(
                  "amount",
                  summaryTotal["amount_usd"],
                  undefined,
                  undefined,
                  "COPYST"
                );
              }
            });
          } else {
            // if there is no any row in makepayment table, then total amount willbe taken from daily rates as like previous.
            if (
              this.props.mainFormName == "tc_commission_entry" ||
              this.props.mainFormName == "tco_commission_entry"
            ) {
              // parameter for this.dynamicForm.setData  function =>(fieldName, fieldData, groupData, index = undefined, calledFrom = undefined)
              this.dynamicForm.setData("total", 0, ".", undefined, "COPYST");
            } else {
              let el = document.getElementById("daily_rates").value;
              this.dynamicForm.setData(
                "amount",
                el,
                undefined,
                undefined,
                "COPYST"
              );
            }
          }
        });
      }
      if (this.props.mainFormName == "freight-commission-invoice") {
        summary.map((s) => {
          let fObj = this.dynamicForm.getStateObject(
            this.dynamicForm.getGroupKey(s.gKey)
          );
          if (
            this.dynamicForm.getGroupKey(s.gKey) === gpKey &&
            fObj &&
            fObj.length > 0
          ) {
            s.showTotalFor.map((st) => {
              summaryTotal[st] = 0;
            });
            fObj.map((e) => {
              s.showTotalFor.map((st) => {
                if (e.is_select && e.hasOwnProperty(st)) {
                  summaryTotal[st] =
                    (summaryTotal[st] ? summaryTotal[st] : 0) + e[st] * 1;
                }
              });
              if (this.props.mainFormName == "freight-commission-invoice") {
                // parameter for this.dynamicForm.setData  function =>(fieldName, fieldData, groupData, index = undefined, calledFrom = undefined)
                this.dynamicForm.setData(
                  "total_amount",
                  summaryTotal["commission_amount"],
                  undefined,
                  undefined,
                  "COPYST"
                );
              }
            });
          } else {
            // if there is no any row in  table, then total amount willbe taken from daily rates as like previous.

            // parameter for this.dynamicForm.setData  function =>(fieldName, fieldData, groupData, index = undefined, calledFrom = undefined)
            this.dynamicForm.setData(
              "total_amount",
              0,
              undefined,
              undefined,
              "COPYST"
            );
          }
        });
      }
    }
    _id = _id + eArray.length;
    //  this.setState({ ...this.state, "dataArray": { ...dArray }, "ids": (_id), "index": eArray.length });
    // console.log(dArray, "data delete array");
    this.setState(
      {
        ...this.state,
        dataArray: { ...dArray },
        ids: _id,
        index: eArray.length - 1,
      },
      () => {
        let gpKey = this.dynamicForm.getGroupKey(editInfo.gKey);
        let value = {};
        value[gpKey] = dArray[gpKey] ? dArray[gpKey] : dArray[editInfo.gKey];

        if (
          mainFormName == "tcov_port_itinerary" ||
          mainFormName == "tab_tcto_port_itinerary" ||
          mainFormName == "voyage_manager_port_itinerary" ||
          this.props.mainFormName == "port_expense_pda_port_expense_adv" ||
          this.props.mainFormName == "port_expense_fda"
        ) {
         
          // this.createStore.dispatch({
          //   type: "delete",
          //   groupName: gpKey,
          //   formName: mainFormName,
          //   index: undefined,
          //   value: value,
          // });

          //  this is creating issue. data is coming nested. in port iteniary if we delete port from middle.
          //  this.createStore.dispatch({ "type": 'add', "groupName": gpKey, "formName": mainFormName, "index": undefined, "value": value })
        } else {
          this.createStore.dispatch({
            type: "add",
            groupName: gpKey,
            formName: mainFormName,
            index: undefined,
            value: value,
          });
        }
      }
    );
  };


  onAddNewBelow = (event) => {
    
    if (event.hasOwnProperty("callType") && event.callType == "ADD_BETWEEN") {
      openNotificationWithIcon(
        "error",
        "First to fill all info in current row, than add more row",
        10
      );
    }
    const { isDisplayOnly, responseData, mainFormName } = this.state;
    let rows = [],
      dAIndex = 0,
      eventGroup = this.dynamicForm.getGroupKey(event.gKey);
    let newRow = this.dynamicForm.getStateObject(eventGroup);
    let newRow1 = [],
      idx = -9e6,
      _index = 0;
    newRow.map((e) => newRow1.push(e));
    newRow1.map((e, i) => {
      let p = Object.assign({}, e);
      p["id"] = idx;
      p["index"] = _index;
      p["key"] = eventGroup + "_table_row_" + _index;
      p["editable"] = true;
      rows.push(p);
      if (i === event.index) {
        let todate = newRow[i]["to_date"];
        let totime = newRow[i]["to_time"];
        _index = _index + 1;
        idx = idx + 1;

        let agp = Object.assign(
          {
            editable: true,
            ocd: true,
            id: idx,
            index: _index,
            key: eventGroup + "_table_row_" + _index,
            from_date: todate,
            from_time: totime,
          },
          responseData.addItemValue
        );

        rows.push(agp);
        idx = idx + 1;
        _index = _index + 1;
      } else {
        idx = idx + 1;
        _index = _index + 1;
      }
    });
    let obj = {};
    obj[event.gKey] = rows;
    this.setState({ ...this.state, dataArray: obj }, () => {
      let value = {};
      value[eventGroup] = rows;
      this.createStore.dispatch({
        type: "add",
        groupName: eventGroup,
        formName: mainFormName,
        index: undefined,
        value: value,
      });
    });
  };




  onAddNew = (editInfo) => {
    
    // alert("adding new edi info")
    // debugger
    // console.log(editInfo,'add new,editinfo')
    const { isDisplayOnly, responseData, mainFormName } = this.state;
    if (!isDisplayOnly) {
      const { index, ids, dataArray } = this.state;
      
      // console.log(index,ids,dataArray,'values from state while adding');
      let gpKey = this.dynamicForm.getGroupKey(editInfo.gKey);
      let _ids = ids;
      let  _index = null;
      let actualData = {};
      let existingData = {};
      let _dataArray = [];
      _dataArray = Object.assign([], dataArray);
      _index = index;
      // console.log(_dataArray,"fdf",gpKey);
      if (index < 0) {
        // Logic for index < 0
      } else {
        actualData = this.dynamicForm.getStateObject(gpKey);
        // console.log(actualData,"actual data",index);
      } 

      if (_dataArray && _dataArray[gpKey]) {
        // alert('case1')
        //existing key matched now check if it has data
        if (_dataArray[gpKey].length === 0) {
          _ids = -9e6;
          _index = 0;
        } // ELSE do nothing as state values will be in sync hopefully
        else {
          // else id or index should be in sync hoefully
          if (
            _dataArray[editInfo.gKey].length > 0 &&
            (index === 0 || _dataArray[editInfo.gKey].length !== index)
          ) {
            // mismatch situation where state is not in sync like two tables displayed in same form and common index continues
            _index = actualData.length;
            _ids = -9e6 + actualData.length;
          }
        }
      } else {
        // alert('case2')
        //SCENARIOS 2: no match for gpKey so editInfo.gKey has to be checked
        if (_dataArray && _dataArray[editInfo.gKey]) {
          if (_dataArray[editInfo.gKey].length === 0) {
            _ids = -9e6;
            _index = 0;
          } else {
            // else id or index should be in sync hoefully
            if (
              _dataArray[editInfo.gKey].length > 0 &&
              (index === 0 || _dataArray[editInfo.gKey].length !== index)
            ) {
              // mismatch situation where state is not in sync like two tables displayed in same form and common index continues
              _index = actualData.length;
              _ids = -9e6 + actualData.length;
            }
          }
        } else {
          //SCENARIOS 3: nokey match means there is nothing yet
          _ids = -9e6;
          _index = 0;
        }
      }

      // Deep copy of dataArray to ensure immutability
      _dataArray = JSON.parse(JSON.stringify(dataArray));

      // Modify _dataArray based on conditions
      if (_index > 0) {
        let agp = {};
        // if (mainFormName === "new_laytime_calculation_form") {
        //   // Logic for new_laytime_calculation_form
        // } else {
          agp = {
            editable: editInfo.editable,
            ocd: true,
            id: _ids,
            index: _index,
            key: gpKey + "_table_row_" + _index,
          };
        //}
        // debugger;   

        if (!_dataArray.hasOwnProperty(editInfo.gKey)) {
          _dataArray[editInfo.gKey] = [agp];
        } else {
          // if (mainFormName === "new_laytime_calculation_form") {
          //   // Logic for new_laytime_calculation_form
          // } else {
            _dataArray[editInfo.gKey].push(agp);
          //}
        }
      } else if (_index === 0) {
        if (!actualData) actualData = {};
        actualData = {
          editable: editInfo.editable,
          ocd: true,
          id: _ids,
          index: _index,
          key: gpKey + "_table_row_" + _index,
        };
        _dataArray[editInfo.gKey] = [actualData];
      }
      
      
      // console.log(_dataArray,"data array befor setting local state");
      this.setState(
        (prevState) => ({
          ...prevState,
          dataArray: _dataArray,
          ids: _ids + 1,
          index: _index + 1,
        }),
        () => {
          let value = {};
          value[gpKey] = _dataArray[gpKey]
            ? _dataArray[gpKey]
            : _dataArray[editInfo.gKey];

          if (
            mainFormName === "tcov_port_itinerary" ||
            mainFormName === "tab_tcto_port_itinerary" ||
            mainFormName === "voyage_manager_port_itinerary" ||
            mainFormName === "port_expense_fda" || 
            mainFormName === "new_laytime_calculation_form"
          ) {
            this.createStore.dispatch({
              type: "add",
              groupName: gpKey,
              formName: mainFormName,
              index: undefined,
              value: value,
            });
          }
          // Logic for dispatching in specific cases
          else {
            this.createStore.dispatch({
              type: "add",
              groupName: gpKey,
              formName: mainFormName,
              index: undefined,
              value: value,
            });
          }
        }
      );
    }
  };

  bodyStyle = (index, objectKeys) => {
    return {
      marginBottom: index + 1 === objectKeys.length ? undefined : "5px",
    };
  };

  synkData = async (stateKey) => {
    let ele = {};
    if (this.props.frmConfig) {
      ele[stateKey] = await this.getDataArray(
        this.props.dataURL,
        this.props.frmConfig
      );
      this.setState({ ...this.state, ...ele });
    }
  };

  rowClassName = (record, config) => {
    let className = "";
    if (config && typeof config === "string") {
      let _config = JSON.parse(config);
      _config.colors.map((e) => {
        if (
          className === "" &&
          e.condition.indexOf(record[e.coloumnName] + "") >= 0
        ) {
          className = e.className;
        }
      });
    }
    return className;
  };

  render() {
    const regex = /^(\-+)|(\.+)$/g;
    const {
      responseData,
      dataArray,
      isDisplayOnly,
      isShowFixedColumn,
      groupKey,
      fullWidth,
      className,
      summary,
      disableTab,
    } = this.state;
    if (
      this.props.mainFormName !== "port_expense_pda_port_expense_adv" &&
      this.props.mainFormName !== "port_expense_fda" &&
      groupKey &&
      groupKey !== "" &&
      dataArray[groupKey] &&
      this.props.formData[groupKey] &&
      JSON.stringify(this.props.formData[groupKey]) !==
        JSON.stringify(dataArray[groupKey])
    ) {
      // alert('synk')
      this.synkData("dataArray");
    }

    return (
      <>
        {/* {this.props.showDiv === true ? <div className="table-group w-100p ssest"></div> : undefined}   */}

        {responseData.objectKeys.map((e, i) => {
          let eg2 = "";
          if (typeof e !== "object") {
            eg2 = e.match(regex);
            eg2 = eg2 && eg2.hasOwnProperty("length") ? eg2[0] : "";
          }

          return (
            <div
              id={className}
              
              key={className + "_table-group_" + i}
              className={
                className +
                " table-group " +
                (fullWidth === true
                  ? "w-100p"
                  : `w-${responseData.group[0]["actual_width"]}p`) +
                " " +
                (disableTab && disableTab.hasOwnProperty("tabName")
                  ? "pa"
                  : undefined)
                  
                  
              }
            >
              {disableTab && disableTab.hasOwnProperty("message") ? (
                <div className="disable-info">{disableTab.message}</div>
              ) : disableTab && !disableTab.hasOwnProperty("message") ? (
                <div className="disable-info"></div>
              ) : undefined}
              {typeof e === "object" ? (
                <div
                  key={"row-" + i}
                  className="row"
                  style={{ marginLeft: "15px", marginRight: "15px" }}
                >
                  {e.map((e1, i1) => {
                    let eg = !e1.match(regex);
                    eg = eg && eg.hasOwnProperty("length") ? eg[0] : "";
                    return (
                      <div
                        className={
                          "col-md-" +
                          (12 / (100 / responseData.groupWidth[e1])).toString()
                        }
                        style={{ padding: 0, marginBottom: "5px" }}
                        key={e1 + "-" + i1}
                      >
                        {this.dynamicForm.getStateObject("processing") ===
                        true ? (
                          <div className="processing"></div>
                        ) : undefined}

                        <Card
                          title={
                            e1 !== "g"
                              ? e1.length !== eg.length
                                ? e1
                                : undefined
                              : undefined
                          }
                          bodyStyle={{ padding: 0 }}
                          key={e1}
                        >
                          <Table
                            key={this.dynamicForm.getGroupKey(e1)}
                            className="inlineTable"
                            bordered
                            columns={responseData.columns[e1]}
                            dataSource={
                              dataArray && dataArray[e1]
                                ? dataArray[e1]
                                : this.onAddNew({ editable: true, gKey: e1 })
                            }
                            pagination={false}
                            scroll={
                              typeof isShowFixedColumn === "string" &&
                              e1 === isShowFixedColumn
                                ? {
                                    x:
                                      responseData.objectLength[e1] > 400
                                        ? responseData.objectLength[e1]
                                        : 400,
                                  }
                                : typeof isShowFixedColumn === "object" &&
                                  isShowFixedColumn.indexOf(e1) > 0
                                ? {
                                    x:
                                      responseData.objectLength[e1] > 400
                                        ? responseData.objectLength[e1]
                                        : 400,
                                  }
                                : undefined
                            }
                            rowClassName={(record) =>
                              this.rowClassName(
                                record,
                                responseData &&
                                  responseData.group &&
                                  responseData.group.hasOwnProperty("length") &&
                                  responseData.group.length > 0 &&
                                  responseData.group[0].hasOwnProperty(
                                    "color_config"
                                  )
                                  ? responseData.group[0]["color_config"]
                                  : undefined
                              )
                            }
                            footer={(pg) => {
                              let summaryTotal = {},
                                sKeys = [],
                                sum = "",
                                button = "";

                              summary.map((s) => {
                                let fObj = this.dynamicForm.getStateObject(
                                  this.dynamicForm.getGroupKey(s.gKey)
                                );
                                if (
                                  this.dynamicForm.getGroupKey(s.gKey) ===
                                    this.dynamicForm.getGroupKey(e) &&
                                  fObj
                                ) {
                                  s.showTotalFor.map((st) => {
                                    summaryTotal[st] = 0;
                                  });
                                  fObj.map((e) => {
                                    s.showTotalFor.map((st) => {
                                      if (
                                        e.hasOwnProperty(st) &&
                                        s.dhm === true
                                      ) {
                                        summaryTotal[st] =
                                          (summaryTotal[st]
                                            ? summaryTotal[st]
                                            : 0) +
                                          this.dynamicForm.stringToDaysHoursMinutsObject(
                                            e[st],
                                            true
                                          );
                                      } else if (
                                        e.hasOwnProperty(st) &&
                                        !s.dhm
                                      ) {
                                        summaryTotal[st] =
                                          (summaryTotal[st]
                                            ? summaryTotal[st]
                                            : 0) +
                                          e[st] * 1;
                                      }
                                    });
                                  });
                                }
                              });
                              sKeys = Object.keys(summaryTotal);
                              if (sKeys && sKeys.length > 0) {
                                sum = (
                                  <Row className="summary-total">
                                    <Col span={4}>Total</Col>
                                    {sKeys.map((sk) => {
                                      return (
                                        <Col span={2} className="rt">
                                          {summaryTotal[sk].toFixed(2)}
                                        </Col>
                                      );
                                    })}
                                  </Row>
                                );
                              }
                              if (isDisplayOnly === false) {
                                if (
                                  responseData &&
                                  (!responseData.hasOwnProperty("group") ||
                                    (responseData.hasOwnProperty("group") &&
                                      !responseData.group) ||
                                    (responseData.hasOwnProperty("group") &&
                                      responseData.group &&
                                      !responseData.group.hasOwnProperty(
                                        "isShowAddButton"
                                      )) ||
                                    (responseData.hasOwnProperty("group") &&
                                      responseData.group &&
                                      responseData.group.hasOwnProperty(
                                        "isShowAddButton"
                                      ) &&
                                      responseData.group.isShowAddButton === 1))
                                ) {
                                  button = (
                                    <div className="text-center">
                                      <Button
                                        type="link"
                                        onClick={() =>
                                          this.onAddNew({
                                            editable: true,
                                            gKey: e1,
                                          })
                                        }
                                      >
                                        Add New
                                      </Button>
                                    </div>
                                  );
                                }
                              }
                              return (
                                <>
                                  {sum}
                                  {button}
                                </>
                              );
                            }}
                          />
                        </Card>
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div
                  className={
                    "col-md-" +
                    (
                      12 /
                      (100 /
                        (responseData.groupWidth[e]
                          ? responseData.groupWidth[e]
                          : 100))
                    ).toString()
                  }
                  style={this.bodyStyle(i, responseData.objectKeys)}
                  key={e + "-" + i}
                >
                  {this.dynamicForm.getStateObject("processing") === true ? (
                    <div className="processing"></div>
                  ) : undefined}
                  {/* {console.log(dataArray[e], "===")} */}
                  <Card
                    title={e !== "g" ? (e.length !== eg2.length ? e : "") : ""}
                    bodyStyle={{ padding: 0 }}
                    key={e}
                  >
                    <Table
                      key={this.dynamicForm.getGroupKey(e)}
                      className="inlineTable"
                      bordered
                      columns={responseData.columns[e]}
                      
                      dataSource={
                        dataArray && dataArray[e]
                          ? dataArray[e].filter(
                              (data) => data.editable !== undefined
                            )
                          : this.onAddNew({ editable: true, gKey: e })
                      }
                      pagination={false}
                      scroll={
                        typeof isShowFixedColumn === "string" &&
                        e === isShowFixedColumn
                          ? {
                              x:
                                responseData.objectLength[e] > 400
                                  ? responseData.objectLength[e]
                                  : 400,
                            }
                          : typeof isShowFixedColumn === "object" &&
                            isShowFixedColumn.indexOf(e) >= 0
                          ? {
                              x:
                                responseData.objectLength[e] > 400
                                  ? responseData.objectLength[e]
                                  : 400,
                            }
                          : undefined
                      }
                      rowClassName={(record) =>
                        this.rowClassName(
                          record,
                          responseData &&
                            responseData.group &&
                            responseData.group.hasOwnProperty("length") &&
                            responseData.group.length > 0 &&
                            responseData.group[0].hasOwnProperty("color_config")
                            ? responseData.group[0]["color_config"]
                            : undefined
                        )
                      }
                      footer={(pg) => {
                        let summaryTotal = {},
                          sKeys = [],
                          sum = "",
                          button = "";
                        summary.map((s) => {
                          let fObj = this.dynamicForm.getStateObject(
                            this.dynamicForm.getGroupKey(s.gKey)
                          );
                          summaryTotal["dhm"] = s.dhm && s.dhm === true;
                          if (
                            this.dynamicForm.getGroupKey(s.gKey) ===
                              this.dynamicForm.getGroupKey(e) &&
                            fObj
                          ) {
                            s.showTotalFor.map((st) => {
                              summaryTotal[st] = 0;
                            });
                            fObj.map((e) => {
                              if (e) {
                                s.showTotalFor.map((st) => {
                                  if (e.hasOwnProperty(st) && s.dhm === true) {
                                    summaryTotal[st] =
                                      (summaryTotal[st]
                                        ? summaryTotal[st]
                                        : 0) +
                                      this.dynamicForm.stringToDaysHoursMinutsObject(
                                        e[st],
                                        true
                                      );
                                  } else if (e.hasOwnProperty(st) && !s.dhm) {
                                    summaryTotal[st] =
                                      (summaryTotal[st]
                                        ? summaryTotal[st]
                                        : 0) +
                                      e[st] * 1;
                                  }
                                });
                              }
                            });
                          }
                        });
                        let dhm = summaryTotal["dhm"];
                        delete summaryTotal["dhm"];
                        sKeys = Object.keys(summaryTotal);
                        if (
                          this.props.mainFormName !== "tc_make_payment" &&
                          this.props.mainFormName !== "tco_make_payment" &&
                          this.props.mainFormName !== "tc_commission_entry" &&
                          this.props.mainFormName !== "tco_commission_entry" &&
                          this.props.mainFormName !==
                            "freight-commission-invoice"
                        ) {
                          if (sKeys && sKeys.length > 0) {
                            sum = (
                              <Row className="summary-total">
                                <Col span={4}>Total</Col>
                                {sKeys.map((sk) => {
                                  if (dhm === true) {
                                    let ele =
                                      this.dynamicForm.numberToDaysHoursMinutsObject(
                                        summaryTotal[sk].toFixed(2)
                                      );
                                    return (
                                      <Col span={2} className="rt">
                                        {this.dynamicForm.numberPad(
                                          ele["days"],
                                          2
                                        ) +
                                          "D" +
                                          this.dynamicForm.numberPad(
                                            ele["hours"],
                                            2
                                          ) +
                                          "H:" +
                                          this.dynamicForm.numberPad(
                                            ele["minutes"],
                                            2
                                          ) +
                                          "M"}
                                      </Col>
                                    );
                                  } else {
                                    return (
                                      <Col span={2} className="rt">
                                        {summaryTotal[sk].toFixed(2)}
                                      </Col>
                                    );
                                  }
                                })}
                              </Row>
                            );
                          }
                        }
                        if (
                          isDisplayOnly === false &&
                          responseData &&
                          responseData.hasOwnProperty("group") &&
                          responseData.group &&
                          ((responseData.group[0] &&
                            responseData.group[0].hasOwnProperty(
                              "isShowAddButton"
                            ) &&
                            responseData.group[0]["isShowAddButton"] === 1) ||
                            responseData.group.length === 0)
                        ) {
                          button = (
                            <div className="text-center">
                              <Button
                                type="link"
                                onClick={() =>
                                  this.onAddNew({ editable: true, gKey: e })
                                }
                              >
                                Add New
                              </Button>
                            </div>
                          );
                        }

                        return (
                          <>
                            {sum}
                            {button}
                          </>
                        );
                      }}
                      //rowClassNamer={(r, i) => ((i % 2 === 0) ? undefined : 'dull-color')}
                    />
                  </Card>
                </div>
              )}
            </div>
          );
        })}
      </>
    );
  }
}

export default IndexTableForm;
