import React, { useEffect, useRef, useState } from "react";
import {
  DeleteOutlined,
  EditOutlined,
  MenuOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import _ from "lodash";
import {
  Card,
  Dropdown,
  Popconfirm,
  Table,
  Menu,
  Button,
  Row,
  Col,
} from "antd";
import { v4 as uuidv4 } from "uuid";
import { object } from "yup";
import { URL_WITHOUT_VERSION, getAPICall, useStateCallback } from "..";

const OceanTableForm = (props) => {
  const [state, setState] = useStateCallback({
    mainFormName: props.parentformName
      ? props.parentformName
      : props.mainFormName,
    responseData: { columns: [], objectKeys: [], dataArray: {}, group: {} },
    ids: -9e6,
    index: 0,
    isDisplayOnly: props.displayOnly === true,
    editMode: props.editMode,
    isShowFixedColumn: props.isShowFixedColumn,
    extraTableButton: props.extraTableButton || undefined,
    parentFormdata: props.formData || {}, // replacemnet of dataArray
    tableData: {},
    groupKey: props.groupKey || undefined,
    fullWidth: props.fullWidth ? true : false,
    summary: props.summary || [],
    dyncGroupName: props.dyncGroupName || undefined,
    fieldModalEvent: props.modalEvent || undefined,
    disableTab: props.disableTab || undefined,
    tableRowDeleteAction: props.tableRowDeleteAction || (() => {}),
  });

  const dynamicForm = props.dynamicForm;
  const createStore = props.createStore;
  const tabledataref = useRef(null);

  useEffect(() => {
    /*
    let data = {},
      headers = { extraCols: [] };
    if (props.frmConfig) {
      data = props.frmConfig;

      // Assuming createColumns and getDataArray return promises
      Promise.all([createColumns(data), getDataArray(data)])
        .then(([columns, sourcedata]) => {
          setState((prevState) => ({
            ...prevState,
            responseData: columns,
            tableData: { ...sourcedata },
          }));

          // Update the ref after state is updated
          tabledataref.current = sourcedata;
        })
        .catch((error) => {
          // Handle errors if any
          console.error("Error occurred:", error);
        });
    }


*/

    firstloadtableForm();
  }, []);

  const firstloadtableForm = async () => {
    let data = {},
      headers = { extraCols: [] };
    if (props.frmCode) {
      props.originalForm.map((e) => {
        if (e.f_dyn_col && e.f_dyn_col !== "") {
          let item = JSON.parse(e.f_dyn_col);
          item["tableColumnValue"] = props.formData[item["keyColumn"]];
          headers["extraCols"].push(item);
          headers["formCode"] = e.frm_code;
        }
      });
      const response = await getAPICall(
        `${URL_WITHOUT_VERSION}get/${props.frmCode}`,
        headers
      );
      data = await response["data"];
    } else if (props.frmConfig) {
      data = props.frmConfig;
    }

    if (props.dyncGroupName && props.dyncGroupName !== "") {
      data["groups"].map((e, i) => {
        data["groups"][i] = Object.assign({}, e, {
          group_name: props.dyncGroupName,
          group_key: dynamicForm.getGroupKey(props.dyncGroupName),
        });
      });

      data["frm"].map((e, i) => {
        data["frm"][i] = Object.assign({}, e, {
          group_name: props.dyncGroupName,
        });
      });
    }

    dynamicForm.originalState.setFormFields(data["frm"]);

    // Assuming createColumns and getDataArray return promises
    Promise.all([createColumns(data), getDataArray(data)])
      .then(([columns, sourcedata]) => {
        setState((prevState) => ({
          ...prevState,
          responseData: columns,
          tableData: { ...sourcedata },
        }));

        // Update the ref after state is updated
        tabledataref.current = sourcedata;
      })
      .catch((error) => {
        // Handle errors if any
        console.error("Error occurred:", error);
      });
  };

  const createColumns = async (data) => {
    const {
      isDisplayOnly,
      isShowFixedColumn,
      extraTableButton,
      fieldModalEvent,
      editMode,
    } = state;
    let groups = {};
    let objectKeys = [];
    let groupWidth = {};
    let itemData = [];
    let subItems = [];
    let skipCols = [];
    let objWidth = {};
    let addItems = {};
    if (data && data.groups) {
      data.groups.map((e) => (groupWidth[e.group_name] = e.g_width * 1));
    }

    data.frm.map((e, i) => {
      let colSettings = "";
      if (
        objectKeys.indexOf(e.group_name) === -1 &&
        e.group_name &&
        !groups.hasOwnProperty(e.group_name)
      ) {
        groups[e.group_name] = [];
        objectKeys.push(e.group_name);
      } else if (
        objectKeys.indexOf("g") === -1 &&
        !e.group_name &&
        !groups.hasOwnProperty(e.group_name)
      ) {
        groups["g"] = [];
        objectKeys.push("g");
      }

      if (e.f_default && e.f_default !== "") {
        addItems[e.f_name] = e.f_default;
      }

      if (!objWidth[e.group_name || "g"]) {
        objWidth[e.group_name || "g"] = 0;
      }

      if (
        e.f_t_col_span &&
        e.f_t_col_span !== "" &&
        e.f_t_col_span !== JSON.stringify({})
      ) {
        colSettings = JSON.parse(e.f_t_col_span);

        if (
          typeof colSettings.skipCol === "object" &&
          colSettings.skipCol.hasOwnProperty("length")
        ) {
          colSettings.skipCol.map((e) => skipCols.push(e));
        } else {
          skipCols.push(colSettings.skipCol);
        }

        colSettings.colDetails.children.map((ec, ic) => {
          let ecID = ec.f_id * 1;
          let colValue = Object.assign({}, e);

          if (
            ic >= 1 &&
            (colSettings.skipCol === ecID ||
              colSettings.skipCol.indexOf(ecID) >= 0)
          ) {
            colValue = data.frm.find((e) => e.f_id * 1 === ecID);
          }
          colSettings.colDetails.children[ic] = {
            title: ec.title,
            dataIndex: ec.dataIndex,
            key: ec.key,
            width: ec.width,
            actualField: colValue,
            render: (text, record) =>
              dynamicForm.emptyCell(
                colValue,
                text,
                record,
                false,
                false,
                fieldModalEvent
              ),
          };
          objWidth[e.group_name || "g"] =
            objWidth[e.group_name || "g"] + ec.width * 1;
        });
        groups[e.group_name || "g"].push(colSettings.colDetails);
      } else if (skipCols.indexOf(e.f_id) < 0) {
        groups[e.group_name || "g"].push({
          title: e.name,
          dataIndex: e.f_name,
          key: e.f_name,
          actualField: e,
          width:
            e.f_width && e.f_width !== ""
              ? isNaN(e.f_width * 1)
                ? e.f_width
                : e.f_width * 1
              : undefined,
          render: (text, record) =>
            dynamicForm.emptyCell(
              e,
              text,
              record,
              false,
              false,
              fieldModalEvent
            ), //emptyCell()
        });
        objWidth[e.group_name || "g"] =
          objWidth[e.group_name || "g"] +
          (e.f_width && e.f_width !== "" ? e.f_width * 1 : 0);
      }
    });

    if (isDisplayOnly === false) {
      objectKeys.map((e) => {
        if (
          (data["groups"] &&
            data["groups"].length > 0 &&
            data["groups"][0].hasOwnProperty("isShowActionButtons") &&
            data["groups"][0]["isShowActionButtons"] === 1) ||
          (data["groups"] && data["groups"].length == 0)
        ) {
          objWidth[e] += 100;
          groups[e].push({
            title: "Actions",
            dataIndex: "actions",
            key: "actions",
            fixed:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? "right"
                : undefined,
            width:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? 100
                : 120,
            render: (text, record) => {
              const { editable } = record;
              let gpkey = dynamicForm.getGroupKey(e);
              return (
                <div className="editable-row-operations">
                  {editable ? (
                    <div>
                      {editMode ? (
                        <span className="iconWrapper cancel">
                          <Popconfirm
                            placement={
                              gpkey == "portitinerary" ? "left" : "top"
                            }
                            title={
                              gpkey == "portitinerary" ? (
                                <p>
                                  Deleting this port will require reselecting{" "}
                                  <br /> the below port again to match the
                                  distance.
                                </p>
                              ) : (
                                "Sure to delete?"
                              )
                            }
                            onConfirm={() =>
                              setTimeout(() => {
                                editTableData({
                                  editable: false,
                                  ID: record.id,
                                  gKey: e,
                                  ocd: record.ocd,
                                  index: record.index,
                                  action: "delete",
                                });
                              }, 30)
                            }
                          >
                            <DeleteOutlined />
                          </Popconfirm>
                        </span>
                      ) : (
                        ""
                      )}

                      {extraTableButton &&
                      extraTableButton.hasOwnProperty(e) &&
                      extraTableButton[e].length > 0
                        ? extartButtons(extraTableButton[e], record, e)
                        : undefined}
                    </div>
                  ) : (
                    <>
                      <span
                        className="iconWrapper edit"
                        onClick={(ev) =>
                          editTableData({
                            editable: true,
                            ID: record.id,
                            gKey: e,
                            index: record.index,
                          })
                        }
                      >
                        <EditOutlined />
                      </span>
                      {extraTableButton &&
                      extraTableButton.hasOwnProperty(e) &&
                      extraTableButton[e].length > 0
                        ? extartButtons(extraTableButton[e], record, e)
                        : undefined}
                    </>
                  )}
                </div>
              );
            },
          });
        } else if (
          (data["groups"] &&
            data["groups"].length > 0 &&
            data["groups"][0].hasOwnProperty("isShowActionButtons") &&
            data["groups"][0]["isShowActionButtons"] === 0 &&
            extraTableButton &&
            extraTableButton.hasOwnProperty(e) &&
            extraTableButton[e].length > 0) ||
          (data["groups"] && data["groups"].length == 0)
        ) {
          objWidth[e] += 100;
          groups[e].push({
            title: "Actions",
            dataIndex: "actions",
            key: "actions",
            fixed:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? "right"
                : undefined,
            width:
              isShowFixedColumn &&
              (e === isShowFixedColumn || isShowFixedColumn.indexOf(e) >= 0)
                ? 100
                : 120,
            render: (text, record) => {
              const { editable } = record;
              return (
                <div className="editable-row-operations">
                  {editable
                    ? extartButtons(extraTableButton[e], record, e)
                    : undefined}
                </div>
              );
            },
          });
        }
      });
    }

    objectKeys.map((e) => {
      if (groupWidth[e] < 100) {
        subItems.push(e);
      } else if (groupWidth[e] === 100 || !groupWidth[e]) {
        if (subItems.length > 0) {
          itemData.push(subItems);
          subItems = [];
        }
        itemData.push(e);
      }
    });

    if (subItems.length > 0) {
      itemData.push(subItems);
      subItems = [];
    }

    // addItems=> contain object which has elements placeholders while adding new item using add new btn.
    return {
      columns: groups,
      objectKeys: itemData,
      group: data["groups"],
      groupWidth: groupWidth,
      objectLength: objWidth,
      addItemValue: addItems,
    };
  };

  const getDataArray = async (dataObject) => {
    let dataPart = {};
    let objectKeys = [];
    // this function is used for getting the table data .
    dataObject?.frm?.map((e) => {
      if (e.group_name && objectKeys.indexOf(e.group_name) === -1) {
        objectKeys.push(e.group_name);
      } else if (!e.group_name && objectKeys.indexOf() === -1) {
        objectKeys.push("g");
      }
    });

    if (props.formData) {
      objectKeys?.forEach((fo) => {
        let groupKey = dynamicForm.getGroupKey(fo);

        if (
          props.formData.hasOwnProperty(groupKey) &&
          props.formData[groupKey]
        ) {
          let data = props.formData[groupKey];
          data &&
            data.length > 0 &&
            data.forEach((d, i) => {
              data[i]["index"] = i;
              data[i]["key"] = groupKey + "_table_row_" + i;
            });
          dataPart[groupKey] = data;
        }
      });
    }

    objectKeys?.map((e) => {
      let groupKey = dynamicForm.getGroupKey(e);
      if (dataPart && dataPart[groupKey]) {
        dataPart[groupKey].map((e) => (e["editable"] = true));
      }
    });

    /*

    the final output from this function is an object which containing groupkey with arr
      like this
      gropukey is after getting from the function  dynamicForm.getGroupKey(), 
      datapart={
        'groupkey':[{},{}]
      }

*/

    return dataPart;
  };

  const rowClassName = (record, config) => {
    let className = "";
    if (config && typeof config === "string") {
      let _config = JSON.parse(config);
      _config.colors.map((e) => {
        if (
          className === "" &&
          e.condition.indexOf(record[e.coloumnName] + "") >= 0
        ) {
          className = e.className;
        }
      });
    }
    return className;
  };

  const extartButtons = (data, record, e) => {
    let rows = [];
    data.map((b) => {
      if (!b.hasOwnProperty("isMenu") && !b.hasOwnProperty("onClickAction")) {
        rows.push(
          <span
            key={uuidv4()}
            className={
              "iconWrapper " +
              b.icon +
              " " +
              (b.class && b.class !== "" ? b.class : undefined)
            }
            onClick={() =>
              onAddNewBelow({
                gKey: e,
                index: record.index,
                callType: data[0]["callType"],
              })
            }
          >
            {b.icon}
          </span>
        );
      } else if (
        !b.hasOwnProperty("isMenu") &&
        b.hasOwnProperty("onClickAction") &&
        typeof b.onClickAction === "function"
      ) {
        rows.push(
          <span
            key={uuidv4()}
            className={
              "iconWrapper " +
              b.icon +
              " " +
              (b.class && b.class !== "" ? b.class : undefined)
            }
            onClick={() =>
              b.onClickAction(
                { ID: record.id, gKey: e, index: record.index },
                dynamicForm.stateObject
              )
            }
          >
            {b.icon}
          </span>
        );
      } else if (b.hasOwnProperty("isMenu") && b["isMenu"] === true) {
        const menu = (
          <Menu>
            {b["options"].map((bo) => (
              <Menu.Item
                onClick={() =>
                  bo.onClickAction({
                    ID: record.id,
                    gKey: e,
                    index: record.index,
                    eventType: bo.key,
                    eventData: record,
                  })
                }
                key={bo.key}
              >
                {bo.text}
              </Menu.Item>
            ))}
          </Menu>
        );
        rows.push(
          <Dropdown menu={menu}>
            <span key={uuidv4()} className={"iconWrapper " + b.icon}>
              <SettingOutlined />
            </span>
          </Dropdown>
        );
      }
    });

    return rows;
  };

  const editTableData = (editInfo) => {
    
    const { summary, mainFormName, tableData } = state;
    let _tableData = tabledataref.current;
    let _id = -9e6;
    let eArray = [];
    let upObj = {};
    let gpKey = dynamicForm.getGroupKey(editInfo.gKey);
    _tableData[gpKey]?.map((dag, index) => {
      if (dag.id === editInfo.ID) {
        _tableData[gpKey][index] = {
          ...dag,
          editable: editInfo.editable === false ? undefined : true,
        };
      }
    });

    if (
      editInfo.ocd ||
      (editInfo.hasOwnProperty("action") && editInfo["action"] === "delete")
    ) {
      _tableData[gpKey]?.map((dag, index) => {
        if (
          dag.id === editInfo.ID &&
          dag.ocd === editInfo.ocd &&
          dag.editable == undefined
        ) {
          state.tableRowDeleteAction(editInfo, _tableData[gpKey][index]);
          //  delete dArray[editInfo.gKey][index];
          if (eArray.length === 0) {
            _tableData[gpKey]?.map((p, pi) => {
              if (pi !== index) {
                upObj = {
                  id: _id + eArray.length,
                  index: eArray.length,
                  key: gpKey + "_table_row_" + eArray.length,
                };
                if (p["id"] > 0)
                  upObj = {
                    index: eArray.length,
                    key: gpKey + "_table_row_" + eArray.length,
                  };
                eArray.push(Object.assign(p, upObj));
              }
            });
            _tableData[gpKey] = [...eArray];

            dynamicForm.deleteRow(editInfo);
          }
        }
      });
    }

    _id = _id + eArray.length;

    tabledataref.current = _tableData;

    setState(
      (prevState) => ({
        ...prevState,
        tableData: _.cloneDeep(_tableData),
        //  ids:_id,
      }),
      () => {
        let gpKey = dynamicForm.getGroupKey(editInfo.gKey);
        let value = {};
        value[gpKey] = _tableData[gpKey];

        createStore.dispatch({
          type: "add",
          groupName: gpKey,
          formName: mainFormName,
          index: undefined,
          value: value,
        });
      }
    );
  };

  const onAddNew = (editInfo) => {
    const { isDisplayOnly, mainFormName, responseData } = state;
    let agp = {};
    if (!isDisplayOnly) {
      const { index, ids, tableData } = state;
      let gpkey = dynamicForm.getGroupKey(editInfo.gKey);
      let _ids = ids,
        _index = index;
      let _tableData = JSON.parse(JSON.stringify(tableData));
      let actualTableData = dynamicForm.getStateObject(gpkey);
      if (_tableData && _tableData[gpkey]) {
        if (_tableData[gpkey].length == 0) {
          _ids = -9e6;
          _index = 0;
        } else {
          _ids = -9e6 + actualTableData.length;
          _index = actualTableData.length;
        }
      } else if (_tableData && !_tableData[gpkey]) {
        _ids = -9e6;
        _index = 0;
      }

      agp = {
        editable: editInfo.editable,
        ocd: true,
        id: _ids,
        index: _index,
        key: gpkey + "_table_row_" + _index,
      };

      if (_index > 0) {
        agp = Object.assign(agp, responseData.addItemValue);
        _tableData[gpkey] = [...actualTableData, { ...agp }];
      } else if (_index == 0) {
        agp = Object.assign(agp, responseData.addItemValue);
        _tableData[gpkey] = [agp];
      }

      tabledataref.current = _tableData;
      setState(
        (prevState) => ({
          ...prevState,
          tableData: Object.assign({}, _tableData),
          ids: _ids,
          index: _index,
        }),
        () => {
          let value = {};
          value[gpkey] = _tableData[gpkey];
          createStore.dispatch({
            type: "add",
            groupName: gpkey,
            formName: mainFormName,
            index: undefined,
            value: value,
          });
        }
      );
    }
  };

  const onAddNewBelow = (event) => {};

  //  const onAddNew = (editInfo) => {}

  const bodyStyle = (index, objectKeys) => {
    return {
      marginBottom: index + 1 === objectKeys.length ? undefined : "5px",
    };
  };

  const regex = /^(\-+)|(\.+)$/g;

  const {
    responseData,
    dataArray,
    isDisplayOnly,
    isShowFixedColumn,
    groupKey,
    fullWidth,
    className,
    summary,
    disableTab,
    tableData,
  } = state;

  return (
    <>
      {responseData.objectKeys.map((key, ind) => {
        let eg2 = "";
        if (typeof key !== "object") {
          eg2 = key.match(regex);
          eg2 = eg2 && eg2.hasOwnProperty("length") ? eg2[0] : "";
        }
        return (
          <div
            key={className + "_table-group_" + ind}
            className={
              className +
              " table-group " +
              (fullWidth === true
                ? "w-100p"
                : `w-${responseData.group[0]["actual_width"]}p`) +
              " " +
              (disableTab && disableTab.hasOwnProperty("tabName")
                ? "pa"
                : undefined)
            }
          >
            {typeof key === "object" ? (
              <>
                <h1> this is another table for testing.</h1>
                {/* 
              

<div
  key={"row-" + i}
  className="row"
  style={{ marginLeft: "15px", marginRight: "15px" }}
>
  {e.map((e1, i1) => {
    let eg = !e1.match(regex);
    eg = eg && eg.hasOwnProperty("length") ? eg[0] : "";
    return (
      <div
        className={
          "col-md-" +
          (12 / (100 / responseData.groupWidth[e1])).toString()
        }
        style={{ padding: 0, marginBottom: "5px" }}
        key={e1 + "-" + i1}
      >
        {this.dynamicForm.getStateObject("processing") ===
        true ? (
          <div className="processing"></div>
        ) : undefined}

        <Card
          title={
            e1 !== "g"
              ? e1.length !== eg.length
                ? e1
                : undefined
              : undefined
          }
          bodyStyle={{ padding: 0 }}
          key={e1}
        >
          <Table
            key={this.dynamicForm.getGroupKey(e1)}
            className="inlineTable"
            bordered
            columns={responseData.columns[e1]}
            dataSource={
              dataArray && dataArray[e1]
                ? dataArray[e1]
                : this.onAddNew({ editable: true, gKey: e1 })
            }
            pagination={false}
            scroll={
              typeof isShowFixedColumn === "string" &&
              e1 === isShowFixedColumn
                ? {
                    x:
                      responseData.objectLength[e1] > 400
                        ? responseData.objectLength[e1]
                        : 400,
                  }
                : typeof isShowFixedColumn === "object" &&
                  isShowFixedColumn.indexOf(e1) > 0
                ? {
                    x:
                      responseData.objectLength[e1] > 400
                        ? responseData.objectLength[e1]
                        : 400,
                  }
                : undefined
            }
            rowClassName={(record) =>
              this.rowClassName(
                record,
                responseData &&
                  responseData.group &&
                  responseData.group.hasOwnProperty("length") &&
                  responseData.group.length > 0 &&
                  responseData.group[0].hasOwnProperty(
                    "color_config"
                  )
                  ? responseData.group[0]["color_config"]
                  : undefined
              )
            }
            footer={(pg) => {
              let summaryTotal = {},
                sKeys = [],
                sum = "",
                button = "";

              summary.map((s) => {
                let fObj = this.dynamicForm.getStateObject(
                  this.dynamicForm.getGroupKey(s.gKey)
                );
                if (
                  this.dynamicForm.getGroupKey(s.gKey) ===
                    this.dynamicForm.getGroupKey(e) &&
                  fObj
                ) {
                  s.showTotalFor.map((st) => {
                    summaryTotal[st] = 0;
                  });
                  fObj.map((e) => {
                    s.showTotalFor.map((st) => {
                      if (
                        e.hasOwnProperty(st) &&
                        s.dhm === true
                      ) {
                        summaryTotal[st] =
                          (summaryTotal[st]
                            ? summaryTotal[st]
                            : 0) +
                          this.dynamicForm.stringToDaysHoursMinutsObject(
                            e[st],
                            true
                          );
                      } else if (
                        e.hasOwnProperty(st) &&
                        !s.dhm
                      ) {
                        summaryTotal[st] =
                          (summaryTotal[st]
                            ? summaryTotal[st]
                            : 0) +
                          e[st] * 1;
                      }
                    });
                  });
                }
              });
              sKeys = Object.keys(summaryTotal);
              if (sKeys && sKeys.length > 0) {
                sum = (
                  <Row className="summary-total">
                    <Col span={4}>Total</Col>
                    {sKeys.map((sk) => {
                      return (
                        <Col span={2} className="rt">
                          {summaryTotal[sk].toFixed(2)}
                        </Col>
                      );
                    })}
                  </Row>
                );
              }
              if (isDisplayOnly === false) {
                if (
                  responseData &&
                  (!responseData.hasOwnProperty("group") ||
                    (responseData.hasOwnProperty("group") &&
                      !responseData.group) ||
                    (responseData.hasOwnProperty("group") &&
                      responseData.group &&
                      !responseData.group.hasOwnProperty(
                        "isShowAddButton"
                      )) ||
                    (responseData.hasOwnProperty("group") &&
                      responseData.group &&
                      responseData.group.hasOwnProperty(
                        "isShowAddButton"
                      ) &&
                      responseData.group.isShowAddButton === 1))
                ) {
                  button = (
                    <div className="text-center">
                      <Button
                        type="link"
                        onClick={() =>
                          this.onAddNew({
                            editable: true,
                            gKey: e1,
                          })
                        }
                      >
                        Add New
                      </Button>
                    </div>
                  );
                }
              }
              return (
                <>
                  {sum}
                  {button}
                </>
              );
            }}
          />
        </Card>
      </div>
    );
  })}
</div>

 */}
              </>
            ) : (
              <div
                className={
                  "col-md-" +
                  (
                    12 /
                    (100 /
                      (responseData.groupWidth[key]
                        ? responseData.groupWidth[key]
                        : 100))
                  ).toString()
                }
                style={bodyStyle(ind, responseData.objectKeys)}
                key={key + "-" + ind}
              >
                {dynamicForm.getStateObject("processing") === true ? (
                  <div className="processing"></div>
                ) : undefined}

                <Card
                  title={
                    key !== "g" ? (key.length !== eg2.length ? key : "") : ""
                  }
                  bodyStyle={{ padding: 0 }}
                  key={key}
                >
                  <Table
                    key={dynamicForm.getGroupKey(key)}
                    className="inlineTable"
                    bordered
                    columns={responseData.columns[key]}
                    dataSource={
                      tableData && tableData[dynamicForm.getGroupKey(key)]
                        ? tableData[dynamicForm.getGroupKey(key)].filter(
                            (data) => data.editable !== undefined
                          )
                        : onAddNew({ editable: true, gKey: key })
                    }
                    pagination={false}
                    scroll={
                      typeof isShowFixedColumn === "string" &&
                      key === isShowFixedColumn
                        ? {
                            x:
                              responseData.objectLength[key] > 400
                                ? responseData.objectLength[key]
                                : 400,
                          }
                        : typeof isShowFixedColumn === "object" &&
                          isShowFixedColumn.indexOf(key) >= 0
                        ? {
                            x:
                              responseData.objectLength[key] > 400
                                ? responseData.objectLength[key]
                                : 400,
                          }
                        : undefined
                    }
                    rowClassName={(record) =>
                      rowClassName(
                        record,
                        responseData &&
                          responseData.group &&
                          responseData.group.hasOwnProperty("length") &&
                          responseData.group.length > 0 &&
                          responseData.group[0].hasOwnProperty("color_config")
                          ? responseData.group[0]["color_config"]
                          : undefined
                      )
                    }
                    footer={(pg) => {
                      let summaryTotal = {},
                        sKeys = [],
                        sum = "",
                        button = "";
                      summary.map((s) => {
                        let fObj = dynamicForm.getStateObject(
                          dynamicForm.getGroupKey(s.gKey)
                        );
                        summaryTotal["dhm"] = s.dhm && s.dhm === true;
                        if (
                          dynamicForm.getGroupKey(s.gKey) ===
                            dynamicForm.getGroupKey(key) &&
                          fObj
                        ) {
                          s.showTotalFor.map((st) => {
                            summaryTotal[st] = 0;
                          });
                          fObj.map((e) => {
                            if (e) {
                              s.showTotalFor.map((st) => {
                                if (e.hasOwnProperty(st) && s.dhm === true) {
                                  summaryTotal[st] =
                                    (summaryTotal[st] ? summaryTotal[st] : 0) +
                                    dynamicForm.stringToDaysHoursMinutsObject(
                                      e[st],
                                      true
                                    );
                                } else if (e.hasOwnProperty(st) && !s.dhm) {
                                  summaryTotal[st] =
                                    (summaryTotal[st] ? summaryTotal[st] : 0) +
                                    e[st] * 1;
                                }
                              });
                            }
                          });
                        }
                      });
                      let dhm = summaryTotal["dhm"];
                      delete summaryTotal["dhm"];
                      sKeys = Object.keys(summaryTotal);
                      if (
                        props.mainFormName !== "tc_make_payment" &&
                        props.mainFormName !== "tco_make_payment" &&
                        props.mainFormName !== "tc_commission_entry" &&
                        props.mainFormName !== "tco_commission_entry" &&
                        props.mainFormName !== "freight-commission-invoice"
                      ) {
                        if (sKeys && sKeys.length > 0) {
                          sum = (
                            <Row className="summary-total">
                              <Col span={4}>Total</Col>
                              {sKeys.map((sk) => {
                                if (dhm === true) {
                                  let ele =
                                    dynamicForm.numberToDaysHoursMinutsObject(
                                      summaryTotal[sk].toFixed(2)
                                    );
                                  return (
                                    <Col span={2} className="rt">
                                      {dynamicForm.numberPad(ele["days"], 2) +
                                        "D" +
                                        dynamicForm.numberPad(ele["hours"], 2) +
                                        "H:" +
                                        dynamicForm.numberPad(
                                          ele["minutes"],
                                          2
                                        ) +
                                        "M"}
                                    </Col>
                                  );
                                } else {
                                  return (
                                    <Col span={2} className="rt">
                                      {summaryTotal[sk].toFixed(2)}
                                    </Col>
                                  );
                                }
                              })}
                            </Row>
                          );
                        }
                      }
                      if (
                        isDisplayOnly === false &&
                        responseData &&
                        responseData.hasOwnProperty("group") &&
                        responseData.group &&
                        ((responseData.group[0] &&
                          responseData.group[0].hasOwnProperty(
                            "isShowAddButton"
                          ) &&
                          responseData.group[0]["isShowAddButton"] === 1) ||
                          responseData.group.length === 0)
                      ) {
                        button = (
                          <div className="text-center">
                            <Button
                              type="link"
                              onClick={() =>
                                onAddNew({ editable: true, gKey: key })
                              }
                            >
                              Add New
                            </Button>
                          </div>
                        );
                      }

                      return (
                        <>
                          {sum}
                          {button}
                        </>
                      );
                    }}
                  />
                </Card>
              </div>
            )}
          </div>
        );
      })}
    </>
  );
};
export default OceanTableForm;
