import React, { useState, useEffect, useRef } from 'react';
import { Table, Row, Col, Button } from 'antd';
import dragula from 'dragula';
import 'dragula/dist/dragula.css';

const DragAndDropTable = (props) => {
  const {
    responseData,
    dataArray,
    isShowFixedColumn,
    summary,
    isDisplayOnly,
    mainFormName,
    rowClassName,
    onAddNew,
    dynamicForm,
    e,
  } = props;

  const [summaryTotal, setSummaryTotal] = useState({});
  const [sKeys, setSKeys] = useState([]);
  const [dhm, setDhm] = useState(false);
  const [sum, setSum] = useState('');
  const [button, setButton] = useState('');

  const tableBodyRef = useRef(null);

  useEffect(() => {
    let tempSummaryTotal = {};
    let tempSKeys = [];
    let tempDhm = false;

    summary.forEach((s) => {
      let fObj = dynamicForm.getStateObject(dynamicForm.getGroupKey(s.gKey));
      tempDhm = s.dhm && s.dhm === true;

      if (dynamicForm.getGroupKey(s.gKey) === dynamicForm.getGroupKey(e) && fObj) {
        s.showTotalFor.forEach((st) => {
          tempSummaryTotal[st] = 0;
        });

        fObj.forEach((e) => {
          if (e) {
            s.showTotalFor.forEach((st) => {
              if (e.hasOwnProperty(st) && tempDhm === true) {
                tempSummaryTotal[st] =
                  (tempSummaryTotal[st] ? tempSummaryTotal[st] : 0) +
                  dynamicForm.stringToDaysHoursMinutsObject(e[st], true);
              } else if (e.hasOwnProperty(st) && !tempDhm) {
                tempSummaryTotal[st] =
                  (tempSummaryTotal[st] ? tempSummaryTotal[st] : 0) + e[st] * 1;
              }
            });
          }
        });
      }
    });

    setDhm(tempDhm);
    delete tempSummaryTotal['dhm'];
    setSummaryTotal(tempSummaryTotal);
    setSKeys(Object.keys(tempSummaryTotal));
  }, [summary]);

  useEffect(() => {
    let tempSum = '';
    let tempButton = '';

    if (
      mainFormName !== 'tc_make_payment' &&
      mainFormName !== 'tco_make_payment' &&
      mainFormName !== 'tc_commission_entry' &&
      mainFormName !== 'tco_commission_entry' &&
      mainFormName !== 'freight-commission-invoice'
    ) {
      if (sKeys && sKeys.length > 0) {
        tempSum = (
          <Row className="summary-total">
            <Col span={4}>Total</Col>
            {sKeys.map((sk) => {
              if (dhm === true) {
                let ele = dynamicForm.numberToDaysHoursMinutsObject(
                  summaryTotal[sk].toFixed(2)
                );
                return (
                  <Col span={2} className="rt">
                    {`${dynamicForm.numberPad(ele['days'], 2)}D${dynamicForm.numberPad(
                      ele['hours'],
                      2
                    )}H:${dynamicForm.numberPad(ele['minutes'], 2)}M`}
                  </Col>
                );
              } else {
                return <Col span={2} className="rt">{summaryTotal[sk].toFixed(2)}</Col>;
              }
            })}
          </Row>
        );
      }
    }

    if (
      isDisplayOnly === false &&
      responseData &&
      responseData.hasOwnProperty('group') &&
      responseData.group &&
      ((responseData.group[0] &&
        responseData.group[0].hasOwnProperty('isShowAddButton') &&
        responseData.group[0]['isShowAddButton'] === 1) ||
        responseData.group.length === 0)
    ) {
      tempButton = (
        <div className="text-center">
          <Button type="link" onClick={() => onAddNew({ editable: true, gKey: e })}>
            Add New
          </Button>
        </div>
      );
    }

    setSum(tempSum);
    setButton(tempButton);
  }, [sKeys, dhm, isDisplayOnly, responseData]);

  useEffect(() => {
    const container = tableBodyRef.current;
    const drake = dragula([container]);

    drake.on('drop', (el, target, source, sibling) => {
      const newIndex = Array.from(target.children).indexOf(el);
      const originalIndex = Array.from(source.children).indexOf(el);

      const updatedDataArray = [...dataArray[e]];
      const [draggedItem] = updatedDataArray.splice(originalIndex, 1);
      updatedDataArray.splice(newIndex, 0, draggedItem);

      // Assuming you have a function to update dataArray in the parent component
      // For example, you might have a function like updateDataArray(updatedDataArray)
      // Pass updatedDataArray to this function
    });

    return () => {
      drake.destroy();
    };
  }, [dataArray, e]);


  console.log("DragAndDropTable is rendered with dataArray",dataArray);
  return (
    <>
      <Table
        key={dynamicForm.getGroupKey(e)}
        className="inlineTable"
        bordered
        columns={responseData.columns[e]}
        pagination={false}
        dataSource={dataArray[e]}
        scroll={
          typeof isShowFixedColumn === 'string' && e === isShowFixedColumn
            ? { x: responseData.objectLength[e] > 400 ? responseData.objectLength[e] : 400 }
            : typeof isShowFixedColumn === 'object' && isShowFixedColumn.indexOf(e) >= 0
            ? { x: responseData.objectLength[e] > 400 ? responseData.objectLength[e] : 400 }
            : undefined
        }
        rowClassName={(record) =>
          rowClassName(
            record,
            responseData &&
              responseData.group &&
              responseData.group.hasOwnProperty('length') &&
              responseData.group.length > 0 &&
              responseData.group[0].hasOwnProperty('color_config')
              ? responseData.group[0]['color_config']
              : undefined
          )
        }
        footer={() => <>{sum}{button}</>}
      >
        <tbody ref={tableBodyRef}>
          {dataArray && dataArray[e] && dataArray[e].map((record) => (
            <tr key={record.id}>
              {responseData.columns[e].map((column) => (
                <td key={column.key}>{record[column.key]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default DragAndDropTable;
