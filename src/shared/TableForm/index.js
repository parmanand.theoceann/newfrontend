import React, { Component } from 'react';
import { Button, Card, Table, Form, Input,  Popconfirm, Spin, Alert, Select, InputNumber } from 'antd';
import { EditOutlined, DeleteOutlined,SaveOutlined, CloseOutlined } from '@ant-design/icons';
import URL_WITH_VERSION, { getAPICall, openNotificationWithIcon, URL_WITHOUT_VERSION } from '../../shared';
const FormItem = Form.Item;
const EditableContext = React.createContext();
const Option = Select.Option;

class EditableCell extends React.Component {
  updateData = (value, field) => {
    const { record } = this.props;
    record[field] = value
  }

  getSelectField = () => {
    const { dataIndex, name, f_dyc_extras } = this.props;
    return <Select
      key={dataIndex}
      name={dataIndex}
      showSearch
      style={{ width: "100%" }}
      placeholder={"Select a " + name}
      optionFilterProp="children"
      onChange={(e) => this.updateData(e, dataIndex)}
      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
    >
      {f_dyc_extras && f_dyc_extras.options && f_dyc_extras.options.length > 0 ? (
        f_dyc_extras.options.map(e => {
          return <Option value={e.id.toString()} key={e.id}>{e.name}</Option>
        })
      ) : null}
    </Select>
  }

  getInputField = () => {
    const { inputType, name, dataIndex, className } = this.props;
    return <Input
      key={dataIndex}
      type={inputType ? inputType : "text"}
      name={dataIndex}
      className={className}
      placeholder={"Enter " + name}
      onChange={(e) => this.updateData(e.target.value, dataIndex)}
      onBlur={(e) => this.updateData(e.target.value, dataIndex)}
    />
  }

  getInputNumberField = () => {
    const { inputType, name, dataIndex, className } = this.props;

    return <InputNumber
      key={dataIndex}
      min={0}
      type={inputType ? inputType : "text"}
      name={dataIndex}
      className={className}
      placeholder={"Enter " + name}
      onChange={(e) => this.updateData(e, dataIndex)}
      onBlur={(e) => this.updateData(e, dataIndex)}
    />
  }

  getInput = () => {
    const { inputType } = this.props;
    switch (inputType) {
      case "dropdown": return this.getSelectField()
      case "number": return this.getInputNumberField()
      default: return this.getInputField()
    }
  };

  getFieldName = (restProps) => {
    switch (restProps.f_type) {
      case 'dropdown': {
        let keyIndex = restProps.children[2];
        let optionArr = restProps.f_dyc_extras && restProps.f_dyc_extras.options.length > 0 ? restProps.f_dyc_extras.options : [];

        let index = optionArr.findIndex(item => item.id === keyIndex)
        if (index > -1) {
          return optionArr[index]["name"]
        }
        return null;
      }
      default: return restProps.children
    }
  }

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      f_req,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator, isFieldTouched, getFieldError } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      required: (f_req === 1 ? true : false),
                      message: `Please ${inputType === "dropdown" ? "Select" : "Input"} ${title}!`,
                    }],
                    // initialValue: (record && (record[dataIndex] || record[dataIndex] === 0) ? record[dataIndex] : undefined),
                  })(this.getInput())}
                </FormItem>
              ) : this.getFieldName(restProps)}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

class TableForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      "responseData": { "columns": [], "objectKeys": [], "loading": {}, "dataArray": {}, "editingKey": null },
      editingKey: null,
      editFieldData: null
    }
  }

  componentDidMount = async () => {
    this.setState({ ...this.state, "responseData": { ...this.state.responseData, "loading": true } });
    const response = await getAPICall(`${URL_WITHOUT_VERSION}get/${this.props.frmCode}`);
    const data = await response['data'];
    const editFieldData = (this.props.editFieldData ? Object.assign({}, this.props.editFieldData) : null);
    this.setState({ ...this.state, "responseData": this.createColumns(data), "editFieldData": editFieldData }, () => {
      this.props.frmProp.form.validateFields()
      if (editFieldData) {
        this.getFieldUpdate();
      }
    });
  }

  componentDidUpdate(prevProps) {
    let currentProps = this.props;
    if (prevProps.callbackCall !== currentProps.callbackCall) {
      currentProps.callback(this.state.responseData.dataArray)
    }
    if (prevProps.resetFields !== currentProps.resetFields) {
      this.onResetFields()
    }
    if (prevProps.editFieldData !== currentProps.editFieldData) {
      this.getFieldUpdate();
    }
  }

  getFieldUpdate = () => {
    const dataResp = Object.assign({}, this.state.editFieldData);
    let dataArray = Object.assign({}, this.state.responseData.dataArray);

    Object.keys(dataResp).forEach(e => {
      if (e === "consumptions") {
        Object.keys(dataResp[e]).forEach(key => {
          let data = dataResp[e][key];
          data.sort(function (a, b) {
            return +(a.id < b.id) || +(a.id === b.id) - 1;
          });
          dataArray[key] = { "data": data, "totalRows": data.length }
        })
      }
    });

    this.setState({
      ...this.state,
      responseData: {
        ...this.state.responseData,
        dataArray
      }
    })
  }

  createColumns = (data) => {
    let groups = {},
      objectKeys = [],
      loading = {},
      dataArray = {},
      editingKey = null;

    data.frm.map((e, i) => {
      if (!groups.hasOwnProperty(e.group_name) && e.group_name) {
        groups[e.group_name] = [];
        objectKeys.push(e.group_name);
        loading[e.group_name] = false;
        dataArray[e.group_name] = { "totalRows": 0, "data": [] };
      } else if (!groups.hasOwnProperty(e.group_name) && !e.group_name) {
        groups['g'] = [];
        objectKeys.push('g');
        loading["g"] = false
        dataArray["g"] = { "limit": 0, "data": [] };
      }

      groups[e.group_name].push({
        title: e.name,
        inputType: e.f_type,
        dataIndex: e.f_name,
        key: e.f_name,
        editable: "true",
        ...e
      });
    });
    objectKeys.map(e => {
      groups[e].push({
        title: 'Actions', dataIndex: 'action', fixed: 'right', width: 100,
        render: (text, record) => {
          const editable = this.isEditing(record, e);
          return (
            <div className="editable-row-operations">
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => {
                      return (
                        <a className="iconWrapper save" onClick={() => this.save(form, record.id, e)}>
                       <SaveOutlined />

                        </a>
                      )
                    }}
                  </EditableContext.Consumer>
                  <a className="iconWrapper cancel">
                    <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.id, e)}>
                    <CloseOutlined />
                    </Popconfirm>
                  </a>
                </span>
              ) : (
                <>
                  <a className="iconWrapper edit" onClick={(evt) => this.edit(record.id, e, evt.target.tagName)}>
                  <EditOutlined />

                  </a>
                  <a className="iconWrapper cancel">
                    <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id, e)}>
                    <DeleteOutlined />

                    </Popconfirm>
                  </a>
                </>
              )}
            </div>
          );
        },
      });
    });

    return { "columns": groups, "objectKeys": objectKeys, loading, dataArray, editingKey };
  }

  onUpdateState = (field, key, value) => {
    this.setState({
      ...this.state,
      responseData: {
        ...this.state.responseData,
        [field]: {
          ...this.state.responseData[field],
          [key]: value
        }
      }
    })
  }

  getLastKey = (key) => {
    const { data } = this.checkFieldBoolean("dataArray", key) ? this.state.responseData.dataArray[key] : {};
    return (data.length > 0 ? (data[0]["id"] ? data[0]["id"] : data[0]["rowKey"]) + 1 : 1);
  }

  getAddFieldData = (key, dataKey) => {
    let { columns } = this.state.responseData;
    let column = {}
    column["id"] = dataKey
    column["rowKey"] = dataKey
    column["isAddDelete"] = true
    if (columns[key] && columns[key].length > 0) {
      columns[key].map(v => {
        column[v.dataIndex] = null;
      })
    }

    return column;
  }

  onChangeAddNew = (key) => {
    let dataArray = Object.assign({}, this.state.responseData.dataArray);
    if (this.checkFieldBoolean("dataArray", key)) {
      this.removePrevKey()
      let dataKey = this.getLastKey(key);
      let newData = this.getAddFieldData(key, dataKey);
      dataArray[key]['data'].push(newData)
      dataArray[key]['totalRows'] = dataArray[key]['data'].length

      this.setState({
        ...this.state,
        responseData: {
          ...this.state.responseData,
          dataArray,
          editingKey: {
            [key]: dataKey
          }
        }
      })
    }
  }

  removePrevKey = () => {
    let { editingKey, dataArray } = this.state.responseData;
    if (editingKey) {
      Object.keys(editingKey).map(e => {
        let data = dataArray[e]["data"];
        if (data && data.length > 0) {
          let index = data.findIndex(i => (i.hasOwnProperty("isAddDelete") && i.id === editingKey[e]))
          if (index > -1) this.onRowDeletedClick(editingKey[e], e)
        }
      })
    }
  }

  onResetFields = () => {
    let { dataArray } = this.state.responseData;

    Object.keys(dataArray).map(e => {
      dataArray[e] = { "totalRows": 0, "data": [] };
    })
    this.setState({
      ...this.state,
      responseData: {
        ...this.state.responseData,
        dataArray
      }
    }, () => this.getFieldUpdate())
  }

  onRowDeletedClick = (id, keyCol) => {
    let { dataArray } = this.state.responseData;

    if (this.checkFieldBoolean("dataArray", keyCol)) {
      let entry = dataArray[keyCol]["data"];
      const index = entry.findIndex(item => id === item.id);
      dataArray[keyCol]["data"].splice(index, 1)
      this.onUpdateState("dataArray", keyCol, dataArray[keyCol])
    }
  }

  cancel = (id, keyCol) => {
    let { dataArray } = this.state.responseData;
    if (dataArray && dataArray.hasOwnProperty(keyCol) && dataArray[keyCol]["data"]) {
      let dataSource = dataArray[keyCol];
      let index = dataSource.data.findIndex(e => e.id === id);

      if (index > -1 && dataSource.data[index] && dataSource.data[index].hasOwnProperty("isAddDelete")) {
        dataSource.data.splice(index, 1)
        dataSource["totalRows"] = dataSource.totalRows - 1
      }
    }

    this.setState({
      ...this.state,
      responseData: {
        ...this.state.responseData,
        editingKey: null,
        dataArray
      }
    })
  }

  save = async (form, key, keyCol) => {
    form.validateFields(async (error, row) => {
      if (error) {
        let msg = "<div class='row'>";

        Object.keys(error).map(i => {
          if (error[i].hasOwnProperty("errors") && error[i]["errors"].length > 0) {
            msg += "<div class='col-sm-12'>" + error[i]["errors"][0]["message"] + "</div>"
          }
        });
        openNotificationWithIcon('error', <div dangerouslySetInnerHTML={{ __html: msg }} />)
        return;
      }
      let { dataArray } = Object.assign({}, this.state.responseData);
      if (this.checkFieldBoolean("dataArray", keyCol)) {
        let newEntry = dataArray[keyCol]["data"];
        newEntry.sort(function (a, b) {
          return +((a.id ? a.id : a.rowKey) < (b.id ? b.id : b.rowKey)) || +((a.id ? a.id : a.rowKey) === (b.id ? b.id : b.rowKey)) - 1;
        });
        const index = newEntry.findIndex(item => key === item.id);
        if (index > -1) {
          let postData = newEntry[index];
          if (postData.hasOwnProperty("isAddDelete")) {
            delete postData["id"]
            delete postData["isAddDelete"]
            delete postData["action"]
          }
        }
      }
      this.setState({
        ...this.state,
        responseData: {
          ...this.state.responseData,
          dataArray,
          editingKey: null
        }
      })
    });
  }

  isEditing = (record, key) => {
    return (
      this.state.responseData.editingKey
      && this.state.responseData.editingKey.hasOwnProperty(key))
      ? (record.id === this.state.responseData.editingKey[key])
      : false;
  };

  edit(id, key, tagName) {
    if (tagName === "TD" || tagName === "svg") {
      this.removePrevKey()
      this.setState({
        responseData: {
          ...this.state.responseData,
          editingKey: {
            [key]: id
          }
        }
      });
    }
  }

  checkFieldBoolean = (type, key) => {
    const { responseData } = this.state;
    return (responseData[type] && responseData[type].hasOwnProperty(key) && Object.keys(responseData[type][key]).length > 0 ? true : false)
  }

  render() {
    const { responseData } = this.state;
    const components = {
      body: {
        // row: EditableFormRow,
        cell: EditableCell,
      },
    };

    return (
      <div className="table-group">
        <EditableContext.Provider value={this.props.frmProp.form}>
          {
            responseData.objectKeys.length > 0 ? (
              responseData.objectKeys.map(e => {
                return (
                  <Card title={e !== 'g' ? e : ''} bodyStyle={{ padding: 0 }} key={e}>
                    <Table
                      rowKey={(record, index) => e.toLowerCase().replaceAll(' ', '-').replaceAll('(', '').replaceAll(')', '').replaceAll('/', '') + "-" + index}
                      className={"editableFixedHeader inlineTable " + e}
                      bordered
                      components={components}
                      columns={
                        responseData.columns[e].map((col) => {
                          if (!col.editable) {
                            return col;
                          }
                          return {
                            ...col,
                            onCell: record => ({
                              record,
                              ...col,
                              editing: this.isEditing(record, e)
                            }),
                          };
                        })
                      }
                      locale={{ sortTitle: "rowKey" }}
                      size="small"
                      dataSource={this.checkFieldBoolean("dataArray", e) ? responseData.dataArray[e]["data"] : []}
                      scroll={{ y: 370, x: (responseData.columns[e].length > 8 ? 2000 : 0) }}
                      loading={responseData.loading[e]}
                      pagination={false}
                      // onRow={(record) => ({
                      //   onClick: (evt) => { this.edit(record.id, e, evt.target.tagName) }
                      // })}
                      footer={() =>
                        <div className="text-center">
                          <Button type="link" onClick={() => this.onChangeAddNew(e)}>Add New</Button>
                        </div>
                      }
                    />
                  </Card>
                )
              })) : (
              <Spin tip="Loading...">
                <Alert
                  message=" "
                  description="Please wait..."
                  type="info"
                />
              </Spin>
            )
          }
        </EditableContext.Provider>
      </div>
    )
  }
}

export default TableForm;