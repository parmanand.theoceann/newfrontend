import React, { Component } from 'react';
import { Table, Form, Input, Button } from 'antd';
import { FIELDS } from '../../shared/tableFields';
import {  Popconfirm } from 'antd';
import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
import URL_WITH_VERSION, {
  getAPICall,
  awaitPostAPICall,
  openNotificationWithIcon,
  objectToQueryStringFunc,
  postAPICall,
  apiDeleteCall,
  ResizeableTitle
} from '../../shared';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const columns1 = [
  {
    title: 'Vendor Short',
    dataIndex: 'v_name',
  },

  {
    title: 'Inv Type',
    dataIndex: 'inv_type',
  },

  {
    title: 'Inv Date',
    dataIndex: 'inv_date',
  },

  {
    title: 'Inv No',
    dataIndex: 'inv_no',
  },

  {
    title: 'Status',
    dataIndex: 'status',
  },
  {
    title: 'Receivable/payable',
    dataIndex: 'r_payable',
  },
];
const data = [
  {
    key: '1',
    v_name: 'VRHX',
    i_type: 'PDA',
    i_date: '20/01/2021',
    i_no: '123',
    status: 'Posted',
    r_payable: 'payble',
  },
];

class PortExpenseSummary extends Component {
  components = {
    header: {
      cell: ResizeableTitle,
    },
  };

  constructor(props) {
    super(props);
    const tableAction = {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => {
        return (
          <div className="editable-row-operations">
            <span className="iconWrapper" onClick={(e) => this.redirectToAdd(e, record, true)}>
            <EditOutlined />

            </span>
            <span className="iconWrapper cancel">
              <Popconfirm title="Are you sure, you want to delete it?" onConfirm={() => this.onRowDeletedClick(record.id)}>
              <DeleteOutlined />

              </Popconfirm>
            </span>
          </div>
        )
      }
    };

    let tableHeaders = Object.assign(
      [],
      FIELDS && FIELDS['port_expense_summary'] ? FIELDS['port_expense_summary']['tableheads'] : []
    );

    tableHeaders.push(tableAction)
    this.state = {
      voyID: this.props.voyID,
      loading: false,
      columns: tableHeaders,
      responseData: [],
      pageOptions: { pageIndex: 1, pageLimit: 30, totalRows: 0 },
      isAdd: true,
      isVisible: false,
      sidebarVisible: false,
      formDataValues: {},
      formDataValuesAddress: {},
      addresseditVisible: false,
      responseData: [],
      vessel: '',
      vessel_code: '',
      port: '',
      voyage_manager_id: '',
      filterStatus: false,
      filteredData: []
    };
  }

  getTableData = async () => {
    const { voyID, responseData } = this.state
    if (voyID) {
      const responsePort = await getAPICall(`${URL_WITH_VERSION}/port-call/pelist/${voyID}`);
      const data = await responsePort['data'];
      this.setState({ ...this.state, responseData: data })
    } else {
      const responsePort = await getAPICall(`${URL_WITH_VERSION}/port-call/pelist`);
      const data = await responsePort['data'];
      this.setState({ ...this.state, responseData: data })
    }
  }



  componentDidMount = async () => {
    this.getTableData()
  }

  //resizing function
  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };


  redirectToAdd = async (e, id, boolVal) => {
    const { history } = this.props;
    history.push(`/edit-portcall-details/${id.id}`);
  }


  onRowDeletedClick = (id) => {
    let _url = `${URL_WITH_VERSION}/port-call/pedelete`;
    apiDeleteCall(_url, { id: id }, (response) => {
      if (response && response.data) {
        openNotificationWithIcon('success', response.message);
        // this.getTableData(1);
      } else {
        openNotificationWithIcon('error', response.message);
      }
    });
  };

  handleChange(event, name) {
    this.setState({ [name]: event.target.value });
  }

  clearSearchData = () => this.setState({ ...this.state, voyage_manager_id: '', vessel_code: '', port: '', vessel: '', filteredData: [], filterStatus: false })

  searchData = () => {
    const { voyage_manager_id, vessel, vessel_code, port, responseData } = this.state
    let newArray = []
    responseData.map(value => {
      let code = value.vessel_code;
      let port_name = value.port;
      let voy = value.voyage_manager_id;
      let vessel_name = value.vessel;
      if ((code.toString().toLowerCase() == vessel_code.toLowerCase() && vessel_name.toString().toLowerCase() == vessel.toLowerCase()) ||
        (code.toString().toLowerCase() == vessel_code.toLowerCase() && voy.toString().toLowerCase() == voyage_manager_id.toLowerCase()) ||
        (vessel_name.toString().toLowerCase() == vessel.toLowerCase() && voy.toString().toLowerCase() == voyage_manager_id.toLowerCase())) {
        this.setState({
          filteredData: value,
          filterStatus: true
        })
        // }else if( (code.toString().toLowerCase() == vessel_code.toLowerCase() && port_name.toString().toLowerCase() == port.toLowerCase() && voy.toString().toLowerCase() == voyage_manager_id.toLowerCase()) ||
        //           (code.toString().toLowerCase() == vessel_code.toLowerCase() && port_name.toString().toLowerCase() == port.toLowerCase() && vessel_name.toString().toLowerCase() == vessel.toLowerCase())
        //           (voy.toString().toLowerCase() == voyage_manager_id.toLowerCase() && port_name.toString().toLowerCase() == port.toLowerCase() && vessel_name.toString().toLowerCase() == vessel.toLowerCase())
        //           ){
        //           this.setState({
        //             filteredData : value,
        //             filterStatus : true
        //           })
      } else if (code.toString().toLowerCase() == vessel_code.toLowerCase() && voy.toString().toLowerCase() == voyage_manager_id.toLowerCase() &&
        vessel_name.toString().toLowerCase() == vessel.toLowerCase()) {
        this.setState({
          filteredData: value,
          filterStatus: true
        })
      }
      else if (code.toString().toLowerCase() == vessel_code.toLowerCase() ||
        voy.toString().toLowerCase() == voyage_manager_id.toLowerCase() ||
        vessel_name.toString().toLowerCase() == vessel.toLowerCase()) {
        newArray.push(value)
        this.setState({
          filteredData: newArray,
          filterStatus: true
        })
      }
    })
  }

  render() {
    const { columns, responseData, filterStatus, filteredData } = this.state

    const tableColumns = columns
      .filter(col => (col && col.invisible !== 'true' ? true : false))
      .map((col, index) => ({
        ...col,
        onHeaderCell: column => ({
          width: column.width,
          onResize: this.handleResize(index),
        }),
      }));
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <Form>
                <div className="row p10">
                  <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Vessel">
                      <Input name='vessel' size="default" value={this.state.vessel} onChange={(e) => this.handleChange(e, 'vessel')} />
                    </FormItem>
                  </div>
                  <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Code">
                      <Input name='vessel_code' size="default" value={this.state.vessel_code} onChange={(e) => this.handleChange(e, 'vessel_code')} />
                    </FormItem>
                  </div>
                  <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Voy">
                      <Input name='voyage_manager_id' size="default" value={this.state.voyage_manager_id} onChange={(e) => this.handleChange(e, 'voyage_manager_id')} />
                    </FormItem>
                  </div>
                  {/* <div className="col-md-3">
                    <FormItem {...formItemLayout} label="Port">
                      <Input name='port' size="default" value={this.state.port}  onChange={(e)=>this.handleChange(e, 'port')}/>
                    </FormItem>
                  </div> */}
                </div>

                <div className="row p10">
                  <div className="col-md-12">
                    <div className="action-btn text-right">
                      <Button onClick={() => this.searchData(data)} className="ant-btn ant-btn-primary">Search</Button>
                      <Button onClick={() => this.clearSearchData()} className="ant-btn ant-btn-danger ml-2">Reset</Button>
                    </div>
                  </div>
                </div>
              </Form>
              <div className="row p10">
                <div className="col-md-12">
                  <Table
                    rowKey={record => record.id}
                    bordered
                    // columns={columns} 
                    columns={tableColumns}
                    components={this.components}
                    dataSource={filterStatus === true ? filteredData : responseData}
                    pagination={false} />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PortExpenseSummary;
