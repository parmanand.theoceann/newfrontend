import React, { Component } from "react";
import html2canvas from "html2canvas";
import { Table, Tabs, Input, Row, Col, Form, Spin, Alert, Button } from "antd";
//import ToolbarUI from "components/ToolbarUI";

import ToolbarUI from "../../../../components/ToolbarUI";
import { SaveOutlined } from "@ant-design/icons";
import moment from "moment";
import URL_WITH_VERSION, { openNotificationWithIcon } from "../../..";
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;

const columns = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },

  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "8%",
  },
  {
    title: "Actual",
    dataIndex: "actual",
    key: "actual",
    width: "8%",
  },
  {
    title: "Posted",
    dataIndex: "posted",
    key: "posted",
    width: "8%",
  },
  {
    title: "Cash In",
    dataIndex: "cash_in",
    key: "cash_in",
    width: "8%",
  },
  {
    title: `Diff(Act.-Est.)`,
    dataIndex: "Diff",
    key: "Diff",
    width: "8%",
  },
  {
    title: "%Diff",
    dataIndex: "perDiff",
    key: "perDiff",
    width: "8%",
  },
  {
    title: "Post Vs Cash",
    dataIndex: "sec_variance",
    key: "sec_variance",
    width: "8%",
  },
  {
    title: "% Post Vs Cash",
    dataIndex: "sec_per",
    key: "sec_per",
    width: "8%",
  },
];

const columns2 = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "12%",
    align: "right",
  },
  { title: "", dataIndex: "", key: "blank", width: "68%" },
];

class PL extends Component {
  callback = (evt) => { };

  constructor(props) {
    super(props);
    this.state = {
      dollarUSLocale: Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      formData: this.props.formData || {},
      estimateDatavalue: this.props.estimateData || {},
      showPL: false,
      showEstimatePl: this.props.showEstimatePl,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || ["Actual &  Operation View"],

      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: 0,
          children: [
            { key: "rev00", description: "TCO Hire $", estimate: 0 },
            { key: "rev01", description: "TCO Hire  B. comm", estimate: 0 },
            { key: "rev02", description: 'TCO Hire add com', estimate: 0 },
            { key: "rev03", description: 'TCO BB', estimate: 0 },
            { key: "rev04", description: 'TCO BB Comm', estimate: 0 },
            { key: "rev05", description: 'Net Ballast Bonus', estimate: 0 },
            { key: "rev06", description: "Misc. Revenue", estimate: 0 },
            { key: "rev07", description: 'Total TCO Net Hire', estimate: 0 },
            { key: "rev08", description: 'Total Net TCO BB', estimate: 0 },
            { key: "rev09", description: "Gross Revenue", estimate: 0 },
            { key: "rev10", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses",
          estimate: 0,
          children: [
            {
              key: "ex11",
              description: "Vessel expenses",
              estimate: 0,
              children: [
                { key: "ex111", description: "Hire Expenses", estimate: 0 },
                { key: "ex112", description: "TCI Add. Comm.", estimate: 0 },
                { key: "ex113", description: "TCI Broker Comm.", estimate: 0 },
                { key: "ex114", description: "TCI Ballst Bonus", estimate: 0 },
                { key: "ex115", description: "TCI BB Comm", estimate: 0 },
                { key: "ex116", description: "Reposition Cost", estimate: 0 },
                { key: "ex117", description: "Emission Expense", estimate: 0 }
              ],
            },
            { key: "ex12", description: "Port Expenses", estimate: 0 },
            {
              key: "ex13",
              description: "Bunker Expenses",
              estimate: 0,
              children: [
                {
                  key: "ex131",
                  description: "Sea Consumption Non ECA",
                  estimate: 0,
                  children: [
                    { key: "ex1311", description: "IFO", estimate: 0 },
                    { key: "ex1312", description: "MGO", estimate: 0 },
                    { key: "ex1313", description: "VLSFO", estimate: 0 },
                    { key: "ex1314", description: "LSMGO", estimate: 0 },
                    { key: "ex1315", description: "ULSFO", estimate: 0 },
                  ],
                },
                {
                  key: "ex132",
                  //description: "Port Consumption Non ECA",
                  description: "Port Consumption",
                  estimate: 0,
                  children: [
                    { key: "ex1321", description: "IFO", estimate: 0 },
                    { key: "ex1322", description: "MGO", estimate: 0 },
                    { key: "ex1323", description: "VLSFO", estimate: 0 },
                    { key: "ex1325", description: "LSMGO", estimate: 0 },
                    { key: "ex1324", description: "ULSFO", estimate: 0 },
                  ],
                },

                {
                  key: "ex133",
                  description: "Sea Consumption ECA",
                  estimate: 0,
                  children: [
                    { key: "ex1331", description: "IFO", estimate: 0 },
                    { key: "ex1332", description: "MGO", estimate: 0 },
                    { key: "ex1333", description: "VLSFO", estimate: 0 },
                    { key: "ex1334", description: "LSMGO", estimate: 0 },
                    { key: "ex1335", description: "ULSFO", estimate: 0 },
                  ],
                },
                // {
                //   key: "ex134",
                //   description: "Port Consumption ECA",
                //   estimate: 0,
                //   children: [
                //     { key: "ex1341", description: "IFO", estimate: 0 },
                //     { key: "ex1342", description: "MGO", estimate: 0 },
                //     { key: "ex1343", description: "VLSFO", estimate: 0 },
                //     { key: "ex1345", description: "LSMGO", estimate: 0 },
                //     { key: "ex1344", description: "ULSFO", estimate: 0 },
                //   ],
                // },
              ],
            },
            { key: "ex17", description: "Other Expenses", estimate: 0 },
            { key: "ex18", description: "Gross Expenses", estimate: 0 },
            { key: "ex19", description: "Net Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr20", description: "Profit (Loss)", estimate: 0 },
            { key: "vr21", description: "Daily Profit (Loss)", estimate: 0 },
            { key: "vr22", description: "CO2 Cost", estimate: 0 },
            { key: "vr23", description: "CO2 Adjusted Profit (Loss)", estimate: 0 },
            { key: "vr24", description: "CO2 Adjusted (Net) TCE", estimate: 0 },
            { key: "vr25", description: "Total Sea Days", estimate: 0 },
            { key: "vr26", description: "Total Port Days", estimate: 0 },
            { key: "vr27", description: "Net Voyage Days", estimate: 0 },

          ],
        },
      ],
    };

    this.elementRef = React.createRef();
    this.loading = false;
    this.saveLoading = false;
  }

  findCpPassage = (fuelType, passageType, consArr) => {
    let cp_price = 0;
    let fuel_cons = 0;
    consArr?.map((el) => {
      const { fuel_type, fuel_code } = el;
      if (fuel_type == fuelType) {
        cp_price = parseFloat(el.cp_price);
        if (passageType == 1) {
          fuel_cons = isNaN(el?.ballast_value)
            ? 0
            : parseFloat(el.ballast_value);
        } else if (passageType == 2) {
          fuel_cons = isNaN(el?.laden_value) ? 0 : parseFloat(el.laden_value);
        }
      }
    });

    cp_price = isNaN(cp_price) ? 0 : parseFloat(cp_price);
    fuel_cons = isNaN(fuel_cons) ? 0 : parseFloat(fuel_cons);
    return { cp_price, fuel_cons };
  };
  EcaSeaconsCalculation = (formdata) => {
    let ecaSeaCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    let fuelType = formdata?.["eca_fuel_grade"] ?? 7;
    let ecafuelConsPrice = 0;

    formdata?.portitinerary?.map((el) => {
      const { eca_days, passage } = el;
      const { cp_price, fuel_cons } = this.findCpPassage(
        fuelType,
        passage,
        formdata["."]
      );

      ecafuelConsPrice += cp_price * eca_days * fuel_cons;
    });

    switch (fuelType) {
      case "3": // IFO
        ecaSeaCons["ifo"] = ecafuelConsPrice;
        break;

      case "4": // MGO
        ecaSeaCons["mgo"] = ecafuelConsPrice;
        break;

      case "5": //Vlsfo
        ecaSeaCons["vlsfo"] = ecafuelConsPrice;
        break;

      case "7": // lsmgo
        ecaSeaCons["lsmgo"] = ecafuelConsPrice;
        break;

      case "10": // ulsfo
        ecaSeaCons["ulsfo"] = ecafuelConsPrice;
        break;
      case "11": // HFO
        ecaSeaCons["hfo"] = ecafuelConsPrice;
        break;
    }

    return ecaSeaCons;
  };

  nonEcaSeaconsCalculation = (formdata) => {
    let nonEcaSeaCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    let fuelType = formdata?.["eca_fuel_grade"];

    formdata?.portitinerary?.map((el) => {
      const { eca_days, passage, tsd } = el;
      let nonEcadays = tsd;

      switch (fuelType) {
        case "3":
          nonEcadays = tsd - eca_days;
          break;

        case "4":
          nonEcadays = tsd - eca_days;
          break;

        case "7":
          nonEcadays = tsd - eca_days;
          break;

        case "5":
          nonEcadays = tsd - eca_days;
          break;

        case "10":
          nonEcadays = tsd - eca_days;
          break;

        default:
          return nonEcadays;
      }

      let nonifo = this.findCpPassage(3, passage, formdata["."]);
      let nonmgo = this.findCpPassage(4, passage, formdata["."]);
      let nonlsmgo = this.findCpPassage(7, passage, formdata["."]);
      let nonvlsfo = this.findCpPassage(5, passage, formdata["."]);
      let nonulsfo = this.findCpPassage(10, passage, formdata["."]);

      if (fuelType == 3) {
        nonEcaSeaCons["ifo"] += nonifo.cp_price * nonEcadays * nonifo.fuel_cons;
      } else {
        nonEcaSeaCons["ifo"] += nonifo.cp_price * tsd * nonifo.fuel_cons;
      }

      if (fuelType == 4) {
        nonEcaSeaCons["mgo"] += nonmgo.cp_price * nonEcadays * nonmgo.fuel_cons;
      } else {
        nonEcaSeaCons["mgo"] += nonmgo.cp_price * tsd * nonmgo.fuel_cons;
      }

      if (fuelType == 7) {
        nonEcaSeaCons["lsmgo"] +=
          nonlsmgo?.cp_price * nonEcadays * nonlsmgo?.fuel_cons;
      } else {
        nonEcaSeaCons["lsmgo"] +=
          nonlsmgo?.cp_price * tsd * nonlsmgo?.fuel_cons;
      }

      if (fuelType == 5) {
        nonEcaSeaCons["vlsfo"] +=
          nonvlsfo?.cp_price * nonEcadays * nonvlsfo?.fuel_cons;
      } else {
        nonEcaSeaCons["vlsfo"] +=
          nonvlsfo?.cp_price * tsd * nonvlsfo?.fuel_cons;
      }

      if (fuelType == 10) {
        nonEcaSeaCons["ulsfo"] +=
          nonulsfo?.cp_price * nonEcadays * nonulsfo?.fuel_cons;
      } else {
        nonEcaSeaCons["ulsfo"] +=
          nonulsfo?.cp_price * tsd * nonulsfo?.fuel_cons;
      }
    });

    return nonEcaSeaCons;
  };

  nonEcaPortConsCalculation = (formdata) => {
    let nonEcaPortCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    formdata?.bunkerdetails?.map((el) => {
      nonEcaPortCons["ifo"] += isNaN(el["pc_ifo"])
        ? 0
        : parseFloat(el["pc_ifo"]);
      nonEcaPortCons["mgo"] += isNaN(el["pc_mgo"])
        ? 0
        : parseFloat(el["pc_mgo"]);
      nonEcaPortCons["lsmgo"] += isNaN(el["pc_lsmgo"])
        ? 0
        : parseFloat(el["pc_lsmgo"]);
      nonEcaPortCons["vlsfo"] += isNaN(el["pc_vlsfo"])
        ? 0
        : parseFloat(el["pc_vlsfo"]);
      nonEcaPortCons["ulsfo"] += isNaN(el["pc_ulsfo"])
        ? 0
        : parseFloat(el["pc_ulsfo"]);
    });
    return nonEcaPortCons;
  };

  totalEcaSecafuelCons = (consObj) => {
    let fuelValue = Object.values(consObj);
    return fuelValue.reduce((ac, el) => ac + el, 0);
  };

  ecaPortConsCalculation = () => {
    let ecaPortCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };
    // till now we dont have any update on this, how to calculate.
    return ecaPortCons;
  };



  __getEstimatePL = (calData, colName) => {
    const { dollarUSLocale } = this.state;
    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      totalSeaConsumptionEca = 0,
      totalPortConsumptionEca = 0,
      vesselExpenses = 0;
    let ballast_bonus = 0;
    let _estimateData = Object.assign([], this.state.estimateData);

    Object.keys(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]).map((e) => (totalSeaConsumption += ("" + calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"][e]).replaceAll(",", "") * 1));

    Object.keys(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]).map((e) => (totalPortConsumption += ("" + calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"][e]).replaceAll(",", "") * 1));
    Object.keys(calData["estimate"]["expenses"]["vessel_expenses"]).map((e) => (vesselExpenses += ("" + calData["estimate"]["expenses"]["vessel_expenses"][e]).replaceAll(",", "") * 1));

    //----
    Object.keys(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]).map((e) => (totalSeaConsumptionEca += ("" + calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"][e]).replaceAll(",", "") * 1));
    Object.keys(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]).map((e) => (totalPortConsumptionEca += ("" + calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"][e]).replaceAll(",", "") * 1));
    //----

    _estimateData[0][colName] = calData["estimate"]["revenue"]["net_rev"];
    _estimateData[0]["children"][0][colName] = calData["estimate"]["revenue"]["tco_hire"];
    _estimateData[0]["children"][1][colName] = calData["estimate"]["revenue"]["tco_hire_br_comm"];
    _estimateData[0]["children"][2][colName] = calData["estimate"]["revenue"]["tco_hire_add_comm"];
    _estimateData[0]["children"][3][colName] = calData["estimate"]["revenue"]["tco_bb"];
    _estimateData[0]["children"][4][colName] = calData["estimate"]["revenue"]["tco_b_comm"];//TCO BB Comm
    _estimateData[0]["children"][5][colName] = calData["estimate"]["revenue"]["tco_net_blast_revenue"];//Net Ballast Bonus
    _estimateData[0]["children"][6][colName] = calData["estimate"]["revenue"]["misc_rev"];
    _estimateData[0]["children"][7][colName] = calData["estimate"]["revenue"]["tco_hire"];//Total TCO Net Hire
    _estimateData[0]["children"][8][colName] = calData["estimate"]["revenue"]["tco_bb"];//Total Net TCO BB
    _estimateData[0]["children"][9][colName] = calData["estimate"]["revenue"]["gross_rev"];
    _estimateData[0]["children"][10][colName] = calData["estimate"]["revenue"]["net_rev"];

    _estimateData[1][colName] = calData["estimate"]["expenses"]["netExpenses"];
    let hire_expenses = calData["estimate"]["expenses"]["vessel_expenses"]["hire_expenses"] ? calData["estimate"]["expenses"]["vessel_expenses"]["hire_expenses"].replaceAll(",", "") * 1 : 0;

    let totalvesselExpenses = hire_expenses + ballast_bonus;
    _estimateData[1]["children"][0][colName] = dollarUSLocale.format(totalvesselExpenses);
    _estimateData[1]["children"][0]["children"][0][colName] = calData["estimate"]["expenses"]["vessel_expenses"]["hire_expenses"];
    _estimateData[1]["children"][0]["children"][1][colName] = calData["estimate"]["expenses"]["tci_add_comm"];
    _estimateData[1]["children"][0]["children"][2][colName] = calData["estimate"]["expenses"]["tci_broker_comm"];
    _estimateData[1]["children"][0]["children"][3][colName] = calData["estimate"]["expenses"]["vessel_expenses"]["ballast_bonus"];
    _estimateData[1]["children"][0]["children"][4][colName] = calData["estimate"]["expenses"]["vessel_expenses"]["tci_bb_comm"];
    _estimateData[1]["children"][0]["children"][5][colName] = calData["estimate"]["expenses"]["vessel_expenses"]["reposition_cost"];
    _estimateData[1]["children"][0]["children"][6][colName] = calData["estimate"]["expenses"]["vessel_expenses"]["emission_expense"];

    _estimateData[1]["children"][1][colName] = calData["estimate"]["expenses"]["port_expenses"];

    _estimateData[1]["children"][2][colName] = dollarUSLocale.format(totalPortConsumption + totalSeaConsumption + totalPortConsumptionEca + totalSeaConsumptionEca);
    _estimateData[1]["children"][2]["children"][0][colName] = dollarUSLocale.format(totalSeaConsumption);

    _estimateData[1]["children"][2]["children"][0]["children"][0][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]["ifo"]);
    _estimateData[1]["children"][2]["children"][0]["children"][1][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]["mgo"]);
    _estimateData[1]["children"][2]["children"][0]["children"][2][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]["vlsfo"]);
    _estimateData[1]["children"][2]["children"][0]["children"][3][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]["lsmgo"]);
    _estimateData[1]["children"][2]["children"][0]["children"][4][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_sea_cons"]["ulsfo"]);

    _estimateData[1]["children"][2]["children"][1][colName] = dollarUSLocale.format(totalPortConsumption);
    _estimateData[1]["children"][2]["children"][1]["children"][0][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]["ifo"]);
    _estimateData[1]["children"][2]["children"][1]["children"][1][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]["mgo"]);
    _estimateData[1]["children"][2]["children"][1]["children"][2][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]["vlsfo"]);
    _estimateData[1]["children"][2]["children"][1]["children"][3][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]["lsmgo"]);
    _estimateData[1]["children"][2]["children"][1]["children"][4][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_noneca_port_cons"]["ulsfo"]);


    //---------------------------------

    _estimateData[1]["children"][2]["children"][2][colName] = dollarUSLocale.format(totalSeaConsumptionEca);
    _estimateData[1]["children"][2]["children"][2]["children"][0][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]["ifo"]);
    _estimateData[1]["children"][2]["children"][2]["children"][1][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]["mgo"]);
    _estimateData[1]["children"][2]["children"][2]["children"][2][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]["vlsfo"]);
    _estimateData[1]["children"][2]["children"][2]["children"][3][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]["lsmgo"]);
    _estimateData[1]["children"][2]["children"][2]["children"][4][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_sea_cons"]["ulsfo"]);

    // _estimateData[1]["children"][2]["children"][3][colName] = dollarUSLocale.format(totalPortConsumptionEca);
    // _estimateData[1]["children"][2]["children"][3]["children"][0][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]["ifo"]);
    // _estimateData[1]["children"][2]["children"][3]["children"][1][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]["mgo"]);
    // _estimateData[1]["children"][2]["children"][3]["children"][2][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]["vlsfo"]);
    // _estimateData[1]["children"][2]["children"][3]["children"][3][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]["lsmgo"]);
    // _estimateData[1]["children"][2]["children"][3]["children"][4][colName] = dollarUSLocale.format(calData["estimate"]["expenses"]["bunker_expenses"]["total_eca_port_cons"]["ulsfo"]);
    //-----------------------------------
    _estimateData[1]["children"][3][colName] = calData["estimate"]["expenses"]["mis_expenses"];
    _estimateData[1]["children"][4][colName] = calData["estimate"]["expenses"]["gross_expenses"];
    _estimateData[1]["children"][5][colName] = calData["estimate"]["expenses"]["netExpenses"];

    _estimateData[2][colName] = calData["estimate"]["voyage_result"]["profit_loss"].replace(/[&\/\\#,+()$~%'":*?<>{}]/g, "");
    _estimateData[2]["children"][0][colName] = calData["estimate"]["voyage_result"]["profit_loss"].replace(/[&\/\\#,+()$~%'":*?<>{}]/g, "");
    _estimateData[2]["children"][1][colName] = calData["estimate"]["voyage_result"]["daily_profit_loss"];
    _estimateData[2]["children"][2][colName] = calData["estimate"]["voyage_result"]["co2expense"];
    _estimateData[2]["children"][3][colName] = calData["estimate"]["voyage_result"]["CO2_adj_profit"];
    _estimateData[2]["children"][4][colName] = calData["estimate"]["voyage_result"]["co2_adjusted_tce"];
    _estimateData[2]["children"][5][colName] = calData["estimate"]["voyage_result"]["total_sea_days"];
    _estimateData[2]["children"][6][colName] = calData["estimate"]["voyage_result"]["total_port_days"];
    _estimateData[2]["children"][7][colName] = calData["estimate"]["voyage_result"]["net_voyage_days"];

    return _estimateData;
  };

  __pl = () => {
    let { estimateData, dollarUSLocale, formData, estimateDatavalue, showEstimatePl } = this.state;
    let netExpenses = 0;
    let grossExpenses = 0;
    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      totalArriveConsumption = 0,
      totalDepConsumption = 0,
      totalAdjConsumption = 0;
    let colName = "actual";
    if (showEstimatePl == true) {
      estimateData = this.__getEstimatePL(estimateDatavalue, "estimate");
    }

    let totalVoyageDays = formData["total_days"] ? (isNaN(("" + formData["total_days"]).replaceAll(",", "") * 1) ? 0 : ("" + formData["total_days"]).replaceAll(",", "") * 1) : 0;
    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      grossRevenue = 0,
      netRevenue = 0,
      demmurage = 0,
      MiscRevenue = 0,
      lumpsum = 0,
      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      totalExpenses = 0,
      tci_bb = 0,
      cpQty = 0;

    let posted = {};
    let cash = {};

    let ecaSeacons = this.EcaSeaconsCalculation(formData);
    let nonEcaSeacons = this.nonEcaSeaconsCalculation(formData);
    let nonecaPortcons = this.nonEcaPortConsCalculation(formData);
    let ecaPortCons = this.ecaPortConsCalculation(formData);
    let totalecaSeacons = this.totalEcaSecafuelCons(ecaSeacons);

    let totalnonEcaSeacons = this.totalEcaSecafuelCons(nonEcaSeacons);
    let totalnonecaPortcons = this.totalEcaSecafuelCons(nonecaPortcons);
    let totalecaPortCons = this.totalEcaSecafuelCons(ecaPortCons);


    let bunkerExpense = totalecaSeacons + totalnonEcaSeacons + totalnonecaPortcons + totalecaPortCons;

    let {
      tcohireExpenses_posted,
      tcoHireBComm_posted,
      tcoHireAddComm_posted,
      tcoBB_posted,
      miscRev_posted,
      vesselExpenses_posted,
      hireExpenses_posted,
      tciaddcom_posted,
      tcibcom_posted,
      portExpenses_posted,
      bunkerExpenses_posted,
      miscExpenses_posted,
      grossExpenses_posted,
      netExpenses_posted,

    } = estimateDatavalue && estimateDatavalue.post

    let tcoBB_com_posted = 0; let tcoBB_com_cash = 0;
    let tcoBB_net_posted = 0; let tcoBB_net_cash = 0;
    let tcoBB_ttl_tco_net_posted = 0; let tcoBB_ttl_tco_net_cash = 0;
    let tcoBB_ttl_net_posted = 0; let tcoBB_ttl_net_cash = 0;

    let { tcohireExpenses_cash,
      tcoHireBComm_cash,
      tcoHireAddComm_cash,
      tcoBB_cash,
      miscRev_cash,
      vesselExpenses_cash,
      hireExpenses_cash,
      tciaddcom_cash,
      tcibcom_cash,
      portExpenses_cash,
      bunkerExpenses_cash,
      miscExpenses_cash,
      grossExpenses_cash,
      netExpenses_cash } = estimateDatavalue && estimateDatavalue.cash


    let gross_revenue_posted = 0,
      gross_revenue_cash = 0,
      net_revenue_posted = 0,
      net_revenue_cash = 0;
    let gross_exp_posted = 0,
      gross_exp_cash = 0,
      net_exp_posted = 0,
      net_exp_cash = 0;




    const postVScash = (p, c) => {
      let diff = 0;
      if (p && c) {
        diff = p * 1 - c * 1
      } else if (p && !c) {
        diff = p * 1
      } else if (!p && c) {
        diff = c * -1
      } else {
        diff = 0;
      }
      return diff.toFixed(2);
    }


    const perPostVScash = (p, c) => {
      let perdiff = 0;
      if (p && c) {
        perdiff = (((p * 1 - c * 1) / p * 1) * 100)
      } else if (p && !c) {
        perdiff = 100
      }
      else if (!p && c) {
        perdiff = 0;
      }
      return perdiff.toFixed(2);
    }

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portItinerary = formData["portitinerary"];
      portItinerary.map((e) => {
        tsd += (e.tsd + "").replaceAll(",", "") * 1;
        tpd += (e.t_port_days + "").replaceAll(",", "") * 1;
      });

      tsd = tsd * 1;
      tpd = tpd * 1;
    }
    totalVoyageDays = ((tpd + tsd) > 0 ? (tpd + tsd) : totalVoyageDays);
    // Revenue vm tcto---------------------------------------------

    let tco_bb_com = 0;
    let net_blast_bonus = 0;
    let ttl_tco_net_hire = 0;
    let ttl_net_tco_bb = 0;

    let otherCost = formData["other_cost"] ? (isNaN(("" + formData["other_cost"]).replaceAll(",", "") * 1) ? 0 : ("" + formData["other_cost"]).replaceAll(",", "") * 1) : 0;
    let hire = formData["tci_d_hire"] ? (formData["tci_d_hire"] + "").replaceAll(",", "") * 1 : 0;
    let addPercentage = formData["add_percentage"] ? (formData["add_percentage"] + "").replaceAll(",", "") * 1 : 0;
    let amt_add_percentage = hire * totalVoyageDays * addPercentage * 0.01;
    let broPercentage = formData["bro_percentage"] ? (formData["bro_percentage"] + "").replaceAll(",", "") * 1 : 0;

    let amt_bro_percentage = hire * totalVoyageDays * broPercentage * 0.01;

    //MiscRevenue = formData["mis_cost"] ? (formData["mis_cost"] + "").replaceAll(",", "") * 1 : 0;

    let tco_hire = 0;
    let tco_bb = 0;
    let dailyhire = 0;
    let duration = 0;
    if (formData?.tcoterm && formData.tcoterm.length > 0) {
      for (let i = 0; i < formData.tcoterm.length; i++) {
        if (formData.tcoterm[i]?.dailyhire) {
          dailyhire += parseFloat(formData.tcoterm[i].dailyhire.toString().replaceAll(',', ''));
        }
        if (formData.tcoterm[i]?.duration) {
          duration += parseFloat(formData.tcoterm[i].duration.toString().replaceAll(',', ''));
        }

        if (formData.tcoterm[i]?.bb) {
          tco_bb += parseFloat(formData.tcoterm[i].bb.toString().replaceAll(',', ''));
        }
      }
    }

    tco_hire = dailyhire * duration;

    // Expenses vm tcto-----------------------------------------------------

    let portExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      seaExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivalrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      deprev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivaladjuestmentrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 };
    tci_bb = formData["bb"] ? (isNaN(("" + formData["bb"]).replaceAll(",", "") * 1) ? 0 : ("" + formData["bb"]).replaceAll(",", "") * 1) : 0;
    if (formData && formData.hasOwnProperty("bunkerdetails")) {
      let bunkerDetails = formData["bunkerdetails"];
      let i = 0;
      bunkerDetails.map((e, i, { length }) => {
        seaExpenses["ifo"] += e["ifo"] ? (isNaN(("" + e["ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ifo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["mgo"] += e["mgo"] ? (isNaN(("" + e["mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["mgo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["vlsfo"] += e["vlsfo"] ? (isNaN(("" + e["vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["vlsfo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["lsmgo"] += e["lsmgo"] ? (isNaN(("" + e["lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["lsmgo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["ulsfo"] += e["ulsfo"] ? (isNaN(("" + e["ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ulsfo"]).replaceAll(",", "") * 1) : 0;

        portExpenses["ifo"] += e["pc_ifo"] ? (isNaN(("" + e["pc_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ifo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["mgo"] += e["pc_mgo"] ? (isNaN(("" + e["pc_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_mgo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["vlsfo"] += e["pc_vlsfo"] ? (isNaN(("" + e["pc_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_vlsfo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["lsmgo"] += e["pc_lsmgo"] ? (isNaN(("" + e["pc_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_lsmgo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["ulsfo"] += e["pc_ulsfo"] ? (isNaN(("" + e["pc_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ulsfo"]).replaceAll(",", "") * 1) : 0;

        if (i == 0) {
          arrivalrev["ifo"] += e["arob_ifo"] ? (isNaN(("" + e["arob_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_ifo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["mgo"] += e["arob_mgo"] ? (isNaN(("" + e["arob_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_mgo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["vlsfo"] += e["arob_vlsfo"] ? (isNaN(("" + e["arob_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_vlsfo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["lsmgo"] += e["arob_lsmgo"] ? (isNaN(("" + e["arob_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_lsmgo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["ulsfo"] += e["arob_ulsfo"] ? (isNaN(("" + e["arob_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_ulsfo"]).replaceAll(",", "") * 1) : 0;
        }

        //last element
        if (i + 1 === length) {
          deprev["ifo"] += e["dr_ifo"] ? (isNaN(("" + e["dr_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_ifo"]).replaceAll(",", "") * 1) : 0;
          deprev["mgo"] += e["dr_mgo"] ? (isNaN(("" + e["dr_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_mgo"]).replaceAll(",", "") * 1) : 0;
          deprev["vlsfo"] += e["dr_vlsfo"] ? (isNaN(("" + e["dr_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_vlsfo"]).replaceAll(",", "") * 1) : 0;
          deprev["lsmgo"] += e["dr_lsmgo"] ? (isNaN(("" + e["dr_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_lsmgo"]).replaceAll(",", "") * 1) : 0;
          deprev["ulsfo"] += e["dr_ulsfo"] ? (isNaN(("" + e["dr_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_ulsfo"]).replaceAll(",", "") * 1) : 0;
        }
      });
    }

    if (formData && formData.hasOwnProperty(".")) {
      let cpData = formData["."];
      let price_cp_eco = formData.hasOwnProperty("price_cp_eco") && formData["price_cp_eco"] ? 1 : 0; // 1 is P$ else CP$
      cpData.map((e) => {
        let _price = 0;
        let _pprice = 0;
        if (e["cp_price"] && !isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)) {
          _price = ("" + e["cp_price"]).replaceAll(",", "") * 1;
        }
        if (e["purchase_price"] && !isNaN(("" + e["purchase_price"]).replaceAll(",", "") * 1)) {
          _pprice = ("" + e["purchase_price"]).replaceAll(",", "") * 1;
        }

        switch (e.fuel_code) {
          case "IFO":
            seaExpenses["ifo"] = price_cp_eco == 1 ? seaExpenses["ifo"] * _pprice : seaExpenses["ifo"] * _price;
            portExpenses["ifo"] = price_cp_eco == 1 ? portExpenses["ifo"] * _pprice : portExpenses["ifo"] * _price;
            arrivalrev["ifo"] = arrivalrev["ifo"] * _price;
            deprev["ifo"] = deprev["ifo"] * _pprice;
            arrivaladjuestmentrev["ifo"] = deprev["ifo"] - arrivalrev["ifo"];
            break;

          case "MGO":
            seaExpenses["mgo"] = price_cp_eco == 1 ? seaExpenses["mgo"] * _pprice : seaExpenses["mgo"] * _price;
            portExpenses["mgo"] = price_cp_eco == 1 ? portExpenses["mgo"] * _pprice : portExpenses["mgo"] * _price;
            arrivalrev["mgo"] = arrivalrev["mgo"] * _price;
            deprev["mgo"] = deprev["mgo"] * _pprice;
            arrivaladjuestmentrev["mgo"] = deprev["mgo"] - arrivalrev["mgo"];
            break;

          case "VLSFO":
            seaExpenses["vlsfo"] = price_cp_eco == 1 ? seaExpenses["vlsfo"] * _pprice : seaExpenses["vlsfo"] * _price;
            portExpenses["vlsfo"] = price_cp_eco == 1 ? portExpenses["vlsfo"] * _pprice : portExpenses["vlsfo"] * _price;
            arrivalrev["vlsfo"] = arrivalrev["vlsfo"] * _price;
            deprev["vlsfo"] = deprev["vlsfo"] * _pprice;
            arrivaladjuestmentrev["vlsfo"] = deprev["vlsfo"] - arrivalrev["vlsfo"];
            break;

          case "LSMGO":
            seaExpenses["lsmgo"] = price_cp_eco == 1 ? seaExpenses["lsmgo"] * _pprice : seaExpenses["lsmgo"] * _price;
            portExpenses["lsmgo"] = price_cp_eco == 1 ? portExpenses["lsmgo"] * _pprice : portExpenses["lsmgo"] * _price;
            arrivalrev["lsmgo"] = arrivalrev["lsmgo"] * _price;
            deprev["lsmgo"] = deprev["lsmgo"] * _pprice;
            arrivaladjuestmentrev["lsmgo"] = deprev["lsmgo"] - arrivalrev["lsmgo"];
            break;

          case "ULSFO":
            seaExpenses["ulsfo"] = price_cp_eco == 1 ? (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _pprice : (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _price;
            portExpenses["ulsfo"] = price_cp_eco == 1 ? (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price : (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price;
            arrivalrev["ulsfo"] = (isNaN(arrivalrev["ulsfo"]) ? 0 : arrivalrev["ulsfo"]) * _price;
            deprev["ulsfo"] = (isNaN(deprev["ulsfo"]) ? 0 : deprev["ulsfo"]) * _pprice;
            arrivaladjuestmentrev["ulsfo"] = deprev["ulsfo"] - arrivalrev["ulsfo"];
            break;
        }
      });
    }

    Object.keys(seaExpenses).map(
      (e) => (totalSeaConsumption += seaExpenses[e])
    );
    Object.keys(portExpenses).map(
      (e) => (totalPortConsumption += portExpenses[e])
    );
    Object.keys(arrivalrev).map(
      (e) => (totalArriveConsumption += arrivalrev[e])
    );
    Object.keys(deprev).map((e) => (totalDepConsumption += deprev[e]));
    Object.keys(arrivaladjuestmentrev).map(
      (e) => (totalAdjConsumption += arrivaladjuestmentrev[e])
    );

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portitinerary = formData["portitinerary"];
      portitinerary.map((e) => (pi += isNaN(("" + e.p_exp).replaceAll(",", "") * 1) ? 0 : ("" + e.p_exp).replaceAll(",", "") * 1));
    }


    let bro_percentage_tco = 0, tco_add_percentage = 0;

    if (formData?.tcoterm && formData.tcoterm.length > 0) {
      for (let i = 0; i < formData.tcoterm.length; i++) {
        if (formData.tcoterm[i]?.acom) {
          tco_add_percentage += parseFloat(formData.tcoterm[i].acom.toString().replaceAll(',', ''));
        }

        if (formData.tcoterm[i]?.bcom) {
          bro_percentage_tco += parseFloat(formData.tcoterm[i].bcom.toString().replaceAll(',', ''));
        }
      }
    }

    tco_bb_com = (tco_bb * tco_add_percentage * 0.01) + (tco_bb * bro_percentage_tco * 0.01)

    bro_percentage_tco = tco_hire * bro_percentage_tco * 0.01;
    tco_add_percentage = tco_hire * tco_add_percentage * 0.01;

    grossRevenue = tco_hire + tco_bb + MiscRevenue;

    netRevenue = grossRevenue - (bro_percentage_tco + tco_add_percentage);


    let co2cost = 0;
    if (formData.hasOwnProperty("-----")) {
      co2cost = formData["-----"]?.ttl_co2_cost || 0;
    }

    grossExpenses = hire * totalVoyageDays +
      // + amt_add_percentage + amt_bro_percentage 
      + totalSeaConsumption + totalPortConsumption + pi + otherCost;

    netExpenses = grossExpenses - (amt_add_percentage + amt_bro_percentage);

    estimateData[0][colName] = dollarUSLocale.format(netRevenue.toFixed(2));
    let reves = estimateData[0]["estimate"] ? estimateData[0]["estimate"].replaceAll(",", "") * 1 : 0;
    let revac = estimateData[0]["actual"] ? estimateData[0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["Diff"] = (revac - reves).toFixed(2);
    estimateData[0]["perDiff"] = reves == 0 ? "0.00" : (((revac - reves) / reves) * 100).toFixed(2);
    estimateData[0]["posted"] = net_revenue_posted ? net_revenue_posted : '0.00';
    estimateData[0]["cash_in"] = net_revenue_cash ? net_revenue_cash : '0.00';
    estimateData[0]["sec_variance"] = postVScash(net_revenue_posted, net_revenue_cash);
    estimateData[0]["sec_per"] = perPostVScash(net_revenue_posted, net_revenue_cash);

    estimateData[0]["children"][0][colName] = dollarUSLocale.format(tco_hire);
    let tcohirees = estimateData[0]["children"][0]["estimate"] ? estimateData[0]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let tcohireac = estimateData[0]["children"][0]["actual"] ? estimateData[0]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][0]["Diff"] = (tcohireac - tcohirees).toFixed(2);
    estimateData[0]["children"][0]["perDiff"] = tcohirees == 0 ? "0.00" : (((tcohireac - tcohirees) / tcohirees) * 100).toFixed(2);
    estimateData[0]["children"][0]["posted"] = tcohireExpenses_posted ? tcohireExpenses_posted : '0.00';
    estimateData[0]["children"][0]["cash_in"] = tcohireExpenses_cash ? tcohireExpenses_cash : '0.00';
    estimateData[0]["children"][0]["sec_variance"] = postVScash(tcohireExpenses_posted, tcohireExpenses_cash);
    estimateData[0]["children"][0]["sec_per"] = perPostVScash(tcohireExpenses_posted, tcohireExpenses_cash);

    estimateData[0]["children"][1][colName] = dollarUSLocale.format(bro_percentage_tco.toFixed(2));
    let tcobroperes = estimateData[0]["children"][1]["estimate"] ? estimateData[0]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let tcobroperac = estimateData[0]["children"][1]["actual"] ? estimateData[0]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][1]["Diff"] = (tcobroperac - tcobroperes).toFixed(2);
    estimateData[0]["children"][1]["perDiff"] = tcobroperes == 0 ? "0.00" : (((tcobroperac - tcobroperes) / tcobroperes) * 100).toFixed(2);
    estimateData[0]["children"][1]["posted"] = tcoHireBComm_posted ? tcoHireBComm_posted : '0.00'
    estimateData[0]["children"][1]["cash_in"] = tcoHireBComm_cash ? tcoHireBComm_cash : '0.00'
    estimateData[0]["children"][1]["sec_variance"] = postVScash(tcoHireBComm_posted, tcoHireBComm_cash);
    estimateData[0]["children"][1]["sec_per"] = perPostVScash(tcoHireBComm_posted, tcoHireBComm_cash);

    estimateData[0]["children"][2][colName] = dollarUSLocale.format(tco_add_percentage.toFixed(2));
    let tcoaddperes = estimateData[0]["children"][2]["estimate"] ? estimateData[0]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let tcoaddperac = estimateData[0]["children"][2]["actual"] ? estimateData[0]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][2]["Diff"] = (tcoaddperac - tcoaddperes).toFixed(2);
    estimateData[0]["children"][2]["perDiff"] = tcoaddperes == 0 ? "0.00" : (((tcoaddperac - tcoaddperes) / tcoaddperes) * 100).toFixed(2);
    estimateData[0]["children"][2]["posted"] = tcoHireAddComm_posted ? tcoHireAddComm_posted : '0.00'
    estimateData[0]["children"][2]["cash_in"] = tcoHireAddComm_cash ? tcoHireAddComm_cash : '0.00';
    estimateData[0]["children"][2]["sec_variance"] = postVScash(tcoHireAddComm_posted, tcoHireAddComm_cash);
    estimateData[0]["children"][2]["sec_per"] = perPostVScash(tcoHireAddComm_posted, tcoHireAddComm_cash);

    estimateData[0]["children"][3][colName] = dollarUSLocale.format(tco_bb.toFixed(2));
    let tco_bbes = estimateData[0]["children"][3]["estimate"] ? estimateData[0]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let tco_bbac = estimateData[0]["children"][3]["actual"] ? estimateData[0]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][3]["Diff"] = (tco_bbac - tco_bbes).toFixed(2);
    estimateData[0]["children"][3]["perDiff"] = tco_bbes == 0 ? "0.00" : (((tco_bbac - tco_bbes) / tco_bbes) * 100).toFixed(2);
    estimateData[0]["children"][3]["posted"] = tcoBB_posted ? tcoBB_posted : '0.00'
    estimateData[0]["children"][3]["cash_in"] = tcoBB_cash ? tcoBB_cash : '0.00';
    estimateData[0]["children"][3]["sec_variance"] = postVScash(tcoBB_posted, tcoBB_cash);
    estimateData[0]["children"][3]["sec_per"] = perPostVScash(tcoBB_posted, tcoBB_cash);

    //-----------------------
    //TCO BB Comm
    estimateData[0]["children"][4][colName] = dollarUSLocale.format(tco_bb_com.toFixed(2));
    let tco_bb_com_es = estimateData[0]["children"][4]["estimate"] ? estimateData[0]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let tco_bb_com_ac = estimateData[0]["children"][4]["actual"] ? estimateData[0]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][4]["Diff"] = (tco_bb_com_ac - tco_bb_com_es).toFixed(2);
    estimateData[0]["children"][4]["perDiff"] = tco_bb_com_es == 0 ? "0.00" : (((tco_bb_com_ac - tco_bb_com_es) / tco_bb_com_es) * 100).toFixed(2);
    estimateData[0]["children"][4]["posted"] = tcoBB_com_posted ? tcoBB_com_posted : '0.00'
    estimateData[0]["children"][4]["cash_in"] = tcoBB_com_cash ? tcoBB_com_cash : '0.00';
    estimateData[0]["children"][4]["sec_variance"] = postVScash(tcoBB_com_posted, tcoBB_com_cash);
    estimateData[0]["children"][4]["sec_per"] = perPostVScash(tcoBB_com_posted, tcoBB_com_cash);
    //-----------------------

    //---------Net Ballast Bonus
    net_blast_bonus = tco_bb - tco_bb_com;
    estimateData[0]["children"][5][colName] = dollarUSLocale.format(net_blast_bonus.toFixed(2));
    let tco_bb_net_es = estimateData[0]["children"][5]["estimate"] ? estimateData[0]["children"][5]["estimate"].replaceAll(",", "") * 1 : 0;
    let tco_bb_net_ac = estimateData[0]["children"][5]["actual"] ? estimateData[0]["children"][5]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][5]["Diff"] = (tco_bb_net_ac - tco_bb_net_es).toFixed(2);
    estimateData[0]["children"][5]["perDiff"] = tco_bb_net_es == 0 ? "0.00" : (((tco_bb_net_ac - tco_bb_net_es) / tco_bb_net_es) * 100).toFixed(2);
    estimateData[0]["children"][5]["posted"] = tcoBB_net_posted ? tcoBB_net_posted : '0.00'
    estimateData[0]["children"][5]["cash_in"] = tcoBB_net_cash ? tcoBB_net_cash : '0.00';
    estimateData[0]["children"][5]["sec_variance"] = postVScash(tcoBB_net_posted, tcoBB_net_cash);
    estimateData[0]["children"][5]["sec_per"] = perPostVScash(tcoBB_net_posted, tcoBB_net_cash);

    //---------Misc. Revenue
    estimateData[0]["children"][6][colName] = dollarUSLocale.format(MiscRevenue.toFixed(2));
    let mises = estimateData[0]["children"][6]["estimate"] ? estimateData[0]["children"][6]["estimate"].replaceAll(",", "") * 1 : 0;
    let misvac = estimateData[0]["children"][6]["actual"] ? estimateData[0]["children"][6]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][6]["Diff"] = (misvac - mises).toFixed(2);
    estimateData[0]["children"][6]["perDiff"] = mises == 0 ? "0.00" : (((misvac - mises) / mises) * 100).toFixed(2);
    estimateData[0]["children"][6]["posted"] = miscRev_posted ? miscRev_posted : '0.00'
    estimateData[0]["children"][6]["cash_in"] = miscRev_cash ? miscRev_cash : '0.00'
    estimateData[0]["children"][6]["sec_variance"] = postVScash(miscRev_posted, miscRev_cash);
    estimateData[0]["children"][6]["sec_per"] = perPostVScash(miscRev_posted, miscRev_cash);

    //--------Total TCO Net Hire
    ttl_tco_net_hire = tco_hire;
    estimateData[0]["children"][7][colName] = dollarUSLocale.format(ttl_tco_net_hire.toFixed(2));
    let ttl_tco_net_hire_es = estimateData[0]["children"][7]["estimate"] ? estimateData[0]["children"][7]["estimate"].replaceAll(",", "") * 1 : 0;
    let ttl_tco_net_hire_ac = estimateData[0]["children"][7]["actual"] ? estimateData[0]["children"][7]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][7]["Diff"] = (ttl_tco_net_hire_ac - ttl_tco_net_hire_es).toFixed(2);
    estimateData[0]["children"][7]["perDiff"] = ttl_tco_net_hire_es == 0 ? "0.00" : (((ttl_tco_net_hire_ac - ttl_tco_net_hire_es) / ttl_tco_net_hire_es) * 100).toFixed(2);
    estimateData[0]["children"][7]["posted"] = tcoBB_ttl_tco_net_posted ? tcoBB_ttl_tco_net_posted : '0.00'
    estimateData[0]["children"][7]["cash_in"] = tcoBB_ttl_tco_net_cash ? tcoBB_ttl_tco_net_cash : '0.00'
    estimateData[0]["children"][7]["sec_variance"] = postVScash(tcoBB_ttl_tco_net_posted, tcoBB_ttl_tco_net_cash);
    estimateData[0]["children"][7]["sec_per"] = perPostVScash(tcoBB_ttl_tco_net_posted, tcoBB_ttl_tco_net_cash);

    //---------Total Net TCO BB
    ttl_net_tco_bb = tco_bb;
    estimateData[0]["children"][8][colName] = dollarUSLocale.format(ttl_net_tco_bb.toFixed(2));
    let ttl_net_tco_bb_es = estimateData[0]["children"][8]["estimate"] ? estimateData[0]["children"][8]["estimate"].replaceAll(",", "") * 1 : 0;
    let ttl_net_tco_bb_ac = estimateData[0]["children"][8]["actual"] ? estimateData[0]["children"][8]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][8]["Diff"] = (ttl_net_tco_bb_ac - ttl_net_tco_bb_es).toFixed(2);
    estimateData[0]["children"][8]["perDiff"] = ttl_net_tco_bb_es == 0 ? "0.00" : (((ttl_net_tco_bb_ac - ttl_net_tco_bb_es) / ttl_net_tco_bb_es) * 100).toFixed(2);
    estimateData[0]["children"][8]["posted"] = tcoBB_ttl_net_posted ? tcoBB_ttl_net_posted : '0.00'
    estimateData[0]["children"][8]["cash_in"] = tcoBB_ttl_net_cash ? tcoBB_ttl_net_cash : '0.00'
    estimateData[0]["children"][8]["sec_variance"] = postVScash(tcoBB_ttl_net_posted, tcoBB_ttl_net_posted);
    estimateData[0]["children"][8]["sec_per"] = perPostVScash(tcoBB_ttl_net_posted, tcoBB_ttl_net_posted);

    //------------Gross Revenue 

    grossRevenue = tco_hire + bro_percentage_tco + tco_add_percentage + tco_bb + tco_bb_com + net_blast_bonus + MiscRevenue + ttl_tco_net_hire + ttl_net_tco_bb;
    estimateData[0]["children"][9][colName] = dollarUSLocale.format(grossRevenue.toFixed(2));
    let grosses = estimateData[0]["children"][9]["estimate"] ? estimateData[0]["children"][9]["estimate"].replaceAll(",", "") * 1 : 0;
    let grossac = estimateData[0]["children"][9]["actual"] ? estimateData[0]["children"][9]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][9]["Diff"] = (grossac - grosses).toFixed(2);
    estimateData[0]["children"][9]["perDiff"] = grosses == 0 ? "0.00" : (((grossac - grosses) / grosses) * 100).toFixed(2);
    estimateData[0]["children"][9]["posted"] = gross_revenue_posted ? gross_revenue_posted : '0.00';
    estimateData[0]["children"][9]["cash_in"] = gross_revenue_cash ? gross_revenue_cash : '0.00';
    estimateData[0]["children"][9]["sec_variance"] = postVScash(gross_revenue_posted, gross_revenue_cash);
    estimateData[0]["children"][9]["sec_per"] = perPostVScash(gross_revenue_posted, gross_revenue_cash);

    // Net Revenue
    estimateData[0]["children"][10][colName] = dollarUSLocale.format(netRevenue.toFixed(2));
    let netreves = estimateData[0]["children"][10]["estimate"] ? estimateData[0]["children"][10]["estimate"].replaceAll(",", "") * 1 : 0;
    let netrevac = estimateData[0]["children"][10]["actual"] ? estimateData[0]["children"][10]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[0]["children"][10]["Diff"] = (netrevac - netreves).toFixed(2);
    estimateData[0]["children"][10]["perDiff"] = netreves == 0 ? "0.00" : (((netrevac - netreves) / netreves) * 100).toFixed(2);
    estimateData[0]["children"][10]["posted"] = net_revenue_posted ? net_revenue_posted : '0.00'
    estimateData[0]["children"][10]["cash_in"] = net_revenue_cash ? net_revenue_cash : '0.00'
    estimateData[0]["children"][10]["sec_variance"] = postVScash(net_revenue_posted, net_revenue_cash)
    estimateData[0]["children"][10]["sec_per"] = perPostVScash(net_revenue_posted, net_revenue_cash)

    //expenses
    estimateData[1][colName] = dollarUSLocale.format(netExpenses.toFixed(2));
    let expnses = estimateData[1]["estimate"] ? estimateData[1]["estimate"].replaceAll(",", "") * 1 : 0;
    let expnsac = estimateData[1]["actual"] ? estimateData[1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["Diff"] = (expnsac - expnses).toFixed(2);
    estimateData[1]["perDiff"] = expnses == 0 ? "0.00" : (((expnsac - expnses) / expnses) * 100).toFixed(2);
    estimateData[1]["posted"] = net_exp_posted ? net_exp_posted : '0.00';
    estimateData[1]["cash_in"] = net_exp_cash ? net_exp_cash : '0.00';
    estimateData[1]["sec_variance"] = postVScash(net_exp_posted, net_exp_cash)
    estimateData[1]["sec_per"] = perPostVScash(net_exp_posted, net_exp_cash)


    estimateData[1]["children"][0][colName] = dollarUSLocale.format((totalVoyageDays * hire).toFixed(2));
    let veselexpnses = estimateData[1]["children"][0]["estimate"] ? estimateData[1]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let veselexpnsac = estimateData[1]["children"][0]["actual"] ? estimateData[1]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["Diff"] = (veselexpnsac - veselexpnses).toFixed(2);
    estimateData[1]["children"][0]["perDiff"] = veselexpnses == 0 ? "0.00" : (((veselexpnsac - veselexpnses) / veselexpnses) * 100).toFixed(2);
    estimateData[1]["children"][0]["posted"] = vesselExpenses_posted ? vesselExpenses_posted : '0.00';
    estimateData[1]["children"][0]["cash_in"] = vesselExpenses_cash ? vesselExpenses_cash : '0.00';
    estimateData[1]["children"][0]["sec_variance"] = postVScash(vesselExpenses_posted, vesselExpenses_cash);
    estimateData[1]["children"][0]["sec_per"] = perPostVScash(vesselExpenses_posted, vesselExpenses_cash);

    estimateData[1]["children"][0]["children"][0][colName] = dollarUSLocale.format((hire * totalVoyageDays).toFixed(2));
    let hireexpnses = estimateData[1]["children"][0]["children"][0]["estimate"] ? estimateData[1]["children"][0]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let hireexpnsac = estimateData[1]["children"][0]["children"][0]["actual"] ? estimateData[1]["children"][0]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][0]["Diff"] = (hireexpnsac - hireexpnses).toFixed(2);
    estimateData[1]["children"][0]["children"][0]["perDiff"] = hireexpnses == 0 ? "0.00" : (((hireexpnsac - hireexpnses) / hireexpnses) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][0]["posted"] = hireExpenses_posted ? hireExpenses_posted : '0.00';
    estimateData[1]["children"][0]["children"][0]["cash_in"] = hireExpenses_cash ? hireExpenses_cash : '0.00';
    estimateData[1]["children"][0]["children"][0]["sec_variance"] = postVScash(hireExpenses_posted, hireExpenses_cash);
    estimateData[1]["children"][0]["children"][0]["sec_per"] = perPostVScash(hireExpenses_posted, hireExpenses_cash);

    estimateData[1]["children"][0]["children"][1][colName] = dollarUSLocale.format(amt_add_percentage);
    let tciaddcomes = estimateData[1]["children"][0]["children"][1]["estimate"] ? estimateData[1]["children"][0]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let tciaddcomac = estimateData[1]["children"][0]["children"][1]["actual"] ? estimateData[1]["children"][0]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][1]["Diff"] = (tciaddcomac - tciaddcomes).toFixed(2);
    estimateData[1]["children"][0]["children"][1]["perDiff"] = tciaddcomes == 0 ? "0.00" : (((tciaddcomac - tciaddcomes) / tciaddcomes) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][1]["posted"] = "0.00";
    estimateData[1]["children"][0]["children"][1]["cash_in"] = "0.00";
    estimateData[1]["children"][0]["children"][1]["sec_variance"] = "0.00";
    estimateData[1]["children"][0]["children"][1]["sec_per"] = "0.00";

    estimateData[1]["children"][0]["children"][2][colName] = dollarUSLocale.format(amt_bro_percentage);
    let tcibrocomes = estimateData[1]["children"][0]["children"][2]["estimate"] ? estimateData[1]["children"][0]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let tcibrocomac = estimateData[1]["children"][0]["children"][2]["actual"] ? estimateData[1]["children"][0]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][2]["Diff"] = (tcibrocomac - tcibrocomes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["perDiff"] = tcibrocomes == 0 ? "0.00" : (((tcibrocomac - tcibrocomes) / tcibrocomes) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["posted"] = tcibcom_posted ? tcibcom_posted : '0.00';
    estimateData[1]["children"][0]["children"][2]["cash_in"] = tcibcom_cash ? tcibcom_cash : '0.00';
    estimateData[1]["children"][0]["children"][2]["sec_variance"] = postVScash(tcibcom_posted, tcibcom_cash);
    estimateData[1]["children"][0]["children"][2]["sec_per"] = perPostVScash(tcibcom_posted, tcibcom_cash);


    estimateData[1]["children"][0]["children"][3][colName] = dollarUSLocale.format(tci_bb);
    let blstbnses = estimateData[1]["children"][0]["children"][3]["estimate"] ? estimateData[1]["children"][0]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let blstbnsac = estimateData[1]["children"][0]["children"][3]["actual"] ? estimateData[1]["children"][0]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][3]["Diff"] = (blstbnsac - blstbnses).toFixed(2);
    estimateData[1]["children"][0]["children"][3]["perDiff"] = blstbnses == 0 ? "0.00" : (((blstbnsac - blstbnses) / blstbnses) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][3]["posted"] = "0.00";
    estimateData[1]["children"][0]["children"][3]["cash_in"] = "0.00";
    estimateData[1]["children"][0]["children"][3]["sec_variance"] = "0.00";
    estimateData[1]["children"][0]["children"][3]["sec_per"] = "0.00";

    let tci_bb_comm = tci_bb * (amt_bro_percentage + amt_add_percentage);
    estimateData[1]["children"][0]["children"][4][colName] = dollarUSLocale.format(tci_bb_comm);
    let blstbncomses = estimateData[1]["children"][0]["children"][4]["estimate"] ? estimateData[1]["children"][0]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let blstbcomnsac = estimateData[1]["children"][0]["children"][4]["actual"] ? estimateData[1]["children"][0]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][4]["Diff"] = (blstbcomnsac - blstbncomses).toFixed(2);
    estimateData[1]["children"][0]["children"][4]["perDiff"] = blstbncomses == 0 ? "0.00" : (((blstbcomnsac - blstbncomses) / blstbncomses) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][4]["posted"] = "0.00";
    estimateData[1]["children"][0]["children"][4]["cash_in"] = "0.00";
    estimateData[1]["children"][0]["children"][4]["sec_variance"] = "0.00";
    estimateData[1]["children"][0]["children"][4]["sec_per"] = "0.00";

    let reposition_cost = 0;
    estimateData[1]["children"][0]["children"][5][colName] = dollarUSLocale.format(reposition_cost);
    let reposition_costes = estimateData[1]["children"][0]["children"][5]["estimate"] ? estimateData[1]["children"][0]["children"][5]["estimate"].replaceAll(",", "") * 1 : 0;
    let reposition_costac = estimateData[1]["children"][0]["children"][5]["actual"] ? estimateData[1]["children"][0]["children"][5]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][5]["Diff"] = (reposition_costac - reposition_costes).toFixed(2);
    estimateData[1]["children"][0]["children"][5]["perDiff"] = reposition_costes == 0 ? "0.00" : (((reposition_costac - reposition_costes) / reposition_costes) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][5]["posted"] = "0.00";
    estimateData[1]["children"][0]["children"][5]["cash_in"] = "0.00";
    estimateData[1]["children"][0]["children"][5]["sec_variance"] = "0.00";
    estimateData[1]["children"][0]["children"][5]["sec_per"] = "0.00";

    let emission_expense = co2cost;
    estimateData[1]["children"][0]["children"][6][colName] = dollarUSLocale.format(emission_expense);
    let emission_expes = estimateData[1]["children"][0]["children"][6]["estimate"] ? estimateData[1]["children"][0]["children"][6]["estimate"].replaceAll(",", "") * 1 : 0;
    let emission_expac = estimateData[1]["children"][0]["children"][6]["actual"] ? estimateData[1]["children"][0]["children"][6]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][0]["children"][6]["Diff"] = (emission_expac - emission_expes).toFixed(2);
    estimateData[1]["children"][0]["children"][6]["perDiff"] = emission_expes == 0 ? "0.00" : (((emission_expac - emission_expes) / emission_expes) * 100).toFixed(2);
    estimateData[1]["children"][0]["children"][6]["posted"] = "0.00";
    estimateData[1]["children"][0]["children"][6]["cash_in"] = "0.00";
    estimateData[1]["children"][0]["children"][6]["sec_variance"] = "0.00";
    estimateData[1]["children"][0]["children"][6]["sec_per"] = "0.00";

    //************************************************ */

    estimateData[1]["children"][1][colName] = dollarUSLocale.format(pi.toFixed(2));
    let prtexpnses = estimateData[1]["children"][1]["estimate"] ? estimateData[1]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtexpnsac = estimateData[1]["children"][1]["actual"] ? estimateData[1]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][1]["Diff"] = (prtexpnsac - prtexpnses).toFixed(2);
    estimateData[1]["children"][1]["perDiff"] = prtexpnses == 0 ? "0.00" : (((prtexpnsac - prtexpnses) / prtexpnses) * 100).toFixed(2);
    estimateData[1]["children"][1]["posted"] = portExpenses_posted ? portExpenses_posted : '0.00';
    estimateData[1]["children"][1]["cash_in"] = portExpenses_cash ? portExpenses_cash : '0.00';
    estimateData[1]["children"][1]["sec_variance"] = postVScash(portExpenses_posted, portExpenses_cash);
    estimateData[1]["children"][1]["sec_per"] = perPostVScash(portExpenses_posted, portExpenses_cash);

    estimateData[1]["children"][2][colName] = dollarUSLocale.format(bunkerExpense.toFixed(2));
    let bnkrexpnses = estimateData[1]["children"][2]["estimate"] ? estimateData[1]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let bnkrexpnsac = estimateData[1]["children"][2]["actual"] ? estimateData[1]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["Diff"] = (bnkrexpnsac - bnkrexpnses).toFixed(2);
    estimateData[1]["children"][2]["perDiff"] = bnkrexpnses == 0 ? "0.00" : (((bnkrexpnsac - bnkrexpnses) / bnkrexpnses) * 100).toFixed(2);
    estimateData[1]["children"][2]["posted"] = bunkerExpenses_posted ? bunkerExpenses_posted : '0.00'
    estimateData[1]["children"][2]["cash_in"] = bunkerExpenses_cash ? bunkerExpenses_cash : '0.00'
    estimateData[1]["children"][2]["sec_variance"] = postVScash(bunkerExpenses_posted, bunkerExpenses_cash);
    estimateData[1]["children"][2]["sec_per"] = perPostVScash(bunkerExpenses_posted, bunkerExpenses_cash);


    estimateData[1]["children"][2]["children"][0][colName] = dollarUSLocale.format(totalnonEcaSeacons.toFixed(2));
    let seacnsmpes = estimateData[1]["children"][2]["children"][0]["estimate"] ? estimateData[1]["children"][2]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let seacnsmpac = estimateData[1]["children"][2]["children"][0]["actual"] ? estimateData[1]["children"][2]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["Diff"] = (seacnsmpac - seacnsmpes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["perDiff"] = seacnsmpes == 0 ? "0.00" : (((seacnsmpac - seacnsmpes) / seacnsmpes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][0]["children"][0][colName] = dollarUSLocale.format(nonEcaSeacons["ifo"].toFixed(2));
    let seaexpnsifoes = estimateData[1]["children"][2]["children"][0]["children"][0]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsifoac = estimateData[1]["children"][2]["children"][0]["children"][0]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["children"][0]["Diff"] = (seaexpnsifoac - seaexpnsifoes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][0]["perDiff"] = seaexpnsifoes == 0 ? "0.00" : (((seaexpnsifoac - seaexpnsifoes) / seaexpnsifoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][0]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][0]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][0]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][0]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][0]["children"][1][colName] = dollarUSLocale.format(nonEcaSeacons["mgo"].toFixed(2));
    let seaexpnsmgoes = estimateData[1]["children"][2]["children"][0]["children"][1]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsmgoac = estimateData[1]["children"][2]["children"][0]["children"][1]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["children"][1]["Diff"] = (seaexpnsmgoac - seaexpnsmgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][1]["perDiff"] = seaexpnsmgoes == 0 ? "0.00" : (((seaexpnsmgoac - seaexpnsmgoes) / seaexpnsmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][1]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][1]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][1]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][1]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][0]["children"][2][colName] = dollarUSLocale.format(nonEcaSeacons["vlsfo"].toFixed(2));
    let seaexpnsvlsfoes = estimateData[1]["children"][2]["children"][0]["children"][2]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsvlsfoac = estimateData[1]["children"][2]["children"][0]["children"][2]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["children"][2]["Diff"] = (seaexpnsvlsfoac - seaexpnsvlsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][2]["perDiff"] = seaexpnsvlsfoes == 0 ? "0.00" : (((seaexpnsvlsfoac - seaexpnsvlsfoes) / seaexpnsvlsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][2]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][2]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][2]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][2]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][0]["children"][4][colName] = dollarUSLocale.format(nonEcaSeacons["ulsfo"].toFixed(2));
    let seaexpnsulsfoes = estimateData[1]["children"][2]["children"][0]["children"][4]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsulsfoac = estimateData[1]["children"][2]["children"][0]["children"][4]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["children"][4]["Diff"] = (seaexpnsulsfoac - seaexpnsulsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][4]["perDiff"] = seaexpnsulsfoes == 0 ? "0.00" : (((seaexpnsulsfoac - seaexpnsulsfoes) / seaexpnsulsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][4]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][4]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][4]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][4]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][0]["children"][3][colName] = dollarUSLocale.format(nonEcaSeacons["lsmgo"].toFixed(2));
    let seaexpnslsmgoes = estimateData[1]["children"][2]["children"][0]["children"][3]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnslsmgoac = estimateData[1]["children"][2]["children"][0]["children"][3]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][0]["children"][3]["Diff"] = (seaexpnslsmgoac - seaexpnslsmgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][3]["perDiff"] = seaexpnslsmgoes == 0 ? "0.00" : (((seaexpnslsmgoac - seaexpnslsmgoes) / seaexpnslsmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][0]["children"][3]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][3]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][3]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][0]["children"][3]["sec_per"] = "0.00";

    //port cons non eca
    estimateData[1]["children"][2]["children"][1][colName] = dollarUSLocale.format(totalnonecaPortcons.toFixed(2));
    let prtcnspmes = estimateData[1]["children"][2]["children"][1]["estimate"] ? estimateData[1]["children"][2]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmpac = estimateData[1]["children"][2]["children"][1]["actual"] ? estimateData[1]["children"][2]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["Diff"] = (prtcnsmpac - prtcnspmes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["perDiff"] = prtcnspmes == 0 ? "0.00" : (((prtcnsmpac - prtcnspmes) / prtcnspmes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["sec_per"] = "0.00";
    //ifo port cons non eca
    estimateData[1]["children"][2]["children"][1]["children"][0][colName] = dollarUSLocale.format(nonecaPortcons["ifo"].toFixed(2));
    let prtcnsmpifoes = estimateData[1]["children"][2]["children"][1]["children"][0]["estimate"] ? estimateData[1]["children"][2]["children"][1]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmpifoac = estimateData[1]["children"][2]["children"][1]["children"][0]["actual"] ? estimateData[1]["children"][2]["children"][1]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["children"][0]["Diff"] = (prtcnsmpifoac - prtcnsmpifoes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][0]["perDiff"] = prtcnsmpifoes == 0 ? "0.00" : (((prtcnsmpifoac - prtcnsmpifoes) / prtcnsmpifoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][0]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][0]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][0]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][0]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][1]["children"][1][colName] = dollarUSLocale.format(nonecaPortcons["mgo"].toFixed(2));
    let prtcnsmpmgoes = estimateData[1]["children"][2]["children"][1]["children"][1]["estimate"] ? estimateData[1]["children"][2]["children"][1]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmpmgoac = estimateData[1]["children"][2]["children"][1]["children"][1]["actual"] ? estimateData[1]["children"][2]["children"][1]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["children"][1]["Diff"] = (prtcnsmpmgoac - prtcnsmpmgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][1]["perDiff"] = prtcnsmpmgoes == 0 ? "0.00" : (((prtcnsmpmgoac - prtcnsmpmgoes) / prtcnsmpmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][1]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][1]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][1]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][1]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][1]["children"][2][colName] = dollarUSLocale.format(nonecaPortcons["vlsfo"].toFixed(2));
    let prtcnsmpvlsfoes = estimateData[1]["children"][2]["children"][1]["children"][2]["estimate"] ? estimateData[1]["children"][2]["children"][1]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmpvlsfoac = estimateData[1]["children"][2]["children"][1]["children"][2]["actual"] ? estimateData[1]["children"][2]["children"][1]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["children"][2]["Diff"] = (prtcnsmpvlsfoac - prtcnsmpvlsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][2]["perDiff"] = prtcnsmpvlsfoes == 0 ? "0.00" : (((prtcnsmpvlsfoac - prtcnsmpvlsfoes) / prtcnsmpvlsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][2]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][2]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][2]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][2]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][1]["children"][3][colName] = dollarUSLocale.format(nonecaPortcons["lsmgo"].toFixed(2));
    let prtcnsmplsmgoes = estimateData[1]["children"][2]["children"][1]["children"][3]["estimate"] ? estimateData[1]["children"][2]["children"][1]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmplsmgoac = estimateData[1]["children"][2]["children"][1]["children"][3]["actual"] ? estimateData[1]["children"][2]["children"][1]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["children"][3]["Diff"] = (prtcnsmplsmgoac - prtcnsmplsmgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][3]["perDiff"] = prtcnsmplsmgoes == 0 ? "0.00" : (((prtcnsmplsmgoac - prtcnsmplsmgoes) / prtcnsmplsmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][3]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][3]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][3]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][3]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][1]["children"][4][colName] = dollarUSLocale.format(nonecaPortcons["ulsfo"].toFixed(2));
    let prtcnsmpulsfoes = estimateData[1]["children"][2]["children"][1]["children"][4]["estimate"] ? estimateData[1]["children"][2]["children"][1]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let prtcnsmpulsfoac = estimateData[1]["children"][2]["children"][1]["children"][4]["actual"] ? estimateData[1]["children"][2]["children"][1]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][1]["children"][4]["Diff"] = (prtcnsmpulsfoac - prtcnsmpulsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][4]["perDiff"] = prtcnsmpulsfoes == 0 ? "0.00" : (((prtcnsmpulsfoac - prtcnsmpulsfoes) / prtcnsmpulsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][1]["children"][4]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][4]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][4]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][1]["children"][4]["sec_per"] = "0.00";



    //------------
    //Sea cons ECA
    estimateData[1]["children"][2]["children"][2][colName] = dollarUSLocale.format(totalecaSeacons.toFixed(2));
    let seacnsmpecaes = estimateData[1]["children"][2]["children"][2]["estimate"] ? estimateData[1]["children"][2]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let seacnsmpecaac = estimateData[1]["children"][2]["children"][2]["actual"] ? estimateData[1]["children"][2]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["Diff"] = (seacnsmpecaac - seacnsmpecaes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["perDiff"] = seacnsmpecaes == 0 ? "0.00" : (((seacnsmpecaac - seacnsmpecaes) / seacnsmpecaes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][2]["children"][0][colName] = dollarUSLocale.format(ecaSeacons["ifo"].toFixed(2));
    let seaexpnsecaifoes = estimateData[1]["children"][2]["children"][2]["children"][0]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsecaifoac = estimateData[1]["children"][2]["children"][2]["children"][0]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["children"][0]["Diff"] = (seaexpnsecaifoac - seaexpnsecaifoes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][0]["perDiff"] = seaexpnsecaifoes == 0 ? "0.00" : (((seaexpnsecaifoac - seaexpnsecaifoes) / seaexpnsifoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][0]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][0]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][0]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][0]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][2]["children"][1][colName] = dollarUSLocale.format(ecaSeacons["mgo"].toFixed(2));
    let seaexpnsecamgoes = estimateData[1]["children"][2]["children"][2]["children"][1]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsecamgoac = estimateData[1]["children"][2]["children"][2]["children"][1]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["children"][1]["Diff"] = (seaexpnsecamgoac - seaexpnsecamgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][1]["perDiff"] = seaexpnsecamgoes == 0 ? "0.00" : (((seaexpnsecamgoac - seaexpnsecamgoes) / seaexpnsmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][1]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][1]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][1]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][1]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][2]["children"][2][colName] = dollarUSLocale.format(ecaSeacons["vlsfo"].toFixed(2));
    let seaexpnsecavlsfoes = estimateData[1]["children"][2]["children"][2]["children"][2]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsecavlsfoac = estimateData[1]["children"][2]["children"][2]["children"][2]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["children"][2]["Diff"] = (seaexpnsecavlsfoac - seaexpnsecavlsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][2]["perDiff"] = seaexpnsecavlsfoes == 0 ? "0.00" : (((seaexpnsecavlsfoac - seaexpnsvlsfoes) / seaexpnsecavlsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][2]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][2]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][2]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][2]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][2]["children"][4][colName] = dollarUSLocale.format(ecaSeacons["ulsfo"].toFixed(2));
    let seaexpnsecaulsfoes = estimateData[1]["children"][2]["children"][2]["children"][4]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsecaulsfoac = estimateData[1]["children"][2]["children"][2]["children"][4]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["children"][4]["Diff"] = (seaexpnsecaulsfoac - seaexpnsecaulsfoes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][4]["perDiff"] = seaexpnsecaulsfoes == 0 ? "0.00" : (((seaexpnsecaulsfoac - seaexpnsecaulsfoes) / seaexpnsulsfoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][4]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][4]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][4]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][4]["sec_per"] = "0.00";

    estimateData[1]["children"][2]["children"][2]["children"][3][colName] = dollarUSLocale.format(ecaSeacons["lsmgo"].toFixed(2));
    let seaexpnsecalsmgoes = estimateData[1]["children"][2]["children"][0]["children"][3]["estimate"] ? estimateData[1]["children"][2]["children"][0]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let seaexpnsecalsmgoac = estimateData[1]["children"][2]["children"][0]["children"][3]["actual"] ? estimateData[1]["children"][2]["children"][0]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][2]["children"][2]["children"][3]["Diff"] = (seaexpnsecalsmgoac - seaexpnsecalsmgoes).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][3]["perDiff"] = seaexpnsecalsmgoes == 0 ? "0.00" : (((seaexpnsecalsmgoac - seaexpnsecalsmgoes) / seaexpnslsmgoes) * 100).toFixed(2);
    estimateData[1]["children"][2]["children"][2]["children"][3]["posted"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][3]["cash_in"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][3]["sec_variance"] = "0.00";
    estimateData[1]["children"][2]["children"][2]["children"][3]["sec_per"] = "0.00";

    // Port cons ECA
    // estimateData[1]["children"][2]["children"][3][colName] = dollarUSLocale.format(totalecaPortCons.toFixed(2));
    // let prtcnspmecaes = estimateData[1]["children"][2]["children"][3]["estimate"] ? estimateData[1]["children"][2]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecaac = estimateData[1]["children"][2]["children"][3]["actual"] ? estimateData[1]["children"][2]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["Diff"] = (prtcnsmpecaac - prtcnspmecaes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["perDiff"] = prtcnspmecaes == 0 ? "0.00" : (((prtcnsmpecaac - prtcnspmecaes) / prtcnspmecaes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["sec_per"] = "0.00";

    // estimateData[1]["children"][2]["children"][3]["children"][0][colName] = dollarUSLocale.format(ecaPortCons["ifo"].toFixed(2));
    // let prtcnsmpecaifoes = estimateData[1]["children"][2]["children"][3]["children"][0]["estimate"] ? estimateData[1]["children"][2]["children"][3]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecaifoac = estimateData[1]["children"][2]["children"][3]["children"][0]["actual"] ? estimateData[1]["children"][2]["children"][3]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["children"][0]["Diff"] = (prtcnsmpecaifoac - prtcnsmpecaifoes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][0]["perDiff"] = prtcnsmpecaifoes == 0 ? "0.00" : (((prtcnsmpecaifoac - prtcnsmpecaifoes) / prtcnsmpecaifoes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][0]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][0]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][0]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][0]["sec_per"] = "0.00";

    // estimateData[1]["children"][2]["children"][3]["children"][1][colName] = dollarUSLocale.format(ecaPortCons["mgo"].toFixed(2));
    // let prtcnsmpecamgoes = estimateData[1]["children"][2]["children"][3]["children"][1]["estimate"] ? estimateData[1]["children"][2]["children"][3]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecamgoac = estimateData[1]["children"][2]["children"][3]["children"][1]["actual"] ? estimateData[1]["children"][2]["children"][3]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["children"][1]["Diff"] = (prtcnsmpecamgoac - prtcnsmpecamgoes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][1]["perDiff"] = prtcnsmpecamgoes == 0 ? "0.00" : (((prtcnsmpecamgoac - prtcnsmpecamgoes) / prtcnsmpecamgoes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][1]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][1]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][1]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][1]["sec_per"] = "0.00";

    // estimateData[1]["children"][2]["children"][3]["children"][2][colName] = dollarUSLocale.format(ecaPortCons["vlsfo"].toFixed(2));
    // let prtcnsmpecavlsfoes = estimateData[1]["children"][2]["children"][3]["children"][2]["estimate"] ? estimateData[1]["children"][2]["children"][3]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecavlsfoac = estimateData[1]["children"][2]["children"][3]["children"][2]["actual"] ? estimateData[1]["children"][2]["children"][3]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["children"][2]["Diff"] = (prtcnsmpecavlsfoac - prtcnsmpecavlsfoes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][2]["perDiff"] = prtcnsmpecavlsfoes == 0 ? "0.00" : (((prtcnsmpecavlsfoac - prtcnsmpecavlsfoes) / prtcnsmpecavlsfoes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][2]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][2]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][2]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][2]["sec_per"] = "0.00";

    // estimateData[1]["children"][2]["children"][3]["children"][3][colName] = dollarUSLocale.format(ecaPortCons["lsmgo"].toFixed(2));
    // let prtcnsmpecalsmgoes = estimateData[1]["children"][2]["children"][3]["children"][3]["estimate"] ? estimateData[1]["children"][2]["children"][3]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecalsmgoac = estimateData[1]["children"][2]["children"][3]["children"][3]["actual"] ? estimateData[1]["children"][2]["children"][3]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["children"][3]["Diff"] = (prtcnsmpecalsmgoac - prtcnsmpecalsmgoes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][3]["perDiff"] = prtcnsmpecalsmgoes == 0 ? "0.00" : (((prtcnsmpecalsmgoac - prtcnsmpecalsmgoes) / prtcnsmpecalsmgoes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][3]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][3]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][3]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][3]["sec_per"] = "0.00";

    // estimateData[1]["children"][2]["children"][3]["children"][4][colName] = dollarUSLocale.format(ecaPortCons["ulsfo"].toFixed(2));
    // let prtcnsmpecaulsfoes = estimateData[1]["children"][2]["children"][3]["children"][4]["estimate"] ? estimateData[1]["children"][2]["children"][3]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    // let prtcnsmpecaulsfoac = estimateData[1]["children"][2]["children"][3]["children"][4]["actual"] ? estimateData[1]["children"][2]["children"][3]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    // estimateData[1]["children"][2]["children"][3]["children"][4]["Diff"] = (prtcnsmpecaulsfoac - prtcnsmpecaulsfoes).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][4]["perDiff"] = prtcnsmpecaulsfoes == 0 ? "0.00" : (((prtcnsmpecaulsfoac - prtcnsmpecaulsfoes) / prtcnsmpecaulsfoes) * 100).toFixed(2);
    // estimateData[1]["children"][2]["children"][3]["children"][4]["posted"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][4]["cash_in"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][4]["sec_variance"] = "0.00";
    // estimateData[1]["children"][2]["children"][3]["children"][4]["sec_per"] = "0.00";

    //-----------

    estimateData[1]["children"][3][colName] = dollarUSLocale.format(otherCost.toFixed(2));
    let miscstes = estimateData[1]["children"][3]["estimate"] ? estimateData[1]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let miscstac = estimateData[1]["children"][3]["actual"] ? estimateData[1]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[1]["children"][3]["Diff"] = (miscstac - miscstes).toFixed(2);
    estimateData[1]["children"][3]["perDiff"] = miscstes == 0 ? "0.00" : (((miscstac - miscstes) / miscstes) * 100).toFixed(2);
    estimateData[1]["children"][3]["posted"] = miscExpenses_posted ? miscExpenses_posted : '0.00';
    estimateData[1]["children"][3]["cash_in"] = miscExpenses_cash ? miscExpenses_cash : '0.00';
    estimateData[1]["children"][3]["sec_variance"] = postVScash(miscExpenses_posted, miscExpenses_cash);
    estimateData[1]["children"][3]["sec_per"] = perPostVScash(miscExpenses_posted, miscExpenses_cash);

    estimateData[1]["children"][4][colName] = dollarUSLocale.format(grossExpenses.toFixed(2));
    let grossest = estimateData[1]["children"][4]["estimate"] ? estimateData[1]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let grossact = estimateData[1]["children"][4]["actual"] ? estimateData[1]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;

    estimateData[1]["children"][4]["Diff"] = (grossact - grossest).toFixed(2);
    estimateData[1]["children"][4]["perDiff"] = grossest == 0 ? "0.00" : (((grossact - grossest) / grossest) * 100).toFixed(2);;
    estimateData[1]["children"][4]["posted"] = "0.00";
    estimateData[1]["children"][4]["cash_in"] = "0.00";
    estimateData[1]["children"][4]["sec_variance"] = "0.00";
    estimateData[1]["children"][4]["sec_per"] = "0.00";

    estimateData[1]["children"][5][colName] = dollarUSLocale.format(netExpenses.toFixed(2));
    let netexpest = estimateData[1]["children"][5]["estimate"] ? estimateData[1]["children"][5]["estimate"].replaceAll(",", "") * 1 : 0;
    let netexpact = estimateData[1]["children"][5]["actual"] ? estimateData[1]["children"][5]["actual"].replaceAll(",", "") * 1 : 0;

    estimateData[1]["children"][5]["Diff"] = (netexpact - netexpest).toFixed(2);
    estimateData[1]["children"][5]["perDiff"] = netexpest == 0 ? "0.00" : (((netexpact - netexpest) / netexpest) * 100).toFixed(2);;
    estimateData[1]["children"][5]["posted"] = "0.00";
    estimateData[1]["children"][5]["cash_in"] = "0.00";
    estimateData[1]["children"][5]["sec_variance"] = "0.00";
    estimateData[1]["children"][5]["sec_per"] = "0.00";

    let itemValue = netRevenue - netExpenses;
    let tceHire = (itemValue - (totalExpenses - (tco_bb + pi + 0 + otherCost + amt_add_percentage + amt_bro_percentage)) / (itemValue / (totalVoyageDays - 0))).toFixed(2);
    estimateData[2][colName] = itemValue >= 0 ? dollarUSLocale
      .format(itemValue.toFixed(2))
      .replaceAll(
        ",",
        ""
      ) : dollarUSLocale
        .format((itemValue * 1).toFixed(2))
        .replaceAll(",", "");

    estimateData[2]["children"][0][colName] = itemValue >= 0 ? dollarUSLocale
      .format(itemValue.toFixed(2))
      .replaceAll(
        ",",
        ""
      ) : dollarUSLocale
        .format((itemValue * 1).toFixed(2))
        .replaceAll(",", "");

    let vygreses = estimateData[2]["estimate"] ? estimateData[2]["estimate"].replaceAll(",", "") * 1 : 0;
    let vygresac = estimateData[2]["actual"] ? estimateData[2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["Diff"] = (vygresac - vygreses).toFixed(2);
    estimateData[2]["perDiff"] = (((vygresac - vygreses) / vygreses) * 100).toFixed(2);
    estimateData[2]["posted"] = "0.00";
    estimateData[2]["cash_in"] = "0.00";
    estimateData[2]["sec_variance"] = "0.00";
    estimateData[2]["sec_per"] = "0.00";

    let profitlses = estimateData[2]["children"][0]["estimate"] ? estimateData[2]["children"][0]["estimate"].replaceAll(",", "") * 1 : 0;
    let profitlsac = estimateData[2]["children"][0]["actual"] ? estimateData[2]["children"][0]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][0]["Diff"] = (profitlsac - profitlses).toFixed(2);
    estimateData[2]["children"][0]["perDiff"] = profitlses == 0 ? "0.00" : (((profitlsac - profitlses) / profitlses) * 100).toFixed(2);
    estimateData[2]["children"][0]["posted"] = "0.00";
    estimateData[2]["children"][0]["cash_in"] = "0.00";
    estimateData[2]["children"][0]["sec_variance"] = "0.00";
    estimateData[2]["children"][0]["sec_per"] = "0.00";

    estimateData[2]["children"][1][colName] = dollarUSLocale.format((itemValue / (totalVoyageDays - 0)).toFixed(2));
    let dlprofitlses = estimateData[2]["children"][1]["estimate"] ? estimateData[2]["children"][1]["estimate"].replaceAll(",", "") * 1 : 0;
    let dlprofitlsac = estimateData[2]["children"][1]["actual"] ? estimateData[2]["children"][1]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][1]["Diff"] = (dlprofitlsac - dlprofitlses).toFixed(2);
    estimateData[2]["children"][1]["perDiff"] = dlprofitlses == 0 ? "0.00" : (((dlprofitlsac - dlprofitlses) / dlprofitlses) * 100).toFixed(2);
    estimateData[2]["children"][1]["posted"] = "0.00";
    estimateData[2]["children"][1]["cash_in"] = "0.00";
    estimateData[2]["children"][1]["sec_variance"] = "0.00";
    estimateData[2]["children"][1]["sec_per"] = "0.00";

    //--------- added by vikas

    let co2adjustedcost = vygresac - co2cost;
    let cO2_adjusted_net_tce = totalVoyageDays !== 0 ? co2adjustedcost / totalVoyageDays : 0;

    estimateData[2]["children"][2][colName] = dollarUSLocale.format((co2cost - 0).toFixed(2));
    let co2costes = estimateData[2]["children"][2]["estimate"] ? estimateData[2]["children"][2]["estimate"].replaceAll(",", "") * 1 : 0;
    let co2costac = estimateData[2]["children"][2]["actual"] ? estimateData[2]["children"][2]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][2]["Diff"] = (co2costac - co2costes).toFixed(2);
    estimateData[2]["children"][2]["perDiff"] = co2costes == 0 ? "0.00" : (((co2costac - co2costes) / co2costes) * 100).toFixed(2);
    estimateData[2]["children"][2]["posted"] = "0.00";
    estimateData[2]["children"][2]["cash_in"] = "0.00";
    estimateData[2]["children"][2]["sec_variance"] = "0.00";
    estimateData[2]["children"][2]["sec_per"] = "0.00";

    estimateData[2]["children"][3][colName] = dollarUSLocale.format((co2adjustedcost - 0).toFixed(2));
    let co2adjProfites = estimateData[2]["children"][3]["estimate"] ? estimateData[2]["children"][3]["estimate"].replaceAll(",", "") * 1 : 0;
    let co2adjProfitac = estimateData[2]["children"][3]["actual"] ? estimateData[2]["children"][3]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][3]["Diff"] = (co2adjProfitac - co2adjProfites).toFixed(2);
    estimateData[2]["children"][3]["perDiff"] = co2adjProfites == 0 ? "0.00" : (((co2adjProfitac - co2adjProfites) / co2adjProfites) * 100).toFixed(2);
    estimateData[2]["children"][3]["posted"] = "0.00";
    estimateData[2]["children"][3]["cash_in"] = "0.00";
    estimateData[2]["children"][3]["sec_variance"] = "0.00";
    estimateData[2]["children"][3]["sec_per"] = "0.00";

    estimateData[2]["children"][4][colName] = dollarUSLocale.format((cO2_adjusted_net_tce - 0).toFixed(2));
    let co2adjNetes = estimateData[2]["children"][4]["estimate"] ? estimateData[2]["children"][4]["estimate"].replaceAll(",", "") * 1 : 0;
    let co2adjNetac = estimateData[2]["children"][4]["actual"] ? estimateData[2]["children"][4]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][4]["Diff"] = (co2adjNetac - co2adjNetes).toFixed(2);
    estimateData[2]["children"][4]["perDiff"] = co2adjNetes == 0 ? "0.00" : (((co2adjNetac - co2adjNetes) / co2adjNetes) * 100).toFixed(2);
    estimateData[2]["children"][4]["posted"] = "0.00";
    estimateData[2]["children"][4]["cash_in"] = "0.00";
    estimateData[2]["children"][4]["sec_variance"] = "0.00";
    estimateData[2]["children"][4]["sec_per"] = "0.00";

    estimateData[2]["children"][5][colName] = dollarUSLocale.format((tsd * 1).toFixed(2));
    let tsdes = estimateData[2]["children"][5]["estimate"] ? estimateData[2]["children"][5]["estimate"].replaceAll(",", "") * 1 : 0;
    let tsdac = estimateData[2]["children"][5]["actual"] ? estimateData[2]["children"][5]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][5]["Diff"] = (tsdac - tsdes).toFixed(2);
    estimateData[2]["children"][5]["perDiff"] = tsdes == 0 ? "0.00" : (((tsdac - tsdes) / tsdes) * 100).toFixed(2);
    estimateData[2]["children"][5]["posted"] = "0.00";
    estimateData[2]["children"][5]["cash_in"] = "0.00";
    estimateData[2]["children"][5]["sec_variance"] = "0.00";
    estimateData[2]["children"][5]["sec_per"] = "0.00";

    estimateData[2]["children"][6][colName] = dollarUSLocale.format((tpd * 1).toFixed(2));
    let tpdes = estimateData[2]["children"][6]["estimate"] ? estimateData[2]["children"][6]["estimate"].replaceAll(",", "") * 1 : 0;
    let tpdac = estimateData[2]["children"][6]["actual"] ? estimateData[2]["children"][6]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][6]["Diff"] = (tpdac - tpdes).toFixed(2);
    estimateData[2]["children"][6]["perDiff"] = tpdes == 0 ? "0.00" : (((tpdac - tpdes) / tpdes) * 100).toFixed(2);
    estimateData[2]["children"][6]["posted"] = "0.00";
    estimateData[2]["children"][6]["cash_in"] = "0.00";
    estimateData[2]["children"][6]["sec_variance"] = "0.00";
    estimateData[2]["children"][6]["sec_per"] = "0.00";

    estimateData[2]["children"][7][colName] = dollarUSLocale.format((totalVoyageDays - 0).toFixed(2)); // OFF Hire Day => 0
    let netvygdses = estimateData[2]["children"][7]["estimate"] ? estimateData[2]["children"][7]["estimate"].replaceAll(",", "") * 1 : 0;
    let netvygdsac = estimateData[2]["children"][7]["actual"] ? estimateData[2]["children"][7]["actual"].replaceAll(",", "") * 1 : 0;
    estimateData[2]["children"][7]["Diff"] = (netvygdsac - netvygdses).toFixed(2);
    estimateData[2]["children"][7]["perDiff"] = netvygdses == 0 ? "0.00" : (((netvygdsac - netvygdses) / netvygdses) * 100).toFixed(2);
    estimateData[2]["children"][7]["posted"] = "0.00";
    estimateData[2]["children"][7]["cash_in"] = "0.00";
    estimateData[2]["children"][7]["sec_variance"] = "0.00";
    estimateData[2]["children"][7]["sec_per"] = "0.00";

    console.log('estimateData :', estimateData);
    return estimateData;
  };

  handleClick = async () => {
    // Fetch the element by class name
    const element = document.querySelector(
      ".ant-table-row-expand-icon-collapsed"
    );

    if (element) {
      await element.click();
    }
  };

  savePNL = async (captureScreenshot_Url) => {
    const _id = this.state?.formData?.id;

    const currentUser = localStorage.getItem('currentUser');
    const currentTime = moment(new Date()).format("YYYY-MM-DD, hh:mm:ss A");
    const savePayload = {
      scr_url: captureScreenshot_Url, pnl_data: this.state.estimateData,
      user: currentUser, created_at: currentTime, voyage_no: _id
    };

    const myurl = `${URL_WITH_VERSION}/voyage-manager/voyage-pnl/save`
    try {
      const response = await fetch(myurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(savePayload)
      });

      const data = await response.json();
      if (data.data) {
        openNotificationWithIcon('info', data.message)
      } else {
        openNotificationWithIcon('error', data.message)
      }
    } catch (err) {
      console.log('error in savePNL :', err)
      openNotificationWithIcon('error', 'Something went wrong', 3)
    }
  }

  deleteCapturedScreenshot = async () => {
    console.log("deleteCapturedScreenshot is called..");
    const fileUrl =
      "https://s3-ap-southeast-1.amazonaws.com/toc-base-bucket/theoceann/2024-03-21_07:59:23_captured_image.png";
    const token = localStorage.getItem("oceanToken");

    // Make the DELETE request
    const payload = { url: fileUrl };
    fetch(`${URL_WITH_VERSION}/s3/delete`, {
      method: "DELETE",

      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        console.log("Deleted successfully:", data);
        openNotificationWithIcon('info', 'Deleted successfully', 3)
        this.setState({ ...this.state, showPL: true, saveLoading: false });
      })
      .catch((error) => {
        console.error("Error deleting:", error);
        openNotificationWithIcon('info', 'Something went wrong..', 3)
        this.setState({ ...this.state, showPL: true, saveLoading: false });
      });

  }

  captureScreenshot = async () => {
    const element2 = this.elementRef.current;
    if (element2) {
      html2canvas(element2).then((canvas) => {
        var dataURL = canvas.toDataURL("image/png");
        // Create an image element from the data URL
        var img = new Image();
        img.src = dataURL;
        img.download = dataURL;
        // Create a link element
        var a = document.createElement("a");
        var now = new Date();
        var fileName =
          "pnl_save_" + now.toISOString().replace(/[:.]/g, "-") + ".png";

        a.target = "_blank";
        // Set the href of the link to the data URL of the image
        a.href = img.src;

        a.download = fileName;
        // Append the link to the page
        document.body.appendChild(a);
        // Click the link to trigger the download
        //a.click();

        this.setState({ ...this.state, showPL: false });
        //this.deleteCapturedScreenshot();

        canvas.toBlob(async (blob) => {
          try {
            // Create FormData object
            const formData = new FormData();
            formData.append("user_file", blob, "captured_image.png");
            // Make a POST request to your API endpoint
            const response = await fetch(`${process.env.REACT_APP_ATTACHMENT}/s3/upload`, {
              method: "POST",
              body: formData,
            });
            if (!response.ok) {
              throw new Error('Failed to upload image');
            }
            const data = await response.json();
            //console.log("API Response:", data);
            await this.savePNL(data.data);
          } catch (error) {
            //console.error("Error sending image data to API:", error);
            openNotificationWithIcon('error', "Something went wrong...", 3)
          }
          this.setState({ ...this.state, showPL: true, saveLoading: false });
        }, "image/png");

      });
    }
  }

  handleButtonClick = async () => {
    this.setState({ ...this.state, saveLoading: true })
    // Call handleClick function multiple times to expand menues
    for (let i = 0; i < 7; i++) {
      await this.handleClick();
    }

    setTimeout(() => {
      this.captureScreenshot();
    }, 1000);
  };

  componentDidMount() {
    // console.log('call count');
    this.setState(
      {
        ...this.state,
        showPL: false,
        estimateData: this.__pl(),
      },
      () => this.setState({ ...this.state, showPL: true })
    );
  }

  render() {
    const { estimateData, showPL, viewTabs, saveLoading } = this.state;
    return <>
      <Button type="primary" shape="round" icon={<SaveOutlined />} loading={saveLoading} size='middle' onClick={this.handleButtonClick}>
        Save PNL
      </Button>
      <div>
        <ToolbarUI routeUrl={"pl-main"} callback={(e) => this.callback(e)} />
      </div>

      {showPL === true ? (
        <div ref={this.elementRef}><Tabs defaultActiveKey="1">
          {viewTabs.map((e) => {
            if (e === "Actual &  Operation View") {
              return <TabPane tab={e} key="ao2">
                <Table className="pl-summary-list-view" bordered columns={columns} dataSource={estimateData} pagination={false} rowClassName={(r, i) => (i % 2 === 0 ? "table-striped-listing" : "dull-color table-striped-listing")} />
                <Row gutter={16} className="m-t-18">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <FormItem label="Remark" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} style={{ margin: '10px 0 0 9px' }}>
                      <div style={{ border: '1px solid blue' }} className="vk">
                        <TextArea placeholder="Remark" autoSize={{ minRows: 6, maxRows: 6 }} />
                      </div>
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>;
            } else if (e === "Estimate View") {
              return <TabPane tab={e} key="ev1">
                <Table className="pl-summary-list-view" bordered columns={columns2} dataSource={estimateData} pagination={false} rowClassName={(r, i) => (i % 2 === 0 ? "table-striped-listing" : "dull-color table-striped-listing")} />
                <Row gutter={16} className="m-t-18">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <FormItem label="Remark" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} style={{ margin: '10px 0 0 9px' }}>
                      <div style={{ border: '1px solid blue' }} className="vk">
                        <TextArea placeholder="Remark" autoSize={{ minRows: 6, maxRows: 6 }} />
                      </div>
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>;
            } else if (e === "Account View") {
              return <TabPane tab={e} key="av3">
                Accounts
              </TabPane>;
            }
          })}
        </Tabs></div>) : (<div className="col col-lg-12">
          <Spin tip="Loading...">
            <Alert message=" " description="Please wait..." type="info" />
          </Spin>
        </div>)}
    </>;
  }
}

export default PL;
