import React, { Component } from "react";
import { Table, Tabs, Input, Row, Col, Form, Spin, Alert } from "antd";
// import ToolbarUI from 'components/ToolbarUI';

import ToolbarUI from "../../../../components/ToolbarUI";
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;

const columns = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Invoice No.",
    dataIndex: "invoice_no",
    key: "invoice_no",
    width: "8%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "8%",
  },
  {
    title: "Actual",
    dataIndex: "actual",
    key: "actual",
    width: "8%",
  },
  {
    title: "Posted",
    dataIndex: "posted",
    key: "posted",
    width: "8%",
  },
  {
    title: "Cash In",
    dataIndex: "cash_in",
    key: "cash_in",
    width: "8%",
  },
  {
    title: "Est. Vs Act",
    dataIndex: "variance",
    key: "variance",
    width: "8%",
  },
  {
    title: "% Est. Vs Act",
    dataIndex: "per",
    key: "per",
    width: "8%",
  },
  {
    title: "Post Vs Cash",
    dataIndex: "sec_variance",
    key: "sec_variance",
    width: "8%",
  },
  {
    title: "% Post Vs Cash",
    dataIndex: "sec_per",
    key: "sec_per",
    width: "8%",
  },
];

const columns2 = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "12%",
    align: "right",
  },
  { title: "", dataIndex: "", key: "blank", width: "68%" },
];

class PL extends Component {
  callback = (evt) => {};

  constructor(props) {
    //console.log('props PL:',props);
    super(props);

    this.state = {
      dollarUSLocale: Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      formData: this.props.formData || {},
      showPL: false,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || [
        "Estimate View",
        "Actual &  Operation View",
        "Account View",
      ],
      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: 0,
          children: [
            { key: "rev10", description: "Gross TCO Hire $", estimate: 0 },
            {
              key: "rev11",
              description: "Gross TCO Hire  B. Comm",
              estimate: 0,
            },
            {
              key: "rev12",
              description: "Gross TCO Hire add Com",
              estimate: 0,
            },
            { key: "rev13", description: "Gross TCO BB", estimate: 0 },
            { key: "rev14", description: "Gross TCO BB Comm", estimate: 0 },
            { key: "rev15", description: "Net Ballast Bonus", estimate: 0 },
            { key: "rev16", description: "Misc. Revenue", estimate: 0 },
            { key: "rev17", description: "Total TCO Net Hire", estimate: 0 },
            { key: "rev18", description: "Total Net TCO BB", estimate: 0 },
            { key: "rev19", description: "Gross Revenue", estimate: 0 },
            { key: "rev20", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses",
          estimate: 0,
          children: [
            {
              key: "ex10",
              description: "Vessel expenses",
              estimate: 0,
              children: [
                { key: "ex100", description: "TCI Hire Expenses", estimate: 0 },
                { key: "ex101", description: "TCI Add. Comm.", estimate: 0 },
                { key: "ex102", description: "TCI Bro. Comm.", estimate: 0 },
                { key: "ex103", description: "TCI Ballst Bonus", estimate: 0 },
                { key: "ex104", description: "TCI BB Comm", estimate: 0 },
                { key: "ex105", description: "Reposition Cost", estimate: 0 },
                { key: "ex106", description: "Emission Expense", estimate: 0 },
              ],
            },

            { key: "ex11", description: "Port Expenses", estimate: 0 },
            { key: "ex12", description: "Other Expenses", estimate: 0 },

            {
              key: "ex13",
              description: "Bunker Expenses",
              estimate: 0,
              children: [
                {
                  key: "ex130",
                  description: "Sea Consp Non ECA",
                  estimate: 0,
                  children: [
                    { key: "ex1300", description: "IFO", estimate: 0 },
                    { key: "ex1301", description: "VLSFO", estimate: 0 },
                    { key: "ex1302", description: "LSMGO", estimate: 0 },
                    { key: "ex1303", description: "MGO", estimate: 0 },
                    { key: "ex1304", description: "ULSFO", estimate: 0 },
                  ],
                },
                {
                  key: "ex131",
                  description: "Sea Consp ECA",
                  estimate: 0,
                  children: [
                    { key: "ex1310", description: "IFO", estimate: 0 },
                    { key: "ex1311", description: "VLSFO", estimate: 0 },
                    { key: "ex1312", description: "LSMGO", estimate: 0 },
                    { key: "ex1313", description: "MGO", estimate: 0 },
                    { key: "ex1314", description: "ULSFO", estimate: 0 },
                  ],
                },
                {
                  key: "ex132",
                  //description: "Port Consp None ECA",
                  description: "Port Consp",
                  estimate: 0,
                  children: [
                    { key: "ex1320", description: "IFO", estimate: 0 },
                    { key: "ex1321", description: "VLSFO", estimate: 0 },
                    { key: "ex1322", description: "LSMGO", estimate: 0 },
                    { key: "ex1323", description: "MGO", estimate: 0 },
                    { key: "ex1324", description: "ULSFO", estimate: 0 },
                  ],
                },
                // {
                //   key: "ex133",
                //   description: "Port Consp ECA",
                //   estimate: 0,
                //   children: [
                //     { key: "ex1330", description: "IFO", estimate: 0 },
                //     { key: "ex1331", description: "VLSFO", estimate: 0 },
                //     { key: "ex1332", description: "LSMGO", estimate: 0 },
                //     { key: "ex1333", description: "MGO", estimate: 0 },
                //     { key: "ex1334", description: "ULSFO", estimate: 0 },
                //   ],
                // },
              ],
            },

            { key: "ex14", description: "Gross Expenses", estimate: 0 },
            { key: "ex15", description: "Net Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr10", description: "Profit (Loss)", estimate: 0 },
            { key: "vr11", description: "Daily Profit (Loss)", estimate: 0 },

            { key: "vr12", description: "CO2 Cost", estimate: 0 },
            {
              key: "vr13",
              description: "CO2 Adjusted Profit (Loss)",
              estimate: 0,
            },
            { key: "vr14", description: "CO2 Adjusted (Net) TCE", estimate: 0 },
            { key: "vr15", description: "Total Sea Days", estimate: 0 },
            { key: "vr16", description: "Total Port Days", estimate: 0 },
            { key: "vr17", description: "Net Voyage Days", estimate: 0 },
          ],
        },
      ],
    };
  }

  dollarFormatedValue = (value) => {
    const { dollarUSLocale } = this.state;
    return isNaN(value)
      ? "0.00"
      : dollarUSLocale.format(parseFloat(value).toFixed(2));
  };

  DemurrageDespatchCalculation = (itenaryarr) => {
    let demurrage = 0;
    let despatch = 0;
    itenaryarr.forEach((el) => {
      if (el.dem_disp === "Despatch") {
        despatch += parseFloat(el.dem_disp_amt);
      } else if (el.dem_disp === "Demmurage") {
        demurrage += parseFloat(el.dem_disp_amt);
      }
    });
    return { demurrage, despatch };
  };

  findCpPassage = (fuelType, passageType, consArr) => {
    let cp_price = 0;
    let fuel_cons = 0;
    consArr?.map((el) => {
      const { fuel_type, fuel_code } = el;
      if (fuel_type == fuelType) {
        cp_price = parseFloat(el.cp_price);
        if (passageType == 1) {
          fuel_cons = isNaN(el?.ballast_value)
            ? 0
            : parseFloat(el.ballast_value);
        } else if (passageType == 2) {
          fuel_cons = isNaN(el?.laden_value) ? 0 : parseFloat(el.laden_value);
        }
      }
    });

    cp_price = isNaN(cp_price) ? 0 : parseFloat(cp_price);
    fuel_cons = isNaN(fuel_cons) ? 0 : parseFloat(fuel_cons);
    return { cp_price, fuel_cons };
  };

  EcaSeaconsCalculation = (formdata) => {
    let ecaSeaCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    let fuelType = formdata?.["eca_fuel_grade"] ?? 7;
    let ecafuelConsPrice = 0;

    formdata?.portitinerary?.map((el) => {
      const { eca_days, passage } = el;
      const { cp_price, fuel_cons } = this.findCpPassage(
        fuelType,
        passage,
        formdata["."]
      );

      ecafuelConsPrice += cp_price * eca_days * fuel_cons;
    });

    switch (fuelType) {
      case "3": // IFO
        ecaSeaCons["ifo"] = ecafuelConsPrice;
        break;

      case "4": // MGO
        ecaSeaCons["mgo"] = ecafuelConsPrice;
        break;

      case "5": //Vlsfo
        ecaSeaCons["vlsfo"] = ecafuelConsPrice;
        break;

      case "7": // lsmgo
        ecaSeaCons["lsmgo"] = ecafuelConsPrice;
        break;

      case "10": // ulsfo
        ecaSeaCons["ulsfo"] = ecafuelConsPrice;
        break;
      case "11": // HFO
        ecaSeaCons["hfo"] = ecafuelConsPrice;
        break;
    }

    return ecaSeaCons;
  };

  nonEcaSeaconsCalculation = (formdata) => {
    let nonEcaSeaCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    let fuelType = formdata?.["eca_fuel_grade"];

    formdata?.portitinerary?.map((el) => {
      const { eca_days, passage, tsd } = el;
      let nonEcadays = tsd;

      switch (fuelType) {
        case "3":
          nonEcadays = tsd - eca_days;
          break;

        case "4":
          nonEcadays = tsd - eca_days;
          break;

        case "7":
          nonEcadays = tsd - eca_days;
          break;

        case "5":
          nonEcadays = tsd - eca_days;
          break;

        case "10":
          nonEcadays = tsd - eca_days;
          break;

        default:
          return nonEcadays;
      }

      let nonifo = this.findCpPassage(3, passage, formdata["."]);
      let nonmgo = this.findCpPassage(4, passage, formdata["."]);
      let nonlsmgo = this.findCpPassage(7, passage, formdata["."]);
      let nonvlsfo = this.findCpPassage(5, passage, formdata["."]);
      let nonulsfo = this.findCpPassage(10, passage, formdata["."]);

      if (fuelType == 3) {
        nonEcaSeaCons["ifo"] += nonifo.cp_price * nonEcadays * nonifo.fuel_cons;
      } else {
        nonEcaSeaCons["ifo"] += nonifo.cp_price * tsd * nonifo.fuel_cons;
      }

      if (fuelType == 4) {
        nonEcaSeaCons["mgo"] += nonmgo.cp_price * nonEcadays * nonmgo.fuel_cons;
      } else {
        nonEcaSeaCons["mgo"] += nonmgo.cp_price * tsd * nonmgo.fuel_cons;
      }

      if (fuelType == 7) {
        nonEcaSeaCons["lsmgo"] +=
          nonlsmgo?.cp_price * nonEcadays * nonlsmgo?.fuel_cons;
      } else {
        nonEcaSeaCons["lsmgo"] +=
          nonlsmgo?.cp_price * tsd * nonlsmgo?.fuel_cons;
      }

      if (fuelType == 5) {
        nonEcaSeaCons["vlsfo"] +=
          nonvlsfo?.cp_price * nonEcadays * nonvlsfo?.fuel_cons;
      } else {
        nonEcaSeaCons["vlsfo"] +=
          nonvlsfo?.cp_price * tsd * nonvlsfo?.fuel_cons;
      }

      if (fuelType == 10) {
        nonEcaSeaCons["ulsfo"] +=
          nonulsfo?.cp_price * nonEcadays * nonulsfo?.fuel_cons;
      } else {
        nonEcaSeaCons["ulsfo"] +=
          nonulsfo?.cp_price * tsd * nonulsfo?.fuel_cons;
      }
    });

    return nonEcaSeaCons;
  };

  nonEcaPortConsCalculation = (formdata) => {
    let nonEcaPortCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    formdata?.bunkerdetails?.map((el) => {
      nonEcaPortCons["ifo"] += isNaN(el["pc_ifo"])
        ? 0
        : parseFloat(el["pc_ifo"]);
      nonEcaPortCons["mgo"] += isNaN(el["pc_mgo"])
        ? 0
        : parseFloat(el["pc_mgo"]);
      nonEcaPortCons["lsmgo"] += isNaN(el["pc_lsmgo"])
        ? 0
        : parseFloat(el["pc_lsmgo"]);
      nonEcaPortCons["vlsfo"] += isNaN(el["pc_vlsfo"])
        ? 0
        : parseFloat(el["pc_vlsfo"]);
      nonEcaPortCons["ulsfo"] += isNaN(el["pc_ulsfo"])
        ? 0
        : parseFloat(el["pc_ulsfo"]);
    });
    return nonEcaPortCons;
  };

  totalEcaSecafuelCons = (consObj) => {
    let fuelValue = Object.values(consObj);
    return fuelValue.reduce((ac, el) => ac + el, 0);
  };

  ecaPortConsCalculation = () => {
    let ecaPortCons = {
      ifo: 0,
      mgo: 0,
      lsmgo: 0,
      vlsfo: 0,
      ulsfo: 0,
    };

    // till now we dont have any update on this, how to calculate.

    return ecaPortCons;
  };

  __pl = () => {
    let { estimateData, dollarUSLocale, formData } = this.state;

    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      totalArriveConsumption = 0,
      totalDepConsumption = 0,
      totalAdjConsumption = 0,
      colName = "estimate";
    let tco_cove1 = 0,
      tco_ilhoc = 0,
      grossRevenue = 0,
      netRevenue = 0;
    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      tci_hire = 0,
      demmurage = 0,
      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      totalExpenses = 0,
      cpQty = 0,
      bunkerExpense = 0;
    if (formData.hasOwnProperty("estimatePL")) {
      colName = "actual";
    }

    //total voyage days

    let totalVoyageDays = formData["tot_voy_days"]
      ? isNaN(("" + formData["tot_voy_days"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["tot_voy_days"]).replaceAll(",", "") * 1
      : 0;

    let tco_total_port_expense = 0;
    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portItinerary = formData["portitinerary"];
      portItinerary.map((e) => {
        // tsd += (e.tsd + "").replaceAll(",", "") * 1;
        // tpd += (e.t_port_days + "").replaceAll(",", "") * 1;
        // tco_total_port_expense += (e.p_exp + "").replaceAll(",", "") * 1

        tsd += e.tsd ? (e.tsd + "").replaceAll(",", "") * 1 : 0;
        tpd += e.t_port_days ? (e.t_port_days + "").replaceAll(",", "") * 1 : 0;
        tco_total_port_expense += e.p_exp
          ? (e.p_exp + "").replaceAll(",", "") * 1
          : 0;
      });

      tsd = tsd * 1;
      tpd = tpd * 1;
      tco_total_port_expense = tco_total_port_expense * 1;
    }

    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;

    //Revenue tcto

    let tco_hire =
      parseFloat(
        (formData?.tcoterm?.[0]?.dailyhire || "").replaceAll(",", "")
      ) || 0;
    let duration =
      parseFloat(
        (formData?.tcoterm?.[0]?.duration || "").replaceAll(",", "")
      ) || 0;

    tco_hire = tco_hire * duration;

    let bro_percentage_tco =
      parseFloat((formData?.tcoterm?.[0]?.bcom || "").replaceAll(",", "")) || 0;
    bro_percentage_tco = tco_hire * bro_percentage_tco * 0.01;

    let tco_add_percentage =
      parseFloat((formData?.tcoterm?.[0]?.acom || "").replaceAll(",", "")) || 0;
    tco_add_percentage = tco_hire * tco_add_percentage * 0.01;

    let tco_bb =
      parseFloat((formData?.tcoterm?.[0]?.bb || "").replaceAll(",", "")) || 0;

    let tco_bb_percentage = tco_bb * (tco_add_percentage + bro_percentage_tco);

    // newly added
    let tco_add_value =
      parseFloat((formData?.tcoterm?.[0]?.acom || "").replaceAll(",", "")) || 0;
    let tco_bro_value =
      parseFloat((formData?.tcoterm?.[0]?.bcom || "").replaceAll(",", "")) || 0;
    let gross_tco_bb_comm =
      tco_bb * tco_add_value * 0.01 + tco_bb * tco_bro_value * 0.01;

    //let tco_net_Blast_Revenue = tco_bb - tco_bb_percentage;
    let tco_net_Blast_Revenue = tco_bb - gross_tco_bb_comm;
    let tco_misc_revenue = 0; // 0 as for now it is not clear
    let total_tco_net_hire = tco_hire;
    let total_net_tco_bb = tco_bb;

    let tco_gross_revenue =
      tco_hire +
      bro_percentage_tco +
      tco_add_percentage +
      tco_bb +
      tco_bb_percentage +
      tco_net_Blast_Revenue +
      tco_misc_revenue +
      total_tco_net_hire +
      total_net_tco_bb;
    let tco_net_revenue =
      tco_gross_revenue -
      (bro_percentage_tco + tco_add_percentage + tco_bb_percentage);

    let other_rev = formData["other_rev"]
      ? (formData["other_rev"] + "").replaceAll(",", "") * 1
      : 0;
    let portExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      seaExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivalrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      deprev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivaladjuestmentrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 };

    if (formData && formData.hasOwnProperty("bunkerdetails")) {
      let bunkerDetails = formData["bunkerdetails"];
      bunkerDetails.map((e, i, { length }) => {
        seaExpenses["ifo"] += e["ifo"]
          ? isNaN(("" + e["ifo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["ifo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["mgo"] += e["mgo"]
          ? isNaN(("" + e["mgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["mgo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["vlsfo"] += e["vlsfo"]
          ? isNaN(("" + e["vlsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["vlsfo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["lsmgo"] += e["lsmgo"]
          ? isNaN(("" + e["lsmgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["lsmgo"]).replaceAll(",", "") * 1
          : 0;
        seaExpenses["ulsfo"] += e["ulsfo"]
          ? isNaN(("" + e["ulsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["ulsfo"]).replaceAll(",", "") * 1
          : 0;

        portExpenses["ifo"] += e["pc_ifo"]
          ? isNaN(("" + e["pc_ifo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_ifo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["mgo"] += e["pc_mgo"]
          ? isNaN(("" + e["pc_mgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_mgo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["vlsfo"] += e["pc_vlsfo"]
          ? isNaN(("" + e["pc_vlsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_vlsfo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["lsmgo"] += e["pc_lsmgo"]
          ? isNaN(("" + e["pc_lsmgo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_lsmgo"]).replaceAll(",", "") * 1
          : 0;
        portExpenses["ulsfo"] += e["pc_ulsfo"]
          ? isNaN(("" + e["pc_ulsfo"]).replaceAll(",", "") * 1)
            ? 0
            : ("" + e["pc_ulsfo"]).replaceAll(",", "") * 1
          : 0;

        if (i == 0) {
          arrivalrev["ifo"] += e["arob_ifo"]
            ? isNaN(("" + e["arob_ifo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["arob_ifo"]).replaceAll(",", "") * 1
            : 0;
          arrivalrev["mgo"] += e["arob_mgo"]
            ? isNaN(("" + e["arob_mgo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["arob_mgo"]).replaceAll(",", "") * 1
            : 0;
          arrivalrev["vlsfo"] += e["arob_vlsfo"]
            ? isNaN(("" + e["arob_vlsfo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["arob_vlsfo"]).replaceAll(",", "") * 1
            : 0;
          arrivalrev["lsmgo"] += e["arob_lsmgo"]
            ? isNaN(("" + e["arob_lsmgo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["arob_lsmgo"]).replaceAll(",", "") * 1
            : 0;
          arrivalrev["ulsfo"] += e["arob_ulsfo"]
            ? isNaN(("" + e["arob_ulsfo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["arob_ulsfo"]).replaceAll(",", "") * 1
            : 0;
        }
        //last element
        if (i + 1 === length) {
          deprev["ifo"] += e["dr_ifo"]
            ? isNaN(("" + e["dr_ifo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["dr_ifo"]).replaceAll(",", "") * 1
            : 0;
          deprev["mgo"] += e["dr_mgo"]
            ? isNaN(("" + e["dr_mgo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["dr_mgo"]).replaceAll(",", "") * 1
            : 0;
          deprev["vlsfo"] += e["dr_vlsfo"]
            ? isNaN(("" + e["dr_vlsfo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["dr_vlsfo"]).replaceAll(",", "") * 1
            : 0;
          deprev["lsmgo"] += e["dr_lsmgo"]
            ? isNaN(("" + e["dr_lsmgo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["dr_lsmgo"]).replaceAll(",", "") * 1
            : 0;
          deprev["ulsfo"] += e["dr_ulsfo"]
            ? isNaN(("" + e["dr_ulsfo"]).replaceAll(",", "") * 1)
              ? 0
              : ("" + e["dr_ulsfo"]).replaceAll(",", "") * 1
            : 0;
        }
      });
    }

    if (formData && formData.hasOwnProperty(".")) {
      let cpData = formData["."];
      let price_cp_eco =
        formData.hasOwnProperty("price_cp_eco") && formData["price_cp_eco"]
          ? 1
          : 0; // 1 is P$ else CP$
      cpData.map((e) => {
        let _price = 0;
        let _pprice = 0;
        if (
          e["cp_price"] &&
          !isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)
        ) {
          _price = ("" + e["cp_price"]).replaceAll(",", "") * 1;
        }
        if (
          e["purchase_price"] &&
          !isNaN(("" + e["purchase_price"]).replaceAll(",", "") * 1)
        ) {
          _pprice = ("" + e["purchase_price"]).replaceAll(",", "") * 1;
        }

        switch (e.fuel_code) {
          case "IFO":
            seaExpenses["ifo"] =
              price_cp_eco == 1
                ? seaExpenses["ifo"] * _pprice
                : seaExpenses["ifo"] * _price;
            portExpenses["ifo"] =
              price_cp_eco == 1
                ? portExpenses["ifo"] * _pprice
                : portExpenses["ifo"] * _price;
            arrivalrev["ifo"] =
              price_cp_eco == 1
                ? arrivalrev["ifo"] * _pprice
                : arrivalrev["ifo"] * _price;
            deprev["ifo"] =
              price_cp_eco == 1
                ? deprev["ifo"] * _pprice
                : deprev["ifo"] * _price;
            arrivaladjuestmentrev["ifo"] = deprev["ifo"] - arrivalrev["ifo"];

            break;
          case "MGO":
            seaExpenses["mgo"] =
              price_cp_eco == 1
                ? seaExpenses["mgo"] * _pprice
                : seaExpenses["mgo"] * _price;
            portExpenses["mgo"] =
              price_cp_eco == 1
                ? portExpenses["mgo"] * _pprice
                : portExpenses["mgo"] * _price;
            arrivalrev["mgo"] =
              price_cp_eco == 1
                ? arrivalrev["mgo"] * _pprice
                : arrivalrev["mgo"] * _price;
            deprev["mgo"] =
              price_cp_eco == 1
                ? deprev["mgo"] * _pprice
                : deprev["mgo"] * _price;
            arrivaladjuestmentrev["mgo"] = deprev["mgo"] - arrivalrev["mgo"];
            break;

          case "VLSFO":
            seaExpenses["vlsfo"] =
              price_cp_eco == 1
                ? seaExpenses["vlsfo"] * _pprice
                : seaExpenses["vlsfo"] * _price;
            portExpenses["vlsfo"] =
              price_cp_eco == 1
                ? portExpenses["vlsfo"] * _pprice
                : portExpenses["vlsfo"] * _price;
            arrivalrev["vlsfo"] =
              price_cp_eco == 1
                ? arrivalrev["vlsfo"] * _pprice
                : arrivalrev["vlsfo"] * _price;
            deprev["vlsfo"] =
              price_cp_eco == 1
                ? deprev["vlsfo"] * _pprice
                : deprev["vlsfo"] * _price;
            arrivaladjuestmentrev["vlsfo"] =
              deprev["vlsfo"] - arrivalrev["vlsfo"];
            break;

          case "LSMGO":
            seaExpenses["lsmgo"] =
              price_cp_eco == 1
                ? seaExpenses["lsmgo"] * _pprice
                : seaExpenses["lsmgo"] * _price;
            portExpenses["lsmgo"] =
              price_cp_eco == 1
                ? portExpenses["lsmgo"] * _pprice
                : portExpenses["lsmgo"] * _price;
            arrivalrev["lsmgo"] =
              price_cp_eco == 1
                ? arrivalrev["lsmgo"] * _pprice
                : arrivalrev["lsmgo"] * _price;
            deprev["lsmgo"] =
              price_cp_eco == 1
                ? deprev["lsmgo"] * _pprice
                : deprev["lsmgo"] * _price;
            arrivaladjuestmentrev["lsmgo"] =
              deprev["lsmgo"] - arrivalrev["lsmgo"];
            break;

          case "ULSFO":
            seaExpenses["ulsfo"] =
              price_cp_eco == 1
                ? (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) *
                  _pprice
                : (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) *
                  _price;
            portExpenses["ulsfo"] =
              price_cp_eco == 1
                ? (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) *
                  _pprice
                : (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) *
                  _price;
            arrivalrev["ulsfo"] =
              price_cp_eco == 1
                ? (isNaN(arrivalrev["ulsfo"]) ? 0 : arrivalrev["ulsfo"]) *
                  _pprice
                : (isNaN(arrivalrev["ulsfo"]) ? 0 : arrivalrev["ulsfo"]) *
                  _price;
            deprev["ulsfo"] =
              price_cp_eco == 1
                ? (isNaN(deprev["ulsfo"]) ? 0 : deprev["ulsfo"]) * _pprice
                : (isNaN(deprev["ulsfo"]) ? 0 : deprev["ulsfo"]) * _price;
            arrivaladjuestmentrev["ulsfo"] =
              deprev["ulsfo"] - arrivalrev["ulsfo"];

            break;
        }
      });
    }

    Object.keys(seaExpenses).map(
      (e) => (totalSeaConsumption += seaExpenses[e])
    );
    Object.keys(portExpenses).map(
      (e) => (totalPortConsumption += portExpenses[e])
    );
    Object.keys(arrivalrev).map(
      (e) => (totalArriveConsumption += arrivalrev[e])
    );
    Object.keys(deprev).map((e) => (totalDepConsumption += deprev[e]));
    Object.keys(arrivaladjuestmentrev).map(
      (e) => (totalAdjConsumption += arrivaladjuestmentrev[e])
    );

    //grossRevenue = tco_hire + tco_bb + other_rev;
    //grossRevenue = grossRevenue.toFixed(2)
    //netRevenue = grossRevenue - (bro_percentage_tco + tco_add_percentage);
    //netRevenue = netRevenue.toFixed(2)

    // expensess

    tci_hire = formData["tci_d_hire"]
      ? isNaN(("" + formData["tci_d_hire"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["tci_d_hire"]).replaceAll(",", "") * 1
      : 0;
    let tci_add_com = formData["add_percentage_tci"]
      ? isNaN(("" + formData["add_percentage_tci"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["add_percentage_tci"]).replaceAll(",", "") * 1
      : 0;
    let add_per_tci_amt = tci_hire * totalVoyageDays * tci_add_com * 0.01;
    add_per_tci_amt = parseFloat(add_per_tci_amt.toFixed(2));

    let bro_percentage_tci = formData["bro_percentage_tci"]
      ? isNaN(("" + formData["bro_percentage_tci"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["bro_percentage_tci"]).replaceAll(",", "") * 1
      : 0;

    let bro_per_tci_amt =
      tci_hire * totalVoyageDays * bro_percentage_tci * 0.01;
    bro_per_tci_amt = parseFloat(bro_per_tci_amt.toFixed(2));
    let bb = formData["bb"]
      ? isNaN(("" + formData["bb"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["bb"]).replaceAll(",", "") * 1
      : 0;
    let misCost = formData["mis_cost"]
      ? isNaN(("" + formData["mis_cost"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["mis_cost"]).replaceAll(",", "") * 1
      : 0;

    let addPercentage = formData["add_percentage"]
      ? (formData["add_percentage"] + "").replaceAll(",", "") * 1
      : 0;

    let broPercentage = formData["bro_percentage"]
      ? (formData["bro_percentage"] + "").replaceAll(",", "") * 1
      : 0;

    let tci_bb_comm = bb * (bro_per_tci_amt + add_per_tci_amt);

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portitinerary = formData["portitinerary"];
      portitinerary.map(
        (e) =>
          (pi += isNaN(("" + e.p_exp).replaceAll(",", "") * 1)
            ? 0
            : ("" + e.p_exp).replaceAll(",", "") * 1)
      );
    }

    let grossexpnse =
      totalSeaConsumption * 1 +
      totalPortConsumption * 1 +
      //  + add_per_tci_amt*1 + bro_per_tci_amt*1
      totalVoyageDays * tci_hire * 1 +
      bb * 1 +
      pi * 1 +
      misCost * 1;

    grossexpnse = grossexpnse.toFixed(2);
    totalExpenses = grossexpnse - (add_per_tci_amt * 1 + bro_per_tci_amt * 1);

    if (formData.hasOwnProperty("estimatePL")) {
      estimateData = this.__getEstimatePL(this.state.formData["estimatePL"]);
    }

    let ecaSeacons = this.EcaSeaconsCalculation(formData);
    let nonEcaSeacons = this.nonEcaSeaconsCalculation(formData);
    let nonecaPortcons = this.nonEcaPortConsCalculation(formData);
    let ecaPortCons = this.ecaPortConsCalculation(formData);

    let totalecaSeacons = this.totalEcaSecafuelCons(ecaSeacons);

    let totalnonEcaSeacons = this.totalEcaSecafuelCons(nonEcaSeacons);
    let totalnonecaPortcons = this.totalEcaSecafuelCons(nonecaPortcons);
    let totalecaPortCons = this.totalEcaSecafuelCons(ecaPortCons);

    bunkerExpense =
      totalecaSeacons +
      totalnonEcaSeacons +
      totalnonecaPortcons +
      totalecaPortCons;

    //revune mapping

    estimateData[0][colName] = dollarUSLocale.format(tco_net_revenue);
    estimateData[0]["children"][0][colName] = dollarUSLocale.format(tco_hire);
    estimateData[0]["children"][1][colName] =
      dollarUSLocale.format(bro_percentage_tco);
    estimateData[0]["children"][2][colName] =
      dollarUSLocale.format(tco_add_percentage);
    estimateData[0]["children"][3][colName] = dollarUSLocale.format(tco_bb);
    // console.log('gross_tco_bb_comm final :',gross_tco_bb_comm)
    estimateData[0]["children"][4][colName] =
      dollarUSLocale.format(gross_tco_bb_comm);
    estimateData[0]["children"][5][colName] = dollarUSLocale.format(
      tco_net_Blast_Revenue
    );

    estimateData[0]["children"][6][colName] =
      dollarUSLocale.format(tco_misc_revenue);

    estimateData[0]["children"][7][colName] =
      dollarUSLocale.format(total_tco_net_hire);
    estimateData[0]["children"][8][colName] =
      dollarUSLocale.format(total_net_tco_bb);
    estimateData[0]["children"][9][colName] =
      dollarUSLocale.format(tco_gross_revenue);
    estimateData[0]["children"][10][colName] =
      dollarUSLocale.format(tco_net_revenue);

    estimateData[1][colName] = dollarUSLocale.format(totalExpenses);

    // Voyage Result Mapping
    let voyage_result = tco_net_revenue - totalExpenses;
    let daily_profit_loss =
      totalVoyageDays !== 0
        ? this.dollarFormatedValue(voyage_result / totalVoyageDays)
        : 0;
    
    let cO2Cost = 0;
    if (formData["-----"]?.inc_pnl === true) {
      cO2Cost = formData["-----"]?.ttl_co2_cost
        ? isNaN(("" + formData["-----"]?.ttl_co2_cost).replaceAll(",", "") * 1)
          ? 0
          : ("" + formData["-----"]?.ttl_co2_cost).replaceAll(",", "") * 1
        : 0;
    }

    let cO2_adjusted_profit_loss = voyage_result - cO2Cost;

    let total_sea_days = dollarUSLocale.format(
      isNaN(tsd * 1) ? 0 : (tsd * 1).toFixed(2)
    );
    let total_port_days = dollarUSLocale.format(
      isNaN(tpd * 1) ? 0 : (tpd * 1).toFixed(2)
    );
    let net_voyage_days = totalVoyageDays;
    let cO2_adjusted_net_tce =
      net_voyage_days !== 0 ? cO2_adjusted_profit_loss / net_voyage_days : 0;

    // --------------- expenses children --------------------------------
    estimateData[1]["children"][0][colName] = dollarUSLocale.format(
      (totalVoyageDays * tci_hire).toFixed(2)
    );
    estimateData[1]["children"][0]["children"][0][colName] =
      dollarUSLocale.format((totalVoyageDays * tci_hire).toFixed(2));
    estimateData[1]["children"][0]["children"][1][colName] =
      dollarUSLocale.format(add_per_tci_amt);
    estimateData[1]["children"][0]["children"][2][colName] =
      dollarUSLocale.format(bro_per_tci_amt);
    estimateData[1]["children"][0]["children"][3][colName] =
      dollarUSLocale.format(bb);
    estimateData[1]["children"][0]["children"][4][colName] =
      this.dollarFormatedValue(tci_bb_comm);

    //Reposition Cost
    //estimateData[1]["children"][0]["children"][5][colName] = dollarUSLocale.format();
    estimateData[1]["children"][0]["children"][6][colName] =
      dollarUSLocale.format(cO2Cost);

    estimateData[1]["children"][1][colName] = this.dollarFormatedValue(
      tco_total_port_expense
    );
    estimateData[1]["children"][2][colName] = dollarUSLocale.format(misCost);

    estimateData[1]["children"][3][colName] =
      this.dollarFormatedValue(bunkerExpense);
    // non ecA SEA CONS
    estimateData[1]["children"][3]["children"][0][colName] =
      this.dollarFormatedValue(totalnonEcaSeacons);
    estimateData[1]["children"][3]["children"][0]["children"][0][colName] =
      this.dollarFormatedValue(nonEcaSeacons.ifo);
    estimateData[1]["children"][3]["children"][0]["children"][1][colName] =
      this.dollarFormatedValue(nonEcaSeacons.vlsfo);
    estimateData[1]["children"][3]["children"][0]["children"][2][colName] =
      this.dollarFormatedValue(nonEcaSeacons.lsmgo);
    estimateData[1]["children"][3]["children"][0]["children"][3][colName] =
      this.dollarFormatedValue(nonEcaSeacons.mgo);
    estimateData[1]["children"][3]["children"][0]["children"][4][colName] =
      this.dollarFormatedValue(nonEcaSeacons.ulsfo);
    // ecA SEA CONS
    estimateData[1]["children"][3]["children"][1][colName] =
      this.dollarFormatedValue(totalecaSeacons);
    estimateData[1]["children"][3]["children"][1]["children"][0][colName] =
      this.dollarFormatedValue(ecaSeacons.ifo);
    estimateData[1]["children"][3]["children"][1]["children"][1][colName] =
      this.dollarFormatedValue(ecaSeacons.vlsfo);
    estimateData[1]["children"][3]["children"][1]["children"][2][colName] =
      this.dollarFormatedValue(ecaSeacons.lsmgo);
    estimateData[1]["children"][3]["children"][1]["children"][3][colName] =
      this.dollarFormatedValue(ecaSeacons.mgo);
    estimateData[1]["children"][3]["children"][1]["children"][4][colName] =
      this.dollarFormatedValue(ecaSeacons.ulsfo);

    // non ecA PORT CONS
    estimateData[1]["children"][3]["children"][2][colName] =
      this.dollarFormatedValue(totalnonecaPortcons);
    estimateData[1]["children"][3]["children"][2]["children"][0][colName] =
      this.dollarFormatedValue(nonecaPortcons.ifo);
    estimateData[1]["children"][3]["children"][2]["children"][1][colName] =
      this.dollarFormatedValue(nonecaPortcons.vlsfo);
    estimateData[1]["children"][3]["children"][2]["children"][2][colName] =
      this.dollarFormatedValue(nonecaPortcons.lsmgo);
    estimateData[1]["children"][3]["children"][2]["children"][3][colName] =
      this.dollarFormatedValue(nonecaPortcons.mgo);
    estimateData[1]["children"][3]["children"][2]["children"][4][colName] =
      this.dollarFormatedValue(nonecaPortcons.ulsfo);
    // ecA PORT CONS
    // estimateData[1]["children"][3]["children"][3][colName] =
    //   this.dollarFormatedValue(totalecaPortCons);
    // estimateData[1]["children"][3]["children"][3]["children"][0][colName] =
    //   this.dollarFormatedValue(ecaPortCons.ifo);
    // estimateData[1]["children"][3]["children"][3]["children"][1][colName] =
    //   this.dollarFormatedValue(ecaPortCons.vlsfo);
    // estimateData[1]["children"][3]["children"][3]["children"][2][colName] =
    //   this.dollarFormatedValue(ecaPortCons.lsmgo);
    // estimateData[1]["children"][3]["children"][3]["children"][3][colName] =
    //   this.dollarFormatedValue(ecaPortCons.mgo);
    // estimateData[1]["children"][3]["children"][3]["children"][4][colName] =
    //   this.dollarFormatedValue(ecaPortCons.ulsfo);

    estimateData[1]["children"][4][colName] =
      dollarUSLocale.format(grossexpnse);
    estimateData[1]["children"][5][colName] =
      dollarUSLocale.format(totalExpenses);

    /*
    miscexpense =bb+miscost as per new excel formula 
    code updated at 28 aug 2023 by Roushan kumar
    */

    /*----------------------- Voyage Result -------------------------------*/

    estimateData[2][colName] =
      voyage_result >= 0
        ? dollarUSLocale.format(voyage_result.toFixed(2))
        : dollarUSLocale.format((voyage_result * 1).toFixed(2));
    estimateData[2]["children"][0][colName] =
      voyage_result >= 0
        ? dollarUSLocale.format(voyage_result.toFixed(2))
        : dollarUSLocale.format((voyage_result * 1).toFixed(2));
    estimateData[2]["children"][1][colName] = daily_profit_loss;
    estimateData[2]["children"][2][colName] = this.dollarFormatedValue(cO2Cost);
    estimateData[2]["children"][3][colName] = this.dollarFormatedValue(
      cO2_adjusted_profit_loss
    );
    estimateData[2]["children"][4][colName] =
      this.dollarFormatedValue(cO2_adjusted_net_tce);
    estimateData[2]["children"][5][colName] = total_sea_days;
    estimateData[2]["children"][6][colName] = total_port_days;
    estimateData[2]["children"][7][colName] =
      this.dollarFormatedValue(net_voyage_days);
    return estimateData;
  };

  componentDidMount() {
    this.setState(
      {
        ...this.state,
        showPL: false,
        estimateData: this.__pl(),
      },
      () => this.setState({ ...this.state, showPL: true })
    );
  }

  render() {
    const { estimateData, showPL, viewTabs } = this.state;

    return (
      <>
        <div>
          <ToolbarUI routeUrl={"pl-main"} callback={(e) => this.callback(e)} />
        </div>

        {showPL === true ? (
          <Tabs defaultActiveKey="1">
            {viewTabs.map((e) => {
              if (e === "Actual &  Operation View") {
                return (
                  <TabPane tab={e} key="ao2">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Estimate View") {
                return (
                  <TabPane tab={e} key="ev1">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns2}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Account View") {
                return (
                  <TabPane tab={e} key="av3">
                    Accounts
                  </TabPane>
                );
              }
            })}
          </Tabs>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </>
    );
  }
}

export default PL;
