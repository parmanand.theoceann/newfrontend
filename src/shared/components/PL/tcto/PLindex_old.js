import React, { Component } from "react";
import { Table, Tabs, Input, Row, Col, Form, Spin, Alert } from "antd";
// import ToolbarUI from "components/ToolbarUI";

import ToolbarUI from "../../../../components/ToolbarUI";
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;

const columns = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "8%",
  },
  {
    title: "Actual",
    dataIndex: "actual",
    key: "actual",
    width: "8%",
  },
  {
    title: "Posted",
    dataIndex: "posted",
    key: "posted",
    width: "8%",
  },
  {
    title: "Cash In",
    dataIndex: "cash_in",
    key: "cash_in",
    width: "8%",
  },
  {
    title: `Diff`,
    dataIndex: "Diff",
    key: "Diff",
    width: "8%",
  },
  {
    title: "%Diff",
    dataIndex: "perDiff",
    key: "perDiff",
    width: "8%",
  },
  {
    title: "Post Vs Cash",
    dataIndex: "sec_variance",
    key: "sec_variance",
    width: "8%",
  },
  {
    title: "% Post Vs Cash",
    dataIndex: "sec_per",
    key: "sec_per",
    width: "8%",
  },
];

const columns2 = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "12%",
    align: "right",
  },
  { title: "", dataIndex: "", key: "blank", width: "68%" },
];

class PL extends Component {
  callback = (evt) => { };

  constructor(props) {
    super(props);

    this.state = {
      dollarUSLocale: Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      formData: this.props.formData || {},
      estimateDatavalue:this.props.estimateData||{},
      showPL: false,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || [
       
        "Actual &  Operation View",
      
      ],
      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: 0,
          children: [
            { key: "rev11", description: "Freight", estimate: 0 },
            { key: "rev12", description: "Freight Commission", estimate: 0 },
            { key: "rev19", description: "Misc. Revenue", estimate: 0 },
            { key: "rev20", description: "Gross Revenue", estimate: 0 },
            { key: "rev21", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses.",
          estimate: 0,
          children: [
            {
              key: "ex11",
              description: "Vessel expenses",
              estimate: 0,
              children: [
                { key: "ex111", description: "Hire Expenses", estimate: 0 },
                { key: "ex112", description: "Ballast Bonus", estimate: 0 },
                {
                  key: "ex113",
                  description: 'Bunker Adjustment',
                  estimate: 0,
                  children:
                    [
                      { key: "ex1131", description: 'IFO adjustment', estimate: 0 },
                      { key: "ex1132", description: 'VLSFO adjustment', estimate: 0 },
                      { key: "ex1133", description: 'LSMGO adjustment', estimate: 0 },
                      { key: "ex1134", description: 'MGO adjustment', estimate: 0 },
                      { key: "ex1135", description: 'ULSFO adjustment', estimate: 0 }

                    ]
                },
              ],
            },
            { key: "ex12", description: "TCI Add. Comm.", estimate: 0 },
            { key: "ex13", description: "TCI Broker Comm.", estimate: 0 },
            { key: "ex14", description: "Port Expenses", estimate: 0 },
            {
              key: "ex15",
              description: "Bunker Expenses",
              estimate: 0,
              children: [
                {
                  key: "ex151",
                  description: "Sea Consumption",
                  estimate: 0,
                  children: [
                    { key: "ex1511", description: "IFO", estimate: 0 },
                    { key: "ex1512", description: "MGO", estimate: 0 },
                    { key: "ex1513", description: "VLSFO", estimate: 0 },
                    { key: "ex1514", description: "LSMGO", estimate: 0 },
                    { key: "ex1515", description: "ULSFO", estimate: 0 },
                  ],
                },
                {
                  key: "ex152",
                  description: "Port Consumption",
                  estimate: 0,
                  children: [
                    { key: "ex1521", description: "IFO", estimate: 0 },
                    { key: "ex1522", description: "MGO", estimate: 0 },
                    { key: "ex1523", description: "VLSFO", estimate: 0 },
                    { key: "ex1525", description: "ULSFO", estimate: 0 },
                    { key: "ex1524", description: "LSMGO", estimate: 0 },
                  ],
                },
              ],
            },
            { key: "ex17", description: "Misc. Expenses", estimate: 0 },
            // { key: "ex18", description: "Total Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr11", description: "Profit (Loss)", estimate: 0 },
            { key: "vr12", description: "Daily Profit (Loss)", estimate: 0 },
            { key: "vr13", description: "TCE Hire ( Net Daily )", estimate: 0 },
            { key: "vr14", description: "Gross TCE", estimate: 0 },
            { key: "vr15", description: "Freight rate ($/T)", estimate: 0 },
            {
              key: "vr16",
              description: "Breakeven & Freight rate ($/T)",
              estimate: 0,
            },
            // { key: "vr18", description: "Off Hire Days", estimate: 0 },
            { key: "vr19", description: "Total Sea Days", estimate: 0 },
            { key: "vr20", description: "Total Port Days", estimate: 0 },
            { key: "vr17", description: "Net Voyage Days", estimate: 0 },
          ],
        },
      ],
    };
  }

  __getEstimatePL = (calData,colName) => {
 
    const { dollarUSLocale } = this.state;
    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      vesselExpenses = 0;
    let _estimateData = Object.assign([], this.state.estimateData);

    Object.keys(calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"]).map(
      (e) =>
      (totalSeaConsumption +=
        (
          "" + calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"][e]
        ).replaceAll(",", "") * 1)
    );

    Object.keys(calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]).map(
      (e) =>
      (totalPortConsumption +=
        (
          "" + calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"][e]
        ).replaceAll(",", "") * 1)
    );
    Object.keys(calData['estimate']["expenses"]["vessel_expenses"]).map(
      (e) =>
      (vesselExpenses +=
        ("" + calData['estimate']["expenses"]["vessel_expenses"][e]).replaceAll(",", "") *
        1)
    );

   

     _estimateData[0][colName] = calData['estimate']['revenue']["net_revenue"];

     _estimateData[0]["children"][0][colName] = calData['estimate']["revenue"]["freight"];
    _estimateData[0]["children"][1][colName] = calData['estimate']["revenue"]["freight_commission"];
    _estimateData[0]["children"][2][colName] = calData['estimate']["revenue"]["mis_revenue"];
    _estimateData[0]["children"][3][colName] =calData['estimate']["revenue"]["gross_revenue"];
    _estimateData[0]["children"][4][colName] = calData['estimate']["revenue"]["net_revenue"];

    _estimateData[1][colName] = calData['estimate']["expenses"]["total_expenses"];
    let hire_expenses=calData['estimate']["expenses"]["vessel_expenses"]["hire_expenses"]?(calData['estimate']["expenses"]["vessel_expenses"]["hire_expenses"]).replaceAll(",", "") * 1 : 0;
    let ballast_bonus=calData['estimate']["expenses"]["vessel_expenses"]["ballast_bonus"]?(calData['estimate']["expenses"]["vessel_expenses"]["ballast_bonus"]).replaceAll(",", "") * 1 : 0;
   
   let totalvesselExpenses=hire_expenses+ballast_bonus;
    _estimateData[1]["children"][0][colName] = dollarUSLocale.format(totalvesselExpenses);
     _estimateData[1]["children"][0]["children"][0][colName] = calData['estimate']["expenses"]["vessel_expenses"]["hire_expenses"];
    _estimateData[1]["children"][0]["children"][1][colName] =calData['estimate']["expenses"]["vessel_expenses"]["ballast_bonus"];
    _estimateData[1]["children"][1][colName] = calData['estimate']["expenses"]["tci_add_comm"];
    _estimateData[1]["children"][2][colName] =calData['estimate']["expenses"]["tci_broker_comm"];
     _estimateData[1]["children"][3][colName] = calData['estimate']["expenses"]["port_expenses"];
    _estimateData[1]["children"][4][colName] = dollarUSLocale.format(totalPortConsumption + totalSeaConsumption);
    _estimateData[1]["children"][4]["children"][0][colName]= dollarUSLocale.format(totalSeaConsumption);
    _estimateData[1]["children"][4]["children"][0]["children"][0][colName] = calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"]["ifo"];
    _estimateData[1]["children"][4]["children"][0]["children"][1][colName] =calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"]["mgo"];
     _estimateData[1]["children"][4]["children"][0]["children"][2][colName] =calData["estimate"]['expenses']["bunker_expenses"]["sea_consumption"]["vlsfo"];
    _estimateData[1]["children"][4]["children"][0]["children"][3][colName] =calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"]["lsmgo"];
    _estimateData[1]["children"][4]["children"][0]["children"][4][colName] =calData['estimate']["expenses"]["bunker_expenses"]["sea_consumption"]["ulsfo"];
    _estimateData[1]["children"][4]["children"][1][colName]= dollarUSLocale.format(totalPortConsumption);
    _estimateData[1]["children"][4]["children"][1]["children"][0][colName] =calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]["ifo"];
    _estimateData[1]["children"][4]["children"][1]["children"][1][colName] = calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]["mgo"];
    _estimateData[1]["children"][4]["children"][1]["children"][2][colName] = calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]["vlsfo"];
    _estimateData[1]["children"][4]["children"][1]["children"][3][colName] =calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]["ulsfo"];
    _estimateData[1]["children"][4]["children"][1]["children"][4][colName] =calData['estimate']["expenses"]["bunker_expenses"]["port_consumption"]["lsmgo"];
    _estimateData[1]["children"][5][colName] =calData['estimate']["expenses"]["mis_expenses"];
   // _estimateData[1]["children"][6][colName] =calData['estimate']["expenses"]["total_expenses"];

    _estimateData[2][colName] = (calData['estimate']["voyage_result"]["profit_loss"]).replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
    _estimateData[2]["children"][0][colName] = (calData['estimate']["voyage_result"]["profit_loss"]).replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
    _estimateData[2]["children"][1][colName] =calData['estimate']["voyage_result"]["daily_profit_loss"];
    _estimateData[2]["children"][2][colName] =calData['estimate']["voyage_result"]["tce_hire_net_daily"];
    _estimateData[2]["children"][3][colName] = calData['estimate']["voyage_result"]["gross_tce"];
    _estimateData[2]["children"][4][colName] =calData['estimate']["voyage_result"]["freight_rate"];
    _estimateData[2]["children"][5][colName] =calData['estimate']["voyage_result"]["breakeven_and_freight_rate"];
    _estimateData[2]["children"][6][colName] =calData['estimate']["voyage_result"]["total_sea_days"];
    _estimateData[2]["children"][7][colName] =calData['estimate']["voyage_result"]['total_port_days'];
    _estimateData[2]["children"][8][colName] = calData['estimate']["voyage_result"]["net_voyage_days"];
    // _estimateData[2]["children"][9]["estimate"] =
    //   calData["voyage_result"]["total_port_days"];

    return _estimateData;
  };


  __pl = () => {
    let { estimateData, dollarUSLocale, formData,estimateDatavalue } = this.state;
  
    

    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      totalArriveConsumption = 0, totalDepConsumption = 0, totalAdjConsumption = 0;
     
    let  colName="actual"
     if(!formData.hasOwnProperty("estimatePL")) {
   
     estimateData=Object.keys(estimateDatavalue).length>0?this.__getEstimatePL(estimateDatavalue,"estimate"):estimateData;
    }



    if(!formData.hasOwnProperty("showExactPL")){
    
      estimateData=this.__getEstimatePL(formData,"estimate");

    }
  
    let totalVoyageDays = formData["total_days"]
      ? isNaN(("" + formData["total_days"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["total_days"]).replaceAll(",", "") * 1
      : 0;
    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      grossRevenue = 0,
      netRevenue = 0,
      demmurage = 0,
      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      totalExpenses = 0,
      cpQty = 0;
    let bb = formData["bb"]
      ? isNaN(("" + formData["bb"]).replaceAll(",", "") * 1)
        ? 0 : ("" + formData["bb"]).replaceAll(",", "") * 1 : 0;
    let misCost = formData["mis_cost"]
      ? isNaN(("" + formData["mis_cost"]).replaceAll(",", "") * 1) ? 0 :
        ("" + formData["mis_cost"]).replaceAll(",", "") * 1 : 0;
    let hire = formData["tci_d_hire"] ? (formData["tci_d_hire"] + "").replaceAll(",", "") * 1 : 0;
    let addPercentage = formData["add_percentage"] ? (formData["add_percentage"] + "").replaceAll(",", "") * 1 : 0;
    let amt_add_percentage = (hire * totalVoyageDays) * addPercentage * 0.01;

    let broPercentage = formData["bro_percentage"] ? (formData["bro_percentage"] + "").replaceAll(",", "") * 1 : 0;
    let amt_bro_percentage = hire * totalVoyageDays * broPercentage * 0.01;

    let portExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      seaExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivalrev = { 'ifo': 0, 'mgo': 0, 'vlsfo': 0, 'ulsfo': 0, 'lsmgo': 0 },
      deprev = { 'ifo': 0, 'mgo': 0, 'vlsfo': 0, 'ulsfo': 0, 'lsmgo': 0 },
      arrivaladjuestmentrev = { 'ifo': 0, 'mgo': 0, 'vlsfo': 0, 'ulsfo': 0, 'lsmgo': 0 };

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portItinerary = formData["portitinerary"];
      portItinerary.map((e) => (tsd += (e.tsd + "").replaceAll(",", "") * 1));
      tsd = tsd * 1;
    }

    if (formData && formData.hasOwnProperty("portdatedetails")) {
      let portDateDetails = formData["portdatedetails"];
      portDateDetails.map(
        (e) => (tpd += (e.pdays + "").replaceAll(",", "") * 1)
      );
      tpd = tpd * 1;
    }

   
    if (formData && formData.hasOwnProperty("bunkerdetails")) {
      let bunkerDetails = formData["bunkerdetails"];
      let i = 0;
      bunkerDetails.map((e, i, { length }) => {

        seaExpenses["ifo"] += e["ifo"] ? isNaN(("" + e["ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ifo"]).replaceAll(",", "") * 1 : 0;
        seaExpenses["mgo"] += e["mgo"] ? isNaN(("" + e["mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["mgo"]).replaceAll(",", "") * 1 : 0;
        seaExpenses["vlsfo"] += e["vlsfo"] ? isNaN(("" + e["vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["vlsfo"]).replaceAll(",", "") * 1 : 0;
        seaExpenses["lsmgo"] += e["lsmgo"] ? isNaN(("" + e["lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["lsmgo"]).replaceAll(",", "") * 1 : 0;
        seaExpenses["ulsfo"] += e["ulsfo"] ? isNaN(("" + e["ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ulsfo"]).replaceAll(",", "") * 1 : 0;

        portExpenses["ifo"] += e["pc_ifo"] ? isNaN(("" + e["pc_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ifo"]).replaceAll(",", "") * 1 : 0;
        portExpenses["mgo"] += e["pc_mgo"] ? isNaN(("" + e["pc_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_mgo"]).replaceAll(",", "") * 1 : 0;
        portExpenses["vlsfo"] += e["pc_vlsfo"] ? isNaN(("" + e["pc_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_vlsfo"]).replaceAll(",", "") * 1 : 0;
        portExpenses["lsmgo"] += e["pc_lsmgo"] ? isNaN(("" + e["pc_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_lsmgo"]).replaceAll(",", "") * 1 : 0;
        portExpenses["ulsfo"] += e["pc_ulsfo"] ? isNaN(("" + e["pc_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ulsfo"]).replaceAll(",", "") * 1 : 0;

        if (i == 0) {
          arrivalrev['ifo'] += (e['arob_ifo'] ? (isNaN(("" + e['arob_ifo']).replaceAll(',', '') * 1) ? 0 : (("" + e['arob_ifo']).replaceAll(',', '') * 1)) : 0);
          arrivalrev['mgo'] += (e['arob_mgo'] ? (isNaN(("" + e['arob_mgo']).replaceAll(',', '') * 1) ? 0 : (("" + e['arob_mgo']).replaceAll(',', '') * 1)) : 0);
          arrivalrev['vlsfo'] += (e['arob_vlsfo'] ? (isNaN(("" + e['arob_vlsfo']).replaceAll(',', '') * 1) ? 0 : (("" + e['arob_vlsfo']).replaceAll(',', '') * 1)) : 0);
          arrivalrev['lsmgo'] += (e['arob_lsmgo'] ? (isNaN(("" + e['arob_lsmgo']).replaceAll(',', '') * 1) ? 0 : (("" + e['arob_lsmgo']).replaceAll(',', '') * 1)) : 0);
          arrivalrev['ulsfo'] += (e['arob_ulsfo'] ? (isNaN(("" + e['arob_ulsfo']).replaceAll(',', '') * 1) ? 0 : (("" + e['arob_ulsfo']).replaceAll(',', '') * 1)) : 0);
        }
               
        //last element
        if (i + 1 === length) {
          deprev['ifo'] += (e['dr_ifo'] ? (isNaN(("" + e['dr_ifo']).replaceAll(',', '') * 1) ? 0 : (("" + e['dr_ifo']).replaceAll(',', '') * 1)) : 0);
          deprev['mgo'] += (e['dr_mgo'] ? (isNaN(("" + e['dr_mgo']).replaceAll(',', '') * 1) ? 0 : (("" + e['dr_mgo']).replaceAll(',', '') * 1)) : 0);
          deprev['vlsfo'] += (e['dr_vlsfo'] ? (isNaN(("" + e['dr_vlsfo']).replaceAll(',', '') * 1) ? 0 : (("" + e['dr_vlsfo']).replaceAll(',', '') * 1)) : 0);
          deprev['lsmgo'] += (e['dr_lsmgo'] ? (isNaN(("" + e['dr_lsmgo']).replaceAll(',', '') * 1) ? 0 : (("" + e['dr_lsmgo']).replaceAll(',', '') * 1)) : 0);
          deprev['ulsfo'] += (e['dr_ulsfo'] ? (isNaN(("" + e['dr_ulsfo']).replaceAll(',', '') * 1) ? 0 : (("" + e['dr_ulsfo']).replaceAll(',', '') * 1)) : 0);
        }
      });
    }

    if (formData && formData.hasOwnProperty(".")) {
      let cpData = formData["."];
      let price_cp_eco = formData.hasOwnProperty('price_cp_eco') && formData['price_cp_eco'] ? 1 : 0; // 1 is P$ else CP$
      cpData.map((e) => {
        let _price = 0;
        let _pprice = 0
        if (e["cp_price"] && !isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)) {
          _price = ("" + e["cp_price"]).replaceAll(",", "") * 1;
        }
        if (e['purchase_price'] && !isNaN(("" + e['purchase_price']).replaceAll(',', '') * 1)) {
          _pprice = ("" + e['purchase_price']).replaceAll(',', '') * 1;
        }

        switch (e.fuel_code) {
          case "IFO":
            seaExpenses["ifo"] = (price_cp_eco == 1) ? seaExpenses['ifo'] * _pprice : seaExpenses["ifo"] * _price;
            portExpenses["ifo"] = (price_cp_eco == 1) ? portExpenses["ifo"] * _pprice : portExpenses["ifo"] * _price;
            arrivalrev['ifo'] = arrivalrev['ifo'] * _price;
            deprev['ifo'] = deprev['ifo'] * _pprice;
            arrivaladjuestmentrev['ifo'] = deprev['ifo'] - arrivalrev['ifo'];
            break;

          case "MGO":
            seaExpenses["mgo"] = (price_cp_eco == 1) ? seaExpenses["mgo"] * _pprice : seaExpenses["mgo"] * _price;
            portExpenses["mgo"] = (price_cp_eco == 1) ? portExpenses["mgo"] * _pprice : portExpenses["mgo"] * _price;
            arrivalrev['mgo'] = arrivalrev['mgo'] * _price;
            deprev['mgo'] = deprev['mgo'] * _pprice;
            arrivaladjuestmentrev['mgo'] = deprev['mgo'] - arrivalrev['mgo'];
            break;

          case "VLSFO":
            seaExpenses["vlsfo"] = (price_cp_eco == 1) ? seaExpenses["vlsfo"] * _pprice : seaExpenses["vlsfo"] * _price;
            portExpenses["vlsfo"] = (price_cp_eco == 1) ? portExpenses["vlsfo"] * _pprice : portExpenses["vlsfo"] * _price;
            arrivalrev['vlsfo'] = arrivalrev['vlsfo'] * _price;
            deprev['vlsfo'] = deprev['vlsfo'] * _pprice;
            arrivaladjuestmentrev['vlsfo'] = deprev['vlsfo'] - arrivalrev['vlsfo'];
            break;

          case "LSMGO":
            seaExpenses["lsmgo"] = (price_cp_eco == 1) ? seaExpenses["lsmgo"] * _pprice : seaExpenses["lsmgo"] * _price;
            portExpenses["lsmgo"] = (price_cp_eco == 1) ? portExpenses["lsmgo"] * _pprice : portExpenses["lsmgo"] * _price;
            arrivalrev['lsmgo'] = arrivalrev['lsmgo'] * _price;
            deprev['lsmgo'] = deprev['lsmgo'] * _pprice;
            arrivaladjuestmentrev['lsmgo'] = deprev['lsmgo'] - arrivalrev['lsmgo'];
            break;

          case "ULSFO":
            seaExpenses["ulsfo"] = (price_cp_eco == 1) ? (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _pprice : (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _price;
            portExpenses["ulsfo"] = (price_cp_eco == 1) ? (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price : (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price;
            arrivalrev['ulsfo'] = (isNaN(arrivalrev['ulsfo']) ? 0 : arrivalrev['ulsfo']) * _price;
            deprev['ulsfo'] = (isNaN(deprev['ulsfo']) ? 0 : deprev['ulsfo']) * _pprice;
            arrivaladjuestmentrev['ulsfo'] = deprev['ulsfo'] - arrivalrev['ulsfo'];
            break;
        }
      });
    }

    Object.keys(seaExpenses).map(
      (e) => (totalSeaConsumption += seaExpenses[e])
    );
    Object.keys(portExpenses).map(
      (e) => (totalPortConsumption += portExpenses[e])
    );
    Object.keys(arrivalrev).map(e => totalArriveConsumption += arrivalrev[e]);
    Object.keys(deprev).map(e => totalDepConsumption += deprev[e]);
    Object.keys(arrivaladjuestmentrev).map(e => totalAdjConsumption += arrivaladjuestmentrev[e]);
  
    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;
  
    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portitinerary = formData["portitinerary"];
      portitinerary.map(
        (e) =>
        (pi += isNaN(("" + e.p_exp).replaceAll(",", "") * 1)
          ? 0
          : ("" + e.p_exp).replaceAll(",", "") * 1)
      );
    }

    let avgfreightRate = 0, avgfreightRateOpt = 0, frtAvg = 0, cpQtyOpt = 0;
    if (formData && formData.hasOwnProperty("cargos")) {
      let cargos = formData["cargos"];
      cargos.map((e, i) => {
        let frtRate = 0,
          commission = 0, frtAmount = 0;
        let frt_rate = e.freight_rate || e.frat_rate || e.f_rate || 0;
        let cp_qty = e.cp_qty || e.quantity || 0;
        let b_commission = e.b_commission || e.commission || 0;

        let opt_per =
          parseFloat(e.option_percentage) || parseFloat(e.opt_percentage) || 0;
        mr += e.extra_rev ? (e.extra_rev + "").replaceAll(",", "") * 1 : 0;
        cpQty += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
        cpQtyOpt += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;

        if (opt_per !== 0) {

          let _cpq = cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
          let _fr = frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0;
          opt_per = opt_per ? (opt_per + "").replaceAll(",", "") : 0;
          let cpOptQty = (_cpq + ((_cpq * opt_per) / 100));
          cpQtyOpt = cpQtyOpt - _cpq + cpOptQty; //way to use opt% of cargo qty : refactor
          frtAmount = cpOptQty * _fr;
          commission =
            (frtAmount *
              (b_commission
                ? (b_commission + "").replaceAll(",", "") * 1
                : 0)) /
            100;
          frtRate = (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
        } else {
          frtAmount =
            (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
          commission =
            (frtAmount * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          frtRate = frtAmount;
        }

        fr += frtAmount;
        freightCommission += commission;
        frtAvg += frtRate;
        // demmurage += e.dem_rate_pd * 1;  // there is no way to know yet Port days maybe there but not all can be used
      });
      avgfreightRateOpt = fr / cpQtyOpt;
      avgfreightRate = frtAvg / cpQty;
    }
 
    // console.log(fr)
    // console.log(mr)
    // console.log(demmurage)
    
    grossRevenue = fr + freightCommission + mr + (demmurage * totalVoyageDays * -1); // we dont know days of demurrage
    netRevenue =
      grossRevenue - (freightCommission + demmurageCommission) + dispatch; // dispatch is revenue
    totalExpenses =
      totalVoyageDays * hire +
      bb +
      pi +
      misCost +
      amt_add_percentage +
      amt_bro_percentage +
      totalSeaConsumption +
      totalPortConsumption +
      totalAdjConsumption +
      0;


      

    estimateData[0][colName] = dollarUSLocale.format(netRevenue.toFixed(2));
   let reves= estimateData[0]['estimate']?(estimateData[0]['estimate']).replaceAll(",", "") * 1:0;
   let revac= estimateData[0]['actual']?(estimateData[0]['actual']).replaceAll(",","")*1:0;
   estimateData[0]["Diff"]=(revac-reves).toFixed(2);
   estimateData[0]['perDiff']=reves==0?0:(((revac-reves)/reves)*100).toFixed(2);
   estimateData[0]['posted']="N/A";
   estimateData[0]['cash_in']="N/A";
   estimateData[0]['sec_variance']="N/A";
   estimateData[0]['sec_per']="N/A"



    estimateData[0]["children"][0][colName] = dollarUSLocale.format(fr.toFixed(2));
    let frtes= estimateData[0]["children"][0]['estimate']?( estimateData[0]["children"][0]['estimate']).replaceAll(",","")*1:0;
    let frtac= estimateData[0]["children"][0]['actual']?( estimateData[0]["children"][0]['actual']).replaceAll(",","")*1:0;
    estimateData[0]["children"][0]["Diff"]=(frtac-frtes).toFixed(2);
    estimateData[0]["children"][0]["perDiff"]=frtes==0?0:((frtac-frtes)/frtes).toFixed(2);
    estimateData[0]["children"][0]['posted']="N/A";
    estimateData[0]["children"][0]['cash_in']="N/A";
    estimateData[0]["children"][0]['sec_variance']="N/A";
    estimateData[0]["children"][0]['sec_per']="N/A"
 
   


    estimateData[0]["children"][1][colName] = dollarUSLocale.format(freightCommission.toFixed(2));
  let frtcmsnes=estimateData[0]["children"][1]['estimate']?(estimateData[0]["children"][1]['estimate']).replaceAll(",","")*1:0;
  let frtcmsnac=estimateData[0]["children"][1]['actual']?(estimateData[0]["children"][1]['actual']).replaceAll(",","")*1:0
  estimateData[0]["children"][1]["Diff"]=(frtcmsnac-frtcmsnes).toFixed(2);
  estimateData[0]["children"][1]['perDiff']=frtcmsnes==0?0:(((frtcmsnac-frtcmsnes)/frtcmsnes)*100).toFixed(2);
  estimateData[0]["children"][1]['posted']="N/A";
  estimateData[0]["children"][1]['cash_in']="N/A";
  estimateData[0]["children"][1]['sec_variance']="N/A";
  estimateData[0]["children"][1]['sec_per']="N/A"

 






    estimateData[0]["children"][2][colName] = dollarUSLocale.format(mr.toFixed(2));
  let miscreves=estimateData[0]["children"][2]['estimate']?(estimateData[0]["children"][2]['estimate']).replaceAll(",","")*1:0;
  let miscrevac=estimateData[0]["children"][2]['actual']?(estimateData[0]["children"][2]['actual']).replaceAll(",","")*1:0;
  estimateData[0]["children"][2]['Diff']=(miscrevac-miscreves).toFixed(2);
  estimateData[0]["children"][2]['perDiff']=miscreves==0?0:(((miscrevac-miscreves)/miscreves)*100).toFixed(2)
  estimateData[0]["children"][2]['posted']="N/A";
  estimateData[0]["children"][2]['cash_in']="N/A";
  estimateData[0]["children"][2]['sec_variance']="N/A";
  estimateData[0]["children"][2]['sec_per']="N/A"

 







    estimateData[0]["children"][3][colName] = dollarUSLocale.format(grossRevenue.toFixed(2));
    let grossreves=estimateData[0]["children"][3]['estimate']?(estimateData[0]["children"][3]['estimate']).replaceAll(",","")*1:0
    let grossrevac=estimateData[0]["children"][3]['actual']?(estimateData[0]["children"][3]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][3]['Diff']=(grossrevac-grossreves).toFixed(2);
    estimateData[0]["children"][3]['perDiff']=grossreves==0?0:(((grossrevac-grossreves)/grossreves)*100).toFixed(2);
    estimateData[0]["children"][3]['posted']="N/A";
    estimateData[0]["children"][3]['cash_in']="N/A";
    estimateData[0]["children"][3]['sec_variance']="N/A";
    estimateData[0]["children"][3]['sec_per']="N/A"
 
   

    


    estimateData[0]["children"][4][colName] = dollarUSLocale.format(netRevenue.toFixed(2));
    let netreves=estimateData[0]["children"][4]['estimate']?(estimateData[0]["children"][4]['estimate']).replaceAll(",","")*1:0;
    let netrevac=estimateData[0]["children"][4]['actual']?(estimateData[0]["children"][4]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][4]["Diff"]=(netrevac-netreves).toFixed(2);
    estimateData[0]["children"][4]['perDiff']=netreves==0?0:(((netrevac-netreves)/netreves)*100).toFixed(2);
    estimateData[0]["children"][4]['posted']="N/A";
    estimateData[0]["children"][4]['cash_in']="N/A";
    estimateData[0]["children"][4]['sec_variance']="N/A";
    estimateData[0]["children"][4]['sec_per']="N/A"
 
   



    estimateData[1][colName] = dollarUSLocale.format(totalExpenses.toFixed(2));
    let expnses=estimateData[1]['estimate']?(estimateData[1]['estimate']).replaceAll(",","")*1:0
    let expnsac=estimateData[1]['actual']?(estimateData[1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["Diff"]=(expnsac-expnses).toFixed(2);
    estimateData[1]['perDiff']=expnses==0?0:(((expnsac-expnses)/expnses)*100).toFixed(2);
    estimateData[1]['posted']="N/A";
    estimateData[1]['cash_in']="N/A";
    estimateData[1]['sec_variance']="N/A";
    estimateData[1]['sec_per']="N/A"
 



    estimateData[1]["children"][0][colName] = dollarUSLocale.format((totalVoyageDays * hire + bb + totalAdjConsumption).toFixed(2));
    let veselexpnses=estimateData[1]["children"][0]['estimate']?(estimateData[1]["children"][0]['estimate']).replaceAll(",","")*1:0;
    let veselexpnsac=estimateData[1]["children"][0]['actual']?(estimateData[1]["children"][0]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["Diff"]=(veselexpnsac-veselexpnses).toFixed(2)
    estimateData[1]["children"][0]['perDiff']=veselexpnses==0?0:(((veselexpnsac-veselexpnses)/veselexpnses)*100).toFixed(2);
    estimateData[1]["children"][0]['posted']="N/A";
    estimateData[1]["children"][0]['cash_in']="N/A";
    estimateData[1]["children"][0]['sec_variance']="N/A";
    estimateData[1]["children"][0]['sec_per']="N/A"
 




    // estimateData[1]["children"][0]["children"][0][
    //   colName
    // ] = dollarUSLocale.format((totalVoyageDays * hire).toFixed(2));

    estimateData[1]["children"][0]["children"][0][colName] = dollarUSLocale.format((hire * totalVoyageDays).toFixed(2));
    let hireexpnses=estimateData[1]["children"][0]["children"][0]['estimate']?(estimateData[1]["children"][0]["children"][0]['estimate']).replaceAll(",","")*1:0;
    let hireexpnsac=estimateData[1]["children"][0]["children"][0]['actual']?(estimateData[1]["children"][0]["children"][0]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][0]['Diff']=(hireexpnsac-hireexpnses).toFixed(2);
    estimateData[1]["children"][0]["children"][0]['perDiff']=hireexpnses==0?0:(((hireexpnsac-hireexpnses)/hireexpnses)*100).toFixed(2);
    estimateData[1]["children"][0]["children"][0]['posted']="N/A";
    estimateData[1]["children"][0]["children"][0]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][0]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][0]['sec_per']="N/A"
 



    estimateData[1]["children"][0]["children"][1][colName] = dollarUSLocale.format(bb);
   let blstbnses=estimateData[1]["children"][0]["children"][1]['estimate']?(estimateData[1]["children"][0]["children"][1]['estimate']).replaceAll(",","")*1:0;
   let blstbnsac=estimateData[1]["children"][0]["children"][1]['actual']?(estimateData[1]["children"][0]["children"][1]['actual']).replaceAll(",","")*1:0
   estimateData[1]["children"][0]["children"][1]['Diff']=(blstbnsac-blstbnses).toFixed(2);
   estimateData[1]["children"][0]["children"][1]['perDiff']=blstbnses==0?0:(((blstbnsac-blstbnses)/blstbnses)*100).toFixed(2);
   estimateData[1]["children"][0]["children"][1]['posted']="N/A";
   estimateData[1]["children"][0]["children"][1]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][1]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][1]['sec_per']="N/A"
 



   

    estimateData[1]["children"][0]["children"][2][colName] = dollarUSLocale.format(totalAdjConsumption.toFixed(2));
    let bnkradjstmntes=estimateData[1]["children"][0]["children"][2]['estimate']?(estimateData[1]["children"][0]["children"][2]['estimate']).replaceAll(",","")*1:0
    let bnkradjstmntac=estimateData[1]["children"][0]["children"][2]['actual']?(estimateData[1]["children"][0]["children"][2]['actual']).replaceAll(",","")*1:0
    estimateData[1]["children"][0]["children"][2]["Diff"]=(bnkradjstmntac-bnkradjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]['perDiff']=bnkradjstmntes==0?0:(((bnkradjstmntac-bnkradjstmntes)/bnkradjstmntes)*100).toFixed(2);
    estimateData[1]["children"][0]["children"][2]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]['sec_per']="N/A"
 





    estimateData[1]["children"][0]["children"][2]["children"][0][colName] = dollarUSLocale.format(arrivaladjuestmentrev['ifo'].toFixed(2));
    let ifoadjstmntes=estimateData[1]["children"][0]["children"][2]["children"][0]['estimate']?(estimateData[1]["children"][0]["children"][2]["children"][0]['estimate']).replaceAll(",","")*1:0;
    let ifoadjstmntac=estimateData[1]["children"][0]["children"][2]["children"][0]['actual']?(estimateData[1]["children"][0]["children"][2]["children"][0]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][2]["children"][0]['Diff']=(ifoadjstmntac-ifoadjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["children"][0]['perDiff']=ifoadjstmntes==0?0:(((ifoadjstmntac-ifoadjstmntes)/ifoadjstmntes)*100).toFixed(2)
    estimateData[1]["children"][0]["children"][2]["children"][0]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][0]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][0]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][0]['sec_per']="N/A"
 




    estimateData[1]["children"][0]["children"][2]["children"][1][colName] = dollarUSLocale.format(arrivaladjuestmentrev['vlsfo'].toFixed(2));
    let vlsfoadjstmntes=estimateData[1]["children"][0]["children"][2]["children"][1]['estimate']?(estimateData[1]["children"][0]["children"][2]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let vlsfoadjstmntac=estimateData[1]["children"][0]["children"][2]["children"][1]['actual']?(estimateData[1]["children"][0]["children"][2]["children"][1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][2]["children"][1]['Diff']=(vlsfoadjstmntac-vlsfoadjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["children"][1]['perDiff']= vlsfoadjstmntes==0?0:(((vlsfoadjstmntac-vlsfoadjstmntes)/vlsfoadjstmntes)*100).toFixed(2)
    estimateData[1]["children"][0]["children"][2]["children"][1]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][1]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][1]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][1]['sec_per']="N/A"
 






    estimateData[1]["children"][0]["children"][2]["children"][2][colName] = dollarUSLocale.format(arrivaladjuestmentrev['lsmgo'].toFixed(2));
    let lsmgoadjstmntes=estimateData[1]["children"][0]["children"][2]["children"][2]['estimate']?(estimateData[1]["children"][0]["children"][2]["children"][2]['estimate']).replaceAll(",","")*1:0;
    let lsmgoadjstmntac=estimateData[1]["children"][0]["children"][2]["children"][2]['actual']?(estimateData[1]["children"][0]["children"][2]["children"][2]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][2]["children"][2]['Diff']=(lsmgoadjstmntac-lsmgoadjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["children"][2]['perDiff']=lsmgoadjstmntes==0?0:(((lsmgoadjstmntac-lsmgoadjstmntes)/lsmgoadjstmntes)*100).toFixed(2)
    estimateData[1]["children"][0]["children"][2]["children"][2]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][2]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][2]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][2]['sec_per']="N/A"
 






    estimateData[1]["children"][0]["children"][2]["children"][3][colName] = dollarUSLocale.format(arrivaladjuestmentrev['mgo'].toFixed(2));
    let mgoadjstmntes=estimateData[1]["children"][0]["children"][2]["children"][3]['estimate']?(estimateData[1]["children"][0]["children"][2]["children"][3]['estimate']).replaceAll(",","")*1:0;
    let mgoadjstmntac=estimateData[1]["children"][0]["children"][2]["children"][3]['actual']?(estimateData[1]["children"][0]["children"][2]["children"][3]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][2]["children"][3]['Diff']=(mgoadjstmntac-mgoadjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["children"][3]['perDiff']=mgoadjstmntes==0?0: (((mgoadjstmntac-mgoadjstmntes)/mgoadjstmntes)*100).toFixed(2)
    estimateData[1]["children"][0]["children"][2]["children"][3]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][3]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][3]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][3]['sec_per']="N/A"
 







    estimateData[1]["children"][0]["children"][2]["children"][4][colName] = dollarUSLocale.format(arrivaladjuestmentrev['ulsfo'].toFixed(2));

    let ulsfoadjstmntes=estimateData[1]["children"][0]["children"][2]["children"][4]['estimate']?(estimateData[1]["children"][0]["children"][2]["children"][4]['estimate']).replaceAll(",","")*1:0;
    let ulsfoadjstmntac=estimateData[1]["children"][0]["children"][2]["children"][4]['actual']?(estimateData[1]["children"][0]["children"][2]["children"][4]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][0]["children"][2]["children"][4]['Diff']=(ulsfoadjstmntac-ulsfoadjstmntes).toFixed(2);
    estimateData[1]["children"][0]["children"][2]["children"][4]['perDiff']= ulsfoadjstmntes==0?0:(((ulsfoadjstmntac-ulsfoadjstmntes)/ulsfoadjstmntes)*100).toFixed(2)
    estimateData[1]["children"][0]["children"][2]["children"][4]['posted']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][4]['cash_in']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][4]['sec_variance']="N/A";
    estimateData[1]["children"][0]["children"][2]["children"][4]['sec_per']="N/A"
 





    // estimateData[1]["children"][1][colName] = dollarUSLocale.format(
    //   (((addPercentage * hire * totalVoyageDays) / 100) * -1).toFixed(2)
    // );
  
    estimateData[1]["children"][1][colName] = dollarUSLocale.format(amt_add_percentage.toFixed(2));
    let tciaddcomes=estimateData[1]["children"][1]['estimate']?(estimateData[1]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let tciaddcomac=estimateData[1]["children"][1]['actual']?(estimateData[1]["children"][1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][1]['Diff'] =(tciaddcomac-tciaddcomes).toFixed(2);
    estimateData[1]["children"][1]['perDiff'] = tciaddcomes==0?0:(((tciaddcomac-tciaddcomes)/tciaddcomes)*100).toFixed(2)
    estimateData[1]["children"][1]['posted']="N/A";
    estimateData[1]["children"][1]['cash_in']="N/A";
    estimateData[1]["children"][1]['sec_variance']="N/A";
    estimateData[1]["children"][1]['sec_per']="N/A"
 

    // estimateData[1]["children"][2][colName] = dollarUSLocale.format(
    //   (((broPercentage * hire * totalVoyageDays) / 100) * -1).toFixed(2)
    // );

    estimateData[1]["children"][2][colName] = dollarUSLocale.format(amt_bro_percentage.toFixed(2));
    let tcibrocomes=estimateData[1]["children"][2]['estimate']?(estimateData[1]["children"][2]['estimate']).replaceAll(",","")*1:0;
    let tcibrocomac=estimateData[1]["children"][2]['actual']?(estimateData[1]["children"][2]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][2]['Diff'] =(tcibrocomac-tcibrocomes).toFixed(2);
    estimateData[1]["children"][2]['perDiff'] = tcibrocomes==0?0:(((tcibrocomac-tcibrocomes)/tcibrocomes)*100).toFixed(2)
    estimateData[1]["children"][2]['posted']="N/A";
    estimateData[1]["children"][2]['cash_in']="N/A";
    estimateData[1]["children"][2]['sec_variance']="N/A";
    estimateData[1]["children"][2]['sec_per']="N/A"
 




    estimateData[1]["children"][3][colName] = dollarUSLocale.format(pi.toFixed(2));
   let prtexpnses=estimateData[1]["children"][3]['estimate']?(estimateData[1]["children"][3]['estimate']).replaceAll(",","")*1:0;
   let prtexpnsac=estimateData[1]["children"][3]['actual']?(estimateData[1]["children"][3]['actual']).replaceAll(",","")*1:0;
   estimateData[1]["children"][3]['Diff']=(prtexpnsac-prtexpnses).toFixed(2);
   estimateData[1]["children"][3]['perDiff']=prtexpnses==0?0:(((prtexpnsac-prtexpnses)/prtexpnses)*100).toFixed(2)
   estimateData[1]["children"][3]['posted']="N/A";
   estimateData[1]["children"][3]['cash_in']="N/A";
   estimateData[1]["children"][3]['sec_variance']="N/A";
   estimateData[1]["children"][3]['sec_per']="N/A"


 


    estimateData[1]["children"][4][colName] = dollarUSLocale.format((totalSeaConsumption + totalPortConsumption).toFixed(2));
    let bnkrexpnses=estimateData[1]["children"][4]['estimate']?(estimateData[1]["children"][4]['estimate']).replaceAll(",","")*1:0;
    let bnkrexpnsac=estimateData[1]["children"][4]['actual']?(estimateData[1]["children"][4]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]['Diff']=(bnkrexpnsac-bnkrexpnses).toFixed(2);
    estimateData[1]["children"][4]['perDiff']=bnkrexpnses==0?0: (((bnkrexpnsac-bnkrexpnses)/bnkrexpnses)*100).toFixed(2)
  estimateData[1]["children"][4]['posted']="N/A";
   estimateData[1]["children"][4]['cash_in']="N/A";
   estimateData[1]["children"][4]['sec_variance']="N/A";
   estimateData[1]["children"][4]['sec_per']="N/A"





    estimateData[1]["children"][4]["children"][0][colName] = dollarUSLocale.format(totalSeaConsumption.toFixed(2));
   let seacnsmpes=estimateData[1]["children"][4]["children"][0]['estimate']?(estimateData[1]["children"][4]["children"][0]['estimate']).replaceAll(",","")*1:0;
   let seacnsmpac=estimateData[1]["children"][4]["children"][0]['actual']?(estimateData[1]["children"][4]["children"][0]['actual']).replaceAll(",","")*1:0;
   estimateData[1]["children"][4]["children"][0]['Diff']=(seacnsmpac- seacnsmpes).toFixed(2);
   estimateData[1]["children"][4]["children"][0]['perDiff']= seacnsmpes==0?0:(((seacnsmpac- seacnsmpes)/seacnsmpes)*100).toFixed(2);
   estimateData[1]["children"][4]["children"][0]['posted']="N/A";
   estimateData[1]["children"][4]["children"][0]['cash_in']="N/A";
   estimateData[1]["children"][4]["children"][0]['sec_variance']="N/A";
   estimateData[1]["children"][4]["children"][0]['sec_per']="N/A"








    estimateData[1]["children"][4]["children"][0]["children"][0][colName] = dollarUSLocale.format(seaExpenses["ifo"].toFixed(2));
   let seaexpnsifoes=estimateData[1]["children"][4]["children"][0]["children"][0]['estimate']?(estimateData[1]["children"][4]["children"][0]["children"][0]['estimate']).replaceAll(",","")*1:0;
   let seaexpnsifoac=estimateData[1]["children"][4]["children"][0]["children"][0]['actual']?(estimateData[1]["children"][4]["children"][0]["children"][0]['actual']).replaceAll(",","")*1:0;
   estimateData[1]["children"][4]["children"][0]["children"][0]['Diff']=(seaexpnsifoac-seaexpnsifoes).toFixed(2);
   estimateData[1]["children"][4]["children"][0]["children"][0]['perDiff']=seaexpnsifoes==0?0: (((seaexpnsifoac- seaexpnsifoes)/seaexpnsifoes)*100).toFixed(2)
   estimateData[1]["children"][4]["children"][0]["children"][0]['posted']="N/A";
   estimateData[1]["children"][4]["children"][0]["children"][0]['cash_in']="N/A";
   estimateData[1]["children"][4]["children"][0]["children"][0]['sec_variance']="N/A";
   estimateData[1]["children"][4]["children"][0]["children"][0]['sec_per']="N/A"





    estimateData[1]["children"][4]["children"][0]["children"][1][colName] = dollarUSLocale.format(seaExpenses["mgo"].toFixed(2));
    let seaexpnsmgoes=estimateData[1]["children"][4]["children"][0]["children"][1]['estimate']?(estimateData[1]["children"][4]["children"][0]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let seaexpnsmgoac=estimateData[1]["children"][4]["children"][0]["children"][1]['actual']?(estimateData[1]["children"][4]["children"][0]["children"][1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][0]["children"][1]['Diff']=(seaexpnsmgoac-seaexpnsmgoes).toFixed(2);
    estimateData[1]["children"][4]["children"][0]["children"][1]['perDiff']= seaexpnsmgoes==0?0:(((seaexpnsmgoac- seaexpnsmgoes)/seaexpnsmgoes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][0]["children"][1]['posted']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][1]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][1]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][1]['sec_per']="N/A"
 






    estimateData[1]["children"][4]["children"][0]["children"][2][colName] = dollarUSLocale.format(seaExpenses["vlsfo"].toFixed(2));
    let seaexpnsvlsfoes=estimateData[1]["children"][4]["children"][0]["children"][2]['estimate']?(estimateData[1]["children"][4]["children"][0]["children"][2]['estimate']).replaceAll(",","")*1:0;
    let seaexpnsvlsfoac=estimateData[1]["children"][4]["children"][0]["children"][2]['actual']?(estimateData[1]["children"][4]["children"][0]["children"][2]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][0]["children"][2]['Diff']=(seaexpnsvlsfoac-seaexpnsvlsfoes).toFixed(2);
    estimateData[1]["children"][4]["children"][0]["children"][2]['perDiff']=seaexpnsvlsfoes==0?0: (((seaexpnsvlsfoac- seaexpnsvlsfoes)/seaexpnsvlsfoes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][0]["children"][2]['posted']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][2]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][2]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][2]['sec_per']="N/A"
 






    estimateData[1]["children"][4]["children"][0]["children"][4][colName] = dollarUSLocale.format(seaExpenses["ulsfo"].toFixed(2));
    let seaexpnsulsfoes=estimateData[1]["children"][4]["children"][0]["children"][4]['estimate']?(estimateData[1]["children"][4]["children"][0]["children"][4]['estimate']).replaceAll(",","")*1:0;
    let seaexpnsulsfoac=estimateData[1]["children"][4]["children"][0]["children"][4]['actual']?(estimateData[1]["children"][4]["children"][0]["children"][4]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][0]["children"][4]['Diff']=(seaexpnsulsfoac-seaexpnsulsfoes).toFixed(2);
    estimateData[1]["children"][4]["children"][0]["children"][4]['perDiff']=seaexpnsulsfoes==0?0: (((seaexpnsulsfoac- seaexpnsulsfoes)/seaexpnsulsfoes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][0]["children"][4]['posted']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][4]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][4]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][4]['sec_per']="N/A"
 

    
    estimateData[1]["children"][4]["children"][0]["children"][3][colName] = dollarUSLocale.format(seaExpenses["lsmgo"].toFixed(2));
    let seaexpnslsmgoes=estimateData[1]["children"][4]["children"][0]["children"][3]['estimate']?(estimateData[1]["children"][4]["children"][0]["children"][3]['estimate']).replaceAll(",","")*1:0;
    let seaexpnslsmgoac=estimateData[1]["children"][4]["children"][0]["children"][3]['actual']?(estimateData[1]["children"][4]["children"][0]["children"][3]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][0]["children"][3]['Diff']=(seaexpnsulsfoac-seaexpnslsmgoes).toFixed(2);
    estimateData[1]["children"][4]["children"][0]["children"][3]['perDiff']= seaexpnslsmgoes==0?0:(((seaexpnslsmgoac- seaexpnslsmgoes)/seaexpnslsmgoes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][0]["children"][3]['posted']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][3]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][3]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][0]["children"][3]['sec_per']="N/A"
 



    estimateData[1]["children"][4]["children"][1][colName] = dollarUSLocale.format(totalPortConsumption.toFixed(2));
    let prtcnspmes= estimateData[1]["children"][4]["children"][1]['estimate']?( estimateData[1]["children"][4]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let prtcnsmpac=estimateData[1]["children"][4]["children"][1]['actual']?( estimateData[1]["children"][4]["children"][1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][1]['Diff']=(prtcnsmpac-prtcnspmes).toFixed(2);
    estimateData[1]["children"][4]["children"][1]['perDiff']=prtcnspmes==0?0:(((prtcnsmpac-prtcnspmes)/prtcnspmes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][1]['posted']="N/A";
    estimateData[1]["children"][4]["children"][1]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][1]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][1]['sec_per']="N/A"
 






    estimateData[1]["children"][4]["children"][1]["children"][0][colName] = dollarUSLocale.format(portExpenses["ifo"].toFixed(2));
   let prtcnsmpifoes=estimateData[1]["children"][4]["children"][1]["children"][0]['estimate']?(estimateData[1]["children"][4]["children"][1]["children"][0]['estimate']).replaceAll(",","")*1:0;
   let prtcnsmpifoac=estimateData[1]["children"][4]["children"][1]["children"][0]['actual']?(estimateData[1]["children"][4]["children"][1]["children"][0]['actual']).replaceAll(",","")*1:0;
   estimateData[1]["children"][4]["children"][1]["children"][0]['Diff']=(prtcnsmpifoac-prtcnsmpifoes).toFixed(2)
   estimateData[1]["children"][4]["children"][1]["children"][0]['perDiff']=prtcnsmpifoes==0?0: (((prtcnsmpifoac-prtcnsmpifoes)/prtcnsmpifoes)*100).toFixed(2)
   estimateData[1]["children"][4]["children"][1]["children"][0]['posted']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][0]['cash_in']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][0]['sec_variance']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][0]['sec_per']="N/A"
 




    estimateData[1]["children"][4]["children"][1]["children"][1][colName] = dollarUSLocale.format(portExpenses["mgo"].toFixed(2));
    let prtcnsmpmgoes=estimateData[1]["children"][4]["children"][1]["children"][1]['estimate']?(estimateData[1]["children"][4]["children"][1]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let prtcnsmpmgoac=estimateData[1]["children"][4]["children"][1]["children"][1]['actual']?(estimateData[1]["children"][4]["children"][1]["children"][1]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][1]["children"][1]['Diff']=(prtcnsmpmgoac-prtcnsmpmgoes).toFixed(2)
    estimateData[1]["children"][4]["children"][1]["children"][1]['perDiff']=prtcnsmpmgoes==0?0: (((prtcnsmpmgoac-prtcnsmpmgoes)/prtcnsmpmgoes)*100).toFixed(2)
   estimateData[1]["children"][4]["children"][1]["children"][1]['posted']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][1]['cash_in']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][1]['sec_variance']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][1]['sec_per']="N/A"
 




    estimateData[1]["children"][4]["children"][1]["children"][2][colName] = dollarUSLocale.format(portExpenses["vlsfo"].toFixed(2));
    let prtcnsmpvlsfoes=estimateData[1]["children"][4]["children"][1]["children"][2]['estimate']?(estimateData[1]["children"][4]["children"][1]["children"][2]['estimate']).replaceAll(",","")*1:0;
    let prtcnsmpvlsfoac=estimateData[1]["children"][4]["children"][1]["children"][2]['actual']?(estimateData[1]["children"][4]["children"][1]["children"][2]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][1]["children"][2]['Diff']=(prtcnsmpvlsfoac-prtcnsmpvlsfoes).toFixed(2)
    estimateData[1]["children"][4]["children"][1]["children"][2]['perDiff']=prtcnsmpvlsfoes==0?0: (((prtcnsmpvlsfoac-prtcnsmpvlsfoes)/prtcnsmpvlsfoes)*100).toFixed(2)
   estimateData[1]["children"][4]["children"][1]["children"][2]['posted']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][2]['cash_in']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][2]['sec_variance']="N/A";
   estimateData[1]["children"][4]["children"][1]["children"][2]['sec_per']="N/A"
 



       estimateData[1]["children"][4]["children"][1]["children"][3][colName] = dollarUSLocale.format(portExpenses["lsmgo"].toFixed(2));
       let prtcnsmplsmgoes=estimateData[1]["children"][4]["children"][1]["children"][3]['estimate']?(estimateData[1]["children"][4]["children"][1]["children"][3]['estimate']).replaceAll(",","")*1:0;
       let prtcnsmplsmgoac=estimateData[1]["children"][4]["children"][1]["children"][3]['actual']?(estimateData[1]["children"][4]["children"][1]["children"][3]['actual']).replaceAll(",","")*1:0;
       estimateData[1]["children"][4]["children"][1]["children"][3]['Diff']=(prtcnsmplsmgoac-prtcnsmplsmgoes).toFixed(2)
       estimateData[1]["children"][4]["children"][1]["children"][3]['perDiff']=prtcnsmplsmgoes==0?0: (((prtcnsmplsmgoac-prtcnsmplsmgoes)/prtcnsmplsmgoes)*100).toFixed(2)
       estimateData[1]["children"][4]["children"][1]["children"][3]['posted']="N/A";
       estimateData[1]["children"][4]["children"][1]["children"][3]['cash_in']="N/A";
       estimateData[1]["children"][4]["children"][1]["children"][3]['sec_variance']="N/A";
       estimateData[1]["children"][4]["children"][1]["children"][3]['sec_per']="N/A"
     
  






    estimateData[1]["children"][4]["children"][1]["children"][4][colName] = dollarUSLocale.format(portExpenses["ulsfo"].toFixed(2));
    let prtcnsmpulsfoes=estimateData[1]["children"][4]["children"][1]["children"][4]['estimate']?(estimateData[1]["children"][4]["children"][1]["children"][4]['estimate']).replaceAll(",","")*1:0;
    let prtcnsmpulsfoac=estimateData[1]["children"][4]["children"][1]["children"][4]['actual']?(estimateData[1]["children"][4]["children"][1]["children"][4]['actual']).replaceAll(",","")*1:0;
    estimateData[1]["children"][4]["children"][1]["children"][4]['Diff']=(prtcnsmpulsfoac-prtcnsmpulsfoes).toFixed(2)
    estimateData[1]["children"][4]["children"][1]["children"][4]['perDiff']=prtcnsmpulsfoes==0?0:(((prtcnsmpulsfoac-prtcnsmpulsfoes)/prtcnsmpulsfoes)*100).toFixed(2)
    estimateData[1]["children"][4]["children"][1]["children"][4]['posted']="N/A";
    estimateData[1]["children"][4]["children"][1]["children"][4]['cash_in']="N/A";
    estimateData[1]["children"][4]["children"][1]["children"][4]['sec_variance']="N/A";
    estimateData[1]["children"][4]["children"][1]["children"][4]['sec_per']="N/A"
     







    estimateData[1]["children"][5][colName] = dollarUSLocale.format(misCost.toFixed(2));
   let miscstes=estimateData[1]["children"][5]['estimate']?(estimateData[1]["children"][5]['estimate']).replaceAll(",","")*1:0;
   let miscstac=estimateData[1]["children"][5]['actual']?(estimateData[1]["children"][5]['actual']).replaceAll(",","")*1:0;
   estimateData[1]["children"][5]['Diff']=(miscstac-miscstes).toFixed(2);
   estimateData[1]["children"][5]['perDiff']=miscstes==0?0: (((miscstac-miscstes)/miscstes)*100).toFixed(2);
   estimateData[1]["children"][5]['posted']="N/A";
   estimateData[1]["children"][5]['cash_in']="N/A";
   estimateData[1]["children"][5]['sec_variance']="N/A";
   estimateData[1]["children"][5]['sec_per']="N/A"
     
// estimateData[1]["children"][6][colName] = dollarUSLocale.format(totalExpenses.toFixed(2));
    
    let itemValue = netRevenue - totalExpenses;
    let tceHire = (
      itemValue -
      (totalExpenses -
        (bb + pi + 0 + misCost + amt_add_percentage + amt_bro_percentage)) /
      (itemValue / (totalVoyageDays - 0))
    ).toFixed(2);
    estimateData[2][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2)).replaceAll(",","")
     
       :  dollarUSLocale.format((itemValue * -1).toFixed(2)).replaceAll(",","") ;
    estimateData[2]["children"][0][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2)).replaceAll(",","")
        
       :  dollarUSLocale.format((itemValue * -1).toFixed(2)).replaceAll(",","") ;
        let vygreses= estimateData[2]['estimate']?( estimateData[2]['estimate']).replaceAll(",","")*1:0;
        let vygresac=estimateData[2]['actual']?( estimateData[2]['actual']).replaceAll(",","")*1:0;
        estimateData[2]['Diff'] =(vygresac-vygreses).toFixed(2);
        estimateData[2]['perDiff'] =(((vygresac-vygreses)/vygreses)*100).toFixed(2);
        estimateData[2]['posted']="N/A";
        estimateData[2]['cash_in']="N/A";
        estimateData[2]['sec_variance']="N/A";
        estimateData[2]['sec_per']="N/A"
             

       
        let profitlses=estimateData[2]["children"][0]['estimate']?(estimateData[2]["children"][0]['estimate']).replaceAll(",","")*1:0;
        let profitlsac=estimateData[2]["children"][0]['actual']?(estimateData[2]["children"][0]['actual']).replaceAll(",","")*1:0
        estimateData[2]["children"][0]['Diff']=(profitlsac-profitlses).toFixed(2);
        estimateData[2]["children"][0]['perDiff']=profitlses==0?0: (((profitlsac-profitlses)/profitlses)*100).toFixed(2);
        estimateData[2]["children"][0]['posted']="N/A";
        estimateData[2]["children"][0]['cash_in']="N/A";
        estimateData[2]["children"][0]['sec_variance']="N/A";
        estimateData[2]["children"][0]['sec_per']="N/A"
            
 


    estimateData[2]["children"][1][colName] = dollarUSLocale.format((itemValue / (totalVoyageDays - 0)).toFixed(2));
    let dlprofitlses=estimateData[2]["children"][1]['estimate']?(estimateData[2]["children"][1]['estimate']).replaceAll(",","")*1:0;
    let dlprofitlsac=estimateData[2]["children"][1]['actual']?(estimateData[2]["children"][1]['actual']).replaceAll(",","")*1:0
    estimateData[2]["children"][1]['Diff']=(dlprofitlsac-dlprofitlses).toFixed(2);
    estimateData[2]["children"][1]['perDiff']=dlprofitlses==0?0: (((dlprofitlsac-dlprofitlses)/dlprofitlses)*100).toFixed(2);
    estimateData[2]["children"][1]['posted']="N/A";
    estimateData[2]["children"][1]['cash_in']="N/A";
    estimateData[2]["children"][1]['sec_variance']="N/A";
    estimateData[2]["children"][1]['sec_per']="N/A"
        




    estimateData[2]["children"][2][colName] = dollarUSLocale.format(tceHire);
    let tcehirees= estimateData[2]["children"][2]['estimate']?( estimateData[2]["children"][2]['estimate']).replaceAll(",","")*1:0;
    let tcehireac=estimateData[2]["children"][2]['actual']?( estimateData[2]["children"][2]['actual']).replaceAll(",","")*1:0;
    estimateData[2]["children"][2]['Diff'] =(tcehireac-tcehirees).toFixed(2);
    estimateData[2]["children"][2]['perDiff'] =tcehirees==0?0: (((tcehireac-tcehirees)/tcehirees)*100).toFixed(2)
    estimateData[2]["children"][2]['posted']="N/A";
    estimateData[2]["children"][2]['cash_in']="N/A";
    estimateData[2]["children"][2]['sec_variance']="N/A";
    estimateData[2]["children"][2]['sec_per']="N/A"
        




    estimateData[2]["children"][3][colName] = dollarUSLocale.format((addPercentage > 0 ? tceHire / (1 - addPercentage) : 0).toFixed(2));
    let grstcehirees= estimateData[2]["children"][3]['estimate']?( estimateData[2]["children"][3]['estimate']).replaceAll(",","")*1:0;
    let grstcehireac=estimateData[2]["children"][3]['actual']?( estimateData[2]["children"][3]['actual']).replaceAll(",","")*1:0;
    estimateData[2]["children"][3]['Diff'] =(grstcehireac-grstcehirees).toFixed(2);
    estimateData[2]["children"][3]['perDiff'] =grstcehirees==0?0:(((grstcehireac-grstcehirees)/grstcehirees)*100).toFixed(2)
    estimateData[2]["children"][3]['posted']="N/A";
    estimateData[2]["children"][3]['cash_in']="N/A";
    estimateData[2]["children"][3]['sec_variance']="N/A";
    estimateData[2]["children"][3]['sec_per']="N/A"
        


       estimateData[2]["children"][4][colName] = dollarUSLocale.format(avgfreightRate);
       let avfrtes= estimateData[2]["children"][4]['estimate']?( estimateData[2]["children"][4]['estimate']).replaceAll(",","")*1:0;
       let avfrtac=estimateData[2]["children"][4]['actual']?( estimateData[2]["children"][4]['actual']).replaceAll(",","")*1:0;
       estimateData[2]["children"][4]['Diff'] =(avfrtac-avfrtes).toFixed(2);
       estimateData[2]["children"][4]['perDiff'] = avfrtes==0?0:(((avfrtac-avfrtes)/avfrtes)*100).toFixed(2)
       estimateData[2]["children"][4]['posted']="N/A";
       estimateData[2]["children"][4]['cash_in']="N/A";
       estimateData[2]["children"][4]['sec_variance']="N/A";
       estimateData[2]["children"][4]['sec_per']="N/A"
           





    estimateData[2]["children"][5][colName] = netRevenue / cpQtyOpt >= 0 ?
    dollarUSLocale.format(((netRevenue - totalExpenses) / cpQtyOpt).toFixed(2)) :
      "(" + dollarUSLocale.format((((netRevenue - totalExpenses) / cpQtyOpt) * -1).toFixed(2)) + ")"; // Profit is 0 since we are doing Breakeven

      let brkevnfrtes=estimateData[2]["children"][5]['estimate']?(estimateData[2]["children"][5]["estimate"]).replaceAll(",","")*1:0;
      let brkevnfrtac=estimateData[2]["children"][5]['actual']?(estimateData[2]["children"][5]['actual']).replaceAll(",","")*1:0;
      estimateData[2]["children"][5]['Diff'] =(brkevnfrtac-brkevnfrtes).toFixed(2)
      estimateData[2]["children"][5]['perDiff']=brkevnfrtac==0?0: (((brkevnfrtac-brkevnfrtes)/brkevnfrtac)*100).toFixed(2)
      estimateData[2]["children"][5]['posted']="N/A";
      estimateData[2]["children"][5]['cash_in']="N/A";
      estimateData[2]["children"][5]['sec_variance']="N/A";
      estimateData[2]["children"][5]['sec_per']="N/A"
          



    estimateData[2]["children"][6][colName] = dollarUSLocale.format((tsd * 1).toFixed(2));
    let tsdes=estimateData[2]["children"][6]['estimate']?(estimateData[2]["children"][6]["estimate"]).replaceAll(",","")*1:0;
    let tsdac=estimateData[2]["children"][6]['actual']?(estimateData[2]["children"][6]['actual']).replaceAll(",","")*1:0;
    estimateData[2]["children"][6]['Diff'] =(tsdac-tsdes).toFixed(2)
    estimateData[2]["children"][6]['perDiff']= tsdes==0?0:(((tsdac-tsdes)/tsdes)*100).toFixed(2)
    estimateData[2]["children"][6]['posted']="N/A";
    estimateData[2]["children"][6]['cash_in']="N/A";
    estimateData[2]["children"][6]['sec_variance']="N/A";
    estimateData[2]["children"][6]['sec_per']="N/A"
        




    estimateData[2]["children"][7][colName] = dollarUSLocale.format((tpd * 1).toFixed(2));
    let tpdes=estimateData[2]["children"][7]['estimate']?(estimateData[2]["children"][7]["estimate"]).replaceAll(",","")*1:0;
    let tpdac=estimateData[2]["children"][7]['actual']?(estimateData[2]["children"][7]['actual']).replaceAll(",","")*1:0;
    estimateData[2]["children"][7]['Diff'] =(tpdac-tpdes).toFixed(2)
    estimateData[2]["children"][7]['perDiff']=tpdes==0?0: (((tpdac-tpdes)/tpdes)*100).toFixed(2)
    estimateData[2]["children"][7]['posted']="N/A";
    estimateData[2]["children"][7]['cash_in']="N/A";
    estimateData[2]["children"][7]['sec_variance']="N/A";
    estimateData[2]["children"][7]['sec_per']="N/A"
        




    estimateData[2]["children"][8][colName] = dollarUSLocale.format((totalVoyageDays - 0).toFixed(2)); // OFF Hire Day => 0
    let netvygdses=estimateData[2]["children"][8]['estimate']?(estimateData[2]["children"][8]["estimate"]).replaceAll(",","")*1:0;
    let netvygdsac=estimateData[2]["children"][8]['actual']?(estimateData[2]["children"][8]['actual']).replaceAll(",","")*1:0;
    estimateData[2]["children"][8]['Diff'] =(netvygdsac-netvygdses).toFixed(2)
    estimateData[2]["children"][8]['perDiff']=netvygdses==0?0: (((netvygdsac-netvygdses)/netvygdses)*100).toFixed(2)
    estimateData[2]["children"][8]['posted']="N/A";
    estimateData[2]["children"][8]['cash_in']="N/A";
    estimateData[2]["children"][8]['sec_variance']="N/A";
    estimateData[2]["children"][8]['sec_per']="N/A"
        

   



  
    return estimateData;
  };

  componentDidMount() {
    // console.log('call count');
    this.setState(
      {
        ...this.state,
        showPL: false,
        estimateData: this.__pl(),
      },
      () => this.setState({ ...this.state, showPL: true })
    );
   
  }

  render() {
    const { estimateData, showPL, viewTabs } = this.state;
   
  

    // if (estimateData[0].hasOwnProperty("actual")) {
      
    //   estimateData.map((e) => {
    //     e["variance"] =
    //       e["estimate"].replaceAll(",", "") * 1 -
    //       e["actual"].replaceAll(",", "") * 1;
    //     e["per"] = e["variance"] / (e["estimate"].replaceAll(",", "") * 1);
    //     e["children"].map((ec) => {
    //       ec["variance"] = 0;
    //       ec["per"] = 0;
    //       if (ec.hasOwnProperty("estimate") && ec.hasOwnProperty("actual")) {
    //         ec["variance"] =
    //           ec["estimate"].replaceAll(",", "") * 1 -
    //           ec["actual"].replaceAll(",", "") * 1;
    //         ec["per"] =
    //           ec["variance"] / (ec["estimate"].replaceAll(",", "") * 1);
    //       }
    //       console.log("hello")
    //       if (ec.hasOwnProperty("children")) {
    //         ec["children"].map((ecc) => {
    //           ecc["variance"] = 0;
    //           ecc["per"] = 0;
    //           if (
    //             ecc.hasOwnProperty("estimate") &&
    //             ecc.hasOwnProperty("actual")
    //           ) {
    //             ecc["variance"] =
    //               ecc["estimate"].replaceAll(",", "") * 1 -
    //               ecc["actual"].replaceAll(",", "") * 1;
    //             ecc["per"] =
    //               ecc["variance"] / (ecc["estimate"].replaceAll(",", "") * 1);
    //           }

    //           if (ecc.hasOwnProperty("children")) {
    //             ecc["children"].map((eccc) => {
    //               eccc["variance"] = 0;
    //               eccc["per"] = 0;
    //               if (
    //                 eccc.hasOwnProperty("estimate") &&
    //                 eccc.hasOwnProperty("actual")
    //               ) {
    //                 eccc["variance"] =
    //                   eccc["estimate"].replaceAll(",", "") * 1 -
    //                   eccc["actual"].replaceAll(",", "") * 1;
    //                 eccc["per"] =
    //                   eccc["variance"] /
    //                   (eccc["estimate"].replaceAll(",", "") * 1);
    //               }
    //             });
    //           }
    //         });
    //       }
    //     });
    //   });
    // }

    return (
      <>
        <div>
          <ToolbarUI routeUrl={"pl-main"} callback={(e) => this.callback(e)} />
        </div>

        {showPL === true ? (
          <Tabs defaultActiveKey="1">
            {viewTabs.map((e) => {
              if (e === "Actual &  Operation View") {
                return (
                  <TabPane tab={e} key="ao2">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Estimate View") {
                return (
               
                  <TabPane tab={e} key="ev1">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns2}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Account View") {
                return (
                  <TabPane tab={e} key="av3">
                    Accounts
                  </TabPane>
                );
              }
            })}
          </Tabs>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </>
    );
  }
}

export default PL;
