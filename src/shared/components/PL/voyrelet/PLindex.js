import React, { Component } from "react";
import { Table, Tabs, Input, Row, Col, Form, Spin, Alert } from "antd";
//import ToolbarUI from "components/ToolbarUI";

import ToolbarUI from "../../../../components/ToolbarUI";
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;

const columns = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },

  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "8%",
  },
  {
    title: "Actual",
    dataIndex: "actual",
    key: "actual",
    width: "8%",
  },
  {
    title: "Posted",
    dataIndex: "posted",
    key: "posted",
    width: "8%",
  },
  {
    title: "Cash In",
    dataIndex: "cash_in",
    key: "cash_in",
    width: "8%",
  },
  {
    title: `Diff`,
    dataIndex: "Diff",
    key: "Diff",
    width: "8%",
  },
  {
    title: "%Diff",
    dataIndex: "perDiff",
    key: "perDiff",
    width: "8%",
  },
  {
    title: "Post Vs Cash",
    dataIndex: "sec_variance",
    key: "sec_variance",
    width: "8%",
  },
  {
    title: "% Post Vs Cash",
    dataIndex: "sec_per",
    key: "sec_per",
    width: "8%",
  },
];

const columns2 = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "12%",
    align: "right",
  },
  { title: "", dataIndex: "", key: "blank", width: "68%" },
];

class PL extends Component {
  callback = (evt) => {};

  constructor(props) {
    super(props);

    this.state = {
      dollarUSLocale: Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      formData: this.props.formData || {},
      estimateDatavalue: this.props.estimateData || {},
      showPL: false,
      showEstimatePl: this.props.showEstimatePl,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || ["Actual &  Operation View"],
      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: 0,
          children: [
            { key: "rev00", description: "Gross Frieght", estimate: 0 },
            { key: "rev01", description: "Total Comm.", estimate: 0 },
            { key: "rev02", description: "Other Revenue", estimate: 0 },
            { key: "rev03", description: "Dem Amt", estimate: 0 },
            { key: "rev04", description: "Dem Comm Amt", estimate: 0 },
            { key: "rev05", description: "Des Amt", estimate: 0 },
            { key: "rev06", description: "Des Comm Amt", estimate: 0 },
            { key: "rev07", description: "Gross Revenue", estimate: 0 },
            { key: "rev08", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses.",
          estimate: 0,

          children: [
            { key: "ex10", description: "Gross Frieght", estimate: 0 },
            { key: "ex11", description: "Ttl Comm", estimate: 0 },
            { key: "ex12", description: "Other Revenue", estimate: 0 },
            { key: "rev13", description: "Dem Amt", estimate: 0 },
            { key: "rev14", description: "Dem Comm Amt", estimate: 0 },
            { key: "rev15", description: "Des Amt", estimate: 0 },
            { key: "rev16", description: "Des Comm Amt", estimate: 0 },
            { key: "ex17", description: "Other Exp", estimate: 0 },
            { key: "ex18", description: "Gross Expenses", estimate: 0 },
            { key: "ex19", description: "Net Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr10", description: "Profit (Loss)", estimate: 0 },
            { key: "vr11", description: "Daily Profit (Loss)", estimate: 0 },
            // { key: "vr13", description: "TCE Hire ( Net Daily )", estimate: 0 },
            // { key: "vr14", description: "Gross TCE", estimate: 0 },
            // { key: "vr15", description: "Freight rate ($/T)", estimate: 0 },
            // {
            //   key: "vr16",
            //   description: "Breakeven & Freight rate ($/T)",
            //   estimate: 0,
            // },
            // // { key: "vr18", description: "Off Hire Days", estimate: 0 },
            { key: "vr19", description: "Total Sea Days", estimate: 0 },
            { key: "vr20", description: "Total Port Days", estimate: 0 },
            { key: "vr17", description: "Net Voyage Days", estimate: 0 },
          ],
        },
      ],
    };
  }

  __getEstimatePL = (calData, colName) => {
    const { dollarUSLocale } = this.state;
    let _estimateData = Object.assign([], this.state.estimateData);
    _estimateData[0][colName] = calData["estimate"]["revenue"]["net_revenue"];
    _estimateData[0]["children"][0][colName] =
      calData["estimate"]["revenue"]["grossfreight"];
    _estimateData[0]["children"][1][colName] =
      calData["estimate"]["revenue"]["ttl_comm"];
    _estimateData[0]["children"][2][colName] =
      calData["estimate"]["revenue"]["extra_rev"];

    _estimateData[0]["children"][3][colName] =
    calData["estimate"]["revenue"]["demmurage_sales"];
    _estimateData[0]["children"][4][colName] =
    calData["estimate"]["revenue"]["demmurageCommission_sales"];
    _estimateData[0]["children"][5][colName] =
    calData["estimate"]["revenue"]["desmurage_sales"];
    _estimateData[0]["children"][6][colName] =
    calData["estimate"]["revenue"]["desmurageCommission_sales"];
    
      _estimateData[0]["children"][7][colName] =
      calData["estimate"]["revenue"]["gross_revenue"];
    _estimateData[0]["children"][8][colName] =
      calData["estimate"]["revenue"]["net_revenue"];

    _estimateData[1][colName] = calData["estimate"]["expenses"]["netexpanse"];
    _estimateData[1]["children"][0][colName] =
      calData["estimate"]["expenses"]["grossfright"];
    _estimateData[1]["children"][1][colName] =
      calData["estimate"]["expenses"]["ttl_comm"];
    _estimateData[1]["children"][2][colName] =
      calData["estimate"]["expenses"]["extra_rev"];

    _estimateData[1]["children"][3][colName] =
      calData["estimate"]["expenses"]["demmurage_purch"];  

      _estimateData[1]["children"][4][colName] =
      calData["estimate"]["expenses"]["demmurageCommission_purch"]; 

      _estimateData[1]["children"][5][colName] =
      calData["estimate"]["expenses"]["desmurage_purch"]; 

      _estimateData[1]["children"][6][colName] =
      calData["estimate"]["expenses"]["desmurageCommission_purch"]; 
    // _estimateData[1]["children"][3][colName] =
    //   calData["estimate"]["expenses"]["dem_des"];

    _estimateData[1]["children"][7][colName] =
      calData["estimate"]["expenses"]["other_exp"];
    _estimateData[1]["children"][8][colName] =
      calData["estimate"]["expenses"]["grossexpanse"];
    _estimateData[1]["children"][9][colName] =
      calData["estimate"]["expenses"]["netexpanse"];



    _estimateData[2][colName] =
      calData["estimate"]["voyage_result"]["profit_loss"];
    _estimateData[2]["children"][0][colName] =
      calData["estimate"]["voyage_result"]["profit_loss"];
    _estimateData[2]["children"][1][colName] =
      calData["estimate"]["voyage_result"]["daily_profit_loss"];

    _estimateData[2]["children"][2][colName] =
      calData["estimate"]["voyage_result"]["total_sea_days"];
    _estimateData[2]["children"][3][colName] =
      calData["estimate"]["voyage_result"]["total_port_days"];
    _estimateData[2]["children"][4][colName] =
      calData["estimate"]["voyage_result"]["net_voyage_days"];

    return _estimateData;
  };

  __pl = () => {
    let {
      estimateData,
      dollarUSLocale,
      formData,
      estimateDatavalue,
      showEstimatePl,
    } = this.state;
    let colName = "actual";

    if (showEstimatePl == true) {
      estimateData = this.__getEstimatePL(estimateDatavalue, "estimate");
    }

    let totalVoyageDays = formData["total_days"]
      ? isNaN(("" + formData["total_days"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["total_days"]).replaceAll(",", "") * 1
      : 0;
    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      grossRevenue = 0,
      netRevenue = 0,
      demmurage_amt_sales = 0,
      desmurage_amt_sales = 0,
      des_com_amt_sales =0,
      dem_com_amt_sales =0,
      demmurage_amt_purch = 0,
      desmurage_amt_purch = 0,
      des_com_amt_purch =0,
      dem_com_amt_purch =0,

      freightCommission = 0,
      demmurageCommission = 0,
      dispatch = 0,
      totalExpenses = 0,
      cpQty = 0;

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portItinerary = formData["portitinerary"];
      portItinerary.map((e) => {
        tsd += (e.tsd + "").replaceAll(",", "") * 1;
        tpd += (e.t_port_days + "").replaceAll(",", "") * 1;
      });
      tsd = tsd * 1;
      tpd = tpd * 1;
    }
    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;

    let bb = formData["bb"]
      ? isNaN(("" + formData["bb"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["bb"]).replaceAll(",", "") * 1
      : 0;
    let misCost = formData["mis_cost"]
      ? isNaN(("" + formData["mis_cost"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["mis_cost"]).replaceAll(",", "") * 1
      : 0;
    let hire = formData["tci_d_hire"]
      ? (formData["tci_d_hire"] + "").replaceAll(",", "") * 1
      : 0;
    let addPercentage = formData["add_percentage"]
      ? (formData["add_percentage"] + "").replaceAll(",", "") * 1
      : 0;
    let amt_add_percentage = hire * totalVoyageDays * addPercentage * 0.01;

    let broPercentage = formData["bro_percentage"]
      ? (formData["bro_percentage"] + "").replaceAll(",", "") * 1
      : 0;
    let amt_bro_percentage = hire * totalVoyageDays * broPercentage * 0.01;
    let avgfreightRate = 0,
      avgfreightRateOpt = 0,
      frtAvg = 0,
      cpQtyOpt = 0;

    let rev_frt = 0,
      rev_ttl_com = 0,
      rev_extra_rev = 0,
      rev_demdes = 0;
    let exp_frt = 0,
      exp_ttl_com = 0,
      exp_extra_rev = 0,
      exp_demdes = 0;

    if (formData && formData.hasOwnProperty("cargos")) {
      let cargos = formData["cargos"];
      cargos.map((e, i) => {
        let frtRate = 0,
          commission = 0,
          frtAmount = 0;
        let frt_rate = e.freight_rate || e.frat_rate || e.f_rate || 0;
        let frttype = e.frt_type || e.frt_type || e.frt_type || 0;
        let lumsum = e.lumsum || e.lumsum || e.lumsum || 0;
        let demmurage = e.dem_rate_pd || e.dem_rate_pd || e.dem_rate_pd || 0;
        let desmurage = e.des_rate_pd || e.des_rate_pd || e.des_rate_pd || 0;

        let cp_qty = e.cp_qty || e.quantity || 0;
        let b_commission = e.b_commission || e.commission || 0;

        let opt_per =
          parseFloat(e.option_percentage) || parseFloat(e.opt_percentage) || 0;
        mr = e.extra_rev ? (e.extra_rev + "").replaceAll(",", "") * 1 : 0;
        cpQty += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
        cpQtyOpt += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;

        if (opt_per !== 0) {
          let _cpq = cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
          let _fr = frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0;
          opt_per = opt_per ? (opt_per + "").replaceAll(",", "") : 0;
          let cpOptQty = _cpq + (_cpq * opt_per) / 100;
          cpQtyOpt = cpQtyOpt - _cpq + cpOptQty; //way to use opt% of cargo qty : refactor
          frtAmount = cpOptQty * _fr;
          if(frttype=="104"){
            frtAmount = lumsum ? (lumsum + "").replaceAll(",", "") * 1 : 0;
          }
          
          commission =
            (frtAmount *
              (b_commission
                ? (b_commission + "").replaceAll(",", "") * 1
                : 0)) /
            100;
          frtRate =
            (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
        } else {
          frtAmount =
            (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) *
            (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
          
          if(frttype=="104"){
            frtAmount = lumsum ? (lumsum + "").replaceAll(",", "") * 1 : 0;
          }  
          commission =
            (frtAmount *
              (b_commission
                ? (b_commission + "").replaceAll(",", "") * 1
                : 0)) /
            100;
          frtRate = frtAmount;
        }

        if (e.sp_type == 186) {
          rev_frt += frtAmount;
          rev_ttl_com += commission;
          rev_extra_rev += mr;
          demmurage_amt_sales += demmurage * 1;
          desmurage_amt_sales += desmurage * 1;

          dem_com_amt_sales += (demmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          des_com_amt_sales += (desmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;

        } else if (e.sp_type == 187) {
          exp_frt += frtAmount;
          exp_ttl_com += commission;
          exp_extra_rev += mr;
          demmurage_amt_purch += demmurage * 1;
          desmurage_amt_purch += desmurage * 1;

          dem_com_amt_purch += (demmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          des_com_amt_purch += (desmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          
        }

      
      
      });
      avgfreightRateOpt = fr / cpQtyOpt;
      avgfreightRate = frtAvg / cpQty;
    }

    let rev_gross_rev = rev_frt + rev_extra_rev + (demmurage_amt_sales - desmurage_amt_sales);
    
    let rev_net_rev = (rev_gross_rev - rev_ttl_com)  - (dem_com_amt_sales + des_com_amt_sales); 
    
    rev_net_rev = rev_net_rev.toFixed(2);

    let exp_gross_expense = (exp_frt + misCost + exp_demdes - exp_extra_rev) - (demmurage_amt_purch + desmurage_amt_purch);
    exp_gross_expense = exp_gross_expense.toFixed(2);

    let exp_net_expense = (exp_gross_expense - exp_ttl_com) + (dem_com_amt_purch - des_com_amt_purch);  
    exp_net_expense = exp_net_expense.toFixed(2);

    totalVoyageDays = totalVoyageDays.toFixed(2);

    if (formData.hasOwnProperty("estimatePL")) {
      estimateData = this.__getEstimatePL(this.state.formData["estimate"]);
    }

    estimateData[0][colName] = dollarUSLocale.format(rev_net_rev);
    let netreves = estimateData[0]["estimate"]
      ? estimateData[0]["estimate"].replaceAll(",", "") * 1
      : 0;
    let netrevac = estimateData[0]["actual"]
      ? estimateData[0]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["Diff"] = (netrevac - netreves).toFixed(2);
    estimateData[0]["perDiff"] =
      netreves == 0 ? 0 : (((netrevac - netreves) / netreves) * 100).toFixed(2);
    estimateData[0]["posted"] = "N/A";
    estimateData[0]["cash_in"] = "N/A";
    estimateData[0]["sec_variance"] = "N/A";
    estimateData[0]["sec_per"] = "N/A";

    estimateData[0]["children"][0][colName] = dollarUSLocale.format(
      rev_frt.toFixed(2)
    );
    let revfrtes = estimateData[0]["children"][0]["estimate"]
      ? estimateData[0]["children"][0]["estimate"].replaceAll(",", "") * 1
      : 0;
    let revfrtac = estimateData[0]["children"][0]["actual"]
      ? estimateData[0]["children"][0]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["children"][0]["Diff"] = (revfrtac - revfrtes).toFixed(2);
    estimateData[0]["children"][0]["perDiff"] =
      revfrtac == 0 ? 0 : ((revfrtac - revfrtes) / revfrtes).toFixed(2);
    estimateData[0]["children"][0]["posted"] = "N/A";
    estimateData[0]["children"][0]["cash_in"] = "N/A";
    estimateData[0]["children"][0]["sec_variance"] = "N/A";
    estimateData[0]["children"][0]["sec_per"] = "N/A";

    estimateData[0]["children"][1][colName] = dollarUSLocale.format(
      rev_ttl_com
    );
    let revttlcomes = estimateData[0]["children"][1]["estimate"]
      ? estimateData[0]["children"][1]["estimate"].replaceAll(",", "") * 1
      : 0;
    let revttlcomac = estimateData[0]["children"][1]["actual"]
      ? estimateData[0]["children"][1]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["children"][1]["Diff"] = (
      revttlcomac - revttlcomes
    ).toFixed(2);
    estimateData[0]["children"][1]["perDiff"] =
      revttlcomes == 0
        ? 0
        : (((revttlcomac - revttlcomes) / revttlcomes) * 100).toFixed(2);
    estimateData[0]["children"][1]["posted"] = "N/A";
    estimateData[0]["children"][1]["cash_in"] = "N/A";
    estimateData[0]["children"][1]["sec_variance"] = "N/A";
    estimateData[0]["children"][1]["sec_per"] = "N/A";

    estimateData[0]["children"][2][colName] = dollarUSLocale.format(
      rev_extra_rev.toFixed(2)
    );
    let revmiscreves = estimateData[0]["children"][2]["estimate"]
      ? estimateData[0]["children"][2]["estimate"].replaceAll(",", "") * 1
      : 0;
    let revmiscrevac = estimateData[0]["children"][2]["actual"]
      ? estimateData[0]["children"][2]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["children"][2]["Diff"] = (
      revmiscrevac - revmiscreves
    ).toFixed(2);
    estimateData[0]["children"][2]["perDiff"] =
      revmiscreves == 0
        ? 0
        : (((revmiscrevac - revmiscreves) / revmiscreves) * 100).toFixed(2);
    estimateData[0]["children"][2]["posted"] = "N/A";
    estimateData[0]["children"][2]["cash_in"] = "N/A";
    estimateData[0]["children"][2]["sec_variance"] = "N/A";
    estimateData[0]["children"][2]["sec_per"] = "N/A";



    estimateData[0]["children"][3][colName] = dollarUSLocale.format(demmurage_amt_sales.toFixed(2));
    let demmuragees=estimateData[0]["children"][3]['estimate']?(estimateData[0]["children"][3]['estimate']).replaceAll(",","")*1:0
    let demmurageac=estimateData[0]["children"][3]['actual']?(estimateData[0]["children"][3]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][3]['Diff']=(demmurageac-demmuragees).toFixed(2);
    estimateData[0]["children"][3]['perDiff']=demmuragees==0?0:(((demmurageac-demmuragees)/demmuragees)*100).toFixed(2);
    estimateData[0]["children"][3]['posted']="N/A";
    estimateData[0]["children"][3]['cash_in']="N/A";
    estimateData[0]["children"][3]['sec_variance']="N/A";
    estimateData[0]["children"][3]['sec_per']="N/A"
  
    estimateData[0]["children"][4][colName] = dollarUSLocale.format(dem_com_amt_sales.toFixed(2));
    let desmuragees=estimateData[0]["children"][4]['estimate']?(estimateData[0]["children"][4]['estimate']).replaceAll(",","")*1:0
    let desmurageac=estimateData[0]["children"][4]['actual']?(estimateData[0]["children"][4]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][4]['Diff']=(desmurageac-demmuragees).toFixed(2);
    estimateData[0]["children"][4]['perDiff']=desmuragees==0?0:(((desmurageac-demmuragees)/desmuragees)*100).toFixed(2);
    estimateData[0]["children"][4]['posted']="N/A";
    estimateData[0]["children"][4]['cash_in']="N/A";
    estimateData[0]["children"][4]['sec_variance']="N/A";
    estimateData[0]["children"][4]['sec_per']="N/A"
  
    estimateData[0]["children"][5][colName] = dollarUSLocale.format(desmurage_amt_sales.toFixed(2));
    let demcomamtes=estimateData[0]["children"][5]['estimate']?(estimateData[0]["children"][5]['estimate']).replaceAll(",","")*1:0
    let demcomamtac=estimateData[0]["children"][5]['actual']?(estimateData[0]["children"][5]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][5]['Diff']=(demcomamtac-demcomamtes).toFixed(2);
    estimateData[0]["children"][5]['perDiff']=demcomamtes==0?0:(((demcomamtac-demcomamtes)/demcomamtes)*100).toFixed(2);
    estimateData[0]["children"][5]['posted']="N/A";
    estimateData[0]["children"][5]['cash_in']="N/A";
    estimateData[0]["children"][5]['sec_variance']="N/A";
    estimateData[0]["children"][5]['sec_per']="N/A"
  
    estimateData[0]["children"][6][colName] = dollarUSLocale.format(des_com_amt_sales.toFixed(2));
    let descomamtes=estimateData[0]["children"][6]['estimate']?(estimateData[0]["children"][6]['estimate']).replaceAll(",","")*1:0
    let descomamtac=estimateData[0]["children"][6]['actual']?(estimateData[0]["children"][6]['actual']).replaceAll(",","")*1:0
    estimateData[0]["children"][6]['Diff']=(descomamtac-descomamtes).toFixed(2);
    estimateData[0]["children"][6]['perDiff']=descomamtes==0?0:(((descomamtac-descomamtes)/descomamtes)*100).toFixed(2);
    estimateData[0]["children"][6]['posted']="N/A";
    estimateData[0]["children"][6]['cash_in']="N/A";
    estimateData[0]["children"][6]['sec_variance']="N/A";
    estimateData[0]["children"][6]['sec_per']="N/A"

    estimateData[0]["children"][7][colName] = dollarUSLocale.format(
      rev_gross_rev
    );
    let revgrsreves = estimateData[0]["children"][7]["estimate"]
      ? estimateData[0]["children"][7]["estimate"].replaceAll(",", "") * 1
      : 0;
    let revgrsrevac = estimateData[0]["children"][7]["actual"]
      ? estimateData[0]["children"][7]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["children"][7]["Diff"] = (
      revgrsrevac - revgrsreves
    ).toFixed(2);
    estimateData[0]["children"][7]["perDiff"] =
      revgrsreves == 0
        ? 0
        : (((revgrsrevac - revgrsreves) / revgrsreves) * 100).toFixed(2);
    estimateData[0]["children"][7]["posted"] = "N/A";
    estimateData[0]["children"][7]["cash_in"] = "N/A";
    estimateData[0]["children"][7]["sec_variance"] = "N/A";
    estimateData[0]["children"][7]["sec_per"] = "N/A";

    estimateData[0]["children"][8][colName] = dollarUSLocale.format(
      rev_net_rev
    );
    let revnetreves = estimateData[0]["children"][8]["estimate"]
      ? estimateData[0]["children"][8]["estimate"].replaceAll(",", "") * 1
      : 0;
    let revnetrevac = estimateData[0]["children"][8]["actual"]
      ? estimateData[0]["children"][8]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[0]["children"][8]["Diff"] = (
      revnetrevac - revnetreves
    ).toFixed(2);
    estimateData[0]["children"][8]["perDiff"] =
      revnetreves == 0
        ? 0
        : (((revnetrevac - revnetreves) / revnetreves) * 100).toFixed(2);
    estimateData[0]["children"][8]["posted"] = "N/A";
    estimateData[0]["children"][8]["cash_in"] = "N/A";
    estimateData[0]["children"][8]["sec_variance"] = "N/A";
    estimateData[0]["children"][8]["sec_per"] = "N/A";

    
    estimateData[1][colName] = dollarUSLocale.format(exp_net_expense);
    let exp_netexpnsses = estimateData[1]["estimate"]
      ? estimateData[1]["estimate"].replaceAll(",", "") * 1
      : 0;
    let exp_netexpnssac = estimateData[1]["actual"]
      ? estimateData[1]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["Diff"] = (exp_netexpnssac - exp_netexpnsses).toFixed(2);
    estimateData[1]["perDiff"] =
      exp_netexpnssac == 0
        ? 0
        : (
            ((exp_netexpnssac - exp_netexpnsses) / exp_netexpnsses) *
            100
          ).toFixed(2);
    estimateData[1]["posted"] = "N/A";
    estimateData[1]["cash_in"] = "N/A";
    estimateData[1]["sec_variance"] = "N/A";
    estimateData[1]["sec_per"] = "N/A";



    estimateData[1]["children"][0][colName] = dollarUSLocale.format(
      exp_frt.toFixed(2)
    );
    let expfrtes = estimateData[1]["children"][0]["estimate"]
      ? estimateData[1]["children"][0]["estimate"].replaceAll(",", "") * 1
      : 0;
    let expfrtac = estimateData[1]["children"][0]["actual"]
      ? estimateData[1]["children"][0]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][0]["Diff"] = (expfrtac - expfrtes).toFixed(2);
    estimateData[1]["children"][0]["perDiff"] =
      expfrtes == 0 ? 0 : (((expfrtac - expfrtes) / expfrtes) * 100).toFixed(2);
    estimateData[1]["children"][0]["posted"] = "N/A";
    estimateData[1]["children"][0]["cash_in"] = "N/A";
    estimateData[1]["children"][0]["sec_variance"] = "N/A";
    estimateData[1]["children"][0]["sec_per"] = "N/A";

    estimateData[1]["children"][1][colName] = dollarUSLocale.format(
      exp_ttl_com.toFixed(2)
    );
    let exp_totalcomes = estimateData[1]["children"][1]["estimate"]
      ? estimateData[1]["children"][1]["estimate"].replaceAll(",", "") * 1
      : 0;
    let exp_totalcomac = estimateData[1]["children"][1]["actual"]
      ? estimateData[1]["children"][1]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][1]["Diff"] = (
      exp_totalcomac - exp_totalcomes
    ).toFixed(2);
    estimateData[1]["children"][1]["perDiff"] =
      exp_totalcomes == 0
        ? 0
        : (((exp_totalcomac - exp_totalcomes) / exp_totalcomes) * 100).toFixed(
            2
          );
    estimateData[1]["children"][1]["posted"] = "N/A";
    estimateData[1]["children"][1]["cash_in"] = "N/A";
    estimateData[1]["children"][1]["sec_variance"] = "N/A";
    estimateData[1]["children"][1]["sec_per"] = "N/A";


    estimateData[1]["children"][2][colName] = dollarUSLocale.format(
      exp_extra_rev.toFixed(2)
    );
    let exp_extrareves = estimateData[1]["children"][2]["estimate"]
      ? estimateData[1]["children"][2]["estimate"].replaceAll(",", "") * 1
      : 0;
    let exp_extrarevac = estimateData[1]["children"][2]["actual"]
      ? estimateData[1]["children"][2]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][2]["Diff"] = (
      exp_extrarevac - exp_extrareves
    ).toFixed(2);
    estimateData[1]["children"][2]["perDiff"] =
      exp_extrareves == 0
        ? 0
        : (((exp_extrarevac - exp_extrareves) / exp_extrareves) * 100).toFixed(
            2
          );
    estimateData[1]["children"][2]["posted"] = "N/A";
    estimateData[1]["children"][2]["cash_in"] = "N/A";
    estimateData[1]["children"][2]["sec_variance"] = "N/A";
    estimateData[1]["children"][2]["sec_per"] = "N/A";

    estimateData[1]["children"][3][colName] = dollarUSLocale.format(
      demmurage_amt_purch.toFixed(2)
    );
    let demmurage_amt_purches = estimateData[1]["children"][3]["estimate"]
      ? estimateData[1]["children"][3]["estimate"].replaceAll(",", "") * 1
      : 0;
    let demmurage_amt_purchac = estimateData[1]["children"][3]["actual"]
      ? estimateData[1]["children"][3]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][3]["Diff"] = (
      demmurage_amt_purchac - demmurage_amt_purches
    ).toFixed(2);
    estimateData[1]["children"][3]["perDiff"] =
    demmurage_amt_purches == 0
        ? 0
        : (((demmurage_amt_purchac - demmurage_amt_purches) / demmurage_amt_purches) * 100).toFixed(2);
    estimateData[1]["children"][3]["posted"] = "N/A";
    estimateData[1]["children"][3]["cash_in"] = "N/A";
    estimateData[1]["children"][3]["sec_variance"] = "N/A";
    estimateData[1]["children"][3]["sec_per"] = "N/A";


    estimateData[1]["children"][4][colName] = dollarUSLocale.format(
      dem_com_amt_purch.toFixed(2)
    );
    let dem_com_amt_purches = estimateData[1]["children"][4]["estimate"]
      ? estimateData[1]["children"][4]["estimate"].replaceAll(",", "") * 1
      : 0;
    let dem_com_amt_purchac = estimateData[1]["children"][4]["actual"]
      ? estimateData[1]["children"][4]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][4]["Diff"] = (
      dem_com_amt_purchac - dem_com_amt_purches
    ).toFixed(2);
    estimateData[1]["children"][4]["perDiff"] =
    dem_com_amt_purches == 0
        ? 0
        : (((dem_com_amt_purchac - dem_com_amt_purches) / dem_com_amt_purches) * 100).toFixed(2);
    estimateData[1]["children"][4]["posted"] = "N/A";
    estimateData[1]["children"][4]["cash_in"] = "N/A";
    estimateData[1]["children"][4]["sec_variance"] = "N/A";
    estimateData[1]["children"][4]["sec_per"] = "N/A";


    estimateData[1]["children"][5][colName] = dollarUSLocale.format(
      desmurage_amt_purch.toFixed(2)
    );
    let desmurage_amt_purches = estimateData[1]["children"][5]["estimate"]
      ? estimateData[1]["children"][5]["estimate"].replaceAll(",", "") * 1
      : 0;
    let desmurage_amt_purchac = estimateData[1]["children"][5]["actual"]
      ? estimateData[1]["children"][5]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][5]["Diff"] = (
      desmurage_amt_purchac - desmurage_amt_purches
    ).toFixed(2);
    estimateData[1]["children"][5]["perDiff"] =
    desmurage_amt_purches == 0
        ? 0
        : (((desmurage_amt_purchac - desmurage_amt_purches) / desmurage_amt_purches) * 100).toFixed(2);
    estimateData[1]["children"][5]["posted"] = "N/A";
    estimateData[1]["children"][5]["cash_in"] = "N/A";
    estimateData[1]["children"][5]["sec_variance"] = "N/A";
    estimateData[1]["children"][5]["sec_per"] = "N/A";
   

    estimateData[1]["children"][6][colName] = dollarUSLocale.format(
      des_com_amt_purch.toFixed(2)
    );
    let des_com_amt_purches = estimateData[1]["children"][6]["estimate"]
      ? estimateData[1]["children"][6]["estimate"].replaceAll(",", "") * 1
      : 0;
    let des_com_amt_purchac = estimateData[1]["children"][6]["actual"]
      ? estimateData[1]["children"][6]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][6]["Diff"] = (
      des_com_amt_purchac - des_com_amt_purches
    ).toFixed(2);
    estimateData[1]["children"][6]["perDiff"] =
    des_com_amt_purches == 0
        ? 0
        : (((des_com_amt_purchac - des_com_amt_purches) / des_com_amt_purches) * 100).toFixed(2);
    estimateData[1]["children"][6]["posted"] = "N/A";
    estimateData[1]["children"][6]["cash_in"] = "N/A";
    estimateData[1]["children"][6]["sec_variance"] = "N/A";
    estimateData[1]["children"][6]["sec_per"] = "N/A";

    // estimateData[1]["children"][7][colName] = dollarUSLocale.format(
    //   demmurage_amt_purch.toFixed(2)
    // );
    // let exp_demdeses = estimateData[1]["children"][7]["estimate"]
    //   ? estimateData[1]["children"][7]["estimate"].replaceAll(",", "") * 1
    //   : 0;
    // let exp_demdesac = estimateData[1]["children"][7]["actual"]
    //   ? estimateData[1]["children"][7]["actual"].replaceAll(",", "") * 1
    //   : 0;
    // estimateData[1]["children"][7]["Diff"] = (
    //   exp_demdesac - exp_demdeses
    // ).toFixed(2);
    // estimateData[1]["children"][7]["perDiff"] =
    //   exp_demdeses == 0
    //     ? 0
    //     : (((exp_demdesac - exp_demdeses) / exp_demdeses) * 100).toFixed(2);
    // estimateData[1]["children"][7]["posted"] = "N/A";
    // estimateData[1]["children"][7]["cash_in"] = "N/A";
    // estimateData[1]["children"][7]["sec_variance"] = "N/A";
    // estimateData[1]["children"][7]["sec_per"] = "N/A";





    estimateData[1]["children"][7][colName] = dollarUSLocale.format(
      misCost.toFixed(2)
    );
    let miscostes = estimateData[1]["children"][7]["estimate"]
      ? estimateData[1]["children"][7]["estimate"].replaceAll(",", "") * 1
      : 0;
    let miscostac = estimateData[1]["children"][7]["actual"]
      ? estimateData[1]["children"][7]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][7]["Diff"] = (miscostac - miscostes).toFixed(2);
    estimateData[1]["children"][7]["perDiff"] =
      miscostes == 0
        ? 0
        : (((miscostac - miscostes) / miscostes) * 100).toFixed(2);
    estimateData[1]["children"][7]["posted"] = "N/A";
    estimateData[1]["children"][7]["cash_in"] = "N/A";
    estimateData[1]["children"][7]["sec_variance"] = "N/A";
    estimateData[1]["children"][7]["sec_per"] = "N/A";

    estimateData[1]["children"][8][colName] = dollarUSLocale.format(
      exp_gross_expense
    );
    let exp_grsexpnses = estimateData[1]["children"][8]["estimate"]
      ? estimateData[1]["children"][8]["estimate"].replaceAll(",", "") * 1
      : 0;
    let exp_grsexpnsac = estimateData[1]["children"][8]["actual"]
      ? estimateData[1]["children"][8]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][8]["Diff"] = (
      exp_grsexpnsac - exp_grsexpnses
    ).toFixed(2);
    estimateData[1]["children"][8]["perDiff"] =
      exp_grsexpnses == 0
        ? 0
        : (((exp_grsexpnsac - exp_grsexpnses) / exp_grsexpnses) * 100).toFixed(
            2
          );
    estimateData[1]["children"][8]["posted"] = "N/A";
    estimateData[1]["children"][8]["cash_in"] = "N/A";
    estimateData[1]["children"][8]["sec_variance"] = "N/A";
    estimateData[1]["children"][8]["sec_per"] = "N/A";


    estimateData[1]["children"][9][colName] = dollarUSLocale.format(
      exp_net_expense
    );

    let exp_netexpnses = estimateData[1]["children"][9]["estimate"]
      ? estimateData[1]["children"][9]["estimate"].replaceAll(",", "") * 1
      : 0;
    let exp_netexpnsac = estimateData[1]["children"][9]["actual"]
      ? estimateData[1]["children"][9]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[1]["children"][9]["Diff"] = (
      exp_netexpnsac - exp_netexpnses
    ).toFixed(2);
    estimateData[1]["children"][9]["perDiff"] =
      exp_netexpnses == 0
        ? 0
        : (((exp_netexpnsac - exp_netexpnses) / exp_netexpnses) * 100).toFixed(
            2
          );
    estimateData[1]["children"][9]["posted"] = "N/A";
    estimateData[1]["children"][9]["cash_in"] = "N/A";
    estimateData[1]["children"][9]["sec_variance"] = "N/A";
    estimateData[1]["children"][9]["sec_per"] = "N/A";


    let itemValue = rev_net_rev - exp_net_expense;
    estimateData[2][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2))
        : dollarUSLocale.format((itemValue * 1).toFixed(2));
    let vygreses = estimateData[2]["estimate"]
      ? estimateData[2]["estimate"].replaceAll(",", "") * 1
      : 0;
    let vygresac = estimateData[2]["actual"]
      ? estimateData[2]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["Diff"] = (vygresac - vygreses).toFixed(2);
    estimateData[2]["perDiff"] = (
      ((vygresac - vygreses) / vygreses) *
      100
    ).toFixed(2);
    estimateData[2]["posted"] = "N/A";
    estimateData[2]["cash_in"] = "N/A";
    estimateData[2]["sec_variance"] = "N/A";
    estimateData[2]["sec_per"] = "N/A";

    estimateData[2]["children"][0][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2))
        : dollarUSLocale.format((itemValue * 1).toFixed(2));
    let profitlses = estimateData[2]["children"][0]["estimate"]
      ? estimateData[2]["children"][0]["estimate"].replaceAll(",", "") * 1
      : 0;
    let profitlsac = estimateData[2]["children"][0]["actual"]
      ? estimateData[2]["children"][0]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["children"][0]["Diff"] = (profitlsac - profitlses).toFixed(
      2
    );
    estimateData[2]["children"][0]["perDiff"] =
      profitlses == 0
        ? 0
        : (((profitlsac - profitlses) / profitlses) * 100).toFixed(2);
    estimateData[2]["children"][0]["posted"] = "N/A";
    estimateData[2]["children"][0]["cash_in"] = "N/A";
    estimateData[2]["children"][0]["sec_variance"] = "N/A";
    estimateData[2]["children"][0]["sec_per"] = "N/A";

    estimateData[2]["children"][1][colName] = dollarUSLocale.format(
      (itemValue / (totalVoyageDays - 0)).toFixed(2)
    );
    let dlprofitlses = estimateData[2]["children"][1]["estimate"]
      ? estimateData[2]["children"][1]["estimate"].replaceAll(",", "") * 1
      : 0;
    let dlprofitlsac = estimateData[2]["children"][1]["actual"]
      ? estimateData[2]["children"][1]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["children"][1]["Diff"] = (
      dlprofitlsac - dlprofitlses
    ).toFixed(2);
    estimateData[2]["children"][1]["perDiff"] =
      dlprofitlses == 0
        ? 0
        : (((dlprofitlsac - dlprofitlses) / dlprofitlses) * 100).toFixed(2);
    estimateData[2]["children"][1]["posted"] = "N/A";
    estimateData[2]["children"][1]["cash_in"] = "N/A";
    estimateData[2]["children"][1]["sec_variance"] = "N/A";
    estimateData[2]["children"][1]["sec_per"] = "N/A";

    estimateData[2]["children"][2][colName] = dollarUSLocale.format(
      tsd.toFixed(2)
    );
    let tsdes = estimateData[2]["children"][2]["estimate"]
      ? estimateData[2]["children"][1]["estimate"].replaceAll(",", "") * 1
      : 0;
    let tsdac = estimateData[2]["children"][2]["actual"]
      ? estimateData[2]["children"][1]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["children"][2]["Diff"] = (tsdac - tsdes).toFixed(2);
    estimateData[2]["children"][2]["perDiff"] =
      tsdes == 0 ? 0 : (((tsdac - tsdes) / tsdes) * 100).toFixed(2);
    estimateData[2]["children"][2]["posted"] = "N/A";
    estimateData[2]["children"][2]["cash_in"] = "N/A";
    estimateData[2]["children"][2]["sec_variance"] = "N/A";
    estimateData[2]["children"][2]["sec_per"] = "N/A";

    estimateData[2]["children"][3][colName] = dollarUSLocale.format(
      tpd.toFixed(2)
    );
    let todes = estimateData[2]["children"][3]["estimate"]
      ? estimateData[2]["children"][3]["estimate"].replaceAll(",", "") * 1
      : 0;
    let tpdac = estimateData[2]["children"][3]["actual"]
      ? estimateData[2]["children"][3]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["children"][3]["Diff"] = (tpdac - todes).toFixed(2);
    estimateData[2]["children"][3]["perDiff"] =
      todes == 0 ? 0 : (((tpdac - todes) / todes) * 100).toFixed(2);
    estimateData[2]["children"][3]["posted"] = "N/A";
    estimateData[2]["children"][3]["cash_in"] = "N/A";
    estimateData[2]["children"][3]["sec_variance"] = "N/A";
    estimateData[2]["children"][3]["sec_per"] = "N/A";

    estimateData[2]["children"][4][colName] = dollarUSLocale.format(
      totalVoyageDays
    );
    let netdayes = estimateData[2]["children"][4]["estimate"]
      ? estimateData[2]["children"][4]["estimate"].replaceAll(",", "") * 1
      : 0;
    let netdayac = estimateData[2]["children"][4]["actual"]
      ? estimateData[2]["children"][4]["actual"].replaceAll(",", "") * 1
      : 0;
    estimateData[2]["children"][4]["Diff"] = (netdayac - netdayes).toFixed(2);
    estimateData[2]["children"][4]["perDiff"] =
      netdayes == 0 ? 0 : (((netdayac - netdayes) / netdayes) * 100).toFixed(2);
    estimateData[2]["children"][4]["posted"] = "N/A";
    estimateData[2]["children"][4]["cash_in"] = "N/A";
    estimateData[2]["children"][4]["sec_variance"] = "N/A";
    estimateData[2]["children"][4]["sec_per"] = "N/A";

    return estimateData;
  };

  componentDidMount() {
    // console.log('call count');
    this.setState(
      {
        ...this.state,
        showPL: false,
        estimateData: this.__pl(),
      },
      () => this.setState({ ...this.state, showPL: true })
    );
  }

  render() {
    const { estimateData, showPL, viewTabs } = this.state;
    return (
      <>
        <div>
          <ToolbarUI routeUrl={"pl-main"} callback={(e) => this.callback(e)} />
        </div>

        {showPL === true ? (
          <Tabs defaultActiveKey="1">
            {viewTabs.map((e) => {
              if (e === "Actual &  Operation View") {
                return (
                  <TabPane tab={e} key="ao2">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Estimate View") {
                return (
                  <TabPane tab={e} key="ev1">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns2}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Account View") {
                return (
                  <TabPane tab={e} key="av3">
                    Accounts
                  </TabPane>
                );
              }
            })}
          </Tabs>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </>
    );
  }
}

export default PL;
