import React, { Component } from "react";
import { Table, Tabs, Input, Row, Col, Form, Spin, Alert } from "antd";
//import ToolbarUI from "components/ToolbarUI";

import ToolbarUI from "../../../../components/ToolbarUI";
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;

const columns = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Invoice No.",
    dataIndex: "invoice_no",
    key: "invoice_no",
    width: "8%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "8%",
  },
  {
    title: "Actual",
    dataIndex: "actual",
    key: "actual",
    width: "8%",
  },
  {
    title: "Posted",
    dataIndex: "posted",
    key: "posted",
    width: "8%",
  },
  {
    title: "Cash In",
    dataIndex: "cash_in",
    key: "cash_in",
    width: "8%",
  },
  {
    title: "Est. Vs Act",
    dataIndex: "variance",
    key: "variance",
    width: "8%",
  },
  {
    title: "% Est. Vs Act",
    dataIndex: "per",
    key: "per",
    width: "8%",
  },
  {
    title: "Post Vs Cash",
    dataIndex: "sec_variance",
    key: "sec_variance",
    width: "8%",
  },
  {
    title: "% Post Vs Cash",
    dataIndex: "sec_per",
    key: "sec_per",
    width: "8%",
  },
];

const columns2 = [
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    width: "20%",
  },
  {
    title: "Estimated",
    dataIndex: "estimate",
    key: "estimate",
    width: "12%",
    align: "right",
  },
  { title: "", dataIndex: "", key: "blank", width: "68%" },
];

class PL extends Component {
  callback = (evt) => {};

  constructor(props) {
    super(props);

    this.state = {
      dollarUSLocale: Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      formData: this.props.formData || {},
      showPL: false,
      vesselAmount: 0,
      viewTabs: this.props.viewTabs || [
        "Estimate View",
        "Actual &  Operation View",
        "Account View",
      ],
      estimateData: [
        {
          key: "revenue",
          description: "Revenue",
          estimate: 0,
          children: [
            { key: "rev00", description: "Gross Frieght", estimate: 0 },
            { key: "rev01", description: "Ttl Comm", estimate: 0 },
            { key: "rev02", description: "Other Revenue", estimate: 0 },
            { key: "rev03", description: "Dem Amt", estimate: 0 },
            { key: "rev04", description: "Dem Comm Amt", estimate: 0 },
            { key: "rev05", description: "Des Amt", estimate: 0 },
            { key: "rev06", description: "Des Comm Amt", estimate: 0 },
            { key: "rev07", description: "Gross Revenue", estimate: 0 },
            { key: "rev08", description: "Net Revenue", estimate: 0 },
          ],
        },
        {
          key: "expenses",
          description: "Expenses.",
          estimate: 0,

          children: [
            { key: "ex10", description: "Gross Frieght", estimate: 0 },
            { key: "ex11", description: "Ttl Comm", estimate: 0 },
            { key: "ex12", description: "Other Revenue", estimate: 0 },
            { key: "rev13", description: "Dem Amt", estimate: 0 },
            { key: "rev14", description: "Dem Comm Amt", estimate: 0 },
            { key: "rev15", description: "Des Amt", estimate: 0 },
            { key: "rev16", description: "Des Comm Amt", estimate: 0 },
            { key: "ex17", description: "Other Exp", estimate: 0 },
            { key: "ex18", description: "Gross Expenses", estimate: 0 },
            { key: "ex19", description: "Net Expenses", estimate: 0 },
          ],
        },
        {
          key: "voyage-result",
          description: "Voyage Result",
          estimate: 0,
          children: [
            { key: "vr10", description: "Profit (Loss)", estimate: 0 },
            { key: "vr11", description: "Daily Profit (Loss)", estimate: 0 },
            // { key: "vr13", description: "TCE Hire ( Net Daily )", estimate: 0 },
            // { key: "vr14", description: "Gross TCE", estimate: 0 },
            // { key: "vr15", description: "Freight rate ($/T)", estimate: 0 },
            // {
            //   key: "vr16",
            //   description: "Breakeven & Freight rate ($/T)",
            //   estimate: 0,
            // },
            // // { key: "vr18", description: "Off Hire Days", estimate: 0 },
            { key: "vr19", description: "Total Sea Days", estimate: 0 },
            { key: "vr20", description: "Total Port Days", estimate: 0 },
            { key: "vr17", description: "Net Voyage Days", estimate: 0 },
          ],
        },
      ],
    };
  }

  __pl = () => {
    let { estimateData, dollarUSLocale, formData } = this.state;
    let totalSeaConsumption = 0,
      totalPortConsumption = 0,
      totalArriveConsumption = 0,
      totalDepConsumption = 0,
      totalAdjConsumption = 0,
      colName = "estimate";
    if (formData.hasOwnProperty("estimatePL")) {
      colName = "actual";
    }

    let totalVoyageDays = formData["total_days"]
      ? isNaN(("" + formData["total_days"]).replaceAll(",", "") * 1)
        ? 0
        : ("" + formData["total_days"]).replaceAll(",", "") * 1
      : 0;
    let tsd = 0,
      tpd = 0,
      pi = 0,
      fr = 0,
      mr = 0,
      grossRevenue = 0,
      netRevenue = 0,
      demmurage_amt_sales = 0,
      desmurage_amt_sales = 0,
      demcommissionamt_sales=0,
      descommissionamt_sales=0,
      demmurage_amt_purch = 0,
      desmurage_amt_purch = 0,
      demcommissionamt_purch=0,
      descommissionamt_purch=0,
      freightCommission = 0,
      
      dispatch = 0,
      totalExpenses = 0,
      cpQty = 0;

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portItinerary = formData["portitinerary"];
      portItinerary.map((e) => {
        tsd += (e.tsd + "").replaceAll(",", "") * 1;
        tpd += (e.t_port_days + "").replaceAll(",", "") * 1;
      });
      tsd = tsd * 1;
      tpd = tpd * 1;
    }
    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;

    let bb = formData["bb"] ? (isNaN(("" + formData["bb"]).replaceAll(",", "") * 1) ? 0 : ("" + formData["bb"]).replaceAll(",", "") * 1) : 0;
    let misCost = formData["mis_cost"] ? (isNaN(("" + formData["mis_cost"]).replaceAll(",", "") * 1) ? 0 : ("" + formData["mis_cost"]).replaceAll(",", "") * 1) : 0;
    let hire = formData["tci_d_hire"] ? (formData["tci_d_hire"] + "").replaceAll(",", "") * 1 : 0;
    let mis_cost = formData["mis_cost"] ? (formData["mis_cost"] + "").replaceAll(",", "") * 1 : 0;
    let addPercentage = formData["add_percentage"] ? (formData["add_percentage"] + "").replaceAll(",", "") * 1 : 0;
    let amt_add_percentage = hire * totalVoyageDays * addPercentage * 0.01;

    let broPercentage = formData["bro_percentage"] ? (formData["bro_percentage"] + "").replaceAll(",", "") * 1 : 0;
    let amt_bro_percentage = hire * totalVoyageDays * broPercentage * 0.01;

    let portExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      seaExpenses = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivalrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      deprev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 },
      arrivaladjuestmentrev = { ifo: 0, mgo: 0, vlsfo: 0, ulsfo: 0, lsmgo: 0 };

    if (formData && formData.hasOwnProperty("bunkerdetails")) {
      let bunkerDetails = formData["bunkerdetails"];
      let i = 0;
      bunkerDetails.map((e, i, { length }) => {
        seaExpenses["ifo"] += e["ifo"] ? (isNaN(("" + e["ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ifo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["mgo"] += e["mgo"] ? (isNaN(("" + e["mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["mgo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["vlsfo"] += e["vlsfo"] ? (isNaN(("" + e["vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["vlsfo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["lsmgo"] += e["lsmgo"] ? (isNaN(("" + e["lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["lsmgo"]).replaceAll(",", "") * 1) : 0;
        seaExpenses["ulsfo"] += e["ulsfo"] ? (isNaN(("" + e["ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["ulsfo"]).replaceAll(",", "") * 1) : 0;

        portExpenses["ifo"] += e["pc_ifo"] ? (isNaN(("" + e["pc_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ifo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["mgo"] += e["pc_mgo"] ? (isNaN(("" + e["pc_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_mgo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["vlsfo"] += e["pc_vlsfo"] ? (isNaN(("" + e["pc_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_vlsfo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["lsmgo"] += e["pc_lsmgo"] ? (isNaN(("" + e["pc_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_lsmgo"]).replaceAll(",", "") * 1) : 0;
        portExpenses["ulsfo"] += e["pc_ulsfo"] ? (isNaN(("" + e["pc_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["pc_ulsfo"]).replaceAll(",", "") * 1) : 0;

        if (i == 0) {
          arrivalrev["ifo"] += e["arob_ifo"] ? (isNaN(("" + e["arob_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_ifo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["mgo"] += e["arob_mgo"] ? (isNaN(("" + e["arob_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_mgo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["vlsfo"] += e["arob_vlsfo"] ? (isNaN(("" + e["arob_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_vlsfo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["lsmgo"] += e["arob_lsmgo"] ? (isNaN(("" + e["arob_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_lsmgo"]).replaceAll(",", "") * 1) : 0;
          arrivalrev["ulsfo"] += e["arob_ulsfo"] ? (isNaN(("" + e["arob_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["arob_ulsfo"]).replaceAll(",", "") * 1) : 0;
        }

        //last element
        if (i + 1 === length) {
          deprev["ifo"] += e["dr_ifo"] ? (isNaN(("" + e["dr_ifo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_ifo"]).replaceAll(",", "") * 1) : 0;
          deprev["mgo"] += e["dr_mgo"] ? (isNaN(("" + e["dr_mgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_mgo"]).replaceAll(",", "") * 1) : 0;
          deprev["vlsfo"] += e["dr_vlsfo"] ? (isNaN(("" + e["dr_vlsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_vlsfo"]).replaceAll(",", "") * 1) : 0;
          deprev["lsmgo"] += e["dr_lsmgo"] ? (isNaN(("" + e["dr_lsmgo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_lsmgo"]).replaceAll(",", "") * 1) : 0;
          deprev["ulsfo"] += e["dr_ulsfo"] ? (isNaN(("" + e["dr_ulsfo"]).replaceAll(",", "") * 1) ? 0 : ("" + e["dr_ulsfo"]).replaceAll(",", "") * 1) : 0;
        }
      });
    }

    if (formData && formData.hasOwnProperty(".")) {
      let cpData = formData["."];
      let price_cp_eco = formData.hasOwnProperty("price_cp_eco") && formData["price_cp_eco"] ? 1 : 0; // 1 is P$ else CP$
      cpData.map((e) => {
        let _price = 0;
        let _pprice = 0;
        if (e["cp_price"] && !isNaN(("" + e["cp_price"]).replaceAll(",", "") * 1)) {
          _price = ("" + e["cp_price"]).replaceAll(",", "") * 1;
        }
        if (e["purchase_price"] && !isNaN(("" + e["purchase_price"]).replaceAll(",", "") * 1)) {
          _pprice = ("" + e["purchase_price"]).replaceAll(",", "") * 1;
        }

        switch (e.fuel_code) {
          case "IFO":
            seaExpenses["ifo"] = price_cp_eco == 1 ? seaExpenses["ifo"] * _pprice : seaExpenses["ifo"] * _price;
            portExpenses["ifo"] = price_cp_eco == 1 ? portExpenses["ifo"] * _pprice : portExpenses["ifo"] * _price;
            arrivalrev["ifo"] = arrivalrev["ifo"] * _price;
            deprev["ifo"] = deprev["ifo"] * _pprice;
            arrivaladjuestmentrev["ifo"] = deprev["ifo"] - arrivalrev["ifo"];
            break;

          case "MGO":
            seaExpenses["mgo"] = price_cp_eco == 1 ? seaExpenses["mgo"] * _pprice : seaExpenses["mgo"] * _price;
            portExpenses["mgo"] = price_cp_eco == 1 ? portExpenses["mgo"] * _pprice : portExpenses["mgo"] * _price;
            arrivalrev["mgo"] = arrivalrev["mgo"] * _price;
            deprev["mgo"] = deprev["mgo"] * _pprice;
            arrivaladjuestmentrev["mgo"] = deprev["mgo"] - arrivalrev["mgo"];
            break;

          case "VLSFO":
            seaExpenses["vlsfo"] = price_cp_eco == 1 ? seaExpenses["vlsfo"] * _pprice : seaExpenses["vlsfo"] * _price;
            portExpenses["vlsfo"] = price_cp_eco == 1 ? portExpenses["vlsfo"] * _pprice : portExpenses["vlsfo"] * _price;
            arrivalrev["vlsfo"] = arrivalrev["vlsfo"] * _price;
            deprev["vlsfo"] = deprev["vlsfo"] * _pprice;
            arrivaladjuestmentrev["vlsfo"] = deprev["vlsfo"] - arrivalrev["vlsfo"];
            break;

          case "LSMGO":
            seaExpenses["lsmgo"] = price_cp_eco == 1 ? seaExpenses["lsmgo"] * _pprice : seaExpenses["lsmgo"] * _price;
            portExpenses["lsmgo"] = price_cp_eco == 1 ? portExpenses["lsmgo"] * _pprice : portExpenses["lsmgo"] * _price;
            arrivalrev["lsmgo"] = arrivalrev["lsmgo"] * _price;
            deprev["lsmgo"] = deprev["lsmgo"] * _pprice;
            arrivaladjuestmentrev["lsmgo"] = deprev["lsmgo"] - arrivalrev["lsmgo"];
            break;

          case "ULSFO":
            seaExpenses["ulsfo"] = price_cp_eco == 1 ? (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _pprice : (isNaN(seaExpenses["ulsfo"]) ? 0 : seaExpenses["ulsfo"]) * _price;
            portExpenses["ulsfo"] = price_cp_eco == 1 ? (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price : (isNaN(portExpenses["ulsfo"]) ? 0 : portExpenses["ulsfo"]) * _price;
            arrivalrev["ulsfo"] = (isNaN(arrivalrev["ulsfo"]) ? 0 : arrivalrev["ulsfo"]) * _price;
            deprev["ulsfo"] = (isNaN(deprev["ulsfo"]) ? 0 : deprev["ulsfo"]) * _pprice;
            arrivaladjuestmentrev["ulsfo"] = deprev["ulsfo"] - arrivalrev["ulsfo"];
            break;
        }
      });
    }

    Object.keys(seaExpenses).map(
      (e) => (totalSeaConsumption += seaExpenses[e])
    );
    Object.keys(portExpenses).map(
      (e) => (totalPortConsumption += portExpenses[e])
    );
    Object.keys(arrivalrev).map(
      (e) => (totalArriveConsumption += arrivalrev[e])
    );
    Object.keys(deprev).map((e) => (totalDepConsumption += deprev[e]));
    Object.keys(arrivaladjuestmentrev).map(
      (e) => (totalAdjConsumption += arrivaladjuestmentrev[e])
    );

    totalVoyageDays = tpd + tsd > 0 ? tpd + tsd : totalVoyageDays;

    if (formData && formData.hasOwnProperty("portitinerary")) {
      let portitinerary = formData["portitinerary"];
      portitinerary.map((e) => (pi += isNaN(("" + e.p_exp).replaceAll(",", "") * 1) ? 0 : ("" + e.p_exp).replaceAll(",", "") * 1));
    }

    let rev_frt = 0,
      rev_ttl_com = 0,
      rev_extra_rev = 0,
      rev_demdes = 0;
    let exp_frt = 0,
      exp_ttl_com = 0,
      exp_extra_rev = 0,
      exp_demdes = 0;

    let avgfreightRate = 0,
      avgfreightRateOpt = 0,
      frtAvg = 0,
      cpQtyOpt = 0;
    if (formData && formData.hasOwnProperty("cargos")) {
      let cargos = formData["cargos"];
      cargos.map((e, i) => {
        let frtRate = 0,
          commission = 0,
          frtAmount = 0;


        let frt_rate = e.freight_rate || e.frat_rate || e.f_rate || 0;
        let ftype = e.f_type || e.f_type || e.f_type || 0;
        let lumpsumval = e.lumpsum || e.lumpsum || e.lumpsum || 0;
        let demmurage = e.dem_rate_pd || e.dem_rate_pd || e.dem_rate_pd || 0;
        let desmurage = e.des_rate_pd || e.des_rate_pd || e.des_rate_pd || 0;



        let cp_qty = e.cp_qty || e.quantity || 0;
        let b_commission = e.b_commission || e.commission || 0;

        let opt_per = parseFloat(e.option_percentage) || parseFloat(e.opt_percentage) || 0;
        mr = e.extra_rev ? (e.extra_rev + "").replaceAll(",", "") * 1 : 0;
        cpQty += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
        cpQtyOpt += cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;

        if (opt_per !== 0) {
          let _cpq = cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0;
          let _fr = frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0;
          opt_per = opt_per ? (opt_per + "").replaceAll(",", "") : 0;
          let cpOptQty = _cpq + (_cpq * opt_per) / 100;
          cpQtyOpt = cpQtyOpt - _cpq + cpOptQty; //way to use opt% of cargo qty : refactor
          frtAmount = cpOptQty * _fr;
          if(ftype==104){
            frtAmount = lumpsumval ? (lumpsumval + "").replaceAll(",", "") * 1 : 0; 
          }
          commission = (frtAmount * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          frtRate = (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) * (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
        } else {
          
          frtAmount = (cp_qty ? (cp_qty + "").replaceAll(",", "") * 1 : 0) * (frt_rate ? (frt_rate + "").replaceAll(",", "") * 1 : 0);
          if(ftype==104){
            frtAmount = lumpsumval ? (lumpsumval + "").replaceAll(",", "") * 1 : 0; 
          }
          commission = (frtAmount * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          frtRate = frtAmount;
        }

        if (e.sp_type == 186) {
          rev_frt += frtAmount;
          rev_ttl_com += commission;
          rev_extra_rev += mr;
          
          demmurage_amt_sales += demmurage * 1;
          demcommissionamt_sales += (demmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          
          desmurage_amt_sales += desmurage * 1;
          descommissionamt_sales += (desmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          //rev_demdes += e.dem_rate_pd * 1;
        } else if (e.sp_type == 187) {
          exp_frt += frtAmount;
          exp_ttl_com += commission;
          exp_extra_rev += mr;

          demmurage_amt_purch += demmurage * 1;
          demcommissionamt_purch += (demmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          
          desmurage_amt_purch += desmurage * 1;
          descommissionamt_purch += (desmurage * (b_commission ? (b_commission + "").replaceAll(",", "") * 1 : 0)) / 100;
          
          //exp_demdes += e.dem_rate_pd * 1;
        }
        

        


      });
      avgfreightRateOpt = fr / cpQtyOpt;
      avgfreightRate = frtAvg / cpQty;
    }

    let rev_gross_rev = rev_frt + rev_extra_rev + (demmurage_amt_sales - desmurage_amt_sales);
    rev_gross_rev = rev_gross_rev.toFixed(2);

    let rev_net_rev = (rev_gross_rev - rev_ttl_com) - (demcommissionamt_sales + descommissionamt_sales);
    rev_net_rev = rev_net_rev.toFixed(2);

    let exp_gross_expense = (exp_frt + exp_demdes + mis_cost - exp_extra_rev) - (demmurage_amt_purch + desmurage_amt_purch);
    exp_gross_expense = exp_gross_expense.toFixed(2);


    let exp_net_expense = (exp_gross_expense - exp_ttl_com) + ( demcommissionamt_purch - descommissionamt_purch);
    exp_net_expense = exp_net_expense.toFixed(2);

    totalVoyageDays = totalVoyageDays.toFixed(2);

    if (formData.hasOwnProperty("estimatePL")) {
      estimateData = this.__getEstimatePL(this.state.formData["estimate"]);
    }

    estimateData[0][colName] = dollarUSLocale.format(rev_net_rev);
    estimateData[0]["children"][0][colName] = dollarUSLocale.format(
      rev_frt.toFixed(2)
    );
    estimateData[0]["children"][1][colName] = dollarUSLocale.format(
      rev_ttl_com.toFixed(2)
    );
    estimateData[0]["children"][2][colName] = dollarUSLocale.format(
      rev_extra_rev.toFixed(2)
    );



    estimateData[0]["children"][3][colName] = dollarUSLocale.format(
      demmurage_amt_sales.toFixed(2)
    );

    estimateData[0]["children"][4][colName] = dollarUSLocale.format(
      demcommissionamt_sales.toFixed(2)
    );

    estimateData[0]["children"][5][colName] = dollarUSLocale.format(
      desmurage_amt_sales.toFixed(2)
    );

    estimateData[0]["children"][6][colName] = dollarUSLocale.format(
      descommissionamt_sales.toFixed(2)
    );
    estimateData[0]["children"][7][colName] = dollarUSLocale.format(
      rev_gross_rev
    );
    estimateData[0]["children"][8][colName] = dollarUSLocale.format(
      rev_net_rev
    );

    estimateData[1][colName] = dollarUSLocale.format(exp_net_expense);
    estimateData[1]["children"][0][colName] = dollarUSLocale.format(
      exp_frt.toFixed(2)
    );
    estimateData[1]["children"][1][colName] = dollarUSLocale.format(
      exp_ttl_com.toFixed(2)
    );
    estimateData[1]["children"][2][colName] = dollarUSLocale.format(
      exp_extra_rev.toFixed(2)
    );

    estimateData[1]["children"][3][colName] = dollarUSLocale.format(
      demmurage_amt_purch.toFixed(2)
    );

    estimateData[1]["children"][4][colName] = dollarUSLocale.format(
      demcommissionamt_purch.toFixed(2)
    );

    estimateData[1]["children"][5][colName] = dollarUSLocale.format(
      desmurage_amt_purch.toFixed(2)
    );

    estimateData[1]["children"][6][colName] = dollarUSLocale.format(
      descommissionamt_purch.toFixed(2)
    );

    

    
    estimateData[1]["children"][7][colName] = dollarUSLocale.format(mis_cost);
    estimateData[1]["children"][8][colName] = dollarUSLocale.format(
      exp_gross_expense
    );
    estimateData[1]["children"][9][colName] = dollarUSLocale.format(
      exp_net_expense
    );

    let itemValue = rev_net_rev - exp_net_expense;

    estimateData[2][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2))
        : dollarUSLocale.format((itemValue * 1).toFixed(2));
    estimateData[2]["children"][0][colName] =
      itemValue >= 0
        ? dollarUSLocale.format(itemValue.toFixed(2))
        : dollarUSLocale.format((itemValue * 1).toFixed(2));
    estimateData[2]["children"][1][colName] = dollarUSLocale.format(
      itemValue / totalVoyageDays
    );
    estimateData[2]["children"][2][colName] = dollarUSLocale.format(
      tsd.toFixed(2)
    );
    estimateData[2]["children"][3][colName] = dollarUSLocale.format(
      tpd.toFixed(2)
    );
    estimateData[2]["children"][4][colName] = dollarUSLocale.format(
      totalVoyageDays
    );

    return estimateData;
  };

  componentDidMount() {
    // console.log('call count');
    this.setState(
      {
        ...this.state,
        showPL: false,
        estimateData: this.__pl(),
      },
      () => this.setState({ ...this.state, showPL: true })
    );
  }

  render() {
    const { estimateData, showPL, viewTabs } = this.state;

    return (
      <>
        <div>
          <ToolbarUI routeUrl={"pl-main"} callback={(e) => this.callback(e)} />
        </div>

        {showPL === true ? (
          <Tabs defaultActiveKey="1">
            {viewTabs.map((e) => {
              if (e === "Actual &  Operation View") {
                return (
                  <TabPane tab={e} key="ao2">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Estimate View") {
                return (
                  <TabPane tab={e} key="ev1">
                    <Table
                      className="pl-summary-list-view"
                      bordered
                      columns={columns2}
                      dataSource={estimateData}
                      pagination={false}
                      rowClassName={(r, i) =>
                        i % 2 === 0
                          ? "table-striped-listing"
                          : "dull-color table-striped-listing"
                      }
                    />
                    <Row gutter={16} className="m-t-18">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <FormItem
                          label="Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea
                            placeholder="Remark"
                            autoSize={{ minRows: 6, maxRows: 6 }}
                          />
                        </FormItem>
                      </Col>
                    </Row>
                  </TabPane>
                );
              } else if (e === "Account View") {
                return (
                  <TabPane tab={e} key="av3">
                    Accounts
                  </TabPane>
                );
              }
            })}
          </Tabs>
        ) : (
          <div className="col col-lg-12">
            <Spin tip="Loading...">
              <Alert message=" " description="Please wait..." type="info" />
            </Spin>
          </div>
        )}
      </>
    );
  }
}

export default PL;
