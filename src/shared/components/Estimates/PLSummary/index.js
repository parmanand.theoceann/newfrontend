import React, { Component } from 'react';
import { Form, Input, Select, Button, Row, Col } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

function onCommencing(value) {
    // console.log(`selected ${value}`);
}

class PLSummary extends Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-wrapper">
                            <div className="form-heading">
                                <h4 className="title"><span>Estimate Summary</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="TCO Hire Rate">
                            <Input size="default" placeholder="0" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Hire Curr/X Rate">
                            <InputGroup compact>
                                <Input style={{ width: '20%' }} defaultValue="USD" />
                                <Input style={{ width: '80%' }} defaultValue="1.00" />
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Gross Income">
                            <Input size="default" placeholder="0" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Ballast Bonus">
                            <Input size="default" placeholder="0" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="(-)Adds + Brok Com">
                            <Input size="default" placeholder="0" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Misc Revenues">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Net Income">
                            <Input size="default" placeholder="1" />
                        </FormItem>
                    </div>

                </div>

                <div className="row">
                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Vessel Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Ballast Bonus">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="(-)Adds + Brok Com">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Bunker Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Port/Canal Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Misc Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="LessRebills">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Total Pos Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Total RePos Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Total Expenses">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Profit">
                            <Input size="default" placeholder="1" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Daily Profit">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Breakeven Rate">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="TCE (USD/d)">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                </div>

                <hr />

                <div className="row">
                    <div className="col-md-12">
                        <div className="form-wrapper">
                            <div className="form-heading">
                                <h4 className="title"><span>Voyage Days</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="TCO Duration">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Commencing">
                            <InputGroup compact>
                                <Select defaultValue="Option One" onChange={onCommencing}>
                                    <Option value="Option One">Option One</Option>
                                    <Option value="Option Two">Option Two</Option>
                                </Select>
                            </InputGroup>
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Completing">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Voyage Days">
                            <Input size="default" placeholder="0.00" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Remarks">
                            <TextArea placeholder="" autoSize={{ minRows: 1, maxRows: 3 }} />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Last Updated By">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                    <div className="col-md-12">
                        <FormItem
                            {...formItemLayout}
                            label="Last Updated GMT">
                            <Input size="default" placeholder="" />
                        </FormItem>
                    </div>

                </div>

            </>
        )
    }
}

export default PLSummary;