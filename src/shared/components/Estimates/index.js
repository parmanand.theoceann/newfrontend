import React, { Component } from 'react';
import { Tabs } from 'antd';
import PLSummary from './PLSummary';
import EstimateSummary from './EstimateSummary';

const TabPane = Tabs.TabPane;

class Estimates extends Component {
    render() {
        return (
            <article className="article">
                <div className="box box-default">
                    <div className="box-body">
                        <Tabs defaultActiveKey="plsummary" size="small">
                            <TabPane tab="PL Summary" key="plsummary"><PLSummary /></TabPane>
                            <TabPane tab="Estimates Summary" key="estimatessummary"><EstimateSummary /></TabPane>
                        </Tabs>
                    </div>
                </div>
            </article>
        )
    }
}

export default Estimates;