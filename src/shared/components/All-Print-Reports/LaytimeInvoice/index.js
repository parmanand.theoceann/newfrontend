import React, { Component } from 'react';
import ReactToPrint from "react-to-print";
import { PrinterOutlined } from '@ant-design/icons';
import {  Table } from 'antd';
import '../print-report.scss';

const paymentcolumns = [
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: 'Amount (USD)',
    dataIndex: 'amount',
    key: 'amount',
  },
];

const paymentdata = [
  {
    key: "1",
    description: "Dispatch for all port combined (SANTOS)",
    amount: "(2233333)",
  },
];

class ComponentToPrint extends React.Component {
  render() {
    return (
      <article className="article">
        <div className="box box-default print-wrapper">
          <div className="box-body">

            <div className="invoice-wrapper">
              <section className="invoice-container" id="invoice">

                <div className="invoice-inner">
                  <div className="row">
                    <div className="col-12">
                      <span className="title">LDC</span>
                      <p className="sub-title">Louis Dreyfus Company</p>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <p className="">LOUIS DREYFUS COMPANY FREIGHT ASIA PTE LTD 12 MARINA BAY BOULEWARD MARINA BAY FINANCIAL CENTER TOWER 3#33-03</p>
                    </div>
                  </div>

                  <hr />

                  <div className="row view-list">
                    <div className="col-6">
                      <div className="">
                        <div><b>Due To : </b></div>
                        <div>LOUIS DREYFUS COMPANY SUISES</div>
                        <div>SA</div>
                        <div>ASIA PTE LTD 12</div>
                        <div>PO BOX 123</div>
                        <div>PO BOX 123</div>
                        <div>CH-444</div>
                        <div>INDIA</div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="text-right">
                        <div className="sub-heading">Credit Memo</div>
                        <div> <b>Invoice# Invoice Date : MAR 24, 2020</b></div>
                        <div><b>Due date : MAR 24, 2020</b></div>
                        <div className="row bg-colors-box">
                          <div className="col-6">
                            <ul className="list-unstyled">
                              <li><strong>Vessel:</strong> GOLDEN ROSE</li>
                              <li><strong>Voyage:</strong> #1</li>
                              <li><strong>IMO No. :</strong> 20190483</li>
                            </ul>
                          </div>

                          <div className="col-6">
                            <ul className="list-unstyled">
                              <li><strong>Loading ports :</strong> Santos</li>
                              <li><strong>Discharging Ports :</strong> BAYCON</li>
                              <li><strong>B/L Date :</strong> 12/23/2020</li>
                              <li><strong>CP Date :</strong> 12/23/2020</li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="divider my-3" />

                  <div className="row">
                    <div className="col-md-12 custum-table">
                      <Table
                        className="invoice-table laytimereport"
                        columns={paymentcolumns}
                        dataSource={paymentdata}
                        pagination={false}
                      />
                    </div>

                    <div className="col-12 invoice-sum text-right">
                      <ul className="list-unstyled">
                        <li><strong>Subtotal:</strong> 23,400</li>
                        <li><strong>Total:</strong> 12,93883</li>
                      </ul>
                    </div>
                  </div>

                  <div className="divider my-3" />

                  <div className="row">
                    <div className="col-12">
                      <div className="sub-heading">Payment Terms:</div>
                      <hr />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-6">
                      <div><b>Banking Details :</b></div>
                      <div>CITABANK NA LANDON</div>
                      <div>Swift Code : 347363635</div>
                      <div>Account No. : 347363635</div>
                      <div>2 MARINA BAY BOULEWARD MARINA BAY FINANCIAL</div>
                    </div>
                    <div className="col-6">
                      <div><b>Remit To :</b></div>
                      <div>LOUIS DREYFUS COMPANY SUISES</div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <div><b>Memo :</b></div>
                      <div>LOUIS DREYFUS COMPANY FREIGHT ASIA PTE LTD</div>
                    </div>
                  </div>

                </div>
              </section>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class LaytimeInvoice extends Component {

  constructor() {
    super();
    this.state = {
      name: 'Printer'
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    return (
      <div className="body-wrapper modalWrapper">

        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                    </ul>
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li><ReactToPrint
                        trigger={() => <span className="text-bt"><PrinterOutlined /> Print</span>}
                        content={() => this.componentRef}
                      /></li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={(el) => (this.componentRef = el)} />
            </div>
          </div>
        </article>

      </div>
    )
  }
}

export default LaytimeInvoice;