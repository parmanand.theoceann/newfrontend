import React, { Component } from 'react';
import ReactToPrint from "react-to-print";

import { Icon, Table } from 'antd';
import '../print-report.scss';

const invoicecolumns = [
    {
        title: 'Comp',
        dataIndex: 'comp',
        key: 'comp',
    },
    {
        title: 'LOB',
        dataIndex: 'lob',
        key: 'lob',
    },
    {
        title: 'Vessel',
        dataIndex: 'vessel',
        key: 'vessel',
    },
    {
        title: 'Voyage',
        dataIndex: 'voyage',
        key: 'voyage',
    },
    {
        title: 'Voyage Ref',
        dataIndex: 'voyageref',
        key: 'voyageref',
    },
    {
        title: 'Dept',
        dataIndex: 'dept',
        key: 'dept',
    },
    {
        title: 'Port',
        dataIndex: 'port',
        key: 'port',
    },
    {
        title: 'A/C',
        dataIndex: 'acc',
        key: 'acc',
    },
    {
        title: 'A/C Name',
        dataIndex: 'accname',
        key: 'accname',
    },
    {
        title: 'Credit Acct.',
        dataIndex: 'creditacct',
        key: 'creditacct',
    },
    {
        title: 'Credit Acct. Name',
        dataIndex: 'creditacctname',
        key: 'creditacctname',
    },
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
    },
];

const invoicedata = [
    {
        key: "1",
        comp: "",
        lob: "DRYB",
        vessel: "MAAN",
        voyage: "1",
        voyageref: "",
        dept: "",
        port: "",
        acc: "600504",
        accname: "OTHER VOYAGE COSTS",
        creditacct: "",
        creditacctname: "SSY, Houston",
        description: "MAAN-1-ROUTING",
        amount: "1,200.00",
    },
];

const paymentcolumns = [
    {
        title: 'Bank Code',
        dataIndex: 'bankcode',
        key: 'bankcode',
    },
    {
        title: 'Approval',
        dataIndex: 'approval',
        key: 'approval',
    },
    {
        title: 'Date Paid',
        dataIndex: 'datepaid',
        key: 'datepaid',
    },
    {
        title: 'By',
        dataIndex: 'by',
        key: 'by',
    },
    {
        title: 'Check/WT No.',
        dataIndex: 'checkwtno',
        key: 'checkwtno',
    },
    {
        title: 'Amount Paid',
        dataIndex: 'amountpaid',
        key: 'amountpaid',
    },
    {
        title: 'Base Equiv Pay Trans No.',
        dataIndex: 'baseequivpaytransno',
        key: 'baseequivpaytransno',
    },
];

const paymentdata = [
    {
        key: "1",
        bankcode: "",
        approval: "",
        datepaid: "",
        by: "",
        checkwtno: "",
        amountpaid: "0.00",
        baseequivpaytransno: "0.00",
    },
];

class ComponentToPrint extends React.Component {
    render() {
        return (
            <article className="article">
                <div className="box box-default print-wrapper">
                    <div className="box-body">

                        <div className="invoice-wrapper">
                            <section className="invoice-container" id="invoice">

                                <div className="invoice-inner">
                                    <div className="row">
                                        <div className="col-12">
                                            <p className="main-heading">INVOICE DATA ENTRY RECORD</p>
                                        </div>
                                    </div>

                                    <hr />

                                    <div className="row view-list">
                                        <div className="col-12">
                                            <div className="sub-heading">Trans No.:</div>
                                        </div>

                                        <div className="col-3 text-left">
                                            <ul className="list-unstyled">
                                                <li>
                                                    <strong>Invoice Type:</strong> 1 Payable
                                                </li>

                                                <li className="null-value">
                                                    <strong>Vendor:</strong> N/A
                                                </li>

                                                <li className="null-value">
                                                    <strong>Invoice Date:</strong> N/A
                                                </li>

                                                <li className="null-value">
                                                    <strong>Invoice No.:</strong> N/A
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="col-3 text-left">
                                            <ul className="list-unstyled">
                                                <li className="null-value">
                                                    <strong>P.O. No.:</strong> N/A
                                                </li>

                                                <li>
                                                    <strong>Text:</strong> ROUTING
                                                </li>

                                                <li className="null-value">
                                                    <strong>Approval:</strong> N/A
                                                </li>

                                                <li>
                                                    <strong>Opr Last User:</strong> Minesh
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="col-3 text-left">
                                            <ul className="list-unstyled">
                                                <li className="null-value">
                                                    <strong>Status:</strong> N/A
                                                </li>

                                                <li>
                                                    <strong>Amount:</strong> 1,200.00
                                                </li>

                                                <li>
                                                    <strong>Currency:</strong> USD
                                                </li>

                                                <li>
                                                    <strong>Base Equiv:</strong> 1,200.00
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="col-3 text-left">
                                            <ul className="list-unstyled">
                                                <li className="null-value">
                                                    <strong>AP/AR No.:</strong> N/A
                                                </li>

                                                <li>
                                                    <strong>Act Date:</strong> 28-04-2021
                                                </li>

                                                <li className="null-value">
                                                    <strong>Due Date:</strong> N/A
                                                </li>

                                                <li className="null-value">
                                                    <strong>Act Last User:</strong> N/A
                                                </li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div className="divider my-2" />

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="sub-heading">Invoice Details:</div>
                                        </div>
                                        <div className="col-md-12">
                                            <Table
                                                className="invoice-table"
                                                columns={invoicecolumns}
                                                dataSource={invoicedata}
                                                pagination={false}
                                            />
                                        </div>
                                        <div className="col-12 invoice-sum text-right">
                                            <ul className="list-unstyled">
                                                <li><strong>Total: 1,200.00</strong></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="sub-heading">Payment Details:</div>
                                        </div>
                                        <div className="col-md-12">
                                            <Table
                                                className="invoice-table"
                                                columns={paymentcolumns}
                                                dataSource={paymentdata}
                                                pagination={false}
                                            />
                                        </div>
                                        <div className="col-12 invoice-sum text-right">
                                            <ul className="list-unstyled">
                                                <li><strong>Total: 0.00</strong></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div className="row signature">
                                        <div className="col-md-6 left">
                                            <p><strong>Approved By</strong></p>
                                        </div>
                                        <div className="col-md-6 right">
                                            <p><strong>Checked By</strong></p>
                                        </div>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </article>
        );
    }
}

class ReportingCenter extends Component {

    constructor() {
        super();
        this.state = {
            name: 'Printer'
        };
    }

    printReceipt() {
        window.print();
    }

    render() {
        return (
            <div className="body-wrapper modalWrapper">

                <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="toolbar-ui-wrapper">
                                <div className="leftsection">
                                    <span key="first" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><ReactToPrint
                                                trigger={() => <span className="text-bt"><PrinterOutlined /> Print</span>}
                                                content={() => this.componentRef}
                                            /></li>
                                            <li><span className="text-bt"><Icon type="schedule" /> Preview</span></li>
                                            <li><span className="text-bt"><SaveOutlined /> Save</span></li>
                                        </ul>
                                    </span>
                                </div>
                                <div className="rightsection">
                                    <span className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt"><Icon type="file-pdf" /> Create PDF</span></li>
                                            <li><span className="text-bt"><Icon type="file-excel" /> Create Excel</span></li>
                                            <li><span className="text-bt"><MailOutlined /> Email</span></li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <ComponentToPrint ref={(el) => (this.componentRef = el)} />
                        </div>
                    </div>
                </article>

            </div>
        )
    }
}

export default ReportingCenter;