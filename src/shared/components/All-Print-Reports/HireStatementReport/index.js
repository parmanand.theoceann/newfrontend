import React, { Component } from "react";
import { PrinterOutlined } from '@ant-design/icons';
import { Button, Tooltip } from "antd";
import ReactToPrint from "react-to-print";
import jsPDF from "jspdf";
import * as htmlToImage from "html-to-image";
import "../print-report.scss";
import moment from "moment";

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      invoiceData: this.props.invoiceData || {},
    };
  }

  render() {
    const { invoiceData } = this.state;


    return (
      <article className="article">
        <div className="box box-default print-wrapper" id="divToPrint">
          <div className="box-body">
            <div className="invoice-wrapper">
              <section className="invoice-container hire" id="invoice">
                <div className="invoice-inner">


                  <div className="invoice-inner-download mt-3">
                    <div className="row">
                      <div className="col-12 text-center">
                        {invoiceData && invoiceData["my_company_logo"] ? (
                          <img
                            className="reportlogo"
                            src={invoiceData["my_company_logo"]}
                            crossOrigin="anonymous"
                            alt="no Img"
                          />
                        ) : (
                          undefined
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-10 mx-auto">
                      <div className="text-center invoice-top-address">
                        <p>
                          {invoiceData && invoiceData["my_company_name"]
                            ? invoiceData["my_company_name"]
                            : ""}
                        </p>
                        <p>
                          {" "}
                          {invoiceData && invoiceData["my_company_address"]
                            ? invoiceData["my_company_address"]
                            : ""}
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="row view-list">
                    <div className="col-12">
                      <div className="sub-heading text-right">
                        Hire Statement
                      </div>
                    </div>

                    <div className="col-12">
                      <div className="wrap-data">
                        <div className="col-4 text-left">
                          <ul className="list-unstyled">
                            <li className="laytime-list hire-report hire-width">
                              <span>Vessel:</span>{" "}
                              <span>{invoiceData["vessel_name"]}</span>
                            </li>
                            <li className="laytime-list hire-report hire-width">
                              <span>Delivery:</span>
                              <span>
                                {invoiceData["delivery_date"] === ""
                                  ? ""
                                  : invoiceData["delivery_date"]}
                              </span>
                            </li>
                          </ul>
                        </div>

                        <div className="col-4 text-left">
                          <ul className="list-unstyled">
                            <li className="laytime-list hire-report hire-width1">
                              <span>CP Date:</span>
                              <span>
                                {!invoiceData["cp_date"]
                                  ? ""
                                  : invoiceData["cp_date"]}
                              </span>
                            </li>
                            <li className="laytime-list hire-report hire-width1">
                              <span>Redelivery:</span>
                              <span>
                                {invoiceData["redelivery_date"] === ""
                                  ? ""
                                  : invoiceData["redelivery_date"]}
                              </span>
                            </li>
                          </ul>
                        </div>

                        <div className="col-4 text-left">
                          <ul className="list-unstyled">
                            <li className="laytime-list">
                              <span>Charterered From:</span>
                              <span>{!invoiceData["chartrer_name"]
                                ? ""
                                : invoiceData["chartrer_name"]}.</span>
                            </li>
                            <li className="laytime-list">
                              <span>Printed On:</span>
                              <span> {invoiceData["printed"] ? moment(invoiceData["printed"]).format('YYYY-MM-DD HH:mm') : " "}</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="divider my-2" />

                  <div className="row laytime-heading">
                    {invoiceData["invoice"] && invoiceData["invoice"].length > 0
                      ? invoiceData["invoice"].map((e) => {
                        return (
                          <>
                            <div className="col-md-12">
                              <div className="table-responsive custom-table">
                                <table className="table">
                                  <thead>
                                    <tr>
                                      <th scope="col" rowSpan="3">
                                        {e.title}
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {e["description"].map((ed) => {
                                      return (
                                        <>
                                          <tr className="content">
                                            <th
                                              scope="row"
                                              className="no-padding"
                                            >
                                              {ed.details}
                                            </th>
                                            {
                                              <td className="text-right no-padding">
                                                &nbsp;
                                              </td>
                                            }

                                            {
                                              <td className="text-right no-padding">
                                                {ed["amount"] > 0
                                                  ? ed["amount"]
                                                  : ed["amount"] * -1}
                                              </td>
                                            }
                                          </tr>
                                        </>
                                      );
                                    })}
                                    <tr>
                                      <th scope="row">Net {e.title}</th>
                                      <td />
                                      <td
                                        className="text-right"
                                        style={{
                                          borderTop: "2px solid #000",
                                        }}
                                      >
                                        {e["net_amount"] < 0
                                          ? "(" + e["net_amount"] * -1 + ")"
                                          : e["net_amount"]}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div className="spacer" />
                          </>
                        );
                      })
                      : undefined}
                  </div>

                  <div className="divider my-2" />

                  <div className="row">
                    <div className="col-md-12 hire_stament_report_bank">
                      <div className="table-responsive custom-table">
                        <table className="table">
                          <thead>
                            <tr style={{ fontSize: "1.1rem" }}>
                              <th scope="col">Total Invoice Amount</th>
                              <td className="text-right" />
                              <td
                                className="text-right"
                                style={{ borderTop: "2px solid #000" }}
                              >
                                {invoiceData["total_amount"]}
                              </td>
                            </tr>
                          </thead>
                          {invoiceData["bank_ajent"] ? (
                            <tbody>
                              <tr style={{ borderTop: "1px solid #000" }}>
                                <td colSpan="3">
                                  <p style={{ marginBottom: "0px" }}>
                                    <b>Benificiary Name:</b>{" "}
                                    {
                                      invoiceData["bank_ajent"][
                                      "benificiary_name"
                                      ]
                                    }
                                  </p>
                                  <p style={{ marginBottom: "0px" }}>
                                    <b>Bank Name:</b>{" "}
                                    {
                                      invoiceData["bank_ajent"][
                                      "benificiary_bank_name"
                                      ]
                                    }
                                  </p>
                                  <p style={{ marginBottom: "0px" }}>
                                    <b>Account No:</b>{" "}
                                    {invoiceData["bank_ajent"]["account_no"]}
                                  </p>
                                  <p style={{ marginBottom: "0px" }}>
                                    <b>Branch:</b>{" "}
                                    {invoiceData["bank_ajent"]["branch"]}
                                  </p>

                                  <p style={{ marginBottom: "0px" }}>
                                    <b>IBAN:</b>{" "}
                                    {invoiceData["bank_ajent"]["iban"]}
                                  </p>

                                  <p style={{ marginBottom: "0px" }}>
                                    <b>Swift Code:</b>{" "}
                                    {invoiceData["bank_ajent"]["swift_code"]}
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          ) : (
                            undefined
                          )}
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </article>
    );
  }
}

class HireStatementReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Printer",
      invoiceData: this.props.invData || {},
    };


  }

  printReceipt() {
    window.print();
  }
  printDocument() {
    htmlToImage
      .toPng(document.getElementById("divToPrint"), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement("a");
        link.download = "my-image-name.jpeg";
        const pdf = new jsPDF();

        const imgProps = pdf.getImageProperties(dataUrl);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = pdf.internal.pageSize.getHeight();
        pdf.addImage(dataUrl, "PNG", 0, 0, pdfWidth, pdfHeight);
        pdf.save("HireStatement.pdf");
      });
  }

  render() {
    const { invoiceData } = this.state;
    return (
      <div className="body-wrapper modalWrapper modal-report-wrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                {/* <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <Tooltip title="Back">
                          <Button
                            type="dashed"
                            shape="circle"
                            icon="double-left"
                            size={'default'}
                            onClick={() => this.props.changeState(true)}
                          />
                        </Tooltip>
                      </li>
                      <li>
                        <span className="text-bt">Hire Statement</span>
                      </li>
                    </ul>
                  </span>
                </div> */}
                <div className="rightsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <span className="text-bt" onClick={this.printDocument}>
                          {" "}
                          Download
                        </span>
                      </li>
                      <li>
                        <ReactToPrint
                          documentTitle="TC-Hire Statement-Invoiced"
                          trigger={() => (
                            <span className="text-bt">
                              <PrinterOutlined />Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body print-scroll">
              <ComponentToPrint
                invoiceData={invoiceData}
                ref={(el) => (this.componentRef = el)}
              />
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default HireStatementReport;
