import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import moment from 'moment';

import { PrinterOutlined } from '@ant-design/icons';
import '../print-report.scss';
import URL_WITH_VERSION, {
  getAPICall,
  //awaitPostAPICall,
  //openNotificationWithIcon,
  //objectToQueryStringFunc,
  //postAPICall,
  //apiDeleteCall,
} from '../../../../shared';

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false,
      name: 'Printer',
      responseData: this.props.responseData,
      vesselID: this.props.responseData.vessel,
      vesselDetails: {},
    };
  }


  componentDidMount = async () => {
    this.setState({ ...this.state, loading: true }, () => this.getPortData());
  };


  getPortData = async () => {
   const {vesselID} = this.state
   const response = await getAPICall(`${URL_WITH_VERSION}/vessel/list/${vesselID}`);
   const data = await response['data'];
   this.setState({
      ...this.state,
      vesselDetails:data,
    }, () => this.setState({ ...this.state, loading: false }));
  }

  render() {
    const{responseData, vesselDetails, loading} = this.state
    const{agentdetails, cargodetails, instruction_set, portcalldetails, } = responseData
    return (
      <>
     {/* {loading === false && vesselDetails.hasOwnProperty('contacts') && vesselDetails.hasOwnProperty('capacityanddraft')?  */}
     {loading === false ? 

      <article className="article">
        <div className="box box-default print-wrapper">
          <div className="box-body">
            <div className="invoice-wrapper">
              <section className="invoice-container">
                <div className="invoice-inner">
                  <div className="row">
                    <div className="col-12">
                      <p className="main-heading inner-heading" >Appointment with PDA Request</p>
                    </div>
                  </div>

                 

                  <div>
                    <h5 className="headingh5">
                      Note : All Date & Time Will be consider as UTC Date & Time
                    </h5>
                  </div>
                  <div className="agencyprint">
                    <div className="equal-space">
                      <div className="mb-20 style">
                        <div>Appointment With PDA Request</div>
                        <div>Pro-Forma Disbursement Account Request</div>
                      </div>
                      <div className="style">
                        {/* <div>TO : Demo Shipping Mumbai</div> */}
                        <div>TO : {agentdetails.map((e, i)=>{
                          return (
                            <div key={i}>
                            {e.agent_name && e.agent_name ? e.agent_name : ""}
                            </div>
                          )
                        })}
                        </div>
                        <div>SUBJECT : Appointment with PDA request / {vesselDetails.vessel_name && vesselDetails.vessel_name ? vesselDetails.vessel_name : ""} / {portcalldetails.port && portcalldetails.port ? portcalldetails.port : ""}</div>
                        <div>Please provide us PDA for the subject call</div>
                      </div>
                    </div>
                    <div className="equal-space wrap-btn-icons">
                      <div>
                        {/* <img src="../../assets/images/export.svg" alt="export"/> */}
                      </div>
                      <div>Suman Singapore</div>
                      <div>Singapore</div>
                      <div>04748393382828</div>
                    </div>
                  </div>


                  <div>
                    <h4 className="inner-heading">Vessel Details</h4>
                  </div>

              

                  <div className="row view-list">
                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span><strong>Vessel Name:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.vessel_name && vesselDetails.vessel_name ? vesselDetails.vessel_name : ""}</span>
                        </li>

                        <li>
                           <span> <strong>Suez Id:</strong></span>
                        </li>

                        <li>
                           <span><strong>Call Sign:</strong></span> 
                           <span className="agencylistvalue">{vesselDetails && vesselDetails.vesselidentification && vesselDetails.vesselidentification.call_letters ? vesselDetails.vesselidentification.call_letters : "-"}</span> 
                        </li>

                        <li>
                           <span><strong>DWT:</strong> </span> 
                           <span className="agencylistvalue">{vesselDetails.vessel_dwt && vesselDetails.vessel_dwt ? vesselDetails.vessel_dwt : "" }</span> 
                        </li>

                        <li>
                           <span> <strong>Suez Canal GT:</strong> </span> 
                           <span className="agencylistvalue">{vesselDetails && vesselDetails.capacityanddraft && vesselDetails.capacityanddraft.suez_gross ? vesselDetails.capacityanddraft.suez_gross : "-"}</span>
                        </li>

                        <li>
                          <span> <strong>IMO No.:</strong></span> 
                          <span className="agencylistvalue">{vesselDetails.imo_no && vesselDetails.imo_no ? vesselDetails.imo_no : ""}</span> 
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span> <strong>NRT:</strong></span> 
                          <span className="agencylistvalue">{vesselDetails.capacityanddraft && vesselDetails.capacityanddraft ? vesselDetails.capacityanddraft.nrt_intl : ""}</span> 
                        </li>

                        <li>
                          <span> <strong>Telex:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.contacts && vesselDetails.contacts? vesselDetails.contacts.telex : ""}</span> 
                        </li>

                        <li>
                           <span><strong>Tropical Draft:</strong></span>
                          <span className="agencylistvalue">{vesselDetails && vesselDetails.capacityanddraft && vesselDetails.capacityanddraft.max_draft ? vesselDetails.capacityanddraft.max_draft : "-"}</span> 
                        </li>

                        <li>
                          <span><strong>Class:</strong> </span>
                          <span className="agencylistvalue">{vesselDetails.class_society && vesselDetails.class_society ? vesselDetails.class_society : ""}</span>
                        </li>

                        <li>
                           <span><strong>GRT:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.capacityanddraft && vesselDetails.capacityanddraft ? vesselDetails.capacityanddraft.grt_intl : ""}</span>
                        </li>

                        <li>
                           <span><strong>SATC:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.contacts && vesselDetails.contacts ? vesselDetails.contacts.sat_c : ""}</span> 
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span><strong>Fresh Draft:</strong></span>
                          <span className="agencylistvalue">{vesselDetails && vesselDetails.capacityanddraft && vesselDetails.capacityanddraft.winter_draft ? vesselDetails.capacityanddraft.winter_draft : "-"}</span> 
                        </li>

                        <li>
                          <strong>Vessel Type:</strong> {vesselDetails.vessel_type_name && vesselDetails.vessel_type_name ? vesselDetails.vessel_type_name : ""}
                        </li>

                        <li>
                          <span><strong>Suez Canal NT:</strong> </span>
                          <span className="agencylistvalue">{vesselDetails && vesselDetails.capacityanddraft && vesselDetails.capacityanddraft.net ? vesselDetails.capacityanddraft.net : "-"}</span> 
                        </li>

                        <li>
                           <span><strong>MMSI No.:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.mmsi && vesselDetails.mmsi ? vesselDetails.mmsi : ""}</span>
                        </li>

                        <li>
                           <span><strong>Summer Draft:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.sw_summer_draft && vesselDetails.sw_summer_draft ? vesselDetails.sw_summer_draft : ""}</span> 
                        </li>

                        <li>
                          <span> <strong>SATB Telefax:</strong></span>
                          <span className="agencylistvalue">{vesselDetails.contacts && vesselDetails.contacts ? vesselDetails.contacts.sat_b : ""}</span> 
                        </li>
                      </ul>
                    </div>
                  </div>


                  <div>
                    <h4 className="inner-heading">
                      PortCall Details (Please fill the Mendatory Port Details)
                    </h4>
                  </div>

               

                  <div className="row view-list">
                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span> <strong>Country:</strong></span>
                          <span className="agencylistvalue">{portcalldetails.country && portcalldetails.country ? portcalldetails.country : ""}</span> 
                        </li>

                        <li>
                          <span> <strong>ETD:</strong></span>
                          <span className="agencylistvalue">{ portcalldetails.etd && portcalldetails.etd ? moment(portcalldetails.etd).format('YYYY-MM-DD') : ""}</span> 
                        </li>

                        <li>
                           <span><strong>ETA:</strong></span>
                          <span className="agencylistvalue">{portcalldetails.eta && portcalldetails.eta ? moment(portcalldetails.eta).format('YYYY-MM-DD') : ""}</span> 
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span><strong>Port:</strong> </span>
                          <span className="agencylistvalue">{portcalldetails.port && portcalldetails.port ? portcalldetails.port : ""}</span> 
                        </li>

                        <li>
                           <span><strong>Next Port:</strong></span>
                          <span className="agencylistvalue">{portcalldetails.next_port && portcalldetails.next_port ? portcalldetails.next_port : ""}</span> 
                        </li>

                        <li>
                         
                          <span> <strong>Arr.Draft (m):</strong> </span>
                          <span className="agencylistvalue">

                          
                          {
                          portcalldetails.arr_draft_m && portcalldetails.arr_draft_m  ? portcalldetails.arr_draft_m  : ""
                          | 
                          portcalldetails.arr_draft_cm && portcalldetails.arr_draft_cm ? portcalldetails.arr_draft_cm : ""
                          | 
                          portcalldetails.arr_draft_sw && portcalldetails.arr_draft_sw ? portcalldetails.arr_draft_sw : ""}
                          </span>
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                           <span><strong>Voyage No.:</strong></span>
                          <span className="agencylistvalue">{portcalldetails.voyage_number && portcalldetails.voyage_number ? portcalldetails.voyage_number : ""}</span> 
                        </li>

                        <li>
                           <span><strong>Previous Port:</strong></span>
                          <span className="agencylistvalue">{portcalldetails.previous_port && portcalldetails.previous_port ? portcalldetails.previous_port : ""}</span> 
                        </li>
                      </ul>
                    </div>
                  </div>

            

                  <div>
                    <h4 className="inner-heading">
                      Corgo / Activity Details (Please fill the Mendatory Corgo Details)
                    </h4>
                  </div>

                

                  <div className="row view-list">
                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <span> <strong>Charterer's Name:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.charterer_name && cargodetails.charterer_name ? cargodetails.charterer_name : ""}</span>
                        </li>

                        <li>
                           <span><strong>Activity:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.activity && cargodetails.activity.map((e, idx)=>{
                              return(
                                <div key={idx}>
                                  {e} &nbsp;
                                </div>
                              )
                          })}
                          </span>
                        </li>

                        <li>
                           <span><strong>CP Place:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.cp_place && cargodetails.cp_place ? cargodetails.cp_place : ""}</span> 
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                           <span><strong>Quantity:</strong></span>
                          <span className="agencylistvalue">
                          Min. {cargodetails && cargodetails.quantity_min && cargodetails.quantity_min ? cargodetails.quantity_min : ""}  | Max.{cargodetails && cargodetails.quantity_max && cargodetails.quantity_max ? cargodetails.quantity_max : ""} 
                          </span>
                        </li>

                        <li>
                          <span> <strong>Name Of Corgo:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.cargo_name && cargodetails.cargo_name ? cargodetails && cargodetails.cargo_name : ""}</span> 
                        </li>

                        <li>
                           <span><strong>Laycan Start:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.laycan_start && cargodetails.laycan_start ? moment(cargodetails.laycan_start).format('YYYY-MM-DD') : ""}</span> 
                        </li>
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>
                           <span><strong>Unit:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.unit && cargodetails.unit ?  cargodetails.unit : ""}</span> 
                        </li>

                        <li>
                           <span><strong>CP Date:</strong></span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.cp_date && cargodetails.cp_date ? moment(cargodetails.cp_date).format('YYYY-MM-DD') : ""}</span>
                        </li>

                        <li>
                          <span><strong>Laycan End:</strong> </span>
                          <span className="agencylistvalue">{cargodetails && cargodetails.laycan_end && cargodetails.laycan_end ? moment(cargodetails.laycan_end).format('YYYY-MM-DD'): ""}</span>
                        </li>
                      </ul>
                    </div>
                  </div>

                  

                  <div>
                    <h4 className="inner-heading">
                      Agent Instruction (Please Check the Box for Agent Instruction)
                    </h4>
                  </div>

                

                  <div className="row view-list">
                    {/* <div className="col-1 text-left">
                      <ul className="list-unstyled">
                        <li>
                          <CheckOutlined />
                        </li>
                      </ul>
                    </div> */}

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>Sub-title</li>
                        {
                          instruction_set['-'] && instruction_set['-'] ? instruction_set['-'].map((e, idx)=>{
                            return(
                              <div key={idx}>
                                <li key={idx}>{e.sub_title}</li>
                              </div>
                            )
                          }) : undefined
                        }
                      </ul>
                    </div>
                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>Write up</li>
                        {
                          instruction_set['-'] && instruction_set['-'] ? instruction_set['-'].map((e, idx)=>{
                            return(
                              <div key={idx}>
                                <li key={idx}>{e.write_up}</li>
                              </div>
                            )
                          }):undefined
                        }
                      </ul>
                    </div>

                    <div className="col-4 text-left">
                      <ul className="list-unstyled">
                        <li>Attachment</li>
                        {
                          instruction_set['-'] && instruction_set['-'] ? instruction_set['-'].map((e, idx)=>{
                            return(
                              <div key={idx}>
                                <li key={idx}>{e.attachment}</li>
                              </div>
                            )
                          }): undefined
                        }
                      </ul>
                    </div>
                  </div>

                 
                  <div className="row view-list">
                    {/* <div className="col-6 text-left">
                      <ul className="list-unstyled">
                        <li>
                          Remark
                          <br />
                          <strong>
                            It is a long established fact that a reader will be distracted by the
                            readable content of a page when looking at its layout. The point of
                            using Lorem Ipsum is that it has a more-or-less normal distribution of
                            letters, as opposed to using 'Content here, content here', making it
                            look like readable English. Many desktop publishing packages and web
                            page editors now use Lorem Ipsum as their default model text, and a
                            search
                          </strong>
                        </li>
                      </ul>
                    </div> */}

                    <div className="col-12 text-right">
                      {/* <ul className="list-unstyled">
                        <li>
                          Signature
                          <br />
                          <strong>Suman Mumbai 03444333</strong>
                        </li>
                      </ul> */}
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </article> : ""

      }
       </>

    );
  }
}

class AgencyAppointmentReport extends Component {
  constructor(props) {

    super(props);
    this.state = {
      name: 'Printer',
      responseData: this.props.responseData
    };
  }

  printReceipt() {
    window.print();
  }

  render() {
    const{responseData} = this.state
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              <div className="toolbar-ui-wrapper">
                <div className="leftsection">
                  <span key="first" className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      {/* <li>
                        <span className="text-bt">
                          <SaveOutlined /> Save
                        </span>
                      </li> */}
                    </ul>
                  </span>
                </div>
                <div className="rightsection">
                  <span className="wrap-bar-menu">
                    <ul className="wrap-bar-ul">
                      <li>
                        <ReactToPrint
                          trigger={() => (
                            <span className="text-bt">
                          <PrinterOutlined /> Print
                            </span>
                          )}
                          content={() => this.componentRef}
                        />
                      </li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              <ComponentToPrint ref={el => (this.componentRef = el)}  responseData = {responseData}/>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default AgencyAppointmentReport;
