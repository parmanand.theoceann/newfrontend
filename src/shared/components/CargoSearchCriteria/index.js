import React, { Component } from 'react';
import { Form, Input, DatePicker, Row, Col } from 'antd';

const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

class CargoSearchCriteria extends Component {
    render() {
        return (
            <div className="body-wrapper modalWrapper modal-report-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Form>
                                <Row gutter={16}>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Cargo">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Charterer">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Load Port">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                </Row>

                                <Row gutter={16}>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Discharge Port">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="CP Qty From">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="CP Qty To">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                </Row>

                                <Row gutter={16}>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Cargo ID">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Cargo COA">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Booking No.">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                </Row>

                                <Row gutter={16}>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Company">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Laycan From">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Laycan To">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                </Row>

                                <Row gutter={16}>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Nominated Vessel">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Trade Area">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </Col>
                                </Row>

                            </Form>
                        </div>
                    </div>
                </article>
            </div>
        )
    }
}

export default CargoSearchCriteria;