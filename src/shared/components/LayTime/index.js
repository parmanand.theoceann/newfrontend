import React from 'react';
import { Input, Form, Select, Row, Col, Checkbox, Table, Button, Tabs, DatePicker, TimePicker, Modal } from 'antd';
import ToolbarUI from '../../../components/ToolbarUI';

import LaytimeInvoice from '../All-Print-Reports/LaytimeInvoice';
const FormItem = Form.Item;
const Option = Select.Option;
const InputGroup = Input.Group;
const { TabPane } = Tabs;
const { TextArea } = Input;

const columns1 = [{
  title: 'Charterer',
  dataIndex: 'charterer',
  key: 'charterer',
}, {
  title: 'S',
  dataIndex: 's',
  key: 's',
  render: text => <Checkbox></Checkbox>,
}];

const data1 = [{
  key: '1',
  charterer: 'LDC LAT',
  s: '',
}];

const columns2 = [{
  title: 'Port Name',
  dataIndex: 'port_name',
  key: 'port_name',
}, {
  title: 'Corgo',
  dataIndex: 'corgo',
  key: 'corgo',
}, {
  title: 'BL Qty',
  dataIndex: 'bl_qty',
  key: 'bl_qty',
}, {
  title: 'Unit',
  dataIndex: 'unit',
  key: 'unit',
}, {
  title: 'F',
  dataIndex: 'f',
  key: 'f',
}, {
  title: 'Status',
  dataIndex: 'status',
  key: 'status',
}];

const data2 = [{
  key: '1',
  port_name: 'Santos',
  corgo: 'SYYYYY',
  bl_qty: '3444',
  unit: 'MT',
  f: 'L',
  status: 'SA',
}];

const columns3 = [{
  title: 'Claim Id',
  dataIndex: 'claim',
  key: 'claim',
}, {
  title: 'CounterParty',
  dataIndex: 'Counterparty',
  key: 'Counterparty',
}, {
  title: 'Type',
  dataIndex: 'Type',
  key: 'Type',
}, {
  title: 'Ports',
  dataIndex: 'Ports',
  key: 'Ports',
}, {
  title: 'Dem/Des',
  dataIndex: 'Dem_Des',
  key: 'Dem_Des',
}, {
  title: 'Invoice Date',
  dataIndex: 'Invoice_date',
  key: 'Invoice_date',
}, {
  title: 'Amount USD',
  dataIndex: 'amount',
  key: 'amount',
}, {
  title: 'Status',
  dataIndex: 'status',
  key: 'status',
}];

const data3 = [{
  key: '1',
  claim: '45555',
  Counterparty: 'LDC HHHH',
  Type: 'Chartere',
  Ports: 'Santos',
  Dem_Des: 'Dispatch',
  Invoice_date: '12/05/2022',
  amount: '33535',
  status: 'Settled',
}];

const columns4 = [{
  title: 'Port Name',
  dataIndex: 'port_name',
  key: 'port_name',
}, {
  title: 'Corgo ',
  dataIndex: 'Corgo',
  key: 'Corgo',
}, {
  title: 'F ',
  dataIndex: 'f',
  key: 'f',
}, {
  title: 'Quatity ',
  dataIndex: 'Quatity',
  key: 'Quatity',
}, {
  title: 'L/D Rate ',
  dataIndex: 'ld_rate',
  key: 'ld_rate',
}, {
  title: 'Terms ',
  dataIndex: 'terms',
  key: 'terms',
}, {
  title: 'Dem Rate/D ',
  dataIndex: 'dem_rate',
  key: 'dem_rate',
}, {
  title: 'Des Rate/D ',
  dataIndex: 'des_rate',
  key: 'des_rate',
}, {
  title: 'Allowed',
  dataIndex: 'allowed',
  key: 'allowed',
}, {
  title: 'Used',
  dataIndex: 'used',
  key: 'used',
}, {
  title: 'Deduction',
  dataIndex: 'deduction',
  key: 'deduction',
}, {
  title: 'Balance',
  dataIndex: 'balance',
  key: 'balance',
}, {
  title: 'Laycon From',
  dataIndex: 'laycon_from',
  key: 'laycon_from',
}, {
  title: 'Laycon To',
  dataIndex: 'laycon_to',
  key: 'laycon_to',
}];

const data4 = [{
  key: '1',
  port_name: 'Santos',
  Corgo: 'TYUUII888',
  f: 'L',
  Quatity: '4543',
  ld_rate: '55',
  terms: '5345',
  dem_rate: '455',
  des_rate: '4353',
  allowed: '5345',
  used: '24',
  deduction: '423',
  balance: '45345',
  laycon_from: '353',
  laycon_to: '535',
}];

const columns5 = [{
  title: 'Day',
  dataIndex: 'Day',
  key: 'Day',
}, {
  title: 'Activity',
  dataIndex: 'activity',
  key: 'activity',
}, {
  title: 'From Date',
  dataIndex: 'f_date',
  key: 'f_date',
}, {
  title: 'Time',
  dataIndex: 'time',
  key: 'time',
}, {
  title: 'Duration',
  dataIndex: 'duration',
  key: 'duration',
}, {
  title: '%',
  dataIndex: 'per',
  key: 'per',
}, {
  title: 'To Date',
  dataIndex: 't_date',
  key: 't_date',
}, {
  title: 'Time',
  dataIndex: 'time',
  key: 'time',
}, {
  title: 'Remark',
  dataIndex: 'remark',
  key: 'remark',
}, {
  title: 'Deduction',
  dataIndex: 'deduction',
  key: 'deduction',
}];

const data5 = [{
  key: '1',
  Day: 'TH',
  activity: 'North',
  f_date: '12/12/2022',
  f_time: '12:20',
  duration: '23',
  per: '23',
  t_date: '23/10/2022',
  t_time: '12:78',
  remark: 'lorem',
  deduction: '345.5555',
}];

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User', // Column configuration not to be checked
  }),
};

class LayTime extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isfirstPage: true
    }
  }

  onNextStep = () => {
    this.setState({ isfirstPage: false });
  }

  onBackStep = () => {
    this.setState({ isfirstPage: true });
  }

  callback = (evt) => {
    if (evt === "invoice") {
      this.setState({ isVisibleInvoice: true });
    }
  }

  handleCancel = () => {
    this.setState({ isVisibleInvoice: false });
  }

  render() {

    return (
      <>
        {
          this.state.isfirstPage ?
            <div className="body-wrapper modal-body-wraper">
              <article className="article toolbaruiWrapper m-b-18 m-t-28">
                <div className="box box-default">
                  <div className="box-body top-heading">
                    <div className="form-wrapper toolbar-ui-wrapper">
                      <div className="form-heading">
                        <h4 className="title"><span>New Laytime Calculation Setup</span></h4>
                      </div>
                      <div className="action-btn">
                        <Button type="primary" htmlType="submit" onClick={this.onNextStep}>Ok</Button>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body">
                    <Form>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Vessel"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Select defaultValue="Vessel">
                              <Option value="Vessel">Vessel</Option>
                            </Select>
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Voy No."
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Voy No." />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Counterparty Type"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Counterparty Type" />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Method"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Method" />
                          </FormItem>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body common-fields-wrapper">
                    <Form>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={9} lg={9} xl={9}>
                          <Table columns={columns1} dataSource={data1} pagination={false} bordered />
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                          <Table rowSelection={rowSelection} columns={columns2} dataSource={data2} pagination={false} bordered />
                        </Col>
                        <Col xs={24} sm={24} md={3} lg={3} xl={3}>
                          <Checkbox.Group>
                            <Row>
                              <Col span={24}><Checkbox value="Include Transit Ports">Include Transit Ports</Checkbox></Col>
                              <Col span={24}><Checkbox value="Include Purging ports">Include Purging ports</Checkbox></Col>
                              <Col span={24}><Checkbox value="Include Clening ports">Include Clening ports</Checkbox></Col>
                              <Col span={24}><Checkbox value="Include Waiting ports">Include Waiting ports</Checkbox></Col>
                              <Col span={24}><Checkbox value="Reversible Selected Ports">Reversible Selected Ports</Checkbox></Col>
                              <Col span={24}><Checkbox value="Laytime Expires">Laytime Expires</Checkbox></Col>
                              <Col span={24}><Checkbox value="Once On/ Always On Dem">Once On/ Always On Dem</Checkbox></Col>
                            </Row>
                          </Checkbox.Group>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body common-fields-wrapper">
                    <Form>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                          <Table columns={columns3} dataSource={data3} pagination={false} bordered />
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </div>
              </article>

            </div>
            :
            <div className="body-wrapper modal-body-wraper">
              <article className="article toolbaruiWrapper m-b-18 m-t-28">
                <div className="box box-default">
                  <div className="box-body top-heading">
                    <Form>
                      <div className="form-wrapper toolbar-ui-wrapper">
                        <div className="form-heading">
                          <h4 className="title"><span>LayTime Calculation For Charterer - AR Claim</span></h4>
                        </div>
                        <div className="action-btn">
                          <Button type="secondary" onClick={this.onBackStep}>Back</Button>
                          <Button type="primary" htmlType="submit">Save</Button>
                        </div>
                      </div>
                    </Form>
                  </div>
                </div>
              </article>

              <article className="article toolbaruiWrapper m-b-18 m-t-28">
                <div className="box box-default">
                  <div className="box-body top-heading">
                    <ToolbarUI routeUrl={'laytime-toolbar'} callback={(e) => this.callback(e)} />
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body">
                    <Form>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Charterer"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Charterer" disabled />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Vessel"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <div>
                              <Input addonAfter="D3536633" defaultValue="Golden Rose" disabled />
                            </div>
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="CP Form"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="CP Form" disabled />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Status"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Select defaultValue="Under Review">
                              <Option value="Under Review">Under Review</Option>
                            </Select>
                          </FormItem>
                        </Col>
                      </Row>
                      <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Invoice No."
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Invoice No." disabled />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Voyage No/Opr Type"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <div>
                              <Input addonAfter="TCOV" defaultValue="1" disabled />
                            </div>
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="CP Date"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="CP Date" disabled />
                          </FormItem>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                          <FormItem
                            label="Analyst"
                            labelCol={{ span: 12 }}
                            wrapperCol={{ span: 12 }}
                          >
                            <Input placeholder="Analyst" disabled />
                          </FormItem>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body common-fields-wrapper">
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <Table rowSelection={rowSelection} columns={columns4} dataSource={data4} pagination={false} bordered />
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <Row type="flex" justify="end" className="m-t-18">
                          <FormItem
                            label="Total"
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 18 }}
                          >
                            <InputGroup compact>
                              <Input style={{ width: '25%' }} defaultValue="12.666666" disabled />
                              <Input style={{ width: '25%' }} defaultValue="12.666666" disabled />
                              <Input style={{ width: '25%' }} defaultValue="12.666666" disabled />
                              <Input style={{ width: '25%' }} defaultValue="12.666666" disabled />
                            </InputGroup>
                          </FormItem>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body common-fields-wrapper">
                    <Tabs defaultActiveKey="1">
                      <TabPane tab="Santos" key="1">
                        <Table columns={columns5} dataSource={data5} pagination={false} bordered footer={() =>
                          <div className="text-center">
                            <Button type="link" onClick={this.handleAdd}>Add New</Button>
                          </div>
                        } title={() => <b>Port Activities</b>} />
                        <div className="m-t-18">
                          <Button type="primary">Reset Percentages</Button>
                        </div>
                        <Row gutter={16}>
                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              label="Laytime Commenced"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <InputGroup compact>
                                <DatePicker placeholder="Date" style={{ width: '50%' }} />
                                <TimePicker placeholder="Time" style={{ width: '50%' }} />
                              </InputGroup>
                            </FormItem>
                            <FormItem
                              label="Laytime Completed"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <InputGroup compact>
                                <DatePicker placeholder="Date" style={{ width: '50%' }} />
                                <TimePicker placeholder="Time" style={{ width: '50%' }} />
                              </InputGroup>
                            </FormItem>
                          </Col>
                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              label="Counterparty Qty"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <InputGroup compact>
                                <Input style={{ width: '50%' }} placeholder="Used" />
                                <Input style={{ width: '50%' }} placeholder="Allowed" />
                              </InputGroup>
                            </FormItem>
                            <FormItem
                              label="Total Qty"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <InputGroup compact>
                                <Input style={{ width: '50%' }} placeholder="Used" />
                                <Input style={{ width: '50%' }} placeholder="Allowed" />
                              </InputGroup>
                            </FormItem>
                          </Col>
                          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <FormItem
                              label="Allocated Used"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <Input placeholder="Allocated Used" disabled />
                            </FormItem>
                            <FormItem
                              label="Allocated Allowed"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <Input placeholder="Allocated Allowed" disabled />
                            </FormItem>
                            <FormItem
                              label="Minimum Allowed"
                              labelCol={{ span: 10 }}
                              wrapperCol={{ span: 14 }}
                            >
                              <Input placeholder="Minimum Allowed" />
                            </FormItem>
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tab="Bayuquan" key="2">Bayuquan</TabPane>
                      <TabPane tab="All Ports" key="3">All Ports</TabPane>
                    </Tabs>
                  </div>
                </div>
              </article>

              <article className="article m-b-18">
                <div className="box box-default">
                  <div className="box-body common-fields-wrapper">
                    <div className="m-t-18">
                      <Button type="primary">Import Activities</Button>
                    </div>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          label="Calculation"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Calculation" defaultValue="standard" />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <Checkbox.Group>
                          <Row>
                            <Col span={8}><Checkbox value="Laytime expires">Laytime expires</Checkbox></Col>
                            <Col span={8}><Checkbox value="Include in P&L">Include in P&L</Checkbox></Col>
                            <Col span={8}><Checkbox value="Continuous Latime">Continuous Latime</Checkbox></Col>
                            <Col span={8}><Checkbox value="HH:MM Format">HH:MM Format</Checkbox></Col>
                            <Col span={8}><Checkbox value="Once On/Always On Dem">Once On/Always On Dem</Checkbox></Col>
                          </Row>
                        </Checkbox.Group>
                      </Col>
                    </Row>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          label="Net Used Time"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Net Used Time" />
                        </FormItem>
                        <FormItem
                          label="Allowed Days"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Allowed Days" defaultValue="12.443332" disabled />
                        </FormItem>
                        <FormItem
                          label="Amount Paid To Owner"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Amount Paid To Owner" defaultValue="" disabled />
                        </FormItem>
                        <FormItem
                          label="Agreed Dispatch"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Agreed Dispatch" defaultValue="0.00000" />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          label="Used Days"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Used Days" defaultValue="12.443332" disabled />
                        </FormItem>
                        <FormItem
                          label="Balance Days"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Balance Days" defaultValue="12.443332" disabled />
                        </FormItem>
                        <FormItem
                          label="Dispatch Amount"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Dispatch Amount" defaultValue="12.443332" disabled />
                        </FormItem>
                        <FormItem
                          label="Difference"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Difference" defaultValue="-12.22222" disabled />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                        <FormItem
                          label="Original Claim Amount"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Original Claim Amount" defaultValue="12.443332" />
                        </FormItem>
                        <FormItem
                          label="Curr/Exch Rate"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <InputGroup compact>
                            <Input style={{ width: '50%' }} placeholder="USD" />
                            <Input style={{ width: '50%' }} placeholder="Allowed" defaultValue="12.443332" disabled />
                          </InputGroup>
                        </FormItem>
                        <FormItem
                          label="Agreed Demurage"
                          labelCol={{ span: 10 }}
                          wrapperCol={{ span: 14 }}
                        >
                          <Input placeholder="Agreed Demurage" defaultValue="0.00000" />
                        </FormItem>
                      </Col>
                    </Row>
                    <Row gutter={16}>
                      <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                        <FormItem
                          label="Difference Reason"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea placeholder="Difference Reason" autoSize={{ minRows: 6, maxRows: 6 }} />
                        </FormItem>
                      </Col>
                      <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                        <FormItem
                          label="Calculation Remark"
                          labelCol={{ span: 24 }}
                          wrapperCol={{ span: 24 }}
                        >
                          <TextArea placeholder="Calculation Remark" autoSize={{ minRows: 6, maxRows: 6 }} />
                        </FormItem>
                      </Col>
                    </Row>
                  </div>
                </div>
              </article>


            </div>
        }
        <Modal
          title="Invoice"
         open={this.state.isVisibleInvoice}
          width={1300}
          onCancel={this.handleCancel}
          footer={false}
        >
          <LaytimeInvoice />
        </Modal>
      </>
    )
  }
}

export default LayTime;
