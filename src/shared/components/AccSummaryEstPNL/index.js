import React, { Component } from 'react';
import { Form, Input, Button, Table, Row, Col, DatePicker, Tabs } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const { Column, ColumnGroup } = Table;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

const data = [
    {
        key: '1',
        somedata4: "Hire",
        description: "",
        invoiceno: "INV00001",
        somedata3: "179,644",
        somedata2: "209,490",
        somedata1: "209,490",
        inout: "In",
        actest: "29,846",
        percent1: "17",
        actpst: "(20,280)",
        percent2: "(0)"
    },
    {
        key: '2',
        somedata4: "Hire",
        description: "",
        invoiceno: "INV00001",
        somedata3: "179,644",
        somedata2: "209,490",
        somedata1: "209,490",
        inout: "In",
        actest: "29,846",
        percent1: "17",
        actpst: "(20,280)",
        percent2: "(0)"
    },
    {
        key: '3',
        somedata4: "Hire",
        description: "",
        invoiceno: "INV00001",
        somedata3: "179,644",
        somedata2: "209,490",
        somedata1: "209,490",
        inout: "In",
        actest: "29,846",
        percent1: "17",
        actpst: "(20,280)",
        percent2: "(0)"
    },
    {
        key: '4',
        somedata4: "Hire",
        description: "",
        invoiceno: "INV00001",
        somedata3: "179,644",
        somedata2: "209,490",
        somedata1: "209,490",
        inout: "In",
        actest: "29,846",
        percent1: "17",
        actpst: "(20,280)",
        percent2: "(0)"
    },
    {
        key: '5',
        somedata4: "Hire",
        description: "",
        invoiceno: "INV00001",
        somedata3: "179,644",
        somedata2: "209,490",
        somedata1: "209,490",
        inout: "In",
        actest: "29,846",
        percent1: "17",
        actpst: "(20,280)",
        percent2: "(0)"
    },
];

const dataSource = [
    {
        key: '1',
        somedata4: "Profit (Loss)",
        estimate: "(69,278)",
        actual: "(99,081)",
        posted: "(119,436)",
        inout: "",
        actest: "(29,803)",
        percent1: "(43)",
        actpst: "20,355",
        percent2: "(21)",
    },
    {
        key: '2',
        somedata4: "Net Voyage Days",
        estimate: "(69,278)",
        actual: "(99,081)",
        posted: "(119,436)",
        inout: "",
        actest: "(29,803)",
        percent1: "(43)",
        actpst: "20,355",
        percent2: "(21)",
    },
    {
        key: '3',
        somedata4: "T/C Equivalent",
        estimate: "(69,278)",
        actual: "(99,081)",
        posted: "(119,436)",
        inout: "",
        actest: "(29,803)",
        percent1: "(43)",
        actpst: "20,355",
        percent2: "(21)",
    },
];

const dataSource2 = [
    {
        key: '1',
        somedata1: "Total/Off hire days",
        somedata2: "25.66",
        somedata3: "",
        somedata4: "29.93",
        somedata5: "1.79",
        somedata6: "",
        somedata7: "",
        somedata8: "4.27",
        somedata9: "1.79",
        somedata10: "",
        somedata11: "",
        somedata12: "",
    },
    {
        key: '2',
        somedata1: "Port/Sea days",
        somedata2: "8.08",
        somedata3: "17.58",
        somedata4: "10.50",
        somedata5: "19.43",
        somedata6: "",
        somedata7: "",
        somedata8: "2.42",
        somedata9: "1.85",
        somedata10: "",
        somedata11: "",
        somedata12: "",
    },
];

class AccSummaryEstPNL extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Form>
                                <div className="form-wrapper">
                                    <div className="form-heading">
                                        <h4 className="title"><span>Account Summary</span></h4>
                                    </div>
                                    <div className="action-btn">
                                        <Button type="primary" htmlType="submit">Voyage Period Journals</Button>
                                        <Button type="primary" htmlType="submit">Save</Button>
                                        <Button>Reset</Button>
                                    </div>
                                </div>

                                <Row gutter={16}>
                                    <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="VESSEL">
                                            <Input size="default" placeholder="AARGAU" />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Company">
                                            <Input size="default" placeholder="" defaultValue="SIBD" disabled />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Voyage Commencing">
                                            <DatePicker />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Chtr Specialist">
                                            <Input size="default" placeholder="" defaultValue="" />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Ops Coordinator">
                                            <Input size="default" placeholder="" defaultValue="" />
                                        </FormItem>
                                    </Col>

                                    <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Voy No./Opr Type">
                                            <InputGroup compact>
                                                <Input style={{ width: '30%' }} defaultValue="AAAU" />
                                                <Input style={{ width: '40%' }} defaultValue="1" />
                                                <Input style={{ width: '30%' }} defaultValue="TCOV" />
                                            </InputGroup>
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="TC Code/Hire">
                                            <InputGroup compact>
                                                <Input style={{ width: '50%' }} defaultValue="AAAU-I0001" />
                                                <Input style={{ width: '50%' }} defaultValue="7,000.00" disabled />
                                            </InputGroup>
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Market Hire">
                                            <Input size="default" placeholder="" defaultValue="" />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Voyage Completing">
                                            <DatePicker disabled />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Last Update GMT">
                                            <DatePicker disabled />
                                        </FormItem>
                                    </Col>

                                    <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Voyage Status">
                                            <Input size="default" placeholder="" defaultValue="Completed" />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Trade Area">
                                            <Input size="default" placeholder="" defaultValue="CLUSTER - 4" />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Fixture No.">
                                            <Input size="default" placeholder="" defaultValue="20160269" disabled />
                                        </FormItem>
                                        <FormItem
                                            {...formItemLayout}
                                            label="Bunker Calc Method">
                                            <Input size="default" placeholder="" defaultValue="LIFO" />
                                        </FormItem>
                                    </Col>
                                </Row>
                            </Form>

                            <hr />

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="table-info-wrapper">
                                        <Table dataSource={data} bordered size="small" pagination={false}>
                                            <ColumnGroup title="Basis / Compare">
                                                <Column title="" dataIndex="somedata4" key="somedata4" />
                                                <Column title="Description" dataIndex="description" key="description" />
                                                <Column title="Invoice No." dataIndex="invoiceno" key="invoiceno" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Estimated">
                                                <Column title="" dataIndex="somedata3" key="somedata3" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Actual">
                                                <Column title="" dataIndex="somedata2" key="somedata2" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Posted">
                                                <Column title="" dataIndex="somedata1" key="somedata1" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Cash">
                                                <Column title="In / (Out)" dataIndex="inout" key="inout" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Variance">
                                                <Column title="Act -Est" dataIndex="actest" key="actest" />
                                                <Column title="%" dataIndex="percent1" key="percent1" />
                                                <Column title="Act - Pst" dataIndex="actpst" key="actpst" />
                                                <Column title="%" dataIndex="percent2" key="percent2" />
                                            </ColumnGroup>
                                        </Table>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-4">
                                    <FormItem label="Remarks">
                                        <TextArea placeholder="Remarks" autoSize={{ minRows: 9.8, maxRows: 9.8 }} />
                                    </FormItem>
                                </div>
                                <div className="col-md-8">
                                    <div className="table-info-wrapper">
                                        <Table dataSource={dataSource} bordered pagination={false}>
                                            <ColumnGroup title="">
                                                <Column title="" dataIndex="somedata4" key="somedata4" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Estimated">
                                                <Column title="" dataIndex="estimate" key="estimate" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Actual">
                                                <Column title="" dataIndex="actual" key="actual" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Posted">
                                                <Column title="" dataIndex="posted" key="posted" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Cash">
                                                <Column title="In / (Out)" dataIndex="inout" key="inout" />
                                            </ColumnGroup>
                                            <ColumnGroup title="Variance">
                                                <Column title="Act -Est" dataIndex="actest" key="actest" />
                                                <Column title="%" dataIndex="percent1" key="percent1" />
                                                <Column title="Act - Pst" dataIndex="actpst" key="actpst" />
                                                <Column title="%" dataIndex="percent2" key="percent2" />
                                            </ColumnGroup>
                                        </Table>
                                    </div>

                                    <div className="table-info-wrapper hide-table-header">
                                        <Table dataSource={dataSource2} bordered pagination={false}>
                                            <Column title="" dataIndex="somedata1" key="somedata1" />
                                            <Column title="" dataIndex="somedata2" key="somedata2" />
                                            <Column title="" dataIndex="somedata3" key="somedata3" />
                                            <Column title="" dataIndex="somedata4" key="somedata4" />
                                            <Column title="" dataIndex="somedata5" key="somedata5" />
                                            <Column title="" dataIndex="somedata6" key="somedata6" />
                                            <Column title="" dataIndex="somedata7" key="somedata7" />
                                            <Column title="" dataIndex="somedata8" key="somedata8" />
                                            <Column title="" dataIndex="somedata9" key="somedata9" />
                                            <Column title="" dataIndex="somedata10" key="somedata10" />
                                            <Column title="" dataIndex="somedata11" key="somedata11" />
                                            <Column title="" dataIndex="somedata12" key="somedata12" />
                                        </Table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Tabs defaultActiveKey="pl" type="card">
                                <TabPane tab="Estimate" key="estimate">Estimate</TabPane>
                                <TabPane tab="Operations" key="operations">Operation</TabPane>
                                <TabPane tab="Invoices" key="invoices">Invoices</TabPane>
                                <TabPane tab="P&L" key="pl">P&L</TabPane>
                                <TabPane tab="Contacts" key="contacts">Contacts</TabPane>
                                <TabPane tab="Notes" key="notes">Notes</TabPane>
                                <TabPane tab="Revisions" key="revisions">Revisions</TabPane>
                                <TabPane tab="Instructions" key="instructions">Instructions</TabPane>
                            </Tabs>
                        </div>
                    </div>
                </article>

            </div>
        )
    }
}

export default AccSummaryEstPNL;