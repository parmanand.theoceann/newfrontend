// import React, { Component } from 'react';
// import { Form, Input, Select, Table, Icon } from 'antd';

// const FormItem = Form.Item;
// const Option = Select.Option;
// const InputGroup = Input.Group;

// function vessel(value) {
//     // console.log(`selected ${value}`);
// }

// const dataSource = [
//     {
//         key: "1",
//         broker: "Broker",
//         type: "Type",
//         invoiceno: "Invoice No",
//         date: "Date",
//         curr: "Curr",
//         amount: "Amount",
//         st: "St"
//     },
//     {
//         key: "2",
//         broker: "Broker",
//         type: "Type",
//         invoiceno: "Invoice No",
//         date: "Date",
//         curr: "Curr",
//         amount: "Amount",
//         st: "St"
//     },
//     {
//         key: "3",
//         broker: "Broker",
//         type: "Type",
//         invoiceno: "Invoice No",
//         date: "Date",
//         curr: "Curr",
//         amount: "Amount",
//         st: "St"
//     },
//     {
//         key: "4",
//         broker: "Broker",
//         type: "Type",
//         invoiceno: "Invoice No",
//         date: "Date",
//         curr: "Curr",
//         amount: "Amount",
//         st: "St"
//     },
//     {
//         key: "5",
//         broker: "Broker",
//         type: "Type",
//         invoiceno: "Invoice No",
//         date: "Date",
//         curr: "Curr",
//         amount: "Amount",
//         st: "St"
//     },
// ];

// const columns = [
//     {
//         title: 'Broker',
//         dataIndex: 'broker',
//         key: 'broker',
//     },
//     {
//         title: 'Type',
//         dataIndex: 'type',
//         key: 'type',
//     },
//     {
//         title: 'Invoice No.',
//         dataIndex: 'invoiceno',
//         key: 'invoiceno',
//     },
//     {
//         title: 'Date',
//         dataIndex: 'date',
//         key: 'date',
//     },
//     {
//         title: 'Curr',
//         dataIndex: 'curr',
//         key: 'curr',
//     },
//     {
//         title: 'Amount',
//         dataIndex: 'amount',
//         key: 'amount',
//     },
//     {
//         title: 'St',
//         dataIndex: 'st',
//         key: 'st',
//     },
// ];

// class FreightCommission extends Component {
//     render() {
//         return (
//             <div className="body-wrapper modalWrapper">

//                 <article className="article toolbaruiWrapper">
//                     <div className="box box-default">
//                         <div className="box-body">
//                             <div className="toolbar-ui-wrapper">
//                                 <div className="leftsection">
//                                     <span key="first" className="wrap-bar-menu">
//                                         <ul className="wrap-bar-ul">
//                                             <li><span className="text-bt"><Icon type="folder-open" /></span></li>
//                                             <li><span className="text-bt"><Icon type="file-excel" /></span></li>
//                                             <li><span className="text-bt"><Icon type="reload" /></span></li>
//                                         </ul>
//                                     </span>
//                                 </div>
//                                 <div className="rightsection">
//                                     <span className="wrap-bar-menu">
//                                         <ul className="wrap-bar-ul">
//                                             <li><span className="text-bt"><Icon type="file" /> New Payment</span></li>
//                                         </ul>
//                                     </span>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </article>

//                 <article className="article">
//                     <div className="box box-default">
//                         <div className="box-body">
//                             <Form>
//                                 <div className="row">
//                                     <div className="col-md-4">
//                                         <FormItem label="Vessel">
//                                             <InputGroup compact>
//                                                 <Select style={{width: '80%'}} defaultValue="Option One" onChange={vessel}>
//                                                     <Option value="Option One">Option One</Option>
//                                                     <Option value="Option Two">Option Two</Option>
//                                                 </Select>
//                                                 <Input style={{ width: '20%' }} defaultValue="" />
//                                             </InputGroup>
//                                         </FormItem>
//                                     </div>

//                                     <div className="col-md-4">
//                                         <FormItem label="Voyage">
//                                             <Input size="default" placeholder="" />
//                                         </FormItem>
//                                     </div>
//                                 </div>

//                                 <hr />

//                                 <div className="row">
//                                     <div className="col-md-12">
//                                         <div className="table-info-wrapper">
//                                             <Table size="small" bordered pagination={false} columns={columns} dataSource={dataSource} />
//                                         </div>
//                                     </div>
//                                 </div>

//                             </Form>
//                         </div>
//                     </div>
//                 </article>

//             </div>
//         )
//     }
// }

// export default FreightCommission;


