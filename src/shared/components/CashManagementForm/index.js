import React, { Component } from 'react';
import { Form, Input, Table, Icon, DatePicker, Checkbox, Popconfirm, Button, Switch } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;

function onChange(checkedValues) {
    // console.log('checked = ', checkedValues);
}

const dataSourcePay = [
    {
        key: "1",
        invtransno: "Inv Trans No",
        date: "Date",
        invoice: "Invoice",
        description: "Description",
        duedate: "Due Date",
        curr: "Curr",
        amtneedtopay: "Amt Need To Pay",
        payamount: "Pay Amount",
        xchgdiff: "XchgDiff"
    },
    {
        key: "2",
        invtransno: "Inv Trans No",
        date: "Date",
        invoice: "Invoice",
        description: "Description",
        duedate: "Due Date",
        curr: "Curr",
        amtneedtopay: "Amt Need To Pay",
        payamount: "Pay Amount",
        xchgdiff: "XchgDiff"
    },
    {
        key: "3",
        invtransno: "Inv Trans No",
        date: "Date",
        invoice: "Invoice",
        description: "Description",
        duedate: "Due Date",
        curr: "Curr",
        amtneedtopay: "Amt Need To Pay",
        payamount: "Pay Amount",
        xchgdiff: "XchgDiff"
    },
    {
        key: "4",
        invtransno: "Inv Trans No",
        date: "Date",
        invoice: "Invoice",
        description: "Description",
        duedate: "Due Date",
        curr: "Curr",
        amtneedtopay: "Amt Need To Pay",
        payamount: "Pay Amount",
        xchgdiff: "XchgDiff"
    },
    {
        key: "5",
        invtransno: "Inv Trans No",
        date: "Date",
        invoice: "Invoice",
        description: "Description",
        duedate: "Due Date",
        curr: "Curr",
        amtneedtopay: "Amt Need To Pay",
        payamount: "Pay Amount",
        xchgdiff: "XchgDiff"
    },
];

const columnsPay = [
    {
        title: 'Inv Trans No',
        dataIndex: 'invtransno',
        key: 'invtransno',
    },
    {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'Invoice No',
        dataIndex: 'invoice',
        key: 'invoice',
    },
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: 'Due Date',
        dataIndex: 'duedate',
        key: 'duedate',
    },
    {
        title: 'Curr',
        dataIndex: 'curr',
        key: 'curr',
    },
    {
        title: 'Amt Need To Pay',
        dataIndex: 'amtneedtopay',
        key: 'amtneedtopay',
    },
    {
        title: 'Pay Amount',
        dataIndex: 'payamount',
        key: 'payamount',
    },
    {
        title: 'XchgDiff',
        dataIndex: 'xchgdiff',
        key: 'xchgdiff',
    },
];

class CashManagementForm extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="body-wrapper modalWrapper">

                <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="toolbar-ui-wrapper">
                                <div className="leftsection">
                                    <span key="first" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt"><Icon type="folder-open" /></span></li>
                                            <li><span className="text-bt"><SaveOutlined /> Save and Post</span></li>
                                        </ul>
                                    </span>
                                </div>
                                <div className="rightsection">
                                    <span className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt"><Icon type="file" /> New Payment/Receipt</span></li>
                                            <li><span className="text-bt"><Icon type="file" /> New Advance</span></li>
                                            <li><span className="text-bt"><Icon type="bank" /> Finalize Bank Transfer</span></li>
                                            <li><span className="text-bt"><CloseOutlined /> Cancel Payment/Receipt</span></li>
                                            <li><span className="text-bt"><Icon type="snippets" /> Report</span></li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Form>
                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Transaction No">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="User Initials">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Value Date">
                                            <DatePicker />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Memo">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Make Payment">
                                            <Switch checkedChildren="Yes" unCheckedChildren="No" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Enter Receipt">
                                            <Switch checkedChildren="Yes" unCheckedChildren="No" />
                                        </FormItem>
                                    </div>
                                </div>

                                <hr />

                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="table-info-wrapper">
                                            <Table size="small" bordered pagination={false} columns={columnsPay} dataSource={dataSourcePay} />
                                        </div>
                                    </div>
                                </div>

                                <div className="row p10">
                                    <div className="col-md-4">
                                        <FormItem label="Remaining To Pay / Total Pay Amount">
                                            <InputGroup compact>
                                                <Input style={{ width: '40%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Inv Curr/Base Curr Exchg Rate">
                                            <Input size="default" placeholder="1.000000" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Base Curr Amount">
                                            <InputGroup compact>
                                                <Input style={{ width: '70%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="USD" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Bank Charge">
                                            <InputGroup compact>
                                                <Input style={{ width: '40%' }} defaultValue="" />
                                                <Input style={{ width: '30%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Other Charge">
                                            <InputGroup compact>
                                                <Input style={{ width: '40%' }} defaultValue="" />
                                                <Input style={{ width: '30%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Total Bank Amount">
                                            <InputGroup compact>
                                                <Input style={{ width: '70%' }} defaultValue="0.00" />
                                                <Input style={{ width: '30%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Check No">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Reference">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Mode">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Date">
                                            <DatePicker />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Company">
                                            <InputGroup compact>
                                                <Input style={{ width: '50%' }} defaultValue="" />
                                                <Input style={{ width: '50%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Vendor/Customer">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Details">
                                            <TextArea placeholder="" autoSize={{ minRows: 1, maxRows: 3 }} />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Bank Name">
                                            <InputGroup compact>
                                                <Input style={{ width: '70%' }} defaultValue="" />
                                                <Input style={{ width: '30%' }} defaultValue="" />
                                            </InputGroup>
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Third Party Transaction">
                                            <Switch checkedChildren="Yes" unCheckedChildren="No" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Final">
                                            <Switch checkedChildren="Yes" unCheckedChildren="No" />
                                        </FormItem>
                                    </div>

                                </div>


                            </Form>
                        </div>
                    </div>
                </article>

            </div>
        )
    }
}

export default CashManagementForm;