import React, { Component } from 'react';
import { Form, Input, Table } from 'antd';

const FormItem = Form.Item;
const { TextArea } = Input;

const dataSource = [
    {
        key: '1',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '2',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '3',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '4',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '5',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '6',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
    {
        key: '7',
        description: "Description",
        invoiceno: "Invoice No",
        estimated: "Estimated",
        actual: "Actual",
        posted: "Posted",
        cashin: "Cash In",
        variance: "Variance",
        percent: "%",
        variance2: "Variance",
        percent2: "%"
    },
];

const columns = [
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: 'Invoice No.',
        dataIndex: 'invoiceno',
        key: 'invoiceno',
    },
    {
        title: 'Estimated',
        dataIndex: 'estimated',
        key: 'estimated',
    },
    {
        title: 'Actual',
        dataIndex: 'actual',
        key: 'actual',
    },
    {
        title: 'Posted',
        dataIndex: 'posted',
        key: 'posted',
    },
    {
        title: 'Cash In',
        dataIndex: 'cashin',
        key: 'cashin',
    },
    {
        title: 'Variance',
        dataIndex: 'variance',
        key: 'variance',
    },
    {
        title: '%',
        dataIndex: 'percent',
        key: 'percent',
    },
    {
        title: 'Variance',
        dataIndex: 'variance2',
        key: 'variance2',
    },
    {
        title: '%',
        dataIndex: 'percent2',
        key: 'percent2',
    },
];

class Operations extends Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-info-wrapper">
                            <Table dataSource={dataSource} columns={columns} pagination={false} bordered size="small" scroll={{ y: 300 }} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Form>
                            <FormItem label="P & L Remarks">
                                <TextArea placeholder="" autoSize={{ minRows: 5, maxRows: 5 }} />
                            </FormItem>
                        </Form>
                    </div>
                </div>
            </>
        )
    }
}

export default Operations;