import React, { Component } from 'react';
import {  Tabs } from 'antd';
import {CaretDownOutlined }  from '@ant-design/icons';
import Operations from './operations';

const TabPane = Tabs.TabPane;

class PLEstPNL extends Component {
    render() {
        return (
            <div className="body-wrapper">

                <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                        <div className="box-body">
                            <div className="toolbar-ui-wrapper">
                                <div className="leftsection">
                                    <span key="first" className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt">Voyage Period Journals</span></li>
                                            <li><span className="text-bt">Snapshot <CaretDownOutlined /></span></li>
                                        </ul>
                                    </span>
                                </div>
                                <div className="rightsection">
                                    <span className="wrap-bar-menu">
                                        <ul className="wrap-bar-ul">
                                            <li><span className="text-bt">Basis: Actual <CaretDownOutlined /></span></li>
                                            <li><span className="text-bt">Compare: Posted <CaretDownOutlined /></span></li>
                                            <li><span className="text-bt">Show: All Periods <CaretDownOutlined /></span></li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article className="article">
                    <div className="box box-default">
                        <div className="box-body">
                            <Tabs defaultActiveKey="operations" type="card">
                                <TabPane tab="Operations" key="operations"><Operations /></TabPane>
                                <TabPane tab="Accounts" key="accounts">Accounts</TabPane>
                            </Tabs>
                        </div>
                    </div>
                </article>
            </div>
        )
    }
}

export default PLEstPNL;