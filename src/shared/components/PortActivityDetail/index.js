import React, { Component } from 'react';
import { Form, Input, Table, Icon, Modal, DatePicker, Row, Col, Tabs } from 'antd';
import PortActivities from './port-activities-listing';
import PortBunkerActivityReport from '../../../routes/operation-reports/PortBunkerActivityReport'
import NormalFormIndex from '../../../shared/NormalForm/normal_from_index';
import URL_WITH_VERSION, {
  postAPICall, getAPICall, URL_WITHOUT_VERSION, openNotificationWithIcon, apiDeleteCall, hasErrors,
} from '../../../shared';
import moment from 'moment';
import { SaveOutlined } from '@ant-design/icons';
const FormItem = Form.Item;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;


class PortActivityDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modals: {
        BunkerPurchasedReport: false,
      },
      frmName: "port_bunker_and_activity",
      formData: {},
      frmOptions: [],
      frmVisible: false,
      bunk_id:null,
    };
  }

  componentDidMount = async () => {
    const { data, editData } = this.props;
    let portArray = [];
    let index = editData.index;

    data &&
      data["portitinerary"] &&
      data["portitinerary"].length > 0 &&
      data["portitinerary"].map((val, ind) => {
        portArray.push({
          id: val.port_id,
          name: val.port,
          voyage_manager_id: val.voyage_manager_id,
        });
      });

     let idd=data && data['bunkerdetails'] && data['bunkerdetails'][index] && data['bunkerdetails'][index]['id'] ;
     this.setState({...this.state,'bunk_id':idd})
    if (editData) {

     try{
      const response = await getAPICall(
        `${URL_WITH_VERSION}/port-bunker-activity/initialize?pe=${
          editData.eventData.port
        }&e=${this.props.data.voyage_number}&be=${idd}`
      );
      let respdata = await response;
      let data1=respdata['data'];
      if (data && data1) {
      
        data1["curr_port"] = editData.eventData.port;
        data1["arrival_lt"] = data1["arrival_lt"]
          ? data1["arrival_lt"]
          : editData.eventData.arrival_date_time;
        data1["departure_lt"] = data1["departure_lt"]
          ? data1["departure_lt"]
          : editData.eventData.departure;
        data1["id"] = data1["id"] ? data1["id"] : editData.ID;
        data1["pre_date_dep"] = data["bunkerdetails"][index - 1]
          ? data["bunkerdetails"][index - 1]["departure"]
          : "";
        data1["tsd"] = data1["tsd"]
          ? data1["tsd"]
          : data["portitinerary"][index]
            ? parseFloat(data["portitinerary"][index]["tsd"])
            : 0;
        data1["tpd"] = data1["tpd"]
          ? data1["tpd"]
          : data["portitinerary"][index]
            ? parseFloat(data["portitinerary"][index]["t_port_days"])
            : 0;
      
     
    }else{
      openNotificationWithIcon('error',respdata.message,3)
    }
    this.setState({
      frmOptions: [{ key: "curr_port", data: portArray }],
      frmVisible: true,
      formData: data1,
    });
    }catch(err){
      console.log('error: ' , err)
      openNotificationWithIcon('error', 'Something Went Wrong',3);
    }

    } else if (
      (editData == null && data.hasOwnProperty("portitinerary")) ||
      data.hasOwnProperty("portdatedetails")
    ) {
      // const response = await getAPICall(`${URL_WITH_VERSION}/port-bunker-activity/initialize?pe=${portArray[0].id}&e=${portArray[0].voyage_manager_id}`)
      // let formdata = await response['data']
    
      let formdata = {};
      formdata["curr_port"] = portArray[index] ? portArray[index].id : "";
      formdata["prev_port"] = portArray[index - 1]
        ? portArray[index - 1].id
        : "";
      formdata["destination"] = portArray[index + 1]
        ? portArray[index + 1].id
        : "";
      formdata["pre_date_dep"] = data["bunkerdetails"][index - 1]
        ? data["bunkerdetails"][index - 1]["departure"]
        : "";
      formdata["tsd"] = data["portdatedetails"][index]
        ? parseFloat(data["portdatedetails"][index]["tsd"])
        : 0;
      formdata["tpd"] = data["portdatedetails"][index]
        ? parseFloat(data["portdatedetails"][index]["pdays"])
        : 0;
      if (
        data &&
        data["portdatedetails"] &&
        data["portdatedetails"].length > 0
      ) {
        formdata["arrival_lt"] = data["portdatedetails"][0].arrival_date_time;
        formdata["departure_lt"] =
          data["portdatedetails"][data["portdatedetails"].length - 1].departure;
      }
      this.setState({
        frmOptions: [{ key: "curr_port", data: portArray }],
        frmVisible: true,
        formData: formdata,
      });
    } else {
      this.setState({
        frmVisible: true,
        formData: {},
      });
    }
    try {
      // For checking blank and pass empty values
      this.state.formData &&
        this.state.formData.bunkerconsumptionupdate &&
        this.state.formData.bunkerconsumptionupdate.length > 0 &&
        this.state.formData.bunkerconsumptionupdate.map((item, index) => {
          this.state.formData.bunkerconsumptionupdate[
            index
          ].port_cons = parseFloat(
            typeof item.port_cons !== "undefined" ? item.port_cons : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].received_qty = parseFloat(
            typeof item.received_qty !== "undefined" ? item.received_qty : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].cons_rate = parseFloat(
            typeof item.cons_rate !== "undefined" ? item.cons_rate : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].rob_arr = parseFloat(
            typeof item.rob_arr !== "undefined" ? item.rob_arr : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].rob_departure = parseFloat(
            typeof item.rob_departure !== "undefined" ? item.rob_departure : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].sea_consumption = parseFloat(
            typeof item.sea_consumption !== "undefined"
              ? item.sea_consumption
              : 0
          ).toFixed(2);
          this.state.formData.bunkerconsumptionupdate[
            index
          ].rob_prev = parseFloat(
            typeof item.rob_prev !== "undefined" ? item.rob_prev : 0
          ).toFixed(2);
        });
    } catch (error) {
      openNotificationWithIcon("error", "Something Went Wrong.", 5);
    }
  };

  BunkerPurchasedReport = async (showBunkerPurchasedReport) => {
    const { editData } = this.props;
    let portid = editData.eventData.port_id;
    let voyage_manager_id = this.state.formData.voyage_no;

  if(showBunkerPurchasedReport){
    try {
      const responseReport = await getAPICall(
        `${URL_WITH_VERSION}/port-bunker-activity/report?pe=${portid}&e=${voyage_manager_id}`
      );
      const respDataReport = await responseReport["data"];
      let formDataReportValue = this.state.formData;

      // Just For Report
      formDataReportValue.after_draft_m =  formDataReportValue["-"] && formDataReportValue["-"].after_draft_m?formDataReportValue["-"].after_draft_m:0;
      formDataReportValue.forward_draft_m =formDataReportValue["-"] && formDataReportValue["-"].forward_draft_m?formDataReportValue["-"].forward_draft_m:0;
      formDataReportValue.mid_draft_m =formDataReportValue["-"] && formDataReportValue["-"].mid_draft_m?formDataReportValue["-"].mid_draft_m:0
      formDataReportValue.steam_hours = parseFloat(typeof formDataReportValue.steam_hours !== "undefined"? formDataReportValue.steam_hours: 0);
      const {
        address: company_address,
        full_name: company_name,
        logo: comapny_logo,
        function: function_name,
      } = respDataReport;

      formDataReportValue = {
        ...formDataReportValue,
        comapny_logo,
        company_address,
        company_name,
        function_name,
      };

      if (respDataReport) {
        this.setState(
          { ...this.state, reportFormData: formDataReportValue },
          () =>
            this.setState({
              ...this.state,
              isShowBunkerPurchasedReport: showBunkerPurchasedReport,
            })
        );
      } else {
        openNotificationWithIcon("error", "Unable to show report", 5);
      }
    } catch (error) {
      console.log('error', error);
      openNotificationWithIcon("error", "Something Went wrong.", 3);
    }

  }else{

    this.setState({
      ...this.state,
      isShowBunkerPurchasedReport: showBunkerPurchasedReport,
    })

  }
  };

  saveFormData = async (data) => {
    let url = "save";
    let _method = "post";
    if (data && data.hasOwnProperty("id") && data["id"] !== undefined) {
      url = "update";
      _method = "put";
    }
  
    if (data) {
      let newarr = [];
      data.portactivity && data.portactivity.length > 0 && 
        data.portactivity.map((val, ind) => {
          const { port_activity_name, port_cargo_name, ...restval } = val;
          val = { ...restval };
          newarr.push(val);
        });

      data.portactivity = [...newarr];
      data = {
        ...data,
        arrival_lt: data.arrival_lt!==""&&data.arrival_lt !=='Invalid date'? data.arrival_lt:'',
        departure_lt: data.departure_lt!==""&&data.departure_lt !=='Invalid date'? data.departure_lt:'',
        tsd: data.tsd && !isNaN(data.tsd) ? data.tsd : 0,
        tpd: data.tpd && !isNaN(data.tpd) ? data.tpd : 0,
        pre_date_dep:data.pre_date_dep?data.pre_date_dep:moment(),
        bunk_id:this.state.bunk_id
      };

      let _url = `${URL_WITH_VERSION}/port-bunker-activity/${url}?frm=${
        this.state.frmName
      }`;
   
      await postAPICall(`${_url}`, data, `${_method}`, (response) => {
        if (response && response.data == true) {
          openNotificationWithIcon("success", response.message);
          if (typeof this.props.modalCloseEvent == "function") {
           this.props.modalCloseEvent();

            setTimeout(() => {
              window.location.reload();
            }, 3000);
          }
        } else if (response && response.data == false) {
          if (typeof response.message === "string") {
            openNotificationWithIcon("error", response.message);
          } else {
            openNotificationWithIcon("error", "Server Error");
          }
        }
      });
    }
  };

  showHideModal = (visible, modal) => {
    const { modals } = this.state;
    let _modal = {};
    _modal[modal] = visible;
    this.setState({
      ...this.state,
      modals: Object.assign(modals, _modal),
    });
  };
  render() {
    const {
      frmVisible,
      frmName,
      frmOptions,
      formData,
      isShowBunkerPurchasedReport,
      reportFormData,
    } = this.state;
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article">
          <div className="box box-default">
            <div className="box-body">
              {frmVisible ? (
                <NormalFormIndex
                  key={"key_" + frmName + "_0"}
                  formClass="label-min-height"
                  formData={formData}
                  showForm={true}
                  frmCode={frmName}
                  frmOptions={frmOptions}
                  addForm={true}
                  showToolbar={[
                    {
                      isLeftBtn: [
                        {
                          isSets: [
                            {
                              id: "3",
                              key: "save",
                              type: <SaveOutlined />,
                              withText: "Save",
                              showToolTip: true,
                              event: (key, data) => this.saveFormData(data),
                            },
                          ],
                        },
                      ],
                      isRightBtn: [
                        {
                          isSets: [
                            {
                              key: "CargoHandling",
                              isDropdown: 0,
                              withText: "Cargo Handling",
                              type: "",
                              menus: null,
                              event: (key) =>
                                this.showHideModal(true, "Cargo Handling"),
                            },
                            {
                              key: "BunkerPurchasedReport",
                              isDropdown: 0,
                              withText: "Report",
                              type: "",
                              menus: null,
                              event: (key) => this.BunkerPurchasedReport(true),
                            },
                          ],
                        },
                      ],
                    },
                  ]}
                  inlineLayout={true}
                  // showSideListBar={null}
                />
              ) : (
                undefined
              )}
            </div>
          </div>
        </article>

        {isShowBunkerPurchasedReport ? (
          <Modal
            className="page-container"
            style={{ top: "2%" }}
            title="Reports"
           open={isShowBunkerPurchasedReport}
            onOk={this.handleOk}
            onCancel={() => this.BunkerPurchasedReport(false)}
            width="95%"
            footer={null}
          >
            <PortBunkerActivityReport data={reportFormData} />
          </Modal>
        ) : (
          undefined
        )}
      </div>
    );
  }
}

export default PortActivityDetail;
