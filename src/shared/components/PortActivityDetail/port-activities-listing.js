import React, { Component } from 'react';
import { Table, Popconfirm, Input,  Button } from 'antd';
import { SaveOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons';
// Start table section
const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        activity: "Activity",
        at: "AT",
        cargo: "Cargo",
        bl_code: "BL Code",
        remarks: "Remarks",
        date_from: "Date From",
        time: "Time",
    });
}

const EditableCell = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);
// End table section

class PortActvitiesLisiting extends Component {

    constructor(props) {
        super(props);
        // Start table section
        this.columns = [{
            title: 'Activity',
            dataIndex: 'activity',
            width: 150,
            render: (text, record) => this.renderColumns(text, record, 'activity'),
        },
        {
            title: 'AT',
            dataIndex: 'at',
            width: 30,
            render: (text, record) => this.renderColumns(text, record, 'at'),
        },

        {
            title: 'Cargo',
            dataIndex: 'cargo',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'cargo'),
        },
        {
            title: 'BL Code',
            dataIndex: 'bl_code',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'bl_code'),
        },

        {
            title: 'Remarks',
            dataIndex: 'remarks',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'remarks'),
        },
        {
            title: 'Date From',
            dataIndex: 'date_from',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'date_from'),
        },
        {
            title: 'Time',
            dataIndex: 'time',
            width: 100,
            render: (text, record) => this.renderColumns(text, record, 'time'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            width: 100,
            // fixed: 'right',
            render: (text, record) => {
                const { editable } = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <span className="iconWrapper save" onClick={() => this.save(record.key)}><SaveOutlined /></span>
                                    <span className="iconWrapper cancel">
                                        <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                        <DeleteOutlined />
                                        </Popconfirm>
                                    </span>
                                </span>
                                : <span className="iconWrapper edit" onClick={() => this.edit(record.key)}><EditOutlined /></span>
                        }
                    </div>
                );
            },
        }];
        this.state = { data };
        this.cacheData = data.map(item => ({ ...item }));
        // End table section
    }

    // Start table section
    renderColumns(text, record, column) {
        return (
            <EditableCell
                editable={record.editable}
                value={text}
                onChange={value => this.handleChange(value, record.key, column)}
            />
        );
    }

    handleChange(value, key, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.setState({ data: newData });
        }
    }

    edit(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({ data: newData });
        }
    }

    save(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            delete target.editable;
            this.setState({ data: newData });
            this.cacheData = newData.map(item => ({ ...item }));
        }
    }

    cancel(key) {
        const newData = [...this.state.data];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
    }
    // End table section

    render() {
        return (
            <>

                <Table
                    bordered
                    dataSource={this.state.data}
                    columns={this.columns}
                    scroll={{ x: 'max-content' }}
                    size="small"
                    pagination={false}
                    footer={() => <div className="text-center">
                        <Button type="link">Add New</Button>
                    </div>
                    }
                    rowClassName={(r, i) =>
                        i % 2 === 0 ? 'table-striped-listing' : 'dull-color table-striped-listing'
                    }
                />
            </>
        )
    }
}

export default PortActvitiesLisiting;