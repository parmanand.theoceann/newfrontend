import React, { Component } from 'react';
import { Form, Input, Table, Icon, Select } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const dataSourcePay = [
    {
        key: "1",
        invoiceno: "Invoice No",
        date: "Date",
        remarks: "Remarks",
        commission: "Commission",
        st: "ST",
    },
    {
        key: "2",
        invoiceno: "Invoice No",
        date: "Date",
        remarks: "Remarks",
        commission: "Commission",
        st: "ST",
    },
    {
        key: "3",
        invoiceno: "Invoice No",
        date: "Date",
        remarks: "Remarks",
        commission: "Commission",
        st: "ST",
    },
    {
        key: "4",
        invoiceno: "Invoice No",
        date: "Date",
        remarks: "Remarks",
        commission: "Commission",
        st: "ST",
    },
    {
        key: "5",
        invoiceno: "Invoice No",
        date: "Date",
        remarks: "Remarks",
        commission: "Commission",
        st: "ST",
    },
];

const columnsPay = [
    {
        title: 'Invoice No',
        dataIndex: 'invoiceno',
        key: 'invoiceno',
    },
    {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'Remarks',
        dataIndex: 'remarks',
        key: 'remarks',
    },
    {
        title: 'Commission',
        dataIndex: 'commission',
        key: 'commission',
    },
    {
        title: 'St',
        dataIndex: 'st',
        key: 'st',
    },
];

function onVessel(value) {
    // console.log(`selected ${value}`);
}

class TCCommissionSummary extends Component {
    render() {
        return (
            <>
                <div className="body-wrapper modalWrapper">

                    <article className="article toolbaruiWrapper">
                        <div className="box box-default">
                            <div className="box-body">
                                <div className="toolbar-ui-wrapper">
                                    <div className="leftsection">
                                        <span key="first" className="wrap-bar-menu">
                                            <ul className="wrap-bar-ul">
                                                <li><span className="text-bt"><Icon type="file-excel" /></span></li>
                                                <li><span className="text-bt"><Icon type="sync" /></span></li>
                                            </ul>
                                        </span>
                                    </div>
                                    <div className="rightsection">
                                        <span className="wrap-bar-menu">
                                            <ul className="wrap-bar-ul">
                                                <li><span className="text-bt"><Icon type="file-text" /> New Payment</span></li>
                                            </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article className="article">
                        <div className="box box-default">
                            <div className="box-body">
                                <Form>

                                    <div className="row">
                                        <div className="col-md-4">
                                            <FormItem label="Vessel">
                                                <InputGroup compact>
                                                    <Select style={{ width: '60%' }} defaultValue="Option One" onChange={onVessel}>
                                                        <Option value="Option One">Option One</Option>
                                                        <Option value="Option Two">Option Two</Option>
                                                    </Select>
                                                    <Input style={{ width: '40%' }} defaultValue="" />
                                                </InputGroup>
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="TC Code">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="Broker">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-4">
                                            <FormItem label="Currency">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>
                                    </div>

                                    <hr />

                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="table-info-wrapper">
                                                <Table size="small" bordered pagination={false} columns={columnsPay} dataSource={dataSourcePay} />
                                            </div>
                                        </div>
                                    </div>

                                </Form>

                            </div>
                        </div>
                    </article>

                </div>
            </>
        )
    }
}

export default TCCommissionSummary;