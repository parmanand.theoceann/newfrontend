import React, { Component } from 'react';
import { Form, Input, Table, Icon, DatePicker } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const { TextArea } = Input;

const dataSourcePay = [
    {
        key: "1",
        description: "Description",
        periodfromgmt: "Period From (GMT)",
        periodtogmt: "Period To (GMT)",
        tcamtusd: "TC Amt (USD)",
        rate: "Rate",
        commusd: "Comm (USD)",
        c: "C"
    },
    {
        key: "2",
        description: "Description",
        periodfromgmt: "Period From (GMT)",
        periodtogmt: "Period To (GMT)",
        tcamtusd: "TC Amt (USD)",
        rate: "Rate",
        commusd: "Comm (USD)",
        c: "C"
    },
    {
        key: "3",
        description: "Description",
        periodfromgmt: "Period From (GMT)",
        periodtogmt: "Period To (GMT)",
        tcamtusd: "TC Amt (USD)",
        rate: "Rate",
        commusd: "Comm (USD)",
        c: "C"
    },
    {
        key: "4",
        description: "Description",
        periodfromgmt: "Period From (GMT)",
        periodtogmt: "Period To (GMT)",
        tcamtusd: "TC Amt (USD)",
        rate: "Rate",
        commusd: "Comm (USD)",
        c: "C"
    },
    {
        key: "5",
        description: "Description",
        periodfromgmt: "Period From (GMT)",
        periodtogmt: "Period To (GMT)",
        tcamtusd: "TC Amt (USD)",
        rate: "Rate",
        commusd: "Comm (USD)",
        c: "C"
    },
];

const columnsPay = [
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: 'Period From (GMT)',
        dataIndex: 'periodfromgmt',
        key: 'periodfromgmt',
    },
    {
        title: 'Period To (GMT)',
        dataIndex: 'periodtogmt',
        key: 'periodtogmt',
    },
    {
        title: 'TC Amt (USD)',
        dataIndex: 'tcamtusd',
        key: 'tcamtusd',
    },
    {
        title: 'Rate',
        dataIndex: 'rate',
        key: 'rate',
    },
    {
        title: 'Comm (USD)',
        dataIndex: 'commusd',
        key: 'commusd',
    },
    {
        title: 'C',
        dataIndex: 'c',
        key: 'c',
    },
];

class TCCommissionPayment extends Component {
    render() {
        return (
            <>
                <div className="body-wrapper modalWrapper">

                    <article className="article toolbaruiWrapper">
                        <div className="box box-default">
                            <div className="box-body">
                                <div className="toolbar-ui-wrapper">
                                    <div className="leftsection">
                                        <span key="first" className="wrap-bar-menu">
                                            <ul className="wrap-bar-ul">
                                                <li><span className="text-bt"><SaveOutlined /> Save</span></li>
                                                <li><span className="text-bt"><CloseOutlined /></span></li>
                                            </ul>
                                        </span>
                                    </div>
                                    <div className="rightsection">
                                        <span className="wrap-bar-menu">
                                            <ul className="wrap-bar-ul">
                                                <li><span className="text-bt"><Icon type="file-text" /> Report</span></li>
                                            </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article className="article">
                        <div className="box box-default">
                            <div className="box-body">
                                <Form>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <FormItem label="Vessel">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="TC Code">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="Broker">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-4">
                                            <FormItem label="Inv No">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="Date">
                                                <DatePicker />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="Currency">
                                                <Input size="default" placeholder="USD" />
                                            </FormItem>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-4">
                                            <FormItem label="Exchange Rate">
                                                <Input size="default" placeholder="" />
                                            </FormItem>
                                        </div>

                                        <div className="col-md-4">
                                            <FormItem label="Remarks">
                                                <TextArea placeholder="" autoSize={{ minRows: 1, maxRows: 3 }} />
                                            </FormItem>
                                        </div>
                                    </div>

                                    <hr />

                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="table-info-wrapper">
                                                <Table size="small" bordered pagination={false} columns={columnsPay} dataSource={dataSourcePay} />
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </article>

                </div>
            </>
        )
    }
}

export default TCCommissionPayment;