import React, { Component } from 'react';
import { Form, Input, DatePicker, Radio } from 'antd';

const FormItem = Form.Item;

class ReceiptInstructions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 1,
        }
    }

    onChange = e => {
        // console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    };

    render() {
        return (
            <div className="body-wrapper modalWrapper">
                <article className="article toolbaruiWrapper">
                    <div className="box box-default">
                        <div className="box-body">
                            <Form>
                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Company">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Bank">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Vendor">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4">
                                        <FormItem label="Date">
                                            <DatePicker />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Invoice">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>

                                    <div className="col-md-4">
                                        <FormItem label="Curr">
                                            <Input size="default" placeholder="" />
                                        </FormItem>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        <Radio.Group onChange={this.onChange} value={this.state.value}>
                                            <div className="row">
                                                <div className="col-md-4">
                                                    <Radio value={1}>Received Check</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={2}>Wire Transfer In</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={3}>Bank Credit Memo</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={4}>Other Credit</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={5}>Write Off</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={6}>Write Off</Radio>
                                                </div>
                                                <div className="col-md-4">
                                                    <Radio value={7}>Vendor to vendor transaction</Radio>
                                                </div>
                                            </div>
                                        </Radio.Group>

                                    </div>
                                </div>
                            </Form>
                        </div>
                    </div>
                </article>
            </div>
            
        )
    }
}

export default ReceiptInstructions;