import React from 'react';
import { Select, Spin } from 'antd';
import debounce from 'lodash/debounce';
import URL_WITH_VERSION, { getAPICall } from '../../shared';

const { Option } = Select;

class Autosuggest extends React.Component {
    constructor(props) {
        super(props);
        this.fetchUser = debounce(this.fetchUser, 800);
        this.state = { data: [], fetching: false }
    }

    fetchUser = async (search) => {
        let state = { data: [], fetching: false };
        if (search.length > 2) {
            let headers = {};
            headers["order_by"] = { [this.props.orderBy]: "desc" };
            headers['where'] = { [this.props.searchBy]: { "l": search } };

            this.setState({ ...this.state, fetching: true, data: [] });

            let _url = `${URL_WITH_VERSION}/${this.props.apiLink}`;
            const response = await getAPICall(_url, headers);
            const responesData = await response;

            let data = (responesData && responesData.data ? responesData.data : []);
            state = { data, fetching: false };
        }
        this.setState({
            ...this.state,
            ...state
        });
    };

    handleChange = (e) => {
        if (this.props.onRowSelect)
            this.props.onRowSelect(e)
    }

    render() {
        const { fetching, data } = this.state;
        return (
            <div className="body-wrapper">
                <article className="article">
                    <div className="box box-default">
                        <div className="box-body row">
                            <div className="col-lg-12">
                                <Select
                                    showSearch
                                    placeholder={"Search Vessel Name"}
                                    defaultActiveFirstOption={false}
                                    showArrow={false}
                                    filterOption={false}
                                    onSearch={this.fetchUser}
                                    onChange={(index, obj) => this.handleChange({ value: index, label: obj.props.children })}
                                    notFoundContent={fetching ? <Spin size="small" /> : null}
                                >
                                    {data.map(d => (
                                        <Option key={d.vessel_id}>{d.vessel_name}</Option>
                                    ))}
                                </Select>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        );
    }
}
export default Autosuggest;