import React, { Component } from 'react'
import {
  CBadge,
  CCol,
  CButton,
  CRow,
  CInput,
  CTooltip,
  CSelect,
  CLink,
  CImg,
  CPagination,
  CSpinner,
  CModal,
  CModalBody,
  CModalFooter
} from '@coreui/react'
import { CALL_MASTER_API, GET_STATUS_BADGE, GET_STATUS_TEXT, FREIGHT_RATE } from '../../shared';
import { SaveOutlined, ExportOutlined,UploadOutlined } from '@ant-design/icons';
import TABLE_FIELDS from '../tableFields';
import ModalAlertBox from '../ModalAlertBox';

let fields = Object.assign([], TABLE_FIELDS);
let lastScrollY = 0;
let ticking = false;

class Dyc extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      data: [],
      currentPage: 1,
      pageLimit: 10,
      totalPages: 0,
      loading: true,
      tableFields: [],
      isPagination: false,
      pageWidths: null,
      tableKey: null,
      apiKey: null,
      addNewField: false,
      children: null,
      callback: null,
      modalData: null,
      saveBtnDisabled: true,
      postSaveData: {},
      onModalOpen: false,
      modalStatus: false,
      modalBody: null,
      modalFooter: null,
      modalColor: null,
      saveData: { 'term_name': null, 'rate': null, 'type': null, 'status': 1 }
    }
    this.previous = {};
    this.modalStatus = false;
  }

  componentDidMount() {
    this.setScrollingSetting();
    this.getTableData();
    // console.log("compoenetdidmount")
    setTimeout(() => this.setFieldsForm(), 1000);
  }

  componentWillUnmount() {
    this.resetScrolling();
  }

  componentDidUpdate(prevProps) {
    let currentProps = this.props;
    if (prevProps.tableKey !== currentProps.tableKey) {
      let currentProps = (currentProps && currentProps.tableKey ? fields[currentProps.tableKey] : []);
      let tableFields = (fields && currentProps && currentProps["tableheads"] ? currentProps["tableheads"] : []);
      this.setState({
        tableKey: currentProps.tableKey,
        data: [],
        currentPage: 1,
        pageLimit: 10,
        totalPages: 0,
        children: currentProps.children,
        callback: currentProps.callback,
        apiKey: (fields && currentProps && currentProps["key"] ? currentProps["key"] : null),
        pageWidths: (fields && currentProps && currentProps["pageWidths"] ? currentProps["pageWidths"] : null),
        tableFields,
        postSaveData: {}
      }, () => {
        this.previous = {};
        this.resetScrolling();
        this.setScrollingSetting();
        this.getTableData();
        // console.log("componentDidUpdate")
        this.setFieldsForm();
      })
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props && props.tableKey && !state.tableKey) {
      let currentProps = (props && props.tableKey ? fields[props.tableKey] : []);
      let tableFields = (fields && currentProps && currentProps["tableheads"] ? currentProps["tableheads"] : []);

      return {
        tableKey: props.tableKey,
        children: props.children,
        callback: props.callback,
        isPagination: (fields && currentProps && currentProps["isPagination"] ? currentProps["isPagination"] : false),
        apiKey: (fields && currentProps && currentProps["key"] ? currentProps["key"] : null),
        pageWidths: (fields && currentProps && currentProps["pageWidths"] ? currentProps["pageWidths"] : null),
        tableFields
      }
    }
    return null;
  }

  resetScrolling = () => {
    lastScrollY = 0;
    ticking = false;
    window.removeEventListener('scroll', this.handleScroll);
  }

  setScrollingSetting = () => {
    if (!this.state.isPagination) {
      let table = document.getElementById("scrollbarTable");
      table.addEventListener('scroll', this.handleScroll, true);
    }
  }

  handleScroll = (evt) => {
    let scrollVal = evt.target.scrollTop
    if (!ticking && scrollVal >= lastScrollY) {
      window.requestAnimationFrame(() => {
        lastScrollY = window.scrollY;
        ticking = false;
        const { currentPage, totalPages, data } = this.state;

        if (totalPages > data.length)
          this.getTableData(currentPage + 1);
      });
      ticking = true;
    }
  };

  getTableData = (pageIndex = 1) => {
    let qParams = {};
    const { apiKey, pageLimit, isPagination } = this.state;
    qParams['p'] = pageIndex;
    qParams['l'] = pageLimit;

    if (apiKey) {
      CALL_MASTER_API('get', apiKey, null, qParams, (data) => {
        const totalRows = data && data.total_rows ? data.total_rows : 0;
        let dataArr = (data && data.data ? data.data : []);
        if (dataArr.length > 0 && (totalRows > this.state.data.length)) {
          // console.log("getabledata")
          this.setState({
            ...this.state,
            data: (!isPagination ? [...this.state.data, ...dataArr] : dataArr),
            totalPages: totalRows,
            totalPaginationPages: (Math.ceil(totalRows / pageLimit)),
            currentPage: pageIndex,
            loading: false,
            addNewField: false,
            postSaveData: {}
          });
        }
      })
    }
  }

  onUpdateField = (evt, field, regex = null) => {
    let value = evt.target.value,
      saveBtnDisabled = false,
      { postSaveData } = this.state,
      check = (regex ? true : false);
    // console.log(postSaveData);
    var checkerRegex = new RegExp(regex);
    if (check && !checkerRegex.test(value)) {
      return;
    }


    postSaveData[field] = value
    Object.keys(postSaveData).map(item => {
      // console.log(postSaveData[item]);
      if (!["id"].includes(item) && postSaveData[item] === null && postSaveData[item] === undefined)
        saveBtnDisabled = true
    })

    this.setState({
      ...this.state,
      postSaveData,
      saveBtnDisabled
    })
  }

  rowClick = (event, data, index) => {
    // console.log("rowClick");
    const target = event.target;

    if (index !== this.previous.index && this.previous.hasOwnProperty("index")) {
      this.onRowCancelClick(this.previous.event, this.previous.index)
    }
    this.previous = { "event": event, "index": index };

    if (
      (target.tagName !== 'BUTTON' && !['group-buttons', 'action-btn'].includes(target.className))
      &&
      (target.tagName !== 'A' && !['cancelClick'].includes(target.className))
    ) {
      let ele = document.getElementById("tr-" + index);
      let ele1 = ele.getElementsByClassName("bview")
      let ele2 = ele.getElementsByClassName("form-control");
      let btns = ele.getElementsByClassName("sc-btns")
      let dbtn = ele.getElementsByClassName("d-btns")
      // console.log("rowclick")
      this.setFieldsForm(data)

      for (let i = 0; i < ele1.length; i++) {
        if (ele1[i]) {
          ele1[i].className = "bview hidden"
        }
        if (ele2[i]) {
          ele2[i].className = "form-control"
        }
      }
      for (let i = 0; i < btns.length; i++) {
        if (btns[i]) { btns[i].className = "sc-btns" }
        if (dbtn[i]) { dbtn[i].className = "d-btns hidden" }
      }
    }
  }

  onRowSaveClick = (event) => {
    event.preventDefault();
    let { apiKey, postSaveData } = this.state,
      isFilled = true;

    let postData = Object.assign({}, postSaveData);
    Object.keys(postData).map(item => {
      if (postData[item] === null && postData[item] === undefined && !["id"].includes(item))
        isFilled = false
    })

    if (isFilled) {
      let method = "post";
      if (postData && postData.id) method = "put";
      else delete postData["id"];

      this.setState({
        ...this.state,
        loading: true
      })

      CALL_MASTER_API(method, apiKey, postData, null, (data) => {
        if (data && data.data) {
          this.setState({
            ...this.state,
            data: [],
            loading: true
          }, () => {
            if (this.previous.index && this.previous.hasOwnProperty("index")) {
              this.onRowCancelClick(this.previous.event, this.previous.index);
            }
            // console.log("onrowsaveclick")
            this.setFieldsForm()
            this.getTableData();
          });
        }
      })
    }
  }

  onRowCancelClick = (event, index, newEntry = false) => {
    event.preventDefault()
    let ele = document.getElementById("tr-" + index);
    if (ele) {
      let ele1 = ele.getElementsByClassName("bview")
      let ele2 = ele.getElementsByClassName("form-control");
      let btns = ele.getElementsByClassName("sc-btns")
      let dbtn = ele.getElementsByClassName("d-btns")

      for (let i = 0; i < ele1.length; i++) {
        if (ele1[i]) {
          ele1[i].className = "bview"
        }
        if (ele2[i]) {
          ele2[i].className = "form-control hidden"
        }
      }
      for (let i = 0; i < btns.length; i++) {
        if (btns[i]) { btns[i].className = "sc-btns hidden" }
        if (dbtn[i]) { dbtn[i].className = "d-btns" }
      }
    }

    if (newEntry) {
      this.setState({
        ...this.state,
        addNewField: false
      })
    }
  }

  toggle = (e) => {
    let { tableFields, callback, addNewField } = this.state;
    tableFields.map(item => item["visible"] = true);
    if (callback)
      callback("unhideAllFields", true);

    // console.log("toggle")
    this.setState({
      ...this.state,
      tableFields,
      // postSaveData: {},
      addNewField: !addNewField
    });
  }

  setCurrentPage = (pageIndex) => {
    this.getTableData(pageIndex)
  }

  getstartingNumber = (index) => {
    const { pageLimit, currentPage } = this.state;
    return (currentPage > 1 ? (currentPage - 1) : 0) * pageLimit + index
  }

  onTriggerCallback = (item) => {
    let { callback } = this.state;
    if (callback)
      callback("openModal", item);
  }

  setFieldsForm = (data = null) => {
    // console.log("setfieldform")
    let postSaveDataObj = {},
      saveBtnDisabled = false,
      { tableFields, postSaveData } = this.state;

    if (data) {
      Object.keys(data).map(v => {
        if (!["status"].includes(v)) {
          postSaveDataObj[v] = (data && (data[v] || data[v] === 0) ? data[v] : null)

          if (postSaveDataObj[v] === null && postSaveDataObj[v] === undefined)
            saveBtnDisabled = true
        }
      })
    } else {
      postSaveDataObj["id"] = null
      tableFields.map(v => {
        if (!["action", "sno"].includes(v.key)) {
          postSaveDataObj[v.key] = (
            postSaveData && postSaveData.hasOwnProperty(v.key) && postSaveData[v.key]
              ? postSaveData[v.key]
              : (data && (data[v.key] || data[v.key] === 0) ? data[v.key] : null)
          )
          if (postSaveDataObj[v.key] === null && postSaveDataObj[v.key] === undefined)
            saveBtnDisabled = true
        }
      })
    }

    // console.log("postSaveDataObj", postSaveDataObj)
    this.setState({
      ...this.state,
      postSaveData: postSaveDataObj,
      saveBtnDisabled
    }, () => console.log("posssomg", this.state.postSaveData))
  }

  modalFooterDeleteBtn = () => {
    let { modalStatus } = this.state;
    return (
      <>
        <CButton
          color="primary"
          onClick={(evt) => this.onRowDeletedClick(evt)}
        >Ok</CButton>
        <CButton
          color="secondary"
          onClick={() => this.changeModalStatus(!modalStatus)}
        >Cancel</CButton>
      </>
    )
  }

  onRowDeletedClick = (event) => {
    if (this.previous && this.previous.hasOwnProperty("index")) {
      let postData = {},
        { data, apiKey } = this.state;
      this.closeOpenModal();
      if (data.length > 0 && data[this.previous.index]) {
        postData["id"] = data[this.previous.index]['id'];

        CALL_MASTER_API('delete', apiKey, postData, null, (data) => {
          if (data && data.data) {
            this.setState({
              ...this.state,
              data: [],
              loading: true,
              modalBody: data.message,
              modalStatus: true,
              modalFooter: null,
              modalColor: "success",
              modalHeader: "Success"
            }, () => {
              this.modalStatus = false;
              this.previous = {};
              this.getTableData()
              setTimeout(() => this.closeOpenModal(), 3000)
            });
          }
        })
      }
    }
  }

  changeModalStatus = (status, index = null) => {
    this.setState({
      ...this.state,
      modalStatus: status,
      modalHeader: "Delete Confirmation",
      modalBody: "Are you sure you want to delete it?",
      modalColor: "danger",
      modalFooter: this.modalFooterDeleteBtn
    });
    this.previous = { ...this.previous, "deletedKey": index }
    // this.modalStatus = status;
  }

  closeOpenModal = (status) => {
    this.setState({
      ...this.state,
      modalHeader: null,
      modalBody: null,
      modalFooter: null,
      modalStatus: status,
      modalColor: null
    })
  }

  render() {
    const { data, currentPage, tableFields, pageWidths, totalPaginationPages, addNewField, children, isPagination, saveBtnDisabled, loading, modalStatus, modalBody, modalFooter, modalHeader, modalColor } = this.state;
    // default settings
    let columns = {}
    if (tableFields.length > 0)
      tableFields.map(item => columns[item.key] = item.field);
    //end default setting

    const showField = (value, key, index, newEntry = false) => {
      let fieldName = "";

      tableFields.map(item => {
        if (item.key === key) {
          fieldName = item.field
        }
      });
      if (!["action", "sno", "status"].includes(key)) {
        let { postSaveData } = this.state;
        let _value = (postSaveData && postSaveData.hasOwnProperty(key) && postSaveData[key] ? postSaveData[key] : value)
        return (
          <CInput
            type="text"
            id={"fi-" + key}
            name={key}
            className={!newEntry ? "hidden" : null}
            required
            placeholder={"Enter " + fieldName}
            defaultValue={_value}
            onChange={evt => this.onUpdateField(evt, key)}
          />
        )
      } else if (key === "status") {
        return (
          <>
            <CSelect
              custom
              name={key}
              autoComplete={key}
              className={!newEntry ? "form-control hidden" : "form-control "}
              id={"fi-" + key}
              required
              placeholder={"Select " + fieldName}
              defaultValue={value}
              onChange={evt => this.onUpdateField(evt, key)}
            >
              <option value="1">Active</option>
              <option value="0">Inactive</option>
              <option value="3">Deleted</option>
            </CSelect>
          </>
        )
      } else if (key === "sno") {
        return (isPagination ? this.getstartingNumber(index + 1) : (index + 1));
      } else {
        return <div className="group-buttons">
          <span className={!newEntry ? "sc-btns hidden" : "sc-btns "}>
            <CButton
              size={"sm"}
              onClick={(event) => this.onRowSaveClick(event)}
              color={"primary"}
              disabled={saveBtnDisabled}
              className="m-0 mr-1"
            >Save</CButton>
            <CButton
              size={"sm"}
              onClick={(event) => this.onRowCancelClick(event, index, newEntry)}
              color={"danger"}
              className="m-0"
            >Cancel</CButton>
          </span>
          <span className={newEntry ? "d-btns hidden" : "d-btns "}>
            <CButton
              size={"sm"}
              color={"danger"}
              className="mt-0"
              onClick={(evt) => this.changeModalStatus(!modalStatus, index)}
            > Delete </CButton>
          </span>
        </div>
      }
    }

    const showNewAddField = (field, data = []) => {
      if (field.length > 0) {
        let key = data.length;
        // console.log(this.state.postSaveData)
        // Remove Previous Row Click Data Fields
        if (this.previous.hasOwnProperty("index")) {
          this.onRowCancelClick(this.previous.event, this.previous.index);
          this.previous = {};
          // console.log("shownewaddfield")
          this.setFieldsForm();
        }
        // end

        return (
          <tr key={key} id={"tr-" + key}>
            {
              tableFields.map((f, fi) => {
                return (
                  <td key={fi} style={f._style} className={f.key === "action" ? "action-btn" : ""}>
                    {showField(null, f.key, key, true)}
                  </td>
                )
              })
            }
          </tr>
        )
      }
    }

    return (
      <>
        <CRow>
          <CCol
            xs={pageWidths && pageWidths.xs ? pageWidths.xs : "12"}
            sm={pageWidths && pageWidths.sm ? pageWidths.sm : "12"}
            md={pageWidths && pageWidths.md ? pageWidths.md : "12"}
            lg={pageWidths && pageWidths.lg ? pageWidths.lg : "12"}
          >
            <CCol xs="12" sm="12" md="12" lg="12" className={!isPagination ? "mt-2 mb-2" : ""}>
              <div className="table-filter-wrapper">
                <div className="section">
                  <CTooltip content="Save">
                    <CButton variant="outline" color="secondary" size="sm" className="ml-1">
                    <SaveOutlined />
                    </CButton>
                  </CTooltip>
                  <CTooltip content="Import">
                    <CButton variant="outline" color="secondary" size="sm" className="ml-1">
                    <UploadOutlined/>
                    </CButton>
                  </CTooltip>

                  <CTooltip content="Export">
                    <CButton variant="outline" color="secondary" size="sm" className="ml-1">
                      <ExportOutlined/>
                    </CButton>
                  </CTooltip>
                </div>
                {
                  isPagination ? (
                    <div className="section">
                      <CPagination
                        activePage={currentPage}
                        pages={totalPaginationPages}
                        align="end"
                        className="mt-3"
                        onActivePageChange={(i) => this.setCurrentPage(i)}
                      />
                    </div>
                  ) : null
                }
              </div>
            </CCol>

            <CCol xs="12" sm="12" lg="12">
              <CCol className={loading ? "loader-table" : "hidden"}>
                <CSpinner
                  color="primary"
                  style={{ width: '4rem', height: '4rem' }}
                />
              </CCol>
              <CCol className="table-auto-scroll" id="scrollbarTable">
                <form id="master-frm">
                  <table width="100%" className="table-fixed inline-table-edit table table-striped table-bordered">
                    <thead className="thead-light fixedHeader">
                      <tr>
                        {(tableFields.length > 0) ?
                          tableFields.map((tr, ti) => {
                            if (tr.visible === true || tr.visible === undefined) {
                              return <th key={ti} style={tr._style} align={tr.align}>{tr.field}</th>
                            }
                          }) : null
                        }
                      </tr>
                    </thead>
                    <tbody className="scrollContent" style={{ maxHeight: 500 }}>
                      {
                        data.length > 0 ?
                          <>
                            {
                              data.map((d, i) => {
                                return (
                                  <tr key={i} id={"tr-" + i} onClick={(event) => this.rowClick(event, d, i)}>
                                    {
                                      (tableFields.length > 0) ?
                                        tableFields.map((f, fi) => {
                                          if (f.visible === true || f.visible === undefined) {
                                            return (
                                              <td key={fi} style={f._style} className={f.key === "action" ? "action-btn" : ""}>
                                                {
                                                  f.isModalClickable ? (
                                                    <span className="bview">
                                                      <CLink className="cancelClick" onClick={() => this.onTriggerCallback(true)}>
                                                        {d[f.key]}
                                                      </CLink>
                                                    </span>
                                                  ) : (
                                                    <span className="bview">{
                                                      (f.key === "status") ? (
                                                        <div className="status-btn">
                                                          <CBadge color={GET_STATUS_BADGE(d[f.key])}>
                                                            {GET_STATUS_TEXT(d[f.key])}
                                                          </CBadge>
                                                        </div>
                                                      ) : (f.key !== "sno") ? d[f.key] : null
                                                    }</span>
                                                  )
                                                }
                                                {showField(d[f.key], f.key, i)}
                                              </td>
                                            )
                                          }
                                        }) : null
                                    }
                                  </tr>
                                )
                              })
                            }
                          </>
                          : null
                      }
                    </tbody>
                  </table>
                </form>
              </CCol>
              <table width="100%" className="table-fixed table table-striped table-bordered inline-table-edit reduce-width">
                {addNewField ?
                  (<tbody className={data.length > 9 ? "add-new-row-body" : ""}>
                    {showNewAddField(tableFields, data)}
                  </tbody>
                  ) : null}
                <tfoot>
                  <tr>
                    <th className="mt-2 pt-2 table-btn" colSpan={tableFields.length} style={{ textAlign: "center", width: "100%" }}>
                      <CLink
                        disabled={addNewField}
                        onClick={(event) => this.toggle(event)}
                      >
                        Add New
                      </CLink>
                    </th>
                  </tr>
                </tfoot>
              </table>
            </CCol>
            <ModalAlertBox modalHeader={modalHeader} modalBody={modalBody} modalStatus={modalStatus} modalFooter={modalFooter} modalColor={modalColor} closeOpenModal={(evt) => this.closeOpenModal(evt)} />
          </CCol>
          {children}
        </CRow>
      </>
    )
  }
}

export default Dyc
