class DefaultValidate {
    constructor(rows_info) {
        this.rows_info = rows_info;
        this._decimalRegex = new RegExp('([0-9]*\.[0-9]+|[0-9]+)')
        this._numRegex = new RegExp('([0-9]*)')
        this._strRegex = new RegExp('[^@.#]*')
    }

    // /*
    // Mehtod: Validation Required
    // @type f_value: string | number | boolean | alphanumeric | decimal | etc
    // @param f_value: Column value of form under processing
    // @type v_type: int
    // @param v_type: Value type is either integer or others ( 1 for Interger OR 0 for Others) 
    // */
    // // _validateRequired = (f_value, f_name, f_req, v_type = 0) => {
    // //     let val = null;
    // //     let required = true;
    // //     let str_value = String(f_value)

    // //     if (f_req === 1 && v_type === 1){
    // //         if (!f_value || (f_value === 0) || typeof (f_value) !== float || typeof (f_value) !== int)
    // //             required = false
    // //         else if (f_req === 1 && v_type === 0){
    // //             if (!f_value || f_value === '' || str_value.trim() === '' || str_value === 'null')
    // //                 required = False
    // //         }
    // //     }
    // //     if (!required)
    // //         val = `${f_name} is required.`

    // //     return [required, val];
    // // }

    // // _checkMinMaxLen = (min_len, max_len, value) => {
    // //     let returnVal = true;
    // //     if (min_len && (min_len > 0) && max_len && (max_len > 0) && ((value.length) < min_len || (value.length) > max_len))
    // //         returnVal = false
    // //     else if (min_len && (min_len > 0) && (!max_len || max_len === 0) && (value.length) < min_len)
    // //         returnVal = false
    // //     else if ((!min_len || min_len === 0) && max_len && max_len > 0 && (value.length) > max_len)
    // //         returnVal = false
    
    // //     return returnVal
    // // }

    // // _validateInteger = (int_value, f_obj) => {
    // //     let returnVal, 
    // //         val = this._validateRequired(int_value, f_obj['f_name'], f_obj['f_req'], v_type = 1)
    
    // //     let column_value = String(int_value).trim()
    // //     if (!f_obj['f_regex']) {
    // //         regex = new RegExp(f_obj['f_regex'])
    // //         val_str = regex.match(column_value)
    // //     }
    // //     else
    // //         val_str = self._numRegex.match(column_value)
        
    // //     /// /// /// /// /// /// /// /// /// /// /// /// ///
    // //     if(val_str.end() !== (column_value.length) && returnVal)
    // //         returnVal = False
    
    // //     if(returnVal && !this._checkMinMaxLen(f_obj['f_min_len'], f_obj['f_max_len'], column_value))
    // //         returnVal = False
        
    // //     if (!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`
    
    // //     return [returnVal, val]
    // // }

    // // _validateDecimal = (decimal_value, f_obj) => {
    // //     let returnVal, 
    // //         val = this._validateRequired(decimal_value, f_obj['f_name'], f_obj['f_req'], v_type=1)
    
    // //     let column_value = String(decimal_value).trim()
    // //     if(f_obj['f_regex']){
    // //         regex = new RegExp(f_obj['f_regex'])
    // //         val_str = regex.match(column_value)   
    // //     } else
    // //         val_str = this._decimalRegex.match(column_value)

    // //     ////////////////////////////////////////////////////////
    // //     if(val_str.end() !== len(column_value) && returnVal)
    // //         returnVal = False

    // //     if(returnVal && !this._checkMinMaxLen(f_obj['f_min_len'], f_obj['f_max_len'], column_value))
    // //         returnVal = False

    // //     if(!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`;
    // //     return [returnVal, val]
    // // }

    // // _validateText(text_value, f_obj){
    // //     let returnVal, val = this._validateRequired(text_value, f_obj['f_name'], f_obj['f_req'], v_type=1)
    // //     let column_value = String(text_value).trim()
        
    // //     if(f_obj['f_regex']){
    // //         regex = new RegExp(f_obj['f_regex'])
    // //         column_value = regex.match(column_value)

    // //         ////////////////////////////////////////////////////////////////////////////////
    // //         if(len(column_value.group()) !== (column_value.length))
    // //             column_value = ''
    // //         else
    // //             column_value = column_value.group()
    // //     }
    // //     if((column_value.length) === 0 && returnVal)
    // //         returnVal = False

    // //     if(returnVal && !this._checkMinMaxLen(f_obj['f_min_len'], f_obj['f_max_len'], column_value))
    // //         returnVal = False

    // //     if(!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`;
                
    // //     return [returnVal, val]
    // // }

    // // _validateAlphaNumeric = (alphanum_value, f_obj) => {
    // //     let returnVal, 
    // //         val = this._validateRequired(alphanum_value, f_obj['f_name'], f_obj['f_req'], v_type=1)
    
    // //     let column_value = String(alphanum_value).trim()
    // //     if(f_obj['f_regex']){
    // //         let regex = new RegExp(f_obj['f_regex'])
    // //         column_value = regex.match(column_value)
    // //         ////////////////////////////////////////////////////////////////////////////////
    // //         if(!column_value || (column_value && len(column_value.group()) !== (column_value.length)))
    // //             column_value = False
    // //     } else column_value = this.isAlphaNumeric(column_value)
    
    // //     if(!column_value && returnVal)
    // //         returnVal = False

    // //     if(returnVal && !this._checkMinMaxLen(f_obj['f_min_len'], f_obj['f_max_len'], String(alphanum_value).trim()))
    // //         returnVal = False

    // //     if(!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`;

    // //     return [returnVal, val]
    // // }

    // // isAlphaNumeric = ch => {
    // //     return ch.match(/^[a-z0-9]+$/i) !== null;
    // // }

    // // _validateDate = (date_value, f_obj) => {
    // //     let returnVal, val = this._validateRequired(date_value, f_obj['f_name'], f_obj['f_req'], v_type=1)

    // //     let column_value = String(date_value).trim()
    // //     if(!f_obj['f_regex']){
    // //         regex = new RegExp(f_obj['f_regex']);
    // //         column_value = regex.match(column_value)
    // //         ////////////////////////////////////////////////////////////////////////////////
    // //         if(len(column_value.group()) !== (column_value.length))
    // //             returnVal = False
    // //     } else if(typeof (column_value) is not datetime.date && returnVal)
    // //         returnVal = False
            
    // //     if(!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`;

    // //     return [returnVal, val]
    // // }

    // // _validateTime = (time_value, f_obj) => {
    // //     let returnVal, 
    // //         val = self._validateRequired(time_value, f_obj['f_name'], f_obj['f_req'], v_type=1);

    // //     let time_value = String(time_value).trim()
    // //     if(!f_obj['f_regex']){
    // //         regex = new RegExp(f_obj['f_regex'])
    // //         time_value = regex.match(time_value)
    // //         ////////////////////////////////////////////////////////////////////////////////
    // //         if(len(column_value.group()) !== (time_value.length))
    // //             returnVal = False
    // //     ////////////////////////////////////////////////////////////////////////////////
    // //     } else if(type(time_value) is not datetime.time && returnVal)
    // //         returnVal = False

    // //     if(!returnVal)
    // //         val = `Please enter valid value for ${f_obj['f_name']}.`;


    // //     return returnVal, val
    // // }
}