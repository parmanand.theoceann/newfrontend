
import React, { Component } from 'react';
import { Table} from 'antd';



const columns = [
  {
    title: 'Vessel Name',
    dataIndex: 'v_name',
  },

  {
    title: 'Vessel Code',
    dataIndex: 'v_code',
  },

  {
    title: 'IMO Number',
    dataIndex: 'i_number',
  },

  {
    title: 'TCI Code',
    dataIndex: 't_code',
  },

  {
    title: 'TCOV ID',
    dataIndex: 'tcov_id',
  },
  {
    title: 'Fixture Number',
    dataIndex: 'f_number',
  },
  {
    title: 'Status',
    dataIndex: 'status',
  },
  {
    title: 'Date of Commencement',
    dataIndex: 'date_commencement',
  },
];
const data = [
  {
    key: '1',
    v_name: 'Sea Donna',
    v_code: 'D01535',
    i_number: '9591254',
    t_code: '123',
    tcov_id: '1',
    f_number: '123',
    status: 'Available',
    date_commencement: '12/07/2021',
  },
];

class SearchModal extends Component {
  render() {
    return (
      <div className="body-wrapper modalWrapper">
        <article className="article toolbaruiWrapper">
          <div className="box box-default">
            <div className="box-body">
              
              <div className="row p10">
                <div className="col-md-12">
                  <Table bordered columns={columns} dataSource={data} pagination={false} />
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default SearchModal;

